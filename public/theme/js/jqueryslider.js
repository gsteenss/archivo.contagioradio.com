function loadjqueryslider() {
	$(".active").show();
	$("#slider-div").hover(function(){$(".btn-carousel").show();},function(){$(".btn-carousel").hide();})
	$(".btn-carousel").on('click',function(){
		var id = $(this).attr('id');
		var nav;
		if(id=="previous") {
			nav = $("img.active").prev('img');			
		} else if(id=="next") {
			nav = $("img.active").next('img');			
		}
		if(nav.length == 0) {
			nav = $("img.active");
		}
		else {
			$("img.active").hide();
			$("img.active").removeClass("active");
			nav.addClass("active");
			nav.fadeIn(200);
		}	
	});
};
window.addEventListener('load', loadjqueryslider, false);
