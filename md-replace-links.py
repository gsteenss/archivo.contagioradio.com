#!/usr/bin/python3

import re
import sys
import glob

# article links and attachments
link_attachment = {}
postname_imgs = {}
# fotos for an article
fotos = {}

# load config values
try:
  exec(open('pelicanconf.py').read())
except:
  print('pelicanconf.py not found')

def replace_urls(text, replace_me=None):

    # if specific replace_me urls are given	
	if replace_me: return text.replace(replace_me[0],replace_me[1])

    # else: use img_url_replace & link_url_replace vars
	for replace_url in img_url_replace:	  
	  text=text.replace(replace_url[0],replace_url[1])
	for replace_url in link_url_replace:	  
	  text=text.replace(replace_url[0],replace_url[1])    	      	  
	return text


def load_link_file():
	link_attachments_file=open(link_attachment_urls, 'r')
	for line in link_attachments_file:				
		#if line.find("<link>https://www.contagioradio.com/")<6 :
		attachment=link_attachments_file.readline()
		link=re.sub("\s*<.*?>\s*", "",line)
		attachment=re.sub("\s*<.*?>\s*", "",attachment)		
		#print(link,'=>',attachment)
		link_attachment[link]=attachment
		
def load_postname_imgs():
	postname_imgs_file=open(postname_imgfile, 'r')
	postname_imgs_file.readline()
	for line in postname_imgs_file:
		article = re.search('"(.+)","(.+)"',line)			
		arturl="https://www.contagioradio.com/"+article.group(1)
		imgurl="https://www.contagioradio.com/wp-content/uploads/"+article.group(2)
		#print(arturl,' => ',imgurl)
		postname_imgs[arturl]=imgurl		

def add_image_slider(fotos):
	slider='<div id="slider-div">'
	active='active'
	styledisplay='style="display:block"'
	for foto in fotos:
		slider+='\r\n<img class="'+active+'" src="'+foto+'" alt="foto" '+styledisplay+'></img>'
		active=''
		styledisplay='style="display:none"'
	slider+='<div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div>'
	slider+='</div>'
	return slider

# start main program

# load files for postname-to-attachment_url functions
load_link_file()
load_postname_imgs()

# loop over .md files
for filename in glob.glob("markdown/*.md"):
  with open(filename, 'r') as filein :
    filedata = filein.read()           
    article = re.search("markdown\/(.+)\.md",filename)
    article="https://www.contagioradio.com/"+article.group(1)
    fotos[article]=[]

    # check if slider is already present   
    slider_already_present=filedata.find('div id="slider-div')>=0
    if slider_already_present==False:
      # find articule in link_attachment dictionary and add fotos for each article
      for item in link_attachment: 
        if item.find(article)==0: 
          fotos[article].append(link_attachment[item])        
      if len(fotos[article])==0:
        for item in postname_imgs:
          if item.find(article)==0:
            fotos[article].append(postname_imgs[item])
            
      # add image slider
      if len(fotos[article])>0:
        slider=add_image_slider(fotos[article])
        filedata=re.sub('Status: published', 'Status: published\r\n'+slider,filedata)                      
        if len(sys.argv)>1 and sys.argv[1]=='--dry-run': print(filedata)

    #clean up old elementor tags/img includes
    filedata=re.sub('{.*?style=.*?}','',filedata)
    filedata=re.sub('\[(gallery|metaslider) .*?\]','',filedata)
    if len(sys.argv)>1 and sys.argv[1]=='--dry-run': print(filedata)
    
    # replace all urls for images to cdn and links to archivo
    #if filedata.find("https://www.contagioradio.com/")>=0 : 
    filedata=replace_urls(filedata)    
    #print(filedata)
        
    #correct links : https://archivo.contagioradio.com/mocion-de-censura-botero-oportunidad-para-redisenar-politica-de-defensa-articulo-123456/
    re_match_url_articulo="(https:\/\/archivo.contagioradio.com\/[0-9A-Za-z-_%]+)-articulo-[0-9]+\/"
    foundmatch = re.search(re_match_url_articulo,filedata)    
    while foundmatch: 
      filedata=replace_urls(filedata,(foundmatch.group(),foundmatch.group(1)+'/'))
      #print(foundmatch.group(),'=>',foundmatch.group(1)+'/')            
      foundmatch = re.search(re_match_url_articulo,filedata)            		
        
    # apply changes to source file            
    if len(sys.argv)>1 and sys.argv[1]=='--dry-run':
      print('--dry-run: Not applying changes to '+filename+'q file!')
    else:
      with open(filename, 'w') as fileout: fileout.write(filedata)	            
      
# loop over .md files
# insert image related to post (load post_id, post_name, img_url files)

# replace_images link with cdn link:
#    https://www.contagioradio.com/wp-content/uploads/ => https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/
# replace_ ahref link with archivo.contagioradio.com link:
#    https://www.contagioradio.com/duque-no-desconozca-el-camino-ya-andado-farc-articulo-53934/ => https://archivo.contagioradio.com/duque-no-desconozca-el-camino-ya-andado-farc/

# agregar fotos en articulos que tienen '##### Foto:'

