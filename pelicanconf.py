AUTHOR = 'Contagio Radio'
SITENAME = "Archivo Contagio Radio"
SITESUBTITLE = 'Artículos 2014 - 2020'
SITEURL = 'https://archivo.contagioradio.com'
TIMEZONE = "America/Bogota"
THEME = "./themes/contagio"
#GOOGLE_TAGS='G-DZBL3T758L'
GOOGLE_TAGS='UA-11652077-4'

# config values for md-replace-links.py
cdn_img_url = "https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/"

# CDN image replacement
img_url_replace = (("https://www.contagioradio.com/wp-content/uploads/",cdn_img_url),
				   ("https://i0.wp.com/www.contagioradio.com/wp-content/uploads/",cdn_img_url),
				   ("https://i1.wp.com/www.contagioradio.com/wp-content/uploads/",cdn_img_url),
				   ("https://i2.wp.com/www.contagioradio.com/wp-content/uploads/",cdn_img_url),
			      )

archivo_link_url="https://archivo.contagioradio.com/"

link_url_replace = (("https://www.contagioradio.com/",archivo_link_url),
					("https://www2.contagioradio.com/",archivo_link_url),
					("http://www.contagioradio.com/",archivo_link_url),
					("http://www2.contagioradio.com/",archivo_link_url),
					("http://contagioradio.com/",archivo_link_url),					
					("http://contagioradio.decemedialab.com/",archivo_link_url),
					("contagioradio.decemedialab.com","contagioradio.com"),					
			       )

link_attachment_urls = "link_attachment_urls.xml"
postname_imgfile = "wp_postmeta.csv"

# can be useful in development, but set to False when you're ready to publish
RELATIVE_URLS = False

# Do not show categories in many menu
DISPLAY_CATEGORIES_ON_MENU = False
MENUITEMS = (('2020', 'entradas/2020/'),
			 ('2019', 'entradas/2019/'),
			 ('2018', 'entradas/2018/'),
			 ('2017', 'entradas/2017/'),
			 ('2016', 'entradas/2016/'),
			 ('2015', 'entradas/2015/'),
			 ('2014', 'entradas/2014/'),)

YEAR_ARCHIVE_SAVE_AS = 'entradas/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'entradas/{date:%Y}/{date:%m}/index.html'
YEAR_ARCHIVE_URL = 'entradas'
MONTH_ARCHIVE_URL = 'entradas'


SITEMAP = {
	"format": "xml",
    "priorities": {
        "articles": 1.0,
        "indexes": 0.5,
        "pages": 0.0
    },
	"changefreqs": {
        "articles": "never",
        "indexes": "never",
        "pages": "never"
    },
    "exclude": ["tag/", "pages/"],
}

#REVERSE_CATEGORY_ORDER = True
#LOCALE = "C"
DEFAULT_LANG = 'es'
DEFAULT_PAGINATION = 10
#DEFAULT_DATE = (2012, 3, 2, 14, 1, 1)

#Disable feed generation
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

#Disable tag generation
TAG_SAVE_AS = False
TAG_URL = False

SOCIAL_WIDGET_NAME= "Redes sociales"
LINKS_WIDGET_NAME = "Enlaces"

LINKS = (('Pagina actual', 'https://www.contagioradio.com'),
         ('Señal en vivo', "https://stream.contagioradio.com:8443"),)

SOCIAL = (('twitter', 'https://twitter.com/contagioradio1'),
          ('facebook', 'https://www.facebook.com/contagioradio'),
          ('youtube', 'https://www.youtube.com/user/ContagioRadio'),)

# global metadata to all the contents
#DEFAULT_METADATA = {'yeah': 'it is'}

# path-specific metadata
#EXTRA_PATH_METADATA = {
#    'extra/robots.txt': {'path': 'robots.txt'},
#    }

# static paths will be copied without parsing their contents
#STATIC_PATHS = [
#    'images',
#    'extra/robots.txt',
#    ]

# custom page generated with a jinja2 template
#TEMPLATE_PAGES = {'pages/jinja2_template.html': 'jinja2_template.html'}

# there is no other HTML content
#READERS = {'html': None}

# code blocks with line numbers
#PYGMENTS_RST_OPTIONS = {'linenos': 'table'}

