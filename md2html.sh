/home/gsteenss/.local/bin/pelican -s ./pelicanconf.py -o public markdown
sed -i 's/href="entradas/href="https:\/\/archivo.contagioradio.com\/entradas/g'  public/*.html
sed -i 's/href="entradas/href="https:\/\/archivo.contagioradio.com\/entradas/g'  public/author/*.html
sed -i 's/href="entradas/href="https:\/\/archivo.contagioradio.com\/entradas/g'  public/drafts/*.html
sed -i 's/href="entradas/href="https:\/\/archivo.contagioradio.com\/entradas/g'  public/category/*.html
sed -i 's/href="entradas/href="https:\/\/archivo.contagioradio.com\/entradas/g'  public/entradas/*.html
sed -i 's/href="entradas/href="https:\/\/archivo.contagioradio.com\/entradas/g'  public/entradas/*/*.html
sed -i 's/href="entradas/href="https:\/\/archivo.contagioradio.com\/entradas/g'  public/entradas/*/*/*.html
sed -i 's/>CtgAdm<\/a>/>Contagio Radio<\/a>/g'  public/*.html
sed -i 's/>CtgAdm<\/a>/>Contagio Radio<\/a>/g'  public/author/*.html
sed -i 's/>CtgAdm<\/a>/>Contagio Radio<\/a>/g'  public/drafts/*.html
sed -i 's/>CtgAdm<\/a>/>Contagio Radio<\/a>/g'  public/category/*.html
sed -i 's/>CtgAdm<\/a>/>Contagio Radio<\/a>/g'  public/entradas/*.html
sed -i 's/>CtgAdm<\/a>/>Contagio Radio<\/a>/g'  public/entradas/*/*.html
sed -i 's/>CtgAdm<\/a>/>Contagio Radio<\/a>/g'  public/entradas/*/*/*.html
gzip -k -9 $(find public -type f)

