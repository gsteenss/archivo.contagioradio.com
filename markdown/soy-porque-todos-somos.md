Title: "Soy porque todos somos"
Date: 2019-07-12 12:44
Author: CtgAdm
Category: yoreporto
Tags: cacarica, Comisión de Justicia y Paz, Terre des Hommes, territorio
Slug: soy-porque-todos-somos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-11-at-3.02.47-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Soy-porque-todos-somos.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0223.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0227.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0257.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0263.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0295.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0461.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0483.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0486.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0543.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0567.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0622.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0726.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0744.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Soy porque todos somos, Campeonato Intercultural por la Vida y el Territorio en la Zona Humanitaria Nueva Vida de [Cacarica](https://archivo.contagioradio.com/incidente-de-desacato-obliga-al-estado-a-cumplirle-a-las-comunidades-de-cavida/), Chocó, del 12 al 15 de julio de 2019 en el Bajo Atrato y Urabá Antioqueño. Una estrategia de integración y acompañamiento a niños, niñas, jóvenes y adolescentes que busca incentivar a través de la cultura y el deporte, un sentido de pertenencia y arraigo a los territorios.

Este encuentro de comunidades tendrá como metodología el trabajo cooperativo y multiétnico; que resalta la equidad de género, y promueve alternativas de ver el deporte como una terapia de encuentro participativo, de hermandad y diversión.

Las voces de las y los jóvenes serán las que dirijan el encuentro; así mismo, el espacio busca fortalecer el liderazgo que asumen ellos y ellas a través del deporte como fuente de celebración de la vida, la memoria y como instrumento de construcción de paz y reconciliación desde los territorios.

"Soy porque todos somos"

Por la vida en el territorio: Apoya Terre des Hommes (TdH).

\[caption id="attachment\_70322" align="aligncenter" width="286"\]![Encuentro Yo soy, porque nosotros somos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-11-at-3.02.47-PM-212x300.jpeg){.wp-image-70322 width="286" height="405"} Foto: Comisión Intereclesial de Justicia y Paz, y TDH\[/caption\]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>
