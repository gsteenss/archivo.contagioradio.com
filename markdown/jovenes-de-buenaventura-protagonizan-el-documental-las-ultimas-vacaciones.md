Title: Jóvenes de Buenaventura protagonizan el Documental "Las últimas vacaciones"
Date: 2015-04-29 12:42
Author: CtgAdm
Category: 24 Cuadros
Tags: Cinemateca Distrital, Don Mister, IDARTES, Jonathan Medina, Las últimas vacaciones, Mr. Sombra, Ojo de pez, Señal Colombia, Wilmar Sánchez
Slug: jovenes-de-buenaventura-protagonizan-el-documental-las-ultimas-vacaciones
Status: published

*El audiovisual cuenta, que con el apoyo de **Señal Colombia**, **IDARTES** y la **Cinemateca Distrital**, hizo parte del pasado **Festival Internacional de Cine de Cartagena** y de la sección Panorama Documental del **Festival de cine Latino de Toulouse .***

Del trabajo mancomunado durante los últimos tres años del director **Manuel Contreras** y la productora **Tatiana Villacob**, resulta el documental **"Las últimas vacaciones"**. Un viaje a través de la cultura y sueños de 3 jóvenes afrodecendientes oriundos de Buenaventura para quienes la música lo es todo y al finalizar su etapa escolar tendrán que tomar decisiones sobre el nuevo rumbo de sus vidas.

[![ultimas vacaciones](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/ultimas-vacaciones.png){.wp-image-7792 .aligncenter width="311" height="413"}](https://archivo.contagioradio.com/jovenes-de-buenaventura-protagonizan-el-documental-las-ultimas-vacaciones/ultimas-vacaciones/)

Las productoras **"Don Mister"** y **"Ojo de pez"**, desplazaron sus equipos a una zona del país en la que la belleza natural y la riqueza cultural convive con la pobreza, la desigualdad y la violencia derivada del conflicto colombiano.

Y es precisamente ese contraste el que permite encontrar personajes como **John Jaime Cortés (Mr. Sombra)**, **Jonathan Medina (Lápiz)** y **Wilmar Sánchez (Presi)** , quienes sobresalen por "su talento innato por la música, desbordando alegría y sabor propios del pacífico, sentimientos que en el documental logran trasladar al espectador y lo hacen por momentos olvidar la imágen desesperanzadora, difundida por algunos medios de comunicación.

Un factor clave para la frescura, la naturalidad que ser percibe en el audiovisual fue la cercanía alcanzada entre los protagonistas y el director, quien, en palabras de Tatiana Villacob a Contagio Radio **"más que ser una película y tratarlos como personajes y director esto fue una relación de amigos"**. Relación que favoreció los procesos de rodajes, postproducción, y ahora en la difusión y circulación del largometraje.

<iframe src="https://www.youtube.com/embed/keAh49CRYZI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Las Últimas Vacaciones hizo parte de la pasada edición del Festival Internacional de Cine de Cartagena de Indias "FICCI 55" en la sección Cine en los Barrios con una proyección absolutamente llena en la plaza Trinidad de Getsemaní según cuenta la productora **"Fué increíble la recepción, era el estreno mundial de la película, donde hicimos una proyección al Aire libre en una plaza donde habian alrededor de 300 a 350 personas"**

El largometraje documental tendrá su estreno en mayo, mes en que se conmemora el mes de la Herencia Africana en Colombia, que promueve entre otras cosas la multiculturalidad existente en nuestro país y la no discriminación. La distribución y exhibición esta soportada en la iniciativa no convencional denominada **"ADOPTA UNA FUNCIÓN"**, que busca proyectar la cinta en todos los espacios en los que instituciones, entidades, universidades, colegios o personas interesadas deseen fomentar la cultura, el arte y la música a traves del cine colombiano.

Para más información sobre la película facebook.com/Lasúltimasvacaciones y en Twitter @UltimVacaciones
