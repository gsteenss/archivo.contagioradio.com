Title: Tortura contra coordinador indígena José Camayo retrata la aguda crisis del Cauca
Date: 2019-10-03 18:24
Author: CtgAdm
Category: DDHH, Nacional
Tags: Amenazas contra indígenas, Cauca, CRIC
Slug: tortura-contra-coordinador-indigena-jose-camayo-retrata-la-aguda-crisis-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/55949702_1676960492406329_5701197755452489728_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

El pasado 2 de octubre en el departamento del Cauca, después de haber sido raptado y dejado atado en un poste con un alambre de púas en  la parte alta del Territorio denominado Alto la Chapa, fue hallado con vida el coordinador y **líder indígena José Albeiro Camayo Guetio** quien habría sido abordado por hombres que, según algunas versiones iniciales, buscaban liberar a personas detenidas por la Guardia Indígena.

Según Edwin Capaz, coordinador de DD.HH. de la Asociación de Cabildos Indígenas del Norte del Cauca (ACIN), las autoridades se percataron de la ausencia de Albeiro e iniciaron su búsqueda hasta encontrarlo. Albeiro relata que en el lugar se encontraban cerca de cuatro o cinco hombres que portaban camuflados y armas de largo y corto alcance quienes lo torturaron e interrogaron sobre la presencia de un 'secuestrado', antes de  dejarlo atado en el lugar.

Aunque los hechos son materia de investigación, Capaz señala una de las teorías que manejan es que los sujetos estarían en búsqueda de una persona en particular que habría sido retenida en medio del ejercicio que realiza la Guardia Indígena. [(Le puede interesar: ACIN denuncia aparición de nuevo panfleto que amenaza a la guardia indígena)](https://archivo.contagioradio.com/acin-panfleto-guardia-indigena/)

El defensor de DD.HH.  afirma que en los municipios de **Buenos Aires,  Suárez y Santander de Quilichao, además de las zonas rurales aledañas**, existen pruebas de la presencia del ELN y de la columna móvil 'Jaime Martínez', además de algunos micro grupos que podrían tener una capacidad militar similar a la descrita por José Albeiro, sin embargo aún no se han establecido responsables de lo sucedido.

No es la primera vez en que el líder indígena es blanco de agresiones y amenazas, según explica Capaz, este ha sido víctima de atentados en la vía de Santander de Quilichao, además de ser nombrado junto a otros dirigentes en panfletos, todo debido al ejercicio de control territorial que representa. En esta ocasión en particular, aunque Albeiro cuenta con medidas de protección, señalan que existió un problema de coordinación que permitió que fuera raptado.

### Los elementos de la crisis en el Cauca

Entre los elementos más dicientes de la crisis de DD.HH. en el departamento es que en lo corrido del 2019 han sido asesinados por lo menos 36 líderes sociales hecho sumado al del pasado 1 de septiembre cuando se perpetró la masacre en la que fue asesinada la candidata a la alcaldía Karina García junto a otro aspirante a elección popular y sus familiares.

Según la Fundación Paz y Reconciliación y diversas organizaciones sociales y de DDHH, la presencia de carteles de la droga, disidencias de las FARC, el ELN y de grupos paramilitares, al lado de la fuerte pero inoperante y a veces cómplice militarización de los territorios, hacen que la crisis del departamento siga agudizándose. [(Lea también: Su compromiso con la educación hoy tiene bajo amenaza a profesores del Cauca)](https://archivo.contagioradio.com/compromiso-con-la-educacion-hoy-tiene-bajo-amenaza-a-profesores-del-cauca/)

Al mismo tiempo las políticas del gobierno que niegan los acuerdos de la Habana hacen que la situación económica de las más de 1,404,205 personas que habitan el departamento sea cada vez más precaria por lo que se convierten en blanco fácil de las propuestas de cultivos de uso ilícito, de los grupos criminales o de las bandas del narcotráfico. [(Lea también: Guardia Indígena es atacada en la vía Caloto-Toribio, Cauca)](https://archivo.contagioradio.com/guardia-indigena-es-atacada-en-la-via-caloto-toribio-cauca/)

### Cómo construir la verdad en el departamento

Desde organizaciones sociales y de DD.HH. se vienen impulsando iniciativas de Acuerdos Humanitarios Regionales que buscan que el nivel de confrontación se reduzcan al igual que los niveles de violencia ejercidos contra la población civil por parte de los grupos armados y de las mismas autoridades militares y de Policía. Francia Márquez, lideresa del PCN ha afirmado que "ya que el Estado no hace presencia y no quiere respetar los acuerdos es el turno de las sociedad".

De igual forma, se han exigido medidas de protección que más allá de brindar esquemas de protección a los líderes amenazados tengan un alcance territorial, entre ellos la construcción de vías terciarias, la facilitación para los mercados de productos autóctonos, el desarrollo de infraestructura básica, la ampliación de los cupos escolares y la atención especializada en salud son algunas de estas peticiones.

<iframe id="audio_42762979" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_42762979_4_1.html?c1=ff6600"></iframe>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
