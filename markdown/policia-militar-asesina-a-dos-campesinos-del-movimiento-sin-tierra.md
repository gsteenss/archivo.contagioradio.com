Title: Policia Militar asesina a dos campesinos del Movimiento Sin Tierra
Date: 2016-04-08 13:15
Category: El mundo, Entrevistas
Tags: movimiento sin tierra brasil, MST, paraná
Slug: policia-militar-asesina-a-dos-campesinos-del-movimiento-sin-tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/MST-Brasil.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Patria Grande ] 

###### [8 Abril 2016]

Este jueves en la tarde familias integrantes del Movimiento de los Trabajadores Rurales Sin Tierra MST, que se encontraban en el Campamento Dom Tomas Baluino, en el estado de Paraná, fueron víctimas de una emboscada por parte de la Policía Militar y hombres de la seguridad privada de la empresa maderera 'Araupel'. Hasta el momento se conoce que **dos de los campesinos resultaron muertos y por lo menos siete heridos**.

Luego del ataque, la Policía se tomó las calles, cerró la comisaria y el hospital e impidió a los familiares de las víctimas, abogados y periodistas acercarse a los heridos y acudir al lugar de los hechos para verificar lo sucedido, acción que se presume, podría permitirles **destruir las pruebas que esclarecerían su responsabilidad en lo ocurrido**.

El campamento ubicado en el municipio de Quedas do Iguaçu, había sido construido por tres mil familias en mayo de 2015, en terrenos que la empresa maderera 'Araupel' habría apropiado fraudulentamente, pero que la justicia declaró como **tierras públicas que debían ser destinadas para la reforma agraria**.

Este ataque se da luego de dos días de la visita de representantes de la cúpula de la Polícia de Paraná y de Valdir Rossonil, Secretario de la Casa Civil, en la que se determinó el **envió de un contingente de más de 60 **[**policías**] para el municipio, poniendo en riesgo la vida de las familias que han vivido en la región desde así casi 20 años y que han buscado avances en la reforma agraria, reivindicando que la función social que debe cumplir la tierra.

Frente a estos hechos el MST exige que se ponga en marcha una investigación penal, que lleve a prisión a los policías, miembros de la seguridad privada y a los autores intelectuales del crimen; así como la **retirada de la Policía Militar y el cuerpo de seguridad contratado por 'Araupel'**; además de garantizar la protección de las familias en los territorios que sean expropiados a la empresa y destinados a la concreción de la Reforma Agraria.

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
