Title: Entre críticas y expectativa se estrena en Colombia "Que viva la música"
Date: 2015-10-29 11:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Andrés Caicedo, Carlos Moreno Director de Cine, Cine Colombiano, Grupo de Cali, Luis Ospina, María del Carmen Huerta, Que viva la música película
Slug: entre-criticas-y-aplausos-se-estrena-en-colombia-que-viva-la-musica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/c210a17c19c40be0efedec7e1cf83d2b-e1446070899482.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [29 Oct 2015]

¿Que tan acertada o pifiada puede resultar la adaptación de una de las novelas más representativas de la literatura "juvenil" colombiana?, es la pregunta que muchos espectadores podrán resolver este jueves cuando se estrene oficialmente en Colombia "**Que viva la música**",  inspirada versión de la obra homónima de Andrés Caicedo, publicada de manera póstuma luego del suicidio del escritor a los 25 años de edad.

Desde que fue anunciada la adaptación cinematográfica de la novela corta , no ha parado de recibir toda clase de comentarios tanto de aquellos que conocieron a Caicedo en vida, lo complejo de su personalidad y que entienden su obra desde una perspectiva particular, como de los lectores "neófitos" que se acercan a su trabajo por primera vez y que con gran expectativa esperan su estreno.

Las fuertes críticas de algunos periodistas, sumadas a la opinión de Rosario Caicedo, hermana del escritor caleño, le han añadido cierto "picante" al estreno de la producción dirigida por Carlos Moreno (Perro come perro, Todos tus muertos) y protagonizada por la debutante Paulina Dávila (María del Carmen) , sobre la que han recaído gran parte de las opiniones poco favorables expresadas sobre su interpretación.

**"Que viva la música**" es el  cuarto largometraje en la carrera del director valluno,  quien asegura que para la adaptación se conservaron los elementos que resultaron útiles a la hora de construir el guion, entre ellos, los nombres de los personajes, algunos elementos de la trama y los poquísimos lugares que aún se conservan de la época en que se escribió la novela.

Después de su estreno en la sección "New Frontier" de Sundance a principios de año, la evaluación para "Que viva la música" pasará por la aceptación del público general, el mismo que paulatinamente empieza a ser más receptivo en relación con las películas producidas en Colombia, y que la recibe con una expectativa especial por lo que representa el escrito para el imaginario colectivo de por lo menos 3 generaciones.

La película es el resultado de la coproducción entre Dynamo, Colombia; Ítaca Films, de México; y Labo Digital, con participación de Caracol Cine y Cine Colombia.

<iframe src="https://www.youtube.com/embed/apm_j_pMPR0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
