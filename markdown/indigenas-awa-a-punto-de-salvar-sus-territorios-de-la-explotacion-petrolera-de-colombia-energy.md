Title: Indígenas Awa a punto de salvar sus territorios de la explotación petrolera de Colombia Energy
Date: 2015-11-24 15:07
Category: Economía, Nacional
Tags: ANLA, Consorcio Colombia Energy, daño ambiental, Derechos Humanos, Explotaci, Explotación petrolera Putumayo, indígenas, Locomotora Petrolera, ONIC, Pueblo indígena Awa, Putumayo, Radio derechos Humanos
Slug: indigenas-awa-a-punto-de-salvar-sus-territorios-de-la-explotacion-petrolera-de-colombia-energy
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/awa_putumayo_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:elmercuriodigital.net 

###### [24 Nov 2015]

Hoy ante la Corte Constitucional **el magistrado Alberto Rojas Ríos solicitará que se tomen medidas frente al proyecto de exploración y explotación petrolera que ejecuta el Consorcio Colombia Energy**, el cual afecta a un cabildo en Putumayo de la comunidad Awa en el derecho a la vida, el medio ambiente y la integridad cultural, esto por la llamada locomotora petrolera.

Un informe presentado por la Defensoría Delegada para Asuntos indígenas presentado el 30 de septiembre de este año evidencia que con este proyecto **de los 99 miembros de la comunidad, 55 niños han padecido vómito, diarrea, dolor de cabeza y desnutrición** ya que “no es posible el consumo de pescado, debido a que presenta un fuerte sabor a petróleo”.

La denuncia por estos casos inició luego de que Juvencio Nastacuas Pai, gobernador del cabildo, iniciara una acción de tutela contra la Autoridad Nacional de Licencias Ambientales, el Consorcio Colombia Energy y contra el Ministerio del Interior, ya que la explotación de hidrocarburos en las zonas del Quinde, Cohembí y Quillacinga, **vulnera el derecho a la vida, la identidad cultural y el ambiente de la comunidad que no fue consultada para este proyecto** y al día de hoy extrae 14 mil barriles de crudo diario.

La ponencia del magistrado Rojas Ríos indica que “**El asunto de la referencia demuestra los altos contrastes sociales que se observan cuando el Estado privilegia los intereses económicos sobre la protección al ambiente y a las comunidades étnicas minoritarias**” y agrega que mientras el proyecto de exploración y explotación petrolera representa un recaudo de ingresos para la Nación cercano a los \$85 mil millones, la Defensoría del Pueblo ha indicado que la comunidad indígena **awá se encuentra en condiciones de extrema pobreza**”.

Según informes de organizaciones de DDHH como la Comisión de Justicia y Paz y la defensoría del Publo desde 1980 las comunidades indígenas en Putumayo han venido siendo perjudicadas por las exploraciones que comenzaron a realizar Ecopetrol y Texaco en los corregimientos de Puerto Vega, La Carmelita, Alto Cuembí, Teteyé y Bajo Cuembí, proyectos que dejó Ecopetrol en el 2002 y retomó el Consorcio Colombia Energy  y que está conformado por las firmas Vetra E&P Colombia, Petrotesting Colombia y Southeast Invesment Corporation.

### **Ambiente en riesgo:**

Esta explotación de hidrocarburos ha traído para el territorio de los awa el **desplazamiento de diferentes especies nativas** como “las serpientes, la gata, el güío, la X, la 24, entre otras numerosas especies de animales silvestres como la danta ancestral del Putumayo, el cachirre, el gurre, el guara, el cerillo, la boruga, variedades de peces como el raya, el dentón, la piraña, la guaraja, el bocachico, el singo, el temblón, el picadillo, el bagre, y de aves como las guacamayas, tucanes, colibríes, búhos, loros cabeza azul, la guacharaca, el azulejo, el picaflor, el gallinazo, la gallineta, las pavas, la torcaza, especies que están desapareciendo con ocasión de la exploración y explotación petrolera”, afirmó el gobernador Juvencio Nastacuas Pai, además que **las fuentes hídricas y humedales perdieron volumen**, sin un plan para mitigar los daños.

 

**Defensa del Consorcio Colombia Energy:**

**Según la Energy la licencia ambiental para la explotación del crudo fue otorgada según la ley y en el 2008 el Ministerio del Interior certificó que en las zona del proyecto Putumayo no había comunidades indígenas ni negras y la Autoridad Nacional de Licencias Ambientales** (ANLA), también se pronunció en el mismo sentido.

Por su parte el Incoder pidió negar la tutela, así como Ecopetrol y el Ministerio de Hacienda, este valoró que el cese de operaciones de los pozos generaría una pérdida de 800 empleos y la disminución en la producción de crudo del país en 1,4 % y menos ingresos a la Nación calculado en \$85 mil millones, el 40 % de los ingresos del departamento en regalías.

Con esta ponencia y estos datos el **magistrado Rojas pide a la Corte Constitucional tutelar los derechos de los awás, ordenar adelantar un proceso de consulta previa con ellos, que la ANLA en menos de un mes incluya a la comunidad aborigen en el plan de zonificación de manejo ambiental** en las áreas de influencia de los campos Quinde, Quillacinga y Cohembí y que se suspendan los pozos 4, 8 y 11 de este último.

Según Rojas Ríos, el Consorcio Colombia Energy deberá adoptar en tres meses una estrategia para mitigar los impactos del proyecto, con el fin de que compensen la pérdida de recursos de los humedales y se creen nuevas reservas para recuperar los animales nativos. Por último, la ponencia busca que la Fiscalía investigue los delitos perpetrados contra los awás en el marco del conflicto armado y que el Ejército proteja a esa etnia en vez de estar asociándola, tal como lo documentó la Defensoría del Pueblo, con la guerrilla.
