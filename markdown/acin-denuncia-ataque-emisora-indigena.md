Title: ACIN denuncia ataque en contra de emisora indígena
Date: 2018-12-26 14:59
Author: AdminContagio
Category: Comunidad, Nacional
Tags: ACIN, Comunidad Nasa, Radio comunitaria
Slug: acin-denuncia-ataque-emisora-indigena
Status: published

###### Foto: ACIN 

###### 26 Dic 2018 

La Asociación de Cabildos Indígenas del Norte del Cauca (ACIN) denunció un ataque en contra de Radio Pa'yumat que dejó a la emisora nasa fuera del aire en la noche del 20 de diciembre.

Según la denuncia, sujetos desconocidos entraron a las instalaciones de la radio comunitaria en el resguardo Munchique los Tigres, ubicado en el municipio Santander de Quilichao, y cortaron el cable de transmisión, sacando a la emisora del aire. **La Asociación denunció** **esta agresión como un ataque en contra de la libertad de expresión y prensa del movimiento indígena.**

Radio Pa'yumat, parte del tejido de comunicaciones de la ACIN desde el 2001, transmite diariamente boletines de información, los cuales incluyen convocatorias para movilizaciones sociales y denuncias de amenazas a líderes sociales, además de programación cultural.

Debido a que **varias comunidades indígenas cuentan con Radio Pa'yumat como su única fuente de información**, la organización aseguró que los hechos pretenden aislar a las comunidades y debilitar el movimiento de defensa del territorio. La Fundación para la Libertad de Prensa (FLIP) también condenó estos ataques y hizo un llamado a la Fiscalía General de la Nación para que investigue estos hechos.

### Ola de agresiones en el Norte del Cauca 

La ACIN manifestó que el atentado del jueves no es la primera vez que han intentado de silenciar la expresión libre del pueblo nasa. En marzo, Eider Campo Hurtado, comunicador de la emisora Pelsxhab Stereo y guardia indígena, fue asesinado en su casa en Caldono, Cauca. Radio Pa’yumat también recortó su programación este año por cuestiones de seguridad.

Estas agresiones contra los medios de comunicación se presentan entre en "el marco de la ola de violencia que afecta el norte del Cauca," en el cual han sido asesinados dos lideres indígenas en solo el mes de diciembre. Previamente, panfletos, firmados por las Águilas Negras, han circulado que ofrecen recompensa por el asesinato de lideres sociales del Norte del Cauca.

(Le puede interesar:["Recuperar su territorio le está costando la vida a indígenas en Cauca](https://archivo.contagioradio.com/nos-estan-asesinando-llamado-de-comunidades-indigenas-del-cauca/)")

###### Reciba toda la información de Contagio Radio en [[su correo]
