Title: Trabajadores despedidos de la ETB se encadenan a sus puestos de trabajo
Date: 2016-06-24 14:38
Category: Economía, Nacional
Tags: asamblea permanente, Venta de la ETB
Slug: trabajadores-despedidas-de-la-etb-se-encadenan-a-sus-puestos-de-trabajo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  @ETB\_No\_Se\_Vende] 

###### [24 Junio 2016 ] 

El pasado 23 de junio, más de 10 empleadas y empleados fueron despedidos injustificadamente de la Empresa de Telefonía de Bogotá ETB. En rechazo a esta situación, cuatro de los empleados decidieron **encadenarse a sus puestos de trabajo exigiendo el reintegro inmediato a sus actividades.**

De acuerdo con los trabajadores, estos despidos se dan en forma de retaliación debido a las diferentes[protestas que han sostenido por la venta de la empresa pública ETB y la defensa del patrimonio público](https://archivo.contagioradio.com/bogotanos-se-manifestaron-contra-la-venta-de-la-etb-frente-al-concejo/).  Los empleados exigen a la alcaldía de Peñalosa y al director de la empresa Enrique Castellanos, **el respeto por el derecho al trabajo y la reintegración inmediata de los cargos para todos los empleados despedidos**. Esta mañana el edificio de la ETB amaneció acordonado por disponibles del ESMAD y los trabajadores se declararon en asamblea permanente.

Organizaciones como [SintraTeléfonos y Atelca han expresado en un comunicado de prensa que los despidos son injustificados y  señalan que este hecho es una masacre laboral](https://archivo.contagioradio.com/venta-de-etb-sera-demandada-por-sindicatos/), además exigen que se cesen los despidos, la persecución contra los trabajadores de la ETB, y que se suspenda todo acto que vulnere los derechos constitucionalmente protegidos y garantizados .

A su vez, hacen un llamado a las organizaciones sindicales y sociales, de orden nacional e internacional para que "se pronuncien sobre los actos de **violencia que la administración de la ETB y la alcaldía Mayor de Bogotá han perpetrado**".

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
