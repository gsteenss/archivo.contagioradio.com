Title: Crece respaldo al proceso de conversaciones de paz con FARC y el ELN
Date: 2015-02-05 20:54
Author: CtgAdm
Category: Paz, Política
Tags: conversaciones de paz, ELN, encuestas, FARC
Slug: crece-respaldo-al-proceso-de-conversaciones-de-paz-con-farc-y-el-eln
Status: published

###### Foto: marcha patriótica 

La encuesta de Polimétrica, publicada hoy por diversos medios de información, da cuenta del respaldo creciente de la población colombiana al proceso de conversaciones de paz con la guerrilla de las **FARC y con el ELN**.

Según la encuesta de Cifras y Conceptos el respaldo al proceso de conversaciones viene en aumento. Mientras que en enero de 2013 el respaldo era del 33%, en enero de 2014 subió al 39% y en enero de **2015 llegó al 41%**.

Por otra parte la gran parte de la población encuestada considera que el proceso de conversaciones con las **FARC llegará a un feliz término puesto que el 47% responde que sí**, y esta cifra va en crecimiento si se considera que en octubre de 2014 era de 39% y en enero de 2015 subió a 47%.

En cuanto al proceso de conversaciones con el **ELN la situación es similar, puesto que el 44% responde que sí**, mientras que en enero de 2014, era sólo el 22%.

[Crece el respaldo al proceso de Paz en Colombia](https://es.scribd.com/doc/254845281/Crece-el-respaldo-al-proceso-de-Paz-en-Colombia "View Crece el respaldo al proceso de Paz en Colombia on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_99234" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/254845281/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-ng7hNmI8WJF1nffQdgAL&amp;show_recommendations=true" width="70%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="1.7790927021696252"></iframe>
