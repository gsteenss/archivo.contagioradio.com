Title: ELN: "El país necesita que se abra la mesa sin ningún tipo de dilaciones"
Date: 2016-09-01 14:03
Category: Nacional, Paz
Tags: Diálogos de paz ELN, ELN Nicolas Rodríguez Bautista, Negociaciones con ELN
Slug: eln-el-pais-necesita-que-se-abra-la-mesa-sin-ningun-tipo-de-dilaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Nicolás-Rodríguez-ELN-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Red + Noticias] 

###### [1 Sep 2016] 

En entrevista para 'Red + Noticias', Nicolás Rodríguez Bautista, máximo comandante del ELN afirmó que esta guerrilla respetará las zonas veredales transitorias de concentración de las FARC-EP y la votación en el plebiscito. Así mismo insistió en que **debe abrirse la fase pública de conversaciones de paz** que fue [[anunciada desde el pasado mes de marzo](https://archivo.contagioradio.com/conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln/)] y que está congelada por cuenta de "la imposición unilateral de condiciones inaceptables", por parte del presidente Juan Manuel Santos, frente al tema del secuestro.

El comandante aseguró que la guerrilla del ELN no acepta imposiciones, pues si bien han hecho retenciones por causas políticas y económicas, éste es un tema cuya discusión está contemplada en el quinto punto de la agenda de negociación, por lo que no comprenden por qué si ya hay un orden de discusión, aparece la postura de una de las partes para imponerse de forma unilateral. "**El futuro de Colombia no puede ser la guerra, es necesario atenernos a una agenda que está definida**, el país necesita que se abra la mesa sin ningún tipo de dilaciones", agregó.

Frente al proceso de paz con la guerrilla de las FARC-EP, Rodríguez afirmó que pese a que el ELN no comparta ni la agenda ni el curso que tomaron las conversaciones, respeta [[los acuerdos](https://archivo.contagioradio.com/acuerdo-final-de-paz-entre-gobierno-y-farc/)] y está dispuesto a aportar para su implementación, al punto de que tan pronto supieron dónde y cuáles eran las zonas veredales de concentración, **dieron la orden a todas las unidades de ser prudentes frente al proceso** y responsables con su realidad.

El comandante aseveró que "el proceso de paz en este momento no es con el ELN, el cese al fuego bilateral no es con el ELN y la gran mayoría de zonas veredales de concentración están en áreas dónde está el ELN desde hace muchos años, y esa es una realidad muy compleja (...) porque **hay acciones punitivas del Ejército colombiano por aire y tierra sobre las áreas del ELN** (...) por eso es importante el avance del proceso de paz con el ELN".

Sobre el plebiscito Rodríguez indicó que como alzados en armas no pueden ejercer el derecho al voto, pero que de acuerdo con sus principios respetarán a la población y **no impedirán, ni truncarán bajo ninguna circunstancia su derecho a expresarse** en las urnas, pues existe la necesidad de [[refrendar los acuerdos](https://archivo.contagioradio.com/el-decreto-que-oficializa-el-plebiscito-para-la-paz/)] y han dicho que respetan ese proceso y sus acontecimientos.

"Si la [[participación de la sociedad en el proceso de paz](https://archivo.contagioradio.com/los-fuerte-de-este-proceso-es-la-participacion-de-la-sociedad-eln/)] colombiano no es protagónica, no llegaremos a puerto seguro, porque el conflicto de más de 50 años lo ha padecido la sociedad colombiana, sobre todo la más excluida, y esa **sociedad tiene que participar en la construcción del proceso**, nadie puede remplazar a un pueblo que quiere expresarse", enfatizó el comandante.

Según lo expresado por Rodríguez, "pasada la coyuntura particular que vivimos hoy, **es indispensable la unidad y la organización del movimiento popular porque la lucha continúa**, la búsqueda de la paz sigue, los acuerdos de La Habana son unos compromisos que hasta ahora no se han desarrollado (...) pero la única manera de que los alcances de un proceso de paz auténtico en Colombia lleguen, es mediante la [[participación, la lucha, la organización](https://archivo.contagioradio.com/el-pos-acuerdo-implicara-un-mejor-escenario-para-la-movilizacion-social/)] y la unidad del movimiento de masas".

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
