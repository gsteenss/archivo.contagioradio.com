Title: Diego Palacio a la Jurisdicción Especial para la Paz
Date: 2017-04-04 12:32
Category: Judicial, Nacional, Política
Tags: Alvaro Uribe, Diego Palacio, Reelección, Yidis Medina, Yidispolítica
Slug: habia-que-hacer-lo-que-fuera-para-lograr-la-reeleccion-diego-palacio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/diego_palacio_exministro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [04 Abr. 2017] 

**Diego Palacio**, ex ministro de Protección Social del gobierno de Álvaro Uribe **reconoció que compró la reelección**. Así lo reveló Néstor Correa, secretario ejecutivo de la Jurisdicción Especial para la Paz – JEP -, a propósito de un derecho de petición que llegó a su despacho y en el que Palacio le solicita permitirle acogerse a este tribunal de justicia.

En el derecho de petición, Palacio asegura que había que hacer lo que fuera necesario para lograr la reelección del ahora senador Álvaro Uribe para poder seguir con su meta de acabar con la guerrilla de las FARC **“el Gobierno de Álvaro Uribe tenía un enfrentamiento de alto nivel con la guerrilla y al finalizar el periodo presidencial tenía que continuar con los enfrentamiento (...)"**

"Para ellos **era necesario reelegir al presidente** por lo que se incurrió en el delito de cohecho ofreciendo notarías al Congreso con el fin de poner los votos para la reelección” añadió Correa. Le puede interesar: [Ex ministros de Alvaro Uribe condenados por escándalo de la "Yidispolítica"](https://archivo.contagioradio.com/ex-ministros-de-alvaro-uribe-condenados-por-escandalo-de-la-yidispolitica/)

Correa además dijo que **dicho escrito ya fue entregado a un juez de ejecución de penas** quien definirá si puede darse la libertad provisional o no a Diego Palacio, pero además si podría acceder a la JEP, es decir, si el crimen de cohecho tiene o no relación con el conflicto armado.

Sin embargo, según el propio Correa, **de no ser aceptada la petición por parte del juez, en otro momento Diego Palacio** y cualquier otro que desee acogerse a la JEP puede volver a intentar ser admitido, para ser juzgado por los crímenes que se le endilguen.

Cabe recordar que en varias ocasiones el exministro de Protección Social negó que la reelección presidencial de Uribe hubiera sido comprada, sin embargo, la Corte Suprema de Justicia en diciembre de 2015 lo condenó a 80 meses de prisión, momento para el que se declaró como “perseguido político”.

**Por la reelección presidencial, también ha sido condenada la ex congresista Yidis Medina** por el delito de cohecho por haber vendido su voto y apoyar de esta manera la reelección de Álvaro Uribe. Le puede interesar: ["Sectores de Ultraderecha y empresarios le temen a la JEP" Iván Cepeda](https://archivo.contagioradio.com/sectores-de-ultraderecha-y-empresarios-le-temen-a-la-jep-ivan-cepeda/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
