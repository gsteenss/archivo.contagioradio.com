Title: "Demostraremos que los defendidos no representan un peligro para nadie"
Date: 2015-07-13 12:54
Category: DDHH, Nacional
Tags: captura, congreso de los pueblos, Fiscalía, jovenes, Jóvenes capturados, ley de seguridad ciudadana, papas bomba, porvenir
Slug: demostraremos-que-los-defendidos-no-representan-un-peligro-para-nadie
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/estudiantes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: El Pais 

<iframe src="http://www.ivoox.com/player_ek_4784051_2_1.html?data=lZyllpWZdY6ZmKiak5yJd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkNDnjMrTx8jYs9SfxcqYzsaPkMbtjMnSjbjJq9bmysnOxpCnrdbYwsnO0MaPvYzgwpDQw9XYuY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Manuel Garzón, Abogado] 

##### [13 jul 2015] 

Para el equipo de 12 abogados defensores, es claro que, **a pesar de la prisa con que las autoridades emitieron juicios mediáticos contra los jóvenes** intentando mostrar resultados ante la opinión pública, el proceso jurídico no se adelanta por ese motivo. "No hay ninguna relación de hechos, ni de pruebas, que conecten a estos muchachos y muchachas con las explosiones de hace 2 semanas. La fiscalía no los está investigando por esos hechos", afirma el abogado Manuel Garzón. Vea también [Las "pruebas" de la fiscalía.](https://archivo.contagioradio.com/sin-argumentos-solidos-legalizan-captura-de-13-lideres-sociales/)

<div>

Por el contrario, **los hechos presentados por la Fiscalía tendrían relación con una protesta estudiantil realizada el pasado 20 de mayo**. Para los abogados, es desproporcionado que pretendan imputarse cargos de "terrorismo" a personas que hayan hecho parte de manifestaciones. Ver [Perfiles de los jóvenes capturados](https://archivo.contagioradio.com/estos-son-los-perfiles-de-las-personas-capturadas-en-bogota/).

Este, según diversos analistas, sería uno de los efectos de la implementación de la **Ley de Seguridad Ciudadana**, ya que actos de protesta social y organización que no estaban tipificados anteriormente, son clasificados como delitos políticos mayores, y por lo tanto, castigados con 15-20 años de prisión. Vea también [Nuevo código de policía es dictatorial.](https://archivo.contagioradio.com/nuevo-codigo-de-policia-es-dictatorial-y-viola-ddhh-alberto-yepes/)

**Este lunes 13 de Julio se adelanta la audiencia de imputación jurídica**, en que se conocerá claramente los delitos con los cuales la Fiscalía busca judicializar a las y los jóvenes capturados. Entre tanto, los 13 jóvenes investigados sufren el "prejuzgamiento" y tratamiento por parte de los medios tradicionales de información, y el mismo presidente de la república, quienes anunciaron su captura como un golpe contra estructuras insurgentes.

Las audiencias contra los **13 jóvenes capturados el pasado miércoles 8 de julio en Bogotá**, acusados ante los medios de comunicación de ser perpetradores de las explosiones ocurridas en Bogotá el jueves 2 de julio, han sido maratónicas. La audiencia de legalización de captura se prolongó por 26 horas, mientras la audiencia de imputación fáctica alcanzó 10 y 8 horas continuas.

</div>
