Title: De la solidaridad ante la muerte
Date: 2016-12-14 06:00
Category: Cesar, Opinion
Tags: Chapecoense, conflicto armado, solidaridad
Slug: de-la-solidaridad-ante-la-muerte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Chapecoense-y-nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Getty images 

#### Por  **[Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)** 

###### 14 Dic 2016 

La tragedia aérea en territorio colombiano que  terminó con la muerte de todo el equipo de fútbol “Chapecoense”, de Brasil, que venía a disputar una final de campeonato continental ha generado un par análisis relacionados en general con la política y lo social. Digamos que se pueden resumir en la consideración de que la solidaridad expresada por los colombianos con los deportistas fallecidos debería, o tendría que, ser la misma que con las víctimas del conflicto armado y en particular con los líderes sociales que recientemente han sido asesinados  luego de la firma del acuerdo con las FARC.

[El asunto no es menor tamaño. Se trata de la actitud que nos corresponde asumir como seres humanos frente a la muerte - trágica, natural, violenta -.  Claro, en la guerra la percepción sobre el acabar de la vida - su]*[anticipación]*[ - se endurece; y disminuye más aún cuando, como es regular en enfrentamientos militares prolongados (y de intervención extranjera), la degradación (lo sucio, lo degenerado, lo inhumano, lo irracional) va surgiendo como proceso.]

[Por supuesto, hablar del “ser humano” es una abstracción, a veces necesaria como en este caso. En realidad no hay]*[humanidad]*[ a secas. Lo que conocemos por tal es la expresión de una serie casi infinita de sectores sociales en los que están divididas las diferentes sociedades. La categoría de]*[clase]*[ alude justamente a tales “clasificaciones”. La humanidad está diseccionada en clases sociales y hay una que domina y tiene hegemonía sobre las otras: la burguesía, sea nacional o sea extranjera. El campo de los vencidos está atiborrado de sectores sociales: campesinos, mujeres trabajadoras urbanas, precariado, desplazados, sin casa, pobladores y todos los demás que conocemos de sobra.]

[La ideología burguesa es un fenómeno histórico y filosófico-político. Es la argamasa indispensable para la dominación. Penetra todos y cada uno de los poros de la humanidad, del ser. Y en parte lo logra mediante el valor de cambio de la mercancía; el fetichismo es, entonces, otro de los mecanismos para la sujeción. Hace unas pocas semanas en medio de un partido de fútbol entre el Real Madrid y el Atlético hubo roces verbales entre KoKe, del Atlético, y Cristiano Ronaldo; el primero le dijo “maricón” al segundo y éste le respondió: “sí, lo soy, pero lleno de dinero”…]

[Como este caso hay diariamente millones. La forma dinero de la mercancía copa buena parte del imaginario y la realidad social.]

[El fútbol es no solo un deporte sino un objeto de estudio histórico y social. Los dueños de los equipos son empresarios capitalistas, o un conjunto de ellos, para quienes el “equipo” y cada uno de sus jugadores son mercancías. En eso no hay nada de extraño o de novedoso. El espinoso asunto es que los equipos y el deporte mismo aparecen ante nuestros ojos como “neutrales”, “apolíticos”; son espacios en los que lo “social” no lo es: en los estadios no hay conservadores, liberales, verdes, polistas, socialistas y demás; hay]*[jugadores]*[ y]*[espectadores]*[.  Son noventa minutos en los que vencedores y vencidos dejan de serlo y son, simplemente, “hinchas”, “torcedores” de uno u otro equipo,  y en los que el calor humano se transmite gracias a la pasión y al ocio sano. El seguidor del equipo ve en éste su propia superación y la manera en que le es posible derrotar al adversario. De allí la estrecha relación entre autoritarismo, ideología, dictaduras militares, Estado y fútbol.]

[En la política, se dice, las cosas son diferentes y tienen otro precio. Para mejor decir, en las coyunturas en las que se llama a votar por determinado asunto y en la que la vida cotidiana  se define y determina alrededor de interpretaciones provenientes de quienes dirigen las maquinarias en el Congreso y quienes controlan los medios de comunicación del Capital - o de quienes dirigen los grupos políticos denominados partidos -, en la política, repito, la “neutralidad” se pierde y la intolerancia perturba las mentes. El conflicto social armado dividió a los colombianos, y por falta de educación política independiente y crítica en las jóvenes generaciones (Mannheim) se fue incubando la venganza, el desquite, el odio; para buena parte de aquellas no existe la responsabilidad del Estado ni la posibilidad de salida política negociada. Se tiene una visión en blanco y negro: este para los malos, aquel para los buenos.]

[Esta vía de análisis tal vez pudiera superar lo mencionado arriba en cuanto a la solidaridad ante la muerte.  Ante la tragedia del Chipacoense, fue dirigida y manipulada por los propietarios del Atlético Nacional y de un medio de comunicación, es cierto, pero sentida en lo profundo del ser por los colombianos, y por los paisas, claro, sin hipocresías. Por eso me parece errática la opinión de Renán Vega Cantor en “La hipócrita solidaridad de los paisas del Atlético Nacional” (][[www.rebelión.org]](http://www.rebelión.org)[). En esta ocasión Tartufo es el Capital, no los “paisas” ... Con su carita de yo no fui Tartufo sabía hacia dónde se dirigía; proyectó.]

[En cuanto a las víctimas, las del conflicto social armado y las del Acuerdo reciente, poca solidaridad. Pero yo no podría deducir que los seguidores del buen fútbol que llenaron el  estadio Atanasio Girardot en Medellín para despedir de la vida al Chipacoense, todos sin excepción, fueron ajenos a la solidaridad con las víctimas por el conflicto social armado. Exageraría.]

[Tal dicotomía - solidaridad sí, solidaridad no … -  se puede y debe racionalizar y hay por delante todo un objeto de estudio por trabajar. La solidaridad ante la muerte violenta y trágica tiene que ser pareja, nivelada. La ausencia de equilibrio - y de educación política - en el caso que comentamos nos obliga a preguntar algo que hace parte de la cruda realidad en nuestro país: ¿No hubo falta de consideración, y de solidaridad, con los fallecidos del equipo brasilero, y con pasajeros y tripulación,  con el argumento de que tal nivel de solidaridad debería también expresarse hacia las víctimas? Al fin y al cabo, se dice, el accidente fue algo “casual”.  En otras palabras, ¿la baja o poca solidaridad con las víctimas tasa, mide, la solidaridad con la tragedia?]

[Me niego a creer que tengamos que apagar e irnos.]
