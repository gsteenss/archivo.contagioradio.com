Title: Crímenes contra el Colectivo de Abogados José Alvear Restrepo llegan a la Corte Interamericana
Date: 2020-07-09 21:14
Author: PracticasCR
Category: Actualidad, DDHH
Tags: colectivo de Abogados José Alvear Restrepo, Corte Interamericana de Derechos Humanos, Derechos Humanos, Persecución a defensores de derechos humanos
Slug: crimenes-contra-el-colectivo-de-abogados-jose-alvear-restrepo-llegan-a-la-corte-interamericana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Colectivo-de-Abogados-Jose-Alvear-Restrepo-en-la-Corte-Interamericana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Comisión Interamericana de Derechos Humanos —CIDH, anunció que **someterá el caso de interceptaciones ilegales, persecución, hostigamiento y otras graves violaciones a los derechos humanos contra el Colectivo de Abogados José Alvear Restrepo** **—[CAJAR](https://www.colectivodeabogados.org/) a la Corte del Sistema Interamericano.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con esta remisión, el proceso adquiere un carácter contencioso en el que tanto CAJAR como el Estado Colombiano podrán allegar pruebas y **el Alto Tribunal Internacional, con base en la evidencia recopilada, emitirá una sentencia**, la cual es inapelable y vinculante, es decir no tiene ningún recurso de impugnación y es de obligatorio cumplimiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El caso del CAJAR lleva su curso en el Sistema Interamericano desde el año 2001, cuando el Colectivo acudió a la CIDH para someter a su consideración los casos de amenazas sistemáticas y generalizadas sufridas por sus miembros desde los años noventa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Reinaldo Villalba, presidente del CAJAR, en el año 2006 la solicitud fue admitida por la Comisión y en octubre del año 2019 **se emitió un primer informe donde se establecía la responsabilidad del Estado** y la formulación de unas recomendaciones para que dicha situación de vulneración de derechos cesara; no obstante, según Villalba, dichas recomendaciones no fueron atendidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con lo dicho por el presidente del CAJAR, **el caso compromete a varios gobiernos** **como el de Álvaro Uribe Vélez** que utilizó los aparatos de inteligencia del Estado, como las extintas DAS y Agencia de Inteligencia para perseguir a los miembros del Colectivo. **También al gobierno Duque**, en los perfilamientos ilegales ejecutados por el Ejército dados a conocer recientemente, lo cual, constituye un accionar «permanente» de vulneraciones a los Derechos Humanos. (Le puede interesar: **[Espionaje ilegal del Ejército de Colombia apunta a periodistas y Defensores de DDHH](https://archivo.contagioradio.com/espionaje-ilegal-del-ejercito-de-colombia-apunta-a-periodistas-y-defensores-de-ddhh/)**)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las violaciones de DD.HH que denuncia el Colectivo de Abogados José Alvear Restrepo y sus pretensiones frente al caso

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el presidente del CAJAR en el informe emitido en octubre de 2019, **la Comisión tomó la decisión de declarar responsable al Estado Colombiano por la violación de derechos humanos en contra de los miembros de CAJAR y sus familias**, particularmente de los derechos a la vida, integridad personal, garantías judiciales, honra, intimidad, libertad de expresión, derecho de asociación, entre muchos otros. **Lo que llevó al desplazamiento y exilio de muchos de los miembros del Colectivo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El CAJAR espera que con la remisión del caso a la Corte Interamericana, se proscriba la **doctrina estatal de considerar a los defensores de DD.HH y en general a toda voz disidente como «enemigos internos»**, cese la impunidad, se brinden garantías de no repetición y que el caso repercuta no solo en Colombia sino en la región porque **«la situación de personas defensoras de Derechos Humanos en el continente es bastante crítica»**, anotó Villalba.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, Viviana Krsticevic, directora ejecutiva del Centro por la Justicia y el Derecho Internacional —[CEJIL](https://www.cejil.org/), celebró la decisión de la CIDH y aseguró que esta contribuirá con la modificación de las prácticas institucionales al interior de los Estados en una región como Latinoamérica que es la «más violenta para el ejercicio y la defensa de los DD.HH» y especialmente en Colombia que es el país que presenta el peor panorama  en esta materia. (Lea también: **[ONU y CIDH estarían preparando pronunciamientos sobre situación de DDHH en Colombia: CEJIL](https://archivo.contagioradio.com/onu-cidh-cejil-colombia/)**)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Colombia en el contexto latinoamericano y mundial, no es la excepción sino que por el contrario, tristemente es el país que lidera estos alarmantes niveles de violencia contra las personas defensoras de Derechos Humanos. \[Los registros en Colombia\] dan cuenta de más de 150 líderes sociales y personas defensoras de DD.HH asesinadas solo en este año, para que se den una idea a nivel mundial en los últimos años hubo alrededor de 300 asesinatos de personas defensoras \[…\] lo cual da cuenta de una proporción enorme»**
>
> <cite>Viviana Krsticevic, directora ejecutiva del CEJIL</cite>

<!-- /wp:quote -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/75884689790/videos/989743161483676/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/75884689790/videos/989743161483676/

</div>

<figcaption>
Rueda de Prensa, Colectivo de Abogados José Alvear Restrepo

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
