Title: Las razones de los recicladores para demandar licitación al servicio de aseo
Date: 2017-01-12 16:35
Category: Nacional, yoreporto
Tags: administración Enrique Peñalosa, Bogotá, gestión de residuos, recicladores
Slug: recicladores-bogota-licitacion-aseo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/recicladores-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Asociación de Recicladores ESP 

##### 12 Ene 2016 

#### **Por  Camilo A. Leal\* - Yo Reporto** 

[Recicladores de la ciudad de Bogotá demandaron la licitación del servicio de aseo impulsada por la Administración de Enrique Peñalosa](https://archivo.contagioradio.com/recicladores-demandan-licitacion-del-servicio-aseo-de-bogota/). Luis Alberto Romero, uno de los firmantes de la demanda, aceptó responder algunas preguntas en relación con la gestión de los residuos en la capital del país.

**La administración de Enrique Peñalosa puso en marcha la licitación del servicio de aseo en Bogotá, como se supo por los medios, ustedes demandaron la licitación a través de una acción de tutela pidiendo suspender la licitación hasta que no se garantice el mínimo vital, el trabajo digno, y hasta que no haya igualdad entre los empresarios del aseo y las organizaciones de recicladores. ¿Cuáles son las razones por las que demandaron la licitación?**

- Nosotros demandamos esa licitación porque en dicha licitación no se articula todo el servicio de aseo de la ciudad. Como muy bien se sabe solo esta la parte de RBL (Recolección, barrido y limpieza), recolección de residuos no aprovechables, poda de árboles, barrido y limpieza de las calles y no está articulada con el servicio público de aprovechamiento. Esto desmejora la situación de los recicladores al no estar articulada junto con el servicio que nosotros prestamos.

- **Uno en la calle ve que los recicladores, sobre todo los que trabajan en el espacio público, compiten todo el tiempo con los carros compactadores por recuperar los residuos antes de que los echen al compactador… ¿esa situación se soluciona en la licitación?**

- En la licitación no se menciona este problema, y esa situación se genera por la libre competencia que existe en el servicio de aprovechamiento de los residuos, y es la que tiene marginado al gremio reciclador, eso es lo que realmente nos mantiene subyugados a los operadores privados, a los bodegueros y a los comercializadores y otros actores de la cadena del reciclaje.

- ¿**Ustedes consideran que en esa licitación existen obligaciones contractuales de los operadores de RBL para incluir en el servicio público de aseo a los recicladores, tal como lo ordeno la Corte Constitucional desde el año 2003?**

- No existe ninguna vinculación, inclusive el mismo Distrito no reconoce en la licitación las acciones afirmativas que ha ordenado la corte, no menciona ninguna actividad en la que se integren a los recicladores, una de las pocas cosas que menciona es implementar contenedores que se le otorgaran a los prestadores del servicio de aprovechamiento, pero en ningún momento menciona cuales son los prestadores, ni cuales son los mecanismos para que los recicladores accedan a dichos contenedores.

- **¿Podemos decir entonces que aunque no se licita el aprovechamiento, mantiene en las mismas condiciones técnicas y operativas precarias que se ha mantenido a la población recicladora?**

- Si, nos mantiene en las mismas condiciones, y aún peor, porque no se mantienen las condiciones que han ganado los recicladores en los últimos años, sino las desmejora, a pesar de que la Corte Constitucional le había ordenado al distrito que no podía desmejorar las condiciones ganadas por la población recicladora, en ningún esquema de aseo permitido por la ley que se quisiera implementar en la ciudad de Bogotá.

- **¿Como ven ustedes el proceso de separación en la fuente en la ciudad, y que tiene que ver esto con la licitación?**

- Es terrible porque al no haber un proceso avanzado de separación en la fuente, esos mismos residuos aprovechables serían los mismos residuos ordinarios, entonces al no haber una clara separación, estaríamos compitiendo -como siempre lo hemos hecho-, con los empresarios del aseo de RBL por recoger los mismos residuos, porque no habría diferencias entre aprovechables y no aprovechables porque todos vienen mezclados.

- **En noviembre de 2016 la UAESP “actualizó” el Plan de Gestión Integral de Residuos Sólidos dejado por la administración PETRO. ¿Ustedes comparten esa actualización del PGIRS hecha por la administración Peñalosa?**

- Nosotros no la compartimos, inclusive tenemos documentos, derechos de petición que le enviamos a la UAESP donde le informamos que nosotros no estábamos de acuerdo con dicha “actualización”, que no es una actualización sino la modificación de todo un PGIRS que se hizo en 2015. En el nuevo PGIRS que presenta la Administración Peñalosa, lo que hace es recortar una serie de recursos, proyectos y programas que ayudaban a la formalización de la población recicladora, viéndose afectada nuestra formalización.

- **¿El Plan de Inclusión que avaló la Corte Constitucional como la política pública para incluir a los recicladores, se ve reflejado en el PGIRS de Peñalosa?**

- Totalmente no, el último informe de la Procuraduría General de la Nación así lo demuestra donde por ningún lado se ve involucrado dicho Plan de Inclusión, a pesar de que ahí si están las condiciones que necesitan los recicladores para formalizarse y lograr prestar el servicio como un operador en dignas condiciones.

- **En entrevista con el Espectador del 19 de Diciembre, Nohra Padilla representante de la ARB señaló que entregar bodegas y carros a las organizaciones es una solución absurda y deteriora la situación de los recicladores… ¿Está usted de acuerdo con esta afirmación?**

- Estoy en total desacuerdo con la representante de la Asociación de Recicladores de Bogotá Nohra Padilla, porque entonces de que forma vamos a operar los recicladores sin tener la infraestructura, sin tener los vehículos, sin tener la ayuda técnica, administrativa, financiera y de logística para prestar el servicio. Si es así prestaríamos un servicio en las mismas condiciones precarias e ineficientes, y lo que ordena la corte es totalmente contrario a lo que afirma la compañera Nohra Padilla en El Espectador.

- **¿Nohra Padilla y la ARB representan los intereses de todos los recicladores de Bogotá?**

- Nohra Padilla no nos representa a nosotros, ni tampoco a otras organizaciones de la ciudad de Bogotá. Existe una alianza de la ARB con una parte de la población bodeguera, nosotros no nos oponemos a que entren los bodegueros al servicio, pero vale recordar que el servicio público de aprovechamiento está regido por el Decreto Nacional 596 de 2016 y la Ley 142 de 1994, y ellos como los demás privados deben cumplir la ley, porque las acciones afirmativas son para la población recicladora, en ningún momento la corte habló de acciones afirmativas para población pudiente como lo son los bodegueros de la ciudad.

- **Hoy el servicio público de aprovechamiento funciona en una “libre competencia informal”, sin mayor regulación… ¿Está usted de acuerdo con esta libre competencia en el servicio público de aprovechamiento de los residuos?**

- No estamos de acuerdo con la libre competencia. Nosotros hicimos propuestas en el PGIRS de la Administración Petro en las que ya hablábamos de unas zonas operativas, donde se hacía una verificación de los recicladores que estaban operando y se empezaban a mejorar las condiciones de los recicladores, convirtiéndolos progresivamente en prestadores del servicio. Estás zonas serían similares a las áreas de servicio exclusivo que tienen los empresarios del aseo de RBL, pero no serían iguales porque se irían implementado progresivamente, y se garantizaría que todas las organizaciones de recicladores tuvieran un sector de la ciudad en el que pudieran operar, ser rentables y prestar el servicio al 100% de los usuarios.

- **¿Han hablado con otros recicladores y recicladoras sobre estos temas?**

- En el último tiempo hemos tenido 4 mesas de trabajo con varios líderes de varias organizaciones, también han asistido recicladores independientes, donde hemos trabajado cuales son los beneficios de unas zonas operativas para el reciclaje, y como esa libre competencia es la que nos tiene en la marginalización, es la que no deja que los recicladores se puedan formalizar. En esta mesa hay líderes como Gustavo Martínez y Paula Rengifo, entre otros que vienen trabajando estos temas con nosotros.

**- ¿Por qué creen ustedes entonces que en el esquema de aseo propuesto por Peñalosa se mantiene en libre competencia el aprovechamiento, mientras que se les dan áreas de servicio exclusivo a los empresarios del RBL?**

Lo que creo es que se quiere mantener al gremio reciclador en la marginalización, y es la única forma para eliminar a los recicladores de la ciudad. Es más, le digo una cosa los empresarios del Aseo dueños de Lime, Aseo Capital, etcétera no prestarían el servicio en libre competencia... imagínese usted un carro de Lime, un carro de Ciudad Limpia, recorriendo toda la ciudad compitiendo por un usuario, eso no es rentable para nadie, financieramente se quebraría una empresa. Esa situación lleva a una competencia desleal entre las empresas que degenera en oligopolios, carteles y alianzas para que las empresas más poderosas financieramente se queden con el mercado… y eso es lo que está pasando en el reciclaje.

- **¿Tiene EMRS propuestas para mejorar el aprovechamiento de los residuos de la ciudad?**

- Nosotros hemos hecho varias propuestas, inclusive le hemos radicado propuestas a la CRA, al Ministerio de Vivienda, a la Procuraduría General de la Nación, a la Superintendencia de Servicios Públicos y a la misma Alcaldía Mayor de Bogotá. Para superar la libre competencia hemos hecho propuesta de Zonas Operativas en las que todas las organizaciones de recicladores cuentan con las garantías económicas, las herramientas jurídicas y la capacidad técnica y operativa para aprovechar al máximo los residuos que generan los bogotanos y garantizar la cobertura del 100% de los usuarios de la ciudad, así se disminuye gran parte de los residuos que llegan al Relleno Sanitario Doña Juana. La base de esa propuesta está en el PGIRS que se construyó en la administración pasada y que Peñalosa derogó con el Decreto Distrital 495 de 2016.

- **¿Cuál es el llamado que le hace usted a los líderes y lideresas de las organizaciones de recicladores, a los recicladores de base y a la misma UAESP sobre el nuevo esquema de aseo y la licitación que está en marcha?**

- La cuestión es que nosotros no podemos aceptar lo que está proponiendo la actual administración, porque de tajo está dejando por fuera a la mayoría de los recicladores de la ciudad, solo algunos pocos podrían estar ahí, pero ni siquiera podría mantenerse sin tener los apoyos necesarios en maquinaría, vehículos, bodegas, equipamiento y asistencia técnica profesional permanente… y el llamado que le hago a los compañeros es a unirnos, a no dejar que esta gente siga pisoteando nuestros derechos.. que empiecen a escuchar las propuestas que tenemos, que les aseguramos que son viables.

- **Se atreverían a hacer ustedes un debate público con las demás organizaciones de recicladores, con la UAESP y con las otras entidades responsables del servicio público de aseo, sobre ¿cuál sería el mejor esquema de aseo para la ciudad?**

- Nosotros llamamos a la UAESP a hacer un debate técnico, estamos en la capacidad de demostrarle a la administración Peñalosa que no solo social, sino técnica y ambientalmente está equivocada, si es que quieren lograr el aprovechamiento de los residuos de la ciudad… Nosotros le hicimos este llamado por City TV, pero este canal editó y cortó la parte donde nosotros le pedíamos a la administración que hiciéramos un debate público para mostrar cuál sería el servicio ideal que le sirve a Bogotá.

###### \* Historiador de la Universidad Nacional de Colombia, Candidato a Magíster en Desarrollo Sustentable y Gestión Ambiental de la Universidad Distrital FJC - Investigador en gestión y aprovechamiento de residuos 
