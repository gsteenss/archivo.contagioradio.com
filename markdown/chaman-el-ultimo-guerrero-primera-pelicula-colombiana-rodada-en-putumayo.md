Title: "Chamán, el último guerrero" primera película colombiana rodada en Putumayo
Date: 2016-07-21 15:23
Author: AdminContagio
Category: 24 Cuadros
Tags: Chaman, Cine Colombiano, el último guerrero, Putumayo
Slug: chaman-el-ultimo-guerrero-primera-pelicula-colombiana-rodada-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/chaman.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Fotograma Trailer "Chamán el último guerrero" 

##### [21 Jul 2016]

Este martes fue presentado oficialmente el trailer de "**Chamán, el último guerrero**", la primera película colombiana independiente rodada completamente en el Putumayo, que se estrenará en salas del país el próximo 13 de octubre.

Jaguar Films y Ruge Films son las productoras responsables de traer la cinta que además de mostrar la majestuosa geografía de la región, busca llevar un mensaje de respeto y un llamado a la protección de sus riquezas naturales ante la creciente avalancha extractivista que se ha volcado sobre todo el país.

Un objetivo reafirmado por el director del largometraje Sandro Meneses,  al asegurar que el filme es "una invitación a tener sentido de pertenencia por el territorio y a mirar con respeto a los demás, a los animales, al agua, a la herencia cultural y a las comunidades indígenas para protegerlos del avance desmesurado de la industria y el desarrollo que lo destruyen todo a su paso, llevados por la ambición y la sed de poder".

El encanto visual de los parajes nativos, encuentra sustento narrativo con la participación de actores naturales pertenecientes a comunidades indígenas del Putumayo y el uso racionado de efectos especiales que le aportan a la historia un toque de misticismo propio de las culturas ancestrales de la Colombia profunda.

<iframe src="https://www.youtube.com/embed/YDlJaIiYJ_w" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
