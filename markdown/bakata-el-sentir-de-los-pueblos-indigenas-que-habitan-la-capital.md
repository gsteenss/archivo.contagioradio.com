Title: "Bakata", el sentir de los pueblos indígenas que habitan la Capital
Date: 2015-12-11 15:06
Category: Comunidad, Nacional
Tags: Bakata reportaje documental, Canal Capital, Documentales indigenas, ONIC, Organizaciones Indígenas de colombia
Slug: bakata-el-sentir-de-los-pueblos-indigenas-que-habitan-la-capital
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/bakata.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: onic.org.co 

##### [11 Dic 2015] 

La noche del jueves 10 de diciembre, fue lanzado oficialmente el reportaje documental "Bakata", coproducido por la Organización Nacional Indígena de Colombia ONIC y Canal Capital, financiado por la Autoridad Nacional de Televisión.

La serie, dividida en 10 capítulos, tuvo su presentación en el auditorio Kimy Pernía Domicó de la ONIC, con presencia de autoridades indígenas, realizadores y protagonistas de los episodios pertenecientes a las comunidades que hacen parte del II Encuentro de Generaciones de la organización.

La producción permite hacer un acercamiento al "vivir del pueblo originario Muiska, así como más de 20 que cohabitan en Bogotá (Bakatà) en medio de las oportunidades y las dificultades que la ciudad y sus procesos pueden ofrecerles en su búsqueda resistente de seguir cultivando su Ser Indígena", dando cuenta del desconocimiento que en la cotidianidad de la ciudad se tiene del aporte vital de los pueblos indígenas.

Los episodios de la serie se emitirán por el Canal Capital a partir del 14 de Diciembre desde las 8 p.m. y en 2016 estarán disponibles en el Centro de Documentación  \`Anatolio Quirà Güaña\` de la ONIC, junto con los 75 programas de "Colombia Nativa" y todo el material audiovisual referente a la  memoria y resistencia de los Pueblos Indígenas.

##### Con información de comunicaONIC 
