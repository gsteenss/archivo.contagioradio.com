Title: Páramo de Guerrero ha perdido el 98% de bosque andino
Date: 2015-11-20 16:07
Category: Ambiente, eventos
Tags: Centro de Memoria Paz y reconciliación, Encuentro Regional somos Páramos de Guerrero, Liceo Integrado, Páramo de Guerrero, Zipaquirá
Slug: paramo-de-guerrero-ha-perdido-el-98-de-bosque-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Páramo-de-Guerrero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [agenciadenoticias.unal.edu.co]

<iframe src="http://www.ivoox.com/player_ek_9458230_2_1.html?data=mpmimpeXdI6ZmKiak5iJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlIa3lIquk9fFsdCfxcqYqdrJttPZ09SYysaPtMbmxc7R0ZDJsIytmYqfl5DIqYzW0Nje18qPpc%2FYytPcj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Johanna González, Ambientalista] 

###### [20 Nov 2015] 

El Páramo de Guerrero, es uno de los más afectados de Colombia, teniendo en cuenta que es el tercer ecosistema de este tipo con más títulos mineros. Además, en los últimos 30 años ha perdido **el 98% de bosque andino y un 30% de la vegetación propia del páramo.**

Es por eso, que este 21 de noviembre desde las 9 de la mañana, se realizará en Zipaquirá el segundo  "Encuentro Regional somos Páramo de Guerrero”, donde se busca que **“todos soñemos el cómo se puede armonizar la conservación del páramo con las actividades campesinas”**, señala Johana Gonzáles, quien hace parte de la Red Somos Páramo de Guerrero.

De acuerdo con la ambientalista, el conflicto en este ecosistema se centra en un plan de ordenamiento ambiental con el que el gobierno busca declarar la zona como una reserva forestal protectora **y Distrito de manejo integrado**, en donde actualmente habitan **10.384 campesinos,** obligándolos a desplazarse de su territorio**.**

Según González, lo más contradictorio y lo que asusta a la comunidad es que suceda lo mismo que pasó hace unos años con el río Ceibas en el departamento del Huila. Este lo declararon zona de reserva forestal, obligaron a los campesinos a dejar su territorio y meses después le dieron permiso a una petrolera para realizar actividades de exploración.

Teniendo en cuenta lo anterior, durante el Encuentro, se fortalecerá las estrategias que se tienen frente a la **agroecología, ecoturismo y otras actividades que respeten el ecosistema pero que a su vez permitan que los campesinos puedan subsistir y permanecer en sus territorios.**

El Encuentro será de 9 de la mañana a 5 de la tarde en el **área múltiple IM Liceo Integrado cra 12 con calle 8 en Zipaquira,** y servirá como antesala para el II Foro Tejiendo Páramos, que se realizará en el Centro de Memoria Paz y Reconciliación.

<iframe src="https://www.youtube.com/embed/ZQnASxJFrxI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
