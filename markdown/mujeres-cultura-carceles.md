Title: Las mujeres y la cultura en las Cárceles del país
Date: 2018-11-09 16:48
Author: AdminContagio
Category: Expreso Libertad
Tags: cultura en las prisiones, mujer, presos politicos
Slug: mujeres-cultura-carceles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/descarga-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio radio 

###### 30 Oct 2018 

Adela Pérez es una ex prisionera política de la entonces guerrilla FARC, ahora movimiento político, que compartió en los micrófonos del Expreso Libertad, la apuesta de las mujeres en la Cárcel del Buen Pastor por generar espacios de cultura para las mujeres que les permitiera construir confianza entre ellas y tener espacios de empoderamiento desde la música, la pintura, el teatro y el general el arte.

<iframe id="audio_29868529" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29868529_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
