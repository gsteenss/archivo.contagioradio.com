Title: En riesgo la vida de Odin Sánchez y las conversaciones con ELN
Date: 2016-11-25 17:31
Category: Nacional, Paz
Slug: odin-sachez-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/eln-Odin-Sánchez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elpais] 

###### [25 Nov 2016]

En un comunicado dado a conocer la tarde de este viernes, la guerrilla del Ejército de Liberación Nacional, ELN, señalan que la intensidad de los operativos militares y el constante desembarco de tropas en el departamento del Chocó están provocando **enfrentamientos diarios que han imposibilitado la entrega de las pruebas de supervivencia de Odin Sánchez, así como el operativo d liberación que estaría en marcha.**

Del mismo modo, en la comunicación dirigida a los países garantes, Ecuardor, Venezuela, Chile, Brasil, Cuba y Noruega, los firmantes, Pablo Beltrán, Aureliano Carbonal y Bernardo Tellez, integrantes de la delegación de paz de esa guerrilla, afirmaron que **las propias conversaciones de paz corren peligro**. De lo que se puede deducir que la liberación de Sánchez es una condición para el inicio de la fase pública. ([Lea también Gobierno le incumple al ELN](https://archivo.contagioradio.com/gobierno-le-incumple-al-eln-comandante-nicolas-rodriguez/))

También señalan que **han preguntado al gobierno nacional si se está desarrollando un operativo de liberación de Odin Sánchez, sin embargo, la respuesta ha sido negativa** “la respuesta ha sido que No las adelantan y que privilegian la acción de la comisión humanitaria para concretar el acuerdo hecho con el ELN”. (Le puede interesar [En qué va el proceso con el ELN](https://archivo.contagioradio.com/avances-las-conversaciones-eln/))

Aseguran que han comparado la respuesta del gobierno con lo que ocurre en el departamento y que concluyen que “o que fuerzas adversas a Santos que operan dentro de las fuerzas armadas estatales, están persiguiendo un resultado trágico de este proceso”

Concluyen que este tipo de operaciones militares buscan deblitar el proceso de conversaciones con el ELN y así hacer fracasar la mesa que estaba preparada para iniciarse el pasado 27 de Octubre “buscan debilitar al gobierno y llevar al fracaso esta mesa de conversaciones con el ELN”.

![comunicado ELN](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/CyIdZ3rW8AAvKmC.jpg){.alignnone .size-full .wp-image-32892 width="1200" height="761"}

###### Reciba toda la información de Contagio Radio en [[su correo]
