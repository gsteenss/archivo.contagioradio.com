Title: Shell suspende operaciones de exploración en la costa de Alaska
Date: 2015-09-28 17:31
Category: Ambiente, El mundo
Tags: La extracción de petróleo supone una liberación de más gases de efecto invernadero, petrolera anglo-holandesa Royal Dutch Shell, riesgo en el ártico
Slug: ecologistas-cantan-victoria-por-cierre-de-compania-petrolera-shell
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/petrolera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### www.nuestromar.org 

###### [Sep 28 2015] 

Ante los malos resultados obtenidos en la búsqueda de yacimientos de gas y petróleo, la compañía **anglo-holandesa Royal Dutch Shell, anunció la detención de sus operaciones de exploración en la costa de Alaska.** Decisión que ha sido celebrada por organizaciones defensoras del ambiente en el mundo.

Mas allá de la presión ejercida por organizaciones como Green Peace en contra de las exploraciones el ártico, las motivaciones que acusa la multinacional para frenar sus operaciones en la zona, por lo menos en el futuro cercano, están relacionadas con los resultados "decepcionantes" obtenidos que no corresponde con la alta inversión realizada hasta la fecha, **cercana a los 7.000 millones de dólares.**

Sin embargo, los ambientalistas consideran la decisión tomada por la compañía un triunfo para la campaña "Salva el ártico" adelantada desde hace más de 3 años, en la que lograron poner en los ojos del mundo la actividad de Shell y la protección de esa región única en el planeta, iniciativa a la que se sumaron al rededor de **7 millones de personas en todo el mundo.**

**"Este es un día decisivo para el Ártico. Es una gran victoria para los millones de personas que estaban en contra de Shell y un desastre para otras compañías petroleras con intereses en la región**. Shell apostó en grande y perdió, tanto en términos de coste financiero y como en su reputación pública”, manifestó KumiNaidoo, Director de Greenpeace Internacional.

Por su parte, Marvin Odum, presidente de Shell en Estados Unidos declaro que la compañía "continua viendo el potencial de la cuenca para la explotación petrolera y el área se está convirtiendo en un lugar estratégico para Alaska y Estados Unidos", teniendo en cuenta que **el Ártico contiene un 22 por ciento de las reservas de gas y petróleo** que aún existen en el mundo, en yacimientos bajo tierra y bajo el mar.

**Mapa de extracción**

Las regiones que se han visto afectadas por la extracción de crudo son **Canadá, Dinamarca, Estados Unidos, Finlandia, Islandia, Noruega, Suecia y Rusia**. La mezcla de petróleo con las aguas del océano causa gran preocupación para los animales que habitan allí como osos polares, zorros árticos, morsas entre otros.

Estos animales corren gran riesgo debido que su hábitat natural deberá cambiar ,esto conlleva a que se generen cambios como su alimentación y lugar de apareamiento, la vida marina estará afectada por la reducción de algas en la capa helada o de fitoplancton de los cuáles se nutren.
