Title: En Putumayo no cesa la violencia contra la comunidad
Date: 2020-06-05 11:29
Author: CtgAdm
Category: Actualidad, DDHH
Tags: pandemia, Putumayo, violencia
Slug: en-putumayo-no-cesa-la-violencia-contra-la-comunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/COMUNICADO-NUEVOS-ASESINATOS.pdf" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En los últimos días se ha hecho aún mas repetitivos los actos de violencia contra las [comunidades rurales](https://archivo.contagioradio.com/ejercito-y-narcotrafico-verdugos-de-la-sustitucion-de-la-coca/), **especialmente a aquellos que exigen el cumplimiento del Acuerdo de Paz**, **y un alto a la erradicación forzada**; y que en los últimos días acabó con la vida de jóvenes en el departamento de Putumayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según información presentada por la [Comisión Intereclesial de Justicia y Paz](https://www.justiciaypazcolombia.com/continuan-asesinatos-en-el-bajo-putumayo/), el primer hecho se registró el 3 de junio sobre las 6:30 de la tarde, en la vereda Kilili, a pocos minutos de Puerto Asís, en donde fue asesinado **Diego Alejandro Núñez joven perteneciente a la comunidad El Silencio, en el corregimiento de Piñuñu Blanco .**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la organización, Núñez **fue atacado por hombres armados con armas de fuego, quienes le dispararon en múltiples ocasiones**, y agregaron que él joven, pertenecía al programa Nacional de Sustitución de Cultivos de Uso Ilícito, en dónde **estaba inscrito como recolector.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ese mismo día algunas horas después, sobre las 9:30 pm en y la vereda Santa Teresa del municipio de Valle de Gómez, a 400 metros de la escuela municipal, **la comunidad encontró el cuerpo sin vida de un joven indígena de 25 años de edad, identificado como Guillermo Jojoa Jamijoy.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según medios locales el joven no tenía ningún tipo de documento que lo identificara, así que, *"fue trasladado a la morgue La Hormiga, lugar en donde horas después llegó un familiar a reconocerlo"*. **Jojoa pertenecía a la etnia indígena de San Miguel y vivía en el resguardo San Fidel**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante los hechos la Comisión señaló qué estos se cometieron en zonas controladas ***"por el grupo armado conocido como La Mafia, estructura heredera del paramilitarismo al servicio del narcotráfico"***, y resaltaron que estos hechos de violencia se dan en medio de la presencia de la Brigada XXVII de selva y la Policía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A estos hechos se suma el del pasado lunes **1 de junio 2020, *"siendo las 10 am, en zona Rural del Municipio de Orito, fue asesinado Hernan Fajardo Figueroa*** *de 26 años de edad, hombres armados le dispararon hasta  
ocasionarle su muerte inmediata"*, denuncia la Red de Derechos Humanos de Putumayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregaron,"*es necesario insistir que cada uno de estos hechos se presentan en zonas fuertemente militarizadas, ampliando el listado de casos en la impunidad, acrecentando incluso el miedo generalizado de la comunidad".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente exigieron respuestas a las investigación y esclarecimiento de estos hechos, y convocaron a las Comunidad Nacional e Internacional, ***"para que en el marco de la defensa de los Derechos visibilicemos y denunciemos las acciones que ponen en riesgo la vida e integridad física de los sectores sociales"***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:file {"id":85085,"href":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/COMUNICADO-NUEVOS-ASESINATOS.pdf"} -->

<div class="wp-block-file">

[*Comunicado por la Red de Derechos Humanos de Putumayo*](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/COMUNICADO-NUEVOS-ASESINATOS.pdf)[Descarga](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/COMUNICADO-NUEVOS-ASESINATOS.pdf){.wp-block-file__button}

</div>

<!-- /wp:file -->

<!-- wp:block {"ref":78955} /-->
