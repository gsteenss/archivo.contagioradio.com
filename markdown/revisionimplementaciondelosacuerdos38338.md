Title: A revisión implementación de los acuerdos de Paz en Cartagena
Date: 2017-03-25 11:58
Category: Paz
Tags: Cartagena, FARC, Gobierno, paz
Slug: revisionimplementaciondelosacuerdos38338
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/El-pais.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País] 

###### [25  Mar. 2017]

A través de un comunicado la Comisión de Seguimiento, Impulso y Verificación a la Implementación -CSIVI- aseguró que se hace necesario **revisar diversos puntos del proceso de implementación del Acuerdo de Paz** entre el Gobierno y las FARC luego de 100 días de haber dado inicio a dicha fase, razón por la cual **durante este fin de semana se reunirán en la ciudad de Cartagena.**

En dichas reuniones pretenden evaluar si han habido o no avances en la fase de implementación,  para que de tal manera se puedan **“tomar las decisiones necesarias para acelerar la implementación de los acuerdos**” dice el comunicado. Le puede interesar: [Se creará Instancia con Pueblos Étnicos para implementación de Acuerdos de Paz](https://archivo.contagioradio.com/se-creara-instancia-con-pueblos-etnicos-para-implementacion-de-acuerdos-de-paz/)

Gobierno y FARC realizarán **jornadas de trabajo por separado durante la jornada del sábado** y para este domingo se articularán en mesas de trabajo conjunto, de tal modo que se puedan **compartir al final de la jornada las conclusiones. **Le puede interesar: [Se creará Grupo de Expertos en Tierras para implementación de Acuerdos de Paz](https://archivo.contagioradio.com/se-creara-grupo-de-expertos-en-tierras-para-implementacion-de-acuerdos-de-paz/)

De esta Comisión hacen parte integrantes del Gobierno como el Alto Comisionado para la Paz, **Sergio Jaramillo**; el ministro del Interior, **Juan Fernando Cristo**, y el consejero para el Posconflicto, **Rafael Pardo** e integrantes de las Farc como **"Iván Márquez", "Victoria Sandino" y "Jesús Santrich**" fue instalada el pasado 2 de diciembre en Bogotá. Le puede interesar: [Los retos de la implementación con enfoque de género](https://archivo.contagioradio.com/los-retos-d-ela-implementacion-con-enfoque-de-genero/)

Cabe recordar que en días pasados se anunció que el expresidente español, **Felipe González, y el de Uruguay, José 'Pepe' Mujica, serán parte del componente internacional de esta Comisión.** Designación que se hará oficial el próximo 30 de Marzo.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
