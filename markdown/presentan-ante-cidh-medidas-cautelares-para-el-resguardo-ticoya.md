Title: Resguardo Ticoya solicita medidas cuatelares ante la CIDH
Date: 2020-09-14 15:08
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Amazonía, CIDH, indigenas, Puerto Nariño, Resguardo Ticoya
Slug: presentan-ante-cidh-medidas-cautelares-para-el-resguardo-ticoya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Captura-de-Pantalla-2020-09-07-a-las-9.29.16-a.-m..png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-14-at-17.13.35.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-14-at-17.13.35-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-14-at-17.14.46.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-14-at-17.13.34.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-14-at-17.13.33.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Las comunidades indigenas que integran el **Resguardo Ticoya, a través de tres organizaciones defensoras de derechos humanos, solicitaron medidas cautelares ante la Comisión Interamericana de Derechos Humanos** (CIDH), producto del olvido sistemático en el que se encuentran y el riesgo que afrontan como pueblos ancestrales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas medidas **instan al Estado de Colombia, a [protejer](https://archivo.contagioradio.com/autoridades-tradicionales-denuncian-agresiones-de-la-policia-en-putumayo/) la vida y la integridad personal de las comunidades** Tikuna, Cocama y Yagua - Ticoya, asentadas a lo largo de los ríos, caños y lagos del municipio de Puerto Nariño.

<!-- /wp:paragraph -->

<!-- wp:image {"id":89866,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-14-at-17.13.33-1024x576.jpeg){.wp-image-89866}  

<figcaption>
*Foto: Camila Muñoz|Cealdes*

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

La acción fue interpuesta, por la [**Comisión de Justicia y Paz**,](https://www.justiciaypazcolombia.com/sos-amazonas-ticoyaporlavida/) el Centro de Alternativas al Desarrollo, (**CEALDES)**, y la organización italiana **Fundación Luca Coscioni**, para evitar el riesgo inminente en el que se encuentran estas comunidades, que ha aumentado producto del contexto de pandemia ante la falta de insumos de bioseguridad, y sistemas de salud que protejan a las personas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "El Gobierno de Colombia no ha cumplido su compromiso con los pueblos indigenas"

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Camila Forero, integrante del equipo jurídico de la Comisión de Justicia** **y Paz**, indicó que estas medidas cautelares, son relevantes y se elevaron a nivel internacional, *"porque **a pesar de que existen decisiones en el sistema judicial interno, sigue estando en riesgo la vida y la integridad de las y los indígenas**, así como su permanencia como pueblo ancestral".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente la defensora señaló que *"**internamente el Gobierno de Colombia no ha cumplido con su deber con los pueblos indígenas, ni a la suscripción firmada en la Convención Americana**, que le obliga a proteger a los pueblos ancestrales".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo, **Lina Caicedo, abogado de CEALDES,** aseguró que las medidas fortalecen la protección de los derechos del Resguardo Ticoya, *"esta acción ante la CIDH, extiende el llamado a los mecanismo internacionales sobre la **importancia de que los Estados atiendan las necesidades particulares y diferenciadas de las comunidades indígenas de la Amazonía"***.

<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":89863,"sizeSlug":"large"} -->

<div class="wp-block-image">

<figure class="aligncenter size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-14-at-17.13.34-1024x576.jpeg){.wp-image-89863}  
<figcaption>
*Foto: Camila Muñoz|Cealdes*
</figcaption>
</figure>

</div>

<!-- /wp:image -->

<!-- wp:paragraph -->

En ese mismo sentido, a la solicitud de medidas cautelares, se sumaron varias acciones de tutela presentadas por el Resguardo Ticoya, que buscan respaldar la exigencia y garantía de derechos que no ha cumplido el Estado colombiano.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### *"Ante la crisis, el Gobierno marginó aún más al Resguardo Ticoya"*

<!-- /wp:heading -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:html /-->

<!-- wp:paragraph -->

Previo a la solicitud de medidas cautelares, distintas organizaciones, incluidas las participantes de la solicitud ante la CIDH, lanzaron la campaña [\#TicoyaPorLaVida](https://archivo.contagioradio.com/en-el-amazonas-por-cada-diez-mil-personas-hay-182-contagios/); con la que se buscaba recolectar donaciones para **proteger a 11 comunidades indígenas de las 22 que integran el Resguardo, identificando a estas como las de mayor riesgo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Iniciativa que surgió con el **objetivo de recolectar elementos de bioseguridad para proteger a los pueblos ancestrales del resguardo ticoya,** en medio del incremento de los casos de contagio en el Amazonas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Humberto Monge , presidente del Resguardo Ticoya**, señaló que ante la falta de recursos hospitalarios y respuestas eficientes, por parte del Gobierno, la única garantía para proteger su vida y atender a la población enferma, era acudiendo a la naturaleza y a la medicina ancestral, ***"a través de sabedoras y médicos tradicionales, obtuvimos un resultado que nos permitió salvaguardar la vida nuestros compañeros y compañeras en medio de esta pandemia".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Reflejo de esta desatención, según Monge es, *"**nuestro centro de salud, ubicado en la cabecera municipal, está en pésimas condiciones, y lo evidenciamos mucho antes de la pandemia**, pero hasta la fecha el Gobierno no nos han dado respuesta".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que, pese a estos esfuerzos, en medio de las acciones tomadas por el Gobierno nacional, a la luz de los estudios realizados y acatando las medidas proferidas por la Organización Mundial de la Salud, ***"se desconoció completamente el enfoque diferencial y las poblaciones étnicas** dejado de lado saberes y costumbres que realizamos como comunidades".*

<!-- /wp:paragraph -->

<!-- wp:image {"id":89861,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-14-at-17.13.35-1-1024x576.jpeg){.wp-image-89861}  

<figcaption>
*Foto: Camila Muñoz|Cealdes*

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### "El riesgo es doble, se pierden vidas y con ellas conocimientos ancestrales"

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Camila Muñoz , investigadora regional en el Amazonas de CEALDES,** destacó que al identificar el panorama de crisis que vivían las comunidades indigenas de Leticia y especialmente en el Resguardo Ticoya, *"era alarmante saber que aquí el riesgo era doble, porqué no solamente se perderían vidas producto de la pandemia, sino que con ellas sus conocimientos ancestrales"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por lo tanto, la tarea de fortalecer sus mecanismos de protección ante la pandemia eran la prioridad, *"sin embargo esto va acompañado de garantías de vida"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"small"} -->

*(Le puede interesar ver el desarrollo de esta información en Otra mirada: \#TicoyaPorLaVida en defensa de los pueblos de la Amazonía)*

<!-- /wp:paragraph -->

<!-- wp:html /-->

<!-- wp:block {"ref":78955} /-->
