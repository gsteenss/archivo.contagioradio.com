Title: Piedad Córdoba pide a Iván Márquez y a Jesús Santrich retomar el camino de la paz
Date: 2020-12-04 16:07
Author: AdminContagio
Category: Actualidad, Nacional
Tags: acuerdo de paz, Iván Márquez, Jesús Santrich, Piedad Córdoba
Slug: piedad-cordoba-pidio-a-ivan-marquez-y-jesus-santrich-retomar-el-camino-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Piedad-Cordoba-en-la-Comision-de-la-Verdad-Santrich-Marquez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Piedad Córdoba / Foto: Comisión de la Verdad

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este jueves la exsenadora Piedad Córdoba dio a conocer en medio de una intervención en la [Comisión de la Verdad](https://twitter.com/ComisionVerdadC/status/1334578016551366661), **una carta dirigida a Iván Márquez y Jesús Santrich en la que los convoca a retomar el camino de la paz, pese a los que calificó como “*incumplimientos, desconocimientos, adulteraciones y actos de perfidia*” que recayeron en contra del Acuerdo de Paz por parte del Estado colombiano.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *“Me duele como a nadie verlos nuevamente levantados en armas, cuando ustedes estaban destinados a cumplir el mismo papel realizado en la construcción del Acuerdo de Paz \[…\] No comparto su decisión pero a diferencia de muchos entiendo que no fue un capricho”.*
>
> <cite>Extracto carta de Piedad Córdoba dirigida a Iván Márquez y Jesús Santrich</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

“*De ustedes escuché en reiteradas oportunidades la frase de Jacobo Arenas que nos recordaba que el destino de Colombia no puede ser la guerra civil*”, expresa en un aparte la exsenadora Piedad Córdoba, instando tanto a Santrich como a Márquez a retomar el camino de la paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la misiva Piedad Córdoba les pide hacerle saber si existe algún camino  para restablecer lo acordado en la Habana y para “*reparar el daño a la paz infligido por el Estado colombiano y sus funcionarios*” e insiste en que se niega a “*creer que el retorno a la guerra se inamovible y que la paz sea una quimera*”. (Le puede interesar: [Chuzadas de la Fiscalía pretendían afectar el Proceso de Paz](https://archivo.contagioradio.com/se-confirma-nueva-accion-para-afectar-el-proceso-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, Piedad Córdoba se expresa particularmente crítica contra el Estado y sus representantes y asegura que el Acuerdo de Paz en la práctica “*no tiene vigor*” por cuenta de los incumplimientos y ataques en contra del mismo, desplegados desde la misma institucionalidad. Asimismo expresa su preocupación por el futuro del Acuerdo y señala que “*se equivoca el Nobel Juan Manuel Santos cuando afirma que la paz no está en peligro*”. (Le puede interesar: [Así fue “el entrampamiento al Acuerdo de Paz” del exfiscal Néstor Humberto Martínez](https://archivo.contagioradio.com/asi-habria-sido-el-entrampamiento-al-acuerdo-de-paz-del-exfiscal-nestor-humberto-martinez/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *“Néstor Humberto \[Martínez\] es un pérfido, así como todos quienes lo respaldaron en su plan. De igual forma ustedes y yo sabemos que quienes dirigieron el operativo del Batallón de Combate No. 12 Diosa del Chairá y las otras agresiones militares en el entonces ETCR de Miravalle, Caquetá durante julio de 2018, cometieron un crimen contra la paz”*
>
> <cite>Extracto carta de Piedad Córdoba dirigida a Iván Márquez y Jesús Santrich</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Finalmente, Piedad Córdoba cerró reafirmando su disposición para contribuir en la superación del conflicto armado y señaló que “*construir garantías y confianzas es una tarea ardua pero no inviable*”, ya que pese a las circunstancias, los tiempos políticos en Colombia y en el continente están a  favor de la paz.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/piedadcordoba/status/1334897998665486336","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/piedadcordoba/status/1334897998665486336

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
