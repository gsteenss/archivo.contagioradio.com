Title: Fiscalía niega asesinatos sistemáticos contra líderes y defensores de DDHH
Date: 2016-12-09 13:21
Category: DDHH, Nacional
Tags: Asesinatos selectivos, Criminalización movimientos sociales
Slug: fiscalia-niega-asesinatos-sistematicos-en-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/pazcalle.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Paz a la Calle ] 

###### [9 Dic 2016] 

Tras la realización de la  Audiencia Que la Paz no cueste la vida, un escenario que sirvió para denunciar y dar un balance  frente a los actos de violencia en torno a los defensores de derechos humanos y líderes pertenecientes a movimientos sociales, la Fiscalía se comprometió a dar soluciones **prontas a estas acciones y a ser más eficaces con las investigaciones que den respuesta a quiénes se encuentran detrás de estos delitos.**

Sin embargo, para el senador Iván Cepeda, por el Polo Democrático, uno de los puntos  polémicos durante la audiencia fue la afirmación del Fiscal General Néstor Humberto Martínez  frente a los asesinatos “**El aseguro que no había evidencia de que se tratara de una cadena de asesinatos de índole sistemática** y ahí es donde está la gran discusión nosotros consideramos que  hay hechos que se han presentado en todo el país que tienen un mismo perfil, que están dirigidos contra un mismo tipo de personas”.

De acuerdo con Cepeda se debe recalcar que **los asesinatos han sido en contra de líderes  y organizaciones políticas y sociales que estaban adelantando funciones especiales en sus territorios**. “Ejemplo de ellos son los líderes asesinados por defender los acuerdos de paz o por hacer parte de procesos de restitución de tierras” afirmó. Le puede interesar: ["Estado no mostró voluntad política en la CIDH frente a desaparición forzada: CCJ"](https://archivo.contagioradio.com/estado-no-muestra-voluntad-politica-en-el-tema-de-desaparicion-forzada/)

Una de las conclusiones de la Audiencia es que esos **actos de violencia podrían haberse disminuido con la activación de mecanismos** que ya se encontraban estipulados en los Acuerdos de Paz y si los mismos estuvieran andando. De otro lado el **cumplimiento de los pactos que genere el gobierno y sus instituciones, deberán estar en constante seguimiento por las organizaciones sociales**, esto con el fin de que no se rompan o quebranten los acuerdos.

Además,  dentro de las conclusiones también se dijo que es importante que e**xista una presencia integral del Estado en los territorios** a través de los organismos institucionales que den las condiciones para que se puede generar un ambiente propicio que contrarreste las expresiones de violencia. Le puede interesar: ["Tras firma del Acuerdo  se han agudizado actos de violencia en contra de líderes sociales"](https://archivo.contagioradio.com/tras-firma-del-acuerdo-se-han-agudizado-actos-violentos-contra-lideres/)

De igual forma, el ente se comprometió a crear una unidad para consolidar toda la información que se ha venido acopiando durante el genocidio de la Unión Patriótica, hasta la actualidad, para dar resultados sobre estos hechos en particular y sus responsables. De igual forma el Fiscal menciono que **ya existen siete capturas frente a algunos de los asesinatos** que se han presentado este año, y que se encuentran adelantando investigaciones frente a los hechos.

<iframe id="audio_14807297" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14807297_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
