Title: 20 familias desplazadas por inundaciones de Hidrosogamoso
Date: 2016-12-06 14:54
Category: Ambiente, Nacional
Tags: Hidroelectrica, hidrosogamoso, ISAGEN
Slug: 20-familias-desplazadas-inundaciones-hidrosogamoso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/WhatsApp-Image-2016-12-06-at-6.49.36-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [6 Dic 2016] 

A través de un comunicado, el Movimiento Social en Defensa de los ríos Sogamoso y Chucurí alertó sobre la grave situación que atraviesan 20 de las familias asentadas aguas abajo de la represa Hidrosogamoso, quienes tuvieron que abandonar sus hogares pues la empresa Isagen ordenó la apertura de compuertas ocasionando grandes inundaciones.

Claudia Ortíz una de las defensoras del territorio, denunció que "estas familias perdieron sus hogares, cultivos y animales, están muy preocupados porque habían adquirido créditos y ahora no van a tener como pagar".

“El flujo natural del río ya no existe, antes se recibía con alegría las 5 o 6 horas de subienda, pero ahora es descontrolado, en 2011 hubo una creciente de 17 días”, asegura Ortíz y añade que "las empresas camuflan el fenómeno climático que generan las represas y los afectados siempre serán as comunidades aguas abajo".

La lideresa comunitaria señaló que “Isagen se acerco el sábado a algunas personas para notificar que abrirían las compuertas , pero no le consultó a la comunidad y aún sin autorización en la noche del domingo procedieron”. Le puede interesar: [73 familias afectadas por hidrosogamoso esperan regresar a sus territorios tras acuerdo con Isagen.](https://archivo.contagioradio.com/73-familias-afectadas-por-hidrosogamoso-esperan-regresar-a-sus-territorios-tras-acuerdo-con-isagen/)

El 5 de Diciembre la empresa se reunió con algunos integrantes de la comunidad “pero no quedó acta, no firmaron ningún compromiso (…) dijeron que se trataba de una operación para iniciar el embalse y que ellos estaban en la capacidad de controlar las inundaciones, pero la comunidad está atemorizada, no es la primera vez que mienten” puntualizó Ortíz.

Por último las familias afectadas y organizaciones sociales, a través de la misiva, solicitan la “intervención inmediata de la ANLA, Defensoría del Pueblo y Unidades de Riesgo del Departamento de Santander para que hagan un informe de los daños y reparen a las comunidades”.

###### Reciba toda la información de Contagio Radio en [[su correo]
