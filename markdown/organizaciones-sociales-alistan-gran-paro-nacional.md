Title: 46 Organizaciones sociales del país se alistan para gran Paro Nacional
Date: 2016-02-04 10:40
Category: Movilización, Nacional
Tags: Paro Nacional, Plan Nacional de Desarrollo, Reforma tributaria
Slug: organizaciones-sociales-alistan-gran-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/DSC1707.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_10315959_2_1.html?data=kpWgk5qdeZqhhpywj5WbaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncZWqjLTfycbSrdvVxM7c0MrXb9TjxM7OzsrXb8XZzZDdw4qnd4a1pdiY1cqPpc3d1NnO0JDUpdPVjMzfj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Luis Alejandro Pedraza, CUT] 

###### [4 Feb 2016] 

46 Organizaciones de carácter social se dieron cita el pasado 2 de febrero para plantear la iniciativa de un próximo paro nacional debido a las últimas decisiones del gobierno que perjudican al pueblo colombiano y en cambio **“privilegian a las multinacionales y al gran capital”,** como  planteado asegura Luis Alejandro Pedraza, presidente de la Central Unitaria de Trabajadores, CUT.

Organizaciones indígenas, comunidades afrocolombianas, estudiantes, dignidades agropecuarias, la Cumbre Agraria, centrales sindicales y representantes políticos, llegaron a la conclusión de **reactivar el Comando Nacional Unitario**, que se encargará de la planeación de lo que será “una gran movilización nacional y un paro nacional”.

El presidente de la CUT, explicó que ese Comando determinará el cronograma, los asuntos que le corresponden a cada organización y la agenda que será presentada al gobierno nacional, donde se evidenciará **“las serias dificultades del Plan Nacional de Desarrollo de Santos que castiga los sectores sociales con una reforma tributaria que es totalmente regresiva para la mayoría de los colombianos”**, también, “las políticas de privatización, como estaría sucediendo con el Acueducto de Bogotá, la ETB, la industria petroquímica, e incluso una posible privatización del sector educativo como lo ha anunciado el alcalde de la capital Enrique Peñalosa”, señala  Pedraza.

Tras la reunión del pasado martes, ahora las organizaciones deberán preparar sus bases internamente para asumir todo lo que corresponde a la difusión de lo planeado en las respectivas localidades y departamentos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
