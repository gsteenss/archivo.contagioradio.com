Title: En menos de 24 horas dos líderes de Colombia Humana son asesinados
Date: 2020-10-20 22:11
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Asesinato a líderes sociales, Asesinato de líder político, Cauca, Colombia Humana, Huila
Slug: en-menos-de-24-horas-dos-lideres-de-colombia-humana-son-asesinados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Cream-and-Blue-Wallpaper.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: tomadas de twitter

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este martes, se dieron a conocer los asesinatos de dos líderes pertenecientes al movimiento Colombia Humana en los departamentos del Huila y el Cauca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

E**l primer hecho se perpetró el lunes 19 de octubre en donde fue asesinado el reconocido líder social y agrario Eduardo Alarcón en Campoalegre, Huila.** (Le puede interesar: [Asesinan al líder Jhon Jairo Guzmán en Tarazá, Antioquia](https://archivo.contagioradio.com/asesinan-al-lider-jhon-jairo-guzman-en-taraza-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1318584306051260418","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1318584306051260418

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El líder de 72 años, **Eduardo Alarcón**, era concejal y militante de la Colombia Humana en el municipio de Campoalegre, fue asesinado aproximadamente a las 11 de la noche en su finca en la vereda Las Vueltas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la información que se dio a conocer, la víctima habría salido de su vivienda al escuchar algunos ruidos en los cultivos de arroz aledaños a su hogar, allí hombres armados le propinaron múltiples impactos de arma de fuego, causando su muerte. Hasta el momento, las autoridades adelantan investigaciones para dar con el paradero de los responsables de este hecho. (Le puede interesar: [«La Minga es una expresión de unidad en medio de la violencia»](https://archivo.contagioradio.com/la-minga-es-una-expresion-de-unidad-en-medio-de-la-violencia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Alarcón, junto con otros campesinos, era reconocido tras lograr que se hiciera la reforma agraria de la región. Además consiguió la parcelación y tierra gratis para los campesinos que cultivan arroz. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según [el Instituto de estudios para el desarrollo y la paz -(Indepaz](http://www.indepaz.org.co/)), ya son 11 los líderes sociales asesinados en lo que va del 2020 en el departamento del Huila; de esta cifra, cuatro eran campesinos. Además, de los 236 los líderes asesinados en todo el país, 72 eran campesinos.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Gustavo Herrera, líder político de la Colombia Humana asesinado en Cauca
------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el departamento del Cauca, este 20 de octubre al medio día, el líder político Gustavo Herrera fue interceptado por hombres armados que dispararon a la camioneta en la que se transportaba por el sector de la Cabrera en la vía que conduce de Popayán al municipio de Coconuco.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1318624143189921795","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1318624143189921795

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El líder que desempeñó como gerente de campaña presidencial del senador Gustavo Petro en el Cauca, fue trasladado a la Clínica La Estancia en Popayán, donde falleció por la gravedad de las heridas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante lo sucedido, el senador Iván Cépeda llamó a la solidaridad con el movimiento político *«que encarna la esperanza de cambio»*, a través de su twitter.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1318621228987539458","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1318621228987539458

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

De acuerdo con Indepaz, con los asesinatos de Eduardo Alarcón y Gustavo Herrera ya son 236 los líderes que han perdido la vida durante este año y 642 desde que Iván Duque asumió la presidencia del país.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
