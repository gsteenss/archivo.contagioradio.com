Title: Asesinan al líder y beneficiario de plan de sustitución Manuel Gregorio González en Córdoba
Date: 2019-06-27 11:35
Author: CtgAdm
Category: Nacional, Paz
Tags: asesinato de líderes sociales, cordoba, Montelíbano
Slug: asesinan-al-lider-social-manuel-gregorio-gonzalez-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Manuel-Gregorio-Sarmiento.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AA] 

Según la Fundación Sumapaz, en horas de la mañana fue hallado sin vida y con signos de tortura el campesino y líder social **Manuel Gregorio González** en el municipio de **Montelíbano, Córdoba**. Manuel era beneficiario del Plan Nacional Integral de Sustitución (PNIS), hacía parte de Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana (COCCAM), de Marcha Patriótica y Asociación de Campesinos del Sur de Córdoba (ASCSUCOR)

El 26 de junio, cerca de las 4:00 pm, Manuel Gregorio salió de su parcela ubicada en las veredas unidas del corregimiento El palmar y en el trayecto a la altura de la finca Santa Catalina, desapareció. Cerca de las 5:20 pm el animal de carga en el que se transportaba y el mercado que había comprado fue regresado por quién sería un miembro del clan del Golfo, según información recibida por ACSUCOR.

Previo al hallazgo de su cuerpo, también fue encontrado el sombrero y la cubierta de machete de Manuel Gregorio. Sobre este territorio la **Defensoría del Pueblo** había advertido en una alerta temprana por cuenta del riesgo evidente para la vida. [(Lea también: Con asesinato de María del Pilar Hurtado, paramilitares siguen cumpliendo amenazas en Córdoba)](https://archivo.contagioradio.com/con-asesinato-de-maria-del-pilar-hurtado-paramilitares-siguen-cumpliendo-amenazas-en-cordoba/)

Noticia en desarollo

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
