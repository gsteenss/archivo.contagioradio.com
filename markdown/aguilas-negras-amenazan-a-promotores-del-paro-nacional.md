Title: Águilas Negras amenazan a promotores del Paro Nacional
Date: 2020-02-06 18:08
Author: CtgAdm
Category: DDHH, Nacional
Tags: Amenazas, Terrorismo
Slug: aguilas-negras-amenazan-a-promotores-del-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Aguilas-Negras.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @IvanCepedaCast {#foto-ivancepedacast .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado miércoles 5 de febrero el senador Iván Cepeda denunció un panfleto amenazante firmado por las Águilas Negras Bloque Capital contra defensores de derechos humanos, figuras del Paro Nacional y periodistas. En el panfleto, se los señala por ser 'caja de resonancia' del ELN y les exigen abandonar sus labores así como desistir de su apoyo al paro nacional.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1225083386634653699","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1225083386634653699

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **La intención de las Águilas Negras con sus panfletos**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el panfleto se menciona al periodista Ignacio Gómez; los senadores Antonio Sanguino y Feliciano Valencia; defensores de derechos humanos como Jael Quiroga, León Valencia y Alirio Uribe; y figuras públicas como David Florez, Diógenes Orjuela o Jennifer Pedraza, entre otros. (Le puede interesar: ["Gobierno guarda silencio ante amenazas de Águilas Negras a dirigentes de izquierda"](https://archivo.contagioradio.com/gobierno-guarda-silencio-ante-amenazas-aguilas-negras-dirigentes-izquierda/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por las personas amenazadas y el momento en que llega el panfleto, David Flórez sostuvo que responde al gran momento de movilización nacional, que encuentra una gran expresión en el paro nacional. En ese sentido, afirmó que con el panfleto "buscan intimidar a personas que hemos participado de una forma del mismo", para inmovilizar a la ciudadanía.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Una estrategia del Estado para desmovilizar?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Organizaciones defensoras de derechos humanos han señalado en distintos informes que 'Águilas Negras' es una ["razón social"](https://pares.com.co/2018/12/29/que-son-las-aguilas-negras/) usada por distintos actores para amenazar a líderes en todo el territorio nacional, tomando en cuenta que nunca se ha capturado a nadie de esa estructura ni se ha encontrado material perteneciente a la misma.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Profundizando esta afirmación, Flores declaró que es claro que en estas amenazas hay participación de agentes del Estado, porque solo la Policía y el Ejército tienen la capacidad organizativa a nivel nacional para hacer seguimientos, control y operaciones contra líderes sociales. (Le puede interesar: ["Águilas Negras amenazan a mesas locales de víctimas y defensores de derechos humanos"](https://archivo.contagioradio.com/aguilas-negras-amenazan/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, sostuvo que no es la única estrategia para la intimidación, pues también está la impunidad: "Porque no es posible que surjan amenazas en todo el país, y no haya una sola captura", mientras la Fiscalía sí se apresura en casos como el de Mateo Gutiérrez, que al final resultan en falsos positivos judiciales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a este tema, en mayo de 2019 se informó de la captura de dos policías en el Magdalena Medio que estarían presuntamente vinculados con las Águilas Negras entre 2008 y 2009; no obstante, se desconoce resultados de rastreos a correos electrónicos o de acciones en contra de este grupo que según el último informe semestral del Programa Somos Defensores (enero a junio de 2019), fue el que más amenazó a líderes sociales (con 141 amenazas).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
