Title: JEP otorga medidas cautelares a archivos de inteligencia del DAS
Date: 2018-03-21 12:19
Category: Otra Mirada, Paz
Tags: archivos de inteligencia, das, JEP, medidas cautelares
Slug: jep-otorga-medidas-cauteles-a-archivos-de-inteligencia-del-das
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [21 Mar 2018] 

[La Jurisdicción Especial para la Paz, que abrió sus puertas para empezar a funcionar el 15 de marzo, elaboró el Auto 001 donde la Secretaría Ejecutiva estableció medidas cautelares anticipadas como **medida para proteger la información de inteligencia** y contrainteligencia del extinto Departamento Administrativo de Seguridad, DAS. Esto permitirá al organismo conservar los documentos relacionados con el conflicto armado.]

[El Auto establece además, que la Oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos, “resaltó la facultad otorgada al Secretario Ejecutivo de la Jurisdicción Especial para la Paz **para ordenar la protección de archivos públicos** o privados que puedan contener información relacionada con el conflicto armado”.]

### [**Archivos del Estado son de interés para la construcción de memoria**] 

[Las medidas cautelares que se otorgan garantizan el acceso a la información y funcionan como una "herramienta fundamental para la **satisfacción del derecho a la verdad** de las víctimas de actuaciones arbitrarias y el derecho a la memoria histórica de la sociedad". En ese sentido los archivos del Estado son “de especial interés para la memoria histórica, la justicia transicional y la construcción de paz”.]

[Además la JEP enfatizó en que la Corte Constitucional “estableció que la medida de reserva legal sobre cierta información **está sujeta a principios de razonabilidad** y proporcionalidad, y deben existir controles administrativos y judiciales sobre la misma toda vez que afecta una serie de derechos fundamentales”.  (Le puede interesar:["Con la JEP llegó la hora para las víctimas"](https://archivo.contagioradio.com/jep_arranca_paz_victimas/))]

[Así mismo, el ente indicó que el Centro Nacional de Memoria Histórica “publicó en el año 2017 la Política Pública de archivos de derechos humanos, memoria histórica y conflicto armado” en donde se reconoce que los archivos de seguridad del Estado “han sido considerados esenciales para la realización de los derechos de las víctimas a la reparación integral, la verdad y la justicia”.]

### **Archivos del DAS son valiosos para la consecución de la verdad** 

[En lo que tiene que ver con la importancia y el riesgo en el que se encuentran los archivos del DAS, la JEP indicó que con ocasión de la supresión del DAS, “el Gobierno Nacional resolvió que **la custodia de los archivos de gastos reservados**, inteligencia y contrainteligencia quedarían a cargo del Archivo General de la Nación”.]

[Son importantes en la medida en que las investigaciones penales, que ya concluyeron o que se encuentran en curso, “se fundamentaron en gran medida en documentos que reflejaban **el presunto actuar delictivo de funcionarios** y empleados del DAS y las relaciones con grupos al margen de la ley”. Por esto para la JEP es importante que se tenga acceso a esta información y así contribuir con la consecución de la verdad. (Le puede interesar[:"Víctimas de crímenes de Estado exigen celeridad en investigaciones de la JEP"](https://archivo.contagioradio.com/victimas_crimenes_estado_jep/))]

[Con esto en mente, el ente transicional decidió que es necesario **otorgar protección** a este tipo de archivos por lo que las medidas cautelares buscarán contrarrestar el riesgo en el que se encuentran estos documentos. Entre algunas consideraciones, el Estado colombiano deberá poner a disposición de la Jurisdicción Especial para la Paz los archivos del DAS “permaneciendo bajo custodia del Archivo General de la Nación”.]

[Auto No. 001 Medidas Cautelares Anticipadas](https://www.scribd.com/document/374535665/Auto-No-001-Medidas-Cautelares-Anticipadas#from_embed "View Auto No. 001 Medidas Cautelares Anticipadas on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_20333" class="scribd_iframe_embed" title="Auto No. 001 Medidas Cautelares Anticipadas" src="https://www.scribd.com/embeds/374535665/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-P6jXRwgGop1BWbZMyyWV&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6522053506869125"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
