Title: En debate de Jurisdicción Especial para la Paz algunos buscan impunidad
Date: 2017-01-31 16:50
Category: Entrevistas, Paz
Tags: comision de la verdad, imelda daza, Implementación de los Acuerdos de paz, jurisdicción especial para la paz
Slug: jurisdiccion-especial-para-la-paz-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/marcha-de-las-flores101316_264.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [31 Ene 2017] 

**La vocera del Movimiento Voces de Paz, Imelda Daza**, aseguró que sectores del congreso apuntan con el debate sobre la Jurisdicción Especial para la Paz –JEP–, “acomodar la ley de manera que sea posible una cierta dosis de impunidad para los victimarios” y que dichos argumentos han sido masificados como “una opinión mayoritaria” a través de los medios, “que niegan la verdad y promueven sólo la opinión de los violentos”.

Daza manifiesta que el sector que más ha obstaculizado el debate sobre la JEP en el Congreso ha sido “la derecha política colombiana que participó de los delitos de lesa humanidad, que ha promovido la violencia vinculada al paramilitarismo”, y resalto que aunque el movimiento Voces de Paz sólo tiene voz en el Congreso, han dejado claro que “es prioritario el respeto a la vida y la dignidad del pueblo, el derecho legitimo que tenemos de participar en este proceso sin ser agredidos ni asesinado”.

Además, señaló que muchas personas que se encontraron culpables de distintos crímenes, en el marco del conflicto armado, “tendrán que confesar verdades para acogerse a la Jurisdicción Especial para la Paz” y eso podría tener repercusiones sobre otros personajes que hasta el momento no han sido juzgados. Le puede interesar: [Voces de Paz rechaza propuesta para detener debates sobre JEP.](https://archivo.contagioradio.com/voces-de-paz-rechaza-propuesta-para-detener-debates-sobre-jep/)

También denuncio que frente a la “violencia que ensombrece el movimiento social y popular” el Gobierno de Juan Manuel Santos no ha “tomado las medidas necesarias” para esclarecer los cientos de asesinatos contra líderes sociales durante el 2016 y el primer mes de 2017, y contrarrestar “el paramilitarismo que no ha desparecido, resucita hoy con mil nombres para seguir haciendo lo mismo”.

### **La Jurisdicción Especial es para dar verdad a las víctimas** 

Imelda Daza, comentó que lo fundamental de la JEP, es la verdad para las víctimas, "pasar la pagina de la guerra perdonando, hay que conocer la verdad para reconciliarnos”, también indicó que por ejemplo “de nada sirve que los responsables del genocidio de la UP se pudran en la picota, queremos saber quienes fueron para poder perdonarlos”.

La vocera dijo que es la Comisión de la Verdad, la encargada de que “se sepa la verdad, se repare el daño y los victimarios se comprometan a que eso nunca más sucederá (…) no hay otro camino para construir paz distinto a la reconciliación”, puntualizó.

<iframe id="audio_16745927" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16745927_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
