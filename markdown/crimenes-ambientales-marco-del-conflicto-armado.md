Title: La Comisión de la Verdad y el esclarecimiento de crímenes ambientales
Date: 2017-01-14 20:02
Category: Ambiente, Voces de la Tierra
Tags: comision de la verdad, conflicto armado en Colombia, crimenes ambientales, paz
Slug: crimenes-ambientales-marco-del-conflicto-armado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/minería-ilegal-en-colombia-e1484439883951.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [Ejercito Nacional de Colombia]

###### [14 Ene 2017] 

Como los ambientalistas han asegurado, la guerra en Colombia no solo ha dejado víctimas humanas, sino que además ha dejado bosques arrasados, ríos contaminados con petróleo, cientos de animales muertos y otros tantos heridos en medio de los bombardeos. Crímenes que hoy los defensores del ambiente esperan sean reparados con la ayuda de la Comisión de la Verdad establecida en La Habana entre el gobierno colombiano y las FARC.

**“Paz para los animales y la naturaleza, ellos también son víctimas”,** fue justamente una de las arengas que acompañó en el 2016, la firma de los acuerdos. Un grito que busca que el ambiente sea reconocido como víctima del conflicto armado colombiano, y que, en medio de la tragedia humana, ha impedido reconocer y contabilizar a los animales, los árboles, los ríos, como víctimas.

Es por ello que desde diferentes lugares del territorio colombiano, se escuchan las voces que buscan la construcción de verdad frente a los crímenes que se cometieron en contra del ambiente, entendiendo este escenario como “una **condición necesaria para la restauración y reparación integral de las víctimas -no humanas y humanas- de la guerra”**, como lo han manifestado organizaciones como Censat Agua Viva.

Aunque en el acuerdo final, no se establece directamente al ambiente como una víctima de la guerra, si se dan las bases para que se esclarezcan crímenes contra la naturaleza, entendiendo que se crea una cadena de victimizaciones que terminan violentando el derecho que tienen las y los colombianos a vivir en un ambiente sano.

### Crímenes ambientales y el conflicto armado 

Censat Agua Viva, habla de casos tales como el emplazamiento de la represa de Urrá I, la palmicultura en Chocó y Tumaco, las fumigaciones en Putumayo y Catatumbo, los derrames petroleros en Arauca, Putumayo y Nariño, el batallón de Alta Montaña en Sumapaz, la desecación de ciénagas en el Caribe, entre otros hechos que han dejado huellas imborrables en los territorios y sus ecosistemas.

El conflicto armado se volvió una excusa para sacar a la fuerza a las comunidades de sus territorios, para que luego estos sean objeto del modelo extractivista. Un ejemplo de ello son las denuncias que ha hecho el Movimiento Ríos Vivos frente al proyecto hidroeléctrico Hidroituango de Empresas Públicas de Medellín. Allí la organización contabiliza entre los años 1999 y 2005, **49 masacres cometidas por grupos paramilitares en los municipios directamente afectados** por la construcción de la represa, que además actualmente está acabando con más de 4 mil hectáreas de bosque seco tropical.

La Ciénaga Grande de Santa Marta es otro caso para resaltar. El complejo de humedales más importante del país se encuentra a punto de desaparecer por las actividades de la compañía agropecuaria RHC. De acuerdo con la Unidad de Víctimas, **150 campesinos que vivían hace 15 años en las tierras de la Ciénaga, fueron víctimas de desplazamiento forzado por parte de grupos paramilitares** al mando de Salvatore Mancuso y Rodrigo Tovar Pupo, alias Jorge 40. Más tarde, esas mismas tierras despojadas, fueron adquiridas por la empresa agropecuaria de Rafael Hoyos Cañavera y filial de Serinco de Córdova S.A., que actualmente construye diques, terraplenes, realiza actividades agropecuarias y de ganadería que están acabando con ese ecosistema.

En medio de la lógica de la guerra, se ha visto un afán por acabar con los cultivos de uso ilícito. Frente a esta actividad, el gobierno colombiano le ha apostado a las fumigaciones con glifosato. Una acción que ha contaminado las aguas, ha contribuido a la deforestación, disminuyendo la disponibilidad de alimentos y afectando la fauna. Además en 2001 la Comisión Europea clasificó el **glifosato como “tóxico para los organismos acuáticos”** y como un producto que puede “acarrear efectos nefastos para el ambiente en el largo plazo”.

Por otra parte, de acuerdo con la Asociación Colombiana del Petróleo, los ataques guerrilleros han causado el **derrame de 4.1 millones de barriles de crudo** en los últimos 30 años. Esas acciones han conllevado graves impactos en la fauna, los ríos, cultivos, selvas y zonas forestales protegidas.

Otro de los crímenes ambientales es la minería ilegal  de la cual se financian las guerrillas**.** Una actividad que ha generado, entre otras consecuencias, que las aguas de la Ciénaga de Ayapel y el río San Jorge tengan un alto porcentaje de mercurio. De esa misma forma han desaparecido otros afluentes del país, y además se ha afectado la fauna y flora especialmente del Chocó y Antioquia.

### ¿Cómo deberían esclarecerse los crímenes ambientales? 

Como ha afirmado la ONU, los delitos ambientales amenazan la paz y la seguridad, y por eso la propuesta de las organizaciones ambientalistas, es analizar los casos que permitan **definir los patrones de acción a emprender para la restauración de ciclos naturales** y territorios afectados por la confrontación armada. Adicionalmente esto permitirá la generación y aplicación de nuevos modelos de reparación social en el proceso de posacuerdo, con base en la definición de responsabilidades para que los actores se comprometan en su no repetición.

La Comisión debería contemplar la reconstrucción de la memoria ambiental y ecológica del país, y con ella la búsqueda de verdad sobre el origen, el desarrollo y las consecuencias de los conflictos socio-ambientales asociados a la confrontación armada, en los que la naturaleza fue escenario y botín  de guerra, siendo no solo  violentada por los bombardeos y fumigaciones, sino también por la **militarización de los territorios que cedieron importantes beneficios a sectores empresariales.**

Lo anterior se logra si se comprende el derecho que tienen las y los colombianos a vivir en un ambiente sano, pero además entendiendo el enfoque territorial que debe de tener en cuenta el Sistema de Verdad Justicia y reparación del acuerdo final de paz, para que la naturaleza empiece a verse como sujeto de derecho.

###### Reciba toda la información de Contagio Radio en [[su correo]
