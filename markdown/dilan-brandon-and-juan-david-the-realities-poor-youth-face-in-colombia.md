Title: Dilan, Brandon and Juan David: the realities poor youth face in Colombia
Date: 2019-11-28 17:43
Author: CtgAdm
Category: English
Tags: Brandon Cely, Dilan Cruz, Juan David Rojas, youth
Slug: dilan-brandon-and-juan-david-the-realities-poor-youth-face-in-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/jovenes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Contagio Radio] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[The tragic deaths of Dilan Cruz, Juan David Rojas and Brandon Cely, who committed suicide after he announced his support for the national strike and was accused by his superiors of “extremist leftism” and of being “manipulative,” are an example of the realities that impoverished youth face in Colombia.]

[The murder of Dilan Cruz, who died after he was struck by a projectile launched by an officer of the National Police’s riot control unit, is only the latest of a string of acts of police brutality that have proved lethal. There are also other cases of fatalities during the protests that have not gained much media attention. Juan David Rojas, a 7th grade student was killed on Nov. 22 during a protest in the Bogotá neighborhood of Bosa.]

[According to Willie Carmona, a professor in Bosa, Rojas was not killed by a police officer but rather a common criminal. Still, his death occurred within the context of the demonstrations and have not attracted as much media attention as the death of Cruz although both were marching to demand their rights. ]

[“The news hasn’t had as much of an echo because it seems that they are trying to frame this murder to appear like that of many others in the country and not within the context of the strike,” said the professor. “This case is the product of an economic model that excludes many layers of people and in particular, youth that does not have guarantees to employment, health, recreation or dignified housing and they end up immersed in contexts where common crime and micro-trafficking prevail.”]

### **There aren’t guarantees for youth in the Army**

[Another example of the difficult reality youth face in Colombia is the recent cases of two soldiers of the National Army that expressed their support for the strike and their right to education. The first, 21-year-old Brandon Cely, dreamed of becoming a doctor but never had the economic conditions to access higher education. After he was stigmatized within the Army for supporting the strike and criticizing the institution, he committed suicide. ]

[The second case is of that of the soldier Juan Sebastián Mendieta, who made a video supporting the strike and who was transferred by his superiors to Bogotá once he announced he would not erase the video. “In practice, the only thing that an 18-year-old from a poor neighborhood can count on is military service, which does not apply to youth from upper classes who have other possibilities,” said the professor. ]

### **Lack of opportunities for youth**

[Carmona said that in Colombia, people between the ages of 18 and 24 are the ones that have the most difficulties finding well-paid work. In Bogotá, only 4% of the population that graduates from public high schools have access to higher education. Such was the case of the high school student Cruz and Cely, who were both barred from going to college because of their economic circumstances.]

[“When one grows up, one clashes with a social reality in which they is no employment, social security nor the possibility to continue your studies and where aggression and stigmatization happen every day in the rural regions. What opportunity does a youth have in this country?” the professor said.]

[Although the realities of these three, young men are that of thousands in the country, the professor underlined the other reality that is beginning to unfold: during the days of strikes and protests, most of those that have flooded the streets are youth. “We have hope that this youth will wake up from the lethargy. These are the two sides of the coin,” said Carmona.]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
