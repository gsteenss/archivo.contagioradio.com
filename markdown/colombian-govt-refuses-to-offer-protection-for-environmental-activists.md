Title: Colombian gov't refuses to offer protection for environmental activists
Date: 2019-10-04 11:49
Author: CtgAdm
Category: English
Tags: Chancellor’s Office, environmental activists, Escazú Agreement
Slug: colombian-govt-refuses-to-offer-protection-for-environmental-activists
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Marcha-por-el-agua-e1464906866438.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: (Colprensa - El Nuevo Día/Jorge Cuéllar)] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[The Chancellor’s Office announced on Sept. 30 that Colombia will not sign on to the Escazú Agreement, arguing that the move would be unnecessary because the country is already committed to other “national and international instruments” that cover the same issues. This news comes as a blow for those who demand more commitment from Colombia when it comes to the protection of environmental activists, given that 24 of the 164 environmental defenders killed last year were Colombian.]

### **Why is the Escazú Agreement necessary?**

[According to the Environment and Society Association, in the last year, more than 10 environmental leaders have been killed in Colombia. This figure reflects the dangers environmental defenders face in Colombia. According to the United Nations, Colombia is the second country in Latin America with the highest number of attacks against social leaders. This has only increased since the signing of the peace deal. ]

[Meanwhile, environmental organizations have denounced an increase in mining operations, massive monocultures that use agrochemicals, an increase in endangered species, illegal explorations in protected territories, among other cases. This negatively affects the environment and advances the rate of climate change. In this sense, the association asked whether if the norms that the Chancellor’s Office contemplates for the protection of nature and its defenders are sufficient and pertinent.]

### **What is the Escazú Agreement?**

[The agreement was born on March 4, 2018, with the endorsement of 24 Latin American and Caribbean states, including Colombia. Originally known as the Regional Agreement on Access to Information, Participation and Justice in Environmental Matters fro Latin America and the Caribbean, it was renamed the Escazú Agreement for the province in Costa Rica where it was approved. ]

[Fourteen countries signed on to the agreement on Sept. 27, 2018 during the annual meeting of the United Nations General Assembly, which includes protocols to protect the environment and environmental activists.]

[The agreement “ensures the full and effective implementation in Latin America and the Caribbean of the rights to access environmental information, public participation in environmental decision-making processes and access to justice in environmental matters, as well as the creation and strengthening of capacities and cooperation, contributing to the protection of the right of each person, of present and future generations, to live in a healthy environment and to sustainable development.”]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
