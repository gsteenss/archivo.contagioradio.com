Title: Victoria sindical contra Coca-Cola en España
Date: 2015-04-16 17:30
Author: CtgAdm
Category: El mundo, Movilización
Tags: Anulan ere coca-cola España, Coca-Cola España, Lucha de los trabajadores de Coca-Cola España, victoria sindical trabajadores España
Slug: victoria-sindical-contra-coca-cola-en-espana
Status: published

###### Foto:Lavanguardia.com 

El tribunal supremo español  ha confirmado la sentencia de la Audiencia Nacional y ha anulado el **ERE (expediente de regulación de empleo) de Coca-Cola por la vulneración del derecho a huelga.**

Con la sentencia **los despidos se convierten en improcedentes** y por tanto se abonan completamente las cantidades asignadas en la indemnización por el despido improcedente, esto **supone también la readmisión de 800 trabajadores**.

A pesar de todo la **multinacional ya ha cerrado las 4 fábricas** y ahora queda en el aire el futuro de los trabajadores que deberían ser readmitidos por orden de la sentencia condenatoria.

A principios de **2004 la multinacional anunció el cierre de 4 de sus 11 fábricas**, fue entonces cuando los trabajadores **comenzaron la lucha** que ha durado un año y medio, además también iniciaron un boicot a todos los productos marca Coca-Cola que produjo el descenso de las ventas en España.

Una de las fábricas, la de **Fuenlabrada en Madrid, ha sido custodiada por los trabajadores durante todo el tiempo** que han durado los juicios y las reiteras huelgas, con el objetivo de que la directiva no pudiera cerrarla. Dicha fábrica ha sido objeto de represión por parte de la policía, donde se han podido ver imágenes de detenciones, enfrentamientos y heridos.

Para la dirección del **sindicato Comisiones Obreras está ha sido una victoria clara del movimiento obrero** y da aliento a otras luchas sindicales ha continuar denunciando el mal uso de los Expedientes de Regulación de Empleo.
