Title: ¿Quién fue Policarpo Guzmán? El histórico líder social de Argelia, Cauca que fue asesinado
Date: 2019-04-11 17:37
Author: CtgAdm
Category: Líderes sociales, Memoria
Tags: Argelia, asesinato de líderes sociales, Cauca
Slug: quien-fue-policarpo-guzman-el-historico-lider-social-de-argelia-cauca-que-fue-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Líder-Policarpo.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Anzorc] 

[En horas de la madrugada del pasado 10 de abril en el corregimiento de El Plateado, municipio de Argelia, Cauca, después de escucharse algunos disparos, fue hallado sin vida el campesino, defensor de DD.HH. y víctima del conflicto,]**Policarpo Guzmán Mage**[, destacado líder de la comunidad quien fue fundador de la Asociación Campesina de Trabajadores de Argelia y la Red de DD.HH.  Francisco Isaías Cifuentes.]

Según **Christian Delgado, coordinador de Garantías y DD.HH. de Marcha Patriótica**, Policarpo quien trabajaba como motero en El Plateado, salió aquel día de su casa, abordó su moto y posteriormente se escucharon varios disparos; quienes lo vieron, afirman que su cuerpo fue encontrado sobre la motocicleta con más de 10 impactos de armas de fuego sobre la cabeza y dos tiros en su cuerpo", junto a él fueron hallados los casquillos de la pistola y todas sus pertenencias personales, lo que evidenciaría la naturaleza del homicidio en una zona en la que los grupos paramilitares han ejercido gran presión.

### **Un organizador nato** 

**Guzmán denunció constantemente la presencia de grupos armados en los lugares en los que habitó**, lo que lo convirtió en "un sobreviviente que en varias regiones se desplazó forzadamente a raíz de estas amenazas"destaca Christian Delgado quien también lo recuerda como "un organizador nato, una persona que buscaba resistir ante las injusticias y cambiar las situaciones en la que se encontraban las comunidades".

Policarpo quien también era esposo y padre, **"fue un líder muy destacado y muy querido por las comunidades"** quien años atrás, a raíz de una de las incursiones paramilitares de Los Rastrojos  en conjunto con operaciones del Ejército se vio obligado a dejar  la región; arribó al Meta donde también trabajó en defensa de los derechos humanos y luego regresó al Cauca, **específicamente al Naya donde lideró procesos juveniles  para regresar dos años más tarde a Argelia.** [(Le puede interesar: Verdad en el asesinato de Temístocles Machado quedó a medias)](https://archivo.contagioradio.com/verdad-temistocles-machado-quedo-medias/)

> Sabes que en esta foto yacen 2 líderes campesinos caucanos muertos en menos de un año?Se llamaban Ibes Trujillo y "Polo" Guzman.Hoy asesinaron a Polito en Argelia. Hasta cuando durará esta horrible noche para el campesinado Colombiano?[@ANZORC\_OFICIAL](https://twitter.com/ANZORC_OFICIAL?ref_src=twsrc%5Etfw) [@ONUHumanRights](https://twitter.com/ONUHumanRights?ref_src=twsrc%5Etfw) [@SemanaRural](https://twitter.com/SemanaRural?ref_src=twsrc%5Etfw) [pic.twitter.com/QnYlGlbm2D](https://t.co/QnYlGlbm2D)
>
> — ACIT (@ACITCAMPESINOS) [11 de abril de 2019](https://twitter.com/ACITCAMPESINOS/status/1116197927624617984?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **La comunidad resiste**

Delgado precisa que la investigación para conocer lo ocurrido ya está en proceso, pero dada la forma tan violenta en la que fue asesinado pareciera que fue un mensaje para atemorizar a la comunidad, pues se trataba del primer presidente de la Asociación Campesina. Aunque existe consternación al interior de la organización y el corregimiento, los pobladores han expresado que "hay que echar pa'lante, cuidarse y proteger los mecanismos de protección comunitaria" pues como indica Delgado, las garantías del Estado han sido nulas en la región.

<iframe id="audio_34370370" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34370370_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
