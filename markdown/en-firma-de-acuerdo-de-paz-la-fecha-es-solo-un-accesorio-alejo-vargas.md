Title: En firma de acuerdo de paz la fecha es sólo un “accesorio”: Alejo Vargas
Date: 2016-01-13 15:38
Category: Entrevistas, Paz
Tags: Fase final conversaciones de paz, Inicio ciclo conversaciones, proceso de paz
Slug: en-firma-de-acuerdo-de-paz-la-fecha-es-solo-un-accesorio-alejo-vargas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Diálogos-de-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: anarkismo.net ] 

<iframe src="http://www.ivoox.com/player_ek_10062959_2_1.html?data=kpWdmJedeZqhhpywj5WdaZS1lJ2ah5yncZOhhpywj5WRaZi3jpWah5yncabijMvW1NLFb8XZjMbQ18rWqNCfxcqY0sbeb83VjMvSxc3Fb8bnjNiSpZiJhpTg0JDi0JCJiZOZmZWSm6jFp8TZjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejo Vargas, U. Nacional] 

###### [13 Ene 2016 ] 

[Frente al inicio del nuevo ciclo de conversaciones entre las delegaciones de paz del Gobierno y las FARC-EP, Alejo Vargas, director del ‘Centro de Pensamiento y Seguimiento al Diálogo de Paz de la Universidad Nacional’, asegura que pese al buen ambiente entre las partes no es realista pensar que en marzo se de la firma del acuerdo final, pues los temas pendientes son álgidos y “**lo menos conveniente en este tipo de procesos de terminación de conflictos armados es poner fechas**, son procesos que merecen su tiempo, el tema de la fecha es secundario”.]

[El analista, considera más acertado pensar en que para mayo o junio de este año la Mesa de Conversaciones haya concluido la discusión de los temas agendados para este ciclo: el fin del conflicto armado; la implementación, verificación y refrendación de los acuerdos y las salvedades que han quedado pendientes en los 3 puntos ya acordados, pues “sin duda es muy probable que en este primer semestre lleguemos a la firma del acuerdo final de paz, **lo fundamental y que debemos tener en cuenta todos los colombianos es que estamos en la fase final y la terminación del conflicto ya se avizora**”.]

[En los siguientes días las delegaciones discutirán temas relacionados con el cese del fuego y de hostilidades bilateral y definitivo; la dejación de armas y reincorporación política, social y económica a la vida civil de los miembros de las FARC-EP; las garantías de seguridad para excombatientes; la lucha estatal contra los grupos criminales que atentan contra la sociedad civil; la adecuación de la institucionalidad para implementación de los acuerdos y la lucha contra el paramilitarismo.]

[“**Las medidas de confianza son importantes para construir un clima adecuado para llegar  a la terminación del conflicto**, y en ese sentido están [pendientes algunos acuerdos](https://archivo.contagioradio.com/presos-politicos-siguen-esperando-gestos-de-paz-del-gobierno/) como el indulto a 30 miembros de las FARC-EP o el mejoramiento de sus condiciones de atención en salud en las cárceles” asegura Vargas, y agrega que se espera que con el inicio del ciclo se traten esos asuntos y se ejecuten acciones concretas al respecto.]
