Title: No tengo nada que conciliar con Nestor Humberto Martinez: Senador Jorge Robledo
Date: 2017-07-12 13:54
Category: Judicial, Nacional
Tags: Fiscalía General de la Nación, Jorge Robledo, Nestor Humberto Martínez, Odebrecht
Slug: jorge-robledo-nestor-humberto-martinez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Néstor-Humberto-Martínez-y-Jorge-Robledo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [12 Jul 2017]

En rueda de prensa ofrecida en el congreso de la república, el Senador del Polo Democrático y precandidato presidencial Jorge Robledo, aseguró que no tiene nada que conciliar con el actual fiscal Nestor Humberto Martinez y recalcó que se **ratifica en todas las denuncias que ha realizado y que vincularían a Martinez** con el escándalo de Odebrecht.

La declaración la hace el senador luego de que la Corte Suprema de Justicia lo citara a una diligencia de conciliación en el marco de una denuncia por injuria y calumnia interpuesta por Nestor Humberto Martinez en razón de las declaraciones del Senador en la que le **exige al fiscal declararse impedido para investigar el caso de Odebrecht** en el que estaría vinculada su firma de abogados y el cargo de Super Ministro que ocupaba en tiempos del contrato de la Ruta del Sol II. [Lea también reforma política no es más que una cortina de humo para que el país se olvide de las investigaciones sobre el caso Odebrecht](https://archivo.contagioradio.com/36606/)

Ante la demanda Robledo aseguró que "Es difícil encontrar un ataque más feroz que este que me está haciendo el Fiscal Martínez" y que además con esa demanda se estaría violando un el principio de inviolabilidad parlamentaria presente en la sentencia SU-049 de 1999 de la Corte Constitucional, y rechazó la denuncia afirmando que "O el Fiscal Martínez no entiende nada de criterios democráticos o le importan un pepino".

**Jorge Robledo aseguró también que el fiscal Nestor Humberto Martinez hace parte de la directiva del partido Cambio Radical** y además contaría con el apoyo del presidente Santos y la bancada de la Unidad Nacional. De estas afirmaciones se podría inferir que la acusación de Robledo gira en torno a los intereses políticos que estaría favoreciendo el fiscal Martinez. [Le puede interesar: Fiscal debe declararse impedido frente al caso Odebrecht](https://archivo.contagioradio.com/fiscal-martinez-declararse-impedido-investigar-caso-odebrecht/)

Aunque Robledo no se refirió concretamente a si acudirá o no a la **citación de la Corte para este 18 de Julio**, si queda claro que no habría punto de conciliación puesto que el senador se ratifica en lo que ha afirmado mientras que Martinez asegura que el Robledo está actuando en razón de su campaña por la presidencia de la República, contienda en la que participaría en 2018.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
