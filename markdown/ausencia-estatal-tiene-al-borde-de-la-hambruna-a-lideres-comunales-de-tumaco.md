Title: Ausencia estatal tiene al borde de la hambruna a líderes comunales de Tumaco
Date: 2020-04-29 21:37
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Abandono Estatal, Tumaco
Slug: ausencia-estatal-tiene-al-borde-de-la-hambruna-a-lideres-comunales-de-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Image-2020-04-29-at-7.03.54-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Tumaco / Cortesía {#foto-tumaco-cortesía .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/federaciones-accion-comunal-narino-sobre-abandono_md_50585976_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt;Federaciones de Acción Comunal de Nariño sobre abandono estatal  

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de la crisis humanitaria que se vive en el país a raíz del Covid-19 y el aislamiento obligatorio que ha obligado a los sectores de la población más vulnerable a cesar sus actividades laborales diarias, las Juntas de Acción Comunal de Tumaco, en Nariño, han hecho un llamado al Gobierno para que atienda la emergencia que viven las comunidades, pues desde que se inició la cuarentena no han recibido ningún tipo de ayuda a nivel local, municipal o nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"**Estamos aguantando física hambre ya que no podemos salir a cumplir con nuestras actividades de rebusque, como comunales no tenemos un sueldo, ni un puesto, ni mucho menos administraciones; por lo tanto, nos toca salir a rebuscarnos el cómo salir a mantener a nuestras familias"** afirma Leeder Rodríguez, vicepesidente de las Federaciones de Acción Comunal frente a la crisis humanitaria que se vive en Nariño.  
  
Para el comunero, solamente se presta atención a Tumaco cuando "ocurren los asesinatos de nuestros líderes, vivimos de la caridad que alguien nos pueda dar, porque el Gobierno no ha asumido su responsabilidad, como si nosotros nos fuéramos colombianos". [(Le puede interesar: Obispos de Cauca y Nariño piden parar el flagelo del narcotráfico)](https://archivo.contagioradio.com/obispos-de-cauca-y-narino-piden-parar-el-flagelo-del-narcotrafico/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Estamos pasando demasiado trabajo cuando lo que hemos hecho es aportarle al país para construirlo desde su inicio, más de 60 años para ayudar a construir el país y el Gobierno nos ha dado la espalda", concluye.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Tumaco rural, el más afectado

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, Luis Vásquez, presidente de la Comuna 3 de Tumaco exigió al [Ministerio del Interior](https://twitter.com/MinInterior) que se tengan en cuenta a las Juntas de Acción Comunal, y en general, a 393 veredas y corregimientos en la parte fluvial y rural del departamento. [(Le puede interesar: Disputa por Triángulo de Telembí en Nariño, impide el regreso de 1.179 familias a su hogar)](https://archivo.contagioradio.com/habitantes-chagui-soluciones-integrales/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Modesto Quiñones, representante de las juntas de acción comunal de las zonas rurales,** advierte que de las zonas rurales es de donde salen los recursos para las grandes capitales, **"lo poco que llega de ayuda se lo reparten aquí en la cabecera municipal, a los amigos políticos de la Alcaldía y la Gobernación, por lo tanto como comunales quedamos por fuera".** [Lea también: En Tumaco opera la justicia de los grupos residuales en medio de la militarización)](https://archivo.contagioradio.com/en-tumaco-opera-la-justicia-de-los-grupos-residuales-en-medio-de-la-militarizacion/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Covid-19 la nueva amenaza en el territorio

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Con respecto a la violencia que a menudo se ve en el municipio de Tumaco, Leeder Rodríguez advierte que aunque se han presentado casos aislados de agresiones, la realidad del Covid-19 ha impedido el avance y actuar de los grupos armados en el territorio. [(Le recomedamos leer: El colegio de Tumaco que se cansó de la muerte: 21 asesinatos de estudiantes en 4 años)](https://archivo.contagioradio.com/el-colegio-de-tumaco-que-se-canso-de-la-muerte-21-asesinatos-de-estudiantes-en-4-anos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Mientras la frontera con Ecuador sigue sin control, los casos de contagio de Covid-19 en Tumaco e Ipiales continúan en aumento,** según el líder social, las adecuaciones del Hospital San Andrés de Tumaco (un centro de salud de segundo nivel) no cumplen con las exigencias para hacer frente a la pandemia. Y advierten que la misma empeorará después del 11 de mayo si no continúa la cuarentena, "no tenemos camas, no tenemos URI, no tenemos respiradores ni las condiciones para atender a la población".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el departamento de Nariño, al 29 de abril han sido registrados 113 casos positivos, 8 fallecidos y 16 recuperados, siendo Ipiales, Tumaco y Pasto las poblaciones más frágiles.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
