Title: Paramilitares amenazan a estudiantes, profesores y trabajadores de la UIS
Date: 2016-06-22 12:08
Category: DDHH, Nacional
Tags: Paramilitares amenazan estudiantes UIS, paramilitarismo en Colombia, uis
Slug: paramilitares-amenazan-a-estudiantes-profesores-y-trabajadores-de-la-uis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/UIS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Taringa ] 

###### [22 Junio 2016 ] 

En la última semana, en los baños de la Facultad de Ciencias Humanas de la Universidad Industrial de Santander, aparecieron panfletos firmados por el grupo paramilitar** Águilas Negras en los que declaran objetivo militar a líderes sociales**, estudiantes y sindicalistas por su supuesta pertenencia a la guerrilla y por su apoyo a las movilizaciones en el marco de la Minga Nacional.

Robinson Duarte, miembro de la Red Juvenil de Unidad Popular y uno de los líderes que fue amenazado, asegura que el comandante de Policía afirmó que en Santander no existen paramilitares; sin embargo, para el estudiante **es evidente que en la región persiste el paramilitarismo,** [[cómo lo confirman diversas investigaciones](https://archivo.contagioradio.com/es-fantasioso-asegurar-que-el-paramilitarismo-no-sigue-vigente/)]. Las directivas de la institución han denunciado la situación ante la Fiscalía, la Policía y la Defensoría, esperando que se investigue a fondo.

<iframe id="audio_11993416" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11993416_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Conozca los panfletos:

[![Panfleto UIS](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Panfleto-UIS.jpg){.aligncenter .size-full .wp-image-25612 width="900" height="1200"}](https://archivo.contagioradio.com/paramilitares-amenazan-a-estudiantes-profesores-y-trabajadores-de-la-uis/panfleto-uis/)

[![Panfleto UIS\_](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Panfleto-UIS_.jpg){.aligncenter .size-full .wp-image-25613 width="429" height="579"}](https://archivo.contagioradio.com/paramilitares-amenazan-a-estudiantes-profesores-y-trabajadores-de-la-uis/panfleto-uis_/)
