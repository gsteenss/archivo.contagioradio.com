Title: Guardia Indigena y Cimarrona serían garantes de acuerdo de paz en sus territorios
Date: 2016-10-06 15:50
Category: Nacional, Paz
Tags: acuerdos de paz, Subcomisión étnica
Slug: guardia-indigena-y-cimarrona-serian-garantes-de-acuerdo-de-paz-en-sus-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/1DSC_0849.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Yumbo.gov.co] 

###### [06 de Oct 2016]

[Comunidades afro e indígenas preparan programa que les permita implementar los acuerdos de paz en sus territorios, para lograrlo la Guardia Indígena y Cimarrona, ya han avanzado en temas como protocolos de seguridad que **generen un relacionamiento tranquilo y pacífico entre las zonas de concentración y sus tierras**.]

[Después de que se conocieron los resultados del 2 de octubre, la subcomisión étnica se reunió en conjunto con las autoridades territoriales para estructurar una propuesta, que será presentada al gobierno y  [que permita que se apliquen los acuerdos de paz en sus territorios, en donde mayoritariamente gano el sí](https://archivo.contagioradio.com/indigena-y-afro-se-unen-para-presentar-propuestas-frente-al-proceso-de-paz/).]

[“Pese a todo lo que esta guerra nos ha generado, estamos muy entusiasmados **porque los acuerdos incorporaron un capitulo étnico**, que es la posibilidad de garantizar, en el posconflicto los derechos fundamentales y consuetudinarios adquiridos por las comunidades” afirmó Hernán Cortez, miembro del Proceso de Comunidades Negras.]

[Por otro lado, frente a la falta de claridades por parte del gobierno  y el movimiento que se está dando de las unidades de la guerrilla del as FARC-EP a zonas seguras, Cortez hizo un llamado a las autoridades e instituciones para que tomen acciones que eviten que retorne la guerra, “[las comunidades indígenas y afrodesendientes hemos sido las más afectadas por el conflicto armado](https://archivo.contagioradio.com/victimas-del-pais-exigen-participacion-directa-en-pacto-politico-nacional/), hemos sido objetivo de los grupos armados tanto de izquierda como de derecha y por eso es importante que los colombianos en general se solidaricen con nosotros”.]

[Durante el proceso de paz en la Habana, se conformó una subcomisión con miembros de las comunidades afros e indígenas para incorporar a los acuerdos de paz unas **salvaguardas y garantías a los derechos de los pueblos étnicos,** de igual modo tanto las delegaciones de las FARC-EP como del gobierno, se comprometieron a involucrar a esta subcomisión en la hoja de ruta de implementación de los mismos.]

<iframe id="audio_13210317" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13210317_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU)] 
