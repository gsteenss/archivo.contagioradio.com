Title: Más de 100 organizaciones del mundo piden aplicación de amnistía a líderes sociales
Date: 2016-12-26 07:42
Category: DDHH, Nacional
Tags: acuerdos proceso de paz, Amnistia, Congreso, Ley de Amnistia, presos politicos
Slug: amnistia-a-lideres-sociales-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Crisis-carcelaria-e1482755978940.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Reder 

###### 26 Dic 2016 

Más de 100 organizaciones sociales de Colombia y del mundo emitieron el 24 de diciembre un comunicado en el que piden que la **ley de amnistía se aplique a líderes sociales colombianos** que están detenidos por haber sido relacionados con delitos de rebelión y que fueron acusados de pertenecer a las FARC o al ELN. Según las organizaciones, **la amnistía contemplada en el acuerdo no es clara en ese sentido**.

**El comunicado, dirigido al presidente Juan Manuel Santos y a Timoleón Jiménez  jefe de las FARC**, expone que los avances en el acuerdo y su aplicación deben tener en cuenta que hay cientos de personas en las cárceles que han recibido condenas por delitos conexos o relacionados con el conflicto armado, quienes deberían recibir también el beneficio de la ley de amnistía que será tramitada este martes en plenarias de Senado Cámara. [27 de Diciembre será la "prueba de fuego" para la ley de amnistía](https://archivo.contagioradio.com/27-de-diciembre-prueba-de-fuego-ley-de-amnistia-33926/)

“Con el objetivo de solicitar se impulsen procesos para levantar las medidas de aseguramiento y/o extinción de la responsabilidad penal de cientos de Defensores de Derechos Humanos y líderes que hoy están privados de la libertad en las cárceles colombianas” expresa el texto firmado por el Movimiento de Trabajadores sin Tierra (MST) de Brasil, entre otras organizaciones representativas del movimiento social en el mundo.

Según las organizaciones, el acuerdo es claro en cuanto a la situación de integrantes de las FARC o agentes del Estado, pero **hay incertidumbre en torno a defensores y líderes sociales.** “Vemos con preocupación cómo se avanza en los temas de la insurgencia, en amnistías e indultos y libertades otorgadas, mientras estamos en la incertidumbre de la situación de nuestros defensores de Derechos Humanos y líderes” Le puede interesar [Cerca de 1500 líderes sociales podrían recuperar su libertad si se aprueba la amnistía](https://archivo.contagioradio.com/cerca-de-1500-lideres-sociales-podrian-recuperar-su-libertad-si-se-aprueba-la-amnistia/)

Las organizaciones firmantes destacan algunas situaciones que las personas privadas de la libertad tienen que sufrir al interior de las cárceles, recordando que **ellos nunca aceptaron los cargos de los que se les acusan por las implicaciones que ello tiene**. “Admiramos que, ante las presiones, nuestros dirigentes hayan resistido a admitir que son de las FARC-EP o del ELN, ya que el solo hecho de admitir que hacen parte, tiene unas repercusiones inimaginables, políticas en sus vidas, sus familias y sus organizaciones”.

Se espera que a Ley de Amnistía, que cumple este 27 de diciembre un trámite crucial en las plenarias de Senado y Cámara en el Congreso de la República, **pueda aclarar la situación de estas personas y también definirlos como objeto de cesación de la persecución penal u otras figuras que permitirían que recobren la libertad de manera inmediata**.

[Carta Organizaciones Sociales y personalidades por la liberación inmediata de defensores de DDHH](https://www.scribd.com/document/335067119/Carta-Organizaciones-Sociales-y-personalidades-por-la-liberacio-n-inmediata-de-defensores-de-DDHH#from_embed "View Carta Organizaciones Sociales y personalidades por la liberación inmediata de defensores de DDHH on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_13804" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/335067119/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-PvGbfBNV3m7S7puCCfhN&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
