Title: Proyecto de Centro Democrático pone en riesgo la verdad de los Colombianos
Date: 2018-10-19 12:47
Author: AdminContagio
Category: Paz, Política
Tags: Centro Democrático, Comisión de Búsqueda de Personas Desaparecidas, comision de la verdad, JEP
Slug: centro-democratico-riesgo-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/22_06_francisco_de_roux_arquivo_pessoal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [19 Oct 2018] 

En audiencia pública inició formalmente el trámite del **Proyecto de Acto Legislativo 087** en Cámara de Representantes, que busca restringir el acceso a información de la Fuerza pública por parte de las instituciones que hacen parte del **Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR).** Durante su presentación, diferentes voces afirmaron que el proyecto podría acabar completamente con el Sistema.

Una de esas voces fue la del **Padre Francisco de Roux, presidente de la Comisión de la Verdad**, quien manifestó su preocupación sobre el proyecto. El sacerdote manifestó que el Sistema fue acordado para llevar a buen fin el proceso de paz, y está completamente fundamentado en la verdad; por lo tanto, **"si se obstruye la posibilidad de llegar a la verdad, será imposible que se logre la paz,** en los términos en que se pensó en la Habana".

En el proyecto promovido por el Centro Democrático, **se prohíbe que cualquier institución integrante del SIVJRNR solicite información sobre hechos como operaciones militares, acciones de inteligencia o contrainteligencia,** y datos sobre la vida profesional o privada de miembros de las "Fuerzas Militares, de la Policía Nacional y de los organismos de seguridad e inteligencia del Estado". (Le puede interesar: ["La paz en Colombia pende de un hilo: Caravana Internacional de Juristas"](https://archivo.contagioradio.com/paz-en-colombia-pende-de-un-hilo/))

Para el religioso, estas limitaciones constituyen una afrenta a los marcos legales nacionales e internacionales que existen sobre estos temas; porque por ejemplo el decreto "desconoce completamente la Ley de Transparencia, en la que se señala que todo ciudadano puede acceder a información referente a derechos humanos o delitos de guerra". (Le puede interesar: ["Inician los encuentros entre la Comisión de la V. y los periodistas en Colombia"](https://archivo.contagioradio.com/inician-los-encuentros-entre-la-comision-de-la-verdad-y-los-periodistas-en-colombia/))

En la exposición de motivos del proyecto, el representante a la Cámara por el Centro Democrático Oscar Darío Pérez, asevera que la intención del mismo es proteger la soberanía nacional, y argumenta, que personas que integran el Sistema son afines a las ideologías de izquierda, e incluso **"muchos han mostrado aquiescencia con algunos de lo más macabros hechos ejecutados por las guerrillas** (...) lo cual hace que se cree un manto de dudas sobre la utilización que se pueda derivar de la información a la cual puedan acceder".

### **"Lo que está en juego no es el Sistema, ni la Comisión, sino el derecho a la verdad que tienen los colombianos"** 

Frente al argumento del Centro Democrático, De Roux sostuvo que **ninguno de los miembros del Sistema tienen interés político**, y son absolutamente independientes de las FARC o el Gobierno pasado. Hecho comprobable, porque fueron elegidos para integrar las diferentes instituciones en procesos públicos, que fueron decididos por distintos actores independientes. (Le puede interesar: ["Ni conflicto ni paz forman parte del lenguaje del gobierno de Duque"](https://archivo.contagioradio.com/ni-conflicto-ni-paz-forman-parte-del-lenguaje-del-gobierno-duque-antonio-madariaga/))

El presidente de la Comisión para el esclarecimiento de la verdad aclaró que esta institución no hace política, no está en contra de nadie y no hace oposición, al contrario, "estamos contra la mentira, contra el miedo y el silencio"; y concluyó, al señalar que de aprobarse el Proyecto, **lo que estaría en riesgo no es el SIVJRNR, ni la institución que él preside, sino un elemento para la dignidad de los colombianos: la verdad.**

<iframe id="audio_29470065" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29470065_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
