Title: En Puerto Gaitán cambian votos por tierras usurpadas: Cepeda
Date: 2015-09-18 17:56
Category: DDHH, Entrevistas
Tags: Centro Democrático, Comunidad El Porvenir, Desplazamiento forzado, Puerto Gaitán, Senador Ivan Cepeda, Usurpación de tierras, victor carranza
Slug: en-puerto-gaitan-cambian-votos-por-tierras-usurpadas-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Josúe-Ardila.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: archivo Josúe Ardila ] 

###### [18 Sept 2015] 

[Iván Cepeda Castro, senador del Polo Democrático Alternativo presentó al Senado una constancia en la que asegura que **el candidato a la alcaldía de Puerto Gaitán por el Centro Democrático, Josué Ardila, está haciendo campaña entregando tierras usurpadas por Víctor Carranza en Puerto Gaitán.**]

[El Senador indica que este hecho afecta directamente a las comunidades del Porvenir, quienes aseguran que hay empresas que quieren despojarlos de las tierras que han habitado por más de 40 años, y se relaciona con la **estructura criminal para transacciones fraudulentas de adjudicación de baldíos de la nación, puesta en marcha por Víctor Carranza**.]

[Tras el seguimiento que el Senador le ha dado al caso, se logró que el Incoder anunciará que iba a ponerse al frente de la problemática, pero no ha habido más que un despliegue mediático, pues **no se ha dado la restitución de las 26 mil hectáreas que la autoridad habría anunciado**. Lo que se ha visto es que **hombres delegados por la familia Carranza están cercando terrenos y desplazando a las comunidades campesinas del Porvenir y el Mataratón**.]

[El Senador asegura que estos hechos permiten probar que **el Incoder se ha desentendido totalmente de los derechos de los pueblos campesinos**, antes bien **continúa patrocinando el despojo de tierras**. Por lo que los campesinos han acudido a servidores públicos para que hacer valer sus derechos.]

[Pese a la denuncia formal por parte del Senador, **el expresidente Uribe presente en la plenaria, evadió declaraciones**. Entre tanto Cepeda espera visitar la región y prestar acompañamiento a las comunidades afectadas.]

[Vea también: [[Comunidad “El Porvenir” en Puerto Gaitán en riesgo de desplazamiento forzado](https://archivo.contagioradio.com/comunidad-el-provenir-en-puerto-gaitan-en-riesgo-de-desplazamiento-forzado/)]]
