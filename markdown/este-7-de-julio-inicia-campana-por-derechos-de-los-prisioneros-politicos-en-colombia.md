Title: Este 7 de Julio inicia campaña por derechos de los prisioneros políticos en Colombia
Date: 2020-06-30 19:57
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: 21n, Expreso Libertad, exprisioneros políticos, prisioneros políticos, Prisioneros políticos en Colombia
Slug: este-7-de-julio-inicia-campana-por-derechos-de-los-prisioneros-politicos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/politika-01-06-2015-presos.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/images-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/unnamed.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/unnamed-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Organizaciones defensoras de derechos humanos y familiares de víctimas de montajes judiciales, lanzarán el próximo **7 de Julio** una campaña de solidaridad hacia los prisioneros políticos en Colombia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Esta iniciativa contará con diversos foros que buscan exponer la situación de persecución al pensamiento crítico en Colombia.

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con Laura Escobar, compañera de Yeison Franco, víctima de un montaje judicial el pasado 21 de Enero del presente año, este tipo de acciones permiten que la sociedad conozca cómo se establecen los falsos positivos judiciales en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, el profesor Miguel Beltrán y la abogada Angie Medine expresan lo importante de que en la academia se abran estos escenarios, debido a que la gran mayoría de víctimas de los montajes judiciales son estudiantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Escuche en este **Expreso Libertad** el surgimiento de esta campaña y la relevancia de hablar de la situación de las víctimas de montajes judiciales en el país.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/891473654683156/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/891473654683156/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

[Vea otros programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
