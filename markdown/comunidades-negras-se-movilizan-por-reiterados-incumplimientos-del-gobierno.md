Title: Comunidades negras se movilizan por reiterados incumplimientos del gobierno
Date: 2016-04-25 13:49
Category: Movilización, Nacional
Tags: Cauca, comunidades afro, Gobierno Nacional
Slug: comunidades-negras-se-movilizan-por-reiterados-incumplimientos-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Mujeres-afro-e1461609635108.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mujeres Negras Caminan 

###### [25 Abr 2016] 

Desde las 9 de la mañana integrantes de los **41 consejos comunitarios afrocolombianos del Norte del Cauca, ACONC,** se movilizan de forma pacífica por la vía panamericana entre Cachimbal-Mandivá y la vereda de Quinamayó, en Santander de Quilichao, para denunciar la actual situación de los derechos humanos que vive esa población.

**El objetivo es presentar ante el gobierno nacional un pliego de peticiones** para que se garantice el acceso a la tierra, la reparación de las víctimas del conflicto y la protección de los líderes sociales y defensores de derechos humanos quienes han recibido múltiples amenazas durante este año.

Las comunidades aseguran que se ven obligadas a movilizarse pues el gobierno nacional les ha incumplido con lo pactado meses atrás como ha sucedido con la compra de tierras y titulación colectiva. A**demás desde el Ministerio del Interior se sigue afirmando que no existen comunidades afrodescendientes en el Norte del Cauca lo cual le ha servido al gobierno para no garantizar el derecho a la consulta previa,** libre e informada y en cambio se sigue dando permisos a empresas para llevar a cabo proyectos de desarrollo.

Así mismo, se reclama  el **incumplimiento sobre el acuerdo de la Salvajina,** firmada en la asamblea permanente en el INCODER, luego de un derrumbe registrado en una mina en Santander de Quilichao.

A las Mujeres Afrodescendientes por el Cuidado de la Vida y los Territorios Ancestrales, también les han incumplido, pues no se ha desarrollado un Plan de Intervención Integral para que las personas no tuvieran que someterse a trabajar en la minería ilegal y así, pudieran tener la oportunidad de volver a sus actividades ancestrales para sobrevivir. Además, la Unidad de Protección les había prometido medidas de seguridad colectivas debido a que las individuales no han sido suficientes, y la población sigue siendo objeto de constantes vulneraciones a sus derechos humanos.

Finalmente, frente al proceso de negociación en La Habana las **comunidades negras asociadas en ACONC expresan su respaldo al CONPA y la Comisión Étnica** que integran afros e indígenas,  reclamando su participación e incidencia en los acuerdos de manera que se garantice un enfoque diferencial en el proceso y se establezca una ruta de participación de esta población.

Desde ACONC, se ha anunciado que **se mantendrán en asamblea permanente hasta que el Gobierno Nacional responda por los compromisos pactados** y que se han incumplido reiteradamente, lo que ven como un **racismo institucional y estructural.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
