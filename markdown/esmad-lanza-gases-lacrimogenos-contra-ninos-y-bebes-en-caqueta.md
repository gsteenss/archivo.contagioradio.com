Title: En Pauji continúa la represión de la mano de las empresas petroleras
Date: 2016-09-02 16:57
Category: DDHH, Nacional
Tags: agresión ESMAD caquetá, ESMAD, extracción petrolera Caqueta, Movilizaciones, Represión
Slug: esmad-lanza-gases-lacrimogenos-contra-ninos-y-bebes-en-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/ESMAD-Caquetá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa Rural ] 

###### [2 Sep 2016 ] 

Este viernes la vía que comunica a Paujil con Cartagena del Chairá amaneció con bloqueos intermitentes por parte de integrantes del ESMAD que reprimen violentamente la movilización de las comunidades del Caquetá, en rechazo a las actividades de exploración petrolera de la empresa Petroseimic. Los pobladores denuncian que sin mediar palabra, **los efectivos comenzaron a lanzar gases lacrimógenos contra los campesinos manifestantes** y contra quienes estaban en los vehículos esperando pasar.

En los hechos resultaron heridas por lo menos dos personas, una de ellas en estado de coma, y **afectados varios niños y bebes que estaban en los vehículos y en las fincas** hacia las que los integrantes del ESMAD lanzaron los gases lacrimógenos y las granadas. En estos momentos la vía permanece con bloqueos intermitentes por parte del ESMAD y el Ejército que también hace presencia en el casco urbano de Paujil en dónde los pobladores buscan como defenderse de los ataques de los que están siendo víctimas.

En el marco de estas movilizaciones contra la explotación petrolera en Caquetá, ha sido constante la [[violenta represión](https://archivo.contagioradio.com/esmad-destruye-tres-fincas-en-medio-de-movilizacion-en-caqueta/)] por parte del **ESMAD, que ha agredido a más de 18 pobladores y destruido tres fincas**. Pese a ello, los campesinos adelantan el proceso para una consulta popular que les permita preservar la biodiversidad endémica del departamento; continúan con sus denuncias a nivel internacional y para dentro de quince días tienen programada una gran marcha.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
