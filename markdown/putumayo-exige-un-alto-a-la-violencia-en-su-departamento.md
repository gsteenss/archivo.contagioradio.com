Title: Juan Carlos Castillo, excombatiente de Farc es  asesinado en Putumayo
Date: 2020-03-31 18:47
Author: CtgAdm
Category: Actualidad, DDHH, Nacional
Tags: DDHH, FARC, narcotrafico, violencia
Slug: putumayo-exige-un-alto-a-la-violencia-en-su-departamento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Asesinan-a-Edwin-de-Jesús-Carrascal-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @SandraFARC

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La violencia en el suroeste del país no se detiene, según la red de Derechos Humanos del Putumayo durante los últimos días, cuatro personas han sido asesinadas; el caso más reciente es el de **Juan Carlos Castillo**, excombatiente, integrante del Espacio de Capacitación y Reincorporación **(ETCR) Heiler Mosquera Carmelita**.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/SandraFARC/status/1244972590868975623","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/SandraFARC/status/1244972590868975623

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Juan Carlos Castillo, tenia 29 años y fue hallado sin vida y con varios impacto de arma de fuego en su cuerpo por indígenas del resguardo Kwisna Cxhab, ubicado en el mismo corregimiento **La Carmelita**, en Puerto Asís.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Wilmar Madroñero, integrante de la Red de Derechos Humanos del Putumayo señaló que, *"La comunidad lo encontró al parecer ya hace 5 días , pero al ver que ninguna autoridad procedió a recogerlo, ellos decidieron enterrarlo"*. Con este caso ya son 14 excombatientes asesinados en Putumayo y 191 en todo el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que el caso de Juan Carlos entristece a la población por dos razones, la primera tiene que ver con que él, hacia parte de una *"población muy especial para nosotros, ellos dejaron las armas por estar tranquilos en un territorio y ahora tienen que afrontar nuevamente la violencia"*; y el segundo con que es caso ya anunciado, donde hacia pocos días se había vivido [algo similar](https://archivo.contagioradio.com/antonio-gallego-mesa-exguerrillero-de-las-farc-es-asesinado-en-la-macarena/).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "En Putumayo en los últimos días han muerto más personas asesinadas que por causa del Covid-19"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Madroñero son tres razones las que han intensificado la persecución asesinato y estigmatización de líderes sociales en el Putumayo; la primera se refiere a la disputa del territorio por parte de grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Se han identificado tres de estos, uno y al cual podemos decir que le corresponden el 90% de los asesinatos en Putumayo es, "La Mafia" que trabaja al servicio narcotráfico"*; el defensor añadió que otro responsable de la violencia es el frente Carolina de las FARC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La segunda razón está relacionada a la presencia de multinacionales, *"el poder que tienen las multinacionales y empresas en relación al tema minero, ha causado no solo un profundo daño ambiental, también amenazas y miedo en la comunidad que defiende su territorio*".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente señaló que la erradicación forzada ha sido uno de los mayores promotores de violencia, pero especialmente del olvido por parte del Estado que desde hace hace años, *"viene anunciando sus tragedias, y de las cuales hoy se han podido registrar mas asesinatos por causa de la violencia que por el coronavirus".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Pedimos un alto al Gobierno, y que valore formas distintas de abordar las situaciones en nuestro departamento"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Madroñero destacó que de cara al tema de los tres [migrantes venezolanos asesinados](https://www.justiciaypazcolombia.com/asesinados-tres-venezolanos/) , se puede reflejar no solo la violencia sistemática que aqueja el territorio, sino también la insuficiencia del Ejército, *"es absurdo pensar que este hecho paso a solo 10 minutos del casco urbano donde tiene jurisdicción el Ejército que tiene como objetivo velar por la seguridad y paz en el territorio".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el mismo sentido afirmó que hoy ven con preocupación esto hechos *"nosotros hemos venido denunciando desde un inicio de que hay que tomar medidas para minimizar esta acción que está viviendo el departamento del Putumayo, así como medidas que realmente presenten resultados que justifiquen la presencia militar empezando por la identificación y captura de los responsables".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Nosotros decimos que si realmente hay un compromiso del Gobierno para trabajar el tema de la salubridad que hoy vive mundo, debe hacer un alto a la erradicación, y buscar construir una propuesta diferente a lo que hoy está planteando, y que no ha lleva nada"*
>
> <cite> Wilmar Madroñero| Red de Derechos Humanos del Putumayo </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y concluyó con el llamado que hacen desde la Red de DDHH para que las misma autoridades cumplan con las medidas sanitarias del Covid-19, *"la erradicación genera concentraciones, de erradicadores, antinarcóticos e integrantes de Ejército, que a su vez causan que cerca de 300 personas salgan a defender su territorio".*

<!-- /wp:paragraph -->
