Title: Guerra y Covid-19, comunidades indígenas y afro bajo doble confinamiento en Valle del Cauca
Date: 2020-04-08 08:09
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: comunidades indígenas, Valle del Cauca
Slug: guerra-y-covid-19-comunidades-indigenas-y-afro-bajo-doble-confinamiento-en-valle-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/pichima-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Picihimá Valle Del Cauca/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Antes de la pandemia del Covid-19 y desde el año pasado ya existía una situación de confinamiento de los indígenas, Wounaan Nonam y las comunidades afrodescendientes del Bajo Calima, Bajo San Juan y del Litoral San Juan debido al control que ejercen estructuras armadas herederas del paramilitarismo, bandas criminales, el ELN e incluso la presencia de la infantería de marina en sus territorios que debería garantizar la seguridad de las comunidades étnicas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Santiago Mera, defensor de DD.HH. de la [Comisión de Justicia y Paz,](https://twitter.com/Justiciaypazcol) dicho confinamiento ha obligado a que las comunidades étnicas del Valle del Cauca hayan entrado en una crisis alimentaria porque no han podido asistir a sus lugares de trabajo, lo que sumado a un intenso verano en la región, ha afectado las cosechas de pancoger. [(Lea también: 500 indígenas Wounaan del resguardo Pichimá permanecen confinados por enfrentamientos armados)](https://archivo.contagioradio.com/indigenas-wounan-del-resguardo-pichima-permanecen-confinados-por-enfrentamientos-armados/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Declarada la pandemia, las comunidades se sienten doblemente confinadas, porque **a pesar de haberse implementado medidas de restricción sobre la movilidad fluvial, hay actores que siguen trasladándose libremente, incluso con la presencia de la Fuerza Armada**, mientras ocurren asesinatos, persecuciones a las lanchas de las comunidades e intimidaciones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Afros e indígenas del Valle del Cauca sin garantías mínimas de salud

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Las comunidades de Santa Rosa de Guayacán, el **Consejo Comunitario de Cabeceras y las comunidades indígenas de Wounaan de Unión Agua Clara y Pichimá Quebrada** han hecho declaraciones de calamidad por el Covid-19, advirtiendo que en las zonas no hay centros de atención ni de salud, en gran parte porque han sido desplazadas y en los compromisos adquiridos por el Gobierno al momento de su retorno, no se ha cumplido con brindarles estas garantías de salud. [(Le puede interesar: Sin garantías del Estado, comunidad indígena de Pichimá Quebrada regresa a su territorio)](https://archivo.contagioradio.com/pichima-quebrada-retorno/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Santiago Mera explica que uno de estos incumplimientos ha sido la no restauración de un puesto de salud en el Bajo San Juan que no ha sido asumido por el Gobierno y que, de ser dotado, podría garantizar atención a más de 3.000 personas de estos territorios. **Mientras, el puesto de atención más cercano se encuentra en Buenaventura a más de cuatro horas en lancha del hogar de las comunidades.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sin centros de atención, las poblaciones dependen únicamente de su medicina tradicional mientras viven una crisis de salud, "existen enfermedades de piel e infecciones intestinales sobre todo en la población infantil, hay diarrea, vómito y dolor de cabeza, sumado a la imposibilidad de adquirir alimentos porque no están entrando a los territorios", explica el defensor de DD.HH.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Con más de 250 casos confirmados de Covid-19 en Valle del Cauca, más de 11.000 casos de epidemia por dengue y la ausencia de agua potable que ha generado las infecciones intestinales en los territorios**, hoy las comunidades indígenas y afro del departamento son más vulnerables que nunca.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Indígenas desplazados hace más de 8 meses siguen esperando atención estatal

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado jueves 2 de abril se cumplieron 8 meses desde que la comunidad Nonam Nuevo Haití, que habitaba en el Resguardo Santa María de Pángala, municipio de Istmina (Chocó), tuvo que desplazarse a Buenaventura e Istmina por confrontaciones entre Fuerza Pública y el Ejército de Liberación Nacional (ELN) así como el posterior bombardeo habría causado (o causó, no sé) la muerte de 5 personas, incluidos dos menores.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Se trata de un desplazamiento de cerca de 16 familias (más de 100 personas), de las cuales 11 están en Buenaventura alojadas en una casa. Según José Bartolo, integrante de la comunidad que tuvo que desplazarse, están enfrentando problemas para obtener su alimentación y tampoco ven próximo el retorno a su territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**La[Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/5-indigenas-habrian-muerto-por-bombardeo-dos-de-ellos-ninos-en-enfrentamientos-con-el-eln/) alertó sobre esta situación cuando habían transcurrido siete meses desde el desplazamiento**, y de acuerdo a Bartolo, aún no han recibido la atención estatal para suplir sus necesidades básicas mientras están desplazados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
