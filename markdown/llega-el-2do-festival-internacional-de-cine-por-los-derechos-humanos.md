Title: Llega el 2do Festival Internacional de Cine por los Derechos Humanos
Date: 2015-03-21 14:36
Author: CtgAdm
Category: 24 Cuadros, DDHH
Tags: Cine alternativo, Cinemateca Distrital, Festival de Cine y Derechos Humanos
Slug: llega-el-2do-festival-internacional-de-cine-por-los-derechos-humanos
Status: published

###### Foto: Festival de Cine y DDHH 

El  Festival Internacional de Cine por los Derechos Humanos – Bogotá es una iniciativa que busca, a través de la realización audiovisual, reflejar el contexto actual del conflicto, la lucha y la legitimidad de los DD.HH. en comunidades de todo el mundo. Se creó para generar un espacio público de diálogo y reflexión, y construir una posición crítica del contexto colombiano y del panorama mundial en el tema.

Para el 2° Festival Internacional de Cine por los Derechos Humanos que se realizará entre el 7 y el 11 de abril de 2015, la convocatoria contó con 1365 películas (largometrajes, cortometrajes, documentales) y 53 países en competencia. La selección oficial en esta edición la conforman 80 producciones audiovisuales de 23 países en competencia, que serán presentadas en diez escenarios de la ciudad (en la página web [www.festivaldecineddhhbogota.com](http://www.festivaldecineddhhbogota.com/) se pueden consultar los seleccionados). Dicha selección se lanzó esta mañana.

Además de la muestra audiovisual en diferentes localidades y escenarios del Distrito, se contempla una agenda académica con expertos nacionales e internacionales.

La inauguración del Festival que se realizará el 7 de abril en el Auditorio Huitaca de la Alcaldía Mayor de Bogotá se hará con el estreno de la película La Sargento Matacho de William González. Con un conversatorio entre este y Alina Hleap. Y la intervención de Guillermo Rivera, Consejero Presidencial para los Derechos Humanos.

El 9 de abril, en el marco del Día nacional de la memoria y la solidaridad con las víctimas, se tendrá un Conversatorio con representantes de algunos de los grupos de víctimas que participaron en la Mesa de conversaciones de La Habana. Este se desarrollará a partir de las 6:30 p.m. en el Aula Máxima de la Universidad de Bogotá y será dirigido por el Padre Francisco Roux.

El acto de premiación y clausura será el 11 de abril desde las seis de la tarde con la presentación de la Orquesta Filarmónica Juvenil de Cámara y trasmitido por Canal Capital.

El Festival cuenta con el apoyo de la Consejería Presidencial para los Derechos Humanos, la Secretaría General de la Alcaldía Mayor de Bogotá, la Dirección Distrital de Relaciones Internacionales, la Alta Consejería por los Derechos de las Víctimas, la Paz y la Reconciliación, la Red de Bibliotecas Públicas (BibloRed), la Fundación Rema, la Universidad Jorge Tadeo Lozano de Bogotá, Canal Capital, entre otras instituciones.

El Festival es organizado por Impulsos Films en alianza con el archivo de Bogotá y la Fundación Konrad Adenauer.

[Programación Oficial Festival de Cine y Derechos Humanos](https://es.scribd.com/doc/259424385/Programacion-Oficial-Festival-de-Cine-y-Derechos-Humanos "View Programación Oficial Festival de Cine y Derechos Humanos on Scribd")

<iframe id="doc_53705" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/259424385/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="60%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
