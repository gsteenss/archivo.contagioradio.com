Title: JEP pide reforzar seguridad de Alfamir Castillo, madre de víctima de ejecución extra judicial
Date: 2019-01-14 13:31
Author: AdminContagio
Category: DDHH, Nacional
Tags: Ataque, Atentado, Ejecuciones Extra Judiciales, JEP
Slug: jep-seguridad-alfamir-castillo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Diseño-sin-título.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Ene 2019] 

La Jurisdicción Especial para la Paz (JEP) pidió, mediante un comunicado a la Unidad Nacional de Protección (UNP), reforzar el esquema de seguridad de Alfamir Castillo, madre de Darbey Mosquera Castillo, víctima de ejecución extra-judicial en 2008. La solicitud se hace luego de que este viernes Alfamir sufriera un atentado con arma de fuego mientras se desplazaba por la vía que conduce de Palmira a Pradera en Valle del Cauca.

La líder, quien ha denunciado constantemente a los miembros de las Fuerzas Militares que asesinaron en Manizales (Caldas) a su hijo, ha recibido amenazas desde 2012, y tras su participación en las audiencias del General (r) Montoya, dichas intimidaciones se han incrementado. (Le puede interesar: ["Los casos por los que debería responder el General (r) Montoya ante la JEP"](https://archivo.contagioradio.com/casos-responder-montoya-ante-jep/))

> Ayer atentaron contra Alfamir Castillo Bermudez, una valiente madre que ha denunciado constantemente a los oficiales que asesinaron a su hijo en un caso de falsos positivos.
>
> Dos hombres desconocidos dispararon 3 veces contra su carro. Esta semana había recibido estos mensajes: [pic.twitter.com/hgvjJPkUoh](https://t.co/hgvjJPkUoh)
>
> — José Miguel Vivanco (@JMVivancoHRW) [12 de enero de 2019](https://twitter.com/JMVivancoHRW/status/1084051470423650304?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
El ataque contra Castillo no logró su objetivo porque ella se desplazaba en la camioneta que le fue asignada por la UNP en 2012, momento en que le fueron otorgadas medidas cautelares de protección por parte de la Comisión Interamericana de Derechos Humanos (CIDH), no obstante, la JEP pidió que se refuerce su esquema, y solicitó a la Fiscalía adelantar las investigaciones sobre las amenazas recibidas. (Le puede interesar: ["Consejo de Seguridad de Naciones Unidas preocupado por asesinato de líderes sociales"](https://archivo.contagioradio.com/onu-preocupada-por-el-asesinato-de-lideres-sociales-en-colombia/))

Sobre el atentado contra la líder se pronunció el Procurador General de la Nación, la Defensoría del Pueblo y la CIDH. De igual forma, la Oficina en Colombia del Alto Comisionado de las Naciones Unidas para los Derechos Humanos, rechazó lo ocurrido e instó a la UNP y la Policía "a adoptar de manera urgente todas las medidas de protección que sean necesarias para garantizar la vida y la integridad de Alfamir Castillo, de su representante legal, y de todas las víctimas que están interesadas en ejercer sus derechos" ante la JEP.

[JEP Rechaza Atentado Contra...](https://www.scribd.com/document/397438188/JEP-Rechaza-Atentado-Contra-Alfamir-Castillo#from_embed "View JEP Rechaza Atentado Contra Alfamir Castillo on Scribd") by on Scribd

<iframe class="scribd_iframe_embed" title="JEP Rechaza Atentado Contra Alfamir Castillo" src="https://www.scribd.com/embeds/397438188/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=false&amp;access_key=key-EUTsmXR9UF3gWDMc1PWn" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="true" data-aspect-ratio="null"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
