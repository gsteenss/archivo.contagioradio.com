Title: Teatro Vreve presenta: La técnica del hombre Blanco
Date: 2015-09-24 20:00
Category: Cultura, Hablemos alguito
Tags: La técnica del hombre Blanco, Teatro Seki sano, Teatro vreve, Temporada teatral, Víctor Viviescas
Slug: teatro-vreve-presenta-la-tecnica-del-hombre-blanco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/tecnica-e1443206929702.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### <iframe src="http://www.ivoox.com/player_ek_8593128_2_1.html?data=mZqmlZaWfI6ZmKiakpaJd6Kkloqgo5mbcYarpJKfj4qbh46kjoqkpZKUcYarpJLBx8bYttCft9fS2MqPtNPZ1Mrb1saJd6KfrcaY1oqnd4a1msjby8jFb8XZzZDV0dLGtsafo9HO0MjTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Víctor Viviescas, Teatro Vreve] 

###### [23 Sep 2015]

Con una trayectoría de más de 15 años de trabajo independiente, El "Teatro Vreve" de Bogotá, es una propuesta estético teatral que le apuesta a la  dramaturgia contemporánea, a partir de montajes propios y adaptaciones de obras de autores de Europa y América.

Victor Viviescas, director, dramaturgo, escritor, investigador y profesor, nació en Medellín y desde hace 20 años se encuentra radicado en la capital, donde fundó junto con un grupo de colegas, provenientes de la academia, el proyecto teatral que explora lenguajes escénicos, destacando la presencia del actor en escena sobre la escenografía y elementos de ambientación.

La producción de El teatro Vreve, incluye las obras Cámara lenta (2013) del Argentino Eduardo Pavlovsky, Salvajes-Hombre de ojos tristes (2005) del austriaco Klaus Händl o La mujer de antes (2009) del alemán Ronald Schimmelpfennig,

*Estados* *de Vulnerabilidad* de Víctor Viviescas; *Somos los que no están muertos – Paisaje con figuras I-V*, creación de Víctor Viviescas y Jaidy Díaz; la obra *Senderos – Hombre que sale a la noche a la caída del sol*, dramaturgia de Milton Lopezarrubla y Víctor Viviescas y las obras de este último autor *Los Adioses de José*, monólogo actuado por Fernando Pautt, *Yellow Taxi o La esquina o Cómo murieron los futbolistas que mataron a Karim* y *La técnica del hombre blanco*, son algunas de las producciones que integran el repertorio de la compañía.

[![hombre blanco](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/hombre-blanco-e1443132793202.jpg){.aligncenter .wp-image-14644 .size-full width="1060" height="516"}](https://archivo.contagioradio.com/?attachment_id=14644)

Del 23 al 26 de Septiembre "La estrategia del hombre blanco" estará en corta temporada en la sala Seki Sano, ubicada en la calle 12 \#2-65

*Sinopsis:*

#### La Técnica del hombre blanco cuenta la historia de Korvan, también llamado Roscoe, quien ha cazado a un hombre negro y lo mantiene escondido en el sótano de su casa. Agnès, su mujer y quien se hace llamar Maggy, sospecha de su marido. En un contexto de agitación social y de enfrentamiento, Korvan altera el comportamiento habitual de quienes se encuentran en conflicto con el gesto inesperado de la captura de Nirvana, el hombre negro. La presencia de este inquilino contra su voluntad desatará el resquebrajamiento de las seguridades y certezas y la caída de las máscaras de esa pequeña sociedad que conforman Korvan con su esposa Agnes y los fantasmas de sí mismos que son Roscoe y Maggy. Hasta que un nuevo equilibrio sea conseguido. 

   
   
   
 
