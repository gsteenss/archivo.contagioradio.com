Title: Entregan en Bogotá el premio Franco Alemán de Derechos Humanos
Date: 2016-12-20 13:30
Category: DDHH, Otra Mirada
Tags: premio derechos humano
Slug: se-entrego-en-bogota-el-premio-franco-aleman-de-derechos-humanos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/premio-franco-aleman-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Naciones Unidas] 

###### [20 Dic 2016]

La embajada de Francia y Alemania, en un gesto de unidad, entregaron este lunes el séptimo '**Premio Franco Alemán de Derechos Humanos Antonio Nariño'  **en Colombia, galardón que es otorgado a organizaciones o proyectos que trabajen en la divulgación, promoción y defensa de los derechos humanos, inspirados en la Declaración Universal promulgada en 1948.

Con la presencia de los embajadores **Michael Bock de Alemania y** el **Embajador de Francia Jean - Marc Laforêt**, así como de **Jean-Marc Ayrault, Ministro de Asuntos Exteriores** y Desarrollo Internacional, la canciller colombiana **María Ángela Holguin** se entregó el premio que contó con 63 postulados.

La organización **Foro Interétnico de Solidaridad Chocó**, una iniciativa de convergencia de más de 78 organizaciones de diversos sectores, fundada en el año 2000, fue la principal galardonada por su trabajo en la visibilización tanto de las violaciones de los Derechos Humanos de las comunidades negras como sus construcciones y sus apuestas en los territorios.

En la ceremonia se entregaron reconocimientos a la la lideresa [**Piedad** **Córdoba**](https://archivo.contagioradio.com/?s=piedad+cordoba) por su labor en la liberación de personas en poder de la guerrilla de las FARC EP y el acompañamiento al proceso de paz; a la **Canciller Holguín** por su trabajo en el marco del proceso de conversaciones de paz con la guerrilla de las FARC y  al **[Programa Somos Defensores](https://archivo.contagioradio.com/?s=somos+defensores)** por su acción en la  protección de los defensores de  derechos humanos.

Igualmente, **Contagio Radio** fue galardonado con una Mención de Honor, en palabras del embajador Francés por aportar a la construcción de la paz a partir de la apuesta de los medios de comunicación y desde un punto de vista diferente.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
