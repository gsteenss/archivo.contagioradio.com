Title: La marginalidad en 'Los Olvidados' de Buñuel
Date: 2016-07-27 13:44
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Cine mexicano, Luis Buñuel, Marginalidad
Slug: la-marginalidad-en-los-olvidados-de-bunuel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/olvidados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotograma: Los Olvidados 

###### [29 Jul 2016] 

Se cumplieron 33 años de la muerte de Luis Buñuel, una perdida lamentable para el mundo del cine puesto que el director español, posteriormente nacionalizado mexicano; representa una de las figuras más importantes y representativas de la historia del séptimo arte.

Referente y pieza clave del movimiento Surrealista, Luis Buñuel dejó una de las filmografías más importantes del siglo pasado, entre la que figuran joyas del cine como ‘Los Olvidados’, ‘El Ángel Exterminador’, ‘Viridiana’, ‘El Discreto Encanto de la Burguesía’, ‘Un Perro Andaluz' (cortometraje escrito, dirigido e interpretado por el propio Buñuel con la colaboración en el guion del pintor Salvador Dalí).

Durante la tercera década del siglo XX, con el estallido de la Guerra Civil española Luis Buñuel decide abandonar su país y exiliarse en el continente americano y luego de un paso poco prospero en Estados Unidos termina asentándose definitivamente en Mexico. Es precisamente en este país donde Buñuel alcanza el que sería el punto más alto de su carrera y es  a esta tierra a quien le dedica la mejor pieza de su filmografía: Los Olvidados.

Buñuel retrata con solvencia la realidad de la vida en un barrio marginal de la Ciudad de México y aunque recoge muchos aspectos del Neorrealismo Italiano, presenta en muchas de sus escenas aquello que hace de Buñuel un director único y es su capacidad para conducirnos a través de los sueños más particulares de sus protagonistas, exponiendo sus deseos más fuertes y en sus miedos más profundos. Las secuencias oníricas y surrealistas de este film son unas de las escenas más bellas no solo del cine de Buñuel sino del séptimo arte.

La calidad de la película y la complejidad de su temática no paso desaparecida. Buñuel obtuvo el reconocimiento al mejor director en el Festival de Cannes y así mismo está cinta fue declarada Memoria de Mundo por la Unesco. Los Olvidados es sin lugar a dudas una de las obras más representativas de la carrera del cineasta Español y uno de los pilares más importantes del cine latinoamericano.

Los invitamos a escuchar está y más historias del cine todos los miércoles desde las 6p.m. en 24 cuadros.

<iframe src="https://player.vimeo.com/video/57837968" width="640" height="471" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[Luis Buñuel, Los olvidados, México, 1950](https://vimeo.com/57837968) from [Exilio Regreso](https://vimeo.com/user15959999) on [Vimeo](https://vimeo.com).
