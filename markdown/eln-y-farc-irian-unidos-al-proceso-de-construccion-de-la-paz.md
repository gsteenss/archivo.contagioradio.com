Title: ELN Y FARC irían unidos al proceso de la paz
Date: 2015-05-17 23:53
Author: CtgAdm
Category: Nacional, Paz
Tags: Ascenso nacionalistas Escoceses, Comando Central COCE, ELN, ELN Nicolas Rodríguez Bautista, FARC, Proceso de conversaciones de paz, Timoleon Jimenez
Slug: eln-y-farc-irian-unidos-al-proceso-de-construccion-de-la-paz
Status: published

A través de un comunicado publicado en la cuenta de Twitter de la radio “Patria Libre” del **Ejército de Liberación Nacional** se publicó un comunicado del Comando Central, COCE, que informa la visión de esa guerrilla respecto al reciente encuentro entre **Nicolás Rodríguez Bautista y Timoleon Jiménez de las FARC**. En el comunicado el ELN señala que se avanzó en “especificar tareas de unidad en el movimiento popular y revolucionario en Colombia”.

El comunicado expresa que asumen “***el reto de un proceso de paz con la responsabilidad y el compromiso histórico que merecen***” desarrollando una agenda seria de negociación que “no genere falsas expectativas” y que apunte a la solución de las “causas estructurales que generaron el levantamiento armado”.

Frente al éxito del proceso de paz el ELN afirma que solamente sería posible si se garantiza la participación amplia de la sociedad y que se acompañe de la movilización para lograr los cambios sociales que se requieren. Resaltan que ese objetivo se lograría “***tomando distancia de la paz exprés como proceso sometimiento de la insurgencia***”.

[![COCE\_ELN\_contagio\_radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/CFQfjnkWIAAdL_a.jpg){.aligncenter .size-full .wp-image-8811 width="1023" height="524"}](https://archivo.contagioradio.com/eln-y-farc-irian-unidos-al-proceso-de-construccion-de-la-paz/cfqfjnkwiaadl_a/)

Adicionalmente el ELN **explica los hechos sucedidos en el municipio de Convención en Norte de Santander,** en los cuales se les acusó de usar como trofeo de guerra la pierna amputada de un integrante de las FFMM. A ese respecto el ELN asegura que la falsa acusación hace parte de una campaña “morbosa” para debilitarlo políticamente frente a la opinión pública y **sacar réditos en la mesa de conversaciones de paz.**

Para aclarar estos hechos esa guerrilla afirma que **podrá decretar un cese al fuego unilateral en la región** y así facilitar las labores de una comisión internacional que investigue los hechos y arroje un informe certero sobre lo ocurrido.

[![hechos\_convencion\_ELN\_contagio\_radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/CFQdGI0WYAElTPZ.jpg){.aligncenter .size-full .wp-image-8812 width="1023" height="524"}](https://archivo.contagioradio.com/eln-y-farc-irian-unidos-al-proceso-de-construccion-de-la-paz/cfqdgi0wyaeltpz/)
