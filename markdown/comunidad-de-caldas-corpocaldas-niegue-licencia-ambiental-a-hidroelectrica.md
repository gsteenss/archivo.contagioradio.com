Title: Comunidades esperan que Corpocaldas niegue licencia ambiental a Hidroeléctrica
Date: 2017-04-05 15:18
Category: Ambiente, Nacional
Tags: Caldas, Corpocaldas, El Edén, Hidroelectrica, Montebonito
Slug: comunidad-de-caldas-corpocaldas-niegue-licencia-ambiental-a-hidroelectrica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Montebonito-caldas-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: historiayregion] 

###### [05 Abr. 2017] 

Este 28 de abril se estará llevando a cabo en la comunidad de Agua Bonita, municipio de Manzanares, departamento de Caldas, una **audiencia pública** en la cual los pobladores **podrán escuchar cuáles serán las modificaciones que se realizarán a la licencia ambiental otorgada a la Central Hidroeléctrica Montebonito S.A.S E.S.P.** e intervenir dando a conocer nuevamente las nefastas consecuencias de este proyecto en sus territorios.

La Central Hidroeléctrica Montebonito S.A.S E.S.P es la encargada de la construcción de un túnel que permitiría la conducción de aguas de la zona para una Hidoeléctrica, que según los pobladores afectaría las veredas La Gallera, La Sonrisa, La Laguna – San Roque, La Suecia, Naranjal, Santa Clara – Madroño, Raizal 1 y Raizal 2.

**Ariel Floresta, habitante de esta comunidad** manifestó que el día de la audiencia estarán participando **esperando que “Corpocaldas no le dé la licencia a esta entidad para que no construyan esa Hidroeléctrica en la zona”**.  Le puede interesar: [100 fuentes de agua están en riesgo en Monte Bonito, Caldas](https://archivo.contagioradio.com/100-fuentes-de-agua-estan-en-riesgo-en-monte-bonito-caldas/)

Según Floresta, las diversas personas integrantes de estas veredas han intentado establecer varias veces comunicaciones con la empresa y con el gerente de Corpocaldas sin haberlo podido lograr “y siguen adelante – con el proyecto” asegura.

**Los pobladores han realizado visitas a la Hidroeléctrica que se encuentra en Bolivia, Caldas constatando de primera mano los daños causados** “y eso ha sido toda una devastación muy horrible a todos los recursos naturales. Y lo que dicen es que ellos – la empresa - hacen carreteras y polideportivos, pero lo realmente importante es el agua (…) **uno va a Bolivia y es desolador las cosas que le muestran a uno, acueductos secos y uno se preocupa”** añadió Floresta. Le puede interesar: [Hidroeléctrica el Edén acaba con el agua de Bolivia, Caldas](https://archivo.contagioradio.com/sin-agua-corregimiento-en-caldas-por-cuenta-de-la-hidroelectrica-el-eden/)

Las comunidades de estas veredas se han opuesto de manera continua a la llegada de las hidroeléctricas a sus territorios, sin embargo, cuenta Floresta que ha sido muy difícil “porque **es una competencia muy injusta porque ahí hay mucho dinero y es muy duro luchar contra ellos y también el gobierno está ahí**, tiene acciones en esas empresas, entonces es muy duro”. Le puede interesar: [Micro-hidroeléctricas en Caldas han secado 19 fuentes de agua](https://archivo.contagioradio.com/construccion-de-hidroelectricas-deja-sin-agua-a-90-familias-en-caldas/)

Sin embargo, **gracias a la fuerte oposición de parte de las comunidades el proyecto, que estaba previsto para que iniciara a principios de 2016**, se ha visto retrasado, sin conocer hasta el momento una nueva fecha para la puesta en marcha de la construcción de este túnel por parte de la Hidroeléctrica. Le puede interesa: [Ríos más biodiversos del mundo se encuentran en peligro por hidroeléctricas](https://archivo.contagioradio.com/biodiversidad-de-los-rios-en-peligro-por-represas/)

<iframe id="audio_17987863" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17987863_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
