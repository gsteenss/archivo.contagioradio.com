Title: Tercerización laboral otro mal en Buenaventura
Date: 2015-09-22 18:59
Category: Economía, Nacional
Tags: afro, afrocolombianos, Concejo afrocolombiano laboral, CUT, Foro afrolaboral colombiano, OIT, tercerización laboral
Slug: tercerizacion-laboral-otro-mal-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:youtube.com 

<iframe src="http://www.ivoox.com/player_ek_8553270_2_1.html?data=mZqilZebdI6ZmKiak5qJd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmMbmxMrfy9%2FFp8qZpJiSpJjSb83Vw9Tfw9GPs9Xm0JDaw9GPqc%2Bfo9rS0Mbaqc%2Fo1tfOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Agripina Hurtado, Consejo Laboral Afrocolombiano] 

###### [22 sep 2015]

La audiencia pública “**Trabajo Digno región de paz**” organizada por la Central Unitaria de Trabajadores (CUT) se realizó en Buenaventura y dejó un balance desalentador en cuanto a las condiciones de trabajo de la comunidad afro y del papel del gobierno para mejorar estas condiciones.

Precarización laboral, pobreza, racismo y ausencia estatal fueron las principales problemáticas que se discutieron en el foro, esto para **generar condiciones laborales a las comunidades negras** del país.

Agripina Hurtado, presidenta del Consejo Laboral Afrocolombiano, indicó que “*promover la **formalización laboral** e invertir en el pacífico, es fundamental para que se pueda hablar de paz y justicia*”.

En el evento participaron distintas organizaciones sociales, organizaciones pertenecientes al **concejo laboral afrocolombiano**, el Embajador de U.S.A y ningún representante por parte del gobierno.

### **Otras Actividades:** 

“**Tercer foro Afrolaboral colombiano**” que se realizará el 24 y 25 de septiembre en Bogotá y organiza la Organización internacional del Trabajo (OIT).

Marcha 5 de octubre en Bogotá y foro para redactar documento dirigido a la mesa de negociación en la Habana, para que se discuta sobre las condiciones de la región pacífica en cuestiones laborales y sociales.
