Title: "ANLA se subordina ante EMGESA" ASOQUIMBO
Date: 2015-06-23 16:42
Category: Ambiente, Nacional
Tags: ANLA, ASOQUIMBO, Autoridad Nacional de Licencias Ambientales, Codensa, Contraloría General de la Nación, El Quimbo, EMGESA, Germán Vargas Lleras, Huila, José Antonio Vargas Lleras
Slug: anla-se-subordina-ante-emgesa-asoquimbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/quimbo11.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Asoquimbo 

<iframe src="http://www.ivoox.com/player_ek_4679059_2_1.html?data=lZukm5WZfY6ZmKialJaJd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRha%2FAopDgx5DXucPj08nW0MaPqtPZz9nSjcaPia67priujdXTtozdz9nS1Iqnd4a1mtjS1ZDJp9Dihqigh6eXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Miller Dussán, ASOQUIMBO] 

###### [23 Jun 2015] 

Según integrantes de ASOQUIMBO, la Agencia Nacional de Licencias Ambientales, ANLA, está subordinada a los intereses de la empresa EMGESA, puesto que las víctimas de la empresa exigieron que la gobernación citara a la ANLA y en dos ocasiones esa entidad ha omitido la asistencia a la audiencia.

Miller Dussán, investigador de ASOQUIMBO denuncia que debido a la **subordinación de la Autoridad Nacional de Licencias Ambientales, ANLA,  frente a EMGESA**, el Gobierno del Huila comunicó a ASOQUIMBO, que atendiendo a las denuncias de las comunidades, se ha solicitado dos veces audiencia pública ambiental ante la ANLA, sin embargo, la respuesta siempre ha sido negativa, razón por la que la Contraloría General  de la Nación cuestiona el accionar de la ANLA, teniendo en cuenta que **no se ha realizado ningún tipo de seguimiento frente a las afectaciones que han sufrido los pobladores.**

“Sin argumentos de fondo y forma, la ***ANLA ha rechazado las solicitudes de audiencia pública***”,  dice Miller Dusán, quien agrega que eso mismo ha sucedido  con otras tres solicitudes formuladas desde el 2012, que había hecho la organización a la que él pertenece.

La Contraloría ha evidenciado que la ANLA está cumpliendo con sus deberes constitucionales de atender a las denuncias ambientales que ha presentado la comunidad y en cambio **“es una subsidiaria, subordinada a los intereses económicos de la empresa”,** afirma Dussán.

De acuerdo a un comunicado de ASOQUIMBO, la subordinación de la ANLA se demuestra porque: “se ha modificado la Licencia Ambiental con relación a los compromisos y ampliación de los plazos de cumplimiento de las obligaciones sociales y ambientales (un solo ejemplo, la  resolución 0395 del 02 de mayo del 2013). Por otro lado, se ha negado a responderle a la CAM las "medidas preventivas", y no cumplió con el Plan de Mejoramiento solicitado por la Contraloría frente a 14 graves hallazgos que atentan contra la vida de los colombianos. Además, frente a los Informes de Cumplimiento Ambiental -ICA-, acepta y acomoda las "verificaciones de campo" a lo dispuesto por EMGESA" expresando que SI ha cumplido con todos los compromisos de la Licencia Ambiental”.

**Un ejemplo concreto es que se ha hecho 14 modificaciones a la licencia ambiental,** por cuenta de las solicitudes de EMGESA para aplazar los tiempos de reasentamiento de las comunidades, dice el investigador de ASOQUIMBO. Pese a la ampliación del tiempo la fechase cumplió el pasado 30 de agosto de 2014 y EMGESA nunca cumplió, por su parte, la ANLA no se ha manifestado al respecto. **Mientras tanto la compañía sacó resoluciones para el desalojo violento de las comunidades,** “en este momento hay órdenes de desalojar a mil pescadores, y se ha empezado un proceso de deforestación que genera graves consecuencias ambientales y se corre el riesgo de desaparición de la pesca artesanal e industrial”, señala el investigador.

Para Miller Dusán, la respuesta es simple, la ANLA se ha subordinado ante EMGESA de acuerdo a los intereses económicos que hay detrás, teniendo en cuenta que se le ha cedido el 14.07% de las acciones a altos cargos del gobierno. Por ejemplo a **José Antonio Vargas Lleras, quien hace parte de la junta directiva de empresas como CODENSA y EMGESA y además es hermano del vicepresidente de Germán Vargas Lleras.**

Debido a esa situación, **a partir del 13 de junio desde ASOQUIMBO, se empezó una movilización permanente que concluirá en Bogotá en la Plaza de Bolívar,** previo a una audiencia pública ambiental donde participarán las organizaciones en defensa de la vida y el territorio.

Cabe recordar que EMGESA solo reconoce que ha afectado a **3 mil personas, desconociendo que en total son aproximadamente 30 mil los pobladores** que han sufrido las consecuencias de esta obra. Además la construcción de la hidroeléctrica ya ha dejado más de 12 mil familias desplazadas. Por otro lado, de 2.900 hectáreas de tierra que debían ser entregadas a los campesinos, “no se ha entregado ni un milímetro de tierra a la fecha”, denuncia Miller Dusán.
