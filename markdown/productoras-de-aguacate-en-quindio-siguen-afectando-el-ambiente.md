Title: Productoras de aguacate en Quindío siguen afectando el ambiente
Date: 2019-10-01 18:31
Author: CtgAdm
Category: Ambiente
Tags: aguacate hass, Ambiente, Comercio, Quindío, Salento
Slug: productoras-de-aguacate-en-quindio-siguen-afectando-el-ambiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/hass-avocado-2685821_960_720.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Sandid] 

Organizaciones ambientalistas denunciaron de manera pública los daños en el ecosistema que ha generado la falta de control en la producción de aguacate hass en el Quindío, que han afectado al rededor de 2.600 héctareas, y dejado varios terrenos según la denuncia destruidos en zonas como Navarco en  Salento, y adjudican la poca regulación y atención en este caso a la Corporación Regional del Quindío (CRQ). (Le puede interesar: [Aguacate hass ¿una amenaza para el paisaje cafetero?](https://archivo.contagioradio.com/aguacate-hass-amenaza-paisaje-cafetero/))

Según Néstor Ocampo, ambientalista y presidente de la Fundación Ecológica Cosmos, en el último año y luego de las denuncias hacia los daños ambientales generados por las empresas aguacateras,  los cultivos se han duplicado o triplicado  de manera desmesurada en esta zona,"daños como cobertura forestal de algunas quebradas,  vertimiento de aguas residuales sin el debido proceso o el permiso de la Corporación, fuentes hídricas que se encuentran en la parte alta del Río Quindío, y que terminan afectando la salud de quienes consumen este recurso", añadió Ocampo.

Adicional el ambientalista resalta que la Corporación  ha tardado mas de un años en presentar  cargos hacia  empresas aguacateras como lo son  CampoSol de Perú, Altos del Valle,  Green SuperFood las dos de  Chile y diferentes invesionistas mexicanos que cultivan en esta zona y han causado según los pobladores graves daños en  los terrenos,"estas empresas han estado evitando las leyes, y por ello no se les han adjudicado cargos que corresponden con denuncias que se han hecho hace un año", agregó Ocampo.

### ¿Qué esta pasando con los entes reguladores?

Según Ocampo estas no son las primeras denuncias que presentan ante la CRQ , de hecho hace un años se presentaron  acusaciones sobre apertura de carreteras sin ningún cuidado, ocupacional indebida de causes, cierre de caminos tradicionales, uso indebido de agroquimicos  y venenos en la parte alta de cuencas hidrográficas. (Le puede interesar: [Impulsan consulta popular para salvar el páramo de Chilí en Quindio](https://archivo.contagioradio.com/consulta-popular-cordoba-quindio/)).

Ante ello hacen un llamado a una mayor diligencia por parte de la Corporación para que la producción  de aguacate  de estas empresas no sigan creciendo y afectando el ecosistema del Quindío, ni tampoco se sigan pasando por alto las denuncias frente a los casos de afectaciones ambientales en otros territorios.

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
