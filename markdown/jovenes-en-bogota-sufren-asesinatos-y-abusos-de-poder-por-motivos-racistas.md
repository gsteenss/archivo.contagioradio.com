Title: Jóvenes en Bogotá sufren asesinatos y abusos de poder por motivos racistas
Date: 2015-04-16 16:16
Author: CtgAdm
Category: Comunidad, Nacional
Tags: afro, cimarron, ciudad bolivar, juan de dios quintero, Paramilitar, soacha
Slug: jovenes-en-bogota-sufren-asesinatos-y-abusos-de-poder-por-motivos-racistas
Status: published

###### Foto: Movimiento Cimarrón 

<iframe src="http://www.ivoox.com/player_ek_4365320_2_1.html?data=lZijl5iWdI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRjoa3lIqvldvJssbnjMrbjafTq9Dohqigh6aVb9Tpx9fS0JDFt8bnytPO1tTXb9qfwsfi1dTXb8XZjNXcxsrWcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan de Dios Quintero, Movimiento Nacional Cimarrón] 

El **Movimiento Nacional Cimarrón** realizó este jueves 17 de abril un plantón en la Plaza de Bolívar, para denunciar el **asesinato selectivo de 6 jóvenes afrocolombianos** en el sur de Bogotá, así como abusos de poder y negligencia por parte de la policía y la Fiscalía.

Según expone Juan de Dios Quintero, presidente del Movimiento Cimarrón, en las últimas semanas jóvenes de los barrios **Caracolí, Los Robles, El Oasis, La Isla y Rincón del Lago en la localidad de Ciudad Bolívar en Bogotá** y del municipio de Soacha en cercanías a la capital, han sido víctimas de amenazas por parte de paramilitares, que los insultan y amenazan de muerte. Ya son 6 los jóvenes asesinados en lo que va corrido del 2015, y un menor de 15 años baleado.

A esto se suman los abusos por parte de la policía, que según denuncia el Movimiento, "ha estado agrediendo físicamente, causando lesiones, a comunidades en Usme y otras zonas de la ciudad", y añaden que "no vemos por parte de la policía una intervención para acabar con esa corrupción y abusos al interior de la institución".

"Son jóvenes estudiantes, artistas, que no están vinculados con actividades delincuenciales ni han tenido problemas en sus comunidades", asegura Juan de Dios.

Estas familias afrocolombianas provenientes de Cali, Buenaventura y el Chocó, que residen en Bogotá como resultado de desplazamientos forzados, se han visto obligados a enviar a sus hijos hacia otros departamentos del país, e incluso a otras zonas de Bogotá, sufriendo procesos de revictimización.

Es por esto que la comunidad reclama de las autoridades atención eficaz e intervención frente a estas situaciones. "Parece que los muertos afrocolombiano no le importaran a las autoridades. No hay retratos hablados, no hay ofrecimiento de recompensas, ni ruedas de prensa para informar a la comunidad. No queremos impunidad", enfatiza Mosquera.

El Movimiento Nacional Cimarrón exigen a la Alcaldía de Bogotá establecer una mesa de crisis que atienda la problemática.

#### Comunicado del Movimiento Cimarrón 

\[caption id="attachment\_7418" align="aligncenter" width="472"\][![Movimiento Cimarron](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/IMG_20150415_170457.jpg){.wp-image-7418 width="472" height="689"}](https://archivo.contagioradio.com/jovenes-en-bogota-sufren-asesinatos-y-abusos-de-poder-por-motivos-racistas/img_20150415_170457/) Movimiento Cimarron\[/caption\]
