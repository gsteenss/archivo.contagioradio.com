Title: Crepes & Waffles, El Corral y Cine Colombia promocionan las corridas de toros
Date: 2015-01-29 19:52
Author: CtgAdm
Category: Política, Voces de la Tierra
Tags: Animales, Cine Colombia, corridas de toros, Crepes &amp; Waffles, El Corral, Maltrato animal
Slug: crepes-waffles-el-corral-y-cine-colombia-promocionan-las-corridas-de-toros
Status: published

##### Foto: [blogs.perfil.co]

###### 20 Ene 2015

El pasado sábado, 24 de enero, logos de El Corral y Crepes & Waffles se vieron en la corrida de toros organizada por un grupo de empresarios que** quieren mantener vivas las actividades taurinas.  **El evento se realizó en la Plaza de Toros Marruecos, en Puente Piedra (Cundinamarca), que pertenece al rejoneador Luis Miguel Londoño.

En la apertura de la corrida, Luis Guillermo Echeverri, empresario taurino y rejoneador, le confirmó a la revista Dinero, cuáles son las empresas que apoyan la “fiesta brava”.

“El sector privado se sigue vinculando para patrocinar las corridas. Por ejemplo – señala Echeverri - dentro de los patrocinadores del evento en la Plaza de Marruecos están la Industria Licorera de Caldas y los restaurantes Hamburguesas **El Corral, Crepes & Waffles, Juan José me importa un chorizo, y La Fragata**”.

Además, Contagio Radio conoció una denuncia del colectivo Juventudes Animalistas Colombia que hace parte de la Plataforma ALTO (Animales Libres de Tortura), donde se evidencia que otras empresas como **Cine Colombia apoyan esta actividad, promocionando los eventos taurinos.**

[![unnamed](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/unnamed-300x180.jpg){.aligncenter .wp-image-3925 .size-medium width="300" height="180"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/unnamed.jpg)

##### Foto: Juventudes Animalistas Colombia 

Las corridas de toros, corralejas y demás actividades tradicionales en el país, son tema de debate en las últimas semanas, debido a los casos de maltrato animal que se vienen presentando durante estos eventos, es por eso que las organizaciones animalistas emprendieron una campaña a través de las redes sociales para que las personas que están en contra de las corridas de toros, **no sigan consumiendo en estas empresas.**
