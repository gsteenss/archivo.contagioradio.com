Title: Unidad de Restitución de Tierras mete un mico a la ley 1448
Date: 2017-05-23 17:22
Category: Nacional, Paz
Tags: Restitución de tierras, víctimas del conflicto armado
Slug: unidad-de-restitucion-de-tierras-mete-un-mico-a-la-ley-1448
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/tierras-curvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 May 2017] 

En una rueda de prensa, víctimas y organizaciones defensoras de derechos humanos denunciaron las modificaciones que se estaría pensando hacer a la ley 1448, en cuanto a la Restitución de Tierras, sin que esto haya sido pactado en los acuerdos de paz de La Habana. **La propuesta la estaría realizando La Unidad de Restitución de Tierras y constaría de 4 puntos**.

En primera instancia propone limitar la temporalidad de solicitud de restitución a un año cuando no hay límite de tiempo, además expone que las personas que hayan adquirido sus predios y que provengan de actividades ilícitas **no entraran en el proceso de restitución**, dejando de lado a los campesinos víctimas del despojo pero que están vinculados a cultivos de uso ilícito.

En la propuesta de la Unidad de Restitución de Tierras, también se desdibujan los límites de los predios restituidos ya que se permite la imposición de servidumbres mineras, que permite el **acceso a la tierra para llevar a cabo actividades de exploración y extracción minera**. Le puede interesar: ["Proyecto de ley del gobierno acabaría con el Banco de Tierras para la Paz"](https://archivo.contagioradio.com/proyecto-de-ley-de-gobierno-profundizaria-modelo-de-acaparamiento-de-tierras/)

Otro de los conflictos que crea la propuesta es que limita la reparación a las víctimas y a la superación de la categoría de víctima con la sentencia de restitución, **dejando de lado las demás formas de reparación integral**. Le puede interesar: ["Víctimas exigen a Congreso mantener la esencia de los Acuerdos de Paz"](https://archivo.contagioradio.com/victimas-exigen-a-congreso-mantener-esencia-de-los-acuerdos-de-paz/)

Finalmente, la Unidad de Tierras le recomienda a los Jueces y magistrados de restitución que no apliquen el principio de **"buena fé exenta de culpa"** que se tiene en cuenta con las personas que hayan comprado o adquirido un predio sin saber que este fue objeto de despojo o del que se desplazaron personas.

Otro de los puntos es que se invita a los jueces y magistrados de restitución a **retrasar por un tiempo amplio la entrega de predios restituidos** en procesos en los que el predio estaba ocupado por otra persona después del hecho que generó víctimas porque probablemente a la persona que tiene ocupado el predio con la restitución le pueden vulnerar sus derechos.

Las víctimas señalaron que es necesario que se comprenda que la política pública de atención y reparación integral a las víctimas **“es una sola” por lo tanto, los procesos de reparación colectiva, retornos al territorio y reubicación deben estar relacionados** con la restitución de tierras, de igual modo afirmaron que es importante que se continúe con los proyectos productivos para que las familias se puedan sostener económicamente.

Además le exigieron al Congreso y al Gobierno Nacional que **rechace cualquier iniciativa que tenga esta finalidad** y expresaron que la que se está presentando por parte de la Unidad de Restitución es regresiva con los derechos de las víctimas en el país. Le puede interesar: ["Organizaciones Sociales proponen reforma a la Ley de víctimas y Restitución de Tierras"](https://archivo.contagioradio.com/organizaciones-sociales-proponen-reforma-a-ley-de-victimas-y-restitucion-de-tierras/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
