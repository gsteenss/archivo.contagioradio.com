Title: Desalojos del ESMAD dejan cuatro indígenas heridos en el Norte del Cauca
Date: 2019-02-14 13:00
Author: AdminContagio
Category: Comunidad, DDHH
Tags: Abuso de fuerza ESMAD, Amenazas a indígenas, indígenas cauca, Norte de Cauca
Slug: desalojos-del-esmad-dejan-cuatro-indigenas-heridos-en-el-norte-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/foto-la-Emperatriz-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CRIC 

###### 14 Feb 2019 

A lo largo de la semana se han presentado dos episodios contras las comunidades indígenas del Norte del Cauca, el primero en  **Lopez Adentro, Caloto** en la vereda El Carmelo donde sus habitantes fueron desalojados en el desarrollo de una celebración ritual por el **Escuadrón Móvil Antidisturbios (ESMAD)** y en el que cuatro personas resultaron heridas;  el segundo de estos incidentes obedece a la circulación de panfletos amenazantes contra guardias y  autoridades locales del **Resguardo de Corinto.**

### **Represión de la fuerza pública**

**Edwin Capaz, coordinador de Derechos Humanos de la Asociación de Cabildos Indígenas del Norte del Cauca** relata que mientras las comunidades ejercían lo que es conocido como el Proceso de Liberación de la Madre Tierra, las fuerzas del ESMAD utilizaron gases lacrimógenos y fueron disparadas armas de fuego, según el reporte entregado por las comunidades indígenas.

De este suceso resultaron cuatro integrantes de la comunidad heridos, tres fueron atendidos en puntos locales y uno fue remitido al Hospital de Corinto,  sin embargo Capaz señala que la Policía intentó impedir el ingreso del vehículo de labor médica, "rechazamos la utilización de forma desmedida por parte del ESMAD, no deja de ser una agresión a los derechos humanos" indica el coordinador.

### **Amenazas contra guardias indígenas del Cauca**

A esta situación se suman las amenazas que **han circulado a través de panfletos amenazantes de la disidencia de las Farc: Dagoberto Ramos, quienes declaran objetivo militar a seis guardias indígenas**,  así como a una de las autoridades locales del Resguardo de Corinto, los cuales cumplen cargos de coordinación al interior del resguardo. [(Le puede interesar: Recuperar su territorio le está costando la vida a indígenas en Cauca) ](https://archivo.contagioradio.com/nos-estan-asesinando-llamado-de-comunidades-indigenas-del-cauca/)

Según Capaz, tales amenazas se dan en respuesta al ejercicio legítimo de justicia realizado por las autoridades de ACIN sobre ocho integrantes del grupo armado quienes atentaron  contra un grupo de guardias el pasado 6 de febrero, "sin duda alguna aquí lo que hay es una retaliación ni siquiera contras los guardias y las autoridades, es contra todo el mecanismo de justicia del gobierno indígena, a eso es a lo que se le amenaza realmente"   agrega.

Frente a dichos sucesos, el defensor de derechos humanos expresó su rechazo y preocupación pues "de lado y lado tenemos acciones contra la comunidad" y ha explicado que las autoridades indígenas están detallando lo sucedido para hacer las denuncias correspondientes mientras continúan un ejercicio de vigilancia en el territorio.

<iframe id="audio_32557334" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32557334_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
