Title: Alertando sobre la extracción de cobre
Date: 2019-07-22 10:24
Author: Censat
Category: CENSAT, Opinion
Tags: Atico Mining, Cobre, extracción de cobre, impactos socioambientales, industria energética
Slug: alertando-sobre-la-extraccion-de-cobre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Alertando-sobre-la-extracción-de-cobre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: https://camiper.com 

**¿Qué tan justo es el cambio de la matriz energética que nos proponen?**

[El reciente impulso de **extracción de cobre** y otros minerales se encuentra coordinado con el esfuerzo por transformar la matriz energética mundial, para pasar del motor de combustión (que usa petroleo y carbón) a un motor eléctrico (que emplea metales). La crisis ambiental causada por el modelo de desarrollo imperante ha llevado a que se desarrollen distintas propuestas para cambiar la matriz energética mundial. Las propuestas desde la institucionalidad y las empresas se limitan al cambio de materias y tecnologías; no obstante otros argumentamos que es necesario transformar nuestros modos de vida y nuestra relación con el entorno para lograr un cambio estructural y real a la crisis global.]

> [La matriz energética necesaria para garantizar las transformaciones e innovación tecnológica ha llevado a que se fomente la extracción de cobre para la transición.]

[Este metal se usa por su alta conductividad térmica y eléctrica, ya que al ser resistente a la corrosión y no ser un metal magnético, industrias como la energética, de servicios y datos le emplean en su producción de artículos tecnológicos e industriales. Pero esta propuesta de transición no va al fondo de la crisis global, pues tiene prácticas de extracción igualmente agresivas y a escalas inmensas en aras de su exportación, evidenciando el poder corporativo que cada vez tiene más respaldo en los estados, a costa de los derechos y autonomía de las comunidades.]

[El cobre es usado para construir paneles solares, cableados eléctricos, tuberías, etc. Estas tecnologías se plantean limpias, porque sustituye los fósiles, fuente de emisiones de CO2 y CO pero no mencionan lo que implica la extracción de los metales y los requerimientos energéticos que son indispensables para su elaboración. En el caso de la extracción primaria de cobre esta implica graves impactos socioambientales, pues sacarlo a gran escala requiere dos formas básicas de minería, a cielo abierto y subterránea. En ambos casos, los proyectos que se han ejecutado en países como Chile, Perú y China, los mayores extractores a nivel mundial, dan cuenta de las afectaciones a los territorios y comunidades que viven a sus alrededores.]

[En Chile, el país latinoamericano que más extrae cobre, ha implicado profundos impactos en su territorio tales como: acaparamiento de las aguas para la minería y su consecuente contaminación, desplazamiento de comunidades locales y la concentración de la economía en la extracción de metales, dejando de lado otras actividades productivas históricas de las regiones; incluso ha llegado a plantear la existencia de “zonas de sacrificio” en los lugares donde se concentran comunidades vulneradas por la contaminación de extracción de minerales como el carbón y metales como el cobre. Otra alerta es la cantidad de energía que demanda sacarlo y transformarlo. Sergio Hernández, vicepresidente Ejecutivo de Cochilco, señala, en relación al consumo de energía que, para producir 5,6 millones de toneladas de cobre fino en 2016, se demandó “]*[168.572 TJ (Tera Julio), lo que representa un alza de 1,4% en relación a 2015, y un 12% del total de energía consumida en el país]*[.” Es decir, se busca hacer eficiente la producción y transmisión de energía con extracciones que emplean grandes cantidades de la misma, entonces la cura resulta peor que la enfermedad.]

> [En 2012, el gobierno colombiano declaró el cobre como un mineral estratégico, y en los últimos años  fomenta su extracción.]

[De la misma manera, las empresas mineras afirman que es necesario extraer el cobre para seguir usando tecnología de punta. Hoy, en Colombia, la extracción de cobre se hace en el departamento del Chocó con el proyecto El Roble, de la multinacional Atico Mining Corporation, de capital canadiense. Esta mina ha generado graves impactos socioambientales en la región, puesto que en 2017 realizó vertimientos de sustancias contaminantes en el río Atrato, lo que implicó el cierre de la operación durante un tiempo.]

[Así mismo, se han concretado esfuerzos para vislumbrar otros lugares de extracción, por lo cual desde hace 15 años se otorgaron títulos mineros en los departamentos de Córdoba, Chocó, Tolima y Antioquia. En este último, **AngloGold Ashanti** plantea el proyecto minero Quebradona en el municipio de Jericó, que fue declarado Proyecto de Interés Nacional Estratégico, negando que la zona es de vocación campesina y turística, por lo cual gran parte de la comunidad rechaza la minería.]

[No proponemos una transición que implique abandonar la tecnología de la que nos beneficiamos como herramienta que facilita la conexión en el mundo, hablamos de replantear el modo de producción-consumo y distribución. Nos plantea retos como sociedad pero también como individuos, pues es vital dejar de ser clientes y volver a ser personas que podemos acceder a la energía. Lo que nos obliga a repensar la energía no como un bien acaparable y que beneficie a unos pocos, sino como un derecho; que conlleva procesos de reutilización y reciclaje. En el caso del cobre se ha demostrado que es un metal 100% reciclable, y en dado caso se extraiga lo estrictamente necesario para el bienestar general de la humanidad y el planeta.]

[Proponemos una transición energética justa; es decir, debe garantizar el respeto a los derechos constitucionales al territorio, al trabajo digno, a los derechos humanos y de la naturaleza. Esto implica un cambio que conlleve transformaciones culturales, sociales y una relación distinta con la naturaleza. No podemos permitir que proyectos a gran escala como Quebradona, además de los 11 proyectos de cobre que están en exploración en Colombia sigan vendiéndose como la solución a un modelo que obvia los impactos que genera y prioriza sus ganancias. Así mismo, es indispensable continuar vigilando y demandando que proyectos como El Roble asuman las afectaciones que generan. Para esto será fundamental cuestionar y vigilar el poder de las corporaciones al tiempo que se escuchan las soluciones y demandas de comunidades y organizaciones.]

Leer más[opiniones de Censat Agua Viva ](https://archivo.contagioradio.com/censat-agua-viva/)
