Title: Asesinan a Hugo Albeiro George líder social y afectado por Hidroituango
Date: 2018-05-03 11:58
Category: DDHH, Nacional
Tags: Antioquia, asesinato de líderes sociales, EPM, Hidroituango, lideres sociales
Slug: asesinan-hugo-albeiro-george-lider-social-afectado-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/Hugo-Albeiro-George-Pérez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [03 May 2018] 

El Movimiento Ríos Vivos denunció que el 2 de mayo a las 11:00 am, fue asesinado en Puerto Valdivia, Antioquia el líder social Hugo Albeiro George Pérez. En el hecho también fue asesinado su sobrino Domar Edigio Zapata de 23 años. El líder deja a una familia de 12 hijos y se suma a la lista de **46 líderes y defensores de derechos humanos asesinados** en 2018 según la organización Somos Defensores.

De acuerdo con el comunicado del Movimiento Ríos Vivos, el líder social hacia parte de la Asociación de **Víctimas y Afectados por Megaproyectos**, del municipio de El Aro, que estaba articulada a Ríos Vivos. Isabel Cristina Zuleta, integrante del Movimiento, manifestó que el homicidio se presentó cuando dos hombres en una motocicleta le propinaron dos disparos cuando el líder se encontraba en una cafetería.

**Familia del líder social ha sido afectado por la construcción de Hidroituango**

Rios Vivos aseguró que la familia de George “ha habitado de manera ancestral en la finca Manzanares y son tenedores de buena fe de esta propiedad”. De acuerdo con la organización, esta familia ha sido una de las más de **500 núcleos familiares que han sido afectadas** por la construcción de Hidroituango en donde, por ejemplo, "no se les ha reconocido la posesión de la finca" que cuenta con cultivos afectados por la construcción de la vía que conduce de Puerto Valdivia a la presa de la hidroeléctrica.

Según Zuleta, el líder social venía trabajando por la construcción de una **asociación específica del corregimiento de El Aro**, “porque había muchas dificultades para asociarse con las víctimas de megaproyectos de Ituango, teniendo en cuenta la distancia entre los municipios de Ituango y Puerto Valdivia”. (Le puede interesar:["15 familias afectadas dejó la inundación producto del taponamiento de un túnel para Hidroituango"](https://archivo.contagioradio.com/15-familias-afectadas-dejo-la-inundacion-producto-del-taponamiento-de-un-tunel-para-hidroituango/))

Sin embargo, George trabajó por expresar las inconformidades “sobre la manera como estaban haciendo **las negociaciones sobre las tierras** y estuvo en desacuerdo con los precios y la vía que se construyó entre Puerto Valdivia y la zona de presa”. Informó que tras el asesinato, la Policía no protegió la escena del crimen y esta fue alterada.

### **El asesinato ocurrió en el contexto de la movilización social** 

Desde hace varios días, los afectados por Hidroituango han venido preparando una movilización social teniendo en cuenta “la angustia que vive la población aguas abajo del muro de la represa por una **posible avalancha** producto del represamiento que se da por la obstrucción del túnel de desviación de las aguas del Río Cauca”.

Por esto, las comunidades le han venido exigiendo a Empresas Públicas de Medellín que haga los **procesos necesarios para la reubicación** de cientos de familias que habitan en las riberas del río, que tiene una disminución en su caudal y ha mantenido en zozobra y peligro a las personas. (Le puede interesar:["Memoria y resistencia en el Cañón del Río Cauca"](https://archivo.contagioradio.com/memoria-y-resistencia-en-el-canon-del-rio-cauca/))

El Movimiento pidió que se investigue el asesinato del líder social y se brinden garantías para su familia y los demás integrantes de Ríos Vivos que han venido siendo víctimas de **estigmatización por oponerse al proyecto hidroeléctrico**. Pidieron además que se atienda a las familias que viven en la parte alta y baja del río Cauca pues temen que ocurra una tragedia producto de los desprendimientos de la tierra que ha ocasionado el mal manejo del material vegetal talado que ha obstruido el sistema de llenado de la presa.

<iframe id="audio_25771781" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25771781_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
