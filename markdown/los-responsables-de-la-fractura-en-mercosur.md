Title: Los responsables de la fractura en MERCOSUR
Date: 2016-08-06 06:00
Category: Cesar, Opinion
Tags: América Latina y del caribe, Mercosur, politica latinoamericana
Slug: los-responsables-de-la-fractura-en-mercosur
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/mercosur.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: brasil247 

#### **[Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)** 

###### 6 Ago 2016 

[La intensa lucha política que se advierte hoy en América Latina y el Caribe tiene su más reciente expresión en Mercosur. Como se conoce, éste organismo fue fundado en 1991 por Brasil, Argentina, Paraguay y Uruguay; posteriormente se vincularon Venezuela y Bolivia. Parte de sus principios y valores tienen que ver con el ejercicio de la democracia y el impulso al desarrollo económico en un contexto de integración productiva. Varios acuerdos comerciales y políticos  con países y regiones se han concretado hasta hoy.]

[Pero lo político va de la mano con lo económico y en numerosas ocasiones lo rige. Visiones del mundo, percepciones filosóficas, escuelas económicas, profesiones de fe religiosas y otros aspectos distancian a quienes dirigen a los Estados. A veces ellas son evidencias de complejas confrontaciones ideológico-políticas que no tienen punto de solución o de reconciliación; es el caso de lo que está sucediendo hoy en Mercosur.]

[Hay una presidencia rotativa por orden alfabético; ante el cumplimiento del periodo, Uruguay hace pocos días informó que entregaba el cargo. Le corresponde a Venezuela asumir la dirección, pero con maniobras diplomáticas  Paraguay, Brasil y Argentina la han detenido. Y tiene su explicación. Cuando la derecha paraguaya adelantó en 2012 el golpe de Estado contra el sacerdote-presidente Fernando Lugo, Venezuela movió sus fichas y contribuyó a separar a Paraguay de Mercosur (lo que además le facilitó el ingreso a ese organismo); ahora se desquita. En Brasil, la derecha destituyó hace un par de meses mediante un golpe de Estado (“juicio político”) a la presidenta Dilma Rousseff; siendo enemiga tanto del gobernante Partido de los Trabajadores en su país como de su homólogo venezolano, el Partido Socialista Unificado de Venezuela, esta corriente política ve ahora la oportunidad para saldar cuentas con el presidente Nicolás Maduro. En Argentina, el reciente proceso electoral  presidencial llevó a elegir al derechista empresario Mauricio Macri, quien ahora reemplaza a Cristina Fernández de Kirchner cuyo modelo político-económico apareció, sin razón, como formando parte del “progresismo” continental.  ]

[En breve, la derecha continental comienza a pasar cuenta de cobro a quienes gestionaron el Estado en nombre de la justicia social (Brasil, Paraguay y Venezuela). La derecha, esa misma corriente que introdujo el neoliberalismo y a las transnacionales que nos atracan para llevarse nuestros recursos naturales patentizando la flora para beneficio de las farmacéuticas, y quiebran el subsuelo con el fracking; la derecha, esa misma corriente que odia y teme el movimiento de la historia de los de abajo, y que gobierna con los métodos del terror de Estado. Esa misma corriente es la que hoy juzga sobre lo “políticamente correcto” y se opone al derecho que le asiste a Venezuela para ocupar la presidencia pro-témpore de Mercosur.]

[Así lo decidió ilegalmente la “triple alianza” - Argentina, Brasil y Paraguay - en la reunión del 4 de agosto. Complotaron contra Venezuela para impedirle asumir la dirección del grupo. Burda y antidemocrática maniobra consistente en ejercer ella, la triple alianza, la presidencia temporal. La responsabilidad por la fractura del Mercosur no recae, entonces, en la soberana Venezuela; que no nos vengan con cuentos.  ]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
