Title: Tribunal confirma sentencia contra 16 empresarios y paramilitares por despojo de tierras
Date: 2016-12-06 12:39
Category: DDHH, Nacional
Tags: Curvarado, Empresarios, Jiguamiandó, Palma Africana, paramilitares, paz, territorio
Slug: tribunal-confirma-sentencia-contra-16-empresarios-y-paramilitares-por-despojo-de-tierras
Status: published

###### [Foto: Contagio Radio] 

###### [6 Dic. 2016] 

El Tribunal Superior de Medellín confirmó **sentencia condenatoria de primera instancia contra 16 empresarios, paramilitares y comisionistas de tierras, por su responsabilidad en el desplazamiento forzado y los daños ambientales causados en Curvaradó y Jiguamiandó, por la implementación de megaproyectos productivos, principalmente de palma aceitera y ganadería extensiva.**

La sentencia judicial da cuenta además de la responsabilidad de la Fuerza Pública, por su actuar mancomunado con los grupos paramilitares, en el desplazamiento forzado de las comunidades ancestrales.

Manuel Garzón abogado de la Comisión Intereclesial de Justicia y Paz manifestó que **“lo novedoso en este caso es la confirmación de la sentencia condenatoria que había sido expedida en el año 2014.** Es muy importante este hecho porque de esta manera se da más fuerza a las tesis que han mantenido las comunidades y las organizaciones que las acompañamos”.

**De esta manera el Tribunal reitera que lo acontecido en Curvaradó y Jiguamiandó se dio en un contexto de violaciones sistemáticas a los derechos humanos** y que los proyectos empresariales desarrollados en esta zona eran proyectos del grupo paramilitar “en esa medida es una sentencia que sigue marcando historia en tanto es la primera vez que empresarios son condenados por desplazamientos forzados, daños ambientales y concierto para delinquir” agregó Garzón.

Así mismo, el abogado asegura que **este tipo de sentencias se traducen en una de las manera en las que se puede contribuir a reconstruir el tejido social de las comunidades** “el aporte que puede hacer a la reconstrucción de la memoria histórica y del tejido social destruido en esa región y en muchas otras regiones del país por el accionar conjunto entre empresarios y paramilitares”.

De igual modo, el abogado aseguró que durante el proceso las víctimas sufrieron un trato deshumanizado “(…) en algunos casos pretendían mostrar a los testigos como personas mentirosas o que tenían intereses ocultos por apropiarse de las tierras o que tenían vínculos con la insurgencia, por fortuna nada de esto fue tenido en cuenta”.

**A lo largo del proceso las víctimas, testigos y declarantes demostraron la ocurrencia de homicidios, desalojos forzados y masacres en la región, cometidos por grupos paramilitares y la Fuerza Pública.** Se refirieron también al desplazamiento forzado de la mayor parte de los habitantes de territorios colectivos, para su apropiación violenta y control.

Por último, Manuel Garzón exhortó al Gobierno Nacional para que tome las medidas suficientes y necesarias tendientes a reparar los daños ambientales y socioculturales ocasionados; y a la Fiscalía para que investigue, a miembros de la Fuerza Pública, funcionarios y demás particulares que hubiesen estado involucrados en la estructura criminal.

<iframe id="audio_14665973" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14665973_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
