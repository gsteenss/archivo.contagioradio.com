Title: ¡Por ahí no es!
Date: 2020-06-08 00:21
Author: CtgAdm
Category: Columnistas invitados, Opinion
Tags: Comité de reconciliación ONG, JEP
Slug: militares-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/militares-jep-falsos-positivos.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/militares-JEP.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

**Por**: Mayor ® César Maldonado

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Patricia Linares, presidenta de la Jurisdicción Especial para la Paz (JEP), envío la semana pasada una solicitud al presidente Iván Duque, que seguramente prenderá las alarmas en el partido de Gobierno. A finales del mes pasado, la senadora Paloma Valencia, anticipó que el Centro Democrático radicará ante el Congreso, en la legislatura que inicia después del 20 de julio, un proyecto para modificar la justicia transicional que se acordó en el proceso de paz con las Farc.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué quieren los magistrados de la JEP?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Que el presidente Duque, solicite al secretario general de ONU prorrogar el mandato de la Misión de Verificación de las Naciones Unidas, que terminará en septiembre próximo. Pero, también quieren colgarle a la ONU la tarea de vigilar el cumplimiento de las sanciones impuestas por la Jurisdicción Especial para la Paz. Lo cual no está contenido en el mandato de la misión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las sanciones están enmarcadas en los TOAR (trabajos, obras, y actividades con enfoque reparador), y están dirigidas a actores del conflicto que reconocieron tempranamente verdad y responsabilidad. En otras palabras, son trabajos en la infraestructura nacional, desminado, sustitución de cultivos, y tecnologías en regiones afectadas por el conflicto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El afán de la magistratura se debe a que La JEP se alista a imponer las primeras sanciones, especialmente de los macrocasos 001 (abierto el 6 de julio de 2018) retención ilegal de personas por parte de las FARC-EP, y el 003 (abierto el 17 de julio de 2018) sobre muertes ilegítimamente presentadas como bajas en combate por agentes del Estado. [Lea también: La JEP le cierra la puerta a Salvatore Mancuso](https://archivo.contagioradio.com/la-jep-le-cierra-la-puerta-a-salvatore-mancuso/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Modificar la JEP, en qué beneficia a los militares?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esta es la tercera vez que el Uribismo insiste en reformar la JEP. Aunque no se conozca el contenido del nuevo intento, todo indica que el esfuerzo del proyecto está dirigido a desmejorar los beneficios obtenidos por las FARC en el marco de la negociación. Fuera de una pálida alusión respecto a la presunción de inocencia, los miembros de la fuerza pública en la JEP, no son la prioridad del proyecto del Centro Democrático. Pero tampoco del Gobierno. Están presentes en toda la retórica patriotera, pero nada más.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Quedó claro que en la Habana no se negoció el tema de los militares, todo el anexo al acuerdo final, se diseñó en Bogotá, ¿en dónde estaban sus “mejores aliados” cuando se cocinaba semejante desplante a los soldados? Ese era el momento para incidir a su favor, no ahora cuando la olla ya hirvió. Usar a los militares como pretexto para impulsar luchas vindicativas, le resta credibilidad a un esfuerzo, por demás, infructuoso.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Lanzar a las calles a más de dos mil hombres que caminaron 20 años por el sendero de la guerra y 10 más tras los barrotes, sin someterlos a un esquema de resocialización, es sumamente irresponsable y desleal.
>
> <cite>Mayor (R) Cesar Maldonado</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Si se quiere pasar de las palabras a los hechos, los “mejores aliados” deben pedir al gobierno del presidente Iván Duque, que vire su mirada hacia los militares comparecientes en la JEP, hoy abandonados a su suerte, incluyéndolos en un prometedor programa de reincorporación similar al que gozan los excombatientes de las FARC, que les permita un mejor desarrollo social, laboral, educativo, y de vivienda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lanzar a las calles a más de dos mil hombres que caminaron 20 años por el sendero de la guerra y 10 más tras los barrotes, sin someterlos a un esquema de resocialización, es sumamente irresponsable y desleal. Son hombres que, si bien se equivocaron, fueron mayores sus aciertos durante su permanencia en filas. Hoy merodean las calles, sin dolientes, sin trabajo por causa de los antecedentes penales, buscando ser reconocidos por su abnegada entrega, ahí sí, a la patria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hasta luego,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**[(\*) Presidente Fundación Comité de Reconciliación ONG   ](https://www.facebook.com/ComiteDeReconciliacion/)**

<!-- /wp:paragraph -->
