Title: La salida más fácil es la cadena perpetua y no es la correcta
Date: 2020-06-29 13:26
Author: AdminContagio
Category: Actualidad, Columnistas, Nacional
Slug: la-salida-mas-facil-es-la-cadena-perpetua-y-no-es-la-correcta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/images.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/1592532227_203787_1592532695_noticia_normal_recorte1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: Colombia.as {#foto-de-colombia.as .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Por: Elkin Sarria

<!-- /wp:heading -->

<!-- wp:paragraph -->

Anna Göldin fue, según cuentan las historias, la última mujer condenada por la inquisición en 1782 con 48 años de edad, luego de ser acusada por su patrón de envenenar a una de sus hijas. Anna fue castigada públicamente, torturada, vejada hasta que tuvo que aceptar los cargos para morir de una vez y evitar, de una vez, el suplicio del escarnio público y privado - en la sala de tortura- al que fue expuesta por semanas. Fue decapitada en plaza pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La sociedad de su momento, las víctimas comunes de la brujería, tuvieron por fin el contentillo de verla morir, aunque sabían que con eso no solucionarían el problema de la llamada “brujería”.  Sencillamente escogieron la salida más fácil, la muerte, aunque también se hubiesen podido inclinar por la tortura perpetua y eso también estaría bien, el pueblo pedía sangre. **[Göldin fue absuelta en 2007, 225 años más tarde.](https://www.lavanguardia.com/cultura/20180712/45837290956/anna-goldi-ultima-bruja-europa.html)**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### En febrero de 2019 las personas de CAVIDA en Cacarica, recibieron a quienes años atrás consideraron sus victimarios y a quienes temían hasta el tuétano.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los recibieron y de frente los reconocieron como humanos, el miedo quedó atrás y ahora van por la verdad y la restauración del daño. Otra salida es pedir que se les condene a cadena perpetua por patear la cabeza de Marino López, o que se les queme en la hoguera por el fusilamiento de varios de sus familiares y amigos, o que se les torture igual como lo hicieron los paramilitares con ellos antes y después del desplazamiento forzado en 1997.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero no, estas gentes escogieron la salida menos fácil, dejaron de considerarse víctimas petrificadas a ellas y victimarios diabólicos a ellos, un proceso no sencillo y riguroso, de reflexiones y aprendizajes, así como de caídas y levantes – un proceso de veinte años-. Ahora ellas han asumido que si se siguen sintiendo víctimas no van a superar ese dolor y van a encontrar la muerte – figurativamente -; por el contrario, si encuentran la forma de tramitar ese dolor, analizar sus causas y buscar salidas van a encontrar la vida – literalmente-. Fue como decirles no somos iguales de malos a ustedes, pero ustedes tampoco son los malos, aunque hayan cometido lo malo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Colombia, las atrocidades de la guerra han sido muchas, duelen como nada, merecen el rechazo total, pero no por ello la salida debe ser la más fácil, inmediatista y digna del sistema capitalista. La cadena perpetua no va a resolver los problemas que generan las conductas atroces, la muerte en la hoguera tampoco, valga decirlo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Después de tanta muerte necesitamos redignificar la vida, la humanidad, rescatarla de la profundidad en la que se pretende esconder su brillante presencia, salvar su valor.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Y es tarea de todas y todos, además todas y todos podemos hacerlo. La motosierra, la tortura, la desaparición forzada, la burla a las víctimas, la impunidad, los malos gobiernos son razones suficientes sí, pero esas mismas vías no nos aportan salidas correctas. No es la ley del talión la que nos hará mejores.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6,"textColor":"vivid-cyan-blue"} -->

###### [Le puede interesar: OtraMirada: Cadena perpetua en Colombia.](https://archivo.contagioradio.com/otramirada-cadena-perpetua-en-colombia/) {#le-puede-interesar-otramirada-cadena-perpetua-en-colombia. .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los y las congresistas que votaron si a la cadena perpetua y el gobierno que la celebró demostraron ser parte de esa turba irracional que asesinó a Goldí y a cientos de personas acusadas de brujería. Además, demostraron vivir y pensar como en el siglo XVIII, casaron la pelea contra la evolución de la constitución y contra la Corte Constitucional a quienes no dudarán en juzgar y empujar a la hoguera o a la decapitación cuando diga que esa aberración de la cadena perpetua es inexequible.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Está en manos de la ciudadanía, la verdadera dueña de la Constitución y la ley, demostrar que hemos progresado, que no estamos en el siglo XVIII y que no somos iguales, demostrar que estamos dispuestos y dispuestas a pasar la página de la guerra fratricida y a reconocernos como responsables del cambio, de la verdad y de la reconciliación. No podemos perder esta oportunidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **Es el momento de la altura ética, la humanidad y la dignidad.**

<!-- /wp:heading -->
