Title: 16 personas asesinadas en 2 meses en Soacha
Date: 2016-06-27 13:08
Category: DDHH, Nacional
Tags: asesinatos en Soacha, derechos humanos en Soacha, Red Juvenil de Suacha
Slug: 16-asesinatos-selectivos-entre-mayo-y-junio-alarman-a-habitantes-de-soacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Cazucá-Soacha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Las 2 Orillas ] 

###### [27 jun 2016]

En menos de una semana la Defensoría del Pueblo ha emitido dos alertas tempranas al Ministerio del Interior por los 16 asesinatos selectivos registrados durante el último mes en la Comuna 4 del municipio de Soacha que, de acuerdo con información recopilada en terreno, estarían **estrechamente vinculados con el accionar de los grupos armados que resultaron tras la desmovilización** paramilitar, que usan pandillas locales para sus actividades de microtráfico.

El pasado viernes, fueron asesinados los defensores de derechos humanos Luis Fernando Ortega y Norberto Martínez Macana en el barrio la Isla. Los **cuerpos fueron hallados en una vía pública, desnudos y con signos de tortura**. En los otros 14 homicidios reportados por la Defensoría, las víctimas han sido jóvenes de entre 24 y 29 años, oriundos de municipios como Chigorodó, Antioquia y Ciénaga, Magdalena, todos ellos muertos por impactos de arma de fuego.

En las alertas emitidas por la Defensoría se llama la atención por la reciente **circulación de panfletos, llamadas y mensajes de texto en los que se amenazan a líderes, pobladores y representantes de organizaciones de víctimas**. A este tipo de violencia se suma a la continua práctica de extorsión a propietarios de establecimientos comerciales a quienes les cobran montos de \$50 mil y \$60 mil semanales. El organismo también alerta por los hechos violentos que se relacionan con venganzas por el control del microtráfico.

Prueba de la magnitud de esta problemática son las más recientes cifras del Instituto Nacional de Medicina Legal, según las cuales entre enero y mayo de este año ha habido [[58 homicidios en Soacha](https://archivo.contagioradio.com/asesinan-a-klaus-zapata-integrante-de-la-juventud-comunista/)], 26 de ellos únicamente en la Comuna 4 de Cazucá. De otro lado, los registros oficiales reportan que durante los últimos 5 años han llegado al municipio 10.640 desplazados por la violencia y, según autoridades administrativas, **el 53,8% de la población está por debajo de la línea de pobreza y el 20,4% por debajo de la línea de indigencia**.

Mientras el Ministerio del Interior se pronuncia sobre las medidas que se adoptarán para atender esta problemática que preocupa a las comunidades, Cristian Quintero asegura que la 'Red Juvenil de Suacha' prepara para este sábado un foro en el que líderes, autoridades locales y organizaciones reflexionarán sobre la situación de derechos humanos en este municipio y las propuestas que en clave de paz permitan su transformación. **El evento tendrá lugar en el Teatro Sua desde el medio día hasta las 6 de la tarde**. Para participar puede inscribirse en [[este enlace](https://docs.google.com/forms/d/1N6P6NBU-tjsffg5ijDtMy9LPMyiRCap04YM_mJ0dyBc/viewform)].

<iframe src="http://co.ivoox.com/es/player_ej_12043786_2_1.html?data=kpedlpibfJehhpywj5aUaZS1k5uah5yncZOhhpywj5WRaZi3jpWah5yncaTmytjhy8bSb7LpytPhx9fTb46fs8rRja_ZusbiytGYxsqPl9bVxM3Oj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
