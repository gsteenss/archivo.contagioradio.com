Title: Hoy es el día "D+1": Gobierno y Farc
Date: 2016-12-02 13:19
Category: Nacional, Paz
Tags: Dia D, FARC, Gobierno Nacional, paz
Slug: hoy-es-el-dia-d1-gobierno-y-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/farc_gobierno_en_cuba_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [2 Dic. 2016] 

A través de un comunicado firmado conjuntamente por el Gobierno Nacional y la guerrilla de las Farc, se dio a conocer que **la entrada en vigor del Día “D”, luego de la refrendación del Acuerdo de Paz ya se inició. Según la misiva, en consecuencia “el día “D” es el día de hoy – 1 de Diciembre de 2016 -, según los términos del acuerdo”.**

En la comunicación también aseguran que se realizará la instalación de la Comisión de Seguimiento, Impulso y Verificación de la implementación del Acuerdo Final y el Consejo Nacional de Reincorporación. Le puede interesar: [Corte deberá decidir el acto legislativo para la paz](https://archivo.contagioradio.com/corte-debera-decidir-el-acto-legislativo-para-la-paz/)

**Esta Comisión tendrá a cargo revisar el estado de avance de cumplimiento de los diversos compromisos que se adquirieron entre las partes y que han tenido algunos retrasos debido resultado del plebiscito que conllevo a reabrir las conversaciones.**

Por lo anterior, aseguran las partes, existirán algunos compromisos que se habían acordado en el cronograma y que deberán ser reprogramados para asegurar su cumplimiento. Le puede interesar: [Comisión Interamericana de Derechos será veedora del Acuerdo de Paz](https://archivo.contagioradio.com/comision-interamericana-veedora-del-acuerdo-de-paz/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
