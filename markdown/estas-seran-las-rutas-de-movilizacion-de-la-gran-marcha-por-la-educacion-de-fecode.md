Title: Estas serán las rutas de movilización de la gran marcha por la educación de FECODE
Date: 2017-05-29 16:53
Category: Movilización, Nacional
Tags: fecode, Movilización Docente
Slug: estas-seran-las-rutas-de-movilizacion-de-la-gran-marcha-por-la-educacion-de-fecode
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/parotrabajadores-e1494955113483.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 May 2017] 

Tras la decisión de suspender los diálogos con el Ministerio de Educación, FECODE re activo las conversaciones el día de hoy y anunció que la gran toma y movilización en las 5 ciudades del país continúa y que se espera la participación de millones de maestros y maestras que llegarán a **Bogotá, Barranquilla, Cali, Medellín y Bucaramanga para exigir el cumplimiento del pliego de peticiones.**

En Bogotá los puntos de encuentro serán 3, el Colegio Manuela Beltrán, ubicado en la Avenida Caracas con calle 57, el SENA de la carrera 30 con Primera de Mayo y el Colegio Nicolas Esguerra, ubicado en la calle 9C \#68-52. **La hora de encuentro es a las 9 de la mañana y las movilizaciones llegarán todas a la Plaza de Bolívar**.

En Barranquilla, la jornada de movilización iniciará con una eucaristía en la Catedral de Barranquilla, a las 8 de la mañana, que será oficiada por el Monseñor Jairo Jaramillo. Sobre las 9:30 de la mañana se espera que las y los docentes se reúnan en dos puntos de encuentro: **Murillo con carrera 21 y el barrio Cordialidad, aún no se ha definido el punto de llegada.**

En Bucaramanga el punto de salida será la **Puerta del Sol y la Universidad Industrial de Santander**, desde las 9:30 de la mañana y culminará en la Plaza Galán. Le puede interesar:["Docentes se preparan para movilización nacional el 31 de mayo"](https://archivo.contagioradio.com/31-de-mayo-dia-de-movilizacion-nacional-de-docentes/)

En Cali los docentes saldrán de la **Institución Educativa Eustaquio Palacios a las 8:30** de la mañana, aún no se ha establecido el lugar de llegada de la movilización. Le puede interesar:["Hay propuestas pero el Gobierno no tiene voluntad para negociar: FECODE"](https://archivo.contagioradio.com/maestros-tiene-propuestas-pero-el-gobierno-no-tiene-voluntad-para-negociar/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
