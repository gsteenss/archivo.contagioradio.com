Title: Las recomendaciones de la MOE para veeduría de la segunda vuelta presidencial
Date: 2018-06-12 14:28
Category: Nacional, Política
Tags: Elecciones presidenciales, Fraude electoral, MOE, segunda vuelta electoral
Slug: las-recomendaciones-de-la-moe-para-veeduria-de-la-segunda-vuelta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/moe-elecciones-guajira.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOE] 

###### [12 Jun 2018] 

Con la segunda vuelta presidencial a la vuelta de la esquina, la **Misión de Observación Electoral** recordó la importancia de la veeduría ciudadana y afirmó que trabajarán durante la jornada electoral atendiendo las denuncias de fraude electoral como lo hicieron durante las votaciones de la primera vuelta.

De acuerdo con Fabián Hernández, integrante de la MOE, para avanzar en la transparencia del conteo de votos y evitar que suceda una situación parecida a la que ocurrió con los formularios E-14, donde los ciudadanos denunciaron fraude electoral, se necesita que “la Registraduría **publique los formularios de Claveros** que son los que cuentan los votos”. Esto evita que haya una alta percepción de fraude.

### **Estas son las recomendaciones de la MOE para la segunda vuelta** 

La MOE va a solicitar que se refuerce el tema de la biometría en la medida en que “en la primera vuelta **hubo varios reportes sobre suplantación**”. Para esto, es necesario que la Registraduría cuente con un número mayor de puestos biométricos en el país, pues en la primera vuelta sólo la Costa Caribe en 15 municipios contó con esta tecnología.

Adicional a esto, la MOE trabajará en la segunda vuelta atendiendo las denuncias ciudadanas sobre delitos e **irregularidades electorales**. Hernández indicó que en primera vuelta fueron numerosas las denuncias por compra de votos por lo que desde la entidad continúan avanzando con las investigaciones. (Le interesa:["Ciudadanos deben ser garantes electorales en próximos comicios")](https://archivo.contagioradio.com/ciudadanos-deben-ser-garantes-electorales-en-proximos-comicios/)

### **Ciudadanos e instituciones deben fortalecer mecanismos que garantizan la transparencia** 

Para propender por la transparencia en el desarrollo de las elecciones, la MOE indicó que se implementará un esquema similar al de la primera vuelta donde trabajarán alrededor de **2.800 observadores nacionales y 150 internacionales** tanto en Colombia como en los demás países donde ya iniciaron el proceso electoral.

También implementarán esquemas de vigilancia en los escrutinios y “se hará una revisión de muestra como se hizo en la primera vuelta”. Hernández recordó que el valor de los **testigos electorales** debe ser reconocido por las campañas presidenciales teniendo en cuenta que ellos están facultados para impugnar, recontar y generar reclamaciones que permiten “hacer trazabilidad a los votos”.

Finalmente, la MOE afirmó que las medidas de transparencia que se adopten desde la Registraduría y el Consejo Nacional Electoral **“abonan confianza”** para las elecciones. Enfatizó en que con la coyuntura del país ha salido a flote la debilidad del sistema electoral colombiano por lo que se necesita una reforma urgente.

<iframe id="audio_26497503" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26497503_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
