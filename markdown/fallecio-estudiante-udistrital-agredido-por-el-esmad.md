Title: Falleció estudiante UDistrital agredido por el ESMAD
Date: 2016-06-03 09:31
Category: DDHH, Nacional
Tags: Brutalidad policial Colombia, ESMAD, Miguel Ángel Barbosa, Universidad Distrital en paro
Slug: fallecio-estudiante-udistrital-agredido-por-el-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/miguel-angel-barbosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Embudo ] 

###### [3 Junio 2016]

De acuerdo con los familiares, sobre la 1:50 de la mañana de este viernes falleció Miguel Ángel Barbosa, estudiante de la Universidad Distrital que se encontraba en estado de coma inducido tras ser **agredido con una granada aturdidora que habrían lanzado miembros del ESMAD**, durante la arremetida a la [[toma cultural de la Sede Tecnológica](https://archivo.contagioradio.com/en-estado-de-coma-estudiante-udistrital-agredido-por-esmad/)]del pasado 21 de abril. Hasta el momento no ha habido un pronunciamiento oficial por parte del personal médico del Hospital del Tunal, en el que estaba hospitalizado.

Cabe resaltar que esta movilización fue llevada a cabo por los estudiantes con el fin de denunciar la crisis estructural que enfrenta la institución y la desatención por parte de las directivas, quienes de acuerdo con el estudiante Bryan Silva, estarían involucradas en la [[pérdida del material fílmico](https://archivo.contagioradio.com/habrian-borrado-videos-que-pobrarian-agresion-de-esmad-contra-estudiante-de-udistrital/)] que probraría que Miguel Ángel fue agredido por el ESMAD.

Durante este mes y medio en el que el estudiante estuvo en estado de coma inducido, sus [[compañeros se movilizaron](https://archivo.contagioradio.com/universidad-distrital-se-moviliza-para-exigir-mayor-financiacion/)] para exigir que se esclarecieran los hechos ocurridos en la Sede Tecnológica y se sancionaran a los responsables, mientras que el Consejo Superior de la Universidad emitió un comunicado en el que **exhortaron a los estudiantes para que no se manifestaran.**

Vea también: [[Se agudiza la crisis en las universidades públicas colombianas](https://archivo.contagioradio.com/se-agudiza-la-crisis-en-las-universidades-publicas-colombianas/)]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
