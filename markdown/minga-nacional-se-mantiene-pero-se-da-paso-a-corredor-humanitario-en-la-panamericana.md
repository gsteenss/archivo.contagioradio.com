Title: Minga Nacional se mantiene y se abre corredor humanitario en la Panamericana
Date: 2016-06-09 13:36
Category: Paro Nacional
Tags: Gobierno Nacional, Minga Nacional, Paro Nacional
Slug: minga-nacional-se-mantiene-pero-se-da-paso-a-corredor-humanitario-en-la-panamericana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/CkhzHoJWsAAfvvU-e1465496563449.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cumbre Agraria 

###### [9 Jun 2016] 

Las comunidades negras, campesinas, afrocolombianas e indígenas de todo el país continúan en Minga pese a que exista un pre-acuerdo entre el gobierno y el Concejo Regional Indígena del Cauca. **Cerca de 18 mil indígenas caucanos siguen en movilización, pues lo que se busca es que se el gobierno atienda las reclamaciones a nivel nacional.**

“La gente ha aguantado tanto, que un día más sin suministros se puede aguantar, la gente está cansada de aguantar las políticas regresivas del gobierno, están mamados de que su territorio sea destruido”, expresa José Santos, vocero de la Cumbre Agraria y líder del Proceso de Comunidades Negras, quien añade que **las organizaciones que hacen parte de la Cumbre “están más unidas que nunca”.**

Desde la Minga Nacional se denuncia desinformación en torno a la continuidad del paro, mientras que **gobierno se prepara una arremetida de la fuerza pública contra los manifestantes** en todo el territorio nacional.

Así sucedió ayer en la noche cuando nuevamente el ESMAD  llegó al campamento de la Minga en Bruselas, Huila; o como sucedió en Quinamayó, Cauca donde ayer llegaron cerca de 8 camiones con personal militar para desplazarse la vía Panamericana, pese a que la Cumbre anunció este jueves que **se abrirá un corredor humanitario sobre la Vía Panamericana.**

El vocero del Proceso de Comunidades Negras, denuncia que **el gobierno mantiene actitudes racistas y discriminatorias contra la población negra que participa del paro,** pues no existe voluntad política de parte del Ministro del Interior, Juan Fernando Cristo, para entablar conversaciones en territorio de comunidades negras, como Quinamayó y en cambio responde con represión.

Santos asegura que la Cumbre Agraria no tiene comunicación directa con el gobierno, lo que señala como una “incapacidad de interlocución del Ministro Cristo con las comunidades”, por lo que menciona que “el presidente Juan Manuel Santos debería hacer el relevo del Ministro”.

“La gente está dispuesta a seguir, la gente se cansa y aburre cuando el gobierno no escucha, cuando **tenemos un gobierno intransigente como lo ha demostrado el ministro Cristo**”, concluye el líder de comunidades negras.

<iframe src="http://co.ivoox.com/es/player_ej_11842891_2_1.html?data=kpallpecfZKhhpywj5aXaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncavj1IqwlYqlfYzHwtPh0diPcYzE09TQx9jTb6Tjztrby8nFqMbnjLPSydfFt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
