Title: Actividades criminales del DAS a punto de prescribir
Date: 2017-01-30 16:07
Category: DDHH, Nacional
Tags: Alvaro Uribe, chuzadas, das, interceptaciones ilegales
Slug: a-juicio-exdectectives-del-das-35421
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/chuzadas-DAS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 Ene. 2017] 

Doce exdetectives del extinto Departamento Administrativo de Seguridad (DAS) fueron llamados **a juicio por abuso de autoridad, violación ilícita de comunicaciones y utilización ilícita de equipos de transmisión y recepción **durante el gobierno de Álvaro Uribe. En contraposición, altos mandos del DAS, como José Miguel Narváez en la actualidad han sido beneficiados con medidas como el cese de la acción penal en su contra.

Según la Fiscalía, las pruebas que existen en contra de estas personas permiten considerar que **los acusados tienen una responsabilidad por haber obtenido, entregado, analizado y ejecutado operaciones de inteligencia y contrainteligencia** en contra de defensores de derechos humanos, abogados y periodistas.

De los 12 acusados que han sido llamados a juicio, solo se encuentra privado de la libertad **Ronal Harbey Rivera por los cargos de tortura psicológica en el caso de la periodista Claudia Julieta Duque,** proceso en el que han sido vinculadas otras dos personas que hacen parte de los 12 llamados a juicio. Le puede interesar: [Ex-funcionarios del DAS a indagatoria por "tortura psicológica"](https://archivo.contagioradio.com/ex-funcionarios-del-das-a-indagatoria-por-tortura-psicologica/)

Pese a los avances en materia de investigación que se han tenido en este caso, en los que actualmente existen 16 funcionarios condenados, organizaciones sociales y periodistas afectados por las acciones ilegales del DAS han asegurado que **la justicia ha “minimizado los hechos”** dado que ha considerado como delitos menores hechos sistemáticos de persecución.

Adicionalmente, la Fiscalía aseguró que estas 12 personas llamadas a juicio hacen parte de la última etapa de investigaciones por el caso del DAS y las “chuzadas ilegales” realizadas por el G-3. Le puede interesar: '[ChuzaDAS' confirmaría "cacería criminal" durante gobierno Uribe](https://archivo.contagioradio.com/chuzadas-confirmaria-caceria-criminal-durante-gobierno-uribe/)

Hasta el momento, además de José Miguel Narváez, otras personas como Carlos Alberto Herrera Romero, Oscar Barrero López, Carlos Fabián Sandoval Sabogal, Ibet Senobia Gutiérrez Guardo, Neider de Jesús Ricardo Hoyos, Juan Carlos Gutiérrez Galván y Juan Carlos Benavides Suárez, todos exfuncionarios del DAS, han sido beneficiados con la preclusión de investigaciones en su contra.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
