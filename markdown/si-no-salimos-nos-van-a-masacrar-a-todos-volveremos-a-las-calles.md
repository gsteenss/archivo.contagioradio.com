Title: "Si no salimos nos van a masacrar a todos... volveremos a las calles"
Date: 2020-08-25 22:26
Author: AdminContagio
Category: Actualidad, Movilización
Tags: Comité del Paro Nacional., CUT, Defendamos la Paz, Movimiento estudiantil, Regreso de las masacres
Slug: si-no-salimos-nos-van-a-masacrar-a-todos-volveremos-a-las-calles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Calles.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Movilización en las calles / Contagio Radio - Carlos Zea

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de una crisis humanitaria que ha dejado como resultado 182 personas asesinadas en 45 masacres cometidas en lo corrido del 2020, más de 349 líderes sociales desde la firma del Acuerdo de Paz y una difícil situación en términos de inconstitucionalidad, educación, economía y salud diferentes gremios, organizaciones y la ciudadanía han manifestado la necesidad de volcarse a las calles y exigir al Gobierno soluciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la Central Unitaria de Trabajadores (CUT), organización con 600 sindicatos afiliados se ha podido tener una visión general del país. Su vicepresidente, Jorge Cortés, plantea que la situación del país se agudizó a raíz de la Covid-19 y mientras el desempleo en junio se ubicó en 19,8 % con 4,2 millones de colombianos que no cuentan con un empleo y cerca de un 65% de la población que trabaja en la informalidad, con la pandemia, las pequeñas y medianas empresas "se precipitaron al abismo".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los 6 puntos claves que exigen desde el Comité del Paro Nacional

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Tras el 21 de noviembre, fecha en la que se dio un gran estallido social que permitió la conformación de un Comité Nacional de Paro, se presentó al Gobierno un pliego de peticiones que se articulaba en 13 ejes divididos en 1.134 puntos, sin embargo ante la crisis de la pandemia estos se redujeron en 6 puntos esenciales parte de un pliego de emergencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Cortés se trata de dar importancia al sector de la salud donde existen profesionales que no han recibido un salario en 17 meses y que afrontar la pandemia sin equipos de bioseguridad; asegurar la Renta básica para 9 millones de familias y proteger la producción nacional de la pequeña y mediana industria del campo y la ciudad, pues advierte que no de defenderla, se perderían cerca del 80% de empleos de este sector.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los otros tres puntos del pliego hacen referencia al sector de la educación, fomentando la matrícula cero y el subsidio para estudiantes de universidades públicas y privadas, la protección de la vida y garantías en los territorios en contraste con las masacres y asesinatos contra defensores de DD.HH. y comunidades y finalmente la derogatoria de más de 100 decretos que el presidente Duque ha instaurado en pandemia, muchos de los que cuales "han favorecido a la banca y a las grandes multinacionales".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cortés señala que han expuesto al Gobierno diversas alternativas de obtener recursos, entre ellas el recaudo de bancos internacionales, un prestamo del Banco de la República por un valor cercano a los 100 billones de pesos, deja de pagar durante un año al Fondo Internacional Monetario la deuda externa que llegaría este año a un 59,2 % del PIB nacional, y de la que podrían surgir 52 billones de pesos para afrontar la crisis; pese a las propuestas, ninguna ha sido aceptada.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Edgar Mojica, secretario general de la CUT** agrega que ante un "Gobierno que "mantiene su arrogancia de no aceptar la posibilidad de discutir alternativas" y la creación de un pliego de emergencia, se está evaluando la posibilidad de retornar a las calles durante el mes de octubre.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque Jorge Cortés admite que las personas están atemorizadas de salir a las calle señala que desde la CUT, van a apoyar las movilizaciones que sean necesarias y con todas las medidas de bioseguridad y **"una vez pase el pico y estemos en condiciones se debe seguir, necesitamos salir a rechazar la política del Gobierno, aquí debe haber un estallido una vez finalice este problema".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Si no existe un movimiento social que defienda los derechos y los bienes del social, esto va a seguir igual y en peores condiciones, la normalidad no puede darse como venía siendo antes", advierte el líder sindical. [(Le recomendamos leer: Marcha por la Dignidad aportó para reactivar la movilización social](https://archivo.contagioradio.com/marcha-por-la-dignidad-aporto-para-reactivar-la-movilizacion-social/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Defendamos la Paz apela a la comunidad internacional

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Patricia Lara, periodista e integrante de [Defendamos la Paz](https://twitter.com/DefendamosPaz)** expresó su preocupación sobre el incremento en la violencia, razón por la cual desde la plataforma se ha planteado realizar una conferencia internacional con el fin de proteger la implementación del Acuerdo de paz para el 26 de septiembre cuando se cumplirán cuatro años de la firma del Acuerdo de La Habana.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para dicho evento se extendió la invitación a la Misión de Verificación de la ONU, al partido FARC, a los países garantes y a demás integrantes de la comunidad internacional interesados en la paz del país, a su vez se invitó al presidente Duque, al Consejero Presidencial para la Estabilización y la Consolidación y demás integrantes de su gabinete, con la esperanza de que acudan al llamado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Es una oportunidad para que el Gobierno piense en el bien del país, es impresionante esa ceguera, esa sordera e incapacidad de dialogar, es un Gobierno que está ausente de los territorios",** afirma la periodista que atribuye la violencia a la no implementación del Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Lara señala la urgencia de acudir a la movilización con inteligencia y acudiendo a otras formas que promuevan las medidas de bioseguridad pero que puedan hacerle llegar al Gobierno esta voz de alarma de desesperación así como en Samaniego se la hicieron llegar los pobladores". [(Lea también: Con gritos de paz y justicia rechazan presencia de Iván Duque en Samaniego)](https://archivo.contagioradio.com/rechazan-presencia-de-ivan-duque-en-samaniego/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Mantener viva la llama del movimiento estudiantil

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Sarah Isabel Klinger, líder estudiantil de la Universidad del Cauca** e integrante de la Unión Nacional de Estudiantes de Educación Superior (UNEES) resalta que el escenario de la pandemia ha impuesto algunos retos para los estudiantes en particular el de mantener su vigencia y la de sus reivindicaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Es necesario ubicar los elementos que atañen al escenario de la movilización social, como la reconfiguración del paramilitarismo fortalecido al interior de la región y que actúan en el marco de la pandemia y el control del territorio", un elemento al que la líder estudiantil suma la participación de los partidos políticos tradicionales que "utilizan su maquinaria para estigmatizar a líderes y movimientos sociales".  [(Lea también: Las razones que explican el regreso de las masacres a Colombia)](https://archivo.contagioradio.com/las-razones-que-explican-el-regreso-de-las-masacres-a-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Hay que volver a las calles con un sentido de responsabilidad social y si hablamos de defender la vida también se trata de proteger nuestra salud pero eso nos lleva a plantearnos cómo puede ser diversificado", afirma la líder estudiantil quien señala que deben luchar por un escenario de cohesión, no necesariamente conglomerados, **"la idea es tratar de confluir en contextos que son tan diferentes pero que permitan la movilización sin que choquen y disminuyan las movilizaciones".**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Desde los territorios también se prepara el regreso a las calles

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte desde Buenos Aires, Cauca, **Armando Caracas, coordinador nacional de la Guardia Cimarrona e integrante del Proceso de Comunidades Negras** también señala que desde los territorios ya se preparan estrategias para volver a las calles, mientras se toman decisiones se está analizando la situación de departamentos como Antioquia, Cauca y Nariño, donde se ha intensificado la violencia en las últimas semanas,"

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Nosotros no nos vamos a quedar quietos, si bien el Gobierno está gobernando a través de puros decretos, nosotros nos estamos preparando para salir a las calles nuevamente"
>
> <cite>**Armando Caracas, coordinador nacional de la Guardia Cimarrona**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

"El cómo es la estrategia que nos estamos planteando para que la gente se pueda movilizar en medio de la pandemia", afirma el coordinador quien advierte que es necesario hacer presencia en lugares como la Vía Panamericana reivindicando los derechos de las comunidades, **"si no salimos nos van a masacrar a todos y antes de que eso suceda, muy pronto, en el mes de octubre estaríamos en las calles".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque Caracas relata que el salir a **exigir sus derechos ha significado una agudización de las amenazas contra los liderazgos y el retorno de prácticas paramilitares para causar terror en las comunidades**, seguirán denunciando para dar a conocer la situación que se vive no solo en el Cauca sino en otras regiones, "no nos podemos quedar quietos si el covid nos está matando, el Estado y su inacción nos está matando mucho más".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La pérdida de los jóvenes en contextos de violencia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Con relación al incremento de la violencia contra la población joven del país, Sarah Klinger sostiene que "las juventudes han sido un músculo de las movilizaciones en los anteriores años" algo que parte de que se trata una generación que ya se ha movilizado por la educación y el Acuerdo de Paz, lo que convierte estos ataques en un hecho sistemático.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Creo que los jóvenes hemos demostrado que nos apartamos de ese sesgo que se nos han impuesto de que son solo los adultos quienes pueden participar y eso también delata una ola de violencia contra la juventud", por lo que señala, **la tarea que tienen los y las jóvenes es impulsar su participación en todos los escenarios, electorales, académicos y de movilizacion.  ** [(Le puede interesar: El terror de las masacres no puede frenar la movilización social en Colombia)](https://archivo.contagioradio.com/el-terror-de-las-masacres-no-puede-frenar-la-movilizacion-social-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Los jóvenes son el futuro del país y no por nada usted está ve que precisamente son a quienes vienen asesinando , son la fuerza política que quieren apagar", afirma el líder cimarrón quien agrega a esta problema la muertes de los sabedores y ancestros como fruto de la pandemia, "están acabando con los jóvenes pero con los adultos mayores y buscan detener esa cadena de formación política".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"No es fácil perder a un joven que está liderando un proceso y de los jovenes que caminan a su lado. Más que la pérdida de un ser humano es lo que implica para el desarrollo de una nación". agrega.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
