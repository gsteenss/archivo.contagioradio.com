Title: Comunidad señala a la Fuerza Pública de asesinar a un menor en Caloto, Cauca
Date: 2019-06-06 15:00
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Caloto, Cauca, Fuerza Pública
Slug: comunidad-senala-a-la-fuerza-publica-de-asesinar-a-un-menor-en-caloto-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-06-at-2.13.03-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Menor-de-edad-Caloto.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-06-at-3.41.46-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

La comunidad campesina de **Caloto, Cauca,** denuncia que la Fuerza Pública asesinó a **Jefferson Trochez, menor de 16 años de edad** en la zona del Carmelo, en el marco de un procedimiento de desalojo que adelantaba en la vereda Vista Hermosa. Según la información preliminar, el **joven era campesino e integrante de la Asociación de Trabajadores Campesinos de Caloto.**

Según la Asociación de Cabildos Indígenas del Norte del Cauca (ACIN)los sucesos ocurrieron desde las 11 de la mañana en el  sector de Vista Hermosa territorio de Lopez Adentro Caloto, cuando **arribaron fuerzas combinadas del Escuadrón Móvil Anti Disturbios ESMAD y el Escuadrón Móvil de Carabineros EMCAR de la Policía Nacional y el Ejército Nacional acompañados por un grupo de hombres de civil** a una finca ocupada por indígenas desde hace más de 3 años, y procedieron a atacarlos indiscriminadamente.

![Jefferson caloto](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-06-at-3.41.46-PM-300x225.jpeg){.aligncenter .wp-image-68435 width="496" height="374"}

El reporte indica que promediando las **11:30 a.m disparos de arma fuego impactaron en el cuerpo de Jefferson de manera directa hiriendolo de gravedad** y agrega que a pesar de que Jefferson estaba lesionado en el piso"un grupo de integrantes del Escuadrón Móvil Anti Disturbios ESMAD de la Policía Nacional **lo arrastraron del lugar en el que cayó mal herido y durante un lapso de aproximadamente 30 minutos lo golpearon en repetidas ocasiones**"

La comunidad también denuncia que hasta el momento, la Fuerza Pública no permitió que la delegada municipal de derechos humanos, ni la mamá del menor se acerquen a ver el estado en que se encontraba Jefferson, por el contrario los **agentes de la policía procedieron a lanzar de forma intimidante gases lacrimógenos y perdigones**, Hasta que cerca de las 3:07 de la tarde  una comisión del Cuerpo Técnico de Investigaciones de la Fiscalía General de la Nación realizo el levantamiento del cuerpo sin vida  del joven que según algunas versiones presentaba lesiones con arma blanca y múltiples golpes.

Además del caso de Trochez, la denuncia advierte que **resulto herido el comunero indígena Alexander Quiguanas Velasco, de 19 años de edad**. [(Lea también: Asesinan a Juliana Chirimuskai, menor misak de 15 años en Cauca)](https://archivo.contagioradio.com/asesinan-a-juliana-chirimuskai-menor-misak-de-15-anos-en-cauca/)

Trochez era integrante de La Guardia Campesina de Caloto, de la Junta de Acción Comunal de la Vereda El Carmelo, de la Asociación de Trabajadores Pro-Constitución Zonas de Reserva Campesina de Caloto - ASTRAZONACAL de la Asociación Nacional de Zonas de Reserva Campesina – ANZORC, del Proceso de Unidad Popular del Suroccidente Colombiano – PUPSOC, y de la Coordinación Social y Política Marcha Patriótica Cauca.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
