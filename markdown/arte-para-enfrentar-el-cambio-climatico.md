Title: Arte para enfrentar el cambio climático
Date: 2016-10-31 16:11
Category: eventos
Tags: Ambiente, arte, cambio climatico, fondo acción, pedagogia
Slug: arte-para-enfrentar-el-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/14925734_1317023631655099_3702061394308880436_n.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Fondo Acción Colombia 

##### [31 Oct 2016] 

Hasta el 6 de noviembre está abierta la convocatoria para "**CambiARTE 2016**",  un concurso que busca premiar **propuestas pedagógicas en las que el arte sea el medio** para despertar emociones, crear conciencia y enseñar sobre el papel de todos y todas **frente al cambio climático.**

La intención de la competencia es **potencializar la capacidad que tienen las diferentes manifestaciones artísticas para crear realidades a partir de la visión de quien lo crea**, ofreciendo una nueva perspectiva sobre el mundo a partir de la generación de nuevas sensaciones que conectan a los espectadores con sus emociones. Le puede interesar: [La tercera parte de las especies se extinguirá por el cambio climático](https://archivo.contagioradio.com/la-tercera-parte-de-las-especies-animales-en-el-mundo-se-extinguiran-por-el-cambio-climatico/).

Se busca que los concursantes identifiquen claramente **cual es el público que se pretende sensibilizar** y sus características sociales, culturales y económicas, de modo que al finalizar la propuesta **puedan sentir que el cambio climático tiene que ver con sus vidas**. Le puede interesar: [Cambio climático afecta con mayor fuerza a la población vulnerable del mundo](https://archivo.contagioradio.com/cambios-climaticos-afectan-con-mayor-fuerza-a-la-poblacion-vulnerable-del-mundo/).

Los interesados en participar, deberán enviar sus propuestas en **artes plásticas, literarias, musicales y escénicas**, con piezas como esculturas, pinturas, grabados, dibujos música, danza, la poesía y la literatura, la fotografía, la historieta, el circo y la magia, la moda, el diseño  gráfico, el arte digital, la artesanía, el teatro y el cine.

Los finalistas serán seleccionados entre el **7 y el 17 de noviembre** y la premiación tendrá lugar el 21 del mismo mes, para aquellos que ocupen los tres primeros lugares, quienes recibirán **20, 10 y 5 millones de pesos** respectivamente, para desarrollar desde el mes de enero de 2017 su propuesta pedagógica.

Para conocer las bases, términos y condiciones del concurso puede ingresar a <http://www.fondoaccion.org/es/convocatorias> o escribir al correo o cambiarte@fondoaccion.org.

<iframe src="https://www.youtube.com/embed/z1xd4rpzjAU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
