Title: Cierre de la Universidad de Antioquia "es una decisión arbitraria" del rector
Date: 2015-10-29 11:12
Category: Educación, Nacional
Tags: cierre universidad de Antioquia, Derechos Humanos, ESMAD, Exámen de admisión UDEA, Movilización social, Radio derechos humans, Represión, Sonsón, UDEA, Universidad de Antioquia, Yarumal
Slug: cierre-de-la-universidad-de-antioquia-es-una-decision-arbitraria-del-rector
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/UdeA1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_9197656_2_1.html?data=mpammZuZeo6ZmKiakpyJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbX1dTfjcjNqdPmwpDO1MfNuNPV087Oz8rSuMafttPW2MrWt8rYwsmYxsqPhc%2FoytTe187FcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Isaac Butrago, estudiante  UDEA] 

###### [28 oct 2015] 

Mauricio Alviar, rector de la **Universidad de Antioquia** (UDEA), ordenó desde hoy hasta el próximo 5 de noviembre, un cese de actividades en la institución por presuntas amenazas de alteración durante la jornada de la **realización del examen de admisión.**

Frente a la **orden de suspensión, ** Isaac Buitrago, estudiante de la Facultad de Derecho de la UDEA indica que esta “decisión muestra lo **arbitraria que es la conducta del rector**”.  El universitario señala que Alviar, debió **consultar la medida con la comunidad universitaria** ya que esa orden altera el desarrollo de las actividades culturales y académicas de los alumnos.

Según explica Isaac, esta toma de decisiones del rector no se ha dado solo por el tema de los exámenes de admisión, sino también por declaraciones de las cuales tienen conocimiento los estudiantes s**obre la rentabilidad que deben tener las investigaciones ** desde la mirada del rector Alviar. Es por esto que los estudiantes buscarán en otras instituciones el apoyo que necesitan para remediar esta situación.

Los estudiantes de otras sedes de la UDEA también se encuentran preocupados debido a que las **sedes de Sonsón y Yarumal**, podrían cerrarse en los próximos días por orden de las directivas y sin el consentimiento de la comunidad estudiantil, “cortando ese acceso a la educación que tienen las sedes más remotas de nuestro departamento”, afirma el estudiante de Derecho.

Para el día de hoy los estudiantes convocan una manifestación en contra de las decisiones arbitrarias del rector, y también para “**exigir respeto a la movilización social**”, teniendo en cuenta que esta ha sido criminalizada. Por ejemplo, según cuenta Buitrago, para la jornada de hoy alrededor de la Universidad ya había escuadrón del ESMAD.
