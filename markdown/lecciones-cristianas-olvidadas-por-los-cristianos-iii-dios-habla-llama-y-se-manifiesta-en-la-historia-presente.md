Title: Lecciones cristianas olvidadas por los cristianos III: Dios habla, llama y se manifiesta en la historia presente.
Date: 2020-09-07 13:51
Author: CtgAdm
Category: A quien corresponda, Opinion
Slug: lecciones-cristianas-olvidadas-por-los-cristianos-iii-dios-habla-llama-y-se-manifiesta-en-la-historia-presente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Con la medida que midan a los otros, los medirán a ustedes** (Mateo 7,2) 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Hay cristianos que no reconocemos los pecados personales, sociales y ambientales, mientras exigimos a los demás que no pequen, o los criticamos por pecadores.  *

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Estimado **Hermano en la fe**, cristianos, cristianas, personas interesadas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hay comprensiones y convicciones de fe, que tienen el riesgo de llevar a “certezas” sobre la vida, sobre la voluntad de Dios, sobre lo bueno y justo, sobre el deber ser y el actuar de las personas, que son contrarias a la fe cristiana y sin relación con el Evangelio de Jesús el Cristo. En otros momentos he compartido que, con frecuencia, se repite la historia  dolorosa de gente creyente persiguiendo y asesinando al Hijo de Dios por defender la religión, por defender su manera de vivirla fe, por hacer respetar su comprensión de Dios que consideran la verdadera. Sin embargo, esta manera de de vivir la fe cuestionada por Jesús de Nazaret, quien experimentaba a Dios como un Padre tierno y cercano, quien les recordaba que el culto principal era el amor, el respeto y la justicia en la relación con los demás, independientemente de si son creyentes o no. Por esto la “gente religiosa” consideró a Jesús un blasfemo y lo persiguieron a muerte.    

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **¿Cómo fue posible que “gente buena”, “bien intencionada” y “fiel a Dios”, “que hacía lo mandado por la tradición religiosa”, llegara a aprobar o legitimar el asesinato del hijo de Dios? **

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una primera respuesta puede estar en reconocer que se olvidó que la religión bíblica no es una religión solo del pasado, sino que es una religión del presente; que no es una religión solo del futuro y para la otra vida, sino que es una religión para el presente y para esta vida. Hoy diríamos que, **con frecuencia, las y los cristianos olvidan que Dios se manifiesta, habla y llama en la vida, en las realidades, en los problemas y en las crisis del presente. **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En tiempo de Jesús, la gente religiosa se esforzaba con “todo el corazón y con toda el alma” en vivir los mandamientos del pasado, en pensar solo en las historias del pasado y a celebrar la ritualidad religiosa de siempre. La mayoría lo hacía honestamente, pero sin contextualizarlas. En otras palabras, sin preguntarse en qué realidad habían nacido esos mandatos, rituales e historias, qué les decía Dios en ese momento. Por eso no fueron capaces de descubrir la presencia de Dios en los hechos, en las palabras y en la vida de Jesús de Nazaret. La gente religiosa tenía el corazón tan encerrado en esa comprensión y vivencia religiosa, que no pudo descubrir la presencia y la acción de Dios en el presente, en las “nuevas realidades” que estaba viviendo el pueblo. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A través de los siglos las personas, fieles a Dios de verdad, siempre tuvieron presente esta pregunta, o preguntas parecidas: **¿Si Dios en ese tiempo y en esa situación concreta le habló al pueblo de esa manera y le pidió pensar y actuar así; en este tiempo y en esta situación concreta cómo nos habla y qué no s pide pensar y actuar?** 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En otras palabras, hoy en Colombia, en América Latina y en el mundo, los cristianos/as deberíamos preguntarnos: **¿Qué está pidiendo Dios que hagamos en esta realidad concreta y en este momento histórico? ¿Cuáles deberían ser las actitudes, los pensamientos y  las acciones de los creyentes frente a las realidades que vivimos hoy?** 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si Dios habla, llama y se manifiesta en la historia presente, como lo hizo ayer en tiempo de Jesús, los cristianos/as deberíamos preocuparnos por los mismos temas que le preocuparon a Jesús, y no solo por los problemas de las iglesias, que la gente deje de creer o crea de otra manera. Recordemos que para Jesús la salvación dependía de la actitud asumida frente a quienes tenían hambre, sed, eran emigrantes (desplazados), estaban desnudos, enfermos o en la cárcel, porque *“todo lo que hagan a uno de estos hermanos míos más pequeños a mí me lo hacen”;* o “*todo lo que dejen de hacer con uno de estos hermanos mías pequeños a mí me lo dejan de hacer”* (Mt 25,31-46). 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy, son realidades urgentes, en el mundo y en Colombia, el empobrecimiento de las mayorías, el endeudamiento, el aumento del desempleo y subempleo que se agudizan con la pandemia; el calentamiento global consecuencia del extractivismo depredador; la profundización de la brecha entre uno pocos dueños de casi todo y la mayoría que carece de casi todo, la corrupción en todos los niveles, el recorte de los derechos fundamentales aprovechado la crisis; la doble moral de sectores dirigentes y de los “referentes de la sociedad”; las mentiras en los medios de información o en sectores oficiales; los cristianos y cristianas manipulados y engañados actuando en contra de lo propuesto por el Evangelio de nuestro Señor Jesucristo. Y sobre todo, el baño de sangre de las masacres y los asesinatos que están aumentando dramáticamente en el los últimos meses. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si queremos ser cristianos/as, o creyentes honestos, deberíamos pregúntanos**: ¿Qué nos pide Dios, a qué nos llama, desde la muerte violenta de 196 líderes y lideresas sociales y 8 de sus familiares, desde el asesinato de 38 firmantes de los acuerdos de paz (que estaban cumpliendo), desde las 47 masacres y desde policías y militares que vuelven a ser noticia por su muerte violenta?** Hechos y datos de este año 2020. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y como primer paso, podríamos evitar las disculpas y evasiones*: “eso no me toca”,* “*no podemos hacer nada”,* *“yo dejo todo en manos de Dios”,* *“en algo andaban”,* “*nada bueno estarían haciendo”;* es sangre derramada y Dios sigue repitiendo: ***“La voz de la sangre de tu hermano clama a Mí desde la tierra*”** (Gen 4,10). 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y algo más concreto, podemos y debemos tener en cuenta los hechos y los nombres de las víctimas en oraciones, cultos y celebraciones; informarnos mejor tomando, al menos dos fuentes diversas, para analizar los mismos hechos; preguntarnos por las causas reales y contrastar las explicaciones oficiales y la de los opositores. Si solo vemos un lado, no tendremos una mirada crítica. ¡Una sola fuente es la mirada de una sola cara de la realidad! Debemos evitar caer en la justificación de los hechos, pero buscando entenderlos mejor y con perspectiva histórica. Usemos nuestra inteligencia. En todo caso, siempre será más cristiana la posición de quienes se oponen a la violencia, a la muerte, a las justificaciones; de quienes buscan la paz, trabajan por la justicia, y piden el respeto a los derechos humanos, a la vida de campesinos, de indígenas,  de afrocolombianos, de defensores ambientales, de líderes y lideresas sociales… y ¡que pare toda muerte!. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una buena lección desde la realidad de las masacres y buscando el compromiso con la vida, la dieron los **“Creyentes indignados por las muertes violentas en Colombia”** con la convocatoria, el pasado viernes 28 de agosto, a un *Viacrucis por la vida de los jóvenes, desde los lugares de su muerte violenta*. Ellos recordaban que **Jesús hoy es condenado a muerte en Colombia.** Y esa tarde se conectaron, virtualmente, víctimas y animadores de la fe desde 15 lugares marginales del país donde ocurrieron masacres y asesinatos de jóvenes en los últimos meses: Piamonte, Villagarzón, Leiva, Samaniego, Ricaurte, Buenos Aires, Villa Rica, Puerto Tejada, Toribio, Llano Verde, Buenaventura, Ituango, Tarazá, Campo Dos, El Tarra y Bogotá (barrio Santafé) y compartieron el dolor, la indignación, la solidaridad, la unión en la tragedia y la fe en Jesucristo.  Quienes tuvimos la oportunidad de participar quedamos cuestionados, fortalecidos y motivados a hacer algo, a no quedarnos quietos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Relacionar la fe con las realidades que ponen en riesgo la vida humana y del planeta abre la mente, el corazón y el horizonte para descubrir lo que Dios quiere que hagamos frente a estas realidades, y el tipo de relaciones que nos pide con los demás seres humanos, con la naturaleza y con el mismo Dios.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Le puede interesar: **Lecciones cristianas olvidadas por los cristianos II: la santidad económica**](https://archivo.contagioradio.com/lecciones-cristianas-olvidadas-por-los-cristianos-ii-la-santidad-economica/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dejemos que resuenen en nuestras mentes y en nuestros corazones, las oraciones, las celebraciones, las reflexiones y la pregunta por las causas. Que los lugares donde han ocurrido masacres este año nos generen y nos lleven a hacer algo para que la historia no se repita: Tarazá, Jamundí, Cáceres, Puerto Asís, Puerto Leguízamo, Medellín, Salgar, Palmarito, Palmar, Bogotá, Barranquilla, Cartagena, Piamonte, Corinto, Ituango, San Bernardo de los Farallones, El Quebradón, Totumito, Tibú, San José de Uré, San José de Cúcuta, Palmarito, Puerto Santander, Bello, California, Leiva, Llano Verde, Venecia, Puerto Angosto, Samaniego, Ricaurte, Mercaderes, Buenos Aires, Capitán Largo, Norcasia, Ibagué, Cinaruco, Tumaco, Guaduas, el Tambo, Andes... 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fraternalmente, su hermano en la fe,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

P. Alberto Franco, CSsR, J&P <francoalberto9@gmail.com>

<!-- /wp:paragraph -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:heading {"level":6} -->

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

<!-- /wp:heading -->
