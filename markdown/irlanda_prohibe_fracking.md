Title: Senado de Irlanda prohíbe el fracking
Date: 2017-06-30 16:36
Category: Ambiente, El mundo
Tags: fracking, Irlanda
Slug: irlanda_prohibe_fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/DDa3Es6XcAAtqPB-e1498858406384.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [@love\_leitrim]

###### [30 Jun 2017] 

El senado de Irlanda avaló la prohibición del Fracking, **y será el presidente, Michael Higgins, quien deberá firmar la Ley para que ese país se sume a una lista de naciones que han decidido prohibir dicha actividad** petrolera, ante los posibles impactos ambientales y sociales que esta puede traer.

[Eddie Mitchell, portavoz de Love Leitrim, asegura que el resultado es gracias a una campaña comunitaria a través de la cual se informó sobre el tema. Asimismo las organizaciones ciudadanas desarrollaron un importante lobby con representantes locales y nacionales evidenciando los argumentos para prohibir el fracking.]{lang="es"}

[**"La difícil situación de las comunidades en Canadá y Australia actuó como una advertencia para nosotros,** nos mostraron que podíamos ponernos de pie y hacer un cambio para tener éxito como sucedió en Nueva York. No podemos permitirnos perder todo", expresa Mitchell.]{lang="es"}

[Se trató de una campaña comunitaria en la que participaron agricultores, pescadores, artistas y diversos profesionales para convencer a los parlamentarios por qué era importante dar este paso. "Sabíamos que no teníamos que ser políticos. Teníamos que ganarnos los corazones y las mentes ", y agrega, "**Todos tenemos que ser capaces de unirnos para la lucha más grande que el planeta enfrenta ahora, el cambio climático.** Esperamos que nuestra exitosa campaña sea un catalizador para otras comunidades demostrando que se puede lograr".]{lang="es"}

###### Reciba toda la información de Contagio Radio en [[su correo]
