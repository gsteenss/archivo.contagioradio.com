Title: ¿La religión influye sobre su orientación sexual?
Date: 2015-05-06 19:01
Author: CtgAdm
Category: closet, LGBTI
Tags: Catolicismo, cristianismo., Derechos Humanos, Homosexualidad, LGBTI, Orientación sexual, Religión
Slug: la-religion-influye-sobre-su-orientacion-sexual
Status: published

##### [Programa 6 de Abril] 

<iframe src="http://www.ivoox.com/player_ek_4457862_2_1.html?data=lZmimZ2ado6ZmKiakpeJd6Klkoqgo5WXcYarpJKfj4qbh46kjoqkpZKUcYarpJKSpZeJhqfAwpDfx9HNq8qZpJiSpJjSb8rix9Hi28qPt9DW08qY1dqPs9PdxtPhw8jNaaSnhqeg0JDXqdnpwtGSlauRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Closet abierto abre el debate en el contexto religioso en Colombia, un país que está regido en su mayoría por el catolicismo y que se convierte en la base del rechazo a la comunidad LGBTI, la sociedad colombiana mantiene ideales religiosos que llevan a la discriminación por orientación sexual.

No se generaliza al decir que la comunidad LGBTI es rechazada en todas las religiones o en todas las iglesias, pero en su gran mayoría se busca la manera de cambiar en cierta medida su orientación sexual acercándolos a Dios, demostrando que no se acepta que personas que pertenecen a la comunidad se acerquen a la religión sin que sean juzgados.

Se enfrentan dos religiones que predominan en Colombia, el catolicismo y el cristianismo, desde dos posturas marcadas, la primera por parte del hermano Alfonso, quien se está preparando para ser sacerdote y la segunda por parte de Maria Paula Macías quien predica en una iglesia cristiana, cada uno expone su punto de vista desde la religión hacia la comunidad LGBTI.
