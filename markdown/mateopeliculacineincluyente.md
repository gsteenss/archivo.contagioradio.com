Title: Película 'Mateo' con audiodescripción se proyecta en Bogotá
Date: 2016-12-02 13:24
Author: AdminContagio
Category: 24 Cuadros, eventos
Tags: Cine, colombia, Dia de las personas con discapacidad, Mateo, Película
Slug: mateopeliculacineincluyente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/57150_2534_imagen__.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Mateo película 

##### 2 Dic 2016 

En el marco de la celebración del **Día Internacional de las Personas con Discapacidad**, el Festival Internacional de Cine por los Derechos Humanos Bogotá, la Fundación Saldarriaga Concha y el programa Cine para Todos del MinTIC, invitan a la proyección de la **película Mateo**, que cuenta **con audiodescripción**, para la inclusión tecnológica de personas con discapacidad visual y auditiva en el cine.

La película **dirigida por María Gamboa**, cuenta la historia de Mateo, un joven de 16 años, quien cobra cuotas extorsivas a comerciantes de Barrancabermeja para su tío, un jefe criminal. Su mamá desaprueba de las actividades de Mateo, pero acepta por necesidad el dinero que él trae a casa.

Para mostrar su valía, **Mateo** accede a infiltrarse en un grupo de teatro con la misión de exponer las actividades políticas de sus miembros. A medida que se empieza a fascinar con el estilo de vida del grupo, su tío le exige con vehemencia información para incriminar a los actores. Mateo debe tomar decisiones bajo una presión cada vez mayor.

A través de esta historia basada en experiencias reales, Mateo y su mamá encuentran la dignidad cuando se enfrentan a las estructuras establecidas del conflicto armado en Colombia. Le puede interesar: [Convocatoria abierta para el 4to Festival de Cine por los DDHH](https://archivo.contagioradio.com/convocatoria-festival-de-cine-ddhh/)

[![Mateo poster](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/15259275_1091807004250704_4415844407374013997_o.jpg){.wp-image-33220 .aligncenter width="434" height="562"}](https://archivo.contagioradio.com/convocatoria-festival-de-cine-ddhh/)

En el evento que tendrá lugar este **viernes 2 de diciembre a partir de las 6 de la tarde** en el **Aula Múltiple de la Universidad Jorge Tadeo Lozano** (cra.4 n.° 22-61), las personas con discapacidad que asistan de manera gratuita, recibirán tabletas y audífonos para que disfruten de la película.

<iframe src="https://www.youtube.com/embed/jjuwrzTP5gE" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
