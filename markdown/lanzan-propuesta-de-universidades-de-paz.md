Title: Lanzan propuesta de Universidades de Paz
Date: 2018-11-09 16:22
Author: AdminContagio
Category: Educación, Nacional
Tags: Restauración, Universidades de Paz
Slug: lanzan-propuesta-de-universidades-de-paz
Status: published

###### Foto: Archivo 

###### 9 Nov 2018 

El pasado 8 de noviembre la Comisión de Justicia y Paz realizó en la ciudad de Bogotá el lanzamiento de las Universidades de Paz, como una de las propuestas de las comunidades para alcanzar reparación integral y proteger la vida humana y natural es sus territorios.

Una de las conclusiones a las que han llegado diversos sectores de víctimas del conflicto en Colombia es que no hay reparación ni transformación enviando a la cárcel a los responsables, algo que se busca cambiar con las **Universidades de Paz,** las cuales  impulsarían escenarios de diálogo como un encuentro de saberes.

Tras un acuerdo entre Gobierno y Farc  y la previa revisión de más de 100 espacios  de construcción comunitaria, se eligen  **cerca de 13 espacios en distintos territorios** de los cuales, será el territorio de Cacarica, Chocó el primero de estos espacios que estarán destinados a enaltecer la memoria y el aprendizaje.

Según el coordinador nacional de la Comisión Intereclesial de Justicia y Paz, Danilo Rueda, las Universidades de Paz persiguen dos objetivos básicos, el primero, y de ahí parte su nombre, es el de **vincular el conocimiento técnico y universitario con los saberes de las comunidades** mientras el segundo es **ofrecer a la JEP un espacio donde los responsables de crímenes de lesa humanidad puedan cumplir su respectiva sanción** y reconstruir la verdad de la mano de los afectados.

El proyecto apunta a que exista un encuentro entre actores del conflicto y afectados, lo cual no solo incluye a actores armados sino también a empresarios que también tuvieron responsabilidad en los hechos ocurridos para que junto a las comunidades pueden “proyectar un nuevo modelo de país”, expresó.

Durante el lanzamiento de esta iniciativa estuvieron presentes diferentes líderes y representantes de las víctimas de Estado, entre ellos **Luz Marina Cuchumbe, lideresa  de Inzá, Cauca** quien afirmó que "el diálogo se puede construir desde las mismas regiones para charlar de este país y cómo debemos cambiar, encontrándonos víctimas y victimarios, mirándonos a los ojos y hablando de la realidad y pensar que al igual que nosotros, todos somos seres humanos".

**Diseño y arquitectura**

Sin dejar de lado el aporte a la restauración, el proyecto también se pensó como una forma de generar justicia socio ambiental que permita reconocer lo necesario de dinámicas para la formación de una generación nueva.

Los componentes de los módulos fueran reciclables, incluyendo la elaboración de sus fachada, techo y puerta, algo que fue posible gracias a **Roberto Álvarez** quien a través de la recuperación ambiental logró la creación de techos, puertas y páneles con una patente que convierte al plástico en un material completamente maleable.

Las comunidades han expresado su voluntad para que los organismos de verdad, justicia y reparación puedan realizar sus actividades regionales en dichos territorios resaltando la importancia de su implementación para avanzar en temas de reconstrucción.

Se espera que para febrero del 2019, conmemorando los 22 años de la **Operación Génesis, que generó el desplazamiento masivo de las poblaciones en el Cacarica**  se inaugure el primero de los módulos de la Universidad de Paz.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
