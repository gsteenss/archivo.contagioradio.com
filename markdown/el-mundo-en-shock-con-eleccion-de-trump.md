Title: El mundo está en shock con la elección de Donald Trump
Date: 2016-11-09 16:21
Category: El mundo, Política
Slug: el-mundo-en-shock-con-eleccion-de-trump
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Donald-Trump.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Taringa ] 

###### [09 Nov. de 2016] 

**Donald Trump es el nuevo presidente de Estados unidos, con 270 votos de los miembros del Colegio Electoral frente a los 218 de su rival del Partido Demócrata, Hillary Clinton.** Con estas cifras, una de las potencias del mundo amaneció con un nuevo presidente que ocupará la Casa Blanca desde el próximo 20 de enero.

Algunos analistas y diversos sectores han manifestado que lo sucedido en Estados Unidos puede asemejarse a lo que aconteció en meses pasados con el Brexit en el Reino Unido y que igual que en dicho país, muchos sectores en USA habían pensado que Trump no iba a ganar.

Andrés Torres Contreris integrante de Democracy Now, aseguró para Contagio Radio que pese a que Estados Unidos es conocido como una de las democracias más grandes del mundo esto es falso y agregó que “**con la elección de Trump el nazismo en el sentido moderno ha llegado a Estados Unidos.** Veremos si la resistencia de verás va a surgir para enfrentar ese sistema”. Así mismo, aseguró que estas elecciones se hicieron en contra del statuo quo.

Según el periodista alternativo de Democracy Now, Donald Trump pudo llegar a la Casa Blanca porque representa un posible cambio al sistema que ha estado dirigiendo a Estados Unidos hace varios años. Sin embargo ganó muy a pesar de que es “un declarado acosador de mujeres, que dice que los violadores mexicanos están invadiendo a Estados Unidos, que asegura que hay que hacer pruebas a musulmanes cuando ingresen al país, entre otros temas”.

**El papel de los medios de comunicación**

Analistas como Noam Chomsky han asegurado que el papel de los medios de comunicación durante la campaña ha sido dramático y que el foco de los medios estuvo de una forma masiva sobre Trump. Además indicó que luego de un análisis que realizó pudo darse cuenta que **las cadenas televisivas le dedicaban tanto tiempo a Trump porque ‘era maravilloso para sus audiencias’, lo que suponía más anuncios.**

Ante esta realidad, Andrés Torres aseveró que los medios de comunicación son parte de un sistema que falla y que termina en elecciones fraudulentas y replicando un sistema fraudulento que no reconoce el voto popular.

E instó a los medios alternativos a "incrementar su resistencia frente al sistema masivo de comunicación hegemónico que existe en Estados Unidos”.

**“Supuestamente ganó el cambio”**

Según el integrante de Democracy Now, muchos de los latinos que votaron por Trump lo hicieron porque quieren un cambio a pesar de no saber qué significa este cambio **“ganó el cambio a pesar de que puede ser peor y hay mucha gente latina que cayó en esa trampa” afirmó Torres.**

Además, agregó que los movimientos sociales deben fortalecerse y convertirse en resistencia real para “combatir el nazismo que está llegando a Estados Unidos, no lo digo de manera light porque además ha llegado con el apoyo del Senado y la Cámara Baja, que estará controlada por los republicanos”. Le puede interesar: [“En Estados Unidos ganó el miedo y el racismo” Lilia Ramírez](https://archivo.contagioradio.com/en-estados-unidos-gano-el-miedo-y-el-racismo-lilia-ramirez/)

<iframe id="audio_13686212" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13686212_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u][[[bit.ly/1ICYhVU]]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-bit.ly1nvao4u-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio-bit.ly1icyhvu}
