Title: 528 años después continúa el exterminio para razas y sectores sociales
Date: 2020-10-12 11:06
Author: AdminContagio
Category: Actualidad, DDHH
Tags: feminicidios, Líderes Sociales, pueblos indígenas
Slug: 528-anos-despues-continua-el-exterminio-para-razas-y-sectores-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Marcha-por-la-dignidad.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Dignidad-3.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: Darwin Torres

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de la conmemoración del 12 de octubre, desde Contagio Radio realizamos un balance alrededor de la población de nuestro país que es a menudo víctima de agresiones en los territorios nacionales y que en el 'Día de la Raza' tanto como en su diario vivir, enfrentan las amenazas que surgen desde los actores armados legales e ilegales y la desatención estatal.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Población joven en riesgo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En el 2018 se emitió el más reciente informe Forensis de Medicina Legal, que dio cuenta de 710 menores de edad entre cero y 17 años fueron asesinados. En promedio, dos diarios. El rango con más casos es el comprendido entre 15 y 17 años, con 545 casos. Entre 2018 y 2019, 883 menores de entre 0 y 10 años fueron asesinados en el país, según cifras del Instituto de Medicina Legal.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo la guerra se ha ensañado contra la población joven de Colombia. Ya había sido denunciado el bombardeo a un grupo de integrantes de las guerrillas en el que, a pesar del conocimiento previo de la presencia de menores de edad el Gobierno ordenó el ataque dejando como saldo 12 menores asesinados.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este año fueron asesinados 2 niños en Leiva, Nariño, mientras llevaban sus tareas, cinco en Llano Verde en Cali, 9 jóvenes universitarios fueron víctimas de masacres en Samaniego. Todos ellos y ellas en medio de la guerra oficial e ilegal que se han ensañado contra la población menor en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

También, el pasado 9 y 10 de Septiembre en la ciudad de Bogotá se presentaron por lo menos 13 asesinatos de jóvenes en medio de las protestas contra la brutalidad policial, se denunciaron cerca de 150 casos de detenciones y un amplio número de denuncias por torturas. [(Lea también: En Quibdó han sido asesinados 97 jóvenes en 2020)](https://archivo.contagioradio.com/en-quibdo-han-sido-asesinados-97-jovenes-en-2020/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Así mismo, se han denunciado ampliamente casos de abuso sexual de la policía en contra de niñas al interior de los CAI o en medio de los llamados “traslados por protección”. Otros jóvenes han sido víctimas de balas en sus rostros por las cuales han perdido sus ojos. Hoy como hace 528 años los y las jóvenes se están convirtiendo en otra de las víctimas del exterminio. Ser joven se está convirtiendo en un riesgo de muerte.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En peligro los liderazgos sociales

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El panorama de los liderazgos sociales en Colombia tampoco es alentador, a través de su informe anual el Programa Somos Defensores reveló que 2019 fue el año más violento de toda la década para los defensores de DD.HH. Pese a que se registró un leve descenso en los asesinatos de líderes sociales, a su vez aumentaron las agresiones y expresiones de violencia, **algo que se podría explicar por el mayor control de las estructuras armadas en determinados territorios.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En contraste, según el ACNUDH, se documentaron en el 2019 un total de 108 asesinatos de personas defensoras de DD.H.H, del total de homicidios, un 75% ocurrió en zonas rurales y el 86% en zonas donde la pobreza supera la media nacional. [(Le puede interesar: Asesinan al líder afrocolombiano Patrocinio Bonilla)](https://archivo.contagioradio.com/asesinan-al-lider-afrocolombiano-patrocinio-bonilla/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para 2020, casi concluyendo el año, según organizaciones como el [Instituto de estudios para el Desarrollo y la Paz (Indepaz)](http://www.indepaz.org.co/) el registro de líderes asesinados se ha elevado a 225, mientras el informe trimestral que recopila la ONU ante su Consejo de Seguridad advierte sobre el asesinato de 48 líderes sociales y 51 sucesos de este tipo que están en proceso. [(Lea también: Oswaldo Rojas, líder comunitario del Bajo Atrato fue asesinado por las autodenominadas AGC)](https://archivo.contagioradio.com/oswaldo-rojas-lider-asesinado-bajo-atrato/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En ascenso violencias contra las mujeres

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La violencia contra la mujer y en particular contra las defensoras de DD.HH. también ha ido en aumento. Según la campaña No Es Hora de Callar, en lo corrido del año, 178 mujeres han sido víctimas de feminicidio, en el 38% por sus actuales compañeros y el 22% de los casos por sus exparejas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque muchas denunciaron las agresiones físicas, psicológicas o sexuales, las medidas y respuestas que recibieron por parte de las autoridades no habrían sido suficientes. Según la misma organización, de la cifra total de casos que tenía registrada la Fiscalía para la mitad del año, de 76, solo 4 tenían condena contra el agresor y solo 13 habían tenido una audiencia de imputación de cargos, el resto permanecían en indagación sin evidenciar un mayor avance judicial.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante este nivel de impunidad, las organizaciones sociales han advertido que durante la cuarentena se dio un aumento de este tipo de violencias de género, una situación que ha tendido a empeorar pues en las últimas dos semanas, al menos una mujer ha sido asesinada en el país cada día, según No Es Hora de Callar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Si se habla en particular del rol de las defensoras de DD.HH., para organizaciones como Somos Defensores y organizaciones como la Cumbre Nacional de Mujeres y Paz, Sisma Mujer, GPAZ y la Liga Internacional de Mujeres por la Paz y la Libertad las agresiones cometidas **entre 2013 y 2019 contra lideres han llegado a 1.338 hechos violentos, siendo la amenaza la forma de violencia más utilizada contra las defensoras.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la Oficina de la Alta Comisionada de Naciones Unidas para los DD.HH. para 2019, los asesinatos contra defensoras de derechos humanos se incrementaron en cerca del 50% en comparación con el 2018 mientras, del 2013 hasta el 2019 se han registrado por Somos Defensores, 84 asesinatos de defensoras. de DD.HH. [(Lea también: Homicidios contra defensoras de DD.HH alcanzan el 91% de impunidad.)](https://archivo.contagioradio.com/casos-homicidios-contra-defensoras-de-dd-hh-se-encuentran-en-un-91-de-impunidad/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un 12 de octubre opaco para las comunidades étnicas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma el Alto Comisionado de las Naciones Unidas para los Derechos Humanos (ACNUDH) expresó su preocupación ante el alto número de asesinatos de indígenas en departamentos como Cauca donde según Medicina Legal, en 2019 registró un incremento de casi el 52% en los homicidios de indígenas comparado con 2018, registrando el asesinato de 66 miembros del pueblo indígena Nasa en el norte del Cauca, incluidas 16 autoridades indígena.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que en 2009 la Corte constitucional se pronunció a través del auto 004 advirtiendo que 34 pueblos indígenas estaban en riesgo de extinción, mientras se enfrentan a diario no solo a conflictos por sus tierras contra sectores privados legales e ilegales sino a una frecuente lucha por preservar sus saberes ancestrales que se han visto amenazados de mayor forma en tiempos de pandemia con la muerte de sus sabedores. [(Lea también: En Gobierno Duque han ocurrido 1.200 violaciones de DDHH contra el pueblo Awá en Nariño)](https://archivo.contagioradio.com/en-gobierno-duque-han-ocurrido-1-200-violaciones-de-ddhh-contra-el-pueblo-awa-en-narino/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para junio de 2020, 269 líderes indígenas habían sido asesinados en el país, de los que 167 de estos crímenes se cometieron en el actual gobierno, "en los dos años de mandato del presidente Duque constatamos con extrema preocupación cómo los grupos armados ilegales han exacerbado la barbarie en contra de las comunidades y en especial contra los pueblos indígenas", ha manifestado la Organización Nacional Indígena de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

SI hablamos de la población afro en Colombia, Según información de la ONG Temblores, al menos el 73% de los abusos policiales ocurridos en Colombia son contra personas afro. Mientras, según el DANE, en los 100 municipios con mayor presencia afro el 48% de los hogares son pobres, meintras el 59% no puede acceder a educación de calidad, el 37% no tiene acceso a acueductos y el 20% de los hogares cuenta con una persona analfabeta.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este exterminio también se ve reflejado a través de otro tipo de agresiones no necesariamente físicas como el racismo estructural que vienen denunciando las poblaciones afro del país y que se evidencia en sucesos como la subrepresentación que sufrieron en el censo de 2018 que redujo el registro de cerca de 1.3 millones de personas identificadas en este grupo étnico . [(Lea también: DANE desapareció a más de 1 millon de personas afro del Censo Poblacional)](https://archivo.contagioradio.com/dane-desaparecio-a-mas-de-1-millon-de-personas-afro-del-censo-poblacional/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

«Sabemos que es un tema histórico que no ha desaparecido y que permanece en las instituciones del Estado pues ese trato también se representa en la desatención de muchas de las instituciones», han expresado desde el Consejo Regional Indígena del Cauca con relación a estos hechos en los que se desconoce a todo un pueblo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Y qué pasa con la cultura este 12 de octubre?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Colombia cuenta con cerca de **65 lenguas indígenas, dos lenguas criollas y el romaní propio del pueblo gitano, sin embargo más de 30 están en peligro de extinción** y la permanencia o no de muchas otras y su legado podría definirse en los próximos treinta años según estudios del Ministerio de Cultura, esto debido a la poca cantidad de personas que las hablan.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Colombia Plural, hay cerca de cinco lenguas casi extintas: la Tinigua con un solo hablante, la Pisamira con cerca de 25 hablantes, la Nonuya con 3 hablantes, la Carijona con cerca de 30 hablantes pasivos, que la entienden pero no la hablan y la Totoró con 4 hablantes activos y 50 pasivos.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
