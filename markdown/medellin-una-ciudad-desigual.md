Title: Medellín una ciudad desigual
Date: 2014-12-16 15:41
Author: CtgAdm
Category: Video
Slug: medellin-una-ciudad-desigual
Status: published

\[embed\]https://www.youtube.com/watch?v=iBFXEo8QKTI\[/embed\]  
Durante el pasado Foro social urbano alternativo y popular en Medellín se desarrollo el Carnaval por la Vida Digna, hombres y mujeres salieron a las calles a demostrar porque ésta ciudad antioqueña vive en la inequidad, y como lo que han querido llamar innovador soló aplica para las clases altas de esta ciudad de Colombia
