Title: Policía Nacional estaría involucrada en masacre de los niños en Llano Verde, Cali
Date: 2020-08-15 13:49
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Cali, FFMM, masacre, niños, Policía
Slug: policia-nacional-estaria-involucrada-en-masacre-de-los-ninos-en-llano-verde-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/10000000_920284588478862_4181670271530909713_n.mp4" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/10000000_920284588478862_4181670271530909713_n-1.mp4" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/10000000_920284588478862_4181670271530909713_n-2.mp4" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Archivo Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"normal"} -->

Familiares de las víctimas de la masacre de los cinco menores en Llano Verde, Cali, asesinados el pasado martes **denuncian que se debe investigar a los integrantes de la Policía Nacional quienes conducían dos patrullas que estuvieron en el lugar del asesinato. Varios videos de testimonios de** los familiares dan cuenta de la presencia de la policía y denuncian la alta probabilidad de su participación en la masacre de Juan Manuel Montaño de 15 años, Jair Andrés Cortez de 14 años, Jean Paul Perlaza de 15 años, Leyder Cárdenas de 15 años, Álvaro José Caicedo de 14 años.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AlexLopezMaya/status/1294091023833018369?s=19","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AlexLopezMaya/status/1294091023833018369?s=19

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Ante la falta de respuesta por parte la Policía Nacional **los familiares empiezan a revelar versiones sobre el hecho en dónde involucran cuatro policías, mientras divulgan un vídeo donde se registran los cuerpos sin vida producto del brutal asesinato.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este según los familiares **se evidencia a miembros de la Policía Nacional que se marcharon del lugar antes de ser encontrados los cuerpos sin vida de los menores**, acción que para los allegados hace aún más sospechosa la presencia de los uniformados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En otro vídeo se ve como un hombre, familiar de las víctimas, pide que se investiguen las dos patrullas registradas en el lugar de los hechos, afirmando que estos ***"sabían lo que estaba pasando, salieron y pasaron por encima de ellos, tienen que investigar eso a fondo".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que además **deben realizarse también investigaciones a los guardias de seguridad que encontraron a las victimas de esta masacre** quienes según el hombre, llevaban colgados en su pantalones machetes. ([5 niños afro fueron masacrados en el barrio Llano Verde en Cali](https://archivo.contagioradio.com/5-ninos-afro-fueron-masacrados-en-el-barrio-llano-verde-en-cali/)).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Hubo intento de desaparición de los menores asesinados en Llano Verde

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una de las versiones de los familiares que se pronunciaron durante el sepelio de los niños aseguró que en la caseta de vigilancia del cañaduzal donde fueron encontrados los menores hay presencia permanente de guardias de seguridad que al momento de llegar tendrían armas blancas (machetes) untados de sangre al lado de policías de dos patrullas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

además denunciaron que los cuerpos de los niños fueron quemados para desaparecerlos al otro día cuando llegara la maquinaria que se dispone para recoger los desechos de la caña. Cómo si fuera poco, la familiar también denuncia que al llegar al lugar parecía que les hubiesen tendido una emboscada para asesinarlos.

<!-- /wp:paragraph -->

<!-- wp:video {"id":88341,"align":"center"} -->

<figure class="wp-block-video aligncenter">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/10000000_920284588478862_4181670271530909713_n-1.mp4">
</video>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

La otra pregunta que le queda a los familiares es la razón por la que la policía no actuó si habían cinco cadáveres, personas armadas con sangre en los machetes y en la cara y al lado de ellos estaban los policías quienes no detuvieron a los vigilantes a pesar de las evidencias.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En Llano Verde ya se había registrado el asesinato de un gestor de paz en el que también estarían involucrados policías.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ejemplo de ello es el caso denunciado el reciente 11 de marzo en el **corregimiento de Navarro, zona rural de Cali, en donde fue encontrado sin vida y con un disparo en la cabeza, Cristián Adrián Angulo**, de 24 años, y de quién según su padre los principales sospechosos resultan ser miembros de la Policía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Según el hombre, su hijo había salido a reclamar su cédula en una estación de Policía**, la cuál le había sino retenida por un oficial en confusos hechos, esto debido a que la versión del uniformado acusado del crimen no coincida con la entregada por los familiares de la víctima.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este tipo de hechos de abuso, agresiones y hostigamiento por parte de miembros de la Polícia, [Ejército](https://www.justiciaypazcolombia.com/reinaldo-perdomo-hite-5/)y Esmad se han vuelto cada mas recientes en los últimos meses, casos que han sido registrados en vídeo especialmente en aquellas zonas rurales donde el olvido estatal es respaldado por el aumento de la fuerza pública. ([En Corinto tras intervención de fuerza pública se reportan dos muertos y tres heridos](https://archivo.contagioradio.com/en-corinto-tras-intervencion-de-fuerza-publica-se-reportan-dos-muertos-y-tres-heridos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El 2020 en la ciudad de Cali acumula una **cifra de asesinatos de 613 casos**, evidenciando así al igual que en otras zonas del país el aumento de la violencia en medio de la pandemia, en donde además muchos de estos casos involucran a la fuerza pública. ([Operativo de erradicación forzada en Guayabero deja 4 heridos y 20 campesinos retenidos por las FFMM](https://archivo.contagioradio.com/guayabero-erradicacion-forzada/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
