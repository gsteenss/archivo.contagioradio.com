Title: Estas son las razones del Paro Judicial en Colombia
Date: 2017-06-06 13:07
Category: Movilización, Nacional
Tags: Paro de Centrales Obrerar, Paro de Rama Judicial
Slug: las-razones-del-paro-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Asonal-Judicial-e1496772441737.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [06 Jun 2017] 

Los trabajadores de la rama Judicial entraran en paro por **48 horas para exigirle al gobierno una solución sobre el reajuste salarial del 2017**, que finalice la tercerización laboral, la ampliación de la planta laboral y la modernización de los centros de servicios. De esta forma se unen a la movilización de las centrales obreras en el país.

De acuerdo con Fernando Otalvaro, presidente de ASONAL Judicial, **el 90% de los juzgados entro en cese de actividades** y no se presentará atención en grandes ciudades como Bogotá, Bucaramanga, Medellín, Cúcuta y en Cali. Sin embargo, aclaró que se están desarrollando actividades en relación con detenidos para efecto de libertades, así no vulnerar el derecho a libertad, las medidas que tengan que ver con tutelas al derecho de vida y los exámenes en Medicina Legal.

### **La falta de herramientas para trabajar en la Rama Judicial** 

Otalvaro señaló que en Medicina Legal desde el año 2013 habían negociado un incremento de la planta en 700 cargos que debería ser progresiva y terminar en el 2018, hasta el momento solo se han creado **205 plazas, faltando 500 más, situación que aporta a congestión en la rama judicial**.

Además, Otalvaro señaló que el Postconflicto necesitará que se añada a la planta de trabajo 400 empleos porque “se realizarán desplazamientos por todo el país para la identificación de cadáveres y restos humanos”. Le puede interesar: ["Gobierno está repartiendo la pobreza pero no aumenta presupuesto: FECODE"](https://archivo.contagioradio.com/gobierno-esta-repartiendo-la-pobreza-pero-no-aumenta-presupuesto-fecode/)

Para la descongestión de la Rama Judicial, el gobierno había ofrecido una solución parcial, generando 106 empleos para todos los juzgados del país. Otalvaro manifestó que esto es insuficiente y que han exigido un presupuesto necesario **que permita que se desarrolle la justicia en la nueva modalidad que es la oralidad**, que necesitará salas de audiencias y mecanismos técnicos que no se han creado.

### **Tercerización Laboral** 

El presidente de ASONAL Judicial, expresó que actualmente las plantas de trabajo se están manejando de forma amañada “**muchos servicios se le vienen quitando a las entidades**, se viene desvinculado y acabando con ciertos trabajos, que luego se vinculan a través de cooperativas y otro tipo de organizaciones”. Le puede interesar: ["Este es el acuerdo entre Comité Cívico de Buenaventura y Gobierno"](https://archivo.contagioradio.com/acuerdan-plan-de-desarrollo-social-a-10-anos-en-buenaventura/)

De igual forma, Otalvaro afirmó que estos contratos generan un detrimento del servicio que deben prestar las empresas estatales en especial con el tema de administración de justicia provocando un **“boquete para que se vaya privatizando el servicio”.**

### **El paro podría continuar** 

Otalvaro señaló que de no tener avancen en la mesa sectorial con la delegación del gobierno Nacional, los trabajadores de la rama judicial estarán dispuestos a tomar las acciones pertinentes, “si se considera que este paro hay que prolongarlo así lo haremos y si se considera que hay que realizar un paro cívico allá estaremos” y agregó que “**el único responsable de esta situación de la Justicia es el gobierno Nacional** que ha desantendido las peticiones de los judiciales del país y de los estatales”.

<iframe id="audio_19110579" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19110579_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
