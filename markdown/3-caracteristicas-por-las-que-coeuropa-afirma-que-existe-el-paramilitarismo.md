Title: 3 características por las que COEUROPA afirma que existe el paramilitarismo
Date: 2017-06-08 16:04
Category: DDHH, Entrevistas
Tags: colombia, Paramilitarismo
Slug: 3-caracteristicas-por-las-que-coeuropa-afirma-que-existe-el-paramilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/auc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [08 Jun 2018] 

Una sentencia del Tribunal de Justicia y Paz, concluyó que los paramilitares en Colombia fueron “franquicias de matones y dinámicas distintas que jamás obedecieron al Estado Mayor Conjunto", afirmación que **vulnera a las víctimas del conflicto armado, al Sistema de Verdad, Justicia y Reparación y que atenta con el derecho a la no repetición**, de acuerdo con el vocero de la Coordinación Colombia- Europa – Estados Unidos, José Humberto Torres.

Según Torres,  esta es una decisión lamentable frente a la aplicación de una ley que se crea en el año 2005 y que lleva 12 años en su aplicación “los tribunales de justicia y paz, en los distintos lugares del país, **han ratificado la existencia del fenómeno paramilitar**” afirmó Torres. Le puede interesar:["Paramilitares obligan a comunidad a asistir a reunión en Cacarica, Chocó"](https://archivo.contagioradio.com/paramilitares-obligan-a-comunidad-a-asistir-a-reunion-en-cacarica-choco/)

De igual forma, otros sectores de la opinión pública como la Academia,  han manifestado las características del paramilitarismo: ser un aparato organizado de poder, tener una cadena de mando al interior de la organización y tener un control territorial en regiones del país. Mientras que instituciones del Estado como la **Defensoría del Pueblo y la Procuraduría General de la Nación reconocieron en diferentes audiencias e informes, el fenómeno paramilitar**.

Torres expresó que esperan que en algún momento el Tribunal rectifique su decisión debido a que incluso los paramilitares han reconocido, en el solo **Bloque Norte de las Autodefensas más de 6.000 víctimas ejecutadas extrajudicialmente**, “si eso no se hizo bajo el amparo de un aparato organizado de poder, sino de unas bandas criminales que utilizaban una franquicia para hacerlo, realmente es una afirmación descabellada y fuera de contexto” afirmó Torres.

### **El surgimiento del Paramilitarismo en Colombia** 

El vocero de COEUROPA, recordó que el paramilitarismo inició con las Autodefensas Campesinas de Córdoba y Urabá, que se desplegaron a través de la Costa Atlántica y posteriormente en Norte de Santander y Santander, **adquiriendo una estructura política y militar, divida en bloques, frentes y comisiones**. Prueba de ello es el computador de alias Jorge 40 que cuenta con la suficiente información para demostrar la existencia de una organización.

Además, COIEUROPA tiene estudios e informes, conocidos por la Fiscalía y la Procuraduría de la Nación, que demuestran que los **defensores de derechos humanos, los líderes sociales y la oposición política fueron las víctimas de los paramilitares** y que tenían la misión de hacerse al poder local, de controlar las finanzas públicas y sembrar el terror, a través de los asesinatos, las torturas y los desplazamientos forzados.  Le puede interesar: ["La macabra alianza entre el paramilitarismo y las empresas bananeras"](https://archivo.contagioradio.com/la-macabra-alianza-entre-los-paramilitares-y-las-empresas-bananeras/)

### **Las consecuencias de negar el paramilitarismo** 

Los Acuerdos de Paz entre las FARC-EP y el Gobierno Nacional, incluyeron diferentes decretos para el desmote del paramilitarismo, entre ellos **la creación de una Unidad** que tenga como misión encontrar la verdad detrás de los crímenes cometidos por esta organización.  Le puede interesar: ["Los retos de la Unidad para el Desmonte del Paramilitarismo"](https://archivo.contagioradio.com/se-crea-en-colombia-unidad-de-desmonte-del-paramilitarismo/)

Razón por la cual, la decisión del Tribunal de Justicia y paz, para Torres, cae como **“un baldado de agua fría”** y podría tener repercusiones en el proceso de paz con el ELN, dado que el asesinato de líderes sociales y defensores de derechos humanos y la existencia del paramilitarismo es uno de los puntos que esa organización guerrillera resalta en sus comunicados.

Además, justo cuando el congreso discute la ley estatutaria de la Jurisdicción Especial de Paz y se escogen los magistrados y magistradas que harán parte de la Comisión para el esclarecimiento de la Verdad, una sentencia como esta **podría modificar la perspectiva global para entender y clarificar el conflicto armado en Colombia.**

Torres aseguró que aunque dicha comisión es independiente no se puede imponer una visión de la verdad sin que antes las versiones sean sometidas a revisión. Además la tipificación del delito de paramilitarismo quedaría sin piso probatorio porque la sentencia afirma que el paramilitarismo no existe.

### **¿El Paramilitarismo sigue vivo en Colombia?** 

Organizaciones defensoras de derechos humanos y políticos, han denunciado que detrás del asesinato, la persecución y amenazas a defensores de derechos humanos, reclamantes de tierras y líderes sociales, está el paramilitarismo. Sin embargo, tanto el Ministerio de Defensa como el Presidente Santos han negado esta afirmación, sin dar paso al debate.

“Durante estos 6 años de gobierno del presidente Santos hemos querido hacer una mesa de garantías para discutir el tema de la existencia o no y persistencia del fenómeno paramilitar, el gobierno hasta ahora no ha dado ninguna muestra de querer dar ese debate” afirmó José Humberto Torres.

Asimismo, agregó que “**los 117 asesinatos de defensores el año anterior, los 40 asesinatos que van en lo corrido del año, lo que demuestran es una continuidad en la estrategia paramilitar** de alguna manera borrar de la faz de la tierra colombiana a todo aquel que haga oposición al establecimiento.” Le puede interesar: ["Hay patrones comunes en asesinatos de defensores de derechos humanos"](https://archivo.contagioradio.com/hay-patrones-comunes-en-asesinatos-de-defensores-de-ddhh-en-el-pais/)

<iframe id="audio_19161515" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19161515_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
