Title: Parte de verdad en crimen de Álvaro Gómez se ocultaría en la "Masacre de Mondoñedo"
Date: 2020-10-07 21:50
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Álvaro Gómez Hurtado, Carlos Antonio Lozada, Fiscalía general, Julián Gallo Cubillos, Rodrigo Londoño
Slug: parte-de-verdad-en-crimen-de-alvaro-gomez-se-ocultaria-en-la-masacre-de-mondonedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Comunicado-JEP.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Rodrigo-Londono-y-Carlos-Antonio-Lozada.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En la tarde de este miércoles, **la Fiscalía General de la Nación, citó a rendir declaración juramentada a Rodrigo Londoño y Carlos Antonio Lozada;** luego de que ambos manifestarán públicamente la responsabilidad de FARC-EP —del cual fueron integrantes— en el homicidio de Álvaro Gómez Hurtado. (Le puede interesar: [FARC- EP reconoce ante la JEP la verdad sobre 6 homicidios, entre ellos el de Álvaro Gómez Hurtado](https://archivo.contagioradio.com/farc-ep-reconoce-ante-la-jep-la-verdad-sobre-6-homicidios-entre-ellos-el-de-alvaro-gomez-hurtado/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FiscaliaCol/status/1313931392452001794","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FiscaliaCol/status/1313931392452001794

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Pese a que en el comunicado donde informa de la citación a los dos miembros del Partido FARC, la Fiscalía señala que lo hace en ejercicio del «*principio de colaboración armónica entre las instituciones del Estado*», **algunos sectores aseguran que esta competencia es exclusiva de la Jurisdicción Especial para la Paz -[JEP](https://www.jep.gov.co/Paginas/Inicio.aspx)-, justicia encargada de los hechos acaecidos en el marco del conflicto armado colombiano, especialmente cuando ese Tribunal señaló el pasado 3 de octubre que el caso ya se encontraba en trámite para su competencia.**

<!-- /wp:paragraph -->

<!-- wp:image {"id":91094,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Comunicado-JEP.jpeg){.wp-image-91094}  

<figcaption>
Comunicado de la Jurisdicción Especial para la Paz. 03-Oct-2020

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Carlos Antonio Lozada, señaló que el asesinato fue ejecutado por la  Red Urbana Antonio Nariño -RUAN-, unidad guerrillera de la cual era comandante para ese momento. **Lozada reconoció que fue quien ejecutó la orden y planeó el operativo para asesinar a Álvaro Gómez Hurtado en noviembre de 1995, por orden expresa del extinto comandante Jorge Briceño alias “Mono Jojoy”.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a la participación de otros actores en el crimen, Lozada señaló que en el hecho participaron exclusivamente guerrilleros urbanos de la RUAN, por lo cual la responsabilidad solo era atribuible a las FARC-EP. En igual sentido se había pronunciado Rodrigo Londoño excomandante de las FARC, quién señaló en una carta dirigida a Álvaro Leyva que había que dejar de  buscar culpables donde no los había. (Lea también: [Rodrigo Londoño se comprometió con el «Movimiento Nacional por la Verdad»](https://archivo.contagioradio.com/rodrigo-londono-se-comprometio-con-el-movimiento-nacional-por-la-verdad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lozada sostuvo que días después del asesinato, las autoridades estuvieron tras la pista de los autores, pero no lograron dar con ellos e individualizarlos. **Además, reveló que dos de los autores materiales, fueron asesinados, tiempo después, por la Policía en la Masacre de Mondoñedo, y que quizá ahí, pudieron establecer que eran los responsables del homicidio, pero que dado el nivel de barbarie con el que los ejecutaron, tuvieron que ocultar el hecho, desviar la atención y evitar exponer a los responsables.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tanto Carlos Lozada, como Rodrigo Londoño han manifestado, luego de la declaración conjunta en la que los miembros del antiguo secretariado de las FARC-EP reconocieron la responsabilidad del grupo armado en el homicidio de Gómez Hurtado y el de otras 5 personas, su disposición de comparecer y ofrecer detalles de estos y otros hechos del conflicto ante la Jurisdicción Especial para la Paz.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
