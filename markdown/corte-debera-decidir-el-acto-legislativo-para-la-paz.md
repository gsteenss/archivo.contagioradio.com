Title: Corte deberá decidir el acto legislativo para la Paz
Date: 2016-12-01 13:33
Category: Nacional, Paz
Tags: #AcuerdosYA, Cámara de Representantes, Implementación de Acuerdos, Representante Alirio Uribe
Slug: corte-debera-decidir-el-acto-legislativo-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/23_08_2015corte_constitucional-e1473979535347.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [1 Dic 2016] 

El pasado 30 de Noviembre con 130 votos a favor fueron refrendados los acuerdos de paz en la Cámara de Representantes, ahora **la Corte Constitucional es la que tiene la potestad de decidir cuál será el acto legislativo para la implementación.**

El representante por el Polo Democrático Alirio Uribe Muñoz, destacó que **“votamos a favor del pueblo, de la vida y contra la guerra, votamos para que no haya mas victimas en Colombia,** votamos para que las FARC transite de ser una organización armada a una política”.

Otro aspecto valioso que dejaron las 15 horas de debate para Uribe Muñoz, fue “la posibilidad de escuchar a mas de 100 representantes de otras regiones del país quienes optaron por garantizar un mejor futuro a los jóvenes del país **para que no tengan que morir como combatientes ni como militares, policías o paramilitares”.**

La aprobación por refrendación directa realizada ayer, representa un importante avance hacia la materialización de la paz y la consolidación del cese definitivo y bilateral, en palabras de Alirio Uribe **“la refrendación de los acuerdos beneficia a los del no, a los del si y también al 62% que no votó, es una decisión que involucra a todo el país”.**

Adicionalmente, el representante denunció los actos de odio sucedidos en la Plaza de Bolívar mientras se llevaba a cabo el debate, “lo importante es expresar las diferentes visiones de los sectores a través del diálogo, **desde la cámara rechazamos el acto del opositor quien golpeo a una joven periodista el día de ayer”.**

Por otra parte, hizo un llamado a la Corte para “que este a la altura del momento histórico y político del país” y puntualizó sobre la **responsabilidad de la sociedad civil en la exigencia de celeridad y veeduría al proceso de implementación. **Le puede interesar: [Se están activando mecanismos para la implementación de acuerdo de Paz.](https://archivo.contagioradio.com/activados-mecanismos-para-la-implementacion-de-acuerdo-de-paz/)

Por último, la bancada del centro democrático y el partido conservador se retiraron de la plenaria, aunque este ultimo manifestó que **si bien están de acuerdo con el proceso de Paz, no lo está con el mecanismo de refrendación.**

<iframe id="audio_14384408" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14384408_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
