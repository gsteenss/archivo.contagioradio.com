Title: Nuevo Código de Policía es dictatorial y viola DDHH: Alberto Yepes
Date: 2015-06-18 11:29
Category: DDHH, Entrevistas
Tags: abuso de autoridad, Alberto Yepes, código de policía, Congreso de la República, Constitución Política, Coordinación Colombia- Europa-Estados Unidos, Protesta social, taser, violación de Derechos humanos
Slug: nuevo-codigo-de-policia-es-dictatorial-y-viola-ddhh-alberto-yepes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/cantoviv.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [cantovivo.wordpress.com]

<iframe src="http://www.ivoox.com/player_ek_4658327_2_1.html?data=lZuimpiWe46ZmKiak5uJd6KnmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRktbZ19SYpYqnd4a2lMnWydSPqMafsdTZy8iJh5SZoqnOjcrXb8XdxNnO1tTWrcLgjN6Yw9rYs9Pd1cbfy9SJd6Khhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alberto Yepes, Coordinación Colombia Europa Estados Unidos.] 

###### [18 de junio 2015]

En el Congreso de la República se debate el nuevo Código de Policía,  que ha generado bastante polémica debido a que diversos sectores de la sociedad denuncian que se trata de un Código de Policía autoritario y dictatorial, por medio del cual las autoridades podrían **abusar de su poder,  lo que conllevará a la violación de derechos humanos** como lo asegura Alberto Yepes, integrante de la Coordinación Colombia Europa Estados Unidos.

**“Este Código de Policía es mucho más autoritario que el que rige en este momento"** asegura Yepes, quien añade que, se trata de un **“régimen dictatorial”**, por lo que los ciudadanos pasarían a vivir “épocas tenebrosas” bajo ese nuevo Código, que entre otras cosas criminaliza la protesta social, taponando las vías legales para impedir las manifestaciones que promueven la exigencia de derechos de los colombianos y las colombianas.

Así mismo, con este Código, se implementaría el “traslado por protección” que consiste en que la Policía estará autorizada para decidir si es necesario retener a una persona cuando está “bajo alteración de la conciencia”, es decir, que esté bajo algún efecto de sustancias psicoactivas o alcohol. Además, se **permite el ingreso de la policía a los domicilios residenciales sin necesidad de que exista una orden judicial.**

Para Yepes, la implementación de estas medidas significan la violación a derechos fundamentales que  están contenidos en la Constitución Política, como la libertad, la libre movilización, la privacidad y el derecho a la protesta social, entre otros.

Sumado a eso, como también se permite el **uso de armas como taser** (arma electrochoque), se estaría aprobado **aparatos destinados a la tortura y violación de derechos humanos,** señala el defensor de DDHH, lo que indica que se está acudiendo a instrumentos dictatoriales para obtener el control de los individuos por medio de acciones de violencia.

En cambio, se reducen las posibilidades de que las autoridades paguen por violaciones a derechos humanos, ya que únicamente **los ciudadanos podrán reclamar 5 días después algún tipo de abuso de autoridad,** como el asesinato de una persona en medio de una protesta.

Para Alberto Yepes, la única vía para detener este proyecto de Ley que reforma el Código de Policía, y que ya pasó el primero de cuatro debates, es hacer u**so de las manifestaciones ciudadanas y acudir a organismos internacionales.** Esto, teniendo en cuenta que para el integrante del Observatorio de Derechos Humanos y Derecho Humanitario, **la Corte Constitucional no tomará ninguna acción ante este Código de Policía,** que a luz pública es inconstitucional, debido a que por los actos de los magistrados, es muy posible que la Corte “vaya a estar del lado de los ciudadanos", dice Yepes.
