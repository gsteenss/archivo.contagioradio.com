Title: Atentan contra Arley Velasco, lider social del Putumayo
Date: 2017-12-06 14:37
Category: DDHH, Nacional
Tags: asesinato de líderes sociales, defensores de derechos humanos, lideres sociales, Putumayo, violencia contra líderes sociales
Slug: atentan-contra-arley-velasco-lider-social-del-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/putumayo-teteye-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Miputumayo.com.co] 

###### [06 Dic 2017] 

De acuerdo con la Red de Comunicadores Populares del Sur, el día de ayer el líder social **Arley Velasco fue gravemente herido con 9 disparos** que le propiciaron hombres vestidos de negro. El hecho sucedió en el corredor de Puerto Vega-Teteyé en zona rural de Puerto Asís en el Putumayo. Debido a la crítica situación que viven los líderes sociales, ha habido un desplazamientos de cerca de 30 familias.

La Red indica que Arley Velasco salía de su casa el 5 de diciembre a las 6:00 am, que se encuentra ubicada en la vereda la Carmelita en donde está también el **Espacio de Reincorporación** que agrupa a los frentes 32, 48 y 49 del bloque sur de las FARC. Fue en ese momento cuando hombres fuertemente armados le dispararon en 9 ocasiones.

Velasco fue trasladado al hospital de Puerto Asís en donde se encuentra fuera de peligro y en recuperación. El atentado ocurre en un momento donde la situación de seguridad para los líderes sociales es precaria y **se encuentran bajo constante riesgo**. El líder hace parte del sindicato de trabajadores del Putumayo SINCAFROMAYO, que hace parte de la Federación Nacional Sindical Unitaria Agropecuaria FENSUAGRO. (Le puede interesar: ["Colombia entre los 4 países más letales para los defensores de DDHH"](https://archivo.contagioradio.com/colombia-4-paises-letales-defensores-de-ddhh/))

### **Asesinatos de líderes y defensores en el Putumayo continúan en aumento** 

Un día antes del atentado contra Arley Velasco, fue asesinado el presidente de la Junta de Acción Comunal de la vereda la Brasilia, **Luis Alfonso Giraldo**. Ante este hecho, la diputada del Putumayo, Yuri Quintero, indicó que el crimen está siendo investigado por las autoridades encargadas.

Según la información de la Red de Comunicadores, Quintero manifestó que los presidentes de las juntas de Acción Comunal del Corredor Puerto Vega Teteyé **están amenazados** en parte por promover el programa de sustitución voluntaria de cultivos ilícitos en Putumayo y Cauca. (Le puede interesar: ["Asesinan a Luis Alfonso Giraldo, líder social de la Carmelita en Putumayo"](https://archivo.contagioradio.com/asesinan_luis_alfonso_giraldo_camelita_putumayo/))

Esto ha hecho que, como afirmó Quintero a la Red de Comunicadores, **“las comunidades se encuentren bajo una gran incertidumbre** desestabilizando de alguna manera la continuidad y la participación activa de las comunidades en la aplicación e implementación del programa de sustitución”.

### **Violencia ha causado el desplazamiento de varias familias** 

Debido a los constantes hechos de violencia que se han registrado, varias comunidades campesinas de la zona rural **se han desplazado a la cabecera municipal de Puerto Asís**. Los integrantes de las juntas de acción comunal y sus familias son quienes se han desplazado temiendo por su vida y la integridad física de sus familiares.

Finalmente, los integrantes del partido político FARC, la Fuerza Pública, defensores de derechos humanos y la comunidad del corredor Puerto Vega-Teteyé, han estado analizando la situación para **activar mecanismos de protección** basados en las garantías de no repetición y el reconocimiento de los campesinos como sujetos de derecho.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
