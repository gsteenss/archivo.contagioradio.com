Title: Empieza la fiesta audiovisual en Bogotá "Cinemateca al parque"
Date: 2015-11-18 09:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Cinemateca al parque, Cinemateca Distrital, Ciro Guerra, el abrazo de la serpiente, Franco Lolli, Gente de bien, IDARTES, Julian David Correa
Slug: empieza-la-fiesta-audiovisual-en-bogota-cinemateca-al-parque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/cinemateca-al-parque-e1447799390121.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [17 Nov 2015]

Desde hoy Bogotá vive su "Fiesta audiovisual" con la 4ta edición de la "Cinemateca al Parque", un espacio que convoca a todo el público capitalino que disfruta el cine, bien sea como aficionados, cinéfilos o realizadores, interactuando con directores, colectivos de realización y expertos de Colombia y el mundo.

Desde el 17 hasta el 21 de noviembre, en el Parque Nacional Enrique Olaya y la Cinemateca Distrital se proyectarán de manera gratuita producciones nacionales e internacionales, debates, conferencias, talleres y encuentros abiertos para grandes y chicos.

El evento inicia con la Asamblea Audiovisual, en la sede de la Cinemateca, con participación de los representantes del Consejo Distrital Audiovisual y un grupo de expertos entre los que se encuentran para conversar sobre las necesidades actuales del sector y estructurar a partir de ellas políticas públicas

Algunas de las producciones destacadas de esta edición son las galardonadas: "Gente de bien" de Franco Loli y  "El abrazo de la serpiente" de Ciro Guerra, y los cortometrajes realizados por las localidades de Bogotá  que se estrenan en el marco de la estrategia territorial y poblacional "Cinemateca Rodante".

De la progamación académica, vale la pena destacar el Taller de Cine Recursivo, impartido por el cineasta Miguel Urrutia, quien se ha destacado por crear técnicas, tecnologías y estrategias para rodar películas con poco presupuesto de excelente calidad y que incluirá el rodaje de una secuencia de su película "Los suicidios de Sara" en el marco del evento.

Otras de la charlas incluidas en esta edición están relacionadas con temas como la creación audiovisual y demostraciones de efectos especiales y corrección de color,  derechos de autor y opciones de financiación más allá de las convocatorias públicas para realizar proyectos audiovisuales.

Julián David Correa, director de la Cinemateca distrital explica que "Cinemateca al parque abarca cuatro días en la Cinemateca y uno en el Parque Nacional, cinco días en los que proyectaremos nuevos filmes colombianos, impulsaremos el desarrollo de nuevos proyectos audiovisuales a través de Cinemateca Lab, realizaremos conferencias, una feria audiovisual y tendremos espacios de creación para los niños y sus familias".

El cierre del evento, a realizarse el próximo 21 de noviembre, será con el gran evento metropolitano del cine que el sábado 21 se desarrollará en el Parque Nacional. Encuentre aqui toda la [progamación](http://idartes.gov.co/attachments/article/4432/Programaci%C3%B3n%20Cinemateca%20al%20parque%202015.pdf).
