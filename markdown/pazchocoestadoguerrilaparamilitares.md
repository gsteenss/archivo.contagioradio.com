Title: La paz en el Chocó: entre sombras y luces
Date: 2017-03-20 11:50
Category: Nacional, Paz
Tags: Chocó, ELN, estado, exigencias, FARC-EP, Paramilitarismo, paz
Slug: pazchocoestadoguerrilaparamilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Caminata-por-la-paz-60.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 20 Mar 2017 

Organizaciones étnicoterritoriales del Chocó y las diócesis que tienen jurisdicción en ese Departamento, manifestaron sus **preocupaciones frente al presente y futuro de los acuerdos de paz de La Habana** y su implementación, la actual **situación de la guerrilla del ELN** y el avance de **otras agrupaciones armadas** que el gobierno no puede controlar. Elevando una serie de exigencias a los diferentes actores del conflicto.

En un documento de dos páginas, **el Foro Interétnico, la Mesa Departamental Indígena, y las Diócesis de Apartadó, Itsmina y Quibdó**, reiteran que siguen poniendo sus esperanzas en una solución negociada de los conflictos armados y sociales que han afectado al país, las Organizaciones se refieren a algunos fenómenos que "pueden poner en crisis nuestra fe en un eventual desenlace exitoso de los procesos de paz".

Para el caso de los acuerdos con la guerrilla de las FARC-EP, las organizaciones exponen su **preocupación por "la falta de adecuación logística de las Zonas Veredales**, la tendencia de algunos sectores sociales y políticos a **desconocer los acuerdos ya firmados**; la falta de claridad en el d**esmonte de las estructuras de milicianos** tanto en las zonas rurales como urbanas; y las fallas en la aplicación del capítulo étnico de los acuerdos, especialmente por el **desconocimiento de las normas de consulta previa**".

En cuanto al ELN la comunicación titulada "La paz en el Chocó: entre sombras y luces",  se refiere a la manera como "**han venido ocupando el territorio de poblaciones étnicas y sus espacios comunitarios**, lo cual pone en riesgo a las mismas comunidades y a sus autoridades". Le puede interesar: [Persiste la presencia de paramilitares sobre el río Tamboral en el Chocó](https://archivo.contagioradio.com/persiste-la-presencia-de-estructuras-neoparamilitares-sobre-el-rio-tamboral-en-el-choco/).

Las Organizaciones expresan su temor frente al "avance amplio, abierto y sistemático de **grupos armados ilegales con discurso y actitudes de paramilitarismo** que ocupan territorios que la Fuerza Pública no logra controlar" y la continuidad de la **violencia y criminalidad en los centros urbanos**, especialmente contra jóvenes y sectores marginales que generan en la población "desespero por la inseguridad y poca efectividad de las autoridades".

Ante las situaciones expuestas, los firmantes proponen una serie de exigencias y exhotaciones **al Estado colombiano, para que implemente mecanismos de protección a los habitantes del Chocó**, "recuperando el ejercicio del monopolio de las armas según las normas del Estado de Derecho", así como resultados concretos en el **control y desarticulación de bandas criminales y de paramilitares**, ante la permisividad que hace perder credibilidad en el proceso de paz.

A las guerrillas, FARC EP y ELN, les exigen "**perseverar en la voluntad y el compromiso de poner fin al conflicto armado** respondiendo a las expectativas de la Sociedad Civil", tal como lo manifestaron al inicio de los diálogos de paz, y extienden una invitación al cuerpo diplomático acreditado en Colombia y al sistema de Naciones Unidas a **continuar apoyando al Proceso y todas las iniciativas de la Sociedad Civil** "que conducen al logro de la terminación del conflicto armado y a la construcción de la paz".

Por último, exhortan a los medios de comunicación, locales, nacionales e internacionales a "**a informar con veracidad lo que ocurre en este contexto, siendo aliados importantes para visibilizar la crisis humanitaria en el Chocó**", una de las regiones que más ha sufrido por el abandono estatal, el desplazamiento y la violencia.

[Comunicado Paz Chocó 2017](https://www.scribd.com/document/342463156/Comunicado-Paz-Choco-2017#from_embed "View Comunicado Paz Chocó 2017 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_34991" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/342463156/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-IwMGSk9NdtzOidR5hvbs&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
