Title: Lista la consulta popular de minería en Pijao, Quindío.
Date: 2017-05-16 13:03
Category: Ambiente, Nacional
Tags: consulta popular, Mineria, Pijao
Slug: lista-la-consulta-popular-de-mineria-en-pijao-quindio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/salinas-1-e1494957735234.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Poderciudadano.com.co] 

###### [16 mayo 2017] 

**"¿Está usted de acuerdo, si o no, en que en el municipio de Pijao, se lleven a cabo procesos de mineria de metal?".** Esta es la pregunta que fue aprobada para que la comunidad de Pijao en Quindío decida si quiere o no minería en sus territorios que se ven amenazados por 5 títulos y 14 solicitudes, muchas de ellas relacionadas con la minera Anglo Gold Ashnati.

La pregunta que había propuesto el alcalde municipal de Pijao, Edison Aldana Martínez ante el Consejo Municipal y que fue aprobada. Se espera que en un tiempo **no superior a mes y medio se lleve a cabo la consulta popular en este municipio.** Con ese proceso electoral se daría fin a un trabajo de más de 9 años para que los habitantes de ese municipio definan si quieren ese tipo de actividades en su territorio. Le puede interesar: ["Vía libre para consulta popular minera en Pijao"](https://archivo.contagioradio.com/via-libre-para-la-consulta-popular-minera-en-pijao/)

### **El proceso de la consulta popular en Pijao** 

Después de que la Corte Constitucional emitiera un fallo en 2016 a favor de las comunidades, y que otorga potestad a los entes territoriales para decidir sobre el uso del suelo y el subsuelo de sus municipios, **los habitantes de Pijao retomaron la carrera por defender su riqueza ambiental a través de la figura de consulta popular.**

Para Mónica Flórez, miembro del Comité Ecológico y la Fundación Pijao ChittaSlow, el objetivo de la consulta es que "**los habitantes de Pijao manifiesten su sentido de apropiación por un territorio que todavía tiene aire puro".**

La lucha que mantiene por la realización de la consulta lleva en curso desde 2009. En este año, Flórez solicitó a la Agencia Nacional de Minería información acerca de los títulos mineros otorgados en este municipio. **La ANM entregó un informe en el que figuraban 23 títulos otorgados a empresas mineras que buscan metales como oro y niquen.** Le puede interesar: ["Municipios decidirán su propio modelo de desarrollo"](https://archivo.contagioradio.com/municipios-decidiran-su-propio-modelo-de-desarrollo/)

### **Las amenazas de la minería en Pijao** 

Flórez afirma que para 2017 hay otorgados 5 nuevos títulos y 14 nuevas solicitudes de otorgamiento de licencias. Para Flórez, este aumento de solicitudes se debe a que el gobierno ha cambiado las reglas de juego queriendo parar las consultas populares y así **favorecer a las empresas como Anglo Gold Ashanti.**

Mónica Flórez recordó que el proceso desarrollado en Pijao, “es un camino más que se abre para otros territorios pues sienta un precedente a nivel nacional y latinoamericano para la defensa de la vida en los territorios”.

### **Pijao es una joya del eje cafetero** 

Pijao es un municipio ubicado en la cordillera de los Andes al sur del Quindío y hace parte de la reserva central Forestal. **Tiene paisaje de valle, páramo y montaña y tiene varios afluentes hídricos.** En 2011 fue declarado como un área de influencia del paisaje cultural cafetero y en 2014 entró a la lista de pueblos del buen vivir que promueven la tranquilidad y la biodiversidad.  
Además, según Mónica Flórez, Pijao tiene fallas geológicas como la falla geológica de San Jerónimo, la falla de Silvia Cauca y en 1999 **Pijao fue parcialmente destruido cuando ocurrió el terremoto que devastó parte del eje cafetero.  **

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
