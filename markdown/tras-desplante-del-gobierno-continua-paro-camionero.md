Title: Tras desplante del gobierno continúa paro camionero
Date: 2016-06-13 13:34
Category: Entrevistas, Movilización
Tags: Gobierno Nacional, Paro camionero
Slug: tras-desplante-del-gobierno-continua-paro-camionero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/camioneros-e1465842235230.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Patria 

###### [13 Jun 2016]

**"El paro camionero continúa", así lo aseguró Juan Carlos Bobadilla, secretario general de la Asociación Colombiana de Camioneros**, quien explica que el gobierno sigue incumpliéndole a los transportadores, pues este fin de semana el Ministro de Transporte, Jorge Eduardo Rojas, los dejó plantados y por eso situación siguen estancadas las tres sesiones de trabajo que se habían acordado la semana pasada.

“Se entregó [el pliego de peticiones](https://archivo.contagioradio.com/camioneros-de-colombia-se-unen-a-minga-nacional/), y aunque se había establecido tres sesiones de trabajo con el ministro, quedó en llamarnos porque iba a hacer unas consultas, pero no se ha acordado nada”, explica Bobadilla, quien añade que una cosa es lo que se dice el gobierno en los medios de comunicación, pero otra es la que hace.

**“No sé si se está jugando a desgastar a los camioneros, y el gobierno estaba apostándole a salir de uno de los dos paros**”, expresa el vocero. Además, se denuncia que a la fecha no aparecen los 2 billones de pesos de los planes de chatarrización, ni se ha cumplido los puntos del paro camionero anterior.

Teniendo en cuenta esa situación, los camioneros señalan que **el paro pacífico se fortalece, y por eso desde este lunes hay 7 puntos nuevos de concentración en Antioquia, Valle del Cauca y Eje Cafetero.** Asimismo, las manifestaciones y movilizaciones continúan en Cundinamarca, Huila, Boyacá y Santander donde en el punto de La Lizama, al igual que los campesinos, fueron víctimas de la arremetida por parte del ESMAD y el Ejército.

“El gobierno responde con represión, y lo que necesitamos son soluciones… **Pide que la movilización sea pacífica, pero de esa manera no la quiere atender**” expresa Juan Carlos Bobadilla, y agrega que la crisis en la industria y el aumento de la inflación va a empezar a verse en los próximos días si el gobierno no dialoga con los camioneros.

<iframe src="http://co.ivoox.com/es/player_ej_11886050_2_1.html?data=kpalmpuUeZGhhpywj5WdaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncavpwtOYpcbWsNDnjKfcxMbIrc3gwpCajabXs8TdwsjWh6iXaaOnz5Cw0dHTscPdwtPOjcnJb6TVzs7cj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
