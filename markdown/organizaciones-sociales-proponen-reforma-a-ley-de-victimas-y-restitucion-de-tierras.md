Title: Organizaciones sociales proponen reforma a Ley de Víctimas y Restitución de Tierras
Date: 2017-05-03 13:50
Category: DDHH, Nacional
Tags: Acuerdos de La Habana, MOVICE, víctimas del conflicto armado
Slug: organizaciones-sociales-proponen-reforma-a-ley-de-victimas-y-restitucion-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Movice.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [03 May 2017] 

El Movimiento de Víctimas de Crímenes de Estado, exigió a través de un comunicado, que se promulgue un decreto de reforma a la Ley de Víctimas y Restitución de Tierras (1448), para que se **amplíe la definición de víctima y que esta, sea compatible con lo que se estableció en el Acuerdo Final de La Habana.**

Además, instó a que se incluya como hechos víctimizantes los genocidios políticos, los etnocidios y el desplazamiento forzado transfronterizo, como requisitos para la inscripción del Registro Único de Víctimas y que sean derogados los artículos 50, 51, 52 y 53 de la Ley 975 de 2005 por **ser contrarios o regresivos para los derechos de las víctimas.** Asimismo, expresó la importancia de que se realicen ajustes a los decretos nacionales 1084 de 2015 y 2569 de2014.

De igual forma, proponen que este registro **sea reabierto por un periodo de 5 años más** (contados a partir de la reforma de la Ley 1448) y que sea el gobierno sea el encargado de realizar una amplia estrategia de difusión para conocer las modificaciones. Le puede interesar:["En 2017 han sido asesinados 41 líderes sociales"](https://archivo.contagioradio.com/en-el-transcurso-del-2017-han-sido-asesinados-41-lideres-sociales/)

Igualmente expuso propuestas en términos de Salud, participación y memoria que garanticen una reparación a las víctimas del conflicto armado:

### **El derecho a la Salud de las víctimas** 

Para garantizar la atención especial de las víctimas y de sus necesidades específicas como sujetos de protección constitucional, el MOVICE propone **crear un Sistema de Atención Especializado que deberá estar a cargo del Estado** y que no tendría la intervención de aseguradoras para prestar el servicio, de igual forma, este sistema deberá garantizar la cobertura de asistencia en salud a las víctimas por el simple hecho de estar inscritas en el RUV y a aquellas que demuestren su condición de víctima así no se encuentren en el registro.

Otra de las medidas que propone el Movimiento de Víctimas, es la **creación de una política Nacional de Atención Psicosocial y de Salud para la Reparación Integral,** en conjunto con las víctimas del conflicto y organizaciones de víctimas, que este bajo los parámetros ordenados por la Corte Interamericana de Derechos Humanos y la Corte Constitucional de Colombia. Le puede interesar: ["Las víctimas de crímenes de Estado tenemos derecho a ser escuchadas"](https://archivo.contagioradio.com/las-victimas-de-crimenes-de-estado-tenemos-derecho-a-ser-escuchadas/)

### **La Participación de las víctimas** 

En materia de participación, el MOVICE propone la **creación de un protocolo para alcaldes, gobernadores y el Comité Ejecutivo de Atención y Reparación a las víctimas**, en donde las mesas de participación no sean el único mecanismo, sino que se tenga en cuenta escenarios alternos. Este protocolo deberá contar con el apoyo y a financiación por parte del Gobierno Nacional y de los entes territoriales.

### **La memoria para la reparación** 

En este punto el Movimiento propone que como ejercicio de memoria nacional el gobierno reconozca los **lugares de memora no gubernamentales como espacios de salvaguarda de la memoria de los pueblos** y del esclarecimiento de la verdad y que le garantice cuatro principios para su existencia: un carácter participativo, autonomía, sostenibilidad y seguridad.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
