Title: La verdadera crisis no se desprende de fallos de la JEP: Defendamos la paz
Date: 2019-05-21 10:19
Author: CtgAdm
Category: Judicial, Paz
Tags: Crisis Institucional, Defendamos la Paz, JEP, paz
Slug: crisis-no-se-desprende-fallos-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-21-at-10.09.21-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Así lo expresó esta mañana el movimiento “defendamos la paz” al mismo tiempo en que el presidente Iván Duque convocaba una serie de consultas a los partidos políticos, para promover un llamado Acuerdo Nacional ante lo que el uribismo ha llamado la crisis de la justicia, en relación con la decisión de la JEP de cobijar con la garantía de no extradición a Jesús Santrich. (Le puede interesar: ["Defendamos la Paz denuncia ante la ONU presiones contra magistrados en Colombia"](https://archivo.contagioradio.com/defendamos-la-paz-denuncia-ante-la-onu-presiones-contra-magistrados-en-colombia/))

El movimiento Defendamos la Paz, que incluye a una serie de personas que han estado vinculadas la política afirma que si hay una crisis en el país pero que está generada por el asesinato de líderes sociales, el desplazamiento masivo, la ausencia del Estado y las presiones a las que se han sometido a las Fuerzas Militares para el incremento de los resultados operacionales, lo que podría redundar en una nueva ola de los llamados “falsos positivos”.

En el comunicado el movimiento también llamó la atención en torno a la posibilidad de que el gobierno declarara un Estado de Conmoción para tomar medidas extraordinarias, una de las cuales sería la extradición administrativa de Santrich. Según ellos, una decisión como esa sería “asestar un golpe contra la democracia, e indefectiblemente dar un giro hacia la entronización de un poder totalitario.” (Le puede interesar: ["Crece rechazo al llamado acuerdo nacional convocado por el presidente Duque"](https://archivo.contagioradio.com/rechazan-acuerdo-nacional/))

### **Una actuación meticulosa tejida para provocar una crisis de legitimidad** 

Para ellos es importante que se respeten las decisiones judiciales que está tomando la JEP, pues allí está representado el acuerdo de paz que se alcanzó con las FARC y no respetarlo implica una serie de situaciones que desconocen lo avanzado por parte de ese órgano de justicia y los fallos que cometió la fiscalía podrían evidenciar una suerte de accionar deliberado para afectar a la propia JEP y al proceso de paz.

“Estos hechos generan la fuerte impresión de que todo lo ocurrido pareciera una actuación meticulosamente tejida para provocar una crisis de legitimidad a la JEP, causar un grave daño al proceso de paz, inducir la equivocada percepción de que ha acontecido un cambio traumático en el funcionamiento del mecanismo de extradición, y promover la sensación de que se ha producido una situación de caos institucional que solo puede resolverse con medidas excepcionales.”

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
