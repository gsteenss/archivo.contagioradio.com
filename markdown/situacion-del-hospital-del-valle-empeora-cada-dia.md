Title: Defensa del Hospital Universitario del Valle llegó a Bogotá
Date: 2015-10-20 15:24
Category: Movilización, Nacional
Tags: Alejandro Gaviria, crisis de la salud, feu, Hspital Universitario del Valle, Ley 100, Marcela Urrea, Marcha por la Salud, Ministerio de Salud, Universidad del Valle
Slug: situacion-del-hospital-del-valle-empeora-cada-dia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/marcha_estudiantes_HUV_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elpais 

<iframe src="http://www.ivoox.com/player_ek_9100141_2_1.html?data=mpadkpaYdY6ZmKiakpqJd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbaxtPgw5DIqc2fqdTg0s7Ypc2fttPW2MrWt8rowtfW0ZDIqc2ft8bZzsqPsM3ZyIqwlYqmd4zVjKeah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Marcela Urrea, Estudiante U.V.] 

###### [20 oct 2015]

El día de hoy estudiantes de la UniValle **adelantan movilizaciones en la ciudad de Bogotá por la crisis que aún vive el Hospital Universitario del Valle (HUV)**, las cuales buscan que el Ministro de salud Alejandro Gaviria de soluciones prontas a esta situación.

Desde Cali se movilizaron **en 18 buses 800 personas**, que buscan **concretar esta reunión** con el Ministerio de Salud, ya que “las mesas departamentales no han surtido efecto” y la “situación del hospital en vez de mejorar empeora” indica Marcela Urrea, representante estudiantil al consejo superior de la Universidad del Valle.

Los estudiantes que salieron desde la Universidad Nacional, se reunieron en la Plaza de Bolívar y luego en el Coliseo el Campín, también se realizará un plantón frente al ministerio de salud "**la exigencia de los estudiantes es que podamos concertar una reunión con el ministro Alejandro Gaviria y lograr gestionar un plan de salvamento**", indicó Urrea. Las movilizaciones y plantones se llevaron a cabo hasta mañana.

**Ver también: **[HUV reduce prestación de servicios al 40%](https://archivo.contagioradio.com/por-crisis-el-huv-reduce-prestacion-de-servicios-al-40/)
