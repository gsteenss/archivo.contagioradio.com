Title: Universidad del Atlántico de nuevo bajo la mira del terror
Date: 2019-07-09 23:42
Author: CtgAdm
Category: Comunidad, Educación
Tags: Aguilas Negras, Bomba, Panfleto, Universidad del Atlántico
Slug: universidad-del-atlantico-mira-terror
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/WhatsApp-Image-2019-07-09-at-10.37.25-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El pasado lunes en horas de la mañana la Universidad del Atlántico fue desalojada por una alerta de aparato explosivo, que fue encontrado por estudiantes en un baño del Bloque D. El artefacto fue detonado de forma controlada por miembros de la Policía, y en horas de la tarde, los directivos de la Universidad emitieron un comunicado afirmando que se retomarían clases a partir de la 1 de la tarde del mismo día; sin embargo, **los estudiantes no respondieron al llamado por el temor que causaron los hechos ocurridos en la mañana.**

### **La bomba, los panfletos y el incendio**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fudeatlantico%2Fphotos%2Fa.282206358522611%2F2315035135239713%2F%3Ftype%3D3&amp;width=500" width="500" height="595" frameborder="0" scrolling="no"></iframe>

Sobre las 8:30 de la mañana estudiantes alertaron sobre el artefacto, que fue encontrado en los baños del Bloque D, al interior del Campus. En el lugar también **había panfletos firmados por las Águilas Negras**, en los que se amenazaba al grueso del movimiento estudiantil, que tuvo una fuerte participación en el Paro Nacional que se adelantó en el segundo semestre de 2018. (Le puede interesar: ["¿Por qué pidió un descanso no remunerado Carlos Prasca, rector de la U. del Atlántico?"](https://archivo.contagioradio.com/carlos-prasca-rector/))

Ante la alerta por el artefacto, fue necesaria la presencia de la Policía, que más tarde realizó una detonación controlada; y posteriormente, en declaraciones, sostuvieron que la bomba era de mediano alcance y tenía un rango mortal de 5 metros. **Luego de una revisión de las instalaciones de la Universidad, la Administración llamó a reanudar las actividades a partir de la 1 de la tarde.**

No obstante, **la Universidad del Atlántico vio interrumpida su normalidad por un incendió que se presentó en dos pisos del mismo Bloque en el que explotó el artefacto.** Aunque los cuerpos de bomberos controlaron el incendio, y dijeron que se debió a un corto circuito; según Fabian Salcedo, un estudiante de la Universidad, el hecho generó temor y escepticismo entre la comunidad.

### **La bomba "recordó hechos del pasado, en que miembros de la Universidad han sido asesinados por las fuerzas del paramiltiarismo"**

De acuerdo a Salcedo, desde el paro estudiantil "han ocurrido amenazas, intimidaciones y persecución" a los estudiantes; de allí que la aparición de la bomba y los panfletos haya generado zozobra en la comunidad estudiantil, al recordar "hechos del pasado, en que miembros de la Universidad han sido asesinados por las fuerzas del paramilitarismo debido a su talante crítico". (Le puede interesar: ["Águilas Negras amenazan a defensores de DD.HH. en la Costa Atlántica"](https://archivo.contagioradio.com/aguilas-negras-amenazan-a-defensores-de-ddhh-de-la-costa-atlantica/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38274111" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38274111_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
