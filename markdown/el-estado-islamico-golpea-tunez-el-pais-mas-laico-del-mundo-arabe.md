Title: El Estado islámico golpea Túnez, el país más laico del mundo árabe
Date: 2015-03-19 19:26
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Atentado terrorista Túnez, Awatef ketiti, Túnez país laico
Slug: el-estado-islamico-golpea-tunez-el-pais-mas-laico-del-mundo-arabe
Status: published

##### Foto:Vozpopuli.com 

###### **Entrevista con [Awatef Ketiti], profesora de Comunicación:** 

<iframe src="http://www.ivoox.com/player_ek_4237388_2_1.html?data=lZegmZicfI6ZmKiak5eJd6KnkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdXZz9nOxtSPuMbm09Tfy9jYpYzX0NPh1MaPqc2f0cbfzsbRqc%2Fo0JDS0JC4aaSnhqeu0Mreb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En la **mañana del martes** un joven **ametralló a un grupo de turistas** que provenían de un crucero por el mediterráneo, minutos después otros **tres hombres intentaron acceder al parlamento**, pero fueron interceptados por la policía. En medio del fuego cruzado tomaron como rehenes a otro grupo de turistas que estaban accediendo al museo del Bardo de Túnez cerca del propio parlamento.

Tras el asalto de la policía, el balance **total es de 23 muertos y más de 30 heridos**. Todos los **terroristas del EI** han sido abatidos e identificados, dos de ellos estaban siendo investigados por la policía por vínculos con el islamismo extremista.

**Túnez** fue el país donde comenzaron las **protestas de la Primavera Árabe**, que más tarde se extenderían a países como Bahrein, Egipto o Siria. Las protestas acabaron con la dictadura para dar paso a elecciones democráticas donde ganaron fuerzas islamistas. Después de otra oleada de protestas se repitieron las elecciones y esta vez ganaron con **mayoría las fuerzas políticas laicas reformando la constitución**, las leyes y el propio parlamento.

Según **Awatef Ketiti, profesora de comunicación  y activista tunecina**, el ataque terrorista del martes **rompe** con las **dinámicas de paz** del país y responde al descontento del islamismo extremista con el gobierno laico. En un país donde no ha habido conflicto armado, ni milicias y donde el ejército es pequeño la población no está acostumbrada a este tipo de acciones terroristas.

Para la activista, el **Estado Islámico** **no tiene mucha fuerza en el país,** pero si existe una red de predicadores que son la base para futuras cooptaciones. El grupo armado calificado por la profesora como **“transnacional”** tiene dos vertientes de lucha una la nacional y otra la **internacional, esta última financiada por la OTAN, en la última década**.

Entre los **objetivos del EI, están el impedir constituciones laicas o gobiernos no islamistas** en países árabes, así como fundar estados basados en la interpretación radical y extremista de la ley islámica.

**Awatef ketiti** añade que para la **sociedad tunecina ha sido un duro choque** los atentados y por ello **todas las fuerzas políticas** han respondido **condenando unánimemente la acción terrorista.**
