Title: Parques eólicos ¿una nueva amenaza para los territorios Wayúu?
Date: 2019-04-08 20:05
Author: CtgAdm
Category: Voces de la Tierra
Tags: energía eólica, Guajira, Wayuu
Slug: parques-eolicos-una-nueva-amenaza-territorios-wayuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Parque-éolico-Jepírachi-1-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:La Cháchara.co 

Bajo la premisa de encontrar caminos para la generación de energías limpias, alternativas a los combustibles fósiles y las hidroeléctricas, los parques eólicos se han venido constituyendo en apuestas que se extienden por territorios donde las condiciones naturales son favorables para su implementación. Sin embargo, una reciente investigación del **Instituto de Estudios para el Desarrollo y la Paz (INDEPAZ)**, revela que en La Guajira, región con alto potencial energético de Colombia, podría presentarse una ocupación territorial por cuenta de la instalación de más de** 2.500 torres aerogeneradoras, ubicadas en 65 parques pertenecientes a 19 empresas**, la mayoría de ellas multinacionales, que podría afectar la vida de **al menos 500 comunidades**.

En Voces de la tierra, nos acompañaron **Camilo González Posso**, presidente de INDEPAZ y la investigadora **Joanna Barney**, autores del libro **"El viento del Este llega con revoluciones": multinacionales y la transición eólica en territorio Wayúu,** para hablar sobre los hallazgos de su trabajo consignados en la publicación; resaltando la importancia que tienen para el suministro energético del país, paralelamente al cuidado que implica el que se desarrollen en territorios colectivos de comunidades indígenas. Energías limpias con políticas limpias para los pueblos.

<iframe id="audio_34198544" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34198544_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información ambiental en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en Contagio Radio 

<div class="yj6qo ajU">

</div>
