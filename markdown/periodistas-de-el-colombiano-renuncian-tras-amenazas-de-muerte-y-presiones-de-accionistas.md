Title: Periodistas de “El Colombiano” renuncian tras amenazas de muerte y presiones de accionistas
Date: 2015-04-21 16:25
Author: CtgAdm
Category: DDHH, Nacional
Tags: Blanquicet, El Colombiano, FLIP, Fundación para la Libertad de Prensa, Hacienda Flor del Monte, Hernández de la Cuesta, INCORA, Juan Carlos Hernández de la Cuesta, Ley de Victimas y Restitución de Tierras, Pedro Vaca, Restitución de tierras
Slug: periodistas-de-el-colombiano-renuncian-tras-amenazas-de-muerte-y-presiones-de-accionistas
Status: published

###### Foto: frentean.org 

Los periodistas, de los que la Fundación para la Libertad de Prensa, **FLIP**, se reserva el nombre se vieron **obligados a renunciar a su trabajo luego de presiones por parte de los accionistas para que no publicaran información concerniente a la restitución de tierras** en la región del Urabá antioqueño y luego de recibir una amenaza de muerte en medio de una visita a la región en la que investigaban los avances de los procesos de restitución.

Según la denuncia realizada por la FLIP y confirmada por Pedro Vaca, director, el pasado 11 de Febrero unos de los periodistas del medio **“El Colombiano”,** publicó la denuncia acerca de la desaparición de una de las líderes de **restitución de tierras** de la región, quien afortunadamente apareció al día siguiente. Sin embargo **Juan Carlos Hernández de la Cuesta**, accionista del medio mencionado y también de “La República” envió una nota de protesta al diario por la publicación realizada y solicitando consultar otras fuentes.

En otro de los hechos que rodean esta denuncia está que el “*17 de febrero un camarógrafo y un reportero de El Colombiano iniciaron una misión en Apartadó para investigar lo que sucedía en el proceso de restitución de tierras en la región del Urabá. Según le informaron a la FLIP, el reportero recibió una llamada del señor Jorge Andrés Hernández de la Cuesta, hermano de Juan Carlos, quien lo citó a una reunión para hablar de la investigación que él realizaría. Durante el encuentro, Hernández de la Cuesta le recordó que él era “su patrón y le indicó con cuáles fuentes debía reunirse*.”

El 19 de febrero, uno de los periodistas fue abordado por un hombre en motocicleta que le entregó una amenaza en contra de los dos reporteros que se encontraban haciendo la investigación.

La familia Hernández de la Cuesta es propietaria de la hacienda **Flor del Monte**, ubicada en **Blanquicet**, vereda de Turbo, y cuenta con 736 hectáreas de extensión, todas ellas englobadas en la década de los 90, después de ser propiedad de varios campesinos a quienes el **INCORA** les adjudicó los predios, sin embargo todos los contratos de Compraventa está firmados por Raúl Mora Pérez o su padre, Raúl Mora Abad.

A esta amenaza se suman los problemas en el programa de restitución de tierras. Según cifras de la URT, de las más de 300.000 solicitudes que se esperan, solo se han recibido cerca de **76.000, y de las recibidas solo se ha resuelto el 0.3%,** lo cual deja entredicho la meta trazada por el mismo gobierno y la capacidad, así como voluntad política para realizar el proceso de manera efectiva.

De cara al proceso de conversaciones de paz, este es una de las principales reclamaciones de las comunidades, dado que el acceso y la propiedad de la tierra se están garantizando para conglomerados económicos pero no para las comunidades negras, indígenas y campesinas, que las reclaman en todo el territorio nacional.
