Title: Mesetas firma Pacto social por la paz y la reconciliación
Date: 2017-03-19 18:48
Category: Nacional, Paz
Tags: colombia, FARC-EP, Mesetas, paz, Zona Veredal
Slug: mesetasmetapazcolombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/138ef99d-405f-4d27-a566-415be09233d71.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 19 Mar 2017 

Hombres y mujeres representantes de sectores sociales, políticos, productivos, población vulnerable, víctimas, comunidades étnicas y campesinas y delegados de las FARC-EP entre otros, reunidos en la en la población de Mesetas, Meta, **firmaron este domingo un Pacto social por la paz y la reconciliación**, en apoyo a las comunidades campesinas por la implementación de los Acuerdos.

En el documento los participantes ponen de manifiesto **su intención de honrar la voluntad de paz de los colombianos y la memoria de las víctimas**, declarando a Mesetas territorio de paz, proponiendo** un pacto social y político departamental** que incluya todas las fuerzas vivas del Meta, sus autoridades y a las FARC EP.

Al mismo tiempo  señalan que se sienten representados por los Acuerdos alcanzados en La Habana por ser **"una oportunidad transformadora para el país"**, y su voluntad de implementarlos **"conforme al espíritu de su letra y de la necesidad de superar la página de la guerra, la exclusión y la pobreza"** que ha predominado en los territorios donde el conflicto se ha enraizado por tantos años.

Como parte de las apuestas fundamentales para construir la paz, los Mesetenses mencionan la necesidad de tr**abajar por la defensa, protección y mantenimiento de los ecosistemas estratégicos** y demás bienes comunes para el beneficio de las generaciones actuales y futuras. Proponen además requerir a la Comisión de Seguimiento, Impulso, Verificación e Implementación, CSIVI, la **gestión de un proyecto productivo de alto impacto** "que genere desarrollo económico, empleo, fortalecimiento comercial y bienestar social para los municipios del pie de monte llanero".

En busca de aportar a la consolidación de una paz completa que incluya a otros actores del conflicto, los firmantes proponen **generar y fortalecer espacios de diálogo con el ELN** que terminen en la firma de un nuevo acuerdo de paz, con el apoyo de la sociedad para que este se blinde de peligros y produzca** la confianza necesaria para que otras guerrillas se acerquen al diálogo**. Le puede interesar: [Las nuevas vidas en la Zona Veredal de Buenavista en Mesetas, Meta](https://archivo.contagioradio.com/en-fotos-la-vida-en-la-zona-veredal-en-buena-vista-mesetas/).

Con toda la disposición para emprender acciones reales de paz con autonomía en sus territorios, los promotores de la inciativa esperan **designar una delegación de participantes en el evento para que se traslade a la Zona Veredal Transitoria de Normalización de Buenavista**, ubicada en Mesetas, para que la comandancia de las FARC en esa zona tambien firme el documento si lo considera pertinente.
