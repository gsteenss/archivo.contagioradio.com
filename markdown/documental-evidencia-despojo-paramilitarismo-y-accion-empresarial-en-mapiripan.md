Title: Documental evidencia despojo, paramilitarismo y acción empresarial en Mapiripan
Date: 2015-08-11 16:25
Category: DDHH, Nacional
Tags: Bloque Meta, después Heroes de Guaviare, Environmental Investigation Agency, ERPAC., Jiw, mapiripan, Palma en Mapiripan, paramiliatares, Poligrow, Sicuany
Slug: documental-evidencia-despojo-paramilitarismo-y-accion-empresarial-en-mapiripan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Captura-de-pantalla-2015-08-11-a-las-16.51.09.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [Entre el agua y el aceite de Palma] 

<iframe src="http://www.ivoox.com/player_ek_6545162_2_1.html?data=l5qhl5aado6ZmKiak5uJd6KompKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiNDX1tLS0NnFsIzZ187Rx9PHrcKfxcrg0tTOs4ampJDdw9fFscrgytnO1M7XsdCf2pDOxcjNaaSnhqeg0JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Defensor de DDHH de Comisión Justicia y Paz] 

###### [11 Ago 2015] 

Una reciente publicación de la **Comisión de Justicia y Paz**, **Environmental Investigation Agency**, EIA por sus siglas en Inglés, con el apoyo de Contagio Radio, deja al descubierto la acción nociva de las e**mpresas palmeras en la región de Mapiripán**, evidencia también la situación de las comunidades indígenas y campesinas, que en medio del control paramilitar se oponen a la ampliación del monocultivo de Palma de Aceite.

Según el documental, la **acción de la empresa Poligrow afecta no solamente el derecho al territorio de las comunidades indígenas Jiw y Sicuany,** sino también su derecho al agua, dado que la ampliación de las [[siembras de palma de Aceite destruye el ecosistema](https://archivo.contagioradio.com/audiencia-en-congreso-de-la-republica-por-accion-de-poligrow-en-mapiripan/)] de “morichales” vital para la producción y conservación del líquido que abastece a los llanos orientales colombianos.

Además, Fabio Pedraza, defensor de DDHH de la Comisión de Justicia y Paz afirma que las empresas palmeras como Poligrow **están actuando en una región controlada totalmente por los grupos paramilitares que actúan tanto en el casco urbano de Mapiripán.**

Según Ariza, en la zona rural hay puntos de control públicos en “La Cooperativa”, “San Andrés”, “Gaucamayas”, la finca Santa Ana entre otras. *“Sabemos de la presencia de los urabeños”* que hacen parte de estructuras antiguas como **Bloque Meta, después Heroes de Guaviare y luego el ERPAC.**

En cuanto a la complicidad con estructuras militares, el defensor de DDHH asegura que hay evidencias que comprueban que hay consentimiento y complicidad para tráfico de armamento y tráfico de ilegal de drogas.

\[embed\]https://www.youtube.com/watch?v=waW6p98GWto\[/embed\]

Sobre la [[empresa Poligrow](https://archivo.contagioradio.com/poligrow-palma-mapiripan/)] y la **complicidad con el paramilitarismo**, la empresa habría llegado al territorio en 2008 y varias familias fueron obligadas a vender sus tierras por presión paramilitar, además denuncian el asesinato de Giovanni, joven que trabajaba en la empresa palmera, asesinado en las propias instalaciones de la empresa Poligrow.
