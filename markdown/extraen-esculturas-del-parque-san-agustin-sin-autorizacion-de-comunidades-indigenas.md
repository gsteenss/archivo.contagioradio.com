Title: Extraen esculturas precolombinas en Huila sin autorización de comunidades indígenas
Date: 2019-02-06 16:55
Author: AdminContagio
Category: Comunidad, Nacional
Tags: Preservación Arqueológica, San Agustín
Slug: extraen-esculturas-del-parque-san-agustin-sin-autorizacion-de-comunidades-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/escultura-san-agustin-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sebastian Coronado Espitia 

###### 6 Feb 

[Las **Comunidades Yanakunas, e Ingas de la Gaitana** al Sur del Huila en San Agustín denuncian que desde el día 4 de febrero, personal del Parque Arqueológico de San Agustín ingresó a sus predios por orden **del Instituto Colombiano de Antropología e Historia ICANH p**[ara remover y trasladar las piezas arqueológicas que se encuentran allí para ser puestas en otro lugar.  ]]

[Según **Sayari Campo, pedagoga y acompañante de procesos educativos con los niños de la comunidad de San Agustín,** las esculturas ya han sido extraídas y envueltas en plástico listas para ser transportadas, sin embargo la comunidad menciona que en ningún momento les pidieron autorización para ingresar a los predios y proceder a remover las piezas arqueológicas.]

\

[Esta situación ya se había presentado años atrás, cuando el ICANH organizó una exposición en el Museo Nacional de Bogotá la cual buscaba recrear parte del parque arqueológico en la capital; según explica  Sayari Campo, cada piedra tiene un simbolismo diferente y una función en el territorio, razón por la cual la comunidad se negó a extraer las esculturas, **suceso durante el cual algunas de las piezas resultaron dañadas.**  
  
Aunque aún no se conocen los argumentos para desplazar las piezas arqueológicas, se espera que el jueves 7, el ICANH se reúna con las comunidades y aclare la situación, por ahora se conoce que las estatuas serían trasladadas a otro complejo para que estén más cerca a otras figuras que harían parte de una nueva expansión del  parque, **“esto lo hacen más por la parte turística y económica y no se tiene en cuenta el simbolismo que pueden tener estas piezas para la comunidad”** señala la pedagoga.]

[Según explica Campo, acorde a  las tradiciones indígenas de San Agustín, **cada piedra tiene una vibración importante en la tierra y una función de protección del territorio, del agua y del ecosistema en general** por eso al ser removidas  generan caos físico y espiritual en la de la comunidad, razón por la que rechazan estos hechos y piden respeto por los vestigios y lugares sagrados.]

\*El título de esta nota fue a cambiado por precisiones geográficas, aclarando que las esculturas removidas están ubicadas en La Gaitana y no hacen parte del Parque San Agustín.

###### Reciba toda la información de Contagio Radio en [[su correo]
