Title: Con nombramiento de Paola Meneses, la independencia de la Corte Constitucional quedaría en vilo
Date: 2020-12-10 17:08
Author: CtgAdm
Category: Actualidad, Judicial
Tags: Corte Constitucional, Francisco Barbosa, Iván Duque
Slug: con-nombramiento-de-paola-meneses-la-independencia-de-la-corte-constitucional-queda-en-vilo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Paola-Meneses.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Paola Meneses / @FiscaliaCol

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con 77 votos a favor, la plenaria del Senado eligió a Paola Meneses como nueva magistrada de la [Corte Constitucional](https://www.corteconstitucional.gov.co/) durante los próximos ocho años. Sobre Meneses conocida por su estrecha relación con el presidente Iván Duque se ciernen dudas sobre su independencia y experiencia constitucional en un cargo que debe velar por la integridad de la Constitución en un contexto **en el que se ha advertido existiría una concentración del poder de la rama ejecutiva.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 24 de noviembre, el presidente Iván Duque presentó la terna que postularía para la elección de un nuevo magistrado de la Corte Constitucional en reemplazo de Carlos Bernal. Frente a los tres postulados: **Paola Meneses, José del Castillo y Fernando Grillo,** varios sectores han señalado que la razón para ser ternados estaría más ligada a la cercanía con el presidente que por sus méritos y experiencia en el ámbito constitucional.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/MauroToroO/status/1337148334545522689","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/MauroToroO/status/1337148334545522689

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Portales como **La Silla Vacía** han señalado que la ahora magistrada Meneses **no cuenta con experiencia en derecho constitucional.** A su vez han resaltado que Meneses es amiga de la juventud de Iván Duque graduándose del mismo colegio y la misma promoción en 1994. [(Le puede interesar: Terna de Duque le daría el control de la Corte Constitucional al gobierno)](https://archivo.contagioradio.com/esta-es-la-terna-con-la-que-el-gobierno-duque-tendria-una-mayoria-en-la-corte-constitucional/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque Meneses hizo una especialización en derecho constitucional en el Centro de Estudios Constitucionales de Madrid hace más de 15 años, su carrera se ha enfocado en el área de los servicios públicos, desempeñándose como abogada en la vicepresidencia jurídica de la Empresa de Telecomunicaciones de Bogotá (ETB) y secretaria general de Federación Nacional de Departamentos (FDN) institución a la que llegó de la mano del exministro Amylkar Acosta, amigo de su padre.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El mismo Iván Duque la nombró en 2018 como Superintendente de Subsidio Familiar, puesto al que renunció después de año y medio para trabajar como fiscal delegada contra la criminalidad organizada nombrada por **Francisco Barbosa, fiscal general, también conocido por su estrecha amistad con el mandatario.** [(Le puede interesar: En dos años del Gobierno Duque se ha derrumbado institucionalidad)](https://archivo.contagioradio.com/en-dos-anos-del-gobierno-duque-se-ha-derrumbado-institucionalidad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para septiembre de este año**, la organización Transparencia Internacional y su capítulo en Colombia alertó sobre una concentración del poder de la rama ejecutiva en el país** a propósito de la elección de personas cercanas al Gobierno de Iván Duque para encabezar diferentes organismos de control lo que pondría en riesgo la independencia que deben tener dichos órganos externos de control frente al poder ejecutivo.  [(Le recomendamos leer: Las críticas tras la elección de Margarita Cabello en Procuraduría)](https://archivo.contagioradio.com/las-criticas-tras-la-eleccion-de-margarita-cabello-en-procuraduria/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con esta elección, parece repetirse una constante que se ha vivido durante el último año con funcionarios como el amigo de la infancia de Iván Duque, Francisco Barbosa, ahora fiscal general, la procuradora electa Margarita Cabello, antes ministra de Justicia de este Gobierno y el defensor del Pueblo Carlos Camargo, nombramientos catalogados como maniobras para la cooptación del ejecutivo sobre el poder público y las ramas legislativas y judiciales del país. [Transparencia Internacional alerta sobre concentración del poder en gobierno Duque](https://archivo.contagioradio.com/transparencia-internacional-alerta-sobre-concentracion-del-poder-en-gobierno-duque/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
