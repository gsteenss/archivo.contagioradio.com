Title: Ana Rosa Castiblanco regresa junto a su familia tras 32 años del holocausto del Palacio de Justicia
Date: 2017-11-18 10:44
Category: DDHH, Nacional
Tags: Ana Rosa Castiblanco, Desaparecidos del Palacio de Justicia, Palacio de Justicia
Slug: ana_rosa_castibanco_desaparecida_palacio_justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/CwtHg7dXgAAzLRr.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Scoopnest.com] 

###### [17 Nov 2017] 

El 6 de noviembre de 1985, Ana Rosa Castiblanco, le había prometido a su hijo, de apenas 4 años, que llegaría de su trabajo, en la cafetería del Palacio de justicia, con un superman como regalo, sin embargo, esa tarde nunca regresó **"Todavía estoy esperando ese muñeco", expresa Raúl, hijo de Ana.**

Ese es el recuerdo más fuerte que le quedó de su infancia a Raúl Osvaldo, a quien la toma y retoma del Palacio de Justicia le dejó no solo una madre desaparecida sino un hermano, que apenas tenía 8 meses en el vientre de su madre.

**Tras 32 años, y justo cuando Ana cumpliría 64 años de edad, para lo que se reúne su familia,** no es para compartir una torta para celebrar la vida de su familiar, sino para poder enterrar dignamente sus restos, después de que estos hubieran permanecido desaparecidos durante años, en medio de lo que ha sido la poca eficiencia del Estado colombiano para dar respuesta a quienes siguen lamentando la desaparición de sus seres queridos.

Ana era una mujer sonriente y humilde, que se ganaba la vida con su trabajo en la cafetería del Palacio. Justo ese 6 de noviembre decidió no llevar a su pequeño de 4 años al trabajo, y a cambio le prometió un regalo a su regreso. Buscaba dedicarse a la modistería cuando naciera su bebé y con ello mantener a su familia.

### **La búsqueda de Ana Rosa** 

Hoy lo único en lo que puede pensar su familia, es en que su lucha dio algunos frutos, por tardíos que parezcan. De la mano de René Guarín, hermano de otra de las desaparecidas, Raúl y su familia empezaron la búsqueda, y **16 años después de su desaparición, se encontraron los primeros restos óseos de Ana.** Estos fueron entregados de manera obligada, en una notaría a su familia, asegurándoles que de no recibirlos, serían dejados en una fosa común.

Sin embargo, a partir de la sentencia de la Corte Constitucional, se dictaminó que los restos no fueron entregados debidamente, y por tanto fueron exhumados. Efectivamente se trataba de Ana Rosa, pero aún no hay rastros del bebé que llevaba en su ser. No obstante, esta vez la entrega debe ser en el marco del respeto a  la familia  y a la memoria de la víctima. **Lo que espera el hijo de Ana Rosa es que el Estado colombiano diga la verdad** sobre la desaparición de su madre y su hermano.

Las preguntas son muchas. Según videos sobre lo que fue el holocausto, Ana habría salido viva del Palacio acompañada de militares. Al parecer había sido llevada para la Casa del Florero, pero nunca se supo qué pasó. Algunas versiones aterradoras apuntarían a que ella fue trasladada en un camión del Ejército, y **en su camino dio a luz al bebé, que luego habría sido "rifado" entre los militares, como lo contó el oficial Gámez Mazuera.** Luego habría sido asesinada.

Siempre hay preguntas sin responder e incertidumbres, que ningún gobierno de turno ha respondido, "yo me puedo morir y no voy a saber qué fue lo que pasó, yo siento que el Estado no quiere decir lo que pasó", dice Raúl, a quien le es imposible, hablar a fondo de su madre, porque recalca que la vida se la arrebató y no pudo vivir junto a ella como cualquier niño debería hacerlo.

### **La entrega** 

Raúl agradece la otra familia que tiene junto a las demás víctimas de los desaparecidos,  "Perdimos un familiar, pero conseguimos una familia que no es de sangre, pero si de causa".

Precisamente esa gran familia fue la que acompañó a los hijos, nietos, y hermanas de Ana. Con canciones de Julio Iglesias, y haciendo homenaje a su nombre, las rosas blancas y rojas presenciaron el reencuentro de Ana con su familia.

Para Raúl **"esto no muere aquí, el caso sigue, y debemos seguir presionando al Estado para que cuente la verdad"**. Este sábado él y sus familiares tuvieron una pequeña dosis de alivio. Los restos de Ana, reposarán en la capilla del Colegio San Bartolomé, acompañados de las demás víctimas del Palacio de Justicia.

\

<iframe id="audio_22334819" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22334819_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
