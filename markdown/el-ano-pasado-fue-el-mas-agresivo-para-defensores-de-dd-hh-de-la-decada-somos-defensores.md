Title: 2019 fue el año más agresivo contra defensores de DDHH en toda la década: Somos Defensores
Date: 2020-05-25 12:01
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Defensor de DD.HH., Somos defensores
Slug: el-ano-pasado-fue-el-mas-agresivo-para-defensores-de-dd-hh-de-la-decada-somos-defensores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Agresiones-a-defensores-de-DD.HH_.-registrados-por-el-SIADDHH.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Asesinato-por-tipo-de-líderes-según-el-SIADDHH.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Agresiones-en-la-década-para-defensores-de-DD.HH_..png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: {#foto .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El próximo martes 26 de mayo el Programa **Somos Defensores** presentará su informe anual sobre 2019 respecto a agresiones que registraron contra personas defensoras de derechos humanos. En este balance registraron un leve descenso en los asesinatos de líderes sociales, pero un aumento en las agresiones que se podría explicar por el mayor control de ciertas estructuras armadas en determinados territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque la presentación oficial del documento será a mitad de semana, Contagio Radio tuvo acceso al mismo previamente. Su título, "La Ceguera", está inspirado en el ensayo de José Saramago y busca hacer alusión a lo que el Gobierno ha querido negar, lo que desconoce, y en contraposición, se expresa la visión de una ciudadanía que cada está más atenta a ver.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### De la ceguera del Gobierno

<!-- /wp:heading -->

<!-- wp:paragraph -->

El informe señala que se esperaba que 2019 fuese un año tranquilo, en tanto era el primero del gobierno Duque y se asumía como una transición. No obstante, en términos humanitarios, la curva de crecimiento de agresiones a quienes defiende los DD.HH. aumentó en medio del negacionismo de este fenómeno por parte del Gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre las críticas a la acción estatal el informe destaca la creación de proyectos que no responden a las necesidades de las y los líderes sociales, o cuya ejecución no dio los resultados esperados. Tal es el caso del Pacto por la Vida, el Plan de Acción Oportuna (PAO) o la iniciativa de la Procuraduría y otros entes titulada "Lidera la Vida".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El cuestionamiento a dichas acciones tiene que ver con que no fueron consultadas con las organizaciones defensoras de DD.HH. y las personas directamente afectadas por las mismas, desconocían otras iniciativas creadas en el marco del Acuerdo de Paz o que no tenían un carácter vinculante para las instituciones del Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otro elemento que resalta el informe es la puja del Gobierno para mantener su ceguera respecto al problema estructural de los ataques a defensores; dicha situación pasa por el negacionismo del fenómeno mediante la presentación de cifras que son, por lo menos, cuestionables sobre agresiones a defensores. En ese sentido, recuerda declaraciones oficiales sobre reducciones de entre 25% y 47% en las agresiones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todo ello, mientras la Alta Comisionada de Naciones Unidas para los DD.HH., señala que en 2019 registró 108 asesinatos, es decir, apenas 2 menos que los registrados a lo largo de 2018. (Le puede interesar: ["Colombia inicia el año con aumento de violencia contra líderes sociales"](https://archivo.contagioradio.com/colombia-inicia-el-ano-con-aumento-de-violencia-contra-lideres-sociales/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A ello se suma una disputa respecto a lo que el Gobierno entiende como agresiones, puesto que se limita a los asesinatos, dejando por fuera amenazas y otras formas de violencia contra líderes y lideresas sociales. En el mismo sentido, limita las razones de las victimizaciones a la existencia en el territorio de Grupos Armados Organizados (GAO), minería ilegal y narcotráfico.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El desconocimiento de razones estructurales más allá de las tres razones anteriormente mencionadas, como la persistencia del conflicto, la falta de tierras para campesinos y otros problemas históricos para Somos Defensores se traduce en que la respuesta institucional se limite a medidas de corte policivo y militar, así como de protección mediante la Unidad Nacional de Protección (UNP) que son insuficientes.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Sobre la claridad ciudadana y de la comunidad internacional

<!-- /wp:heading -->

<!-- wp:paragraph -->

En cambio, en el informe se destaca el papel de una ciudadanía cada vez más activa, que ante el asesinato de líderes se dejó conmover por el homicidio, un ejemplo de esto es el caso Maria del Pilar Hurtado y la protesta del 26 de julio en un grito por la vida. De igual forma, resalta el Paro Nacional como la acción de una ciudadanía consciente que encontró en la protesta una forma de luchar por ver protegidos sus derechos económicos, pero también sociales y culturales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otros actores relevantes en análisis del Programa son la Defensoría del Pueblo, los medios de comunicación, los artistas y la comunidad internacional. Sobre la Defensoría, destacando su labor misional de estar junto a las comunidades en el territorio y llamando la atención a otras instituciones del Estado con sus Alertas Tempranas y sus informes, en los que se cuentan 134 homicidios contra activistas en 2019.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En cuanto a medios y artistas se subraya su papel preponderante para comunicar sobre las vidas de los líderes y sus iniciativas. Mientras se presenta el ejemplo de la acción de la comunidad internacional en el informe presentado por el relator especial de Naciones Unidas sobre la situación de defensores de DD.HH., Michel Forst, sobre Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe causó malestar en el Gobierno por su contenido, al punto que se denunció que no se permitió una última visita oficial de Forst para hacer seguimiento a las entrevistas que había realizado previamente. Dicho informe resalta, entre otras cosas, una tasa de impunidad en casos de homicidios a líderes y lideresas que ronda el 95%; así como cuestiona la estigmatización a la labor que realizan estas personas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El aumento de control por estructuras armadas significan nuevos riesgos para defensores de DD.HH.

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Sistema de Información sobre Agresiones contra personas defensoras de Derechos Humanos (SIADDHH) de Somos Defensores registró durante 2019 124 líderes y lideresas asesinadas, 628 casos de amenazas, 52 atentados y 7 casos de robos de información. La cantidad de homicidios se redujo en 31 casos respecto a 2018, ello no significó un menor riesgo para defensores de DD.HH.

<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":84716,"sizeSlug":"large"} -->

<div class="wp-block-image">

<figure class="aligncenter size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Agresiones-a-defensores-de-DD.HH_.-registrados-por-el-SIADDHH.png){.wp-image-84716}  
<figcaption>
Agresiones registradas por el SIADDHH en 2018 y 2019
</figcaption>
</figure>

</div>

<!-- /wp:image -->

<!-- wp:paragraph -->

Somos Defensores explica que hubo más agresiones en las zonas donde antes las FARC ejercían total control. Por lo tanto, el proceso de paz generó vacíos que no llenó la institucionalidad y fueron objeto de disputa por la reconfiguración del poder. (También lo invitamos a consultar:["En enero se registraron 55 hechos de violencia política contra líderes políticos, sociales y comunales"](https://archivo.contagioradio.com/en-enero-se-registraron-55-hechos-de-violencia-politica-contra-lideres-politicos-sociales-y-comunales/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la misma medida se destaca que en los territorios donde hay más liderazgos también se presentan más agresiones, lógica que respondería a que precisamente dichos liderazgos tienen más capacidad de respuesta a los poderes presentes. (Lo invitamos a seguir leyendo: ["No nos dejen solos, la petición de líderes sociales del Chocó ante amenazas"](https://archivo.contagioradio.com/no-dejen-solos-lideres-sociales-choco/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con este panorama, vale la pena resaltar que en 30 de los 32 departamentos del país se presentaron agresiones, siendo los casos más graves el de Cauca (con 237 casos), seguido por Antioquia (60) y Arauca (52). En cuanto a los liderazgos más afectados por agresiones (no solo asesinatos) se encuentran en primer lugar líderes indígenas (271), seguidos por defensores de DD.HH. (128) y luego comunitarios (97).

<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":84717,"sizeSlug":"large"} -->

<div class="wp-block-image">

<figure class="aligncenter size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Asesinato-por-tipo-de-líderes-según-el-SIADDHH.png){.wp-image-84717}  
<figcaption>
Tipo de liderazgo asesinado en 2019 según Somos Defensores
</figcaption>
</figure>

</div>

<!-- /wp:image -->

<!-- wp:paragraph -->

En cuanto a los posibles responsables de las 844 agresiones registradas por el Sistema de Información, un 47% correspondería a estructuras paramilitares, 34,2% a agentes desconocidos, 13% a grupos disidentes, 4,2% al Ejército de Liberación Nacional (ELN) y un 3,5% a miembros de la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El año pasado fue el año más agresivo de la década para defensores de DD.HH.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aunque hubo un leve descenso en los homicidios registrados entre 2018 y 2019 (31 casos menos), según Somos Defensores en 2019 se contaron en promedio 2,3 agresiones por día, "convirtiendo este año como el más agresivo de la década. En su explicación, la menor cantidad de homicidios se explicaría en el aumento de control por parte de ciertas estructuras armadas en algunos territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con la reconfiguración en favor de un solo actor armado, los niveles de violencia más evidente (los homicidios) se reducen, dando paso a las demás formas existentes de control social a las comunidades: amenazas, atentados, desapariciones o robos de información. Adicionalmente, el mayor control por parte de una sola estructura supone que las y los líderes tengan que dialogar con dichos actores armados, sometiéndolos a un escenario de riesgo también en materia judicial.

<!-- /wp:paragraph -->

<!-- wp:gallery {"ids":[84719]} -->

<figure class="wp-block-gallery columns-1 is-cropped">
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Agresiones-en-la-década-para-defensores-de-DD.HH_..png){.wp-image-84719}
    </figure>

</figure>
<!-- /wp:gallery -->

<!-- wp:paragraph -->

Si desea consultar la totalidad del informe, y los anteriores producidos por el Programa Somos Defensores, lo invitamos a seguir este [link](https://somosdefensores.org/informe-anual-1/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
