Title: Procuraduría declara 'Ilegal' detención de Milagro Sala
Date: 2016-02-29 16:07
Category: El mundo, Judicial
Tags: Mauricio Macri, Milagro Sala, organización Túpac Amaru
Slug: procuraduria-determina-que-detencion-de-milagro-sala-es-ilegal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/00135003531.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: INFOBAE 

###### [29 Feb. 2016]

La Procuraduría de violencia Institucional Argentina declaró que la lideresa de la organización barrial ‘Túpac Amaru’, Milagro Sala, **fue detenida “ilegalmente" y "se impone la urgente e inmediata resolución que ordene su libertad**", cesando los efectos de un delito que “se está repitiendo mientras persista la situación actual”.

La entidad, dependiente de la procuración General de la nación, argumenta su decisión en el **artículo 16 de la ley 27120** que “reconoce la inmunidad de arresto para los legisladores del Parlasur” apartado que **aplica plenamente para el caso de la dirigente popular **electa desde el 25 de octubre de 2015, lo que obliga a jueces y fiscales acatarla y aplicarla.

El dictamen proferido por los fiscales general de la **PROCUVIN** Miguel Ángel Palazzani y ad hoc José Alberto Nebbia, advierte además que los jueces Raúl Gutiérrez, Gastón Mercau y la fiscal Liliana Fernández de Montiel, **cometen el delito de “privación ilegítima de la libertad”**, por lo que serían objeto de acciones penales.

La determinación se da tras analizar una denuncia enviada por Federico Zurrueta, a cargo de la Fiscalía 2 de San Salvador de Jujuy y por la presentación judicial hecha por otros parlamentarios de Mercosur, en la que denuncian irregularidades en el armado de la causa judicial.

Los fiscales aclaran que Milagro Sala **debe cumplir con el proceso penal que hay en su contra** por supuestamente beneficiarse de fondo públicos, pero **en condiciones de libertad**.
