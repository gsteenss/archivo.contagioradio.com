Title: "Ni una víctima más": Soraya Bayuelo, Montes de María
Date: 2015-04-13 10:58
Author: AdminContagio
Category: Paz, Resistencias, Video
Tags: centro de memoria, habana, montes de maría, museo de la memoria, soraya bayuelo, víctimas
Slug: ni-una-victima-mas-soraya-bayuelo-montes-de-maria-2
Status: published

###### Foto: Contagio Radio 

El jueves 9 de abril en el marco de la conmemorición del Día de las víctimas del conflicto armado, se dio entrega al predio en el cual se construirá el Museo Nacional de la Memoria. El acto, precedido por el Alcalde Gustavo Petro, contó con la presencia de la delegación de víctimas que viajó a La Habana para ser escuchada en la Mesa de Diálogos de Paz.

Soraya Bayuelo habló a nombre del grupo de víctimas, instando a que el Museo se convierta en un espacio de múltiples voces y experiencias, para que el 9 de abril la consigna sea "Ni una víctima más".

\[embed\]https://www.youtube.com/watch?v=X05tItNeh9w\[/embed\]
