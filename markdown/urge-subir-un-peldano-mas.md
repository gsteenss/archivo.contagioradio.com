Title: ¡Urge subir un peldaño más!
Date: 2017-08-31 06:00
Category: Mision Salud, Opinion
Tags: Acceso a los medicamentos, Acceso a Salud, Medicamentos
Slug: urge-subir-un-peldano-mas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/pills-1885550_1280.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nosheep 

#### **Por[ Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)** 

###### 31 Ago 2017 

La hepatitis C es la alteración de la función del hígado provocada por el virus del mismo nombre (VHC). En la fase avanzada la infección es muy progresiva, pudiendo llegar a producir cirrosis hepática o cáncer del hígado, lo que hace de ella una enfermedad potencialmente mortal.

La existencia de la hepatitis C es muy reciente, ya que sólo fue postulada en 1970 y confirmada en 1989. En la actualidad no existe vacuna para prevenirla pero en cambio se ha avanzado enormemente en el desarrollo de tratamientos cada vez más eficaces y menos prolongados, siendo de destacar dos recomendados por la OMS: la combinación de sofosbuvir y ledipasvir (Harvoni®), del laboratorio Gilead, y la combinación de Daklinza® (declatasvir), de Bristol-Myers Squibb, y Sovaldi® (sofosbuvir) de Gilead. Ambas combinaciones requieren un tratamiento en unos casos de 12 semanas y en otros de 24 semanas y pueden lograr tasas de curación superiores al 95%.

Infortunadamente el acceso a estos tratamientos es muy limitado, debido a que, a pesar de que el costo del tratamiento de 12 semanas utilizando cualquiera de estas combinaciones se estima en 1,19 dólares diarios, incluido un margen de utilidad del 50%, los laboratorios productores cobran por el mismo precios exorbitantes, llegando al extremo de 1.750 dólares diarios en Estados Unidos, lo que significa 147.000 dólares el tratamiento de 12 semanas y 294.000 dólares el de 24 semanas.

En Colombia según la OPS actualmente viven 425.000 personas con hepatitis C. De ellas, el Ministerio de Salud estima que en los próximos 10 años cerca de 60.000 requerirán de alguno de estos tratamientos para curarse. Su precio para 12 semanas era hasta hace pocos días de \$114,3 millones y \$137,2 millones por paciente, respectivamente, o sea \$1.360.714 y \$1.633.333 diarios, sumas impagables sin poner en riesgo la sostenibilidad financiera del sistema de salud y la prestación de otros servicios prioritarios.

Para controlar esta amenaza a la salud pública y asegurar que los pacientes tengan acceso a los tratamientos sin que ello implique la quiebra del sistema, el Ministro de Salud anunció recientemente que ha decidido recurrir a un mecanismo de compras centralizadas desarrollado por la Organización Panamericana de la Salud, lo que permitirá reducir el costo del primer tratamiento aquí mencionado a \$23,5 millones y el del segundo a \$29 millones, generando ahorros para el sistema de salud del orden de \$290.000 millones anuales.

Este anuncio constituye una noticia tan necesaria como encomiable y plausible, desde luego que significa una caída del 80% de los precios de estos tratamientos. No obstante, es imperativo subir un peldaño más, desde luego que los nuevos precios son también escandalosos: cerca de \$300.000 diarios por paciente para un tratamiento cuyo costo es de alrededor de \$3.500 diarios. Para lograrlo será necesario que el gobierno recurra a alguno de los mecanismos de control de precios existentes, entre ellos la declaración de interés público con fines de licencia obligatoria y las importaciones paralelas de los medicamentos de países donde el precio es mucho más bajo, por ejemplo Egipto, donde se ofrece un tratamiento de 12 semanas, con efectividad comprobada cercana al 100%, a menos de 300 dólares, precio equivalente a menos del 4% de lo que obtendrá Colombia a través de la OPS.

Sabido es que este es un reto difícil, como todo lo que implica anteponer el interés general por encima de los intereses comerciales de la industria más poderosa y deshumanizada del planeta, pero es un paso indispensable e impostergable en el noble propósito de asegurar a todas las víctimas de esta terrible enfermedad la posibilidad de curarse.

###### *\* Texto publicado originalmente en el blog “*[Medicamentos sin barreras](http://blogs.eltiempo.com/medicamentos-sin-barreras/2017/08/16/urge-subir-un-peldano-mas/)*” el 16 de agosto de 2017.* 

#### **[Leer más columnas de Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
