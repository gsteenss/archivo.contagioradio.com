Title: Altas temperaturas actuales serán una constante para 2035
Date: 2016-11-10 12:33
Category: Ambiente, El mundo
Tags: Animales, cambio climatico, contaminación, Ecosistemas
Slug: altas-temperaturas-actuales-seran-nuevo-promedio-2035
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/cambio-climatico-e1478798978460.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Diario Hoy 

###### [10 Nov 2016] 

El 2016 viene batiendo records de las temperaturas más altas a nivel mundial, y el 2015 fue el año más caluroso desde que se tiene registro. De seguir a ese ritmo, en el que priman actividades como la ganadería, la minería, la extracción petrolera, y el uso cotidiano de ciertos objetos que aumentan la emisión de gases de efecto invernadero, **el ser humano habrá establecido un nuevo promedio de temperatura para el año 2035.**

Así lo revela un reciente estudio publicado por la **Universidad Nacional de Australia,** en el Boletín de la American Meteorological Society, que concluye que los años record de calor actuales serán  el promedio dentro 20 años.

Sophie Lewis, una de las principales investigadoras asegura que las actividades humanas han  fijado una nueva temperatura promedio que se consolidará a más tardar en 2040. Se trata de **temperaturas que en varios lugares del mundo han generado graves incendios forestales, épocas de sequía, inundaciones,** entre otras consecuencias.

De acuerdo con Lewis, lo único que podría hacerse, es tomar [medidas urgentes y radicales para disminuir la emisión de carbono](https://archivo.contagioradio.com/medidas-urgentes-ante-el-cambio-climatico/)para que algunas regiones del mundo no sufran semejantes temperaturas, pues esta situación ya viene generando graves consecuencias para la salud humana y los ecosistemas.

La contaminación en el aire es uno de los principales causantes de enfermedades respiratorias que **generan al año la muerte de 1,3 millones de personas.** Además, referente a los daños en los ecosistemas, [una tercera parte de las especies animales del planeta podrían extinguirse ](https://archivo.contagioradio.com/la-tercera-parte-de-las-especies-animales-en-el-mundo-se-extinguiran-por-el-cambio-climatico/)si no se actúa desde ya para frenar el cambio climático.

El estudio se realizó analizando cuándo aparecerían nuevos estados normales bajo las cuatro vías de emisiones del Panel Intergubernamental de Cambio Climático. Los investgadores  examinaron temperaturas estacionales de diciembre a febrero en Australia, Europa, Asia y América del Norte.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
