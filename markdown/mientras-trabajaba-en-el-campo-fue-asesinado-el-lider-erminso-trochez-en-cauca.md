Title: Mientras trabajaba en el campo fue asesinado el líder Erminso Trochez en Cauca
Date: 2020-08-06 13:32
Author: AdminContagio
Category: Actualidad, DDHH
Tags: asesinato de líderes sociales, Cauca
Slug: mientras-trabajaba-en-el-campo-fue-asesinado-el-lider-erminso-trochez-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Erminso-Trochez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Erminso Trochez/ @ConpazCol\_

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los hechos ocurrieron alrededor de las 05:40 de la mañana del 5 de agosto mientras el campesino **Erminso Trochez** se encontraba ordeñando junto a su hijo de 12 años cuando fue atacado por hombres armados. El líder social era integrante de la Asociación de Trabajadores Pro-Constitución Zonas de Reserva Campesina de Caloto **(ASTRAZONACAL)**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según **la Red de Derechos Humanos del Suroccidente Colombiano ‘Francisco Isaías Cifuentes’**, los hombres dispararon con arma de fuego contra Erminso Trochez quien fue atendido por habitantes del sector mientras otro grupo de campesinos salió en búsqueda de los atacantes sin lograr alcanzarlos. [(Lea también: Sigue la muerte, durante el fin de semana asesinan tres líderes y excombatientes)](https://archivo.contagioradio.com/sigue-la-muerte-durante-el-fin-de-sesmana-asesinan-tres-lideres-y-excombatientes/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a ser ayudado por sus vecinos y la comunidad, quienes escucharon los disparos e incluso lo trasladaron al centro hospitalario de Caloto, Erminzo falleció como consecuencia de la gravedad de sus heridas. [(Le recomendamos leer: Asesinan al líder sindical Alexis Vergara en Cauca)](https://archivo.contagioradio.com/asesinan-al-lider-sindical-alexis-vergara-en-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

ASTRAZONACAL es una de las organizaciones de esta región del país que ha denunciado asesinatos, amenazas y persecución contra sus integrantes. En octubre de 2019, uno de sus miembros, **Flower Jair Trompeta Pavi** habría sido asesinado por la Fuerza Pública según denuncias de las comunidades mientras otro de sus integrantes, el excombatiente **Ánderson Pérez Osorio** quien cumplía su proceso de reincorporación, también fue víctima de asesinato en junio del mismo año.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la **Red de Derechos Humanos del Suroccidente Colombiano ‘Francisco Isaías Cifuentes’** denuncian que el Estado únicamente hace presencia en la región a través de la Fuerza Pública y aunque en reiteradas ocasiones los hechos suceden en zonas del departamento donde existe múltiple presencia militar, los ataques contra la població, continúan.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Erminso Trochez, era además defensor de derechos humanos y miembro de la Junta de Acción Comunal de la vereda El Carmelo, en Caloto. [(Lea también: Comunidades del Cauca responsabilizan al Ejército del asesinato de Flower Jair Trompeta)](https://archivo.contagioradio.com/comunidades-del-cauca-responsabilizan-al-ejercito-del-asesinato-de-flower-jair-trompeta/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
