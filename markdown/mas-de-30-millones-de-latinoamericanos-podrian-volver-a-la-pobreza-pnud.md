Title: Más de 30 millones de latinoamericanos podrían volver a la pobreza: PNUD
Date: 2016-06-15 16:22
Category: Economía, El mundo
Tags: 'Progreso Multidimensional: bienestar más allá del ingreso, informe PNUD 2016, pobreza en América Latina
Slug: mas-de-30-millones-de-latinoamericanos-podrian-volver-a-la-pobreza-pnud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Pobreza-en-América-Latina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [15 Junio 2016 ] 

De acuerdo con el más reciente informe del Programa de las Naciones Unidas para el Desarrollo PNUD, titulado 'Progreso Multidimensional: bienestar más allá del ingreso', de los 72 millones de latinoamericanos que salieron de la pobreza, **más de 30 millones están en inminente riesgo de recaer en ella**. Por lo instan a las autoridades para que se replantee el modelo latinoamericano de progreso hacia una horizonte multidimensional que más allá de privilegiar el crecimiento económico, fortalezca la garantía de derechos y la protección del medio ambiente.

La mayoría de quienes podrían recaer en la pobreza son mujeres y jóvenes, actualmente vinculados a empleos precarios, que hacen parte de un grupo de 220 millones de personas, el **38% de la población total latinoamericana, que oficialmente no es pobre pero tampoco logró ascender a la clase media** y se sitúa en estado de vulnerabilidad inminente.

El PNUD asegura que entre 2003 y 2013, los mercados laborales y la educación fueron los grandes motores para que cerca de **72 millones de latinoamericanos dejaran la pobreza y 94 millones entraran a la clase media**; sin embargo, durante los últimos tres años se ha identificado una reversión en la tendencia y entre 2015 y 2016 el número absoluto personas pobres aumentó.

"En este momento, por un lado hay que proteger los logros alcanzados, lo cual incluye **prevenir la caída en pobreza de millones de personas y por otro lado hay que impulsar políticas** y estrategias inclusivas e integrales adaptadas a poblaciones que sufren de discriminaciones y exclusiones históricas", aseveró Jessica Faieta, Subsecretaria General del PNUD.

Discriminaciones que tienen que ver con **el campo laboral que en América Latina es mayoritariamente informal** y relaciones desiguales entre hombres y mujeres. Por un lado, la mitad de los 300 millones de trabajadores son asalariados en microempresas con menos de cinco puestos de trabajo, autoempleados sin calificación o no perciben ingresos. Y por el otro, aun cuando la proporción de mujeres con estudios universitarios en la región es más alta (17.3%) que la correspondiente proporción de hombres (14.8%), **las mujeres reciben un salario promedio por hora 16.4% menor en relación a los hombres**.

###### [Reciba toda la información de Contagio Radio en [[su corre]o](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

 
