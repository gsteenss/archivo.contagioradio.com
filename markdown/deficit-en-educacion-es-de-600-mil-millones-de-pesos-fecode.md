Title: Déficit en educación es de 600 mil millones de pesos: FECODE
Date: 2017-05-09 14:16
Category: Educación, Nacional
Tags: fecode, Ministerio de Eduación, Paro Docente
Slug: deficit-en-educacion-es-de-600-mil-millones-de-pesos-fecode
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/FECODE4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [09 May 2017] 

El próximo 11 de mayo, los docentes de Colombia iniciarán un gran paro Nacional en rechazó a los incumplimientos, por parte del gobierno del presidente Santos y el Ministerio de Educación que no han propuesto soluciones al déficit de **600 mil millones de pesos para mejorar la calidad educativa de los estudiantes del país ni las condiciones laborales para los docentes.**

### **Colombia y la deuda con la Educación** 

De acuerdo con Over Dorado, integrante de FECODE, el gobierno de Colombia afronta un déficit presupuestal para la educación de **600 mil millones de pesos, que según los cálculos de esta organización para el 2018 puede aumentar a 1.1 billones de pesos**. Le puede interesar: ["Recursos de Ciencia y Tecnología no se están invirtiendo en Investigación"](https://archivo.contagioradio.com/fondo-de-ciencia-y-tecnologia/)

Este déficit, para Dorado, es producto del cambio de modelo de educación que se realizó en el año 2001 y que, durante 7 años, **quito de los recursos de la educación 73 billones pesos** y que con el paso del tiempo se han ido aumentando hasta a cifra actual.

Para los docentes, esta desfinanciación se ve reflejada en que **no haya fondos que garanticen la nómina docente de los años venideros, comedores escolares, el cambio a la jornada única**, en la cobertura y que obliga a los padres, pese a no tener los recursos, a matricular a sus hijos en colegios privados.

Aumentado la brecha en el sector educativo entre las posibilidades de acceder a la educación, las cifras que ha recolectado FECODE señalan que **1.446.295 niños y niñas, se encuentran por fuera del sistema educativo**, mientras que en matrícula no oficial y privada hay 1.889.649 niños y niñas. Le puede interesar: ["Ser Pilo Paga ha gastado más de 350 mil millones del presupuesto de Educación"](https://archivo.contagioradio.com/ser-pilo-paga-se-llevo-373-290-470-719-del-presupuesto-de-educacion/)

“Hay padres de familia del estrato 0,1,2 y 3 pagando la educación privada, **hay aproximadamente 27.830 estudiantes que hacen parte del estrato cero**, nosotros decimos que las garantías de cerrar la brecha del derecho a la educación, acceso y cobertura la tiene que hacer el gobierno con financiación” afirmó Dorado.

Over Dorado, señala que la inversión en canasta educativa tampoco es favorable “el gobierno está dando por año un promedio de **2.417.000 mil pesos por cada niño en preescolar, el costo debería ser de 5 millones**, hay un faltante de 3 millones”, evidenciando la falta de calidad.

### **Docentes de Colombia sin garantías para trabajar** 

El gremio docente desde el 2015, viene gestando un proceso de movilización frente a la falta de aumento salarial y las condiciones laborales que le permitan continuar ejerciendo la profesión. El último acuerdo al que se había llegado con el gobierno, era aumentar 12 puntos en la tabla de títulos y experiencia de trabajadores del Estado, para esta ocasión le piden al gobierno aumentar otros **4 puntos para estar a la par que los demás trabajadores públicos que cuentan con 28 puntos**.

Otra de las peticiones que hace FECODE es el aumento presupuestal en el sector educativo, sin embargo, durante la reunión que se llevó a cabo el día de hoy, el Ministerio de Hacienda señaló que no hay posibilidades de que la exigencia de aumentar a **7.5% las fuentes de financiación que vienen del Producto Interno Bruto**, debido al problema fiscal que afecta al sistema nacional de participación y por ende al sistema educativo.

Hecho que para Dorado demuestra que “**el gobierno no está pensando en cómo solucionar el tema del déficit histórico que tiene con el sector educativo**, sino que está pensando en cómo cumple la regla fiscal, le cumple a la OCDE y a la Banca Internacional”. Le puede interesar: ["Miles de docentes se movilizan en defensa de la Educación"](https://archivo.contagioradio.com/miles-de-docentes-se-movilizan-en-defensa-de-la-educacion/)

El paro iniciará el próximo 11 de mayo y en él participarán docentes de todo el país, a la espera de que tanto el **presidente Santos como el Ministerio de Educación establezcan una ruta o acciones que den pronta solución a esta problemática**.

[Pliego de exigencias de FECODE](https://www.scribd.com/document/347859966/Pliego-de-exigencias-de-FECODE#from_embed "View Pliego de exigencias de FECODE on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_7280" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/347859966/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-bVcWBoDevOAYQ9XaQrQs&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
<iframe id="audio_18588995" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18588995_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
