Title: Diócesis de Tumaco denuncia graves violaciones de DDHH y abandono institucional
Date: 2015-04-11 10:25
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asesinatos, Comunicado Diócesis de Tumaco, desplazamiento Tumaco, Diócesis Tumaco denuncia violaciones DDHH, Magüí Payán y Barbacoas, Roberto Payán
Slug: diocesis-de-tumaco-denuncia-graves-violaciones-de-ddhh-y-abandono-institucional
Status: published

###### Foto:Programaddhhcolombia.org 

La **Diócesis de Tumaco** ha emitido un **comunicado donde denuncia y se solidariza** con las víctimas de la creciente violencia y de las reiteradas violaciones de los DDHH en los municipios de **Roberto Payán, Magüí Payán y Barbacoas**.

Desplazamiento, violencia relacionada con la delincuencia, graves daños ambientales, y abandono institucional acompañado de escasa sensibilidad con las víctimas. La diócesis denuncia toda esta situación e **insta a las instituciones a responsabilizarse por lo sucedido, además hace un llamamiento a la Defensoría del Pueblo para que actué bajo sus propias competencias.**

**Tumaco es un municipio del suroccidente de Nariño** actualmente su **tasa de homicidios supera en tres veces la nacional**, víctima del conflicto armado entre bandas criminales, paramilitares, insurgencia y ejército colombiano. La población civil se encuentra en una situación de emergencia humanitaria.

Es a partir de los **90´**cuando comienza a agravarse la situación debido a que es el **municipio con más hectáreas de coca cultivadas y una de las zonas de Colombia con más minas antipersona**. En esta época es cuando el gobierno comienza a intervenir militarmente, cometiéndose las **peores violaciones de los DDHH** de su historia.

A continuación el comunicado completo.

**DIÓCESIS DE TUMACO  COMUNICADO PÚBLICO:**

La Diócesis de Tumaco expresa su solidaridad con todos los habitantes de los municipios de Roberto Payán, Magüí Payán y Barbacoas por todo el dolor y sufrimiento que están viviendo. Así mismo pone en conocimiento de la comunidad local, regional, nacional e internacional hechos ocurridos en los últimos 4 meses en los municipios de Barbacoas, Magüí Payán y Roberto Payán de la zona del río Telembí, que evidencian la grave situación de violaciones a los DD.HH.

**En la vía Junín-Barbacoas:**

En septiembre 28 y noviembre 5 de 2014 fueron **asesinados** por delincuentes los **docentes Pedro Augusto Arizala y Jorge Enrique Valencia**.

El pasado **25 de febrero de 2015** fueron **victimizadas 6 personas** que viajaban en un vehículo que presta el servicio de transporte público, 3 de ellas fueron asesinadas entre ellas el conductor del vehículo dónde se movilizaban y un juez del municipio de Roberto Payán, 3 personas quedaron heridas.

**Asesinatos**

En el municipio de Barbacoas con una población aproximada de 30.000 habitantes y gran presencia de Fuerza Pública, los asesinatos son constantes tanto en la zona urbana como en lugares apartados. Algunas personas dicen que “la muerte se ha tomado la comunidad” y que “los mayores están enterrando a los jóvenes”.

En el año **2014, por lo menos, 23 personas fueron asesinadas. En los 2 primeros meses del 2015 han sido asesinadas 11 personas y en su mayoría jóvenes**.

En el último trimestre de 2014 hubo una orden que prohibía movilizarse después de las 9:00 de la noche por las calles de la zona urbana de Barbacoas, bajo la amenaza de que “corría peligro la vida de quien fuera encontrado pasada esa hora”. Esta orden según versiones fue dada por parte del **grupo al margen de la ley denominado “Gaitanistas”**.

**Insensibilidad e irresponsabilidad Institucional.**

El **25 de octubre del 2014, fue asesinada Mabel Evangelina Cabezas de 34 años de edad, una mujer viuda y cabeza de familia con 7 hijos menores edad** a quienes sostenía lavando ropa. Ese día, dos personas se acercaron en una motocicleta a su casa ubicada en el barrio El Paraíso (sector Divino Niño). Uno de ellos bajó de la motocicleta, la empujó y le disparó tres veces. En **el momento de su asesinato estaba con su niña de 6 años quien presenció el hecho y sus otros hijos también** miraron el hecho, aunque estaban más lejos. Inmediatamente la comunidad solicitó hacer el levantamiento, pero las autoridades no lo hicieron con el argumento de no tener camioneta para llevar el cuerpo.

**Los hijos permanecieron junto al cuerpo de su madre y algunas personas buscaron una camioneta privada para que las autoridades hicieran el levantamiento, quienes llegaron al lugar, hicieron el procedimiento y dejaron el cadáver en manos de los dueños de la camioneta**. Estas personas se atrevieron a llevar el cadáver hasta el cementerio, con gran temor porque en ese momento hubo suspensión del servicio de energía y estaba lloviendo. Al día siguiente día le practicaron la necropsia, encima de una tumba, a la vista y exposición de todos como es costumbre en este municipio porque no existe una morgue y algunas veces tampoco hay un médico para la intervención.

**Desplazamiento**

En **18 de enero de 2015** se registró un **desplazamiento de 69 personas, 19 familias residentes en el municipio de Barbacoas**, quienes fueron amenazadas por grupos **paramilitares** generando la salida a otras ciudades. Muchos de ellos se encuentran en precarias condiciones.

**Daño al medio ambiente**

La minería indiscriminada con retroexcavadoras y la contaminación con hidrocarburo del oleoducto Trasandino de **ECOPETROL en ríos y otras fuentes hídricas está generando una catástrofe ambiental** irreversible que pone en riesgo la salud de los pobladores afrocolombianos de la veredas como loma linda, palo seco entre otras y el mismo casco urbano del municipio de **Roberto Payán**.

En el año 2014 pasado se registraron **tres eventos de contaminación con hidrocarburo en los ríos Güelmanbí y Telembí** que afectaron a comunidades negras de los municipios de Barbacoas y Roberto Payán. En dichos municipios, la mayoría de los pobladores no tienen agua potable para lavar, cocinar, ni beber; realidad vigente en las comunidades negras y los pueblos indígenas, habitantes ancestrales de la Costa Pacífica. En el territorio de los municipios de Roberto Payán, Magüí Payán y Barbacoas, que conforman la zona con mayor riqueza aurífera (ORO) en la región, se dice que existen al menos 200 retroexcavadoras y 150 dragones (maquinaria para extraer oro del río) causando diversos daños ecológicos, entre ellos las fuentes hídricas; afectaciones ambientales irreparables, por lo menos, en 100 años.

Frente a todas estas graves situaciones, **muchas personas expresan una gran desconfianza hacia las autoridades civiles y militares**, pues a pesar de que se han realizado Consejos de Seguridad, y existen una alta presencia de Fuerza Pública e instituciones responsables de aplicar justicia, algunos victimarios siguen “saliendo en libertad”. Se dice, por ejemplo, que en Barbacoas hay un Alcalde elegido por voto popular que no gobierna en el municipio. En relación con el tema minero, la Gobernación de Nariño denunció estos hechos de explotación minera y daño al medio ambiente. Las máquinas retroexcavadoras han entrado por la única vía existente que también está militarizada por la policía y ejército nacional.

**SOLICITUDES**

A las autoridades competentes cumplir con su deber de garantizar el derecho a la vida de la población que está sufriendo estos hechos en esta subregión del Pacífico nariñense. A las autoridades competentes tomar las medidas de investigación relativas a la adjudicación de autorizaciones y licencias para adelantar labores de minería con retroexcavadoras, así como adoptar el control respectivo a quien corresponda.

Así mismo, realizar un **estudio del impacto y daños ambientales e implementar las respectivas medidas de reparación ambiental**. A la **Defensoría del Pueblo**, a las Personerías de los municipios de Roberto Payán, Magüí Payán y Barbacoas; a la Procuraduría Regional y Nacional **emprender las acciones pertinentes en relación con las responsabilidades por acción y omisión de las autoridades,** frente a todos estos preocupantes hechos. A la Oficina del **Alto Comisionado de Naciones Unidas para los Derechos Humanos hacer el seguimiento** a tan lamentables hechos e instar a las instituciones gubernamentales al respeto de los derechos humanos.

Marzo 28 de 2015 Diócesis de Tumaco (Nariño)
