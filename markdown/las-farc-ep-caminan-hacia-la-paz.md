Title: Las FARC-EP caminan hacia la paz
Date: 2017-02-02 17:05
Category: Fotografia, Nacional
Tags: FARC, Zonas Veredales Transitorias de Normalización
Slug: las-farc-ep-caminan-hacia-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/curvarado-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ultima-marcha-de-la-guerrillas-de-las-farc-5.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ultima-marcha-de-la-guerrillas-de-las-farc-11.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ultima-marcha-de-la-guerrillas-de-las-farc.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/zonas-veredales-2-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: @NCprensa - Contagio Radio 

###### 2 Feb 2017 

Guerrilleros y guerrilleras llevan a hombro sus bultos y el arma que los ha acompañado por años. Una marcha que luego de 52 años de lucha armada tomará un nuevo rumbo hacia la vida civil en la que continuarán con su apuesta política.

Compartimos las imágenes históricas que nos deja esta gran marcha de las FARC-EP hacia las **Zonas Veredales Transitorias de Normalización. Ver **[Iván Márquez y gobierno acompañarán la última marcha del frente Martín Caballero](https://archivo.contagioradio.com/ivan-marquez-y-gobierno-acompanaran-la-ultima-marcha-del-frente-martin-caballero/ "Iván Márquez y gobierno acompañarán la última marcha del frente Martín Caballero")

\

<figure>
[![curvarado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/curvarado-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/curvarado-1.jpg)

</figure>
<figure>
[![ultima-marcha-de-la-guerrillas-de-las-farc-5](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ultima-marcha-de-la-guerrillas-de-las-farc-5.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ultima-marcha-de-la-guerrillas-de-las-farc-5.jpg)

</figure>
<figure>
[![ultima-marcha-de-la-guerrillas-de-las-farc-11](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ultima-marcha-de-la-guerrillas-de-las-farc-11.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ultima-marcha-de-la-guerrillas-de-las-farc-11.jpg)

</figure>
<figure>
[![ultima-marcha-de-la-guerrillas-de-las-farc](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ultima-marcha-de-la-guerrillas-de-las-farc.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ultima-marcha-de-la-guerrillas-de-las-farc.jpg)

</figure>
<figure>
[![zonas-veredales-2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/zonas-veredales-2-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/zonas-veredales-2-1.jpg)

</figure>

