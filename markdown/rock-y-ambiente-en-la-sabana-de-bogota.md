Title: Rock por el ambiente en la Sabana de Bogotá
Date: 2016-07-22 14:00
Category: Ambiente, eventos
Tags: Ambiente, Eco Rock Sabana, Festivales
Slug: rock-y-ambiente-en-la-sabana-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/logo-festival-rock-ecosabana-curvas.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Rock Eco Sabana 

##### [22 Jul 2016] 

[El llamado de la tierra por la protección del ambiente retumbará a ritmo de rock en el **Festival Rock Eco Sabana**, evento que en su primera versión reunirá al público de Bogotá y sus alrededores en torno a la concientización sobre los impactos provocados por el hombre en la naturaleza.]

Con una programación que incluye géneros musicales como el rock, punk, metal, blues, jazz y fusiones alternativas, el festival busca conectar a los asistentes con su lado natural, promoviendo un alto sentido y compromiso ecológico, comprendiendo que enfrentar los cambios que atraviesa el planeta son responsabilidad colectiva.

El cartel del evento reúne agrupaciones de renombre como Estados Alterados,  Masacre, Koyi K utho, Nepentes, Revolver plateado, V for volume, Kilcrops y la Real Academia del sonido, con nuevas propuestas provenientes de municipios de la sabana como Red Devil de Tocancipá, The Hunters de Guasca e Hypnotica de Zipaquirá entre otras, convirtiéndose además en una plataforma de apoyo a nuevos artistas locales.

[El **Festival Rock Eco Sabana** tendrá lugar los días 13 y 14 de agosto, en el "Club de Golf Briceño", de Sopo, con entradas disponibles en Ticket Shop.  Para más información sobre el listado y presentación de bandas puede consultar [aquí](http://festivalrockeco.com/cartel-2016.html).]

<iframe src="https://www.youtube.com/embed/lejoyoHBzJY" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
