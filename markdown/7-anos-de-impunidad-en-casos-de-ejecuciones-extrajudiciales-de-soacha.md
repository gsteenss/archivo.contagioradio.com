Title: 7 años de impunidad en casos de ejecuciones extrajudiciales de Soacha
Date: 2015-11-12 14:16
Category: DDHH, Nacional
Tags: Crímenes cometidos por militares, ejecuciones extrajudiciales en Colombia, falsos positivos, Falsos positivos Colombia, Falsos positivos Soacha, Fuerzas Militares de Colombia, impunidad en Colombia
Slug: 7-anos-de-impunidad-en-casos-de-ejecuciones-extrajudiciales-de-soacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Geo Activismo ] 

<iframe src="http://www.ivoox.com/player_ek_9366930_2_1.html?data=mpijmJ6XdI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRe4zVhqigh6eVs9SfxcqYy9LUuc%2FdxcbRjcrSb8TV1NTgjcnJb8bexsjixc7TssbnjMrl1tfFrtbYysiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jaqueline Castillo] 

###### [12 Nov 2015 ] 

[Luego de 7 años de las **ejecuciones extrajudiciales** de cerca de 20 jóvenes habitantes de Soacha y Ciudad Bolívar, madres y familiares de 6 de ellos **continúan esperando las audiencias de imputación de cargos** por los procesos judiciales que cursan contra los **militares responsables** de la comisión de este delito, varios de los cuales están libres.   ]

[De acuerdo con Jaqueline Castillo, hermana de uno de los jóvenes ejecutados, las **audiencias citadas para esta semana fueron suspendidas pues no asistieron ni los militares implicados ni sus abogados defensores**. De acuerdo con la información suministrada por el juez encargado de los 6 casos, las audiencias se llevarían a cabo hasta febrero del próximo año.]

[En estos años de espera y dilaciones las madres se han tenido que enfrentar con **amenazas, falta de acompañamiento e incumplimiento de promesas por parte del Estado Colombiano**. Según Castillo, el Gobierno se había comprometido a dar gratuitamente la libreta militar a los hermanos de estos jóvenes, pero a la fecha no han dado una respuesta efectiva.  ]

[Pese a que las madres de los jóvenes temen por sus vidas anuncian que no cesaran en su búsqueda de verdad y justicia pues en el actual proceso de paz "**el primero que debería dar ejemplo es el Estado colombiano reconociendo todos los crímenes que ha cometido**", concluye Castillo. ]
