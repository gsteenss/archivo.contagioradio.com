Title: Actividad petrolera ya había contaminado 49 aljibes y 58 pozos de agua en Acacias, Meta
Date: 2017-04-12 20:04
Category: Ambiente, Voces de la Tierra
Tags: Ecopetrol, Guamal, Meta
Slug: actividad_petrolera_habia_contaminado_pozos_aljibes_ecopetrol
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/marta-chivata-700x467.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las 2 Orillas 

###### [12 Abr 2017] 

La actividad petrolera en el departamento del Meta y sus impactos ambientales y sociales hoy prenden las alarmas de varios de los habitantes de Guamal que buscan impedir la exploración petrolera de la empresa Ecopetrol. Un asunto que no es nuevo, y del cual se tiene documentación que sostiene las preocupaciones de las comunidades, como se evidencia con un estudio del **Observatorio de Conflictos Ambientales de la Universidad Nacional.**

El análisis publicado en noviembre del 2016, se realiza sobre las posibles consecuencias que pueda generar esta actividad en la plataforma Lorito 1, ubicado en territorios de los municipios de Castilla La Nueva, Guamal y del Centro Poblado Turístico de Humadea. Además demuestra como ejemplo **el caso de Acacias**, también ubicado en el mismo departamento, donde las afectaciones ambientales y sociales son evidentes, a causa de la operación de **las estaciones Castilla-Chichimene, del bloque petrolero Cubarral y el bloque CPO9, ambos también propiedad Ecopetrol.   **

Allí se explica que Cormacarena impidió la exploración de crudo en Lorito 1, teniendo en cuenta los riesgos que podría implicar para **el abastecimiento de agua del municipio de Castilla la Nueva,** pues en caso de que se presentara algún tipo de derrame de crudo el agua que consume la comunidad de esa zona se contaminaría. Además se amenaza una de las **principales actividades económicas de la comunidad como es el turismo**, pues se correr el riesgo de que el río Humedea, que es visitado por más de 22.000 personas al año pueda ser eventualmente contaminado por crudo. [(Le puede interesar: La polémica detrás del proyecto petrolero en Guamal, Meta)](https://archivo.contagioradio.com/la-polemica-detras-del-proyecto-ecopetrol-guamal-meta/)

En ese sentido el Observatorio de Conflictos Ambientales, se refiere al caso de Acacias, donde son visibles los daños a las fuentes hídricas debido a la explotación de hidrocarburos. Allí la exploración y explotación de crudo afectó las actividades económicas históricas de las cuales se sostenía la población.

En el caso del agua, la falta de acueductos aumentó los riesgos de desabastecimiento para la población que consume agua directamente de los aljibes, pues en medio del desarrollo del proyecto petrolero,  **49 aljibes resultaron contaminados sumado a otros 58 pozos de agua, dejando a 60 familias de la vereda La Esmeralda** sin el líquido vital. Esto obligó a Ecopetrol a suministrar agua a través de carros cisterna, pese a que la empresa se había comprometido a construir un acueducto, que aún no ve la comunidad, como asegura el estudio de la Universidad Nacional.

El análisis también se refiere a los beneficios que hace ver Ecopetrol sobre la generación de empleo. Frente a ese punto señala que en 2006 la empresa cambió maquinaria utilizada en la perforación de los pozos por nuevos aparatos que no requieren personal para su manejo, de manera que **el número de personas contratadas por la empresa ha disminuido.**

Finalmente el estudio menciona que si bien el proyecto petrolero en Acacias fue socializado e incluso la empresa adquirió ciertos compromisos, estos nunca fueron cumplidos por Ecopetrol, como en el caso de la construcción del acueducto ya nombrado.

El observatorio, concluye su análisis con las advertencias que también se han hecho desde entidades como el Instituto de Investigación de Recursos Biológicos Alexander Von Humboldt,  que menciona que muchos proyectos agroindustriales y mineroenergéticos no han sido planificados de la mejor manera, por lo que es imperante que las autoridades ambientales presenten mayor atención, pues en **lugares ecológicamente estratégicos como el pie de monte llanero se están presentando unas transformaciones irreversibles** de las cuales aún se desconoce las reales afectaciones sobre el ambiente y las comunidades. [(Le pude interesar: Comunidades de 8 municipios del Meta rechazan proyecto de Ecopetrol)](https://archivo.contagioradio.com/comunidades-8-municipios-del-meta-rechazan-proyecto-petrolero-ecopetrol/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
