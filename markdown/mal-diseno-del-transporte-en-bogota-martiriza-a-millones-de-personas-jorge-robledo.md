Title: Mal diseño del transporte en Bogotá “martiriza a millones de personas”: Jorge Robledo
Date: 2016-02-05 11:06
Category: Nacional, Política
Tags: Jorge Robledo
Slug: mal-diseno-del-transporte-en-bogota-martiriza-a-millones-de-personas-jorge-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Jorge-Robledo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Revista El Crisol 

###### [5 Feb 2015]

A propósito del día sin carro, el senador Jorge Enrique Robledo envió una carta abierta sobre el metro al alcalde de Bogotá Enrique Peñalosa en la que expresó que la idea del “mejor transporte del mundo en desarrollo”, promulgada por alcalde durante su posesión, significa que no será como el de países desarrollados sino de baja categoría. Para Peñalosa esto representa disminución en los costos y más oportunidades de construir otras líneas de Transmilenio.

**Además, hace referencia al irreparable daño ambiental que sufriría la ciudad en caso de construirse el metro elevado y rechaza su privatización** a manos de la empresa Transmilenio por el incremento de costos en el pasaje que ello representaría y porque el metro quedaría relegado a ser un alimentador más del sistema de buses al contar con estaciones cada tres kilómetros y no cada uno.

Robledo hace énfasis en que lo peor de la propuesta de Peñalosa es que no cuenta con los estudios correspondientes que la sustenten, a pesar de ser el tema del transporte público un asunto de tanta complejidad técnica y un derecho de todos los bogotanos.

Según el senador el mal diseño del actual sistema martiriza a millones de personas hacinadas diariamente en los buses que recorren las diferentes vías de la ciudad, lo cual demuestra que Transmilenio no hace lo mismo que un metro y que es inconcebible tirar a la basura los diseños y estudios de las anteriores alcaldías.

Finalmente, **el senador reitera que el metro de Bogotá debe ser de primera categoría, respaldado por los estudios** que ya existen e invita al actual alcalde a apoyarlo y a unirse con los bogotanos en la exigencia al gobierno nacional para que aporte los recursos que requiera su construcción sin afectar las finanzas de la ciudad.

*Bogotá D.C., 29 de enero de 2016*

*Doctor*

*ENRIQUE PEÑALOSA LONDOÑO*

***Alcalde Mayor de Bogotá***

*Ciudad*

***Ref.:    Carta abierta sobre el metro de Bogotá***

*Atento saludo,*

*Con esta le hago respetuosos comentarios técnicos sobre el transporte público en Bogotá, porque las cosas están muy mal pero van camino de empeorarse.*

*El día de su posesión, Doctor Peñalosa, usted señaló que Bogotá tendrá “el mejor transporte público del mundo en desarrollo”, teoría que no puedo compartir. Porque “del mundo en desarrollo” significa que no será como el de los países desarrollados, es decir, del mayor nivel técnico y confort, sino uno de inferior calidad, de tercera o peor categoría. ¿Su único argumento? Que así será más barato y que, al invertir la capital menos en el metro, podrá construir otras líneas de Transmilenio. Pero ese argumento no resiste análisis, en razón de que hay cosas que han de hacerse bien desde el principio, así cuesten más. ¿Sería aceptable que, por ahorrarse una plata, la torre de control de El Dorado aumentara los riesgos de los aviones? ¿Las estructuras de los edificios bogotanos deben ofrecer menos garantías que los de Nueva York? ¿Se equivocan los pobres que autoconstruyen sus viviendas por etapas cuando, desde el principio, les hacen cimientos para varios pisos?*

*La mediocridad del metro de su propuesta no ofrece dudas. Usted ha dicho que será aéreo, pero ello le provocaría un irreparable daño ambiental a la ciudad, dado que la Caracas y demás vías no poseen las dimensiones mínimas necesarias. Tampoco acierta en que en la Caracas tendrá “estaciones cada tres kilómetros, y no cada uno, como suele hacerse”. Y peor resulta que defienda las estaciones escasas aduciendo que son para que “la persona se baje al primer piso, y siga en Transmilenio” (*[*http://bit.ly/1PKc1pY*](http://bit.ly/1PKc1pY)*). Sería a mi juicio el peor metro del mundo, concebido como un alimentador de un sistema de buses.*

*También dijo que “Transmilenio hace lo mismo que un metro”. Pero ello no es así. Como los metros le crean otro piso a la ciudad, no les quitan las vías a los otros medios de transporte ni congestionan las intersecciones, los pasajeros disfrutan de estaciones más cómodas y se suben y bajan del vagón más rápido que de los buses, los trenes pueden desplazarse con mayor velocidad y, sobre todo, nada puede igualar ni superar su capacidad de carga en los corredores de mayor congestión. El doloroso hacinamiento del Transmilenio de la Caracas demuestra que haber cambiado en esa vía el metro por el Transmilenio fue la peor decisión técnica de la historia de Bogotá. Y su ocurrencia de convertirlo en esa ruta en un Transmilenio “Lamborgini o Ferrari” (¿!), que puede generar un desastre ambiental, también muestra la encartada en la que quedó Bogotá.*

*Pero lo peor de su propuesta es que usted reconoce que “no hay ningún estudio que la sustente”, como tampoco lo tiene para tirar a la basura los diseños que su administración heredó de las anteriores alcaldías, los cuales costaron años de esfuerzos de los mejores especialistas nacionales y extranjeros. ¿Un asunto de tanta complejidad técnica, que tanto afectará a Bogotá por décadas y hasta siglos, decidido sin estudios, sin la precisión de las cifras ni los análisis en detalle? No. Así no debe ser. Con franqueza me toca decirle que no es responsable. Mala idea decidir otra vez con “un 90 por ciento de inspiración y un 10 por ciento de técnica”, como dice un alto colaborador suyo que decidió sobre la creación del sistema Transmilenio (*[*http://bit.ly/1NFXwfh*](http://bit.ly/1NFXwfh)*).*

*El transporte público es demasiado importante para tratarlo a punta de ocurrencias o con estrechez de miras. Porque su mal diseño martiriza a millones de personas y además imposibilita la correcta organización del conjunto urbano. No cabe la idea de una buena ciudad con un mal sistema público de transporte. Y se sabe que no pueden resolverse los complejos problemas de la movilidad sobre la base de vehículos particulares. ¿Pero alguien va a dejar su carro o su moto para desplazarse en un metro incómodo y además costoso, porque usted también ha dicho que lo operará Transmilenio, es decir, que será privatizado, con ganancias que salen de lesionar la comodidad de los ciudadanos?*

*Frente a los costos del metro de Bogotá –que debe ser el de primera categoría que respaldan los estudios y diseños que ya existen–, cordialmente, lo invito a apoyarlo y a unirse con los bogotanos, sin distingos, en la exigencia al gobierno del presidente Juan Manuel Santos para que aporte los recursos que requiera su construcción, sin maltratar las finanzas de la ciudad. Los enormes aportes de los bogotanos al total de los impuestos nacionales les dan el derecho a disfrutar de un metro de país desarrollado, así como a beneficiarse de otros modos de transporte de primer nivel.*

*Cordialmente,*

* *
