Title: La estrategia del uribismo que busca lavar la imagen de Andrés Felipe Arias
Date: 2019-07-16 13:16
Author: CtgAdm
Category: Judicial, Política
Tags: agro ingreso seguro, Andés Felipe Arias, Centro Democrático, Doble Instancia
Slug: uribismo-andres-felipe-arias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Andrés-Felipe-Arias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Uniminuto Radio  
] 

[El pasado jueves 11 de julio llegó a Colombia **Andrés Felipe Arias, extraditado desde Estados Unidos para que responda ante la justicia colombiana por el delito de peculado en el programa de Agro Ingreso Seguro.** El caso de Arias llamó la atención de la opinión pública porque el trato que se le ha dado ha sido preferencial: no fue recluido en centro penitenciario, llegó en un vuelo chárter y su defensa es el Centro Democrático, encabezado por el senador Álvaro Uribe Vélez.]

[Sobre este último hecho, la defensa desde un sector político como el uribismo en favor de Arias, puede radicar la diferencia en el trato que ha recibido él respecto a otros extraditados desde EE.UU., como David Murcia Guzmán. Para el **analista político Sergio Fernández,** los argumentos principales con los que se buscan exculpar a Arias son que no se robó un peso, y que es víctima de persecución política, pero ambas cosas son mentira. (Le puede interesar: ["Andrés F. Arias debe ir a un centro carcelario si es extraditado a Colombia"](https://archivo.contagioradio.com/andres-felipe-arias-como-cualquier-colombiano-condenado-por-corrupcion/))  
]

> Al normalizar el saqueo de AIS, el Uribismo legitima con descaro al hampón Arias y lo hace pasar por héroe y perseguido.  
> Es decir, su actuación es digna de repetir para q haya más héroes. Brutal lavada de cerebro le están pegando a la masa obediente.[\#UribeEsElCáncerDeColombia](https://twitter.com/hashtag/UribeEsElC%C3%A1ncerDeColombia?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/iWptN31fBP](https://t.co/iWptN31fBP)
>
> — Gustavo Bolívar (@GustavoBolivar) [July 15, 2019](https://twitter.com/GustavoBolivar/status/1150564611252510720?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **¿Andrés Felipe Arias no se robó un peso?** 

[Fernández recordó que el programa **Agro Ingreso Seguro (AIS)** nació como una política pública para permitir que el agro colombiano enfrentara los Tratados de Libre Comercio con EE.UU. y la Unión Europea, mediante la entrega de subsidios, y créditos blandos (es decir, créditos con bajas tasas de interés). Sin embargo, dicha política no funcionó porque mientras **AIS tenía recursos por 250 millones de dólares,** en Norteamérica se destinaban 50 mil millones de dólares y en la U.E. 25 mil más que en Estados Unidos para el mismo propósito.]

[Más allá de la idoneidad de la política, el exministro de Agricultura durante el primer gobierno de Uribe Vélez fue declarado culpable por peculado, que como lo explicó el analista, "es cojer y robar plata en función de intereses particulares; lo que se demostró y está probado judicialmente es que él (Arias) tomó recursos públicos para favorecer a sus amigos". Entre los beneficiados por estos recursos figuran Sarmiento Angulo, la familia Iragorri, y empresas como Algarra; compañías y personas que fueron financiadoras de la campaña presidencial de Uribe, así como de Arias en su momento.]

### **Un perseguido político... por integrantes del mismo Centro Democrático** 

[El segundo argumento del uribismo para cuestionar la condena hacía Arias señala que **es un perseguido político y no tuvo las instancias debidas para defenderse.** Sobre el primer punto de la premisa, Fernández recordó la línea del tiempo del caso, que involucra a Arias y dos actuales funcionarios del gobierno Duque: Vivian Morales, embajadora en Francia; y Alejandro Ordóñez, embajador ante la Organización de Estados Americanos (OEA). (Le puede interesar:["Andrés F. Arias y el escándalo de corrupción de Agro Ingreso Seguro"](https://archivo.contagioradio.com/en-estados-unidos-se-definira-situacion-de-andres-felipe-arias/))  
]

[El escándalo de AIS se destapó en 2006; en 2009 volvió a tomar fuerza y las pruebas en contra de Arias llevaron a que entre **2010 y 2011 fuera sancionado por el entonces procurador general de la nación, Alejandro Ordóñez.** Posteriormente, la fiscalía de Vivian Morales abrió investigación en su contra y en 2015, la Corte Suprema de Justicia lo condenó a pagar una multa de 16 millones de dólares y 17 años de cárcel. Como concluyó el Integrante del Polo, "era tan corrupto que era inocultable".]

[Ante la sanción, Arias decidió escapar hacia Estados Unidos y solicitó asilo político, alegando que era un perseguido; pero en 2017, un juez federal determinó que no es así, y ordenó su extradición; el proceso pasó entonces a través del Secretario de Estado de la administración Trump, que dejó en firme la decisión. El resultado de este proceso tiene a Arias en Colombia desde el pasado jueves, y al Centro Democrático proponiendo la Ley Andrés Felipe Arias, para que se establezca la doble instancia en casos de aforados, incluyendo aquellos que ya fueron juzgados en el pasado.]

### **¿Qués la doble instancia y por qué recobra importancia en el caso de Andrés Felipe Arias?**

[Los aforados son funcionarios o exfuncionarios públicos que por las responsabilidades que asumen en su trabajo con el Estado tiene garantías especiales de juzgamiento; ese es el caso de ministros, congresistas, presidentes, expresidentes, entre otros. La instancia encargada de juzgar a los altos funcionarios, o aforados, es la Corte Suprema de Justicia; que es un tribunal compuesto por varios magistrados y tiene su propio órgano de investigación, razón por la que no se precisa una segunda instancia.]

[No obstante Arias fue juzgado y encontrado culpable en el marco de un proceso que se considera acorde a los derechos que tiene como acusado, el Centro Democrático anunció que presentaría una Ley para crear la doble instancia para aforados; **cuya función sería revisar los casos de todos los que fueron juzgados por la Corte Suprema en una sola instancia**. Muchos han señalado que el nombre propio de esta Ley es Andrés Felipe Arias.]

### **En el Congreso habría ambiente para aprobar la Ley  
**

[Lo que no han señalado, y puso de presente Fernández, es que de aprobarse esta Ley, todos los que han sido condenados por la Corte Suprema podrían buscar esta segunda instancia; **haciendo que procesos juzgados, como el de Antonio Guerra** (hermano de Maria del Rosario Guerra, culpable en el marco del caso por Odebrecht y acusado de tener vínculos con el paramilitarismo) revivan, y regresen nuevamente a etapa de juicio. (Le puede interesar: ["Los dudosos entornos del Centro Democrático"](https://archivo.contagioradio.com/dudosos-entornos-del-centro-democratico/))  
]

[En ese sentido, el Analista dijo estar preocupado ante el apoyo que pueda tener en el Congreso este Proyecto, pues la Corte Suprema ha condenado a decenas de altos funcionarios de casi todos los partidos con una sola instancia en casos que van desde el recordado proceso 8 mil, pasando por la 'para política', hasta el caso Odebrecht. Así las cosas, la Ley Andrés Felipe Arias podría ser la Ley que cobije a muchos militantes de partidos políticos con poder que quieran buscar una resolución diferente a su caso.]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38462921" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38462921_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
