Title: Huber Ballesteros en libertad
Date: 2017-01-13 16:22
Category: Paz
Tags: Huber Ballesteros, Marcha Patrótica
Slug: huber-ballesteros-en-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/ballesteros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [13 Enero 2017] 

En las próximas horas,  Huber Ballesteros, dirigente agrario de Marcha Patriótica **será liberado, luego de más de tres años de estar en la cárcel,** después de que el Juzgado 24 de control de garantías estableciera que “de acuerdo a la nueva realidad política se produjo decaimiento de los fines constitucionales para mantenerlo privado de la libertad y se tomó como referencia la ley 1820 “por medio de las cuales **se dictan disposiciones sobre amnistías e indultos y tratamientos penales especiales y otras disposiciones**”.

Además el juez concluyo que **no existen fundamentos constitucionales en el contexto actual para mantenerlo privado de la libertad** y en consecuencia ordeno su libertad inmediata. De igual forma, durante la audiencia se dieron a conocer los **nombres de más de 300 líderes sociales**, que formalmente las FARC-EP relacionaron, para aplicársele el artículo 35 y el tratamiento penal diferenciado. Le puede interesar:["Más de 100 organizaciones en el mundo piden aplicación de amnistías a líderes sociales"](https://archivo.contagioradio.com/amnistia-a-lideres-sociales-colombia/)

En el momento de su captura, **Ballesteros se encontraba haciendo parte de las actividades de coordinación del Paro agrario que se desarrollaba en agosto 23**, para ese momento los campesinos estaban exigiengo el mejoramiento de las condiciones de vida y las garantías de trabajo de quienes producen los alimentos que llegan a diario a la mesa de los colombianos.

Ballesteros es dirigente sindical, miembro del comité ejecutivo de la Central Unitaria de Trabajadores (CUT), vicepresidente de la Federación Nacional Sindical Unitaria Agropecuaria (Fensuagro), vocero de la Mesa de Interlocución y Acuerdo (MIA), e integrante de la Junta Patriótica Nacional de nuestra organización.
