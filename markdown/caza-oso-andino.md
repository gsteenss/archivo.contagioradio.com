Title: La pérdida de su territorio y la caza: Los mayores riesgos del Oso Andino
Date: 2018-08-30 16:59
Category: Ambiente, Nacional
Tags: Ambiente, Caza, Oso Andino, oso de anteojos
Slug: caza-oso-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/MANU-PARK-PERU-CLOUD-FOREST.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  MANU-PARK-PERU-CLOUD-FOREST] 

###### [30 Ago 2018] 

Tras el asesinato y descuartizamiento de un Oso Andino a manos de un indígena Uwa, en inmediaciones del municipio de **Saravena, Arauca**; **Daniel Rodríguez**, biólogo y representante legal de la Fundación WII, asegura que en el país no existe un estudio definitivo sobre la cantidad de individuos de esta especie, ni se han tomado las medidas adecuadas para proteger a esta especie, declara en estado de vulneración.

Aunque el indígena justificó sus acciones asegurando que el Oso iba a ser destinado para el consumo, Rodríguez, afirma que esta afirmación es sospechosa porque **"para casi todas las comunidades indígenas latinoamericanas pre hispánicas, el Oso tenía una posición muy importante en su cosmogonía y en sus mitos"**, es decir, es una especie muy respetada.

De hecho, el experto afirmó que no estaba registrado que un miembro de la comunidad Uwa  matara un oso para comer, ya que el único pueblo que en Colombia consumía esta especie, eran los Yukpa de la Serranía del Perijá, en la frontera Colombo-Venezolana; sin embargo, su consumo era puramente ritual y ocurría una vez al año en el marco de su Fiesta del Maíz.

### **"La situación de los Osos Andinos en Colombia es preocupante"** 

Para Rodríguez, la problemática de conservación del Oso es muy grande y está asociada a las condiciones de abandono de las poblaciones indígenas y campesinas que habitan las zonas en las subsisten estas especies. Esto, porque **las principales amenazas para su vida son la pérdida de su hábitat y su cacería por parte de los campesinos en retaliación por comerse sus animales domésticos.**

El experto afirmó que la pérdida de su hábitat está relacionada con la falta de definición de la frontera agrícola, y el uso de la tierra para la siembra de cultivos de uso ilícito; por esa razón, es trascendental hacer pedagogía en l0s **75 territorios** que hacen parte de su hábitat, y ofrecer alternativas de desarrollo económico para campesinos e indígenas.

En cuanto al censo de la especie, el biólogo aseguró que **no hay "datos sobre cuantos animales nos quedan en el país"**, siendo el único estudio sobre densidad de su población el elaborado en conjunto con la empresa de Acueducto y Alcantarillado de Bogotá para el macizo de Chingaza. Gracias a esa investigación, que consistió en la instalación de 120 cámaras al rededor del parque natural para evaluar las zonas de amortiguación en áreas protegidas, se pudo calcular que en esa región hay "**a lo sumo entre 120 y 160 animales".**

Rodríguez precisa con preocupación que según datos del estudio, la población de Chingaza esta compuesta mayoritariamente por individuos machos, y es en general adulta, lo que significa que es una población en riesgo. Y añadió que "haciendo una extrapolación atrevida, de los cálculos obtenidos en Chingaza al nivel nacional, **en Colombia podría haber entre 9 mil y 12 mil animales".**

### **La pérdida de un solo oso afecta enormemente la población de una región** 

El experto afirmó que la muerte de un sólo oso afecta a toda la población en la región porque las tazas de crecimiento de la especie son lentas, "las hembras están teniendo dos oseznos por camada, a diferencia de otras especies que llegan a tener hasta 4, y la diferencia entre una camada y otra ronda los 3 años".

Es posible encontrar al Oso Andino en Colombia, Venezuela, Ecuador, Perú, Bolivia, y la región norte de Argentina; y conforme a los estudios hechos por Rodríguez, el mamífero es un **"jardinero del bosque"**, es decir que es capaz de regular la entrada de luz a las partes bajas de este biosistema, permitiendo que las plantas bajas crezcan y la flora se renueve. Adicionalmente, es un "efectivo dispersor de semillas", logrando controlar el Bosque, que es el ecosistema encargado de regular el agua.

Aunque recientemente se ha incrementado la visibilidad del oso en la agenda pública, situandolo como una especie vulnerable y que necesita protección especial, para Rodríguez esto fue producto de la presión ciudadana para que así fuera, e igualmente debería ocurrir con las Dantas de Montaña, el Manatí Antillano y los Jaguares. (Le puede interesar: ["Sujetos apuñalaron a oso homiguero en Flandes, Tolima"](https://archivo.contagioradio.com/sujetos-apunalaron-a-oso-hormiguero-en-flandes-tolima/))

<iframe id="audio_28222509" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28222509_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
