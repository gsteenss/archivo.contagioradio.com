Title: ?️ - Carlos Pizarro Leongómez, 29 años Sin Olvido
Date: 2019-04-26 13:44
Author: CtgAdm
Category: Sin Olvido
Tags: Carlos Pizarro Leongómez, memoria, Sin Olvido
Slug: carlos-pizarro-leongomez-29-anos-sin-olvido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Carlos-Pizarro-Leongómez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

> A 29 años del asesinato de **Carlos Pizarro Leongómez**, revivimos junto a su hija María José, el legado de un hombre que apostó por la paz a costa de su propia vida. \#SinOlvido
>
> <iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fsinolvidoesmemoria%2Fvideos%2F2351555371756686%2F&amp;width=800&amp;show_text=false&amp;appId=894195857389402&amp;height=411" width="800" height="411" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
