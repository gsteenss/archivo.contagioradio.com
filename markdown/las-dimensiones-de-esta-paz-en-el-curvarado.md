Title: Las dimensiones de esta paz en el Curvaradó
Date: 2016-08-01 08:22
Category: Abilio, Opinion
Tags: Curvarado, Zidres
Slug: las-dimensiones-de-esta-paz-en-el-curvarado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/curvarado-e1473650667277.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión Justicia y Paz 

#### **[Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/) -[@[Abiliopena]

###### [31 Jul 2016]

Justo en estos días en Cármen del Darién por convocatoria del Ministerio del Interior,  se está aprobando un reglamento interno para el uso de los territorios  y se elegirá al representante legal de la la cuenca del Curvaradó. El gobierno niega el derecho a participar con voz y voto a afromestizos, en una interpretación estrecha de los autos de la Corte Constitucional que le ordenaron acompañar ese proceso.

El reglamento también fue  redactado por el Ministerio de Interior, como lo testimonian líderes comunitarios. Aunque exhibe actas de reuniones donde supuestamente fue discutido,  son solo la formalidad de la imposición de un modo empresarial de concebir  el uso del territorio, excluyendo de las decisiones a los cientos de hombres y mujeres que fueron víctimas de la palma aceitera implantada por el gobierno, de la mano de paramilitares como Vicente Castaño, Sor Teresa Gómez, entre otros. Como constancia, las comunidades afromestizas que habitan el territorio guardan las cerca de 40 páginas de su propio reglamento que del todo fue desconocido por el gobierno y decenas de comunicaciones públicas dirigidas al propio gobierno en la que clamaban que se revisara el proceso que formalizaba su exclusión en un país supuestamente democrático.

El grueso del   texto a aprobar que será “transitorio” pero que en los “micos” de la redacción jurídica  aclara que será incluido en el   reglamento final, se ocupa de la extracción de madera, de la extracción minera y del la implementación de agronegocios en el territorio. Este énfasis  encaja con la ley Zonas de Interés de  Desarrollo Rural Económico y Social -Zidres- en la que se pueden implementar dichos proyectos en territorios colectivos de comunidades negras cuando lo aprueba el Ministerio del Interior y lo  solicita el representante legal del consejo comunitario . ¿Será que el Ministerio de Interior desaprobará estos proyectos? ¿Será que el representante elegido por mayorías que no habitan los territorios no los solicitará?

En el territorio conviven ya proyectos agroindustriales impulsados por Unibán, Banacol, Delmonte, algunas de ellas que hicieron acuerdos con Vicente Castaño y Sor Teresa para instalarse a través de Asoprobeba. Así mismo está instalada toda la infraestructura de drenajes  que dirigió el empresario condenado por concierto para delinquir  Irving Bernal, quien manifestó en una de sus versiones ante jueces de la república que aspiraba recuperar su inversión  una vez las tierras fueran entregadas “a las comunidades negras” y se venciera el principal obstáculo que según él representa la Comisión de Justicia y Paz. Con el mismo tono circuló una “alerta” dos días atrás en contra de Justicia y Paz (<http://justiciaypazcolombia.com/La-mentira-como-medio-para-asegurar-la-ilegalidad>).

Y sobre la recuperación de su  inversión el paraempresario Bernal aseveró en sus declaraciones: “Yo tendría la esperanza de que si algún día le entregan las tierras a las comunidades afros ese valor que hay en infraestructura ya sea ellos o el gobierno, me puedan dar alguna fórmula de como descargar el valor contable y fiscal que tengo allí”. (<http://justiciaypazcolombia.com/A-quien-devolveran-las-tierras-en>).

Pareciera que el momento les ha llegado. Ellos,  empresarios beneficiarios del paramilitarismo, de las políticas agrarias del gobierno de Uribe, pueden encontrar en el gobierno de la paz, el de las víctimas, el del presidente Santos, la concreción de sus sueños de recuperar su  inversión y de seguir pujando por un modelo de producción que aprecia de las comunidades sólo su tierra, que ve solo recursos a explotar,  que se  vale del paramilitarismo, que controla todas las cadenas de valor y que desprecia la economía campesina.

El punto uno del acuerdo  entre el gobierno y las Farc-Ep,  donde  se consigna la contradicción de las partes entre soberanía alimentaria y seguridad alimentaria,  se resuelve en la práctica por el apabullamiento gubernamental a los habitantes del territorios que creen en un desarrollo que garantice  la protección del medio ambiente, en la participación  real  en toda la cadena de valor, que considera las alianzas sobre la base de de una inversión similar con los otros inversionistas y que su músculo de inversión debe ser proporcionado por el Estado.

Con decisiones excluyentes como las que se tomarán en la “aprobación” del reglamento y la “elección” de representante legal, poca o ninguna cabida tendrá el acuerdo de Desarrollo Agrario Integral de la Habana, pues el impulso a la economía familiar, campesina, el desarrollo de vías, la constitución de centros de acopio, conceptos como el del “buen vivir”, el  ordenamiento socioambiental sostenible con enfoque étnico territorial,  el apoyo a la producción agropecuaria y a la economía solidaria y cooperativa,  el mercadeo, los seguros de cosecha, la oposición al uso de semillas transgénicas, quedarán supeditadas a los mandatos  de las empresas que como Unibán, Banacol, entre otras, que  ven en el Curvaradó la posibilidad de expandir la frontera agrícola.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
