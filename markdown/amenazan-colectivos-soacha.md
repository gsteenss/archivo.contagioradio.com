Title: Armados amenazan a colectivos ambientales de Soacha
Date: 2019-03-12 17:09
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: amenaza, Bogotá, Cundinamarca, soacha
Slug: amenazan-colectivos-soacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Caminantes-de-Soacha-Amenazados-e1552428556308.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Caminando el Territorio] 

###### [12 Mar 2019] 

"Caminando el territorio", colectivo ambientalista de Soacha, Cundinamarca, denunció que mientras realizaban un recorrido por uno de los cerros principales del Municipio, dos de sus miembros fueron abordados por tres hombres armados que los amenazaron y golpearon. Según Gabriel Romero, también integrante de Caminando el Territorio, este sería un acto perpetrado por los grupos armados que operan en el territorio.

De acuerdo al relato de Romero, el pasado domingo el colectivo realizó una caminata en la que participaron el Cabildo Muisca de Bosa, un grupo de Boys Scout y cerca de 45 personas, con la que querían hacer una jornada de limpieza al Cerro Chebá, en la vereda Panamá de Soacha, que había sido afectado por un incendio forestal "que al parecer fue provocado".

Mientras realizaban esta caminata, dos de los integrantes del colectivo fueron emboscados por tres hombres armados que los golpearon y constantemente amenazaron con asesinarlos, situación ante la que uno de los agredidos reaccionó intentando huir del lugar, fracturándose la clavícula y el tobillo. Por su parte, el otro agredido sufrió golpes causados por los armados.

Romero sostiene que el sentido de la agresión fue unicamente amenazar la actividad del Colectivo, pues las pertenencias de ambos agredidos no fueron hurtadas; afortunadamente, ambas personas fueron atendidas diligentemente por quienes acompañaban la marcha y recibieron la atención médica pertinente. (Le puede interesar: ["Así se estarían reclutando jóvenes en Bogotá"](https://archivo.contagioradio.com/asi-se-estarian-reclutando-jovenes-en-bogota/))

### **¿Quiénes amenazan el ejercicio de territorialidad en Soacha?** 

La Defensoría del Pueblo ha emitido diferentes alertas tempranas sobre la situación de derechos humanos en el Municipio vecino a Bogotá, y allí ha señalado la posible presencia de diferentes grupos armados entre los que se encuentran el ELN, las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y Los Rastrojos. Allí también han expuesto la relación de estos grupos con economías ilegales, especialmente de narcotráfico.

El Integrante de Caminando el Territorio recuerda que la población de Soacha conoce en general la presencia de estructuras criminales relacionadas con minería ilegal, loteo ilegal y 'deshuasadero' de carros, al punto que se cree en la existencia de casas de pique. Estas estructuras, dados los requerimientos de este tipo de negocios, serían de tipo paramilitar, y para Romero, estarían detrás de la agresión sufrida por sus dos compañeros.

Ante los hechos, las autoridades del Municipio se pusieron en comunicación con el colectivo para indicar la ruta de acción que deben emprender para denunciar esta agresión. (Le puede interesar:["Atentan contra líder social de Aheramigua en Bogotá"](https://archivo.contagioradio.com/atentan-contra-la-vida-de-un-lider-social-en-bogota/))

<iframe id="audio_33319781" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33319781_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
