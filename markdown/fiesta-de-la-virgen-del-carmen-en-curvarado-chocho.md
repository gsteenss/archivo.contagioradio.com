Title: Fiesta de la Virgen del Carmen en Curvaradó, Chochó
Date: 2015-01-06 15:42
Author: CtgAdm
Category: Comunidad
Slug: fiesta-de-la-virgen-del-carmen-en-curvarado-chocho
Status: published

**Fotografía**: Comisión Intereclesial de Justicia y Paz

 El 16 de julio se llevó a cabo la fiesta de la Virgen del Carmen en la comunidad las Camelias, fecha que ancestralmente celebran las comunidades del Curvarado en el departamento del Chocó.  
Durante la celebración se hizo curso pre-baustismal con los padres y padrinos de los niños y niñas que fueron bautizadas, se celebró la santa misa, y se realizó una oración por las víctimas del conflicto armado del territorio. También se llevó a cabo una procesión que se empezó en el Puerto de Brisas hasta los límites de la comunidad las Camelias. Durante el recorrido se hizo una oración en la finca del señor Jose del Carmen Villalba, en donde se encuentran los cuerpos de Isaac Tuberquia y Julio Mendosa, asesinados por paramilitares en conjunto con el Ejército Nacional en el año 1997.

Como es costumbre la celebración culminó con una fiesta en la cual se recogen fondos para el funcionamiento del Comité de Cultura del Concejo Comunitario de las Camelias.

[Descargar Audio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/fsckWECq.mp3)
