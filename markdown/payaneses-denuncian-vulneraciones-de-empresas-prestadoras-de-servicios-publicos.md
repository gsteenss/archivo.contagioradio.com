Title: Empresas de servicios públicos atentan contra usuarios de Popayán
Date: 2015-05-22 14:11
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Acueducto Municipal de Popayán, Agua, Asociación de Usuarios de Servicios Públicos del Cauca, Cauca, Defensa de los servicios públicos, energía, luz, Popayá, servicios públicos, Transnacional Energética del Occidente
Slug: payaneses-denuncian-vulneraciones-de-empresas-prestadoras-de-servicios-publicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/super.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: [www.elpueblo.com.co]

<iframe src="http://www.ivoox.com/player_ek_4535483_2_1.html?data=lZqgl5mcd46ZmKiak52Jd6KpmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMLtwtPS1crXb8XZz9rbxc7Fsozq1tHbx9fFp8rjz8rgjdXTtozkwtfhx5DIqYzZztXfx9jFt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Fanny Mera, Asociación de Usuarios de Servicios Públicos del Cauca] 

La Asociación de Usuarios de Servicios Públicos del Cauca, denuncia la falta de atención de las autoridades de la ciudad para atender las **problemáticas alrededor de la prestación de servicios públicos, específicamente sobre la luz y el agua.**

Respecto al tema de la energía, Fanny Mera, quien hace parte de esa asociación, asegura que la población de Popayán se ha visto afectada por los constantes **atropellos de la Transnacional Energética del Occidente,** por lo que la comunidad se encuentra en un proceso de resistencia civil, y han decidido “no pagar, agua, luz, ni catastro, por la situación crítica que vivimos en la ciudad y en todo el departamento”, dice Mera, quien además cuenta que últimamente **el ESMAD y la policía** van a los barrios más pobres a suspender el servicio de energía para cambiar los medidores de disco por los digitales, sin tener en cuenta que los medidores de disco y las redes abiertas de luz fueron construidas con esfuerzo de la comunidad.

“La empresa quiere quitar lo de la comunidad y poner innovación tecnológica que no es otra cosa que menoscabar en los bolsillos de la población para que la empresa se enriquezca, ya que **nos van a cobrar más**”, expresa la integrante de la asociación, quien cuenta que a las personas a quienes ya les impusieron los nuevos medidores, la factura de la luz les ha llegado hasta por 300 mil pesos. Las autoridades no se han hecho cargo del proceso de descontaminación.

Por otro lado, de acuerdo a la integrante en defensa de los servicios públicos, **el derecho al agua está próximo a su privatización,** y además las autoridades han permitido que las fuentes hídricas estén contaminándose debido a que el Acueducto Municipal de Popayán no ha querido implementar tubos diferentes para las aguas lluvias y las residuales, lo que ha generado que ambas tipos de agua lleguen a las fuentes hídricas que proveen de agua a la ciudad.

Finalmente, otra de las denuncias que hizo Fanny Mera, es sobre el botadero “El Ojito”, ubicado al occidente de la ciudad. Este ya fue clausurado por acción de la comunidad. Sin embargo, **las autoridades no han querido empezar el proceso de descontaminación,**  por lo que varias familias que viven cerca al botadero se han visto afectadas por los desagradables olores que deben soportar.

Hasta el momento la comunidad se encuentra en un proceso de resistencia ante las vulneraciones por parte de las entidades que prestan estos servicios; además, están trabajando barrio por barrio para la presentación de propuestas a la administración municipal para que se tomen cartas sobre estos asuntos.
