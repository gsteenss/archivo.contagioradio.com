Title: Catarsis: Contracultura en movimiento
Date: 2014-12-17 16:48
Author: CtgAdm
Category: Hablemos alguito
Slug: catarsis-contracultura-en-movimiento
Status: published

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsgN4mSQ.mp3)  
Catarsis es un colectivo que retoma los aprendizajes de las experiencias de la cultura contra hegemónica en la ciudad y los convierte en nuevos espacios de participación para el debate, la propuesta, la diversión y la lucha. Buscan promover con la música libertaria espacios en donde confluyan expresiones musicales-culturales-políticas. El hardcore/punk como escenario de encuentro y canalización del inconformismo hecho ruido; el hip hop como el porta voz de las clases populares y el arte urbano como arma para la libertad.  
Queremos ser otra pestaña para aportarle a la guerra ideológica, a la contrainformación y la movilidad musical y social siempre con un horizonte de poder popular, de nuevo proyecto de país y de conciencia crítica con perspectiva de justicia.
