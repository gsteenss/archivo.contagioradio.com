Title: Víctimas piden a la JEP hacer públicas declaraciones de militares comparecientes
Date: 2020-10-29 18:23
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Ejecuciones Extrajudiciales, JEP, jurisdicción especial para la paz, Militares ante la JEP, víctimas
Slug: victimas-piden-a-la-jep-hacer-publicas-declaraciones-de-militares-comparecientes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Mario-Montoya-ante-la-JEP.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este miércoles organizaciones sociales y representantes de víctimas, **solicitaron formalmente a la Jurisdicción Especial para la Paz -JEP-, hacer públicas las versiones de los miembros de la Fuerza Pública que comparecen ante ese Tribunal.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las víctimas hacer públicas las versiones, garantiza la transparencia y la posibilidad de **que el país escuche a los militares comparecientes para evaluar si su aporte está siendo efectivo para reconstruir la verdad del conflicto armado** y los crímenes en los que se vieron involucrados agentes del Estado. (Le puede interesar: [Tras la verdad, hay un sector que no quiere perder el poder del control ideológico](https://archivo.contagioradio.com/sector-teme-verdad-no-quiere-perder-poder-control-ideologico/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los solicitantes son víctimas de ejecuciones extrajudiciales y señalaron que considerando que estos crímenes fueron cometidos de forma masiva y sistemática contra civiles o personas por fuera de toda confrontación armada, las diligencias “*deben dirigirse a la sociedad en general, en razón a que con la gravedad de estas conductas no solo se agredió a las víctimas, sino a los valores y la ética de la humanidad*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para justificar esta solicitud, las víctimas acudieron a pronunciamientos de la misma Jurisdicción de Paz, en los que ha señalado que **“*los procedimientos adelantados ante la JEP son por regla general públicos y esto debe garantizarse en todas las etapas procesales,*** *entendiendo la publicidad no solo en el marco interno de este escenario de justicia sino también externo* (frente a la sociedad)”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Otra de las razones es que según las víctimas, los militares no han contribuido con la verdad a la que se comprometieron.** Según el abogado Sergio Arboleda, integrante de la [Corporación Jurídica Libertad](https://cjlibertad.org/), la cual representa a algunas de las víctimas; si bien no se desconoce que algunos militares están comprometidos, la gran mayoría usa “*una estrategia de defensa en la que se sigue culpando a los mandos medios para quitarle la responsabilidad a los altos mandos*”. (Le puede interesar: [Se instaló el mural «¿Quién dio la orden? 2.0»](https://archivo.contagioradio.com/se-instalo-el-mural-quien-dio-la-orden-2-0/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunos otros han optado por guardar silencio esperando a que los crímenes les sean probados por parte de la JEP, aun cuando esta no es la naturaleza de ese Tribunal, sino que quienes acuden a este, lo hacen con el compromiso de aportar verdad y esclarecer los hechos del conflicto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la solicitud quedó expreso que **“*el principio de publicidad de las actuaciones judiciales no puede verse como una simple formalidad procesal, sino como un presupuesto de eficacia de la función judicial y de legitimación de la democracia participativa*”.** Incluso, los solicitantes agregaron que la publicidad ha sido aplicada por la Corte Interamericana de Derechos Humanos -CIDH- y respaldada por el Comité de Derechos Humanos de la Organización de Naciones Unidas -ONU-.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Audiencias de exmiembros de FARC-EP ante la JEP son públicas**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hace poco más de un mes, Rodrigo Londoño, excomandante de la extinta guerrilla de las FARC-EP; hizo una solicitud en el mismo sentido a la JEP para  que se hicieran públicas las audiencias en las que han participado miembros del hoy Partido FARC **narrando los hechos que se registraron durante el conflicto, solicitud a la que accedió el Tribunal de Paz.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**En ese momento Londoño y otros miembros del antiguo secretariado de las FARC-EP** recalcaron la importancia de que las audiencias en las que declaraban se publicaran completas para evitar que se tomaran solo algunos apartes y luego fueran tergiversadas. (Le puede interesar: [Rodrigo Londoño se comprometió con el «Movimiento Nacional por la Verdad»](https://archivo.contagioradio.com/rodrigo-londono-se-comprometio-con-el-movimiento-nacional-por-la-verdad/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
