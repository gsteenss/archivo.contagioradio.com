Title: Deuda Argentina alcanzaría cifras históricas al finalizar 2016
Date: 2016-11-03 15:35
Category: El mundo, Otra Mirada
Tags: Argentina, deuda, fondos buitre, Gobierno Macri, Tarifazo
Slug: deuda-argentina-crisis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/tapa-2645.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Kaoz en la red 

##### 3 Nov 2016

Adportas de cumplir su primer año en el poder, el gobierno de Mauricio Macri apunta a superar cualquier registro histórico para la deuda Argentina. Cifras en rojo que **conducirían al término de 2016 a una crisis económica**, similar a la de 2001, cuando la deuda superó el 50% del Producto Interno Bruto.

Las medidas económicas adoptadas por el gobierno para regresar a los mercados internacionales del capital, entre las que se cuentan el ajuste a las tarifas de servicios públicos hasta en un 300% conocido como el "**Tarifazo**" y la negociación con los denominados "**fondos buitre**" lograron que se pagara una deuda de 9.300 millones de dólares con tenedores de bonos especulativos. Le puede interesar: [Medidas de Macri perjudican a los argentinos más pobres](https://archivo.contagioradio.com/medidas-de-macri-convulsionan-a-los-argentinos-mas-pobres/).

Con esa libertad, el Tesoro Nacional colocó títulos en moneda extranjera por unos 24.700 millones de dólares y bonos en moneda local por 231.079 millones de pesos argentinos (15.300 millones USD), así como letras en pesos y dólares por 5.800 millones, a**lcanzando en 11 meses un monto cercano a los 45.800 millones USD**; ritmo que provocará, según analistas, que se llegue a los **50 mil millones al finalizar 2016**. Le puede interesar: [Maestros argentinos se unen a movilizaciones contra los tarifazos](https://archivo.contagioradio.com/maestros-argentinos-se-unen-a-movilizaciones-contra-los-tarifazos/).

La posición de algunos medios de información en Argentina, que celebran el hecho de retornar a los mercados internacionales después de 15 años, contrasta con la preocupación de algunos economistas quienes ven **cerca la posibilidad de repetir crisis** como la mencionada de 2001, la de 1989 cuando se solicitaron varios préstamos para frenar la inflación no funcionaron y provocaron que el ambiente de incertidumbre condujera a una hiperinflación y a la fuga de capitales y lo ocurrido durante la dictadura en 1979 cuando se hicieron varias minidevaluaciones sin reducir el gasto y sin poder contener la pérdida de reservas, obligando a realizar una devaluación traumática de la moneda.

El planteamiento de los analistas, parte de que los préstamos solicitados y recibidos por el gobierno **no se gastan en planes de inversión que generen ingresos** que permitan cancelar las deudas adquiridas, y se utilicen en pagos de caja menor, reducción del deficít fiscal y aumento de las reservas internacionales; teniendo en cuenta que **Argentina es uno de los paises con los mayores niveles de gasto público de la región**, tendencia que de mantenerse va a dificultar el subsanar los compromisos con sus acreedores.
