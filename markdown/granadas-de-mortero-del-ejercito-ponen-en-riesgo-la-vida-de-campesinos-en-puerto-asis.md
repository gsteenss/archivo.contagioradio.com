Title: Granadas de mortero del Ejército ponen en riesgo la vida de campesinos en Puerto Asís
Date: 2015-07-21 21:54
Category: Comunidad, DDHH
Tags: Brigada 27 del Ejército, DDHH, DIH, Puerto Asís
Slug: granadas-de-mortero-del-ejercito-ponen-en-riesgo-la-vida-de-campesinos-en-puerto-asis
Status: published

###### 

###### [21 julio 2015]

El pasado 20 de julio, la comunidad campesina de la vereda Puerto Colombia, municipio de Puerto Asís, Putumayo, fue agredida física y psicológicamente con el lanzamiento de morteros provenientes de la Brigada 27 de Selva del Ejército Nacional. Afectación que se suma a las detenciones arbitrarias y amenazadas con tiros al aire por parte de los militares, denunciadas en un informe presentado por la Red de Derechos Humanos de Putumayo .

De acuerdo con la comunicación, los días 17 y 18 de julio, 46 familias se desplazaron para prevenir posibles daños por las granadas de mortero que explotaban a pocos metros de sus hogares. Las comunidades han manifestado su preocupación y temor de que llegue a suceder algún evento irreversible. Así mismo, los habitantes de Puerto Colombia, denuncian que el niño Esteban Portillo, de 12 años de edad, se encuentra desaparecido desde la mañana del lunes.

\[caption id="attachment\_11476" align="aligncenter" width="448"\][![Ataque mortero](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/SAM_3088.jpg){.wp-image-11476 .size-full width="448" height="336"}](https://archivo.contagioradio.com/granadas-de-mortero-del-ejercito-ponen-en-riesgo-la-vida-de-campesinos-en-puerto-asis/sam_3088/) Zona afectada por el mortero\[/caption\]

Estos eventos no son casos aislados, pues la misma base militar ha cometido continuas y sistemáticas infracciones al Derecho Internacional Humanitario y a los Derechos Humanos en lo que va del 2015.

"Hoy 20 de julio que las comunidades campesinas viven el cese de hostilidades por parte de la insurgencia de las FARC-EP, la respuesta del Estado no puede ser la militarización y violación sistemática de los Derechos Humanos de las comunidades", señala el comunicado.
