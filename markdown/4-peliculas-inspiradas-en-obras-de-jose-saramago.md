Title: 4 películas inspiradas en obras de José Saramago
Date: 2015-06-18 14:48
Author: AdminContagio
Category: 24 Cuadros
Slug: 4-peliculas-inspiradas-en-obras-de-jose-saramago
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/josc3a9-saramago-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [18, jun, 2015] 

Con la partida el 18 de junio de 2010, del escritor José Saramago, premio nobel de literatura en 1998, su legado representado por su obra y pensamiento, mantiene vigencia gracias a las re-publicaciones y compilaciones que a manera de homenaje se hacen desde su muerte de la que se cumplen cinco años.

Más alla de las páginas, de las letras, de las entrevistas radiales y televisivas que circulan con gran regularidad en formatos físicos y virtuales, es posible encontrar la influencia del trabajo de Saramago en diferentes manifestaciones artisticas de las que el cine no es la excepción.

En Contagio Radio les presentamos cuatro producciones cinematográficas inspiradas en obras del escritor, ensayista, poeta, periodista, novelista y dramaturgo portugués.

[BLINDNESS (2008) (*Ensaio sobre a Cegueira)*]

En el año 1998, Saramago publica una de sus novelas más conocidas, "Ensayo sobre la Ceguera", que el mismo escritor definia como "la novela que plasmaba, criticaba y desenmascaraba a una sociedad podrida y desencajada". La adaptación de la obra fue llevada al cine bajo la dirección del brasilero Fernando Meirelles (Ciudad de Dios, El Jardinero Fiel) con guion de Don Mckellar  y participación de los actores estadounidenses Julianne Moore, Mark Ruffalo, Danny Glover y el mexicano Gael García Bernal, entre otros.

<iframe src="https://www.youtube.com/embed/IPrv9tFknxk" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[LA BALSA DE PIEDRA] (2002) inspirada en *A jangada de pedra*

Es la historia ficticia de cómo una falla geológica separa la Península Ibérica del resto del continente Europeo y la convierte en una inmensa balsa de piedra que navega a la deriva por el océano atlántico, un reflejo crítico del escritor a la manera en que la unificación de Europa desplaza los países ibéricos, navegando a la deriva sin una entidad cultural, social o económica propia. Su adaptación cinematográfica del año 2002 fue una co-producción entre España, Portugal y Holanda dirigida por el realizador francés George Sluizer. El rodaje de esta película se ha realizado en Portugal, Asturias, Navarra, Andalucía y Madrid.

<iframe src="https://www.youtube.com/embed/EwiA_MF5QLE" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[EMBARGO] (2010) inspirada en *Embargo*

Nuno es un hombre que trabaja en un puesto de perritos calientes, que también ha inventado una máquina con la que pretende revolucionar la industria del calzado: un escáner de pie. En medio de un embargo de gasolina y encontrándose en una extraña situación, Nuno se queda misteriosamente confinado en su coche, encontrando su vida de repente embargada. La adaptación cinematográfica es una coproducción entre Portugal, España-Brasil grabada en Portugal en el verano de 2009, dirigida por el portugués António Ferreira y cuenta en su reparto con los actores Filipe Costa, Cláudia Carvalho, Pedro Diogo, Laura Matos, João Caetano, Eloy Monteiro y Nuno Ávila entre otros.

<iframe src="https://www.youtube.com/embed/1NGgo7bjhSw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[ENEMY] (2013) inspirada en *O homem duplicado*

Estrenada en hispanoamerica como "El hombre duplicado", es la adaptación de la novela homónima producida como un thriller psicológico bajo la dirección de Denis Villeneuve, con guión de Javier Gullón quien adapta ligeramente la novela publicada en 2002, el film cuenta la historiade Adam (Jake Gylenhall) un anodino profesor de historia, con una vida monótona y gris, quien ve en una película a un actor físicamente igual a él, obsesionándose a partir de ese momento con encontrarlo. La película cuenta además con la participación de las actrices Mélanie Laurent, Isabella Rossellini y Sarah Gadon. La cinta fue proyectada en la sección de Presentación Especial en el Festival Internacional de Cine de Toronto de 2013.

<iframe src="https://www.youtube.com/embed/ty3U5RRb2YI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
