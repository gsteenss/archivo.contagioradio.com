Title: Arrancó paro de trabajadores de la DIAN
Date: 2017-10-17 13:11
Category: Movilización, Nacional
Tags: DIAN, Gobierno Nacional, Paro de la DIAN, trabajadores de la DIAN
Slug: arranco-paro-de-trabajadores-de-la-dian
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/ParoDian.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Maxinoticias] 

###### [17 Oct 2017] 

Por incumplimientos en los acuerdos con el Gobierno Nacional, los trabajadores de la Dirección de Impuestos y Aduanas Nacional (DIAN) **iniciaron hoy un paro indefinido.** Lo peor del asunto es que no están pidiendo adiciones presupuestales sino decretos que permitan el cumplimiento de los acuerdos previos que cumplen ya dos años.

De acuerdo con John Fredy Restrepo, presidente de Sintradian, uno de los sindicatos de trabajadores de la DIAN, **llevan varios años negociando con la DIAN y el Gobierno Nacional** y ya se ha llegado a acuerdos sobre el fortalecimiento laboral en la entidad. Dijo que han buscado que se amplié la planta de trabajo, la tecnificación de algunos mecanismos para prestar un mejor servicio y el reconocimiento salarial, no un aumento.

Si bien ya se había llegado a un acuerdo, que atendía la solicitud de los trabajadores, **“el gobierno se ha negado a cumplir esos acuerdos argumentando que hay una falta de presupuesto”.** Sin embargo, los trabajadores han manifestado que no han solicitado presupuesto adicional, si no que se invierta en las necesidades de la entidad. (Le puede interesar: ["Trabajadores de la DIAN a paro el 22 de Agosto"](https://archivo.contagioradio.com/trabajadores-de-la-dian-podrian-ir-a-paro-el-23-de-agosto/))

### **En la DIAN hay más de 1800 trabajadores que son temporales** 

Una de las solicitudes de los trabajadores es que, es necesario que se incluya a la planta laboral de manera permanente, **los más de 1800 trabajadores que son temporales** y que no tienen las garantías de un trabajo estable que cumpla con los estándares de la ley.

Restrepo indicó que ha habido **falta de voluntad política del Gobierno Nacional** para expedir los decretos de una ley que ya ha sido firmada y que contiene los acuerdos pactados. “Como capricho de la función pública, han evadido en muchas ocasiones pidiéndonos más tiempo y asegurando que ya va a salir el decreto y no sucede nada”.

### **Los colombianos ya se han visto afectados por la falta de adecuación de la entidad** 

El funcionario indicó que es desafortunado que se tenga que poner en riesgo el servicio de la DIAN, en donde los colombianos se verán afectados, por la falta de voluntad política para cumplir unos acuerdos ya pactados. Sin embargo, recordó que los servicios que presta la entidad a la ciudadanía **son ineficientes por la falta de mejoras**, y “desde hace rato los colombianos se han visto afectados”.

Ante esto manifestó que “para que las personas obtengan el RUT tienen que pedir una cita con 15 días de anticipación, **hacer filas de 3 horas**, y eso no es junto para la gente que contribuye al país”. Es por esto que han pedido una ampliación de la planta laboral de 11 mil trabajadores a más de 15 mil para garantizar un mejor servicio. (Le puede interesar: ["Paro estudiantil de l UPTC logra acuerdo sobre definición en costo de matrículas")](https://archivo.contagioradio.com/paro-estudiantil-de-la-uptc-acuerdo-sobre-definicion-en-costo-de-matriculas/)

Restrepo criticó la postura del Gobierno quien, para atender la falta de trabajadores, **“ha puesto a la Policía Nacional a hacer funciones** de control aduanero cuando debería estar protegiendo a la ciudadanía”. Creen que la solución es fortalecer la entidad que ha sido creada con el propósito de desarrollar el control aduanero, tributario y cambiario en el país.

Finalmente, los trabajadores manifestaron que **“ya no hay necesidad de más diálogos**, lo que queda por hacer es que el Gobierno cumpla con los que ya ha acordado”. Esta semana se intensificarán los cierres de varias sedes de la DIAN en el país, “para generar presión para que el Gobierno cumpla y tramite los decretos”.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="osd-sms-wrapper">

</div>
