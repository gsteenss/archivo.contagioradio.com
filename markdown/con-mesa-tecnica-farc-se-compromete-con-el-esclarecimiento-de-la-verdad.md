Title: Con Mesa Técnica, FARC se compromete con el esclarecimiento de la verdad
Date: 2019-06-12 18:05
Author: CtgAdm
Category: Memoria, Paz
Tags: comision de la verdad, FARC
Slug: con-mesa-tecnica-farc-se-compromete-con-el-esclarecimiento-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Comisión-de-la-Verdad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de la Verdad] 

Este miércoles en la sede de la **Comisión de la Verdad** fue instalada la **Mesa Técnica de aporte a la Verdad,** una muestra de compromiso por parte del movimiento **Fuerza Alternativa Revolucionaria del Común (FARC)**  para aportar al esclarecimiento de lo ocurrido en el marco del conflicto armado, a través de testimonios y reflexiones que buscarán contribuir al conocimiento público de los responsables y enfocarse en la no repetición.

**Francisco De Roux, presidente de la Comisión** destacó el rol que cumplirán los integrantes del partido FARC en este proceso en búsqueda de verdad histórica, "queremos decirles gracias por haber tenido el coraje de parar la guerra manteniendo los mismos ideales. Pasar de las armas a la controversia política y concebidos como partido, enfrentar la enorme aventura de hacer política en el país", expresó.

### Construyendo paz y verdad 

**La Comisión de la Verdad y FARC** comenzaron un proceso de acercamiento en septiembre de 2018, cuando el partido expresó a través de una carta, su deseo de contribuir al esclarecimiento de la verdad. Según **Jaime Alberto Parra, delegado de la colectividad,** esta es la oportunidad para demostrar al país "que sí estamos construyendo paz”.

Después de plantearse una metodología que contextualice lo ocurrido durante el conflicto y sus actores, se propuso la "recolección de testimonios individuales, colectivos e historias de vida" a lo largo del territorio nacional a través de las Casas de la Verdad, un proceso que será revisado una vez al mes por integrantes de ambas delegaciones. [(Lea también: Comisión de la Verdad lista para tejer nuevos relatos de memoria en Medellín)](https://archivo.contagioradio.com/comision-de-la-verdad-lista-para-tejer-nuevos-relatos-de-memoria-en-medellin/)

> [\#Hoy](https://twitter.com/hashtag/Hoy?src=hash&ref_src=twsrc%5Etfw) hemos entregado a la [@ComisionVerdadC](https://twitter.com/ComisionVerdadC?ref_src=twsrc%5Etfw) un símbolo que representa la lucha por miles de hombres y mujeres de las FARC-EP [@PartidoFARC](https://twitter.com/PartidoFARC?ref_src=twsrc%5Etfw) que anduvieron trochas, caminos y ofrendaron su vida por materializar la Paz de [\#Colombia](https://twitter.com/hashtag/Colombia?src=hash&ref_src=twsrc%5Etfw). Todos y todas vamos a cuidar la Primavera ! [pic.twitter.com/C5gUARrnrP](https://t.co/C5gUARrnrP)
>
> — Camilo Fagua (@FaguaCamilo) [12 de junio de 2019](https://twitter.com/FaguaCamilo/status/1138849128685940737?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
