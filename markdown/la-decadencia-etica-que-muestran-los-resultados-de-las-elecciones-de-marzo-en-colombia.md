Title: La decadencia ética que muestran los resultados de las elecciones de marzo en Colombia
Date: 2018-03-14 06:00
Category: Abilio, Opinion
Tags: elecciones colombia, FARC, Senado
Slug: la-decadencia-etica-que-muestran-los-resultados-de-las-elecciones-de-marzo-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/votaciones-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### Foto: Silla Vacia 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 14  Mar 2018 

[Los resultados de las elecciones para el Congreso de la República y para las consultas  en los que ganan el Centro Democrático y Cambio Radical, los dos partidos cuyos candidatos están más comprometidos en escándalos de parapolítica,  paraeconomía, en nada deben sorprender. El primero obtuvo cerca de 2.5 millones de votos al Senado y 2.35 millones de la Cámara; el segundo obtuvo el 2.2 millones de votos al Senado y 2.1 millones a la Cámara.]

[En la consulta para la escogencia de candidato presidencial, por su lado el  candidato del centro Democrático obtuvo 4.0] [millones de votos contra 2.8 millones del candidato de izquierda, poniéndolo en clara desventaja. Los otros ganadores minoritarios al congreso, que se ubican en la línea de los derechos humanos, de la anticorrupción, congregan una fuerza que sigue ubicándose en  lógica de resistencia, que puede garantizar buenos debates de control político, como lo han hecho en la legislatura que está concluyendo; algunos de ellos corriendo graves riesgos.]

[ Las reglas de la política se siguen guiando por el control de los medios masivos de información, por el poder del dinero para incidir en la voluntad de votos, por la mecánica electoral que garantiza que sólo  los ricos sigan concentrando el poder de la tierra, de la industria, del sistema financiero y para poderlo sostener, controlen todo el poder político que les permita poner en función suya los poderes legislativo,  ejecutivo y judicial.]

[El acuerdo de paz  entre el gobierno y las FARC quiso reformar el modo de hacer política en Colombia,  con la asignación de la Misión Electoral Especial (MEE) que se encargó de proponer una reforma electoral. [La propuesta](https://moe.org.co/wp-content/uploads/2017/04/Libro-Reforrma-completo-2017-1-1.pdf) se hizo por parte  de la MEE . Sin embargo, no tenía ninguna posibilidad de ser siquiera estudiada en el congreso que está mayoritariamente conformado por representantes del establecimiento que llegaron a su curul gracias a la mecánica electoral vigente.]

[ El representante a la cámara Alirio Uribe, quien lamentablemente sale del congreso, comentó  hace varios meses que un congresista de un partido tradicional le había dicho, mientras se debatía  en la Cámara de representantes la aprobación del acuerdo de la Habana: “espere que lleguemos a discutir el punto de participación política, que de eso sí sabemos nosotros”, sentenciando lo que realmente pasó:  El hundimiento de las 16 curules para las víctimas y el quite a la discusión de la reforma electoral, de la financiación de las campañas y el acceso equitativo a los medios de información.]

[Los resultados de éstas elecciones  vuelven a denunciar el talante ético del país, donde la inversión de valores atraviesa todos los sectores de la vida social, garantizando que sean los promotores del engaño, la compra de votos, los implicados en crímenes contra la humanidad, los promotores del paramilitarismo, los  que se cimientan en el soborno, quienes siguen ostentando, mayoritariamente, el poder político y quienes tal vez ganarán la presidencia de la República.]

[La mala ética del dinero fácil,  de los atajos, del crimen como escalera, de la prevalencia de los fines sobre los medios,  de la construcción mafiosa del poder, de la promulgación de la mentira, de la exaltación de la guerra, es la que se sigue imponiendo. Aplican, en el caso Colombiano, expresiones proféticas fuertes de la tradición del Antiguo Testamento que invitan a reflexionar sobre aquello en lo que se cree, máxime cuando el [97% de los colombianos se denominan creyente en Dios](http://caracol.com.co/programa/2017/12/04/6am_hoy_por_hoy/1512388155_461749.html).]

[ Jeremías, por ejemplo, ante la decadencia de Israel, sentenció con expresiones ásperas: “Escuchan, jefes de Jacob, dirigentes de Israel: ¿no les corresponde a ustedes conocer el derecho? Pero ustedes odian el bien y aman el mal, arrancan la piel de encima y la carne de sus huesos… Escuchen esto, jefes de Jacob, gobernantes de Israel, que desprecian la justicia y tuercen el derecho, que edifican a Sión con sangre y a Jerusalén con crímenes (Miq 3,1-2,9-10).]

[O el caso de Isaías que ofrece, también, palabras que  se puedan aplicar a ésta realidad, cuando concluye su parábola de la Viña: “La viña del Señor todopoderoso es el pueblo de Israel, y los hombres de Judá su plantel escogido. Esperaba de ellos derecho, y no hay más que asesinatos, esperaba justicia, y solo hay lamentos” (Is 5,7). Y a los jefes  les decía: “¡Ay de los que llaman bien al mal y mal al bien, que toman las tinieblas por luz y la luz por tinieblas, que consideran a lo amargo dulce y a lo dulce amargo!” (Is 5,20).]

[El camino a seguir para la reconstrucción de la esperanza está también ya encubado en esta misma sociedad y se expresa  aún en ese mismo parlamento en quienes son capaces, desde su condición minoritaria de denunciar estos poderes, de exigir derechos, de defender el ejercicio   de la movilización Noviolenta de la sociedad, sin que se imponga la fuerza de la represión, de afirmar la protección de la vida natural, de todas las especies. Seguramente es difícil lidiar con ese poder corrupto y sostenerse fiel a los valores de los ancestros, esos que le dan tranquilidad al corazón,  que se expresan en el brillo de los ojos.]

[La esperanza también está, como bellamente  lo ha señalado [William Ospina](http://www.elespectador.com/noticias/politica/la-paz-del-pueblo-ausente-por-william-ospina-articulo-743599), en la gente, en las colombianas y colombianos que nunca son interpretados, aquellos por los que decía Jorge Eliecer Gaitán que “el pueblo es superior a sus dirigentes”.]

[ Eso implica volver abajo, tender puentes, hacer trabajo de hormiga, tener especial cuidado a los métodos, tener paciencia, volver a los ancestros, saber combinar el sentimiento con el pensamiento, construir desde el leguaje de los hechos, reconocer en el arte la expresión de lo que somos, rendir cuentas y prender el fuego de la Acción Noviolenta como camino para la afirmación de derechos desde los valores, que en sí mismo validan las acciones que se emprendan, contra la lógica del fácil éxito.  ]

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio
