Title: Cátedra de Periodismo, para reciclar o tirar a la basura
Date: 2015-07-24 09:49
Category: Opinion, Ricardo
Tags: comunicacion, medios, paz, periodismo catedra, ricardo ferrer espinoza
Slug: catedra-de-periodismo-para-reciclar-o-tirar-a-la-basura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Periodismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [Ricardo Ferrer Espinosa     @ferr-es]

#### 

###### [24 jul 2015]

Como buen obsesivo-compulsivo releo de vez en cuando mis viejos apuntes de clase... y me río.

“El periodismo, los periódicos, no son el Estado. Hay entre ellos unos lazos o vínculos institucionales, pero no se puede reemplazar su actuación. Puede haber colaboración armónica, como la que existe entre las ramas del poder público, pero nunca un traspaso de responsabilidades. Así como los periódicos, con toda razón, no consienten que desde el Estado se les trate de dirigir, los periódicos tampoco pueden pretender ocupar el sitio del Estado, asumir su papel y reemplazarlo de hecho”. \[1\]

Son textos para una cátedra de periodismo, útiles para un país normal. Pero en Colombia, ni la prensa ni el Estado, lo son. Nuestra generación vio cómo los periódicos capitalinos cuestionaban a ministros y éstos caían, señalaban la inconveniencia de un decreto y éste era derogado, arremetía el Estado contra los campesinos y los diarios, cómplices, les agregaban el adjetivo de bandoleros. Mientras ardían las sedes donde se vendía “Voz proletaria”, los editorialistas hacían silencio sobre “abusos” militares. Ni una línea sobre el marcial ascenso de genocidas.

Desde 1948, Lauchlin Currie \[2\] y sus tesis depredadoras deformaron la administración pública, con cicatrices que duelen hasta hoy. Cada frase del virrey que nos envió el Banco Internacional de Reconstrucción y Fomento (Banco Mundial) era reproducida y acatada mansamente por los gobernantes y su prensa. El poder presidencial se concentró aún más, la planeación nacional y las grandes decisiones quedaron en manos de un grupo cada vez más excluyente y cerrado. La jerarquía católica monopolizó la vida religiosa, las libertades individuales se suspendían durante el sempiterno “Estado de sitio”. En Macondo, las políticas más duras contra el campesinado acentuaron su filo.

Las críticas al modelo de Currie - BM, solo podían proceder de pasquines, el calificativo habitual para la prensa de periferia. Periodistas insumisos denunciaban que en las zonas rurales y en los municipios lejanos se calcaron los modelos franquistas, con señoritos intocables y gamonales que impusieron la exclusión, la inequidad y la violencia. “La justicia, para los de ruana”.

Mientras tanto, los profesores de periodismo nos recalcaban:

“...los periódicos no pueden ser jueces o tratar de ir más allá de los jueces, persistiendo en condenar y atribuirle cargos a los sospechosos o a los presuntos implicados en las acciones terroristas si no lo hacen los propios jueces...”

Los reporteros tampoco vinieron a esta clase: Al detenido de piel morena le asignan el alias de “El Negro”, quien se llame Francisco será “alias Pacho”, y a quien es docente, el editor lo describirá como “alias el Profe”. Luego, cuando los jueces exoneran a los sospechosos, los redactores omiten rectificar.

Nos insistían: “Los periodistas no debemos ir más allá de los jueces”. Pero hoy, los casos de corrupción, destapados por ciudadanos y periodistas que fueron más allá de los jueces, son incontables.

Nos enfrentamos a caudillos que dirigen todo: El Estado, la prensa, haciendas, y los partidos políticos. Soñamos que Colombia dejará de ser el botín exclusivo de 300 familias que tratan al ciudadano como ganado.

Recientemente ha cambiado la composición entre los grupos que se repartían el poder. Se abre una ventana y hasta nos hablan de Un nuevo país. Dicen que finalizará el conflicto y supuestamente debe surgir un nuevo modelo de periodismo, para un nuevo ciudadano con acceso a los medios. Ese día callará el periodista y hablará el vecino.

##### \[1\] Jorge Alberto Velásquez Betancur: Los periodistas no somos culpables. Texto citado por Antonio Caballero en el Seminario Taller “La comunicación como estrategia de paz”, Alcaldía de Medellín 1995 –1998. 

##### *\[2\]* Laureano Gómez, la Misión Currie y el proyecto de reforma constitucional de 1952. Miguel Malagón Pinzón, Diego Diego Nicolás Pardo Motta. 

##### <http://revistas.javerianacali.edu.co/index.php/criteriojuridico/article/viewFile/330/884> 
