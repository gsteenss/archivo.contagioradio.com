Title: En la VII Cumbre de las Américas triunfaron Cuba y Venezuela
Date: 2015-04-13 13:16
Author: CtgAdm
Category: El mundo, Entrevistas
Tags: Cuba, Cumbre de los Pueblos, Encuentro Obama y Raúl Castro Panamá, Evo Morales, Nicolas Maduro, Panama, Rafael Correa, Silvio Rodríguez, VII cumbre de las Américas Panamá
Slug: en-la-vii-cumbre-de-las-americas-triunfaron-cuba-y-venezuela
Status: published

###### Foto:Elmundo.es 

###### **Entrevista con [Olmedo Carrasquillas], del colectivo Panameño Radio Temblor:** 

<iframe src="http://www.ivoox.com/player_ek_4346365_2_1.html?data=lZihmJiaeY6ZmKiakp6Jd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9bhw9fSjcnJb83V1JCuz4qnd4a1mtfWxcbXb8bijLXO0MbRaaSnhqaejZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La **VII cumbre de las Américas** entre jefes de estado de todo el continente ha concluido con una **foto histórica** entre **Barack Obama y Raúl Castro**, como inicio de el restablecimiento de las relaciones entre ambos países después de medio siglo.

Desde que en **1961 se rompieran las relaciones** no se habían vuelto a reencontrar un mandatario Cubano y uno Estadounidense para mantener una reunión de una hora en que se discutió el inicio de las relaciones comenzando con la apertura de las embajadas en ambos países.

Pero no fue el único encuentro también **Obama y Maduro mantuvieron una pequeña conversación** de apenas 10 minutos donde pudieron conversas sobre las declaraciones de EEUU considerando al país como una amenaza para su seguridad. A pesar de la oposición de 33 países, la mayoría, en contra de dichas declaraciones Obama mantuvo su posición y en la cumbre su distancia con la delegación venezolana.

**Raúl Castro**, en el momento de su **intervención** aludió que nunca se le había permitido participar con lo cual su intervención sería de **48 minutos, 8 minutos que le corresponden por las 6 cumbres en las que no participó.**

Según **Olmedo Carrasquillas de Radio Temblor y Voces Ecológicas,** organizaciones sociales Panameñas, a la cumbre **no fue invitada la delegación de la sociedad civil Cubana** pero si otra, cuyos miembros pertenecían a la disidencia cubana en Miami, financiada por terroristas como Luis posada Carriles.

Lo mismo sucedió con los colectivos **venezolanos opositores** afincados en Panamá que fueron autorizados para protestar contra Maduro, **provocando incidentes**, a pesar de las demandas de organizaciones sociales al estado Panameño para que prohibieran las manifestaciones.

Muchos venezolanos opositores aprovecharon el cambio en el mercado negro de dólares a bolívares para hacer fortunas y poder utilizar el paraíso fiscal de Panamá como lugar de resguardo de sus ingresos.

Olmedo nos explica que hubo muchas **temáticas censuradas** como las de la **desigualdad** creciente, la **extrema pobreza** o la **corrupción** en el continente, hecho que también fue rechazado por el mandatario de Bolivia Evo Morales.

Para el periodista Panameño fue importante que se discutiera sobre las **Malvinas, los 43 desaparecidos de Ayotzinapa, la invasión de EEUU y posterior masacre en Panamá de 1889, el cese del uso de transgénicos o el proceso de paz en Colombia.**

Todas estas **temáticas** responden a la **agenda unitaria de la gran mayoría de países de Sudamérica** y además supone un logro para el futuro dejando claro en esta cumbre que el norte ya no va a poder mandar sobre el sur y que unas nuevas relaciones se van a tener que establecer de cara a un futuro.
