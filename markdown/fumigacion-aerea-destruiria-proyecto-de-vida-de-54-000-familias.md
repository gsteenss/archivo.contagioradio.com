Title: 54.000 familias serían afectadas por reanudación de fumigación aérea
Date: 2018-12-14 12:18
Author: AdminContagio
Category: Ambiente, Nacional
Tags: cultivos ilícitos, Fumigación con Glifosato
Slug: fumigacion-aerea-destruiria-proyecto-de-vida-de-54-000-familias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Glifosato-Putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 14 Dic 2018 

Como parte de su nueva estrategia antidrogas, el Gobierno anunció que impondrá nuevamente la fumigación aérea para combatir los cultivos de uso ilícito, aunque se evalúa qué químico será usado, no se descarta el glifosato, una medida que ya se ha demostrado científicamente es nociva para la salud y que fue condicionada por la Corte Constitucional.

Leider Miranda, vocero de la **Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana (COCCAM)**, expresó su preocupación ante el regreso de la política que fue usada durante el Plan Colombia y que fue suspendida en 2015 bajo el gobierno de Juan Manuel Santos, quien acogió las recomendaciones de la Organización Mundial de la Salud sobre los efectos nocivos que causaría esta sustancia química para la salud.

El integrante de la COCCAM señaló que los cutivadores quieren continuar con el programa de sustitución, una medida que según la fundación Ideas para la Paz, **se han acogido** **54.000 familias de las 180.000 que trabajaban con cultivos de coca**. Miranda también indicó que la política de fumigación va en contravía del Acuerdo de Paz, donde se acordó precisamente que si las comunidades deciden sustituir sus cultivos, la erradicación forzada no tendría cabida en sus territorios.

### **Volver al glifosato es volver al pasado** 

Miranda aduce que tal como fue demostrado en el pasado, los programas de fumigación no dan ningún resultado, “nosotros hemos visto que lo único que hace es desplazar los cultivos, además de dañar el territorio y a los cultivos de pan coger" y agregó que en lugar de combatir al narcotráfico esta medida afecta al cultivador que no tiene otra opción para vivir”.

“Las fumigaciones con glifosato perjudican más que el cultivo de coca” afirma Miranda quien fue enfático en señalar que al Gobierno no le importan los efectos nocivos que ha causado y podría causar nuevamente la fumigación con glifosato sobre los territorios, las fuentes hídricas y el peligro que representa para las mujeres embarazadas, los animales y la población en general.

El vocero  manifestó que en respuesta a esta decisión, se están coordinando acciones con los delegados departamentales para evaluar la audiencia pública del pasado lunes en la cual estuvieron ausentes los ponentes del Gobierno, lo que para los cultivadores demuestra que no hay un verdadero interés de construir una política de paz para el campo colombiano, Miranda, advirtió que de aprobarse esta medida los cultivadores entrarían en paro nacional.

<iframe id="audio_30797615" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30797615_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
