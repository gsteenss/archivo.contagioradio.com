Title: Senador Cepeda desmiente acusaciones de Alias Monoleche
Date: 2018-08-01 15:33
Category: Nacional, Política
Tags: Alvaro Uribe, Iván Cepeda, Monoleche, paramilitares
Slug: senador-cepeda-desmiente-acusaciones-de-alias-monoleche
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/semana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Semana ] 

###### [01 Agosto 2018] 

Iván Cepeda manifestó que los supuestos sobornos que menciona Jesús Roldan, alias "Monoleche" ex jefe paramiliar, que dice haber recibido por parte de un recluso a nombre del senador, hace parte de **una estrategia para desviar la atención de la investigación en curso contra el ex presidente Álvaro Uribe**.

Según Roldan, cuando se encontraba recluido en la Cárcel de Itagüi, un compañero de celda se le acercó en distintas ocasiones para ofrecerle dinero y beneficios jurídicos con tal de hacer parte de un montaje contra los hermanos Uribe Vélez y vincularlos al paramilitarismo. De igual forma, **aseguró haber tenido una reunión con Cepeda en donde el senador le habría preguntado por su situación carcelaria**.

Cepeda confirmó haber participado en una reunión en la Cárcel de Itagüi con varios exjefes paramilitares, entre los que estaba Roldan. Sin embargo, expresó que no recuerda particularmente haber interactuado con alias "Monoleche" y **calificó las aseveraciones como un "testimonio desdeñable"**. La reunión habría tenido como finalidad, dialogar sobre cómo responderían esos jefes a las víctimas en materia de verdad.

El senador del Polo Democrático, señaló que "todo eso es parte del mismo libreto que intenta, a través de un masivo ataque informativo o desinformativo, llenar a la opinión pública de elementos que desvían la atención de lo esencial que **es el llamado de la Corte a Uribe y al representante Prada por hechos muy graves"**. (Le puede interesar:["Uribe esta haciendo una campaña tenebrosa contra la Corte Suprema"](https://archivo.contagioradio.com/uribe-esta-haciendo-una-tenebrosa-campana-contra-la-corte-suprema-ivan-cepeda/))

<iframe id="audio_27490022" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27490022_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
