Title: Alcanzado acuerdo nuclear entre Irán y el G5+1
Date: 2015-04-06 16:44
Author: CtgAdm
Category: El mundo, Política
Tags: Acuerdo nuclear Iran y G5+1, Acuerdo nuclear Iran-EEUU, Enriquecimiento de uranio Irán, Ignacio Álvarez Osorio
Slug: alcanzado-acuerdo-nuclear-entre-iran-y-el-g51
Status: published

###### Foto:Americatve.com 

###### **Entrevista con [Ignacio Álvarez Osorio], analista político:** 

<iframe src="http://www.ivoox.com/player_ek_4313093_2_1.html?data=lZielZWdd46ZmKiak5WJd6KnmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhcTpxtfR0ZDSucTgxsbfjcrSuNPZjKyih5emdYzZjK7fh6iXaaKlz5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Después de **8 años** ininterrumpidos de **sanciones y bloqueo económico a Irán** por su programa nuclear, el pasado jueves fue alcanzado un **acuerdo** entre las 5 potencias con derecho a veto del consejo de seguridad de la ONU, más Alemania.

Han sido ocho jornadas de grandes negociaciones, salpicadas por las declaraciones de Israel en contra de cualquier acuerdo con el país persa, que incluso su presidente Benjamin Netanyahu llegó a acudir al congreso d los EEUU, sin haber sido invitado por el presidente, a realizar propaganda en contra de la negociación.

Para **John Kerry,** negociador de EEUU y su homólogo iraní **Mohammad Javad Zarif**, el acuerdo ha sido alcanzado pero aún existen diferencias que hay que limar para que este se pueda cumplir**.**

Sin duda el acuerdo tiene como objetivo levantar las sanciones y evitar que Irán pueda construir la bomba atómica, el acuerdo se traduce en  9 puntos.

**Irán deberá someterse durante 25 años al estricto** **control de los observadores de la** OEIA, el 95% del uranio ya producido deberá ser enviado al exterior, y el enriquecimiento de este tendrá que ser supervisado durante 10 años. También su uso científico será restringido solo para fines civiles y no militares.

Con este acuerdo se levantarían las sanciones económicas y diplomáticas impuestas contra el país.

Según **Ignacio Álvarez Osorio**, profesor de la universidad de Alicante sobre **estudios árabes**, el acuerdo no solo es importante por la estabilidad que va a producir en la región, sino que también en un futuro puede ser un **cambio de relaciones entre EEUU y el mundo árabe**.

Actualmente los principales aliados estadounidenses son países de mayoría sunita, quienes están detrás del islamismo extremista. Al firmar dicho **acuerdo podrían afianzarse nuevos  lazos entre EEUU y países de mayoría chií en la región**.

El **30 de junio es el plazo máximo** para que se firme el acuerdo, comiencen las restricciones contra el programa nuclear y se levanten las sanciones contra Irán.
