Title: Calakmul, una ciudad maya en medio de la naturaleza
Date: 2015-01-23 14:41
Author: CtgAdm
Category: datos
Tags: Calakmul, mexico
Slug: calakmul-una-ciudad-maya-en-medio-de-la-naturaleza
Status: published

En el estado de Campeche al sureste de México existe un lugar que ha convivido durante siglos con la naturaleza, su nombre es **Calakmul**, que significa **dos montículos juntos o adyacentes.**

Esta es una ciudad** ** descubierta en la década de los 30 del siglo pasado y fue **habitada  por la civilización maya.** Para algunos antropólogos esta ciudad fue una de las más importantes para esta civilización en los años 600 a 800.

La ciudad  vive en **el segundo pulmón natural más grande del mundo** y se destaca por su arquitectura prehispánica entre la que se encuentran las **dos  pirámides  que sobresalen entre los arboles **de este lugar, éstos dos templos son considerados entre los más grandes construidos durante el periodo clásico maya.

\[caption id="attachment\_3231" align="aligncenter" width="599"\][![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/mural-Calakmul-1.jpg){.wp-image-3231 .size-full width="599" height="533"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/mural-Calakmul-1.jpg) Mural - Según antropólogos la mujer carga el Atole, bebida indígena\[/caption\]

En la ciudad se encontró en el año  2004 murales mayas que se conservaban luego de pasar más de 1300 años ocultos, en estos se representan las actividades cotidianas de los indígenas como  nunca antes se había visto, ya que siempre se han presentado imagenes que obedecen a las deidades o cultos religiosos.

La diversidad silvestre y de vegetación que rodea a Calakmul fueron razón suficiente para que en el año 1989 se convirtiera en reserva, un lugar que alberga a más de 86 especies de mamíferos y hogar para 282 especies de aves,   50 especies de reptiles, 400 de mariposas y 73 tipos de orquídeas salvajes.

\[caption id="attachment\_3259" align="aligncenter" width="604"\][![09-lagunita\_fasada\_desno-web](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/09-lagunita_fasada_desno-web-1024x754.jpg){.wp-image-3259 .size-large width="604" height="445"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/09-lagunita_fasada_desno-web.jpg) Lagunita, descubierto por Ivan Šprajc, en la Reserva de la Bíosfera Calakmul\[/caption\]

Clalkmul fue declarado **patrimonio Mixto de la Humanidad por la UNESCO** en el año  2002 y el año 2014 se inscribió como primer bien mixto de México.

Esta ciudad está abierta al público y se puede visitar durante todo el año, un lugar único para conocer la riqueza maya y la riqueza de la naturaleza ha guardado durante cientos de años.
