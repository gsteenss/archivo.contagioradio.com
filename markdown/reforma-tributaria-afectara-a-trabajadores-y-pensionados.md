Title: Reforma Tributaría afectará a trabajadores y pensionados
Date: 2016-10-10 12:59
Category: Economía, Nacional
Tags: Aumento del IVA, Plebiscito, Reforma tributaria
Slug: reforma-tributaria-afectara-a-trabajadores-y-pensionados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/valencia1-e1476123137264.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: JusticiaTributaria] 

###### [10 Oct de 2016 ] 

En horas de la tarde de este Lunes, Mauricio Cárdenas Ministro de Hacienda presentará ante el gabinete de Juan Manuel Santos el proyecto que busca realizar la tercera Reforma Tributaria de este Gobierno. Según lo propuesto, **el 71 % del nuevo recaudo provendrá de impuestos indirectos como el IVA, que aumentará a un 19%**, la canasta familiar y la gasolina, gravámenes que golpean directamente a la clase trabajadora sin importar sus ingresos.

Mario Alejandro Valencia economista, subdirector del Centro de Estudios del Trabajo e integrante de la Red por la Justicia Tributaria en Colombia, señaló que este anuncio no debería sorprender puesto que **meses atrás Santos dijo que la reforma tributaria se hacía independientemente del resultado del plebiscito.** Añade que una reforma tributaria “[en estos momentos es absolutamente inconveniente](https://archivo.contagioradio.com/sindicatos-rechazan-reforma-tributaria-y-convocan-a-paro-general/) (…) dirigida no a mejorar las finanzas de la nación sino para dar gobernabilidad a Santos durante los próximos dos años”.

Valencia comenta que “si bien el Estado necesita una reforma tributaria, ésta debería eliminar los beneficios tributarios para solventar el hueco fiscal que tiene Colombia”, más de 11mil millones de pesos adeudados en gran medida por **impuestos que no están pagando las grandes empresas mineras, petroleras y el sector financiero.** “El Gobierno nacional debe implementar mecanismos de control financiero para evitar que el dinero se siga fugando hacia paraísos fiscales”.

Esta es una medida que pretende solventar “el derroche de los recursos de la bonanza minera y petrolera de tantos años”. Mientras tanto, sectores ricos y poderosos no serán tocados. “[Esta reforma no ayudará a cerrar la brecha entre ricos y pobres](https://archivo.contagioradio.com/reforma-tributaria-desaparecera-el-94-de-las-organizaciones-sociales-del-pais/) ni a mejorar el bienestar y la competitividad” concluyó Valencia.

<iframe id="audio_13259634" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13259634_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
