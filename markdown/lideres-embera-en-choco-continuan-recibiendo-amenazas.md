Title: Líderes embera en Chocó continúan recibiendo amenazas
Date: 2020-12-01 09:39
Author: AdminContagio
Category: Actualidad, Nacional
Tags: amenazas a líderes, Chocó, Líderes embera
Slug: lideres-embera-en-choco-continuan-recibiendo-amenazas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/indigenas-desaparecidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 28 de noviembre, [la Comisión Intereclesial de Justicia y Paz](https://twitter.com/Justiciaypazcol/status/1332939777054797824) denunció que sobre las 10:45 de la mañana, los líderes embera **Luis Siniguí y Argemiro Bailarín**, fueron amenazados por segunda ocasión, en menos de seis semanas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los dos líderes, quienes hace parte del resguardo **CAMERUJ** **en Jiguamiandó, departamento del Chocó,** recibieron la amenaza a través de un mensaje en que se les reitera los seguimientos de los que está siendo objetivo el gobernador de Padado, Luis Siniguí. **«Persona que estamos buscando de resguardo Urada Jiguamiando de la comunidad Patado y el tiene un gran problema con nosotros pero estaremos vigente del proceso de seguimiento al señor Luis Eduardo y que buen tarde mi jefe»,** indica el mensaje.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De manera similar sucedió el pasado mes de octubre cuando un mensaje anónimo llegó al celular de Bailarín, el cual expresaba que se estaba haciendo seguimiento y habían intenciones de atentar contra la vida del gobernador Luis Siguiní. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas amenazas se producen en un contexto de control territorial por parte de las Autodefensas Gaitanistas de Colombia (AGC). La Comisión recuerda que desde el pasado mes de julio más de 12 líderes del resguardo se encuentran en riesgo debido al operativo de erradicación de 150 hectáreas con siembras ilícitas de coca respaldadas por las AGC. (Le puede interesar: [Octubre comienza con campaña paramilitar de las AGC](https://archivo.contagioradio.com/campana-paramilitar-agc/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, el 10 de noviembre la Comisión [denunció](https://www.justiciaypazcolombia.com/agc-en-retenes-realizan-seguimiento-a-lideres-indigenas-amenazados-e-integrantes-de-jp/) que integrantes de las AGC siguieron a líderes indígenas, entre ellos Argemiro Bailarín y dos integrantes de la Comisión de Justicia y Paz, sobre la vía de Mutatá-Pavarandó. Estos registraron fotográficamente el vehículo de protección en el que se desplazaban los líderes embera y los miembros de la Comisión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, ya habían instalado un retén ejerciendo control en la movilidad en búsqueda de los líderes indígenas que habían promovido la erradicación en el Resguardo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y es que a pesar de haber presencia de tropas de las Fuerzas Militares en el sector y de las medidas adoptadas por la JEP para el Resguardo, las AGC operan con completa libertad, sin que se tome ninguna medida efectiva para proteger la vida de los y las líderes embera del Chocó. (Le puede interesar: [Enfrentamientos armados producen el desplazamiento de 85 familias en Chocó](https://archivo.contagioradio.com/enfrentamientos-armados-producen-el-desplazamiento-de-85-familias-en-choco/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
