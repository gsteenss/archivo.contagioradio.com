Title: Incumplimientos en la restitución de tierras irán a la CIDH
Date: 2019-05-07 17:27
Author: CtgAdm
Category: Comunidad, Nacional
Tags: CIDH, despojo, lideres sociales, Restitución de tierras
Slug: incumplimientos-restitucion-de-tierras-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/restitucion-de-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El próximo jueves 9 de mayo se adelantará una audiencia en la Comisión Interamericana de Derechos Humanos, (CIDH), que tendrá como protagonistas a reclamantes de tierras y organizaciones defensoras de derechos humanos, en la que se expondrán los retos que enfrenta el proceso de restitución en Colombia. Entre los temas a tratar estarán las amenazas contra líderes reclamantes, las trabas al proceso de restitución y las posibles soluciones.

### **La restitución ha avanzado poco y quienes la lideran están en riesgo**

Como explicó el abogado y director de la Fundación Forjando Futuros, Gerardo Vega, la Ley 1448 de 2011 vence en junio de 2021 y hasta el momento no presenta buenos resultados: de 6,5 millones de hectáreas que deberían ser entregadas, solo se ha avanzado en 270 mil. Adicionalmente, el año pasado se cerraron muchas de las oficinas de restitución, lo que terminó encareciendo el proceso de reclamación. (Le puede interesar:["Crece preocupación en Urabá por garantías de seguridad para restitución de tierras"](https://archivo.contagioradio.com/crece-preocupacion-en-uraba-por-garantias-de-seguridad-en-restitucion-de-tierras/))

Debido a esta reducción en la capacidad de atención y la poca difusión que hubo para comunicar la situación, de las 320 mil solicitudes que esperaba atender la Unidad de Restitución de Tierras, solo se han recibido 120 mil; es decir, solamente el 38% de las estimadas. (Le puede interesar: ["Octubre, fecha crucial para la restitución de tierras"](https://archivo.contagioradio.com/octubre-limite-restitucion-de-tierras/))

A esta situación se suma el riesgo físico para quienes lideran los procesos, pues como indicó Vega, quienes se empoderan de estas iniciativas terminan siendo perseguidos por los grupos despojadores mediante el uso de la violencia física y de estrados judiciales que permiten hacer persecuciones mediante los juzgados. (Le puede interesar: ["Europarlamentarios piden retirar proyecto de Maria Fernanda Cabal contra restitución de tierras"](https://archivo.contagioradio.com/eurodiputados-restitucion-cabal/))

### "Quieren legalizar el despojo" 

Recientemente la representante a la cámara Maria Fernanda Cabal, propuso una modificación a la Ley 1448 que pretendía reformar 3 puntos esenciales de la norma: invertir las cargas de las pruebas, de tal forma que las víctimas probaran la culpabilidad de los despojadores y no que ellos o ellas prueben su inocencia, haciendo más difícil un proceso que ya es complejo. También pretendía excluir la buena fe exenta de culpa; lo que para el abogado, sería quitarle uno de los aspectos centrales a la Ley, pues se reconocería que quienes compraron tierras despojadas no sabían cómo habían sido obtenidas.

Además, se quería crear una segunda instancia en los procesos, haciendo que la restitución efectiva pasara de durar cerca de uno a diez años. Modificaciones que, en visión del Abogado, buscaban  "legalizar el despojo de los últimos 25 años".

En ese sentido, en la audiencia ante la CIDH que se realizará el próximo 9 de mayo entre las 15:45 y las 16:45, las nueve organizaciones participantes pedirán que no se hagan modificaciones a la Ley 1448, que se prorrogue su acción, haya dispocisión del Gobierno para garantizar el presupuesto de su funcionamiento y se detenga el asesinato de líderes sociales y de restitución.

> Hoy ante la [@CIDH](https://twitter.com/CIDH?ref_src=twsrc%5Etfw) en Jamaica, 12 org. de la sociedad civil colombiana presentarán un informe de los incumplimientos del Estado colombiano a la obligación de reparar integralmente a víctimas de despojo y abandono forzado de tierras [\#ColombiaEnLaCIDH](https://twitter.com/hashtag/ColombiaEnLaCIDH?src=hash&ref_src=twsrc%5Etfw) [\#TiempoDeRestituir](https://twitter.com/hashtag/TiempoDeRestituir?src=hash&ref_src=twsrc%5Etfw) -Hilo ?- [pic.twitter.com/wTBJ3zO0GT](https://t.co/wTBJ3zO0GT)
>
> — CCJ (@Coljuristas) [9 de mayo de 2019](https://twitter.com/Coljuristas/status/1126558578700103680?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
<iframe id="audio_35506511" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35506511_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
