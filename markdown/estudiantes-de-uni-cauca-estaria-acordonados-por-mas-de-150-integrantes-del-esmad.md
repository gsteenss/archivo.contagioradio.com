Title: Estudiantes de Uni.Cauca están acordonados por más de 150 integrantes del ESMAD
Date: 2018-11-23 12:33
Author: AdminContagio
Category: Educación, Movilización
Tags: ESMAD, Paro Nacional, UNEES, Universidad del Cauca
Slug: estudiantes-de-uni-cauca-estaria-acordonados-por-mas-de-150-integrantes-del-esmad
Status: published

###### [Foto:UNEES Popayán] 

###### [23 Nov 2018] 

Estudiantes de la Universidad del Cauca denunciaron que desde las 5 de la mañana, más de 150 miembros del ESMAD, en conjunto con integrantes de la Policía y agentes vestidos de civil, mantienen acordonado el centro universitario. Según la denuncia han arremetido en contra del plantel educativo **quebrando los vidrios y poniendo en riesgo a las personas que se encuentran allí.**

"Estamos totalmente sitiados, y desde más o menos las 6 de la mañana, arremetieron contra la Facultad, **quebraron todos los vídrios, nos gasearon con gas pimienta y hay compañeros con afectaciones respiratorias**" afirmó Edison Valencia, estudiante de la Institución educativa.

Frente a esta situación, tanto las directivas de la Universidad como voceros estudiantiles de la UNEES,** adelantan conversaciones con la Alcadia municipal, que dio la orden de desalojo**, para que se visibilice el alto riesgo que estarían afrontando las personas.  (Le puede interesar: ["Las 3 condiciones de los estudiantes para levantar el paro"](https://archivo.contagioradio.com/condiciones-levantar-paro-nacional/))

Hasta el momento serían 150 personas las que se encuentran dentro de esta Facultad, entre estudiantes, docentes y administrativos, que desde hace un mes están acampando en la institución como parte del Paro Nacional por la Educación. (Le puede interesar: ["Estudiantes de Uni.Cauca denuncia posible desalojo violento por parte del ESMAD"](https://archivo.contagioradio.com/estudiantes-de-la-uni-cauca-denuncian-un-posible-desalojo-violento-por-parte-del-esmad/))

Noticia en desarrollo...

<iframe id="audio_30285148" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30285148_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###  
