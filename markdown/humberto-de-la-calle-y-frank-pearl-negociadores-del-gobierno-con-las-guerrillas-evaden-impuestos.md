Title: Humberto De la Calle y Frank Pearl negociadores del gobierno con las guerrillas ¿ evaden impuestos?
Date: 2016-04-20 08:45
Category: Abilio, Opinion
Tags: colombia, Frank Pearl, Humberto de la Calle, Panama Papers
Slug: humberto-de-la-calle-y-frank-pearl-negociadores-del-gobierno-con-las-guerrillas-evaden-impuestos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/negociadores-de-paz-y-papeles-panama.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: Presidencia 

#### **[Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/) -[@[Abiliopena]

###### 19 Abr 2016

Como ocurrió con los Wikileaks, el escándalo de los Papeles del Panamá, ya está pasando. Razones habrá por parte de los sectores de poder para haberlos publicado, si tenemos en en cuenta que es una fuerte alianza de medios la que los sacó a la luz. Eso no quita la verdad de lo que revelan: la posible evasión de impuestos, lavado de activos y una expresión más de la crisis de un modelo económico que en sí mismo protege las riquezas de las personas muy ricas que se amparan en la corrupción del sistema financiero y que desprecia a los empobrecidos, por ejemplo, expulsando de sus viviendas cuando no pueden pagar las cuotas de usura de los bancos.

El escándalo de los papeles de Panamá ha dejado notables adinerados del mundo damnificados. De un lado las mas sonadas son las renuncias del ministro español de industria, la del primer ministro de Islandia, la del director de Chile Transparente, la del cónsul de Panamá en Frankfurt Alemania. En Londres sus ciudadanas se han movilizado para pedir la dimisión del primer ministro David Cameron.

Por los lados de Colombia, un país anclado en la mafiosa corrupción, nada pasa. Hasta la notable periodista Darcy Quinn que nos sorprendía con algunas investigaciones en caracol radio, ahora calla sobre el tema, su esposo el industrial Alberto Ríos aparece implicado en el escándalo.

Entre las personas que figuran en “Los papeles de Panamá” están, nada más ni nada menos, que los dos jefes de delegación del gobierno en los diálogos con las guerrilla de las Farc-Ep y con el Eln, **Humberto de la Calle y Frank Pearl**. Los dos han sabido a sumir la orientación del presidente de que “el modelo no se discute” y “ el modelo no se negocia”. Ellos mismos beneficiarios de la acumulación de riqueza, tienen fondos adicionales a los que seguramente tendrán en el país, para montar empresas en Panamá, país que ha sido declarado Paraíso Fiscal por el gobierno Francés.

Por su parte el ex director de la DIAN **Juan Ricardo Ortega**, afirma que el gran negocio de Panamá es la evasión fiscal, pues “[allá va a parar buena una parte de la plata de la corrupción política, del crimen organizado y de la evasión de impuestos](http://www.eltiempo.com/mundo/latinoamerica/entrevista-con-exdirector-de-la-dian-sobre-evasion-de-impuestos-en-colombia/16559588)”. Y ante las excusas generalizadas de que las empresas creadas han estado o están inactivas afirma que “eso es un cuento chimbo” y da cifras de lo que cuesta sostener una empresa en Panamá que debe tener al menos un millón de dólares para que se justifique ese gasto.

Las respuestas de De la Calle y de Perl desdicen enormemente del “Estado de Derecho” que dicen representar. De la Calle afirma que lo hizo por razones de seguridad. De ser cierto, su justificación tiene serias implicaciones. El Estado que representa, no tiene la capacidad de defender su riqueza, por eso acude a otro para salvaguardarla y aún así afirma que el modelo no se negocia, que no se requieren cambios estructurales. La respuesta de De la Calle no está acompañada de soporte alguno que despeje la obvia duda que nos ronda de que estuviera evadiendo impuestos, con lo que si no se los robaran, habría algo de distribución para paliar, los deficit en salud, educación, vivienda, empleo que hay en nuestro país. Sería deseable, como ha ocurrido en otros países, que al menos exponga públicamente su declaración de renta, como ha dicho estar dispuesto a hacerlo. Lo mismo dijo Alvaro Uribe hace algunos años al ser cuestionada la acelerada riqueza de sus jóvenes hijos, pero ésta es la hora en que no la ha mostrado.

Por los lados de Pearl, la respuesta no es más satisfactoria que de la De la Calle, ante revelaciones que lo comprometen a aún más. Tiene dos compañías figurando en el escándalo, una claramente activa y la otra en la que afirma que nunca generó dividendos. Dice en su defensa, que la legislación colombiana permite tener empresas en el exterior. Así sea de dientes para fuera, no ofreció publicar su declaración de renta y patrimonio.

Sobre las posibles responsabilidades penales, según [Caracol Radio](http://caracol.com.co/radio/2016/04/06/judicial/1459917776_353631.html), la Fiscalía ha establecido una lista de 231 clientes colombianos. De acuerdo con “ las primeras indagaciones estos clientes habrían constituido 1861 offshore, lo que significaría un promedio de 8 empresas por cada cliente, lo que a juicio de la fiscalía despierta una sospecha si la verdadera intención era la de esconder capitales y evadir impuestos”

De la Calle y Pearl, dignos representantes del injusto modelo de sociedad que ha repetido a los cuatro vientos que no está en discusión en las mesas de diálogo del gobierno con las guerrillas, al menos deberían tener la decencia de mostrar sus declaraciones de renta. Para hacer que renuncien, nos falta mucha fuerza social y más aún para intentar construir un modelo que garantice los mínimos vitales a las mayorías pobres de nuestro país.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
