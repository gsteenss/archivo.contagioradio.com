Title: "Dictamen sobre Dora Lilia Gálvez es impreciso"
Date: 2017-01-16 19:34
Category: Mujer, Nacional
Tags: Dora Lilia Gálvez, feminicidios en Colombia, Violencia de género, Violencia de género en Colombia
Slug: dictamen-dora-lilia-galvez-impreciso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Dora_Lilia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Caracol] 

###### [16 Ene 2017] 

Así lo aseguraron sus familiares, quienes denuncian que el dictamen de Medicina Legal, el cual asegura que Dora Lilia Gálvez falleció por causas naturales y no por la violencia perpetrada en su contra, registrada y corroborada por el Hospital de Buga, "es impreciso y le quita responsabilidad a los entes de salud, judiciales y al agresor".

### Testimonio de la familia V.s Medicina Legal 

Rosa Elvira Castillo integrante del movimiento de mujeres de Cali, señaló que **la información de medicina legal se contrapone al testimonio de Luis Fernando Gálvez, hijo de Dora Lilia,** quien dijo a medios de comunicación locales, que el 6 de noviembre encontró a su mamá "desnuda, en el suelo, con moretones y quemaduras en sus labios, brazos, piernas y genitales". Le puede interesar: [Durante el 2016, 100 mil mujeres han sido violentadas en Colombia.](https://archivo.contagioradio.com/2016-100-mil-mujeres-han-sido-violentadas-colombia/)

Los familiares de Dora Lilia han manifestado que desconfían "de los criterios de una institucionalidad que no ha cumplido con los protocolos ni con la investigación rigurosa, y que además **no tiene los argumentos fundamentados y suficientes para probar que no se cometió violencia contra Dora, y que claramente vimos que sí fue así** (…) no aceptamos las declaraciones del Instituto Nacional de Medicina Legal y Ciencias Forenses como definitivas para concluir sobre el caso".

Para desmentir la información entregada por Medicina Legal y hacer un sentido homenaje a Dora Lilia, sus familiares, amigos y el movimiento de mujeres de Cali convocan a la ciudadanía para asistir el próximo 17 de enero a la Casa Diana de Buga, Cra. 13 \# 10 – 59 desde las 10:00 de la mañana, a una rueda de prensa y elaboración de un mural. Le puede interesar: Dora Lilia Gálvez,[otra víctima de la violencia feminicida.](https://archivo.contagioradio.com/dora-liliana-galvez-otra-victima-de-la-violencia-feminicida/)

Por último, Rosa Elvira Castillo indicó que el movimiento de mujeres hará veeduría de la investigación en el caso de Dora y que “ahora más que nunca **debemos estar unidas para que nuestras niñas y nuestras mujeres sean valoradas y respetadas por nuestra sociedad,** para que no falte ni una mujer más en el país y se castigue a los perpetradores con todo el peso de la ley”.

<iframe id="audio_16388176" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16388176_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
