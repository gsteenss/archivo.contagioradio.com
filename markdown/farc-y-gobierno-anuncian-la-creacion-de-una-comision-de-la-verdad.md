Title: FARC y gobierno anuncian la creación de una Comisión de la Verdad
Date: 2015-06-04 14:21
Category: Nacional, Paz
Tags: Conversacioines de paz en Colombia, Cuba, FARC, Juan Manuel Santos, Noruega, Rodolfo Benítez Verson
Slug: farc-y-gobierno-anuncian-la-creacion-de-una-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/paises-garantes-paz-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: dialogosdepaz 

###### [4 Jun 2015] 

**Rodolfo Benítez Verson**, delegado de Cuba para el proceso de conversaciones de paz, anunció hoy desde la Habana que se creará una Comisión de la Verdad, una vez se hayan firmado los acuerdos de paz entre el gobierno colombiano y la **guerrilla de las FARC**. Esta Comisión de la Verdad tendrá un tiempo limitado de duración, tendrá participación de las víctimas, y la información que se recoja servirá para el reconocimiento de las mismas.

“*Comisión deberá contribuir al esclarecimiento de lo ocurrido y ofrecer explicación de la complejidad del conflicto. Deberá promover al reconocimiento de las víctimas, cuyos derechos fueron vulnerados*”. También agregó  que la **Comisión de la Verdad** será independiente, funcionará durante tiempo limitado, y tendrá participación de las víctimas.

"L*os esfuerzos de la Comisión estarán centrados en garantizar la participación de las víctimas del conflicto, asegurar su dignificación y contribuir a la satisfacción de su derecho a la verdad en particular y en general de sus derechos a la justicia, la reparación integral y las garantías de no repetición; siempre teniendo en cuenta el pluralismo y la equidad*", indican el comunicado conjunto.

Así las cosas, esta comisión tiene tres objetivos:

1.  Contribuir al esclarecimiento de lo ocurrido y ofrecer una explicación amplia de las razones del conflicto en Colombia.
2.  Promover el reconocimiento de las víctimas como ciudadanos que vieron sus derechos afectados y el reconocimiento voluntario de responsabilidades individuales y colectivas.
3.  Promover la convivencia en los territorios por medio de un ambiente de diálogo y la creación de espacios en que las víctimas se vean dignificadas.

Este es el documento completo elaborado por las partes...

[Comisión de La Verdad en Colombia](https://es.scribd.com/doc/267817202/Comision-de-La-Verdad-en-Colombia "View Comisión de La Verdad en Colombia on Scribd")

<iframe id="doc_41520" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/267817202/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="70%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
