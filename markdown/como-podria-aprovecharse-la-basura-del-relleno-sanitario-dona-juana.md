Title: ¿Cómo podría aprovecharse la basura del relleno sanitario Doña Juana?
Date: 2016-05-20 11:55
Category: Ambiente, Nacional
Tags: asamblea popular del sur, botadero doña juana, Relleno sanitario Doña Juana
Slug: como-podria-aprovecharse-la-basura-del-relleno-sanitario-dona-juana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Relleno-Doña-Juana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FM] 

###### [20 Mayo 2016 ]

Desde el año 2005 los habitantes de los 136 barrios y las 7 veredas que conforman la mitad de Ciudad Bolívar y Usme, y que han sido declarados como zona de influencia del relleno sanitario 'Doña Juana', han venido investigando sobre tecnologías de aprovechamiento de residuos sólidos para la producción de energías alternativas, a fin de lograr el **cierre del basurero y el pago de la deuda social** por los impactos ambientales y en la salud de los pobladores.

De acuerdo con Javier Reyes, miembro del 'Proceso Social Asamblea Sur', en cinco años es posible pagar los US\$1200 millones que podría costar una planta para la **gasificación de residuos sólidos y producir derivados del petróleo como el etanol**, de esta forma se sustituiría el enterramiento de la basura que está impactando negativamente las fuentes de agua, el aire, los territorios y a las comunidades.

Otra de las técnicas para el aprovechamiento de residuos es la termolisis, que básicamente consiste en el rompimiento de moléculas con presiones de temperatura para generar gas y mover turbinas para la producción de electricidad. De acogerse esta tecnología, podría **evitarse la construcción de las ocho represas que Codensa pretende poner en funcionamiento en el Sumapaz**, según afirma Reyes.

Los biodigestores, son otra de las tecnologías que las comunidades proponen, teniendo en cuenta que con estas plantas se aprovecharían los residuos orgánicos, previamente seleccionados en zonas de separación de residuos, de tal modo que **"la combinación de diferentes tecnologías nos permitiría manejar nuestros propios residuos sin afectar la región"**, asegura Reyes.

Pese a que los pobladores han buscado dialogar estas propuestas con la administración del distrito, las mesas de concertación que se habían establecido, se levantaron sin acuerdos sobre la metodología para el cierre del basurero y el pago de la deuda social. Por su parte la CAR aseguró que **antes de septiembre de este año la alcaldía debe presentar su propuesta de sustitución del relleno**.

Mientras que la administración rompe con la mesa de diálogo, los pobladores continúan enfrentando los impactos sociales, ambientales y económicos del relleno sanitario y que se traducen en **enfermedades respiratorias, epidérmicas y digestivas que no tienen ningún tipo de atención diferencial** en las 3 Unidades de Atención Primaria con las que estos barrios cuentan

Es por esta situación que los pobladores **han convocado para este sábado una asamblea a la que citaron al Secretario General de la ONU, a Juan Manuel Santos**, al Presidente de la Comisión Quinta del Senado, a los Ministros de Minas y Energía, Salud, Medio Ambiente y Vivienda, al Defensor del Pueblo, al Director de la CAR y a los rectores de diferentes universidades, con el fin de se acuerde la [[sustitución del relleno sanitario](https://archivo.contagioradio.com/comunidades-reclaman-cierre-definitivo-del-relleno-dona-juana/)].

La asamblea se llevará a cabo en la **vereda Mochuelo Alto de Ciudad Bolívar este sábado desde las 8 de la mañana**, y sí usted quiere hacer parte, podrá llegar a la sede tecnológica de la Universidad Distrital, o a la Casa de la Justicia de Ciudad Bolívar o al barrio Las Quintas, dónde habrán buses dispuestos para transportarle hasta el lugar del encuentro.

<iframe src="http://co.ivoox.com/es/player_ej_11603548_2_1.html?data=kpajkpiZeJmhhpywj5eVaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncavV187S1JC2qdrZ1JCajbXWs8TZ1NSYtdTHrcLgjKbgw9LGsMbVjLji1JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
