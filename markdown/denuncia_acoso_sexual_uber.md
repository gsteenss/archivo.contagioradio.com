Title: Denuncian un nuevo caso de acoso sexual en Uber
Date: 2017-10-04 16:31
Category: Mujer, Nacional
Tags: agresión sexual, Uber, violencia sexual
Slug: denuncia_acoso_sexual_uber
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/uber_risk-1200x800-e1507148819593.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Metro PR] 

###### [4 Oct 2017 ] 

Eliana Riaño, defensora de los derechos de las mujeres, no imaginó que un en servicio como Uber, que se vende como un servicio seguro, fuera el escenario de un acto de violencia sexual contra ella. "**El conductor empezó a hacer comentarios sobre mi cuerpo, sobre mi sexualidad, y empezó a tocarse"**, cuenta Riaño.

De acuerdo con su relato, sobre las 2:20 de la tarde del lunes 2 de Octubre decidió tomar un servicio de Uber. Como es usual se sentó en el puesto del copiloto, pero después de unos minutos el conductor identificado en la aplicación como "Eduardo A" empezó a hacerle comentarios sexuales que generaron rabia, pero sobre todo temor al verse encerrada en un carro en el que podía sucederle cualquier cosa.

"Uno se sube adelante para proteger a los conductores de Uber de los taxistas, y resulta que a las mujeres nos ponen en riesgo por salvaguardarlos a ellos", expresa Eliana. Su reacción no fue violenta. Prefirió llamar a una amiga durante su ruta como herramienta para que ese sujeto supiera que ya alguien conocía el camino y el carro en el que iba. "Estaba mencionando mi trayecto en aras de defenderme al estar al lado de una persona que es un agresor, mientras yo tenía un cinturón. **No le hice ningún reclamo, porque estaba en un momento de vulnerabilidad, estaba en un territorio que no era mio, era su carro"**.

### **¿Cómo deben ser la reacción antes estas situaciones?** 

Como feminista, Eliana Riaño recomienda analizar bien la situación en la que se encuentran las mujeres si llegan a ser víctimas de algún tipo de agresión sexual. En el caso de ella, considera que lo mejor era no hacer ningún tipo de reclamo pues se encontraba complemente vulnerable al estar en un carro en el que en cualquier momento el conductor hubiese cambiado de rumbo y la pudo haber llevado a otro lugar y eso podría ser mucho peor.

"Hay imaginarios de que el defenderse debe ser de forma violenta, pero se debe equilibrar el nivel de riesgo en el que nos encontramos. Si hubiese reclamado de manera violenta, eso podía haber hecho que esa persona reaccionara de una forma peor", y agrega "**las reacciones hay que analizarlas bien, mi reacción fue llamar para poner en alerta a una amiga**".

### **La respuesta de Uber** 

Dos horas después de que sucedió la agresión sexual, la víctima decidió hacer la respectiva denuncia ante la plataforma, pero se encontró con un primer obstáculo: la ruta para hacer las denuncias en la aplicación no es clara. Luego de haber hallado la forma de poner la denuncia, Riaño no recibió respuesta.

El martes volvió a escribir la denuncia con mayor información, pidiendo los datos del agresor, y señalando que lo que había pasado era un delito tipificado. "Les escribí que tenían que hacerse responsables porque podían ser cómplices de un agresor que puede estar violentando a otras mujeres".

Fue hasta las 10:30 de la noche del martes que Uber respondió la denuncia de Eliana, quienes le señalaron que por protección de datos no podían dar dicha información. "Cuando terminas el viaje, se borra la placa del carro, así que solicité esos datos para hacer la denuncia penal, pero no me la quisieron dar. Los conductores tienen toda nuestra información, hasta la dirección a la que vamos, mientras que Uber no nos da los datos del conductor".

**Lo único que respondió Uber fue que esta no era la primera vez que llegaban este tipo de denuncias contra ese hombre y** que por eso lo sacarían de la plataforma. Hecho que no ha podido ser comprobado por la usuaria.

### **¿Cómo debió actuar Uber?** 

Como integrante de la Red de Salud de Mujeres Latinoamericanas y del Caribe, Eliana Riaño indica que lo primero es que Uber priorice el nivel de respuesta a las quejas de los usuarios. En este caso, **dejó pasar más de 30 horas**. Un tiempo en el que otras mujeres pudieron haber sido víctimas del conductor que agredió a Eliana.

"La empresa debería dar una respuesta de inmediato, al vender un servicio en una lógica de la seguridad" además debe entregar los datos pertinetes para que las mujeres víctimas de este tipo de hechos puedan poner las denuncias. Asimismo, Uber debería actuar a la primera queja por agresión sexual que tenga una usuaria, y no dejar que sea una segunda denuncia la que los obligue a sacar de esta actividad al conductor.

Cabe recordar que esta **no es la primera denuncia que se hace contra la plataforma de Uber.** En mayo de este año la víctima había sido una joven de 18 años a quien un conductor le hizo preguntas incómodas y le tocó la entrepierna. La respuesta de ese momento de Uber fue la misma, aseguró que bloquearía al hombre agresor aunque la familia de la joven tomó acciones jurídicas contra la aplicación.

Ante tal situación, Eliana Raño ha decidido hacer algunas recomendaciones para las mujeres:

*"1- Tomar pantallazos de TODOS los servicios que pida antes de llegar a mi destino, para así coger sus placas antes.*  
*2- Llamar al conductor antes para guardar su número de celular.*  
*3- En el viaje llamar a alguien cercano y decir que voy en un UBER e indicar el trayecto.*  
*4- Si voy sola, me subo atrás, no voy a violar mi seguridad para mantener la de los conductores. (Estar sentada al lado de un posible agresor, nos pone en más riesgo)."*

<iframe id="audio_21274237" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21274237_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div>

</div>
