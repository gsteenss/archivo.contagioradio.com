Title: Panfleto del Quintín Lame ¿una estrategia para justificar asesinatos de líderes?
Date: 2019-01-30 14:21
Author: AdminContagio
Category: Comunidad, DDHH, Líderes sociales
Tags: guardia indígena, Norte del Cauca, ONIC, Quintin Lame
Slug: panfleto-del-quintin-lame-una-estrategia-para-justificar-asesinatos-de-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/NASA1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Victoria Argoty] 

###### [30 Ene 2019] 

A través de un comunicado, integrantes de la comunidad Nasa, excombatiente del Movimiento Quintín Lame, una guerrilla indígena de 1984 y desmovilizada en 1991, **negó su responsabilidad con panfletos en los que se incitaba a los pueblos indígenas del Norte** del Cauca a tomar las armas y afirmaron que la Guardia Indígena, es una estructura que aboga por la construcción de paz.

Luis Acosta, coordinador de la Guardia Indígena expresó que "rechazan este panfleto que pone en riesgo a la Guardia Indígena y a las autoridades", y señaló que la región del Norte del Cauca, ha venido siendo víctima de hechos de violencia, como los asesinatos a los gobernadores indígenas, las amenazas y la persecución a integrantes de los cabildos.

Frente a ese panorama, Acosta aseguró que la posición política indígena ha sido el repudio a cualquier acción violenta, razón por la cual su Guardia porta un bastón de mando y no un arma. Además enfatizó en que el panfleto debe provenir de **"quienes quieren profundizar la guerra y no la reconciliación".**

> [\#AlAire](https://twitter.com/hashtag/AlAire?src=hash&ref_src=twsrc%5Etfw) "Yo creo que el llamado a retomar el Quintín Lame es una estrategia para justificar asesinatos contra el movimiento indígena, es un plan de exterminio contra quienes estamos con y por la paz": Luis Acosta, [@ACIN\_Cauca](https://twitter.com/ACIN_Cauca?ref_src=twsrc%5Etfw), [@ONIC\_Colombia](https://twitter.com/ONIC_Colombia?ref_src=twsrc%5Etfw). <https://t.co/mBizQIbcmt>
>
> — Contagio Radio (@Contagioradio1) [30 de enero de 2019](https://twitter.com/Contagioradio1/status/1090621366368387074?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **La responsabilidad del gobierno en la desprotección a las comunidades indígenas** 

Acosta afirmó que en reiteradas ocasiones le han exigido al Estado Colombiano protección a líderes sociales y comunidades indígenas y hasta el momento no se ha generado ningún consejo de seguridad, "ni se ha prestado atención a esta situación".  (Le puede interesar: ["Asesinan a Dilio Corpus Guetio, miembro de la guardia campesina en Cauca"](https://archivo.contagioradio.com/dilio-corpus-guetio/))

Motivo que ha llevado a los resguardos indígenas a fortalecer su guardia, a través de campañas comunicativas en las radios comunitarias, en donde invitan a la juventud a unirse a ese grupo de defensa de la autonomía indígena y rechazar cualquier llamado de grupos armados de unirse a ellos.

Asimismo, el Coordinador de la guardia expresó que la decisión de las comunidades indígenas ha sido rechazar la intervención de **cualquier actor armado en sus territorios, razón por el cual les piden que salgan de estos lugares**.

<iframe id="audio_32001847" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32001847_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
