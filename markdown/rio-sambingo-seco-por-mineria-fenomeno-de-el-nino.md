Title: Minería legal e ilegal dejó seco al río Sambingo en el Cauca
Date: 2016-02-01 20:12
Category: Ambiente, Nacional
Tags: Cauca, Fenómeno del Niño, Mineria, río Sambingo
Slug: rio-sambingo-seco-por-mineria-fenomeno-de-el-nino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Rio-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ejército Nacional 

<iframe src="http://www.ivoox.com/player_ek_10287360_2_1.html?data=kpWfmpyXepGhhpywj5WcaZS1lp6ah5yncZOhhpywj5WRaZi3jpWah5ynca7dz8rfh6iXaZm4wpDZx8zFsIzZjM7Zx8zFsIzYxs%2BSpZiJfZSf1MrQ0ZDFsIzmhqigh52os4znwtLPy9PLs4zZz5Cah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Wilder Meneses] 

###### [1 Feb 2016] 

El río Sambingo, ubicado en el departamento del Cauca, se encuentra totalmente seco, y aunque este tipo de hechos el gobierno nacional los asocia con las altas temperaturas generadas por el fenómeno de El Niño, las comunidades que viven del río aseguran que más allá de eso, **el río ha perdido el agua por de la actividad minera legal e ilegal que se realiza en la zona.**

Wilder Meneses, integrante del Comité de Integración del Macizo Colombiano, afirma que la intervención del río se viene desarrollando **hace 1 año cuando entró la maquinaria ilegal,** y aunque la población ha hecho las denuncias correspondientes al Ministerio de Ambiente, Interior y Minas y Energía, la comunidad denuncia que no se ha hecho nada para impedir la actividad ilegal.

Para los pobladores, el Estado es quien tiene la mayor parte de la responsabilidad frente a la sequía que atraviesa el río Sambingo, que afecta a aproximadamente a **80 mil personas que viven del caudal del afluente,** además, según el Ejército esa situación ha generado la extinción de varias especies de animales en la zona, y la pérdida de 360 hectáreas de bosque nativo.

La comunidad asegura que hay dos batallones en el lugar, pero la fuerza pública no ha hecho nada para detener la actividad minera, **“las autoridades no hacen efectivas sus labores de control y en cambio con permisivos”,** dice Meneses, quien añade que los líderes y organizaciones que han decidido interponer las denuncias han sido amenazados.

Desde el Ejército Nacional, se publicaron las fotos en las que se visibiliza la total sequía del río que es afluente del río San Jorge y del río Patía. Las fuerzas militares han asegurado que **“para la recuperación de la zona, se requerirían cerca de 100 mil millones,** lo que equivale a la construcción de cerca de tres mil viviendas de interés social y casi 100 años de trabajo ambiental, para lograr la reforestación y recuperación de los niveles básicos de habitabilidad”.

Ante la falta de respuesta estatal, la comunidad se ha declarado en asamblea permanente para impedir la entrada de la maquinaria, y el próximo 13 de febrero se tiene planeada una movilización en los municipios afectados.

### **En contexto:** 

Según los datos entregados por las propias comunidades en la audiencia pública realizada el pasado 27 de Noviembre de 2015, en la ciudad de Popayán, sobre el territorio del departamento hay presencia de intereses mineros y energéticos que abarcan 1.913.943 de las 3.101.532 hectáreas que tiene el departamento y en las cuales habitan cerca de un millón trescientos mil habitantes, lo que indica que la población estaría siendo fruto de confinamiento.

Según esas misas cifras, en el Cauca hay 259 títulos mineros en 380.654 hectáreas y unas 386 solicitudes sobre 681.701 hectáreas, es decir que más de la tercera parte del territorio se enfrenta a los intereses de **las empresas mineras lideradas por la Anglo Gold Ashanti, que a través de diversas filiales con 39 títulos p**ara un total de 61.000has y otras 46 solicitudes para otros 137.000 hectáreas.

En cuanto al impacto ambiental que denuncian las comunidades el panorama es más que alarmante, puesto que en 16 de los municipios con mayor densidad poblacional, **por lo menos 29 fuentes de agua entre ríos y quebradas se están viendo afectadas** y esto podría provocar una crisis ambiental más grave que la actual.

 
