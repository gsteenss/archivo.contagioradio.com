Title: Inició verificación del Cese Bilateral
Date: 2016-11-05 17:44
Category: Nacional, Paz
Slug: verificacion-del-cese-bilateral-inicia-el-lunes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/verificación-cese-bilateral.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Flick] 

###### [04 Nov 2016] 

El Mecanismo tripartiro de verificación del Cese Bilateral iniciará su trabajo formal el próximo lunes. Así lo anunciaron los voceros de la ONU, las fuerzas militares y las FARC este viernes en rueda de prensa. Explicaron que la verificación se dará en fases, la primera de ellas se ubicándose en 8 zonas del país donde hay pre concentración de integrantes de las FARC.

El mecanismo funcionará en una sede nacional en Bogotá, 8 sedes regionales y 27 sedes locales, según lo explicó el General Pérez Aquino, Jefe de los observadores internacionales de la Misión de la ONU en Colombia, quien agregó que la diferencia con el mecanismo establecido es que la verificación será a través de visitas a los puntos de pre agrupamiento.

También explicaron que en esta fase no se realizará la verificación de la dejación de armas, puesto que ese proceso se inicia una vez se ratifique el acuerdo de paz, que en este momento se está reestructurando, luego del resultado del plebiscito por la paz del pasado 2 de Octubre. El Mecanismo se concentrará en la verificación del Cese Bilateral.

Tanto el delegado del gobierno, el Contralmirante Orlando Romero, como el delegado de las FARC, Marco León Calarcá, ratificaron el compromiso con el Cese Bilateral y con la construcción de la paz estable y duradera. Calarcá también informó que 300 integrantes de las FARC harán parte de ese mecanismo.

### **Se blinda el Cese Bilateral** 

Aunque el Cese Bilateral se firmó el 22 de Junio de este año, entró en vigor a partir del decreto presidencial y luego de los resultados del plebiscito tuvo que extenderse hasta el 31 de Diciembre de 2016 para que su pueda revisar el acuerdo sin que volviera la confrontación.

Con este anuncio una de las preocupaciones tanto a nivel nacional, como internacional se comienzan a disipar puesto que, en las condiciones actuales, el Cese Bilateral se entendía como frágil porque podrían darse provocaciones que lo pusieran en riesgo.

Este es el link al video de la rueda de prensa <https://www.youtube.com/watch?v=jIbrzv1JtBE>
