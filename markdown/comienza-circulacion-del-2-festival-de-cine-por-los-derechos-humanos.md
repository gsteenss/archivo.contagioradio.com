Title: Comienza circulación del 2° Festival de Cine por los Derechos Humanos
Date: 2015-08-31 17:30
Author: AdminContagio
Category: 24 Cuadros, eventos
Tags: 2° Festival de Cine por los Derechos Humanos, Archivo de Bogotá, Diana Arias, Fundación Konrad Adenauer, Impulsos Films
Slug: comienza-circulacion-del-2-festival-de-cine-por-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/ER29_0022_FernandoBrachoBracho.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Fernando Bracho (detrás de cámara de "El Regreso") 

###### [31 Ago 2015] 

Los días 1 y 2 de septiembre, inicia la circulación de la selección oficial del la más reciente edición del Festival Internacional de Cine por los Derechos Humanos de Bogotá; con una muestra de 15 cortometrajes nacionales y las producciones que conformaron la categoría "Nuevos Realizadores".

El Archivo de Bogotá, es el espacio escogido para la muestra itinerante, en el que las y los capitalinos podrán apreciar de manera gratuita, las diferentes producciones que abordan temáticas enfocadas a la construcción y defensa de los derechos humanos en Colombia y el mundo.

[![CINE DDHH](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/cine-ddhh.jpg){.aligncenter .size-full .wp-image-13098 width="1697" height="656"}](https://archivo.contagioradio.com/?attachment_id=13098)

La circulación del Festival continuará en ciudades como Ibagué, Cartagena, Medellín, Pereira, Soacha, Londres, París y Barcelona.

**Hora:** 2:00 p.m.

**Lugar:** [Archivo de Bogotá](http://www.archivobogota.gov.co/) (Calle 6b n.° 5-75)

**Costo:** Entrada Libre.

**Sobre el Festival**

La segunda edición del Festival Internacional de Cine por los Derechos Humanos–Bogotá, se celebró entre el 7 y el 11 de abril y contó con una selección oficial de 80 producciones audiovisuales de 23 países en competencia. * *(Ver [Bogotá Inaugura el II Festival de Cine por los Derechos Humanos](https://archivo.contagioradio.com/hoy-es-el-lanzamiento-del-festival-de-cine-y-derechos-humanos-de-bogota/))

Premió a cinco obras audiovisuales en las categorías: Cortometraje Nacional, Cortometraje Internacional, Documental Nacional, Documental Internacional y Largometraje, y entregó dos menciones especiales en las categorías Cortometraje Internacional y Documental Internacional. (Ver [Estos son los ganadores del 2° Festival de Cine por los Derechos Humanos](https://archivo.contagioradio.com/estos-son-los-ganadores-del-2-festival-de-cine-por-los-derechos-humanos/))

Por su parte la Fundación [Konrad Adenauer](http://www.kas.de/kolumbien/es/)[,](http://www.kas.de/kolumbien/es/) con su Programa Estado de Derecho para Latinoamérica, galardonó a cuatro de las más de 200 producciones de la categoría Nuevos Realizadores.
