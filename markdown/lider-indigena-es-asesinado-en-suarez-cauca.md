Title: Líder indígena es asesinado en Suárez, Cauca
Date: 2020-12-01 15:15
Author: AdminContagio
Category: Actualidad, Líderes sociales
Tags: asesinato de líderes sociales, Cauca, Líder Indígena
Slug: lider-indigena-es-asesinado-en-suarez-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-01-at-2.32.01-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este lunes 30 de noviembre, fue asesinado el comunero y líder indígena **Romelio Ramos Cuetia en el municipio de Suárez, departamento del Cauca. Ramos era líder social y trabajaba por la vida comunitaria del municipio de Jambaló.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1333781569442615296","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1333781569442615296

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según el [Instituto de estudios para el desarrollo y la paz (Indepaz](http://www.indepaz.org.co/lideres/)), Romelio Ramos Cuetia, de 31 años de edad, había desaparecido el domingo 29 de noviembre luego de salir de la celebración de un cumpleaños en el corregimiento de Betulia. Su cuerpo fue hallado sobre la vía en el sector de El Amparo, a dos kilómetros y medio del casco urbano del municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El líder indígena, residente de la vereda Alejandría del resguardo de Cerro Tijeras se desempañaba como agricultor. Además, tenia una esposa y era padre de dos hijos. (Le puede interesar: [971 defensores de DD.HH. y líderes han sido asesinados desde la firma del Acuerdo de paz](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con Ramos, **ya son 261 los lideres, lideresas y defensores de derechos humanos asesinados en lo corrido de este año**, 84 de estos crímenes ocurridos en el departamento del Cauca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo al informe más reciente presentado por el programa Somos Defensores, **entre los meses de julio a septiembre de 2020 se registraron 184 agresiones contra personas defensoras de derechos humanos en Colombia**, siendo el departamento del **Cauca el más afectado con 48 casos (25%) registrados**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El liderazgo más agredido fue el indígena, con 69 casos que corresponden al 37%**, señala el informe. Ante esta situación, las comunidades y los liderazgos indígenas exigen garantías de vida pues continúan siendo blancos de amenazas y asesinatos. (Le puede interesar: [Agresiones contra defensores de DD.HH. ascendieron a 184 entre julio y septiembre](https://archivo.contagioradio.com/184-agresiones-contra-defensores-de-dd-hh-entre-julio-y-septiembre/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
