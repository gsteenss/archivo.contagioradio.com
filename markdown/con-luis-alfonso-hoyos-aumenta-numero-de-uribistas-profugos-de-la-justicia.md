Title: Con Luis Alfonso Hoyos aumenta número de uribistas prófugos de la justicia
Date: 2015-01-19 18:40
Author: CtgAdm
Category: Judicial, Política
Tags: chuzadas, das, paz, uribe, uribismo, zuluaga
Slug: con-luis-alfonso-hoyos-aumenta-numero-de-uribistas-profugos-de-la-justicia
Status: published

###### **Foto: Internet** 

Luis Alfonso Hoyos, asesor de la campaña presidencial del candidato Uribista, Oscar Iván Zuluaga, habría salido del país y según fuentes cercanas al Centro Democrático se encuentra buscando asilo político en Estados Unidos.

Esta noticia se da luego de que el 18 de enero, la Fiscalía solicitara audiencia de imputación de cargos contra Hoyos y David Zuluaga (hijo del ex-candidato presidencial, quien se encuentra en los EEUU cursando Estudios Políticos) por su participación en el caso del "hacker" Andrés Sepulveda.

La salida de Luis Alfonso Hoyos se suma a los casos de personas investigadas y condenadas como Luis Carlos Restrepo, investigado por la falsa desmovilización del Bloque Cacica Gaitana; el ex-Ministro de Agricultura Andrés Felipe Arias, condenado a 17 años de prisión por el desfalco de Agro Ingreso Seguro; y María del Pilar Hurtado, Ex-Directora del DAS, investigada por actividades ilegales de ese organismo durante el gobierno de Alvaro Uribe.

Entre tanto, el ex-candidato presidencial del Centro Democrático, Oscar Iván Zuluaga, ha manifestado que se declarará "víctima" dentro del proceso de investigación por las interceptaciones ilegales, ante la Corte Suprema de Justicia y anunció que interpondrá una tutela contra la fiscalía por este mismo caso.
