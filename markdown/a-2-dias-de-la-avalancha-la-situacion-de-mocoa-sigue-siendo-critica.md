Title: Los más pobres las principales víctimas de la avalancha en Mocoa, Putumayo
Date: 2017-04-03 13:27
Category: DDHH, Entrevistas
Tags: avalancha, Mocoa, Putumayo
Slug: a-2-dias-de-la-avalancha-la-situacion-de-mocoa-sigue-siendo-critica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/mocoa-9-e1491162791275.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ejército Nacional] 

###### [03 Abr. 2017] 

Los habitantes de los 20 barrios arrasados por la avalancha en Mocoa eran, **en su mayoría, personas de escasos recursos, víctimas de desplazamiento y personas pobres de otras regiones que quisieron probar suerte en el Putumayo**, sin embargo la falta de posibilidades de vida en dignidad, obligó a esas personas a construir sus viviendas y reconstruir sus vidas en las laderas de las quebradas.

Aunque la zona era de alto riesgo y todas las autoridades conocían la situación, se prefirió dar la espalda a una tragedia económica de cerca de 5000 personas, habitantes de 20 barrios. Por esa inoperancia de las autoridades que no atendieron la problemática hace 15 años cuando arrancó el poblamiento, es que hoy se tiene que atender una tragedia de las magnitudes conocidas. Así lo afirma Carlos Fernández defensor de DDHH.

Luego de 2 días de ocurrida la avalancha en la ciudad de Mocoa, departamento de Putumayo la solidaridad de organizaciones defensoras de derechos humanos, de las universidades y de la sociedad civil en general se ha volcado a este lugar del país, sin embargo, la situación continúa siendo crítica. Le puede interesar: [No fue un "desastre natural" la tragedia en Mocoa](https://archivo.contagioradio.com/no-fue-desastre-natural-lo-ocurrido-mocoa/)

Según Carlos Fernández, defensor de derechos humanos, integrante de la Comisión Intereclesial de Justicia y Paz "**la situación humanitaria es bastante compleja**. El desastre natural que se presenta **destruye casi en un 50% a Mocoa y desde el sábado hay una preocupación y un desespero generalizado** en la población intentando buscar a sus seres queridos, rescatar lo poco que dejó la avalancha”.

### **Las Cifras son una tarea difícil pero necesaria**

Según las cifras oficiales **hasta el momento serían 254 personas las que habrían fallecido** a causa de este desastre natural, **de los cuales 43 eran niños y niñas, así mismo, el número de heridos supera las 400 personas y otras 400 más desaparecidas**. Además, muchas personas se han victo en la obligación de desplazarse del lugar para salvaguardar sus vidas.

**“Nos encontramos sin agua, sin energía eléctrica, hay algunas personas que no han podido recibir alimento**. En algunos lugares solo pudieron recibir el almuerzo y bien tarde se pudo lograr el tema de la cena, es una situación humanitaria bastante compleja” recalcó Fernández.

Luego de la visita del presidente Juan Manuel Santos y los anuncios hechos en su alocución en la que prometió que Mocoa sería reconstruida rápidamente, la población de esta ciudad se encuentra esperanzada, pero “al mismo tiempo la realidad que están viviendo ha chocado un poco con esas voluntades que se han expresado. **Si bien ha habido una respuesta de solidaridad las condiciones siguen siendo muy complicadas”** relató Fernández.

### **Pueblo Indígena Nasa uno de los más afectados** 

En el lugar de los hechos también se encontraban cerca de 200 familias del pueblo Nasa, alrededor de 800 personas, quienes estaban teniendo una reunión de autoridades “hasta el momento hay un reporte de 34 personas del pueblo Nasa desaparecidas, aún se está en las labores de búsqueda y 7 personas confirmadas que fallecieron” aseveró Fernández.

### **¿Cómo se puede ayudar?** 

Sigue siendo prioritario que desde diversos lugares del país se haga llegar a Mocoa varios tipos de ayuda que pueden ser **alimentos no perecederos, medicinas y frazadas** “eso es lo básico, algunas personas están aún sin colchonetas en donde dormir, entonces duermen en el suelo, sin cobijas. En lo inmediato hay cosas que se pueden solucionar para tener condiciones para poder continuar en la búsqueda de las personas desaparecidas” relató el defensor de derechos humanos.

### **¿A dónde puedo ir si lo perdí todo en la avalancha?** 

En este momento Mocoa cuenta con varios albergues en los que las personas pueden pasar las horas mientras comienza la re-construcción oficial de la ciudad. Uno de ellos **es el albergue del Instituto Tecnológico del Putumayo, otro se encuentra ubicado en la OCIP y otro en el Coliseo del barrio Las Américas.**

“A cualquier de estos lugares se pueden dirigir quienes quieran albergarse o quienes quieran ubicar a sus familiares, porque al no haber luz las personas no han tenido como comunicarse con sus familias. Además, estamos aprovechando para hacer un censo” manifestó Fernández.

### **La planificación a futuro puede evitar otra tragedia** 

De igual modo Yuri Quintero, Diputada de la Asamblea Departamental ha dado a conocer que se han **enterado de personas inescrupulosas que se hacen pasar por víctimas de la avalancha para acceder a las ayudas** “eso lo tenemos que evitar, no podemos permitir que eso siga pasando y mucho menos en un momento como estos, lleno de tanto dolor e incertidumbre. **Uno ve en las filas personas que no salieron afectadas. Uno se indigna”.**

Ante esta situación la diputada Quintero manifestó que, aunque es lamentable lo que han pensado hacer es poder **revisar los censos de personas damnificadas se puedan comparar con los censos de Juntas de Acción Comunal** y con los que se han realizado en la población “es una comparación bastante bochornosa, es un proceso dispendioso pero que nos puede dar garantías de que la gente que es, son los que lo necesitan”.

### **Hay quienes se pretenden aprovechar del dolor de familias** 

Este tipo de accionar también se conoció por parte de **la Funeraria Normandía,** según una denuncia conocida por Contagio Radio en el que se dice que en Mocoa hay cuerpos que han sido identificados y **no los entregan aún a sus familiares, pues les están cobrando \$300 mil pesos para hacerlo.**

Las personas que se vieron afectadas por la avalancha son víctimas o desplazadas “en su gran mayoría por no decir todas son personas que aparecen hoy en los programas de desplazados, víctimas. Personas que han estado llegando y no se han vinculado a esos programas”.

La situación en el cementerio donde están los cuerpos de los fallecidos es muy tensionante. Ante la demora en el proceso de ingreso, los familiares están intentando ingresar por la fuerza.

Ahora las organizaciones sociales e instituciones que se encuentran en Mocoa continuarán su trabajo para la búsqueda de personas desaparecidas, así como por la alimentación y los albergues para las personas que se vieron afectadas.

“Lo que se está presentando en Mocoa es por no planificar, por permitir que la población crezca, pero sin hacer un estudio pertinente al tema. Quizás en unos 3 meses Mocoa pueda ser algo de lo que era antes” finalizo Quintero.

<iframe id="audio_17929916" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17929916_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_17930997" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17930997_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
