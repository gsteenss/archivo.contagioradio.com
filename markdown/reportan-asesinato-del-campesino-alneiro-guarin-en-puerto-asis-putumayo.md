Title: Reportan asesinato del campesino Alneiro Guarín en Puerto Asís, Putumayo
Date: 2019-06-07 17:58
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Asesinato de campesinos, Puerto Asís, Putumayo
Slug: reportan-asesinato-del-campesino-alneiro-guarin-en-puerto-asis-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/lideres-sociales-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

El pasado 5 de junio, en la vereda La piña en **Puerto Asís, Putumayo** fue asesinado **Alneiro Guarín** a manos de hombres armados que dispararon en cuatro ocasiones contra su cabeza, causándole la muerte de inmediato. Alneiro era oriundo del departamento del Guaviare y trabajaba en el sector de la porcicultura.

Testigos y habitantes de la comunidad señalan que el campesino no tenía problemas con nadie por lo que se desconocen los motivos de lo ocurrido. [(Lea también: Se agudizan agresiones contra campesinos que se acogieron a plan de sustitución en Putumayo)](https://archivo.contagioradio.com/se-agudizan-agresiones-contra-campesinos-que-se-acogieron-a-plan-de-sustitucion-en-putumayo/)

Noticia en desarrollo...

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
