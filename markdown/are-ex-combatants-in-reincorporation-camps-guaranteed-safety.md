Title: Are ex-combatants in reincorporation camps guaranteed safety?
Date: 2019-10-29 22:03
Author: CtgAdm
Category: English
Tags: assassinations of ex-combatants, FARC, reincorporation, reintegration camps
Slug: are-ex-combatants-in-reincorporation-camps-guaranteed-safety
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/zona-veredal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Contagio Radio] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[The assassination of Alexander Parra at the reintegration camp Mariana Páez in Mesetas, Meta on Oct. 24 accentuated the uncertainty and the lack of security guarantees that ex-combatants face despite the close presence of police and the military, tasked with protecting these official sites of reincorporation.]

[Tulio Murillo, a political adviser at the Mesetas camp, said that the security forces deployed to each one of the 24 official camps have allowed ex-combatants to experience a certain level of safety needed to develop their collective businesses. Still, cases like that of Alexander Parra, the first ex-combatant to be assassinated inside a camp, have caused widespread alarm.]

[“It’s the same that is happening in Cauca, in northern Santander, in Nariño. The regions most militarized in the country is where we see more massacres,” said Murillo, adding that he had previously alerted authorities to the presence of men, dressed in civilian clothes and carrying arms, riding down certain parts of the roads next to Mesetas. These sightings have been reported for months, Murillo said, and continued to occur in El Diamante and La Uribe even after Parra was killed.]

### "It's not an issue of military presence in the camps"

[Murillo said that the problem can’t be reduced to the number of soldiers deployed to offer the camps' safety rather it must be addressed by implementing the peace deal and dismantling the paramilitary groups that continue to operate in these regions. “We have military and police, but all of this continues to happen amidst the presence of security forces that should promise us the free right to political, economic and social reincorporation,” the advisor said.]

[The Attorney General’s Office joined the Special Investigation Unit to dismantle the criminal organizations responsible for the deaths of social and political leaders, but Murillo pointed out that the initiative has yet to produce results. In the case of Sebastián Coy Rincón, an ex-combatant who was also killed in Mesetas last year, there are still no reports of advances in the investigation.]

[The advisor said that the communities are in shock and fear following the assassination of Parra. He added that this targeted killing occurred only a few days before local elections and had the aim of discouraging voters from supporting FARC political candidates. Parra was the romantic partner of a FARC candidate running for city council in Mesetas.]

[Despite the outcry over the assassinations, Murillo said that the government has not taken the necessary measures to put a stop to these threats. “I don’t know how many more deaths we’ll have to put. We reached an agreement to disarm in return for our free right to political participation, a reincorporation with social justice and yet, a lot of us are threatened.”]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio.}
