Title: Por falta de atención oportuna han muerto 6 personas en la Cárcel La Picota
Date: 2016-08-08 14:54
Category: DDHH, Nacional
Tags: Presos cárcel la Picota
Slug: por-falta-de-atencion-oportuna-han-muerto-6-personas-en-la-carcel-la-picota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/carcel_colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zonacera] 

###### [8 de Agost] 

La última víctima por la falta de prestación de atención médica a tiempo en la Cárcel La Picota, fue **Juan Camilo Becerra Galvis, un joven de 23 años que murió por tuberculosis** el pasado sábado 6 de agosto. Con el ya son 6 los presos que han fallecido al interior de la penitenciaría.

Galvis presentaba, desde hace 8 meses, un cuadro intenso de dolores en el estómago, motivo por el cual era enviado en repetidas ocasiones al centro de Sanidad de la Cárcel La Picota, **en donde no lo atendían o le daban acetaminofem**. Después de colocar una tutela y de que se diera el curso para el proceso jurídico, finalmente fue atendido por personal médico que le diagnosticó cáncer estomacal, sin embargo al momento de realizar la operación los médicos descubrieron que **Galvis padecía tuberculosis en fase terminal y posteriormente falleció**.

De acuerdo con el profesor Miguel Ángel Beltrán, este no es el único caso "como él hay muchos presos políticos y presos sociales y debemos señalar que **esa responsabilidad recae tanto en el INPEC como en Fiduprevisora que son los encargados de atender la salud de los presos colombianos**".

Los presos de la Cárcel La Picota [se han declarado en desobediencia civil desde hace 21](https://archivo.contagioradio.com/internos-de-la-picota-completan-una-semana-de-desobediencia-civil/)días, en donde a través de diferentes acciones como cocerse la boca, le están exigiendo al gobierno que se declare la emergencia de salud humanitaria al interior de las cárceles. De acuerdo con Beltrán, los internos **seguirán tomando medidas más drásticas como crucificarse o no dejarse encerrar en las celdas**.

<iframe src="http://co.ivoox.com/es/player_ej_12479884_2_1.html?data=kpehmZ6cfJWhhpywj5WbaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5ynca3dxJOYr87LucbgjKbbyYqnd4a1mtGYpMrQuNOZpJiSo5bScYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
