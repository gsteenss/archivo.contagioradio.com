Title: Se incrementan hostigamientos, seguimientos y tortura contra Claudia Julieta Duque y su familia
Date: 2015-08-12 15:36
Category: DDHH, Entrevistas, Judicial
Tags: Actividades ilegales del DAS, Claudia Julieta Duque, das, enrique ariza, Gean Carlo Auque, José Miquel Narváez, Ronald Rivera
Slug: se-incrementan-hostigamientos-seguimientos-y-tortura-contra-claudia-julieta-duque-y-su-familia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/claudia_julieta_duque_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eluniversal 

###### [12 Ago 2015] 

Según denuncia la periodista se está viendo un incremento muy delicado de las situaciones que afectan su seguridad, la de su familia y de sus abogados. Desde el pasado mes de Mayo se conoció, por fuentes de alta credibilidad de **una orden que se habría emitido para secuestrarla y desaparecerla** “esa fue una decisión que se tomó desde unas instancias que no puedo revelar para proteger a mi fuente” afirma Duque.

La periodista señala que el **16 de Junio fue cercada por un taxi y una camioneta en un sitio cercano al juzgado donde se realizara el juicio contra José Miguel Narvaez, Ariza y Auque.** Después de ello ha habido una serie de seguimientos y hostigamientos en sitios públicos muy evidentes y muy preocupantes. Según Duque la situación no pasó “a mayores” por el acompañamiento de Peace Brigades International, PBI.

Otro de los hechos relatados tiene que ver con las **llamadas desde el teléfono personal de la hija a los números de los padres de Claudia Julieta**, lo extraño del hecho es que la joven estaba en pleno vuelo regresando al país después de 2 meses de ausencia y no contaba con servicio de Roaming con el que se pudiera realizar las llamadas, lo que generó **pánico en la familia que presumía que a la joven le había sucedido algo**. Además la joven ha sido fotografiada y hostigada.

Entre el 25 y el 28 de Julio, cuando Claudia Julieta estaba fuera del país, la puerta blindada de su apartamento sufrió un intento de violación que la dejó inservible y evidentemente afectada. Afortunadamente los desconocidos no lograron acceder al domicilio. La periodista relata otro hecho sucedido la semana pasada durante una entrevista con una magistrada del **Tribunal Superior de Bogotá** al frente de la fiscalía general de la nación.

Finalmente afirma que **se ha realizado un mapa en el que se demuestra que los hechos de hostigamientos o amenazas cuando se dan avances en el proceso judicial**, lo que evidencia que la estructura criminal sigue funcionando coordinadamente para evitar que avance la justicia.

### **Los avances agridulces en la justicia** 

Desde que se iniciaron las investigaciones por los crímenes del DAS, el caso por la tortura psicológica contra Claudia Julieta Duque parece ser el único que avanza. Según el recuento de la periodista hay 3 personas que han aceptado y confesado los cargos, 2 personas condenadas, otros 3 sindicados y se ha llamado a juicio **José Miguel Narvaez**, **Enrique Ariza y Gean Carlo Auque**, funcionarios del alto nivel del DAS., Además se solicita la vinculación de 19 es funcionarios del DAS, algunos de ellos trabajando en el Estado.

Dentro de lo que se ha podido establecer en los procesos de juicio puede inferirse que hay una serie de estructuras clandestinas que funcionan de manera jerárquica pero paralelas a las estructuras de mandos oficiales e institucionales que se han ocultado. **Un ejemplo de ello es la captura en 2014 de Ronald Rivera**, detective del DAS demuestra un avance en identificación y actuación contra esas estructuras.

Muchos de esos funcionarios están atomizados en diversas instituciones del instituciones del Estado lo que puede estar empujando y motivando la creciente ola de seguimientos y hostigamientos contra la periodista durante las últimas 3 semanas. **Además está el caso de Gean Carlo Auque, que vive en Santa Marta luego de fugarse** en el mes de Abril y 19 personas más sobre las que se pide sean vinculadas al proceso pero que no han sido tocadas.
