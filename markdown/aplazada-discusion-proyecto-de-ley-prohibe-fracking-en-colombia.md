Title: Aplazada discusión del proyecto de ley que prohíbe Fracking en Colombia
Date: 2018-12-05 12:41
Author: AdminContagio
Category: Ambiente, Entrevistas
Tags: Fracking en Colombia
Slug: aplazada-discusion-proyecto-de-ley-prohibe-fracking-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/WhatsApp-Image-2018-03-26-at-12.01.58-PM-1-e1544654159516.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 5 Dic 2018 

Tras la votación negativa por parte de nueve senadores para discutir la aprobación del proyecto de ley que busca prohibir la **extracción hidráulica en Colombia (fracking)**, el debate fue aplazado para **marzo del 2019** en la Comisión quinta del Senado, decisión que para los impulsores de la iniciativa se trata de una dilatación al proceso.

**Carlos Andrés Santiago, integrante de Cordatec** y de la Alianza Colombia Libre de Fracking,  denunció que desde el Congreso están usando como excusa para no dar el debate el resultado del informe que debe presentar la comisión de expertos asignado por la Ministra de Minas, una labor que para la Agencia de Protección Ambiental de los Estados Unidos tomó seis años y “que el Congreso espera se realice en tres meses”.

El ambientalista advierte que incluso de presentarse el informe, es probable que la técnica del fracking sea aprobada, debido a que **"las mayorías del Gobierno y de la industria representadas en las bancadas"** no le dieron paso a su  solicitud, una muestra de que el "Gobierno no quiere afrontar esta discusión". [(Le puede interesar ¿La economía colombiana puede sobrevivir sin fracking?)](https://archivo.contagioradio.com/economia-colombiana-puede-sobrevivir-sin-fracking/)

Según Santiago, la negativa de los congresistas para hablar de un proyecto de ley propuesto hace cuatro meses, obedece a un cruce de intereses pues se trataría de una "jugada de los congresistas para cotizarse y que su voto tenga un costo mayor", también indicó que es un claro mensaje de las bancadas al Gobierno para que les beneficie a futuro a cambio de hundir el proyecto.

> Estos son los congresistas pro fracking que votaron para aplazar el debate del proyecto de ley que prohíbe el fracking para el 2019[\#SenadoProhibaElFracking](https://twitter.com/hashtag/SenadoProhibaElFracking?src=hash&ref_src=twsrc%5Etfw)  
> Sanción social[@CarlosSantiagoL](https://twitter.com/CarlosSantiagoL?ref_src=twsrc%5Etfw) [@NoAlFrackingCol](https://twitter.com/NoAlFrackingCol?ref_src=twsrc%5Etfw) [@valentinacmpm](https://twitter.com/valentinacmpm?ref_src=twsrc%5Etfw) [@MAmbientalistaC](https://twitter.com/MAmbientalistaC?ref_src=twsrc%5Etfw) [@NataliaParraOso](https://twitter.com/NataliaParraOso?ref_src=twsrc%5Etfw) [@PlataformaALTO](https://twitter.com/PlataformaALTO?ref_src=twsrc%5Etfw) [@REALGAPA](https://twitter.com/REALGAPA?ref_src=twsrc%5Etfw) [pic.twitter.com/xHYPhLfZ2e](https://t.co/xHYPhLfZ2e)
>
> — Juan Camilo Caicedo (@JUANCAELBROKY) [4 de diciembre de 2018](https://twitter.com/JUANCAELBROKY/status/1070092113973534726?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **Mientras en el mundo se toman decisiones en Colombia no hay debate** 

Santiago asegura que acudirán a instancias internacionales como la Comisión Interamericana de Derechos Humanos, el Sistema Universal de Naciones Unidas y seguirán acudiendo y denunciando ante al Consejo De Estado, para mantener el tema del fracking en la agenda pública e impedir su implementación.

En contraste el **Comité de Derechos Económicos, Sociales y Culturales de la ONU,** por primera vez se pronunció y le solicitó al Gobierno argentino que suspenda la práctica de Fracking en el país por su impacto al cambio climático, una medida que la administración del entrante Andrés Manuel López Obrador en México también adoptará y que según Santiago, Colombia también debe acoger.

<iframe id="audio_30608983" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30608983_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
