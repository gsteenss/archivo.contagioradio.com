Title: Familias desplazadas de El Bagre, Antioquia, exigen garantías para retornar
Date: 2016-01-25 12:14
Category: DDHH, Nacional
Tags: Antioquia, El Bagre, paramilitares
Slug: familias-desplazadas-de-el-bagre-antioquia-exigen-garantias-para-retornar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Desplazados-e1481538333709.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Mundo. 

<iframe src="http://www.ivoox.com/player_ek_10193262_2_1.html?data=kpWem5iWepOhhpywj5aYaZS1lZ6ah5yncZOhhpywj5WRaZi3jpWah5yncafVzs7Zy8bXb8XZ1NXZw9%2FFqMLnjMnSjarQb6PVyNfSh5enb6Li1c7c09rNpYampJDS2s7Lqc%2BfyMbfj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Viviana Ramírez, Prensa Rural.] 

###### [25 Ene 2016.] 

**Las 150 familias que debieron desplazarse por cuenta de los enfrentamientos entre paramilitares y guerrilla**, en el corregimiento de Puerto Claver, municipio de [El Bagre](https://archivo.contagioradio.com/?s=El+Bagre+), Antioquia, aseguraron que no retornarán a su territorio debido a la falta respuesta por parte de las autoridades que este fin de semana hicieron presencia en la zona, pero no dieron ninguna solución concreta frente a la situación humanitaria y de inseguridad que viven los pobladores.

La comunidad ha señalado que aún no existen garantías para retornar, además, denuncian que pese a que la Defensoría, la Personería, el Instituto de Bienestar Familiar, y la alcaldía municipal visitaron el refugio el fin de semana, no dieron ningún tipo de solución frente a las exigencias de retorno o reubicación para las **600 personas que se encuentran desplazadas, y que en este momento carecen de alimentos, colchonetas, agua y elementos de aseo.**

“Las familias siguen atemorizadas porque no ven ningún tipo de seguridad en el corregimiento”, dice una de las personas que se encuentra en el refugio humanitario, y que fue desplazada de la vereda La Primavera, donde se desarrollaron los primeros combates.

Viviana Ramírez, periodista de Agencia Prensa Rural y quien hace parte de la comisión de derechos humanos que fue a verificar la situación en el Bagre, reportó a Contagio Radio que **las personas temen hacer denuncias porque  los han amenazado, además dos personas se encuentran desaparecidas y una casa fue totalmente saqueada, allí **dejaron letreros con mensajes que dicen “muerte a milicianos, “muerte a colaboradores de la guerrilla”.

Para la comunidad no representa seguridad la presencia de las fuerzas militares, ya que no entienden cómo los paramilitares ingresaron al casco urbano si el Ejército se encontraba a la entrada del municipio, incluso, se rumora que se **ha visto a agentes estatales dialogando con integrantes de grupos paramilitares.**

Las familias del refugio también denuncian que los militares y policías ingresaron al refugio con armas en las manos y dispuestos a atacar, atemorizado a los más de 90 niños que se encuentran en la casa.

En los próximos días se tiene planeado conformar una comisión  de verificación para conocer cómo se encuentran las familias de las veredas que decidieron permanecer en sus territorios pese a los combates.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
