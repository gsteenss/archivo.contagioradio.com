Title: Una razón de la imposibilidad de hablar “decentemente”
Date: 2019-10-18 18:22
Author: A quien corresponde
Category: A quien corresponda, Opinion
Tags: cristianismo., Cristianos, Religión
Slug: una-razon-de-la-imposibilidad-de-hablar-decentemente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

***Por culpa de ustedes, el nombre de Dios es denigrado entre las naciones***  
***Romanos 2,24***

 

Bogotá, 5 de octubre del 2019

*"Me gusta tu Cristo... No me gustan tus cristianos.*  
*Tus cristianos son muy diferentes a tu Cristo"*  
*Mahatma Gandhi.*

 

Estimado  
**Hermano en la fe**

Cristianos, cristianas, personas interesadas

 

En una de las anteriores cartas, le decía que me había quedado pensando en las razones que nos impedían hablar *“decentemente”* y cómo habíamos llegado a pensar tan diferente siendo creyentes en el mismo Dios y lectores de la misma Biblia.

La reflexión me llevó a reconocer que pensamos, hablamos y actuamos de acuerdo a lo que aprendimos y a lo que *“tenemos”* en la mente y en el corazón, tanto para bien como para mal.  Lo que *“tenemos”* en la mente y el corazón, ordinariamente, no lo elegimos conscientemente, lo tenemos sin darnos cuenta y sin saber ¿por qué?.

Esto explica que a veces, actuemos contrariamente a lo que queremos y a lo que pensamos, actuaciones que no están determinadas por la voluntad, la decisión y el pensamiento consciente, reflexivo y racional sino por aprendizajes culturales, familiares, religiosos y sociales ocurridos en los primeros años de vida, que están dentro de nosotros sin darnos cuenta, sin saberlo y por esto es común que queramos una cosa y hagamos otra.

Hablo de lo que hacemos *“sin darnos cuenta”*, diferente a lo que pensamos, planeamos y ejecutamos conscientes de su valor ético, social o religioso, conociendo sus consecuencias personales, sociales o ambientales. Me refiero a las cosas que hacemos y decimos cuando perdemos el control o actuamos sin pensar, que nos damos cuenta cuando *“metemos la pata”*.

También recordé lecturas que me llevaron a reconocer que *“al principio de nuestra vida”*, no elegimos las circunstancias que nos llevaron a pensar como pensamos, la familia en la que nacimos *(hay familias que ayudan a pensar críticamente otras no)*, la escuela a la que fuimos los primeros años *(hay escuelas que siembran el amor por el conocimiento y otras no)*, los profesores y profesoras que nos tocaron *(unos sembraron el amor por el estudio y otros nos hicieron “cogerle pereza”)*, tampoco elegimos la iglesia a la que fuimos en nuestra infancia *(unas, nos enseñaron el amor a Dios, el respeto a todas las personas, el cuidado a la creación, la justicia y la equidad;* otras, nos enseñaron a tenerle miedo a Dios, a imponer las creencias, a pensar que solo vale nuestra opinión y nuestra creencia). Sería muy larga la lista de hechos que marcaron nuestra vida desde en la infancia, que nos llevaron a ser como somos y a pensar como pensamos, sin posibilidad de elegir.

Igualmente, me vinieron a la memoria personas impulsivas, que de un *“de un momento a otro”* actúan sin pensar, se hacen daño y hacen daño a quienes estaban a su lado, que luego se arrepienten de lo dicho y lo hecho, pero que encuentran cómo restaurar el daño causado. Entonces, caí en la cuenta que hay momentos y circunstancias en los que hacemos y decimos *“cosas”* sin control, que actuamos como no queremos y como no debemos y luego no sabemos cómo remediar el daño causado y las relaciones rotas.

Los estudiosos de la condición humana, dicen que nuestra manera de ser y actuar se determinada en los primeros años de vida, especialmente los seis (6) primeros años, etapa en la que no podemos elegir, decidir, pensar, valernos y defendernos por nuestra cuenta, lo hacen otros, los adultos, por nosotros. De esta época poco recordamos y estamos marcados por ella. Esta realidad ayuda a entender el drama que vivimos cuando somos como no queremos ser y actuamos como no queremos, unos en menor y otros en mayor proporción.

Reconocer esta realidad, puede facilitar una actitud más comprensiva con nosotros y con los demás y una mayor disponibilidad y apertura para el dialogo con quienes piensan y son distintos.

Es posible aprender a manejar nuestros comportamientos involuntarios o inconscientes, a elegir adecuadamente, a tomar decisiones más pensadas y valoradas; para lo cual debemos estimular la comprensión del dolor y el sufrimiento de las personas, el conocimiento de la realidad humana y social, generar pensamientos constructivos y críticos, buscar el querer de Dios en la vida de los seres humanos y de la naturaleza, asumir una actitud sencilla y humilde para buscar la verdad, la justicia y la armonía y tener presente el mandato de Jesucristo:

*"Sean misericordiosos como el Padre de ustedes es misericordioso"*-o compasivo- (Lucas 6,36), con los seres humanos y la naturaleza, la creación.

 

le puede interesar: ([Mirar solo una cara de la realidad, actitud poco cristiana )](https://archivo.contagioradio.com/mirar-solo-una-cara-de-la-realidad-actitud-poco-cristiana/)

Estos aprendizajes hacen la vida humana más sana espiritual y psicológicamente y se logran escuchando para entender lo que quieren decirnos; contemplando la naturaleza y todo lo bello que hay a nuestro alrededor; cultivando el amor por la lectura, las artes, el conocimiento y la sabiduría; reconociendo que sabemos poco de Dios y que conocemos algo de voluntad en las Escrituras, especialmente en los evangelios que muestran la vida, muerte y resurrección de Jesús; viéndolo y escuchándolo en el sufrimiento y dolor humano, en las luchas por hacer el mundo más justo, armonios y equitativo; contemplando a Dios en la creación y en la naturaleza, en su obra maestra en un largo y bello proceso evolutivo.

Con estas reflexiones quiero decirte que llegamos a *“la imposibilidad de hablar civilizadamente y a pensar tan diferente siendo creyentes en el mismo Dios y lectores de la misma Biblia”* por una suma de factores o circunstancias que no elegimos, que nos llevaron a hablar y actuar, instintiva y pasionalmente, cuando abordamos las realidades afectivas y emocionales, las creencias políticas o religiosas, los valores familiares, culturales y patrióticos generando conflictos como los que nos han distanciado. Pero podemos y debemos avanzar en la superación de los conflictos asumiendo las recomendación de San Pablo:

*“No se acomoden a este mundo, por el contrario transfórmense interiormente con una mentalidad nueva, para discernir la voluntad de Dios, lo que es bueno y aceptable y perfecto”* (Romanos 12,2)

Fraternalmente,  
P. Alberto Franco, CSsR, J&P  
francoalberto9@gmail.com
