Title: Es necesaria una reforma política integral para que haya apertura democrática: MOE
Date: 2017-12-01 12:34
Category: Nacional, Política
Tags: MOE, Reforma Política
Slug: reforma_politica_congreso_paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/votaciones-e1512084420951.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: primisidiario] 

###### [30 Nov 2017] 

[Se agotó el tiempo y la voluntad de los congresistas para aprobar la reforma política, que muchos ya calificaban como una reforma electoral que había sido totalmente cambiada a lo que se había estipulado en un principio como elemento clave para la apertura democrática en el marco del posacuerdo.]

[No obstante, teniendo en cuenta ese panorama fue el mismo gobierno nacional, en cabeza del **Ministro del Interior, Guillermo Rivera el que solicitó al Senado archivar la iniciativa,** al ver que los partidos políticos tradicionales impidieron que se transformaran las reglas electorales a tres meses de las elecciones de Congreso.]

Así se lo había pedido la Misión de Observación Electoral, MOE, al gobierno, argumentado que que se habían desmontado los puntos esenciales de la reforma inicial, como lo explica Camilo Vargas, coordinador del Observatorio Político**-**Electoral de la Democracia de la MOE.

### **Una reforma para los mismos de siempre** 

[La apertura democrática se lograba con la reforma que estaba "de acuerdo con las recomendaciones de la MOE y al acuerdo de paz", señala Vargas. En cambio lo que se habría hecho, de aprobarse esa reforma constitucional tal y como está planteada, ponía en riesgo los avances en torno al fortalecimiento de los partidos políticos; la responsabilidad política de partidos y candidatos; la transparencia del sistema electoral, y la actuación en bancadas, entre otros.]

**"Había artículos muy en beneficio de los políticos tradicionales. T**[**odas las bancadas empezaron a legislar para sí mismos"**, expresa el integrante de la MOE, quien concluye que lo necesario para lograr una verdadera apertura política es pensar en una reforma integral con la que se logre mayor autonomía de instancias como el Consejo Nacional Electoral.]

Sin embargo, desde otros sectores lamentan que con la no aprobación de la reforma política, pues entre los únicos puntos que sobrevivían **se encontraba la posibilidad de que de que pequeños movimientos ciudadanos o pequeñas agrupaciones tengan personería jurídica** y puedan actuar como partidos políticos de cara a conseguir representación en el Congreso de la República.

[No obstante, desde la MOE se espera que "En la próxima legislatura se vuelva a presentar el proyecto, pero esta vez esperamos que el congreso tenga la seriedad de sacar adelante una reforma con los puntos necesarios, ya que esta se vio afectada por el periodo electoral", concluye Vargas.]

<iframe id="audio_22404059" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22404059_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
