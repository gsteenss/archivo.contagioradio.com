Title: “Leopoldo López no es un preso político, es un político preso”
Date: 2015-01-29 20:39
Author: CtgAdm
Category: El mundo, Política
Tags: Desestabilización, Nicolas Maduro, Socialismo, Venezuela
Slug: leopoldo-lopez-no-es-un-preso-politico-es-un-politico-preso
Status: published

###### Foto: semana.com 

#### **[[Alex Díaz, analista político]]**  
**[ ]** <iframe src="http://www.ivoox.com/player_ek_4016079_2_1.html?data=lZWemJWbfY6ZmKiak5WJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjKbZx92PiIa3lIqupsbeaZO3jMbbw9HNt9XVjNXczoqnd4a1pdnWxdSPqMaft8rbx9%2FZqc2hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

La reciente visita de los ex presidentes Pastrana de Colombia y Piñera de Chile, a Venezuela para dialogar con Leopolodo López, y presionar al gobierno para que lo libere, ha sido calificada como una **provocación al gobierno**  y a los ciudadanos que respaldan a Nicolás Maduro.

**Leopoldo López**,  líder de la oposición, está detenido y en proceso de juicio por hechos ocurridos en el 2014, conocidos como “Guarimbas” y que provocaron la muerte de 43 personas, es por ello que es calificado **no como un preso político sino un político preso**, acusado de delitos comunes e implicado en la instigación de asesinatos y actos de bandalismo. Señala Alex Díaz, analista político.

Díaz, profesor de la Universidad Bolivariana de Venezuela, afirma que esa acción de los expresidentes solo se entiende en el marco de una **política de la derecha internacional en contra de las políticas sociales aplicadas en el gobierno actual**, en que la ganancia de la empresa petrolera se está redistribuyendo en la población.

La guerra económica que ha generado la escasez, la guerra política y el constante asedio de los medios empresariales de información han generado una **percepción de insostenibilidad** del gobierno venezolano, sin embargo, Díaz afirma que son muchas las personas en **Venezuela que siguen respaldando el proceso**. El analista lamentó que la cancillería colombiana se hubiese prestado al juego de solicitar la liberta de López.

Por su parte los presidentes Maduro y Santos se reunieron en el marco de la cumbre de la CELAC en Costa Rica, y aunque no hubo mayores declaraciones respecto de la tensión en las relaciones, el presidente Maduro afirmó que seguirá respaldando el proceso de Paz de Colombia **"más allá de las diferencias que resurgen cada cierto tiempo"**.

Por su parte el presidente colombiano, Juan Manuel Santos, agradeció el **apoyo de Cuba y Venezuela al actual proceso de conversaciones** de paz con la guerrilla de las FARC que se desarrolla en la isla caribeña.
