Title: En Putumayo rechazan destitución de gobernadora Sorrel Aroca
Date: 2016-11-18 18:27
Category: Nacional, Paz
Tags: Gobernadora de Putumayo, Procuraduría General de la Nación
Slug: comunidad-rechaza-destitucion-de-gobernadora-de-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Sorrel-Aroca-770x437.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ConexiónPutumayo] 

###### [18 Nov 2016] 

La Procuraduría General de la Nación destituyo a la actual gobernadora del Putumayo Sorrel Parisa Aroca Rodríguez, por supuestas actuaciones irregulares realizadas cuando era presidenta de la Asamblea Departamental. **Sin embargo aún queda una instancia para que esta decisión sea un hecho.**

Según el Ente investigador, la sanción sería impuesta por la adquisición de una póliza para los diputados de la Asamblea, por 20 millones de pesos y la falla estaría en que **el proceso de selección y estudio del mismo se habría realizado en un día.**

Para algunos pobladores como Jany Silva esta decisión atenta contra la elección popular de la gobernadora “cuando el pobre estrena, todo es robado, **Sorrel fue elegida por elección popular de los campesinos y la clase baja**, no hace parte de los corruptos de siempre y eso se cobra, si hacemos memoria se han terminado las aspiraciones y se han vetado a las personas elegidas por el pueblo”.

La falta en principio habría sido calificada como gravísima por aparentemente haber desconocido los principios de transparencia, responsabilidad y economía. Sin embargo Jany Silva reitera que **“acá han habido una cantidad de gobernadores, alcaldes corruptos que no han dejado prosperar el departamento que están libres y sin miedo, por eso esto es un atentado contra la democracia”**

Los pobladores además afirman que pese a estas afirmaciones de la Procuraduría Sorrel seguirá siendo su gobernadora y que **podrían estar pensando en algún tipo de acciones que rechace esta decisión y siente su postura. **Le puede interesar: ["Comunidades del Putumayo insisten en plan de sustitución a pesar de erradicación forzada"](https://archivo.contagioradio.com/comunidades-del-putumayo-insisten-en-plan-de-sustitucion-a-pesar-de-erradicacion-forzad/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
