Title: Duque's bill for victims' congressional seats ignores peace deal
Date: 2019-12-18 17:27
Author: CtgAdm
Category: English
Tags: Congress, congressional seats for victims, peace deal
Slug: duques-bill-for-victims-congressional-seats-ignores-peace-deal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Congreso-curules.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: SenadoGovCo] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[A new government-backed bill that revives the congressional seats allocated for victims of the armed conflict is provoking hard criticism from supporters of the peace deal. Instead of creating new vacancies in Congress as is stipulated in the 2016 accord, the victims would take seats voluntarily forfeited by political parties, according to the proposal.]

[In 2017, the Senate sank a bill that created 16 seats for the victims in a controversial vote that was challenged by then-President Juan Manuel Santos. With 50 votes in favor and only seven against, the bill received majority approval, argued the president, because three senators of the 102 were ineligible to vote. This debate is currently under revision by the State Council. ]

[The original bill indicated that those who were candidates to the seats — which would exist during the legislative periods of 2018-2022 and 2022-2026 — should be nominated by legally recognized victims’ organizations, peasant associations and organizations subject to collective reparations that had been created before December 1, 2016, and that demonstrated a history of work with or in favor of victims.]

[The 16 seats would be distributed among Bolívar, Cesar, Córdoba, Arauca, Antioquia, Cauca, Caquetá, Chocó, Meta, Nariño Norte de Santander, Putumayo and Tolima, provinces of the country that had been most hard hit by the armed conflict. ]

[According to the former Minister of the Interior Juan Fernando Cristo, the seats were approved by the Senate in December 2017 and later supported by a decision of the Constitutional Court, which currently studies a writ for protection of fundamental rights brought forward by Senator Roy Barreras, who requested that the “rights to equality, due process, and political participation, violated by Directive Table of the Senate of the Republic” be protected.]

### **The seats that President Duque proposes**

President Iván Duque has now proposed an initiative that seeks to create these seats "without generating more space, more budget costs and that there be generosity and nobility from all the political forces," that is, the bill established these spaces for the victims without creating 16 new seats but rather to recompose the floor of the House of Representatives.

Senator Ernesto Macías showed support for the bill, adding that the seats should be guaranteed for what he considers are the "real victims," or those that have been certified and are considered a priority because they were victims of the FARC, military or the police. Critics have said this discriminates against the people who have lived the armed conflict.

According to Senator Barreras, the bill "restricts established rights in the peace deal and ignores the concept of the integral reparations of the territories," emphasizing that Congress, the Constitutional Court, and the Supreme Court of Justice already approved the seats.

"The government seems to be walking blindly on these issues and is not well informed of what happens with the implementation of the peace deal. The transitory seats of peace were approved by a legislative act two years ago," said Cristo.

Victims' representatives, such as Martha Aguirre, president of the foundation Sonrisas de Colores and a representative of the family members of victims of the massacre of councilpeople in Rivera, Huila, said that this is the minimum that the government and Congress can give the victims. "We can't keep talking about the peace deal and the conflict without taking into consideration our participation and above all else, without political representation."

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
