Title: Estigmatización al Movimiento Estudiantil
Date: 2018-11-26 09:00
Author: AdminContagio
Category: Expreso Libertad
Tags: educacion, Movilización, paro
Slug: estigmatizacion-al-movimiento-estudiantil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/descarga-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio radio 

###### 30 Nov 2018 

Se cumple un mes del Paro Nacional Universitario, que ha estado acompañado por grandes **movilizaciones y campamentos en todo el país.** Actividades que han sido fuertemente reprimidas por parte del Escuadrón Móvil Antidisturbios (ESMAD), que de acuerdo con las y los estudiantes **es producto de una fuerte estigmatización y persecución a este sector del movimiento socia**l.

En este programa del Expreso Libertad, el abogado **Jefferson Tunjano** aportó su balance sobre las violaciones a los derechos humanos cometidas por el ESMAD y su **estrategia para judicializar a quienes participan de las movilizaciones en marco de esta protesta**.

<iframe id="audio_30283234" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30283234_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
