Title: La grave crisis humanitaria que afronta el pueblo Awá los obligó a venir a Bogotá
Date: 2018-11-26 14:31
Author: AdminContagio
Category: DDHH, Nacional
Tags: Awá, colectivo jose alvear restrepo, indígenas, Oleoducto, Tumaco
Slug: crisis-humanitaria-awa
Status: published

###### [Foto: @OrgsColombia] 

###### [26 Nov 2018] 

Integrantes de la comunidad indígena Awá  se encuentran en Bogotá para denunciar la crisis por la que atraviesa su pueblo, por cuenta de las violaciones contra su territorio, el incumplimiento de los acuerdos pactados con el Gobierno, y el aumento de la violencia en las regiones en las que habitan. **Según cifras de la Corte Constitucional, 35% de los pueblos indígenas están en riesgo de extinción, entre ellos, los Awá**.

Según la **Organización Nacional Indígena de Colombia (ONIC)**, el pueblo Awá habita en los departamentos de Putumayo y Nariño, y en algunas regiones del Ecuador. Su población, censada en 2005 por el DANE, es de **25.813 personas quienes viven en “asentamientos dispersos que siguen la corriente de los ríos”**.(Les puede interesar: ["66% de los pueblos indígenas está a punto de desaparecer: ONIC"](https://archivo.contagioradio.com/pueblos-indigenas-a-punto-de-desaparecer-onic/))

Para la coordinadora del equipo de protección del territorio y acciones colectivas del **Colectivo de Abogados José Alvear Restrepo, Rosa María Mateus,** muchos de los problemas que enfrenta la comunidad indígena, específicamente los **resguardos Inda Guacaray e Inda Zabaleta, en Nariño**, tienen que ver con la contaminación de sus fuentes hídricas.

El Colectivo ha trabajado con estas poblaciones desde hace 10 años, y gracias a su acompañamiento, han evidenciado la tensión de derechos que se produce en la zona: por un lado, la Corte Constitucional que se pronuncia sobre el riesgo de extinción de una comunidad que ha tenido que sufrir el conflicto; y por otro lado, **el de un Estado que pone en riesgo a las comunidades, debido a la instalación del Oleoducto Trasandino (OTA)**.

### **El OTA: punto de interés para el Estado y las estructuras armadas** 

El Oleoducto transporta el petróleo extraído en Putumayo y lo lleva hasta Tumaco, Nariño; es fuente importante de ingresos para el Estado, pero también para las estructuras armadas ilegales que encontraron allí una forma de financiación. La instalación de válvulas ilegales que permiten robar el crudo para ser procesado en refinerías ilegales ha sido una constante hasta la actualidad, lo que se suma a la voladura de su estructura como forma de presión al Estado, empleada por las desmovilizadas FARC.

Del uso inapropiado de las válvulas ilegales, los atentados a la infraestructura petrolera, el mal manejo hecho por Cenit, Transporte y Logística de Hidrocarburos (filial de Ecopetrol) del Oleoducto y la incapacidad del Estado para enfrentar el problema, **se ha derivado una contaminación en las fuentes de agua que surten a los Awá**, especialmente, los ríos Inda, Rosario, entre otros.

### **Los problemas derivados del OTA** 

Algunos de los problemas que enfrenta actualmente el pueblo Awá, se desprenden directamente de la contaminación de petróleo de sus principales afluentes, entre ellos el tema de la salud, sus posibilidades económicas y sus tradiciones. La abogada señaló que **tras la voladura del oleoducto, y de las fugas producidas por las válvulas ilegales, se comenzaron a presentar afectaciones estomacales y en la piel** de los miembros de la comunidad.

Adicionalmente, **del mal manejo de las emergencias realizado por Ecopetrol se desprende la permanente contaminación con la que permanecen los afluentes**, hecho que ha generado mortandad de animales que son parte de la dieta de los Awá, como peces y cangrejos; así como de plantas que crecen a la orilla de los ríos, entre ellas, la Baracacha, usada como método medicinal.

A estos problemas se suma la presencia de grupos armados ilegales en la zona, en parte interesados por el oleoducto, y otros interesados en el control territorial para garantizar el cultivo, siembra y extracción de los cultivos de uso ilícito. De acuerdo a información brindada por Mateus, **actualmente el territorio está sufriendo por la reagrupación y rearme de grupos ilegales que han aparecido tras la firma del Acuerdo de Paz**, lo que ha aumentado el riesgo para las comunidades.

### **Los problemas históricos que sufre la comunidad** 

La abogada sostuvo que en las observaciones que ha realizado el Colectivo, se ha evidenciado que los problemas en salud que afectan a los Awá son similares a los que enfrenta todo el sistema colombiano: **falta de cobertura, infraestructura para la atención y personal calificado, en resumen, no se garantiza el acceso a este derecho**. (Le puede interesar: ["Organizaciones indígenas y afro radican tutela contra reforma a Ley de Tierras"](https://archivo.contagioradio.com/tutela-reforma-ley-de-tierras/))

En cuanto a la educación, **Rider Pai, consejero mayor de la Unidad Indígena del Pueblo Awá (UNIPA)**, señaló que tienen problemas respecto al enfoque de etno-educación que debe adelantarse en los resguardos, pues no cuentan con los suficientes profesores que garanticen este derecho. Adicionalmente, dado que el pueblo Awá es una comunidad dispersa, **la infraestructura educativa obliga a algunas comunidades a tener que caminar hasta 3 horas para llegar a sus escuelas**.

Pai también sostuvo que las afectaciones a los derechos humanos se observan en las amenazas constantes que reciben los líderes de la comunidad, y **la violación constante de los territorios indígenas**. Por su parte, Mateus sostuvo que los esquemas de protección individual realizados por la Unidad Nacional de Protección y la Policía son insuficientes, y en muchos casos no tienen el enfoque étnico.

La abogada dijo que el pueblo Awá, es un ejemplo de una sociedad civil que está en medio de un Estado que no cumple su labor, y unos actores ilegales que actúan libremente, incluso, pagando sobornos a miembros de las Fuerzas Militares. (Les puede interesar:["32 líderes indígenas han sido asesinados en el primer semestre de 2018"](https://archivo.contagioradio.com/32-lideres-indigenas-han-sido-asesinados-en-el-primer-semestre-del-2018/))

### **Las 4 exigencias los indígenas:** 

Mateus aseguró que si el daño ambiental de las estructuras armadas es grave, el que realiza el Estado, representado por el Ministerio de Defensa no se queda atrás, puesto que **cuando las fuerzas armadas encuentran pozos de petróleo o refinerías clandestinas, las explotan o queman sin medir el daño ambiental que esto puede generar**.

Por lo tanto, el Consejero Mayor de la UNIPA llegó a Bogotá pidiendo **que se cumplan los acuerdos establecidos con los indígenas**, y que se obedezca lo establecido por los Auto 004 de 2009, Auto 174 de 2011 y Auto 620 de 2017; así como las medidas de protección sugeridas por organismos internacionales.

Por su parte, el Colectivo de Abogados acompaña la demanda de las comunidades Awá que cursa en el Tribunal Administrativo de Cundinamarca, en la que piden que se restaure el río, adoptando medidas de mitigación y limpieza adecuadas para ello; que se realice el proceso de Consulta Previa con los resguardos afectados por el OTA, porque se ha negado este derecho señalando que estos lugares no están en el área de influencia directa del oleoducto; y **que se retire el oleoducto**, o al menos, que no atraviese el Río Inda, para evitar nuevas contaminaciones.

<iframe id="audio_30333090" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30333090_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
