Title: Búsqueda de los restos de Camilo Torres, un gesto hacia la paz
Date: 2016-01-18 17:04
Category: Nacional, Paz
Tags: Alejo Vargas, camilo torres, ELN, proceso de paz
Slug: busqueda-de-camilo-torres-un-gesto-hacia-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/camilo-torres-teatro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Hernán Díaz ] 

<iframe src="http://www.ivoox.com/player_ek_10114595_2_1.html?data=kpWek5mZfZahhpywj5aUaZS1k5yah5yncZOhhpywj5WRaZi3jpWah5yncaOZpJiSpKbXtdbZxcaYxsqPh8LhytHcjbnTttPZ1IqfpZDZsozbxtjh0ZDMpcTdwpDZw5DUpduhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejo Vargas, U. Nacional] 

###### [[18 Enero 2016] ] 

[El pasado sábado el presidente Juan Manuel Santos anunció que La Fiscalía y el Instituto de Medicina Legal con la colaboración de las Fuerzas Militares iniciarían la **búsqueda de los restos mortales del sacerdote Camilo Torres**, quien murió mientras combatía en las filas del ELN en un enfrentamiento con el Ejército Nacional en San Vicente de Chucurí, en febrero de 1966.   ]

[Alejo Vargas, director del ‘Centro de Pensamiento y Seguimiento al Diálogo de Paz de la Universidad Nacional’, asegura que la decisión del mandatario es “un **gesto hacia la reconciliación nacional**” que incluye no solamente a la guerrilla del ELN, con quien tiene previsto instalar formalmente el proceso de diálogos, sino a la iglesia católica y a la Universidad Nacional quienes también habían solicitado autorizar la búsqueda de los restos del sacerdote para ser sepultados dignamente.]

[Se espera que previo al anuncio del inicio formal de conversaciones entre ELN y Gobierno, los **familiares y amigos más cercanos del sacerdote Camilo Torres puedan sepultar dignamente sus restos**, y que quienes quieran puedan dirigirse a la tumba para rendir homenajes, como es el caso de la Universidad Nacional cuyas directivas han anunciado que la **cátedra Manuel Ancisar estará dedicada a la vida y obra del sacerdote, durante este primer semestre**.    ]

**[En contexto:]**

[Varias son las versiones que se han tejido sobre la muerte y el paradero de los restos de Camilo Torres, de acuerdo con Alejo Vargas, el sacerdote había construido una amistad importante por orden familiar, pero también por coincidencias políticas con el entonces coronel del Ejército Álvaro Valencia Tobar, y "por esas paradojas de la historia se vieron enfrentados militarmente, Camilo como miembro del ELN y el coronel Valencia Tovar como comandante de la Quinta Brigada en Bucaramanga” quien, seguramente con el visto bueno del gobierno de la época, tomó la decisión de **inhumar los restos en una tumba que no fuera de conocimiento público pues consideraban que podría ser un factor de alteración del orden**. Otras de las versiones apuntan a que el Ejército tiene todavía en su poder los restos del sacerdote.  ]

[Según indica Alejo Vargas, Camilo Torres fue "un **sacerdote que en los años 60 jugó un papel importante en la vida política nacional**, estudió sociología en la Universidad Católica de Lovaina y una vez regresó, fue nombrado como capellán de la Universidad Nacional, desde dónde contribuyó a la formación de la primera facultad de sociología de América Latina". Además de ser profesor en la Universidad Nacional, integró la planta docente de la Escuela Superior de Administración Pública y fue miembro de la junta directiva del Instituto Colombiano de la Reforma Agraria en representación de la iglesia católica.]

[ Después de varios años de actividad académica y pastoral Camilo Torres **se vinculó con actividades políticas a las que llegó por las dinámicas del movimiento estudiantil de la época**, tras lo que tomó la decisión de retirarse de la iglesia y conformar el 'Frente Unido' que influyó en las dinámicas de organización social del momento. Asegura Vargas y agrega que es luego de este activismo que el sacerdote se integra al ELN en el que dura cerca de 5 meses hasta el combate del 15 de febrero de 1966, en el que muere. ]

[ ]
