Title: A pesar de combates en Ucrania continua la tregua
Date: 2015-01-19 20:12
Author: CtgAdm
Category: El mundo
Tags: Donetsk, Noticias Derechos Humanos, Ucrania
Slug: a-pesar-de-combates-en-ucrania-continua-la-tregua
Status: published

###### **Foto: telecinco.es** 

5 civiles muertos (de los cuales dos eran niños), 4 milicianos  y un total de 30 heridos son el resultado de los combates entre el ejército oficial Ucraniano y las milicias separatistas de Donetsk

Técnicamente la tregua iniciada el 1 de Octubre de 2014 no se ha roto ya que las tropas oficiales no han atravesado la línea del frente fijada en el aeropuerto de Donetsk, aunque han sido en las inmediaciones de este donde se han provocado. Paralelo a los combates en el aeropuerto, fue bombardeada la ciudad por fuerzas ucranianas obligando a la población civil a refugiarse.

El presidente ruso Vladimir Putin se pronunció al respecto de los bombardeos, instando a su homólogo ucraniano a desmovilizar todas las baterías de artillería posicionadas en la zona con el fin de poder verificar la tregua y continuar la negociación de paz.

A día de hoy Ucrania ha movilizado a 50.000 reservistas hacia la frontera con Rusia y con las regiones separatistas. Entre deserciones en el ejército y críticas por parte de la oposición del propio país, de Rusia y de los líderes separatistas se considera a dichos movimientos como una nueva escalada bélica en el conflicto.

Por otro lado los esfuerzos diplomáticos se materializan con la invitación por parte de la delegación alemana para realizar una reunión entre Rusia , Francia y Ucrania, **pero** **sin la presencia de las delegaciones separatistas.**

Sin embargo, el  portavoz del presidente Vladimir Putin cree difícil que se produzca dicha reunión, ya que su homólogo ucraniano acusa a Rusia de injerencia en el conflicto, de armar a las milicias rebeldes , además de remarcar que no existe una guerra civil en su país sino una guerra entre Ucrania y Rusia. El presidente ruso niega dichas acusaciones y recalca que no existe ningún tipo de ocupación por parte de su país.
