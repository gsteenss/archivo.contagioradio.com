Title: Atentado en Ucrania deja dos muertos en el aniversario del Maidan
Date: 2015-02-23 19:38
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: aniversario Maidan, atentado en Jarkov, Euromaidan, Kiev, Maidan, Ucrania
Slug: atentado-en-ucrania-deja-dos-muertos-en-el-aniversario-del-maidan
Status: published

###### Foto:Minuto30.com 

En medio de una manifestación que recordaba el inicio de las protestas conocidas como **"Maidan"en la ciudad rusoparlante de Jarkov en el sureste ucraniano, ha estallado un explosivo** dejando diez personas heridas y dos muertas, de las cuales una era policía.

Las autoridades ucranianas han puesto en marcha una operación calificada de antiterrorista en la ciudad con el objetivo de detener a los culpables.

**Hoy se cumple un año de que comenzaran las protestas en Ucrania con el objetivo de derrocar el gobierno de Víktor Yanukóvich.**

Lo que inició como un movimiento **similar al de los indignados, con el tiempo fue adquiriendo matices políticos distintos**. Al principio el único objetivo era derrocar un régimen corrupto que mantenía a la población sin desempleo y con altas cifras de pobreza.

Mas tarde comenzó a declinarse en favor de la parte europea de Ucrania y con posiciones cercanas a la UE, EEUU y en general a los acuerdos económicos con la Unión Europea, rechazando los históricos lazos económicos con Rusia.

De tal situación se derivó en las **protestas generales de las regiones prorrusas**, es decir con mayoría de población rusoparlante inmigrada del país vecino. **Estas regiones hostigadas por el creciente nacionalismo** y que llegado a este punto, cada vez estaba más infiltrado en el "Maidan", decidieron armarse y comenzaron los combates para reclamar la independencia de dichas regiones.

Las protestas actualmente han derivado en un conflicto armado entre regiones separatistas y las fuerzas armadas de Ucrania, hasta la tregua declarada la semana pasada.
