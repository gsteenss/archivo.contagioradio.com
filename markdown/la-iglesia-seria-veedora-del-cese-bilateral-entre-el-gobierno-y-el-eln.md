Title: Iglesia y ONU serán veedoras del cese bilateral entre el gobierno y ELN
Date: 2017-09-04 08:50
Category: Nacional, Paz
Tags: cese al fuego bilatera, ELN, gobierno nacioanl, proceso de paz
Slug: la-iglesia-seria-veedora-del-cese-bilateral-entre-el-gobierno-y-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/dialogo-eln-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Federico Parra, AFP] 

###### [04 Sept 2017] 

Desde muy tempranas horas de la mañana **se conoció que el gobierno y el ELN habrían logrado pactar un cese bilateral,** que incluye la llegada del Papa Francisco a Colombia y que se extendería por un plazo de 4 meses “prorrogables”. Entre los garantes están la iglesia católica que ha cumplido un papel fundamental y la  ONU

Además las partes** llegaron a algunos acuerdos tras el cese bilateral al fuego.** Por el lado el ELN, se comprometieron a suspender las retenciones voluntarias e involuntarias de colombianos y extranjeros en el país, suspender atentados contra infraestructura especialmente petrolera y a no continuar sembrando minas antipersonales.

Por otro lado, el Gobierno Nacional se habría comprometido a establecer un programa de alertas para proteger a los líderes sociales y a crear un programa de atención para los presos políticos del ELN. (Le puede interesar: ["Temporal y experimental sería el cese unilateral del ELN"](https://archivo.contagioradio.com/temporal-y-experimental-seria-el-cese-unilateral-del-eln/))

El cese bilateral empezará a regir el **1 de octubre y culminará el 12 de enero de 2018.** De acuerdo con el presidente Juan Manuel Santos "la prioridad es proteger a los ciudadanos y cesarán actividades como el secuestro".  Afirmó que este cese bilateral se da en medio de la visita del papa Francisco como "un momento de invitación a la reconciliación".

Para Victor de Currea Lugo, cercano a las conversaciones, **este cese no es solamente por la llegada del Papa** “la visita ayuda pero el fin último es alcanzar la paz” por eso se extendería por 4 meses que podrían ser prorrogables. Además resaltó que esta medida desataría la participación que ya tiene algunos modelos avanzados, incluso podrían darse unas audiencias con algunas organizaciones para acordar detalles de estas medidas.

El analista también señala, respecto al punto de la concentración de las fuerzas de esa guerrilla, que **no se daría la concentración, lo cual no representaría una dificultad para la verificación** pues, según él, hay muchas experiencias que han aportado en verificaciones en condiciones similares. (Le puede interesar: ["COALICO insta a ELN y gobierno Nacional a incluir protección a menores de edad en agenda de diálogo"](https://archivo.contagioradio.com/coalico-insta-a-eln-y-gobierno-a-incluir-proteccion-de-menores-de-edad-en-agenda-de-dialogo/))

[Acuerdo de Quito 4sep2017](https://www.scribd.com/document/357994669/Acuerdo-de-Quito-4sep2017#from_embed "View Acuerdo de Quito 4sep2017 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_27466" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/357994669/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-dHjp25G4K9qqVOxH7Ewf&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7749140893470791"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
