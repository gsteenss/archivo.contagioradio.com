Title: Dilaciones y amenazas caracterizan el proceso por el asesinato de Diego Felipe Becerra
Date: 2015-10-23 16:49
Category: Uncategorized
Tags: “Trípido”, Caso graffitero, Corrupción en la policía nacional, Diego Felpe Becerra, Gustavo Trejos, Padre Diego Felipe Becerra, Patrullero Wilmer Alarcón, Testigos caso Grafitero
Slug: dilaciones-y-amenazas-caracterizan-el-proceso-por-el-asesinato-de-diego-felipe-becerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Contagio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_9143936_2_1.html?data=mpahlZ6Xeo6ZmKialJWJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmrdSY09rJb83ZjNXO1Yqnd4a2lJCxy8rLs4y6xtHW0sqPstCfzcqY0trJqMaf0cbgw9ePpYzhhqigh6aVt4yhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Gustavo Trejos] 

###### [23 oct 2015]

A cuatro años del asesinato de Diego Felipe Becerra ”Trípido”, la noche del 19 de agosto de 2011, Gustavo Trejos, padre del joven, informa que pedirán **medidas cautelares de la Comisión Interamericana de Derechos Humanos (CIDH) ** para que se garantice la continuidad del** proceso y protección a** testigos, fiscales y familiares amenazados.

De las **27 audiencias programadas solo se han llevado a cabo 2** por continuas dilaciones lo que evidencia que “garantías de que se vaya a obtener justicia prontamente” ni por el asesinato de Diego Felipe ni por la alteración de la escena del crímen por parte de la policía.

En **cuanto a la alteración de la escena del crimen, Gustavo Trejos hay trece personas implicadas** , de las cuales tres son civiles y el resto policías. 9 de ellos han decidido contar la verdad incluyendo el testimonio de subintendente Giovanny Tobar que reconoció que llevo el arma con la que pretendieron justificar el asesinato.

Tovar **indicó que un coronel de la policía dio la orden de llevar el arma para inculpar a Felipe Becerra**, y tras la confesión “han recibido amenazas muy fuertes”. Las personas que dicen “la verdad” se “les tilda de sapos”, “como si el principio de la institución fuera trabajar por la mentira”, afirma el padre de Diego Felipe Becerra.

Según el relato “Un coronel da el aval yo llevo el arma al sitio, se la entrego al abogado Fernando Ruiz la mira la manda a disparar al humedal de Córdoba, la entregan de nuevo al abogado Ruiz, saca el pañuelo la limpia y con Wilmer Alarcón la colocan en la escena del crimen”

Este testimonio demostraría que “**el arma sí la colocó la policía nacional**” y que “altos mandos de la policía nacional dieron el aval para que se colocara esta arma” y que “fue plantada por el asesor jurídico de ese momento de la policía metropolitana como por miembros de la policía” afirma Trejos.

Esta información se corrobora con el informe de contra inteligencia de la Policía que llegó a la escena del crímen antes que cualquier otra autoridad. En el informe se menciona que no hay ninguna arma. **Estos testimonios han generado amenazas** contra el abogado del subintendente, el Subintendente Tobar y además seguimiento contra la hija del Fiscal quien aún no cuenta con seguridad después de año y medio de haber denunciado el riesgo en el que se encuentra por esta y otras investigaciones contra policías y militares.

Trejos también denuncia que al Fiscal por distintos medios se están “implementando presiones dentro de la fiscalía para tratar de hacer renunciar al Fiscal”, obstaculizando su trabajo, lo cuál es preocupante.

Por otra parte en el 2013 el general Patiño solicitó personalmente que se abriera una investigación contra él, en el 2014 se pidió investigar a dos coroneles de la policía y faltando **un “año largo” para que se venzan los términos en la procuraduría**, esta “no ha trabajado diligentemente“ ni avanzado en las investigaciones que puedan arrojar sanciones disciplinarias contra los implicados en el caso.

Con las medidas cautelares ante la CIDH lo que es busca es el “acompañamiento de entes internacionales al proceso para que no se presenten las obstaculizaciones que se están presentando a nivel de procuraduría y fiscalía “además que “estamos buscando que se brinde protección a los testigos, a los policías que se han atrevido a decir la verdad”, porque ellos no van a seguir ese acuerdo si no hay seguridad para ellos”, afirma el padre de Diego Becerra.

**El General Patiño**

En el 2013 se hicieron denuncias que daban a entender que el “se reunió con testigos falsos” y los indujo a “colocar una falsa denuncia”, adicional a esto el **general indicó que solo había estado** “cinco minutos en el CAI y que el solo había conocido el caso de Diego Felipe hasta el lunes”, sobre lo cuál la familia y **el padre del graffitero tuvieron conocimiento con material probatorio que** “el general estuvo 2 horas con testigos falsos” y que miembros de la policía les dieron un tratamiento especial a los testigos falsos.

Sin embargo **la fiscalía sexta cambió el fiscal dos veces**, impidiendo la imputación de cargos, hoy se espera que esta cumpla su deber “ya que el material probatorio que hay” indica que “el general es el determinador tanto de la alteración de la escena, como del montaje de la buseta para encubrir la muerte de Diego Felipe”.

Con este panorama la familia de “Trípido” después de 4 años “muy duros” de haber perdido a un familiar indica su padre que “estar en el proceso nos ha ayudado a tomar una fortaleza muy fuerte” y **les ha ayudado a tener unos objetivos claro**s no solo para su hijo sino para otros jóvenes con la **no repetición** ya que “lo que le pasó Diego Felipe no le puede pasar a más jóvenes en el país” además de “buscar la verdad”, que “la policía rectifique” y que “se acabe la corrupción al interior de la policía”.

**Ver también: [Una píldora para la memoria](https://archivo.contagioradio.com/una-pildora-para-la-memoria-diego-felipe-becerra/)**
