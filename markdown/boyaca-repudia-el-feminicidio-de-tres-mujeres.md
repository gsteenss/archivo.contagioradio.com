Title: Alerta en Boyacá por feminicidios de campesinas
Date: 2017-08-02 16:49
Category: Mujer, Nacional
Tags: Asesinatos, Boyacá, feminicidio, mujeres
Slug: boyaca-repudia-el-feminicidio-de-tres-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/feminicidio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [02 Ago. 2017] 

**Tres feminicidios sucedieron en Güicán de la Sierra al norte de Boyacá** entre el mes de mayo y julio, sin que hasta la fecha haya habido una respuesta por parte de las instituciones gubernamentales al respecto, así lo denuncian varias organizaciones que a través de un comunicado repudiaron estos hechos y la actitud silenciosa por parte de las autoridades ante estos asesinatos.

Genoveva Correa de 70 años, Cecilia Velandia de 70 años y prima de Genoveva y Hermelina Barón de 45 años, son las **tres mujeres campesinas que fueron vícitmas de feminicidio y quienes se dedicaban a las labores del campo. **

“Hay mucha incertidumbre, mucho temor en la vereda, un lugar que se ha caracterizado por ser una zona muy tranquila. **Las tres mujeres tienen en común que dedicaron toda su vida al trabajo en el campo**, a las labores en las veredas y a trabajar para sostenerse. Los vecinos de la vereda quedaron sorprendidos con sus desapariciones, por eso inician la búsqueda” Tatiana Triana integrante de la Asociación Nacional de Jóvenes y Estudiantes de Colombia ANJECO.

Todos los cuerpos fueron hallados fuera de sus casas, con fuertes **señales de tortura física, ahogamiento y golpes en sus cuerpos y rostros. **Le puede interesar: [Colombia se disputa segundo lugar en Feminicidios en América Latina](https://archivo.contagioradio.com/colombia-se-disputa-segundo-lugar-en-feminicidios-en-america-latina/)

### **Profesoras no quieren dictar clase por temor** 

Esta vereda, que cuenta con 4 escuelas, cesó sus actividades escolares desde el martes 25 de julio, dado que **sus docentes, en su mayoría mujeres, no han querido regresar a dictar clases por miedo a ser víctimas de feminicidio.**

“Estas profesoras se negaron a asistir a estas escuelas, a trabajar por el temor también de ser asesinadas, pues a raíz de los tres feminicidios se demuestra que estos hechos son justamente contra las mujeres”. Le puede interesar: [413 mujeres fueron víctimas de feminicidio en los últimos 3 años](https://archivo.contagioradio.com/413-mujeres-han-sido-victimas-de-feminicidio-en-colombia-en-los-ultimos-3-anos/)

### **Mayor seguridad no garantiza la no repetición**

El 27 de julio se realizó un Consejo de Seguridad en el que estuvieron presentes las autoridades municipales, policía, ejército y CTI, en este manifiesta Triana, se comprometieron a hacer más patrullajes y a dar con el paradero de quienes efectuaron los asesinatos, sin embargo, hasta el momento no ha habido un pronunciamiento oficial de ninguna autoridad.

**“Los patrullajes no son una garantía real para las comunidades en términos de proteger su vida, su integridad y mucho menos frente a las mujeres**, en realidad esto es un compromiso que no da a ciencia cierta con un resultado de la investigación para que las comunidades puedan conocer qué es lo que pasa en la vereda desde finales de mayo y no garantiza la no repetición”.

### **No se atribuye aún a ningún grupo la autoría de estos feminicidios** 

La comunidad no ha atribuido a ningún grupo específico estos asesinatos y exigieron a las autoridades investiguen quiénes están detrás de estos hechos. Además, solicitan que **se adelanten acciones que garanticen la seguridad y la integridad de mujeres y hombres** que habitan y trabajan en este sector.

Por último, instaron a las instituciones a que se pronuncien frente a estos tres casos e informen a las comunidades de qué es lo que está pasando en la vereda, qué acciones adelantarán para proteger a los habitantes **“exigimos que cesen los abusos y los feminicidios de nuestras mujeres campesinas”** concluye Triana. Le puede interesar: [Conmuévete y Muévete, una campaña contra los feminicidios y asesinatos de líderes](https://archivo.contagioradio.com/la-campana-que-movilizara-a-la-ciudadania-contra-los-feminicidios-y-asesinatos-de-lideres/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
