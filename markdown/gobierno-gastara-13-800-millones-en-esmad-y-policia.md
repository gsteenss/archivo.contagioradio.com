Title: Gobierno gastará $13.800 millones en ESMAD y Policía
Date: 2020-09-25 19:26
Author: AdminContagio
Category: Actualidad, Nacional
Tags: ESMAD, Movilización social, Policía Nacional, Protesta social, Wilson Arias
Slug: gobierno-gastara-13-800-millones-en-esmad-y-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/ESMAD.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este jueves el senador Wilson Arias, del Polo Democrático, dio a conocer a la opinión pública **las cifras que el Gobierno pretende gastar en dotación y equipamiento para la Fuerza Pública,** particularmente para la Policía Nacional y su Escuadrón Móvil Antidisturbios -ESMAD-. **Esto, en medio de la crisis de legitimidad que enfrentan esas instituciones como consecuencia de los asesinatos y agresiones que han llevado a cabo en contra de civiles.** (Lea también: [Testimonio: Así son las torturas de la Policía a ciudadanos detenidos en la protesta social.](https://archivo.contagioradio.com/testimonio-asi-son-las-torturas-de-la-policia-a-ciudadanos-detenidos-en-la-protesta-social/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador Arias, reveló que **la Dirección Administrativa de la Policía ejecutará cuatro contratos que sumados equivalen a un valor de \$13.800 millones de pesos** para aprovisionar de dotación como trajes antimotines y gorras, y de equipamiento como granadas y explosivas al ESMAD. (Lea también: [El aparato del Estado se revistió de legalidad para afectar la Movilización: Análisis](https://archivo.contagioradio.com/aparato-estado-revistio-legalidad-afectar-movilizacion/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de las cifras más impactantes recogidas en los contratos, **es el gasto que supera los 6.400 millones  de pesos para la adquisición de gorras “tipo beisbolera” para la Policía.** Según el informe del senador Arias el precio por unidad de cada gorra es de casi 26 mil pesos, un costo que se podría explicar en una compra por unidad, pero se cuestiona que el importe solicitado asciende a las 250 mil gorras, lo cual debería disminuir ostensiblemente su precio.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1309170613634953227","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1309170613634953227

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Frente a la dotación del ESMAD, Arias, dio a conocer también los costos de cada uno de los elementos que componen el traje antimotines utilizado por estos agentes: **El traje sin casco y canilleras asciende a la suma de 3’395.000 pesos; por su parte el casco táctico tiene un valor de 418.000 pesos; y las canilleras cuestan 326.101 pesos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, otro de los hallazgos del informe es la contratación de una empresa que **realizaría encuestas para medir la imagen de la Policía.** **Según los documentos revelados por Arias, el contrato para practicar estos sondeos los 286 millones de pesos.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1309170616210206721","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1309170616210206721

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Tras dar a conocer la cifras, el senador [Arias](https://wilsonariascastillo.com/gobierno-pretende-gastarse-6-400-millones-este-ano-en-gorras-para-la-policia-wilson-arias/?fbclid=IwAR0AvHJSjOqxutcu-zrFhSYuGiLsxSnq4-j-TP4bX7LToBlCPt8DogMk-Ks), criticó fuertemente al Gobierno señalando que «*sigue invirtiendo en represión en medio de una aguda crisis económica y social del país*” y agregó que «*el Gobierno no se está preparando para solventar los grandes problemas de la economía y de la sociedad, sino para contener la protesta ocasionada*» por esas causas.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
