Title: Tres elementos claves para garantizar unas elecciones transparentes
Date: 2019-10-21 17:36
Author: CtgAdm
Category: Entrevistas, Política
Tags: Elecciones 2019, elecciones regionales, Trashumancia electoral
Slug: tres-elementos-claves-para-garantizar-unas-elecciones-transparentes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Elecciones-MOE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

Llegada la última semana previa a las elecciones regionales del país, con una alerta de más de 2.600 posibles  casos de delitos electorales y una incertidumbre ante un Gobierno que no ha emitido el decreto de orden público que regula hasta cuándo se puede hacer campaña, la **Misión de Observación Electoral (MOE)** revela qué elementos  y prácticas deben ser tenidas en cuenta para garantizar y facilitar la transparencia electoral el próximo 27 de octubre.

### Observar el accionar de funcionarios públicos

Camilo Vargas, coordinador del Observatorio político de la MOE afirma que se debe priorizar la vigilancia por parte de la **Procuraduría** al comportamiento de los funcionarios públicos y a la participación política que realicen esta semana, incluyendo todo tipo de presión que podrían ejercer sobre las personas o la forma de utilizar recursos públicos para favorecer un candidato, dos de las más comunes denuncias que llegan a la entidad.

Según la Registraduría  existen 16 delitos electorales que los ciudadanos deben tener presente de cara a las elecciones que se celebrarán el próximo 27 de octubre, entre ellos, el constreñimiento al sufragante, con el que se presiona o amenaza a un ciudadano para votar por determinado candidato, partido o corriente política.

Con respecto a la participación política de funcionarios públicos, **según la Procuraduría se han recibido alrededor de 950 quejas electorales, 350 relacionadas con este tema** y un total de 22 servidores públicos suspendidos por esta práctica.

### Veeduría al escrutinio

Vargas agrega que la mayor precaución debe existir en el conteo de votos, que comenzará a partir de las 4 pm y que según el investigador, dependiendo la región podría extenderse días, semanas e incluso meses en un proceso que debería ser mucho más ágil, "el conteo de los votos se tramita en las comisiones escrutadoras y lo que vemos en los medios es netamente informativo pero el verdadero resultado se da cuando ya no se presta atención", explicó.

Al respecto, la MOE ha pedido el máximo acompañamiento por parte de testigos electorales y que exista transparencia en el procedimiento, pues según el último mapa de riesgo de la entidad, se habla de posible fraude en 461 municipios del país, de los que destacan 26 casos de alerta máxima.

### Una jornada de elecciones sin violencia

Vargas señala que es necesario prestar una especial atención a regiones como el norte de Antioquia, Chocó, la Costa Atlántica y La Guajira en particular municipios como **Urumita, Hatonuevo y La Jagua del Pilar** donde hay percepción de disturbios y en Barrancas donde se ha alertado de una posible "piedratón" contra la sede de la Registraduría, hechos que serían impulsados por candidatos que creen, es probable pierdan la contienda electoral el próximo domingo.

Según la MOE, El despliegue logístico para estas elecciones, incluye **un total de 3.340 observadores que trabajarán en 566** [de los] **1.122 municipios de Colombia,** donde debe garantizarse la presencia de una autoridad electoral a  través de las mesas de justicia que estarán ubicadas en cada lugar de votación. [(Lea también: Tres medidas para frenar la violencia política según la MOE)](https://archivo.contagioradio.com/tres-medidas-para-frenar-la-violencia-politica-segun-la-moe/)

<iframe id="audio_43333741" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_43333741_4_1.html?c1=ff6600"></iframe>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
