Title: Suspenden proceso contra 13 jóvenes de Congreso de los Pueblos y pasaría a la JEP
Date: 2017-10-05 17:33
Category: Judicial, Nacional
Tags: congreso de los pueblos, Falsos Positivos Judiciales
Slug: suspenden-proceso-13-jovenes-congreso-los-pueblos-pasaria-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/los-13-jovenes-detenidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [5 Oct 2017] 

El proceso en el que mediáticamente se acusó a 13 jóvenes, 10 de los cuales pertenecen al Congreso de los Pueblos, de haber sido responsables de varias explosiones ocurridas en Bogotá, pasaría a ser analizado por la Justicia Especial para la Paz **luego de que el juez del caso recibiera la solicitud por parte de abogados de la defensa** que solicitaron a la secretaría ejecutiva de la JEP que conozca el caso.

Según lo explica el abogado Manuel Garzón, defensor de uno de los procesados, se está a la espera de que la secretaría ejecutiva tramite la solicitud. Sin embargo, ya hay un precedente positivo, puesto que **esa instancia ya recibió el caso de 3 estudiantes** pertenecientes a la Federación de Estudiantes Universitarios, que fueron acogidos por esa jurisdicción y ello podría augurar un resultado positivo en este trámite.

El argumento de la defensa se basa en que **el acuerdo con las FARC deja abierta la puerta para que personas acusadas de delitos en el marco de la protesta social o disturbios internos puedan acogerse a la JEP**. Tal como lo establece la Ley 1820.

Esto no significaría que habría un reconocimiento de responsabilidad, sino que el proceso parte de una acusación por parte de la fiscalía en delitos relacionados con la Ley de Seguridad Ciudadana y no necesariamente por relación directa con las guerrillas u otros grupos armados.

Garzón asegura que, en caso de ser aceptado, el proceso sería conocido por la sala de definición de situaciones jurídicas y no las salas de amnistías o indultos o de reconocimientos de verdad, dado que ese es un escenario en el que se definirá la suerte del proceso. En **el acuerdo se incluye el objetivo “aplicar mecanismos de cesación de procedimientos y de renuncia al ejercicio de la acción judicial”.**

Según Garzón, el juez del caso debería aplicar el principio de “economía procesal” en el que no tiene sentido seguir adelantando un proceso en el que se sabe de antemano que va a terminar en la Jurisdicción Especial de Paz. Incluso para el abogado este caso **sería un “caso tipo” en el que esta sala definirá una situación en la que están inmersas personas detenidas en el marco de la protesta social.**

<iframe id="audio_21306996" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21306996_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
