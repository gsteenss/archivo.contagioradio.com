Title: Comunidades rurales opinan sobre renuncia de Martínez Neira
Date: 2019-05-16 17:03
Author: CtgAdm
Category: Comunidad, Nacional
Tags: COCCAM, comunidades indígenas, Nestor Humberto Martínez, zonas de reserva campesina
Slug: comunidades-rurales-opinan-sobre-la-renuncia-de-martinez-neira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/comunidades-fiscal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archvio] 

A raíz de la renuncia de **Néstor Humberto Martínez Neira como cabeza de la Fiscalía General** y de la decisión tomada por la JEP al aplicar la garantía de no extradición a Jesús Santrich, son muchas las opiniones que han surgido al respecto y que deben ser escuchadas como es el caso de las diferentes comunidades campesinas, étnicas e indígenas quienes coinciden en que la JEP actuó de forma correcta y que la salida del ex fiscal de esta entidad permitirá que se deje de obstruir la implementación de los acuerdos, al menos desde este ente.

**Jani Silva integrante de la Zona de Reserva Campesina Perla Amazónica** manifestó que la salida de Martínez "es una  estrategia que utiliza por estar arrinconado con el tema de Odebrecht" sin embargo advirtió que como comunidades rurales deben continuar atentos a lo que suceda en el país con estas decisiones.

<iframe src="https://co.ivoox.com/es/player_ek_35951545_2_1.html?data=lJqml5aZeJahhpywj5WZaZS1kZeah5yncZOhhpywj5WRaZi3jpWah5yncavVz86Ytc7QusKZk6iYzoqnd4a1pcnS1MrXpYzO0NPOjcnJb7PZ1Mrf2MaPh8Lh0crgy9PFb7HZ09HOjaaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### Martínez Neira y la polarización

Por su parte, Deivin Hurtado del Movimiento Campesino de Cultivadores de Coca, Amapola y Marihuana (COCCAM), sector que ha rechazado las declaraciones de Neira a favor del glifosato y la erradicación forzada, considera su renuncia una noticia positiva que puede devolver a la Fiscalía una legitimidad "que había perdido con la corrupción, y el tema de Odebecht".

<iframe src="https://co.ivoox.com/es/player_ek_35951928_2_1.html?data=lJqml5addpmhhpywj5WZaZS1k5Wah5yncZOhhpywj5WRaZi3jpWah5yncaXZytvW0JCsudPowsnch5enb6TDpKiur5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[**Neis Lame consejero mayor del CRIC** apuntó que Martínez Neira no contribuía a la construcción de paz en medio de un estado de polarización por lo que el permanecer en su cargo "no iba a legitimar a la justicia", una situación que contrasta con lo sucedido en el marco de la Minga del suroccidente del país cuando por declaraciones del propio fiscal, el presidente Duque no asistió a la reunión con las comunidades étnicas en el Cauca. ][(Puede leer: Por primera vez veo a Néstor Humberto Martínez acorralado: Ramiro Bejarano)](https://archivo.contagioradio.com/por-primera-vez-veo-a-nestor-humberto-martinez-acorralado-ramiro-bejarano/)

<iframe src="https://co.ivoox.com/es/player_ek_35951770_2_1.html?data=lJqml5abe5Ghhpywj5WaaZS1kpiah5yncZOhhpywj5WRaZi3jpWah5ynca_ZytiYrsbRqYampJDQ0dPXqcvZ09SYz8bds9OfxcrZjai2jaShhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### Comunidades respaldan a la  JEP

Tanto Jani como Deiner coincidieron en que la JEP tomó una decisión acertada argumentando que no se aportaron pruebas suficientes para extraditar al exnegociador de las Farc, "es necesario seguir respaldando esta figura ya que es uno de los fundamentos principales para la construcción de la paz".[(Lea también: Renuncia el fiscal general de la nación, Néstor Humberto Martínez)](https://archivo.contagioradio.com/renuncia-el-fiscal-general-de-la-nacion-nestor-humberto-martinez/)

"Las Farc mostraron un gesto  de voluntad en el proceso, cosa que el Estado no está garantizando, poco a poco están matando a excombatientes y a líderes comunales y todo queda en la impunidad, es como si volviéramos al tiempo de antes" agrega la lideresa social.

"Más que el caso Santrich debemos ver lo de fondo, no a las personas, la JEP tomó una decisión porque es autónoma y hoy vemos que hay preocupación en el fiscal porque van a salir a relucir algunas verdades" afirma Lame apelando a dejar de lado la posición política que se asume con la JEP.

### ¿Qué se espera de un nuevo fiscal? 

Deivin Hurtado señaló que es necesario recuperar la institucionalidad, **"necesitamos que hayan funcionarios que se den cuenta de las dificultades que tienen las comunidades colombianas"** afirmó refiriéndose a las comunidades que  se ven obligadas a convertir los cultivos de uso ilícito en su forma de economía, por lo que es necesario un tratamiento diferencial para no afectar "a quienes menos tienen sino atacar a los que se enriquecen".

Los tres líderes sociales, coincidieron en que la persona que llegue a ocupar el cargo que deja Martínez Neira debe ser una persona que "vele por la justicia y tenga en cuenta el proceso de paz", afirma Jani Silva a lo que agrega el líder indígena que esperan, tenga "transparencia, responsabilidad  y autonomía en el sentido de no polarizar", agregando que esperan que el próximo fiscal dé a conocer la verdad.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
