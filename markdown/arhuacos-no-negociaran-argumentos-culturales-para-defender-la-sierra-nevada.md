Title: Argumentos cosmogónicos no son negociables: Confederación Indígena Tairona
Date: 2017-12-18 16:24
Category: DDHH, Nacional
Tags: arhuacos, indígenas, pueblos indígenas, Sierra Nevada, tayrona
Slug: arhuacos-no-negociaran-argumentos-culturales-para-defender-la-sierra-nevada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/arhuacos-e1511199795562.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Confederación indígena Tayrona] 

###### [18 Dic 2017] 

La Confederación Indígena Tayrona, en conjunto con representantes del Gobierno Nacional, **instalaron, la mesa de concertación**. Los indígenas le hicieron saber al Gobierno Nacional que los argumentos cosmogónicos sobre la defensa del territorios, no son negociables y deben estar incluidos en la deliberación sobre la protección de la Sierra Nevada de Santa Marta.

De acuerdo con Leonor Zalabata, autoridad indígena e integrante de a Confederación Indígena Tayrona, la **permanencia cultural de los pueblos indígenas** ha sido el tema transversal en la construcción de las exigencias para que la Sierra Nevada sea un territorio libre de minería y proyectos industriales. Indicó que “liberar la Sierra de hidrocarburos, minería y mega proyectos, es el propósito de la movilización”. (Le puede interesar: ["Arhuacos exigen que la Sierra Nevada sea zona libre de minería"](https://archivo.contagioradio.com/arhuacos-exigen-que-la-sierra-nevada-sea-zona-libre-de-mineria/))

**Gobierno Nacional “desconoce propuestas de cosmogonía” de comunidades indígenas**

Zalabata indicó que el Gobierno Nacional **“dice mantener las consideraciones** en las que se destaca la importancia de la Sierra Nevada de Santa Marta pero, sin hacer menciones especiales a la concepción que tienen los pueblos indígenas sobre el origen del mundo y otras cuestiones”. Esto, para las comunidades es un irrespeto hacia la cultura indígena debido a que esto discrimina “las minorías étnicas”.

Es decir que las autoridades gubernamentales, les han pedido a los indígenas que **“eliminen del texto de propuestas la cosmogonía** de las comunidades” para poder discutir simplemente aspectos jurídicos que relacionan la conservación de los territorios ancestrales. Esto para los arhuacos es “negar la diversidad cultural y el derecho a una cultura de un pueblo indígena”.

Otro aspecto que le ha generado confusión a los pueblos indígenas tiene que ver con las disposiciones del Gobierno que afirman “reconocen que las actividades no concertadas con los pueblos indígenas nativos en los territorios ancestrales de la línea negra, generan riesgos, impacto y afectación”. Según Zalabata, esta afirmación **genera confusión** en la medida que “se podría entender que las actividades consentidas por las comunidades no generan afectaciones”. (Le puede interesar: ["Sierra Nevada es como un cuerpo humano" no se le puede quitar un pedazo"](https://archivo.contagioradio.com/decision-del-ministerio-de-ambiente-no-protege-la-totalidad-de-la-sierra-nevada/))

### **Mesa técnica no tuvo la presencia de funcionarios de alto nivel** 

Los indígenas han pedido en varias ocasiones que los funcionarios que representan al Gobierno Nacional, **tengan poder de decisión** al momento de establecer los acuerdos con el pueblo arhuaco. Sin embargo, Zalabata manifestó que esto no ha sucedido.

Los arhuacos recordaron que el Gobierno Nacional ha impuesto, por encima de los derechos de las comunidades, **“el desarrollo de la minería”**.  Por esto, le recordaron al Gobierno Nacional que la Sierra libre de minería no es algo que se vaya a negociar pues "la Sierra Nevada surte agua a más de 3 millones de personas y todo el ecosistema debe ser protegido". (Le puede interesar: ["Más de 1000 solicitudes mineras amenazan la Sierra Nevada de Santa Marta"](https://archivo.contagioradio.com/arhuacos-continuan-en-movilizacion-exigiendo-se-respeten-sus-derechos-sobre-la-sierra-nevada/))

Con las autoridades acordaron continuar con el desarrollo de un **mecanismo jurídico que proteja** los cerca de 17 mil km a la redonda que tiene "el macizo de la Sierra que es independiente de las demás cordilleras". Para continuar con los diálogos, se reunirán nuevamente en enero de 2018. Además, el próximo año continuará la movilización indígena para hacerle saber al presidente Santos que "la protección de la Sierra es su responsabilidad".

###### <iframe id="audio_22715428" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22715428_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
