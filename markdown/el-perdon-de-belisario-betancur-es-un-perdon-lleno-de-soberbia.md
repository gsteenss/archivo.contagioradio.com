Title: El perdón de Belisario Betancur es un "perdón lleno de soberbia"
Date: 2015-11-04 19:30
Category: DDHH, Nacional
Tags: 30 años palacio de justica, 30 años palacio de justicia-la memoria y el arte, 6 de noviembre de 1985, Acto perdón público Palacio de justicia, Belisario Betancur, Juan Manuel Santos, Palacio de Justicia, René Guarín
Slug: el-perdon-de-belisario-betancur-es-un-perdon-lleno-de-soberbia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/belisario-betancurt.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Zonacero.com 

<iframe src="http://www.ivoox.com/player_ek_9273839_2_1.html?data=mpeklZ2XfY6ZmKiak5mJd6KklpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmscrfxoqnd4a2lNOYxsqPl8Li1dTgjcnJsIzk04qwlYqmd9ndztSYmJDIqYzi0NvWx9LGtsafxtiY19OPtMahhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [René Guarín, hermano desparecida] 

###### [4 nov 2015] 

Este viernes se **cumplen 30 años de la toma y retoma del palacio de justicia**, por la que **la** **nación ha sido condenada en 15 oportunidades, **ante su incontrovertible ** **responsabilidad en la muerte de al menos 100 personas y la desparición de otras 12, víctimas por las que aún sus familiares y un amplio sector de la sociedad siguen exigiendo justicia y verdad.

El **ex-presidente Belisario Betancur**, mandatario de la época y quién dio la orden de ingreso del ejército al Palacio de Justicia y no detuvo las operaciones pese a lo horroroso de la retoma pidió perdón, indicando "Si errores cometí pido perdón a mis compatriotas, por esos errores que nunca fueron nada distinto de mi búsqueda de la paz que el presidente Santos está buscando con ansiedad para todos los colombianos".

Para **René Guarín,** hermano de Cristina del Pilar Guarín, el perdón del expresidente Betancur **está “cargado de soberbia”** considerando que lo sucedido en el Palacio enseña que el perdón **debe estar dirigido hacia el “reconocimiento de los crímenes”** y se necesita “que vaya acompañado de la verdad”.

Guarín, afirma que este "no es un perdón reparador", **ni "apunta a la reconciliación"**  en cambio "atiza la hoguera de los odios", indica el perdón debe nacer “de corazón”, siendo un perdón que no sea ofensivo con las víctimas” y en cambio busca que el Estado reconozca su participación, **responsabilidades que a su criterio Betancur no asumió y Santos tampoco hará**.

La indignación de los familiares a la actitud del presidente Santos, está justificada en la posición del mandatario de "acatar y respetar" las decisiones en contra de Betancur "pero no callar", por el contrario le pidió perdón  en nombre de los colombianos al exmandatario "por todas las ofensas", disculpa hizo extensiva al ejército nacional y las fuerzas militares del país.

En cumplimiento de la **sanción proferida por la Corte Interamericana de Derechos Humanos** el año anterior, el Presidente de la república Juan Manuel Santos en representación del Estado **tendrá que pedir perdón en un acto público**, mismo que Guarín considera un “cumplido que tiene que hacer el Estado colombiano” y será el único familiar que no asistirá declarando que no  va a ser "el florero de poner allí para la foto”.

La decisión tomada aproximadamente hace 8 meses por Guarín, esta motivada además por el incumplimiento de otras obligaciones del Estado “no he tenido atención psicosocial (...) los pagos que se ordenaron no se han hecho; y lo que es peor el tema de la verdad”, por la intervención del EStado en las investigaciones que no han permitido llegar a la misma.
