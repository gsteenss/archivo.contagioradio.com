Title: Libertad y absolución para Feliciano Valencia
Date: 2017-06-28 15:18
Category: Judicial, Nacional
Tags: Feliciano Valencia, justicia, Libertad
Slug: feliciano-valencia-libre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/IMAGEN-16375313-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:El tiempo 

###### 28 Jun 2017 

La corte suprema de Justicia ordenó este miércoles la libertad inmediata para el líder indígena de la comunidad Nasa del Cauca [Feliciano Valenci](https://archivo.contagioradio.com/?s=feliciano)a, quien había sido condenado a 18 años de cárcel por el supuesto secuestro y las lesiones a un soldado que infiltró las protestas indígenas en el 2008.

La decisión tomada por la Sala de Casación Penal de la Corte suprema de justicia,  en ponencia presentada por el magistrado Eugenio Fernández Carlier, considera que las acciones del líder actuó en ejercicio de su autoridad indígena, y en aplicación de su jurisdicción la detención del uniformado no puede ser considerado como un secuestro.

Por el fallo absolutorio, la corte resuelve casar la sentencia condenatoria Feliciano Valencia, emitida el 10 de septiembre de 2015 por el Tribunal Superior de Popayán y confirma la sentencia emitida el 24 de marzo de 2015 por el Juzgado Primero Penal del Circuito del Circuito Especializado de Popayán que absolvió al líder indígena por el delito de secuestro.

El dictamen ordena la libertad "inmediata e incondicional del procesado Feliciano Valencia Medina, la cual se hará efectiva si no es requerido por otra autoridad" y disponer que el juez de primer grado cancele los registros y anotaciones que contra el procesado haya originado este diligenciamiento.

Aquí el documento completo.

[SP9243-2017 Caso Feliciano Valencia](https://www.scribd.com/document/352480238/SP9243-2017-Caso-Feliciano-Valencia#from_embed "View SP9243-2017 Caso Feliciano Valencia on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_5536" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/352480238/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-0z3JAY05SDIBt6SL93Oc&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.75"></iframe>
