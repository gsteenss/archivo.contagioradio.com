Title: ¿Qué le ha dejado a Colombia 4 años de TLC con Estados Unidos?
Date: 2016-05-17 12:19
Category: Nacional
Tags: Impactos TLC en Colombia, TLC Colombia Estados Unidos, Tratados de Libre Comercio
Slug: que-le-ha-dejado-a-colombia-4-anos-de-tlc-con-estados-unidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/TLC-Col-EEUU.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Colombiano ] 

###### [17 Mayo 2016]

Tras cuatro años de haberse firmado el TLC entre Colombia y Estados Unidos, el balance es negativo, según Fabio Arias, Secretario General de la Central Unitaria de Trabajadores, quien asegura que este Tratado ha significado la "pérdida de materias primas valiosas", el **detrimento de las condiciones laborales, el aumento de la inflación y múltiples violaciones a los derechos humanos**.

### EMPLEO 

En los últimos cuatro años, en el marco del TLC y del Plan de Acción Laboral, **877 mil trabajadores entraron al mercado de la informalidad, en el sector industrial se perdieron 255 mil empleos**, mientras que en el último año, en el de hidrocarburos la pérdida fue de 70 mil puestos.

### IMPORTACIÓN 

En Colombia **actualmente se importan 12 millones de toneladas de alimentos, entre ellos el maíz, el arroz, la papa y el café** que debería estarse cultivando en nuestros territorios con el respaldo del Estado; como sucede en EEUU donde toda la producción agropecuaria es subsidiada, mientras que en nuestro país los subsidios se quitaron para favorecer a la industria estadounidense.

De acuerdo con Arias, se **desestimuló la producción agropecuaria nacional, para promover la importación de los excedentes agrícolas de EEUU**, y hoy por hoy, somos la mayoría de los colombianos quienes "estamos pagando los platos rotos" de la falta de planeación de una política estatal de seguridad alimentaria.

### INFLACIÓN 

"Nos han llenado de productos importados, que han ocasionado la mayor inflación de los últimos 15 años en Colombia", asegura Arias, e insiste en que la alta tasa de importación sumada a las rebajas en los aranceles de productos como el azúcar, ha llevado a que **la inflación haya aumentado en un 50% durante los dos últimos años**.

### DERECHOS HUMANOS 

Durante los últimos 5 años, 125 dirigentes y líderes sindicales han sido asesinados, sin que a la fecha hayan acciones de las instituciones estatales para reducir **la impunidad que ya se registra en un 95%**.

En los dos últimos años, **se han asesinado a 99 defensores de derechos humanos**, situación que se ha recrudecido por la presencia de las empresas multinacionales y los mecanismos que se han puesto en marcha para impedir la restitución de tierras, sumados a los efectuados para **reprimir a quienes denuncian los impactos ambientales de las compañías en los territorios**.

"Ningún país debería dejar de producir los alimentos para mantener a su población (...) un país soberano debe garantizar que su población tenga los suficientes alimentos para no depender de otros", teniendo en cuenta que **la dependencia alimentaria beneficia al capital privado, que es en últimas el que controla los precios**, concluye Arias, y extiende la invitación a participar del foro en el que se van a ampliar con mayor detalle los impactos del TLC Colombia-Estados Unidos.

Sindicatos hablan sobre los resultados del TLC Colombia-EE-UU:

\[embed\]https://www.youtube.com/watch?v=z0elvnkmuI8\[/embed\]  
<iframe src="http://co.ivoox.com/es/player_ej_11564349_2_1.html?data=kpaimJmXeJqhhpywj5acaZS1lp6ah5yncZOhhpywj5WRaZi3jpWah5yncafVw87cjabWrcLnhpewjbjJp9PZ1cbfy9SPi8bixtfOzpDIqYzgwpCwt7mRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
