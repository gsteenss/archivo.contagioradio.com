Title: No convencen explicaciones del Ejército tras intento de retención a líder Nasa
Date: 2020-01-20 17:30
Author: CtgAdm
Category: DDHH, Nacional
Tags: Camioneta, ejercito, Líder Indígena, Yesid Conda
Slug: no-convencen-explicaciones-del-ejercito-tras-intento-de-retencion-a-lider-nasa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Ejército-intenta-retener-a-Yesid-Conda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @Marianiniecheve {#foto-marianiniecheve .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El domingo 19 de enero el Consejo Regional Indígena del Cauca (CRIC) denunció el desarrollo de un procedimiento irregular por parte de efectivos del Ejército y la Policía contra el líder indígena Yesid Conda, a quién pretendieron retener y trasladar sin ninguna razón. Aunque la división del Ejército que opera en la zona emitió un comunicado explicando los hechos, Conda aún tiene dudas sobre el operativo en su contra.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Los uniformados llegaron a su casa y retuvieron a sus escoltas**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Yesid Conda es líder del pueblo Nasa, fue gobernador del resguardo indígena de Pitayó en dos ocasiones (2017 y 2019), y **ha sido víctima de varias amenazas y dos atentados**, razón por la que tiene asignados dos hombres de seguridad y una camioneta por parte de la Unidad Nacional de Protección (UNP).

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El líder vive en el resguardo de Pitayó en el municipio de Silvia (Cauca), y según relató, mientras se disponía a salir de allí junto a su esposa y familia el pasado domingo a realizar labores de agricultura, fue abordado por hombres armados que se desplazaban en una camioneta Nissan doble cabina negra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los hombres armados se bajaron de la camioneta, inmediatamente inmovilizaron a uno de los escoltas de Conda contra el vehículo mientras al otro lo detuvieron contra una casa, "me querían subir a esa camioneta", relató el líder. Según su testimonio, **ninguno de los hombres se identificó o dijo para qué lo requerían.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Conda señaló que en la camioneta había un par de botas de caucho, y los uniformados intentaron amedrentarlos con sus armas a él y su esposa, ambos forcejearon y pusieron en alerta a la comunidad. Sus vecinos salieron y rodearon el vehículo, entonces **"nos tiran el carro encima y emprenden la huída como delincuentes"**, afirmó.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Preguntas** para el Ejército

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/YesidConda/status/1219232759752921089","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/YesidConda/status/1219232759752921089

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Luego que emprendieron la huída, los indígenas los siguieron hasta la estación de Policía de Jambaló, donde se escondieron y les dieron explicaciones, que quedaron también expresadas en un comunicado publicado por la Tercera División del Ejército. (Le puede interesar: ["Militarizar más al Cauca es una propuesta "desatinada y arrogante": ACIN"](https://archivo.contagioradio.com/militarizar-mas-al-cauca-es-una-propuesta-desatinada-y-arrogante-acin/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la comunicación señalan que confundieron la camioneta de Conda con otra que hizo caso omiso a un puesto de control establecido por los uniformados en la vía. Sin embargo, el líder aseguró que **su camioneta nunca fue registrada, los hombres armados llegaron directamente a reducir sus escoltas y llevarlo a él.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Ejercito_Div3/status/1218997703700512769","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Ejercito\_Div3/status/1218997703700512769

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En el intento de retención de Conda participaron 11 uniformados, 10 pertenecientes al Ejército y un hombre más, integrante de la Policía. De acuerdo a la descripción del líder y las fotos de lo ocurrido, **los hombres portaban armas largas y pertenecían a la cuarta división del Ejército.** (Le puede interesar: ["CRIC no permitirá que asesinato del comunero Deiner Yunda quede en la impunidad"](https://archivo.contagioradio.com/cric-exige-esclarecimiento-del-asesinato-del-comunero-deiner-yuda/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, sostuvo que la camioneta que tiene asignada no se movió esa mañana antes de la llegada de los uniformados, hecho que se puede corroborar con el sistema de monitoreo por GPS que tiene instalado. Por último, el [líder](https://tierradentro.co/lider-indigena-en-el-cauca-nueva-victima-de-la-fuerza-publica/#.XiSGDUqqSj0.whatsapp) señaló que los hombres, que serían integrantes de los comandos especiales **adscritos a la Cuarta Brigada, hacen parte de un grupo que no opera en Cauca.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para concluir Conda puso en duda el papel que desempeña la Fuerza Pública, pues en la zona opera un Batallón y una móvil, adicionalmente, hay seis bases militares; pero **este pie de fuerza no ha evitado que "nuestros territorios sigan siendo la ruta para sacar todo lo ilícito".**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1218993249865601024","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1218993249865601024

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:html -->  
<iframe id="audio_46802814" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46802814_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
