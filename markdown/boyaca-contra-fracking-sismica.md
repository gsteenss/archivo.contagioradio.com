Title: 14 municipios de Boyacá ganaron la pelea contra el Fracking
Date: 2018-09-11 15:05
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Boyacá, fracking, Sísmica
Slug: boyaca-contra-fracking-sismica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-15-a-las-12.54.38-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [11 Sept 2018]

[El pasado 6 de septiembre se conoció la determinación del Fondo Financiero de Proyectos de Desarrollo de **adelantar la terminación del contrato con la empresa Geofyzica Torum Services Sucursal Colombia,** que planeaba hacer **sísmica en 14 municipios de Boyacá.** Este dictamen fue resultado de la unión de las comunidades y el gobierno departamental en contra de la sísmica que planeaba realizarse en la zona.]

[La Red de Colectivos Ambientales de Boyacá afirma que la decisión del FONADE se da a partir de un derecho de petición entregado por los habitantes de los municipios afectados, que fue acompañada por **diversas manifestaciones y acciones ciudadanas, promovidas tanto por las organizaciones de protección ambiental, como por el Gobernador de Boyacá,** en cabeza de Carlos Andrés Amaya, y otros actores del departamento. (Puede interesarle: [¿Existe Fracking en Boyacá?](https://archivo.contagioradio.com/existe-fracking-en-boyaca/))]

[Teresa Suárez, representante de la Red de Colectivos Ambientales de Boyacá, aseguró que la **sísmica causaría serios problemas hídricos en los municipios**, y que es una situación compleja, tomando en cuenta la vocación agropecuaria del departamento. Hecho que motivó a la población a unirse para acabar con esta práctica, “vamos a proteger Boyacá de la sísmica y el Fracking porque nosotros ya entendimos lo que es eso”. ]

### **¿Por que el Gobierno aprueba las licencias si dice estar en contra del Fracking?** 

[La Red de Colectivos Ambientales pone en duda las gestiones de los permisos para las actividades relacionadas con fracturamiento hidráulico**, tomando en cuenta**, que aunque el Gobierno Nacional hace pronunciamientos constantes asegurando que Colombia no es territorio de Fracking, **ya se habían otorgado dos licencias ambientales de perforación de dos pozos exploratorios de Yacimientos No Convencionales** en Chiquinquirá. (Le puede interesar: [\#TerritoriosLibresDeFracking)](https://archivo.contagioradio.com/territorios-libres-de-fracking/)]

[Finalmente, Suárez realizó un llamado al Gobierno Nacional para que analice sus decisiones ambientales, respetando también las determinaciones que tomen las comunidades al respecto de sus territorios, y aseguró que no dejarán que se desarrollen estudios de sísmicas en el departamento “**Boyacá dará ejemplo en Colombia y el mundo** de proteger los páramos, el agua y los campesinos”. También aseguró que se organizará una celebración comunitaria junto con los alcaldes de los 14 municipios.]

<iframe id="audio_28492838" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28492838_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

<div class="osd-sms-title" style="text-align: justify;">

</div>

</div>
