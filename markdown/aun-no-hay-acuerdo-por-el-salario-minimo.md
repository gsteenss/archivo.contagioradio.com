Title: No hubo acuerdo por aumento al salario mínimo
Date: 2016-12-29 15:25
Category: Economía
Tags: 2017, colombia, negociación, salario minimo
Slug: aun-no-hay-acuerdo-por-el-salario-minimo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/captura-pantalla-2016-12-15-19-02-28-1200x600.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Publimetro 

##### 29 Dic 2016 

Este jueves se levanto la mesa de negociación por el aumento al salario mínimo de manera concertada sin alcanzar un acuerdo entre las partes, aun cuando **los trabajadores bajaron su pretensión de conseguir un incremento del 14% al 8.5% y los empresarios subieron la suya hasta el 7%.**

De acuerdo con Julio Roberto Gómez, presidente de la CGT, **ahora queda en manos de la Ministra de trabajo quien deberá adelantar consultas con el Presidente de la República y el Ministro de hacienda**, bajo la nueva propuesta presentada por la representación de los trabajadores.

Al término de la reunión, la ministra de trabajo Clara López Obregón, aseguró que "**antes de levantar la mesa, centrales y gremios dijeron que no era la última palabra**" asegurando que seguirá buscando hasta el último momento acercamiento por acuerdo. Le puede interesar:[La CUT no va más en negociación por aumento al salario mínimo](https://archivo.contagioradio.com/la-cut-no-va-mas-negociacion-aumento-al-salario-minimo/).

La jefe de la cartera resaltó lo "Constructiva que ha sido esta mesa, e**n 12 años de negociaciones nunca existió un mejor clima de negociación**" y que se propone "buscar que la concertación sea el camino de la Paz en Colombia". De no lograrse un acuerdo el gobierno fijará el incremento para 2017 por Decreto.
