Title: FARC-EP denuncia "agresiones sistemáticas" por parte del INPEC
Date: 2017-01-12 17:18
Category: Nacional, Paz
Tags: FARC, Implementación de Acuerdos, INPEC, prisioneros políticos, Trato indigno en cárceles de Colombia
Slug: farc-ep-denuncia-agresiones-sistematicas-por-parte-del-inpec
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/presospolíticos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AVES] 

###### [12 Enero 2017] 

Objetos en los alimentos, golpizas, requisas constantes y daños personales, son algunas de las razones que organizaciones defensoras de derechos humanos, delegaciones de las FARC-EP y  prisioneros políticos recluidos en el pabellón 4 de la cárcel Picota en Bogotá y en la cárcel de Bella Vista de Medellín, han señalado como evidencia de una "agresión sistemática" por parte del Instituto Nacional Penitenciario y Carcelario –INPEC-, contra los integrantes de las FARC recluidos en dichos centros.

### A pesar de las denuncias, los inconvenientes persisten 

En repetidas ocasiones, se han presentado denuncias sobre los tratos indignos en las cárceles, la más reciente ha sido presentada por Rodrigo Londoño jefe de la delegación de las FARC-EP, en la que manifiesta “se han presentado reiteradas novedades con el suministro de alimentos, **aparecen objetos en la sopa y el arroz como: agujas, colillas de cigarrillos, pedazos de plásticos, fibras y vidrios** (…) todo esto ha sido puesto en conocimiento de las autoridades penitenciarias así como de entes de control y organismos internacionales como la Misión de Apoyo al Proceso de Paz en Colombia -MAPP-OEA-”.

Según Rodrigo Londoño, se trata de un "incumplimiento de lo pactado en los Acuerdos de La Habana” y asegura que pese a anteriores denuncias, los hechos persisten. Por su parte **los voceros del pabellón 4 de la Picota manifestaron que no recibirán alimentos hasta tanto no hayan garantías de seguridad. **Le puede interesar: [“En las cárceles de Colombia torturan a los prisioneros” informe CCCT.](https://archivo.contagioradio.com/en-las-carceles-de-colombia-torturan-a-los-prisioneros/)

Por último, en el comunicado, los integrantes de las FARC recluidos en Medellín y Bogotá aseguran que se trata de una alerta temprana al mecanismo de Monitoreo y Verificación y solicitan expresamente que **“de manera inmediata se haga presente el gerente de SERVIALIMENTAR** para dar soluciones de fondo a esta grave problemática y también se haga presente **un delegado del Alto Comisionado de Paz y de la dirección general del INPEC”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
