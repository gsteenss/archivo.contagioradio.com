Title: Revelan irregularidades en el modelo de aseo de Bogotá
Date: 2017-09-13 10:59
Category: Movilización, Nacional
Tags: Asedo de Bogotá, Distrito, Enrique Peñalosa, recicladores
Slug: revelan-irregularidades-en-el-modelo-de-aseo-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/basura-bogotá-e1503091849415.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagio radio] 

###### [12 Sept 2017] 

Son varias las irregularidades que el gremio de recicladores, habitantes de la ciudad y congresistas han encontrado en el modelo de aseo de Bogotá implementado desde el distrito. **Por ello las mismas organizaciones de recicladores y el representante a la Cámara Victor Correa** han insistido en que se realice un debate público este miércoles 13 de Septiembre a partir de las 9 de la mañana.

La idea de la convocatoria es discutir en torno a los problemas sociales, ambientales y económicos que produce un mal manejo del sistema de Aseo. Según las organizaciones de recicladores el modelo propuesto está generando problemas de contaminación de las aguas, del suelo y el entorno, en el caso de “Doña Juana” y además excluye a quienes realizan los trabajos de reciclaje en todo el distrito.

En el caso de las organizaciones de recicladores la situación es dramática, por ello en enero de 2017 interpusieron ante el Tribunal Superior de Bogotá una acción de tutela para proteger sus derechos al mínimo vital, el trabajo y la igualdad, solicitando suspender el proceso licitatorio de la recolección de residuos no aprovechables, barrido y limpieza RBL, porque, según ellos, dicha licitación no incluye obligaciones contractuales de los futuros operadores aseo para garantizarles a los recicladores el acceso cierto y seguro a los residuos aprovechables. [Lea también: Demandan licitación de aseo en Bogotá](https://archivo.contagioradio.com/recicladores-demandan-licitacion-del-servicio-aseo-de-bogota/).

### **El modelo de pagos del sistema de aseo es excluyente** 

Además denuncian que el modelo de pago por peso del material recogido los pone en desventaja con las empresas y los camiones de recolección “los recicladores para ganarnos nuestro mínimo vital competimos contra el carro compactador, **tenemos que buscar en las bolsas de basura rápidamente antes de que los operarios recojan las bolsas y las echen al carro, y así no la pasamos todo el día, todos los días”**

Sobre este particular se ha manifestado la representante a la Cámara, Angela Robledo, quien ha denunciado que el pago por el peso del material recogido y entregado por cada empresa revela que no interesa potenciar el reciclaje "**¿Por qué a los operadores de basuras no les interesa promover reciclaje en la fuente?** Porqué entre más basura recolectan,más pueden cobrar" señala la congresista.![modelo de aseo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/DJczaDkXgAA_9Kx.jpg){.wp-image-46590 .alignleft width="270" height="141"}

Por otra parte, durante el último mes se han venido denunciando serios problemas generados en el relleno sanitario “Doña Juana” y los barrios circunvecinos, que sufren por la aparición de vectores como moscas y otros insectos, además del olor fétido producido por los lixiviados de las basuras allí depositadas. Además los trabajos de mitigación de estos problemas solamente han sido paliativos que no solucionan la problemática de fondo.

Por este motivo se espera que el Distrito de respuestas eficaces a las problemáticas que se han planteado y se desarrolle un modelo en el que prevalezca el derecho a la salud, al trabajo y a la vida digna de todos los habitantes de la capital que se han visto afectados.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
