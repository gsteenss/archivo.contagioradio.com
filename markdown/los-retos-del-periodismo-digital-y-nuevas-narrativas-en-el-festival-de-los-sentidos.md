Title: Los retos del periodismo digital y nuevas narrativas en el Festival de los Sentidos
Date: 2019-11-13 16:51
Author: CtgAdm
Category: eventos, Nacional
Tags: Festival de los Sentidos, Medios independientes, periodismo digital, Premio Nacional Periodismo Digital, radio digital
Slug: los-retos-del-periodismo-digital-y-nuevas-narrativas-en-el-festival-de-los-sentidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-09-20-at-5.40.35-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Comunicado-de-prensa-Festival-de-los-Sentidos-y-Premio-Nacional-de-Periodismo-Digital.pdf" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Invitación-PNPD-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Kienyke.com] 

La segunda edición del **[Festival de los Sentidos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Comunicado-de-prensa-Festival-de-los-Sentidos-y-Premio-Nacional-de-Periodismo-Digital.pdf) **tendrá lugar el próximo **14 de noviembre**, en la que expertos nacionales e internacionales se reunirán para debatir sobre los retos del periodismo digital y las nuevas formas de contar historias a través de las plataformas online. Asimismo, el encuentro abordará el futuro de la industria digital en Colombia.

Además de los paneles, en esta jornada también se hará entrega del **Premio Nacional de Periodismo Digital**, el cuál cuenta en esta primera edición de 13 categorías que reconocen los trabajos periodísticos realizados en ecosistemas digitales.

### **Un ejemplo de radio digital** 

Uno de los ponentes destacados en esta jornada es **Ole Jørgen Torvmark**, quién gestionó la transición que vivió Noruega de la **radio análoga a la digital**, convirtiéndose así en el primer país del mundo en apagar la radio FM nacional. Torvmark expondrá, tomando como referencia la transformación de su país, el proceso que conlleva la digitalización de los medios y cómo orientarse hacia estas nuevas tecnologías.

Además, también participarán destacados panelistas internacionales como **Eliezer Budasoff**, quien fue director editorial de The New York Times en Español o **Jaume Duch**, quién por más de treinta años ha gestionado la comunicación del Parlamento Europeo.

También panelistas nacionales, como **Víctor García Perdomo**, director de la Maestría en Periodismo y Comunicación Digital de la Facultad de Comunicación en la Universidad de La Sabana.

### **Nuevas formas de comunicación y democratización** 

**Daniela Viveros, líder del proyecto *KienyKe.com***, afirma que el festival no descarta el periodismo tradicional, puesto que considera que “Colombia es un país que aún necesita los medios de comunicación análogos, ya que todavía hay territorios que están desconectados del entorno digital”. Lo que buscan con este evento es destacar el papel de las herramientas digitales y las nuevas formas de comunicación.

En esa misma línea, también quieren destacar el papel de los medios digitales en la **democratización de la información**, puesto que “internet permite que sean los usuarios quienes creen su propia agenda mediática y no se vean limitados a la de los grandes medios” manifiesta Viveros.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Invitación-PNPD-1-1024x576.jpg){.aligncenter .size-large .wp-image-76454 width="1024" height="576"}

### [**Premio Nacional de Periodismo Digital**](https://www.premioperiodismodigital.com) 

En el festival, también tendrá lugar la **primera edición del premio**, el cuál ofrece **13 categorías**: caricatura e ilustración, crítica, crónica, entrevista, especial multimedia, generación de contenidos digitales por redes sociales, influenciadores, iniciativa nativo digital, noticia, opinión, reportaje o investigación y responsabilidad ambiental.

Daniela Viveros destaca que la mayoría de los postulados para este premio son **medios independientes y regionales**, los cuales considera de gran importancia para dar visibilidad a este tipo de medios que promueven la democratización de la información.

**Contagio Radio** estará en el Festival de los Sentidos como medio aliado llevando la información y promoviendo la democratización de los medios a través de las plataformas digitales.

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
