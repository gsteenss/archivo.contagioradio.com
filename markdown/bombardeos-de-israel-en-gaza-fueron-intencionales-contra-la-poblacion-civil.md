Title: "Bombardeos de Israel en Gaza fueron intencionales contra la población civil"
Date: 2015-05-04 15:24
Author: CtgAdm
Category: DDHH, El mundo
Tags: Ex-combatientes Israelíes acusan al Estado de masacre en Gaza, Masacre en Gaza 2014, Operación Margen Protector, Rompiendo el Silencio, testimonios de 60 oficiales de Israel acusan de masacre en Gaza, Yuli Novak
Slug: bombardeos-de-israel-en-gaza-fueron-intencionales-contra-la-poblacion-civil
Status: published

###### Foto:Abujna.com 

La **ONG israelí Rompiendo el Silencio** ha recogido el **testimonio de 60 oficiales, suboficiales y soldados** que acusan al estado de permitir y ordenar al ejercito acciones indiscriminadas contra la población que tuvieron como consecuencia la **masacre de civiles Palestinos**, así como de la elevada y desproporcionada destrucción de viviendas y propiedades en Gaza durante la invasión de agosto de 2014.

Rompiendo el silencio es una organización no gubernamental compuesta por **ex-combatientes y oficiales**, suboficiales y soldados en servicio  o en la reserva pertenecientes a rangos medios del ejército **contrarios a la ocupación de Cisjordania y de Gaza**.

Según el portavoz de la ONG **Yuli Novak** “…Mediante estos testimonios nos enteramos de que existe un amplio fracaso ético en las normas de combate del ejército**, **y de que este **fracaso se origina en lo más alto de la cadena de mando** y no es simplemente el resultado de ‘algunas manzanas podridas..’”.

En el informe emitido por la organización de derechos humanos apunta hacía que los objetivos de los **bombardeos no tenían ningún tipo de precisión** y apuntaban directamente contra la población civil, incluso a veces respondían a apuestas de los propios soldados con respecto a la elección de objetivos. Para Rompiendo el silencio es muy grave el **contraste entre las declaraciones de los oficiales cuando la invasión y lo que realmente sucedió.**

La operación **Margen Protector**, que consistió en la invasión de Gaza en agosto de 2014, tuvo un saldo de **2.100 civiles muertos y 11.000 heridos**, la gran mayoría niños y mujeres.

El rechazo de la **Comunidad Internacional** fue unánime y propició una investigación por crímenes de guerra por parte del Alto Comisionado de Naciones Unidas para Palestina, que **calificó la invasión de masacre contra la población civil** y que Israel impidió investigar a los propios técnicos de la ONU.

El informe concluye que los **objetivos respondían a intereses políticos y diplomáticos**, además de que en todo momento el estado era consciente de la masacre que se estaba perpetrando.

Actualmente la **Corte Penal Internacional, en manos de su fiscal Fatou Bensouda** está investigando la posibilidad de enjuiciar a Israel por crímenes de guerra durante la invasión, en la que fueron atacadas sistemáticamente escuelas de la ONU. Además la fiscal añadió que si se abre una investigación será contra los más altos responsables en la cadena de mando del ejército.
