Title: Falta la firma presidencial para que Ley Pepe Sánchez sea una realidad
Date: 2017-05-24 17:53
Category: DDHH, Otra Mirada
Tags: Audiovisual, colombia, derechos de autor, Ley Pépe Sánchez
Slug: ley-pepe-sanchez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/DAi0aykWAAITQop.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Clara Rojas 

###### 24 May 2017 

Los derechos de directores y guionistas de cine y televisión han dado un paso histórico con la **aprobación en cuarto debate y por mayoría en plenaria del Senado de la República de la ley "Pepe Sánchez"**, que busca garantizar que puedan  recibir regalías por la reproducción de sus obras.

La determinación que deberá pasar a sanción presidencial para ser implementada, añade un parágrafo al artículo 98 de la Ley 23 de 1982, por medio del cual se reconoce una remuneración para los autores audiovisuales por una comunicación pública, c**ada vez que sus obras sean reproducidas a nivel nacional e internacional**.

De acuerdo con la senadora Clara Rojas, ponente de la iniciativa, por cuenta del represamiento de los dineros en reconocimiento por de esos derechos, l**os realizadores han dejado de percibir anualmente cerca de 15 millones de euros**, suma que ingresaría al país si se legisla en la materia. Le puede interesar: [Un primer vistazo a "La Defensa del Dragón"](https://archivo.contagioradio.com/cinecannescolombia/).

La lucha por el reconocimiento de estos derechos ha motivado a diferentes **organizaciones que trabajan en la industria audiovisual** no sólo de directores y guionistas, también de actores desde donde han manifestado su apoyo solidario, teniendo en cuenta que el principal inspirador de la ley se desempeñó en ambos campos durante su carrera artística.

La “Ley Pepe Sánchez” está fundada en el que cuando se realiza una obra cinematográfica o audiovisual, **los guionistas, libretistas y directores ceden la totalidad de los derechos de patrimonio sobre las mismas a los productores**, perdiendo en consecuencia cualquier participación económica cuando es reproducida.

 
