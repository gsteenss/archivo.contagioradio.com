Title: Verónika Mendoza y 'la visión del nuevo Perú'
Date: 2016-04-08 17:23
Category: El mundo
Tags: corrupción, EleccionesPeru, Fujimori, impunidad
Slug: veronika-mendoza-y-la-vision-del-nuevo-peru
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/veronika-mendoza_0.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Nacional Perú] 

###### [8 Abril 2016 ]

Con este lema de campaña,[Verónika Mendoza, candidata por el partido Frente](https://archivo.contagioradio.com/miles-de-perso…keiko-fujimori-articulo-22277/)Amplio a la presidencia del Perú y representante de los sectores del movimiento social, asciende en las encuestas a dos días de las elecciones y aumenta su favoritismo al interior de la sociedad peruana.

Mendoza escaló en las últimas dos semanas con una intención de voto del 2% a ubicarse en el segundo lugar de favoritismo en la última encuesta realizada en Perú. Este ascenso, según la encuesta de la firma Ipsos, se debe al cambió que busca la sociedad peruana en donde el nombre de Verónika Mendoza se ha relacionado con la posibilidad real de una Perú justa y con memoria, mientras que los demás candidatos han sido relacionados con las formas tradicionales de hacer política ligada a la corrupción.

Las propuestas políticas que conforman la campaña de la candidata se dividen en tres puntos: el primero se remite a la impunidad que reina en el Perú con los crímenes cometidos durante la dictadura de Fujimori, Mendoza se comprometió a comenzar la búsqueda de los más de 15 mil desaparecidos e iniciar los procesos judiciales por las esterilizaciones forzadas a la población.

El segundo punto propone fortalecer la industria desde las microempresas, la renegociación de los contratos con las empresas mineras y gasíferas y generar una política energética soberana y sostenible a partir de la producción de PetroPerú.

En el último punto Mendoza propone defender los derechos sociales de los peruanos a partir de la revisión de la ley de pensiones, las reformas a las leyes de educación superior.

Este próximo 10 de abril, Mendoza enfrentará en las urnas a Keiko Fujimori y Pedro Pablo Kuczynski, con proyecto político *de cambio para el nuevo Perú*, como lo indica su slogan de campaña.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
