Title: ¿Por qué pidió un descanso no remunerado Carlos Prasca, rector de la U. del Atlántico?
Date: 2019-02-04 21:40
Author: AdminContagio
Category: DDHH, Nacional
Tags: acoso, Carlos Prasca, Universidad del Atlántico, Universidades
Slug: carlos-prasca-rector
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/CarlosPrasca-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [4 Feb 2019] 

Tras la divulgación en medios de comunicación de audios en los que se escuchaba a **Carlos Prasca, rector de la Universidad del Atlántico usar lenguaje obsceno con una estudiante de la Institución**, integrantes de la comunidad universitaria han denunciado que ha decidido pedir una licencia no remunerada; y advierten que podría haber más casos de acoso por parte del directivo.

Según denuncia la integrante de la Unión Universitaria Alternativa y estudiante, Andrea Ariza, lo que se ha hecho visible recientemente es un patrón de acoso por parte del rector Prasca en el que persigue a estudiantes por Facebook y redes sociales, para ofrecerles "ayudas" a cambio de favores sexuales. La estudiante afirma que cuando se conocieron los hechos, Prasca dijo estar siendo víctima de un montaje, pero su voz fue reconocida y **decidió entonces pedir una licencia no remunerada por tres meses.**

Con esta acción, Ariza cree que Prasca intenta burlar las denuncias y **esperar que baje la presión para poder regresar al cargo**; sin embargo, la estudiante manifestó que hasta el momento se conocen 2 casos de hostigamiento por parte del Rector contra compañeras de la Universidad, y aseguró que podrían haber más mujeres vulneradas, por lo que advierte que las denuncias continuarán.

La situación en la Universidad del Atlántico en torno a Prasca llegó a ser tan delicada que, de acuerdo a la activista, se ha identificado un patrón de acción en el que uno de los asesores del rector llevaba a las estudiantes a su oficina para permitir el acoso. Por funcionarios como este, las estudiantes no se sienten seguras denunciando los hechos de los que son víctimas en la Universidad, y han tenido que acudir directamente ante instancias judiciales para castigar estos actos.

La denuncia además escaló a otras instancia, pues el Procurador General de la Nación anunció que iniciaría una investigación formal contra el rector de la Universidad; situación que fue bien recibida por los estudiantes de la institución que han denunciado situaciones irregulares al interior del Campus universitario, adicional a las denuncias de acoso. (Le puede interesar:["Estudiantes de U. del Atlántico exigen garantías para el retorno a clases"](https://archivo.contagioradio.com/u-del-atlantico-retorno-clases/))

<iframe id="audio_32126178" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32126178_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
