Title: Democracia de dinastías
Date: 2015-05-13 16:35
Author: CtgAdm
Category: Opinion, superandianda
Tags: Gaviria, Santos, Vargas
Slug: democracia-de-dinastias
Status: published

[Superandianda](https://archivo.contagioradio.com/superandianda/) - [~~@~~Superandianda](https://twitter.com/Superandianda){.twitter-atreply .pretty-link}

Llamamos dinastías  a aquellas familias que por  generaciones heredan monarquías, poderes políticos o mantienen viva su actividad económica a través de su parentesco. Pero cuando hablamos de democracia, las llamadas dinastías familiares son las causantes de un gran daño para el sistema electoral de un estado, pues se convierten en el puente donde se pasa el poder de  una cabeza a la otra por apellidos y no por propuestas; utilizan el estado participativo por voto popular para  legitimar la misma línea política dictatorial que maquillan dentro de una práctica que descaradamente llaman "pluralista"

“Seguir gobernando en cuerpo ajeno” en eso consiste la democracia colombiana, lo que llamamos voto popular no es[ ]{.Apple-converted-space}más que volver legitimo a un sistema corrupto; el aparato que llaman participativo logra[ ]{.Apple-converted-space}que la misma directriz sea dueña de sus simpatizantes, sus opositores y hasta sus críticos -podriamos hablar de los falsos criticos que organizan a la falsa oposición- manteniendo el control[ ]{.Apple-converted-space}no solo político y económico sino también mental de la opinión pública.

De repente en elecciones siempre vemos a los hijos, las esposas, las amantes, los tíos y un cúmulo de los mismos candidatos y los mismos partidos políticos, a veces dando saltos trapecistas de un partido “diferente” a otro, pero son los mismos círculos familiares que han sometido a nuestro país, los mismos apellidos que se han repartido el poder con sus tradicionales campañas populares, los mismos amigos que comprometen nuestros puestos públicos, los[ ]{.Apple-converted-space}puestos que nos pertenecen, con el descaro de proponer algo diferente -¿algo diferente?- Lo peor y lo vergonzoso es que los colombianos defienden el sistema que no propone nada diferente a la corrupción que[ ]{.Apple-converted-space}nos ha gobernado durante décadas , las mismas décadas donde los delfines y los lagartos han desfilado por las páginas del jet set administrando el país.

El gran temor por el acuerdo de paz en la mesa de la Habana ha indignado a los árboles genealógicos nacionales; pero la democracia no debe ser un árbol genealógico, ni las necesidades del país deben ser un negocio de los grandes grupos económicos, ni en Colombia existe la monarquía –aunque los más arribistas así lo sientan- para que su sistema corrupto sea heredable. No solo existe la extrema derecha y centro como pretenden hacerlo ver los noticieros; opinar diferente no nos hace ser criminales ni terroristas.

El proceso de paz consolida nuevas fuerzas políticas que el[ ]{.Apple-converted-space}método dictatorial no está dispuesto a aflojar.[ ]{.Apple-converted-space}El hecho de contemplar que aquellos que usan armas están dispuestos a cambiarlas por votos para buscar sus propias curules -como cualquier colombiano tiene el derecho a hacerlo- es una amenaza para las dinastías políticas nacionales y grandes grupos económicos ; pero la cuestión no es que el talento político sea heredable, la cuestión es que tenemos derecho a una verdadera participación política distinta a las campañas familiares y mafias económicas.

Ya es hora que abran paso.
