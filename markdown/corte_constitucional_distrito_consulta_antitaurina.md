Title: Corte Constitucional ordena al distrito tramitar la consulta antitaurina
Date: 2017-05-08 17:15
Category: Animales, Voces de la Tierra
Tags: corridas de toros, Corte Constitucional, Maltrato animal
Slug: corte_constitucional_distrito_consulta_antitaurina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/MG_97892.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [8 May 2017] 

Tras casi dos años de pensarse que la consulta popular antitaurina estaba muerta, la Corte Constitucional le da un giro a la historia y deja nulo el concepto del Consejo de Estado mediante el cual se había suspendido la posibilidad de que la ciudadanía bogotana demostrara si hay o no arraigo cultural respecto a ese tipo de actividades.

Lo cierto es que hoy la consulta antitaurina está más viva que nunca. En línea con su reciente fallo sobre la necesidad de que el Congreso debe legislar sobre las corridas de toros, sino estas serán abolidas, esta vez, la Corte le ordena al alcalde Enrique Peñalosa que en un **lapso máximo de 48 horas deberá  “proceder a adelantar los trámites pertinentes para llevar a cabo la consulta popular** autorizada por el cabildo de la ciudad”. [(Le puede interesar: Decisión del Consejo de Estado es una estocada a la democracia)](https://archivo.contagioradio.com/decision-del-consejo-de-estado-fue-una-estocada-a-la-democracia/)

De esa manera, la ponencia del magistrado Luis Ernesto Vargas, logró que se dejara en firme la sentencia del Tribunal Administrativo de Cundinamarca que había dado el concepto de constitucionalidad de la pregunta ** "¿Está usted de acuerdo, Sí o No, con que se realicen corridas de toros y novilladas en Bogotá Distrito Capital?**". [(Le puede interesar: Centro Democrático busca volver patrimonio cultural las corridas de toros)](https://archivo.contagioradio.com/centro_democratico_corridas_toros/)

Pero el fallo del alto tribunal va más allá. Además de obligar a la alcaldía a gestionar la realización de la consulta, si llegara a ganar la aprobación de los espectáculos taurinos, la Corte advierte a **Administración Distrital que de todas formas, deberá implementar las medidas necesarias para desincentivar esa práctica. **

###### Reciba toda la información de Contagio Radio en [[su correo]
