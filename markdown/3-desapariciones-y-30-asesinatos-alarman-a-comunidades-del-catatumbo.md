Title: 3 desapariciones y 30 asesinatos alarman a comunidades del Catatumbo
Date: 2016-05-20 16:11
Category: DDHH, Nacional
Tags: Asociación Campesina del Catatumbo, Catatumbo, paramilitarismo colombia
Slug: 3-desapariciones-y-30-asesinatos-alarman-a-comunidades-del-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/15325287782_6b910b4473_b.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa Rural] 

###### [20 Mayo 2016 ] 

[Desde el pasado 14 de mayo los jóvenes campesinos Jaider Navarro Chinchilla de 21 años y Cristian Romero Rincón de 20 años, desaparecieron en el municipio de El Tarra, Norte de Santander. Pese a que las comunidades alertaron a las autoridades, **hasta el momento la Fiscalía no ha iniciado el proceso de búsqueda** y han sido los mismos pobladores los que la han emprendido. ]

Los jóvenes fueron vistos por última vez de camino a su lugar de trabajo, zona en la que se identificaron utensilios militares, indicios que podrían señalar al Ejército Nacional como uno de los principales sospechosos, según afirma Olga Quintero vocera de ASCAMCAT. De acuerdo con Quintero estas **2 desapariciones se suman a los 30 asesinatos que se han presentado en la región del Catatumbo** en lo que va corrido del año.

Quintero agrega que en los últimos meses han circulado panfletos amenazantes emitidos por grupos paramilitares, como el Clan Usuga, y **se han encontrado cadáveres sin identificar**, como el de un campesino que fue encontrado el pasado 18 de mayo, "hechos que sólo se veían cuando el paramilitarismo estaba vivo en la región". [Pese a que el gobierno ha reconocido la presencia de bandas criminales, el Ejército y la Policía, actúan indiferentes. ]

<iframe src="http://co.ivoox.com/es/player_ej_11605716_2_1.html?data=kpajkpqbdZehhpywj5WdaZS1kpuah5yncZOhhpywj5WRaZi3jpWah5yncbDgyMaYs9rNstXZ09TgjZKPhbS3orKwo7mRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
