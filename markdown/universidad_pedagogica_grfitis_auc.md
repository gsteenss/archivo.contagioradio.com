Title: Universidad Pedagógica aparece con graffitis de las AUC
Date: 2017-11-19 18:07
Category: Educación, Nacional
Tags: AUC, Paramilitarismo, Universidad Pedagógica Nacional
Slug: universidad_pedagogica_grfitis_auc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/upn1-e1511132583683.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ACEU UPN] 

###### [19 Nov 2017] 

En la mañana de este sábado, la sede de la Universidad Pedagógica, ubicada en la calle 72 amaneció con graffitis en las paredes que hacen alución al **grupo paramilitar de las Autodefensas Unidas de Colombias, AUC.** (Le puede interesar: [Estudiantes de la Universidad Pedagógica llaman a paro)](https://archivo.contagioradio.com/estudiantes-de-la-universidad-pedagogica-llaman-a-un-nuevo-paro-nacional-estudiantil/)

De acuerdo con las denuncias en redes sociales, los estudiantes aseguran que "se trata de un acto intimidatorio  las luchas que libra la comunidad universitaria", no obstante, afirman que rechazan y denuncian **"todo acto de sabotaje, amenaza e intimidación  la UPN"**, y que no se dejarán amedrentar por el contrario intensificarán la lucha por la plena financiación por la plena financiación de las universidades públicas y educación gratuita y de calidad para todo el país.

Amalfi Bocanegra, representante de los estudiantes ante el Consejo Superior Universitario, señala que la universidad ha sido amenazada desde su origen. No obstante, también denuncia que actualmente, **los problemas de microtráfico que hay al interior de la universidad** han generado otra serie de problemáticas, aunque asegura que se trata de un tema con el que se buscaría hacerle daño a la comunidad universitaria.

Cabe recordar que no es la primera vez que los estudiantes de la universidad son víctimas de este tipo de amedrentamientos. En anteriores ocasiones, en medio de protestas estudiantiles, han sido amenazados por **grupos paramilitares identificados como las Águilas Negras, y también se han visto intimidados profesores de la universidad.** Aseguran que se trata de una de las formas para acallar al pensamiento crítico en el país. (Le puede interesar: [Hallan muerto a estudiante desaparecido de la U. Pedagógica)](https://archivo.contagioradio.com/hallan-muerto-estudiante-desaparecido-de-la-universidad-pedagogica/https://archivo.contagioradio.com/hallan-muerto-estudiante-desaparecido-de-la-universidad-pedagogica/)

<iframe id="audio_22177464" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22177464_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
