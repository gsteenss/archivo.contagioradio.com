Title: En la impunidad feminicidios de colombianas en México
Date: 2016-08-04 13:44
Category: El mundo, Mujer
Tags: Feminicidios en México
Slug: en-la-impunidad-feminicidios-de-colombianas-en-mexico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/feminicidio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ndmx] 

###### [4 de Agos] 

Cerca de **3 mujeres de nacionalidad colombiana han sido victimas de feminicidios** en lo corrido del año 2016 en México. La última de ellas fue la modelo **Sthepanie Magón Ramírez**, quien fue encontrada desnuda y con signos de tortura en una calle de Colonia Nápoles, en el centro de Ciudad de México. Hasta el momento no se tiene conocimiento o indicio frente al crimen y **se teme que el caso quede en la impunidad.**

Sin embargo, estos no son los únicos casos sobre violencia de género y feminicidios de mujeres colombianas en ese país, **desde el año 2012 se han reportado 5 casos de extrema violencia, con signos de tortura que han quedado en la impunidad,** debido a que, según las autoridades, ninguno de los procesos de investigación ha arrojado resultados frente a los culpables de los crímenes.

Carolina Bedoya, miembro de la organización Me Muevo por Colombia, asegura que **estos asesinatos no han tenido un debido proceso de investigación** porque las instituciones a cargo **han justificado los hechos tras la estigmatización de ser colombianas** y dar por sentado que se encontraban involucradas con el narcotráfico o la prostitución, "lo que las mujeres colombianas en México estamos exigiendo al Estado es que se de una investigación seria de estos asesinatos".

De acuerdo con Carolina, en principio las autoridades mexicanas indicaron que Magón **podría haber muerto por múltiples golpes**, luego el Tribunal Superior de Ciudad de México emitió un comunicado en el que señalaba que la muerte fue provocada por una caída desde un edificio, debido a que la víctima no mostraba señales de defensa o de forcejeo en su cuerpo.

Durante esta semana se van a llevar a cabo diferentes actividades** en rechazó al accionar de las instituciones frente a los casos de feminicidios** y se espera que desde Colombia se produzca una presión más fuerte para obtener resultados concretos de los culpables de estos asesinatos.

<iframe src="http://co.ivoox.com/es/player_ej_12442053_2_1.html?data=kpehlpeUeZShhpywj5aYaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5yncaTV09TZy9PFb6PZxdTmw4qWh4zBxpC618ras4zk0NeYpdTQs87Wysaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
