Title: Campesinos denuncian daños ambientales por derrame de ACPM en Simacota, Santander
Date: 2017-04-24 14:50
Category: Ambiente, Nacional
Tags: extracción de petróleo, Parex, Simacota
Slug: campesinos-denuncian-danos-ambientales-por-derrame-de-acpm-en-simacota-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/simacota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mapio.net] 

###### [24 Abr 2017] 

Tras 10 días de que la empresa Parex Resources se comprometiera a reparar los daños ambientales causados por el volcamiento de uno de sus camiones, con 5.800 galones de ACPM, en un caño, campesinos del municipio de Simacota, Santander denuncian que las limpiezas no se han hecho bien y que el **ACPM estaría llegando a los ríos, provocando la muerte de peces, contaminando el agua y dañando la fauna y flora del territorio**.

De acuerdo con Reinaldo Duarte, integrante del Colectivo para la defensa de las fuentes Hídricas y el medio ambiente COLDIMAFH, “no se le ha dado un manejo adecuado al derrame, debido a que la tierra afectada es muy suave y absorbe como una almohadilla, **provocando que la tierra se impregnará mucho más rápido y que ahora escurra ACPM**”.

<iframe id="audio_18313664" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18313664_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

A su vez, los campesinos están denunciando que **hasta el momento no hay ningún pronunciamiento por parte de la Alcaldía del municipio o de la CAR**. Duarte afirmó que los habitantes del municipio ya no saben a dónde dirigir sus quejas porque finalmente no ven ninguna acción que proteja el territorio. Le puede interesar: ["Campesinos de Simacota se oponen a destrucción que ocasionaría empresa Parex"](https://archivo.contagioradio.com/comunidades-se-oponen-a-exploracion-petrolera-en-simacota-santander/)

Actualmente la preocupación de los campesinos radica en que los vertimientos del agua del caño, con ACPM, están llegando a ríos que dotan de agua a familias y animales en los alrededores y al no tener una medida de contingencia urgente, **este daño podría demorarse mucho más tiempo en ser solucionado aumentado el riesgo de que más quebradas se contaminen.**

De igual forma señalaron que les afana que las construcciones que adelantará la empresa en su territorio, podrían ocasionar que se queden sin agua “**nosotros no tenemos acueductos, recogemos agua de los aljibes y caños, con la perforación a las malas nos van a acabar el agua**”, advirtió Duarte. Le puede interesar: ["La polémica detrás del proyecto de Ecopetrol en Guamal, Meta"](https://archivo.contagioradio.com/la-polemica-detras-del-proyecto-ecopetrol-guamal-meta/)

 El daño ambiental se produjo el pasado 13 de abril, cuando un carrotanque de Parex  Resources se volcó sobre un caño que hace parte de la quebrada la Colorada, además la empresa **ha expresado su intención  de construir vías de acceso, líneas de flujo y líneas eléctricas para realizar actividad petrolera en el territorio.**

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Contaminacion.mp4\[/KGVID\]
