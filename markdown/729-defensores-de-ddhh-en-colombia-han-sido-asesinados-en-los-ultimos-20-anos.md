Title: 729 defensores de DDHH en Colombia han sido asesinados en los últimos 20 años
Date: 2015-11-20 16:21
Category: DDHH, Nacional
Tags: buenaventura, Casanare, cordoba, Daniel Abril, Defensa de derechos humanos en Colombia, John Jairo Ramírez Olaya, Luis Francisco Hernández, Oficina para el Alto Comisionado, ONU
Slug: 729-defensores-de-ddhh-en-colombia-han-sido-asesinados-en-los-ultimos-20-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/ONU.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.proyecto40.com]

###### [20 nov 2015]

**En lo que va corrido del año 2015, se ha superado el promedio de asesinatos a defensores de Derechos Humanos que desde hace 20 años se venía registrando**. Así lo confirma un comunicado emitido por la Oficina del Alto Comisionado para los Derechos Humanos en Colombia.

En un trabajo conjunto realizado entre la Oficina y la Fiscalía General de la Nación, se tomado registro de 729 homicidios de defensores de Derechos Humanos ocurridos entre los años de 1994 hasta el 2015, **“lo que indica un promedio anual de 33 asesinatos (...) Casi todos estos hechos permanecen en la impunidad”**, dice el comunicado.

La Oficina para el Alto Comisionado, recomienda que las entidades estatales encargadas de la protección de los activistas tomen medidas para mitigar estos hechos de violencia en contra de los defensores; además, insta a la Fuerza Pública a aumentar sus esfuerzos en la desactivación de las organizaciones criminales.

Entre el **9 y el 13 de noviembre del 2015, fueron asesinados 3 líderes de Derechos Humanos:** el activista juvenil, John Jairo Ramírez Olaya en Buenaventura; el defensor campesino Daniel Abril en el municipio de Trinidad, Casanare y el líder de restitución de tierras Luis Francisco Hernández en la población de Tierralta, Córdoba.
