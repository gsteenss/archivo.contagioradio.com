Title: Estudiantes de la Universidad Distrital tienen razones de sobra para protestar
Date: 2019-09-26 18:21
Author: CtgAdm
Category: Educación, Movilización
Tags: corrupción, ESMAD, Presupuesto, Universidad Distrital
Slug: estudiantes-de-la-universidad-distrital-tienen-razones-de-sobra-para-protestar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Universidad-Distrital-en-Protesta.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-25-at-11.29.06-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [@jerojasrodrigue]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}  
] 

Estudiantes de la Universidad Distrital iniciaron una jornada de protestas desde el principio de esta semana para manifestarse contra los actos de corrupción que se han denunciado en la institución, y pidiendo la renuncia de las directivas. Las manifestaciones se produjeron luego que la Procuraduría iniciará una investigación contra William Muñoz Prieto, exdirector del Instituto de Extensión de la Universidad, por el presunto uso de más de 10 mil millones de pesos de recursos públicos en beneficio personal y de terceros.

(Le puede interesar: ["Corrupción en la U. Distrital"](https://archivo.contagioradio.com/corrupcion-universidad-distrital/))

### **Las denuncias de corrupción en la Universidad Distrital (y sus efectos)** 

Según explicó Aura Fernández, estudiante de la Universidad, luego del primer hecho de corrupción señalado por la Procuraduría, las investigaciones han avanzado hasta señalar la presunta participación de otros administrativos de la institución en estos hechos, por ejemplo, miembros del Consejo Superior Universitario (CSU). Uno de los presuntos involucrados sería el representante de egresados ante el CSU, así como administrativos que se veían beneficiados por los dineros apropiados por Muñoz.

Integrantes de la Universidad Distrital han alertado porque el descubrimiento de la Procuraduría sería solo la 'punta del iceberg' de una serie de hechos de corrupción que captan los recursos del centro educativo. Algunos de ellos tienen que ver con los llamados "contratos sastre", es decir, contratos que son hechos a la medida de los contratistas entre los que mencionan las compras de sillas por valores que exceden su precio en el mercado, o la licitación para brindar el servicio de alimentación a los estudiantes.

Situaciones que ocurren mientras los estudiantes denuncias que se redujo el presupuesto de viáticos para salidas de campo, lo que implica que muchos de ellos no cuenten con los recursos económicos necesarios para asistir a estos espacios de formación. Adicionalmente, el movimiento estudiantil que se activó en 2018 denunció que la Distrital pasaba por un momento presupuestal difícil, enfrentando un déficit de 68 mil millones de pesos para este año. (Le puede interesar: ["Déficit de U. Distrital superaría los 60 mil millones en 2019"](https://archivo.contagioradio.com/deficit-universidad-distrital-2019/))

A ello se suma el déficit en infraestructura, que según denuncias del concejal Manuel Sarmiento, alcanzaría los 116 mil metros cuadrados "es decir, el equivalente a 16 estadios como El Campín". Esta deuda es una queja constante de estudiantes en diferentes sedes de la Universidad, que dicen estar hacinados en sedes como la Calle 40 (de ingenierías), al tiempo que siguen abriendo nuevos cupos para recibir estudiantes en las carreras y obtener mayores recursos.

### **Estudiantes exigen la renuncia de los directivos, ¿por qué?** 

Ante los hechos de corrupción denunciados, Fernández afirmó que la administración de la Universidad "todavía se hace la de los ojos ciegos, no le ha dado ninguna respuesta a los estudiantes" sobre las acciones que tomará para prevenir este tipo de hechos; por esta razón, argumentó que en la Universidad iniciaron protestas pidiendo la renuncia del rector y las directivas de la Institución. Estas manifestaciones, dijo la estudiante, probablemente continuarán en diferentes sedes pese a la represión sufrida esta semana a cargo del ESMAD.

\[caption id="attachment\_74237" align="aligncenter" width="410"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-25-at-11.29.06-PM-410x1024.jpeg){.size-large .wp-image-74237 width="410" height="1024"} Foto: Redes\[/caption\]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
