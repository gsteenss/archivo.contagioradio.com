Title: ELN niega responsabilidad en hechos de Convención en Norte de Santander
Date: 2015-05-11 14:56
Author: CtgAdm
Category: Nacional, Paz
Tags: Cabo Edward Avila, ELN, Gabino, Juan Carlos Pinzón, Ministro de defensa, Nicolás Rodrígeuz Bautista
Slug: eln-niega-responsabilidad-en-hechos-de-convencion-en-norte-de-santander
Status: published

###### Foto: cablenoticias.com 

###### [11 May 2015] 

La semana anterior el país se conmocionó tras conocer la noticia de la amputación y exhibición de una pierna del **Cabo Edward Avila** en una en una escuela en el municipio de convención en Norte de Santander. En su momento el **Ministro de Defensa Juan Carlos Pinzón** acusó al ELN de ser el autor de este lamentable hecho.

A través de un comunicado publicado este 11 de Mayo a través de su página web, la guerrilla del ELN negó "enfáticamente" su autoría en este hecho y afirmaron que “*Es política del ELN hacer pública la responsabilidad de cada una de sus acciones*” y señalan que con este tipo de acciones lo que se pretende es deslegitimar a la guerrilla y  “cerrarle la puerta al diálogo”.

Esa guerrilla también hace una crítica a los medios masivos de información y afirma que se valen de la “doble moral” para hechos como este puesto que mientras hacen eco de este tipo de noticias guardan silencio frente a violaciones de DDHH cometidas por la **Fuerza Pública.**

También afirman que el país merece la verdad y que este tipo de acusaciones no favorecen en la búsqueda de la paz y que el país necesita la verdad para darle “solución a las causas que dieron orígen y mantienen el conflicto para poder así parar la guerra”.

A continuación anexamos el comunicado completo.

*"Las mentiras no son caminos de paz"*
--------------------------------------

*No es de sorprender el manejo mediático que se ha hecho de la falsa acusación contra el ELN, en la que se afirma que un comando nuestro, exhibió las piernas de un suboficial del Ejército en una escuela de Convención, Norte de Santander.*

*Contrario al accionar estatal, que muestra como trofeos los cuerpos de guerrilleros abatidos en combate, el ELN jamás se valdrá de estos actos degradantes como símbolo de victoria, porque nuestra ética revolucionaria tiene como fundamento el respeto a la dignidad humana.*

*Es política del ELN hacer pública la responsabilidad de cada una de sus acciones, en este caso DESMENTIMOS ENFÁTICAMENTE que este tipo de hechos sean obra nuestra. Con estas acusaciones hay una intención por parte de los enemigos de la paz, de deslegitimar a la insurgencia y cerrarle la puerta al diálogo como parte de un proceso de paz.*

*Nuevamente los medios de comunicación se valen de la doble moral para abordar este tipo de hechos. Pues ante los cientos de casos de violaciones de derechos humanos, por parte de la fuerza pública y su auspicio a las masacres perpetradas por paramilitares se guarda silencio. Por el contrario se ha explotado mediáticamente esta falsa acusación, para distraer la atención del pueblo colombiano imponiendo la idea de que la lucha insurgente ha perdido su esencia.*

*Como en distintas ocasiones lo hemos sostenido, el gobierno es quién ha decidido negociar en medio de la guerra. Como ELN siempre hemos planteado la urgencia y necesidad de un cese al fuego bilateral, en muestra sincera de la voluntad de paz, puesto que los más de 50 años de conflicto armado en el país, han dejado un saldo doloroso y sin duda, son los pobres quienes siguen pagando la crudeza de una guerra que solo beneficia a los guerreristas y ricos del país.*

*Ni las falsas acusaciones, ni las amenazas, ni las presiones, son caminos de paz. El pueblo colombiano merece verdad y un sincero interés por parte de las clases dirigentes, de buscar solución a las causas que dieron origen y mantienen el conflicto para poder así parar la guerra."*
