Title: “Dilación puede poner en riesgo el proceso de Paz": Iván Cepeda
Date: 2016-10-14 14:12
Category: Entrevistas, Paz
Tags: acuerdos de paz ya, Cese al fuego bilateral, Plebiscito
Slug: dilacion-puede-poner-en-riesgo-el-proceso-de-paz-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/epeda-e1476470290248.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Orientación y Lucha] 

###### [14 Oct de 2016] 

Después del anuncio del presidente Santos sobre la nueva fecha de terminación del cese bilateral al fuego, que será hasta el 31 de Diciembre, el senador Iván Cepeda manifestó que “no hay más lugar a dilaciones y supuestas negociaciones, pues las negociaciones son en La Habana, entre el Gobierno Nacional y las FARC”.

Desde que se inició la fase pública de los diálogos, distintas organizaciones sociales, de estudiantes, trabajadores, educadores, víctimas, ambientalistas, defensoras de Derechos Humanos, entre otras, **realizaron durante un tiempo prudente, foros, cátedras y encuentros, justamente para debatir, construir y enviar a La Habana propuestas desde distintos sectores sociales.**

Cepeda, añade que los años que lleva la negociación entre Gobierno y Farc, han permitido un avance importante, **“pero un proceso de paz siempre es frágil y cualquier dilación que pueda poner en riesgo la estabilidad del proceso hay que superarla y evitarla”. **Le puede interesar: [Cese bilateral: plazo para que no retorne la guerra](https://archivo.contagioradio.com/cese-bilateral-plazo-para-que-no-retorne-la-guerra/).

El senador, comenta que con la decisión del 2 de octubre “se produjo un problema en relación al tramite parlamentario”, puesto que inicialmente lo pactado era un tramite mas ágil y efectivo, que el de un proyecto de ley o una reforma constitucional. Señala que existen otros mecanismos, como un **“mensaje de urgencia al Congreso para que gestione los desarrollos normativos que requiere el Acuerdo una vez que finalice la etapa de ajustes”**, aunque las delegaciones han expresado que hay temas que son intocables.

Si bien han surgido impases para la implementación de los Acuerdos, en lo que va de este mes han surgido expresiones desde el movimiento social, que han venido ganando espacios y, en palabras de Cepeda “**que lo pactado no sea soslayado ni por los enemigos del acuerdo”**, resalta que “tienen toda la razón las víctimas, estudiantes, afro descendientes e indígenas, en reclamar que el acuerdo sea ya”.

Por otra parte, afirma que el anuncio del inicio de la fase pública de negociaciones con el ELN, abre la puerta a resolver la incertidumbre en la que se encuentra el país y en particular, **“el estado de interinidad en el que se encuentra la guerrilla, están en una situación ambigua entre la vida guerrillera y el transito a la vida civil**, es necesario que ese limbo se resuelva antes de que finalice este año”. Le puede interesar: [Diálogos con el eln fortalecen esfuerzos hechos en la habana con las FARC](https://archivo.contagioradio.com/dialogos-con-el-eln-fortalecen-esfuerzos-hechos-en-la-habana-con-las-farc/)

Concluye que, aunque hay una expresión radical encabezada por Álvaro Uribe, que quiere imposibilitar el proceso de Paz, estos sectores **“han tenido que reconocer que la discusión se da sobre lo acordado”**, y es el momento propicio para “sumar todas las iniciativas, la movilización es fundamental”.

<iframe id="audio_13324628" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13324628_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
