Title: Así será el funeral de Fidel Castro
Date: 2016-11-26 11:29
Category: El mundo, Movilización
Tags: Cuba, Fidel Castro
Slug: asi-sera-el-funeral-de-fidel-castro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Fidel-en-acto-cdr-foto-roberto-chile-16.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Roberto Chile] 

###### [26 Nov 2016] 

En un comunicado de prensa la Comisión Organizadora del Comité Central del Partido, el Estado y el Gobierno, dieron a conocer como se llevarán a cabo las honras fúnebres del Comandante en Jefe Fidel Castro. **A partir del día 28 hasta el 29 de noviembre  el cuerpo de Castro estará en el Memoria “José Martí”, lugar a donde la población podrá acercarse para rendirle homenaje.**

De igual forma, durante estos dos días, la Comisión informará en que lugares de cada localidad de Cuba, se podrá rendir homenaje y firmar el solemne juramento de cumplir el concepto de Revolución, expresado por Fidel Castro el primero de mayo del año 2000, como intención de dar continuidad a las ideas socialistas. Le puede interesar: ["Aportes de Fidel Castro a la paz de Colombia"](https://archivo.contagioradio.com/aportes-de-fidel-castro-a-la-paz-de-colombia/)

El día 29 de noviembre, a las 19:00 horas, se realizará un acto de masas en la Plaza de la Revolución “José Martí” de la Capital y al día siguiente se iniciará el traslado de sus cenizas por el itinerario que rememora La Caravana de la Libertad en enero de 1959, hasta la provincia de Santiago de Cuba, concluyendo el día 3 de diciembre. Además ese mismo día se realizará un acto de masas en la Plaza “Antonio Maceo”.

**La ceremonia de inhumación se efectuará a las 07:00 horas del día 4 de diciembre en el cementerio de “Santa Ifigenia**”. Se espera que a estos eventos asistan primeros mandatarios de diferentes países, líderes internacionales y personajes de la política.

######  Reciba toda la información de Contagio Radio en [[su correo]
