Title: Mujeres realizan el primer tribunal a la justicia patriarcal en Colombia
Date: 2018-12-12 14:04
Author: AdminContagio
Category: Entrevistas, Mujer
Tags: justicia, mujeres, Violencia de género
Slug: justicia-patriarcal-mujeres
Status: published

###### Foto: Contagio Radio 

###### 11 Dic 2018 

El crecimiento en los indices de impunidad en casos de violencia contra las mujeres y su relación con el modelo de justicia colombiano, motivaron la realización del **primer Tribunal a la justicia patriarcal**. Un espacio articulado por varias organizaciones de mujeres desde los territorios y la capital, unidas por la búsqueda de una justicia feminista y la construcción de rutas de acción conjunta frente al tema como una medida urgente.

El encuentro que tuvo lugar el pasado 10 de diciembre, contó con la participación de las congresistas **Aida Abella, María José Pizarro y Alberto Castilla**, y la intervención de mujeres provenientes de **Arauca, Bucaramanga, Medellín y Barranquilla**, quienes realizaron distintas denuncias instituciones del estado que con dilaciones, omisiones o negligencia no garantizan su vida e integridad.

**Angélica Beltrán, coordinadora nacional de la Confluencia de mujeres para la acción pública** y una de las gestoras del espacio, recordó que **el 96% de casos de violencias contra ellas quedan en la impunidad**, una realidad que tampoco es completa porque muchos no se denuncian o como ocurre en jurisdicciones indígenas, no son recogidas por el modelo de justicia colombiana.

"Nosotras caracterizamos esta justicia con tres apellidos: la primera es que **es una justicia patriarcal,** la segunda que **es una justicia capitalista** y que **es una justicia colonial**" denominaciones que Beltrán explica se encuentran en casos muy concretos: es patriarcal porque **en muchas ocasiones está más al servicio de los hombres que de la misma justicia en realidad**; es capitalista porque es elitista cuando hay hombres o mujeres de clase enriquecida que violentan los derechos de la clase popular da fallos a favor de esas personas; y es colonial cuando existe una agresión sobre las mujeres en sus cuerpos y territorios.

Las promotoras de la iniciativa, sustentan su propuesta en la necesidad de construir una justicia feminista, que aclara **no es sinónimo de justicia sólo para las mujeres sino que esté basada en la igualdad social** de todos los seres incluyendo la naturaleza, así como las diversidades sexuales y de género. **Una justicia que funcione teniendo en cuenta que debe estar al servicio de una vida digna para las mujeres**.

[**Más allá de incrementar el castigo se requiere una transformación cultural**]

Para Beltrán, existe un asunto relacionado con el modelo punitivo de la justicia colombiana, que es estructural y no permite avanzar en la justicia feminista, porque **está basado en un principio de venganza** "para nosotras la respuesta no es darle más cárcel a los agresores, que es el enfoque que esta teniendo el gobierno del actual presidente Duque, **la respuesta para encontrar un vida libre de violencia contra las mujeres esta en poder generar una cultura de la no violencia**, basada en la libertad y que acepte las diversidades".

Adicionalmente, la activista asegura que la justicia debe ser pedagógica, donde **existan otro tipo de sanciones**, conversaciones y reflexiones que ayuden a cerrar los ciclos de agresión propios del modelo punitivo, agregando que **no se trata solo de focalizarse en el agresor sino también en los funcionarios y funcionarias de la rama** que, en muchas ocasiones, no tienen la formación y sensibilización suficiente frente al tema, lo que termina revictimizando a las mujeres.

"**No sólo estamos reclamándole al estado que reconsidere su modelo, estamos también en la práctica construyendo otros tipos de justicia** y eso lo podemos ver con las acciones públicas que estamos realizando los movimientos de mujeres basadas en la sanción social, y las redes de apoyo entre mujeres" puntualiza Beltrán. (Le puede interesar: [En Colombia no hay avances para detener la violencia contra las mujeres](https://archivo.contagioradio.com/en-colombia-no-hay-avances-para-detener-la-violencia-contra-las-mujeres/))

**La unidad entre mujeres, una forma de empoderamiento**

Una de las claves que identifican las impulsoras de la propuesta, radica e**n el empoderamiento colectivo, asegurando que al estar bajo una situación de violencia las mujeres pueden hacer es buscar el acompañamiento de otras mujeres**, organizaciones de mujeres y feministas, de diversidades sexuales, entre otras.

"**Cuando estamos solas nos ganan otro tipo de estructuras e ideas, la idea que fue nuestra culpa, que es normal, que nos estamos inventando cosas**, cuando llegamos a volverlo público, es decir hablar con otras dos amigas y ver que es un problema que nos esta pasando a varias, **así podemos emprender rutas de acción**" asegura Beltrán.

Rutas entre las que se encuentran la **ley 1257 de 2008 y la ley Rosa Elvira Cely**, que es importante conocer a través del estudio individual pero también de los escenarios de capacitación que ofrecen diferentes organizaciones y espacios de la institucionalidad, frente a los cuales deben visibilizarse las barreras de acceso para poder superarlas.

<iframe id="audio_30744792" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30744792_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
