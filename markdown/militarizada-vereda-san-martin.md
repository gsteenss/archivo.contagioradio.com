Title: Militarizada vereda Cuatro Bocas en San Martín luego de entrada de multinacional
Date: 2016-10-21 13:23
Category: Ambiente, Nacional
Tags: Militarización de territorios, No más Fracking, Represión del ESMAD
Slug: militarizada-vereda-san-martin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Ejército.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [21 Oct de 2016] 

El pasado jueves 20 de Octubre, fue **militarizada la vereda Cuatro Bocas, luego de la entrada de maquinaria de Conoco Phillips. **Habitantes de la zona denuncian que continúan las violaciones de Derechos Humanos, y ese mismo jueves el Alcalde decretó toque de queda en todo el municipio.

Romario Torres líder comunitario de San Martín e integrante de CORDATEC, denunció que además del toque de queda, el Alcalde “**dice que no podemos hacer reuniones, ni cacerolazos, ni manifestaciones, y eso viola nuestros derechos constitucionales”**. Esta respuesta por parte de las instituciones, va en contravía a la construcción de Paz, en palabras de Torres “el Gobierno nacional respalda la represión y la extracción sin pensar en la vida”. Le puede interesar: [En medio de gases y lágrimas ConocoPhillips entra maquinaria a San Martín.](https://archivo.contagioradio.com/en-medio-de-gases-y-lagrimas-conocophillips-logra-entrar-maquinaria-a-san-martin/)

Este líder, señaló que “hay registro de video de las agresiones a personas en la finca de cuatro bocas, **a los policías no les importó que hubiesen niños, mujeres embarazadas y ancianos (…) atacar a alguien que este sin armas es cobardía”**. Torres, reveló que no hubo acompañamiento por parte de la Defensoría del Pueblo ni de la Personería, “estuvieron sólo unos días antes del ataque, pero durante los enfrentamientos no los vimos por ahí”.

Torres asegura que las instituciones municipales se han encargado de estigmatizar la protesta pacifica, para justificar la represión del ESMAD, en la pasada marcha que se hizo en el municipio “alguien que no distinguimos quien es, rompió un vidrio de la Alcaldía (…) **creemos que fue un auto-atentado para hacer pensar que nosotros nos tornamos violentos”**. Le puede interesar: [Fuerte represión del ESMAD contra comunidad de San Martín que se opone al fracking.](https://archivo.contagioradio.com/fuerte-represion-del-esmad-contra-comunidad-de-san-martin-que-se-opone-al-fracking/)

Las comunidades han hecho un llamado al Alcalde Saúl Eduardo Celis Carvajal, para que “atienda nuestra voz de protesta, y use la potestad que le otorga el fallo de la Corte para expulsar a esta empresa de nuestro territorio”.

**La respuesta de la alcaldía ha generado inquietudes en la comunidad sobre los vínculos que pueda tener Celis con Conoco Phillips,** sin embargo, Torres enuncia que “no podemos asegurar si el recibe incentivos económicos (…) se le ha dicho que llame a Consulta Popular, que acate las normas constitucionales y ha hecho caso omiso a todos nuestros llamados”.

Por último, Romario Torres, extiende la invitación a varios municipios que al igual que San Martín se están viendo gravemente afectados por actividades extractivas, a que se movilicen. Resalta que es importante “activar alarmas a nivel nacional por la explotación, que el paro sea local y luego nacional, **que todo el mundo sepa que el gobierno quiere pasar por encima de nuestro derecho constitucional a la defensa de nuestros territorios”.**

<iframe id="audio_13421608" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13421608_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
