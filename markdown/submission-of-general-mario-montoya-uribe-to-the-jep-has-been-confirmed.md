Title: Submission of general Mario Montoya Uribe to the JEP has been confirmed
Date: 2019-04-25 23:32
Author: CtgAdm
Category: English
Tags: Extrajudicial executions, JEP
Slug: submission-of-general-mario-montoya-uribe-to-the-jep-has-been-confirmed
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/general_mario_montoya-770x540.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

According to Order 131 of 2019, the Appeal Section of the Special Jurisdiction for Peace (JEP) has confirmed the submission of retired general **Mario Montoya Uribe** to the justice system. This submission,  related to the 003 case concerning deaths illegitimately presented as casualties in combat by agents of the State, have been validated as well as the recognition and citation of the victims to the "Sala de Reconocimiento"**\***.

The representatives of the victims filed appeals for two resolutions of the Sala de Definición de Situaciones Jurídicas**\***. Through these resolutions has been rejected the request of the attorneys seeking to annul the action of submission of General Montoya, trying to use a stratagem in the JEP procedure.

In spite of this, the Appeals Section denied all the petitions formed by Human Rights Colombia, the Cajar, the Colombian Commission of Jurists, the Inter-Church Justice and Peace Commission, the Minga Association and Humanity Valid Legal Corporation. Theys stated that an eventual massive defense-intervention for the victims could obstruct the development of a temporary jurisdiction as the JEP

For this reason, the JEP estimated that "**the defense-intervention for the victims must be proportional to the characteristics, purposes and relevance of each process**" in order to give them priority but in the same way being consistent with the procedure carried out by the judicial mechanism.

According to Human Rights Watch between 2006 and 2008, years in which Montoya was in charge of the Army "**at least 2,500 civilians would have been victims of extrajudicial executions**". In addition Montoya would have to answer for human rights violations committed during Operation Orion in the Commune 13 of Medellín **which affected at least 100 victims**.

###### \*The JEP has three Courts of Justice, made up of 18 magistrates and 6 amicus curiae. The latter are authorized to participate in the processes of the JEP, in order to offer information or concepts that provide greater legal elements for the resolution of a case. 
