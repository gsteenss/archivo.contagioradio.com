Title: Trailer "Ayotzinapa: Crónica de un crimen de Estado"
Date: 2015-05-19 18:39
Author: CtgAdm
Category: El mundo, Movilización
Tags: 43 desaparecidos Ayotzinapa, mexico, Paramilitarismo
Slug: trailer-ayotzinapa-cronica-de-un-crimen-de-estado
Status: published

##### [19 May 2015] 

*El documental que se estrenará el próximo 30 de mayo, lanzó su trailer oficial durante el fin de semana.*

Cuando la justicia se ve comprometida por la responsabilidad, acción u omisión, de aquellos encargados de garantizarla a todos y cada uno de los ciudadanos y ciudadanas de un país, es el momento en que se deben encontrar alternativas para visibilizar ante los ojos del mundo una realidad que está determinada solamente por lo que los grandes medios, locales e internacionales, permiten conocer.

Es justamente donde el documental adquiere un papel determinante en la visibilización de esas realidades, y su facultad de permitir que se levante una voz de indignación y denuncia. Es el caso de Xavier Robles, escritor y director de cine mexicano; una de esas personas que a través de su trabajo audiovisual, demuestra su compromiso y responsabilidad histórica con la realidad que desde hace algún tiempo se vive en su país, donde ha escrito los filmes "Rojo Amanecer", "Los motivos de Luz", "Las Poquianchis" y "Bajo la metralla".

Robles, junto con un equipo de trabajo conformado entre otros por la escritora Guadalupe Ortega en la producción y Bruno Santamaría como director de fotografía; trabajaron por cinco meses aproximadamente en **“Ayotzinapa: Crónica de un crimen de Estado"**, un documental crítico motivado por los hechos ocurridos durante el año inmediatamente anterior, con la desaparición de 43 estudiantes de la Escuela Normal Raúl Isidro Guerrero, por parte de fuerzas del Estado mexicano.

Mediante el relato testimonial y documentado de la desaparición forzada de los 43 estudiantes normalistas, los espectadores podrán escuchar las voces de los entrevistados, quienes son los encargados de narrar la historia que refleja la descomposición actual de México a partir de la matanza en Iguala, que más allá de ser una acción aislada se presenta como la denuncia de un suceso que tiene cercanos antecedentes históricos criminales de Estado.

En el marco del Segundo Encuentro Cinematográfico Nacional mexicano realizado en marzo de este año, Xavier Robles se refirió a la manera en que rechaza la situación actual de su país: **“Hemos permitido que avance la impunidad, la impunidad de los crímenes a un montón de crímenes desde hace décadas en este país (…) ya no queremos corrupción, ya no queremos impunidad, sobre todo ya no queremos crímenes y ya no queremos ser en otras palabras la vergüenza de América Latina y el mundo”.**

El caso de los 43 estudiantes de Ayotzinapa, población del estado de Guerrero en México, ubicada a 5 kilómetros de la ciudad de Tixtla de Guerrero, ha dado la vuelta al mundo, reafirmando de manera negativa la imagen del país, situación que no es ajena para el documentalista “**En este momento se habla de México muy despectivamente en el mundo, nosotros que fuimos los hermanos mayores de América Latina, ahora somos tristemente una vergüenza, así también lo han manifestados nuestros compañeros cineastas como Cuarón y del Toro”**.

<iframe src="https://www.youtube.com/embed/8Qli9Z38Cr8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

##### Producido por la cooperativa El Principio, Producciones en Cine y Video,  para la narración de la cinta participan dos grandes actores, cuyos nombres se reservaron. 

El documental, que estreno su tráiler oficial durante el fin de semana, tendrá su estreno el próximo 30 de mayo en la Cineteca nacional de México, y se espera sea exhibido en diferentes países del mundo. En cuanto a su difusión local Robles afirma que **“Ayotzinapa: crónica de un crimen de Estado” dudo mucho que le interese a las cadenas monopólicas del cine que hay en México, ya que desde luego va a ver censura, pues está película va ser 10 veces más explosiva que Rojo Amanecer”**.
