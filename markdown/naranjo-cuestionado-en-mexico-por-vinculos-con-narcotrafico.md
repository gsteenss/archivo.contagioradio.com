Title: Naranjo, cuestionado en México por vínculos con narcotráfico
Date: 2017-01-12 12:26
Category: DDHH, Judicial
Tags: narcotrafico, Oscar Naranjo, Paramilitarismo, Paramilitarismo en México, Vicepresidencia Santos
Slug: naranjo-cuestionado-en-mexico-por-vinculos-con-narcotrafico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/naranjo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unión Jalisco] 

###### [12 Enero 2017] 

El futuro vicepresidente, Óscar Naranjo Trujillo asumirá el cargo el próximo mes de marzo,  ex general de la Policía, quién hizo parte de la delegación del gobierno en las conversaciones con las FARC EP en La Habana, ha sido cuestionado por nexos con el narcotráfico en México.

En mayo de 2013, Naranjo fue nombrado por el gobierno de Peña Nieto para dirigir el Instituto Latinoamericano de Ciudadanía del Sistema Tecnológico y de Educación Superior de Monterrey, asesorar al Banco Interamericano de Desarrollo y al Gobierno de México.

En enero de 2014, Naranjo fue acusado ante medios de comunicación locales, por el Partido de la Revolución Democrática –PRD– de "favorecer la creación de grupos paramilitares en el estado de Michoacán y tener vínculos con carteles de narcotráfico".

Además, según una investigación de la revista Proceso, Naranjo fue señalado de acudir al paramilitarismo como parte de su estrategia para combatir al crimen organizado. Según, la publicación la Administración para el Control de Drogas Estados Unidos aseguró mediante algunos informes que exparamilitares y narcotraficantes extraditados a ese país han mencionado al general retirado en sus testimonios.Le puede interesar: [DEA vincula al general (r)](https://archivo.contagioradio.com/oscar-naranjo-es-senalado-por-la-dea/)Óscar Naranjo[con narcotráfico.](https://archivo.contagioradio.com/oscar-naranjo-es-senalado-por-la-dea/)

Por otra parte, Alejandro Sánchez Camacho, exsecretario general del PRD solicitó una investigación contra Naranjo, sostuvo que en su cargo como asesor del presidente Enrique Peña Nieto en temas de seguridad **“ha estimulado el surgimiento de grupos de mercenarios para contrarrestar el narcotráfico”.**

### Naranjo y Colombia 

Frente a los señalamientos recibidos en Colombia, Óscar Naranjo dijo a medios locales que dichas acusaciones eran **“una especie de persecución en su contra por los certeros golpes dados al narcotráfico”**, negando también en su momento las declaraciones del narcotraficante Juan Carlos Ramírez, conocido como ‘Chupeta’, en Estados Unidos, quien lo nombró en repetidas ocasiones durante sus testimonios.

De acuerdo con defensores de derechos humanos y promotores de paz en los Estados Unidos, los exparamilitares extraditados en mayo de 2008 por el gobierno de Uribe, indicaron a diferentes agencias de los Estados Unidos **la relación de Naranjo con el paramilitarismo desde finales de los 80 a través de Danilo González,** oficial de la policía con una cuestionada hoja de Vida.  
Los exparamilitares siempre expresaron el gran temor que les rodea de hablar del general Naranjo por el gran poder que obstenta desde los servicios de inteligencia conociendo los grandes secretos de los poderosos de Colombia.

En el libro web del fallecido periodista Manuel Vicente Peña, titulado el General Serrucho, aparece un perfil del general Naranjo.

Otro de los exparamilitares que se habría referido en las declaraciones es Daniel Rendón Herrera, alias “Don Mario”. Este aseguró que "en 2004 Naranjo se encontró con el narcotraficante y también paramilitar Miguel Arroyave justo antes de que asesinaran a este último".

También, según el portal de la periodista mexicana Carmen Aristegui, en abril del 2006 su hermano Juan David Naranjo Trujillo fue aprehendido por la policía alemana, acusado de vender 35 kilogramos de coca a dos policías encubiertos y estar involucrado en una red de narcotraficantes en Europa.

### Naranjo y las Detenciones Irregulares 

Naranjo ha sido cuestionado por las órdenes de detenciones masivas irregulares ocurridas en Cali,  cuando era director de la policía en esta ciudad.

Según el diario El País de Cali, un abogado de las víctimas, Naranjo habría mandado retener a unas 182 personas en un operativo, “las incomunicó y no se les respetaron sus derechos fundamentales (…) **algunos de ellos incluso pasaron hasta dos años sin recibir un proceso apegado a la ley”.**

**Se desconoce si alguna acción judicial en Colombia, México o Estados Unidos estaría abierta** contra el prestigioso policía, reconocido por una agencia de los Estados Unidos como el mejor del mundo.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
