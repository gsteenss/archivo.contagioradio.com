Title: Nueva reforma tributaria significará 'crecimiento económico' para los de siempre
Date: 2019-12-03 15:55
Author: CtgAdm
Category: Economía, Entrevistas
Tags: Impuestos, Ley de Financiación, Paro Nacional, Reforma tributaria
Slug: nueva-reforma-tributaria-significara-crecimiento-economico-para-los-mismos-de-siempre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Foto-Presidencia-e1575405054999.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia de la República  
] 

Esta semana inició la discusión en el Congreso de la 'Ley de Crecimiento Económico', el reemplazo de la Ley de Financiación que tumbó la Corte Constitucional por tener vicios en su procedimiento y que, en visión de académicos y expertos, tampoco respondería a las necesidades del grueso de la población colombiana. Entre las críticas, se señala que esta nueva Ley **no promueve el Crecimiento Económico, beneficia a los grandes capitales y no favorece, como lo señala el Ministro de Hacienda, a las familias con menos ingresos.**

### "Es una contrarreforma que le da más a los poderosos del país, en demérito de la mayoría de la población" 

El senador por el Polo **Wilson Arias** recordó que el Gobierno está presentando artificiosamente una Ley Fiscal, que en realidad es una contrarreforma "que le da más a los poderosos del país, en demérito de la mayoría de la población". Según explicó el Senador, este Proyecto "se caracteriza por reducir el impuesto de renta a las personas jurídicas, reducir el IVA a bienes de capital y hacer un descuento al ICA", y "a renglón seguido, el Gobierno dice que dejará tres días sin IVA" para todos los consumidores.

La sustentación del Gobierno para aprobar dicho Proyecto señala que al reducir la carga tributaria a las empresas, las mismas podrán jalar el crecimiento económico del país; sin embargo, **estudiosos del tema, como el director de Cedetrabajo Mario Valencia, señalan que esta afirmación "no tiene ningún sustento teórico ni empírico"**. A la crítica se sumó Salomón Kalmanovitz, que en una carta conjunta con otros académicos, cuestionaron que los descuentos en el Impuesto de Renta Corporativo, el IVA en adquisición de bienes de capital y el ICA significa dejar de percibir recursos por 9 billones de pesos.

> [\#QuieroSaber](https://twitter.com/hashtag/QuieroSaber?src=hash&ref_src=twsrc%5Etfw) II ⚠La Ley de Crecimiento Económico que radicó el gobierno de Duque y el ministro Carrasquilla, es la misma ley de financiamiento que tumbó la Corte por ilegal, pero con otro nombre. (Abrimos hilo)? [pic.twitter.com/YbdiI1dNJM](https://t.co/YbdiI1dNJM)
>
> — Cedetrabajo (@Cedetrabajo) [November 28, 2019](https://twitter.com/Cedetrabajo/status/1200121357473853442?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
En cambio, el Gobierno expresó que esta Ley incluía beneficios para las personas con menores ingresos, como la devolución del IVA o la propuesta de otorgar tres días al año sin este impuesto. Una medida que según el director de Cedetrabajo, el profesor Kalmanovitz y el senador Arias, no responde a las necesidades de las personas, porque **significaría tener que bancarizar a todos los beneficiarios para hacerles la devolución del IVA, y obligarlos a comprar en el mercado formal durante los tres días exentos de este impuesto.**

### ¿Qué significan los beneficios tributarios al gran capital? 

Según resumió Valencia en una opinión para Las Dos Orillas, **las exenciones en impuestos para los grandes capitales cuestan cada año 17 billones de pesos al Estado colombiano**, lo que para el senador Arias, se traduce en la necesidad de compensar estos recursos que dejan de percibirse con un aumento en la deuda externa, "o adelantando privatizaciones". (Le puede interesar: ["Holding financiero; la jugadita de Duque privatizando los dineros públicos"](https://archivo.contagioradio.com/holding-financiero-la-jugadita-de-duque-privatizando-los-dineros-publicos/))

Adicionalmente, Arias cuestionó que sectores políticos que antes habían expresado su rechazo a la Ley, o al menos habían dicho no apoyarla, ahora anuncian que le dan su voto positivo en el Congreso. Según el Senador, este movimiento respondería a la imposición de sectores tradicionales y empresariales, encabezados por Germán Vargas Lleras y César Gaviria desde Cambio Radical y el partido Liberal, respectivamente. (Le puede interesar: ["Carrasquilla se equivocó de fórmula para reducir el desempleo"](https://archivo.contagioradio.com/carrasquilla-equivoco-reducir-desempleo/))

No obstante, el Congresista por el Polo también consideró la presión que pueda ejercer el Paro Nacional hacia los sectores que apoyan el Proyecto y al Gobierno mismo, **esperando que se modifique esta nueva reforma tributaria, y se cambie por una que sí beneficie a la mayoría de colombianos.** (Le puede interesar:["Duque llevará al Congreso la misma Ley de Financiamiento que le devolvieron"](https://archivo.contagioradio.com/duque-llevara-congreso-misma-ley-de-financiamiento/))

### **Actualización: Congreso aprobó en primer debate la Reforma Tributaria y Reforma Laboral**

Tal como lo señaló el senador [Gustavo Bolívar](https://archivo.contagioradio.com/la-defensa-de-la-vida-un-punto-central-del-paro-nacional/) en entrevista con Contagio Radio, dando la espalda al país que está en movilización, el Congreso aprobó la Reforma Tributaria y la Reforma Laboral el pasado martes 3 de diciembre simultáneamente. De igual forma, el Congreso aprobó en su primer debate el proyecto de Ley que concede la segunda instancia retroactiva para los funcionarios aforados, o llamada Ley Andrés Felipe Arias, que beneficiaría a más de 250 exfuncionarios juzgados por diferentes delitos por la Corte Suprema de Justicia.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45282799" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45282799_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
