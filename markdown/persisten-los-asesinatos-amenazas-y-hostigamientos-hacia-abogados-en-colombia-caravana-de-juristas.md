Title: Amenazas a juristas en Colombia estarían relacionadas con actividad de multinacionales
Date: 2015-04-30 11:35
Author: CtgAdm
Category: DDHH, Nacional
Tags: abogados, caravana de juristas, Derechos Humanos, paz, PND, Proceso de conversaciones de paz de la Habana, Unidad Nacional de protección, UNP
Slug: persisten-los-asesinatos-amenazas-y-hostigamientos-hacia-abogados-en-colombia-caravana-de-juristas
Status: published

###### Foto: Contagio Radio 

La **Caravana Internacional de Juristas** presentó un informe que evalúa las condiciones de DDHH y la normatividad que protege a las y los abogados defensores de Derechos Humanos en Colombia, tras la visita realizada por más de 60 abogados y juristas del mundo.

El informe afirma que en el territorio nacional (Bogotá, Bucaramanga, Cartagena, Medellín, Buenaventura, Cali y Pasto, entre otros), **persisten los asesinatos, amenazas y hostigamientos hacia abogados y abogadas defensoras de Derechos Humanos**, y que los organismos nacionales e internacionales deben prestar atención a esta situación, máxime en un contexto de diálogos de paz, pues "sin abogados no hay justicia, y para alcanzar el postconflicto basado en el Estado de Derecho, el Estado ha de impedir activamente la estigmatización de los abogados defensores, apoyándoles, así como a los jueces incluso en aquellos casos en que los clientes de los primeros hayan desafiado al gobierno políticamente o por medios ilegales".

<div>

El informe incluye **14 recomendaciones a las instituciones colombianas**, **8 a los gobiernos extranjeros y organizaciones internacionales, y 2 a las empresas multinacionales que intervienen en Colombia**, que incluyen el llamado a apoyar decididamente el proceso de paz en Colombia; la revisión de los casos de amenazas y homicidios contra abogados en Colombia; el aumento del presupuesto a la Unidad Nacional de Protección; respeto por parte de las fuerzas militares y de policía; y que se monitoreen las acciones de las empresas multinacionales y su relación con amenazas y asesinatos de defensores de Derechos Humanos, entre otros.

<iframe src="http://www.ivoox.com/player_ek_4430074_2_1.html?data=lZmgkpWbeI6ZmKiakpiJd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8LmwtvO0MaPqMafq9rfy9jYpdSZk6iYtMrHs87Zz8nOxc7TssbnjMaY0MbHrdDiwtHS1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Recomendaciones a entidades nacionales] 

</div>

<iframe src="http://www.ivoox.com/player_ek_4430080_2_1.html?data=lZmgkpWcdI6ZmKiakpiJd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8LmwtvO0MaPqMafy9rfy9jYpdSZk6iYy9PYqdPiwsjW0dPFsMbnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Recomendaciones a entidades internacionales] 

<div>

La Caravana de Juristas alertó principalmente sobre los peligros que conlleva para Colombia la implementación deficiente de la Ley 1448 de Víctimas y Restitución de Tierras; la posible ampliación del fuero penal militar; y la crisis de la rama legislativa generada por los escándalos de paramilitarismo y corrupción del Magistrado Jorge Pretelt.

\[embed\]https://www.youtube.com/watch?v=gETohqHsHM0\[/embed\]

</div>
