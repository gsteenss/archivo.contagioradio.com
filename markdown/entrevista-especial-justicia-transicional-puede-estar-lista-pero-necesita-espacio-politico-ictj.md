Title: Especial DEJusticia Transicional con Camila Moreno de ICTJ
Date: 2015-07-08 15:27
Category: Entrevistas, Nacional
Tags: Camila Moreno, ICTJ, justicia transicional en colombia
Slug: entrevista-especial-justicia-transicional-puede-estar-lista-pero-necesita-espacio-politico-ictj
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/justicia-transicional-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [probono.cl]

###### [8 Jul 2015] 

En el marco de las conversaciones de paz se está discutiendo en torno a la justicia, las propuestas de justicia transicional y lo que hay en la constitución colombiana son algunos de los puntos de partida de este posible acuerdo, sin embargo, existen algunos temas centrales sobre los cuales se están presentando las distancias más grandes entre el gobierno colombiano y la guerrilla de las FARC.

Contagio Radio, dialoga con la doctora Camila Moreno, directora del Centro Internacional para la Justicia Transicional en  Colombia, ICTJ, para analizar los puntos neurálgicos del posible acuerdo.

**[Contagio Radio] ¿Qué hay en el aparato de justicia colombiano que aporta a la mesa de conversaciones de Paz?**

[**Camila Moreno**]: Yo creo que tenemos que analizar varios aspectos, y es cuáles son los requerimientos, desde el punto de vista del sistema de justicia, para lograr, y al mismo tiempo garantizar en el mayor grado posible los derechos de las víctimas, y al mismo tiempo lograr una solución negociada al conflicto; balance que, por supuesto, no es fácil, pero que finalmente las partes tendrán que encontrar ese balance necesario.

Ese balance va a requerir de una capacidad institucional, y de unos mecanismos que, creo yo, el sistema actual de justicia no nos ofrece. Lamentablemente, hay una profunda desconfianza de la sociedad en general, en el sistema actual de justicia; la crisis por la que está atravesando de legitimidad, nos obliga a pensar en mecanismos que posiblemente tengan que estar por fuera de ese sistema para lograr una salida que logre ese balance.

**[*["Lamentablemente, hay una profunda desconfianza de la sociedad en general, en el sistema actual de justicia].*"]**

¿Cuáles pueden ser esos mecanismos? Eso es justamente lo que se está discutiendo en la Habana, pero probablemente una salida que tenga que encontrar un mecanismo que brinde las plenas garantías a las partes, pero que también pueda les generar la legitimidad necesaria tanto a las partes como a la sociedad en su conjunto y probablemente tengamos que inventar un mecanismo nuevo porque el actual sistema no nos da esas garantías.

**[C.R] ¿Qué  va a pasar con lo que está investigándose y con las condenas que ya hay en contra de las FARC?**

**[C.M:]** En el caso de las FARC en particular, el punto de partida es un Estado que ha actuado con todas las herramientas legales que ha tenido a su disposición y con todo el aparato represivo en contra de las FARC. No estamos aquí en una situación en la que podamos decir, (como sucedió cuando la ley 975, ley de justicia y paz), que estábamos en un punto de absoluta impunidad. Aquí al contrario. El aparato de justicia ha funcionado durante 50 años con todas sus herramientas en contra de las FARC, y eso se refleja en las sentencias que hay ya contra las FARC, que son muchísimas.

Obviamente muchas de las sentencias en ausencia, pero esa es otra discusión; si el Estado no ha sido capaz de capturar a quienes hayan sido condenados pues eso es otra discusión, pero lo que no podemos negar es que ha habido justicia en el caso de las FARC, que el Estado ha cumplido con su obligación de investigar, juzgar y sancionar  en el caso de las FARC, y eso nos pone en un punto de partida distinto para cualquier discusión en torno a cuál debe ser el tratamiento judicial a las FARC en el marco del Proceso de Paz.

**[*"[El aparato de justicia ha funcionado durante 50 años con todas sus herramientas en contra de las FARC, y eso se refleja en las sentencias que hay ya contra las FARC, que son muchísimas."]*]**

La discusión es ¿qué hacer con las sanciones que ha impuesto el sistema de justicia?, obviamente también es cierto que muchas de esas sentencias son cuestionables, que no se ha garantizado el debido proceso; las FARC misma no han tenido la posibilidad de controvertir esas decisiones judiciales y cualquier mecanismo que se defina ahora en la mesa tendrá también que incluir la posibilidad de revisión de muchas de esas decisiones que, seguramente, han sido injustas, o que tienen profundas debilidades porque no se ha garantizado el debido proceso.

Ese es un aspecto que habría que considerar y ver en el mecanismo que se diseñe, cómo garantizar que eso se revise, obviamente si es decisión y deseo de las FARC.

El otro tema son las penas, incluso yo quisiera señalar que la misma Fiscalía de la corte penal internacional, en su informe del 2012, ya señalaba que, en principio, de acuerdo con la información que esa Fiscalía tiene, el Estado colombiano habría cumplido con la obligación de investigar, juzgar y sancionar delitos que serían de competencia de la Corte Penal Internacional, en el caso de las FARC.

**[*["El Estado colombiano habría cumplido con la obligación de investigar, juzgar y sancionar delitos que serían de competencia de la Corte Penal Internacional, en el caso de las FARC.]"*]**

La misma Fiscalía de  esta Corte estaría reconociendo que efectivamente Colombia ha cumplido con esa obligación y lo que dice a continuación es que habría que ver cómo se va a resolver el tema de la ejecución de la pena, que se establece en esas sentencias, y obviamente ahí está todo el debate del tipo de sanciones que debería aplicarse; la posibilidad de temas alternativas y de mecanismos que reafirmen el rechazo a esos crímenes internacionales, pero que al mismo tiempo sean consecuentes con la búsqueda de la paz, por supuesto también no es fácil encontrar la solución, habrá que ser muy creativos, pero el Estado colombiano tiene la posibilidad de generar sus propios mecanismos atendiendo a unos parámetros internacionales generales, pero los mismos instrumentos internacionales les dan la posibilidad al Estado  colombiano de definir qué tipo de sanciones deben ser aplicadas en estos casos.

**[C.R:] ¿Qué se plantea en los casos de delitos, violaciones de derechos humanos, e infracciones al DIH cometidas por los militares? **

[**C.M:**] Ese es otro tema súper importante porque cualquier mecanismo que se diseñe en la Habana, tiene que considerar a todas las partes del conflicto. Obviamente aquí no estamos hablando de un mecanismo unilateral ni mucho menos, o un mecanismo solamente pensado para las responsabilidades de las FARC,  como si ellos fueran el único actor que participó en este conflicto; por supuesto que cualquier medida que se diseñe tiene también que incluir las responsabilidades de los agentes del Estado, y aquí va a haber y ya hay un debate sobre la mesa y en la sociedad muy importante y es si debe haber o no simetría en el tratamiento de las partes.

Hay varias posiciones; hay una posición que dice que tiene que haber simetría porque es el mismo conflicto, y que no podría haber un tratamiento más benévolo a las FARC y un tratamiento más severo para los agentes del Estado, sino que se tendría que hacer el mismo; incluso hay quienes dicen que debería ser un tratamiento más benévolo para los agentes del Estado y, por el contrario, más duro en el caso de las FARC.

Nosotros como ICTJ consideramos que lo primero que hay que ver es que las responsabilidades son de naturaleza distinta, efectivamente no es comparable la naturaleza de la responsabilidad de la guerrilla a la naturaleza de la responsabilidad de los agentes del Estado. La fuerza pública tiene una posición de garante, tiene un mandato constitucional que, independientemente de que exista un conflicto armado, tiene que respetar y su carácter es el de garantía y protección de los DDHH, y en esa medida, creemos, tiene una responsabilidad mayor, en el caso de agentes del Estado, y que esa responsabilidad mayor, debido a su posición de garante, tiene que reflejarse en el diseño que se haga de las medidas de justicia transicional.

***["Lo primero que hay que ver es que las responsabilidades son de naturaleza distinta, efectivamente no es comparable la naturaleza de la responsabilidad de la guerrilla a la naturaleza de la responsabilidad de los agentes del Estado."]***

Ahora, no quiere decir eso, llevar esta consideración al extremo y decir que los agentes del Estado tienen que pasar años de años en las cárceles mientras que las FARC no, sino tiene que haber algún tipo de diferenciación en el tratamiento.

**[C.R:] ¿Existe voluntad para que se asuma lo que tiene que asumirse en torno a la justicia?**

[**C.M:**] Yo creo que esa es la pregunta del millón y creo que es la nuez de este asunto. El espacio jurídico para la definición de qué hacer en cuanto a las responsabilidades penales está creado, existe, existe el espacio jurídico para diseñar un mecanismo que se adapte a las necesidades y condiciones de este conflicto armado y los actores de este conflicto armado. Mi preocupación fundamental es que no hay el espacio político y que el espacio político es cada vez más reducido y más complicado, y esto ya no es solamente una discusión jurídica, esto no es un debate técnico meramente, y por eso te digo que incluso si nos vamos a lo técnico,  a lo jurídico, las posibilidades están ya puestas, digamos, los caminos están ya trazados.

[***"Mi preocupación fundamental es que no hay el espacio político y que el espacio político es cada vez más reducido y más complicado"***]

El tema es qué espacio político tenemos hoy para la decisión de qué hacer; la decisión de por qué camino jurídico optar, y yo tengo la sensación de que a veces uno cree que las cosas van mejor en la Habana de lo que van aquí. Yo creo que el espacio político en el país es hoy un espacio político reducido para la negociación en general, y eso es preocupante porque de todas maneras lo que se define en la Habana tiene que contar con algún tipo de legitimidad en la sociedad o sino no va a funcionar, y creo que eso es lo que nos está faltando; un debate amplio en la sociedad en donde se puedan poner todas las posiciones y que podamos lograr, casi como un pacto como sociedad para que los acuerdos de la Habana al final se puedan implementar.

***["Yo creo que no tenemos otra opción que seguir siendo optimistas porque, de lo contrario, estamos en el peor de los mundos, entonces tenemos que seguir siendo optimistas y seguir empujando para que esto salga adelante."]***
