Title: Este Domingo las víctimas se toman la localidad de Bosa en Bogotá
Date: 2015-04-24 18:32
Author: CtgAdm
Category: Movilización, Nacional
Tags: Derechos Humanos, Encuentro local de víctimas, Localidad de Bosa, mujeres
Slug: este-domingo-las-victimas-se-toman-la-localidad-de-bosa-en-bogota
Status: published

<iframe src="http://www.ivoox.com/player_ek_4404310_2_1.html?data=lZmdlpiVdI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRh9aZpJiSpJbFb6Pj1Maah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [*Invitación*] 

La localidad de Bosa será escenario para reconstruir la memoria y rescatar el olvido tejiendo recuerdos, con el **Primer encuentro local de víctimas de la localidad de Bosa**.

Luego de años colmados de amenazas, asesinatos y desplazamientos forzados, las víctimas, jóvenes, hombres, mujeres, niños y niñas, han encontrado un espacio para unir fuerzas en contra del olvido y la impunidad.

\[embed\]https://www.youtube.com/watch?v=Too0cjaQ8nw\[/embed\]

Con foros que abarcarán temas de contexto social como **el conflicto, la memoria y ley de víctimas**, además de un encuentro cálido con la olla comunitaria y puestas artísticas en escena, el Comité permanente por la defensa de los Derechos Humanos, convoca al Primer encuentro local de víctimas de la localidad de Bosa, el cual se llevará a cabo el **domingo 26 de marzo del 2015 en el Colegio Claretiano, Cll 60-s\#80k-02, desde las 08:00 a.m.**

Este será sin duda un espacio donde convergerán distintas experiencias de vida y personas que podrán tejer el hilo de la verdad y la memoria; un escenario que abrirá por primera vez, en Bosa, la posibilidad de reconstruir la dignidad de las víctimas.
