Title: Tauramena se moviliza para exigir respeto de Ecopetrol a Consulta Popular
Date: 2017-02-21 16:23
Category: Ambiente, Nacional
Tags: Consulta Popular Tauramena, Ecopetrol, Ocesa, Tauramena Casanare
Slug: tauramena-se-moviliza-exigir-respeto-ecopetrol-consulta-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/tauramena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Anne Frank] 

###### [21 Feb 2017 ] 

Más de 1.000 personas de distintos sectores sociales y habitantes de Tauramena Casanare, se movilizaron para exigir a Ecopetrol y Ocesa que **cumplan la Consulta Popular que ganó el pueblo en 2013, que retiren la demanda que interpusieron contra la Consulta** y mejoren las condiciones laborales en sus operadoras.

July Méndez, ingeniera e integrante del comité por la Defensa del Agua en Tauramena, explica que desde el 2013 Ecopetrol viene “desprestigiando la voluntad del pueblo”, hace un año interpuso una dacción de nulidad a la Consulta Popular del 15 de Diciembre de 2013, en la que votaron 4.610 ciudadanos de los cuales, **4.426 rechazaron el proyecto Odisea 3D, que contemplaba exploración sísmica, perforación, producción y transporte de hidrocarburos en la zona.**

### Ecopetrol demanda la decisión del pueblo 

La ingeniera señala que la comunidad decidió movilizarse para exigir respuestas a Ecopetrol, Ocesa, entidades ambientales y en general al Estado, para que [“se respete la decisión soberana del pueblo, que es legal, legítima y constitucional”](https://archivo.contagioradio.com/todas-las-decisiones-acerca-de-nosotros-con-nosotros/) y además para que la Agencia Nacional de Hidrocarburos **de respuesta sobre el otorgamiento de un bloque petrolero de 442 km2, “que abarca no sólo Tauramena sino otros 5 municipios** que se ubican en zonas de recarga hídrica”, en las que el uso de suelo según el POT es “uso de suelo de protección y conservación”.

Méndez comenta que además del incumplimiento y afectaciones al ambiente, las petroleras “no tienen en cuenta la mano de obra de habitantes del municipio”, lo que ha generado “una de las crisis económicas más difíciles del municipio”, indica que **muchos jóvenes al terminar sus estudios escolares se van del municipio pues no encuentran oportunidades.**

También señala que la dinámica de la industria petrolera genera que las economías locales se vean desplazadas, “pedimos también que estas empresas promuevan esas pequeñas economías” y que además **cumplan con sus obligaciones de inversión social. **

Revela que además de movilizaciones sociales, hace un buen tiempo la comunidad viene trabajando en alternativas como el ecoturismo, **“sabemos que a la industria petrolera le queda poco tiempo, por eso buscamos alternativas económicas** (…) queremos desarrollo sustentable y seguir defendiendo nuestros recursos, nuestra vida y la permanencia de generaciones futuras”. ([Le puede interesar: Así afectaría explotación petrolera a Caño Cristales](https://archivo.contagioradio.com/presumo-que-hubo-torciditos-para-lograr-esa-licencia-lamacarena/))

<iframe id="audio_17148568" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17148568_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
