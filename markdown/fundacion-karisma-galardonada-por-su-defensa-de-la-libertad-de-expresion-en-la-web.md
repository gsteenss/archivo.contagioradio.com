Title: Fundación Karisma galardonada por defender la libertad de expresión en la web
Date: 2019-04-05 18:05
Author: AdminContagio
Category: DDHH, Tecnología
Tags: Fundación Karisma, Libertad de expresión
Slug: fundacion-karisma-galardonada-por-su-defensa-de-la-libertad-de-expresion-en-la-web
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/56320433_2211325562287718_1005898481937678336_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Karisma] 

###### [5 Abr 2019] 

La Fundación Karisma, organización que busca garantizar y ejercer los derechos de los usuarios en la web, fue galardonada con el **Premio Index Censorship Freedom of Expression 2019** en la categoría de activismo digital, debido al impacto significativo en su lucha contra la censura y  destacando en particular or ser la única organización de América Latina nominada a este galardón.

Pilar Saenz, coordinadora de Proyectos de la Fundación Karisma explica que participaron en esta convocatoria presentando dos iniciativas, la primera **'Alerta Machitroll**', un proyecto que busca, a través del humor llamar la atención sobre el problema del machismo y la violencia contra las mujeres que se expresan en línea.

En segundo lugar, se presentó el proyecto **"Compartir No es delito"**, el cual realizó un acompañamiento al biólogo Diego Gómez, quien tenía un proceso judicial en contra "por compartir una tesis de alguien más en Internet, sin siquiera atribuirse su autoría y con él único fin de compartir conocimiento", promoviendo el libre flujo de información.

La coordinadora señala que la **Fundación Karisma,** competía con otras 400 nominaciones públicas y finalmente fue incluida en la categoría de **activismo digital, de** la cual resultó ganadora entre otras propuestas similares provenientes de Medio Oriente e India, "la Internet ha sido un espacio donde la libertad de expresión se ha visto favorecida, pero que aún no ha sido  de total acceso en igualdad de condiciones", añade.

Para Karima este premio, que ha sido otorgado a otras destacadas figuras como **Julian Assange y la Nobel de Paz, Malala,** significa "un reconocimiento al trabajo de muchos y para las organizaciones que en América Latina trabajan por la libertad de expresión en medios digitales". [(Lea también: Se lanza en Colombia plataforma para combatir el acoso en Internet)](https://archivo.contagioradio.com/la-lucha-contra-el-acoso-en-internet-llega-a-colombia/)

<iframe id="audio_34131139" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34131139_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
