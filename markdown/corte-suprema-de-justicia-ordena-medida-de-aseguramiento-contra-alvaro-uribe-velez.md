Title: Corte Suprema de Justicia ordena medida de aseguramiento contra Álvaro Uribe Vélez
Date: 2020-08-04 14:12
Author: CtgAdm
Category: Actualidad, Judicial
Tags: Álvaro Uribe Vélez, Centro Democrático, Diego Cadena, Manipulación de testigos
Slug: corte-suprema-de-justicia-ordena-medida-de-aseguramiento-contra-alvaro-uribe-velez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/34146277542_20101131f2_b.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

La Corte Suprema de Justicia ordenó medida de aseguramiento al expresidente y senador **Álvaro Uribe Vélez** quien es investigado por presunta manipulación de testigos. Esta se convierte en una decisión histórica al ser primera vez que un expresidente de la República tiene una medida de este tipo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Sala de Instrucción de la Corte Suprema de Justicia se reunió este lunes de manera extraordinaria sin embargo no llegó a ningún acuerdo por lo que el debate continúo este martes y tras dos días de sesión virtual. se tomó la decisión de dictar detención domiciliaria. Cabe señalar que en el proceso participaron cinco de los seis integrantes de la Sala de Instrucción, tras la separación del caso de la también magistrada Cristina Lombana.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Cortes Piden respetar su autonomía

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre el anuncio, el abogado Reinaldo Villalba, apoderado del senador Iván Cepeda señaló que la Corte Suprema  de Justicia contaba con "abundantes y solidas pruebas", además resaltó la independencia que mostró la corte pese a la enorme presión de la que fue objeto incluso por parte del presidente Iván Duque, su gabinete y altos mandos del Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mediante un comunicado conjunto firmado por los presidentes de las altas cortes y de la Jurisdicción Especial para la Paz (JEP) las instituciones habían hecho un llamado para respetar la autonomía de los jueces y rechazaron las presiones de diferentes sectores, incluido el Centro Democrático. [(Lea también: Piden respeto a la Corte Suprema por indagatoria a Álvaro Uribe)](https://archivo.contagioradio.com/piden-respeto-a-la-corte-suprema-por-indagatoria-a-alvaro-uribe/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de una rueda de prensa, el senador Iván Cepeda afirmó que la decisión tomada por la Corte Suprema de Justicia, **"ayuda a consolidar la democracia en Colombia (...) no hay individuos que estén encima de la ley y la justicia por muy poderosas que sean, es una lección que hay que asumir con serenidad".**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Ocho años de un caso que avanza a una nueva etapa

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El proceso que enfrenta el expresidente comenzó en 2012, cuando denunció al congresista [Iván Cepeda](https://twitter.com/IvanCepedaCast) por presunta manipulación de testigos. En aquel entonces, el senador del Polo Democrático preparaba una denuncia en el Senado contra el expresidente por vínculos con el paramilitarismo, por lo que entrevistó a diferentes líderes de grupos paramilitares desmovilizados y que cumplían condenas en centros penitenciarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En 2018, la Corte Suprema de Justicia archivó el caso contra el senador Cepeda y abrió indagación al hoy senador del Centro Democrático quien es investigado por presuntamente haber intentado manipular el testimonio de Juan Guillermo Monsalve, quien lo ha señalado de fundar el bloque Metro de las Autodefensas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Estamos listos para la nueva etapa que inicia hoy, estamos listos a acatar y respetar las decisiones de la Corte y solamente si el caso lo amerita apeláramos a las vías que la justicia nos ofrece", concluyo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un proceso que dio garantías al expresidente Uribe

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El senador Cepeda también destacó que se trató de un proceso en el que la contraparte tuvo a su alcance **"todas herramientas y recursos que están disponibles para garantizar su derecho a la justicia".** [(Lea también: Uribe ha elegido la vía de la presión mediática y política contra el poder judicial: Iván Cepeda)](https://archivo.contagioradio.com/uribe-ha-elegido-la-via-de-la-presion-mediatica-y-politica-contra-el-poder-judicial-ivan-cepeda/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cepeda agregó que su actuación y la de su equipo de abogados a lo largo de un proceso de ocho años de duración se ha limitado a ejercer la defensa, "no hemos llamado a ningún testigo pertenecientes a grupos paramilitares u organizaciones criminales antes la Corte Suprema de Justicia, quienes han recurrido a 22 testigos presentados ante la Corte ha sido la contraparte.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Hoy ha cambiado algo en Colombia y es que la Corte Suprema de Justicia nos ha recordado que existe una Constitución
>
> <cite>Senador Iván Cepeda</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Durante esta semana se espera que también se defina el futuro del abogado Diego Cadena, quien fue apoderado del expresidente y a quien en la actualidad se le han imputado cargos y haber incurrido en soborno de testigos que declararan en contra del senador Iván Cepeda, además de ofrecer beneficios a los declarantes que han vinculado al expresidente Uribe con el paramilitarismo. Según la Fiscalía, el abogado Cadena habría ofrecido \$200 millones para conseguir declaraciones falsas. [(Le puede interesar: “Responsabilidad de Álvaro Uribe en el caso de Diego Cadena es innegable” - Iván Cepeda)](https://archivo.contagioradio.com/alvaro-uribe-tiene-responsabilidad-innegable-en-caso-diego-cadena/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
