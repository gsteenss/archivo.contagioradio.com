Title: Cachetada del Gobierno a trabajadores con aumento del 6% al salario mínimo en 2020
Date: 2019-12-26 19:03
Author: CtgAdm
Category: Economía, Nacional
Tags: 2020, Duque, iva, salario minimo, Trabajadores
Slug: cachetada-del-gobierno-a-trabajadores-con-aumento-del-6-al-salario-minimo-en-2020
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Salario-mínimo-2016.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/16576.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

Este jueves 26 de diciembre, luego de varias semanas de reuniones fallidas entre los gremios de trabajadores y empresarios,  el gobierno encabezado por Ivan Duque decidió, por decreto, que el aumento del salario mínimo legal vigente (SMLV), para el 2020 será del 6%, quedando en \$877.802; lo que representa un incremento de solo \$49.686.

Asímismo informaron que el subsidio de transporte incrementará en el mismo porcentaje, es decir, \$102.853, siendo así el salario mínimo más auxilio: en \$980.657. (Le puede interesar: [Salario mínimo no cuadra con aumento del costo de vida en Colombia](https://archivo.contagioradio.com/salario_minimo_2018_colombia/))

> "Hemos tomado la decisión de firmar el decreto del aumento del [\#SalarioMínimo](https://twitter.com/hashtag/SalarioM%C3%ADnimo?src=hash&ref_src=twsrc%5Etfw), vamos a hacerlo igual que el año pasado, el 6% de aumento de salario mínimo y el 6% de aumento del auxilio de transporte, eso nos lleva a estar cerca de \$980.000" Presidente [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw)
>
> — Presidencia Colombia (@infopresidencia) [December 26, 2019](https://twitter.com/infopresidencia/status/1210224360675532800?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Teniendo en cuenta que en Colombia el mínimo se debe calcular a la luz de las cifras de inflación y productividad,  y que el valor determinado es insuficiente, el Presidente agregó; *"Este año, después de haber hablado con el sector privado y políticos, hemos tomado la decisión de hacer un ejercicio en el Congreso para sacar adelante esa prima adicional que va a permitirnos en 2020 llegar al millón de pesos o más".*

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Salario-mínimo-2016.jpg){.aligncenter .wp-image-19529 width="404" height="269"}

###### [Foto: Archivo ] 

### Sin acuerdo se modifica el salario mínimo para 2020

En el marco de los acuerdos entre el sindicato de trabajadores quienes exigían un aumento de \$1.000.000  más subsidio de transporte, lo que representaba un 8,1%; los empresarios quienes pedían el 5%, y el Gobierno que mediaba entre estas solicitudes, la respuesta resultó completamente alejada de la petición del sector obrero, quienes rechazaron la cifra, al ser un valor que se aleja de las necesidades que tiene un Colombiano promedio.

 

> Incremento del 6%, una burla del presidente Duque al país, a los pobres y una adición a los 10 billones del regalo que en la reforma tributaria le dio a los super ricos. Rechazo total. Ridículo enorme del gobierno y los empresarios por 20mil pesos, no llegar al millón.
>
> — Diógenes Orjuela (@diogenesorjuela) [December 26, 2019](https://twitter.com/diogenesorjuela/status/1210257525167271936?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### Conozca qué valores suben junto con el mínimo

El incremento del salario establecido para el 2020 trae consigo un aumento en otros costos como; servicios notariales, aportes a salud, pensión, y cuotas moderadoras de servicios médicos. De igual forma también habrá un  alza en comparendos de tránsito, tarifas de patios, grúas, Seguro Obligatorio contra Accidentes de Tránsito (SOAT), arriendo y peajes. Vale la pena resaltar que estas cifras se incrementarán conforme lo haga la inflación de 2019, número que se conocerá hasta las primeras semanas de 2020; sin embargo expertos han revelado que esta cifra ya supera el 3%. (Le puede interesar: [Se necesita aumento del 12% en salario mínimo para recuperar la economía: CUT)](https://archivo.contagioradio.com/se-necesita-aumento-del-12-en-salario-minimo-para-recuperar-la-economia-cut/)

El valor exacto se conocerá hasta el próximo años por entidades como el Ministerio de Transporte y de Salud, al igual que las de otras entidades que tendrán en cuenta el aumento del mínimo y los ingresos de los contribuyentes para fijar el monto. (Le puede interesar: [¿Sabe cuánto le cobra su fondo de pensión por administrar su dinero?](https://archivo.contagioradio.com/cuanto-cobra-fondo-pension/))

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/16576.jpeg){.wp-image-78609 .alignleft width="438" height="312"}

 

 

 

 

 

 

 

 

 

Con esta decisión Colombia no cambiará su posición dentro de los países latinoamericanos con el mínimo más bajo; teniendo el cuenta que el aumento que equilibraría la balanza con respecto a las solicitudes de los gremios era de 6,3%, que representaba \$52.171 y no \$49.686. (Le puede interesar: [Gobierno se debe poner en los zapatos de los trabajadores y aumentar 14% salario mínimo](https://archivo.contagioradio.com/gobierno-se-debe-poner-en-los-zapatos-de-los-trabajadores-y-aumentar-14-salario-minimo/))

 
