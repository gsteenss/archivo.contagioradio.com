Title: Minería a cielo abierto llegó a Suesca en Cundinamarca
Date: 2016-10-24 14:49
Category: Ambiente, Nacional
Tags: Gran Minería en Colombia, minería a cielo abierto, planton
Slug: mineria-cielo-abierto-llego-suesca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/suesca-e1477330851956.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Naturaleza y Patrimonio] 

###### [24 Oct 2016] 

Este 24 de Octubre, comunidades de las veredas Chitiva Alto y Bajo de Suesca-Cundinamarca, se reunieron con Nestor Franco director de la Corporación Autónoma Regional de Cundinamarca, para exigir respuestas frente a la resolución 2274, que le otorga licencia ambiental a Cementos Tequendama para hacer minería de cielo abierto.

El perímetro aprobado para **el socavón que dejaría esta empresa, es de 15 hectáreas y 30 metros de profundidad.** Las comunidades de este municipio han denunciado [“fuertes ruidos, ]**contaminación de aire y perdida de fuentes de agua que benefician a las comunidades de las veredas y del casco urbano”,** señaló Raúl Avellaneda, integrante del Colectivo Agua Pura para Suesca.[ ]

[Avellaneda señala que las actividades de esta empresa ya han iniciado, y comunidades como la de Chitiva Alto, “han puesto freno y no han permitido mayor avance de la explotación”. Sin embargo, **n**]**i el gobierno municipal ni departamental han atendido las exigencias de la comunidad.** ** **[Le puede interesar: Minería en][ Colombia ocupa 5 millones de hectáreas y la agricultura solo 4 millones.](https://archivo.contagioradio.com/mineria-en-colombia-ocupa-5-millones-de-hectareas-y-la-agricultura-solo-4-millones/)

Además, Avellaneda manifestó que esta licencia ambiental para extraer arenas silíceas, **“viola todas las obligaciones del Estado frente a la protección, corrección, recuperación y conservación** no solo del ambiente y de la Cuenca Alta del Río Bogotá, sino la vida digna de las comunidades”. Le puede interesar: [5 grandes daños de la](https://archivo.contagioradio.com/5-grandes-danos-de-la-mineria-a-gran-escala/)[[minería]][a gran escala.](https://archivo.contagioradio.com/5-grandes-danos-de-la-mineria-a-gran-escala/)

Avellaneda concluye, que en la reunión con la CAR, se acordó que el próximo 26 de octubre la entidad realizará una visita exhaustiva en la zona afectada, y que es fundamental para este y otros casos de explotación en el país **“el acompañamiento de organizaciones ambientales, defensoras de derechos humanos y la constante movilización social”**.

<iframe id="audio_13458292" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13458292_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
