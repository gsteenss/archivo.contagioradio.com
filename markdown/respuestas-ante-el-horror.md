Title: Respuestas ante el horror
Date: 2015-11-23 08:27
Category: Eleuterio, Opinion
Slug: respuestas-ante-el-horror
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/card_atentado_paris.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ipco] 

**[Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)**

###### [23 Nov 2015 ] 

Ante la oleada de comentarios, opiniones y noticias sobre los atentados del pasado fin de semana en París, el siempre polémico escritor Arturo Pérez Reverte alimentó las discusiones en las redes sociales con algunos tuits. Este otrora  reportero de guerra, cuestionó la capacidad de reacción de los centenares de personas que se vieron sorprendidos por el ataque de unos pocos terroristas armados. ¿Por qué no les enfrentaron si quiera cuando estos recargaban sus armas siendo muchos más en número? El mismo respondía a su pregunta aseverando que la capacidad para asimilar el horror en Occidente se ha perdido y que sus gentes a fuerza de vivir cómodamente alejados de las duras realidades que se viven en muchas otras partes del planeta, no saben reaccionar. Ese horror que ven a diario en sus televisiones les parece irreal cuando lo tienen enfrente pues se supone que ellos viven seguros y la barbarie no debe afectarles.

[Al margen de si se puede juzgar tan a la ligera la reacción de una persona que estando de fiesta se encuentra con alguien que le dispara con un kalashnikov, sí parece cierto que el llamado Occidente se empeña en obviar toda violencia que pueda perturbar su estado de bienestar, aun cuando ese bienestar se derive en gran medida en la tragedia y el sufrimiento de otras geografías.]

[El pasado verano las imágenes de miles de refugiados que huyendo de la guerra en Siria trataban de llegar a la desesperada a la acomodada Europa, tocó la fibra sensible de los europeos y pareció despertar algunas alarmas. Las imágenes eran bien diferentes de las tradicionales llegadas de pateras subsaharianas a las costas del Mediterráneo norte. Esta vez se trataba de familias de clase media, niños incluidos, que habían pagado grandes sumas de dinero para poder realizar un viaje que a muchos les costó la vida. Estas familias vienen huyendo de horrores como el que le tocó vivir a París el pasado viernes. Pero hasta ahora, nadie parecía darse cuenta de que Siria lleva más de cuatro años en guerra, con más de 200 mil muertos y oficialmente 4 millones de exiliados.]

[La guerra en Siria comenzó cuando, sumándose a la ola las revoluciones árabes, la población salió a las calles a exigir al gobierno mayor libertad de expresión y mejoras democráticas y sociales. El régimen respondió de forma atroz y la consecuencia ha sido el surgimiento de grupos atroces, como el Estado Islámico o Al Nusra. En el conflicto intervienen también los intereses de los dos grandes polos islámicos; Irán, que apoya al régimen y Arabia Saudí, que apoya a los islamistas. También Estados Unidos lleva meses bombardeando a conveniencia con más de 7 mil ataques en territorio sirio.]

[La población civil ha vivido, vive, un infierno desde entonces. De los 4 millones de refugiados sirios (según datos oficiales, en realidad la cifra es mucho mayor), Turquía ha acogido a más de dos millones, Líbano con 4 millones de habitantes tiene 1 millón de refugiados, algo menos de los que ha acogido Jordania donde viven 6 millones de personas. También el destruido Irak es uno de los destinos para centenares de miles de refugiados. Cabe decir que Siria era reconocida por sus vecinos como un país de acogida; libaneses, jordanos e iraquís han sido acogidos en Siria durante las guerras y conflictos que han sufrido en la última década. Hoy son los sirios quienes huyen en masa.]

[Tras las imágenes del verano en sus fronteras, en Europa, con una población en torno a 500 millones de personas y unos recursos infinitamente mayores que los de estos países, se ha levantado una enorme polémica sobre la conveniencia o no de aceptar la entrada de menos de 200 mil personas. La comunidad europea prefiere pagar a Turquía como hace en África con Marruecos o Camerún para que controlen los llamados “flujos migratorios”. Las incomodidades y por supuesto el horror, mejor si están lejos y no afectan el cotidiano bienestar. La receta que se parece aplicar para subsanar esta situación es echar más leña al fuego; Francia no ha tardado ni dos días en unirse al bombardeo en Siria.]

[Hace más de diez años, las manifestaciones en todo el continente contra la guerra en Irak fueron masivas y de una fuerza unánime. Parece que es momento de que la sociedad civil europea se replantee la responsabilidad que debe asumir en el actual estado de las cosas.  ]
