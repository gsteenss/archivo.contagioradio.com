Title: Los costos de los desastres naturales en 2016
Date: 2016-12-20 19:49
Category: Ambiente, El mundo
Tags: 2016, cambio climatico, desastres naturales
Slug: los-costos-los-desastres-naturales-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/ecuador-terremoto-e1482281323795.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: EFE/José Jácome 

###### [20 Dic 2016] 

Los efectos del cambio climático impulsados por las actividades humanas, como **terremotos, granizos, tormentas y huracanes**, cobraron este 2016 la vida de 10.000 personas y además costaron 158.000 millones de dólares, es decir, 62.000 millones de dólares más que en 2015.

Así lo afirma la reaseguradora Swiss Re, que además concluye que “la diferencia entre el total de pérdidas y los daños asegurados** muestran que los desastres tuvieron lugar en áreas en las que la cobertura de seguros era baja". **

El costo más preocupante es el social. Las afectaciones sobre la población de menores recursos aumenta los niveles de pobreza. Este año 26 millones de personas entraron en ese rango a causa de las catástrofes naturales. Así mismo, **23 millones de personas tuvieron que desplazarse pues hogares quedaron destruidos.**

Este año se registraron sismos en Taiwán, Japón, Ecuador, Italia y Nueva Zelanda. El peor terremoto** **fue de magnitud 7 en la escala Richter en Kumamoto. Ese tipo de desastres ocasionaron pérdidas por más de **20.000 millones de dólares de los cuales solo 5.000 millones** estaban asegurados.

Fenómenos como El huracán Matthews, generó daños por 8.000 millones de dólares. Haití, un país inmerso en la pobreza, fue la mayor víctima de ese desastre que causó la muerte de más de mil habitantes de la isla.

En Colombia este 2016 se vivió una de las peores sequías. A su vez, este año se contabilizaron más de 50 víctimas mortales por las inundaciones y deslizamientos ocasionados por las lluvias.

###### Reciba toda la información de Contagio Radio en [[su correo]
