Title: Trabajadores de la DIAN podrían ir a paro el 22 de agosto
Date: 2017-08-15 16:20
Category: Nacional
Tags: DIAN, SIndicatos
Slug: trabajadores-de-la-dian-podrian-ir-a-paro-el-23-de-agosto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/paro-de-dian.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Heraldo] 

###### [15 Ago 2017] 

**Trabajadores de la DIAN manifestaron que podrían ir a paro el próximo 22 de agosto**, si el gobierno nacional no cumple con los acuerdos que ya había pactado sobre el aumento de planta laboral, la realización de concursos para dar puestos vacantes y la transformación de incentivos a salarios, que permitirían que la acción que realiza esta entidad mejore drásticamente.

De acuerdo con Pedro Caro, presidente de SineDIAN, los incumplimientos radican principalmente en la falta de voluntad del Departamento Administrativo de la Función Pública, que no ha puesto en marcha los **1.5 millones de recursos que hacen parte del presupuesto designado para esta institución.** (Le puede interesar:["A paro sindicatos del INPEC"](https://archivo.contagioradio.com/guardias-no-permitiran-ingresos-de-mas-reclusos-a-las-carceles-hacinamiento-supero-el-limite/))

### **La Planta de la DIAN** 

En cuanto a la planta laboral, actualmente la DIAN funciona con 7 mil trabajadores y necesitaría de 15 mil más para que funcione acorde a las necesidades actuales que tiene el país, sin embargo, **debido a la falta de expedición de un decreto que formalizaría más de 1.800 trabajos, esta ampliación no se ha podido realizar**.

### **El salario de los trabajadores de la DIAN** 

Sobre el salario Pedro Caro aseguró que desde hace más de **20 años vienen recibiendo incentivos que no constituyen salarios**, evadiendo prestaciones sociales a los trabajadores. Para que esta situación se solvente debería expedirse un decreto que amplíe la asignación básica mensual, que no tendría un costo fiscal.

### **Concursos en la DIAN** 

Finalmente, los trabajadores de la DIAN están solicitando que se genere un concurso para las vacantes que hay al interior de la institución con **la finalidad de que se mejore la atención y rapidez en las actividades que se desarrollan**.

Para Caro, de realizar estas mejoras se podría combatir y dar más resultados en la lucha contra la corrupción, contrabando y evasión de impuestos “cumplirle a los trabajadores de la DIAN **es cumplirle al país, buscando mayores recursos tamponando esa evasión tan grotesca que en Colombia rodea el 30%**”.

<iframe id="audio_20354514" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20354514_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
