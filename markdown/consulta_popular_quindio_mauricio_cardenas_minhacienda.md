Title: Sancionan a Minhacienda por impedir consulta popular en Córdoba, Quindío
Date: 2018-01-30 15:42
Category: Ambiente, Nacional
Tags: consulta popular, cordoba, Mauricio Cárdenas, Quindío
Slug: consulta_popular_quindio_mauricio_cardenas_minhacienda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/CARDENAS-e1517338732686.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

###### [30 Ene 2018] 

Un fallo del **Tribunal Administrativo del Quindío sancionó al Ministro de Hacienda, Mauricio Cárdenas**, por desacatar la orden de tramitar los recursos necesarios para desarrollar la consulta popular en el municipio de Córdoba, Quindío.

Se trataba de una acción de tutela que había interpuesto la Alcaldía de Córdoba de la mano de la comunidad, logrando que fuera amparado el derecho a la participación ciudadana. Ahora, la comunidad está a la espera de que se tramiten los **cerca de 32 millones de pesos necesarios para llevar a cabo la consulta popular.**

"Aunque es una sanción irrisoria, no deja de ser importante, porque ninguno de los argumentos presentados por la defensa de Mauricio Cárdenas fueron aceptados por el tribunal", explica Carlos Gómez, vocero del Comité por el NO en la consulta.

Por el momento, la sanción impuesta fue de un salario mínimo, pero de seguir desacatando los fallos, este funcionario público podría pagar hasta 6 meses de prisión, además de una multa entre 5 y 50 salarios mínimos legales vigentes. (Le puede interesar: [Por segunda vez cancelan consulta en Córdoba)](https://archivo.contagioradio.com/consulta_popular_suspendida_cordoba_quindio/)

Ante esta decisión, la comunidad de Córdoba, espera que esta vez el ministro sí cumpla, y se encuentra a la espera de la comunicación de la Registraduría, sobre la nueva fecha que se establezca para llevar a cabo **la consulta popular con la que buscan ponerle freno a los 48 títulos mineros que se han otorgado en esa zona.**

### **Comunidad pide que la consulta quede para el 11 de marzo** 

Carlos Gómez, señala que con el fin de proteger el erario público, y teniendo en cuenta que este 11 de marzo se llevará a cabo elecciones a congreso, pero también las consultas interpartidista**s, esperan que se abra la oportunidad de que la consulta popular también se realice ese día de manera que solo haga falta agregar un tarjetón y **no se gasten recursos públicos. Esto como una alternativa ante las afirmaciones del Ministerio de Hacienda sobre la falta de dineros para la realización de las consultas.

"Estamos apelando al derecho a la igualdad, por el boquete que se abre a nivel jurídico al acoger las consultas interpartidistas lo que abre la posibilidad a que también se desarrolle la consulta popular en esa fecha. Además se debe tener ne cuenta el principio de autoridad", dice Gómez.

Cabe recordar que la consulta en Córdoba ha sido suspendida en dos ocasiones, debido a que Mauricio Cárdenas, ha dicho que al gobierno no le corresponde costear dicho mecanismo de participación ciudadana, sino que se trata de una responsabilidad que tiene cada una de las entidades territoriales.

<iframe id="audio_23476823" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23476823_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
