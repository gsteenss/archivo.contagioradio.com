Title: Asesinan a la lideresa indígena Magdalena Cucubana en Arauca
Date: 2019-09-05 16:28
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Arauca, Asesinato de líderes indígenas, ONIC, Tame
Slug: asesinan-a-la-lideresa-indigena-magdalena-cucubana-en-arauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Magdalena-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

La madrugada del 3 de septiembre en Tame, Arauca, fue asesinada Magdalena Cucubana, cacica y lideresa indígena Makaguan, denunció la Asociación de Capitanías y Autoridades Tradicionales de Arauca (ASOCATA) y la Asociación de Cabildos y Autoridades Tradicionales Indígenas del Departamento de Arauca (ASCATIDAR).

Según el pronunciamiento de la **Organización Nacional Indígena de Colombia (ONIC)**, Magdalena, de 70 años, quien fue fundadora del Movimiento Alternativo Indígena y Social, habría sido víctima de degollamiento en el barrio 20 de Julio del municipio de Tame. [(Le puede interesar: Asesinan al gestor cultural y audiovisual, Mauricio Lezama en Arauca)](https://archivo.contagioradio.com/asesinan-al-gestor-cultural-y-audiovisual-mauricio-lezama-en-arauca/)

### Con la muerte de Magdalena Cucubana se va una parte de su saber ancestral

Magdalena, además de ser dirigente era conocida **por su amplio conocimiento histórico y cultural de las tradiciones de los macarieros,** comunidad indígena de la que hacía parte. Se destacó también por su lucha al exigir garantías de los derechos de las comunidades indígenas más vulnerables del municipio.

La comunidades indígenas le exigen al Gobierno que cumpla la implementación del plan de salvaguarda de los pueblos indígenas del Departamento de Arauca y a las autoridades que avancen en la investigación para dar con los responsables del asesinato de la líder ancestral. [(Le puede interesar: Asesinan al rector indígena Aquileo Mecheche en Río Sucio, Chocó)](https://archivo.contagioradio.com/asesinan-aquileo-mecheche-rio-sucio/)

Noticia en desarrollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
