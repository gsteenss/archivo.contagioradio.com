Title: ¿Polarizar o no polarizar Dr. Wasserman?
Date: 2019-01-19 07:24
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: polarización, politica, redes sociales, twitter
Slug: polarizar-o-no-polarizar-dr-wasserman
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/polarizar-o-no-polarizar-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 16 Ene 2019 

El movimiento a favor de la polarización no es tan furioso como lo presentan, pero sí es un movimiento en tanto que, como aseguraba Alain Touraine, se enfila no solo ideológicamente sino en la práctica contra el poder y el estado de control. ¿Control de qué? Control ideológico de la tradición del pensamiento político liberal. ¿Cuál poder? Ese que se expresa en términos de los mecanismos que posterior al final de la guerra fría (aquel fenómeno alegórico de la polarización más prominente) colocaron en el trono de la “objetividad”, el “neutralismo”, o el “a-politicismo” una ideología tan definida como el liberalismo.

Mouffe incluso antes de formular el agonismo, solicita superar la visión de ciudadanía que nos presentó la filosofía política liberal. Dicha visión se sostiene aún como la filosofía política del Estado colombiano, es la que genera institucionalistas radicales como los derechistas, o fanáticos civilistas como los llamados nini, auto declarados centro.

Por supuesto, concuerdo con la idea de que la mayor falacia a la hora de hablar sobre política consiste en poner en boca del contrario algo que no dijo, para luego refutarlo. Asimismo, la disposición psicológica para una discusión en un plano específico como es el de las redes sociales, definitivamente desarrolla un conflicto destructivo, que ocupa casi siempre la base de la pirámide Graham en la que el insulto o el ataque ad hominem marcan la forma de tratar al otro.

No obstante abogar por la polarización en redes es un fenómeno sustancialmente diferente al de abogar por la polarización en el discurso y la vida políticos, del mismo modo que abogar contra la polarización en redes, es totalmente diferente de abogar contra la polarización en el discurso y la vida políticos, en este caso, la deliberación se distanciaría del rifirrafe y el contexto determinaría la condición para resolver en buen término la cuestión.

Si en redes sociales no hay argumentos, no podemos esperar que aparezcan y menos exigirlos sin comprender la sociología del contexto, por ello, en la única cosa en la que estoy de acuerdo con Claudia Gurisatti es que las redes sociales son una anarquía (en su significado tácito y no filosófico) entonces… ¿polarizar o no polarizar? Si es para insultarnos en redes, no hace falta confundir vulgaridad con polarización, no se puede dar ese salto porque pondríamos en función de un concepto una significación equivocada, es decir, la vulgaridad es producto de la incapacidad para debatir, pero no es producto de la polarización.

Pero si es para definir una identidad política que lucha en la práctica contra mecanismos de poder o contra un estado de control, entonces la polarización, es la reivindicación de las partes, es la certeza de la existencia de un fin político comunitario, es una reivindicación que constitutivamente no está orientada a dañar al otro, pero orgánicamente no está orientada a buscar puntos medios en la búsqueda de su objetivo político, pues para quienes lucha contra el poder en Colombia, la política en ocasiones es una cuestión de vida o muerte.

Creo yo, para cerrar esta respuesta que puede o no ser leída, que ir en contra de la polarización es una ingenuidad. Pero no esa ingenuidad que es atributo de la docilidad, y con la cual se viene configurando el insulto “tibio”, sino más bien es una ingenuidad debido a la inconsciencia de la afirmación, me explico: la famosa crisis de las ideologías, como lo diría Bobbio, es el discurso más ideológico de todos. Este lleva avanzando en todos los campos sociales desde el fin del siglo XX, y ha sido impulsado por el liberalismo o neoliberalismo en el XXI. Ese nuevo liberalismo, es en sí mismo un modelo político-económico, pero es también una ideología. Lo que sucede es que por el hecho de que se base en conceptos tales como la liberad, el individuo y sea discursivamente “anti-política” no le exime de ser una tendencia absolutamente clara y antitética de otras existentes, por lo tanto: un polo.

Lo que sucede es que la tendencia despolarizadora, se ha situado en lo que Bobbio definiría como un Ni Ni, es decir ni izquierda, ni derecha, se ha dedicado a negarlas. Pero no ha avanzado hacia un Et Et, es decir, reconocer que izquierda y derecha tienen algo que decirnos para componer una síntesis sin negarlas. Por tanto, paradójicamente la tendencia despolarizadora (ni ni) se ha constituido en un polo, un polo que sí es bastante furioso contra todo lo que suene a izquierda o derecha, atribuyéndose equivocadamente, como es característico en la ideología liberal, un manto falaz de neutralidad sustentado en una visión habermasiana (consenso) de la política, que es respetable, pero opuesta a una visión por ejemplo Laclauniana (conflicto) de la política.

Es decir, incluso desde un punto teórico en que se sustentarían los despolarizadores, nuevamente se da en la práctica, una regresión sobre la naturaleza de lo político: el antagonismo.

Por esa tendencia, producto de la ideología liberal, de presentar a los despolarizadores como neutrales, es que quienes apoyamos la polarización somos vistos como pelietas o buscabullas en las redes sociales. No obstante, en la práctica, en la acción política vivida de la mano de diferentes comunidades, **la polarización se constituye en el gran mecanismo equivalente entre tantas diferencias y particularidades de los grupos sociales oprimidos que unifica la lucha contra el poder…** ¿polarizar o no polarizar? Esa es la cuestión.

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
