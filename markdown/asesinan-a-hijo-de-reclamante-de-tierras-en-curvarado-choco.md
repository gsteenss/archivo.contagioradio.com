Title: Asesinan a hijo de reclamante de tierras en Curvaradó, Chocó
Date: 2017-06-01 15:26
Category: DDHH, Movilización
Tags: Asesinatos, Chocó, Curvarado, paramilitares, Reclamante de tierras
Slug: asesinan-a-hijo-de-reclamante-de-tierras-en-curvarado-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/curvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de Justicia y Paz] 

###### [01 Jun. 2017]

Cuatro hombres vestidos de civil que portaban armas de fuego y  se movilizaban en dos motos, **asesinaron al joven Duberney Gómez, es hijo del reclamante de tierras Rafael Taquero**. Los hechos sucedieron el 31 de mayo hacia las 11 de la mañana en la comunidad de La Nevera, territorio colectivo de Curvaradó en el departamento de Chocó.

Según la denuncia dada a conocer por la Comisión de Justicia y Paz, los hombres se bajaron de sus motos y preguntaron por Duberney, quien atendió el llamado, pero posteriormente se regresó a su vivienda siendo perseguido por uno de los hombres armados. Le puede interesar: [Enfrentamientos entre paramilitares y ELN desplazan 300 familias en Chocó](https://archivo.contagioradio.com/desplazamiento-choco-enfrentamientos/)

“Uno de los armados le siguió y sacó a Duberney apuntándole con el arma, obligándole a subir a una motocicleta”. Según la información, **Duberney fue hallado en la vía que conduce hacia El Cerrado** a 1 kilómetro de Llano Rico minutos después con varios impactos de arma de fuego.

En mayo de 2012 Fanny Jhoana Truaquero Gómez, **hija del reclamante de tierras y hermana de Duberney, también fue asesinada por paramilitares** autodenominados Autodefensas Gaitanistas de Colombia. Rafael Truaquero ha sido objeto de múltiples amenazas. Le puede interesar: [Atacan a William Aljure, reclamante de tierras de Mapiripan](https://archivo.contagioradio.com/atacan-william-aljure-reclamante-tierras-meta/)

Además, según la Comisión de Justicia y Paz el lunes 29 de mayo en la entrada de la comunidad de Cetino, territorio colectivo de Curvaradó **fue asesinada una persona que hasta el momento no ha sido identificada.**

### **Con lista en mano paramilitares asesinan y controlan la región** 

Según denuncias de la comunidad recibidas por la Comisión de Justicia y Paz, **desde hace 2 meses se anunció por parte de los paramilitares autodenominados Autodefensas Gaitanistas de Colombia**, que asesinarían a varios líderes y lideresas y a personas que “consideran blanco por ser ladrones, trabajadores sexuales y consumidores”. Le puede interesar: [En Bajo Atrato paramilitares reclutan jóvenes para control social](https://archivo.contagioradio.com/paramilitares-con-armas-cortas-controlan-territorios-del-choco/)

En el listado se conocieron los nombres de personas como Manuel Denis Blandón, Elkín Romaña, Romualdo Salcedo, Erasmo Sierra, Benjamín Sierra, Argemiro Bailarín, Ligia Chaverra, Eustaquio Polo, Eleodoro Polo, Yomaira González y James Ruíz.

Según la ONG **hasta el momento no han recibido respuesta por parte del alto gobierno** pese a conocer de la situación y de los compromisos asumidos con las comunidades. Le puede interesar: [Paramilitares se toman caserío de Domingodó en el Chocó](https://archivo.contagioradio.com/paramilitares-se-toman-caserio-de-domingodo-en-el-choco/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
