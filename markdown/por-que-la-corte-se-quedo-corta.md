Title: ¿Por qué La Corte se quedó corta?
Date: 2015-11-19 10:02
Category: Opinion, Tatianna
Tags: Adopción homoparental, Convenio 33 de La Haya, Corte Constitucional, homosexuales, LGBTI, matrimonio igualitario
Slug: por-que-la-corte-se-quedo-corta
Status: published

##### Foto:Contagio Radio 

#### **Por [Tatianna Riveros](https://archivo.contagioradio.com/opinion/tatianna-riveros/)- [@[tatiannarive]

###### [19 Nov 2015] 

En la época del auge del derecho la igualdad, de la no discriminación, de la diversidad de género y de la inclusión, suele confundirse una mente abierta con no respetar la opinión del otro, y salen argumentos que comprometen desde la iglesia con cuestiones naturales y hasta genéticas que harían que Darwin se sentara a llorar.

Hace un tiempo, era casi impensable ver un hombre besándose con un hombre, y pongo el ejemplo de parejas homosexuales masculinas porque para muchos, dos mujeres besándose son incluso sinónimo de excitación. En la actualidad, países alrededor del mundo han aprobado el matrimonio para parejas del mismo sexo, para no ir muy lejos, Argentina, Chile, Brasil, México, Uruguay, Estados Unidos, y para ir más lejos Bélgica, Canadá, Dinamarca, Eslovenia, España, Francia, Irlanda, Nueva Zelanda, Países Bajos, Portugal, Sudáfrica entre otros, permiten casarse a las parejas del mismo sexo; pero para el caso de Colombia, la Corte dio sólo un pincelazo en el afán de reconocer este derecho a personas del mismo sexo: no está reconocido el matrimonio como institución jurídica, y no cambia el estado civil de la persona, como en el caso de una pareja heterosexual.

Así pues, La Corte se limitó a decir que “las parejas del mismo sexo podrán acudir ante notario o juez competente a formalizar y solemnizar su vínculo contractual”, un vínculo que a la luz de muchos tiene diversas interpretaciones, y más o menos lo mismo hizo con la adopción para parejas del mismo sexo, es decir, les da la opción a parejas homosexuales de poder adoptar cumpliendo con los mismos requisitos que cumpliría una persona o una pareja heterosexual, y la orden de no ser discriminados por su inclinación, a la hora de iniciar el proceso.

¿Alguno ha leído los requisitos para poder adoptar en Colombia? ¿Una pareja homosexual en calidad de extranjero puede adoptar en Colombia? Para el primer interrogante, no, a muy pocos nos ha interesado o nos interesa el tema, y los requisitos van desde la solvencia económica, examen psicológico y la que me parece más difícil, la idoneidad MORAL para poder adoptar, para el segundo, me queda la duda, si para las parejas heterosexuales hay una distinción si el país pertenece o no al Convenio 33 de La Haya, imaginen si son parejas homosexuales extranjeras.

Y es que para hablar de la adopción de parejas homosexuales hay que dejar de lado el argumento de la iglesia, que es contra natura, también el argumento de los curas pederastas, la conformación de una familia por madre y padre, o de los padres heterosexuales que abandonan o maltratan a sus hijos; todos y cada uno de esos argumentos nos hacen caer en la generalización y en prejuicios errados: no todos los curas son pederastas, no todos los padres heterosexuales maltratan o abandonan a sus hijos, los homosexuales también abandonan niños (cuando cambia su inclinación sexual por ejemplo), y no todas las familias están compuestas por mamás y papás. Ya hay niños que tienen dos mamás o dos papás, o niños que ven en otras figuras la conformación de una familia.

De la lista de países que contemplan en su regulación el matrimonio para parejas homosexuales, Argentina, Francia (que está de moda por estos días) y Brasil (por nombrar algunos), permiten la adopción homoparental, y a lo que voy con este escrito, es que La Corte se quedó corta porque se le olvidó terminar de regular el matrimonio, que a mi parecer, estaba primero (como en los demás países, que regularon los dos al tiempo, o primero el matrimonio) , porque así como no todas las parejas heterosexuales y/o homosexuales se quieren casar, no todas las parejas homosexuales, ahora que tienen el derecho van a querer adoptar.

En conclusión, la decisión del otro, su elección, su inclinación sexual, su percepción de familia, que puede ser la misma o variar, debe respetarse, no se debe quedar en Love Wins, o en un plantón frente a la Procuraduría, no es sólo un hashtag o pintar de colores el avatar, tampoco es obligación a que piensen igual, pues habrán muchos argumentos válidos, siempre y cuando no atenten contra la libertad del otro.
