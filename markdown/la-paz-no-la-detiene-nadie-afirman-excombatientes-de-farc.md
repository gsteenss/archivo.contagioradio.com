Title: La paz no la detiene nadie, afirman excombatientes de FARC
Date: 2019-08-30 12:03
Author: CtgAdm
Category: Comunidad, Entrevistas
Tags: etcr, FARC, Implementación del Acuerdo
Slug: la-paz-no-la-detiene-nadie-afirman-excombatientes-de-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Excombatientes-e1567187945860.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

La madrugada de este 29 de agosto, el exjefe negociador de La Habana, Iván Márquez anunció su regreso a las armas para conformar un nuevo grupo armado producto de lo que denominó “la traición al Acuerdo de Paz”. Como respuesta, Rodrigo Londoño, líder del partido político FARC, reiteró su compromiso con el Acuerdo y manifestó que más del 90% de los excombatientes continúan su proceso de reincorporación.

Carolina Vargas, representante de Mujer y Género del ETCR de Tierragrata en Manaure, La Guajira se refirió al anuncio realizado por Iván Márquez, afirmando que ante la falta de cumplimiento del Acuerdo era de esperar que se presentase esta situación, sin embargo, expresa que quienes viven en el ETCR tienen "los ánimos firmes y una clara convicción de seguir luchando por esta paz y alcanzar los objetivos que se firmaron".

"Cada quien responde por sus decisiones y acciones personales, nosotros somos un colectivo y seguimos firmes hacia adelante, sabíamos que esta no era una lucha fácil" afirma Vargas agregando que la postura de los jefes guerrilleros que se hicieron a un lado del acuerdo no significa que los excombatientes vayan a declinar nuestra voluntad.

> **"Estamos convencidos que hacia atrás no nos vamos a ir, seguiremos luchando en unidad porque la paz no la detiene nadie, hay que dejar a un lado el rencor y darle paso a la reconciliación y al amor". **

### "Excombatientes trabajamos junto a las comunidades"

Fruto de este diálogo, expresa Carolina han surgido varias propuestas de trabajo en conjunto con las comunidades de El Mirador, destacando en particular el proyecto que busca llevar agua hasta Tierra Grata, beneficiando no solo al espacio territorial sino también a las poblaciones aledañas.

"Hemos realizado cerca de 565 jornales colectivos y la comunidad ha hecho 264 jornales, es un trabajo en conjunto  que estamos haciendo con muchísimo ánimo y amor" afirma. [(También puede leer: «El Acuerdo es mucho más grande que unas personas que se apartan»)](https://archivo.contagioradio.com/acuerdo-grande-personas-apartan/)

De cara a las elecciones de octubre, la representante de género explica que en Manaure han establecido una lista cerrada por el Partido para participar en el consejo de este municipio, lo que ha puesto en evidencia el apoyo y aceptación de las comunidades, **"tenemos una claridad política, estamos en unidad con todo el partido FARC a nivel nacional, desarrollando tareas políticas y de campaña"**.

Carolina Vargas concluye que este proceso de paz no solo involucra a FARC o al Gobierno sino que es un trabajo de la sociedad en general  "y cada uno debe poner lo mejor de sí para que Colombia finalmente viva en paz". [(Lea también: Información entregada a Unidad de Búsqueda por FARC esclarecería paradero de 276 personas)](https://archivo.contagioradio.com/informacion-entregada-a-unidad-de-busqueda-por-farc-esclareceria-paradero-de-276-personas/)

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_40609067" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_40609067_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
