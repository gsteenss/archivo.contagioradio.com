Title: Milena Quiroz fue condenada al “destierro” abogados de la defensa
Date: 2017-03-31 08:43
Category: DDHH, Nacional
Tags: congreso de los pueblos, Milena Quiroz
Slug: milena-quiroz-fue-condenada-al-destierro-abogados-de-la-defensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/whatsapp_image_2017-03-27_at_12.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [31 Mar 2017] 

Luego de varias horas de audiencia en la que la Fiscalía pretendía imputar **medida de aseguramiento con privación de la libertad en establecimiento carcelario contra Milena Quiroz**, basada en argumentos como que se dedicaba a “organizar marchas” la juez decidió resolver medida de aseguramiento con casa por cárcel pero fuera de la región del Sur de Bolívar e imputar el delito de rebelión.

Para la defensa de la acusada esta medida representa un “destierro” dado que no se permitirá que ella viva en su propia casa, con sus familiares, sino que la obliga a salir del Sur de Bolívar. Además se le imputa el delito de rebelión con argumentos que señalan como **“rebeldes” a todos los y las integrantes de organizaciones sociales que ayuden en la organización de sus comunidades. **Le puede interesar: ["Fiscalía no tiene argumentos para mantener en presión a Líderes sociales del Sur de Bolívar"](https://archivo.contagioradio.com/fiscalia-no-tiene-argumentos-para-mantener-en-prision-a-lideres-sociales-del-sur-de-bolivar-defensa/)

Hoy siguen las diligencias para resolver los casos de **Isidro Alarcón y Francisco Zabaleta** y aunque no se conocen aún los argumentos de la Fiscalía en esos procesos se espera que la medida sea similar o incluso se les conceda la libertad puesto el liderazgo de Milena, fue, al parecer, más relevante para la fiscal. Le puede interesar:["Capturas de líderes de Congreso de los Pueblos podrían declararse ilegales"](https://archivo.contagioradio.com/capturas-de-lideres-sociales-del-congreso-de-los-pueblos-podrian-declararse-ilegales/)

###### Reciba toda la información de Contagio Radio en [[su correo]
