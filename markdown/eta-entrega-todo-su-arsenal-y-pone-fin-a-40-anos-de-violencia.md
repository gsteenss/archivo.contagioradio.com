Title: ETA entrega todo su arsenal y pone fin a 40 años de violencia en España
Date: 2017-04-08 18:21
Category: datos, El mundo
Tags: Conflicto armado España, ETA
Slug: eta-entrega-todo-su-arsenal-y-pone-fin-a-40-anos-de-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/eta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Español] 

###### [08 Abr 2017] 

De acuerdo con la organización armada ETA, **hoy finaliza su proceso de desarme con la entrega de 118 pistolas, rifles, armas automáticas, explosivo**s. Este acto hace parte del proceso que había anunciado esta organización desde el año 2011 de poner fin a un capítulo de 40 años de guerra en España.

**La entrega de armas contó con la participación de la Comisión Internacional de Verificación y las autoridades Francesas** y se realizó en una ceremonia en la ciudad de Bayona, al sur de Francia. De esta manera y como ETA lo había anunciado el pasado 7 de abril, ahora son una organización “completamente desarmada”. El paso siguiente es la investigación por parte de la CIV al material armamentístico entregado.

Diferentes partidos españoles han calificado este hecho como un acto histórico para el avance y la construcción de una democracia diferente al interior del país. De acuerdo con Andeka Larrea, integrante de la dirección de Podemos Euskadi el desarme de ETA llega tarde, **“no es justamente el que se quería, pero es lo que tenemos” y añadió que se debe continuar con el proceso de reconocimiento, justicia y reparación, para las víctimas** de esta organización.

**ETA fue fundada en el año de 1958, durante la dictadura Franquista**. En el año 2011 la organización anuncio el fin de la lucha armada, sin embargo en esos 40 años de conflicto armado, las autoridades les atribuyen más de 800 muertes.

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
