Title: Se desconoce el paradero de presos políticos del ELN en Cárcel de Bellavista, Medellín
Date: 2019-01-20 12:53
Author: AdminContagio
Category: DDHH, Nacional
Tags: ELN, Iván Duque, presos politicos
Slug: prisioneros-politicos-del-eln-se-encuentran-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/carcel-colombia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  cronistadiario] 

###### [20 Ene 2018] 

El Comité de Solidaridad de Prisioneros Políticos denunció que tras la decisión del presidente Iván Duque de cerrar la mesa de diálogos con el ELN en Cuba, el 19 de enero se desarrolló un operativo en la zona técnica del Establecimiento de Mediana Seguridad y Carcelario de Medellín – Bellavista, **en donde se encontraban presos políticos de esa guerrilla de quienes actualmente se desconoce su paradero. **

En total son seis los prisioneros políticos de los que aún no se tiene información, luego de que un comando **del INPEC entrara en las instalaciones de la zona técnic**a, para desmantelar el espacio que se había habilitado desde finales del gobierno de Uribe con la finalidad de generar un canal de comunicación entre el gobierno nacional y la dirigencia del ELN, y así avanzar en la construcción de una agenda de negociación que condujera a la solución política al conflicto armado con esa guerrilla.

Además, en ese escenario se encontraba trabajando el Colectivo Camilo Torres Restrepo, bajo el liderazgo de Juan Carlos Cuellar y con el aval del Alto Comisionado de Paz, razón por la cual el Comité manifestó que exige las garantías de los derechos fundamentales de quienes integran ese grupo de trabajo. (Le puede interesar:["Romper conversaciones con el ELN implica un nuevo ciclo de violencia")](https://archivo.contagioradio.com/romper-conversaciones-con-eln-implica-un-nuevo-ciclo-de-violencia/)

De igual forma la organización defensora de derechos humanos señaló que ve con gran preocupación los actos de provocación por parte del gobierno nacional que alejan cada vez más de una salida negociada al conflicto.

###### Reciba toda la información de Contagio Radio en [[su correo]
