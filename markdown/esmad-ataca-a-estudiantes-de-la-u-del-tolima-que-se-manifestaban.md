Title: ESMAD ataca a estudiantes de la U del Tolima
Date: 2016-06-09 17:23
Category: Movilización, Nacional
Tags: ESMAD en Universidad del Tolima, Minga Nacional, universidad del tolima
Slug: esmad-ataca-a-estudiantes-de-la-u-del-tolima-que-se-manifestaban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/fotos-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotos: Marcela Cruz ] 

###### [9 Junio 2016 ] 

Sobre las 4 de la tarde, 200 estudiantes de la Universidad del Tolima que realizaban un pupitrazo en apoyo a la Minga Nacional y para denunciar la crisis estructural de la institución, fueron agredidos por el ESMAD con chorros de agua. En este momento los **efectivos permanecen fuertemente armados a las afueras de la Universidad**, los estudiantes temen por su integridad, pues no se ha hecho presente ningún organismo defensor de derechos humanos.

[![fotos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/fotos.jpg){.aligncenter .size-full .wp-image-25235 width="1280" height="720"}](https://archivo.contagioradio.com/esmad-ataca-a-estudiantes-de-la-u-del-tolima-que-se-manifestaban/fotos-2/)

Noticia en desarrollo...

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
