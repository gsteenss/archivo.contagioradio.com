Title: Nuevamente el ESMAD olvida que tiene que cumplir protocolos de intervención
Date: 2019-09-25 17:15
Author: CtgAdm
Category: DDHH, Nacional
Tags: #Niunamenos, Distrital, ESMAD, universidad
Slug: esmad-olvida-que-tiene-protocolos-intervencion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Imagen-Javeriana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @mar97angie  
] 

El pasado martes 24 de septiembre, **el Escuadrón Móvil Antidisturbios (ESMAD) atacó a estudiantes y personas que se encontraban en la Universidad Javeriana de Bogotá** en el sector cercano a la carrera Séptima, afectando a más de 20 personas. Según defensores de derechos humanos la intervención no fue justificada, se presentaron hechos irregulares y hubo violación de protocolos internacionales de actuación por parte de la fuerza pública.

### **¿Por qué estaban protestando los estudiantes?** 

Desde el pasado lunes 23 de septiembre, estudiantes de la Universidad Distrital acordaron la realización de un plantón en la sede de la calle 40 con carrera 7ma, para manifestarse contra los hechos de corrupción que se han denunciado en la institución. Según la investigación adelantada por la Procuraduría, se trataría del presunto uso de más de 10 mil millones de pesos de la Universidad para compras privadas de William Muñoz Prieto, director del Instituto de Extensión. (Le puede interesar:["Corrupción en la Universidad Distrital"](https://archivo.contagioradio.com/corrupcion-universidad-distrital/))

Según **Valentina Ávila, integrante de la Red Distrital Universitaria de Derechos Humanos y estudiante de la Universidad Javeriana,** una de las victorias del movimiento estudiantil que se formó el año pasado es las y los estudiantes (de universidades públicas y privadas) sean conscientes de la importancia de proteger los recursos que entran a las instituciones públicas. Por eso, algunos estudiantes de la Javeriana se unieron al plantón propuesto frente a la sede de ingenierías de la Universidad Distrital.

### **Una intervención del ESMAD violatoria de los protocolos internacionales** 

Según relató Ávila, mientras los estudiantes realizaban el plantón sin bloquear el flujo vehicular por la carrera 7ma, el ESMAD comenzó a dispersar la manifestación y encerrar a los estudiantes de la Universidad Distrital. Posteriormente, agredió a estudiantes de la Universidad Javeriana, pasando del otro lado de la Carrera 7ma y llegando incluso a ingresar al campus de la Institución. (Le puede interesar: ["Escuadrón Móvil agrede a campesinos que se oponen a multinacional petrolera en Caquetá"](https://archivo.contagioradio.com/esmad-agrede-a-campesinos-que-se-oponen-a-multinacional-petrolera-en-caqueta/))

Todo esto, "sin una previa presencia de  fuerza disponible, sin el cumplimiento de los previos protocolos que tienen que cumplir para la intervención mediante el uso de la fuerza y sin previa interlocución con el ministerio público". Según continuó explicando Ávila, **los códigos de conducta que debe seguir el Escuadrón se incumplieron a lo largo de su actuación porque no se presentaron hechos que alterarán el orden público** (como la interrupción de vías), por lo que no habría una justificación de la presencia de los mismos en el lugar.

Luego de dispersar la protesta frente a la Universidad Distrital los estudiantes denunciaron que el ESMAD incluso ingresó al campus de la Universidad Javeriana con pistolas marcadoras, disparando balas de goma contra los estudiantes, al tiempo que seguían lanzando gases al interior del lugar. Ávila resaltó que llegaron a la biblioteca de la institución, mientras los gases afectaron a quienes se encontraban en el Hospital San Ignacio que, a su vez, está al interior del recinto universitario.

Ello constituye "una violación a protocolos internacionales" que protegen este tipo de espacios de acciones por parte de actores armados, afirmó la defensora de derechos humanos; y aseguró que **tras la intervención se contabilizaron entre 20 y 25 personas heridas, aunque pudieron ser más porque solo se registraron aquellas que pudieron ser atendidas en el lugar**, y 9 detenidos. (Le puede interesar:["Escuadrón Móvil agrede a comunidades que se habían acogido a sustitución de cultivos de uso ilícito"](https://archivo.contagioradio.com/esmad-agrede-a-comunidades-que-se-habian-acogido-a-sustitucion-de-cultivos-de-uso-ilicito/))

**¿Los uniformados intentaron sobornar a algunos de los detenidos?**

Ávila declaró que "ninguna de las detenciones fue legal, no tuvo previa justificación" y con 3 de los capturados, que serían llevados al Centro de Traslado por Protección (CTP, antes conocido como UPJ), se los habría intentado sobornar para dejarlos en libertad. Esta información fue recolectada por la Red Universitaria Distrital de Derechos Humanos, gracias a información de amigos de los capturados.

Ante los hechos ocurridos durante el martes, estudiantes de la Javeriana realizaron un plantón este miércoles en las instalaciones de la Universidad, y nuevamente, los estudiantes denunciaron a través de redes sociales, que el ESMAD actuó con fuerza contra ellos. (Le puede interesar: ["Campesino fue víctima de la brutalidad policial del Escuadrón Móvil en el Cauca"](https://archivo.contagioradio.com/campesino-fue-victima-de-la-brutalidad-policial-del-esmad-en-el-cauca/))

> [@EnriquePenalosa](https://twitter.com/EnriquePenalosa?ref_src=twsrc%5Etfw) señor alcalde me siento desconcertada por la actitud del ESMAD con los estudiantes de la universidad Javeriana , como madre de dos de ellos dejo mi protesta por tanda difícil situación . [pic.twitter.com/YJeUbSTtZV](https://t.co/YJeUbSTtZV)
>
> — martha quintero (@mairisquintero) [September 25, 2019](https://twitter.com/mairisquintero/status/1176949046235324419?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
> Comunicado de la Pontificia Universidad Javeriana [pic.twitter.com/KxZhMbawKE](https://t.co/KxZhMbawKE)
>
> — Pontificia Universidad Javeriana (@UniJaveriana) [September 25, 2019](https://twitter.com/UniJaveriana/status/1176862305683935232?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
