Title: ETB, una empresa generadora de riqueza que hay que saber administrar
Date: 2016-05-03 13:30
Category: Economía, Nacional
Tags: ETB, Sintrateléfonos, Venta ETB
Slug: etb-generadora-de-riqueza-que-hay-que-saber-administrar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/etb-sindicato.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: confidencialcolombia] 

###### [3 Mayo 2016 ]

El sindicato de la ETB se declaró en asamblea permanente en desacuerdo con la anunciada venta de esta empresa, que pese a los errores administrativos, cuenta con tecnología de punta, tiene **fibra óptica en un millón trescientos mil hogares de Bogotá e Internet de 150 megas**, según afirma William Sierra presidente de Sintratelefonos.

Además de las ventajas tecnológicas de la empresa, Sierra insiste en su importancia para la inversión social de la ciudad. El último aporte que la ETB hizo a la Universidad Distrital fue de **\$7 mil millones, se han logrado instalar portales interactivos gratuitos y 74 puntos de Wi-Fi gratis**. En la actualidad están en marcha otros proyectos que permitirían ampliar la cobertura de Internet para los ciudadanos.

Sierra asegura que la ETB no necesita un socio estratégico, ni ser vendida a privados, "hay que hacerla más rentable y competitiva en el corto, mediano y largo plazo" puesto que "es una generadora de riqueza que hay que saber administrar". Pese a ello, Jorge Castellanos ha decidido **recortar el presupuesto del proyecto de fibra óptica en los hogares capitalinos y poner freno a su ejecución, en la que ya se han invertido \$2.3 billones**.

Durante la presidencia de Castellanos se han despedido a 100 administrativos, la pregunta entonces es **que podría pasar con las familias de los 2800 trabajadores que tiene la ETB sí queda en manos de capital privado**, afirma Sierra, y agrega que defender la empresa es un tema de trabajar por la [[soberanía en el ámbito estratégico de las telecomunicaciones](https://archivo.contagioradio.com/sindicato-de-etb-anuncia-paro-general/)].

<iframe id="audio_11396607" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11396607_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
