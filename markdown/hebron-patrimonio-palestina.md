Title: Hebrón declarada Patrimonio de la Humanidad a pesar de oposición israelí
Date: 2017-07-15 11:00
Category: Onda Palestina
Tags: Apartheid, Hebrón, ONU, Palestina
Slug: hebron-patrimonio-palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Ebron.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: RT 

###### 15 Jul 2017 

La UNESCO ha decidido **incluir en la lista de patrimonio de la humanidad a la ciudad de Hebrón y la mezquita de Ibrahim**, también conocida como la Tumba de los Patriarcas. La decisión se tomó a través de una resolución, que fue aprobada por el Comité del Patrimonio Mundial de la agencia de la ONU reunido en Cracovia, Polonia.

En ella se destaca su inclusión en el listado de sitios en peligro, que en el caso de Hebrón y la mezquita tienen que ver con l**a ubicación de colonias ilegales custodiadas por fuertes puestos de control**. Los palestinos habían urgido a la votación de esta medida al argumentar que Hebrón está en **peligro de destrucción por las "fuerzas de la ocupación israelíes"**.

El reconocimiento pondrá **limitaciones a la construcción y desarrollo del recinto y los alrededores por parte de Israel**, que actualmente mantiene puestos de control y estructuras militares en torno al casco histórico para **proteger a unos 500 colonos**, en la única localidad cisjordana con asentamientos en el centro de la ciudad.

Israel llevó a cabo una campaña para que la votación fuera secreta, como finalmente ocurrió, **aunque 12 países respaldaron la propuesta, 3 se opusieron y 6 se abstuvieron**. Este país se opuso a la declaración argumentando que se estaba negando el pasado judío de la tumba de los patriarcas, pero sus argumentos ocultan que **dicho lugar es una referencia tanto para los Judíos, como Cristianos y Musulmanes**.

El que se haya declarado Patrimonio de la Humanidad **garantiza que todas las religiones tengan este lugar a salvo de la destrucción y obliga a los Estados a proteger este patrimonio y apoyar las inciativas que compartan este objetivo**, incluso la UNESCO podría dirigir recursos hacia dicha protección a través del fortalecimiento de un fondo internacional.

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/category/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en [Contagio Radio](http://bit.ly/1ICYhVU). 
