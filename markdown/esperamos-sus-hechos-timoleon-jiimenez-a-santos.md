Title: “Esperamos sus hechos, Presidente, haga honor a su palabra” Timoleón Jiménez a Santos
Date: 2017-05-20 23:25
Category: Nacional, Paz
Tags: Conversaciones de paz de la habana, FARC, implementación acuerdos de paz, Santos
Slug: esperamos-sus-hechos-timoleon-jiimenez-a-santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/timochenko.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FARC-EP] 

###### [20 Mayo 2017]

En una comunicación publicada este sábado 20 de Mayo, el máximo jefe de las FARC, Timoleón Jiménez hizo un fuerte llamado al presidente Juan Manuel Santos para que con “hechos” haga valer lo acordado en la mesa de conversaciones y luego de una decisión de la Corte Constitucional que “echa al piso” el cuerpo de escoltas para las FARC y “modificación por el Congreso del Acuerdo Final” según Jimenez.

En la carta, Timoleón Jménez explica lo que está significando para los integrantes de las FARC EP entregar las “caletas” de armamento. El jefe guerrillero asegura que además de melancolía por entregar lo construido en años, también se está generando **“indignación” por el incumplimiento de los protocolos de seguridad**, tanto por parte de la propia ONU, garante del proceso, como de la Armada Nacional. [Le puede interesar: Corte Constitucional somete al proceso de paz a más obstáculos](https://archivo.contagioradio.com/corte-constitucional-somete-al-proceso-de-paz-a-mas-obstaculos/)

### **El gobierno cumple "demasiado lentamente su palabra"** 

Según lo comunicado por **Timoleón Jiménez**  el helipuerto de la ONU fue ubicado a 2 kilómetros del sitio, sin tener en cuenta las recomendaciones de los integrantes de las FARC, lo que los obligó a cargar a pié más de 3 toneladas de explosivos y armas y a exponerse a un incidente de seguridad dada la presencia de paramilitares, absolutamente visibles y evidentes en los puertos en los que también se mantienen las FFMM.

Según Timochenko, una de las cosas positivas que se dieron durante la jornada fue que los integrates de la ONU pudieron comprobar que en esa región de frontera con Ecuador, en el departamento de Nariño, hay control y persistencia de paramilitares que integran las llamadas “Nuevas Guerrillas Unidas del Pacífico” que también están ofreciendo 10 millones de pesos a los guerrilleros que se unan a sus filas.

### **La ONU pudo evidenciar la presencia de paramilitares** 

Para el jefe guerrillero, esas situaciones, tanto las decisiones de la Corte Constitucional, como la entrega de las caletas de armas de las FARC y la persistencia del paramilitarismo, son evidencias de esa guerrilla está cumpliendo todo lo acordado, mientras que el gobierno cumple “demasiado lentamente su palabra”  y las demás instituciones “titubean o actúan de modo sospechoso en relación con lo acordado”. [Lea también: Implementación del acuerdo de paz queda en el congelador. Enrique Santiago](https://archivo.contagioradio.com/corte-constitucional-congela-acuerdo-de-paz-con-las-farc-enrique-santiago/)

Esta es la carta completa...

[Carta de Timochenko Al Presidente Juan Manuel Santos](https://www.scribd.com/document/348975569/Carta-de-Timochenko-Al-Presidente-Juan-Manuel-Santos#from_embed "View Carta de Timochenko Al Presidente Juan Manuel Santos on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_19475" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/348975569/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-n5z5U7RLPL6M99oPOrCv&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
