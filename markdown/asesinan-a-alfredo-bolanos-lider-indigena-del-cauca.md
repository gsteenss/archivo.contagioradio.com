Title: Asesinato de Alfredo Bolaños es el noveno contra líderes indígenas en 2015
Date: 2015-10-20 17:26
Category: DDHH, Nacional
Tags: Luis Fernando Arias, ONIC
Slug: asesinan-a-alfredo-bolanos-lider-indigena-del-cauca
Status: published

###### Foto: lauralruiz 

###### [21 Oct 2015] 

Un nuevo hecho de violencia contra las comunidades indígenas se presentó en el Cauca cuando **miembros del ejército** del batallón José Hilario López, el lunes en horas de la noche **dispararon y asesinaron al líder indígena Alfredo Bolaños**, ex-gobernador del resguardo Puracé en el norte de este departamento.

La comunidad encontró al líder cerca de su finca luego de que se escucharan disparos cerca del lugar donde estaban ubicadas las tropas del ejército. Las detonaciones se escucharon cuando Bolaños llegaba a su casa y una de estas impactó en su cabeza.

Luis Fernando Arias, consejero mayor de la ONIC afirmó en su cuenta de Twitter [@[luiskankui]{.u-linkComplex-target}](https://twitter.com/luiskankui){.ProfileHeaderCard-screennameLink .u-linkComplex .js-nav} que "*~~@~~**COL\_EJERCITO** asesina con tiro de gracia en la cabeza a exgobernador indígena. A esta hora audiencia pública decide suerte de los asesinos!*.", por estola zona Centro del CRIC se ha **declarado en asamblea permanente** "para repudiar este hecho", afirma Jose Hildo Peque, miembro de la comunidad.

La comunidad indígena afirma la "**culpabilidad de esta muerte al ejercito**", que "**reconoce que dispararon, pero no el asesinato**" de Alfredo Bolaños. Las agresiones contra las comunidades indígenas se han repetido en distintas ocasiones, dejando 9 indígenas asesinados en este 2015, en donde el último hecho se presentó el 3 de junio en Caloto, con el asesinato de Jaime Poquiguegue.

La comunidad en este momento s**e encuentra militarizada y con la presencia del ESMAD**, quienes "acordonaron la zona, para salvaguardar el pueblo" indica Peque, pero siendo esto realmente "una vulneración a los derechos".
