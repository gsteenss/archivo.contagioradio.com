Title: ¿Por qué se mantiene el secreto del TLC entre la UE y EEUU?
Date: 2015-03-13 13:40
Author: CtgAdm
Category: Economía, El mundo
Tags: Documentos secretos TTIP UE y EEUU, LA UE solo deja dos horas a los eurodiputados ver documentos secretos, TTIP entre UE y EEUU
Slug: los-secretos-del-tlc-de-la-union-europea-con-espana
Status: published

##### Foto:Economíafinanzas.com 

##### **Entrevista con[ Jordi Sebastiá]:** 

<iframe src="http://www.ivoox.com/player_ek_4211073_2_1.html?data=lZeek5Wbd46ZmKiak5eJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8bX08rh0diPqMbgjLm5pZDJstXmxpCy19fTtMKf2pCyp7q5cYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Jordi Sebastiá**, eurodiputado del grupo mixto Primavera Europea, ha solicitado la cita para poder leer los documentos, él nos aclara que no solo **grupos de izquierda** están en **contra del TTIP**, sino que también **grupos empresariales y partidos de derechas** lo ven** inviable**.

Para él todo el protocolo de seguridad no es normal ya que, en EEUU el protocolo impide tener acceso a los documentos en estos casos pero no sucede así en la UE, donde en este tipo de negociaciones los eurodiputados suelen tener acceso. Todas estas complicaciones impuestas por la comisaria, responden al **descontento de partidos y grupos empresariales** debido al carácter **neoliberal** del acuerdo y a su inviabilidad por las propias dinámicas de ambas economías.

El eurodiputado explica que uno de los principales problemas del Tratado es que **EEUU, no está subscrito a la OIT**, por ello se teme una** reducción de los derechos laborales.**

Pero, según Jordi con el auge de partidos de izquierda como **Syriza en Grecia, es posible bloquearlo** para que no sea firmado. Además habría que** refrendarlo por cada estado miembro, lo que complicaría aún más su aprobación**.

Tras las declaraciones de la comisaria de comercio de la UE, aclarando que habría más transparencia en la **negociación del Tratado de Libre Comercio entre EEUU y Europa,**  actualmente la realidad es totalmente distinta.

La **UE solo permite a los eurodiputados acceder a una parte de los documentos secretos**. Solo pueden verlos durante **dos horas y bajo la estricta vigilancia** de un funcionario en un aula habilitada con cámaras y servicio de seguridad.

Tampoco pueden **reproducirlos o hablar al respecto** de lo que han leído,** bajo amenazas de sufrir un proceso penal si lo hicieran**. Es necesario pedir cita y explicar que documentos se quieren leer. Cuando se entra en la sala de lectura no se puede tener ningún dispositivo electrónico ni siquiera bolígrafos.

Según el documento elaborado por la comisaría de la UE **Cecilia Malmström **“...El espionaje puede provocar una grave violación de la integridad política y administrativa, y tener consecuencias graves para la UE y/o los estados miembros...”. Esta es la justificación para las duras medidas de seguridad en el acceso al TTIP.

Pero para la izquierda europea el espionaje no es justificación para tales medidas de seguridad. Grupos parlamentarios como "Primavera europea", partidos como Podemos, o Syriza opinan que la seguridad se debe al creciente descontento con el propio tratado.

Algunos analistas señalan que con el TTIP el aparato de** justicia quedaría supeditada al sector privado **y sus intereses**,**la perdida de derechos laborales en referencia a que** EEUU solo ha firmado 2 de 8 acuerdos de la OIT y la privatización de sectores públicos claves **en beneficio de las multinacionales.
