Title: Al menos 600 paramilitares se asentarían en territorio colectivo del Curvaradó en Chocó
Date: 2015-12-17 17:56
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Bajo Atrato, megaproyectos, paramilitares, Restitución de tierras, territorio colectivo del Curvaradó
Slug: al-menos-600-paramilitares-se-asientan-en-territorio-colectivo-del-curvarado-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Notimundo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto: eluniversal 

###### [17 Dic 2015]

Según las denuncias de los campesinos y afrodescendientes, desde hace varios días un numeroso grupo de paramilitares ha venido llegando al territorio colectivo ubicándose en inmediaciones del caserío de “Llano Rico” a escasos 20 minutos de la estación de policía de Pavarandó y cerca de la base militar del Batallón de Selva Nro 54, construida de manera inconsulta en la propiedad colectiva.

Desde la llegada de los paramilitares han buscado reuniones con habitantes de la región en la que les han afirmado que llegan para recuperar el territorio y cuidarlo a sus dueños, refiriéndose a empresarios palmeros que fueron condenados por desplazamiento forzado, concierto para delinquir de invasión de áreas de especial protección. Debemos “desterrar a la Comisión de Justicia y Paz y a Marcha Patriótica” “Nuestra misión es asegurar la tierra a los que están en la cárcel” habrían afirmado.

Recientemente se conoció que [paramilitares de las Autodefensas Gaitanistas planean atentados](https://archivo.contagioradio.com/autodefensas-gaitanistas-atentarian-contra-9-lideres-en-curvarado-choco/) contra la vida de la lideresa Ledis Tuirán y los líderes Eladio Cordero, Guillermo Díaz, Francisco Pérez, Eliodoro Hernández, Andrés Lance, Alfonso Falla, Julio Gómez, Francisco Gómez y Adrián Pérez de los consejos comunitarios de ese mismo territorio colectivo.

Los pobladores de la región han sufrido una serie de desplazamientos forzados desde el año 2002, su territorio fue ocupado por empresas palmeras de las que luego se conocieron sus vínculos con los grupos paramilitares y desde el 2009 se encuentran en un proceso de restitución efectiva de sus territorios sin que hasta el momento se [haya podido efectuar por la persistencia de los grupos paramilitares y los intereses empresariales](https://archivo.contagioradio.com/control-paramilitar-y-empresarial-impide-restitucion-de-tierras-en-curvarado-y-jiguamiando/).
