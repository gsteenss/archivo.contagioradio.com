Title: Mapa sitúa lucha de las mujeres contra extractivismo en América Latina
Date: 2017-03-09 20:04
Category: Ambiente, El mundo, Mujer
Tags: América Latina, extractivismo, mujeres
Slug: mapa-situa-la-lucha-de-las-mujeres-que-resisten-al-extractivismo-en-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/maxima-e1489107650792.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: salva la selva 

###### [9 Mar 2017] 

La lucha contra el extractivismo en América Latina, es en gran parte liderada por las mujeres. Bien es se ha afirmado, que cuando las multinacionales impactan la tierra con su modelo de desarrollo, el cuerpo de las mujeres se ve paralelamente afectado. Y Justamente para visibilizar esas luchas investigadoras del Instituto de Ciencia y Tecnología Ambientales de la **Universitat Autónoma de Barcelona (ICTA-UAB), la Red Latinoamericana de Mujeres Defensoras de los Derechos Sociales y Ambientales, y Censat Agua Viva-Amigos** de la Tierra de Colombia crearon un mapa donde se sitúan las resistencias de las mujeres.

Se trata del mapa “[Mujeres Latinoamericanas Tejiendo territorios](http://ejatlas.org/featured/mujeres)”, que se presentó en el marco de la conmemoración del Día Internacional de las Luchas de las Mujeres. Allí se destacan 21 conflictos, y con ellos, los testimonios y experiencias de las lideresas que han sido afectadas por las actividades mineras.

“El mapa pone en evidencia el vínculo entre violencia y dominación contra la naturaleza y la violencia contra las mujeres. Pero también pone en cuestión el mito de que estas mujeres son víctimas pasivas. De hecho, hay casos en que las minas han sido detenidas o los proyectos han sido rediseñados como resultado de su activismo”, explica Leah Temper, coordinadora del ACKnowl-EJ y Directora del Ejatlas del ICTA-UAB.

Diversos casos del mapa dan cuenta de la violencia que se ejerce sobre las mujeres por las actividades extractivas. **El asesinato de mujeres es parte de un patrón de persecución que se está denunciando como feminicidio**. Asimismo, visibiliza los casos en que las mujeres son criminalizadas y sus vidas puestas en peligro. Pero también, se muestran las propuestas de paz territorial que se forjan desde las lideresas y sus comunidades.

“Este continente es un hervidero de procesos de defensa territorial donde nosotras las mujeres tenemos un rol protagónico. La lucha por la defensa de nuestros territorios y de nuestros cuerpos es fundamental para enfrentar la andanada de proyectos extractivistas que se cierne sobre América Latina. ¿Qué mejor fecha para hacer este homenaje a las mujeres? El 8 de marzo es un día de resistencia, un día para conmemorar el papel de las mujeres en la construcción de la historia de la humanidad", expresa Daniela Rojas, de CENSAT Agua Viva de Colombia.

El mapa recoge testimonios como el de Rosa Govela, miembro de una red de afectados por la mina Tuligtic en Puebla, México, quien aseguró que se encuentran en resistencia “porque cuando se complica la producción de alimento en un territorio, ese efecto va directamente a las mujeres por la angustia de no saber de cómo darle alimento a su familia. **Otra realidad es el aumento de la prostitución, de la venta del alcohol en las familias, de la violencia contra las mujeres aumenta y del tráfico de personas, la trata**”.

[Consulte el mapa aquí ](http://ejatlas.org/featured/mujeres)

###### Reciba toda la información de Contagio Radio en [[su correo]
