Title: Más de mil familias podrían ser damnificadas por apertura de compuertas de Hidrosogamoso
Date: 2018-05-11 12:40
Category: Ambiente, DDHH
Tags: avalancha, Hidrosoga, Sogamoso
Slug: mas-de-mil-familias-podrian-ser-damnificadas-por-apertura-de-compurtas-de-hidrosogamoso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/Hidrosogamoso-FOTO-II-2017-06-13.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [11 May 2018] 

Los habitantes de las comunidades aledañas al Río Sogamoso denunciaron que de nuevo Hidrosogamoso abrió las compuertas de la represa, sin avisar previamente a toda la ciudadanía de este hecho y provocando que familias quedaran atrapadas por el agua, la perdida de cultivos y de la pesca. **Hasta el momento, el Movimiento Ríos Vivos, calcula que podrían ser alrededor de 1.000 familias las damnificadas**.

Frente a esta situación Claudia Ortiz, integrante de Ríos Vivos, afirmó que, pese a que tanto las instituciones locales como regionales tienen conocimiento de las múltiples afectaciones que provoca Hidrosogamoso y que esta no es la primera vez que las comunidades **son víctimas de las inundaciones, no hay ningún tipo de control sobre la empresa Isagen, dueña de la represa**.

“Se supone que les certifican a ellos para que estén pendientes de la situación, pero no hacen nada. Es decir, lo que le dicen a la gente es deje de sembrar, o no pueden pescar porque ya saben que abrieron compuertas. No hay ninguna presencia ni siquiera para acompañamiento de las comunidades o para saber si la empresa está haciendo bien el plan de contingencia sobre la evacuación del agua” manifestó Ortiz.

De igual forma Ortíz denunció que tampoco hay un inventario de pérdidas de la comunidad y que las únicas respuestas sobre esta situación, por parte de las instituciones, han sido **“saquen lo que puedan sacar, no deberían sembrar o no deberían vivir en la ribera del río”**. Sin que se den soluciones a las personas que habitaban el territorio mucho antes de que llegara la represa.

Además, a esta situación se suman los daños ambientales causados por el derrame de petróleo en la Lizama, que llegó hasta el río Sogamoso. Razón por la cual las personas se están alimentando de pescados contaminados o tienen problemas de salud. (Le puede interesar: ["Seis municipios inundados por la apertura de las compuertas de HidroSogamoso"](https://archivo.contagioradio.com/municipios_afectados_comuertas_hidrosogamoso/))

### **Habitantes del río Sogamoso necesitan ser re ubicación** 

El año pasado los habitantes de las comunidades cercanas al río Sogamoso manifestaron que duraron medio bajo el temor de una avalancha. Asimismo, expresaron que lo que han venido exigiendo es una re ubicación de sus viviendas a **otro lugares en donde puedan ejercer las mismas actividades económicas, como la agricultura y la pesca**.

> Pescador y Agricultor del sector la Playa, municipio de Betulia, integrante del movimiento Ríos Vivos Santander, habla sobre las afectaciones en el Río Sogamoso por derrame crudo Pozo 158 Lizama, Ecopetrol y apertura de compuertas de Hidrosogamoso de la empresa ISAGEN-BROOKFIELD [pic.twitter.com/M8kHE7h6r7](https://t.co/M8kHE7h6r7)
>
> — JuanCarlosG (@elcarlosjuan) [11 de mayo de 2018](https://twitter.com/elcarlosjuan/status/994759553685315590?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
“Si no quieren que la gente se esté quejando, si no quieren estar causando impactos y daños a las comunidades, pues re ubiquen a la gente, sáquenla de ahí, pero con garantías y condiciones de vida mejores antes de la llegada de la represa” Claudia Ortiz.

<iframe id="audio_25923883" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25923883_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
