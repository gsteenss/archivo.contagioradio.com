Title: "No hay día en que uno no se acuerde de Bernardo Beltrán" Sandra Beltrán
Date: 2017-11-03 19:29
Category: DDHH, Entrevistas
Tags: Bernardo Beltrán, Palacio de Justicia
Slug: no-hay-dia-en-que-uno-no-se-acuerde-de-bernardo-beltran-sandra-beltran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/escaner1-e1509747074865.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Familia Bernardo Beltrán] 

###### [03 Nov 2017] 

Este fin de semana se llevarán a cabo los actos de homenaje póstumo a la memoria de Bernardo Beltrán, víctima de desaparición forzada durante la toma y retoma del Palacio de Justicia, hace **32 años y que fue encontrado cuando se realizó la exhumación del cuerpo de Jorge Echeverry, auxiliar de magistrado**.

Para Sandra Beltrán, la entrega del informe forense y los restos óseos de su hermano es un momento agridulce, tras un largo tiempo de estar buscando y preguntando ¿qué pasó con las víctimas del Palacio de Justicia? “**nos sentimos muy tristes, con una ansiedad en el alma espantosa, es un sabor agridulce, estamos como los árboles desojándose**, pero con mucha fuerza y valentía por seguir en la búsqueda de la verdad”.

Y es que luego de que se realizará la exhumación del cuerpo de Jorge Echeverry y encontraran el de Bernardo, más preguntas quedaron para la familia Beltrán, entre ellas por qué si Bernardo **sale del Palacio de Justicia del codo de un policía** hacia la Casa del Florero, aparece dentro del Palacio, o cómo su cuerpo llega hasta Manizales, lugar de la exhumación de Echeverry y finalmente por qué sus huesos están quemados.

![Bernardo Video](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/Bernardo-Video.png){.alignnone .size-full .wp-image-48766 width="585" height="497"}

“Bernardo sale vivo hacia la casa del Florero, de allí no sabemos qué pasó con él, en qué momento regresa al Palacio, es asesinado, es quemado, **32 años después aparece en la tumba de un magistrado auxiliar**, en la ciudad de Manizales, totalmente incomprensible” afirmó Sandra.

### **La búsqueda de Bernardo Beltrán** 

Para Sandra ha existido una ausencia por parte del Estado durante 8 gobiernos, en los que han vivido en la completa soledad e impunidad de un pacto de silencio, “si no es por la sentencia de la Corte Interamericana de Derechos Humanos, Bernardo y sus compañeros jamás hubiesen aparecido”. (Le puede interesar: ["Héctor Jaime Beltrán, desaparecido del Palacio del Palacio de Justicia, abrió la alas para volar"](https://archivo.contagioradio.com/46816/))

Las familias de las víctimas del Palacio de Justicia han manifestado en diferentes ocasiones, que estos 32 años de buscar a sus hijos, hermanos, esposos, también ha significado **la unión de una nueva familia que se convirtió en un apoyo y para Sandra** este también ha sido un motor para continuar luchando contra el olvido.

### **Bernardo Beltrán y el recuerdo de la alegría** 

Bernardo era mesero de la cafetería del Palacio de Justicia, tenía **24 años y estaba interesado en realizar estudios de cocina y culinaria**, sus amigos y familiares lo recuerdan como una persona alegre y con un prometedor futuro en el fútbol colombiano.

“No hay día en que uno no se acuerde de Bernardo por algún detalle, desde el día siguiente de la toma, **nosotros empezamos a buscarlos, a esperarlo por todas partes,** en Medicina Legal, en hospitales, clínicas, en todos lados, pero nunca hubo una respuesta”, aseguró Sandra.

El 4 de noviembre se realizará el informe técnico y científico forense y habrá un segundo informe por parte de la Fiscalía en un acto privado con la familia, en el que Sandra Beltrán, hermana de Bernardo y las demás familias de las víctimas del Palacio de Justicia preguntarán una vez más, como lo han hecho durante estos 32 años, qué pasó con las víctimas de la toma y retoma del Palacio de Justicia y el **5 de noviembre se realizará una eucaristía y homenaje a la 1 pm, en la catedral Santiago Apóstol**.

<iframe id="audio_21869881" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21869881_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
