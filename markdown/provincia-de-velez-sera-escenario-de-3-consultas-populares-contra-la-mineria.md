Title: Provincia de Vélez será escenario de 3 consultas populares contra la minería
Date: 2017-09-04 16:08
Category: Ambiente, Voces de la Tierra
Tags: Consulta populares, Santander
Slug: provincia-de-velez-sera-escenario-de-3-consultas-populares-contra-la-mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/sucre_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:United Nation Office on Drungs and Crime] 

###### [04 Sept 2017]

Los habitantes de Sucre, El Peñón y Jesús María, tres municipios en la provincia de Vélez, en el Santander, decidieron sumarse a las consultas populares que se han hecho en el país, para frenar el **avance de las empresas mineras que tienen títulos que se han otorgado en sus territorios y que de acuerdo con la comunidad acabarían con la producción agrícola del departamento.**

Mauricio Mesa, integrante de la Corporación Compromiso que ha acompañado el proceso de los tres municipios, **manifestó que el 85% de la provincia de Vélez, lugar en el que se encuentran los municipios**, tiene títulos mineros para realizar explotación de carbón y materiales de construcción. (Le puede interesar: ["Con acuerdo municipal Urrao le dice no a la minería"](https://archivo.contagioradio.com/mineria_urrao/))

Para Mesa “ante la irresponsabilidad del gobierno, de la Agencia Nacional Minera, de la Agencia Nacional de Hidrocarburos y de los entes de control ambiental, **de ofertar títulos mineros a diestra y siniestra”** se está poniendo en riesgo el sustento de muchos campesinos.

Además, afirmó que en los tres municipios ha existido un gran compromiso por parte de las Alcaldías y la iglesia que ha permitido avanzar mucho más rápido en el trámite de la consulta popular. Referente a que empresas estarían detrás de estos **títulos mineros Mesa expresó que entre ellas figuran están CEMEX, Cementos ARGOS**, y otras que tendrían títulos en cada uno de estos municipios.

### **Consulta popular en Jesús de María** 

Este municipio tiene un censo electoral de **3.256 ciudadanos, la consulta popular se llevará a cabo el próximo 17 de septiembre y necesitará un umbral de 1.300 votos**. Jesús María tiene 9 títulos mineros ofertados, de ellos 8 son para la explotación de carbón y uno para la explotación de materiales de calcáreos.

### **Consulta popular en Sucre** 

Este municipio tiene un censo electoral de **5.850 habitantes, la consulta popular se llevará a cabo el 1 de octubre y necesita un umbral de 1.600 votos**. En el municipio de Sucre, hay 22 títulos mineros de los cuales 19 son para la explotación de carbón y 3 para materiales calcários. (Le puede interesar: ["Comunidades del Carmen de Chucurí por posibilidad de Fracking en su territorio")](https://archivo.contagioradio.com/carmen-del-chucuri/)

### **Consulta popular en El Peñón** 

Este municipio cuenta con una población de **5.400 habitantes, la consulta popular se realizará el próximo 5 de noviembre y necesitará un umbral de 1.600 votos**, allí hay 12 títulos mineros, todos para la explotación de carbón. (Le puede interesar: ["Hay que descarrilar la locomotora minera: Ambientalistas"](https://archivo.contagioradio.com/hay-que-descarrilar-la-locomotora-minera-ambientalistas/))

Mesa afirmó que el proceso para que los tres municipios decidieran hacer las consultas populares es producto del trabajo de la iglesia por la defensa del territorio y de las comunidades organizadas, “las asociaciones de mora, de guayaba, de bocadillo beleño, los comités de profesores y las ongs de Bogotá, han estado participando y asesorando en el camino”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
