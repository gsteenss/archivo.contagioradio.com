Title: Testigos electorales denuncian fraude en comicios de Sibaté
Date: 2015-10-27 17:48
Category: Nacional, Política
Tags: Cmabio Radicla, Derechos Humanos, Elecciones en Cundinamarca, elecciones regionales, Fraude electoral, Luis Roberto García, Minería ilegal, Movilización ciudadana, Partido Liberal, Radio derechos Humanos, Registraduría Nacional, Sibate
Slug: testigos-electorales-en-sibate-denuncian-delitos-electorales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/DSC_2992.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [sibate-cundinamarca.gov.co]

<iframe src="http://www.ivoox.com/player_ek_9184512_2_1.html?data=mpallpqVdo6ZmKiakpyJd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmMbn1c7U0diPqc3ZxNnc1MbQqdSfxtOYtc7GpdWZpJiSo56PqMbi1tPQy8bSb8XZzc7h0diPqc3ZxNmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Laura Bello, testigo electoral] 

###### 27 oct 2015 

En el municipio de Sibaté la comunidad denuncia que **en las elecciones para Alcaldía de este domingo se presentaron graves irregularidades, **las cuales dieron el triunfo de Luis Roberto Gonzáles, representante del Partido Liberal y Cambio Radical con 7.489 votos.

Laura Bello testigo electoral en las votaciones narró que en la jornada, en el puesto de votación de San Jorge, **candidatos al consejo de parte de Luis Roberto Gonzáles**, **eran testigos electorales** a la hora del conteo, en ese momento un personaje **entró y salió corriendo del lugar con unos papeles en el bolsillo**, pasando en frente de la policía sin que esta lo requisara.

Posteriormente la población encontró al individuo  **“llenando papeles con funcionarios de la Registraduría”** a favor del candidato del Liberal y Cambio Radical. También se denunció que “**los votos no se sacaron de la urna triclave**, sino en unas cajas” que fueron las mismas donde los votantes depositaron sus tarjetones.

Por su parte **la Registraduría Nacional no se ha pronunciado sobre el caso,** pese a las denuncias de la comunidad.

Cabe resaltar que en la empresa italiana **Comind S.A, estaría a favor de la **campaña de Luis Roberto Gonzáles, con el fin de proteger sus intereses frente a la actividad minera que se realiza en zona por parte de esta compañía.
