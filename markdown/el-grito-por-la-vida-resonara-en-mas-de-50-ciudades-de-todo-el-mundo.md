Title: "El Grito" por la vida,  resonará en más de 50 ciudades de todo el mundo
Date: 2019-07-24 17:37
Author: CtgAdm
Category: eventos, Movilización
Tags: 26 de julio, El Grito, lideres sociales, Movilización
Slug: el-grito-por-la-vida-resonara-en-mas-de-50-ciudades-de-todo-el-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/IMG-20190722-WA0011.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/IMG-20190724-WA0017.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/IMG-20190724-WA0025.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/IMG-20190724-WA0024.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/IMG-20190724-WA0019.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/IMG-20190724-WA0018.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/IMG-20190724-WA0017-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/IMG-20190724-WA0004.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Este 26 de julio se llevará a cabo una multitudinaria marcha "El Grito" en más de cincuenta puntos. En Colombia serán más de 20 ciudades y alrededor de todo el mundo  se movilizarán 28 ciudades  como Nueva York, Madrid y Atenas, en defensa de la vida de líderes y  lideresas sociales en Colombia. La movilización fue convocada por el Movimiento Defendamos la Paz al que se le han sumado miles de organizaciones y movimientos sociales de todo el mundo. Según Indepaz, desde la firma de los acuerdos en el 2016  hasta el 20 de mayo de este año han sido asesinados 837 líderes sociales en Colombia.

Estos serán los lugares:

### En Colombia 

**Bogotá**  la cita  es a las 5:00 pm, y será realizada desde el centro de Memoria, Paz y Reconciliación, hacia la Plaza de Bolívar.

**Antioquia:**

Medellín 4:00 pm Marcha del Parque San Antonio al Museo Centro de la Memoria (Parque Bicentenario).

**Atlántico:**

Barranquilla: 5:00 pm  Marcha desde Parque Esthercita Forero hasta la plaza de la PAZ.

**Bolívar:**

Cartagena 5:00 pm    Se realizará un plantón en la Plaza de la Paz.

**Boyacá:**

Tunja  5:00 pm la  marcha será desde Plazoleta San Francisco (San Pacho) hasta la Plaza de Bolívar.

Duitama 6:00 pm Se realizará un velatón en el Parque de los Libertadores.

**Caquetá:**

Florencia  5:00 pm   La marcha iniciará desde la Universidad de la Amazonía al Parque Santander.

**César:**

Valledupar 9:00 am La marcha será desde  Glorieta de la Ceiba hacia la plaza Alfonso López.

**Córdoba:**

Montería: 5:00 pm Plantón en el Parque Laureano Gómez.

Puerto Libertador: 5:00 pm La marcha será desde el Parque frente a la Ese Divino Niño  hasta el parque principal.

San Pedro 4:00 pm Se realizará un plantón en la Plaza principal.

Tierralta 4:00 pm La marcha será desde la  Plaza de la Bonga hasta el parque principal frente a la Alcaldía.

**Guajira:**

Maicao 5:00 pm  Se realizará un plantón en la Casa de la Cultura.

**Huila:**

Neiva  5:00 pm Se realizará un plantón en la Plazoleta de la alcaldía de Neiva.

**Magdalena: **

Santa Marta 5:00 pm Se realizará un plantón  cultural en la Plaza de Bolívar.

Ciénaga 5:00 pm Plantón en la Plaza de las Mártires.

**Montería:**

Puerto Libertador  6:00 pm La marcha desde el Parque frente a la ESE Divino niño hasta el parque principal.

**Santander:**

Barrancabermeja 5:00 Se realizará un plantón en el Parque Camilo Torres.

Bucaramanga: 5:00 pm Plantón Parque San Pío.

Sabana de Torres 6:00 pm Se realizará un plantón en el  Parque Principal.

**Putumayo:**

Mocoa  5:00 pm Se realizará un plantón en Parque General Santander.

Valle del Guamuez 5:00 pm  Parque Principal Los Fundadores de la Hormiga.

**Quindío:**

Armenia 5:00 pm la marcha será desde el Parque Fundadores hasta la Plaza de Bolívar.

**Risaralda:**

Pereira  2:00 pm La marcha será desde La Fiscalía hasta la Plaza de Bolívar.

**Nariño:**

Pasto 3:30 pm  La marcha será desde SIMANA hasta el Parque Nariño.

Tumaco 8:00 am Marcha desde Cancha San Judas.

**Sucre:**

Sincelejo  8:30 am  la marcha será desde Ciledco hasta el Parque Santander.

**Valle del Cauca:**

Buenaventura 5:00pm  Plantón en Boulevard del Centro.

Cali  5:00 pm Se realizará un plantón en la  Plazoleta San Francisco.

Guacarí (Valle)  5:00 pm Se realizará un plantón  en el parque principal.

Jamundí (Valle) 3:00 pm Se realizará un plantón en el parque principal.

### Otras ciudades del mundo 

\

###### [Imágenes: @DefendamosPaz] 

### En Latinoamérica 

Buenos Aires  4:00 pm Se realizará un plantón  en el Obelisco.

México 7:00 pm  Se realizará un plantón en El Ángel de la Independencia.

Montevideo  6:00 pm Se realizará un plantón   en la explanada de la Intendencia.

Santiago de Chile 6:30 pm Se realizará un plantón en la Plaza de Armas, frente a la Catedral.

### África 

Las Palmas Gran Canaria  será el 25 de julio  a las 6:00 pm y se realizará un plantón frente al Consulado de Colombia.

### América del Norte 

Nueva York  6:00 pm Encuentro en Washigton SQ Park.

Minneapolis 5:00 pm  Encuentro en 3702 E Lake Street Copal 2º piso.

Montreal 5:00 pm  Se realizará un velatón frente al Consulado de Colombia.

Ottawa-Gatinea  6:30 pm  Se realizará un plantón  en el Parlamento de Ottawa en la llama del centenario.

Quebec 6:00 pm Atrio de la Iglesia de San Roche.

Toronto  6:00 pm En Matt Cohen Park (Bloor and Spadina).

Washington DC   5:30 pm  La marcha será  desde la Salida del metro Dupont Circle, Q St & Connecticut Ave, NW

Winnipeg  6:00 pm  Se realizará un plantón en la Entrada Grupal del Canadian Museum for Human Rights.

### Australia 

Sidney Se realizará un plantón el 3 de Agosto  en el terminal de pasajeros circular qua.

### Europa 

Atenas 5:00 pm Se realizará un plantón  en Acrópolis, en el Partenón.

Berlín  3:30 pm  la marcha será desde Dorothea-Schlegel-platz a la Taubenstr 23, sede de la Embajada de Colombia en Berlín.

Berna 6:00 pmSe realizará un plantón en Bahnhofplatz Bern.

Bruselas  5:00 pm Se realizará un plantón en Carrefour de l'Europe 2 (Gare Centrale) 1000 Bruxelles Belgique.

Elche  11:00 am Se realizará un plantón  Ayuntamiento.

Estocolmo  5:00 pm Se realizará un plantón  Frente a la Embajada Colombiana en Estocolmo Östermalmsgatan 46. Estación: Stadion

Frakfurt  6:00 pm Se realizará un plantón  frente al monumento del €.

La Haya  4:00 pm Se realizará un perfomance corte Penal Internacional pude Waaisdorperweg. 4pm Se realizará un plantón  en la embajada de Colombia en Países Bajos Groot Hertoginnelaam 14

Londres  4:00 pm el punto de encuentro será BBC Londres.

Lyon  6:00 pm Plaza Bellecour

Madrid  7:00 pm Se realizará un plantón  en el parque El Retiro entrada de la puerta de Alcalá.

Munich 5:00 pm Se realizará un plantón en  la plaza Max-Joseph-Platz.

Roma  7:00 pm FlashMob Piazza Santa María di Loreto - Marcha Pacífica Via dei Fori Imperiali.

Valencia  7:30 pm Se realizará un  plantón en la Plaza de la Virgen.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
