Title: Inicia audiencia final contra Santiago Uribe en caso los 12 Apóstoles
Date: 2019-11-06 10:52
Author: CtgAdm
Category: Entrevistas, Judicial
Tags: 12 apostoles, Daniel Prado, Paramilitarismo, Santiago uribe
Slug: inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @sermeca  
] 

El próximo 6 de noviembre iniciará la audiencia de alegatos finales en el proceso contra Santiago Uribe Vélez, hermano del senador Álvaro Uribe Vélez, acusado por la conformación del grupo paramilitar conocido como los "12 Apóstoles" y el asesinato de Camilo Barrientos. La audiencias se desarrollarán pese al nuevo intento de la defensa de Uribe por retrasar el proceso, aduciendo que hubo poco tiempo para la preparación. (Le puede interesar: ["Santiago Uribe iría a juicio por creación de paramilitares de los 12 apóstoles"](https://archivo.contagioradio.com/santiago-uribe-podria-ir-a-juicio-por-nexos-con-el-paramilitarismo/))

### **En Contexto: Santiago Uribe y los 12 apóstoles  
**

El proceso contra Uribe Vélez está en su etapa final en el Tribunal Superior de Antioquia, donde se investiga la presunta participación del ganadero en la creación del grupo denominado los '12 apóstoles', una organización paramilitar que operaba en el municipio de Yarumales, Antioquia. Asimismo, se evalúa si Uribe tiene que ver con el asesinato de Camilo Barrientos, conductor de una 'chiba' o bus escalera, a cargo del mencionado grupo. (Le puede interesar: ["14 revelaciones en la resolución de acusación contra Santiago Uribe Vélez"](https://archivo.contagioradio.com/las-revelaciones-de-la-resolucion-de-acusacion-contra-santiago-uribe-velez/))

Los 12 apóstoles fueron creados en la década de los noventa por comerciantes y autoridades civiles de Yarumales para 'defender' a la población civil de las guerrillas, ladrones y personas que las pudieran afectar. Los paramilitares habrían ganado terreno en la zona, y posteriormente avanzaron en el uso de la violencia para otros efectos, dando paso a la creación de otros grupos como las Autodefensas Campesinas de Córdoba y Urabá (ACCU). (Le puede interesar: ["Los testimonios que comprometen a Santiago Uribe con los 12 Apóstoles (1era parte)"](https://archivo.contagioradio.com/los-audios-que-comprometen-a-santiago-uribe-con-los-12-apostoles-1era-parte/))

### **Un proceso que ha sido muy dilatado**

Daniel Prado, abogado defensor de derechos humanos y parte en el proceso contra Santiago Uribe ha señalado que el proceso ha sido muy demorado porque la defensa de Uribe ha buscado que así sea. En primer lugar, pidiendo que se repitan pruebas que ya habían sido tomadas en la etapa previa del jucio, luego, aportando testigos con información incompleta y por último, cuestionando las pruebas que pesan en su contra. (Le puede interesar: ["Piden al Estado prevenir manipulación de testigos tras libertad de Santiago Uribe"](https://archivo.contagioradio.com/manipulacion_testigos_santiago_uribe_12_apostoles/))

En esta última etapa del juicio, Prado señaló que la defensa de Uribe ha intentado que se aplace la audiencia, señalando que el vocero Jesús Albeiro Yepes recibió el poder para representar a Santiago Uribe hace un mes, y por lo tanto, no ha tenido tiempo para preparar la vocería. Sin embargo, Prado recordó que desde diciembre de 2018 Yepes le ayudó al abogado Jaime Granados a preparar algunos documentos del caso, por lo que concluyen que Yepes sí ha hecho parte de la defensa y conocer el proceso integralmente. (Le puede interesar: ["Defensa de Santiago Uribe es más mediática que jurídica: Daniel Prado"](https://archivo.contagioradio.com/defensa-de-santiago-uribe-es-mas-mediatica-que-juridica-daniel-prado/))

Pese a esta maniobra, que se puede leer como dilatoria, el juzgado negó la solicitud de aplazamiento, por lo que el abogado defensor de DD.HH. espera que se inice la audiencia, que durará hasta el 13 de noviembre, y así concluya este proceso. (Le puede interesar: ["Una vez más suspenden audiencia contra Santiago Uribe"](https://archivo.contagioradio.com/una-vez-mas-suspenden-audiencia-contra-santiago-uribe/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44239396" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44239396_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
