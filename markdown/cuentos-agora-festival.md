Title: Ágora un Festival para echar cuentos
Date: 2017-07-27 10:35
Category: Cultura, Otra Mirada
Tags: Bogotá, cuentería, Cultura, Festivales
Slug: cuentos-agora-festival
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/El-regalo-2-e1501092929188.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Medios y Artes 

###### 27 Jul 2017 

El arte de contar historias vuelve en la versión **XIII de Ágora, Festival de cuentos y cuenteros de Bogotá,** un evento que abre sus convocatorias para los nuevos narradores en todas las localidades de la ciudad, con espacios de formación, presentaciones artísticas y Narratón.

Juan Carlos Grisales, director del grupo Luna Nueva y del Festival, aseguró que la intención es "**hacer de Bogotá un escenario de la palabra**" añadiendo que al abrir un espacio para el ciudadano del común "**queremos que la voz de los barrios un poco habite el escenario del teatro** en que habitualmente nos presentamos los cuenteros".

Para la presente edición, los organizadores de Ágora **buscan que todas y todos los ciudadanos que no se consideren narradores orales profesionales, se animen a contar las historias** **de sus barrios**, mitos, leyendas, adaptaciones o adaptaciones de literatura o composiciones propias, enmarcadas en las situaciones de su cotidianidad y actividades domésticas.

Los interesados, quienes **deben ser mayores de edad y residir en Bogotá desde hace no menos de un año**, deberán realizar su [inscripción en línea](https://docs.google.com/forms/d/1-DR95Gb8I3NfkIo2pI_YLqa0VeyvolQJYojoMXpoMRk/viewform?ts=596a5405&edit_requested=true) hasta el próximo **10 de agosto**, donde serán seleccionadas algunas de las historias. Aquellos que resulten elegidos participarán en un **taller de montaje de cuento durante 10 horas los días sábados 12 y 19 de agosto**.

El taller estará dividido en dos jornadas durante cada fecha: la primera en el "Teatro Hilos Mágicos", de 8 a 1 de la tarde a cargo de la maestra **Nelly Pardo**, quien cuenta con más de 35 años de trayectoría en cuentería y  la segunda de 2 a 7 pm en cabeza del maestro **Alberto Lozada** en la sala "El trensito de papel", ubicada en el barrio La alqueria.

Para el Director del Festival poder participar de los talleres es ya una ganancia por tratarse de "**una oportunidad de compartir con un par de maestros no solamente de la narración oral sino también del teatro y las artes escénicas en general** que van a poder acercar a la gente a una experiencia muy sensible alrededor de la creatividad como una acción que nos realiza como personas".

Los maestros se encargaran de aconsejar a la organización del evento sobre quienes pueden participar en la Narratón el 26 de agosto, para que el jurado calificador decida que historias harán parte de la programación oficial del Festival. Además de acompañar en las funciones del festival a los narradores profesionales invitados, **los cuentos seleccionados serán publicados en el libro memoria de Ágora 2017**.

El jurado está integrado por el actor **Diego Beltrán** actor de Ensamblaje Teatro, director, narrador oral y docente; **Mauricio Grande**, cuentero e investigador sobre la oralidad e **Ignacio Prieto**, antropólogo y asesor de proyectos como el Abrazo de la Serpiente, película del director Ciro Guerra nominada al Oscar en 2015.

El evento, que tendrán lugar entre el el **12 de Agosto y el 7 de Octubre**, es organizado por la Fundación Francisca Radke en asociación con el grupo Luna Nueva, con apoyo de la Secretaria de Cultura Recreación y Deporte.

<iframe id="audio_20029828" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20029828_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
