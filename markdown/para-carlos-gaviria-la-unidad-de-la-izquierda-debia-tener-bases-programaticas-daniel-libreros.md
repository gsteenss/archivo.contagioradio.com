Title: "Para Carlos Gaviria la unidad de la izquierda debía tener bases programáticas" Daniel Libreros
Date: 2015-04-01 19:16
Author: CtgAdm
Category: Entrevistas, Política
Tags: carlos gaviria, daniel libreros, izquierda, paz, Polo Democrático Alternativo, unidad
Slug: para-carlos-gaviria-la-unidad-de-la-izquierda-debia-tener-bases-programaticas-daniel-libreros
Status: published

#####  Foto: 

<iframe src="http://www.ivoox.com/player_ek_4295703_2_1.html?data=lZeml5yUd46ZmKiakp2Jd6Knl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmscbfw5CnpdPg0NiYqcbardPdwpDZw5DZssrYwsmYxsqPsMKfyt%2Fe187JtsXVjMnSxIqnd4a1pcaY1pKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Daniel Libreros, Profesor universitario] 

Para el profesor e investigados Daniel Libreros, quien hiciera parte del Polo Democrático Alternativo durante la primera década de esta convergencia de izquierda a través del Frente Social y Político, la **muerte de Carlos Gaviria representa una gran perdida**.

Libreros acompañó de cerca a Gaviria durante su pre-candidatura presidencial en el Polo en el año 2005, construyendo la plataforma económica del proyecto de izquierda que **"logró distanciarse de las posturas tradicionales de la dominación política en Colombia"**, y posteriormente en la candidatura a la presidencia en el año 2006 en que se enfrentó a Alvaro Uribe Vélez.

Para el profesor universitario, Gaviria era **"un hombre de una inteligencia bastante grande"** que "logró plantear una propuesta democrática radical en su característica como libre pensador, en temas donde una sociedad mojigata como la nuestra está todavía inmersa", y por esa vía, marcar "una tradición distinta del comportamiento de la izquierda colombiana".

Daniel Libreros indica que para Gaviria siempre fue una **preocupación central lograr la unidad de la izquierda**. Esta, sin embargo, debía tener bases programáticas, criterios y propuestas que sirvieran para transformar el país, "sin sectarismos y sin ambigüedades".

Por este motivo, en el último año **Carlos Gaviria** habría estado preocupado por que la izquierda se perdiera como referente estratégico que no planteara propuestas alternativas de sociedad, por conformarse con resultados electorales de corto plazo. "Creo que el momento político amerita una discusión en medio de toda esta una **crisis institucional** de esta putrefacción de la justicia, en medio de la bancarrota del congreso, del sistema electoral de clientelismo del dinero corrompiendo toda las instancias y las esferas de decisión del país, lo que se llama comúnmente como mermelada, que eso le repugnaba a él en términos absolutos, necesita una reflexión mas allá de un circulo de corto plazo de resultados electorales, y un planteamiento de mirada de largo plazo, o estratégica, para proponer un cambio mas radical en la sociedad".

Finalmente, Libreros manifiesta que la noticia lo golpeó. "Siento que he perdido a un amigo", señala. "En Carlos siempre encontré amistad y complicidad, cualquiera que fuera la circunstancia del encuentro".
