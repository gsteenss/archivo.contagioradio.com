Title: Fiscalía tendría 10 días para entregar pruebas a la JEP en caso Santrich
Date: 2018-05-18 15:04
Category: Política
Tags: Fiscalía, JEP, santrich
Slug: santrich-pruebas-extradicion-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/6b-jesus-santrich.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Heraldo 

###### 18 May 2018 

Luego de conocerse [la decisión de la Jurisdicción Especial de Paz que frenaría el proceso de extradición de Jesús Santrich](https://archivo.contagioradio.com/las-razones-de-la-jep-para-detener-la-extradicion-de-jesus-santrich/), y pese al comunicado de la Fiscalía y del gobierno nacional que la defensa interpreta como una intromisión en la Jurisdicción de paz, el dirigente del partido FARC podría considerar levantar la huelga de hambre que ya completa 39 días y que podría comenzar a afectar sus órganos vitales a partir del día 40.

Gustavo Gallardo, abogado de Santrich, afirma que la decisión de la JEP se ajusta a su mandato y que es una muy buena señal para abrir el camino del cumplimiento del acuerdo de paz firmado en Noviembre de 2016 en el Teatro Colón, sin embargo subrayó que el papel de la Fiscalía General de la Nación al oponerse, es señal de que las instituciones del Estado no están dispuestas a cumplir el acuerdo ni a respetar las instancias que se crearon.

En el mismo sentido, frente a la comunicación del gobierno nacional que hizo un llamado a que el congreso legisle para establecer la competencia de o no de la JEP en el caso particular, Gallardo señaló que no se requiere de manera concreta la norma de procedimiento de la JEP  "toda vez que estamos hablando de la libertad de una persona frente una solicitud que esta en curso y tiene soporte en la Constitución y en la ley, en el Acuerdo de La habana y en los instrumentos internacionales", por lo que asegura es competente para tomar esa decisión y las que se desprendan del estudio del caso.

**Lo que sigue en el proceso **

Gallardo señala que ambas partes, defensa y Fiscalía, tendrán 10 días para presentar pruebas conducentes y pertinentes para demostrar la seriedad del caso ante la JEP, instancia que deberá estudiar a fondo los hechos y las pruebas, para establecer la responsabilidad o no de Santrich en el caso y determinar si sigue siendo competente para adelantar el proceso de extradición o si en su defecto es enviado a la justicia ordinaria. Una vez presentadas las pruebas, la sección de revisión tendrá 120 días para tomar una decisión de fondo.

**La salud de Santrich**

Según reporta el abogado, el dirigente del partido FARC ya se encuentra débil y que los médicos han señalado que esta empezando a presentar algunos indicios de fallas en su sistema renal y en su salud en general. Espera el día de hoy conocer si Santrich tras los últimos acontecimientos considerará levantar la huelga, como se lo han pedido desde instancias políticas y ciudadanas.

Así mismo Gallardo constata que en la fundación Caminos de libertad donde se encuentra recluido Santrich, han podido mantener continua comunicación y se la ha atendido bastante buena, salvo por la presencia de la policía que, en su criterio, genera pánico a transeúntes, por lo que asegura han solicitado que se retire ese anillo de seguridad al considerarlo innecesario.

<iframe id="audio_26056956" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26056956_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
