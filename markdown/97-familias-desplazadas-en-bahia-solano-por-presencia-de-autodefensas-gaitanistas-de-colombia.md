Title: 97 familias desplazadas por presencia de Autodefensas Gaitanistas
Date: 2016-12-20 17:11
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, Desplazamiento forzado
Slug: 97-familias-desplazadas-en-bahia-solano-por-presencia-de-autodefensas-gaitanistas-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/WhatsApp-Image-2016-12-20-at-3.18.32-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Defensoría del Pueblo] 

###### [20 Dic 2016]

En los últimos días la Defensoría del Pueblo de Bahía Solano, en Choco, informó que **97 familias han tenido que huir de este territorio debido a las acciones violentas de grupos identificados como Autodefensas Gaitanistas de Colombia** (AGC) que los han forzado a salir de sus hogares, hecho que se constituye como desplazamiento forzado.

Las 327 personas constituyen las familias de los corregimientos de Nabugá y Huaca, pertenecientes a etnias indígenas y afrocolombianas. Carlos Alfoso Negrete, defensor del pueblo, hizo un llamado a las autoridades para proteger la vida, dignidad e integridad de la población de Bahía Solano y al Ministerio de **Defensa para que se fortalezca la capacidad de respuesta en materia de seguridad.**

La defensoría también ha visibilizado que el temor de la población se ha infundado en actos como los ocurridos el pasado 4 de diciembre, en donde se produjo la **desaparición forzada del indígena Daniel Cabrera Lana y el asesinato Deiber Potes Girón**, el 6 de diciembre. Le puede interesar:["Paramilitares refuerzan su presencia en el Sur de Córdoba" ](https://archivo.contagioradio.com/paramilitares-refuerzan-presencia-sur-cordoba/)

A estos hechos se suman **el homicidio y desmembramiento del joven Roberto Jiménez Bocanegra, en el municipio de Huaca**, el pasado 12 de diciembre. De igual forma la población también ha informado que han llegado diferentes amenazas a personas de la comunidad.

Ante estos actos, desde el 18 de diciembre se están realizando misiones humanitarias a cargo de funcionarios de la Defensoría del Pueblo, en 3 municipios, Juradó, Bahía Solano y Nuquí,  para **verificar la situación de riesgo de desplazamiento y confinamiento de los pobladores**, por la presencia de grupos armados ilegales y la posible instalación de minas antipersona.

###### Reciba toda la información de Contagio Radio en [[su correo]
