Title: Los 8 minutos y 46 segundos que arrodillaron a demócratas de EE.UU. en memoria de George Floyd
Date: 2020-06-06 19:34
Author: CtgAdm
Category: DDHH, El mundo
Tags: EE.UU., George Floyd
Slug: los-8-minutos-y-46-segundos-que-arrodillaron-a-democratas-de-ee-uu-en-memoria-de-george-floyd
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/49953285551_36ddc9bf85_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: George Floyd Flickr/Fibonacci Blue

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 4 de junio, casi 2 semanas después del asesinato de George Floyd, demócratas del Senado de Estados Unidos, dirigidos por el senador Cory Booker, guardaronn 8 minutos y 46 segundos de silencio en memoria del guardia de seguridad afroamericano asfixiado el pasado 25 de mayo por Derek Chauvin y tres oficiales de Policía de Minnesota. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El senador Booker inició con un breve discurso para honrar la vida que finalizó a los 46 años de edad. **«Estamos reunidos aquí con solemne reverencia no solo para recordar su trágica muerte sino para hacer honor a su vida»; además, mencionó que la familia de George Floyd desea que por sobre todo, sea recordado como un hombre cariñoso, piadoso y siempre centrado en la familia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El momento de silencio duró 8 minutos y 46 segundos debido a que esta fue la cantidad de tiempo que el policía Chauvin tuvo a George Floyd presionado contra el pavimento con su rodilla apoyada sobre su cuello. Durante los minutos de respeto, varios demócratas del senado se arrodillaron, considerado como símbolo de la cruel forma en la que el guardia de seguridad murió. [(Lea también: George Floyd y los tres detonantes de las protestas en EE.UU.)](https://archivo.contagioradio.com/george-floyd-y-los-tres-detonantes-de-las-protestas-en-ee-uu/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las **Protestas continúan** en los EE.UU.

<!-- /wp:heading -->

<!-- wp:paragraph -->

A pesar de las amenazas de militarización por parte del presidente[Trump](https://twitter.com/realDonaldTrump), miles de personas continúan en las calles y con consignas, carteles, representaciones, cacerolas, han realizado numerosas manifestaciones, en su mayoría pacíficas, en contra del racismo y la violencia pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por otro lado, a medida que crece la indignación y las protestas, las personas detenidas son aún más. Según la agencia de noticias AP, más de 10.000 personas han sido detenidas en todo el país y la gran mayoría son por delitos de bajo nivel como incumplimiento de toques de queda o no dispersarse. Los Ángeles ha tenido más de una cuarta parte de los arrestos nacionales, seguido de Nueva York con alrededor de 2.000 arrestos y le siguen Dallas y Filadelfia.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La lucha por el fin del racismo policial es mundial**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Este acontecimiento no solo ha generado indignación en el país Norteamericano, sino alrededor del mundo entero. George Floyd se ha convertido en el símbolo de la lucha contra el racismo. Personas a través de redes sociales han expresado su apoyo para acabar con la violencia pública y el racismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras en Colombia, el pasado miércoles se conoció de la muerte de Anderson Arboleda, un afrodescendiente de Puerto Tejada, Cauca quien murió por golpes propinados por policías que, según relatos de personas cercanas al hecho, habrían golpeado al joven en su cabeza ocasionándole muerte cerebral y posterior fallecimiento.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
