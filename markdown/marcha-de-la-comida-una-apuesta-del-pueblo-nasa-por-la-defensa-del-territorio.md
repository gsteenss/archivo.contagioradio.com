Title: Marcha de la Comida, una apuesta del pueblo Nasa por la defensa del territorio
Date: 2019-05-22 18:06
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Cauca, Liberació de la Madre Tierra, marcha de la comida, pueblos indígenas
Slug: marcha-de-la-comida-una-apuesta-del-pueblo-nasa-por-la-defensa-del-territorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Marcha-de-la-Comida-pño.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Marcha-de-la-Comida.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Libertad para la Madre Tierra] 

Los próximos 25 y 26 de mayo se llevará a cabo la Segunda Marcha de la Comida, promovida por el Proceso de Liberación de la Madre Tierra. Esta Iniciativa esta conformada por un grupo de familias del pueblo Nasa del norte del Cauca, quienes desde 2015 decidieron comenzar a sembrar y repartir sus cosechas como respuesta a la proliferación de los cultivos de caña  y a los grandes ingenios del azúcar en el departamento.

Sek y Luna integrantes del Proceso de Liberación de la Madre Tierra,  señalan que esta segunda edición fue aplazada para mayo debido a los eventos de la Minga del suroccidente. Sin embargo, han logrado articularse diferentes sectores que ya han trazado una ruta que permitirá la repartición de la comida en diversas comunidades, **"la liberación de la Madre Tierra no es una lucha de nasas para nasas, las cosechas que se logran, se suman a las cosechas de los resguardos y se comparten con procesos de base a Manizales, Bogotá, Medellín y Cali"** agrega Sek.

### Las barreras de la Liberación de la Madre Tierra

De acuerdo con Sek y Luna, la liberación de la madre tierra está cercada por el Gobierno y una arremetida militar que cada mes, en promedio, hace un intento de desalojo donde llega el ESMAD, la Policía o el Ejército y destruyen  las cosechas. No obstante la respuesta de la Liberación es repeler esos ataques y reinstalar los cambuches.

Sek también se refirió a las dificultades que han surgido en este proceso, entre las que se encuentran la judicialización de integrantes de la Liberación de la Madre Tierra, el desprestigio que han sufrido por parte de algunos medios de comunicación, que afirman "que los indígenas están deteniendo el progreso de la región", y las amenazas que han recibido por parte de las Águilas Negras.

\[caption id="attachment\_67633" align="aligncenter" width="414"\]![Marcha de la Comida](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Marcha-de-la-Comida-pño.jpg){.wp-image-67633 .size-full width="414" height="591"} Foto: Libertad para la Madre Tierra\[/caption\]

### "Plata no hay, plátano sí"

"La liberación no tiene candidaturas, no anda buscando proyectos ni conversar con el Gobierno, le interesa conversar pueblo con pueblo" enfatiza Sek destacando la intención de crear tejidos sociales con otros colectivos. Además de repartir la comida como evento central pues existirán momentos de diálogo a los que han denominado "palabreo" para discutir de dónde vienen las cosechas que comerán aquel día y diversas muestras artísticas bajo el nombre de "desalambrarte".[(Lea también: Comunidades del Cauca realizarán Marcha de la Comida tras proceso de liberación de la madre tierra)](https://archivo.contagioradio.com/comunidades-del-cauca-realizaran-marcha-de-la-comida-tras-proceso-de-liberacion-de-la-madre-tierra/)

Respecto a los alimentos, los integrantes de este proceso indicaron que "durante la marcha se repartirá plátano, yuca, limones, naranjas, cilantro, todo lo que se da en tierra caliente, al hacerse una articulacion con otros resguardos ellos traerán productos de clima frío" afirmó Luna mientras Sek agregó que también llegarán de otros territorios "cebolla, maíz, yuca, fríjol y un producto al que le decimos la rascadera".

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F1732276980337038%2Fvideos%2F674229983005878%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La agenda, hora y lugares de reunión en la diferentes ciudades serán comunicadas a lo largo del fin de semana a través de[sus redes sociales.](https://www.facebook.com/Libertad-para-la-Madre-Tierra-1732276980337038/)

<iframe id="audio_36250118" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36250118_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
