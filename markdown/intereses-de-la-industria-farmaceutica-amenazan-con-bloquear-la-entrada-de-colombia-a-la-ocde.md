Title: Intereses de la industria farmacéutica amenazan con bloquear la entrada de Colombia a la OCDE
Date: 2018-04-28 06:31
Author: CtgAdm
Category: Mision Salud, Opinion
Tags: Acceso a los medicamentos, industria farmacéutica, OCDE
Slug: intereses-de-la-industria-farmaceutica-amenazan-con-bloquear-la-entrada-de-colombia-a-la-ocde
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/ocde.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div class="td-featured-image-rec">

<div class="td-post-featured-image">

###### Foto: Especial 

</div>

</div>

###### 28 Abr 2018 

#### Por **[Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)** 

[Es de conocimiento general que el presidente Juan Manuel Santos tiene como meta dejar a Colombia dentro de la **OCDE** antes de terminar su mandato.]

[Recientemente la actual ministra de comercio, doctora María Lorena Gutiérrez, recibió una carta del Representante Comercial de los Estados Unidos (su homólogo estadounidense) en la que le expresa las preocupaciones de la industria farmacéutica de ese país con respecto a las medidas tomadas por Colombia en pro del acceso a los medicamentos. Le solicita abrir la comunicación para escuchar a la industria farmacéutica antes de que ellos, como Estado miembro de la OCDE, se manifiesten con respecto al estado de la solicitud de membrecía de Colombia a esta organización.]

[30 organizaciones de la sociedad civil de Colombia, Estados Unidos y Europa, se han aliado para solicitar a este ente del gobierno de los Estados Unidos detener las presiones ejercidas en nombre de la industria farmacéutica, y le instamos priorizar la salud de nosotros los ciudadanos colombianos, quienes, con nuestros aportes, sustentamos mes a mes el sistema de salud nacional.]

[A continuación compartimos con ustedes el comunicado de prensa publicado en los Estados Unidos y la carta enviada.]

**Más de 30 organizaciones de la sociedad civil al USTR: Dejen de amenazar con bloquear a la entrada de Colombia a la OCDE por los intereses de la industria farmacéutica y de biotecnológicos**[  
]

[Una amplia coalición internacional de más de 30 organizaciones de derechos humanos, incluyendo algunas de Estados Unidos y América Latina, se ha unido para garantizar que el acceso a la atención médica no se realice a expensas de la salud de la economía colombiana. Colombia está tratando de unirse a la Organización para la Cooperación y el Desarrollo Económicos (OCDE), pero los esfuerzos recientes para ampliar el acceso a medicamentos asequibles en el país han provocado objeciones por parte de poderosos grupos de interés y del propio gobierno de Estados Unidos. Específicamente, Colombia se ha convertido en el blanco de las presiones por explorar una licencia obligatoria para el medicamento contra el cáncer Gleevec® (Glivec® en Colombia) antes de optar por una reducción de precios unilateral, y también por su vía biosimilar, entre otros esfuerzos legales y regulatorios. El Ministerio de Salud de Colombia también está considerando una solicitud de la sociedad civil para obtener licencias obligatorias para los costosos medicamentos para el Virus de la Hepatitis C.]

[Estas preocupaciones han llegado a la oficina del Representante Comercial de los Estados Unidos y la Cámara de Comercio de los EE. UU., quienes han estado presionando a Colombia para que ceda a los intereses de las compañías farmacéuticas si desea unirse a la OCDE. En una carta del 14 de febrero del Representante de Comercio de los EE. UU., el embajador Robert Lighthizer, a la Ministra de Comercio, Industria y Turismo de Colombia, María Lorena Gutiérrez, Lighthizer dijo que el USTR esperaba que Colombia contactara a los "interesados" de la industria farmacéutica con una "centrada y sostenida campaña de divulgación y escucha” antes de que los EE. UU. brinden una opinión positiva sobre el estado de membresía del país en la OCDE.]

[[Andrew S. Goldman]](https://twitter.com/ASG_KEI)[, abogado de políticas y asuntos legales de][[Knowledge Ecology International]](https://twitter.com/KEI_DC)[, dijo:  
Esta es al menos la segunda vez que las grandes compañías farmacéuticas han intentado recientemente vincular políticas no relacionadas con los precios de los medicamentos, para hacer de la política exterior estadounidense un brazo de la industria farmacéutica. Nuestra relación y nuestra solidaridad con el pueblo de Colombia no deben definirse por los intereses de la industria farmacéutica y su búsqueda interminable de precios cada vez más altos para medicamentos que extienden la vida y que salvan vidas.]

[[Andrea Carolina Reyes Rojas,]](https://twitter.com/AndreaCReyesR)[ de][[Misión Salud]](https://twitter.com/MisionSaludCo)[, Colombia, ofreció la siguiente declaración:]

[Los esfuerzos de Colombia para aumentar el acceso a los medicamentos están totalmente alineados con el compromiso del país con la Cobertura Universal de Salud (objetivo 3.8 de los ODS) y con la obligación del país de proteger, respetar y cumplir el derecho humano a la salud reconocido en los acuerdos internacionales (Observación general del CESCR No. 14, párrafo 33) y en las leyes nacionales. Como ciudadanos, hemos fomentado estos esfuerzos durante mucho tiempo y comprendemos que formar parte de la OCDE significaría tener una política más amplia y más sólida que favorezca el acceso a los medicamentos, ya que esa organización promueve políticas "que mejorarán el bienestar económico y social de personas en todo el mundo."] **Consideramos imperativo que el USTR escuche y entienda la importancia de la convocatoria del UNHLP sobre Acceso a Medicamentos y, en** **consecuencia, deje de ejercer presión política y económica sobre los gobiernos para que renuncien al uso de las flexibilidades y comienza a ser una voz que prioriza la salud de las** **personas en todo el mundo.**

[Hacer parte de la OCDE le daría a Colombia una voz en las negociaciones con los países poderosos que representan la mayoría del mercado global. La organización ofrece un foro para que los gobiernos trabajen juntos para resolver problemas comunes y para establecer estándares que podrían convertirse en recomendaciones de políticas para todos los países miembros. Crucial para la economía de Colombia, la membresía de la OCDE hace que un país sea mucho más atractivo para los posibles inversionistas.]**En lugar de castigar a Colombia por priorizar el acceso por encima de los intereses de las empresas farmacéuticas, los esfuerzos de Colombia para ampliar el acceso a medicamentos asequibles deberían ser un ejemplo para los Estados miembros de la OCDE.**

[[Leonardo Palumbo]](https://twitter.com/yankeeu)[, Asesor de Defensa de los Estados Unidos, Campaña de Acceso de Médicos Sin Fronteras / Médecins Sans Frontières (MSF), ofreció el siguiente comentario: La amenaza del USTR para obstaculizar el acceso de Colombia a la OCDE es el último ejemplo de presión inapropiada por parte del gobierno de los EE. UU. a petición de las corporaciones farmacéuticas - presión que ayuda a las empresas y perjudica a las personas que necesitan medicamentos que salvan vidas. Colombia no debería tener que responder ante los Estados Unidos o la industria farmacéutica por el uso de medidas legales permitidas por las normas de comercio internacional para abordar las necesidades de salud pública de sus ciudadanos. En un momento en que los altos precios de los medicamentos son una barrera a nivel mundial, EE. UU. debería alentar, no socavar, a los países que toman medidas para ayudar a las personas a comprar los medicamentos que necesitan para vivir una vida sana y productiva.]

[La coalición insta al USTR a dejar de promover los intereses de las corporaciones farmacéuticas a expensas de la salud de las personas y los derechos legales de Colombia para aumentar el acceso a medicamentos asequibles.]

[[Peter Maybarduk]](https://twitter.com/Maybarduk)[, director del programa de Acceso a medicamentos de][[Public Citizen]](https://twitter.com/Public_Citizen)[, dijo:  
Es un grave error creer, como afirma el presidente Trump, que los precios más altos de los medicamentos en el extranjero llevan a precios más bajos en el país. La administración Trump culpa a los otros países por los precios altos de los medicamentos de los EE. UU. pero el único medio eficaz de reducir los precios de los medicamentos en el país es bajando los precios de los medicamentos en el país. La presión de Washington sobre otros países para elevar los precios de los medicamentos podría provocar la muerte de muchos, mientras se pierde una oportunidad histórica de hacer que los medicamentos sean asequibles para todos.]

[\*\*\*]

###### [Comunicado de prensa:][[Más-de-30-organizaciones-de-la-sociedad-civil-al-USTR\_]](http://blogs.eltiempo.com/medicamentos-sin-barreras/wp-content/uploads/sites/754/2018/03/M%C3%A1s-de-30-organizaciones-de-la-sociedad-civil-al-USTR_.pdf) 

###### [Versión en español de carta enviada al USTR:][[Civil-Society-Letter-to-USTR-re-Colombia-OECD-March20-2018-SpanishVersion]](http://blogs.eltiempo.com/medicamentos-sin-barreras/wp-content/uploads/sites/754/2018/03/Civil-Society-Letter-to-USTR-re-Colombia-OECD-March20-2018-SpanishVersion.pdf) 

###### [Versiones en inglés: ][[https://www.keionline.org/27369]](https://www.keionline.org/27369) 

#### **[Leer más columnas de Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)**

------------------------------------------------------------------------

#### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
