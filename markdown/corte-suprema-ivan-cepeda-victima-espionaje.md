Title: Iván Cepeda es víctima de espionaje militar: Corte Suprema
Date: 2020-09-28 11:46
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Corte Suprema de Justicia, Ejército Nacional, Iván Cepeda, Perfilamientos ilegales
Slug: corte-suprema-ivan-cepeda-victima-espionaje
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Ivan-Cepeda.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Iván Cepeda

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**El senador Iván Cepeda** informó este lunes que la Corte Suprema de Justicia confirmó que su nombre figura entre las personas víctimas de inteligencia y seguimientos ilegales a periodistas, políticos, abogados y defensores de derechos humanos que se habrían hecho desde varias unidades militares dados a conocer en mayo de este año.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la Corte Suprema, el nombre del senador Iván Cepeda aparece en un archivo denominado **“casos especiales’ ‘trabajos especiales’”**, donde se relaciona su nombre de manera recurrente asociado a un análisis de “nodos y nexos”. Otros congresistas como Roy Barreras y Antonio Sanguino ya habían alertado sobre la existencia de operaciones para realizar interceptaciones y seguimientos de forma ilegal.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**“He sido oficialmente informado por la Corte Suprema de Justicia que ha sido localizada una carpeta dentro de la investigación por los llamados perfilamientos realizados por el Ejército Nacional en la que aparece recurrentemente mencionado mi nombre”**, aseguró Iván Cepeda. [(Le recomendamos leer: Espionaje ilegal del Ejército de Colombia apunta a periodistas y Defensores de DDHH)](https://archivo.contagioradio.com/espionaje-ilegal-del-ejercito-de-colombia-apunta-a-periodistas-y-defensores-de-ddhh/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La decisión de la Corte, es la respuesta a un derecho de petición que había sido radicado ante esta instancia y que obedece a una serie de medidas que habían sido anunciadas desde el mes de mayo como un**a denuncia penal contra Rafael Nieto Loaiza**, congresistas del Centro Democrática por ser, presuntamente, quien recibía la información de las interceptaciones, una comunicación dirigida a la Corte Penal Internacional informando la situación y una carta al Congreso de EE.UU. para hacer un seguimiento a los recursos dados por cooperación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Interceptaciones se dan en medio del caso Uribe, señala Cepeda

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Iván Cepeda señala que dichos seguimientos obedecen a los debates de control político que ha hecho "con relación a la actividad en el Ejército de un sector corrupto y de crímenes que se han perpetrado desde ese sector de la institución", de igual forma señala que todas **aquellas expresiones de presiones y amenazas han surgido en torno al proceso por el cual se investiga al exsenador y expresidente Álvaro Uribe**. [(Lea también: Iván Cepeda recusará a fiscal y llevará caso Uribe a instancias internacionales)](https://archivo.contagioradio.com/ivan-cepeda-recusara-a-fiscal-y-llevara-caso-uribe-a-instancias-internacionales/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1310556977861472256","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1310556977861472256

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras dicha investigación también es adelantada por la Fiscalía, de igual forma, la Procuraduría General abrió el pasado 20 de mayo un juicio disciplinario contra 13 militares incluidos dos generales en retiro adscritos a la Jefatura del Estado Mayor Operaciones, el Comando de Apoyo Combate Inteligencia Militar, Comando Brigada de Inteligencia Militar, y los batallones de Ciberinteligencia, inteligencia Militar Estratégico \#4, y de Contrainteligencia de Seguridad de la Información.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que esta revelación llevó al retiro de varios oficiales y suboficiales entre ellos Juan Pablo Prado Torres, Helmont René Ramos Naranjo, Hugo Armando Díaz Hernández., Julio Tobías López Cuadros y Milton Eugenio Rozo Delgado, aún no hay acciones de fondo para impedir que este tipo de hechos se repitan. [(Lea también: No avanza investigación por interceptaciones ilegales del ejército a 4 meses de la denuncia)](https://archivo.contagioradio.com/no-avanza-investigacion-por-interceptaciones-ilegales-del-ejercito-a-4-meses-de-la-denuncia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hace tan solo unas semanas, la [Fundación para la Libertad de Prensa (FLIP)](https://twitter.com/FLIP_org)expresó su preocupación pues siguen sin proporcionarse garantías para la libertad de prensa ni para la protección a las fuentes ante los pocos avances que se han evidenciado casi cuatro meses desde que se revelaron las operaciones de espionaje ilegales. [(Lea también: Prensa bajo la mira de actividades ilegales del ejército en Colombia)](https://archivo.contagioradio.com/prensa-bajo-la-mira/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Otros casos de interceptaciones y seguimientos previos

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Durante el mandato de Álvaro Uribe, el Departamento Administrativo de Seguridad (DAS) fue otra de las instituciones cuestionadas, debido al uso de sus instalaciones, personal y tecnología para el espionaje a periodistas, defensores de derechos humanos, políticos y organizaciones sociales bajo la dirección de Jorge Noguera y María del Pilar Hurtado hoy condenados por dichos seguimientos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Posteriormente en medio del proceso de paz; allanamientos de la Fiscalía dieron a conocer la **operación Andrómeda** y el funcionamiento de una central de inteligencia militar donde trabajaban hackers con la función de intervenir las comunicaciones de los integrantes del equipo negociador de la entonces guerrilla FARC y del equipo del gobierno nacional, encabezado por Sergio Jaramillo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A tales hechos se suman las denuncias de organizaciones como **la Comisión de Justicia y Paz y la Comisión Colombiana de Juristas (CCJ)** que desde el año pasado y enero del 2020 han expresado ser víctimas de la vigilancia de drones que han merodeado en sus instalaciones. Estos hechos también se dieron tan solo semanas después que se diera a conocer cómo el Ejército utilizó un sofisticado sistema de interceptaciones a través de dos de las oficinas de inteligencia y contra inteligencia, durante el segundo semestre de 2019.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
