Title: La sociedad ‘del Animal’
Date: 2017-03-31 11:52
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Cine Colombiano, La mujer del animal, Victor Gaviria, Violencia de género
Slug: la-sociedad-del-animal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/La-Mujer-Del-Animal-1-e1490723821769.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: La Mujer del Animal 

##### Por: Jake Sebastián Estrada Charris 

Podemos calificar como violenta o desgarradora a ‘La Mujer del Animal’, la más reciente película de Víctor Gaviria, porque efectivamente cumple con todos los requisitos para serlo. Pero más allá de eso, la cinta colombiana se preocupa por denunciar problemas como la violencia sexual, la marginalidad y el funcionamiento arcaico de una sociedad particularmente machista.

Aunque parezca imaginado por la más retorcida de las mentes, el filme cuenta una historia real sucedida en Medellín durante la década del 70, en donde una joven es forzada a vivir con un hombre violento al que toda la comunidad conoce como ‘El Animal’. Podríamos pensar que aunque impactante, esta podría ser una historia aislada; sin embargo, las cifras plantean lo contrario:

La Organización Mundial de la Salud señala que “alrededor de una de cada tres (35%) mujeres en el mundo han sufrido violencia física y/o sexual de pareja o violencia sexual por terceros en algún momento de su vida”. Por otra parte y de acuerdo a un informe de 2016 del Instituto Nacional de Medicina Legal, durante enero y agosto de ese año, se registraron 116 casos diarios de violencia contra la mujer en Colombia.

![La Mujer Del Animal](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/La-Mujer-Del-Animal-2-e1490724421707.jpg){.alignnone .size-full .wp-image-38493 width="900" height="583"}

Otro de los puntos importantes que aborda ‘La Mujer del Animal’ es el que retrata a un conjunto de personas que prefieren quedarse en silencio gracias a la cultura del miedo; eligen no denunciar por distintos motivos: abandono del Estado, ausencia de las instituciones y la propagación de la violencia física y psicológica. Todas las anteriores son problemáticas que en Colombia existen en zonas periféricas y rurales del país; inclusive, al interior de las ciudades principales del territorio.

En su amplia carrera en el cine, Víctor Gaviria se ha encargado de poner en evidencia a generaciones en busca de identidad (Rodrigo D: No Futuro - 1990); a infancias permeadas por la droga (La Vendedora de Rosas - 1998) o a la fascinación de la clase media por el mundo del narcotráfico (Sumas y Restas - 2005). No obstante, ‘La Mujer del Animal’ puede ser fácilmente su largometraje más crudo, ya que se abordan temas tan sensibles como la violencia de género y el abuso sexual con el uso de un lenguaje simbólico que trasciende las fronteras de la temporalidad.

<iframe src="https://www.youtube.com/embed/n3y0dj-UDMA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Gracias al brillante trabajo de los actores naturales, encabezado por la magnífica interpretación de Natalia Polo (en el papel de Amparo), quien es acompañada por todo un elenco de personajes reales que son el complemento histriónico ideal para una película que logra despertar sentimientos como la ironía, la rabia y la desesperación.

La estética y cinematografía tan bien lograda de la cinta hacen que la miseria sea el hilo conductor de un relato que durante algunos minutos se confunde con una historia de terror. ‘La Mujer del Animal’ garantiza ser una película que plantea demasiadas preguntas, así como respuestas tan desoladoras como su argumento; es por eso que vale la pena verla y apoyarla, teniendo en cuenta que se encuentra exhibida en reducidas salas de cine.

Nota final: Mientras veía ‘La Mujer del Animal’ en el cine, un grupo de muchachos no paraba de reírse en pasajes de la película que no daban pie ni para la más leve de las sonrisas. No estoy diciendo que haya visto la cinta junto a un grupo de sádicos o amantes de la violencia; sino que me impresionó el nivel de indiferencia que tuvieron al ser testigos de una historia tan horrible. Tal vez esa actitud de indolencia sea la que tomamos siempre que nos topamos ante una verdad incómoda y es esa apatía la que debemos dejar a un lado ante temas tan sensibles y trascendentes como este porque simplemente creemos que no nos toca, cuando la verdad plantea que sucede todo lo contrario.

Le puede interesar: [Las mujeres ecuatorianas y el derecho a decidir sobre su sexualidad](https://archivo.contagioradio.com/mujeresviolenciaabortoecuador/)
