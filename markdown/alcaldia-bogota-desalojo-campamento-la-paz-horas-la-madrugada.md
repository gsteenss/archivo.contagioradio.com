Title: Alcaldía de Bogotá desalojó campamento por la paz en horas de la madrugada
Date: 2016-11-19 13:51
Category: DDHH, Nacional
Tags: acuerdo de paz, Alcaldía de Bogotá, FARC, paz
Slug: alcaldia-bogota-desalojo-campamento-la-paz-horas-la-madrugada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/campamento-3-e1479581388459.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [19 Nov 2016] 

<div class="col-620 cont">

<div class="vworldtop cont">

<div class="capworldtop">

Sobre las dos de la mañana los integrantes del campamento por la paz, **fueron desalojados por parte de la policía por orden de la Alcaldía de Bogotá**. Así lo denunciaron este sábado los activistas que desde el 5 de octubre permanecían en carpas pacíficamente en la Plaza de Bolívar, exigiendo un acuerdo entre el gobierno colombiano y la guerrilla de las FARC.

Según la denuncia, en la madrugada del sábado, cerca de 300 agentes de la Policía Nacional y miembros del Escuadrón Móvil Antidisturbios, ESMAD, irrumpieron y desalojaron violentamente el campamento por la paz.

</div>

</div>

</div>

<div class="col-six">

<div class="cont bordercountry">

<div class="dest cont">

<div class="txt_newworld">

**“La Policía rodeó la plaza y no pudimos volver a entrar. Desmontaron el campamento y quitaron todas las cosas que estaban ahí. Han metido unos buses y dicen que van a montar ahí a la gente”,** expresó Carolina Osorio, vocera del campamento.

Asimismo participantes de campamento aseguraron que fueron desalojados con golpes por parte de la policía y el ESMAD. Uno de los integrantes dijo que policías del ESMAD le atacaron desprevenido "me reventaron la boca y me golpearon en repetidas ocasiones la cabeza, si no me hubiese protegido con un casco me abren la cabeza". Mientras tanto, el concejal Marco Fidel Ramírez, aseveró ante el Concejo que "esa actividad es promovida por las FARC-EP".

Por su parte un comunicado del campamento exigió a la Administración de Enrique Peñalosa explicar las razones por las que "se ordenó el levantamiento de una iniciativa de movilización pacífica, **con cerca de 300 efectivos del ESMAD** de acuerdo con la información aportada por los presentes, cuando por sus características, no hay evidencia de que la misma supusiera riesgos contra la vida humana, la salud pública, la seguridad alimentaria, el medio ambiente o el derecho al trabajo del resto de la ciudadanía Bogotana".

Algunos integrantes del campamento interpusieron acciones iniciaron acciones de denuncia contra este acto de brutalidad policial, pero hasta el momento los entes correspondientes no han iniciado las respectivas investigaciones.

\

</div>

</div>

</div>

</div>
