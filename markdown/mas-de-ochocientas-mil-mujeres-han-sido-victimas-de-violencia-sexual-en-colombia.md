Title: Más de ochocientas mil mujeres han sido víctimas de violencia sexual en Colombia
Date: 2017-08-17 14:31
Category: Mujer, Nacional
Tags: mujeres, Saquen mi cuerpo de la guerra, violencia sexual
Slug: mas-de-ochocientas-mil-mujeres-han-sido-victimas-de-violencia-sexual-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Casa Amazonía] 

###### [17 Ago. 2017]

Más de ochocientas mil mujeres siguen siendo agredidas a través de la violencia sexual, Medellín es la ciudad donde más se presenta este fenómeno. Así lo asegura la más reciente **Encuesta de Prevalencia de Violencia Sexual en contra de las mujeres realizada por 13 organizaciones** en 29 departamentos y en la que participaron mujeres de 15 a 44 años.

“Los resultados realmente son muy preocupantes, para los 142 municipios entre 2010-2015, fueron ochocientos setenta y cinco mil cuatrocientos treinta y siete **(875.437) mujeres las que dijeron haber sido víctimas de algún tipo de violencia sexual” dijo Olga Amparo Sánchez de la Casa de la Mujer.**

Las ciudades más críticas son Medellín con 35.4% casos de violencia sexual, Buenaventura con un 30.6% y Bogotá con 30.2%. Le siguen Cali, Sincelejo, Cucutá, Riohacha y Santander de Quilichao.

“Quienes tienen mayor situación de vulnerabilidad son las mujeres afrodescedientes entre 24 y 35 años. Eso nos habla que además hay una situación de discriminación”.

Fueron **1900 encuestas realizadas casa a casa en 29 departamentos** y en ella se preguntó sobre diversos tipos de violencia, a qué tipo de violencias las mujeres han estado expuestas, por qué no denuncian, qué tipo de armas fueron usadas y si las mujeres consideraban que la violencia sexual contra las mujeres sí era un delito.

### **¿Qué actores son responsables de la violencia sexual contra las mujeres?** 

Para salvaguardar la vida de las mujeres que participaron en la encuesta no se les preguntó si habían sido víctimas de violencia por parte de paramilitares, sin embargo, dice Sánchez que para la encuesta se consideró a actores armados como paramilitares e insurgencia.

“La encuesta se hizo casa a casa y era un elemento de seguridad para las mujeres no preguntar por el actor, porque **buenamente en su casa puede vivir un paramilitar o un actor ilegal”**. Le puede interesar: [Violencia sexual más allá de la agresión](https://archivo.contagioradio.com/violencia-sexual-mas-alla-de-la-agresion/)

Además, se preguntó por la Fuerza Pública y el Actor Privado, este último entendido como la pareja permanente de la mujer o un familiar. La encuesta arrojó que **quien más agrede o violenta a las mujeres es el actor privado, seguido de los actores armados ilegales, la Fuerza Pública** y por último actores no identificados.

“Que el compañero o la pareja sea el actor que más violenta a la mujer es crítico. La Corte Constitucional claramente ha dicho a través del Auto 009 del 2014 que no importa si el actor era armado o no, pero si era en el marco del contexto del conflicto armado se tenía que considerar violencia sexual”.

### **Estado sigue sin responder a la situación de violencias contra las mujeres ** 

Luego de esta encuesta, Sánchez asegura que **hay un Estado que no está respondiendo a las mujeres y que no está sancionando.** La encuesta asegura que el 51% de las mujeres instauró una denuncia en la Fiscalía, el 18,5% en la Comisaría de Familia, el 15% en la Inspección de policía y el 5,3% en la Defensoría del Pueblo.

**“Hay mujeres que no ponen la denuncia porque no creen en la institucionalidad, porque cuando llegan a la institución las re-victimizan** o porque prefieren dejar las cosas así porque no quieren que otra persona se enteren”. Le puede interesar: [Ante la CIDH se denunció 98% de impunidad en casos de violencia sexual en Colombia](https://archivo.contagioradio.com/ante-la-cidh-se-denuncio-98-de-impunidad-en-casos-de-violencia-sexual-en-colombia/)

### **Hay que sacar el cuerpo de las mujeres de la guerra** 

Desde la campaña Violaciones y otras violencias: Saquen mi cuerpo de la guerra seguirán mostrando la magnitud de este problema y **exigiendo a las respectivas instituciones del Estado acciones efectivas y medidas que propendan por la prevención**, protección, investigación, sanción y garantías de no repetición para las mujeres víctimas de violencia sexual.

“La responsabilidad fundamental está en el Estado. En las mujeres la responsabilidad está en denunciar los hechos, en exigir que se cumpla la ley. El Estado debe comprometerse no solamente con la prevención de las violencias contra las mujeres víctimas, sino con generar condiciones para que estas situaciones no se repitan”. Le puede interesar: [Organizaciones de mujeres proponen '5 claves' para enfrentar la violencia sexual](https://archivo.contagioradio.com/organizaciones-de-mujeres-proponen-5-claves-para-enfrentar-la-violencia-sexual/)

<iframe id="audio_20389657" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20389657_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
