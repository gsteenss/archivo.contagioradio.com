Title: ¿Qué pasará con las consultas populares en Colombia?
Date: 2018-07-19 16:32
Category: Ambiente, Movilización
Tags: consulta popular, Corte Constitucional, Cristina Pardo, Cumaral
Slug: que-pasara-con-las-consultas-populares-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/13723966_1624289817860888_6187921756914590328_o-e1532036312429.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tolima Somos Todos] 

###### [19 Jul 2018] 

Con preocupación recibieron ambientalistas y comunidades en dónde se realizaron consultas populares, la ponencia de la magistrada Cristina Pardo que pretendería dejar como último mecanismo de acción la consulta popular. Sin embargo, de acuerdo con Luis Ramírez, abogado asesor de la consulta popular de Cumaral, el Estado debe entender que **“las consultas populares son el grito de las comunidades inconformes con las políticas del país”.**

Esta ponencia es presentada por la empresa Mansarovar Energy, luego de que se realizara la consulta popular en Cumaral y ganará la voluntad de la comunidad porque no se realicen actividades de exploración o explotación de hidrocarburos o minería en su territorio. (Le puede interesar: ["No a la minería en Cajamarca: CorTolima"](https://archivo.contagioradio.com/no-mineria-en-cajamarca-cortolima/))

En ese sentido Ramírez afirmó que espera que la decisión que tomará la Corte Constitucional, la próxima semana, tenga en cuenta los antecedentes jurisprudenciales de este espacio que **durante 25 años ha defendido el estado ecológico colombiano**, los mecanismos de participación ciudadana y el valor de la soberanía popular.

Además, hay otro precedente jurisprudencial de la Corte que señala que una tutela no es el mecanismo para invalidar las decisiones soberanas del pueblo. Sin embargo, hizo un llamado al **gobierno colombiano para que acate las diferentes sanciones que ya ha dictado la Corte frente a las actividades minero energéticas.**

De igual forma señaló que espera que la Corte Constitucional también se pronuncie frente a las diferentes tutelas que se han puesto por la falta de recursos para realizar las consultas populares en los municipios que así lo han solicitado para que por fin haya una respuesta sobre **quiénes deben responder por la falta de financiación de este mecanismo democrático.**

Frente a las inquietudes de los habitantes de Cumaral, Ramírez expresó que la comunidad le ha manifestado su preocupación intentado establecer qué pasará ahora con la decisión que tomaron el pasado 4 de junio de 2017, no obstante, el abogado afirmó que hay que esperar el fallo de la Corte para saber cómo proceder.

<iframe id="audio_27145288" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27145288_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
