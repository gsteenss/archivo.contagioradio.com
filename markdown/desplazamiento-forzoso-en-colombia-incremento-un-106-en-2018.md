Title: Desplazamiento forzoso en Colombia incrementó un 106% en 2018
Date: 2018-12-28 18:21
Author: AdminContagio
Category: DDHH, Nacional
Tags: ACNUR, Cauca, Chocó, Desplazamiento Forzoso, Norte de Santander
Slug: desplazamiento-forzoso-en-colombia-incremento-un-106-en-2018
Status: published

###### Foto: Archivo 

###### 28 Dic 2018 

[A través de un informe, el  Alto Comisionado de las Naciones Unidas para los Refugiados (ACNUR) reveló que más de 30.500 personas han sido desplazadas forzosamente de sus territorios entre enero y noviembre de 2018 como consecuencia del conflicto armado en Colombia.]

[Una de las causas de este incremento en el desplazamiento forzoso contra las poblaciones ha sido el vacío de poder dejado tras la salida de las Farc de los territorios, lo que ha llevado  a que diferentes grupos al margen de la ley se disputen el control de dichas zonas; según el Comité Internacional de la Cruz Roja (CICR) existen al menos cinco nuevos conflictos armados que han afectado directamente a las comunidades a lo largo del país.   ]

[Además de los nuevos conflictos registrados por el CICR, también se identificó otro tipo de agresiones que aunque no alcanzan el umbral del conflicto armado sí representan un riesgo para la población y su acceso a oportunidades, como es el caso del uso de la fuerza o la violencia urbana. [(Le puede interesar: En Colombia hay cinco conflictos armados: CICR)](https://archivo.contagioradio.com/colombia-conflictos-armados-cicr/)]

[De igual forma, el **Consejo Noruego para los Refugiados (CNR)** publicó en agosto un informe que registró 19.220 personas desplazadas en Colombia  durante el primer semestre de 2018, una suma que representa el 106% más en comparación con el mismo periodo del 2017, año en el que 9.075 colombianos fueron desalojados de sus tierras.[(Le puede interesar 429 indígenas han sido desplazados en el Chocó en los últimos dos meses) ](https://archivo.contagioradio.com/439-indigenas-han-sido-desplazados-en-el-choco-en-los-ultimos-dos-meses/)]

[Según la entidad, Norte de Santander con especial énfasis en la región del Catatumbo, Antioquia  en la región del Bajo Cauca, Chocó y Nariño en la región Pacífico han sido los departamentos más afectados por este fenómenos, pues fue en estas regiones donde se identificó un 95% de los desplazamientos.]

######  Reciba toda la información de Contagio Radio en [[su correo]
