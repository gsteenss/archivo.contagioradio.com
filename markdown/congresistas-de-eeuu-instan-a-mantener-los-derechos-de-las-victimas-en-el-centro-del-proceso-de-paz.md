Title: Congresistas de EEUU instan a mantener los derechos de las víctimas en el centro del proceso de paz
Date: 2015-08-04 17:21
Category: Nacional, Paz
Tags: Congreso de Estados Unidos, Conversaciones de paz de la habana, Delegados de paz de las FARC, ELN, FARC, James McGovern, Juan Manuel Santos
Slug: congresistas-de-eeuu-instan-a-mantener-los-derechos-de-las-victimas-en-el-centro-del-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/congreso-eeuu-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: opi97 

###### [4 Agosto 2015] 

Una carta publicada este 3 de Agosto y suscrita por **65 congresistas de Estados Unidos,** liderados por el congresista McGovern insta a la mesa de conversaciones de paz de la Habana a que **se mantengan los derechos de las víctimas en el centro de las negociaciones**, y a que los acuerdos finales no contribuyan a reforzar la historia y cultura de impunidad en el país. También reconocieron los avances de los últimos días y saludan el acuerdo de des escalamiento del conflicto.

En el comunicado también reconocieron que los puntos que quedan por negociar son complejos pero reiteraron su apoyo a las partes “*para que estos últimos acuerdos sean inclusivos, en particular de los puntos de vista, necesidades y prioridades  de los sectores de la sociedad colombiana que más han sido afectados por el conflicto, pero a la vez los más excluidos de las decisiones sobre cómo **reparar el daño social, económico, político,  comunitario, de familia, emocional, y psicológico que han sufrido**.*”

Además, insisten en la necesidad de **acabar con la historia de impunidad en Colombia** “*deseamos expresar, en los términos más fuertes posibles, nuestra preocupación que el último acuerdo no contribuya a la larga y trágica historia de impunidad en  Colombia.*” y agregan que poner fin a la impunidad es fundamental para asegurar la garantía de no repetición.

Consulte la carta en Español

[Carta de Congresistas de Estados Unidos](https://es.scribd.com/doc/273549129/Carta-de-Congresistas-de-Estados-Unidos "View Carta de Congresistas de Estados Unidos on Scribd")

<iframe id="doc_18504" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/273549129/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
