Title: A dos debates de ser aprobado derecho fundamental al agua en Colombia
Date: 2016-11-16 16:16
Category: Ambiente, Nacional
Tags: derecho al agua, despojo de tierras, empresas extractivas
Slug: congreso-debera-respaldar-derecho-fundamental-al-agua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Arroyo-Bruno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Nov 2016] 

En su recta final entra el proyecto de ley que busca incluir dentro de la Constitución, un artículo nuevo que garantice el derecho humano al agua ** “de uso público esencial para la vida”. **De ser aprobado sería posible para las comunidades activar acciones jurídicas para defender la vida y el agua en sus territorios.

El proyecto, al que solo le faltan dos debates, uno en la **Comisión Primera de la Cámara el 30 de Noviembre y el otro en la Plenaria de la Cámara el 12 de Diciembre**, fue radicado por el **Senador de la Alianza Verde Jorge Prieto. **

Son seis los puntos fundamentales que plantea este proyecto en pro de la defensa del agua: **disponibilidad, calidad, accesibilidad física, accesibilidad económica, no discriminación o agua para todos y libre acceso a la información. **

### **¿Qué motivó este proyecto de Ley?** 

Prieto asegura que la iniciativa surge a raíz de la preocupación de las comunidades por los **cientos de ríos que se han secado en el país**, y del hecho que en los lugares más ricos en recursos hídricos, como los llanos orientales “hoy se tengan que hacer excavaciones para extraer agua subterránea y eso **sólo lo pueden hacer las grandes empresas y los terratenientes, despojando al campesino del derecho fundamental del agua”.**

Resalta que la dificultad del acceso al agua también ha ocasionado importantes **cambios en las prácticas culturales y de trabajo de la tierra en comunidades** indígenas, campesinas y afro en todo el territorio nacional. Le puede interesar: [Promover y  respetar el derecho humano al agua, tarea pendiente de Colombia.](https://archivo.contagioradio.com/promover-y-respetar-el-derecho-humano-al-agua-tarea-pendiente-de-colombia-yoreporto/)

El senador llama la atención sobre la **negligencia del Gobierno Nacional en casos como el de la Guajira y Caño Cristales** que está cerrado al turismo debido a que** la actividad petrolera irresponsable ha generado que sus aguas se sequen,**  comenta que “al concretarse el proyecto de ley toda empresa estará obligada a realizar **estudios correspondientes que determinen los daños que se van a causar para evitarlos”.**

Prieto manifiesta que otro de los objetivos de este proyecto es acercar las problemáticas del campo a quienes habitan la ciudad, ya que **“en las ciudades no se piensa en los nevados, los páramos y demás fuentes de agua que se están secando** (…) pasa lo mismo que con la guerra, como en las ciudades no se siente la guerra a la gente no le importa, no les toca”.

Concluye haciendo un llamado de atención sobre las políticas públicas del Gobierno, que **"ponen los intereses económicos de multinacionales extranjeras sobre los intereses de las comunidades que son quienes han levantado este país”.**

El Derecho al agua potable y al saneamiento fue **reconocido en 2010 por la Asamblea General de Naciones Unidas como derecho humano esencial** para poder disfrutar de la vida y de todos los derechos humanos.

### **Este es el proyecto de ley radicado por el senador Jorge Prieto** 

https://issuu.com/clopezsenadora/docs/pal\_agua\_derecho\_fundamental\_14\_de\_

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
