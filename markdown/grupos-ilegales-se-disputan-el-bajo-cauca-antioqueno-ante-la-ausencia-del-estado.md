Title: Grupos ilegales se disputan el Bajo Cauca Antioqueño ante la ausencia del Estado
Date: 2019-06-05 15:03
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Autodefensas Gaitanistas de Colombia, bajo cauca antioqueño, Caparrapos
Slug: grupos-ilegales-se-disputan-el-bajo-cauca-antioqueno-ante-la-ausencia-del-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Censat] 

La disputa entre Caparrapos y Autodefensas Gaitanistas de Colombia (AGC) por el dominio territorial del Bajo Cauca Antioqueño y el control de los corredores estratégicos del narcotráfico, la minería ilegal y la extorsión, continúa  arremetiendo contra los más de 312.000 habitantes de esta región de los cuales cerca de **68.726 personas**, son reconocidos como víctimas del conflicto armado según el **Registro Único de Víctimas.**

El más reciente de estos ataques se dio el pasado 3 de junio cuando hombres armados y vestidos de negro llegaron hasta el corregimiento El Guaimaro, Cáceres y asesinaron a cuatro personas. Según **Óscar Zapata, integrante de la Coordinación Colombia Europa Estados Unidos**, aunque aún se desconocen los responsables, esta es una de las zonas bajo el dominio de Los Caparrapos y donde las AGC o el Clan del Golfo, habrían atacado a modo de retaliación, atentando contra la población que "como siempre está en medio de la confrontación y es la que más afectaciones sufre".

Reflejo de este enfrentamiento también son los sucesos que ocurrieron en **Tarazá** el pasado 17 de mayo cuando una persona murió y otras siete resultaron heridas comoc consecuencia de la explosión de una granada en el corregimiento El Doce.  [(Le puede interesar: Al Bajo Cauca no llega la sustitución pero si la extorsión y el desplazamiento)](https://archivo.contagioradio.com/al-bajo-cauca-no-llega-la-sustitucion-pero-si-la-extorsion-y-el-desplazamiento/)

Pese a que la **Fuerza de Tarea Aquiles** del Ejército, compuesta por más de 4.500 hombres, hace presencia en el Sur de Córdoba y el Bajo Cauca, de la que hacen parte los municipios de El Bagre, Nechí, Cáceres, **Caucasia, Tarazá y Zaragoza**, "El Bajo Cauca experimenta un fuerte grado de ingobernabilidad", sentenció Zapata quien agregó que aquí "no manda el Estado, mandan las estructuras paramilitares que buscan tomarse el poder". [(Lea también: Fuerza de Tarea Aquiles no evitó desplazamiento de más 120 familias en Bajo Cauca](https://archivo.contagioradio.com/fuerza-de-tarea-aquiles-no-evito-desplazamiento-de-mas-120-familias-en-bajo-cauca/)

El defensor de derechos también expresó su preocupación pues de cara a las elecciones locales en la región, es probable que los grupos que se disputan el territorio busquen ubicar funcionarios en las alcaldías y gobernaciones afines a sus objetivos. Zapata mencionó que en el Bajo Cauca también hay presencia del ELN y las disidencias de las FARC, sin embargo prevalecen en una menor proporción. [(Lea también: Se libra una guerra por tomar el control del Nudo de Paramillo)](https://archivo.contagioradio.com/guerra-control-nudo-paramillo/)

<iframe id="audio_36740200" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36740200_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
