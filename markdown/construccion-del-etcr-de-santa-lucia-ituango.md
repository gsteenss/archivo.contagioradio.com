Title: 500 hectáreas para la construcción del ETCR de Santa Lucía, Ituango
Date: 2019-12-13 17:40
Author: CtgAdm
Category: Nacional, Paz
Tags: acuerdos de paz, Antioquia, etcr, FARC-EP, Ituango, pastor alape, Vereda Santa Lucía
Slug: construccion-del-etcr-de-santa-lucia-ituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/ETCR-Vereda-Santa-Lucía-Ituango-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: DiPaz] 

[El Espacio Territorial de Capacitación y Reincorporación (ETCR) de Santa Lucía, Ituango es una realidad. Este 12 de diciembre **se firmó el contrato de arrendamiento de un predio de 500 hectáreas para la construcción y realización del (ETCR),** esto con el fin de generar un espacio de trabajo y reconciliación manejado por los excombatientes de las FARC-EP alojados en la región.]

[El predio fue cedido en arrendamiento a la Cooperativa Multiactiva De Emprendedores Del Campo Colombiano, organización integrada por los excombatientes. Este trabajo se logró gracias a la gestión de organizaciones como la Confraternidad Carcelaria de Colombia y Alemania, junto a la veeduría del Diálogo Intereclesial por la Paz de Colombia (DiPaz), entre otros.][(Lea también: Gobierno apoya los proyectos individuales pero no los colectivos en los ETCR)](https://archivo.contagioradio.com/gobierno-apoya-los-proyectos-individuales-pero-no-los-colectivos-en-los-etcr/)

### **ETCR de Santa Lucía: un lugar para sanar y construir** 

[John William Taborta, quien ha liderado la historia de compra de terrenos y procesos en el territorio, demostró su alegría frente al proceso realizado hasta el momento e invitó a las personas de la región para que visiten y apoyen el proyecto. **“Yo quiero hacer una  invitación para que vengan a conocer nuestro territorio.** Un territorio que nos permitirá crear un arraigo, una estabilidad para la finalidad de la implementación de los acuerdos de paz firmados en la Habana”.]

[Por otro lado, Lácidez Hernández, de la Confraternidad Carcelaria de Colombia, mencionó la importancia de implementar el programa de Comunidades Restaurativas, iniciativa que busca resarcir el daño hecho por los ex combatientes a las víctimas, a partir de construcción de espacios, infraestructuras y diálogos de aceptación de culpas y perdón. ]

[Este plan, como menciona Hernández, “está inspirado en la experiencia de reconciliación que se llevó a cabo en Ruanda”. Para ello, se tomaron tres ejes de trabajo: reconciliación entre comunidad y excombatientes, reparación simbólica a partir de la construcción, y reintegración a la vida civil.][(Le puede interesar: ¿Existen garantías de seguridad para excombatientes que habitan los ETCR?)](https://archivo.contagioradio.com/existen-garantias-de-seguridad-para-excombatientes-que-habitan-los-etcr/)

[Para Tobias Merckle, otra de las voces que acompañaron este proceso desde la Confraternidad Carcelaria de Alemania, es importante **“llevar los procesos realizados en el exterior al contexto colombiano”.** Desde el trabajo realizado en las Comunidades Restaurativas, “los excombatientes de las FARC-EP, ELN, Paramilitares y fuerza pública se reúnen y trabajan juntos para renovar estructuras territoriales”. ]

### **Un hecho histórico en el marco de los cumplimientos de los Acuerdos de Paz** 

[Lo que está sucediendo en Santa Lucía, Ituango, es muy importante, esto debido a que es la “primera vez que una organización no gubernamental compra una finca y se la arrienda a una cooperativa conformada por los excombatientes de una guerrilla” afirmó el representante alemán. ]

[Esto se complementa al trabajo de reconciliación, bajo el tema del perdón y la culpa. “Cuando trabajamos juntos por un futuro mejor la reconciliación puede ser una realidad —comentó Merckle—. Un proceso de reconciliación no es fácil, se necesita mucho coraje para que los excombatientes y las víctimas participen. Lo principal es afrontar la culpa y la responsabilidad para pedir perdón”. ]

[Por su parte, el representante del Consejo Nacional de Reincorporación, Pastor Alape, manifestó el completo apoyo hacia el trabajo que se viene realizando en la vereda de Santa Lucía y la importancia de estar a la altura de este momento histórico.]**“Retejer todo el tejido social en un territorio como este —manifestó Alape— es importantísimo por las**[ **singularidades particulares de este territorio.** Tenemos una riqueza de mucha diversidad natural que la construcción de paz tiene que cuidar y aprovechar”.]

[Con las 500 hectáreas arrendadas a los excombatientes de las FARC-EP, se potenciarán los trabajos campesinos y agropecuarios, convirtiendo el escenario en un proyecto autosustentable que les permita desarrollar una vida digna y resarcir los daños ocasionados por el conflicto a las víctimas a partir de actividades concertadas con las comunidades. ]

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
