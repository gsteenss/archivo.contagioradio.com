Title: América Latina se moviliza para exigir transformaciones educativas
Date: 2016-05-16 16:30
Category: Movilización, Nacional
Tags: crisis universitaria américa latina, educación pública américa latina, universidades america latina
Slug: america-latina-se-moviliza-para-exigir-transformaciones-educativas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Marcha-educación.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sin Embargo] 

###### [16 Mayo 2016]

Durante los primeros meses de este año, en varios países latinoamericanos se han promovido movilizaciones en exigencia de una **mayor inversión de presupuesto para la educación, así como mejoras en las condiciones laborales de los docentes**. Problemáticas que han llevado a profesores, estudiantes y trabajadores a articularse para defender la educación pública y exigir a los Estados mayor responsabilidad en el asunto.

### MÉXICO 

Este fin de semana, por lo menos 500 mil maestros, en distintas ciudades de México, se manifestaron para rechazar las actuales condiciones que enfrenta el magisterio; esta multitudinaria movilización fue convocada por la Coordinadora Nacional de Trabajadores de la Educación CNTE, con el fin de **exigir la instalación de una mesa en la que se acuerden mejoras en las condiciones laborales**.

Según denuncia la CNTE, la reforma educativa de Peña Nieto, ha impuesto **evaluaciones punitivas a los docentes**, desmejorando la calidad de los contratos; así como **injustos incrementos salariales** que "no alcanzan ni para un kilo de tortillas", sumado a la **continúa represión de la que son víctimas**. Para exigir soluciones frente a esta cruenta problemática, maestros de 28 estados mexicanos se suman al paro nacional magisterial.

### ARGENTINA 

Desde el pasado 9 de mayo, estudiantes y profesores se movilizan en Argentina para exigir al Gobierno Macri **mejoras en el presupuesto asignado para las universidades, así como en los salarios docentes**. El 12, llevaron a cabo una multitudinaria marcha en todo el territorio nacional y desde ese día y hasta el 13, protagonizaron un paro total de 48 horas, en defensa de la educación pública.

<div class="txt_newworld">

Estas actividades se realizan en exigencia también de un boleto educativo, más becas para los estudiantes, mayor presupuesto educativo, **derogación de la Ley de Educación Superior por no incluir el derecho al estudio universitario**, y la no criminalización de la protesta. Las movilizaciones son lideradas por la Federación de Estudiantes de la Universidad de Buenos Aires.

</div>

### CHILE 

El pasado 21 de abril, la Confederación de Estudiantes Universitarios de Chile Confech, convocó una movilización para exigir avances en el proyecto de gratuidad de la educación universitaria. De esta marcha hicieron parte estudiantes de secundaria, trabajadores del sector educativo y otros sindicatos, que fueron **reprimidos por la Policía chilena que arrojó agua y gases lacrimógenos en su contra**.

Las denuncias de los estudiantes, tienen que ver con la lentitud por parte del Gobierno en el diseño y la ejecución de la reforma educativa, que sólo alcanzaría una cobertura del 13.9% del total de los alumnos, mientras que **incrementaría la cantidad de estudiantes endeudados, que ya se eleva al 40.6%**, frente al 31.4% que son beneficiarios de becas.

Con esta movilización los estudiantes exigieron trato preferencial para las universidades públicas, fin de la privatización de escuelas municipales, mayor financiamiento y mejoras en la calidad educativa, en suma, una **reforma estructural del sistema que garantice el acceso a educación de calidad para todos los estudiantes chilenos**.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
