Title: Desde la media noche de hoy inicia la hora cero del paro nacional estudiantil indefinido
Date: 2018-10-11 16:35
Author: AdminContagio
Category: Educación, Movilización
Tags: Movimiento estudiantil, Paro Nacional, UNEES
Slug: desde-la-media-noche-de-hoy-inicia-la-hora-cero-del-paro-nacional-estudiantil-indefinido
Status: published

###### [Foto: Archivo Particular] 

###### [11 Oct 2018] 

La Unión Nacional de Estudiantes de la Educación Superior, a través de una rueda de prensa, manifestó que desde la media noche de este 11 de octubre inicia la hora cero del gran paro nacional estudiantil indefinido, en el que participarían **más de 45 instituciones de educación superior de carácter público y privado. **

En esa medida, la delegación de estudiantes expresaron que las universidades irán definiendo una a una, el momento en el que realizarán el cese de actividades y explicaron que es indefinido debido a que solo se levantará cuando el gobierno instaure una mesa de diálogos o de propuestas concretas que respondan al pliego de exigencias. (Le puede interesar: ["Estamos ad portas de un gran paro estudiantil en Colombia"](https://archivo.contagioradio.com/gran-paro-estudiantil/))

Lo estudiantes reiteraron que hay un conjunto de 10 peticiones expuestas al Gobierno de Duque, entre ellas la de aumentar en **4.5 billones de pesos la base presupuestal** para la educación que se inyecte directamente a la instituciones de educación superior públicas.

Asimismo, frente al movimiento estudiantil de universidades privadas, los estudiantes señalaron que exigen la congelación inmediata de las matrículas y aclararon que si bien no pueden tener un cese de actividades **por las condiciones de las instituciones privadas, ya tienen una agenda de movilización y actividades para apoyar el paro nacional.**

Finalmente los estudiantes señalaron que la próxima movilización nacional se esta convocando **para el 17 de octubre**, que además será otro escenario de articulación para el movimiento estudiantil, en donde se espera tener jornadas de socialización del pliego de exigencias con la ciudadanía.

###### Reciba toda la información de Contagio Radio en [[su correo]
