Title: Rosa Elvira Cely: 6 veces victimizada
Date: 2016-05-16 18:28
Category: Mujer, Nacional
Tags: Alcaldía de Bogotá, Miguel Uribe, Rosa Elvira Cely, Violencia contra la mujer
Slug: rosa-elvira-cely-6-veces-victimizada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/rosacely-110506-1-de-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [16 de Mayo] 

El pasado sábado, se conoció que  la oficina de la Secretaría de Gobierno de Bogotá, emitió un concepto donde se asegura que Rosa Elvira Cely, víctima de tortura, violencia sexual y asesinato hace 4 años, fue la culpable de su propio crimen. Este hecho refleja la múltiple victimización de las mujeres por cuenta de la violencia estructural de género que se reproduce a diario en Colombia y de la cual, en gran parte, es responsable el Estado  como se ha evidenciado en el caso de Rosa Elvira.

### **Víctima del sistema laboral y de educación** 

A sus 35 años, esta mujer de bajos recursos y que no pudo terminar sus estudios, pero que tenía el sueño de ser psicóloga y sacar a su hija adelante, debió ingresar al Colegio Técnico Manuela Beltrán, para validar el bachillerato en las noches después de que salía de trabajar de las ventas ambulantes. Allí conoció a su agresor, Javier Velasco.

Cely refleja la inequidad de género respecto al derecho a la educación y al trabajo que existe en Colombia. De acuerdo con ONU Mujeres, si bien las mujeres colombianas tienen un nivel de escolarización más alto que en otros países de América Latina pues el 56% de los graduados universitarios son mujeres, Colombia tiene una tasa de desempleo de mujeres más alta que la de los hombres, además son ellas quienes en su mayoría deben vincularse al mercado laboral de manera informal, lo que pone en riesgo su seguridad y empoderamiento económico, como sucedió con Rosa Elvira, quien vivía de la venta de minutos de celular a 200 pesos, y por cuya labor obtenía un sueldo de 25.000 pesos diarios. Con ese dinero costeaba sus estudios de validación y los 120.000 pesos mensuales que costaba el arriendo de la habitación en la que vivía.

### **Víctima de la violencia física  y sexual** 

Cifras del Instituto de Medicina Legal demuestran que cada 13 minutos una mujer es víctima de algún tipo de agresión en Colombia. Durante 2015, se registraron 1.007 asesinatos cuyas víctimas padecieron por el simple hecho de ser mujeres. Asimismo, se conocieron 16 mil denuncias de abuso sexual en el país el año pasado.

Rosa Elvira se convirtió en un icono de la lucha de las mujeres víctimas de violencia física y sexual. Por esa situación, el juzgado 14 administrativo mixto de descongestión del circuito de Bogotá admitió para su estudio una demanda contra el Ministerio de Defensa y la Policía Nacional por los hechos que rodearon su muerte, en la que se establece que  no hubo una “debida diligencia para prevenir efectivamente la violencia sexual y de género contra la mujer, mediante los cuales se permitió y ocurrió el secuestro, actos de violencia sexual y actos de tortura”, como dice la denuncia.

Esta mujer murió por una infección intraabdominal por la destrucción de sus intestinos; el empalamiento con una rama acabó con su útero sus y trompas de Falopio. Pero además, fue puñalada en la espalda y se le encontraron signos de asfixia en el cuello.

### **Víctima del sistema de salud** 

La familia de Rosa Elvira denuncia que la ambulancia que recogió a su hermana llegó dos horas después de que la víctima llamara a la línea 123 pidiendo auxilio.

Además, la Secretaría de Salud de Bogotá alega que no era su responsabilidad la decisión de trasladar a Rosa Elvira al Hospital Santa Clara pese a que cerca del lugar de los hechos hay cuatro centros de salud: al Hospital San Ignacio o al Hospital Militar una ambulancia hubiera tardado 4 minutos en llegar, a la Clínica Marly se hubiera demorado 5 minutos y a la Clínica Palermo cerca de 8 minutos. Sin embargo, esa noche del 23 de mayo, Cely fue trasladada al hospital público Santa Clara que queda a media hora.

En junio de 2015,  ese hospital fue sancionado por la Superintendencia Nacional de Salud tras encontrarse que no se garantizó una atención adecuada a la víctima. Según un informe presentado por esa entidad, pese a la gravedad del estado de salud, Rosa fue clasificada como Triage 2 y no Triage 1 por la complejidad de su caso, por lo que fue enviada al pasillo de observación y solo cuando sufrió el paro cardiaco, le realizaron los exámenes y procedimientos recomendados para una presunta víctima de violencia sexual.

La Supersalud sostuvo que si se hubieran  realizado todos los procedimientos de acuerdo con los protocolos establecidos se hubiera podido incidir en una disminución del riesgo y mitigación de los efectos de la agresión de la víctima.

### **Víctima del sistema judicial** 

Aunque existían condenas por la violación de una trabajadora sexual, el acceso carnal abusivo que cometió en contra de sus dos hijas menores de edad, y 10 años atrás había asesinado con 17 machetazos a Dismila Ochoa, Javier Velasco, asesino y agresor de Rosa Elvira, permanecía en libertad y sólo tras el feminicidio de su hermana, se conoció públicamente que Velasco estaba implicado en estos hechos violentos contra mujeres.

Además cuando fue capturado por las autoridades, se solicitó  que el asesino fuera trasladado a una clínica de atención psiquiátrica y por ello recuperó la libertad. Más tarde, en diciembre de 2012, la justicia tomó su decisión final y se condenó a 48 años de prisión a Javier Velasco, quien confesó el crimen.

Toda esta situación genera muchos interrogantes en la hermana de Rosa, Adriana Cely: “Nos preguntábamos por qué estaba suelto, por qué si su mujer lo había denunciado estaba suelto, por qué si ya había cometido un asesinato seguía suelto. ¿A cuántas mujeres más lastimó? ¿Y si nunca las encontraron? ¿Y si nunca lo inculparon?”.

### **Víctima de violencia institucional** 

Gracias a la publicación de la periodista Diana Durán del diario El Espectador, se conoció que Cely y su familia nuevamente fueron victimizados. Esta vez por parte de una respuesta de la Secretaría de Gobierno que conceptuó que la culpa exclusiva de la muerte, violación, empalamiento y demás actos de tortura fueron culpa de Rosa Elvira.

De acuerdo con la representante Ángela María Robledo, “es inadmisible que funcionarios públicos estén justificando la violencia de esta manera”, así mismo, aseguró que al ser un caso tan emblemático no era posible que el Secretario de Gobierno Miguel Uribe argumentara que no había leído la respuesta emitida por la abogada Luz Stella Boada contratada por él mismo en enero de este año, quien ya renunció.

Además se pide la renuncia del secretario y respuestas más claras de parte del Alcalde Enrique Peñalosa, quien solo respondió a la indignación de la ciudadanía, congresistas y organizaciones a través de un mensaje en twitter.

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
