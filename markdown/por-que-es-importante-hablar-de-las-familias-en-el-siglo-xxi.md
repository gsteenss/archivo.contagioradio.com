Title: ¿Por qué es importante hablar de las familias en el siglo XXI?
Date: 2020-09-14 17:01
Author: CtgAdm
Category: yoreporto
Slug: por-que-es-importante-hablar-de-las-familias-en-el-siglo-xxi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-18-at-11.52.26-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

¿Cómo repensamos las relaciones familiares para que dejemos de reproducir dinámicas de violencia y discriminación en su interior? Esta y otras reflexiones sobre las familias en Colombia motivo el lanzamiento de la **convocatoria abierta “Familias: espacios de cuidado y bienestar”**, organizada por Fondo Lunaria y la plataforma Familias: Ahora.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fondo Lunaria es un fondo feminista que moviliza recursos para apoyar a organizaciones de mujeres jóvenes en la construcción de un mundo sin machismo, racismo y LGBTI fobias en Colombia. Para esta convocatoria, Fondo Lunaria se unió a Familias: Ahora, una plataforma ciudadana lanzada en agosto pasado que promueve el bienestar de las familias a través de los derechos humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La convocatoria busca apoyar **proyectos innovadores, creativos, formativos y artísticos** enfocados en desarrollar y posicionar narrativas sobre familias lejos de modelos y esquemas rígidos. En esta **pueden participar organizaciones de la sociedad civil con o sin personería jurídica de todo el país** que estén interesadas en promover familias donde el cuidado y el bienestar estén en el centro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las organizaciones interesadas deberán plantear un proyecto presupuestado en 10 millones de pesos. Dentro de las estrategias que pueden usar para plantear el proyecto están las campañas de documentación, la promoción de diálogos físicos o virtuales, la realización de actividades comunitarias, el desarrollo de juegos o herramientas para mejorar las relaciones familiares y el trabajo en redes, entre otras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, se busca que estos proyectos lleguen a públicos no especializados, por eso, se valorará positivamente que estén dirigidos a personas entre los 25 y 35 años, que vivan en pareja, con hijos propios o de su pareja, con grado bachiller o técnico, que no tengan un compromiso sólido con la religión ni con la política, y que usan Facebook y WhatsApp.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las organizaciones o colectivas interesadas pueden consultar los términos de referencia y los formularios de aplicación en esta página:

<!-- /wp:paragraph -->

<!-- wp:core-embed/wordpress {"url":"https://fondolunaria.org/convocatorias/#convocatoria_familias","type":"wp-embed","providerNameSlug":"fondo-lunaria","className":""} -->

<figure class="wp-block-embed-wordpress wp-block-embed is-type-wp-embed is-provider-fondo-lunaria">
<div class="wp-block-embed__wrapper">

https://fondolunaria.org/convocatorias/\#convocatoria\_familias

</div>

</figure>
<!-- /wp:core-embed/wordpress -->

<!-- wp:block {"ref":88544} /-->
