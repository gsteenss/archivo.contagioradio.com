Title: Peñalosa no puede intervenir la Van Der Hammer
Date: 2018-09-17 17:14
Author: AdminContagio
Category: Ambiente, Movilización
Tags: Alcaldía, Bogotá, Tala indiscriminada, Van der Hammen
Slug: penalosa-intervencion-van-der-hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/3373268_n_vir3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [17 Sept 2018] 

El Alcalde **Enrique Peñalosa recibió el primer aval del Gobierno para la intervención en la reserva Van Der Hammen**, sin embargo Maria Mercedes Maldonado, Vocera de la Veeduría Cerros, afirmó que es un simple consentimiento y que aún falta mucho para que el alcalde Enrique Peñalosa pueda intervenir  la reserva.

Luego de conocerse la noticia del  aval, Maldonado a**firmó que existen requisitos que no se han tomado en cuenta** para la realinderación de la reserva, y que hasta ahora, solo se conoce el consentimiento del Ministerio del Interior que asegura que no se encontraron poblaciones indígenas o grupos étnicos residiendo en el terreno, sin embargo, **se desconoce si alguna de estas comunidades ocupa el lugar para desarrollar actividades** relacionadas con su cultura o tradiciones.

Para la vocera de Veeduría Cerros, existe una visión completamente depredadora por parte del Alcalde, ya que según ella "**para él las zonas protegidas son potreros y los cerros se verían mejor urbanizados**” y señaló que su visión de ciudad se basa en "llenar todo de cemento".

También aseguró que desde el inicio de la administración de “Bogotá mejor para todos”, han desmejorado las gestiones para el cuidado del medio ambiente, debido a que  en 2016 se detuvo la restauración de la reserva y **se han invadido zonas delicadas de los cerros para construcción de senderos pavimentados dedicados al turismo.** (Le puede interesar: [Peñalosa desconoce institucionalidad en materia ambiental frente a la Van Der Hammen](https://archivo.contagioradio.com/penalosa_van_der_hammen_propuesta_distrito/))

### **La tala de arboles es "otro capricho del Alcalde"** 

Finalmente Maldonado habló sobre la tala indiscriminada de árboles en la ciudad, y expresó que se trata de **un capricho de Peñalosa** que tendrá grandes consecuencias ambientales, “**no se puede uniformizar la ciudad con dos o tres tipos de árboles que le gusten al alcalde”.**

Además aseveró que las organizaciones ambientalistas y las veedurías seguirán alertas ante las decisiones que se tomen acerca del humedal Van Der Hammen, los cerros y demás zonas ecológicas de Bogotá. (Le puede interesar: [La terquedad de Peñalosa no pudo acabar con conectividad de la Van Der Hammen](https://archivo.contagioradio.com/la-terquedad-de-penalosa-no-pudo-acabar-con-conectividad-de-la-van-der-hammen/))

<iframe id="audio_28683495" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28683495_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo] 
