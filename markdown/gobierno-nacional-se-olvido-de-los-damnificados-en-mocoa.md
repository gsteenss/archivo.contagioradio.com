Title: Gobierno Nacional se olvidó de los damnificados en Mocoa
Date: 2018-03-28 13:26
Category: DDHH, Nacional
Tags: avalancha mocoa, desaparecidos mocoa, Mocoa, tragedia de mocoa
Slug: gobierno-nacional-se-olvido-de-los-damnificados-en-mocoa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Avalanchas-en-Mocoa-e1491160730970.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ejército Nacional] 

###### [28 Mar 2018] 

A un año de la tragedia que azotó a Mocoa en el Putumayo, las comunidades afectadas denunciaron que **no ha habido la suficiente atención** y que el Gobierno Nacional no ha cumplido con las promesas que hizo. En la avalancha perdieron la vida 333 personas, hay más de 100 desaparecidos y hubo más de mil damnificados.

Pese a que el Ministro de Defensa, **Luis Carlos Villegas,** indicó que el acompañamiento a los habitantes de Mocoa se ha hecho de manera adecuada y que se van a empezar a entregar las primeras 100 viviendas, las comunidades no han sentido esta presencia.

De acuerdo con Luis Ulcue, indígena del pueblo Nasa y quien ha hecho un acompañamiento a los damnificados de la avalancha, **“estamos a un año y todo sigue igual”**. Afirmó que las familias en el momento de la tragedia, recibieron el apoyo y la ayuda humanitaria, “sin embargo hoy están en total abandono”.

### **Falta de atención gubernamental ha generado descomposición social** 

Recalcó que una vez ocurrieron los hechos, se dio el registro y “las personas en medio de su dolor y desespero **lo que hicieron fue salvar lo propio** y se fueron”. Con el tiempo, estas personas fueron volviendo a Mocoa “mirando cómo pueden recibir ayuda del Gobierno y se encuentran con que les han dado la espalda”. (Le puede interesar:["Instituciones nacionales se van de Mocoa sin superar la emergencia"](https://archivo.contagioradio.com/mocoa-emergencia/))

Además, allí las personas “en medio del desespero toma los predios y empiezan las confrontaciones”. Esto ha ocasionado una descomposición social y la gente **“está a la deriva”**. En varias oportunidades, los damnificados han manifestado que no ha habido ayuda para recuperar sus hogares y en muchos casos se trata de población víctima del conflicto armado y desplazados.

### **Cifra inexacta de desaparecidos refleja precaria presencia estatal** 

Teniendo en cuenta que después de la avalancha se registraron entre 100 y 300 desaparecidos, las comunidades han denunciado que la precaria presencia estatal se retrata en la **inexactitud de las cifras**. A un año, aún se desconoce el número de personas desaparecidas y hay madres que continúan en la búsqueda de sus hijos pues les han comentado que los han visto con vida sin que ninguna autoridad las acompañe en el proceso.

Ulcue indicó que “fue tanta la cantidad de lodo y piedras que lo que hicieron como acto simbólico fue **declarar la zona como campo santo**”. Sin embargo, no ha habido un pronunciamiento institucional “de que existe la preocupación por los desaparecidos”. Afirmó que actualmente se lleva a cabo un proceso de demolición de las casas que quedaron en pie pero, “la gente ha hecho resistencia y están retornando al lugar donde vivían exponiendo su vida ante una nueva tragedia”.

<iframe id="audio_24926750" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24926750_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
