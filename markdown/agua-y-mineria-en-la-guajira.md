Title: Agua y minería en La Guajira
Date: 2015-08-07 07:00
Category: CENSAT, Opinion
Tags: Agua, ANLA, CENSAT, CORPOGUAJIRA, El Cerrejón, La Guajira, Mineria, OIT, sed y hambre en la Guajira
Slug: agua-y-mineria-en-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Arroyo-Bruno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Arroyo Bruno y la fuete CENSAT] {#foto-arroyo-bruno-y-la-fuete-censat align="CENTER"}

#### [**[Censat Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva) 

###### [7 Ago - 2015]

*Des-arroyo* en su máxima expresión

Los efectos de la actividad minera sobre el agua son tan comprobados como devastadores. Una simple mirada a la actividad extractiva de minería de carbón a cielo abierto en La Guajira permite reconocer las dimensiones de la destrucción ambiental- territorial que la explotación conlleva.

El pasado mes de noviembre, el conglomerado trasnacional Carbones de El Cerrejón obtuvo licencia ambiental para la ampliación de la explotación de carbón a cielo abierto en La Guajira. El proyecto de expansión minera implica la desviación de varias fuentes de agua, empezando por el Arroyo Bruno, y continuando con el represamiento del Río Palomino, entre otras obras de infraestructura, para explotar 40 millones de toneladas de carbón.

La licencia ambiental fue entregada por la Autoridad Nacional de Licencias Ambientales (ANLA), a partir de estudios técnicos proporcionados por la empresa y, hasta donde se conoce, sin comprobación *in situ* por parte de la Autoridad sobre la importancia del Arroyo Bruno para el abastecimiento de agua en zonas rurales y urbanas de Albania y Maicao. El Ministerio del Interior, por su parte, señaló que Campo Herrera era la única comunidad a la que debía hacerse consulta previa, desconociendo por lo menos a 5 comunidades ribereñas del Arroyo y su relación ancestral con el territorio, y la dependencia exclusiva de las aguas del Bruno para su pervivencia. Después de más de 20 años de la incorporación del Convenio 169 de la OIT a la legislación colombiana y una larga producción de jurisprudencia frente a estos pueblos, persiste el desconocimiento y violación a sus derechos territoriales y el saqueo sistemático de sus espacios de vida.

La decisión final para aprobar los permisos de desviación del cauce del Arroyo Bruno la tiene la Corporación Autónoma Regional de La Guajira (CORPOGUAJIRA). A pesar que en años anteriores CORPOGUAJIRA frente a la misma solicitud entregó permisos parciales, dada la presencia de especies forestales vedadas en la zona, actualmente ante las mismas condiciones ambientales, vuelve a estudiar una serie de permisos tales como aprovechamiento forestal, ocupación de cauce y levantamiento de veda forestal. Sin embargo, la Corporación ha señalado no tener la capacidad técnica para realizar dichos análisis y tomar una decisión. Al no recibir respuesta por parte de las universidades a las que ofertó una consultoría para este trabajo, ahora solicita los estudios a la ANLA, quien ya entregó la licencia ambiental, produciéndose por esto profundas dudas sobre la transparencia del proceso y la independencia de CORPORGUAJIRA para tomar una decisión de tal magnitud.

Diversos interrogantes genera el modelo minero general de Colombia, en particular en La Guajira, y en referencia al futuro de las fuentes de agua y su función social: ¿Cuáles son los procedimientos y análisis realizados por el ANLA para permitir proyectos de esta envergadura, con el impacto ambiental y social que implican? ¿Cuáles son los criterios del Ministerio del Interior para seleccionar los pueblos que deben ser consultados frente a un megaproyecto? ¿Cuál es la autonomía de las autoridades ambientales cuando manifiestan no tener capacidad técnica para hacer estudios decisivos para el futuro de las regiones?¿Cómo se da la participación de las comunidades, organizaciones y procesos ante la intervención de sus territorios que pueden afectar sus vidas y sus construcciones territoriales?

Ahora bien, otro aspecto puesto en relieve con la explotación minera consiste en los supuestos beneficios del crecimiento económico del extractivismo. Según el gobierno nacional, la extracción de carbón a gran escala ha significado una suma importante de regalías que se traducen en desarrollo económico y social para la Guajira. No obstante, las cifras de mortalidad infantil denunciadas en meses anteriores, producto de la falta de agua y alimentos, desnudan una profunda desinstitucionalización y desaparición del Estado Social de Derecho. Esta situación manifiesta el favorecimiento del modelo extractivo por encima de la garantía del derecho fundamental al agua, así como de otros derechos fundamentales que afectan radicalmente la vida y la salud de las niñas y los niños. Un solo niño/a sin acceso al agua en Colombia es una tragedia.
