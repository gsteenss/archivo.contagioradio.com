Title: Así fue el cacerolazo ¡No a la venta de la ETB!
Date: 2016-05-07 13:38
Category: Galerias, Otra Mirada
Tags: cacerolazo, ETB, Venta ETB
Slug: asi-fue-el-cacerolazo-no-a-la-venta-de-la-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/cacerolazo-etb-9.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: Gabriel Galindo 

En Bogotá, la noche del 6 de mayo se lleno de música, de baile y  de sonidos de ollas y sartenes en el **cacerolazo en apoyo a la ETB** y contra la venta de la empresa pública. Jóvenes de universidades públicas, colegios,  movimientos sociales, el sindicato de ETB e indignados, caminaron desde la carrera séptima con calle 22  hasta la Plaza de Bolivar. Allí terminó la actividad con consignas en contra de las políticas que adelanta el alcalde de Bogotá Enrique Peñalosa y el grito ¡No a la venta de la ETB!.

\
