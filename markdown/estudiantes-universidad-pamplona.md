Title: Estudiantes detenidos por protestar en la Universidad de Pamplona
Date: 2018-10-05 18:12
Author: AdminContagio
Category: DDHH, Educación
Tags: estudiantes, policia, universidad, Universidad de Pamplona
Slug: estudiantes-universidad-pamplona
Status: published

###### [Foto: Sceneups] 

###### [5 Oct 2018] 

Miembros de la comunidad estudiantil de la **Universidad de Pamplona,** Norte de Santander, denuncian que mientras estudiantes del programa de Ingeniería Mecánica protestaban pacíficamente en una de las sedes de esta Institución, **fueron capturados por integrantes de la policía.**

Los estudiantes **se encontraban bloqueando la sede de Nuestra Señora del Rosario de forma pacífica**, exigiendo que se cumplieran los acuerdos pactados con el rector de la Universidad, que incluían mejoras en infraestructura y el aumento de la planta docente. Sin embargo, **un grupo de personas llegaron hasta el lugar para intentar sacar a los  universitarios,** hechos que terminaron en actos de violencia mediante el uso de piedras, cuchillos y artefactos explosivos.

Ante esta situación, los estudiantes tuvieron que desalojar la sede para proteger sus vidas, y f**ue allí que la policía intervino, capturando a 21 de los universitarios**, y 2 agresores más. Según Jonathan Acevedo, egresado de la Universidad, las 23 personas fueron conducidas a la estación de policía de Pamplona y aún **la Fuerza Pública no permite el ingreso de veedores de derechos humanos al lugar para evaluar el estado de los detenidos.**

En un comunicado, diferentes organizaciones que denunciaron la situación, también **señalaron la presencia del ex concejal de Pamplona, Alexander Castro Martínez** entre el grupo que intentaban desalojar a los estudiantes. Adicionalmente, pidieron que se liberara a los 21 universitarios retenidos, y se los eximiera de cualquier cargo, porque la protesta que realizaban era pacífica y estaba en los términos legales. (Le puede interesar: ["¿Por qué protestan los estudiantes?](https://archivo.contagioradio.com/marchando-los-estudiantes/))

[Organizaciones denuncian retención de estudiantes en Universidad de Pamplona](https://www.scribd.com/document/390210740/Organizaciones-denuncian-retencion-de-estudiantes-en-Universidad-de-Pamplona#from_embed "View Organizaciones denuncian retención de estudiantes en Universidad de Pamplona on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_2440" class="scribd_iframe_embed" title="Organizaciones denuncian retención de estudiantes en Universidad de Pamplona" src="https://www.scribd.com/embeds/390210740/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-V7QXEjYKGhDoOdwxGYo4&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
