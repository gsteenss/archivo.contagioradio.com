Title: Las consecuencias de la sociedad de consumo en la biodiversidad
Date: 2017-01-05 18:42
Category: Ambiente, Voces de la Tierra
Tags: animales en peligro de extinción, biodiversidad, Unión Europa
Slug: las-consecuncias-la-socidad-consumo-la-biodiversidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/mono-e1483659173458.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [National Geographic]

###### [5 Ene 2017] 

Bien se ha dicho que las acciones individuales también contribuyen de manera negativa en el ambiente, y es que lo que consumen los seres humanos trae graves impactos en la biodiversidad. Así lo demuestra un nuevo **informe basado en más de 7.000 especies marinas y terrestres,** que son víctimas de la sociedad de consumo y la producción de alimentos.

El estudio liderado por los científicos Daniel Moran de la Universidad Noruega de Ciencia y Tecnología y Keiichiro Kanemoto de la Universidad de Shinshu en Japón, fue publicado en la revista 'Nature Ecology & Evolution'. Un mapa en el que se evidencia cómo lo que consumen las poblaciones de **Estados Unidos, la Unión Europa, China y Japón** genera consecuencias negativas en la naturaleza.

En el informe se muestra la estrecha relación entre el consumismo y el impacto ambiental, para lo cual, los científicos esperan que sea una herramienta para que los gobiernos, las empresas y las personas puedan tener la información suficiente para contrarrestar las consecuencias de las actividades industriales y cotidianas del ser humano.

"Una vez que se conozca el impacto ambiental de una cadena de suministro, **mucha gente a lo largo de esa cadena podrá actuar**, no sólo a los productores", dice Daniel Moran.

### ¿Qué se puede hacer? 

"Hemos identificado lugares amenazados sobre todo por un pequeño número de países", manifesta Keiichiro Kanemoto, profesor de la Universidad japonesa de Shinshu y uno de los investigadores, quien agrega que este estudio "**debería facilitar que se iniciara una colaboración directa entre productores y consumidores**", de manera que se puedan encontrar soluciones frente a la pérdida de biodiversidad, pues actualmente animales y plantas están desapareciendo 1.000 veces más rápido que hace apenas unos algunos siglos.

Por su parte, Daniel Moran asegura que “si la UE quería mirar el papel que juega en los problemas en Indonesia, podrían mirar los mapas producidos por los investigadores y ver qué tipo de impactos crean los consumidores de la UE en el país y así podría decidir ajustar sus programas de investigación o sus prioridades ambientales para centrarse en ciertos puntos de acceso en el sudeste de Asia”.

A partir de eso, concluye que las empresas también podrían utilizar estos mapas para saber dónde están sus puntos de impacto ambiental y hacer los cambios necesarios para dejar de contribuir al calentamiento global y con ello, la desaparición de especies.

### Estados Unidos uno de los mayores responsables 

El consumo en Estados Unidos tiene en peligro a miles de animales del sur de México, América Central, el sur de Europa, el Sahel, el sureste de Asia y Canadá. Por ejemplo, la producción de aceite de oliva en España y Portugal destinada a Estados Unidos tiene un efecto directo sobre el lince ibérico a causa de la construcción de embalses para controlar la irrigación.

El estudio revela también que para la producción de café y tofu la industria acabó con los bosques en Mato Grosso en Brasil, y también con el de Sumatra, en Indonesia afectando el hábitat de cientos de especies.

Además, aproximadamente **el 2% de la amenaza sobre la rana arlequín en Brasil podría ser responsabilidad de la explotación forestal** para la producción de bienes destinados a Estados Unidos. A su vez, esa desmedida deforestación en la selva amazónica destinada para la cría de ganado, tiene en riesgo al mono araña.

Por otra parte, la producción de nuevas tecnologías como los iPhones hasta los muebles de Ikea, también contribuyen al daño ambiental. [ (Lea también: “Tu plato de carne contamina más que tu auto” Ambiente y Sociedad)](https://archivo.contagioradio.com/tu-plato-de-carne-contamina-mas-que-tu-auto-ambiente-y-sociedad/)
