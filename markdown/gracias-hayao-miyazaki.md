Title: !Gracias Hayao Miyazaki!
Date: 2016-01-05 17:01
Author: AdminContagio
Category: 24 Cuadros
Tags: 10 datos curiosos sobre Hayao Miyazaki, Anime Japones Studio Ghibli, Cine, Hayao Miyazaki 75 años
Slug: gracias-hayao-miyazaki
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/hayao-miyazaki.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [5 Ene 2015] 

En 75 años de vida el legado de Hayao Miyazaki, co-fundador junto a Isao Takahata del "Estudio Ghibli", responsable de algunas de las películas anime con mayor reconocimiento por parte de los espectadores, representa un gran tesoro para el cine japonés y del mundo.

Miyazaki, nacido en Tokio, el 5 de enero de 1941, sintió desde pequeño una gran fascinación por los aviones y la literatura bélica gracias al trabajo de su padre, constructor de timones para aeronaves de guerra, referencias recurrentes en su filmografía, así como de su madre obtendría la inspiración para hacer de las mujeres protagonistas de sus películas.

Aunque en sus inicios trabajó como animador y dibujante,  Miyazaki es un apreciado magaka y productor que ha sido reconocido con dos premios Oscar de la Academia y un Oso de Oro en Berlín entre muchos otros, y a pesar de que anuncio su retiro hace más de dos años tras el estreno de "The Wind Rises", su popularidad y vigencia se mantienen intactas.

Con ocasión de su aniversario 75, compartimos algunos datos clave de su trabajo, así como de su vida, influencias y proyectos.

**1.Influencia en la animación occidental**

John Lasseter, director creativo de Pixar, ha reconocido en múltiples ocasiones su admiración por el trabajo de Miyazaki, entre las que destacan la aparición de su personaje Totoro, en la tercera entrega de la exitosa Toy Story, además de admitir que al sentir que el equipo se atascaba, ponían un par de secuencias de sus películas para inspirarse.

**2. El nombre de Studio Ghibli**

Ghibli es el nombre de un viento caliente del desierto del Sahara y de un avión italiano de los años 30, Miyazaki y Takahata decidieron fundarlo luego del éxito alcanzado con "Nausicaa", primer película producida por el estudio Topcraft, y su primer largometraje sería El Castillo en el cielo

**3. Influencia de los clásicos de la literatura**

La inspiración para la realización de su primer trabajo como estudio, vendría del libro "Los viajes de Gulliver" del escritor Johatahn Swift, referencia que Miyazaki nunca ha negado, tanto así que el nombre de la isla que aparece en su película y la que visita el protagonista del libro es el mismo.

**4. Película más taquillera**

Hasta el estreno en Japón en 1997 de la multiple ganadora del Oscar "Titanic", dirigida por James Cameron, "La princesa Mononoke" fue la cinta  con mejor recaudación en la taquilla del país oriental, registro que sería doblegado en 2001 con el estreno de "El Viaje de Chihiro" misma que conserva el primer puesto hasta hoy.

**5. Consagración internacional**

A pesar del reconocimiento alcanzado en su país desde los años 80, las producciones de Hayao Miyazaki y del Studio Ghibli, no gozaban de la fama en todo el mundo que hoy tiene. Esta llegaría con el estreno de "El viaje de Chihiro", con la que ganó el Oscar de la Academia y el Oso de Oro al Festival de Berlín, siendo la única película animada en conseguirlo.

**6. La polémica de los Oscar**

Miyazaki, un pacifista declarado, se negó a recibir el premio Oscar personalmente, cuando en 2002  "El viaje de Chihiro" ganó el premio como mejor película de animación, como forma de protestar contra la decisión de los Estados Unidos de enviar tropas a Iraq. En 2014 asistiría a recibir su Oscar honorífico en reconocimiento a una vida de trabajo.

**7. Miyazaki en 3d**

La cercanía y contacto con John Lasseter y los estudios de animación Pixar, parece haber tentado al productor, quien recientemente, ha anunciado de estar trabajando en un nuevo proyecto, un cortometraje de animación realizado con la tecnología del ordenador.

**8. Un asiduo lector**

Entre los autores que han influido más Miyazaki se encuentra Ursula K. Le Guin, el propio director ha confesado tener sobre su mesita de noche, "El ciclo de Earthsea". Una pasión que Miyazaki parece haber transmitido también a su hijo Goro, quien en su primer largometraje como director eligió precisamente narrar la historia de Ged el Archimago de Terramar

**9. Hayao politólogo**

A pesar de sus grandes dotes artísticas, Hayao estudió Ciencias Políticas y Económicas en la Universidad de Gakushuin (Tokio), años durante los que leyó a muchos autores occidentales que marcarían su forma de narrar historias.

**10.Miyazaki Activista contra la guerra.**

En el documental el reino de los sueños y la locura, estrenado hace más de tres años, Miyazaki ha confesado sus dudas sobre el Gobierno japonés y sobre su decisión de aumentar el gasto de las armas, además siempre ha sido un activo defensor de la abolición de la energía nuclear

\[embed\]https://vimeo.com/134668506\[/embed\]
