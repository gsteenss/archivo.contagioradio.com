Title: Familiares de desaparecidos de Mocoa exigen que continúe la búsqueda
Date: 2017-04-13 10:07
Category: DDHH, Nacional
Tags: Damnificados de Mocoa, Mocoa
Slug: familiares-de-desaparecidos-de-mocoa-exigen-que-continue-la-busqueda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/mocoa-9-e1491162791275.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [13 Abr 2017]

Por medio de un comunicado los familiares de las víctimas de Mocoa, que siguen en condición de desaparición, **rechazaron la decisión del gobierno nacional de cesar los operativos de búsqueda de las personas** y exigieron que se sigan poniendo todos los recursos necesarios hasta encontrar los cuerpos y poder darles una sepultura digna, así mismo exigieron la identificación plena de las personas que han sido sepultadas en el cementerio de la ciudad.

En el comunicado, señalan en 9 puntos que es necesario que se organicen las rutas tanto de información por parte de las autoridades hospitalarias, fiscalía y medicina legal, que permita a los familiares tener información concreta y completa “**Se ha presentado falta de comunicación y coordinación entre entidades como CTI, Hospital José María Hernández, Fiscalía, Medicina Legal, Policía Nacional**”.

Los familiares también señalan que en los primeros días, después de a tragedia algunos de las personas que llegaban al Hospital José María Hernández, podían acceder a un álbum de las personas fallecidas que habían ingresado, sin embargo, hasta el momento los **allegados de los desaparecidos no han podido tener acceso a dicho material, que sería de gran aporte en la búsqueda**. Le puede interesar:["Persisten denuncian de desvío de ayudas para Mocoa"](https://archivo.contagioradio.com/persisten-denuncias-de-desvio-de-ayudas-para-mocoa/)

Las exigencias, de manera general, giran en torno a la necesidad de organización de la información para que se facilite la búsqueda de los desaparecidos. “**Creemos que son cientos de niños, niñas, hombres y mujeres los que están enterrados y no dejamos de pensar que son los nuestros**” además solicitaron el acompañamiento de organizaciones de derechos humanos con experiencia en la búsqueda de desaparecidos.

**Los 9 puntos de exigencias concretas son:**

1\. Coordinación de la información entre las instancias competentes y la entrega de  
información clara de la ruta de atención para familiares y allegados con relación a los  
desaparecidos y desaparecidas por la avenida torrencial del pasado 31 de Marzo.

2\. Entrega inmediata del informe cuerpos encontrados, indicando sexo, posible edad, lugar de hallazgo, si fue entregado o se encuentra en sepultura temporal.

3\. Entrega inmediata del informe de personas desaparecidas, indicando sexo y edad.

4\. Entrega inmediata de informe de sepulturas temporales de cuerpos no reclamados,  
indicando número de personas y características relevantes.

5\. Entrega inmediata de informe de personas trasladadas a instituciones de salud y protección  con características, lugares de traslado y demás información pertinente.

6\. Entrega del álbum de registro de personas fallecidas que llegaron al Hospital José María  
Hernández, y que fue visto por varios familiares los primeros días de búsqueda y del cual no se tiene conocimiento actualmente.

7\. Entrega del registro fotográfico del CTI de los cadáveres encontrados a la fecha.

8\. Informe del listado de niños y niñas y sus características, que se encuentran en el ICBF.

9\. Acompañamiento psicosocial a las familias de los desaparecidos.

Por último, solicitamos acompañamiento de las organizaciones defensoras de Derechos Humanos, organizaciones nacionales e internacionales expertos en la materia y organismos de control para que sean garantes de nuestros derechos y juntos logremos encontrar a nuestros seres queridos.

Firman  
GRUPO INCANSABLE DE BÚSQUEDA  
TEJEDORAS DE VIDA DEL PUTUMAYO  
FUNDACION PAZ Y TRABAJO DIGNO
