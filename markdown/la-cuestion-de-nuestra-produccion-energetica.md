Title: La cuestión de nuestra energía
Date: 2016-11-18 12:54
Category: CENSAT, Opinion
Tags: Ambiente, energía
Slug: la-cuestion-de-nuestra-produccion-energetica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/produccion-energetica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CENSAT 

#### **[Por: CENSAT Agua Viva /Amigos de la Tierra Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

###### 18 Nov 2016 

El pasado 11 y 12 de noviembre se realizó en Garzón, Huila, la Audiencia Pública Ambiental de Seguimiento al Proyecto Hidroeléctrico El Quimbo. La Audiencia convocada por la Agencia Nacional de Licencias Ambientales - ANLA por solicitud del Gobernador del Huila y de la Mesa Temática para la Verificación, Promoción, Conservación del Medio Ambiente, el Agua y el Territorio congregó miles de personas de todo el Huila que denunciaron los daños que el proyecto hidroeléctrico ocasionó sobre los medios de vida de miles de familias de toda la región centro del departamento, además de los frecuentes incumplimientos de la empresa italo – española Emgesa, responsable del proyecto, entre otros asuntos.

[Como en el Huila, en días pasados, 6 de noviembre, una audiencia pública ambiental convocada por la ANLA fue realizada en Pesca, Boyacá. Esta multitudinaria audiencia debatió en torno a la solicitud de la empresa petrolera Maurel & Prom Colombia B. V. para modificar la licencia ambiental para la exploración de hidrocarburos - Proyecto Área de Interés Exploratorio Muisca - en 15 veredas de Pesca y 2 de Tota.  Durante todo el día, los pobladores resaltaron los daños que la actividad sísmica ha provocado en viviendas, nacimientos de agua y que ha provocado la disminución del caudal de afluentes superficiales y deslizamientos.]

[Es de destacar el rigor con el que comunidades organizadas evidenciaron los daños y problemáticas causadas por los emprendimientos energéticos y que sus posiciones fueran respaldadas por los gobernadores de estos departamentos, que también sustentaron las implicaciones que tienen estos proyectos energéticos en estas dos zonas de tradición agrícola. Aunque los emprendimientos energéticos se ofrecen como garantía para el desarrollo, la realidad es otra, es sabido los casos de desplazamiento, miseria, deterioro ambiental, incremento de la violencia y multitudes de otros problemas laborales, sociales, económicos, ambientales y culturales que provocan los proyectos energéticos sobre los territorios y sus culturas.]

[En Colombia aunque se incrementa la producción energética nacional, aumenta el número de familias desconectadas. En la Costa hace crisis el modelo privatizador, que amenaza con dejar a oscuras la región Caribe, luego de casi dos décadas de implementación. Aún así, en Bogotá, el Consejo Distrital desconociendo este y otros fracasos de la privatización en el mundo, aprueba privatizar el 20% de la Empresa de Energía de Bogotá una de las más importantes empresas de la ciudad.]

[Sin duda la cuestión energética es un asunto central en el debate político actual, que debe ser discutido con toda la sociedad colombiana, que debe ser contemplado en la nueva mesa de negociación con el ELN y debe incorporar los siguientes asuntos:]

[- Cómo dar lugar a una transición energética que rompa con la extrema dependencia de los combustibles fósiles]

[-Cómo frenar el avance de las nuevas fronteras geográficas y tecnológicas y las implicaciones que ello conlleva]

[- Cómo resolver los problemas de acceso a la energía, su uso y los impactos que el sistema energético ocasiones.]

[- Cómo garantizar la participación popular (localidades, comunidades, autoridades locales) en las decisiones, como lo han expresado las sentencias de la Corte constitucional.]

[La cuestión energética nos debe hace entender a la energía más allá de asuntos meramente técnico o económico, y asumirla en toda su integralidad, en la dimensión social, cultural y política. Discutir para quién y para qué la energía y, cómo acceder a la energía, desmontar el rol de la energía como commoditie o mercancía en el proceso de reproducción capitalista y asumirla como esencial para garantizar el buen vivir de los pueblos.]

[El gobierno nacional no puede seguir negándose a hablar sobre el modelo energético, la sociedad se está organizando y ha creado la Mesa Social Minero Energética Ambiental para la Paz, que se prepara a realizar encuentros en varias regiones del país. Mientras tanto, la gente continua en sus localidades debatiendo, elaborando propuestas, documentando los incumplimientos de las empresas energéticas y exigiendo a las autoridades que asuman su papel y garanticen los derechos a los pueblos. De ahí que en los próximos días dos audiencias del Congreso debatirán asuntos importantes. El 18 de noviembre, será la audiencia que lleva el nombre:]*[Utilidad pública beneficio de todos?]*[.]*[ Conflictos del uso de la figura de utilidad pública frente a la implementación de megaproyectos y los desalojos forzosos de población afectada]*[, convocado por el Movimiento Colombiano Ríos Vivos y varios congresistas. Y el próximo 2 de diciembre, en San Martín, Cesar se realizará la audiencia pública del Congreso para debatir sobre el proyecto de fracking de la empresa ConocoPhillips en el bloque VMM-3 ubicado en los municipios de San Martín, Aguachica y Rionegro.]

[El debate ya ha sido puesto, ahora todas y todos tenemos] [la palabra.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.] 
