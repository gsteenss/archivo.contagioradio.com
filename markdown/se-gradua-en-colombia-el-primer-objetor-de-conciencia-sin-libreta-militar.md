Title: Así es el camino para graduarse sin libreta militar
Date: 2015-03-03 10:42
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Diego Carreño Neira, libre de libreta militar, Objetor de conciencia Colombia, Primer objetor de conciencia graduado sin libreta militar
Slug: se-gradua-en-colombia-el-primer-objetor-de-conciencia-sin-libreta-militar
Status: published

###### Foto:JustaPaz 

##### **Entrevista con [Diego Carreño Neira]**: 

<iframe src="http://www.ivoox.com/player_ek_4153436_2_1.html?data=lZailZmXeo6ZmKiakp2Jd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPdzsrfjdTGrsbo0NeYxsqPp9DixM7S0MjNpYzZz5Cw0dHTscPdwpDU1MbIucLY0JDS0JDQpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Diego Carreño Neira ha sido el primer objetor de conciencia en Colombia en graduarse sin necesidad de la libreta militar.**

Gracias a el **articulo numero 2 de la ley de orden público, ya no es necesaria la libreta militar** para obtener cualquier título profesional o graduarse en la universidad. La modificación de la ley fue propuesta por la representante a cámara Angélica Lozano.

\[caption id="attachment\_5466" align="aligncenter" width="476"\][![ACOOC](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/11006435_339390442928846_6453496318966926997_n.jpg){.size-full .wp-image-5466 width="476" height="296"}](https://archivo.contagioradio.com/ddhh/se-gradua-en-colombia-el-primer-objetor-de-conciencia-sin-libreta-militar/attachment/11006435_339390442928846_6453496318966926997_n/) ACOOC\[/caption\]

Como lo señala Carreño este logro se da también gracias a la **lucha** iniciada hace 12 años por **colectivos antimilitaristas y objetores de conciencia que **se negaron a contribuir al gasto militar, en el uso de sus cuerpos y en el rechazo a la obligatoreidad de la libreta militar para graduarse o trabajar.

Diego Carreño Neira, en entrevista con Contagio Radio explica el nacimiento de organización social Acción Colectiva de Objetores y Objetoras de Conciencia (ACOOC), la lucha llevada a cabo por los objetores de conciencia, denunciando y difundiendo la problemática en todo el estado, e indicándonos la importancia que debe tener primero la educación y el trabajo sobre lo militar.

A partir de ahora **los jóvenes no deben tener la libreta para acceder a los trabajos,** sobre todo a los privados, requisito que anteriormente era obligatorio y no permitía a los jóvenes acceder a diversos trabajos, razón por la cual se vulneraban los derechos básicos de la persona.

Según Neira, en este momento existen más casos de objetores de conciencia que se han acogido a la medida  y están a la espera de graduarse. El viernes, ACOOC realizó una jornada de información en las distintas universidades, ésta finalizó con una actividad reivindicando la lucha antimilitarista y el logro histórico que supone la primera graduación sin libre libreta.
