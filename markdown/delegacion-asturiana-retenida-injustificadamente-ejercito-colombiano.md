Title: Delegación Asturiana retenida injustificadamente por Ejército Colombiano
Date: 2017-02-22 13:51
Category: Nacional, Paz
Tags: Delegación Asturiana, Implementación de los Acuerdos de paz, Mecanismo de Monitoreo, Zona Veredal La Elvira
Slug: delegacion-asturiana-retenida-injustificadamente-ejercito-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/zona-veredal-farc-e1484067202705.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [22 Feb 2017] 

A través de un comunicado, el Mecanismo de Monitoreo y Verificación reconoció que la retención por parte del Ejército a Mario Suárez, uno de los integrantes de la XIII Visita Asturiana de Verificación del estado de los Derechos Humanos en Colombia, fue injustificada, además, las autoridades tradicionales del Resguardo Indígena de Pueblo Nuevo, denunciaron que las **unidades militares vienen vulnerando los derechos de toda la población.**

Luego de 3 horas de retención, Mario Suárez y otros integrantes de la Delegación Asturiana, lograron visitar la Zona Veredal de La Elvira, en el Cauca, reunirse con los comandantes Pacho, Walter y Cauca de las FARC y **constatar la precaria situación que atraviesan los excombatientes en el proceso de agrupamiento.** ([Le puede interesar: Continúan incumplimientos del Gobierno en Zonas Veredales](https://archivo.contagioradio.com/continuan-incumplimientos-del-gobierno-en-zonas-veredales/))

### ¿Con qué se encontró la Delegación Asturiana en La Elvira? 

Suárez, señaló que **"se pudo comprobar los pocos avances que el gobierno ha realizado en cuanto a la infraestructura de casas, agua, luz y demás”**, y manifestó que la carencia de estas infraestructuras básicas ha obstaculizado “el proceso de transición y de incorporación de la guerrilla a la sociedad colombiana” y resaltó que pudieron constatar que "por parte de las FARC hay una férrea voluntad de seguir adelante con el proceso de paz".

La Delegación Asturiana hizo un llamado al Gobierno Nacional para que de celeridad a la implementación, **garantías de seguridad para los y las excombatientes, de cumplimiento a lo pactado y “resuelva el tema de presencia paramilitar”** en distintas Zonas Veredales y regiones del país. ([Le puede interesar: Zonas Veredales bajo la sombra paramilitar)](https://archivo.contagioradio.com/zonas-veredales-bajo-la-sombra-paramilitar/)

### No son nuevas las vulneraciones de derechos de las Unidades Militares 

Las autoridades tradicionales de Buenos Aires y Pueblo Nuevo, aclararon que los hechos ocurridos el 21 de febrero, en los que un excombatiente de las FARC resultó herido, no fueron en el bazar que organizó la comunidad "en el marco de la legalidad y la autogestión", sino que ocurrieron dentro de la Zona Veredal de La Elvira, **“no es correcto responsabilizar a la comunidad y sus organizaciones”**, puntualizaron.

Por último, denunciaron que en reiteradas ocasiones, unidades militares han “estigmatizado y criminalizado las iniciativas comunitarias del Resguardo” y **han vulnerado derechos humanos de la población en los puestos de control.**

###### Reciba toda la información de Contagio Radio en [[su correo]
