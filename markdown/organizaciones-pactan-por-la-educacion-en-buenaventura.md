Title: Organizaciones pactan por la educación en Buenaventura
Date: 2015-12-24 08:14
Category: Comunidad, Nacional
Tags: buenaventura, Comunicadores CONPAZ, Consejos comunitarios Buenaventura, educacion
Slug: organizaciones-pactan-por-la-educacion-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Buenaventura-educacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: granadinos2.blogspot.com 

###### [23 Dic 2015] 

Con la participación de 400 personas pertenecientes a organizaciones municipales, eclesiales y los consejos comunitarios de la zona sur y el casco urbano en Buenaventura, Valle del Cauca, se llevó a cabo el evento de revisión y validación para la educación en el puerto más importante sobre el pacífico colombiano.

A partir de la reunión entre los representantes de la Diócesis de Buenaventura, la secretaria de educación, concejales del municipio, y los consejos del Río Naya, Yurrumangui, Mayorquín, Anchicaya, Río Raposo y de Cajambre, se construyó un documento que recoge las propuestas presentadas durante el evento entre las que se encuentran:

-   Creación de un currículo diferencial en el tema del pacto de educación para buenaventura.
-   Remodelación del énfasis.
-   Recuperación y adecuación de la infraestructura (aulas de clase).
-   Construcción participativa de un currículo propio que recoja lo cultural, étnico, social y ambiental.
-   Reestructuración de la secretaria de educación, fijación de los criterios para el nombramiento de docentes.
-   Solución en cuanto al transporte de los educandos y educadores a las instituciones educativas.

Cabe recordar que el sistema educativo en Buenaventura, ha estado en el ojo del huracán, por los múltiples casos de corrupción que se han develado desde 2011, en los que se han revelado irregularidades en la firma de contratos con empresas operadoras del servicio, y por la aparición de "estudiantes fantasmas" que les permitían hacer fraude ante el Ministerio de Educación Nacional.

** Comunicador CONPAZ - Valle del Cauca **
