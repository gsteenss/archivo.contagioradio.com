Title: Corte Suprema conserva competencia para investigar a Uribe por compra de votos
Date: 2020-09-28 19:33
Author: AdminContagio
Category: Actualidad, Judicial
Tags: Álvaro Uribe, Claudia Daza, Ñeñe Hernández, Ñeñepolítica, uribe
Slug: corte-suprema-conserva-competencia-para-investigar-a-uribe-por-implicacion-en-compra-de-votos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Uribe.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este lunes **la Corte Suprema de Justicia -[CSJ](https://twitter.com/CorteSupremaJ)-, anunció que proseguirá la indagación en curso, en la que se investiga al expresidente Álvaro Uribe Vélez por una presunta operación de compra de votos en la  campaña presidencial de 2018** en la que el hoy presidente Iván Duque era candidato por el Partido Centro Democrático. (Le puede interesar: [Marta Lucia Ramírez y otros escándalos del narcotráfico en el gobierno Duque](https://archivo.contagioradio.com/marta-lucia-ramirez-y-otros-escandalos-del-narcotrafico-en-el-gobierno-duque/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CorteSupremaJ/status/1310620288477007873","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CorteSupremaJ/status/1310620288477007873

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La Corte expresó, que pese a la renuncia de Uribe como Senador, conservaría la competencia como quiera que era presumible **una «*participación activa de una de las empleadas de su Unidad de Trabajo Legislativo -UTL-*»** en los hechos, por lo cual, al ser actos relacionados con el servicio parlamentario podía entenderse que el fuero legislativo se extendía para ese caso en particular.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al hacer alusión a la empleada de la UTL, la Corte se refería a **Claudia “Caya” Daza, quien fue captada presuntamente en varias conversaciones telefónicas con el Ñeñe Hernández, en las que se hablaba de pasar «*plata debajo de la mesa*» en medio de la segunda vuelta presidencial del año 2018 en la que resultó electo Iván Duque.** Luego de revelarse esta información, Claudia Daza, renunció a su cargo como asesora del expresidente Uribe, por el pedido implícito de este, y abandonó el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Corte advirtió también que por carecer de relación con las funciones y el ejercicio de congresista, la Sala de Instrucción remitiría a la Fiscalía otras indagaciones preliminares que adelantaba contra el exsenador Uribe, tal como lo había hecho ya en el proceso que cursa en su contra por fraude procesal y soborno. (Le puede interesar: [Juez 30 mantiene la detención de Álvaro Uribe](https://archivo.contagioradio.com/caso-uribe-ley-600-o-ley-906-justicia-o-impunidad/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CorteSupremaJ/status/1310620289622069248","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CorteSupremaJ/status/1310620289622069248

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por otra parte, llama la atención que la Corte en la presente decisión sí acogió el criterio que le había solicitado Iván Cepeda, víctima acreditada en el caso por presunta compra de testigos, en el que **se advertía la participación en los hechos de miembros de la UTL del expresidente, para argumentar que la Sala de Instrucción debía conservar la competencia, aún luego de la renuncia de Uribe a su cargo como senador de la República.** No obstante, la Corte resolvió remitir dicho expediente a la Fiscalía General de la Nación. (Le puede interesar: [Renuncia de Uribe Vélez a su curul puede ser una maniobra para dilatar el proceso y generar impunidad](https://archivo.contagioradio.com/renuncia-alvaro-uribe-velez-no-responder-justicia-victimas/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
