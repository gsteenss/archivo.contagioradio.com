Title: 32 líderes indígenas han sido asesinados en el primer semestre de 2018
Date: 2018-08-30 12:39
Category: DDHH, Nacional
Tags: Cauca, Feliciano Valencia, ONIC
Slug: 32-lideres-indigenas-han-sido-asesinados-en-el-primer-semestre-del-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/arhuacos-indigenas-e1511801126214.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Aventure Colombia] 

###### [30 Agost 2018] 

Feliciano Valencia, senador por el partido MAIS, advirtió que en lo que va corrido del año **32 indígenas han sido asesinados, se perpetró una masacre, se han reportado 207 amenazas a líderes y autoridades tradicionales; y se han presentado 13 denuncias de reclutamiento forzado.** Asimismo señaló que los departamentos del Cauca, Nariño y la región del Catatumbo, son los lugares donde más se presentan violaciones a derechos humanos en contra de estas comunidades.

Adicionalmente, la Organización Nacional Indígena de Colombia ONIC, señaló en rueda de prensa, que en reiteradas ocasiones han denunciado la presencia de estructuras paramilitares como las **Águilas Negras o las Autodefensas Gaitanistas de Colombia,** que estarían utlizando panfletos como forma de intimidar a los líderes, que posteriormente son asesinados.

Ante la gravedad de la situación, la ONIC solicitó al gobierno de Iván Duque, la creación de una comisión conjunta, entre las autoridades gubernamentales, representantes del pueblo indígena y organizaciones de acompañamiento internacional, donde se pueda plantear una estrategia que permita salvaguar la vida de los integrantes de los pueblos orginarios. (Le puede interesar: ["Indígenas de Chocó confinados por presencia paramilitar"](https://archivo.contagioradio.com/indigenas-confinados-paramilitar/))

La plataforma manifestó que el aumento del pie de fuerza no debe ser la salida para solucionar las violaciones a los derechos humanos en el territorio, y que por el contrario debe ser la implementación de los Acuerdos de paz, la puesta en marcha de los planes de sustitución de cultivos de uso ilícito y **la salida dialogada a los conflictos con estructuras armadas**, las que garanticen la construcción de paz en los territorios.

###### Reciba toda la información de Contagio Radio en [[su correo]
