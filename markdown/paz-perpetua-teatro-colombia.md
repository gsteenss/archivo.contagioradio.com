Title: "La paz perpetua" una obra que reflexiona sobre la paz y la guerra
Date: 2017-08-24 15:56
Category: eventos
Tags: Bogotá, Cultura, teatro
Slug: paz-perpetua-teatro-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Paz-Perpetua-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Compañía Quinta Picota 

###### 24 Ago 2017

La Compañía Quinta Picota regresa en corta temporada con "**La Paz Perpetu**a", una obra del reconocido dramaturgo español Juan Mayorga bajo la dirección del mexicano Iván Olivares, con las actuaciones de Alexis Rojas, Iván Carvajal, Alejandro Buitrago y los actores invitados Wilson Forero y Julián Díaz.

**Del 17 al 26 de agosto en la Sala Ágora de la Academia de Artes Guerrero**, se presentará el montaje como preámbulo a la participación de la compañía en el 8vo Festival Internacional de Teatro de Mont – Laurier en Quebec – Canadá y El Festival de Teatro Brújula al Sur en Cali- Colombia durante el mes de septiembre.

La Paz Perpetua representó un gran reto para los actores, por la alta exigencia física en el escenario y la complejidad del texto, que **es muy pertinente por la situación política y social que atraviesa Colombia**.

Una pieza que **invita a reflexionar sobre la violencia y las políticas de estado relacionadas con la estrategia para combatir al crimen**; La paz y la guerra vistas a través de los ojos de cuatro perros a los cuales el autor ha dotado de características humanas y la capacidad de razonar.

<iframe src="https://www.youtube.com/embed/_KR54_85JW4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Sinopsis:**

Un pastor alemán, un cruce de varias razas y un rottweiler impuro compiten por ingresar en un cuerpo de élite antiterrorista y quedarse con el 'collar” del K9; pero estos perros a la vez simbolizan, paradigmáticos y diferentes perfiles humanos: **Odín, (Alexis Rojas)** es un perro de la calle, brillante por su olfato, pero escéptico; **John-John (Alejandro Buitrago)** es fiero pero artificial y **Emmanuel (Iván Carvajal)**, es reflexivo y filósofo. Las pruebas a las que se someten son dirigidas por un cuarto perro, un labrador viejo y mutilado, **Casius, (Wilson Forero)** que es ayudado por un quinto personaje que representa a toda la humanidad, **(Julián Díaz)**.

Las pruebas a las que se someten tienen como objetivo evaluarlos a nivel físico y mental, como la pregunta: ¿si usted tiene que salvar a una sola persona en una situación de terrorismo, ¿quién sería?, ¿el más débil, el más joven, o el de mayor peso simbólico? “Cada personaje responde a una tendencia de cómo abordar el tema de la seguridad frente a cualquier situación que atente contra la integridad, pero al final también los hace cuestionarse respecto a cómo comportarse frente a una situación de terrorismo”, asegura el actor Iván Carvajal.

En la obra de Mayorga, está presente la idea de que el fin no justifica los medios y la búsqueda de la paz no puede rebasar los derechos de las personas, pero a la vez pone de manifiesto que sin trasgredir las reglas de la sociedad es difícil poner fin a la guerra y garantizar la vida de los ciudadanos.

La Paz Perpetua no habla tanto del terrorismo sino de los dilemas que plantea la lucha contra este, para lo cual plantea tres escenarios: **la justicia por mano propia, la defensa organizada por los dirigentes y la razón que desvirtúa la violencia contra la violencia,** pero es el espectador quien tendrá a su alcance las razones de cada posición teniendo la oportunidad de tomar partido.

Bono de apoyo: \$30.000, preventa, estudiantes 2x1 y tercera edad \$20.000.  
Lugar: Sala Ágora (Academia de Artes Guerrero) Carrera 18ª No 43-50  
Informes y reservas: 4627252 - 3115617916
