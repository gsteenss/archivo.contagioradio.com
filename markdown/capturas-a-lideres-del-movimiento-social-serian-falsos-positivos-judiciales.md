Title: Capturas a líderes del movimiento social serían "falsos positivos judiciales"
Date: 2015-07-08 15:57
Category: DDHH, Nacional
Tags: Alberto Castilla, Atentados en Bogotá, Colombia Informa, Comité de Solidaridad con los Presos Políticos, Congreso de los Publos, ELN, falsos positivos, movimiento social en Colombia, senador Polo Democrático Alternativo, Universidad Pedagógica Nacional
Slug: capturas-a-lideres-del-movimiento-social-serian-falsos-positivos-judiciales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/11692989_10153312481105020_198244903_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4739299_2_1.html?data=lZygm5edfY6ZmKiakpiJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8biwsnc1JClsMPZ09ncjajFt9XdzdHOjcnJstbixM7OjcbHuNbVxM6SpZiJhpTijMnSzpDLs8Pdxteah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alberto Castilla, senador PDA] 

<iframe src="http://www.ivoox.com/player_ek_4739303_2_1.html?data=lZygm5iUd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRiMbXzcbfw8jNaaSnhqeg0JDIqYy608bbzdHNsoy3wtjhw4qnd4a2ksrRw5DXs8PmxpDQy8vWpdSfxcqYz9TScYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Franklin Castañeda, Comité de Solidaridad con los Presos Políticos] 

<iframe src="http://www.ivoox.com/player_ek_4739317_2_1.html?data=lZygm5iVe46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRiMbXzcbfw8jNs8%2FZ1JDRx5ClsMbswtPR1MaPhsbmztrRx9%2BPt9DW08qYxcbUuNbmwtiYxsqPrY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alexandra Bermúdez, vocera del Congreso de los Pueblos] 

<iframe src="http://www.ivoox.com/player_ek_4739328_2_1.html?data=lZygm5iWfI6ZmKiakpiJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbXzcbfw8jNs8%2FZ1JDRx5C5ssrqxtfgy8nFqIzExsnOyYqnd4a2lMzWxcaPksLXytTbw9GPt9DW08qah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Helberth Augusto Choachí, Universidad Pedagógica Nacional] 

###### [8 JUL 2015]

Integrantes del movimiento social en Colombia, rechazaron las capturas de las 16 personas, que desde el gobierno y diversos medios masivos, han sido tildados de integrantes del ELN, **estigmatizando y poniendo en riesgo la vida de  líderes y lideresas sociales,** jóvenes, estudiantes y periodistas.

**“*Las personas que están siendo detenidas no van a tener responsabilidad alguna frente a los últimos hechos en Bogotá***” asegura Franklin Castañeda, presidente de Comité de Solidaridad con los Presos Políticos, “*es lastimoso que cuando se habla de paz el gobierno profundice en actos de persecuciones políticas sistemáticas para dar respuesta a los atentados*”.

Alberto Castilla, senador del PDA, llamó** al gobierno para que se responda por el crimen del líder Carlos Pedraza, en lugar de presentar "falsos positivos judiciales"** y denunció la serie de seguimientos y estigmatizaciones de las que ha sido víctima el Congreso de los Pueblos.

La vocera del Congreso de los Pueblos, Alexandra Bermúdez, afirmó que los señalamientos de medios de información y del gobierno a los jóvenes capturados **limitan la independencia judicial y el poder público,** condicionan la acción de los jueces "*anticipando una condena señalándolos como miembros de la guerrilla del ELN*" y exigió el debido proceso y el derecho a la presunción de inocencia.

El secretario general de la **Universidad Pedagógica Nacional, Helberth Augusto Choachí**, afirmó que la comunidad universitaria viene siendo blanco de falsos positivos judiciales e indicó que los estudiantes de la universidad capturados, **“***se encontraban fuera de la ciudad, en Fusagasugá, preparando actividades académicas***”** en el momento de las explosiones.

Durante la rueda de prensa se denunciaron **más de 13 mil demandas administrativas por errores judiciales,** así mismo, entre el 2009 y el 2012, 8600 personas fueron vinculadas a la insurgencia, pero más del 75% de los capturados ya fueron declarados inocentes.

Los voceros del Congreso de los Pueblos afirman que las capturas de los líderes estudiantiles y sociales obedecen al "afan de mostrar resultados" más que a investigaciones serias y eficientes para establecer los verdaderos responsables de las explosiones.

Entre los capturados, se encuentra Heiler Lamprea, representante del Consejo Superior de la Universidad Pedagógica Nacional, Lorena Romo Muñoz, líder estudiantil del proceso de la Mesa Amplia Nacional Estudiantil, MANE y el periodista Sergio Segura, corresponsal de la Agencia Colombia Informa. ([Leer comunicado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/11037306_473751656125526_6999187627873523940_o.jpg)).
