Title: Universidad del Tolima a punto de un nuevo paro
Date: 2016-04-29 18:15
Category: Educación, Nacional
Tags: crisis universidad tolima, feu colombia, universidad del tolima
Slug: universidad-del-tolima-a-punto-de-un-nuevo-paro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/ASPU-Tolima.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASPU Tolima ] 

###### [29 Abril 2016 ]

Pese a la crisis estructural y tras un largo periodo de parálisis, el pasado 14 de marzo las directivas decidieron reanudar las clases en la Universidad del Tolima, eso si **sin los salones, laboratorios, profesores y funcionarios suficientes**, según Marcela Reyes, Representante de la Facultad Ciencias Económicas.

Actualmente hay clases, pero en las [[precarias condiciones](https://archivo.contagioradio.com/profesores-y-estudiantes-exigen-renuncia-del-rector-de-la-universidad-del-tolima/)] que han dejado tres años de mal manejo por parte del actual rector José Herman Muñoz, cuya gestión no ha permitido superar el déficit de más de \$19 mil millones que enfrenta la Universidad, entre otras porque según se denuncia, **miembros del Consejo Superior estarían recibiendo un salario mínimo por asistir a las reuniones**.

"Estamos en clases pero estamos trabajando a media marcha, muchas cosas están fallando y entraremos en cese de actividades", asevera la Representante, y agrega que los **recursos de funcionamiento sólo alcanzarán hasta mayo de este año**, por lo que estiman continuar las movilizaciones y asambleas para denunciar la situación.

Ante las [[demandas de los estudiantes, profesores y trabajadores](https://archivo.contagioradio.com/la-burocracia-le-cuesta-mas-de-10-mil-millones-anuales-a-la-universidad-del-tolima/)], el rector plantea reformar el régimen disciplinario con el que se rige el estudiantado, y que les impediría protestar contra la carga burocrática, que ha **duplicado la planta administrativa y reducido a la mitad del cuerpo docente de la Universidad**, o la desatención del Ministerio de Educación y de la Gobernación del Tolima, quienes no asistieron a la Audiencia Pública a la que estaban citados.

"Queremos una solución verdadera que se apoye en todos los estamentos", afirma Reyes e insiste en que no se debe sacrificar el presupuesto de las residencias ni del restaurante, lo que se debe hacer es **"recortar la burocracia desangradora" que tiene al claustro a punto de entrar en un nuevo paro**.

<iframe src="http://co.ivoox.com/es/player_ej_11378413_2_1.html?data=kpagmZ2YdZShhpywj5abaZS1kZ6ah5yncZOhhpywj5WRaZi3jpWah5ynca7V08jSzsaPlsbtxtiYj5Cpt9Xpxc7O0NnJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en [[Otra Mirada](http://bit.ly/1ICYhVU)] por Contagio Radio ] 
