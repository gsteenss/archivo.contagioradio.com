Title: Ecopetrol y Ocensa demandadas por comunidades de Santa Cruz del Islote
Date: 2016-07-27 15:27
Category: Ambiente, DDHH
Tags: Derrame petróleo golfo de Morrosquillo, Ecopetrol, Odesa, Santa Cruz del Islote
Slug: ecopetrol-y-ocensa-demandadas-por-comunidades-de-santa-cruz-del-islote
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Puerto-Coveñas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Dimar] 

###### [27 Julio 2016]

Tras 2 años del derrame de petróleo ocurrido en el puerto Coveñas y que afectó gran parte del golfo de Morrosquillo, los pobladores del Archipiélago de San Bernardo y particularmente los de las islas de Mucura y Santa Cruz del Islote, interponen ante el Tribunal Administrativo de Bogotá una **demanda colectiva contra Ecopetrol y Ocensa, como principales responsables de los impactos ambientales y sociales** que produjo el derrame y que se agudizaron luego de que las empresas no pusieron en funcionamiento los protocolos de asistencia y seguridad a los que los obliga la normativa.

"Santa Cruz del Islote es conocida como la porción de tierra más pequeña y densamente poblada del mundo (...) menos de media hectárea en la que viven más de 900 personas que se dedican a la pesca y al turismo" asegura el abogado Victor Cortes, quién agrega que desde 2014 la **pesca artesanal de langosta, caracol, pulpo y pescado de la que viven cientos de familias en la isla, se ha disminuido un 80%**, una situación que ha llevado a que el 60% de las madres cabeza de hogar tengan tendencias suicidas al enfrentarse a no contar con una alternativa económica para sostener a sus hijos.

En términos ambientales las consecuencias también han sido negativas, pues pese a que se limpió la mancha de petróleo, no se han logrado detener los efectos tardíos del derrame sobre el ecosistema coralino y se han provocado **migraciones de diversas especies marinas, a causa del alto indice de contaminación del agua**, con efectos devastadores para todo el Archipiélago; teniendo en cuenta que el Islote actúa como una barrera coralina que se ha visto altamente impactada por la operación del puerto petrolero de Coveñas.

La demanda exige una **reparación económica para las comunidades, el restablecimiento del medio marino** y la implementación de proyectos productivos sostenibles, como medios para reparar a los pobladores que aseguran nunca haber sido consultados para la construcción del puerto, ni haber sido atendidos por la emergencia social y ambiental que tuvieron que enfrentar pese a los derechos de petición que dirigieron a entidades como la Dirección Nacional Marítima, la Capitanía del Puerto, Ecopetrol, Ocensa y los Ministerios de Medio Ambiente y Minas y Energía.

<iframe src="http://co.ivoox.com/es/player_ej_12357538_2_1.html?data=kpegl5yZd5mhhpywj5aVaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5yncbfdxNnc1JCns9Pohqigh6adt4ampJCuxNTLpcXjjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
