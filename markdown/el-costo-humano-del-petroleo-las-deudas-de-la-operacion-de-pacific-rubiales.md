Title: "El costo humano del petroleo" Las deudas de la operación de Pacific Rubiales
Date: 2016-07-12 16:25
Category: Economía, Nacional
Slug: el-costo-humano-del-petroleo-las-deudas-de-la-operacion-de-pacific-rubiales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/CnKoVH_XgAAJC6T.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Imágen: Cajar] 

###### [12 Jul 2016]

Desde las 9 de la noche Contagio Radio transmite el lanzamiento del  Informe "El costo humano del petróleo". La recopilación de una serie de situaciones que afectan los derechos de los habitantes del entorno empresarial, así como de los propios trabajadores de la empresa canadiense y el entorno natural de la región.

\[embed\]https://www.youtube.com/watch?v=Tkdg5nypDx0\[/embed\]
