Title: Atentan contra Argemiro Lara, líder de restitución de tierras en Sucre
Date: 2016-11-17 19:18
Category: Nacional, Paz
Slug: atentan-contra-argemiro-lara-lider-de-restitucion-de-tierras-en-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/finca_la_europa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: eluniversal] 

###### [17 Nov 2016]

Las comunidades defensoras de tierras en Sucre, denunciaron que Argemiro Lara, líder de restitución en la finca La Europa, **fue víctima de un atentado del que salió ileso por la rápida reacción de su esquema de seguridad**. Argemiro también hace parte del Comité Permanente por la Defensa de los Derechos Humanos, CPDH, y de la Unión Patriótica. Es también uno de los líderes amenazados en repetidas ocasiones en esa región de Colombia.

Según la denuncia el hecho se produjo hacia las 10 de la mañana en la ciudad de Sincelejo. Hasta el momento se desconocen los resultados de las investigaciones inciales, sin embargo persiste la amenaza y el temor por parte de los habitantes de la finca La Europa, que siguen exigiendo la titulación del predio además de denunciar acciones por parte de paramilitares en repetidas ocasiones.

Según Argemiro Lara el atentado tuvo una relación directa con la lucha por la restitución de tierras y la titulación concreta a las **más de 60 familias que tienen derecho a ese predio**, puesto que desde el 2008 se han presentado fuertes amenazas y hostigamientos en su contra y en contra de la comunidad. ([Le puede interesar Amenazan a defensores de DDHH en Sucre](https://archivo.contagioradio.com/nuevas-amenazas-contra-integrantes-del-movice-capitulo-sucre/))

Adicionalmente Lara reiteró la denuncia sobre amenazas que se han presentado en los últimos meses y frente a las cuales **no se ha logrado ningún tipo de respuesta por parte de la Unidad Nacional de Protección**.

<iframe id="audio_13820750" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13820750_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
