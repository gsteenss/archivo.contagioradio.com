Title: "Debemos trabajar con la mayor imparcialidad y rigor": Patricia Linares
Date: 2017-10-01 08:00
Category: Nacional, Paz
Tags: Comite de Escogencia, JEP, Patricia Linares
Slug: magistrados_jurisdiccion_especial_paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/patricia_linares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Daniel Reina Romero] 

###### [27 Sep 2017] 

[Sectores de la extrema derecha colombiana arrecian contra la elección de los magistrados y magistradas designados para la jurisdicción especial de paz. ]Para **Patricia Linares, designada por el Comité de Escogencia como Presidenta de la Jurisdicción Especial** para la Paz, el debate es sano siempre y cuando las críticas y la polémica se desarrolle en el marco de la Constitución.

Linares señala que era inevitable que la selección del Comité de Escogencia generara distintas opiniones, no obstante, considera que se trata de perfiles elegidos por un Comité que siempre puso a disposición del público las entrevistas y las hojas de vida de las y los aspirantes.

"Cada uno de nosotros tenemos posiciones ideológicas políticas, religiosas y académicas, que hacen que construyamos nuestra propia historia, de eso se trata. Pero ahora, nuestro comportamiento y nuestra actividad debe acotarse al mandato mismo que especificó el Comité. **Un trabajo orientado al debido proceso, con garantía de imparcialidad y el mayor rigor para que se brinde seguridad jurídica** en las decisiones que se produzcan, y así aportar a una reconcilización", expresa Linares, quien agrega que la tarea ahora será tener una dedicación al 100% para que todo funcione de manera armónica.

### El papel de las mujeres 

Para la presidenta de la JEP, es positivo que el Comité le haya apostado a la materialización del principio de pluralidad. Los magistrados y magistradas escogidas representan distintas posiciones académicas y jurídicas, lo que, según ella, "alimenta la producción del trabajo de la JEP".

En el marco de esa pluralidad, asegura que el hecho de que el comité haya escogido al 53% mujeres, significa que **"se está reivindicando a más de la mitad de las victimas del conflicto, se honra a las personas que han logrado superar situaciones extremas de dolor** y de vulneración de derechos para reconstruir sus proyectos de vida, que a su vez contribuyen en la construcción de proyectos de vida regionales; pero también significa que **se reconoce a otras mujeres que hemos venido preparándonos para aportar a las soluciones políticas y jurídicas** para lograr superar el conflicto".

Finalmente, Linares considera que si bien la falta que se apruebe la Ley que pondría en marcha la JEP,  la Secretaría Ejecutiva si ha avanzado en destinar recursos para poner ne marcha la JEP.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
