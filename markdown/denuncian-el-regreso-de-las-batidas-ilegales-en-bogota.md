Title: Denuncian el regreso de las "batidas" ilegales en Bogotá
Date: 2019-08-06 18:20
Author: CtgAdm
Category: DDHH, Nacional
Tags: batidas ilegales, Bogotá, Derechos Humanos, Ley 1861, objeción de conciencia
Slug: denuncian-el-regreso-de-las-batidas-ilegales-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/libreta_militar_-e1463004534745.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Este lunes 5 de agosto se conocieron denuncias acerca  de batidas ilegales que se estaría llevando a cabo el Ejército Nacional en Bogotá. **Se trata de aproximadamente 100 jóvenes que habrían sido reclutados forzosa y arbitrariamente para definir su situación militar.**

Sin embargo, este hecho no es novedoso, pues se ha presentado paulatinamente en diferentes lugares de la capital, principalmente a las afueras de estaciones de trasmilenio y puntos cercanos a distritos militares.

**La Ley 1861 y la prohibición de las batidas**

La controversia de este tipo de acciones radica en que la Corte Constitucional en su sentencia c869 del 2011 cataloga estas batidas como ‘inconstitucionales’. Además, quedaron prohibidas en la ley 1861 art. 4 par. 2, donde se indica: “Por ningún motivo se permitirá a la fuerza pública realizar detenciones ni operativos sorpresa …” a jóvenes sin definición de situación militar. (Le puede interesar: [FFMM continúan realizando batidas ilegales pese a fallo de la Corte Constitucional](https://archivo.contagioradio.com/ffmm-continuan-realizando-batidas-ilegales-pese-a-fallo-de-la-corte-constitucional/))

Para Álvaro Peña abogado de JustaPaz “estas prácticas las hemos venido identificando no solo en Bogotá, sino en Ibagué, en Cali y algunos partes del departamento de Antioquia”. Además, apuntó que este “tipo de prácticas arbitrarias e ilegales” deben ser denunciadas por los jóvenes, por tanto, es importante recoger información acerca de los camiones que están haciendo el operativo, el lugar y los militares a cargo.

De esta manera, se pueda compartir una base de datos que contenga todos los casos y colaborar “con organizaciones de Derechos Humanos, como Defensoría del Pueblo, Personería y autoridades competentes que contribuyan a que el Ejercito Nacional deje de ejercer estás prácticas que están prohibidas legalmente”. Le puede interesar ([Corte Constitucional se pronuncia sobre objeción de conciencia y "batidas"](https://archivo.contagioradio.com/corte-constitucional-batidas/))

**El nuevo método de reclutamiento para jóvenes: “Batidas con invitación”**

La ley 1861 plantea ante todo un ‘debido proceso’, lo que pueden hacer los oficiales o militares es iniciar el proceso de definición militar a través de la inscripción, y posteriormente entregar una citación vía correo electrónico para primer examen. En ese sentido Peña hace hincapié **“la citación debe hacerse a primer examen no a jornada de concentración”.**

Diferentes organizaciones de objeción de consciencia han identificado un nuevo método que han denominado como “batidas con invitación”, donde no suben a los jóvenes a camiones, sino que se les cita y al siguiente día los reclutan. “Están acortando el tiempo en que el joven debe definir su situación militar, casi que en dos días están deteniéndolos lo cual configura una situación arbitraria con fines de reclutamiento”.

### **Los jóvenes ya no quieren prestar servicio militar** 

En gran medida, estos casos de aumento de reclutamiento, mediante batidas ilegales, corresponde a las cuotas que el Ejército Nacional debe tener acerca de las jornadas de incorporación “los distintos distritos militares deben cumplir con una serie de cuotas de jóvenes a reclutar”.

Sin embargo, el experto resaltó que ya los jóvenes no quieren prestar servicio militar de manera obligatoria, lo que en últimas hace que no se presenten a las citaciones. “Esto genera que el ejército tome medidas de batidas ilegales”, ya que por voluntad propia los jóvenes ya no se presentan.

Este caso ya se había presentado en el mes de mayo donde hubo jornadas de reclutamiento e incorporación a nivel nacional, paralelamente, también se denunciaron prácticas ilegales de este tipo. Actualmente, estos casos se repiten en Bogotá dado que, no se cumplieron las cuotas del ejército.

Ante estos hechos el abogado menciona retomar el dialogo sobre la obligatoriedad de la prestación del servicio militar que, en última instancia afecta a las comunidades más vulneradas de las periferias en Bogotá,

### **¿Qué deben hacer los jóvenes retenidos en "batidas" irregulares?** 

Para peña, los jóvenes sometidos a este tipo de prácticas pueden incurrir a la acción del ‘habeas corpus’ con la cual, y “dentro de un término de 36 horas, un juez debe declarar el desacuartelamiento inmediato de los jóvenes reclutados bajo esta práctica”.

Así, el abogado hace un acotamiento importante y es que los jóvenes **no firmen ningún tipo de documentación durante las batidas**, “muchas veces los hacen firman documentos donde declaran que no tiene ninguna causal de exoneración” lo traduciría que están allí voluntariamente.

Para el caso mencionó el artículo 12 de la ley 1861 donde están planteadas todas las causales de exoneración a la prestación de servicio militar. Y donde se estable el derecho a la objeción de conciencia por razones de políticas, éticas y religiosas “**Aquel joven que tenga alguna razón para negarse a prestar servicio militar, también tiene derecho a hacerlo”**. (Le puede interesar:[La Objeción de Conciencia, un derecho ignorado en Colombia](https://archivo.contagioradio.com/objecion-conciencia-derecho-ignorado/))

<iframe id="audio_39648761" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39648761_4_1.html?c1=ff6600"></iframe>
