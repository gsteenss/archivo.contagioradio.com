Title: Primer grupo de observadores de ONU ya esta en Colombia
Date: 2016-06-28 15:46
Category: Otra Mirada
Tags: acuerdo cese al fuego farc y gobierno, proceso de paz, verificación ONU
Slug: primer-grupo-de-observadores-de-onu-ya-esta-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/thumb2-logo-un-silk-un-flag-un-emblem.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  

##### 28 Jun 2016 

Ya se encuentra en Colombia el primer grupo de integrantes de la comisión de la Organización de las Naciones Unidas encargado de observar y verificar [el cese bilateral firmado entre la guerrilla de las FARC - EP y el Gobierno](https://archivo.contagioradio.com/este-es-el-texto-del-acuerdo-sobre-cese-bilateral-firmado-entre-gobierno-y-farc/) en los diálogos de la Habana la semana anterior.

Farhab Haq, portavoz del organismo internacional confirmó a través de la radio ONU, que "Los observadores, 23 en total, son provenientes de Argentina, Bolivia, El Salvador, Guatemala, México, Paraguay y Uruguay" quienes integran esta primer avanzada de la misión política que llegará al país.

Adicionalmente el funcionario aseguró que los miembros de este grupo de observación "se unen al equipo de avanzada de 20 civiles que ya se encontraba en Bogotá preparando el establecimiento de la Misión" añadiendo que se espera la llegada de un segundo grupo a principios del mes de julio.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
