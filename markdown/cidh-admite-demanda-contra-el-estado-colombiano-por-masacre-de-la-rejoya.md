Title: CIDH admite demanda contra el Estado colombiano por masacre de La Rejoya
Date: 2020-09-23 21:18
Author: AdminContagio
Category: Actualidad, DDHH
Tags: CIDH, La Rejoya, Masacre de La Rejoya
Slug: cidh-admite-demanda-contra-el-estado-colombiano-por-masacre-de-la-rejoya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/CIDH-admite-demanda-por-masacre-de-La-Rejoya-scaled.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**La Comisión Interamericana de Derechos Humanos -[CIDH](http://www.oas.org/es/cidh/)-, aceptó la demanda en contra del Estado colombiano, instaurada por las víctimas de la masacre de la vereda La Rejoya** ubicada entre los municipios de Popayán y Cajibío en el Cauca, en la que fueron asesinadas 10 personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según lo que se ha documentado y lo declarado por personas vinculadas a procesos judiciales, como responsables de este hecho; **la masacre fue perpetrada por paramilitares del Bloque «Mártires de Ortega» y miembros de Ejército Nacional.** No obstante, según las víctimas que acuden ante la CIDH «*las investigaciones a nivel penal no han sido encaminadas correctamente a fin de sancionar a todos los particulares y funcionarios públicos que participaron en la masacre de La Rejoya*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las razones que fundamentan la demanda, según las víctimas, son la participación activa de agentes del Estado (miembros del Ejército Nacional) en la masacre y la omisión de los órganos de justicia para condenarlos y ordenar acciones de reparación en favor de los afectados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el marco de estas investigaciones se produjo la condena de siete paramilitares, que **en versiones libres en los procesos ante la jurisdicción de Justicia y Paz declararon que  tuvieron apoyo de miembros del Ejército**; no obstante, a la fecha no se han individualizado los integrantes de la Fuerza Pública que participaron en el crimen.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La masacre de La Rejoya generó el desplazamiento forzado de cerca de 300 familias hacia Cali,  Popayán y otras regiones**, provocando que el tejido social del municipio de Cajibío se desestructurara y cesara la actividad de los líderes sociales de la zona por miedo a represalias que afectaran su vida e integridad y la de sus familias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a la demanda, lo único que respondió el Estado colombiano fue que la acción se presentó «*fuera de término*» y «*que los hechos expuestos en la petición no caracterizan violaciones a los derechos humanos*». No obstante, la CIDH desestimó esos argumentos y decidió dar continuidad al trámite admitiendo la demanda. (Le puede interesar: [Ataques a la CIDH son un golpe al corazón de las víctimas](https://archivo.contagioradio.com/ataques-a-la-cidh-son-un-golpe-al-corazon-de-las-victimas/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1308575834605785089","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1308575834605785089

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### CIDH y su reciente pronunciamiento frente a la responsabilidad del Estado colombiano

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Hace apenas un mes la CIDH condenó al Estado colombiano por considerarlo responsable de la violación de los derechos políticos del hoy senador Gustavo Petro**, cuando fue destituido como Alcalde Mayor de Bogotá e inhabilitado para ejercer cargos públicos por un lapso de 15 años en el año 2013.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esta condena la CIDH ordenó al Estado colombiano adecuar la legislación existente para evitar vulneraciones de esa índole y además ofició para que al senador se le indemnizara económicamente por los perjuicios causados «*por conceptos relativos al daño inmaterial, y el reintegro de gastos y costos»* que implicaron su defensa en el caso. (Lea también: [CIDH condena al Estado en el caso de Gustavo Petro](https://archivo.contagioradio.com/cidh-condena-al-estado-por-la-destitucion-en-la-alcaldia-de-gustavo-petro/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
