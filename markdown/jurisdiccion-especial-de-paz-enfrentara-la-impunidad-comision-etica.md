Title: "Jurisdicción Especial de Paz enfrentará la impunidad": Comisión Ética
Date: 2016-08-31 16:22
Category: Nacional, Paz
Tags: Comisión Ética, Diálogos de paz Colombia, Jurisdicción Especial de Paz
Slug: jurisdiccion-especial-de-paz-enfrentara-la-impunidad-comision-etica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/jurisdiccion-especial-de-paz-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [31 Ago 2106] 

En su más reciente comunicado la Comisión Ética aseguró que la [[Jurisdicción Especial de Paz](https://archivo.contagioradio.com/victimas-y-jurisdiccion-especial-de-paz/)] **"enfrenta la impunidad, en medio de un mar de ausencia de justicia"** y que el Sistema Integral de Verdad, Justicia, Reparación y Garantías de No Repetición es una oportunidad para desmoronar las mentiras mediáticas y los montajes procesales que se han convertido en verdades judiciales que han beneficiado a "los planificadores de la violencia contra los excluidos".

<div class="texte entry-content">

De acuerdo con la Comisión, hoy estamos ante la oportunidad de que el mecanismo de [[elección de magistrados del Tribunal Especial](https://archivo.contagioradio.com/asi-seran-seleccionados-las-y-los-jueces-para-la-jurisdiccion-especial-de-paz/)], asegure la independencia y eficacia judicial, para que quienes sean llamados a rendir cuentas **permitan reconstruir la verdad, sanar y generar una condición básica para la reconciliación**, en el marco de los principios del derecho internacional de los derechos humanos, del derecho humanitario y el derecho público penal internacional.

En la comunicación, la Comisión asegura que si el deber de la memoria y el derecho a la verdad en la Comisión de Esclarecimiento, así como la búsqueda de los desaparecidos, logran, con la participación de diversas organizaciones, su cometido, **cimentarán las bases con otras políticas públicas de educación y cultura**, tolerancia y transformación de los conflictos sin el uso de la fuerza y en el marco de una justicia económica, social y ambiental.

"Esperamos, como lo han expresado las organizaciones de víctimas, que los compromisos firmados sean cumplidos, y que habiliten mecanismos de discusión para transformaciones profundas en las doctrinas y procedimientos militares y policiales, que **siguen concibiendo a las expresiones sociales como enemigos internos**, por defender el ambiente y la riqueza de su país", continúa el comunicado.

La Comisión concluye invitando al gobierno colombiano y a la guerrilla del ELN a [[iniciar formalmente las conversaciones](https://archivo.contagioradio.com/hay-que-insistir-en-que-gobierno-y-eln-se-sienten-a-negociar-victor-de-currea/)], **desarrollando la agenda ya pactada**, para cimentar las bases hacia lo que algunos en Colombia han llamado Paz Completa.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

<p style="text-align: justify;">

</div>

 
