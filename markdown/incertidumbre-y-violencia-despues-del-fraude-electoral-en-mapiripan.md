Title: En Mapiripan continúa la represión contra la población luego del fraude electoral
Date: 2015-10-27 16:12
Category: Nacional, Política
Tags: Alcaldía de Mapiripan, Alexander mejía, Derechos Humanos, Fraude electoral, mapiripan, Partido ASI, Poligrow, Radio derechos Humanos, Registraduría Nacional
Slug: incertidumbre-y-violencia-despues-del-fraude-electoral-en-mapiripan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Mapiripan.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: notimerica.com 

<iframe src="http://www.ivoox.com/player_ek_9183651_2_1.html?data=mpallZuZdY6ZmKialJqJd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Bfrsbdy9fNtMLijMjc0NnNsoa3lIqvo8aPsMKf08rd1MrXrYa3lIqvldOPp9Di1dfOjdHFb9Hjw9HOxc6Jh5Shhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Conrado Salazar, Ex candidato de ASI] 

###### 27 oct 2015

En el municipio de Mapiripán, Meta, **continúan las exigencias ciudadanas** en medio de la incertidumbre y del miedo de la población frente a nuevas represalias de la fuerza pública en contra de la comunidad que ha demostrado su inconformismo con la **elección fraudulenta de Alexander Mejía** como alcalde, que contó con la intervención y beneplácito del actual alcalde, Jorge Iván Duque.

Alexander Conrado del partido Alianza Social independiente, indica que el caso de Mapiripan es **“insólito para la democracia colombiana”** ya que la administración municipal encabezada por el actual alcalde, **“direccionó el presupuesto”** en favor de la campaña de Mejía como aseguran distintos pobladores de la comunidad.

Se denuncia que **antes de las elecciones se informó a la Procuraduría y Fiscalía que se necesitaba de un acompañamiento para la jornada electoral**, pero la respuesta fue el envío de un registrador que “se declara abiertamente seguidor de la campaña oficial". Así mismo, la población afirma que “**el día de las elecciones se montaron unas mesas paralelas en unas fincas**”, donde se encontraron cajas con **logotipos de la Registraduría** y con “gente de comunidades indígenas votando allí”, asegura Conrado.

Pese al llamado de los pobladores, no hubo ninguna acción por parte de la policía  y la respuesta de las autoridades fue que  “la comunidad debía reforzar la vigilancia, ayudando a custodiar el sitio dónde se llevaban a cabo las elecciones”. Sin embargo **nada de esto garantizó la legalidad de las elecciones y con la presencia del alcalde en el conteo,** con marcadores que se borraban fácilmente y con la intervención violenta de la fuerza pública con gases lacrimógenos, las elecciones dieron como ganador al candidato de Mejía.

La situación que vive hoy Mapiripán es de incertidumbre. **“Ley seca” y “toque de queda”** son las medidas que ha dado el actual alcalde. **Distintas comunidades indígenas han querido hacer presencia en el casco urbano y exigir su voto**, mediante manifestaciones pacíficas, sin embargo, según indica Alexander Conrado, la policía “incitaría a que se vuelvan violentas”, además por cuenta de la represión de la fuerza pública varios indígenas fueron violentados “verbal y físicamente” por los policías.

De acuerdo al candidato, Alexander Mejía representante del Partido Liberal, fue impuesto como alcalde para favorecer los intereses del alcalde Jorge Iván Duque quien tiene nexos con la empresa italiana palmera **Poligrow, que “está confabulada con el alcalde municipal y tomarse todas las riendas del manejo administrativo”** del municipio.

De esta forma la comunidad pide una “comisión de alto nivel“ con la intervención de la Procuraduría y la Fiscalía” para “saber que ha sucedido con estas elecciones, y para que se preste atención a esta zona del país.
