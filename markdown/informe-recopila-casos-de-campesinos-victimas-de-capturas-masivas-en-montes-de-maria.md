Title: Informe recopila casos  de campesinos víctimas de capturas masivas en Montes de María
Date: 2020-07-30 21:06
Author: AdminContagio
Category: Actualidad, DDHH
Tags: capturas irregulares, Fuerza Pública, montes de maría, Seguridad Democrática
Slug: informe-recopila-casos-de-campesinos-victimas-de-capturas-masivas-en-montes-de-maria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/22fcf0c3-f2d9-4e88-9ae1-8d5c38fbd7a1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/dbc63fb3-966a-488c-9f1d-067a7d2b26ec.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-30-at-9.45.54-AM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Campesinos de Montes de María/ CSPP - Movice

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Organizaciones sociales y las víctimas de detenciones arbitrarias y masivas de Montes de María, presentaron a la Comisión de la Verdad (CEV) y a la Jurisdicción Especial para la Paz (JEP) el informe “Solo preguntaron por mi nombre" **que documenta 97 testimonios que dan cuenta de** **once patrones que demuestran la arbitrariedad e ilegalidad de las capturas** realizadas por la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El informe creado por el Comité de Solidaridad con los Presos Políticos, el [Movimiento Nacional de Víctimas de Crímenes de Estado](https://twitter.com/Movicecol) Capítulo Sucre y la Asociación de Campesinos y Campesinas de la Finca la Europa recopila las prácticas de detención utilizadas durante el gobierno de Álvaro Uribe Vélez y revela cómo, e**ntre el 7 de agosto de 2002 y el 6 de agosto de 2004, más de seis millares de personas fueron privadas de la libertad** por ser supuestos subversivos, la mayoría sin llegar a ser juzgados.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El gobierno presionó a la Fuerza Pública para detener una gran cantidad de guerrilleros en una sola operación, lo que en realidad significó la detención de cientos de campesinos haciéndolos pasar por insurgentes en el marco de las **Zonas de Rehabilitación y Consolidación,** medidas que en lugar de generar seguridad en sus habitantes, ocasionaron el aumento de ejecuciones extrajudiciales, detenciones y desplazamientos al dar potestad a **la Fuerza Pública para que pudiera realizar allanamientos sin orden judicial, empadronamientos, detenciones por sospecha, entre otros.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que la Corte Constitucional declaró inexequibles estas facultades, en Montes de María, las detenciones arbitrarias fueron masivas y permanentes. **Durante el periodo 2002 - 2009 se efectuaron más de ocho operaciones militares en las cuales aproximadamente 331 personas fueron víctimas de procesos judiciales arbitrarios desarrollados en su contra.** En ellos se violaron los principios internacionales y nacionales en materia de un juicio justo e imparcial. [(Lea también: Informe "Tierra y despojo en los Llanos" será entregado a la Comisión de la Verdad)](https://archivo.contagioradio.com/informe-de-tierra-indigena-campesina-comision-de-la-verdad/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### 156 campesinos capturados arbitrariamente en la Operación Mariscal

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Una de las operaciones militares que mayor impacto tuvo en la región fue la **Operación Mariscal** ocurrida el 17 de agosto de 2003, cundo la Fiscalía capturó a 156 personas en los municipios de Chalán, Ovejas, Corozal, Sincelejo y Colosó por una investigación abierta tan solo cuatro días atrás. Las acusaciones contra los procesados se sustentaron en su totalidad en testimonios entregados por supuestos desmovilizados acogidos en el programa de la red de informantes, bajo el cual recibían beneficios económicos y jurídicos.

<!-- /wp:paragraph -->

<!-- wp:image {"id":87598,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-30-at-9.45.54-AM-1024x931.jpeg){.wp-image-87598}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque **la Fiscalía Delegada estudió el proceso, y decidió ante el Tribunal Superior de Sincelejo revocar la medida de aseguramiento el 7 de noviembre de 2003 argumentando la ausencia de coherencia y veracidad de las pruebas,** el Estado no sólo hizo caso omiso a la decisión judicial y ordenó la recaptura de todas las personas bajo los mismos cargos, sino que ordenó la destitución y detención del fiscal que actuó en derecho.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el informe, a las detenciones les antecedieron prácticas de empadronamiento utilizadas posteriormente en informes de inteligencia de forma ilegal y tergiversada. De igual forma, en la mayoría de casos, la privación de la libertad se produjo sin orden de captura escrita, es decir que para detenerlos, la Policía utilizó artificios como supuestas prácticas de censos o requerimientos administrativos. De igual forma, se denuncia la clonación de los testimonios de informantes lo que llevó a que todos fueran procesados con información genérica e incompleta.

<!-- /wp:paragraph -->

<!-- wp:image {"id":87591,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/dbc63fb3-966a-488c-9f1d-067a7d2b26ec-1024x931.jpg){.wp-image-87591}

</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Medios de comunicación también fueron responsables

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En aquel entonces y con la intención de anunciar avances los avances de la Seguridad Democrática, aún cuando su legalidad y legitimidad eran motivos de duda, el informe denuncia que los medios magnificaron y difundieron las detenciones como “duros golpes a la guerrilla”, exponiendo en algunos casos los nombres y fotografías de las personas, afectando su buen nombre y presunción de inocencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En total, fueron 42 personas condenadas pese a las evidencias de la irregularidad y arbitrariedad de su detención, razón por la que se ha solicitado a la Jurisdicción Especial para la Paz (JEP) que, como tribunal independiente, revise las sanciones e investigaciones penales impuestas y así abrir la posibilidad de dignificar a las personas afectadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Se ha solicitado además, un acto de reconocimiento de responsabilidad por parte del Estado en Montes de María que incluya a los medios de comunicación que masificaron los rostros del campesinado bajo la etiqueta de milicianos y que en esta ocasión con la misma difusión restablezcan su buen nombre e inocencia ante el país. [(Lea también: Un diálogo sobre el impacto del conflicto armado en las poblaciones campesinas)](https://archivo.contagioradio.com/un-dialogo-sobre-el-impacto-del-conflicto-armado-en-las-poblaciones-campesinas/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
