Title: Comunidad Indígena Misak, un mes desplazados y sin respuestas estatales
Date: 2020-03-04 17:15
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Caquetá, Desplazamiento
Slug: comunidad-indigena-misak-un-mes-desplazados-y-sin-respuestas-estatales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Misak.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Comunidad Misak/

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_48622156" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_48622156_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

Después de un mes de desplazamiento no ha sido atendida la situación de desplazamiento de 50 personas de 11 familias de la comunidad Misak de Belén de los Andaquíes en Meta quienes dejaron su territorio después del 9 de febrero [cuando fueron asesinados Felipe Angucho Yunda e Ismael Angucho Yunda,](https://twitter.com/AisoMovimiento/status/1227116933205680129)fundadores de la comunidad del Águila, ausencia que ha ocasionado el temor entre la comunidad y su posterior partida del territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Didier Chirimuscay, comunicador social indígena, quien ha acompañado a la comunidad, desde el asesinato de sus dos líderes, los integrantes del Cabildo El Águila no se han sentido seguros en su territorio por lo que el día 13 se desplazaron al casco urbano del municipio donde habitan **desde hace casi un mes en condiciones de vulnerabilidad un total de 50 personas de las que el 60% son niños.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque desde la Unidad de Víctimas se realizó una entrega de alimentos a las 11 familias del pueblo Misak, que fueron desplazados, sobre su situación, Chirimuscay apunta que no hay respuestas efectivas por parte de las instituciones por lo que por ahora las Autoridades Indígenas del Suroccidente (AISO) han coordinado una misión humanitaria para este 8 y 9 de marzo y así recolectar víveres, ropa y alimentación para las familias desplazadas. [(Le puede interesar: En 2019 se registraron 25.303 nuevos desplazados en Colombia: CICR)](https://archivo.contagioradio.com/en-2019-se-registraron-25-303-nuevos-desplazados-en-colombia-cicr/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Caquetá ha sido históricamente uno de los espacios más estratégicos de los grupos armados y del narcotráfico lo que tiene a las comunidades en situaciones álgidas y de presión", afirma el comunicador quien señala que los grupos ilegales **"necesitan generar miedo en la población, el tema de fondo es el tema territorial".** [(Le puede interesar: Cinco claves para entender el informe de Michel Forst sobre la situación de DD.HH. en Colombia)](https://archivo.contagioradio.com/cinco-claves-para-entender-el-informe-de-michel-forst-sobre-la-situacion-de-dd-hh-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, Felipe Angucho Yunda e Ismael Angucho Yunda, los fundadores de la comunidad misak asesinados, exigían el acceso a los derechos territoriales en el municipio Belén de los Andaquies, para lo cual exigían al Gobierno el cumplimiento de los acuerdos sobre tierras firmado en Cauca en 2005.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Comunidad Misak también debe enfrentar a las empresas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A este escenario se suman la presencia de multinacionales con intereses en monocultivos y ganadería, han dificultado el desarrollo de las comunidades campesinas y los ocho pueblos indígenas que habitan en el departamento; **prueba de ello son los 81 resguardos indígenas que continúan en peligro por 37 contratos petroleros tanto en Putumayo como en Caquetá.** [(Le recomendamos leer:Concesiones petroleras amenazan a 81 resguardos en Amazonía)](https://archivo.contagioradio.com/concesiones-petroleras-amenazan-a-81-resguardos-en-amazonia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Didier Chirismuscay advierte sobre la desinformación que se genera a partir de las declaraciones de los medios de comunicación corporativos y de dirigentes políticos al estigmatizar a las comunidades étnicas o a defensoras y defensores de DD.HH.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Muchas veces opinar desde los micrófonos y las instituciones es una irresponsabilidad, pero la estigmatización a través de los medios o compararnos con actores armados o que las protestas están infiltradas" expresó el comunicador, a propósito de declaraciones que han llegado desde las diversas carteras del Gobierno como la del ministro de Defensa Luis Carlos Villegas quien atribuyó los asesinatos de líderes sociales a "líos de faldas" o las de la ministra del Interior, Alicia Arango que comparó el asesinato de defensores de DD.HH. con la muerte de personas por robos de celulares.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
