Title: ¿Por qué la Ley ZIDRES es anticampesinos?
Date: 2018-05-03 16:12
Category: DDHH, Política
Tags: campesinos, colombia, tierras, Wilson Arias, Zidres
Slug: ley-zidres-es-anticampesinos-willson-arias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/tierras-curvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [03 May 2018] 

Como lamentable calificó Wilson Arias, experto en el tema de tierras e integrante del Polo Democrático, el fallo de la Corte Constitucional que respalda a la ley que permite la creación de las Zonas de Interés de Desarrollo Rural, Económico y Social. Este hecho, de acuerdo con el analista, podría profundizar la crisis del acaparamiento de tierras en el país, impedir la principal intensión del acuerdo de paz de generar una re distribución del territorio y por el contrario dar paso a una paradoja de una ley **"anticampesina y antipobres"**.

"Esta es la posibilidad de que tierras baldías de la nación y tierras de la reforma agraria, sean entregadas al gran capital. Lo que se viene es que todo lo que ha adelantado el gran capital, en donde se ha hablado de **la cancha de las ZIDRES, de varios millones de hectáreas, sea consumado en su hecho**" afirmó Arias.

Para el analista, detrás de estas grandes extensiones de tierra estarían los intereses de empresarios nacionales e internacionales, "los intereses son de corporaciones gigantescas. Solamente Cargill, compañía agroalimentária norteaméricana, **se creó con 52 mil hectáreas, en el Vichada"** expresó Arias y agregó que hay otras empresas que estarían aspirando a adquirir más de 400 mil hectáreas en los llanos orientales.

### **El Decreto Fast Track 902  y la ley ZIDRES**

De igual forma, Arias afirmó que durante la implementación del primer año de los Acuerdos de Paz, se aprobó un decreto que permite que grandes empresarios puedan acceder a la formalización de tierra de la reforma agraria. (Le puede interesar: ["ZIDRES tendrían que ser consultadas con comunidades"](https://archivo.contagioradio.com/zidres-ponen-en-riesgo-la-labor-de-mas-de-13-millones-de-campesinos-en-colombia/))

En principio en el Acuerdo de Paz se había pactado que solo un sujeto definido como campesino o víctima del conflicto armado tendría acceso a esta reforma, sin embargo, luego de que el proyecto pasará por el Congreso se aprobó el ingreso de empresarios, "ahora puede ser Luis Carlos Sarmiento Angulo, Julio Mario Santodomingo, Ardíla Lule. **Los mismos que han acaparado tierras en todo el país ahora van por esta última gran frontera** agrícola colombiana y lo que nos quedaba de tierras campesinas" señaló Arias.

En ese mismo sentido, aseguró que los Acuerdos de Paz de La Habana, prometían la re distribución de la tierra. Una urgencia que comparten diferentes organizaciones que han alertado sobre la concentración de tierras, la desigualdad y la necesidad de una democratización del suelo para garantizar el desarrollo de los sectores productivos.

### **¿El fin de los campesinos en Colombia?** 

Organizaciones defensoras de derechos humanos, campesinas y afro, argumentaron, frente a la ley ZIDRES, que una de sus mayores afectaciones es provocar la desaparición de la fuerza campesina del país, **debido a que no existirán tierras destinadas al desarrollo agrícola del país y las pocas que queden serán de igual forma acaparadas**.

Para Arias, este es un proceso que ya se ha venido dando y profundizado en Colombia, asimismo recordó que en el país, la lucha histórica del movimiento campesino ha sido por lograr una reforma agraria que permita la re distribución equitativa de la tierra, "ha venido haciéndose más angosta la presencia campesina y en el último periodo, con motivo de la agroexportación, lo que ha venido **implantándose es una gran plantación en detrimento de la producción de la economía campesina**".  (Le pude interesar: ["Ley ZIDRES profundiza afectaciones a mujeres rurales"](https://archivo.contagioradio.com/ley-zidres-profundiza-afectaciones-las-mujeres-rurales/))

De igual forma, el analista afirmó que esta situación continúa dándose, a pensar de que se ha comprobado que la producción campesina es mucho mayor, eficiente y adicionalmente, provee el 60% de los alimentos que conforman la canasta familiar.

<iframe id="audio_25771746" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25771746_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
