Title: Trabajadores de la industria petrolera se cosieron la boca exigiendo sus derechos
Date: 2016-05-06 17:54
Category: DDHH, Nacional
Tags: derechos laborales, Industria petrolera
Slug: trabajadores-de-la-industria-petrolera-se-cosieron-la-boca-exigiendo-sus-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Trabajadores-industria-petrolera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [6 May 2016]

12 extrabajadores que ahcen parte de la Asociación de Trabajadores Discapacitados y Enfermos de la Industria Minero Energética, ASOTRADEIMENE, permanecen desde hace 18 días encadenados y algunos con la boca cocida en las puertas del Palacio de Justicia de Neiva, con el fin de llamar la atención frente a la violación de sus derechos fundamentales por parte las empresas de la Industria Minero Energética.

Estos trabajadores se enfermaron y sufrieron accidentes laborales mientras trabajaban en empresas petroleras, que nunca les respondieron por lo sucedido. Desde la ASOTRADEIMENE, denuncian además  los despidos injustos y el no reconocimiento de pérdida de capacidad laboral por enfermedades, accidentes, lesiones y discapacidades originadas en el trabajo.

De acuerdo con los trabajadores y la denuncia publicada por el representa Alirio Uribe, las empresas, por su parte, realizan procesos de reubicación laboral, mal orientados, esto con la intención de justificar un futuro despido de los trabajadores en condición de discapacidad. De igual forma, no se cumple con los procesos de rehabilitación.

En principio fueron 16 personas de ASOTRADEIMENE que se encadenaron, pero hoy quedan 12, 3 de ellos con los labios cocidos exigiendo respuestas y acciones por parte del Estado para que se les garanticen sus derechos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
