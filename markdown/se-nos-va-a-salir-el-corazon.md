Title: Se nos va a salir el corazón
Date: 2016-10-18 08:32
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: acuerdo de paz, marcha de las flores, Movilización Bogotá
Slug: se-nos-va-a-salir-el-corazon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_25.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio/Gabriel Galindo 

#### **Por [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

[Palpita la arena política en el país… se habla de política en el país. Por primera vez un partido de la selección Colombia en donde se triunfa de visitante pasa tan desapercibido. Hoy el país atraviesa una coyuntura que se ha caracterizado entre otras cosas, por una innegable explosión de opiniones y manifestaciones en las redes sociales ante los hechos tan significativos que se han presentado y que han exaltado, exacerbado y hasta fanatizado a muchos en torno a sus posturas políticas.]

[Toda esta manifestación, parece ser síntoma de que culturalmente los colombianos estábamos tan reprimidos políticamente, que solicitábamos que los hechos nos sacudieran con fuerza para que, de una u otra forma, quedáramos inmersos en un debate político absolutamente necesario.   ]

[Es que no es para menos: previo al plebiscito, las manifestaciones de apoyo, de rechazo, los debates, la propaganda, los buenos argumentos, los mitos, las mentiras, las verdades, etc. se convirtieron en una dosis diaria. Luego vino el día de las elecciones y con los resultados, la desazón se apoderó de muchos quienes no comprendían cómo el No había ganado. No importaba que hubiese sido por estrecho margen, total había ganado… para mitigar el desconcierto algunos nos preguntamos ¿qué clase de estadísticas estuvieron desarrollando las firmas encuestadoras? Pues la gran mayoría daba como vencedor al Sí.]

[El uribismo engalanado con una victoria que no esperaba, saltó a la arena como vencedor, y muy a pesar de lo que muchos pensaron, estos tipos elaboraron discursos de paz, de consenso, tal vez pensando en las elecciones del año 2018, actuando con cabeza fría, calculando (como siempre ha sido su característica) lo que harían con esa victoria política insospechada.]

[La gente se manifestó en las redes sociales. Yo creo que el país que más trinó y posteó ese domingo 2 de octubre en la noche y la siguiente semana fue Colombia. La rabia se apoderó de muchos y muchas, rabia que contrastaba asombrosamente con un Uribe que ahora aparecía hablando suavecito. Como si fuera poco, Santos anunció el inminente fin del cese bilateral del fuego y eso cundió de pánico y distopías a la aún traumatizada sociedad colombiana, pues como una corroboración nefasta, Alape y Timochenko trinaban precauciones a sus unidades guerrilleras.]

[El corazón de las víctimas, el corazón de los que comprenden profundamente el conflicto colombiano, comenzó a latir más fuerte de la ansiedad. Mientras unos pastores de iglesia cristiana celebraban el triunfo del No desde una cómoda casa cual si hubiese ganado la selección en Asunción, miles de personas llenaron la plaza de Bolívar y la carrera séptima en Bogotá como no se veía más o menos desde el año 2007 cuando los estudiantes del país se rebotaron ante la reforma educativa.  ]

[Eso fue un respiro, fue un alivio sincero, pues muchos incrédulos comprendieron que salir a marchar es vivir la acción política y eso es algo que vale más que 1000 posteos o trinos. En ese marco, mientras se esperaba qué diría la derecha uribista, al final de reuniones y faranduleadera, salieron con un chorro de babas ante sus seguidores, pues en esencia no propusieron nada nuevo, y a parte de todo se negaron a sentarse con las Farc, lo que dio cuenta de que todo esto más parecía una tonta pataleta de su líder buscando protagonismo o los mejor una jugada certera que apuntaría a embellecer al decaído Centro Democrático quien anda buscando novia de apellido Lleras.  ]

[Aun con la “plebitusa” encima, con una incertidumbre jurídica tan tremenda y aparte de todo con los “nimierdistas” ya presionando para que la gente volviera a la inferencia de siempre y dejara de publicar cosas sobre política, o señalando que nada sirve, que lo mejor es seguir entregado al consumismo como un completo autómata, entonces otro hecho nos sacudió; la noticia de la corrupción y el claro bandolerismo electoral con que se llevó a cabo la campaña del No señalado por Juan Carlos Vélez en una entrevista, indignó hasta a los del No, y se sumó a la cadena de hechos inesperados.]

[Fueron hechos tan inesperados y tan indignantes que inclusive algunos cegados por la rabia el claro error de método que ésta produce, comenzaron a insultar por equivocación a Luis Carlos Velez (el de caracol radio) pues nadie identificaba plenamente quién era ese empresario llamado Juan Carlos Vélez que de una forma absolutamente descarada aseguraba que “buscaron que la gente saliera a votar verraca”.]

[La marcha del miércoles 5 de octubre tuvo efectos políticos claros. De una parte, alentó el rostro de Santos, que lució desfigurado el domingo pasado, refrescó las esperanzas del equipo negociador, mejoraron los trinos de las Farc, quienes sintieron directa e indirectamente con esa manifestación un respaldo masivo si bien no a ellos propiamente, pero si a una causa en la que ellos creen; así mismo, y mal haríamos en decir que no pero ¡sí!, esa movilización tuvo que incidir en la decisión final del comité noruego del Nobel, quien el día viernes 7 de octubre decidió otorgar el Nobel de paz a Juan Manuel Santos. Si bien el Santo no tiene tanto merecimiento como otros personajes que han trabajado mucho más por la paz en Colombia, el milagro internacional lo decía todo: “Colombia: el mundo te tiene en cuenta y quiere la paz”.]

[No sé qué ocurriría con los derechistas del Centro Democrático la mañana del 7 de octubre o quién tendría la embarazosa misión de informar a un líder tan vanidoso como Uribe sobre ese baldado de agua fría; no me imagino qué cara hicieron los Pastores ante una noticia de este calibre… lo que sí es cierto es que, en la arena política, todas las balanzas se movieron, y ese espaldarazo internacional del Nobel funcionó como impulso a la causa política del acuerdo.]

[En menos de una semana ha sucedido todo esto… ¿cómo no decir que se nos va a salir el corazón con esta cantidad de hechos? ¿Cómo no decir que estamos viviendo un momento histórico? ¿Cómo no asegurar que la sociedad colombiana ha comprendido poco a poco que redes sociales y política pueden ir de la mano? ¿cómo negar la utilidad de las marchas? Quedaron sentados los mezquinos defensores de la indiferencia, porque lo que se viene es un cabildo abierto, lo que se viene es más movilización, más opinión, más certeza de que aunque el corazón pareciera que se nos sale, precisamente lo mejor que podemos hacer como colombianos y colombianas, es no tratar de contenerlo, más vale un corazón latiendo con fuerza por causas políticas, que una vida entera sin ser parte de la historia política del país.   ]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
