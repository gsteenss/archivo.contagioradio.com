Title: La violencia contra la mujer es mucho peor al interior de la cárcel
Date: 2015-11-25 18:49
Category: Mujer, Nacional
Tags: Día Internacional de la Eliminación de la no Violencia contra la mujer, Gloria silva, Indulto, Indultos, mujeres, Población penitenciaria, Presas políticas, Reclusas
Slug: al-interior-de-la-carcel-la-violencia-contra-la-mujer-es-mucho-peor
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/mujeres-en-las-carceles.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Federico Ríos Escobar 

<iframe src="http://www.ivoox.com/player_ek_9516042_2_1.html?data=mpqemJWYdo6ZmKiak5WJd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKf187czsrSp8rVjMjc0NnWpYzgwpDa18%2FJtozZ1JDa18jMs4zkxtTfjcbQb8ri1crfy9TWb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Gloria Silva, Comité de Solidaridad con Presos Políticos] 

###### [25 nov 2015]

La población carcelaria femenina es víctima constante  de la violación de sus derechos, para la abogada Gloria Silva,  de la Fundación Comité de Solidaridad con los Presos Políticos, esta vulneración esta enmarcada en dos formas: la primera viene con la violencia que se ejerce en el momento de la captura y la segunda tiene que ver con la situación penitenciaria en general.

Para la jurista  las reclusas tienen necesidades particulares "por el hecho de ser mujeres", pero que en ningún momento les son atendidas necesidades como la de la maternidad. A esto se suma el maltrato que se le da a sus familiares y la imposibilidad de aportar con un sustento para ellos por el hecho de estar en la cárcel, afirma.

De acuerdo con cifras del Inpec, hasta enero del año 2015, la población carcelaria y penitenciaria en Colombia, estaba conformada por 116.760 reclusos; de los cuales, 8120 son mujeres.

“La situación y la problemática de violencia en el país están relacionadas con un sistema de desigualdad social y de diferencia de clases que reproducen unas relaciones de poder en escenarios sociales y familiares” que se observan también en los centros penitenciarios con las reclusas, especialmente a las presas políticas, asegura Silva

Para la abogada, la situación de las detenidas políticas tienen una carga mayor que se ha infundido alrededor de la población reclusa, ya que se les considera más peligrosas que las demás.

### **Indulto a guerrilleros de las FARC** 

Frente al [anuncio del indulto que daría el Gobierno Nacional](http://bit.ly/1IhFKhd)  a 30 guerrilleros que se encuentran en las cárceles del país, indicó que aunque celebra esta iniciativa, deben ampliarse estos gestos humanitarios de paz, tanto del Estado como de parte de las FARC.

Afirmó que desconoce si en la lista de indultados hay mujeres; sin embargo, afirmó que las acciones y huelgas que adelantan otros presos políticos en las cárceles del país no tienen relación con la decisión del Gobierno. Finalmente, Gloria Silva manifestó la importancia de  tener en cuenta la situación y las condiciones de las presas políticas en el país.
