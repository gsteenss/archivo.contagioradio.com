Title: Déficit de Universidad Distrital superaría los 60 mil millones en 2019
Date: 2018-12-07 15:57
Author: AdminContagio
Category: Educación, Movilización
Tags: Déficit, Educación Superior, Manuel Sarmiento, Peñalosa, universidad
Slug: deficit-universidad-distrital-2019
Status: published

###### [Foto: @UneesCol] 

###### [7 Dic 2018] 

En medio de un proceso de movilización social por superar la crisis que atraviesa la educación superior pública, concejales de oposición en Bogotá denuncian que **proyecto de presupuesto de la capital para 2019, dejaría un déficit de cerca de 68 mil millones de pesos a la Universidad Distrital**. Aunque los cabildantes han planteado posibles soluciones a este problema, el Concejo no ha tenido “voluntad política para resolverlo”.

En el primer debate del Proyecto de Acuerdo Nº520 de 2018, con el que se reglamenta el presupuesto de Bogotá para el siguiente año, Ricardo García Duarte, rector de la Universidad Distrital Francisco José de Caldas (UD), explicó que **para funcionamiento de la Institución se requieren 368 mil millones de pesos,** no obstante, **el Concejo aprobó recursos por 332 mil millones**.

\[caption id="attachment\_59022" align="aligncenter" width="755"\][![Presupuesto Universidad Distrital 2019](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Captura-de-pantalla-2018-12-07-a-las-11.25.32-a.m.-755x109.png){.wp-image-59022 .size-medium width="755" height="109"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Captura-de-pantalla-2018-12-07-a-las-11.25.32-a.m..png) Presupuesto Universidad Distrital 2019\[/caption\]

Según el concejal del Polo, Manuel Sarmiento, ni siquiera los 332 mil millones estarían asegurados, puesto que parte de ese dinero, 30 mil millones, se obtendrían vía estampilla; pero en los cálculos iniciales de recaudo de este gravamen se contaban con 15 mil millones que en el proceso de discusión del Presupuesto para Bogotá, ‘mágicamente’ se duplicaron. **En caso tal de que las proyecciones de recaudo se cumplieran, el déficit de la UD sería de 51 mil millones**.

Sarmiento recordó que durante el presente año, la Institución tuvo un déficit de 20 mil millones de pesos, mientras que “en infraestructura es de 116 mil metros cuadrados, es decir, el equivalente a 16 estadios como el Campín”. En caso de que se cumpliera el pronóstico de recaudo de la estampilla presentado inicialmente, y se sumará a ello el déficit en inversión, calculado en 17 mil millones, **a la Universidad le harían falta 68 mil millones de pesos para el próximo año**.

A esta situación se suma la denuncia por pérdida de cupos en pregrado, de acuerdo al Concejal, en los últimos años han entrado a la UD cerca de 2 mil estudiantes menos, mientras se ha aumentado el cupo en los posgrados. Dicha lógica, **respondería a un afán de las Universidades por autofinanciarse**: en razón de que los posgrados tienen un mayor costo en sus matriculas que los pregrados.

### **Dinero sí hay, lo que no hay es voluntad política** 

Sarmiento afirmó que desde la oposición han planteado dos soluciones a esta crisis: usar los dineros que estaban destinados para la construcción del “edificio de lujo que Peñalosa quiere construir en El Retiro”, y que ahora será financiado por la valorización; o redirigir la inversión que pretende hacer el Alcalde en el Bronx, con la construcción de un edificio, que según la Secretaría de Planeación es inviable, en términos de su estructura. (Le puede interesar: ["Concejo de Bogotá parece un comité de aplausos para Peñalosa: Manuel Sarmiento"](https://archivo.contagioradio.com/concejo-de-bogota-parece-un-comite-de-aplausos-para-penalosa-manuel-sarmiento/))

Con la primera propuesta, **el Distrito tendría disponibilidad de 80 mil millones, mientras que con la segunda, la cifra ascendería a 180 mil.** Sin embargo, el Concejal manifestó que el comportamiento de la bancada de concejales que apoya la Alcaldía es de aplanadora; razón por la que no se aprobaron las propuestas de la oposición, que fueron nuevamente presentadas durante el debate del Proyecto que se llevó a cabo en el Concejo.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
