Title: Los triunfos del movimiento estudiantil tras un mes de paro
Date: 2018-11-15 10:06
Author: AdminContagio
Category: Educación, Movilización
Tags: estudiantes, Marchas, Movilizaciones, Paro Nacional
Slug: los-triunfos-del-movimiento-estudiantil-tras-un-mes-de-paro
Status: published

###### [Foto: Kisyoshi Katsukawa] 

###### [14 Nov 2018] 

Tras un mes de paro estudiantil (iniciado el 11 de octubre), integrantes del movimiento hacen una evaluación de las conquistas alcanzadas hasta el momento, los resultados de la Mesa de Concertación con el Gobierno, y los objetivos que aún faltan por alcanzar. Adicionalmente, anuncian los próximos escenarios de movilización y articulación con diferentes movimientos sociales.

Según Valentina Ávila, delegada de la Unión Nacional de Estudiantes por la Educación Superior (UNEES), la coyuntura actual responde a la renovación de un movimiento estudiantil que comenzó a darse desde mediados de 2017, y hoy ve sus frutos con un auge en las 61 Instituciones de Educación Superior (IES) públicas. En esa medida, la primera victoria de este movimiento ha sido el hecho de que se han vinculado institutos técnicos y tecnológicos, así como instituciones privadas.

Que este movimiento se haya construido desde las bases y con amplitud, ha facilitado lograr un pliego de alcance nacional que goza de apoyos generales; así como ha logrado que muchas de las IES construyan pliegos locales, de forma tal que puedan sortear las dificultades propias de cada región y exigir lo que requiere cada Institución de acuerdo a su contexto.

La amplitud del movimiento y la evaluación que ha permitido la construcción de pliegos locales, así como el nacional, también ha favorecido la constitución de un movimiento que según Ávila se piensa en el largo plazo, lo que ha llevado a los estudiantes a exigir no solo temas presupuestales, sino tratar temas como la deuda pública del Estado con la educación superior pública, o la autonomía universitaria.

Una de las más grandes victorias de los estudiantes es que la educación vuelva a ser tema de debate de la opinión pública; en ese sentido, el movimiento ha logrado nuevamente estar en boca de las personas, y cómo lo señala la delegada de la UNEES, ha demostrado que sus manifestaciones son pacíficas, mientras los violentos son los miembros de la Fuerza Pública que las reprimen.

Adicionalmente, el Movimiento ha logrado unir a diferentes sectores de la educación y de la economía, como profesores de bachillerato y trabajadores, al punto de organizar marchas conjuntas y llegar a acuerdos de movilización. Muestra de ello, es la articulación del paro nacional programado para el 15 de diciembre.

Aunque Ávila lamenta que los estudiantes aún no han alcanzado su principal propósito del movimiento, sortear la crisis financiera de la educación, considera una victoria el hecho de que el Gobierno se haya sentado a escuchar sus propuestas; y asegura que se están dando pasos en sentido de cumplir con su meta.

### **Acuerdo de Gobierno con rectores no es una victoria para el movimiento**

A pesar de que muchos han calificado como victoria el Acuerdo concertado entre Rectores y Gobierno, Ávila señaló que hay dos razones para pensar que no es así: Con el Acuerdo, el Gobierno intentó desvincular al sector educación separando a rectores, profesores y estudiantes; por otra parte, el dinero ofrecido por el presidente Duque "no es suficiente", en un momento en el que los estudiantes reclaman soluciones de largo plazo que dependan de la administración que está en curso.

En ese sentido, la integrante de la UNEES señala que tiene voluntad de continuar en la Mesa de Concertación, pero la suspensión de la misma se levantara en la medida en que vean garantías mínimas de negociación, y voluntad política para solucionar la crisis que enfrenta la educación superior. Mientras que eso ocurre, Ávila sostiene que no se levantarán los mecanismos de presión, como paros y asambleas permanentes que se desarrollan por parte de los estudiantes.

### **Garantías para la movilización, uno de los temas importantes** 

Frente a los mecanismos de coerción a la movilización, como los desalojos policiales o la suspensión y amenaza de cancelación de semestres académicos, Ávila afirma que es relevante que el Gobierno brinde garantías para la movilización estudiantil, y sostiene que aunque el movimiento no desescalará sus acciones, sí se requiere protección al derecho constitucional de la protesta y la movilización.

Este derecho se ejercerá nuevamente mañana, 15 de noviembre, en las principales capitales del país. En Bogotá, la movilización partirá de la Universidad Pedagógica Nacional cerca del medio día y llegará al puente de la Calle 100 con Autopista, adicionalmente, el 19 de noviembre se espera que lleguen "Los Hijos de la Manigüa", estudiantes que vienen caminando desde la Amazonía para exigir la negociación con el Gobierno.

Este jueves saldremos nuevamente a las calles para mostrar que tenemos fuerza, que no vamos a desfallecer, esperamos que el 19 lleguen unos compañeros que vienen caminando desde la Amazonía, y dado nuestro diálogo con diferentes sociales, acordamos un paro nacional para el 15 de diciembre.

 

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
