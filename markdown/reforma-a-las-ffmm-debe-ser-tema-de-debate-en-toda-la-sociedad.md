Title: "Reforma a las FFMM propuesto por FARC debería ser tema de debate en toda la sociedad"
Date: 2015-02-09 20:08
Author: CtgAdm
Category: DDHH, Paz, Política
Tags: doctrina militar, enemigo interno, FARC, reforma de las FFMM, seguridad
Slug: reforma-a-las-ffmm-debe-ser-tema-de-debate-en-toda-la-sociedad
Status: published

###### Foto: ElColombiano 

##### <iframe src="http://www.ivoox.com/player_ek_4058477_2_1.html?data=lZWimpmbe46ZmKiak5qJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmsdfc0trJt9XVjMnSjdfJqtDmzsaYz87QrdXV05DRx5DQpdSfp6a%2FpZDHs8rixM7Rx5DHs8%2BfjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>[ ] 

##### [Álvaro Villaraga,  Fundación Cultura Democrática en Colombia] 

Para el analista e investigador Álvaro Villarraga, las propuestas de las FARC que hacen referencia a la desmilitarización de la sociedad y a **reformar las Fuerzas Militares** y de Policía “son propuestas que coinciden con las de académicos, organizaciones no gubernamentales y sectores sociales, y que son pertinentes”.

El 7 de febrero del 2015, la delegación de paz de las FARC emitió un comunicado en el que expone 11 puntos para la “**provisión de garantías reales y materiales de no repetición”**, para el punto de Víctimas que se debatirá en los próximos meses en la Mesa de Diálogos de Paz de La Habana.

En los 11 puntos propuestos, las FARC hacen referencia a la necesidad de reformar la lógica militarista que existe en Colombia a través de la doctrina del enemigo interno, que permita reestructurar las fuerzas militares y desmontar el paramilitarismo, brindar garantías a la organización y la protesta social, y promover la memoria y los Derechos Humanos.

Esta propuesta  ha generado nuevamente un ambiente de debate en Colombia, entre los sectores que la rechazan argumentando que estas no hacen parte de los puntos de discusión del Acuerdo General; hasta aquellas que plantean que **en un proceso de paz no puede dejar de discutirse el papel de las fuerzas militares** en el conflicto armado interno.

Para Álvaro Villarraga, Presidente del **Fundación Cultura Democrática en Colombia**, es comprensible la posición de gobierno que busca limitar los temas que se debaten en la mesa de La Habana, asumiendo de manera estricta la no re-estructuración de la fuerza pública; sin embargo, en términos de la opinión, tampoco es incoherente o inconveniente que las FARC se pronuncien frente a temas políticos que son de su interés. Más aún, **es sano que la llamada “sociedad civil” dé el debate mas allá de la mesa de negociación**. “Este debe ser un tema de la agenda de debate de los colombianos y las colombianas a todo nivel”, y si bien, existe una limitación en los temas que se debaten en La Habana, “este tema no tiene ningún tipo de restricción para debatirse en la agenda legislativa”.

Según lo expuesto por el analista, son muchos los temas que conciernen al fin del conflicto que no se incluyen en los puntos de debate entre FARC y gobierno, como los **planes de desarrollo, el modelo económico, el ambientalismo, los macro-proyectos, las políticas sociales**, y entre ellos, el debate sobre el papel de la fuerza publica en un proceso de paz.

La reestructuración de las **FFMM es “una deuda de la Constitución del 91, que es una constitución garantista en Derechos Humanos** y que choca con la vieja doctrina de la seguridad nacional que aún persiste en el estamento militar: no es esta una critica al estamento militar sino una crítica al propio Estado y al propio desarrollo de política publica” señala Villarraga.

En este sentido, argumenta Villarraga, las propuestas de las FARC no son novedosas, varios expertos en la materia, como Adolfo León de Hortúa, Francisco Leal Buitrago y Alejo Vargas han publicado investigaciones en las que encuentran fundamento y sustentación para adelantar este tipo de reformas al interior de las fuerzas militares; así como el trabajo adelantado por organizaciones de objetores de conciencia ante la Corte Constitucional. “Las FARC coinciden con punto de vista democrático, que está afirmando un **Estado Social de Derecho**”, dice el analista.

Finalmente, enfatiza en el valioso pero frágil momento que vive el país gracias a los diálogos de paz y cese al fuego unilateral. La coyuntura obliga al gobierno a actuar en consecuencia con los anuncios de las FARC, y a no realizar operativos ofensivos en contra de las tropas estacionarias o campamentos de la insurgencia en lo que ha llamado “iniciar el des-escalamiento del conflicto”.

Más valiosa aún resulta -para el analista- la respuesta de la sociedad civil organizada en el **Frente Amplio por la Paz** que a pesar de no recibir consentimiento por parte del gobierno para realizar una veeduría del cese al fuego unilateral, ha realizado acciones de hecho constructivas, prestando sus buenos oficios para “aclimatar la posibilidad de esa tregua bilateral”.
