Title: Sidebar
Date: 2018-03-21 10:06
Author: AdminContagio
Slug: sidebar
Status: published

[  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/pieza-paz.png){width="370" height="490" sizes="(max-width: 370px) 100vw, 370px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/pieza-paz.png 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/pieza-paz-227x300.png 227w"}](/?s=paz+a+la+calle)

#### Síguenos

[  
Facebook  
](https://www.facebook.com/contagioradio)  
[  
Twitter  
](https://twitter.com/contagioradio1)  
[  
Youtube  
](https://www.youtube.com/user/ContagioRadio)  
[  
Instagram  
](https://www.instagram.com/contagioradio/)

#### Recomendado

###### [Festival centro 10 años](https://archivo.contagioradio.com/festival-centro-10-anos/)

[<time datetime="2019-02-27T20:30:13+00:00" title="2019-02-27T20:30:13+00:00">febrero 27, 2019</time>](https://archivo.contagioradio.com/2019/02/27/)

###### [¿Adversarios o enemigos?](https://archivo.contagioradio.com/adversarios-o-enemigos/)

[<time datetime="2019-02-22T06:00:16+00:00" title="2019-02-22T06:00:16+00:00">febrero 22, 2019</time>](https://archivo.contagioradio.com/2019/02/22/)

###### [56 niños palestinos fueron asesinados en 2018 por el ejército Israelí](https://archivo.contagioradio.com/ninos-palestinos-asesinados-israel-2018/)

[<time datetime="2019-02-21T14:29:29+00:00" title="2019-02-21T14:29:29+00:00">febrero 21, 2019</time>](https://archivo.contagioradio.com/2019/02/21/)

###### [“Duque, atienda primero la crisis humanitaria en la Guajira”: mujeres Wayuú](https://archivo.contagioradio.com/guajira/)

[<time datetime="2019-02-21T12:57:20+00:00" title="2019-02-21T12:57:20+00:00">febrero 21, 2019</time>](https://archivo.contagioradio.com/2019/02/21/)

###### [Comunidades afectadas por Hidroituango siguen sin respuesta del Estado](https://archivo.contagioradio.com/comunidades-afectadas-hidroituango-siguen-sin-respuesta-del-estado/)

[<time datetime="2019-02-20T15:45:52+00:00" title="2019-02-20T15:45:52+00:00">febrero 20, 2019</time>](https://archivo.contagioradio.com/2019/02/20/)

#### Publicaciones

RecientePopular

<style>.elementor-1509 .elementor-element.elementor-element-0769cdc > .elementor-container{max-width:800px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post-thumbnail.post-thumbnail-simple.post-thumbnail-simple{max-width:36%;flex:0 0 36%;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing{margin:0px -10px 0px -10px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured{margin:10px 10px 10px 10px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__posts{margin:0px 10px 0px 10px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__title{padding:0px 0px 0px 0px;margin:0px 0px 0px 0px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__filter > .jet-smart-listing__filter-item > a{margin:0px 0px 0px 10px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__filter-more > i{margin:0px 0px 0px 10px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__meta{text-align:left;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__more .jet-smart-listing__more-text{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__more:hover .jet-smart-listing__more-text{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__more-wrap{justify-content:flex-start;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__terms-link{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__terms-link:hover{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__meta{text-align:left;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__more .jet-smart-listing__more-text{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__more:hover .jet-smart-listing__more-text{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__more-wrap{justify-content:flex-start;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__terms-link{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__terms-link:hover{text-decoration:none;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-title-fields__item-label{margin-right:5px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-content-fields__item-label{margin-right:5px;}.elementor-1509 .elementor-element.elementor-element-59a8b78 .jet-processing{opacity:0.5;}.elementor-1509 .elementor-element.elementor-element-59a8b78 > .tippy-popper .tippy-tooltip .tippy-content{text-align:center;}</style>
<div class="section" data-id="0769cdc" data-element_type="section">

[![Festival centro 10 años](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-110x78.jpeg "Festival centro 10 años"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-110x78.jpeg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-370x260.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-275x195.jpeg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-550x385.jpeg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-216x152.jpeg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-240x170.jpeg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-340x240.jpeg 340w"}](https://archivo.contagioradio.com/festival-centro-10-anos/)  
###### [Festival centro 10 años](https://archivo.contagioradio.com/festival-centro-10-anos/)

[<time datetime="2019-02-27T20:30:13+00:00" title="2019-02-27T20:30:13+00:00">febrero 27, 2019</time>](https://archivo.contagioradio.com/2019/02/27/)  
[![¿Adversarios o enemigos?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-110x78.jpg "¿Adversarios o enemigos?"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-340x240.jpg 340w"}](https://archivo.contagioradio.com/adversarios-o-enemigos/)  

###### [¿Adversarios o enemigos?](https://archivo.contagioradio.com/adversarios-o-enemigos/)

[<time datetime="2019-02-22T06:00:16+00:00" title="2019-02-22T06:00:16+00:00">febrero 22, 2019</time>](https://archivo.contagioradio.com/2019/02/22/)  
[![56 niños palestinos fueron asesinados en 2018 por el ejército Israelí](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-110x78.jpg "56 niños palestinos fueron asesinados en 2018 por el ejército Israelí"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-340x240.jpg 340w"}](https://archivo.contagioradio.com/ninos-palestinos-asesinados-israel-2018/)  

###### [56 niños palestinos fueron asesinados en 2018 por el ejército Israelí](https://archivo.contagioradio.com/ninos-palestinos-asesinados-israel-2018/)

[<time datetime="2019-02-21T14:29:29+00:00" title="2019-02-21T14:29:29+00:00">febrero 21, 2019</time>](https://archivo.contagioradio.com/2019/02/21/)  
[![“Duque, atienda primero la crisis humanitaria en la Guajira”: mujeres Wayuú](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-110x78.jpg "“Duque, atienda primero la crisis humanitaria en la Guajira”: mujeres Wayuú"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-340x240.jpg 340w"}](https://archivo.contagioradio.com/guajira/)  

###### [“Duque, atienda primero la crisis humanitaria en la Guajira”: mujeres Wayuú](https://archivo.contagioradio.com/guajira/)

<p>
[<time datetime="2019-02-21T12:57:20+00:00" title="2019-02-21T12:57:20+00:00">febrero 21, 2019</time>](https://archivo.contagioradio.com/2019/02/21/)  

</div>

<style>.elementor-1529 .elementor-element.elementor-element-0769cdc > .elementor-container{max-width:800px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post-thumbnail.post-thumbnail-simple.post-thumbnail-simple{max-width:36%;flex:0 0 36%;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing{margin:0px -10px 0px -10px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured{margin:10px 10px 10px 10px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__posts{margin:0px 10px 0px 10px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__title{padding:0px 0px 0px 0px;margin:0px 0px 0px 0px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__filter > .jet-smart-listing__filter-item > a{margin:0px 0px 0px 10px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__filter-more > i{margin:0px 0px 0px 10px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__meta{text-align:left;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__more .jet-smart-listing__more-text{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__more:hover .jet-smart-listing__more-text{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__more-wrap{justify-content:flex-start;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__terms-link{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__featured .jet-smart-listing__terms-link:hover{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__meta{text-align:left;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__more .jet-smart-listing__more-text{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__more:hover .jet-smart-listing__more-text{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__more-wrap{justify-content:flex-start;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__terms-link{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-smart-listing__post .jet-smart-listing__terms-link:hover{text-decoration:none;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-title-fields__item-label{margin-right:5px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-content-fields__item-label{margin-right:5px;}.elementor-1529 .elementor-element.elementor-element-59a8b78 .jet-processing{opacity:0.5;}.elementor-1529 .elementor-element.elementor-element-59a8b78 > .tippy-popper .tippy-tooltip .tippy-content{text-align:center;}</style>
<div class="section" data-id="0769cdc" data-element_type="section">

[![Festival centro 10 años](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-110x78.jpeg "Festival centro 10 años"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-110x78.jpeg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-370x260.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-275x195.jpeg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-550x385.jpeg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-216x152.jpeg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-240x170.jpeg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-340x240.jpeg 340w"}](https://archivo.contagioradio.com/festival-centro-10-anos/)  
###### [Festival centro 10 años](https://archivo.contagioradio.com/festival-centro-10-anos/)

[<time datetime="2019-02-27T20:30:13+00:00" title="2019-02-27T20:30:13+00:00">febrero 27, 2019</time>](https://archivo.contagioradio.com/2019/02/27/)  
[![¿Adversarios o enemigos?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-110x78.jpg "¿Adversarios o enemigos?"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400-340x240.jpg 340w"}](https://archivo.contagioradio.com/adversarios-o-enemigos/)  

###### [¿Adversarios o enemigos?](https://archivo.contagioradio.com/adversarios-o-enemigos/)

[<time datetime="2019-02-22T06:00:16+00:00" title="2019-02-22T06:00:16+00:00">febrero 22, 2019</time>](https://archivo.contagioradio.com/2019/02/22/)  
[![56 niños palestinos fueron asesinados en 2018 por el ejército Israelí](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-110x78.jpg "56 niños palestinos fueron asesinados en 2018 por el ejército Israelí"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/1543311552_636680_1543312294_noticia_normal_recorte1-770x400-340x240.jpg 340w"}](https://archivo.contagioradio.com/ninos-palestinos-asesinados-israel-2018/)  

###### [56 niños palestinos fueron asesinados en 2018 por el ejército Israelí](https://archivo.contagioradio.com/ninos-palestinos-asesinados-israel-2018/)

[<time datetime="2019-02-21T14:29:29+00:00" title="2019-02-21T14:29:29+00:00">febrero 21, 2019</time>](https://archivo.contagioradio.com/2019/02/21/)  
[![“Duque, atienda primero la crisis humanitaria en la Guajira”: mujeres Wayuú](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-110x78.jpg "“Duque, atienda primero la crisis humanitaria en la Guajira”: mujeres Wayuú"){width="110" height="78" sizes="(max-width: 110px) 100vw, 110px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-275x195.jpg 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-216x152.jpg 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-240x170.jpg 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira-340x240.jpg 340w"}](https://archivo.contagioradio.com/guajira/)  

###### [“Duque, atienda primero la crisis humanitaria en la Guajira”: mujeres Wayuú](https://archivo.contagioradio.com/guajira/)

<p>
[<time datetime="2019-02-21T12:57:20+00:00" title="2019-02-21T12:57:20+00:00">febrero 21, 2019</time>](https://archivo.contagioradio.com/2019/02/21/)  

</div>

#### Newsletter

Suscríbete para obtener noticias

<form method="POST" action="#">
<input type="email" name="email" placeholder="E-mail" data-instance-data="[]">  
[Suscribirme](#)  

</form>

