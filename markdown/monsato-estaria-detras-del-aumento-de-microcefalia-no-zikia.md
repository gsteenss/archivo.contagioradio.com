Title: Zika no sería la causa de la microcefalia sino productos químicos de Monsanto
Date: 2016-02-22 12:48
Category: El mundo, Nacional
Tags: Brasil, fumigaciones, Monsanto, Zika
Slug: monsato-estaria-detras-del-aumento-de-microcefalia-no-zikia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/fumigaciones-e1456162609717.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ejempla 

<iframe src="http://co.ivoox.com/es/player_ek_10528503_2_1.html?data=kpWilJ2ZdJShhpywj5aVaZS1lJmah5yncZOhhpywj5WRaZi3jpWah5yncaLpzsrb1tSPqMafxMbg0diPqMafzs7Q1NTHqcfVzc7OjdjJb8XZw8rfh6iXaaK4wpDOjdbZaaSnhqaxz87Hs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Medardo Ávila] 

###### [22 Feb 2016] 

De acuerdo con un informe realizado por la Red de Médicos de Pueblos Fumigados, un **herbicida producido por una subsidiaria japonesa de Monsanto** sería, probablemente, el causante del aumento de casos de bebés que han nacido con microcefalia en América Latina.

Medardo Ávila Vázquez, pediatra y miembro de la Red de Médicos de Pueblos Fumigados de Argentina, quien participó en la realización del “Informe sobre Dengue, Zika, microcefalia y fumigaciones  masivas con venenos químicos”, reseña que solo **el 4.2% de los bebés que nacieron con microcefalia en Brasil durante 2015, tienen madres que tuvieron el virus, en cambio el 98% vivían en zonas donde se fumiga intensamente** contra el mosquito.

El piriproxifeno sería el herbicida que estaría causando la microcefalia en los recién nacidos y no el virus del Zika como se ha venido asegurando. Según el pediatra Ávila, ese herbicida se está usando para bloquear el desarrollo de las larvas del mosquito que transmite el Zika. El médico señala que el agua que estarían tomando las personas tendría ese químico, que no se usa en países como Colombia, donde aún, no se han reportado casos de bebés que hayan nacido con la enfermedad.

"Nos llama la atención cómo en Brasil, en los últimos años, en vez de garantizar que la gente tenga acceso al agua potable, el gobierno coloca directamente el producto en los recipientes que tiene la gente para cocinar en las favelas", dice el pediatra, quien añade que se ha comprobado que **la aplicación de ese larvicida químico produce malformaciones en los mosquitos.**

Por este tipo de conclusiones, en un estado de Brasil se prohibió la utilización de ese herbicida para combatir el virus del Zika. Medardo Ávila, **cree que este virus no se está tratando como un problema de salud pública sino como un problema de intereses económicos,** en el que las empresas que producen estos químicos estarían obteniendo numerosas ganancias generando un problema mucho pero al propio virus del Zika.

La Organización Mundial de la Salud, OMS, estudia la relación entre el Zika y las malformaciones en los fetos, por su parte desde la Red de Pueblos Fumigados se recomienda la suspensión del uso del piriproxifeno, y aconsejan a los países **mejorar el sistema de agua en los barrios pobres** donde abundan los mosquitos.

[Informe Pueblos Fumigados Sobre ZIKA](https://es.scribd.com/doc/300065290/Informe-Pueblos-Fumigados-Sobre-ZIKA "View Informe Pueblos Fumigados Sobre ZIKA on Scribd")

<iframe id="doc_18164" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/300065290/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
