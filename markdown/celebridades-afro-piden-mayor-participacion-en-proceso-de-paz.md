Title: Celebridades Afro piden mayor participación en Proceso de Paz
Date: 2016-02-24 10:33
Category: Nacional, Paz
Tags: Chao racismo, Chocquibtown, comunidades afro
Slug: celebridades-afro-piden-mayor-participacion-en-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/chocquibtown_800x669.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Revista Diners 

##### [24, Feb 2016] 

La Organización "Chao Racismo" y 20 celebridades de la cultura, la política y el deporte como Chocquibtown, Zulia Mena, Freddy Rincón y Faustino Asprilla, escribieron una carta pública dirigida al presidente Juan Manuel Santos, en la que solicitan una mayor participación de las comunidades Afrodescendientes en el proceso de Paz.

A la fecha la misiva radicada el pasado 16 de febrero, no ha tenido respuesta a los solicitantes que esperan poder entrevistarse con el mandatario para conversar sobre los 6 puntos consignados en la carta, cuyo contenido compartimos a continuación.

**"Carta pública al Presidente Juan Manuel Santos**

Los abajo enunciados, en nuestra condición de Afrocolombianos, pero sobre todo de ciudadanos que hemos aportado desde nuestros diferentes campos al desarrollo, gloria, construcción y orgullo de esta Nación, nos dirigimos a usted con todo respeto y ALTA preocupación en los siguientes términos:

1\. La Paz debe ser el anhelo de todos los colombianos, porque de una u otra forma hemos sido víctimas de una guerra fratricida que ha logrado dolorosamente existir como el conflicto armado más largo del hemisferio occidental.

2\. Todos los que apoyamos esta misiva de una u otra forma hemos alentado la Paz en nuestro País al entregar nuestros triunfos a todos los colombianos; es decir que nuestros goles, canciones, logros, medallas, aportes, y desempeño no han discriminado a nadie; nuestros triunfos nunca han distinguido ideologías, razas, clase social, o diferencia alguna. Nuestros triunfos son de y para todos los colombianos.

3\. Esta guerra nos ha tocado a todos, pero en nuestros territorios de origen se ensañó; ya sea por cuenta de paramilitarismo, guerrilla y a veces el mismo estado, el mapa de la guerra se amplió miserablemente en donde vive o más bien sobrevive nuestra gente.

4\. Todas las causas de la guerra han sido de una u otra manera la Exclusión: La exclusión del pobre, del campesino, de la mujer, de lo rural, del trabajador, del indígena y por supuesto de nosotros los NEGROS o Afrocolombianos. Por lo tanto la añorada Paz solo será formal, si no logramos un País que pase de profesar la tolerancia a practicar la inclusión, que no es otra cosa distinta a incorporar activamente a todas las poblaciones, diversidades, territorios y etnias, SI y Solo Si se garantiza la DIGNIDAD; esta sería la única manera de una Paz Sostenible.

5\. Los muertos o los territorios afrocolombianos no valen más que ningún otro muerto o región, pero si nos preocupa que valgan menos, pues es lo que podemos deducir al advertir que hemos hecho parte de la guerra como víctimas y NO ESTAMOS SIENDO PARTE DEL DISEÑO DE LA PAZ COMO ACTORES.

6\. Los Afrocolombianos queremos y tenemos el derecho de hacer parte del diseño y consenso de La Paz, pero sobre todo y más importante, La Paz nos necesita y no es posible sin nosotros, si lo que se busca es una Paz real más allá de la formalidad de la firma de un acuerdo.

Por todo lo anteriormente escrito los abajo mencionados solicitamos a usted nuestro Presidente, nos reciba en palacio para que se nos permita hacer lo único que sabemos hacer, nos gusta hacer y se reconoce que hemos hecho: Aportar a la grandeza y paz de nuestra amada Colombia.

Por LA PAZ: Los Colombianos y Colombianas"
