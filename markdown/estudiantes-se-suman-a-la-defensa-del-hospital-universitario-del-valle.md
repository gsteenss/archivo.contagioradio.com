Title: Estudiantes se suman a la defensa del Hospital Universitario del Valle
Date: 2015-09-17 12:51
Category: Movilización, Nacional, Política
Tags: crisis de la salud, Deudas de EPS, EPS, Hospital Universitario del Valle, Ministerio de Salud, Ministro de Salud, Movilizaciones en el Valle
Slug: estudiantes-se-suman-a-la-defensa-del-hospital-universitario-del-valle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/eltiempo1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Eltiempo.com 

###### [17 sep 2015] 

160 estudiantes y un grupo de médicos **esperan reunirse el día de hoy con el Ministro de Salud, Alejandro Gaviria**, para que de respuesta frente a los más de 191.000 millones de pesos que adeudan las Empresas Promotoras de Salud al Hospital Universitario del Valle.

Al paro que se adelanta por la crisis del Hospital **se sumaron los alumnos de la Facultad de Salud de la Universidad del Valle** quienes también exigen al Gobierno Nacional que intervenga con las EPS para que cancelen lo que le adeudan, al centro médico.

La actual crisis viene siendo denunciada desde hace aproximadamente dos meses, cuando el centro hospitalario se declaró en alerta amarilla, ante lo cual **el Gobierno Nacional y el Ministerio de Salud, no dieron salida.**

La respuesta que han recibido médicos y pacientes por parte del Ministro de salud es que este año se han girado recursos por \$26.325 millones, sin embargo **la realidad del Hospital, sus trabajadores y pacientes es otra**, tanto así que distintos sectores como los estudiantes se han unido a estas movilizaciones.

Ver también:

[Siete departamentos del país se quedarían sin el único hospital de tercer nivel](https://archivo.contagioradio.com/7-departamentos-se-quedarian-sin-el-unico-hospital-de-3er-nivel/)

[EP´S deben al Hospital Universitario del Valle 192 mil millones de pesos](https://archivo.contagioradio.com/epss-deben-al-hospital-universitario-del-valle-192-mil-millones-de-pesos/)
