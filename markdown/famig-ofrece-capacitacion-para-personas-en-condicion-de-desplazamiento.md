Title: FAMIG ofrece capacitación para personas en condición de desplazamiento
Date: 2015-02-05 20:38
Author: CtgAdm
Category: yoreporto
Tags: Desplazamiento forzado, Fundación de Atención al Migrante
Slug: famig-ofrece-capacitacion-para-personas-en-condicion-de-desplazamiento
Status: published

##### Foto: FAMIG 

<iframe src="http://www.ivoox.com/player_ek_4044729_2_1.html?data=lZWhlpyWfY6ZmKialJaJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiqLBqqyY0cvWqcTZjMjO0sbHrdXVxM6SpZiJhpTijNXO1MaPtMbm1NTbw9iPqc%2Bf1M7h18bHrYa3lIqvldOPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Hermana María Isabel Montenegro] 

La Fundación de Atención al Migrante, tiene como objetivo  desarrollar acciones al servicio de las **personas migrantes o en situación de desplazamiento por cuenta del conflicto armado.**

La fundación, ofrece una ayuda integral ya que cuenta con **tres centros para poder dar acogida, atención humanitaria de emergencia y acompañamiento psicosocial,** con el fin de mitigar las necesidades más inmediatas del grupo familiar que acude en busca de orientación para su ubicación y manutención en la ciudad.

Para tal fin, cuenta con tres centros de atención en Bogotá: el Centro de Acogida de la Terminal de Transporte Terrestre, donde se da la ayuda inmediata; el Centro de Atención al Migrante, como refugio u hogar de paso; y Centro Pastoral y de Capacitación, donde se capacita a las personas para que puedan empezar de nuevo su vida laboral.

Este último, **ofrece capacitación en temas sobre belleza, corte y patronaje, especialización en Uñas, manicure y pedicure, masajes, panadería, pastelería y sistemas.** Las inscripciones están abiertas para las personas en condición de desplazamiento para que se matriculen.

Así mismo, FAMIG invita a las personas interesadas en colaborar con esta causa a visitar las instalaciones de la fundación para que conozcan la labor de acompañamiento que se le hace a las familias víctimas del desplazamiento forzado.

**¿Cómo se puede contribuir?**

-   Apadrina un desplazado
-   Don Amor
-   Donaciones en especie en buen estado calle 17 N°68-75  (CAMIG)
-   Donaciones en efectivo cuenta de ahorros del Banco de Bogotá N°018-30999-7

Cualquier tipo de ayuda para la fundación como ropa, mercados, dinero, trabajo de voluntariado, etc. Es muy importante para que FAMIG pueda seguir adelante en su trabajo.

**Requisitos para los cursos:**

-   Copia de la cédula
-   Copia de la constancia de desplazamiento
-   Copia del carnet de salud o comprobador de derechos
-   Dos fotos 3X4 para carnet
-   Valor de la inscripción \$10.000
-   Disponibilidad de dos días a la semana de 7:30 A.M a 5:00 P.M
-   En el horario de 9:00 A.M a 1:00 P.M O 2:00 P.M a 4:00 P.

**Más información:**

-   Dirección CEPCA:  Cra 73C bis N°38B-16 sur,
-   Kennedy
-   Teléfono: 403 30 37
-   Correo: cepca.famig@gail.com

