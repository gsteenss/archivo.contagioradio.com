Title: 450 familias serían desalojadas de Plaza de Mercado de Tuluá
Date: 2018-02-22 13:22
Category: DDHH, Nacional
Tags: Familias, plaza de mercado, Tuluá
Slug: 450-trabajadores-serian-desalojadas-de-plaza-de-mercado-de-tulua-para-construir-edificio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Jorge-Jimenez-e1519321552474.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [22 Feb 2018] 

450 familias trabajadoras de la Plaza de mercado Galerías en Tuluá, están denunciando que la Alcaldía tiene intenciones de desalojarlos de este lugar para demolerlo y construir un edificio de 4 pisos que sería vendido a empresarios con marcas reconocidas. El sindicato de la "galería" denunció que en medio del proceso de defensa del espacio han recibido amenazas por parte de paramilitares que los señalan de ser un **“estorbo” para el avance de municipio**.

Según la abogada Claudia Moscoso, en la amenaza que le hicieron llegar a través de un panfleto, le informan que ya tienen ubicada a su mamá, que pagaría con su vida y la de sus familiares el “oponerse al progreso”. El documento fue firmado por un grupo auto identificado como los “Taitas”, sin embargo, Claudia expresó que pese a que la Fiscalía y la Personería Municipal ya tienen conocimiento sobre estos hechos, **no se han tomado medidas de protección para ella y los trabajadores de la Plaza**.

### **La plaza de mercado sostiene a 4.000 personas** 

Los trabajadores señalaron que hasta el momento tampoco han recibido ninguna información sobre la posible reubicación de sus puestos de venta, pero si la notificación de que deben desalojar en 31 días. Moscoso aseguró que de cerrarse este lugar **se acabaría con el sustento de más de 4.000 personas**, entre niños, adolescentes y ansíanos que dependen de la actividad económica de este lugar.

En ese sentido Sánchez afirmó que los intereses de la Alcaldía con este proyecto radican en beneficiar a los almacenes de cadena y empresas con marcas reconocidas, que podrían comprar locales en el edificio. Además, la ubicación de la plaza en el centro de Tuluá, permite que el metro cuadrado se **pueda vender en alrededor de 8 mil millones de pesos**.

“Lo que a nosotros nos atormenta en este momento es como nos amenazan y no nos dan la batalla jurídica y legal, **sino que nos intimidan para que salgamos corriendo y dejemos a las familias a merced de ellos**” aseguró Claudia. (Le puede interesar: ["Plaza de mercado en Popayán en peligro de desaparecer"](https://archivo.contagioradio.com/plazas-de-mercado-de-popayan-en-peligro-de-desaparicion/))

Esta **Plaza de mercado lleva funcionando más de 100 años en el municipio y está declarada como patrimonio cultural e histórico del departamento**, además hay locales que tienen más de 60 años funcionando y que han pasado entre las generaciones.

<iframe id="audio_23978083" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23978083_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
