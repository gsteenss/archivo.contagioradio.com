Title: JEP y comunidades hacen historia con primera audiencia en zona rural del Cacarica
Date: 2019-03-05 14:56
Author: CtgAdm
Category: Comunidad, Judicial
Tags: cacarica, Chocó, Festival de las Memorias, JEP
Slug: jep-comunidades-audiencia-cacarica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-05-at-1.01.31-PM-e1551816046921.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [5 Mar 2019] 

La zona humanitaria Nueva Esperanza en Cacarica, Chocó, fue el escenario para la primera audiencia, en zona rural, desarrollada por la Jurisdicción Especial para la Paz (JEP) en el marco del caso 004 sobre la situación territorial de la región de Urabá y Bajo Atrato, por violaciones al Derecho Internacional Humanitario (DIH) en hechos sucedidos entre el 1 de enero de 1986 y 1 de diciembre de 2016.

Víctimas de comunidades de Dabeiba, Curbaradó, Jiguamiandó, Pedegüita testimoniaron los múltiples **hechos victimizantes y violaciones de derechos humanos cometidos contra ellos y ellas desde 1995 hasta 2016**, así como los mecanismos de despojo para la implementación de un modelo económico que implica a diversos sectores empresariales.

La diligencia judicial, desarrollada por magistradas de esta justicia transicional y dos Comisionadas de la Verdad, correspondió a la solicitud de los afectados en profundización del informe: '**Van por nuestras tierras a sangre y fuego'**, entregado el 10 de diciembre pasado por las organizaciones Comisión de Justicia y Paz, Forjando Futuros, IPC y la Corporación Jurídica Libertad.

En sus declaraciones los campesinos, indígenas y afros solicitaron **esclarecimiento judicial ante la impunidad en que se encuentran los desplazamientos que implican desde hace 22 años a integrantes de las Fuerzas Militares** de las Brigadas 4, 11, 17, según informaron algunos de los abogados presentes en la audiencia.

Por cerca de tres horas las comisionadas Nadiezda Narazha Henriquez y María Pilar Valencia de la JEP escucharon atentamente a las víctimas que narraron los hechos de violencia que efectaron profundamente la vida de familias y comunidades. (Le puede interesar: ["Van por nuestras tierras: Informe entregado a la Jurisdicción Especial para la Paz sobre despojos en Urabá"](https://archivo.contagioradio.com/van-por-nuestras-tierras-informe/))

**"Que se haga justicia y se diga la verdad, este un paso para la construcción de una verdadera paz" expresó uno de los participantes luego de la diligencia judicial.**

[![Cacarica](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-05-at-1.01.31-PM-1-800x533.jpeg){.alignnone .size-medium .wp-image-62686 width="800" height="533"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-05-at-1.01.31-PM-1-e1551816078525.jpeg)

Las víctimas también presentaron a la JEP una propuesta de justicia restaurativa a través de la iniciativa de Universidad de Paz. En la misma, proponen que los responsables militares, y los exparamilitares y empresarios que se acojan a la JEP se vinculen a los programas de formación comunitaria en lo terrirorial, ambiental, memoria, justicia, democracia y participación.

Los afectados por el desplazamiento y despojo, que se perpertúa, esperan que los responsables no vayan a las cárceles sino que auténticamente digan la verdad y reparen a las víctimas de acuerdo a los criterios que ellos y ellas están definiendo. (Le puede interesar: ["Lanzan propuesta de Universidades de Paz"](https://archivo.contagioradio.com/lanzan-propuesta-de-universidades-de-paz/))

Un día antes en el Primer Festival de las memorias de los 22 años de las operaciones se inauguró en presencia de la **Comisionada de la Verdad, Lucía González** y delegaciones internacionales y de comunidades de la región y excombatientes una sede física de la Universidad de Paz en que esperan se realicen actos de reconocimiento de responsabilidad

La presencia de la Comisión de la Verdad en el **Festival de las Memorias,** y **la audiencia de la JEP es considerada como histórica** pues estas nuevas instituciones han hecho presencia en territorios remotos de Colombia en un tiempo récord.

Hacia las 2 de la tarde finalizó la audiencia que brinda elementos para el análisis y la investigación en el caso 004 y que servirá para posteriores diligencias con los comparecientes.(Le puede interesar: ["Cinco casos por los que la Jurisdicción pone la lupa a crímenes en Urabá y Bajo Atrato Chocoano"](https://archivo.contagioradio.com/cinco-casos-los-la-jep-pone-la-lupa-crimenes-uraba-atrato-chocoano/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
