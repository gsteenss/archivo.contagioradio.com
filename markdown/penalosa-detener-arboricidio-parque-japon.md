Title: Ordenan a Peñalosa detener tala de árboles en Parque Japón
Date: 2019-01-24 13:03
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Enrique Peñalosa, tala de árboles
Slug: penalosa-detener-arboricidio-parque-japon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/DxjRB2rWsAAZTxr-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CONCEJALALCELIO] 

###### [24 Ene 2019] 

La Procuraduría General de la Nación ordenó al alcalde de Bogotá Enrique Peñalosa,  detener la tala de árboles en el parque Japón, ubicado en la calle 87 con carrera 11, **dado que según el ente, no se cumplió con las especificaciones de manejo ambiental, cultural, histórico y paisajístico** al ordenar la tala de las especies arbóreas sobre las cuales ya se está realizando intervención.

En concepto de la Procuraduría, **no basta con que un funcionario de la autoridad ambiental visite el área a intervenir sino que es necesario que se tenga pleno conocimiento y aprobación de los informes técnicos,** que no solamente justifiquen la tala de los árboles, sino que concrete de manera directa el manejo que se da a las especies así como los planes “silvopastoriles” para la reemplazar dichos ejemplares.

De la misma manera el ente exigió a la Alcaldía y a la autoridad ambiental, los informes técnicos y los planes de manejo silvopastoriles que se darían a las especies de fauna presentes en la zona arborizada, y el manejo que se daría a la situación de las especies que no se pueden desplazar por sus propios medios.

La orden bajo el radicado  1-2019-1373, también **hace énfasis y exige un estudio técnico completo del impacto cultural, histórico y ambiental de este tipo de acciones** por lo que la Alcaldía, antes de continuar con la tala debe presentar y sustentar que esa actividad y el cambio en el uso del suelo tampoco tendrá un impacto nocivo en esos aspectos.

### **La movilización ciudadana ha sido crucial para frenar la tala de árboles** 

Esta misma situación presentando en cerca de **14 parques y zonas de arborización** en la capital, en donde la alcaldía pretende **talar entre 150 y 200 árboles,** lo que correspondería a un 25% del total de individuos presente en la ciudad que se estima en 1´200.000, según Julían Triana, líder ciudadano y los cálculos realizados por la Senadora Angélica Lozano que ha ubicado una cifra de 1 árbol por cada 8,7 habitantes.

\[caption id="attachment\_60474" align="aligncenter" width="397"\]![UTL Angélica Lozano](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/arboles-por-persona-por-localidades-397x479.png){.size-medium .wp-image-60474 width="397" height="479"} UTL Angélica Lozano\[/caption\]

A esta movilización en defensa del conjunto de árboles de la ciudad se han sumado los habitantes de la localidad de Chapinero y de Fontibón, quienes desde el pasado mes de Diciembre han tratado de evitar que se continúe con la tala al considerar que el impacto ambiental es altamente nocivo para la vida natural y de los habitantes de este sector.

Grupos de ambientalistas y ciudadanos están a la espera de que este tipo de acciones por parte de la Procuraduría se extiendan a todos los planes en 14 parques y zonas arborizadas de la ciudad de Bogotá.

[Aprovechamiento forestal de árboles aislados en Bogotá](https://www.scribd.com/document/398156342/Aprovechamiento-forestal-de-arboles-aislados-en-Bogota#from_embed "View Aprovechamiento forestal de árboles aislados en Bogotá on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_58016" class="scribd_iframe_embed" title="Aprovechamiento forestal de árboles aislados en Bogotá" src="https://es.scribd.com/embeds/398156342/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-pVuErLfRPrtlEwP06giW&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
