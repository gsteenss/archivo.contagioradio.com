Title: Atentan contra el senador Feliciano Valencia
Date: 2020-10-29 11:48
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: atentando, indigena, Lider social, senador
Slug: atentan-contra-la-vida-del-senador-feliciano-valencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/feliciano-valencia-734.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Por medio de un boletín de Derechos Humanos, **el Tejido Defensa de la Vida y los Derechos Humanos Çxhab Wala Kiwe (ACIN) , denuncian el atentado que sufrió en las últimas horas el senador Feliciano Valencia**.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1321834053612523520","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1321834053612523520

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

A través de su cuenta de Twitter sobre las 10:19 de la mañana, el senador Feliciano Valencia señaló que había sido víctima de **un atentado contra su vida, en la vía que conduce entre el corregimiento de El Palo y Tacueyó en el departamento del Cauca,** un hecho en el que además confirmo que salió ileso *"gracias a la madre naturaleza"*. ([«La Minga es una expresión de unidad en medio de la violencia»](https://archivo.contagioradio.com/la-minga-es-una-expresion-de-unidad-en-medio-de-la-violencia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el comunicado emitido por la ACIN , **el hecho ocurrió sobre las 9:40 a.m. de este jueves 29 de octubre mientras el senador Valencia se dirigía en su esquema de seguridad** por el sector entre El Pajarito y el Tierrero hacia el municipio de Tacueyó, en el Cauca, lugar en el que se iba a llevar a cabo la conmemoración de un año de la [masacre de La Luz.](https://www.cric-colombia.org/portal/cric-rechaza-la-masacre-perpetrada-por-grupos-armandos-a-la-autoridad-nej-wesx-de-tacueyo-y-a-la-guardia-indigena/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La información da cuenta que su vehículo sufrió tres impactos de arma de fuego, móvil que hace parte del esquema de seguridad del líder, y que en **este momento está siendo protegido por la Guardia indígena quien además realiza en este momento un cerco de seguridad en el lugar del atentado** para dar con los responsables de este hecho.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un año después persiste la violencia

<!-- /wp:heading -->

<!-- wp:paragraph -->

El atentado que sufrió el senador Valencia, se da en el marco de la conmemoración de un año de la **masacre de La Luz , hecho en dónde grupos armados le quitan la vida a 5 comuneros,** *"entre ellos a la Autoridad Ne’h Wesx Cristina Taquinas Bautista del resguardo de Tacueyó y 5 personas más resultaron heridas".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de rechazar el hecho la ACIN también señaló que, *"este intento de asesinato se dio en zona de influencia de la columna móvil de Dagoberto Ramos"*, y que además **días previos las autoridades tradicionales había manifestado su preocupación por el amedrentamiento en contra de la comunidad** que se movilizaría en memoria de los asesinatos por este mismo grupo armado el 29 de octubre del 2019.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/asociacionminga/status/1321842696173047808","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/asociacionminga/status/1321842696173047808

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

A su vez diferentes organizaciones de Derechos Humanos como la Asociación Minga señalaron que el atentado contra Feliciano Valencia se daba a pocos minutos de dar inicio de la Caravana Humanitaria del Cañón del Micay, una movilización que tiene como objetivo exigir la defensa de la vida y las garantías de la labor de líderes y lideresas en el departamento del Cauca y en todo el país.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Justiciaypazcol/status/1321840752486473728","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Justiciaypazcol/status/1321840752486473728

</div>

</figure>
<!-- /wp:core-embed/twitter -->
