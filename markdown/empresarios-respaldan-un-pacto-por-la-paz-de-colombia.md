Title: Empresarios respaldan un Pacto por la paz de Colombia
Date: 2016-10-10 11:04
Category: Nacional, Paz
Tags: Acuerdos de paz de la Habana, Empresarios por la paz, FARC, Juan Manuel Santos
Slug: empresarios-respaldan-un-pacto-por-la-paz-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/arturo-calle-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: larepublica] 

###### [10 Oct 2016]

Un grupo de 400 empresarios colombianos emitieron un comunicado de respaldo a los acuerdos de paz entre el gobierno y las FARC. Según los empresarios todo el pueblo colombiano está a favor de la paz e hicieron un llamado a todas las partes para que se logre un acuerdo definitivo que refleje el anhelo de paz. “**Vemos con gran esperanza la voluntad de paz que han demostrado las partes**” resaltan.

El comunicado de 5 puntos y que refleja la postura de importantes líderes de empresa, resalta también como **positivos los encuentros que ha producido el gobierno con representantes de los votantes por el SI y por el NO** en el plebiscito por la paz y afirman que se trata de “un acto de responsabilidad que debe ser exaltado y acompañado por toda la sociedad”

Los empresarios también afirman que este es un momento histórico y una oportunidad de terminar un largo y dilatado conflicto armado, por ello sería necesario que se materializado en un acuerdo de “forma expedita” y recuerdan que la **sociedad colombiana tiene la responsabilidad de hacer todos los esfuerzos en ese sentido**.

También hicieron un llamado a que los representantes de las diferentes corrientes se manifiesten dando prioridad al el interés nacional por encima de los intereses de partidos y pusieron a su disposición sus oficios para facilitar un acuerdo dentro de ese pacto nacional “Ofrecemos nuestro concurso para facilitar, promover y concretar estos necesarios acuerdos, que hoy son un clamor nacional.”

Este es el texto completo del comunicado...

**[Propuesta de un Pacto por La Paz de Colombia]**[  
  
*[Los abajo firmantes, empresarios colombianos, se permiten informar lo siguiente:]*  
  
*[1.   Nos hacemos parte del anhelo de paz que ha expresado el pueblo colombiano y vemos con gran esperanza la voluntad que han demostrado todas las partes por lograr un acuerdo con amplio apoyo que refleje la este anhelo.]*]

*2.   Reconocemos positivamente los encuentros que se han producido entre los representantes del Sí y del No, al igual que la voluntad expresada por las partes de adelantar un trabajo conjunto en la búsqueda de una Paz incluyente, estable y duradera. Se trata sin duda de un acto de responsabilidad que debe ser exaltado y acompañado por toda la sociedad.*

*3.   Entendemos que después de un doloroso y dilatado conflicto armado, el país ha llegado a un momento único en su historia el cual debe ser capitalizado por la sociedad materializando un acuerdo de paz en forma expedita. Es responsabilidad de esta generación hacer todos los esfuerzos en este sentido.*

*4. Hacemos un llamado a los representantes del Sí, a los representantes del No y a los representantes de las Farc para que impere la razón, el interés nacional, el trabajo responsable y comprometido, deponiendo intereses particulares, dedicando sus máximos esfuerzos a la búsqueda pronta y decidida de un acuerdo definitivo, incluyente y sostenible dentro del marco de un ‘Gran pacto nacional’ que conduzca a la unidad de la nación.*

*5. Ofrecemos nuestro concurso para facilitar, promover y concretar estos necesarios acuerdos, que hoy son un clamor nacional.*
