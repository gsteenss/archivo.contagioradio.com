Title: Lideresa Maritza Ramírez muere tras ser víctima de ataque en Tumaco
Date: 2019-01-25 12:58
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: amenazas a líderes sociales, asesinato de líderes sociales, Plan de Sustitución de Cultivos de Uso ilícito, Tumaco
Slug: lideresa-maritza-ramirez-pierde-vida-tras-ser-victima-ataque-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Maritza-Ramírez-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @SandinoVictoria: 

###### 25 Ene 2019 

[Maritza Chaverra Ramírez, presidenta de Junta de Acción Comunal de Aguas Claras, Nariño falleció este 24 de enero en el Hospital de Pasto después de ser víctima de un atentado el pasado 14 en el que recibió numerosos golpes a manos de desconocidos.  **Maritza, quien promovía el plan de sustitución de cultivos de uso ilícito en Tumaco , había recibido amenazas previamente.**]

Según el **vicepresidente de la Federación de Acción Comunal de Nariño, Leder Rodríguez,** Maritza fue encontrada aún con vida sobre la vía hacia el sector del Tigre y llevada  al centro hospitalario para que fuera atendida, aunque la Policía apunta a que fue un accidente,  Rodríguez inquiere que “cuando fue recogida, ella ya estaba sobre la vía y a la moto no le había pasado nada, no había indicios de accidente”.

[Hasta el momento las autoridades no han realizado la investigación ni visitado el lugar de los hechos porque basaron su versión en los testimonios de las personas que encontraron a Maritza en medio de la carretera, “**es información errada, nosotros tenemos un gran problema de seguridad ya que todos los líderes de la Junta estamos abocados a que nos amenacen y asesinen”** indica el líder social.   
]  
El vicepresidente explica que l**a mayoría de amedrantamientos en Tumaco, donde la cifra de integrantes de la Federación amenazados asciende a 42, son contra quienes apoyan el programa de sustitución de cultivos de uso ilícito**, proyecto en el que han existido problemas de manejo de dinero y de requisitos para aquellos quienes quieren ingresar, lo que sumado a la  presencia de grupos al margen de la ley recrudece la situación.[(Lea también Tumaco, municipio con mayor número de líderes asesinados desde 2016).](https://archivo.contagioradio.com/cauca-narino/)

[**"Esto es sistemático y no se da por accidente”** afirma Leder Rodríguez quien señala la ausencia del Estado en el municipio como otra de las problemáticas, “aquí no se hace ninguna inversión social, en Tumaco hay una tasa de desempleo del 90% , y no hay posibilidad de que la gente tenga proyectos que le permitan desarrollarse de una mejor manera y no acudan a los grupos delincuenciales”.]

[Desde la Junta de Acción Comunal se ha planteado tener algunos esquemas de seguridad sin embargo, Leder Rodríguez, también amenazado y atacado el pasado 3 de enero, señala que las medidas que ha tomado la **Unidad Nacional de Protección** los expone más en lugar de ampararlos, “imagínese una persona que no tiene ni siquiera para su sustento diario y anda con una camioneta de 60 u 80 millones de pesos y dos guardaespaldas, eso nos expone más y no nos da una solución”.]

<iframe id="audio_31791574" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31791574_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
