Title: Temporal y experimental sería el cese unilateral del ELN
Date: 2017-08-28 15:55
Category: Nacional, Paz
Tags: ELN, mesa de conversaciones Quito
Slug: temporal-y-experimental-seria-el-cese-unilateral-del-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/dialogo-eln-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [28 Ago 2017] 

A una semana de la llegada del Papa Francisco a Colombia, en Quito continúan los esfuerzos para que se declare un cese bilateral al fuego, sin embargo. Voceros del ELN han manifestado su interés por declarar un cese unilateral, si no se llega a un acuerdo antes de la visita del Papa, **que sería temporal y experimental acorde a la primera fase que se ha adelantado en el proceso**.

El gobierno por su parte, continúa bajo la premisa de necesitar más acciones humanitarias, situación que para de Currea, es esencial porque evidencia, de un lado la necesidad inmediata de **legitimar la mesa de diálogos y de enviar un mensaje de paz, y del otro lado estarían las tensiones sobre qué tipo de tregua debe pactarse**.

Para el analista político Víctor de Currea, se requiere un gran esfuerzo por parte de ambos lados, **que comprenda qué tipo de tregua le conviene a la paz y de esta forma avanzar en el proceso.** (Le puede interesar:["Reconciliación será el centro en la semana por la paz"](https://archivo.contagioradio.com/semana-por-la-paz/))

“Las partes están entendiendo la tregua no solamente como una pausa porque viene el Papa, sino como un espacio de fuerza para ver qué tipo de modelo de paz se impone, en ese sentido hay diferentes modelos de tregua, el **gobierno pareciera estar forzado en mostrarla como si fuera una tregua final y se hubiera acordado todo**” afirmó Currea.

De igual forma señaló que no puede aspirarse a generar un cese bilateral pasando las líneas rojas del otro y reiteró que el proceso de paz en Quito tiene un gran respaldo por parte de 11 embajadas, Naciones Unidos y la Cruz Roja, que permitiría en dado caso de que se el cese al fuego, activar un mecanismo de monitoreo. (Le puede interesar: ["ELN estudiaría cese unilateral si el gobierno no cede a un cese bilateral"](https://archivo.contagioradio.com/eln-estudiaria-cese-unilateral-si-gobierno-no-cede-a-un-cese-bilateral/))

### **La sociedad civil y el acompañamiento a la mesa en Quito** 

Hace una semana, organizaciones sociales y ciudadanos se ofrecieron como voluntarios para hacer parte del mecanismo de monitoreo al cese bilateral, frente a esta situación Víctor de Currea advierte que la mesa de Quito no solo puede quedar en manos de quienes conversan, sino que debe tener un respaldo mucho más agrande, y con capacidad de hacer propuestas en lo inmediato para solventen temas importantes que no solo afectan al **ELN, sino a la sociedad en general.**

De igual forma, manifestó que, con la llegada del Papa, no solo tiene que hablarse de reconciliación y perdón, sino hacer un ejercicio consciente de lo que ello significa para la paz del país. (Le puede interesar: ["Organizaciones se ofrecen como veedoras del cese bilateral entre ELN-Gobierno"](https://archivo.contagioradio.com/organizaciones-se-ofrecen-como-veedoras-del-cese-bilateral-entre-gobierno-y-eln/))

<iframe id="audio_20563547" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20563547_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
