Title: Comunidades y organizaciones de víctimas piden al Fiscal detener su destrucción mediática de la JEP
Date: 2018-10-05 21:11
Author: AdminContagio
Category: Entrevistas, Paz
Tags: Fiscalía, JEP, Nestor Humberto Martínez, ONU
Slug: fiscal-destruccion-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/COMUNIDADES-JEP-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [5 Oct 2018] 

**82 Comunidades y Asociaciones que han vivido el conflicto armado**, pidieron al fiscal Nestor Humberto Martínez **que cese su persecución contra la Jurisdicción Especial para la Paz (JEP)**, con la que está atentando frente a la institución que posibilitaría la reconciliación sobre la base de la verdad. (Le puede interesar: ["Accionar de la Fiscalía contra la JEP es inadmisible: Comisión Colombiana de Juristas"](https://archivo.contagioradio.com/accionar-de-la-fiscalia-contra-jep-es-inadmisible/))

En la comunicación, las diferentes asociaciones firmantes **rechazaron "la intervención ilegal, arbitraria, contra derecho de la sede y archivos de la Jurisdicción** por orden del Fiscal General de la Nación, en el día de ayer"; hecho que para ellos, es la continuación de una persecución contra el Tribunal de paz, que se ha dado en el marco de una operación mediática y judicial encabezada por Martínez.

Para las agremiaciones, **la JEP ha significado la posibilidad de conocer la verdad, participar en la construcción de memoria, y lograr un proceso restaurador de condiciones de vida digna;** e incluso desde el Congreso, la justicia transicional ha sido sometida a modificaciones sustanciales "por sectores que se oponen a la construcción de una paz en donde todos reconozcan sus responsabilidades".

Por estas razones, los firmantes del [documento](https://www.justiciaypazcolombia.com/apoyamos-a-la-jep-y-objetamos-actuacion-del-fiscal-general-de-la-nacion/) **solicitaron al presidente Iván Duque, que se pronunciara en favor de la JEP**, e igualmente, pidieron que comunidades y ciudadanos respalden a Patricia Linares, presidenta de la institución, y a todos los magistrados que la componen. (Le puede interesar: ["Plena disposición de la JEP para que se esclarezcan investigaciones desde la Fiscalía"](https://archivo.contagioradio.com/plena-disposicion-de-la-jep-para-que-se-esclarezcan-investigaciones-desde-la-fiscalia/))

Por otra parte, pidieron al Sistema Interamericano de Derechos Humanos y de Naciones Unidas, que monitoreen y recomienden al Estado colombiano que se garantice el debido proceso "y la independencia judicial ante esta injerencia indebida y contra el derecho del Fiscal General de la Nación".

### **ONU se sumó al llamado de las Comunidades** 

En una declaración conjunta de la Misión de Verificación de las Naciones Unidas en Colombia, y la Oficina del Alto Comisionado Para los Derechos Humanos **también pidieron que se respetará la autonomía de la JEP**; mientras distintas organizaciones se mostraron de acuerdo con el pedido de las comunidades mediante un  [comunicado](https://es.scribd.com/document/390212801/Fiscal-Debe-Respetar-El-Estado-de-Derecho).

[![Captura de pantalla 2018-10-05 a la(s) 6.30.20 p.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/Captura-de-pantalla-2018-10-05-a-las-6.30.20-p.m.-469x613.png){.size-medium .wp-image-57379 .aligncenter width="469" height="613"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/Captura-de-pantalla-2018-10-05-a-las-6.30.20-p.m..png)

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
