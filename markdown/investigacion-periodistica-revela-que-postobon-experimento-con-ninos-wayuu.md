Title: Investigación periodística revela que Postobón experimentó con niños Wayúu
Date: 2018-02-14 12:51
Category: DDHH, Nacional
Tags: La Guajira, Liga Contra el Silencio, niños Wayuú, postobon
Slug: investigacion-periodistica-revela-que-postobon-experimento-con-ninos-wayuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/join-us-for-a-ride-4-e1518630619374.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Liga Contra el Silencio/Las 2 Orillas] 

###### [14 Feb 2018] 

En una investigación realizada por un periodista que hace parte de la Liga Contra el Silencio, se dio a conocer que entre los meses de julio y septiembre del 2017, la empresa Postobón, realizó un experimento con más de 3 mil niños y niñas de comunidades Wayúu en la Guajira **para evaluar los efectos de ciertos productos**. Informaron que esto se hizo sin claridad sobre los protocolos que exige el Ministerio de Salud y no han presentado los resultados del estudio.

La Liga Contra el Silencio, iniciativa de la Fundación para la Libertad de Prensa y una alianza de diferentes medios de comunicación, publicó esta investigación en el portal de información VICE donde se lee que Postobón “distribuyó de lunes a viernes un combo que incluye dos bebidas: una gasificada, con sabor a “mora azul” y otra que contiene avena y mango, empacada en una cajita de cartón en cuya presentación aparece un **oso sonriente y bonachón**, están identificados con la marca Kufu”.

Estos productos, que no fueron certificados como productos nutricionales ni naturales y que fueron distribuidos entre una **“población altamente vulnerable** como la de La Guajira”, para La Liga, “produce cuestionamientos éticos”. Afirmaron que “algunos niños involucrados en la campaña fueron sometidos a pruebas de laboratorio” y que “Postobón no solicitó la autorización necesaria” ante el Ministerio de Salud.

### **Postobón realizó pruebas de laboratorio a los niños** 

De acuerdo con Senai Alvarado, director de la Liga Contra el Silencio, esta historia, como muchas otras, **“están siendo silenciadas y son relevantes”**. El periodista que estuvo en La Guajira “visitó los colegios donde la empresa estuvo repartiendo estas bebidas que no han salido al mercado”.

Indicó que a un grupo de esos niños y niñas “se les realizó **pruebas de laboratorio** a las cuales todavía no se ha tenido acceso”. Enfatizó en que el Ministerio de Salud le exigió a la empresa “que le entregara un protocolo de investigación por lo que están haciendo pruebas con seres humanos y el Ministerio no ha recibido este protocolo”. (Le puede interesar:["Asesinan líderes que denuncian al ICBF y al Cerrejón"](https://archivo.contagioradio.com/amenazan-de-muerte-a-integrantes-de-nacion-wayuu/))

### **Niños y niñas son de las comunidades indígenas Wayúu** 

Alvarado confirmó que los niños que recibieron estas bebidas hacen parte de diferentes **comunidades Wayúu** y la publicación periodística establece que el experimento se desarrolló en la Institución Etnoeducativa Rural Laachón-Mayapo, “que tiene veintisiete sedes” y “la sede principal recibe a 1.200 estudiantes, 200 de los cuales están internados”.

Además, los “combos" también fueron distribuidos en los resguardos que acogen a 1.452 estudiantes al igual que en instituciones educativas como la “Divina Pastora, en Riohacha, y al **Internado Indígena de Nazareth**, en Uribia”. Esto lo definió Postobón como "una estrategia para contribuir a la nutrición de los niños” y “como un aporte a la prevención de la deserción escolar”.

### **Postobón manifestó que el trabajo se hizo con responsabilidad** 

En la publicación, manifestaron que la empresa de gaseosas emitió una nota institucional en donde indica que las bebidas distribuidas hacen parte de un **proyecto social** diseñado para “acompañar los planes alimenticios de los niños y niñas”. Se lee que Postobón le respondió a DeJusticia que “Kufu no es un suplemento nutricional, y mucho menos un medicamento". (Le puede interesar:["No solamente son niños, es todo el pueblo Wayúu el que está mueriendo"](https://archivo.contagioradio.com/no-solamente-son-ninos-es-todo-el-pueblo-wayuu-el-que-se-esta-muriendo/))

Además, la empresa le dijo a La Liga que “lo que hizo con los niños Wayúu fue un **“seguimiento técnico”** y que hoy analiza “la información producto del seguimiento”. En esa misma comunicación, Postobón enfatizó en que el trabajo desarrollado con el estudio se hizo de forma integral toda vez que realizaron “talleres y capacitaciones sobre estilos de vida activos”.

Finalmente, Alvarado indicó que la investigación se realizó teniendo en cuenta la denuncia que estableció el centro de investigación y de derechos humanos **DeJusticia** en el diario El Espectador. Con esto, La Liga Contra el Silencio retomó la investigación y en el terreno y lograron desarrollar la historia.

<iframe id="audio_23784897" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23784897_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
