Title: Son 1.615 miembros de la Fuerza Pública los que aún no se han reportado ante la JEP
Date: 2019-06-16 16:46
Author: CtgAdm
Category: Judicial, Paz
Tags: Ejecuciones Extrajudiciales, ejercito, JEP
Slug: son-1-615-miembros-de-la-fuerza-publica-los-que-aun-no-se-presentan-ante-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/JEP-Fuerza-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RedMas] 

Para finales del 2019, la **Jurisdicción Especial para la Paz (JEP)** debe ubicar a **1.615 miembros de la Fuerza Pública** involucrados en graves delitos cometidos durante el conflicto armado quienes se comprometieron a presentarse ante el mecanismo judicial al obtener beneficios ante la justicia ordinaria como libertad anticipada y condicional, como parte de lo establecido en el Acuerdo de Paz.

 Antes de que comenzara a operar la  JEP en marzo de 2018, los integrantes de la Fuerza Pública podían pedir su libertad o beneficios como trasladarse de una cárcel a una unidad militar, con la condición de firmar un acta de sometimiento para comprometerse con la verdad, reparación a las víctimas y la promesa de presentarse ante la Sala de Definición de Situaciones Jurídicas de la JEP .

### La Corte Penal Internacional puede incidir 

De no avanzar en este punto y no encontrar a las personas restantes,  la **Corte Penal Internacional (CPI)** puede  interceder en el proceso, si considera que las autoridades judiciales no han progresado en el juzgamiento de los responsables de crímenes de guerra y de lesa humanidad. Cabe resaltar que la CPI abrió desde el 2004 un expediente preliminar contra Colombia por falta de resultados judiciales en el marco del conflicto armado.

Al interior de ese expediente, existe un apartado especial con énfasis en las ejecuciones extrajudiciales cometidas por integrantes de la Fuerza Pública. Según la Sala de Definición de Situaciones Jurídicas de la JEP**, el 99% de integrantes de la Fuerza Pública que firmaron actas de sometimiento a la JEP están vinculados a este crimen o a desapariciones forzadas.** Por ahora, la JEP ha recibido 91 testimonios pertenecientes al caso 003: ‘Muertes ilegítimamente presentadas como bajas en combate por agentes del Estado’.

**La Sala de Definición de Situaciones Jurídicas de la JEP** espera encontrar una forma de encontrar a los militares y policías que no se han reportado, trabajando en conjunto con el Ministerio de Defensa mediante jornadas de convocatoria para explicarles sobre el régimen de condicionalidad del que deben hacer parte.

### ¿Qué responde el Ministerio de Defensa? 

Por su parte desde el Ministerio de Defensa se aseguró que militares y policías que deben presentarse para cumplir con su compromiso con la JEP comparecerán cuando se les requiera y que ninguno de ellos ha intentado evadir esta responsabilidad. [(Lea también: 12 casos de ejecuciones extrajudiciales en Casanare serán revisados por la JEP)](https://archivo.contagioradio.com/jep-revisara-12-casos-de-ejecuciones-extrajudiciales-en-casanare/)

La cartera explicó que al entrar en vigencia el Acuerdo, se recogió un listado con los miembros de la Fuerza Pública que podían tener algún beneficio y enviarlo a la JEP, sin embargo ahora su función se limita a conocer el listado de estas personas y su ubicación.

### La JEP se refiere a las garantías

El ente fue enfático en que debe garantizarse una reparación a las víctimas bajo **"los ejes de verdad, garantía de no repetición y justicia"**, pero que de igual forma se debe dar seguridad jurídica a los comparecientes que al cumplir con su compromiso de verdad y no repetición, puedan mantener vigentes sus beneficios. **Son un total de 2059 los militares y policías sometidos a la JEP.**

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
