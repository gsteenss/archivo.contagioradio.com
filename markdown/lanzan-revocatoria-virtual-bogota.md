Title: Votación virtual para revocar al Alcalde Enrique Peñalosa
Date: 2017-11-09 13:01
Category: Movilización, Nacional
Tags: Bogotá, bogotanos, Enrique Peñalosa, Revocatoria Enrique Peñalosa
Slug: lanzan-revocatoria-virtual-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/revocatoria-e1509130563662.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Silla Vacía] 

###### [09 Nov 2017] 

Organizaciones sociales que apoyan la revocatoria del alcalde de Bogotá Enrique Peñalosa, alistan una iniciativa para que los habitantes de la capital le exijan al Consejo Nacional Electoral que respete y **haga cumplir el derecho de la ciudadanía a decidir** sobre la continuidad de la administración de la ciudad. La votación electrónica será a partir del 12 y hasta el 19 de Noviembre.

De acuerdo con Diego Pinto, integrante y vocero del Comité Unidos Revocamos a Peñalosa, el objetivo de la iniciativa es que **las personas puedan votar de manera simbólica** “para exigirle al Consejo Nacional Electoral que deje de embolatar la revocatoria y que permita que sea citada a las urnas”. Afirmó que “los bogotanos estamos listos para ir a votar ya por la revocatoria de Enrique Peñalosa”.

Además, afirmó que la iniciativa se da en el marco de las movilizaciones que han venido realizando los bogotanos como muestra de su **desacuerdo con las políticas que ha desarrollado Enrique Peñalosa**. Dijo que será un primer ejercicio para que la ciudadanía pueda manifestar si está o no de acuerdo con la revocatoria del alcalde. Además esta medida se hace urgente por la demora del CNE en aprobar las cuentas del comité. (Le puede interesar: ["La revocatoria es en las calles": Comité Unidos Revocamos a Peñalosa"](https://archivo.contagioradio.com/resistraduria-avala-firmas-para-revocatoria-de-penalosa/))

### **Revocatoria aún está viva** 

A pesar de que el Consejo Nacional Electoral sigue sin pronunciarse frente a los documentos que presentó el comité de revocatoria sobre las cuentas de la campaña, Pinto manifestó que estas acciones deben ser un pretexto para que **la ciudadanía ratifique su intención de salir a votar.** Indicó que es urgente que el CNE dé el aval sobre las cuentas para que la Registraduría comience a hacer los trámites necesarios.

Además, dijo que los bogotanos han demostrado su inconformidad con la alcaldía de Peñalosa en la medida que **continuamente se registran bloqueos a Transmilenio** y “las encuestas de percepción reflejan un 70% de des favorabilidad ante la gestión del alcalde”. Por esto recordó que “la ciudadanía ha venido manifestándose en masa no solamente a través de los comités de revocatoria”. (Le puede interesar: ["Alcalde Peñalosa es irresponsable y no respeta la Corte Constitucional": Recicladores"](https://archivo.contagioradio.com/alcalde-penalosa-es-irresponsable-y-no-respeta-a-la-corte-constitucional-recicladores/))

### **Con iniciativa virtual se espera más de un millón de votos** 

Los organizadores de la iniciativa esperan conseguir más de un millón de votos que **ratifiquen la presión ante las autoridades electorales**. Pinto indicó que el asunto de fondo es el ejercicio democrático por lo que “si los ciudadanos vemos que hay un plan de desarrollo que va a afectar a la ciudad, estamos en el derecho de frenarlo”.

Finalmente, recordó que necesario que la ciudadanía detenga la ejecución de un plan de desarrollo que “va a afectar de manera irreversible a la ciudad”. Por esto, invitó a las personas a que hagan parte de la votación virtual “donde se debe llenar un formulario con el número de cédula para evitar que se haga más de un voto por persona”. Los bogotanos podrán responder la siguiente pregunta: **¿Está usted de acuerdo, si o no, con revocar el mandato de Enrique Peñalosa?** La votación se realizará a través de la página web [www.votemosya.com](http://www.votemosya.com)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
