Title: MOVICE capítulo Meta conmemora 10 años luchando contra la impunidad
Date: 2017-08-17 13:15
Category: DDHH, Nacional
Tags: Día de la Dignidad de las Víctimas de Crímenes de Estado, implementación de los acuerdos, víctimas de crímenes de Estado
Slug: movice-capitulo-meta-conmemora-10-anos-luchando-contra-la-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/movice-e1502993685776.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Movice] 

###### [17 Ago 2017] 

En Villavicencio, capital del Meta, se realizará el **Encuentro Departamental de Víctimas de crímenes de Estado**, en el que se conmemorarán los 10 años de lucha contra la impunidad del capítulo Meta del MOVICE y expondrán y escucharán el trabajo de las víctimas en la implementación y cumplimiento del acuerdo final con las FARC.

Vilma Gutiérrez, secretaria técnica del Movimiento de Víctimas de Crímenes de Estado, asegura que **“el encuentro busca mostrar la lucha contra la impunidad** a la vez que visibilizar a las organizaciones que trabajan por la memoria”. (Le puede interesar:["Las víctimas de Crímenes de Estado tenemos derecho a ser escuchadas"](https://archivo.contagioradio.com/las-victimas-de-crimenes-de-estado-tenemos-derecho-a-ser-escuchadas/))

Durante el día, se realizarán exposiciones artísticas y compartirán los **retos para la participación de las víctimas** en el mecanismo de la Comisión del Esclarecimiento de la Verdad de la Justicia Especial para la Paz y la Unidad de Búsqueda de Personas Desaparecidas.

Adicionalmente, Gutiérrez manifiesta que, como víctimas, esperan que quienes se vinculen como parte de estos comités sean personas  **"sensibles e idóneas que comprendan la importancia de trabajar por las víctimas"**. (Le puede interesar: ["Víctimas de Crímenes de Estado piden que se desclasifiquen archivos militares"](https://archivo.contagioradio.com/victimas-de-crimenes-de-estado-piden-que-se-desclasifiquen-archivos-militares/))

La Secretaria técnica indicó que van a escuchar cómo han venido participando las **víctimas en la implementación de los acuerdos,** “queremos ver cuáles son los criterios para la elección de los comisionados para la comisión de la verdad y la comisión de búsqueda de desaparecidos”.

El encuentro se realizará el **viernes 18 de agosto** en el parque “Flores a la Memoria” de Villavicencio a partir de las 9:00 am.

<iframe id="audio_20388658" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20388658_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
