Title: Luego de 25 años Corte IDH fallará sobre genocidio de la Unión Patriótica
Date: 2018-05-16 12:16
Category: DDHH, Paz
Tags: colombia, Comisión Interamericana de Derechos Humanos, Corte Interamericana de Derechos Humano, estado, Unión Patriótica
Slug: luego-25-anos-corte-idh-fallara-genocidio-la-union-patriotica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/union-patriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unión Patriótica] 

###### [16 May 2018] 

La esperanza para las víctimas del genocidio de la Unión Patriótica retorna luego de que la Corte Interamericana de Derechos Humanos aceptara fallar y analizar la responsabilidad del Estado durante el exterminio de este partido. **Este pasó podría significar finalmente conocer la verdad sobre los responsables intelectuales de los más de 6 mil asesinatos.**

Pavel Santodomingo, integrante de la organización Hijos e Hijas por Colombia, afirmó que este espacio se da luego de que existiera una inoperancia de la Justicia interna colombiana por encontrar la verdad. Situación que ha obligado a las víctimas a llevar el caso a instancias internacionales.

“**Son más de 6.528 casos documentados a lo largo de más de 25 años de investigaciones autónomas** y por parte de las víctimas” manifestó Santodomingo y agregó, que una de las exigencias que se ha realizado es la celeridad en los procesos. (Le puede interesar: ["Las amenazas paramilitares son reales y comprobables: Unión Patriótica"](https://archivo.contagioradio.com/las-amenazas-paramilitares-son-reales-y-comprobables-union-patriotica/))

Este caso fue aceptado en 1997 en la Comisión Interamericana de Derechos Humanos, y 21 años después es recibido por la Corte Interamericana de Derechos Humanos, con un conjunto de recomendaciones de fondo hechas por la Comisión al Estado Colombiano.

### **La Comisión para el esclarecimiento de la verdad una luz para las víctimas de la UP** 

De acuerdo con Pavel Santodomingo, confían en que el escenario de la Comisión para el esclarecimiento de la verdad, junto con la unidad de búsqueda de desaparecidos seas un **“instrumento de los Acuerdos de Paz”** que permita conocer qué sucedió durante el conflicto armado “como el primer camino hacia la justicia y la reparación integral”.

Frente a la justicia restaurativa que se aplicará dentro de la Jurisdicción Especial para la paz, Santodomingo aseguró que la reparación hacia las víctimas no debe ser solamente individual, sino que tiene que estar dirigida a una reparación de carácter y de derechos políticos. **Razón por la cual piden que se recuperen las curules que se perdieron tanto en Senado como en Cámara de la UP**.

“El daño que se le ha hecho a la Unión Patriótica y al país es, no solamente asesinar a sus militantes, sino sumirlos en una estigmatización que impide proyectos alternativos hoy puedan surgir, para hacer país, poder y gobierno” afirmó Santodomingo.

<iframe id="audio_26013405" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26013405_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
