Title: FF.MM debe responder por la desaparición forzada de Magistrado Andrade y Alfonso Jacquin
Date: 2018-10-04 17:21
Author: AdminContagio
Category: DDHH, Nacional
Tags: familiares del Palacio de Jjusticia, Fiscalía, Fuerza Militar, Palacio de Justicia
Slug: fiscalia-debe-abrir-investigacion-por-desaparicion-forzada-de-las-ff-mm-del-palacio-de-justicia
Status: published

###### [Foto:] 

###### [04 Oct 2018] 

Un vídeo encontrado por familiares de los desaparecidos del Palacio de Justicia, reveló las imágenes ineditas en las que se observa la salida, con vida, del magistrado Julio Cesar Andrade y  Alfonso Jacquin Gutiérrez, integrante del M-19, ambos custodiados por la Fuerza Militar en los hechos de la toma y retoma del Palacio. Con este material, **nuevas preguntas se abren en este caso sobre el accionar de los militares y la muerte tanto de Jacquin como de Andrade. **

Para Eduardo Carreño, abogado defensor de las víctimas del Palacio de Justicia, e**l vídeo logra establecer que Andrade y Jacquin "quedaron en manos de los militares",** y** **que por el protocolo de ese día, debieron haber sido trasladados a la Casa del Florero, donde eran dirigidos quienes salían del Palacio.

Además, para el abogado también se prueba la falta de iniciativa que siempre ha reflejado la Fiscalía en el esclarecimiento de los hechos tras la toma y retoma del Palacio de Justicia, al igual que la falta de voluntad para continuar con las investigaciones en contra de la alta cúpula militar ,que estuvo a cargo de la operación, y el entonces presidente Belisario Betancur.

### **Magistrado Andrade una víctima de desaparición forzada** 

El vídeo enseñó la salida del magistrado Julio Cesar Andrade acompañado de las Fuerzas Militares. **Las imágenes fueron reconocidas por Gabriel Andrade, hijo del magistrado, que lo identificó de inmediato**.

Asimismo, a este descubrimiento, se suma la búsqueda que deben continuar los familiares del magistrado, debido a que los restos óseos que les fueron entregados, desde hace más de 30 años, después de prácticarles una exhumación, confirmaron que pertenecían al empleado de la cafetería Héctor Jaime Beltrán. (Le puede interesar: ["Héctor Jaime Beltrán, desaparecido del  Palacio de Justicia, abrió sus alas para volar")](https://archivo.contagioradio.com/46816/)

Al confirmar Gabriel Andrade la imagen del magistrado manifestó: “Son dos segundos de felicidad por 33 de pavor, porque después de los dos segundos me imagino cada tortura, cada golpe, cada vejamen, cada insulto, cada humillación que pudo haber sufrido. Duele ver que mi papa salió en medio de militares y fue desaparecido.”

### **Jacquin salió vivo del Palacio** 

En las imágenes se observa a Alfonso Jacquin, segundo comandante a cargo de la operación llevada a cabo por el M-19, salir con vida del Palacio y ser orientado por militares. La identidad de Jacquin fue confirmada por Héctor Elías Pineda, **conocido como “Tico,” exconstituyente y exintegrante del M-19, quien realizó el reconocimiento del vídeo.**

Los reportes iniciales señalaron a Alfonso Jacquin como dado de baja en el cuarto piso durante los hechos del Palacio de Justicia, sin embargo, el vídeo comprobaría que el militante del M-19 fue víctima de desaparición forzada y que **las Fuerzas Militares habrían violado el derecho internacional humanitario para la retención de combatientes.**

Sus restos fueron encontrados luego de que se realizara la exhumación de la tumba de Libardo Duran, y se comprobara que en realidad la persona que allí reposaba era el integrante del M-19. Su cuerpo fue entregado a sus familiares en julio de este año. (Le puede interesar:["Luego de 32 años de búsqueda, serán entregados los restos de Alfonso Jacquin"](https://archivo.contagioradio.com/luego-de-32-anos-de-busqueda-seran-entregados-los-restos-de-alfonso-jacquin/))

### **Fiscalía debe re abrir las investigaciones por los desaparecidos del Palacio** 

Con esta nueva evidencia, que además sustentaría la desaparición forzada del magistrado Andrade, las víctimas del Palacio de Justicia le piden a la Fiscalía que re abra las investigaciones frente a los hechos ocurridos el 6 y 7 de noviembre de 1985 y esclarezca las responsabilidades detrás de lo ocurrido.

<iframe id="audio_29121539" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_29121539_4_1.html?c1=ff6600"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
