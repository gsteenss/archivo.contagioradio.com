Title: Población infantil, la más afectada por el cambio climático
Date: 2015-11-12 14:29
Category: Ambiente, Nacional
Tags: áfrica, Asia, cambio climatico, desnutrición y el paludismo, Fenómeno del Niño, Latinoamérica, niños se verán afectados de hambruna y mortalidad como la diarrea, Organización Mundial de la Salud, Unicef
Slug: poblacion-infantil-la-mas-afectada-por-el-cambio-climatico
Status: published

###### foto:carlosruizzamara.blogspot.com 

###### [12 nov 2015]

Según un estudio realizado por el Fondo de las Naciones Unidas para la Infancia, UNICEF, el cambio climático generará graves consecuencias en once millones de niños del planeta que se verán afectados por la hambruna ocasionando enfermedades como la diarrea, desnutrición y el paludismo, aumentando la mortandad de la población de niños y niñas del mundo.

El fenómeno meteorológico de El Niño implica cambios de temperatura de las aguas en la parte central y oriental del Pacifico Tropical, adicionalmente este fenómeno ha causado sequía e inundaciones en territorio **africano, asiático y latinoamericano.**

De acuerdo con datos de la Organización Mundial de la Salud (OMS),** "más del 88 por ciento de la carga actual de morbilidad atribuible al cambio climático se da en niños menores de cinco años”**, ya que El Niño aumenta el riesgo de sufrir enfermedades como la malaria, el dengue, la diarrea o el cólera, que resultan ser mortales para la población infantil.

Con el informe de la UNICEF se evidencia que los niños son los más vulnerables a los efectos del cambio climático, por lo desde la UNICEF se insta a los gobiernos a tomar medidas inmediatas.
