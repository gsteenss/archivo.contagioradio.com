Title: La lucha contra el fracking, la defensa de la vida
Date: 2019-06-21 09:44
Author: Censat
Category: CENSAT, Opinion
Tags: Cajamarca, fracking, Marcha carnaval, Tolima
Slug: lucha-contra-fracking-defensa-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/fdfdfdf.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Ecoportal 

El pasado 7 de junio se llevó a cabo la “XI Marcha Carnaval en Defensa del Agua, la Vida y el Territorio” y en contra del “fracking” y la minería contaminante. Se estima que participaron alrededor de 70 municipios con 300.000 personas a nivel nacional, de las cuales aproximadamente 130.000 se reunieron en Ibagué.

“La Marcha Carnaval” surgió en Ibagué por iniciativa del Comité Ambiental del Tolima, y este año,fue convocada también por el Movimiento Nacional Ambiental y la Alianza Colombia Libre de Fracking para defender y hacer respetar las consultas populares y los acuerdos municipales en la que decenas de municipios han decidido prohibir proyectos extractivos: mineros, petroleros, de “fracking” y de represas. Como complemento del trabajo pedagógico y político del Comité Ambiental del Tolima, Cosajuca y otras organizaciones tolimenses llevado a cabo por más de una década en torno a los impactos que genera la gran minería, este año la Marcha Carnaval también manifestó un rechazo rotundo al “fracking”.

El fracturamiento hidráulico en lutitas o “fracking”, es la expresión de un modelo sin freno dispuesto a sacrificar lo que sea para obtener más y más petróleo o gas. Esta lucha logra a su vez movilizar y articular, más allá de las convencionales de “no, si es en mi patio trasero”, porque posiciona un mensaje claro y contundente: “vienen por todo, a costa de lo que sea”. Es decir, es una lucha por la vida y el territorio contra la inexorable extinción que quieren imponernos. Es, por tanto, una resistencia que trasciende diferencias políticas y límites geográficos con la consigna “No al fracking ni aquí, ni allá, ni hoy, ni nunca”.

En Colombia, el cuestionamiento colectivo al fracking inició en 2013, cuando un grupo pequeño de organizaciones, entre ellas Censat Agua Viva, el Colectivo para la Defensa de la Provincia del Sugamuxi y Proyecto Gramalote del Meta, decidió convocar la “Primera Jornada Nacional frente al Fracking”, advirtiendo el riesgo que entrañaba la implementación de esta técnica en el país.

El debate nacional sobre esta técnica ha cobrado dimensiones sorprendentes. Un asunto clave ha sido el surgimiento de la Alianza Colombia Libre de Fracking en septiembre de 2016, al fragor de una gran movilización en San Martín, Cesar, durante la “Segunda Jornada Contra el Fracking”, convocada por una decena de organizaciones, además de las antes mencionadas, en la que se denunciaron las políticas y los programas que intentaban promover esta técnica lesiva para la vida y al tiempo se apoyaron las iniciativas de defensa del territorio que venía ganando fuerza en el Magdalena Medio, donde estaban proyectados los primeros proyectos piloto.

Aquí es importante destacar el papel que ha cumplido la resistencia de Cordatec, una organización de pobladores de San Martín, que en poco tiempo logró levantar una voz que resonó a nivel nacional y escalar el ámbito internacional. Es así como del 28 de agosto al 1 de septiembre de 2018, se realiza la “Jornada Internacional Territorios Frente al Fracking en América Latina”, con la presencia de expertos y expertas de América Latina y Estados Unidos.

Actualmente, la Alianza Colombia Libre de Fracking es una articulación que ha logrado congregar a alrededor de 90 organizaciones ambientalistas, sindicatos, académicos y procesos sociales en defensa del territorio, para frenar el “fracking” en Colombia, utilizando diferentes estrategias jurídicas, políticas, comunicativas y de movilización social.

Las acciones y movilizaciones que han tenido lugar los últimos años en el país, especialmente las del pasado 7 de junio, que contaron con el apoyo de actores, actrices y cantantes reconocidos a nivel nacional, por la defensa del agua, la vida y el territorio, pero también por los efectos que tienen sobre la crisis climática el uso de estas energías extremas, son un claro ejemplo de lo que el ecologista Samuel Sossa identifica en su libro, “Resistencia global contra el fracking. El despertar ciudadano ante las crisis climática y democrática”, como una batalla ciudadana, “una rebelión de ciudadanos que protegen su territorio, su agua, su modo de vida” , sin necesariamente contar con un pasado activista.

Las conquistas de la Alianza Colombia Libre de Fracking y cada una de las organizaciones que allí confluyen (la suspensión de la reglamentación por parte del Consejo de Estado y el amplio posicionamiento político) es un gran aliento para seguir en la lucha. Sin embargo, tenemos que estar atentos, reflexivos y activos, pues la arremetida por parte de sus promotores sigue latente y fuerte, por lo que la movilización y oposición en las calles tendrá que aumentar y persistir en el tiempo si no queremos que se inicien con los pilotos de “fracking”, se exploten yacimientos de gas en aguas ultra-profundas del Caribe colombiano, y se continúe ampliando la frontera extractiva.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
