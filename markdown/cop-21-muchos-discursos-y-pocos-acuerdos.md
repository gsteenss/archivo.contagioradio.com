Title: COP 21: muchos discursos y pocos acuerdos
Date: 2015-12-06 08:41
Category: Ambiente, Otra Mirada
Tags: Activismo, Ambietalistas, cambio climatico, COP 21, COP21, Ecologistas en Acción, medio ambiente, Tom Kucharz, Transgénicos, Transnacionales
Slug: cop-21-muchos-discursos-y-pocos-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Cop-21-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: pablosolon 

###### [4 Dic 2015] 

A medida que avanza la COP21 en la ciudad de Francia, las organizaciones sociales, los gobiernos de países del G77, activistas y miembros de la sociedad civil han manifestado su inconformismo con el desarrollo de la cumbre, que para muchos no ha sido el esperado, puesto que hasta el momento, no se ha llegado a ningún acuerdo que proponga mitigar los efectos del cambio climático, ni se han abordado temas pactados en cumbres anteriores.

Para Tom Kucharz, miembro de ecologistas en acción, hasta el momento "Han habido muchos discursos de jefes de Estado, pero en realidad, las negociaciones no avanzan y están desapareciendo algunos de los temas que se habían acordado en años anteriores", entre ellos, se planteaba que en el 2030 se debería alcanzar el máximo de emisiones de gases de efecto invernadero en la atmósfera. No obstante, a 5 días de iniciada la cumbre, este no ha sido un tema de la agenda.

Por otra parte, las naciones que pertenecen al grupo de territorios en vías de desarrollo (G77), entre los que se encuentran países insulares y los más pobres del mundo, no están a gusto con los negociadores de Arabia Saudí, India, China, Estados Unidos y la Unión Europea porque han aplazado cuestiones fundamentales tales como los daños y pérdidas que tienen los pueblos que están siendo afectados en mayor medida por la crisis climática.

En ese sentido, Kucharz expresó que empresas energéticas transnacionales y bancos están limpiando su imagen a costa de su participación en la cumbre y están ofreciendo falsas soluciones para reducir el impacto climático en el planeta: "ellos están interesados únicamente en vender más semillas transgénicas y paquetes tecnológicos; sobre todo, una nueva dependencia de insumos químicos que ofrecen estas corporaciones".

En contraste a esta situación, varios activistas y organizaciones están desarrollando eventos paralelos a la COP21. Una de las más destacadas es la segunda edición del Tribunal Internacional para los Derechos de la Naturaleza, en donde han habido intervenciones importantes de denuncias sobre políticas y comportamientos de las empresas que fomentan crímenes contra los Derechos Humanos y la naturaleza.

Sobre este tipo de iniciativas Kucharz dijo que han tenido un balance positivo, pero que sobre los participantes ha caído un permanente control policial sobre los manifestantes: "en muchas ocasiones hay detenciones preventivas y existe una violencia marcada por la policía que es denunciada por varias organizaciones de Derechos Civiles y por la misma propia coalición francesa del Clima 21".
