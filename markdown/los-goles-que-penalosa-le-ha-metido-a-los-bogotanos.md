Title: Los goles que Peñalosa le ha metido a los bogotanos
Date: 2016-01-21 19:00
Category: Nacional, Política
Tags: Canal Capital, Enrique Peñalosa, Gustavo Petro, Transmilenio
Slug: los-goles-que-penalosa-le-ha-metido-a-los-bogotanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Peñalosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  [massanstidning.] 

###### [21 Ene 2016. ]

[21 días han transcurrido desde que Enrique Peñalosa asumió las riendas de Bogotá con decisiones administrativas en materia ambiental, de movilidad y salud que para muchos sectores son bastante desacertadas y representan retrocesos significativos frente a las políticas sociales implementadas en la administración anterior.]

[**Movilidad**]

[Además de anunciar que la **licitación para la construcción del metro con tramos elevados, podría posponerse para finales de 2016 o principios de 2017**, Peñalosa aseguró que la tarifa en el pasaje de TransMilenio aumentará \$200 a partir de febrero de este año. Una decisión que afecta notablemente a la ciudadanía, obligada a hacer rendir su cada vez más reducido salario mínimo, y que beneficia a las **empresas privadas que por cada \$100 que entran al sistema apropian \$90**, frente a los \$5 que son destinados para los recaudadores y los restantes **\$5 que van a parar al patrimonio del distrito**.]

[Con este aumento en las tarifas del transporte público, **Bogotá ocupa el cuarto lugar dentro del ranking de las ciudades con transportes masivos más caros del continente**, después de Sao Paulo, Santiago de Chile y Lima, que pese a tener costos elevados en el transporte público, cuenta con salarios mínimos mucho más altos y un sistema de transporte completo.]

[**Televisión pública**]

[Pese a que a finales de 2015 **Canal Capital obtuvo un reconocimiento por lograr consolidarse como la mejor opción de televisión pública del país**, por contar con una parrilla de programación diversa e incluyente a favor de la paz y la defensa de los derechos humanos, Enrique Peñalosa no dudó en asegurar que **se oponía a que el canal “vuelva a ser un actor político**”, pues según él la programación se había dedicado a promover no los valores ciudadanos sino a una persona en específico, por lo que **decidió cambiar tanto el enfoque como los programas emitidos**, para “resaltar a los héroes de la sociedad, a esas personas que son modelo de esa vida nueva que queremos impulsar”**.**]

[**Ambiente**   ]

[En los planes de la administración de Gustavo Petro estaba **hacer de la reserva Van der Hammen, el bosque urbano más grande del mundo**, teniendo en cuenta que estos terrenos fueron declarados en 2011 por la Corporación Autónoma Regional de Cundinamarca, CAR, como Área de Reserva Forestal Productora Regional del Norte, lo que significa que tienen un fin forestal que debe conservarse y protegerse. Sin embargo, durante el discurso de posesión Peñalosa, precisó [[algunos comentarios](https://archivo.contagioradio.com/reserva-forestal-van-der-hammen-en-peligro-por-planes-de-penalosa/)] que dejan **en el limbo esta reserva forestal**, cuyos terrenos estarían **destinados a la urbanización y expansión de la ciudad**.]

[**Empresas públicas**]

[Apenas 3 días después de la posesión Enrique Peñalosa [[planteó una posible privatización de la ETB](https://archivo.contagioradio.com/a-un-precio-irrisorio-penalosa-estaria-pensado-vender-la-etb/)], a través de su venta en 1000 millones de dólares, intensión que posiblemente se dirigiría a otras empresas públicas del distrito, “las **empresas de telecomunicaciones europeas** que eran gigantescas, con gobiernos socialistas, **podrían comprar la empresa de teléfonos de Bogotá con la caja menor de un mes**", aseguró. ]

[**Respuesta a protesta social**]

[El caso de la señora Rubiela Chavarro quien a sus 50 años murió a las afueras de una estación de TransMilenio de un infarto provocado posiblemente por la negligencia de la eps Cruz Blanca a la que se encontraba afiliada, ilustra la forma en la que Peñalosa podría responder durante su gobierno frente a la indignación de la ciudadanía, teniendo en cuenta que además ha asegurado que "**la democracia no es bloquear vías**".  ]

**[Hospitales públicos ]**

[En las últimas horas desde la alcaldía se anunció un **recorte presupuestal del 40% en los gastos de funcionamiento de la red de hospitales del distrito**, al que en 2015 le fueron destinados \$2,2 billones de pesos, lo que implicaría que el recorte para este año podría ser de más de \$800.000 millones. ]
