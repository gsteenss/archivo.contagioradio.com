Title: El poliamor como apuesta política
Date: 2017-06-07 11:20
Category: Libertades Sonoras, Mujer
Tags: Amor, Amor Romántico, Libertades Sonoras, monogamia, poliamor, Poligamia
Slug: el-poliamor-como-apuesta-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/POLIAMOR-e1496849880208.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Prensa HN] 

###### [06 Jun. 2017]

¿Qué es el poliamor?¿es una apuesta política?¿has escuchado alguna vez el término anarquía relacional?¿qué piensas de las relaciones abiertas? [**Ángela Robles Laguna**, conocida en el mundo digital y por las personas cercanas como “Alias Angelita”, artista visual, **integrante de la Ruda Colectiva y activista feminista que ha reflexionado sobre el poliamor estuvo en \#LibertadesSonoras** hablándonos a propósito de este tema.]

"El poliamor puede ser leído como múltiples formas de relacionamiento que estaban cuestionando la monogamia como único modelo amoroso"recalcó Ángela.

Además salimos a las calles a preguntarle a las personas **qué piensan del poliamor y cómo lo han entendido.**

También contamos con la voz de **Marcela Salas, l**[**esbiana feminista, gorda, machorra y quien es parte de varios colectivos como el Colectivo Feminista Gordas Sin Chaqueta** para que nos contara sobre el amor libre y cómo esta iniciativa permite que se desmonte o no el amor romántico.]

<iframe id="audio_19116109" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19116109_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
