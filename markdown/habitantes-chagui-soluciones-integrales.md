Title: Disputa por Triángulo de Telembí en Nariño, impide el regreso de 1.179 familias a su hogar
Date: 2020-01-20 18:16
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Desplazamiento forzado, nariño, Tumaco
Slug: habitantes-chagui-soluciones-integrales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Defensoría del Pueblo

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el 8 de enero, los cerca de 4.000 habitantes de las 32 veredas que integran el **Consejo Comunitario Río Chagüí** en zona rural de Tumaco (Nariño), llegaron hasta la cabecera municipal, huyendo de los enfrentamientos entre grupos armados ilegales que se disputan el territorio, una realidad social que data de décadas atrás y que no ha brindado soluciones reales al 90% de la zona rural del municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según explica una de las líderes proveniente de las comunidades del río Chagüi, los combates en esta zona suceden entre **grupos disidentes de las Farc (parte de la columna Oliver Sinisterra), el ELN y el Clan del Golfo** (o autodenominadas Autodefensas Gaitanistas de Colombia -AGC), quienes se disputan el control del Triángulo de Telembí y por consiguiente, sus corredores de narcotráfico y minería ilegal. [(Lea también: Tumaco, municipio con mayor número de líderes asesinados desde 2016)](https://archivo.contagioradio.com/cauca-narino-2/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El último censo apunta a que son un total de **1.179 familias y un estimado de 3031 personas, incluidos 554 niños y 490 niñas, que han huído** de las dinámicas de violencia que se viven en el río Chagüi. Sin embargo, la líder afirma que **esta cifra ascendería a 4.000** personas, que aún desconocen la fecha en la que pueden volver a su hogar. [(Le puede interesar: En Tumaco opera la justicia de los grupos residuales en medio de la militarización)](https://archivo.contagioradio.com/en-tumaco-opera-la-justicia-de-los-grupos-residuales-en-medio-de-la-militarizacion/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque se anunció el envío de 60 toneladas de alimentos para la población, la líder afirma que hasta el momento "apenas hemos conseguido cosas para comer, hay niños enfermos, tienen diarrea y fiebre". A su vez resalta que los más de 1.000 niños y niñas que salieron de sus viviendas **no cuentan hasta el momento con garantías para estudiar**, como resultado de este desplazamiento. [(Lea también: El colegio de Tumaco que se cansó de la muerte: 21 asesinatos de estudiantes en 4 años)](https://archivo.contagioradio.com/el-colegio-de-tumaco-que-se-canso-de-la-muerte-21-asesinatos-de-estudiantes-en-4-anos/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "No sabemos cuándo podemos volver a nuestro territorio, nos dicen que esperemos mínimo 8 meses o 2 meses"

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Chag**üi en disputa: e**xtensas hectáreas de cultivos de uso ilícito y minerales

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El Triángulo de Telembí, territorio en disputa une a las localidades de Magüí Payán, Roberto Payán y Barbacoas por medio de diversos afluentes y vegetación donde existe una **elevada cifra de hectáreas de cultivos de uso ilícitos**, haciendo de este lugar un corredor estratégico para transportar cargas de droga. [(Le recomendamos leer: Tumaco: sin gobierno, sin agua y con mucha violencia)](https://archivo.contagioradio.com/tumaco-gobierno-agua-violencia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Según el Informe de Monitoreo de Territorio Afectados por Cultivos de Uso Ilícito**, para agosto de 2019, el Charco-cuenca alta del río Telembí, junto a otros territorios como el Naya en Valle del Cauca, San Pablo en Bolívar o Tarazá en Antioquia registraron una alta concentración de cultivos, superando las 10 hectáreas/ km2, resultando los núcleos donde existe una mayor densidad de siembra por kilómetro cuadrado en el país.  
  
Voces de las comunidades, e incluso de la alcaldesa, María Emilsen Angulo han sido enfáticas en que **la solución no radica en aumentar el pie de fuerza como se ha propuesto desde la Fuerza de Tarea Hércules**, sino en establecer un [plan de atención inmediata](https://twitter.com/DefensoriaCol/status/1219289378259947520) e integral que brinde soluciones a la ausencia de salud, educación, vivienda, servicios públicos y que por supuesto, permita el regreso con garantías de las comunidades a su territorio.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:html -->  
<iframe id="audio_46797492" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46797492_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
