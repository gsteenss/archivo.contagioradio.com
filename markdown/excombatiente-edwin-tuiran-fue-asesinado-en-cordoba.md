Title: Excombatiente Edwin Tuirán, fue asesinado en Córdoba
Date: 2020-06-09 20:49
Author: CtgAdm
Category: Actualidad, DDHH
Tags: AGC, asesinato de excombatientes, Córdoba
Slug: excombatiente-edwin-tuiran-fue-asesinado-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/asesinados_farc-e1522961371379.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Excombatiente FARC /Teleantioquia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 8 de junio en horas de la tarde fue asesinado** Edwin Tuirán, excombatiente de FARC** quien fue impactado con varios disparos de arma de fuego, cuando estuvo presente en medio de un presunto enfrentamiento armado entre un mismo grupo ilegal en el caserío de Bijao, municipio de Juan José, Córdoba, una región **controlada por las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) .**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la [Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/)y la versión de su familia, **Edwin Tuirán fue firmante del acuerdo de Paz de las FARC quien vivió en Medellín hasta** 2019 cuando decidió volver a su parcela en Bijao para venderla con el objetivo de reintegrarse junto a su familia que habita en el Bajo Atrato.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las AGC manifestaron a su familia que no podían ingresar por el cuerpo de Edwin hasta tener la autorización de su mando, El excombatiente quien vivía con su esposa y sus tres hijas se suma a una larga lista que hoy ya podría superar la cifra oficial de 200 firmantes de la paz asesinados tras la firma del Acuerdo. [(Asesinan a Jhon Montaño, hijo de excombatiente de FARC en Algeciras, Huila)](https://archivo.contagioradio.com/asesinan-a-jhon-montano-hijo-de-excombatiente-de-farc-en-algeciras-huila/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El asesinato ocurre tan solo días después que diputados del Parlamento Europeo pidieran al presidente Iván Duque que fortalezca la protección de las personas excombatientes y redoble los esfuerzos para proteger territorios como Chocó, Cauca, el Bajo Cauca, el sur de Córdoba y el Catatumbo donde el conflicto armado se ha intensificado. [(Lea también: Eurodiputados piden respuestas a gobierno Duque sobre política de DDHH)](https://archivo.contagioradio.com/eurodiputados-piden-respuestas-a-gobierno-duque-sobre-situacion-de-dd-hh/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hace exactamente un año era asesinado en el mismo departamento en Bocas de Conejo, vereda Nain, Tierralta, aguas arriba del embalse de Urra 1 a Jorge Enrique Sepúlveda, también excombatiente quien aguardaba que se le hiciera entrega de su proyecto productivo como parte de su proceso de reincorporación. [(Lea también: Asesinan a excombatiente de FARC, Jorge Enrique Sepúlveda en Córdoba)](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-farc-jorge-enrique-sepulveda-en-cordoba/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
