Title: La música reivindica los derechos de las mujeres
Date: 2017-04-23 11:00
Category: Libertades Sonoras, Mujer
Tags: Arte y feminismo, genero, mujer, Música
Slug: el-feminismo-en-la-musica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/DI1TAIDP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Feminista Music] 

###### [23 Abr. 2017]

**El arte ha sido una herramienta fundamental para la reivindicación de derechos.** La música es una de ellas, en la que al son de diversos géneros musicales se ha criticado el sistema político, hablado de sus luchas, criticar las injusticias. Las mujeres también han estado presentes en esa escena y por eso en Libertades Sonoras estuvimos acompañadas de varias mujeres que lo han hecho.

**Una de ellas es 5ta. con 5ta que es una fundación artística y social que trabaja con niñas, niños y jóvenes** que a través del rap, baile, graffiti decidieron apostarle a la transformación social del país en Norte de Santander. Algunos de los grupos que apoya y con los que se alía son OroNegro Crew{9}, Las Motilonas, Afrodans, Girasoles y Villa Urbana{10}.

Así mismo, **desde Ecuador estuvo presente Caye Cayejera, quien habló de todo su proceso en la música,** de la reivindicación de las luchas, no solo de las mujeres sino de todos los pueblos del Abya Yala.

<iframe id="audio_18274112" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18274112_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
