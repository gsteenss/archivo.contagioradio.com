Title: "Encuestados siguen queriendo paz sin que les cueste nada"
Date: 2015-07-03 18:26
Category: Nacional, Política
Tags: FARC, Gallup, Juan Manuel Santos, Proceso de conversaciones de paz, Resultados encuesta Gallup segundo semestre 2015
Slug: encuestados-siguen-queriendo-paz-sin-que-les-cueste-nada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/encuesta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Imágen: Gallup 

###### [3 Jul 2015] 

Esta semana fueron publicados los resultados de la encuesta Gallup para el Segundo trimestre de 2015. La encuesta contempla estados de ánimo de los encuestados, gestión de gobierno nacional y gobiernos locales, favorabilidad de personajes, aprobación de algunas propuestas, favorabilidad de instituciones y proceso de paz. Afortunadamente las encuestas evalúan estados de ánimo y ellos son cambiantes de acuerdo a la coyuntura especial, señala el politólogo Fernando Giraldo.

**Estados de ánimo positivos en temas publicitados positivamente**

Según Giraldo, parece que algunos de los temas, sobre todo los temas en los que el gobierno hace énfasis o tiene preocupación en atender y tienen espacio en los medios de información son los que tienen una percepción positiva como carreteras e infraestructura en general, en los demás temas hay una percepción positiva.

Frente a la pobreza, el gobierno ha hecho énfasis en mostrar cifras positivas sin embargo la calificación es negativa, lo que evidenciaría que hay un discurso oficial que no se corresponde con la realidad de las personas que son encuestadas.

Además hay que tener en cuenta que esta encuesta obedece a la percepción de una pequeña muestra de ciudadanos de 6 ciudades, lo que estaría mostrando resultados de la cuarta parte de la población total en Colombia. Las encuestas deberían hacerse con muestreos más amplios en ciudades principales, pero también en ciudades intermedias con índices poblacionales menores y en municipios para mostrar la percepción más real y que abarque a otras poblaciones, señala Giraldo.

**“Marketing de la policía le sirve más al director que a la policía”**

En el grupo de preguntas sobre algunos personajes de la vida pública, llama la atención, por una parte la favorabilidad del ministro Vargas Lleras, frente a la “desaprobación” del procurador general Alejandro Ordóñez, que podrían ser contendientes directos en las elecciones presidenciales de 2018, teniendo en cuenta que el segundo es constante referencia de los medios de información, al contrario de Vargas, que tiene un escenario de acción mucho más regional.

Fernando Giraldo también señala que muchas veces las estrategias de publicidad que pretenden mejorar  la imagen de las instituciones favorecen más a los personajes visibles que a las propias instituciones como es el caso del director de la policía y el exministro de defensa. En el caso de la Corte Constitucional es notorio que cuando baja el nivel de publicidad de las denuncias también baja la percepción desfavorable aunque los problemas no estén resueltos, como el del caso del magistrado Pretelt.

**“Seguimos queriendo la paz, pero que no nos cueste nada**”

Las respuestas frente al proceso de paz evidencian que “esta es una sociedad a la que le gusta que le concedan los derechos, pero que no se los concedan a otros”. El reflejo de ello aparece en la medida en que las preguntas comprometen lo que los encuestados arriesgarían o entregarían en favor de las víctimas o de la justicia.

Aunque el panorama de percepción es negativo, en general, Giraldo afirma que es necesario persistir e insistir en la formación de valores democráticos en medio de una sociedad claramente conservadora.

[Encuesta Gallup, Junio de 2015](https://es.scribd.com/doc/270438196/Encuesta-Gallup-Junio-de-2015 "View Encuesta Gallup, Junio de 2015 on Scribd")

<iframe id="doc_67164" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/270438196/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="70%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
