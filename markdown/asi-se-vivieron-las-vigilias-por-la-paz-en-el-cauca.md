Title: Así se vivieron las vigilias por la paz en el Cauca
Date: 2016-11-02 08:54
Category: Nacional, yoreporto
Tags: Cauca, Vigilias por la paz
Slug: asi-se-vivieron-las-vigilias-por-la-paz-en-el-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/vigilias-por-la-paz-cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [**Por: Agencia de Comunicaciones Prensa Alternativa Cauca**] 

###### [2 Nov 2016 ] 

[Atendiendo la invitación de las FARC-EP, distintas comunidades llegaron a los cinco sitios establecidos por la insurgencia para realizar las [vigilias por la paz](https://archivo.contagioradio.com/?s=vigilias+por+la+paz) en el Cauca: Monte Redondo -municipio de Miranda; La Elvira -municipio de Buenos Aires; Pueblo Nuevo -municipio de Caldono; Santa Fe - municipio de El Bordo y en el Sinaí- municipio de Argelia.]

[Con saludos de la guerrilla iniciaron algunas de las vigilias, tal como sucedió en La Elvira -municipio de Buenos Aires- donde un importante número de personas arribaron para conocer de primera mano a las y los integrantes de este grupo alzado en armas.]

["Para nosotros esto es nuevo. Conocimos a los guerrilleros a partir del imaginario que nos dieron los grandes medios de comunicación. Siempre nos decían que eran sanguinarios y violentos. Pero uno llega y los ve, habla con ellos, comparte con ellos y se da cuenta de todo lo contrario. Mira en esas personas el ánimo, la esperanza, las ganas de cambiar el mundo. Su amabilidad, estudio y sonrisas son características de ellos." Comenta Juan Rengifo, trabajador de la zona urbana de Popayán.]

[Las vigilias por la paz, convocadas por la insurgencia luego de la declaraciónde Santos en la que planteó la finalización del cese bilateral de fuegos y hostilidades para el 31 de Octubre, fueron acogidas por la sociedad civil para hacerle un llamado al gobierno nacional de que la paz es imparable y de la necesidad de rodear a combatientes del Estado y la insurrección para que no continúen matándose. Y tuvo su recompensa: días antes de las vigilias en el territorio nacional, el presidente amplió la fecha del cese: hasta el 31 de diciembre.]

[Fueron dos días entre la oración, la fe, lo cultural, deportivo y el intercambio de experiencias entre conversaciones que se dieron a lo largo de las vigilias.]

### **La Elvira, Buenos Aires** 

[Abriendo el espacio, el comandante Walter, del Bloque Occidental, leyó un documento en el que planteó la necesidad imperiosa de la reconciliación y la defensa de los acuerdos. Y para eso llegaron las comunidades; a ratificar que lo plasmado en las 297 páginas es del pueblo y que como pueblo, lo defenderán.]

[Los actos culturales y de reconciliación tuvieron su espacio, para luego dar paso a los partidos de futbol sala, deporte practicado por la gran mayoría de los colombianos y el mundo. Entre porras, gritos y algarabía, los equipos de las FARC, de medios alternativos de comunicaciones y de comunidades en general salieron al campo de juego y en una improvisada hazaña de los locutores, bautizaron la cancha como el Estadio Monumental Manuel Marulanda, en homenaje a su histórico y máximo líder.]

[Al término de los actos deportivos y culturales, comenzó la vigilia por la paz, no sin antes escuchar una muy interesante intervención del comandante Miguel Pascuas, único integrante fundador de las FARC-EP que se mantiene con vida; el marquetaliano compartió sobre el nacimiento de la insurgencia, el porqué de haber escogido el camino de las armas, algunas de las vivencias y la necesidad de la paz en nuestro país.]

[Entre intervenciones religiosas, actos de conmemoración, cantos, rezos, pero sobre todo de fe, guerrilleros y población civil estuvieron juntos alzando su voz por la reconciliación nacional.]

[Las FARC-EP en esta vigilia lograron lo que muy pocas veces se ve en cualquier esfera de la población: reunir a católicos, cristianos, testigos de Jehová, evangélicos y gnósticos en un solo escenario para que juntos clamaran al señor por una Colombia en paz.]

[Bajo la lluvia y un buen plato de pan, salchichas, arequipe, bocadillo, queso doble crema y un vaso de vino, culminó el primer día de oración e intercambio de experiencias.]

[En medio del frio, el fuerte viento que afecta a las alturas y la llovizna, inició el segundo día de vigilia. La misa de uno de los padres motivó la apertura de este importante espacio, luego de ello, los bailes tradicionales de Latinoamérica en cabeza de estudiantes de la Universidad del Cauca, llenaron de alegría el espacio. Y cómo no, el futbol otra vez presente, hasta bien entrada la tarde y para finalizar, en reunión de todos en el espacio principal, se recalcó la necesidad de implementar los acuerdos como bases sólidas de una nueva sociedad donde la democracia, la oposición política y el pensar diferente no sean un motivo para ser asesinados. Le puede interesar: **[Entre vigilias y Campamentos se hace llamado a la paz ](https://archivo.contagioradio.com/entre-vigilias-y-campamentos-por-la-paz/)**]

### **Pueblo Nuevo, Caldono** 

[La jornada comenzó en El Pital, donde se concentraron los integrantes de los resguardos indígenas de San Lorenzo de Caldono, Las Mercedes, La Laguna, Siberia, Pueblo Nuevo, Tumburao, La Aguada, Pioya y la Universidad del Valle. Allí se realizó un acto en homenaje a los dos guardias indígenas caídos durante la Minga Agraria de este año, Marco Aurelio Díaz Ulcue y Gersain Cerón Tombe, símbolos de la resistencia Nasa. De allí se realizó una gran marcha por la paz hacia Pescador, seguida de una caravana hacia Pueblo Nuevo, sitio escogido para la realización de la vigilia por la paz.]

[A pesar de la lluvia y el frío, las actividades se desarrollaron con alegría, un profundo sentimiento de reconciliación y esperanza hacia el porvenir, más allá de la conciencia colectiva que se tiene en torno a que "lo fácil ya pasó, ahora viene lo difícil, la paz política, económica y social". Es que las comunidades tienen ahora la oportunidad de ser protagonistas de la paz y aportar a la reconciliación y a la reintegración con dignidad de los ex combatientes.]

[En nombre de la insurgencia, Marcela pidió perdón por los errores que se cometieron en los territorios durante el accionar en el marco del conflicto, asegurando que fueron consecuencias involuntarias de la complejidad de la guerra. A su vez, valoró y agradeció las vigilias por la paz, la participación de tantas comunidades y organizaciones sociales, demostrando todos juntos con esto el verdadero sentir del pueblo colombiano a favor de la paz estable y duradera. ]

[Luego de los saludos e intervenciones de los diferentes gobernadores de resguardos, organizaciones sociales e iglesias, se pasó a los actos religiosos, donde con la participación de toda la comunidad y la barriga llena por la sabrosa comida compartida, se pasó la noche celebrando el comienzo de la reconciliación y de la vida en paz de los pueblos.]

### **La necesidad de implementar los acuerdos** 

[En todos los escenarios de paz que comunidades, insurgencia, organizaciones políticas y sociales, medios alternativos de comunicaciones, instituciones, entre otros, trabajan y conforman, está presente la exigencia al gobierno nacional de implementar los acuerdos de La Habana.]

[Los acuerdos son del pueblo y para el pueblo colombiano. En esa medida, las comunidades ven en ellos las bases necesarias para construir una Colombia en paz con justicia social.]

[La total implementación de este documento histórico daría por terminada una larga guerra de más de cincuenta años, ya que propone temas fundamentales para la superación de las causas del conflicto: Reforma Rural Integral, participación política, reparación a las víctimas, solución al problema de las drogas ilícitas, finalización del conflicto e implementación.]

[Es por estos puntos que millones de personas están dispuestas a seguir luchando, así lo expresa Gloria Zapata, habitante de Bueno Aires -Cauca: "Los acuerdos no son de la guerrilla ni el gobierno. Los acuerdos son nuestros y por ello vamos a luchar hasta su total implementación. Esos puntos que están en el documento final serán nuestra hoja de ruta por los próximos diez años, será la plataforma de la gran mayoría de los habitantes". ]

**Más Noticias: **

-   **[La memoria renace en vigilia por la Paz en El Castillo, Meta](https://archivo.contagioradio.com/vigilia-por-la-paz-en-el-castillo-meta-2/)**

