Title: En el limbo la construcción de la nueva Cinemateca Distrital
Date: 2016-05-19 16:28
Author: AdminContagio
Category: 24 Cuadros
Tags: Alcaldía Enrique Peñalosa, Cinemateca Distrital, Nueva cinemateca
Slug: gestores-culturales-insisten-en-construccion-de-nueva-cinemateca-distrital
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/d52db21c8b73dfa7443cd2f08e42db28.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: IDARTES 

###### 19 May 2016

Ante el limbo en el que se encuentra la construcción de la nueva Cinemateca distrital de Bogotá, proyecto gestionado durante la administración anterior, miembros del sector cultural del país decidieron elevar una petición ante el alcalde Enrique Peñalosa, destacando la importancia que el proyecto tiene para el desarrollo de la industria cinematográfica colombiana.

En el manifiesto, los gestores destacan la relevancia que para la cultura de la ciudad, las nuevas generaciones de realizadores y espectadores tiene la adecuación de un espacio en el que  se pueda exhibir, almacenar y preservar el patrimonio audiovisual contenido en la entidad, así como también las diferentes publicaciones a las que pueden acceder libremente sus visitantes.

El proyecto para construir la nueva Cinemateca, se materializó gracias al convenio establecido entre la Secretaría General de la Alcaldía Mayor, la Empresa de Renovación Urbana ERU y el Instituto Distrital de las Artes IDARTES,  para hacer el traspaso del lote, ubicado en la carrera 3ra con calle 19, financiación de la construcción y convocatoria nacional para el diseño arquitectónico de la edificación.

El argumento esgrimido desde la administración basado en la falta de recursos para tal inversión, no se comprende desde el sector audiovisual teniendo en cuenta que al consorcio Cine Cultura Bogotá conformado por Inversiones y Construcciones Andes S en C, Royal Suministros SAS y Donado Arce y Compañía le fue adjudicada la licitación pública por un valor de 22.387.660. 628 millones de pesos, además de los 9 mil millones de pesos para legalizar el traspaso del lote propiedad de la ERU a IDARTES, dos instituciones de la ciudad.

Los peticionarios en su solicitud, recuerdan al alcalde que los programas culturales han estado siempre por encima de las cuestiones políticas, lo que ha permitido dar continuidad a programas exitosos entre los que se mencionan los Festivales al parque, los CLAN, y las salas concertadas de teatro y se insta a buscar de ser necesarias alianzas con privados que permitan realizar la obra, tema que había sido mencionado por María Claudia López, secretaria de Cultura, Recreación y Deporte de Bogotá.

La [petición en el portal Change](https://www.change.org/p/alcald%C3%ADa-mayor-de-bogot%C3%A1-el-proyecto-de-la-nueva-cinemateca-de-bogot%C3%A1-deb-continuar) puede ser firmada por  todas aquellas personas que deseen apoyar la iniciativa, sumada a la campaña en redes sociales denominada \#NuevaCinematecaSi en la que se invita a todos los partidarios a enviar un video como forma de apoyo a la propuesta y participar en un twiterazo este jueves 19 de mayo desde las 6 de la tarde.

[![twitterazo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Ci1GGnZWEAEOlWQ.jpg){.aligncenter .size-full .wp-image-24140 width="398" height="375"}](https://archivo.contagioradio.com/gestores-culturales-insisten-en-construccion-de-nueva-cinemateca-distrital/ci1ggnzweaeolwq/)
