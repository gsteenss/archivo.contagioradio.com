Title: La incultura que lucra
Date: 2015-06-10 15:00
Category: Opinion, superandianda
Slug: la-incultura-que-lucra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/image2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### Por [Superandianda](https://archivo.contagioradio.com/superandianda/) - [~~@~~Superandianda](https://twitter.com/Superandianda)

Convertimos en farándula a la realidad para negar y pretender borrar la problemática social que tenemos, la grave crisis cultural que llevamos y la ignorancia que mantenemos.

Por país tenemos a una gran empresa donde todos son los empleados incultos sometidos a sus gerentes y directores; como la misma palabra lo indica simplemente se emplean para un trabajo que no genera pensamiento, analisis, unión y mucho menos patria. Teniendo en cuenta que el pequeño grupo que genera la opinión colombiana es cerrado porque sencillamente no se crea opinión diferente a la que brindan las revistas y periódicos tradicionales.

La política colombiana aburre, la cultura arribista colombiana aburre, la incultura urbana aburre, pero lo que más aburre son sus empleados y sus críticos cultos -quienes hacen de la incultura una empresa en crecimiento- conforman un círculo vicioso donde unos son los que mantienen el negocio y otros son los que falsamente lo critican, para al final seguir siendo los mismos. Nuestro sistema educativo carece de diseño para la organización y la creación de individuos que sean capaces de ejercer una crítica genuina que brinde una solución social, este ha sido un factor determinante para que no tengamos análisis, ni participación política, ni nuevos procesos que propongan una reestructuración del sistema. Las mejores universidades son pagas y las pocas buenas universidades públicas que nos quedan se han vuelto un mercado, en todo el sentido de la palabra.

Nuestra educación es el arribismo y la incultura. Falta poco para que las universidades sean centros comerciales, ahorraríamos tanto en tiempo si tuvieran las carreras en vitrinas y pudiéramos salir a comprarlas inmediatamente sin tener que soportar 5 años cumpliendo una inútil asistencia. Lo realmente triste es como la clase obrera cree estudiar para seguir siendo clase obrera y como las ciencias humanas, el arte y la política quedan en manos de los más acomodados.

Es casi cínico escandalizarse por los colados del transporte masivo, por las niñas cabezas de pandillas, por los niños padres de 12 años; el saqueo, la patanería y la delincuencia de la que se sienten orgullosos, y señalo que se sienten orgullosos porque de otra manera los canales no ganarían más audiencia vanagloriando la conducta salvaje y contraculta en la que nos hemos convertido. No sé qué me causa más temor: ver documentales de pequeños delincuentes o salir a la calle a ver sus actos primarios.

La incultura colombiana ha servido no solamente para patrocinar a los mandatarios corruptos sino también para llenarnos de documentales, películas, novelas, escritores y periodistas que se nutren de ella, de nuestra desgracia y de nuestra violencia. No generan ningún tipo de solución social, como pretenden hacerlo entender, utilizan el deprimente contexto para realizar de ahí sus producciones y campañas, utilizando a los sectores atacados por el olvido del estado para lucrarse de su desgracia.

Rosario, Tijeras, La Virgen de los Sicarios, La vendedora de Rosas; producciones que nos venden como críticas son otra problemática para el país, porque han hecho del entorno natural de la delincuencia un negocio que llaman "producciones creativas" y de la falta de oportunidades de un círculo social olvidado una promoción de patrones de vida con la que sus espectadores buscan identificarse, anhelando ser el problema que ven en la gran pantalla, en la televisión y leen en el libro. Estas producciones no brindan la solución que estos contextos necesitan, al contrario, lo expanden a donde no ha llegado. Sin señalar de qué manera crece la mal literatura y música sobre héroes delincuentes.

La incultura lucra y nuestra evolución es una involución.

“*Sin cultura, y la relativa libertad que implica, la sociedad, incluso cuando es perfecta, es una jungla. Por ello, toda creación auténtica es un regalo para el futuro*” Albert Camus.
