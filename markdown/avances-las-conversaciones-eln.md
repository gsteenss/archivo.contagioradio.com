Title: ¿En qué va el proceso con el ELN? Monseñor Dario Monsalve responde
Date: 2016-11-17 18:09
Category: Nacional, Paz
Tags: Acuerdos de paz en Colombia, conversaciones de paz con el ELN, Diálogos ELN
Slug: avances-las-conversaciones-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/eln-y-gobierno-e1474051328819.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [17 Nov 2016] 

Monseñor Darío de Jesús Monsalve facilitador de la iglesia católica en los diálogos con el ELN aseguró que se ha logrado avanzar en materia de protocolos para la** liberación de secuestrados, la entrega de la verdad** a familiares de víctimas y por su parte el Gobierno ha adelantado los **trámites para la excarcelación de dos guerrilleros que serían gestores de paz una vez instaladas las conversaciones. **

Concorde a lo pactado, el ELN ya entregó a dos secuestrados, un arrocero de Arauca y un ex alcalde de Charalá, al saber que entre ellos no figuraba Odín Sánchez el Gobierno volvió a condicionar el inicio de la fase pública, sin embargo el ELN aseguró que están haciendo gestiones internas con el **frente Cimarrones ubicado en el Chocó para la liberación de Odín Sánchez.**

Por otra parte Monseñor Monsalve  asegura que es fundamental para los diálogos que el ELN **“haga una declaratoria de renuncia a la práctica del secuestro y de entrega de la verdad a los familiares de quienes han sido víctimas de esta práctica”.**

### **¿Cuáles son los retos?** 

El arzobispo de Cali, manifiesta que a pesar de los altibajos que ha tenido este proceso **“lo fundamental es consolidar la voluntad del pueblo colombiano** (…) la voluntad de nosotros como iglesia es resolver a través del diálogo y de manera civilizada los conflictos sociales”.

Monseñor Monsalve menciona que **“sin la mesa no es posible superar los altibajos porque surgen todo el tiempo informaciones y distintas versiones que entorpecen el diálogo”.**

Esta confusa situación ha hecho que las negociaciones sean en medio de la confrontación y “eso ha afectado gravemente a las comunidades, por eso **es necesario que no hayan más dilaciones en la instalación de esta mesa”** manifiesta Monseñor Monsalve. Le puede interesar: “ELN [y Gobierno deben cumplir su palabra” Carlos Velandia.](https://archivo.contagioradio.com/eln-y-gobierno-deben-cumplir-su-palabra-carlos-velandia/)

Por último el arzobispo advierte que **el conjunto de la población debe defender la Paz**, pues "hay un grave riesgo de que la situación de guerra se agudice debido a que muchos **sectores de la sociedad hasta el momento no apoyan la iniciativa de diálogo** con el ELN y **las comunidades son las que deben enfrentar situaciones como la de días pasados en el sur de Bolívar”.**

<iframe id="audio_13805053" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13805053_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
