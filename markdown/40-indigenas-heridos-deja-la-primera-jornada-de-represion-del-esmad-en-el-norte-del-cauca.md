Title: 40 indígenas heridos y 2 muertos deja primera jornada de represión del ESMAD en el Norte del Cauca
Date: 2015-02-27 17:42
Author: CtgAdm
Category: Movilización, Nacional
Tags: ACIN, ESMAD, Feliciano Valencia, Masacre del Nilo, Norte del Cauca, Recuperación de la Madre Tierra, víctimas ESMAD
Slug: 40-indigenas-heridos-deja-la-primera-jornada-de-represion-del-esmad-en-el-norte-del-cauca
Status: published

###### Foto: Reiniciar 

<iframe src="http://www.ivoox.com/player_ek_4143797_2_1.html?data=lZahlZyde46ZmKiak56Jd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYqMrQrcTdwtPcjbvFsMbixM7OjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### [Entrevista con Feliciano Valencia, líder indígena]**[ ]** 

Más de 5000 indígenas habitantes del Norte del Cauca se encuentran desde esta semana en un proceso de **liberación de la Madre Tierra en 6500 hectáreas**, ubicadas en Miranda, Corinto y Santader de Quilichao, tierras usadas como productoras de caña para los ingenios del Valle del Cauca. Los indígenas afirman que  durante 6 meses han esperado que el gobierno cumpla lo que prometió en torno a la asignación de tierras para las comunidades indígenas, asignación que asciende a las 20 mil hectáreas.

Según Feliciano Valencia, vocero de las comunidades, en la primera jornada de represión por parte del ESMAD, la Policía Nacional y el Ejército, **40 indígenas resultaron heridos,  3 de ellos con impactos de arma de fuego**. Según Valencia, las fuerzas de seguridad del Estado, actúan de manera conjunta, “el **ESMAD** viene adelante con gases y bombas recalzadas, detrás la policía disparando armas de fuego  por el otro lado está el ejército disparando contra los indígenas en retirada”. De acuerdo al último reporte de la comunidad, durante el trancurso de la tarde de hoy, ya hay 2 indígenas muertos.

Por su parte, Oscar Quintero, el Alcalde de Corinto, señaló a las comunidades indígenas de destruir el puente que conduce de Corinto a Miranda, destruir el acueducto en construcción e incendiar los cultivos de caña que ocupan las fincas reclamadas por los indígenas.

Sin embargo, Feliciano Valencia afirma que **es una lástima que el Alcalde justifique la represión**, que las comunidades no destruyeron el puente, que no han disparado  que si algún policía resultó herido fue por las balas del ejército que dispara de frente a los indígenas cuando huyen de la policía y que el ESMAD con las bombas aturdidoras y los gases son los que prendieron fuego a los cultivos.

Según Valencia, estas tierras deben estar en manos de los indígenas como parte del acuerdo al que se llegó con las víctimas de la masacre del Nilo, que incluye una extensión de 20000 hectáreas de tierras para las comunidades indígenas en la región del Norte del Cauca.

Se espera que en el trascurso del día se haga presente una comisión de gobierno que frene la represión, evite más víctimas y de respuestas efectivas a las demandas de los comuneros y comuneras.
