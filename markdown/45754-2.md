Title: Entregamos bienes por 963 mil millones de pesos: Pastor Alape de FARC
Date: 2017-08-25 13:55
Category: Nacional, Paz
Tags: acuerdo de paz, Bienes de las FARC, FARC, pastor alape, proceso de paz
Slug: 45754-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-25-at-12.50.38-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [25 Ago. 2017] 

Un total de 963 mil millones de pesos divididos en 20 mil cabezas de ganado vacuno y 597 de equino, 327.520 gramos de oro, **3.753 km de infraestructura vial y 21 mil millones de pesos en muebles y enseres, son algunos de los ítems del listado** que esta guerrilla le ha entregado como inventario a ONU y que fue dado a conocer luego de las críticas hechas por el Fiscal Néstor Humberto Martínez, a los bienes entregados para reparar a las víctimas.

**Según Pastor Alape integrante de las FARC, el Fiscal falla en sus consideraciones frente al inventario entregado** y en el entendimiento de lo que es un inventario “falla al realizar una presentación amañada de la información porque con todo ello conduce a que se induzca a la opinión pública a una valoración distorsionada de un hecho que es cierto y es que cómo las FARC hemos cumplido con lo acordado”.

Dice Alape que **la entrega de este inventario se hizo con la mayor rigurosidad posible,** pero hace la salvedad de que en medio de las dinámicas de guerra hay información que tienen compartimentada e incluso hay otra que han perdido. Le puede interesar: [Gobierno le hace 'conejo' a las FARC: Andrés París](https://archivo.contagioradio.com/gobierno-le-hace-conejo-a-las-farc/)

Por tal motivo, manifiesta las FARC que si el Estado, principalmente por cuenta de la Fiscalía, logra encontrar algunos bienes que puedan ser probados son de esa guerrilla y que no se encuentren en su inventario **“aceptamos esos bienes como nuestros y que estos vayan para la reparación de las víctimas.** (…) Hay que reconocer con sinceridad, que el inventario que hemos entregado apenas representa una contribución modesta a la reparación material de las víctimas”.

### **¿Cómo presentó las FARC este inventario?** 

Manifiesta Alape que la discriminación del inventario se realizó por bloques y frentes que conforman esa organización, de esa manera pudieron recoger la información de los bienes inmuebles rurales.

“Estos se presentaron atendiendo a normas técnicas, pero donde no fue posible, la información que se suministró se hizo ofreciendo los datos que permiten su plena identificación y localización geográfica, en los términos que se utilizan en el campo”. Le puede interesar: [Partido de las FARC empezará vida política el primero de septiembre](https://archivo.contagioradio.com/partido-de-las-farc-empezara-vida-politica-el-primero-de-septiembre/)

Además, recuerda que, **debido a la desatención del Estado, en el campo se han desarrollado múltiples modalidades informales de regulación** con ausencia de registros catastrales o de registros inmobiliarios “el inventario da cuenta de esa realidad objetiva. Estamos dispuestos a hacer una tarea en terreno que cualifique la información entregada, sin embargo, le corresponde al Gobierno proceder a la identificación plena y el aseguramiento de lo reportado”.

### **No procede ridiculizar la entrega de cierto tipo de muebles o enseres** 

Para las FARC, todos los bienes que entregaron están en óptimas condiciones, eso incluye los muebles y enseres “que, si se es riguroso, no procede a ridiculizar la presencia de determinado tipo de bienes y más aún magnificarla de manera malintencionada”.

### **Es un sinsentido desconocer las obras como parte del inventario** 

“Con nuestra presencia territorial ejercimos funciones de Estado y  **la construcción de vías fueron un componente esencial de nuestra economía de guerra.** Gracias a ellas, además, miles de compatriotas de la Colombia olvidada pueden comunicarse entre sí e incluso sacar sus productos al mercado” puntualiza Alape.

### **“No nos interesa incumplir el acuerdo”** 

Si las FARC mintiera al respecto de la entrega de los bienes estarían incumpliendo los compromisos pactados en el Acuerdo de Paz, lo que automáticamente los dejaría por fuera de este y les acarrearía todas las consecuencias penales del Estado.

Por tal motivo **“recordamos que quienes menos interés tienen en incumplir los acuerdos somos nosotros mismos”** recuerda Alape. Le puede interesar: [Fiscal debe dejar de tratar a las FARC como si no hubieran firmado la Paz: Andrés Paris](https://archivo.contagioradio.com/farc/)

### **Inventario entregado por las FARC a las Naciones Unidas** 

![Bienes entregados por las farc](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Bienes-entregados-por-las-farc.png){.alignnone .size-full .wp-image-45774 width="800" height="1200"}

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="osd-sms-wrapper">

</div>
