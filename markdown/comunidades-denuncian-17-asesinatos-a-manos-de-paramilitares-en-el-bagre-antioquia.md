Title: Comunidades denuncian 17 asesinatos a manos de paramilitares en El Bagre, Antioquia
Date: 2016-07-06 11:17
Category: DDHH, Entrevistas
Tags: asesinatos en el bagre antioquia, campamento humanitario en Antioquia, El Bagre Antioquia, paramilitarismo en Colombia
Slug: comunidades-denuncian-17-asesinatos-a-manos-de-paramilitares-en-el-bagre-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Paramilitares....png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [6 Julio 2016 ] 

En este primer semestre del año, por lo menos **diecisiete campesinos han sido asesinados, desmembrados, tirados a ríos o enterrados en fosas comunes** en el municipio de El Bagre, Antioquía, según denuncian las comunidades, a manos de los grupos paramilitares que hacen presencia en la región, denominados como Autodefensas Gaitanistas y conformados tras la desmovilización de las Autodefensas Unidas de Colombia, en 2006. Estos hechos se han puesto en conocimiento de la Defensoría del Pueblo, esperando que se pongan en marcha **acciones pertinentes para el desmonte de las estructuras armadas** y se brinden condiciones de seguridad para las familias que desde el pasado 28 de junio han instalado un campamento refugio humanitario.

Mauricio Sánchez, miembro de la 'Asociación de Hermandades Agroecológicas y Mineras de Guamocó', afirma que **es absurdo que las instituciones insistan en que son normales los asesinatos selectivos, las torturas y desmembramientos** que los paramilitares continúan perpetrando en la región. El líder asevera que la situación de derechos humanos es grave y se agudiza por cuenta de los operativos monumentales que la fuerza pública viene implementando contra la minería ilegal y que podrían generar nuevos desplazamientos en la zona.

Este martes en las veredas de La Unión y Benito, la fuerza pública desplegó un operativo contra la minería ilegal en el que quemaron cerca de cinco retroexcavadoras, **detuvieron a cinco personas e hirieron a un poblador**, para Sánchez resulta desconcertante que se inviertan tantos recursos en este tipo de operativos, pero no en las acciones que las comunidades han demandado para enfrentar a los grupos paramilitares que a diario amenazan su supervivencia y quienes exigen pagos de extorsiones hasta a los vendedores de tintos.

Los operativos se han implementado en el marco del decreto 2235 de 2013 que autoriza a la fuerza pública destruir toda la maquinaría que se está usando para la minería ilegal; sin embargo "el código de minas, la ley 685 de 2001, tiene muchos vacíos jurídicos. **La expresión de minería ilegal queda en el limbo a la interpretación de quién ejecute estos operativos**, porque hay que reconocer que en la región históricamente han existido comunidades que dependen única y exclusivamente de la extracción de oro", afirma Sánchez.

Vea también: [[200 campesinos instalan refugio humanitario en El Bagre, Antioquia](https://archivo.contagioradio.com/200-campesinos-instalan-refugio-humanitario-en-el-bagre-antioquia/)]

<iframe src="http://co.ivoox.com/es/player_ej_12138351_2_1.html?data=kpeelZ2XeZKhhpywj5aVaZS1lJaah5yncZOhhpywj5WRaZi3jpWah5ynca7V1tfWxc7Tb7SZpJiSo5bSp8nZ24qfpZClt9DXysbQy4qnd4a2lNOYxsqPjMbmzsbbxsbIqdSfoszf0crHs82ZpJiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
