Title: Liberadores de la madre tierra denuncian ataque del ejército en Cauca
Date: 2018-07-12 13:23
Category: Nacional
Tags: Cauca, CRIC, liberadores madre tierra
Slug: liberadores-corinto-ejercito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Dh37Jl6U8AAQ6Ei.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:CRIC 

###### 12 Jul 2018 

Integrantes del Consejo Regional Indígena del Cauca CRIC, **denunciaron ataques por parte del ejército colombiano**, mientras adelantaban el proceso de liberación de la madre tierra en el municipio de Corinto, en el norte del departamento.

Las comunidades involucradas, aseguran que los hechos se presentaron el martes 10 de julio hacia las 10 de la mañana, **cuando se encontraban resguardando los terrenos de la hacienda Miraflores, que hace parte de sus territorios ancestrales, titulados a la empresa Incauca**.

Incialmente e**l grupo de liberadores había expulsado a la seguridad privada encargada de apoyar el trabajo de la maquinaria utilizada para la siembra de caña**, que según las comunidades, algunas veces también destruyen sus lotes de cultivo y de pastoreo.

El reporte indica que las fuerzas militares llegaron a respaldar a los guardas privados, a lo que les respondieron “**retírense que el problema no es con ustedes. Ustedes también son pobres, explotados**” palabras que habrían desatado el choque entre las partes (Le puede interesar: [66% de los pueblos indígenas está a punto de desaparecer: ONIC](https://archivo.contagioradio.com/pueblos-indigenas-a-punto-de-desaparecer-onic/))

En la denuncia, el CRIC da cuenta de **algunos disparos realizados por los uniformados, golpes e insultos a mujeres, dos heridos y la detención de uno de los comuneros**, por quien solicitan sea entregado al cabildo indígena de Corinto, donde es originario.

A pesar de los hechos, los liberadores insistieron en que no buscan confrontaciones con la seguridad de Incauca ni con el ejército, y que responderán solo si son atacados, y que seguirán adelante en su propósito “aunque detengan gente, aunque nos maten gente, vamos a seguir liberando la Madre Tierra con más coraje”.

##### **Detenido indígena por amenaza a militar con machete** 

Autoridades notificaron que este miércoles fue capturado **José Bolívar Pilcué Quilcué,** sindicado de haber amenazado con un machete a un soldado el pasado 4 de enero durante una confrontación con trabajadores de Incauca. Según el reporte, el líder indígena que fue en la Hacienda Miraflores, municipio de Corinto, deberá responder por el delito de violencia contra servidor público.

Desde el inicio del caso, **miembros de las comunidades han denunciado que se trata de un falso positivo judicial**, recordando que presuntamente a Pilcué se le incautó una granada de fragmentación y pólvora negra. Adicionalmente **aseguran que se cometieron irregularidades durante el proceso de captura** por uso indebido de la fuerza.

###### Reciba toda la información de Contagio Radio en [[su correo]
