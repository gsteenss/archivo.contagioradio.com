Title: Empleados de Air France completan una semana en huelga
Date: 2016-08-02 16:17
Category: El mundo
Tags: Huelga Francia
Slug: empleados-de-air-france-completan-una-semana-en-huelga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/7C-AIR-FRANCE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Capitalquintanaroo] 

###### [2 de Agos] 

Desde el pasado 27 de Julio **empleados de la aerolínea Air France se encuentran en huelga laboral debido a un reajuste que se realizaría en sus contratos**, en donde se señala que la renovación de los mismos se hará solamente por 17 meses y no de tres a cinco años como lo exigen los trabajadores.

La huelga ha generado que se cancelen **1.030 mil vuelos** que tendrían un costo total de **90 millones de euros para la aerolínea Air France** y que hasta el momento reporta una afectación a **127 mil pasajeros,** según indico el presidente de la empresa Frédéric Gagey.

Por otro lado, este paro de actividades es impulsado por **5 sindicatos de la compañía francesa** en donde se encuentran trabajadores de cabina como lo son las azafatas, los asistentes de cabina, controladores aéreos y los pilotos. [En su pliego de exigencias los sindicatos exponen la necesidad de re evaluar el tiempo de contratación](https://archivo.contagioradio.com/persiste-movilizacion-contra-la-reforma-laboral-en-francia/), el cambio de las reglas de remuneración y el reparto de las actividades entre la aerolínea Air France y sus socios KML Royal Dutch Airlines.

A su vez, los sindicatos han señalado que esta manifestación se une a[las protestas registradas en todo Francia por las reformas laborales](https://archivo.contagioradio.com/3-meses-de-violenta-represion-contra-la-movilizacion-en-francia/) y que se prepararán para movilizarse con los demás gremios del país en la **marcha nacional convocada para el 14 de agosto.**

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
