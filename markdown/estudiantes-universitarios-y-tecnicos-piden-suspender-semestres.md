Title: Estudiantes universitarios y técnicos piden suspender semestres
Date: 2020-03-25 00:23
Author: AdminContagio
Category: Actualidad, DDHH
Tags: ser pilo paga, Universidades
Slug: estudiantes-universitarios-y-tecnicos-piden-suspender-semestres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Estudiantes-piden-suspender-semestres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Agencia de Noticias UN {#foto-agencia-de-noticias-un .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para enfrentar la crisis generada por el Covid-19 y evitar nuevos contagios algunas Instituciones de Educación Superior (IES) como la Universidad Nacional o los Andes han virtualizado sus clases, mientras otras como la Jorge Tadeo Lozano han suspendido actividades académicas y administrativas temporalmente. Sin embargo, en [twitter](https://twitter.com/MafeCarrascal/status/1241824420798308354) se denunció que dicha institución habría suspendido también los contratos de sus trabajadores, lo cual podría generar una situación de inseguridad económica para quienes se vean afectados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la imposibilidad de que algunos estudiantes se conecten a las clases virtuales, la poca preparación de algunas Instituciones para asumir este cambio y la duda sobre la calidad de la educación virtual, estudiantes de educación superior han pedido que se suspendan los semestres. Pero, contrario a lo que hizo la Universidad Tadeo, garantizando la estabilidad financiera de la comunidad universitaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una comunicación enviada por la Universidad Jorge Tadeo Lozano al equipo de Contagio Radio, la institución asegura que no se han suspendido los contratos y tampoco se ha dejado de atender las necesidades de los estudiantes beneficiarios del programa Ser Pilo Paga.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Se estima que un 20% de los beneficiados por Ser Pilo Paga (SPP) no pueden recibir clases virtuales**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Darwin López, estudiante e integrante de la Sociedad Pilo (organización que agremia a beneficiarios de SPP), señaló que se han presentado dificultades en la adaptación a la virtualidad en casi todas las universidades porque algunos estudiantes no tienen los medios físicos para poder conectarse a la clase. López estimó que un 20% de los beneficiados estarían enfrentando este problema.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, sostuvo que otros asuntos como la manutención en la ciudad y la alimentación están siendo un problema para los 'Pilos' porque muchos obtenían un subsidio de las Universidades, o alimentación directamente de las misas. En ese sentido, recordó que muchos de los beneficiarios del programa son foráneos y de recursos económicos limitados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

López afirmó que, por ejemplo, en la Tadeo, los estudiantes dejaron de recibir algunos de estos beneficios como la alimentación. Por esa razón, el integrante de la Sociedad Pilo solicitó el estudio al Gobierno de hacer un giro adicional a los beneficiarios para garantizar su alimentación y hospedaje en Bogotá, y mientras tanto, dijo que seguirán acudiendo a la solidaridad de otros estudiantes para tener elementos de básicos de mercado y ver clases en casa de sus compañeros, como ha ocurrido en algunos casos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **UNEES pide la suspensión de los semestres**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cristian Reyes, vocero de la Unión Nacional de Estudiantes de la Educación Superior (UNEES) declaró que con la virtualización de las clases han encontrado dos problemas: Que las IES no tienen capacidad técnica para asumir las clases virtuales y que una parte importante de los estudiantes no pueden acceder a estas clases, lo que es una vulneración a su derecho a la educación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A ello se suma que en algunos casos, las clases se están resumiendo a "una entrega de trabajos", lo que no aporta al aprendizaje de los estudiantes. En ese sentido, Reyes sostuvo que la virtualidad solo es una herramienta de la educación, y no debe ser tomada como el medio para asegurar este derecho.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por estas razones, el vocero de la UNEES dijo que están pidiendo la suspensión de los semestres, reconociendo que en casos como la Universidad Distrital, el semestre actual es 2019-II y debe finalizarse para tener también un cierre financiero. (Le puede interesar: ["Eduación virtual por Covid-19, una decisión para la que no estamos preparados"](https://archivo.contagioradio.com/eduacion-virtual-por-covid-19-una-decision-para-la-que-no-estamos-preparados/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, en la mayoría de IES la propuesta podría garantizar una adecuada finalización del periodo académico, al tiempo que sería el único recurso para combatir el Covid-19 en los campus. Reyes sostuvo que algunas universidades están adelantando las vacaciones de mitad de año, pero ello supone afectar las garantías laborales de profesores y trabajadores de las Instituciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En cambio, dijo que el ejemplo en este sentido lo está poniendo la Universidad Tecnológica de Pereira, que decidió adelantar los honorarios de las personas vinculadas a la institución al tiempo que suspender los calendarios, lo que "demuestra que la Universidad entiende la función social que tiene", y su compromiso con enfrentar el momento por el que atraviesa el país.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
