Title: Trabajadores de 54 plantas de ECOPETROL protestan contra la posible venta de la empresa estatal
Date: 2020-07-15 17:46
Author: CtgAdm
Category: Actualidad, DDHH
Slug: trabajadores-de-54-plantas-de-ecopetrol-protestan-contra-la-posible-venta-de-la-empresa-estatal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Ecqk4cdXgAA_u61.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La posible venta de Ecopetrol abrió un debate en diferentes sectores del país que **defienden el [patrimonio público](https://archivo.contagioradio.com/trabajadores-de-54-plantas-de-ecopetrol-protestan-contra-la-posible-venta-de-la-empresa-estatal/), al mismo tiempo que exigen garantías dignas para los y las trabajadoras de este sector**, sin dejar de lado que sin Ecopetrol sería imposible una transición energética en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Uno de los sectores que mas ha resonado la consigan de, *"Ecopetrol no se vende, Ecopetrol se defiende"*, son las personas que integran la Unión Sindical Obrera (USO), y la Subdirectiva Única de Oleoducto (SUO), **quienes en medio de la pademia decidieron realizar un acto de protesta en Bogotá el cual cumple ya, 30 días.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde el 25 de junio en el monumento en representación de las válvulas de pozos petroleros, al frente de la sede de Ecopetrol en Bogotá, dos dirigentes obreros se encadenaron en lo que ellos denominaron "El Machín de la resistencia", acto respaldado por los y las trabajadoras de las 54 plantas de transporte de crudo a nivel nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los dirigentes son a **Fabio Díaz, de la SUO - USO,** y **Hernando Silva, perteneciente a la Secretaría de Derechos Humanos y Paz de la SUO,** voceros del sindicado que acatando todas los protocolos de protección determinados por el Gobierno Distrital y Nacional, defienden encadenados sus derechos y el de más de 300 trabajadores que hoy correr el riesgo de quedar desempleados.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/oleoductos/status/1283074255035469825","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/oleoductos/status/1283074255035469825

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Hernando Silva**, reconoció que para que *"los líderes sindicales lleguen a estos extremos de **arriesgar su vida en medio de una pandemia por defender sus Derechos, es porqué ya se han agotado todos los recursos**"*, junto a esto agregó las razones por las cuales iniciaron esta protesta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La primera, se debe al[Decreto 811](http://www.usofrenteobrero.org/index.php/actualidad/7590-el-decreto-811-del-2020-debe-derogarse-es-inconstitucional), emitido por el Gobierno en el marco del Estado de Emergencia, y que *"auto- habilita al Estado para vender empresas públicas que cotizan en la bolsa, entre ellas Ecopetrol y así generar un alivio financiero al país"*, **acción que según el vocero es inconstitucional** y desconoce no solo lo señalado en la Constitución si no los derechos del sector obrero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta posibilidad ha tenido repercusión en el Senado, y en donde el pasado **19 de junio gran parte de los congresistas votaron para que fuese derogado este decreto** y se analizara sobre el manejo que se esta dando al sector público, tras el impacto de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Aquí estamos sensibilizando y generando conciencia para que todo el pueblo colombiano salga a rechazar la venta de Ecopetrol y a defender lo público".*
>
> <cite>**Hernando Silva| Secretaría de Derechos Humanos y Paz de la SUO**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Segundo, el sindicalista indicó que Ecopetrol en el 2013 pasó de ser una sola empresa a convertirse en un conglomerado de empresas una de ellas [CENIT ,](http://www.usofrenteobrero.org/index.php/actualidad/7599-cenit-es-el-corazon-de-ecopetrol) empresa que encabeza el transporte por medio de ductos y poliductos del país, lo que la convierte en la ***"sección más rentable de Ecopetrol y que ahora quiere ser vendida"***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Afirmando que *"**CENIT son las venas de Ecopetrol, y que venderla sería un desangre fatal"***, Silva indicó que esta sección desde el 2013 *"maneja el 82% de la capacidad de transporte total del petróleo en Colombia y 100% en transporte de gasolina,* ***lo que implica cerca del 40% de las utilidades de Ecopetrol".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y añadió que CENIT, desde su inicio no ha representando más que ganancias para el país, *"inició con una inversión de 10 millones de pesos y hoy tiene un valor superior de los 24 billones de pesos o más, entonces **llama la atención se esté posicionando la idea de una posible venta como medida para mitigar la crisis de salud pública, cuando esta es un activo estratégico"**.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una venta que según el dirigente estaría sobre los 9.5 billones de pesos, ***"lo cual está muy por debajo del precio real, es un negocio bastante redondo y a pérdidas del Estado colombiano. Hacer esta venta es una situación absurda"**;* afirmación que ha sido respaldada por diferentes expertos, señalando que es poco rentable para el presente y futuro de la economía colombiana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La tercera y ultima razón, Silva la denominó **"masacre laboral"**, que se da como consecuencia del traslado de nómina de Ecopetrol a CENIT, lo que representa no solo posibles despidos para quienes no acaten esta decisión, sino una significativa disminución de los contratos laborales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cambios que se han empezado a realizar desde hace varios años y que ha afectado a a los 890 trabajadores que conformaban inicialmente la Vicepresidencia de Transporte, de las 54 plantas del país, y que hoy no supera los **500, *"quienes se encuentran resistiendo la agresión, el terrorismo psicológico y laboral que ha impuesto Ecopetrol*** *a través de cartas, llamados de atención y descargo directo".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

13 de los integrantes de esa vicepresidencia se encuentran el riesgo de perder su empleo, esto según Silva, luego de recibir el llamado a descargo; "con este llamado prácticamente les están asegurando la cancelación de su contrato de trabajo, poniéndolos en una situación mucho más preocupante".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez señaló que el cambio a CENIT de cerca de 320 operarios, elimina la *"estabilidad jurídica, dignidad laboral y beneficios que hemos ganado a lo largo de estos [97 años de lucha de la USO](https://archivo.contagioradio.com/conquistas-de-la-lucha-obrera-de-la-uso-a-97-anos-de-existencia/)*". Resaltando que los principales cambios estarían en la salud y la seguridad jurídica.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La venta de Ecopetrol es asunto de todos"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Finalmente el dirigente sindical, concluyó que en estos 20 días de protesta han estado a la espera de que el Gobierno Nacional se pronunció por medio de Ecopetrol, ***"y deje de dar dilación a este tema, esperamos la buena fe por parte de los dirigente de Ecopetrol y que decidan llevar esto a feliz término".***

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **"*Hacemos un llamado al pueblo colombiano para que entre todos fortalezcamos esta defensa,*** *para que se informen y entienda que bloquear las venas de Ecopetrol, terminaría en un colapso total de este sector*"
>
> <cite>**Hernando Silva| Secretaría de Derechos Humanos y Paz de la SUO**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

  
Y agregó que se debe extender este llamado, ***"la venta de Ecopetrol es asunto todos, porqué quién va a sufrir toda esta crisis es el pueblo colombiano, y lo verá en el desangre por medio de más impuestos y reformas tributarias"***. ([Día sin IVA, un insulto a Colombia y una vergüenza mundial](https://archivo.contagioradio.com/dia-sin-iva-un-insulto-a-colombia-y-una-verguenza-mundial/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

***Vea el análisis completo de este tema en : Otra Mirada. ¿Se vende Ecopetrol?***

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D222598968850182&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
