Title: Jóvenes de Colombia se movilizan en contra del cambio climático
Date: 2019-03-18 11:31
Category: Ambiente, Movilización
Tags: cambio climatico, Colectivo SocioAmbiental Juvenil Cajamarcuno COSAJUCA., Fridays for Futures
Slug: jovenes-colombia-se-movilizan-del-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-15-at-2.19.54-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [15 Mar 2019] 

Jóvenes colombianos participaron este viernes en una huelga escolar de nivel mundial, convocada por la ambientalista sueca de 16 años Greta Thunberg para exigir a los gobiernos que adopten políticas urgentes para frenar el avance del cambio climático. Las movilizaciones se realizaron en **más de 1.000 ciudades de 89 países**, incluyendo [Bogotá, ]Barranquilla, Bucaramanga,** **[Cali y Medellín.]

Este movimiento, conocido como Fridays for Futures (FFF), comenzó en Suecia el año pasado cuando Thunberg, actualmente nominada al premio Nobel de Paz, decidió faltar a clase cada viernes y manifestarse fuera del edificio del parlamento sueco para llamar la atención al cambio climático, el cual amenaza el futuro de su generación. Menos de un año después, su protesta individual se volvió un movimiento global.

"Lo que estamos viendo es que **la juventud a nivel mundial ya no va aguantar que los gobiernos vayan pasando por encima de sus derechos** porque, en ultimas, quienes van a tener que sentir la catástrofe ambiental van a ser las generaciones de los 15 años y adelante," afirmó Robinson Mejía, integrante de Colectivo Socio-ambiental Juvenil de Cajamarca (Cosajuca), un grupo de jóvenes que ayudó a detener el proyecto minero, La Colosa, en Cajamarca, Tolima, a través de una consulta popular en el 2017.

Según Mejía, el movimiento ambiental no tiene la misma fuerza en Colombia como en otros países de Europa, sin embargo es un tema que se va avanzando con la pedagogía. "Las discusiones del cambio climático siguen siendo muy académicas y no le llegan a la gente que tiene que mamarse todo el día las condiciones en el Transmilenio o las personas que viven cerca a las zonas industrializadas de la ciudad", el ambientalista sostiene.

### **¿Como contribuye Colombia al cambio climático?** 

Un nuevo informe de las Naciones Unidas indica que se está acabando el tiempo para prevenir los efectos irreversibles  y peligrosos del cambio climático.** **Por tal razón, insta a los gobiernos tomar medidas efectivas para reducir emisiones de efecto invernadero. En comparación con Estados Unidos, China y India, la cantidad de gases que emite Colombia es mínima. Sin embargo, Mejía afirma que Colombia también **contribuya al cambio climático a través de la deforestación de la Amazonía**, considerada el pulmón del planeta.

El líder sostiene que las políticas del Gobierno prioriza la industria extractivista por encima de otras industria, como el turismo o la agricultura, que producen menos daños para el ambiente. Este argumento se evidencia en el Plan Nacional de Desarrollo que  incluye artículos que benefician la industria minero-energética. (Le puede interesar: "[El Plan Nacional de Desarrollo, un respaldo al sector minero-energético](https://archivo.contagioradio.com/plan-nacional-desarrollo-respaldo-al-sector-minero-energetico/)")

### **El rol de los jóvenes** 

Mejía manifiesta que la juventud tiene que actuar y ser constante en su esfuerzos dado que ellos tienen también una responsabilidad en transformar el mundo. "No es la misma responsabilidad que tienen las empresas y los Gobiernos, pero tenemos que hacer algo y yo creo que la movilización es la que va garantizar que los Gobiernos se sientan obligados a tomar estas decisiones porque el claro que no lo van a hacer de manera voluntaria", afirmó.

<iframe id="audio_33421758" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33421758_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
