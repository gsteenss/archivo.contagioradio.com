Title: 300 neoparamilitares transitan por territorios en Chocó sin respuesta del Estado
Date: 2016-12-23 10:25
Category: DDHH, Nacional
Tags: cacarica, cavida, Chocó, neoparamilitares, paramilitares
Slug: 300-neoparamilitares-transitan-por-territorios-en-choco-sin-respuesta-del-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Notimundo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Dic. 2016] 

A través de un comunicado entregado por la Comisión Intereclesial de Justicia y Paz se dio a conocer que **el pasado 22 de Diciembre en los territorios de las comunidades de Balgasira y Bocachica, territorio colectivo de Cacarica en el Chocó fueron observados un grupo de aproximadamente 300 neoparamilitares** de las Autodefensas Gaitanistas de Colombia.

Según la información, **los neoparamilitares portaban armas largas y cortas así como radios de comunicación**. Según los pobladores de la zona, los 300 armados avanzaron hacia la comunidad de Bocachica, Cacarica. Le puede interesar: [Paramilitares hacen presencia en 149 municipios de Colombia: Indepaz](https://archivo.contagioradio.com/paramilitares-hacen-presencia-en-149-municipios-de-colombia-indepaz/)

Razón por la cual, **algunos pobladores temen que como parte de su operación de control social se dirijan hasta la comunidad de San Higinio en dirección a la Zona Humanitaria de Nueva Vida, como lo han estado anunciado desde septiembre de 2015.**

Este presencia neoparamilitar ha sido denuncia por la Comisión Intereclesial de Justicia y Paz y por las comunidades asentadas en esta zona del país desde hace año y medio y según ellas.

Según esta organización “**el problema no ha sido enfrentado por las fuerzas militares a pesar de su conocimiento en el alto nivel del gobierno y a pesar de persistentes reuniones** de seguimiento de medidas cautelares de la Comisión Interamericana de Derechos Humanos que cobijan  los derechos de la población de CAVIDA”. Le puede interesar: [Paramilitares obligan a pobladores a marchar por la Paz](https://archivo.contagioradio.com/paramilitares-obligan-a-marchar-por-la-paz/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
