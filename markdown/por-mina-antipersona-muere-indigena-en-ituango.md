Title: Por mina antipersona muere indígena en Ituango
Date: 2020-09-15 10:42
Author: AdminContagio
Category: Nacional
Tags: indígenas, Mina Antipersona, ONIC, Resguardo de Jaidukamá
Slug: por-mina-antipersona-muere-indigena-en-ituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/indigena-muere-por-mina-antipersona-en-Ituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @ONIC\_Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[La Organización Nacional Indígena de Colombia](https://www.onic.org.co/comunicados-regionales/4030-hombre-embera-eyabida-del-resguardo-jaidukama-cae-en-mina-antipersona-en-el-municipio-ituango-norte-de-antioquia)-[ONIC](https://www.onic.org.co/comunicados-regionales/4030-hombre-embera-eyabida-del-resguardo-jaidukama-cae-en-mina-antipersona-en-el-municipio-ituango-norte-de-antioquia)- denunció este jueves la muerte de Ernesto Jumí, indígena del resguardo de Jaidukamá en Ituango, departamento de Antioquia. **Según la información proporcionada, el indígena de 32 años murió tras sufrir un accidente con mina antipersona. **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El incidente ocurrió el pasado miércoles 9 de septiembre mientras se "dedicaba a labores propias de agricultura indígena" cuando se encontró con la mina antipersona. Ernesto Jumí tenía esposa y un hijo de 8 años de edad. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hasta ayer, se seguía esperando una respuesta por parte del Gobierno para coordinar el levantamiento del cuerpo y la desactivación de más artefactos. (Le puede interesar: [No cesa la barbarie: 55 masacres en 2020](https://archivo.contagioradio.com/no-cesa-la-barbarie-55-masacres-en-2020/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, Jorge Giraldo, secretario de Gobierno de Ituango explicó "estamos coordinando a ver de qué manera se hace la extracción del cadáver, en coordinación con el Ejército Nacional y con la comunidad por instancia del resguardo y del municipio".

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONIC_Colombia/status/1304050457195802625","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONIC\_Colombia/status/1304050457195802625

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

De acuerdo con el comunicado presentado por la ONIC, **con el fallecimiento de Ernesto ya son cinco las personas indígenas que han muerto en Antioquia víctimas de artefactos explosivos, "instalados indiscriminadamente por actores armados al margen de la ley que se disputan el control territorial de los resguardos indígenas del departamento".**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, añaden que la comunidad indígena sigue esperando garantías para la construcción de la paz. Asimismo, hacen un llamado a la institucionalidad regional y nacional ,y a organizaciones defensoras de derechos humanos para que hagan acompañamiento "en especial a las comunidades del municipio de Ituango, Jaidukamá y San Román, quienes fueron incluidas en el Capítulo Étnico, del Acuerdo Final de Paz,como territorios priorizados para desminado humanitario".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
