Title: Ante presión paramilitar, comunidad declara Territorio de Paz en Buenaventura
Date: 2016-09-12 13:05
Category: DDHH, Nacional
Tags: asesinatos en buenaventura, Paramilitares amenazan a comunidad Wounaan, paramilitares en buenaventura, Red CONPAZ
Slug: ante-presion-paramilitar-comunidad-declara-territorio-de-paz-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/La-Esperanza-CONPAZ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CONPAZ ] 

###### [12 Sept 2016] 

El pasado viernes, en el marco de la conmemoración del Día Nacional de los Derechos Humanos, el Consejo Comunitario de la vereda La Esperanza, en Buenaventura, declaró su propiedad colectiva como Territorio de Paz, Humanitario y Ambiental, con el fin de **evitar más daños ambientales y sociales por cuenta de la invasión de la que son víctimas** tras el desplazamiento que se dio en el año 2003.

Según Marina Inestroza, quien completa 36 años viviendo en la vereda y 8 enfrentando la presencia de invasores aliados con Los Urabeños, el Ministerio del Interior permitió que estos invasores se constituyeran como Consejo Comunitario para **vender predios a funcionarios de la alcaldía de Buenaventura por montos que oscilaron entre los \$300 mil y los \$30 millones**, y aprovechar los pagos por cuenta de la construcción de la doble calzada a Cali.

La lideresa asegura que dentro de los invasores tienen identificados a los señores Javier Gamboa Rodríguez, su hermano Andrés Gamboa Rodríguez, Heriberto Riasco y Alberto Cuero, quienes en alianza con [[grupos paramilitares](https://archivo.contagioradio.com/lideresa-doris-valenzuela-es-amenazada-por-paramilitares-en-buenaventura/)] han deforestado para sembrar coca y construir casas, cabañas y restaurantes, cuyos desechos son arrojados en las quebradas. También **han amenazado y atentado contra los legítimos dueños del territorio**, quienes se vieron obligados a desplazarse.

Las familias campesinas cuentan con el acompañamiento de la Comisión Intereclesial de Justicia y Paz, y de la Red de Comunidades Construyendo Paz en los Territorios y declaran la vereda como [[Territorio de Paz](https://archivo.contagioradio.com/espacio-humanitario-puente-nayero-en-buenaventura-cumple-2-anos/)] con el objetivo de que **cesen las agresiones en su contra y que puedan generarse condiciones dignas para quienes no han podido retornar**.

### **RECTIFICACIÓN** 

En esta nota publicada el 12 de septiembre de 2016, manifestamos que la señora Marina Inestrosa había asegurado que el señor Alberto Cuero "en alianza con grupos paramilitares han deforestado para sembrar coca, construir casas..." sin embargo nos permitimos rectificar que la señora Hinestrosa no hizo tal aseveración de manera puntual en la entrevista.

Esta rectificación se hace por solicitud expresa del señor Alberto Cuero quien figura en la presente nota periodística.

En ningún caso nuestro objetivo es desinformar, ni causar daño o riesgo tanto a la lideresa Marina Hinestrosa ni a Alberto Cuero, razón por la cual y **por respeto a todas las personas que nos leen ofrecemos esta rectificación.**

<iframe id="audio_12889890" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12889890_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
