Title: Gobierno debe proteger el cese unilateral de las FARC
Date: 2015-01-28 18:04
Author: CtgAdm
Category: DDHH, Paz
Tags: cese al fuego unilateral FARC-EP, FFMM, Frente Amplio por la PAz
Slug: gobierno-debe-proteger-el-cese-unilateral-de-las-farc
Status: published

###### Foto: ONIC 

#### [Entrevista con Ángela María Robledo:] <iframe src="http://www.ivoox.com/player_ek_4010694_2_1.html?data=lZWekpudeI6ZmKiakpyJd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc%2FbxtHOjbLFtoa3lIqupsaPltDWzcrR0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### 

El **Frente Amplio por la Paz solicita una reunión con el presidente Juan Manuel Santos** para hablar de las recientes declaraciones de la delegación de paz de las FARC, en la que hacen un **S.O.S por la tregua**, y presentar el informe del primer mes de veeduría del cese al fuego, ya que se está afectando por las operaciones militares que se continúan desarrollando por parte de las FFMM.

Ángela María Robledo, representante a la cámara, afirma que **es necesario que se revise el tema del cese bilateral o el desescalamiento** de las hostilidades para garantizar que la tregua se mantenga. Adicionalmente hizo un llamado a la guerrilla de las FARC para que se mantenga en su posición de tregua, e insta a las Fuerzas Militares para que desde su interior se dé trámite al proceso de paz.

Por su parte, la guerrilla de las FARC, hace un llamado de alerta a las unidades guerrilleras y denunció que hay inminencia de combates pues las **FFMM están ocupando sectores estratégicos** con desembarco de tropas.

Adjuntamos el comunicado de la delegación de paz de las **FARC**.

<div>

*"La Habana, Cuba, sede de los diálogos de paz, Enero 27 de 2015*

***SOS por la tregua***

*Alertamos a nuestras fuerzas guerrilleras en todo el país sobre la grave situación*

*Desde el 20 de diciembre sectores guerreristas no han cesado en su empeño de sabotear la tregua unilateral y el proceso de paz, actuando desde la institucionalidad del Estado y desde el comando del ejército en particular.*

</div>

<div>

*En todo el territorio nacional se intensifica la ofensiva militar. El despliegue de tropas acompañado de bombardeos, desembarcos y asaltos, ha ocasionado hasta la fecha, 6 guerrilleros muertos, 6 heridos, 2 capturados, y el también lamentable saldo de 14 militares muertos y 5 heridos.*

*Presidente Santos: en medio de un proceso que busca la reconciliación, es incoherente provocar de esa manera el reinicio del fuego y el ataque a la infraestructura económica del Estado, en lugar de propiciar el silenciamiento de los fusiles.Tal irresponsabilidad ha enrarecido el ambiente, haciendo cada vez más insostenible el cese al fuego unilateral.*

*En el Chocó sus tropas, señor Presidente, están distribuyendo panfletos con las fotos de nuestros voceros de Paz, incitando a la deserción de los guerrilleros con la falacia de que sus comandantes se encuentran en vacaciones en La Habana. En vez de distribuir propaganda sucia, y de perseguir y asesinar líderes populares, hagan algo por generar condiciones favorables a la paz. Nada cuesta responder a la guerrilla con reciprocidad y grandeza.*

*Usted ha rechazado el cese bilateral del fuego aduciendo que la guerrilla utiliza las treguas para fortalecerse política y militarmente, pero lo que estamos viendo es que, es el ejército quien está aprovechando el cese unilateral de nuestras acciones ofensivas, para sacar ventaja militar, como la de patrullar tranquilamente en áreas donde no podía hacerlo, por la presencia de una guerrilla combativa.*

*Al tiempo que alertamos a nuestras fuerzas guerrilleras en todo el país sobre la grave situación, lanzamos un SOS al movimiento social y popular de Colombia, Al Frente Amplio por la Paz, a los pueblos y países amigos, para que defiendan este proceso y exijan el cese de la provocación de sectores guerreristas, que buscan con mezquindad, marchitar la esperanza de paz.*

***DELEGACIÓN DE PAZ DE LAS FARC-EP**"*

</div>
