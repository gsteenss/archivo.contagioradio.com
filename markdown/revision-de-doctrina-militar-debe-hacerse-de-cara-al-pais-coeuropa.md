Title: Revisión de doctrina militar debe hacerse de cara al país: Coeuropa
Date: 2015-09-25 13:22
Category: DDHH, Entrevistas
Tags: Comandante Alberto Mejía, Coordinación Colombia- Europa-Estados Unidos, Desmonte paramilitar, doctrina militar, Foro El ejército del futuro: entre transformación y doctrina para construir la paz, Fuerzas militares, postconflicto, proceso de paz
Slug: revision-de-doctrina-militar-debe-hacerse-de-cara-al-pais-coeuropa
Status: published

###### Foto: ffmm 

<iframe src="http://www.ivoox.com/player_ek_8613517_2_1.html?data=mZuelZqVe46ZmKiak5WJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbqytjWh6iXaaOnz5DRx5DIs8To087bw5DRrc3d1cbfjcnJpsafycbQx9fXqYzYxpDQw9fFb8LgjNWah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alberto Yepes, coordinación Colombia Europa Estados Unidos] 

###### [25 sep 2015] 

Alberto Yepes, integrante de la coordinación Colombia Europa Estados Unidos, indicó que “*esto se percibe como una intención de acomodar las fuerzas armadas al contexto del post conflicto a nuevas tareas*”, respecto del anuncio de la revisión de la Doctrina Militar en el marco del foro: **El Ejército del futuro: entre transformación y doctrina para construir la paz.**

Para este proceso el general Mejía, comandante de las FFMM, indicó que se hará una revisión formal y de toda la doctrina militar, para lo cual se tendrá **apoyo de los Estados Unidos, fuerzas de la OTAN** y fuerzas fundamentales en manejos de normas de conflictos armados.

Además esta voluntad de transformar la doctrina militar tendrá que darse y ver sus efectos con el **desmonte del paramilitarismo**, con el cual el Estado y las fuerzas militares tendrán que tomar acciones, indica Yepes.

Otro hecho preocupante es que “*esto puede ocasionar la intervención de las fuerzas armadas colombianas en conflictos internacionales*”, dice Yepes, sin embargo es necesario que se **abran estas discusiones** con miras al **postconflicto** y qué se puede hacer con un ejercito tan grande como el colombiano, conformado por más de medio millón de personas.

El general Alberto Mejía indicó que el tema se ha tratado desde el 2012, y se espera que en el 2022 termine la elaboración de un **Plan Estratégico para el fortalecimiento de la Educación y la Doctrina**.

Este anuncio se dio en medio del **foro El Ejército del futuro: entre transformación y doctrina para construir la paz**, realizado por la Universidad del Rosario y la Fundación Hans Seidel.
