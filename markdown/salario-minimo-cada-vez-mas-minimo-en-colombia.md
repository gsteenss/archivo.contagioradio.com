Title: Salario mínimo, cada vez más mínimo en Colombia
Date: 2016-12-30 22:12
Category: Economía, Nacional
Tags: CUT, Ministerio de Trabajo, salario minimo
Slug: salario-minimo-cada-vez-mas-minimo-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/salario-mínimo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: archivo] 

###### [30 Dic 2016]

Tanto en 2015 como en 2016, para el llamado aumento del salario mínimo no se tuvo en cuenta el pronóstico de la inflación. A finales de 2015 el incremento del salario real fue de tan solo un punto, en 2016 la situación no será diferente pues se aprobó un incremento del 7%, pero lo que no se menciona es que la inflación rondará el 4.6%, según la firma “Acciones & Valores”, es decir, habrá un ajuste de 2.4% al salario mínimo.

### **Salario mínimo contra la inflación: ¿Aumento o Reajuste?** 

Luego de varias jornadas infructuaosas el gobierno decidió, por decreto, que el aumento del salario mínimo legal vigente, SMLV, para el 2017 será de 737.717. El subsidio de transporte se trazó en 83.149 pesos para un total sumado de 820.857 pesos. ([Lea también: Sin voluntad política no habrá pag](https://archivo.contagioradio.com/sin-voluntad-politica-no-habra-salario-minimo-digno/)o[ digno en Colombia](https://archivo.contagioradio.com/sin-voluntad-politica-no-habra-salario-minimo-digno/))

Según la Central Unitaria de Trabajadores, CUT, los trabajadores del país están sufriendo un doble golpe en este fin de año, puesto que la reforma tributaria golpea de manera directa con un Iva del 19% y el **salario mínimo solamente crecerá en 48.261 pesos**, que comparados con el aumento de impuestos y el incremento del combustible, no representan un cambio positivo en el poder adquisitivo de los colombianos. ([Todo sobre el salario mínimo en Colombia](https://archivo.contagioradio.com/?s=salario+minimo))

### **Las negociaciones del SMLV no han podido dar con un acuerdo en varios años** 

Los representantes de las Centrales Obreras presentaron una propuesta de aumento del 14%, mientras que los gremios empresariales ofrecieron un 6.5%. Para diversos analistas la actitud del gobierno evidencia que sus intereses están más cerca de empresarios que de personas que devengan el SMLV, una poblacion cercana a los 2 millones de personas. ([Le puede interesar. Las razones para levantarse de la negociación según la CUT](https://archivo.contagioradio.com/la-cut-no-va-mas-negociacion-aumento-al-salario-minimo/))

### **Salario mínimo de Colombia entre los más bajos de Latinoamérica** 

Una tabla presentada por Marcha Patriótica ubica a Colombia como el tercer país de America Latina con el salario mas bajo en comparación con la tasa de ingreso Per Capita.

Este análisis ubica a **Colombia solamente después de Nicaragua, México y Brasil con un aproximado de 215 USD**. También ubican a Panamá, Costa Rica y Argentina con los niveles mas altos de salarios mínimos.

![imagen twitter Marcha patriótica](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/salario-mínimo1.jpg){.wp-image-34204 width="454" height="247"}
:   imagen twitter Marcha patriótica

Así las cosas los trabajadores y trabajadoras de Colombia han afirmado que se convocará un gran paro nacional y una jornada de protestas que tendrá un primer momento el próximo martes, con un cacerolazo en contra de la reforma tributaria y en protesta contra el juste del Salario Mínimo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
