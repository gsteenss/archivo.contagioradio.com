Title: La verdad debe ser un bien público, un derecho y un bien ineludible: Francisco De Roux
Date: 2018-11-29 18:17
Author: AdminContagio
Category: Nacional, Paz
Tags: comision de la verdad, Francisco de Roux
Slug: verdad-debe-ser-bien-publico-derecho-bien-ineludible-francisco-de-roux
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/De-Roux-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 29 Nov 2018 

Después de tres meses de preparación, este jueves 29 de noviembre inició la labor de la **Comisión de la Verdad**, que tendrá el reto de recolectar durante los próximos tres años información y testimonios para el esclarecimiento de los hechos de violencia ocurridos en el marco del conflicto armado, y posteriormente, entregarle al país un informe final que recopilará los sucesos más graves acontecidos en casi 60 años de conflicto armado.

Ante más de mil personas que asistieron a Corferias para atestiguar el  comienzo del mandato de la Comisión de la Verdad, su presidente, **Francisco de Roux** fue enfático al aclarar que dicha instancia, la cual trabajará desde las denominadas ‘Casas de la Verdad’ en nueve regiones del país, actuará **"sin sesgos ni intereses políticos”** y que su único propósito es el de cumplir con los postulados de verdad, justicia  y no repetición.

Durante los próximos tres años, **de Roux junto a Ángela Salazar, Saúl Franco, Alfredo Molano, Marta Ruiz, Alejandra Miller, Carlos Ospina, Lucía González, Alejandro Valencia, Carlos Beristain, y Patricia Tobón** serán los once responsables de recorrer el país y hablar con el mayor número de sectores afectados e implicados en el conflicto  para esclarecer qué sucedió, por qué sucedió y por qué se prolongó por tanto años pero sobre todo lograr una transformación en la sociedad para que los crímenes que se cometieron no vuelvan a ocurrir.

**Hablan las víctimas**

Durante el evento también se escucharon las voces de las víctimas y victimarios, que compartieron su testimonio y  reafirmaron su voto de confianza en la Comisión y su compromiso con la paz. La primera en tomar la palabra en representación de las víctimas fue la líder indígena **Aida Quilcue**, quien alzó su voz para **“no queremos seguir contando muertos, no queremos seguir llorando a nuestros amigos y familiares".**

En representación de la comunidad LGBTI, la activista **Darla Cristina González,** víctima de reclutamiento forzoso y de violencia sexual, señaló que las agresiones a su comunidad esperan ser relatas y aseguró que "tener una orientación sexual diversa no puede ser motivo para asesinar, desplazar o amedrentar”.

El Coronel Eduardo Mora, quien denunció ejecuciones extrajudiciales de jóvenes por parte de la Fuerza Pública al interior de su unidad al cual pidió a los presentes cambiar al país, se refirió igualmente en la ceremonia de apertura y aseguró que "para honrar a las víctimas, nuestros hijos no hereden nuestros odios".

[**Los victimarios se comprometen**]

Por su parte, Rodrigo Pérez, excombatiente de las AUC  invitó a los excombatientes de las Farc, presentes en el evento, a trabajar juntos en este nuevo proceso y pidió perdón por todos los actos de violencia que realizó cuando era integrante del grupo paramilitar,   "no nos han alcanzado trece años para arrepentirnos por el dolor generado, por todas las atrocidades que cometimos. Nos comprometemos a aportar todo lo que sea posible para que se cumplan los objetivos de la Comisión”, declaró.

También se pronunció el **excomandante guerrillero Jaime Parra** quien afirmó que el éxito de la paz depende de que el país conozca la verdad para así reparar a las víctimas, un reto histórico con el que ratificó su compromiso. [(Le puede interesar: Archivos del DAS develarían patrones de violencia en el marco del conflicto armado)](https://archivo.contagioradio.com/archivos-del-das-develarian-patrones-de-violencia-en-el-marco-del-conflicto-armado/)

El evento lo clausuró **María Salazar, vocera voluntaria de la Mesa Departamental de Víctimas del Conflicto Armado de Antioquia,** quien además de exaltar el rol de las etnias y las mujeres en la búsqueda de la verdad, aseguró que para conocerla se requiere “**un relato plural de lo que ha sucedido**” algo que solo se logrará con la contribución de todos los colombianos.

###### Reciba toda la información de Contagio Radio en [[su correo]
