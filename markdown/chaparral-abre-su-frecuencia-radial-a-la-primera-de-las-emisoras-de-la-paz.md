Title: Chaparral abre su frecuencia radial a la primera de las emisoras de la paz
Date: 2019-06-25 14:06
Author: CtgAdm
Category: Entrevistas, Paz
Tags: acuerdo de paz, Chaparral, Emisoras Pedagógicas, implementación de los acuerdos, Tolima
Slug: chaparral-abre-su-frecuencia-radial-a-la-primera-de-las-emisoras-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Chaparral.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto:  RadNalCo] 

Tras dos años de trabajo en la implementación del Acuerdo de Paz y tal como se pactó en el punto 6.5 referente a las herramientas de difusión y comunicación, llega a la frecuencia radial de **Chaparral, Tolima en la frecuencia 103.5 FM,** la primera de 20 emisoras enfocadas en la pedagogía de la paz y que tendrán una labor fundamental en las regiones y un claro enfoque territorial.

**Manuel Bolívar director de NC Noticias** y responsable de este proyecto,  indicó que la emisora contará con un equipo de cinco periodistas y un técnico y aunque inicialmente contarán con la programación de Radio Nacional, paulatinamente agregarán contenido a la parrilla.

Inicialmente la 103.5 FM comenzará su transmisión con dos espacios radiales: un informativo nacional y regional de 12:00 m   a 2:00 pm y a parte de las 5:00 pm y hasta las 7:00 pm se contará con una sección territorial denominado **'El Atardecer'.**

### Emisoras desde la región para la región

Para la elaboración de la programación, indicó Manuel, "los contenidos serán elaborados desde la región", con la participación activa de las comunidades, "el respeto por el argumento del otro, la reconciliación  y el perdón son fundamentales", agregó. [(Lea también: Campesinos exigen que vuelva al aire emisora comunitaria en Socorro, Santander)](https://archivo.contagioradio.com/campesinos-exigen-que-vuelva-al-aire-emisora-comunitaria-en-socorro-santander/)

Según Bolívar, su financiación provendrá del Fondo Nacional de Paz tal como fue estipulado en el Acuerdo de La Habana y está planteado que la emisora funcione durante los próximos seis años en tres fases, la primera de consolidación de dos años  y otra de cuatro años **en la que se vincularán equipos de la comunidad y organizaciones de víctimas. **

Finalmente la tercera etapa, será discrecional del Gobierno, que realizará un balance dentro de seis años sobre el funcionamento de las emisoras y el aporte al tejido social que ha hecho al territorio, de ser positivo, se podrá agregar una etapa adicional de 4 años. El próximo 9 de junio se inaugurará la segunda de estas estaciones en Ituango, Antioquia.

<iframe id="audio_37576058" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37576058_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
