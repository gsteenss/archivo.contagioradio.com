Title: Preguntas sobre Paulo VI en Bogotá (1968), Camilo Torres y los campesinos
Date: 2017-10-13 06:00
Category: Opinion
Tags: Apuleyo, camilo torres, Gabriel García Márquez, marquez, Paulo VI
Slug: preguntas-sobre-paulo-vi-en-bogota-1968-camilo-torres-y-los-campesinos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Pablo-VI-en-Bogotá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:uniminutoradio 

#### [Segunda entrega] 

#### [**David Martínez Osorio - Escritor, investigador y consultor**] 

Camilo no estuvo muy conforme con la segunda elección del hogar Márquez Barcha, según nos contó el escritor:

“La madrina fue Susana Linares, la esposa de Germán Vargas, que me había transmitido sus artes de buen periodista y mejor amigo. Camilo era más cercano de Plinio que nosotros, y desde mucho antes, pero no quería aceptarlo como padrino por sus afinidades de entonces con los comunistas, y quizás también por su espíritu burlón que bien podía estropear la solemnidad del sacramento. Susana se comprometió a hacerse cargo de la formación espiritual del niño, y Camilo no encontró o no quiso encontrar otros argumentos para cerrarle el paso al padrino.

“El bautismo se llevó a cabo en la capilla de la clínica Palermo, en la penumbra helada de las seis de la tarde, sin nadie más que los padrinos y yo, y un campesino de ruana y alpargatas que se acercó como levitando para asistir a la ceremonia sin hacerse notar. Cuando Susana llegó con el recién nacido, el padrino incorregible soltó en broma la primera provocación:

“—Vamos a hacer de este niño un gran guerrillero”.

[Apuleyo, Márquez y Torres se habían conocido una docena de años antes en la facultad de derecho de la Universidad Nacional. Algún día de 1947, en el café Asturias, Gabo conoció a Plinio gracias a Camilo y su inseparable amigo Luis Villar Borda. “]*[Me impresionó que a su edad razonaba como un anciano]*[”, fue el testimonio del oriundo de Aracataca, escrito medio siglo después, sobre aquel día en que conoció al hijo del periodista y exministro de Guerra, Plinio Apuleyo Neira. Y como éste, antes de que iniciara el rito religioso, lanzó su anzuelo de nueve palabras en busca de algún pez, la reacción del cura no se hizo esperar, pero la dosificó en tres tiempos, acaso con la finalidad de asegurarse de que el pescador se precipitara al agua y mojara sus sandalias:]

“Camilo, preparando los bártulos del sacramento, contraatacó en el mismo tono: «Sí, pero un guerrillero de Dios». E inició la ceremonia con una decisión del más grueso calibre, inusual por completo en aquellos años:

“—Voy a bautizarlo en español para que los incrédulos entiendan lo que significa este sacramento. Su voz resonaba con un castellano altisonante que yo seguía a través del latín de mis tiernos años de monaguillo en Aracataca. En el momento de la ablución, sin mirar a nadie, Camilo inventó otra fórmula provocadora:

“—Quienes crean que en este momento desciende el Espíritu Santo sobre esta criatura, que se arrodillen.

“Los padrinos y yo permanecimos de pie y quizás un poco incómodos por la marrullería del cura amigo, mientras el niño berreaba bajo la ducha de agua yerta. El único que se arrodilló fue el campesino de alpargatas. El impacto de este episodio se me quedó como uno de los escarmientos severos de mi vida, porque siempre he creído que fue Camilo quien llevó al campesino con toda premeditación para castigarnos con una lección de humildad. O, al menos, de buena educación”.

La importancia de Camilo Torres reside en que se anticipó al CVII, y no sólo en materia litúrgica, en uno de los dos países con las estructuras eclesiales y episcopados más conservadores de Latinoamérica. Paulo VI, quien adelantó tres de las cuatro sesiones del CVII, lo supo; también supo del impacto que aquel sacerdote provocó en la iglesia católica latinoamericana y en algunos sectores católicos europeos con su famosa plataforma del Frente Unido. Las malas lenguas dicen, incluso, que buscó establecer contacto directo con él, pero el cardenal Luis Concha (hijo, como no, del expresidente) se encargó de impedir tal despropósito del pontífice.

[No, no fue gratuito que Bogotá fuera el primer destino latinoamericano de un papa. Tampoco fue gratuita su homilía sobre la necesidad de una reforma agraria en América Latina durante la]*[Santa Misa para los campesinos colombianos]*[, celebrada al pie de la sede de radio Sutatenza, en Mosquera (Cundinamarca).]

[Quizás uno de los mejores relatos de la visita de Paulo VI al país sea el que publicó Alain Gheerbrant (1), en 1969, pues logra captar el espíritu caldeado que se vivía por esos tiempos en la iglesia católica latinoamericana. La figura de Camilo estaba en el centro; tanto que el entonces presidente Lleras Restrepo no pudo abstenerse de aludir a él (2), ni el papa: “]*[No olvidéis que ciertas grandes crisis de la historia habrían podido tener otras orientaciones si las reformas necesarias hubieran prevenido las revoluciones explosivas de la desesperación]*[”.]

¿Y Gabo? Ante una cámara, un poco menos de treinta años antes de publicar sus memorias, dio una posible explicación de lo que escribiría después:

“Lo que más me interesa, en realidad, del mito de Camilo es que es una demostración más, una demostración muy triste, muy dolorosa, de que América Latina no cree sino en héroes muertos. Cosa que sucedió exactamente con el Che Guevara. Es decir, en el momento en que Camilo se sacrificó por lo que estaba defendiendo, muchísima gente que no había creído en él, empezó a creer como diciéndose: ¡Ah, entonces si se hizo matar por eso, entonces decía la verdad, entonces tenía razón!

[“Yo creo que esta experiencia hay que interpretarla es en el sentido de que, por favor, no esperen a que el líder se muera para creer en él. Tenemos que creer en líderes vivos”(3).]

#### [Ver primera parte](https://archivo.contagioradio.com/preguntas-sobre-pablo-vi-en-bogota/) 

------------------------------------------------------------------------

###### [ 1. Hasta 1948, fue el editor de escritores como Bataille, Artuad, Péret y Césaire. Abandonó su trabajo para venir a explorar las cuencas del Amazonas y el Orinoco. Sobre la visita de Paulo VI a Bogotá, véase: Alain Gheerbrant,]*[La iglesia rebelde de América Latina]*[, México, Siglo XXI Ed., 1970 (original: 1969), 319 páginas.] 

###### [2. “Mientras el país se preparaba para recibir la primera visita papal de su historia, el presidente Carlos Lleras Restrepo se creyó obligado a tranquilizar a los colombianos: «En Colombia se goza de libertad, no hay violencia, es un hecho del pasado. Es una leyenda la división entre camilistas y no camilistas. Son mínimos los conflictos ocurridos en Colombia. Podemos decir hoy que hay paz social en Colombia; recibimos con alegría la voz de la Iglesia que predica el cambio social, porque predica lo mismo que nosotros»”. Javier D. Restrepo, “Las huellas de Pablo VI en Colombia”, diario]*[El Tiempo]*[, Bogotá, 24 de agosto de 2017, 3:55 h. (disponible en: www.eltiempo.com/vida/religion/el-mensaje-que-dejo-el-papa-pablo-vi-cuando-visito-colombia-115260).] 

###### [Transcripción de extracto (minuto 50:27 a 51:17) del documental]*[Camilo, el cura guerrillero]*[ de Francisco Norden (Prociner, 1973, disponible en: www.youtube.com/watch?v=EREdTzodSyU).] 
