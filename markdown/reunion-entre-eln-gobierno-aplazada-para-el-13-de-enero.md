Title: Reunión entre ELN-Gobierno aplazada para el 13 de enero
Date: 2017-01-12 08:49
Category: Nacional, Paz
Tags: ELN, Juan Camilo Restrepo, Mesa de conversación de Quito
Slug: reunion-entre-eln-gobierno-aplazada-para-el-13-de-enero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Enero 2017] 

Pese a que el equipo de conversaciones en cabeza de Juan Camilo Restrepo, informó que la reunión se aplazará debido a que miembros  de su delegación no han llegado, **el equipo del ELN ha dejado claro que desde el 10 de enero se encuentra listo para dar inicio** a la reunión con el gobierno.

De acuerdo con Juan Camilo Restrepo, el día de hoy será usado por ambos equipos como día de trabajo preparatorio de lo que será el encuentro de este viernes “**No han acabado de llegar las delegaciones**. Entonces se convino que este jueves **cada delegación trabajaría por separado para trabajo preparatorio**, y el viernes ya llegados todos empieza reunión conjunta”. Por su parte en la cuenta oficial de Ranpal ELN, la guerrilla informó que "el aplazamiento se da por cuestiones de orden logístico" que se espera se resuelvan a la brevedad.

Esta reunión tiene como propósito conversar frente a los diferentes inconvenientes que han surgido luego del anuncio de la fase pública, no obstante el presidente Santos afirmó que “**la liberación de Odín Sánchez sigue siendo fundamental para que la mesa con el ELN pueda tener éxito**”, a lo que ELN ha respondido en diversas ocasiones que esta no era una de las condiciones para iniciar con la fase pública de conversaciones y que por el contrario **se ha convertido en una forma de dilatar el proceso**. Además el ELN también ha señalado **incumplimientos por parte del gobierno al no generar los dos indultos** que hacían parte de los acuerdos para continuar con la instalación de la mesa.

Frente a estos hechos diferentes organizaciones han sido enfáticas en la necesidad de que este paso se dé pronto para tener una paz completa y finalizar el conflicto armado en Colombia. Para ello han exigido a **ambas partes que no tengan inamovibles  y que las conversaciones tengan un carácter más cerrado para que sea mucho más saludable y discreto. **Le puede interesar:["Propuestas de la sociedad civil son claves para destrabar mesa ELN-Gobierno"](https://archivo.contagioradio.com/propuestas-de-la-sociedad-civil-son-claves-para-destrabar-mesa-eln-gobierno/)

Se espera que el día de mañana, desde muy temprano, inicie el encuentro entre ambos equipos de diálogo y se tengan unos primeros acercamientos frente a la continuación del proceso de paz. Le puede interesar: ["ELN reitera su voluntad para continuar proceso de paz con el gobierno"](https://archivo.contagioradio.com/eln-reitera-su-voluntad-para-continuar-el-proceso-de-paz-con-el-gobierno/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
