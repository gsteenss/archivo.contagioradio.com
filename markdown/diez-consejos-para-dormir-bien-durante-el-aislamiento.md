Title: Diez consejos para dormir bien durante el aislamiento
Date: 2020-04-07 20:15
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: Ciudadanos, Covid-19, dormir bien, higiene
Slug: diez-consejos-para-dormir-bien-durante-el-aislamiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/trucos-para-dormir-mejor_b890c86d_1280x720.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":6} -->

###### Por Karla Trujillo

<!-- /wp:heading -->

<!-- wp:paragraph -->

El aislamiento en el que se encuentran ciudadanos y ciudadanas de varios países, entre ellos Colombia, se está convirtiendo en una oportunidad para evaluar nuestras prácticas cotidianas, nuestros hábitos y nuestra manera de relacionarnos con otros y con nosotros mismos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Muchas personas han llegado a afirmar que es necesario un cambio radical en nuestra manera de existir dado que esta situación nos recuerda que somos temporales y muchas de las cosas que creemos esenciales pasan a un segundo plano y aquellas que creíamos poco importantes se convierten en las principales.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Una de ellas es la posibilidad de dormir bien.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Durante la emergencia sanitaria  
los problemas de sueño más comunes son el insomnio e hiperinsomnio que se  
pueden asociar a la sensación de incertidumbre producida por la crisis actual,  
al exceso de información. Sumado a esto, la flexibilización laboral y el uso de  
las tecnologías para relacionarnos, han cambiado nuestra forma de entender y  
consumir la realidad normalizando el consumo de nuestros cuerpos, tiempo e  
identidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### En conclusión, es importante dormir bien, nos ayuda a conservar la energía y a recuperar nuestro cuerpo, sino porque nos aporta a nuestra salud mental reduciendo el estrés.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La sensación permanente de tener que responder con nuestro trabajo, estudio o con nuestras relaciones personales, nos hacen creer que el sueño es un privilegio y no una necesidad, entonces estamos “dispuestos” a cambiar nuestro tiempo de descanso por alguna otra actividad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta situación de confinamiento puede generar ansiedad y estrés que se ven reflejados en mayor dificultad para conciliar el sueño, más despertares por la noche y reducción del número total de horas durmiendo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Porque sabemos la importancia que tiene el sueño para sentirnos bien y cuidar nuestra salud mental, compartimos:

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **10 principios básicos para una buena higiene del sueño**:

<!-- /wp:heading -->

<!-- wp:list {"ordered":true} -->

1.  **Crea un entorno adecuado** para que puedas descansar toda la noche tratando de que las condiciones ambientales no afecten tu sueño. Trata de que sea en un lugar silencioso con un buen colchón y una buena almohada, poco iluminado y reduce el uso de pantallas antes de dormir.  Además, utiliza tu cama solo para dormir, no para jugar videojuegos ni para trabajar en el computador o comer.
2.  **Sigue un ritual para dormir:** Puedes probar con rutinas de relajación, tomarte un baño caliente o leer un libro. Es importante que logres relajarte antes de acostarte. 
3.  **Cuida tu alimentación**, cómo y cuánto comes puede ser un problema a la hora de ir a la cama. Esto incluye tener horarios para las comidas, tu cena no debe ser pesada pero tampoco que te deje con hambre.
4.  **Reduce el consumo de estimulantes** antes de medio día como el café ya que estimula el cerebro e interfiere con el sueño. Puedes consumirlo en la mañana y en cantidades moderadas.
5.  **Realiza actividad física regular**, esto te ayudará a dormir mejor dependiendo la hora del día y tu estado físico. Si haces ejercicio intenso muy cerca de la hora de dormir es probable que se te dificulte conciliar el sueño mientras tu organismo recupera su frecuencia cardíaca normal y vuelve a un estado de reposo que te permita descansar.
6.  **No abuses de la siesta**, descansar 20 o 30 minutos como máximo, después de almuerzo y antes de las 4pm, te puede ayudar a ejercitar la mente. Sin embargo, si tienes problemas de sueño en la noche trata de evitarla.
7.  **Intenta no consumir alcohol antes de dormir**, te puede ayudar a dormir más rápido pero puede producir malestar durante el sueño. Si llevas mucho tiempo tratando de dormir y no lo consigues,
8.  **realiza una actividad que te induzca al sueño** como técnicas de relajación o leer, trata de no quedarte en tu cama porque puede aumentar tu ansiedad.
9.  **Fija una rutina de sueño:** Determina la hora para acostarte y levantarte, permite ajustar tu reloj interno y la calidad del sueño.  
10. **Despeja la cabeza:** Trata de tomarte un tiempo antes de dormir para reflexionar sobre el día, lo que sentiste y lo que tienes que hacer, a la hora de irte a la cama vete con la cabeza tranquila y dispuesta para dormir.

<!-- /wp:list -->

<!-- wp:paragraph -->

Sabemos que a veces es difícil incorporar tantos pasos en nuestras rutinas diarias pero no te preocupes, con tiempo y práctica vas encontrar la forma de prepararte para dormir y tener un buen descanso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También puedes intentar el **método 4-7-8** que es una técnica de respiración para lograr relajar tu cuerpo y tu mente, y te puede ayudar a conciliar el sueño: Inhala durante 4 segundos, mantén la respiración durante 7 segundos y exhala durante 8 segundos. Trata de aplicar las recomendaciones sobre la respiración diafragmática.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Buena noche.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fuente:[psicologia y mente](https://psicologiaymente.com/vida/principios-buena-higiene-sueno)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Vea mas: [nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
