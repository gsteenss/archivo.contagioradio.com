Title: Movimiento indígena caucano rechaza detención de Feliciano Valencia
Date: 2015-09-17 13:21
Category: DDHH, Nacional
Tags: ACIN, Autoridades ind, Captura Feliciano Valencia, constitución de 1991, CRIC, Justicia especial in, Justicia ordinaria en Colombia, Minga Indígena, Movimiento Social Colombiano
Slug: movimiento-indigena-caucano-rechaza-detencion-de-feliciano-valencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/CRIC-40-años-046.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [iniciativascivilesdepaz.blogspot.com] 

<iframe src="http://www.ivoox.com/player_ek_8439417_2_1.html?data=mZmgm5mVe46ZmKiakp2Jd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdDqytLWx9PYs4zdz8mSpZiJhaXbxtPOjcjFucTVz9SY1MrHrMLuwpDRx9nJssTdhqigh6eXsozYxpCzx9HNcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [José Hildo Peque, CRIC] 

###### [17 Sept 2015] 

La captura del líder social Feliciano Valencia, "**representa para las autoridades indígenas un atropello contra el movimiento social colombiano**", afirma José Hildo Peque, Consejero mayor del CRIC.

Velencia fue acusado de secuestro simple y condenado por el Tribunal Superior de Cauca a 16 años de prisión, por el ritual de castigo aplicado al cabo Jairo Danilo Chaparral, quien fue infiltrado en la minga indígena de La María, Cauca, en 2008.

El Consejero mayor del CRIC, asegura que esta determinación de la justicia ordinaria colombiana atenta no sólo contra el movimiento indígena, sino contra **todos los sectores campesinos, afros y urbanos con los que han logrado articular sus demandas y reivindicaciones.**

La **detención de Feliciano Valencia representa un desconocimiento de las garantías constitucionales, aprobadas en las Asamblea Constituyente de 1991,** para la aplicación del sistema de justicia especial indígena, agrega Peque.

De acuerdo con la información del CRIC, Valencia **espera paciente las determinaciones que asuma la justicia ordinaria,** entre tanto la ACIN y el CRIC continúan en asamblea permanente en La María Piendamó, Cauca, para evaluar las acciones que adelantaran, dentro de las que se estima una gran movilización a nivel nacional.

Vea también: [Sectores sociales e indígenas rechazan detención del líder indígena Feliciano Valencia](https://archivo.contagioradio.com/sectores-sociales-e-indigenas-rechazan-detencion-del-lider-indigena-feliciano-valencia/)
