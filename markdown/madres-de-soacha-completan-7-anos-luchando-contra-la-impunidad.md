Title: Madres de Soacha completan 7 años luchando contra la impunidad
Date: 2014-12-10 18:50
Author: CtgAdm
Category: DDHH, Resistencias
Tags: FFMM, impunidad, madres de soacha
Slug: madres-de-soacha-completan-7-anos-luchando-contra-la-impunidad
Status: published

###### Foto: blogs.elpais.com 

Esta mañana Contagioradio.com ha estado en la rueda de prensa ofrecida en el centro de memoria, paz y reconciliación, en la que hemos podido escuchar a las integrantes de las Madres de Soacha a denunciar la excesiva lentitud de sus respectivos casos, en lo referente a toda la parte judicial, demoras, aplazamientos o cambios que al final dejan en total impunidad a las víctimas.

También han realizado un llamado a la solidaridad por la imposibilidad de pagar los gastos derivados de los distintos cementerios donde están albergados los cuerpos. Así como el proceso de uno de sus hijos que aún está en la justicia penal militar. Concluyendo denuncian que todo esto conduce hacia su revictimización.
