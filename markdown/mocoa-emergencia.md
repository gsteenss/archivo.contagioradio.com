Title: Instituciones nacionales se van de Mocoa sin superar la emergencia
Date: 2017-04-19 12:52
Category: DDHH, Nacional
Tags: Atención Psicosocial, Mocoa, Putumayo
Slug: mocoa-emergencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/mocoa.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto: El País] 

###### [19 Abr. 2017] 

Tan solo 19 días después de la tragedia en Mocoa que dejó un saldo de 323 personas muertas y 103 desaparecidas, las **instituciones nacionales anunciaron que se retiran y dejan todo en manos de las autoridades regionales**. Sin embargo la incertidumbre es alta puesto que no se tiene información del nivel de riesgo actual, el proceso de reconstrucción, el acceso a ayudas humanitarias y la atención de emergencia u otras medidas de mediano y largo plazo.

Según **Valentina González del Equipo Directivo Casa Amazonía**, aún no logran entender el porqué de la negativa a recibir más ayudas y donaciones por parte de las instituciones oficiales. Ayudas que han sido fundamentales para sostener y ayudar a las personas de Mocoa luego de la avalancha. Le puede interesar: [Familiares de desaparecidos de Mocoa exigen que continúe la búsqueda](https://archivo.contagioradio.com/familiares-de-desaparecidos-de-mocoa-exigen-que-continue-la-busqueda/)

Aunque la líder de Casa Amazonía reconoce el apoyo de la institucionalidad, dice que las necesidades que persisten son innumerables, desde lo humanitario hasta lo psicosocial “llamamos la atención en qué es lo que viene ahora, **la fase de mediano y largo plazo en donde ya no esta tan claro el acompañamiento**. La gente está en un estado emocional alterado, están en las puertas esperando si viene otra avalancha salir corriendo. No saben qué hacer” asevera González.

De hecho, **la Unidad Nacional para la Gestión del Riesgo y las instituciones del nivel nacional han anunciado que se retirarán de Mocoa este 20 de Abril,** dejando en manos de lo local, es decir, la Alcaldía y la Gobernación la atención. Decisión que hace que las comunidades teman por la capacidad de respuesta que se pueda tener.

### **La reconstrucción de Mocoa.** 

Casa Amazonía ha dicho que hasta el momento **no ha habido una socialización sobre la forma cómo se procederá para la reconstrucción de Mocoa** “si han hecho anuncios como la reubicación de las primeras personas afectadas en un predio de 6 hectáreas, en donde piensan construir apartamentos de 55 mts. cuadrados”.

Este tipo de posibilidades, manifiesta González, desconocen las costumbres de las comunidades de Mocoa, que si bien es un municipio que no es del todo rural, muchas de las personas conservan prácticas de campo **“la mayoría de gente tiene mascotas, tiene gallinas, patos, el patio es fundamental, en un apartamento no sería posible nada de esto”** manifiesta González.

En la actualidad, **en Mocoa hay afectados más de 7 pueblos indígenas**, quienes no están habituados a habitar apartamentos como en las ciudades “eso es un despropósito” dijo González. Le puede interesar: [Un Tango por Mocoa](https://archivo.contagioradio.com/un-tango-por-mocoa/)

De igual modo, en cuanto al tema emocional la intervención que debería hacer el Estado de manera integral no se está llevando a cabo “lo que uno siente estando acá, pues es que hay un sentimiento de adrenalina para estar pendiente de todo lo que llega, del censo, de ir a una oficina, pero más allá de eso **pensar en qué va a ser de la vida, hay mucha desesperanza y mucha tristeza”** afirma González.

Sentimientos que son entendibles pues las pérdidas de vidas humanas, según Medicina Legal, suman 323 personas y las materiales son inmensas **“es fundamental priorizar la atención y el acompañamiento emocional”** reafirma González. Le puede interesar: ["Corrupción también tuvo su parte de responsabilidad en avalancha en Mocoa"](https://archivo.contagioradio.com/corrupciontambienfueculpabledeavalnchademocoa/)

### **Niños y niñas al colegio.** 

A través de las instituciones educativas y desde Secretaria de Educación se ha pretendido realizar una intervención fuerte con los niños, niñas, con los padres y madres y con docentes, para que de esta manera se pueda llegar a toda la comunidad educativa en capacitación en Gestión del Riesgo.

Sin embargo, reitera González “es muy necesario llegar a las zonas rurales y a los barrios donde hay muchas familias, alojando a **otras familias que no quieren enviar a sus niños y niñas al colegio porque tienen miedo.** Es importante llegar a ellas con atención psicosocial y emocional” sostiene González.

### **Situación de los Albergues.** 

“Uno ve la diferencia entre los albergues oficiales y un lugar como la Asociación de Mujeres Indígenas – ASOMI”- dice González, pues según ella,** los albergues institucionales parecen campos de concentración** “donde llegaban familias organizadas en carpas pequeñas y por carpa un número indeterminado de personas independientemente de si eran o no familia. **Conocimos el caso de una mujer que tuvo que dormir en una carpa con 4 hombres que no conocía**”.

Adicionalmente, hay un número reducido de baños en albergues que incluso suman **más de 300 personas, que contaban con 4 baños** y no eran separados para hombres, mujeres, niños y niñas. Le puede interesar: [Persisten denuncias de desvío de ayudas para Mocoa](https://archivo.contagioradio.com/persisten-denuncias-de-desvio-de-ayudas-para-mocoa/)

Así mismo, González dice que la exageración en la presencia de militares en los albergues hace que la situación sea más tensa “están armados como si esto fuera una guerra, si están ayudando, pero esa lógica de la guerra no la sacan de su cabeza y **es entender que en los albergues hay niños, mujeres, víctimas de violencia”**.

 Por otro lado, están lugares que han decidido acoger a damnificados como la  ASOMI en donde se realizan reuniones todo el día, el Taita está continuamente haciendo armonización y las abuelas están haciendo sahumerios. Le puede interesar: [Peligro de epidemias deben ser contrarrestadas con urgencia en Mocoa](https://archivo.contagioradio.com/peligro-de-epidemias-deben-ser-contrarrestados-con-urgencia-en-mocoa/)

“Cosas que son fundamentales para **la espiritualidad y la cohesión de la comunidad y eso ha sido clave para que se mantenga la esperanza**, para que como ellas lo dicen el estar allí no sea un alojamiento temporal sino una posibilidad de reencuentro y de repensarse la vida” sostiene la líder.

<iframe id="audio_18232763" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18232763_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
