Title: Paramilitarismo y ejecuciones extrajudiciales persisten en Colombia
Date: 2015-05-07 18:18
Author: CtgAdm
Category: DDHH, Nacional
Tags: Centro de Investigación y Educación Popular, CINEP, Desplazamiento forzado, ejecuciones extrajudiciale, ELN, falsos positivos, FARC, FFMM, Hidro eléctrica del Quimbo, INPEC, Javier Giraldo, Paramilitarismo, Policía Nacional, SIJIN
Slug: paramilitarismo-y-ejecuciones-extrajudiciales-persisten-en-colombia
Status: published

###### Foto: confidencialcolombia.com 

Según el reciente informe del **Centro de Investigación y Educación Popular, CINEP**, el **paramilitarismo sigue vigente y es una de las principales causas tanto de violaciones a los Derechos Humanos, como de infracciones al Derecho Internacional Humanitario DIH** con 1604 agresiones en total, entre las que se destacan las amenazas y los asesinatos. Por su parte durante el 2014 el CINEP denuncia que por parte de FFMM y Policía **se documentaron 9 casos con 12 víctimas de ejecuciones extrajudiciales.**

El informe resalta que aunque los llamados **“falsos positivos”** han disminuido aún persisten a pesar de la negativa del Estado a reconocerlos y aunque la mayoría de los 5700 casos reconocidos por la propia fiscalía se encuentran en absoluta impunidad. El CINEP resalta que el mayor número de casos se registró **en el periodo 2002-2010 con 698 casos documentados por la red de Bancos de Datos**, y **entre noviembre de 2011 hasta Diciembre de 2014 lograron documentar 240 casos más.**

En cuanto a la ubicación geográfica de las violaciones a DDHH e infracciones al DIH la mayoría de los casos se presentaron **en Bogotá con 345 hechos victimizantes**. Según el padre **Javier Giraldo**, esto es una muestra clara de la presencia del paramilitarismo en la ciudad, que funciona en varias localidades y desde diversas estructuras. En departamentos como Valle del Cauca, Santander y Huila la situación también es preocupante por el alto nivel de denuncias.

El informe también hace un énfasis especial en los conflictos causados por la política minero-energética y hace un muestreo de casos y resalta la situación en el departamento del Huila, en la que señala que las **expropiaciones administrativas** han causado una serie de afectaciones violatorias de los **derechos económicos, sociales y culturales** al provocar el desplazamiento de cerca de 3000 personas y han sido desalojadas por lo menos 700 con el uso de la violencia policial.

En los casos concretos documentados en el marco del DIH, el informe resalta que el **paramilitarismo** es el actor más infractor con 729 hechos, seguido por la **Policía** con 167, en tercer lugar la guerrilla de las **FARC** con 133 hechos documentados y el **ejército** con 132 casos documentados. La guerrilla del **ELN** aparece en el 7mo lugar con 13 casos, seguido por la Fuerza Aérea denunciada 11 veces y el **INPEC** con 7 casos.

A continuación el resumen ejecutivo del informe...

[Informeddhh2014CINEP Contagio Radio](https://es.scribd.com/doc/264567883/Informeddhh2014CINEP-Contagio-Radio "View Informeddhh2014CINEP Contagio Radio on Scribd")

<iframe id="doc_22316" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/264567883/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="70%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
