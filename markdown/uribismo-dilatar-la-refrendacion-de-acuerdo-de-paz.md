Title: Uribismo busca dilatar y quitar tiempo a refrendación: Medina
Date: 2016-11-22 12:28
Category: Nacional, Paz
Slug: uribismo-dilatar-la-refrendacion-de-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/vigilia-por-la-paz-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [22 Nov 2016]

Según el profesor Carlos Medina, integrante del Centro de Pensamiento de la Universidad Nacional, la estrategia que está aplicando el uribismo es para dilatar la refrendación de los acuerdos de paz a los que se llegó con las FARC. Según el catedrático, esta estrateia pretende quitarle tiempo al acuerdo **para que no se alcance a dar el trámite que se necesita para la implementación.**

Además, señala que el comunicado de los voceros del NO es muy diciente en torno a lo que se busca, según Medina es imposible aceptar las modificaciones que ellos exigen puesto que implica la impunidad para los sectores militares cercanos a esas políticas, pero recuerda que, incluso, **varios sectores de militares han respaldado el acuerdo en materia de justicia. **([Le puede interesar: Militares respaldan la Jurisdicción Especial de paz](https://archivo.contagioradio.com/militares-detenidos-respaldan-el-proceso-y-la-jurisdiccion-especial-de-paz/))

**Si se compara la declaración de los voceros del NO con las declaraciones de Humberto De la Calle**, se puede ver claramente que se hicieron cambios sustanciales y que además se incluyeron algunas de las principales exigencias. Además la claridad de la respuesta de De la Calle deja ver que lo que se ha logrado es lo posible y lo que se pude incluir en un acuerdo de paz con una organización guerrillera que no ha sido derrotada militarmente.

En ese mismo sentido se pronunció la representante Ángela Robledo, del Partido Verde, quién, a través de su cuenta de Twitter señalo que los que maquillan el acuerdo son los del NO, “todo lo que hacen es llevarnos a la perpetuidad de la guerra” por ello sería necesaria la refrendación inmediata para que el cese al fuego se pueda asegurar.

### **Combate al paramilitarismo y refrendación de acuerdo de paz** 

Frente a la situación de asesinatos de líderes campesinos y defensores de DDHH el proceso de paz, Medina aseguró que los del NO tendrán que asumir la responsabilidad de las muertes puesto que la no implementación inmediata del acuerdo retrasa la puesta en marcha de mecanismos de protección de las víctimas y de los propios integrantes de las FARC. ([Lea tambien: Extrema derecha debe dar la cara](https://archivo.contagioradio.com/escalda-de-asesinatos-y-atentados-extrema-derecha/))

Declaración de Humberto De la Calle

[Declaración Del Doctor Humberto de La Calle](https://www.scribd.com/document/331965525/Declaracion-Del-Doctor-Humberto-de-La-Calle#from_embed "View Declaración Del Doctor Humberto de La Calle on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_39560" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/331965525/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-MgZYUIbNwUK1ch0hprkr&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

[Comunicado Voceros Del NO](https://www.scribd.com/document/331964997/Comunicado-Voceros-Del-NO#from_embed "View Comunicado Voceros Del NO on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_92210" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/331964997/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-M8nZx4YMQfKcjgaI3aig&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
