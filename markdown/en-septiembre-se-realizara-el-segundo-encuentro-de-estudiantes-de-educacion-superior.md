Title: En septiembre se realizará el segundo encuentro de Estudiantes de Educación Superior
Date: 2018-06-12 15:49
Category: Educación, Movilización
Tags: ENEES, Ministerio de Educación, Universidades Privadas, Universidades Públicas
Slug: en-septiembre-se-realizara-el-segundo-encuentro-de-estudiantes-de-educacion-superior
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/informe-ocde.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [12 Jun 2018] 

A mediados **de septiembre se realizará el segundo Encuentro Nacional de Estudiantes de Educación Superior, ENEES**, en la Universidad de la Amazonía. Uno de los objetivos de este escenario, será convocar a la mayor cantidad de expresiones diversas del movimiento estudiantil para construir una agenda de trabajo y movilización mucho más concreta de cara a exigir el derecho a la Educación en Colombia.

“El objetivo principal será l**a creación de una plataforma nacional que recoja a todas las expresiones del movimiento estudiantil**, para la defensa de los derechos fundamentales y del bien común de la Educación” afirmó Cristian Guzmán, representante de los estudiantes de la Facultad de Ciencias Políticas y Derecho de la Universidad Nacional sede Medellín.

En ese mismo sentido Guzmán señaló que pese a que, realizar el encuentro en la Amazonía exige una gran coordinación y logística desde el estudiantado, hace parte de la descentralización del movimiento estudiantil de las grandes ciudades.

Razón por la cual se realizarán **diferentes actividades hasta septiembre, de organización y construcción de ponencias** que permitan llegar a este espacio con posturas más claras de cara al debate de la defensa del derecho a la Educación superior, desde las universidades públicas y privadas. (Le puede interesar[: "¿Por qué marchan los estudiantes de U. públicas y privadas en Colombia?")](https://archivo.contagioradio.com/por-que-marchan-los-estudiantes-de-u-publicas-y-privadas-en-colombia/)

Así mismo señaló que este espacio tendrá la particularidad de que previo al evento se darán a conocer los documentos base de discusión sobre historia del movimiento estudiantil, políticas públicas de educación y mecanismos de metodologías para el debate.

<iframe id="audio_26497530" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26497530_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
