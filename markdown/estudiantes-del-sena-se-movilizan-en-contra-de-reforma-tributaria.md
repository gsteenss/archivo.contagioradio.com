Title: Estudiantes del SENA se movilizan en contra de Reforma Tributaria
Date: 2016-10-20 12:50
Category: Educación, Nacional
Tags: Movimiento estudiantil Colombia, Reforma tributaria, SENA
Slug: estudiantes-del-sena-se-movilizan-en-contra-de-reforma-tributaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/SENA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sindicato Estudiantil] 

###### [19 de Oct 2016] 

Una movilización de **más de 200 estudiantes del SENA** llegó al Ministerio de Trabajo para exigir que no se apruebe el proyecto de **Reforma Tributaria, que afectaría de forma directa la financiación del SENA**, a su vez le expresaron al presidente Santos que **debe dejar de implementar políticas públicas que aumenten la brecha de pobreza en el país, puesto que esto es otra forma de violencia que no está acorde a la paz que quieren los colombianos.**

Los estudiantes argumentaron que la Reforma Tributaria **“no solamente es lasciva para la sociedad en general, sino que además, pretende eliminar el CREE"** (impuesto sobre la renta para la equidad)  fondo que recolectaba los dineros que financian al SENA, y que de acuerdo con Nicolás Duarte, miembro del Sindicato Estudiantil, le aporta en la actualidad 700 mil millones de pesos.

Con la Reforma Tributaria del año 2012, **se eliminaron los parafiscales que aportaban 2.1 billones de pesos y se estableció el CREE**, sin embargo para Nicolás, la nueva reforma no indica cómo se hará el nuevo recaudo sin el CREE.

Por otro lado los estudiantes señalaron que pese a que respaldan el proceso de paz y los acuerdos de la Habana**, el presidente Santos viene generando una políticas públicas que no están en concordancia con la paz porque generan mayor inequidad en la población**, un claro ejemplo de esto, para ellos, es la política pública 2034 que al finalizar este año lanzará el borrador del nuevo Sistema Nacional de Educación Terciaria. Lea puede interesar: ["Estudiantes colombianos se movilizan contra el acuerdo 2034"](https://archivo.contagioradio.com/estudiantes-colombianos-se-movilizan-contra-el-acuerdo-2034/)

“Este sistema lo que hace es producir más técnicos y tecnólogos sin aumentar la calidad y articular el sector productivo con la educación, lo que significa regionalizar la enseñanza y limitar las posibilidades de estudio de las personas, es decir si en determinada región la mayor producción es caña de azúcar se dará prioridad a las carreras que permitan profundizar está técnica” afirmó Duarte y continuo diciendo **“la educación es Colombia debería ser un derecho pero es un privilegio, acá estudias lo que puedas pagar y no lo que quieras”.**

Las movilizaciones de los estudiantes del SENA continuaran a lo largo de estos meses hasta **diciembre cuando se lance el borrador del Sistema Nacional de Educación Terciaria** y esperan que durante el debate a la reforma Tributaria se solvente las dudas frente a la financiación del SENA y los demás temas que afectan a la sociedad.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
