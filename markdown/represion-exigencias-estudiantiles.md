Title: Gobierno responde con represión a las exigencias estudiantiles
Date: 2018-11-09 18:30
Author: AdminContagio
Category: Educación, Movilización
Tags: educacion, ESMAD, Marchas Estudiantiles, Represión
Slug: represion-exigencias-estudiantiles
Status: published

###### [Foto: @7iGGO] 

###### [9 Nov 2018] 

Durante la movilización estudiantil del 8 de noviembre se presentaron diferentes hechos en los que jóvenes denuncian uso excesivo de la fuerza por parte del Escuadrón Móvil Anti Disturbios (ESMAD), así como en [Bogotá](https://archivo.contagioradio.com/al-menos-30-estudiantes-resultaron-heridos-durante-movilizacion-en-bogota/), **los uniformados actuaron contra los estudiantes en Popayán y Bucaramanga**. Adicionalmente, hay alertas sobre posibles desalojos en centros educativos como la Universidad Distrital (sede Macarena y Calle 40), o la Universidad de Pamplona.

### **ESMAD ataca campamento pacífico en Cauca** 

**Andrés Salazar, integrante de la Comisión de Derechos Humanos de la UNEES** en Cauca, afirmó que el campamento por la educación instalado por estudiantes en el Parque Caldas de Popayán, fue atacado y desalojado por el ESMAD. A pesar de que en la zona había niños y adultos mayores, los uniformados hicieron uso de gases lacrimógenos y bombas aturdidoras, dejando un total de **87 personas heridas.**

Salazar señaló que los efectivos del Escuadrón persiguieron a todas personas que estaban 10 cuadras a la redonda del Parque, convirtiendo la zona en un "campo de batalla". Entre los heridos más graves, se encuentran 3 estudiantes que están siendo atendidos en centros de salud por fracturas en tobillo, clavícula y nariz; adicionalmente, hasta el momento **hay 5 estudiantes de los cuales no se sabe su paradero**.

El estudiante declaró que las razones de la Policía para atacar fueron el supuesto uso de artefactos explosivos contra uniformados en el desarrollo de las movilizaciones estudiantiles, y el consumo de alcohol y sustancias psicoactivas al interior del campamento; sin embargo, aclaró que ninguno de estos hechos eran ciertos, y por el contrario, **lo que se buscaba con el desalojo era poder usar el Parque para otras actividades**.

Para concluir, Salazar sostuvo que un camión recogió algunas de sus pertenencias, incluyendo las carpas,  para trasladar el campamento a la Universidad del Cauca, **donde continua instalado**. (Le puede interesar: ["Fuerza Pública arremete contra comunidad en Popayán"](https://archivo.contagioradio.com/fuerza-publica-arremete-contra-comunidad-en-popayan/))

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F359074944596806%2Fvideos%2F321241565322398%2F&amp;show_text=0&amp;width=267" width="267" height="476" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

 

### **Policía arremete contra estudiantes en la UIS**

**Cristian Moreno, integrante del Comité de Derechos Humanos de la Universidad Industrial de Santander (UIS)**, relató que mientras los estudiantes realizaban una toma pacífica de la Institución en contra la represión de la policía en Cauca, efectivos del ESMAD intervinieron haciendo uso de perdigones, bombas aturdidoras y lacrimógenas. (Le puede interesar: ["Con movilizaciones estudiantes presionarán al Gobierno para negociar"](https://archivo.contagioradio.com/movilizaciones-estudiantes/))

El estudiante manifestó que a pesar de que en el interior de la UIS había defensores de derechos humanos, personas mayores e incluso, miembros de la comunidad internacional, el ESMAD no permitió la salida de ninguna persona del campus hasta las 7 de la noche. Según Moreno, de este ataque se reportaron cerca de **20 personas lesionadas**, y están investigando la posible judicialización de un estudiante en las inmediaciones de la institución.

### **Emiten alertas tempranas sobre intervenciones en algunas instituciones** 

En días anteriores, jóvenes denunciaron que **encapuchados atacaron con papas bomba a integrantes de la comunidad universitaria en la Universidad de Pamplona**, y actualmente, ha circulado información no oficial sobre una posible toma por parte del ESMAD a las instalaciones del Centro Educativo.

Adicionalmente, la Unión Nacional de Estudiantes Universitarios (UNEES) denunció que los campamentos que se adelantan en algunas sedes de esa Institución, han sido amenazados con anuncios de desalojos forzados por parte de miembros de la Fuerza Pública. (Le puede interesar:["Estudiantes detenidos por protestar en la Universidad de Pamplona"](https://archivo.contagioradio.com/estudiantes-universidad-pamplona/))

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FUNEES.COL%2Fposts%2F250161442323119&amp;width=500" width="500" height="737" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
