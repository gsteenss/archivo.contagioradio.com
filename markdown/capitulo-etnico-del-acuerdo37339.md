Title: Delegación de EE.UU insta al Gobierno a cumplir capítulo étnico del acuerdo de paz
Date: 2017-03-07 13:10
Category: Nacional, Paz
Tags: afros, Comisión Ética, Dmócratas, indígenas, ONIC, WOLA
Slug: capitulo-etnico-del-acuerdo37339
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/onic.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

###### [07 Mar. 2017] 

Congresistas y organizaciones de Estados Unidos, se dieron cita en Bogotá para manifestar su respaldo a la inclusión de la Agenda de la Población Étnica de Colombia en los Acuerdos de Paz, pero también para **instar al Gobierno Nacional a cumplir lo pactado y a implementar de manera eficaz lo firmado en La Habana**, para llegar de esta manera a una Paz Completa e Integral.

El capítulo étnico consignado en el punto 6 de los Acuerdos de Paz contempla entre otros temas cómo reconocer las **graves afectaciones de las que han sido víctimas los pueblos étnicos por causa del conflicto,** así como las afectaciones por causa de la exclusión y el despojo. Sin embargo, según los congresistas y las organizaciones, nada de lo pactado en los acuerdos se está cumpliendo.

“Tenemos una preocupación con este tema porque estamos en un tiempo límite del poder que se le da al presidente en el marco del ‘*fast track´* para poder pasar leyes” agregó el Marino Córdoba representante de la Comisión Étnica. Le puede interesar: [Comisión Étnica pide participación en Tribunal para la Paz](https://archivo.contagioradio.com/comision-etnica-pide-participacion-en-tribunal-para-la-paz/)

Así mismo, en una reunión con altos mandatarios de Colombia, la Comisión Étnica acompañada de los congresistas **le han exigido al Gobierno dé respuestas sobre los asesinatos contra líderes sociales que acontece en el país.**

“La gran preocupación es el silencio absoluto del Gobierno nacional frente a esos asesinatos, frente a los actores materiales e intelectuales de los asesinatos. **Si estamos hablando de paz y los actores fundamentales para construir paz vienen siendo asesinados, desplazados, amenazados y no hay garantías de protección, es muy difícil”** relató Marino.

Este respaldo es fruto de un trabajo de incidencia política internacional que comunidades étnicas han realizado desde hace tiempo, en cabeza de varios líderes afros que tuvieron que salir del país para salvaguardar su vida. Le puede interesar: [“Colombia no necesita más víctimas; Necesita líderes comunitarios” Demócratas de EEUU](https://archivo.contagioradio.com/colombia-no-necesita-mas-victimas-necesita-lideres-comunitarios-democratas-de-eeuu/)

**Entre la delegación se encuentran congresistas de la bancada de afroamericanos y demócratas**, integrantes de organizaciones como la Oficina en Washington para Asuntos Latinoaméricanos **WOLA**, la Coalición de Sindicalistas Negros y el  Sindicato United Steelworkers (**Sindicato de Trabajadores de Acero**). Le puede interesar:[ Cinco razones para que EE.UU. apoye la consolidación de la paz según Wola](https://archivo.contagioradio.com/estas-son-5-razones-por-las-que-eeuu-debe-apoyar-a-consolidar-la-paz-segun-wola/)

### **Carta al Presidente Juan Manuel Santos ** 

![Carta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Carta.jpg){.alignnone .size-full .wp-image-37388 width="711" height="955"}

<iframe id="audio_17408596" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17408596_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
