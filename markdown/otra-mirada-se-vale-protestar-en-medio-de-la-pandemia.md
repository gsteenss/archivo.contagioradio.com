Title: Otra Mirada: Se vale protestar en medio de la pandemia
Date: 2020-07-27 22:48
Author: PracticasCR
Category: Nacional
Slug: otra-mirada-se-vale-protestar-en-medio-de-la-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Marcha-por-la-dignidad-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Andrés Zea/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La presencia de grupos legales e ilegales y los actos de violencia contra líderes sociales, defensores de derechos humanos y excombatientes ha aumentado de manera preocupante y sobre todo en departamentos como el Cauca, Antioquia, Putumayo y Bolívar desde la llegada del coronavirus al país. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, colombianos han decidido romper el silencio e iniciar diversas protestas desde diferentes puntos del país, como la Marcha por la Dignidad iniciada el 25 de junio en el Cauca, la Marcha Comunera y la Marcha Libertadora para exigir al Gobierno Nacional, protección en los territorios. (Le puede interesar: [Movilización social sobrevive a la pandemia](https://archivo.contagioradio.com/movilizacion-social-sobrevive-a-la-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los invitados de este programa fueron marchantes como José Daniel Gallego defensor de derechos humanos y estudiante de la UniCauca, Teófilo Acuña, vocero de la Comisión de Interlocución del sur de Bolívar, Lorena Mendoza, fundación de Derechos Humanos Joel Sierra y Erika Prieto, integrante de la Comisión Nacional de Derechos Humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Compartieron qué pasó con la marcha de la dignidad y qué actividades se realizaron los días posteriores en la capital, cuál era el objetivo, y explican por qué están haciendo llamados no solo al gobierno nacional sino también a organismos internacionales. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También, dado que de una u otra manera el ejercicio de protestar se ha visto afectado por la pandemia, explican cuáles han sido esas nuevas formas de encontrarse en la movilización social.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, comentan que lo más importante fue la conciencia de protestar que se logró y recalcan que lograr mantener la articulación y la agenda de movilizaciones es de vital importancia, así sea necesario salir continuamente a las calles hasta que empiecen a escuchar las voces del territorio. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Si desea escuchar el programa del 23 de julio: [Otra Mirada: crisis en el sistema de salud](https://www.facebook.com/contagioradio/videos/412854579652436))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"very-dark-gray"} -->

[Si desea escuchar el análisis completo](https://www.facebook.com/contagioradio/videos/314114349967734)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
