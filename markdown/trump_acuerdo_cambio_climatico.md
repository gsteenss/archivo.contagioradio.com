Title: Trump anuncia que EE.UU no obedecerá acuerdo contra el cambio climático
Date: 2017-06-01 16:33
Category: Ambiente, Voces de la Tierra
Tags: Acuerdo de París, cambio climatico, Donald Trump
Slug: trump_acuerdo_cambio_climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/trump-e1493077662738.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### [1 Jun 2017] 

Pasos hacia atrás continúa dando Estados Unidos en lo que tiene que ver con la protección de  la naturaleza. Donald Trump ha anunciado este jueves que Estados Unidos abandona el acuerdo de París, por medio del cual **las naciones se comprometen a disminuir la contaminación ambiental,** como bandera para enfrentar el cambio climático.

Se  trata de una medida basada en argumentos económicos, y asegurando que con esta decisión se evidencia que para su gobierno está primero Estados Unidos. Desde ya organismos internacionales y otras naciones han criticado el anuncio, teniendo en cuenta que, por un lado, **el país norteamericano [es el segundo más]{.s1}**[** contaminante del mundo,** y además, con el gobierno de]{.s2} Barack Obama, era una nación que se había comprometido a  movilizar 3.000 millones de dólares hacia el Fondo Verde para el Clima de la ONU.

De esta manera, Trump y **no contempla nuevos pagos en su presupuesto para el Fondo Verde,** que entre otras cosas representaba un alivio para países de América Latina afectados por las consecuencias del aumento de las temperaturas, y cuyas situaciones impedían que sus gobierno pudiera dirigir suficientes recursos a la lucha contra el cambio climático.

### Reacciones 

La ONU ya se pronunció sobre las palabras de Donald Trump, a lo que calificó como una "gran decepción" la salida de EE.UU del Acuerdo de París. Por su parte, **Francia, Alemania y Italia emitieron un comunicado conjunto en que aseguran que ese Acuerdo  no puede renegociarse.**

Además, aunque el jefe de Estado señala que se trata de una decisión que favorece la industria y las empresas, el Nobel de Economía Paul Krugman, en su cuenta en twitter rechazó el argumento económico para justificar el abandono del pacto ambiental. A su vez, Brian Krzanich, consejero delegado de Intel, dice que espera que la Casa Blanca encuentre la manera de regresar a hacer parte del acuerdo.

### Desobediencia 

Microsoft afirma que pese a los anuncios de Trump, la compañía seguirá fiel al acuerdo ambiental. Asimismo se ha pronunciado Jeff Immelt, consejero delegado del conglomerado industrial **General Electric, quien en twitter expresó: "La industria debe liderar y no depender del Gobierno". **

El debate aumenta en las ciudades. Por ejemplo el alcalde de Pittsburg, le recordó a Donald Trump que Hillary Clinton se llevó el 80% del apoyo en su ciudad durante las pasadas presidenciales. En esa línea, manifestó que los estados y las ciudades pueden seguir con los objetivos de París, aunque la Casa Blanca se retire.

### Los países que emiten más Co2 a la atmósfera {#los-países-que-emiten-más-co2-a-la-atmósfera .entry-title}

<iframe style="width: 940px; height: 920px; border: 0;" src="http://goo.gl/a88rlh" width="300" height="150" scrolling="no"></iframe>

###### Reciba toda la información de Contagio Radio en **[su correo](http://bit.ly/1nvAO4u) **o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por **[Contagio Radio](http://bit.ly/1ICYhVU).**
