Title: Caso Andino: ¿Qué ha pasado con las víctimas de Falsos positivos judiciales?
Date: 2019-09-11 15:04
Author: CtgAdm
Category: Expreso Libertad
Tags: Atentado andino, Caso de Falsos Positivos, cc andino, Centro comercial andino, Expreso Libertad, falsos positivos
Slug: caso-andino-que-ha-pasado-con-las-victimas-de-falsos-positivos-judiciales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/no-falsos-positivos.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/no-falsos-positivos-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto: Radio Macondo] 

 

En este programa del Expreso Libertad, César Barrera, padre de César, víctima de un montaje judicial en el caso del atentado del Centro Comercial Andino, y el abogado Felipe Caballero, defensor de otras dos personas, denunciaron las irregularidades que se han venido presentando en este proceso. ([le puede interesar: Falsos positivos judiciales contra lideresas sociales](https://archivo.contagioradio.com/falsos-positivos-judiciales-contra-lideresas-sociales/))

Desde acusaciones por parte de la Fiscalía a los abogados defensores, hasta persecuciones e intimidaciones a los familiares, son algunos de los hechos que son denunciados por parte de las víctimas y que de acuerdo con ellos, hacen parte de la presión por parte del Estado para que las diez personas capturadas por estos hecho no salgan en libertad, aunque hasta el momento no se hayan expuesto pruebas que los involucren.

 

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F2336102013385439%2F&amp;width=600&amp;show_text=false&amp;appId=1237871699583688&amp;height=338" width="600" height="338" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
