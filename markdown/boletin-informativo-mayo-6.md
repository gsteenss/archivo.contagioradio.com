Title: BOLETÍN INFORMATIVO MAYO 6
Date: 2015-05-06 18:04
Author: CtgAdm
Category: Uncategorized
Tags: afrodescendientes, Alcaldía de Barrancas, artículo 252, Chancleta, DDHH, El Cerrejón, Estados Unidos, Glifosato, Guajira, Latin American Working Group, Lisa Haugart, narcotrafico, Patilla, Santander, Socorro, Wilman Palmezano
Slug: boletin-informativo-mayo-6
Status: published

*[Noticias del día:]*

<iframe src="http://www.ivoox.com/player_ek_4457769_2_1.html?data=lZmimZyafY6ZmKiakp2Jd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfl5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Comunidades de **Patilla** y **Chancleta** en la **Guajira** apelaron una tutela que ordenó transporte en lugar de reconstrucción de la escuela de 38 niños y niñas de estas comunidades **afrodescendientes**, quienes perdieron su colegio debido a que la empresa **El Cerrejón** y la **Alcaldía de Barrancas**, lo demolieron. Habla **Wilman Palmezano**, del consejo comunitario de negros ancestrales de **Chancleta.**

-En **Estados Unidos** la campaña para prohibir el uso del **glifosato **es permanente. Organizaciones de **DDHH** y de solidaridad con **Colombia** denuncian el uso de este veneno por sus efectos nocivos para la salud y porque ha sido infructuoso en la lucha contra el **narcotráfico**. **Lisa Haugart** directora de de **Latin American Working Group** expone los objetivos de la campaña.

-Este martes fue aprobado en la plenaria del senado el **artículo 252** del **Plan Nacional de Desarrollo**, que promueve la protección animal y que fue propuesto por el senador del **Partido Liberal** **Guillermo García Realpe**, quien asegura que se trata de "un avance histórico que se haya incluido este artículo en pro de la protección animal".

-**Enrique Cortes**, profesor del **Socorro**, **Santander**, camina hacia **Bogotá** como símbolo de apoyo a la **movilización** de **maestros y maestras** en todo el país, que al igual que él, exigen que su pliego de peticiones sea atendido por el gobierno nacional.

-**FECODE** y el **Gobierno colombiano** llegaron a un pre acuerdo para levantar el paro nacional del magisterio que lleva 14 días. Según el informe de **FECODE** el ajuste salarial será de 12% y se establecen comisiones para la revisión del sistema de salud. **Tarsicio Mora** encargado de **DDHH** de **FECODE** habla en Otra Mirada.

-Crisis humanitaria de inmigrantes en las costas europeas se agrava tras la decisión de cerrar los puntos de entrada de población. Según los últimos reportes de la guardia costera italiana y **Médicos sin Fronteras** en esta semana han sido rescatados cerca de 7000 inmigrantes pero hay una cifra aún desconocida de personas retenidas en las fronteras. **Carlos Soledades** integrante de la campaña contra los centros de internamiento de extranjeros.

-Maestros del país se han mostrado inconformes con el pre acuerdo entre **FECODE** y el gobierno nacional. El ajuste salarial del 12% en 4 años es insuficiente y no hay garantías de cumplimiento reales. **William Agudelo** de la **Asociación Colombiana de Educadores.**
