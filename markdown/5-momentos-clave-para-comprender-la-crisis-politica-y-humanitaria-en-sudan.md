Title: 5 momentos clave para comprender la crisis política y humanitaria en Sudán
Date: 2019-06-19 10:10
Author: CtgAdm
Category: El mundo
Tags: Golpe de Estado, Omar Al Bashir, Sudán
Slug: 5-momentos-clave-para-comprender-la-crisis-politica-y-humanitaria-en-sudan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-18-at-8.48.22-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Asociated Press 

**Del gobierno de Al Bashir a la protesta nacional. Diciembre de 2018**

La protesta no violenta en Sudán comienza en diciembre de 2018 con la esperanza de **abrir una ventana al cambio democrático**. Los ciudadanos de Jartum salen a las calles para buscar la renuncia del presidente **Omar al Bashir** que detenta el poder desde 1989.

La mala gestión de las finanzas públicas ha provocado en los últimos años una inflación latente que se ha vuelto insostenible, la escasez de alimentos debido a la reducción de los subsidios para la compra de artículos de primera necesidad y la falta de combustible también son problemas que afectan terriblemente el país. **La movilización que se genera es gestada inicialmente por el Partido de la Nación, la principal agrupación de la oposición, y por los estudiantes del país sobre el modelo de la primavera árabe**, precursor de la lucha democrática en el Medio Oriente.

Desde el inicio de las movilizaciones resultaron las primeras víctimas involucradas, incluidos menores, al caer en manos del ejército de Al Bashir. Los ciudadanos **exigieron al presidente ceder el paso a un gobierno técnico adecuado legitimado por la sociedad sudanesa.** Omar Al Bashir llegó al poder a manos de un golpe de estado, tiene dos órdenes de arresto de parte de la Corte Penal Internacional por crímenes de guerra y ayuda e instigación en represalia del "Genocidio de Darfur" entre minorías étnicas que causó más de 300 mil víctimas en 2003.

**El golpe y la detención del presidente. Abril de 2019**

El jefe del ejército anunció **en Abril la detención del presidente Omar Al Bashir.** Este último, habiendo despedido al gobierno federal y bloqueado las enmiendas a la constitución que le habrían permitido ingresar a la contienda presidencial de 2020, había declarado el estado de emergencia nacional. De acuerdo con un informe de Human Rights Watch, **las protestas que dieron lugar al golpe de estado, iniciado en diciembre dejaron un saldo de 52 víctimas.**

La facción líder anuncia un consejo de transición militar por un período de dos años con el objetivo de preparar nuevas elecciones democráticas. Mientras tanto **la liberación de todos los presos políticos, la suspensión de la constitución actual y el estado de emergencia** que prevé un toque de queda para que los ciudadanos estén predispuestos.

**El consejo militar obstaculiza el Estado democrático. Mayo 2019**

Seguido de las protestas apoyadas por las fuerzas de oposición no se consigue aceptar la autoridad del Consejo Militar, liderada por el **ex ministro de Defensa Awad Ibn Auf**, quien renuncia una semana después de tomar las riendas del gobierno y designa al Jefe de Estado Mayor **Abdel Fattah Abdelrahman Burhan** como su sucesor; una figura que algo irrelevante para la escena política sudanesa. **Salah Abdallah Gosh**, del Servicio Nacional de Inteligencia y Seguridad (Niss), también presenta su renuncia.

La estructura gubernamental transitoria es en ese punto cada vez más débil, pero al mismo tiempo violenta con aquellos que expresan su disidencia. En la nación, **los toques de queda se violan sistemáticamente y se invita a las personas a salir a las calles para evitar la instalación de una forma dictatorial**.

La Asociación de Profesionales de Sudán, principal promotora de las movilizaciones, **clama por la transición a un gobierno civil**, aceptando el compromiso de mantener el Ministerio del Interior y Defensa bajo la dirección del ejército. La Unión Africana también **pide a las fuerzas militares un gobierno democrático** y firmeza en la implementación de los mecanismos legislativos necesarios para la transición.

**Violaciones del gobierno de transición a los derechos humanos. Mayo 2019**

El punto muerto entre la Asociación de Profesionales de Sudán y la instalación militar de transición parece permanecer, tanto que los dos interrumpen el diálogo hasta el 15 de mayo, fecha en la que parece haber un acuerdo. En los mismos días, el ex presidente Omar Al Bashir es acusado de ser el instigador de los asesinatos durante las protestas que llevaron al final de su régimen.

**El acuerdo entre el ejército y la oposición prevé la formación de un consejo de presidencia**, un parlamento de 300 escaños, dos tercios de los cuales deben confiarse a las fuerzas que lideraron la protesta. Sin embargo, l**as manifestaciones  alcanzan un nivel dramático y los asesinatos a manos del ejército continúan perpetrándose** a pesar del anuncio por parte de Abdel Fattah Abdelrahman Burhan de querer reducir el período de transición a nueve meses, una noticia que no parece tener la credibilidad esperada.

La ONU también está dividida sobre el destino del país: China y Rusia vetan la resolución presentada por el Reino Unido y Alemania que condenaría al Consejo Militar por las muertes de las que sería responsable. **Dos meses después del despido del Presidente, Sudán está experimentando momentos de violencia y tensión muy graves** y un bloqueo a los medios de comunicación que impide la difusión de información,tanto para llevar a la suspensión del país de la Unión Africana por parte de los estados miembros.

**El destino de Sudán: un estado a la deriva. Junio ​​2019**

Según las fuentes médicas de The Guardian, las milicias pertenecientes a lo que se convirtió en el jefe “de facto” del estado Mohamed Hamdan diputado a Abdel Fattah Abdelrahaman, habrían reprimido la manifestación a favor de la democracia en Sudán cometiendo también 70 violaciones sexuales, una cifra que podría ser indicativa para muchas personas que por temor no hayan informado los hechos.

Es difícil asegurar qué ocurrirá con el futuro de Sudán que hoy lamenta a 118 víctimas que cayeron durante las protestas. Algunos analistas afirman que uno de los países más grandes y más pobres de África corre el riesgo de terminar como en Libia, donde el fin de la era de Muammar Gaddafi fue el comienzo del caos. El pensamiento más optimista, cree que la revolución llevará a resultados similares a la situación de Sudáfrica después del fin del apartheid, lo que dependerá de  negociaciones complejas pero exitosas entre las partes involucradas.

###### Reciba toda la información de Contagio Radio en [[su correo]
