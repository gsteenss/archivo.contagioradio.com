Title: Tras 16 años de lucha las Comunidades negras del Naya reciben título colectivo
Date: 2015-11-27 11:58
Category: Comunidad, DDHH
Tags: Consejo Comunitario del Río Naya, INCODER, Masacre del Naya, Rodrigo Castillo, Titulación colectiva Río Naya
Slug: tras-16-anos-de-lucha-las-comunidades-negras-del-naya-reciben-titulo-colectivo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/IMG-20151127-WA0000-e1448643456368.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Justicia y Paz] 

##### <iframe src="http://www.ivoox.com/player_ek_9528588_2_1.html?data=mpqfmpqcfI6ZmKiakp2Jd6KnkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNPV1JCemJDFaaSnhqee0diPqMafzdrQysaPsMLnjKjcz9rSrcXVxcrgjdPJq9PV1JDRx9GPksLtwpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Rodrigo Castillo,  Consejo Comunitario del Río Naya] 

##### 27 Nov 2015

La decisión tomada por el Incoder y el Gobierno Nacional de otorgar 177.817 hectáreas de terrenos a las poblaciones negras e indígenas del Naya, ha sido bien recibida. Sin embargo, desde las comunidades se insiste en la necesidad de recibir garantías por parte del Estado para que se acate este fallo y se respete la dignidad de los habitantes, para asi continuar realizando el ejercicio de pertenencia y permanencia en el territorio.

La batalla jurídica que se libró por la titulación de las tierras que limitan con los municipios de Buenaventura y López de Micay en la Cuenca del Río Naya entre las comunidades que viven en el territorio, el Estado y la Universidad del Cauca, duró más de 16 años. La extensión del territorio que se encuentra ubicado en el suroccidente de Colombia, supera la extensión de la zona rural y urbana de Bogotá; que se estima, tiene un área de 153.659 hectáreas.

Rodrigo Castillo, representante legal del Consejo Comunitario del Río Naya, expone que las comunidades también requieren un acompañamiento económico e institucional de parte del Estado: “seguimos exigiendo reales garantías de parte del Gobierno Nacional para que las comunidades podamos desarrollar nuestra forma de vida, porque no es meramente el reconocimiento jurídico del Estado el que necesitamos, sino su apoyo para poder hacer los proyectos que proponemos”.

El título de las tierras pertenece ahora a las comunidades negras e indígenas de la zona que históricamente han habitado el lugar y que de acuerdo a Rodrigo Castillo, “han venido habitando desde hace más de 335 años”.

Castillo expone que uno de los primeros planes que tienen las comunidades, “es seguir preservando este territorio biodiverso que ha pasado por la comunidad de generación en generación”, afirma el líder comunitario. El segundo paso consiste en que los Nayeros desarrollen propuestas productivas que garanticen la estabilidad y el bienestar de las comunidades en el lugar.

La zona ha sido uno de los más golpeados por el conflicto armado en el país. En el 2001, grupos paramilitares perpetuaron una de las masacres más cruentas de las que se tiene registro: ‘La Masacre del Naya”, en donde se estima que más de 200 personas fueron víctimas de asesinatos y torturas, a pesar de las advertencias que los habitantes dieron a las autoridades y al Gobierno.

Este 27 de noviembre, el Ministerio de Agricultura y el Incoder hace entrega de la Resolución 6640 del 19 de noviembre de 2015 a las comunidades negras del Naya en el Bulevar de la ciudad de Buenaventura a las 11 de la mañana, en donde se reconoce en términos jurídicos a las comunidades de este territorio como propietarias de las tierras.
