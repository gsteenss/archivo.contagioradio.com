Title: "Hasta siempre comandante" una canción de todo el mundo para el Che
Date: 2015-10-09 15:49
Category: Cultura, El mundo
Tags: Boikot, Buean vista social club, Carlos Puebla, Che guevara, Cuba, Derechos Humanos, Música, Revolucion, Sargento Gacía, Soledad Bravo
Slug: hasta-siempre-comandante-una-cancion-de-todo-el-mundo-para-el-che
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Che-Guevara1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: mirror.co.uk 

**"Hasta Siempre Comandante**" es una  canción compuesta por el compositor cubano Carlos Puebla. La canción fue escrita antes de morir el Che, el 3 de octubre de 1965, día en que Fidel dio a conocer la carta de despedida del Che, esa misma noche Puebla ingreso en su estudio y no salio de allí hasta que compuso la que hasta hoy es la canción más famosa dedicada al Che Guevara y de la que hay más 200 versiones, interpretada en los más variados géneros musicales y en otros idiomas.

**Carlos Puebla:** es conocido como «el cantor de la revolución», por utilizar sus habilidades musicales para la difusión de los valores de la revolución cubana en el año 1959.

https://www.youtube.com/watch?v=I-064cEZfK8

**Buena Vista Social Club:** En** **el álbum que lleva por título el mismo nombre de la agrupación &lt;&lt;Buena Vista Social Club&gt;&gt;, grabado en marzo de 1996 y publicado el 16 de septiembre de 1997, Compay Segundo, Ibrahim Ferrer, Eliades Ochoa, Omara Portuondo entre otros, incluyeron una versión de "Hasta Siempre Comandante".

https://www.youtube.com/watch?v=JcPm5Rn36Kw  
 

**Soledad Bravo:** La cantante Venezolana de origen español quien incursionó en la salsa, además de la canción folklórica y de protesta donde alcanzó gran popularidad y sirvió de vehículo  para dar a conocer a los compositores más representativos de la llamada “Nueva Trova Cubana” y de la «Nueva canción latinoamericana».

Bravo incluyó en su álbum "Cantos revolucionarios de América Latina" del año 1997, una versión de "Hasta siempre comandante".

https://www.youtube.com/watch?v=c3Cec-bzj0Y  
 

**Oscar Chávez:** El cantante, actor y compositor mexicano, considerado el máximo exponente de la nueva trova mexicana, conocido por sus canciones de protesta contra el gobierno, hizo también un homenaje al Comandante Che Guevara con su canción "Hasta siempre comandante" en el álbum "Concierto de bellas artes" del año 1974.

https://www.youtube.com/watch?v=LLdmOGTsAcs  
 

**Silvio Rodríguez:**  El cantautor, guitarrista y poeta cubano, exponente de la Trova Cubana surgida con la Revolución, también rindió un homenaje al Che Guevara con "Hasta siempre Comandante".

https://www.youtube.com/watch?v=br1VJEN5-x4  
 

**Sargento García:** El músico francés  Bruno García interprete de música fusión hip-hop, ritmos latinos, salsa, reggae y raggamuffin, también hizo una versión con su estilo característico sobre esta canción del compositor cubano.

https://www.youtube.com/watch?v=K2LjtekqRrA  
 

**Boikot**: Esta banda de punk español originaria de Madrid caracterizada por su compromiso social y el mensaje político en sus canciones, también hicieron una versión de "Hasta Siempre Comandante" en su álbum" La ruta del Ché: no escuchar" del año 1997.

<div class="mod" data-md="1001" data-ved="0CCcQkCkoBDAEahUKEwiBvvO607XIAhUDlx4KHWR8CGw">

</div>

https://www.youtube.com/watch?v=8vvYfUpznDA  
 
