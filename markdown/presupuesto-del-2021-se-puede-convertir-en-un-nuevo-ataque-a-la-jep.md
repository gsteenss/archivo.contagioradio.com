Title: Presupuesto del 2021 se puede convertir en un nuevo ataque a la JEP
Date: 2020-10-20 21:53
Author: CtgAdm
Category: Actualidad, Economía
Tags: JEP, paz
Slug: presupuesto-del-2021-se-puede-convertir-en-un-nuevo-ataque-a-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/JEP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 19 de octubre el Congreso de la República aprobó el presupuesto general de la nación para el 2021, con un rubro de \$313,9 billones de pesos, de los cuales **cerca de 20.000 millones serán destinados para proteger a las víctimas del conflicto armado.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/lopezjosedaniel/status/1318663281674981376","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/lopezjosedaniel/status/1318663281674981376

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Un valor que según analistas **representa un fuerte golpe para la Jurisdicción Especial para la Paz, y más aún cuando esta el pasado 13 julio solicitará al Gobierno Nacional** [**recursos adicionales**](https://www.jep.gov.co/Sala-de-Prensa/Paginas/JEP,-preocupada-por-no-aprobaci%C3%B3n--de-recursos-adicionales-en-presupuesto-.aspx)de inversión, reconociendo que era necesario un aumento del 20% de los recursos de inversión que necesitaba para su operación durante el 2021.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según esto la JEP indicó, qué los recursos adicionales de inversión que no fueron aprobados eran de **un total de \$30.014 billones, valor indispensable para el cumplimiento de las obligaciones constitucionales** y legales de la Jurisdicción. ([Duras críticas generó aprobación del Presupuesto de la Nación para 2021](https://archivo.contagioradio.com/duras-criticas-genero-aprobacion-del-presupuesto-de-la-nacion-para-2021/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Monto económico que se hubiese dividido en; **\$19.613 millones para el programa de protección de víctimas, testigos e intervinientes** a cargo de la Unidad de Investigación y Acusación. ([Tras la verdad, hay un sector que no quiere perder el poder del control ideológico](https://archivo.contagioradio.com/sector-teme-verdad-no-quiere-perder-poder-control-ideologico/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo que, **\$4.100 millones para la representación y atención a víctimas en los procesos judiciales,** esto teniendo en cuenta que *"se amplió el plazo para la presentación de informes a la JEP y que aún hay macro casos en curso que demandan la participación de las víctimas"*, señalan en un comunicado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo un problema develado en medio de la pandemia y tiene que ver con el acceso a las nuevas tecnologías en relación a *"una justicia más digital, transparente y ágil que se pueda poner al servicio de las víctimas"*, l**a JEP destinaría \$6.300 millones de pesos para tecnologías de la información y comunicación** en los procesos de los intervinientes y la sociedad en general. [Retos para una educación digna en tiempos de pandemia](https://archivo.contagioradio.com/retos-para-una-educacion-digna-en-tiempos-de-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego de presentar esta explicación detallada del uso del presupuesto adicional solicitado, el 21 de agosto fecha en la que ya se encontraba radicado el proyecto de presupuesto, reiteraron esta solicitud y la repitieron el 28 de agosto y el 7 de octubre.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente la Jurisdicción destacó que desde un inicio no le fueron asignados los recursos solicitados en el anteproyecto de presupuesto, en donde se evidenció una diferencia de \$88,000 millones entre lo solicitado y lo asignado.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
