Title: 17 líderes sociales han sido asesinados este año en Antioquia
Date: 2018-07-09 11:11
Category: DDHH, Nacional
Tags: Antioquia, Asesinato a líderes sociales, Fiscalía, lideres sociales
Slug: 17-lideres-sociales-han-sido-asesinados-este-ano-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/WhatsApp-Image-2018-07-09-at-11.09.50-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [09 Jul 2018] 

El nodo Antioquia de la **Coordinación Colombia Europa Estados Unidos** informó sobre el de homicidio José Fernando Jaramillo Oquendo, miembro de la Junta de Acción Comunal de Pascuita, en el municipio de Ituango,  Antioquia, y quien además era familiar del presidente de esa organización social, José Abraham García, asesinado hace doce días en la misma zona. **Con Oquendo son 17 los líderes asesinados en ese departamento, durante el primer semestre del año.**

De acuerdo con Oscar Zapata, integrante de COEUROPA, los hechos se registraron el pasado sábado 7 de julio, cuando José Fernando Jaramillo se encontraba en su vivienda, hombres armados llegaron y le propinaron varios disparos. Familiares que se encontraban en el lugar, afirmaron que las personas se identificaron **como integrantes de las Autodefensas Gaitanistas de Colombia.**

Jaramillo era familiar de José Abraham García, asesinado el pasado 26 de junio. Ambos líderes sociales se destacaron por apoyar la sustitución de cultivos de uso ilícito, por lo que según Zapata, “se volvieron sujetos incómodos para los intereses de los actores que se han denominado como paramilitares, que han copado todo el territorio” donde antes estaban las FARC.

### **Antioquia uno de los departamentos en donde más se amenaza la vida de líderes** 

De acuerdo con COEUROPA, **de 2010 a 2018, han sido asesinados 180 defensores de derechos humanos,** 17 de ellos en lo corrido del primer semestre de este año, “nos preocupa esta situación, porque en algunos lugares las personas tienen miedo de ejercer alguna función e liderazgo y eso es realmente vergonzoso para la democracia” afirmó Zapata.

Frente a los avances de la Fiscalía sobre las investigaciones de estos hechos de violencia, COEUROPA estableció que **de los 180 asesinatos, en el 14% se ha esclarecido información y en algunos casos se han impuesto sanciones sobre los autores materiales.** Sin embargo, el mayor temor es que no hay avances sobre los autores intelectuales ni los móviles de los asesinatos. (Le puede interesar:["¿Hay sistematicidad en asesinato a líderes sociales en el país?"](https://archivo.contagioradio.com/hay-sistematicidad-en-asesinatos-a-lideres-sociales-en-colombia/))

“Se podría estar hablando que la impunidad sobre el asesinato a líderes en Antioquia puede estar sobre el 100%, aunque las autoridades hablen de una cifra más pequeña” expresó Zapata, **agregando que de 125 municipios que tiene el departamento, en 123 se ha denunciado la presencia de grupos armados**.

<iframe id="audio_26967060" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26967060_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
