Title: Incumplimientos al Acuerdo de paz ¿Están fragmentando al partido FARC?
Date: 2019-05-23 17:30
Author: CtgAdm
Category: Nacional, Política
Tags: etcr, FARC, Iván Márquez, JEP, Rodrigo Londoño
Slug: incumplimientos-al-acuerdo-de-paz-estan-fragmentando-al-partido-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Partido-Farc.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: webinfomil] 

Aunque las falencias en la implementación del Acuerdo de paz son evidentes en los **Espacios Territoriales de Concentración y Reincorporación (ETCR)** donde el proceso para algunos excombatientes que buscan alternativas de vida con proyectos de emprendimiento ha estado llena de dificultades e incertidumbre en cuanto a su financión, es evidente la necesidad de que el partido Fuerza Alternativa Revolucionaria del Común (FARC) se muestre unido ante la opinión pública pese a sus diferencias.

Para el profesor Carlos Medina Gallego, es desacertado el hecho de que tanto **Iván Márquez como Rodrigo Londoño** hayan escrito cartas públicas que parecieran opuestas, pone en evidencia una controversia al interior de un partido político, lo que es natural,  lo que no es natural es que las cartas sean dirigidas a la opinión pública y que estas diferencias no sean resueltas al interior de ese partido, como si lo hacen otros, lo que demostraría una solidez que resultaría positiva para el Acuerdo de paz y la democracia

### Iván Márquez no se apartó del Acuerdo de Paz 

Para el docente de la Universidad Nacional, el jefe negociador del Acuerdo de la Habana no se ha apartado del Acuerdo a pesar de haber afirmado que "entregar las armas fue un error", **lo que está haciendo es "un cuestionamiento a su implementación"**, esto con el fin de dar voz a los excombatientes que se han manifestado en contra del incumplimiento del Estado al que calificó de "tramposo" pues se trata de un Gobierno, que con un nuevo enfoque político "ha hecho a un lado el acuerdo".

Medina hace énfasis en que este cuestionamiento a la forma en que se hizo la entrega de las armas se venía dando desde la Décima Conferencia Guerrillera, junto a la "obstinación del Gobierno" ha generado en Márquez una situación de inseguridad física y judicial que le impide ponerse a disposición de la justicia.

> Tenemos una responsabilidad con todas las gentes del común que creen en la paz, es nuestro compromiso avanzar por un país diferente y jamas volver a la guerra por las generaciones futuras [\#NosUneLaPaz](https://twitter.com/hashtag/NosUneLaPaz?src=hash&ref_src=twsrc%5Etfw)
>
> — Rodrigo Londoño (@TimoFARC) [23 de mayo de 2019](https://twitter.com/TimoFARC/status/1131519650624299008?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Por su parte las declaraciones de Londoño quien llama al partido a tomar distancia del grupo que está articulado entorno a Márquez, resultan equivocadas para el investigador pues "da casi por hecho que las sindicaciones que se le hacen al exnegociador en relación con su sobrino y el caso Santrich son ciertas, prácticamente marginándolo del proceso".

### FARC No se trata solo de Márquez y Londoño

Lo cierto es que la decisión entorno al caso Santrich y la crisis política que ha asumido al Gobierno **ha dejado en inseguridad física y judicial a cerca de los 8.000 excombatientes que están vinculados a los programas de reincorporación**, según cifras otorgadas por el senador de esta bancada Carlos Lozada. Inseguridad aún mayor para los otros **3.000 excombatientes** quienes decidieron iniciar sus proyectos de vida de manera individual, pues desde la firma del tratado han sido asesinado 135 excombatientes.

Para el profesor  existen varios puntos claves para avanzar en esta cuestión resaltando la importancia de consolidar a la JEP  como el tribunal supremo encargado de hacer justicia a las víctimas y poder ofrecer garantías jurídicas a quienes sean juzgados.  [(Lea también: Asesinan a Wilson Saavedra ex comandante de FARC que aportó a la paz)](https://archivo.contagioradio.com/asesinan-a-wilson-saavedra-ex-comandante-de-farc-que-aporto-a-la-paz/)

En segundo lugar, indicó que al caso de Santrich debe dársele una salida pronta y correcta, "la extradición no puede ser un mecanismo arbitrario desde un enfoque político", afirma refiriéndose a un proceso en el que la aparición de pruebas después de dado el veredicto de la JEP no garantiza su transparencia. Finalmente manifestó la importancia de "parar ese proceso de enemistades que no permiten construir un cuerpo cohesionado" al interior de FARC.

<iframe id="audio_36281784" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36281784_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
