Title: 680 hospitales públicos están en riesgo liquidación por parte del Ministerio de Hacienda
Date: 2015-04-22 16:26
Author: CtgAdm
Category: DDHH, Nacional
Tags: ANIR, Anthoc, Caprecoom, Derecho a la salud en colombia, Entidades Promotoras de Salud, EPS's, Hospitales públicos, Ley estatutaria de la salud, Mesa Nacional de la Salud, Ministerio de hacienda, Nueva EPS, Plan Nacional de Desarrollo 2014 - 2018, Plan Obligatorio de Salud, SaludCoop, Yesid Ocampo
Slug: 680-hospitales-publicos-estan-en-riesgo-liquidacion-por-parte-del-ministerio-de-hacienda
Status: published

###### Foto: modep.com 

Según la **Asociación Nacional de Trabajadores Hospitalarios ANTHOC,** el gobierno, a través del Plan Nacional de Desarrollo está destinado a acabar con los hospitales públicos, puesto que la figura de las “cuentas maestras” manejaría los recursos de liquidación de aquellas instituciones que están en alto o mediano riesgo financiero. En el año **2000 Colombia tenía 1380, en 2015 solo hay 948 de los cuales 680 están en mediano y alto riesgo financiero.**

El gobierno no ha hecho nada por que las **Entidades Promotoras de Salud** paguen las deudas a los hospitales, puesto que varias de las que más le adeudan al sistema son entidades afines al gobierno o que están intervenidas por el gobierno como **SaludCoop, Nueva EPS, Caprecoom** entre otras, denuncia Ocampo. Además afirma que en el PND se contempla la re-capitalización durante 7 años, es decir, 7 años en que no se debe pagar la deuda al sistema público.

Hay otro problema que menciona Ocampo, que ha sido propiciado por la reciente **ley estatutaria de la salud,** y es la posibilidad de que las urgencias médicas deben ser atendidas en cualquier entidad de salud, sin embargo, el problema radica en que los hospitales o clínicas privadas están cerrando sus servicios de urgencias y pediatría, puesto que muchas veces es la propia EPS la que decide si una urgencia está o no dentro del **Plan Obligatorio de Salud**, haciendo que el negocio no sea rentable. Uno de los casos críticos es la ciudad de Ibagué en donde solamente hay 3 hospitales públicos para atender todas las urgencias de la ciudad.

Aunque las reivindicaciones de los trabajadores de la salud no son un tema laboral, **Yesid Ocampo** afirma que el sector de la salud es uno de los peor remunerados e informalizados, según Ocampo en los hospitales públicos hay **250 mil trabajadores por contrato de prestación de servicios y** los salarios se han reducido en un 20%.

El próximo lunes habrá una **“toma de Bogotá”** por parte de los trabajadores y trabajadoras de la salud, esperando que el gobierno nacional responda y no siga tomando el derecho a la salud como un problema financiero sino del sistema.
