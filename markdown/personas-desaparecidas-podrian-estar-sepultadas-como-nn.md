Title: Desaparecidos podrían estar sepultadas en cementerios como personas no identificadas
Date: 2016-08-30 15:20
Category: DDHH, Nacional
Tags: Desaparición en Colombia, Desaparición forzada Colombia, EQUITAS
Slug: personas-desaparecidas-podrian-estar-sepultadas-como-nn
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Entrega-de-restos-oseos-20.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [30 Ago 2016 ] 

De acuerdo con el Equipo Colombiano Interdisciplinario de Trabajo Forense y Asistencia Psicosocial EQUITAS, las más de 145 mil personas reportadas como desaparecidas en Colombia podrían estar sepultadas en los más de 1.122 cementerios que existen el país, en calidad de no identificadas o identificadas sin reclamar, y **podrían ser entregadas a sus familiares si el Estado contara con un censo del total de los cementerios** que existen y si ejerciera un control efectivo sobre ellos como ordena la ley.

Según afirma Diana Arango, directora de EQUITAS, es urgente intervenir los cementerios porque allí podrían estar sepultadas muchas de [[las personas que sus familiares continúan buscando](https://archivo.contagioradio.com/el-estado-colombiano-es-el-responsable-del-84-de-las-desapariciones-forzadas/),] una acción para la que se requeriría voluntad política y disposición de recursos por parte del Ministerio del Interior y que debería iniciar por una actualización del censo, teniendo en cuenta que la dependencia ha asegurado que en Colombia existen en total 295 cementerios, desconociendo que **son 1.122 municipios y que en cada uno de ellos pueden haber entre 10 y 30 cementerios**.

Frente a esta problemática EQUITAS plantea que es necesario que en los cementerios se implementen buenas prácticas para que el Estado pueda hacerles un seguimiento efectivo que permita exhumar a las personas desaparecidas que podrían estar allí, por lo que en esta semana sostendrán reuniones con entidades estatales y organismos internacionales para acordar un **plan de intervención de los cementerios a través del que se puedan identificar a quienes están sepultados como NN**.

Vea también: [[Innovadora herramienta para buscar personas desaparecidas en Colombia](https://archivo.contagioradio.com/innovadora-herramienta-para-buscar-personas-desaparecidas-en-colombia/)]

<iframe src="http://co.ivoox.com/es/player_ej_12719261_2_1.html?data=kpekk56WepKhhpywj5WdaZS1lZ6ah5yncZOhhpywj5WRaZi3jpWah5yncaXdwtPOjabWpc_b0IqfpZCplba9tabAj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

######  
