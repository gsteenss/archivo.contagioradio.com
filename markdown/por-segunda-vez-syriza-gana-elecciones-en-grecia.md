Title: Por segunda vez Syriza gana elecciones en Grecia
Date: 2015-09-21 14:22
Category: El mundo, Política
Tags: Grecia, Syriza, Tsipiras
Slug: por-segunda-vez-syriza-gana-elecciones-en-grecia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/tsiripas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: radiomacondo.fm 

###### [21 sep 2015] 

En las elecciones anticipadas que se realizaron este domingo en Grecia el partido de **Syriza** obtuvo el triunfo al obtener una clara **mayoría con el 35.5 % de votos** que corresponden a 145 escaños, obteniendo la victoria sobre los partidos **Griegos Independientes** quienes alcanzaron el **3,69 %** , con 10 escaños y como tercera fuerza se consolidó el **partido neonazi Amanecer Dorado, con el 6,99** % 18 escaños.

Con este triunfo Syriza no sólo regresa al poder sino que además después del referendo y la aprobación del tercer rescate, se dispone a **gobernar por segunda vez,** ya con una confianza recuperada con un amplio sector del pueblo griego.

Por su parte Alexis Tsipras quien también fue  ganador con estas elecciones dijo al enterarse del triunfo de esto que “*Dimos una batalla difícil y estoy muy contento porque el pueblo nos dio un mandato claro para seguir luchando en el interior y el exterior*”.

De esta forma Tsipiras tomará el día de hoy juramento como primer ministro de Grecia y podrá asistir el miércoles a Bruselas en donde se desarrolla la cumbre de los refugiados, el cual es un tema que afecta considerablemente a Grecia.
