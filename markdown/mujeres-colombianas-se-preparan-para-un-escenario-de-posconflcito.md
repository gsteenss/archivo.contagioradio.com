Title: Mujeres colombianas se preparan para un escenario de posconflicto
Date: 2015-07-09 13:32
Category: Mujer, Nacional
Tags: construcción de paz, FARC, Foro Internacional Memoria Histórica y la Verdad de las Mujeres, habana, Marina Gallego, posconflicto, proceso de paz, Ruta Pacífica de las Mujeres, Subcomisión de género
Slug: mujeres-colombianas-se-preparan-para-un-escenario-de-posconflcito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/marcha01.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4748686_2_1.html?data=lZyhmpuceo6ZmKiakp2Jd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmqcbmjdrSb8Lk0Nfhx5DNsdHj09nO0NnJb8KfzcaY0sbeb8XZ1MnSjdHFb8ri1crZy8zJssTdjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Marina Gallego, coordinadora de la Ruta Pacífica de las Mujeres] 

###### [9 de Julio 2015]

El 8 de julio se llevó a cabo el **Foro Internacional Memoria Histórica y la Verdad de las Mujeres**, que contó con la participación de más de 300 personas, quienes realizaron un debate con el fin de preparar a las mujeres para un posible escenario de posconflicto con una eventual **comisión de la verdad y marco jurídico para la paz**.

“**El foro sigue reivindicando que las mujeres somos constructoras de paz,** tenemos condiciones y habilidades, para mantener tejido social en las comunidades y estas tiene que ser aprovechadas para la construcción de la paz”, dice Marina Gallego, coordinadora de la Ruta Pacífica de las Mujeres, quien añade que desde el foro, las mujeres se ofrecen para que “la paz tenga realidades… **hay un aporte importante en la inteligencia emocional de las mujeres”.**

A partir de experiencias internacionales y de las comunidades, se planteó la discusión en torno a temas como la memoria histórica en el posconflicto, el papel de un Museo Nacional de la Memoria en la construcción de paz, la verdad en clave para la reconciliación, entre otras temáticas, teniendo en cuenta que,  como dice la coordinadora de la Ruta Pacífica, **“generalmente las mujeres quedan por fuera de la historia”.**

Durante el foro se valoró como positiva la decisión de las FARC - EP sobre el cese al fuego unilateral **“sentimos alegría, para las regiones volver a un estado de cese unilateral es un descanso, ojalá fuera bilateral”,** expresa Gallego. [(Ver comunicado)](http://www.rutapacifica.org.co/sala-de-prensa/comunicados/2015/290-pronunciamiento-de-organizadoras-del-evento-foro-internacional-memoria-historica-y-verdad-de-las-mujeres-en-colombia)
