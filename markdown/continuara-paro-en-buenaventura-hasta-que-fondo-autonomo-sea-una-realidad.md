Title: Continuará paro en Buenaventura hasta que fondo autónomo sea una realidad
Date: 2017-06-05 13:20
Category: Movilización, Nacional
Tags: buenaventura, Camioneros, Movilización, Paro Buenaventura
Slug: continuara-paro-en-buenaventura-hasta-que-fondo-autonomo-sea-una-realidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/4103147Paro-Buenaventura-EFE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cable Noticias.tv] 

###### [05 jun 2017] 

De los 10 billones de pesos, para el fondo autónomo que habían propuesto los bonaversense, **el gobierno propuso únicamente 1.5 billones.** Los promotores del paro de Buenaventura no levantarán el paro hasta tanto el fondo autónomo sea una realidad y lo aportado por el gobierno satisfaga las necesidades del pueblo de esa ciudad expresadas en las exigencias del Comité del Paro Cívico.

Para Manuel Bedoya, presidente del Comité de Pescadores Artesanales de Buenaventura, “el gobierno tiene voluntad de resolver el problema que tenemos acá, pero después de tres años de acuerdos **seguimos sin agua, sin hospital, sin financiación a proyectos productivos y sin mejoras en la educación”.** Le puede interesar:["El nobel de paz en "modo" represión en Buenaventura"](https://archivo.contagioradio.com/el-nobel-de-paz-en-modo-represion-en-buenaventura/)

Luego de 20 días de paro y movilizaciones, el gobierno ha dicho que se han alcanzado acuerdos con la población en un 50%. Sin embargo, **no han definido la forma como se va a establecer el fondo que demandan los pobladores.**

Para Bedoya, “ya tenemos experiencia con el gobierno y no queremos que los acuerdos se queden en firmas de papel. Necesitamos que nos digan dónde se va a crear el fondo y cuáles van a ser los montos que se van a tener para financiar los muelles, la educación y el sistema de acueducto”. Le puede interesar:["Mesa de negociación de paro de Buenaventura no se traslada a Bogotá". ](https://archivo.contagioradio.com/bunaventura-denuncia-violaciones-a-dd-hh-por-parte-de-la-fuerza-publica/)

**Caravanas escolatadas por la policía pasan por encima de la dignidad de Buenaventura**

Los camioneros tradicionales de Colombia han manifestado, en repetidas ocasiones, su solidaridad con las movilizaciones que se realizan en el país. De igual forma, han denunciado que existen camioneros que **“no les importa el movimiento cívico y manifiestan fastidio contra Buenaventura”.** Esto ha generado un descontento en la población que ha denunciado los intereses económicos del gobierno sobre los de la vida digna de la población.

En un día de trabajo, según los Camioneros Unidos de Colombia, en Buenaventura entran y salen 1600 tractomulas con diferentes cargas. Hoy, dentro del paro cívico únicamente se movilizan 70 escoltadas. Para este gremio, **“esta situación reafirma que los camioneros son el motor del país”.** Ante las movilizaciones de los camiones que no hacen parte del gremio, han dicho que “no tienen nada que perder en el paro pero pueden arruinar el sector de carga”.

[**Atropellos constantes a los Derechos Humanos **]

Aparte de las provocaciones que han causado el tránsito de los camiones de carga de mercancías, los bonaverenses han denunciado en repetidas ocasiones las represiones del Esmad y las desapariciones de algunos de los habitantes. Según Bedoya **“ha habido un atropello a los Derechos Humanos** y el Esmad se mete a los barrios marginados supuestamente buscando malandros”. Le puede interesar: ["Persiste arremetida del Esmad contra población de Buenaventura"](https://archivo.contagioradio.com/persiste-arremetida-del-esmad-contra-poblacion-de-buenaventura/)

En un comunicado, el Comité Ejecutivo del paro cívico informó que, en la mesa de conversación con el gobierno, se han reportado los hechos de represión y desaparición. Esto se hizo para que se establezca una **comisión conformada por defensores de derechos humanos de la ONU,** funcionarios del Estado e integrantes del Comité Ejecutivo del paro cívico en Buenaventura.

Igualmente destacaron allí la presencia del Alto Comisionado de las Naciones Unidas para los Derechos Humanos en Colombia Todd Howland en las negociaciones con el gobierno.

El Comité afirmó que hoy se reanudarán las conversaciones con el gobierno para responder a los problemas que se han presentado en el desarrollo del paro cívico y a las demandas sobre el fondo autónomo para Buenaventura.

<iframe id="audio_19084325" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19084325_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[COMUNICADO-PÚBLICO-No-21](https://www.scribd.com/document/350438376/COMUNICADO-PU-BLICO-No-21#from_embed "View COMUNICADO-PÚBLICO-No-21 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_43519" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/350438376/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-mYQ2e9jzeoHZySwE88Gt&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
