Title: Foro Internacional por la democratización de la comunicación en Colombia
Date: 2015-07-18 15:00
Category: eventos, Nacional
Tags: Canal Capital, Colombia Informa, Foro Internacional “Hacia una Ley de Medios que democratice la comunicación en Colombia: experiencias en países de América Latina”, Fundación para la Libertad de Prensa -FLIP-, Periódico Desde abajo, Periódico Voz, Telesur
Slug: foro-internacional-por-la-democratizacion-de-la-comunicacion-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/hacia-una-ley1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[[[[***18Foro Internacional***]]]

[“[[[***Hacia una Ley de Medios que democratice la comunicación en Colombia: experiencias en países de América Latina”***]]]

[[***[[Con la participación de importantes referentes de la comunicación de América Latina, legisladores y medios populares y públicos de Colombia, los días viernes 24 y sábado 25 de julio se realizará un Foro Público que buscará promover el debate sobre el derecho a la información y la propiedad de los medios de comunicación en el país.]***]]

[[[[El evento es convocado conjuntamente por **Canal Capital**, la **Agencia de Comunicación de los Pueblos Colombia Informa**, **Contagio Radio** y los periódicos **Desde Abajo** y **Voz**. Tendrá lugar en el Auditorio del **Centro Memoria, Paz y Reconciliación de Bogotá**.]]]

[[[[La apertura será el viernes 24 de julio, a las 2 pm. Disertarán el **gerente de Canal Capital, Lisandro Duque**; directivos de la **Fundación para la Libertad de Prensa -FLIP-** , el corresponsal de la cadena **Telesur**  y un **legislador** del Congreso de la República.]]]

[[[[Durante el día sábado se debatirá desde las 8 am. en dos mesas: **“Libertad de prensa o manipulación de la información en América Latina”**y **“Legislación, derechos y propiedad de los medios de comunicación en Colombia y América Latina”.** Para cada instancia contaremos con la participación de ponentes de **Brasil, Argentina, Venezuela,** de importantes medios de comunicación y referentes legislativos de] [[Colombia**.**]]]

[[[El evento tiene como objetivo central alimentar el debate sobre la realidad de los medios de comunicación, y aportar insumos para la elaboración de una Ley de Medios que democratice las comunicaciones en el país, en relación a las experiencias similares que ya se han dado en otros países del continente. Al mismo tiempo, se busca promover]la articulación de los procesos comunicativos populares y comunitarios con los medios públicos estatales, a nivel nacional y en el contexto latinoamericano.]]

[[**[[*Foro Internacional “Hacia una Ley de Medios que democratice la comunicación en Colombia: experiencias en países de América Latina”*]**]]  
[[**Fecha y hora:** Viernes 24 de julio, 2 pm. Sábado 25, 8 am.]]  
[[**Lugar:** Centro Memoria, Paz y Reconciliación de Bogotá. [[Carrera 19b 24-86.]]]  
[[[[Contacto de prensa: *contagioradio@contagioradio.com*]]]
