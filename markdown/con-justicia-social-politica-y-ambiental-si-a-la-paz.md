Title: ¡Con justicia social, política y ambiental, sí a la paz!
Date: 2016-08-02 06:00
Category: Ambiente, CENSAT
Tags: Ambiente, Movimientos sociales, paz, proceso de paz
Slug: con-justicia-social-politica-y-ambiental-si-a-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Si-a-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Alba Movimientos 

#### **Por [CENSAT Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva)

### **La paz de la vida** 

Cincuenta y dos años de conflicto político en Colombia han dejado 6.5 millones de desplazados internos, 7 millones de hectáreas de tierra despojadas, más de 300 mil personas muertas, tierras arrasadas, múltiples formas de violencia y exterminio contra la población (asesinatos selectivos, desapariciones forzadas, masacres, sevicia y tortura, despojos, violencia sexual, terror y control poblacional, etc.); naturaleza arrasada por los bombardeos, deterioro de los ecosistemas por la aspersión con glifosato del Plan Colombia, afectación de las cuencas hídricas por los derrames de petróleo, etc. En suma, una larga y profunda tragedia humanitaria y ambiental. Esto conlleva a la necesidad de la solución negociada del conflicto y la búsqueda de la paz con todas las insurgencias. Las razones humanitarias y de la naturaleza serían suficientes para terminar el conflicto y potenciar la paz, serían fundamentales para la construcción de vida digna en el país.

### **¿Dos agendas en disputa por la paz?** 

[Votar sí a la refrendación del fin del conflicto político significa entonces votar por la existencia de la vida digna. Ahora bien, preguntamos si estas razones humanitarias y ambientales se corresponden con la agenda de las elites. Por el contrario, se observa que esa agenda intenta mantener el]*[statu quo]*[, es decir, un régimen político excluyente, con estructuras dominantes de poder; es claro  por qué en la negociación con las Fuerzas Armadas Revolucionarias de Colombia -FARC- no se considera la discusión del modelo económico, privatizador y neoliberal (Pines, Zidres, monocultivos, mega minería, altas tasas de ganancia y especulación financiera, etc.), mucho menos hacen plausible el debate en torno a las estructuras ideológicas de legitimación y control social estatal y gubernamental y, con ello, tampoco al sistema coercitivo (policía y fuerzas armadas), ni al sistema de relaciones con el orden global establecido, ni al modelo de crecimiento verde.]

[Se posiciona entonces una negociación política con agendas públicas contrapuestas: la de las elites y el bloque hegemónico que negocia la paz, para que todo siga igual -incluso desconociendo acuerdos alcanzados en La Habana con las FARC-, y la de organizaciones, procesos y movimientos sociales que intenta un modelo de inclusión política, de justicia social y ambiental para la garantía de la vida en los territorios. Esta última agenda, la de los sectores populares organizados, se encuentra establecida en los acuerdos alcanzados de manera unitaria por la Cumbre Nacional Agraria, Campesina, Étnica y Popular desde el año 2014, y ratificada vía movilización social en los meses pasados. También se expresa en los postulados y principios de la Mesa Social para la Paz y la Mesa Nacional Minero Energética y Ambiental.]

### **Sí a la paz con justicia social, política y ambiental** 

[Emergen en las construcciones de los pueblos las reivindicaciones invisibilizadas de las organizaciones y movimientos sociales, que concretarían esa agenda]*[otra]*[ que respondería al principio humanitario: la paz con justicia social –económica (zonas de reserva campesina, territorios agroalimentarios, regiones y municipios autónomos para decidir su economía campesina, economía propia, equidad económica de la tierra, el capital y el trabajo, macroeconomía y políticas económicas a favor de los y las trabajadoras, el empleo nacional y las pymes, etc.), justicia política (participación ciudadana, derechos humanos, garantías políticas, ciudadanías, diversidad cultural, etc.) y justicia ambiental (tierras y territorios, solución de los problemas agroalimentarios, protección del agua, la biodiversidad de los páramos y ecosistemas frágiles, equidad en el goce y disfrute de los bienes comunales, ciudades sustentables, tierra y territorios, territorios de agro-biodiversidad, etc.).]

[Refrendar con el voto popular la negociación política del conflicto colombiano es preparar seguidamente el país nacional (todos los sectores sociales y políticos) realmente para la paz, la que demanda construir un país distinto no sólo del de los últimos 52 años, distinto de la forma república constituido hace 200, un país que se reconcilie con la naturaleza, desde otro modelo de vida y sociedad. La paz real significa resolver el conflicto desde las demandas históricas de los sectores sociales que han sido victimizados social, política y ambientalmente. La construcción de la paz debe ser una oportunidad para repensarnos con la naturaleza. Se requiere re-construir la-s verdad-es, la justicia política y la reparación social y ambiental, ante lo cual la Comisión de la Verdad será un hito, pero ésta será insuficiente si no se cuenta con verdad para las víctimas humanas y verdad ambiental, puesto que la historia del conflicto colombiano evidencia no solo el sufrimiento humano, también el sufrimiento de todas las formas de la naturaleza.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
