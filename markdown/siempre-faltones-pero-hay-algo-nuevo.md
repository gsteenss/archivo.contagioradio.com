Title: Siempre faltones, pero hay algo nuevo
Date: 2018-02-09 16:04
Category: Camilo, Opinion
Tags: acuerdos de paz, acuerdos incumplidos, dialogo eln gobierno, FARC
Slug: siempre-faltones-pero-hay-algo-nuevo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/marcha-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div dir="auto" style="text-align: justify;">

</div>

<div dir="auto" style="text-align: justify;">

###### Foto: Gabriel Galindo/Contagio Radio 

#### **Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)**

###### 9 Feb 2018

</div>

El incumplimiento de la palabra de Estado en acuerdos con comunidades locales, organizaciones sociales y las FARC, y en la mesa de Quito, continúa siendo una realidad que será así por la carencia  de una ciudadanía mayoritaria de poder consciente, pero que está siendo transformada en y por ciudadanos transformantes, siendo lo nuevo entre lo turbio.

Los asesinatos de 21 líderes sociales, de 5 integrantes del Nuevo Partido, de 3 policías y 46 heridos en enero de este año, ensombrecen el momento histórico que está naciendo. De alguna eso novedoso está más allá de lo llamado político alternativo que sigue en su extremado sectarismo, en su lenguaje sin atracción, desarrollando ejercicios políticos poco novedosos. La  mesa de Quito  sin nuevos ciudadanos, los narcicismos de los llamados lìderes políticos y de los propios errores (ojalá aprendizajes) de las FARC en el ejercicio de la política sin armas, expresan lo necesario de lo transformante en lo alterativo.

Ese cruento rostro de la realidad reitera que en Colombia, la deficitaria democracia sigue siendo novedosamente consentida. El desprecio por lo pequeño, o así visto el ELN por el establecimiento, tiene costos, bastantes graves y dolorosos. Las acciones  como el atentado de Barranquilla y los asesinatos de integrantes del nuevo partido a manos del ELN, les hace funcionales al modelo autoritario. Escuché en una reunión calificarles de reaccionarios.

Los asesinatos de lideres y de militantes del nuevo Partido en su mayoría son la expresión de un orden de sistema militar político ecónomico que sigue operando bajo la lógica del exterminio. Lo nuevo es que no todos los sectores del establecimiento están allí, y,  tampoco todos los asesinados son por el modelo de persecución de tipo paraestatal.  Las víctimas  no lo son por los mismos móviles, aunque el modos operandi sea en algunos casos similar. Los determinadores y beneficiarios son distintos, según los contextos regionales.  Y en esta realidad es reconocer al mismo tiempo una sistematicidad sí, pero más compleja.

[El Acuerdo de Paz ha fraccionado las tensiones en las FFMM son ciertas, tanto como se expresa en los partidos tradicionales y sus derivaciones novedosas la U y el Centro Democrático. Unos viven a la vieja usanza  con el narco paramilitarismo, otros abogando por la llamada paz territorial en respaldo al Acuerdo del Teatro Colón. Quién sea elegido definirá con que cúpula castrense gobernara la vieja o la nueva.]{.s1}

[Eso novedoso habilitó el neoparamilitarismo un algo distinto a ese paramilitarismo de los 70 que opera hasta hoy, el de las estrategias encubiertas bajo la dirección de sectores militares.  Lo neoparamilitar es un tipo de relación con militares sin subordinación y su interacción corresponde a intereses económicos compartidos temporales. La disposición de los neoparamilitares AGC para su desmantelamiento es un signo no bien interpretado por analistas que indica lo que sucede en el establecimiento la terminación de dar por terminada una fase de guerra sucia con esa estructura.]{.s1}

[El riesgo es que esa disposición se diluya en el nuevo gobierno y sean coptados por la presión militar y política para una actuación a la vieja usanza.]{.s1}

[El Estado colombiano es lo qué es, una arquitectura institucional que ha dirimido asuntos estructurales para el bien de unos pocos, y ha generado unos cuantos problemas estructurales de exclusión, de sometimiento y asentimiento. Las expresiones políticas en ese Estado han sido clientelares, delincuenciales y deshonestas. Estas han creado ciudadanos consumidores,  electores con odios infundados, borregos lógicos. Lo que firman en letra fija es sobre algo muerto o gelatinoso. Han usado de la ley a su acomodo,  y con la represión que le acompaña, amasan riqueza indebida, la propia, la de terratenientes y la de algunos privados. Entonces por qué desconocer que seguirán traicionando todo tipo de Acuerdo.]{.s1}

[El Estado colombiano o mejor su clase dirigente siempre ha deshonrado la palabra. Desde Bolívar  traicionado por los criollos disfrazados de independentistas. Desde esa época se postergó el reconocimiento pleno de derechos a indígenas y afros. Allí en el propio momento de la independencia se incubó, lo que después fue la mentalidad de la ley de caballos muertos, convertida luego en doctrina militar.Una mezcla del maniqueísmo y dualismo católico y la escatología moral democrática anticomunista. Así que ver a la Colombia de los incumplimientos sería lo novedoso hacia un nuevo empoderamiento social que transforme el Estado, rompiendo los complejos edípicos de un Estado hecho para unos.]{.s1}

[Así es, Colombia sigue siendo el segundo país en desigualdad en el continente. Esa dirigencia que ha heredado, con algunas excepciones, la imposibilidad de la inclusión, vive de los miedos infundados por el espejismo ideologizado.  El dogma profesado cuando se piden  argumentos es el respeto absoluto a la propiedad privada frente al ateísmo y el 'sanguinario' colectivismo, así como, la protección de la sacrosanta institución familiar ante  la barbarie  libertad de orientación sexual. En el fondo la Tradición, la Familia y la Propiedad defendida en el sentimiento con irracionalidad y rabia.]{.s1}

[Ahí lo nuevo, que nace de una dejación de armas genera terror al confort mental y el alma material de los dirigentes y sus bases domesticadas. Sí hay ciudadanos que estén en contra de la paz, del Acuerdo del Colón, de la mesa de Quito, del movimiento social tienen el derecho de expresar su malestar con palabras e ideas y una expresión no violenta. ]{.s1}

[Lo que hoy ocurre con esos grupúsculos es grave. Expresa lo acuñado en 200 años de historia en las que se atizó el odio hablando del amor a Dios. Eso nuevo basado en la verdad genera terror.]{.s1}

[El incumplimiento de garantías es del Estado y de la sociedad y lo nuevo naciente está en aquellos que están tomando distancia del rechazo violento al Nuevo Partido.]{.s1}

[Si fue provocador que Timochenko se haya presentado cómo candidato a la presidencia, como muchos así lo creen, generando en algunos la objeción consciente, en otros casos, el saboteo agresivo al  derechos básico a la libre expresión; hoy en eso nuevo, las FARC están en la posibilidad de invitar a una reflexión profunda cesando cualquier actividad política pública, convocando a los partidos en contienda a pronunciarse sobre esas garantías, pero sobretodo acercándose al 30 % de los votantes en blanco ante unas elecciones presidenciales. Ese 30% es otro actor que se distancia de lo viejo en búsqueda de lo nuevo. ¿Lo entenderán todos los llamados alternativo? ¿Lo entenderemos? ]{.s1}

[Solo un alma nacional que reconozca lo negado, lo rechazado, y lo invisibilizado abrirá en este momento de la historia a lo nuevo, a eso creciente en la memoria de los incumplimientos y que pocos ven.]{.s1}

#### [Leer más columnas de opinión de  Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
