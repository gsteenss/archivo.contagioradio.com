Title: Gobierno y FARC acordaron puntos para discutir cese bilateral y dejación de armas
Date: 2015-02-12 20:16
Author: CtgAdm
Category: Nacional, Paz
Tags: cese bilateral del fuego, Dejación de armas, FARC, La Habana, Mesa de conversaciones de paz, paz, proceso de paz
Slug: gobierno-y-farc-acordaron-puntos-para-discutir-cese-bilateral-y-dejacion-de-armas
Status: published

###### Foto: ips.org 

En el comunicado conjunto 51, el gobierno y las FARC afirmaron que ya están listos los puntos que trabajará la **Sub Comisión Técnica para el cese bilateral y la dejación de armas**. Dicho escenario sesionará el próximo 27 de febrero y trabajará en las garantías de incorporación de las FARC a la vida civil, situación de personas privadas de la libertad, ajustes institucionales para la construcción de la paz y el esclarecimiento del fenómeno del paramilitarismo.

Según el comunicado, durante el ciclo 32 se avanzó “en establecer lineamientos y reglas de funcionamiento de la sub-comisión del punto 3"y recordaron "que el objetivo de esta sub-comisión técnica es contribuir en el **análisis de experiencias, generación y discusión de iniciativas y propuestas sobre cese al fuego bilateral y dejación de armas**”

Además se establecieron los lineamientos y el mandato de esa sub comisión técnica así como los puntos estructurales del debate. La mesa de trabajo estará integrada por **10 representantes de cada delegación y** funcionará de manera simultánea con la mesa principal de conversaciones.

A continuación está el documento anexo publicado en el comunicado conjunto.

<!--more-->

*Anexo*

*SUB-COMISIÓN TÉCNICA  DEL PUNTO 3 DE LA AGENDA: “FIN DEL CONFLICTO”.*

*LINEAMIENTOS Y MANDATO.*

*1. Presentación:*

*Las delegaciones  del Gobierno Nacional y de las FARC- EP,  con el fin de esclarecer la misión concreta que tiene en el marco de la Mesa de Conversaciones de Paz de La Habana la Sub-comisión Técnica, creada para tratar aspectos concernientes al punto 3 de la Agenda del Acuerdo General: “Fin del Conflicto”, en lo relativo al numeral 1. Cese al fuego y de hostilidades bilateral y definitivo, y numeral 2. Dejación de las armas., acordaron lo siguiente:*

*2. Antecedentes:*

*La creación de la Sub-comisión Técnica, es una decisión de la Mesa de Conversaciones que se enmarca en el “Acuerdo General para la terminación del conflicto y la construcción de una paz estable y duradera”, suscrito por el Gobierno y las FARC-EP el 26 de agosto de 2012 y tiene también como marco de referencia el comunicado conjunto del 7 de junio de 2014 en el que  “acordamos crear una sub-comisión técnica integrada por miembros de las delegaciones con el fin de iniciar las discusiones sobre el punto 3 “Fin del Conflicto” de la Agenda del Acuerdo General”. *

*A esto se agrega lo acordado entre las partes en cuanto a que “Esta sub-comisión tratará los sub puntos de cese al fuego y de hostilidades bilateral y definitivo y dejación de armas, e iniciará revisando y analizando buenas prácticas nacionales e internacionales en la materia. La Sub-comisión estará conformada por hasta 10 miembros de cada delegación, que establecerán un cronograma de trabajo”, según se consignó en el comunicado conjunto del 5 de agosto de 2014 y se ratificó en comunicado conjunto del 22 de agosto del mismo año, fecha en la que formalmente se dio por instalada la Sub-comisión, precisando que ésta iniciará su trabajo sobre los sub-puntos mencionados “para lo cual estudiará, entre otras cosas, modelos nacionales e internacionales”.*

*3. Objetivo:*

*La Sub-comisión Técnica sobre cese al fuego bilateral y dejación de armas, tiene el propósito de contribuir en el análisis de experiencias, generación y discusión de iniciativas y propuestas que, respecto a estos asuntos, puedan servir de insumos para agilizar y facilitar las discusiones de los plenipotenciarios de Gobierno Nacional y las FARC-EP, que permitan la concreción de acuerdos que conduzcan al fin del conflicto. Los acuerdos sobre estos sub puntos se construirán en la Mesa de Conversaciones.*

*4. Conformación y funcionamiento:*

*La Sub-comisión estará conformada por hasta 10 miembros de cada delegación, de los cuales al menos uno será un plenipotenciario,  y sesionará dentro de los cronogramas y jornadas que acuerden los mismos equipos de trabajo. Durante estas sesiones se analizarán las experiencias, iniciativas y propuestas que se sugieran desde cada uno de los equipos, o se escuchará a los expertos que las partes hayan propuesto dentro del Plan de trabajo de la Sub-comisión Técnica. Al respecto, ésta elaborará informes periódicos sobre conclusiones y propuestas a la Mesa de Conversaciones para que esta evalúe el trabajo de esta Sub-comisión.*

*Los demás plenipotenciarios de ambas delegaciones podrán asistir a las sesiones de la comisión en cualquier momento.*

*Los países garantes asistirán a las reuniones de la Sub-comisión Técnica.*

*La Sub-comisión podrá invitar a reconocidos expertos en la materia con el fin de identificar lecciones  aprendidas de otras experiencias y buenas prácticas. Los expertos los escogerán las delegaciones sobre la base de una lista presentada por los países garantes, o de común acuerdo.*

*El funcionamiento de la Sub-comisión no deberá interferir en la dinámica de La Mesa de Conversaciones.*

*5. Duración:*

*La Sub-comisión Técnica sesionará durante el tiempo que la Mesa considere necesario.*

*6. El Plan de trabajo:*

*La actividad de la Sub- comisión Técnica, con un Plan de trabajo mínimo acordado, se enfocará en los siguientes sub-temas, principalmente:*

<div style="text-align: justify;">

*a. Revisión y análisis de modelos y buenas prácticas nacionales e internacionales relacionadas con el tema del cese al fuego y de  hostilidades bilateral y definitivo.*

</div>

<div style="text-align: justify;">

*b. Revisión y análisis de modelos y buenas prácticas nacionales e internacionales, relacionadas con el tema de la dejación de armas.*

</div>

*7. Integralidad y simultaneidad:*

*Dado que el punto 3, Fin del Conflicto, precisa que este es un proceso integral y simultáneo, que además del cese al fuego y la dejación de armas, incluye cinco aspectos más, de la misma importancia y trascendencia, los cuales conforman un todo, los compromisos de la Agenda imponen tratarlos, no de manera separada o aislada, sino de conjunto y al mismo tiempo. Tal circunstancia implica una permanente interrelación con la Mesa en la labor de intercambio de información y definiciones que se den respecto a cada de uno de los ítem comprometidos:*

<div style="text-align: justify;">

*- Garantías de reincorporación de las FARC-EP a la vida civil - en lo económico, lo social y lo político, de acuerdo con sus intereses.*

</div>

<div style="text-align: justify;">

*- La revisión de la situación de las personas privadas de libertad, procesadas o condenadas, por pertenecer o colaborar con Las FARC-EP.*

</div>

<div style="text-align: justify;">

*- La intensificación del combate para acabar con las organizaciones criminales y sus redes de apoyo, incluyendo la lucha contra la corrupción y la impunidad.*

</div>

<div style="text-align: justify;">

*- La revisión, reforma y ajustes institucionales necesarios para hacer frente a los retos de la construcción de la paz.*

</div>

<div style="text-align: justify;">

*- Las garantías de seguridad.*

</div>

<div style="text-align: justify;">

*- Y el esclarecimiento del fenómeno del paramilitarismo, entre otros fenómenos.*

</div>

*La Mesa de Conversaciones creará los mecanismos necesarios para abordar estos tema*s.
