Title: Víctimas de Crímenes de Estado piden que se desclasifiquen archivos militares
Date: 2017-04-05 16:19
Category: DDHH, Nacional
Tags: Implementación de acuerdos de paz, víctimas de crímenes de Estado
Slug: victimas-de-crimenes-de-estado-piden-que-se-desclasifiquen-archivos-militares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-05-at-3.43.02-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [05 Abril 2017]

Las víctimas de crímenes de Estado, en medio de la firma del decreto para dar paso a la creación de la Comisión de Búsqueda y Esclarecimiento de la Verdad, le exigieron al presidente Santos “que no se comprometa con solo una parte de la verdad, sino con toda la verdad”, **desclasificando los archivos de inteligencia y contrainteligencia sobre el accionar de los agentes estatales**.

La verdad a la que se refiere Alejandra Gaviria, hija de los militantes de la Unión Patriótica María Josefa Serna y de Francisco Gaviria, desaparecido, torturado y asesinado el 10 de diciembre de 1987, es la que se podría encontrar si se tiene acceso a archivos como los del **DAS, que llevó a cabo entre el año 2003 y 2008 una sistemática persecución contra 300 personas catalogadas como amenaza para la seguridad nacional.**

Entre ellas se encontraban **defensores de derechos humanos, líderes sociales, periodistas y miembros de la oposición**, según reconoció la Comisión Interamericana de Derechos Humanos en su Informe Anual para el año 2009. Le puede interesar: ["Corte revisará constitucionalidad de modificaciones del Congreso al Acuerdo Final de Paz"](https://archivo.contagioradio.com/corte-revisara-constitucionalidad-de-modificaciones-del-congreso-al-acuerdo-final-de-paz/)

De acuerdo con Diana Gómez, integrante del MOVICE, esta Comisión de Búsqueda y Esclarecimiento de la Verdad necesita la desclasificación de los archivos de inteligencia y contrainteligencia porque **“allí está gran parte de la verdad y los insumos que permitirían establecer como es qué ha funcionado la criminalidad de Estado”**.

De igual modo, tanto el MOVICE como H.I.J.O.S, dos organizaciones que reúnen a las víctimas de crímenes de estado, hicieron un llamado para evitar que **“asiente una tesis negacioncita del Estado sobre la violencia que han cometido”** para que exista una equidad entre las víctimas del conflicto armado y su derecho a la verdad, justicia y reparación. Le puede interesar: ["JEP mantendrá abierta la posibilidad para la impunidad: WOLA"](https://archivo.contagioradio.com/jep-mantiene-abierta-la-posibilidad-para-la-impunidad-wola/)

Finalmente, durante el evento, Alejandra Gaviria, en representación de H.I.J.O.S, entregó una urna al presidente Santos con la propuesta de establecer un compromiso que propenda por evitar que suceda otro exterminio como el de la Unión Patriótica **“tenemos la idea y la esperanza que los compañeros de las FARC puedan hacer su trabajo en la política y que eso no les cueste la vida ni ellos ni a ningún colombiano”**.

<iframe id="audio_17988745" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17988745_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
