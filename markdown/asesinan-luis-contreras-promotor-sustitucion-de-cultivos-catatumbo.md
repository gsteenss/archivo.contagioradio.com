Title: Asesinan a Luis Contreras, líder de sustitución de cultivos en el Catatumbo
Date: 2019-01-21 15:55
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: asesinato de líderes sociales, Norte de Santander, Sustitución de cultivos
Slug: asesinan-luis-contreras-promotor-sustitucion-de-cultivos-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/lider-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Opinión 

###### 21 Ene 2019 

[El pasado 17 de enero después de haber sido torturado y atacado con ácido, fue asesinado **Luis Alfredo Contreras,** líder social quien coordinaba proyectos de erradicación y sustitución de cultivos de uso ilícito coca por cacao en el corregimiento Las Mercedes en Sardinata, Norte de Santander, región donde la siembra de cultivos ilícitos y el asesinato de líderes sociales continúa en aumento.   
  
Con la información recolectada se pudo establecer que cerca de las 10:00 pm, Luis Alfredo mejor conocido como ‘Paramillo’ fue abordado por cuatro desconocidos mientras conducía su camioneta junto a su pareja a quien dejaron en libertad  y le obligaron a abandonar el vehículo.  
]

[Contreras fue transportado por los desconocidos hasta Higuerón, vía que del casco urbano de Sardinata conduce al corregimiento de Las Mercedes, donde fue torturado y posteriormente atacado con ácido sobre su rostro e impactado por proyectiles de bala en el pecho y la cabeza que le ocasionaron la muerte de forma inmediata.  
  
Luis Alfredo **fue presidente de la Junta de Acción Comunal de Las Mercedes  y lideraba proyectos de erradicación y sustitución de cultivos de uso ilícito por cacao,** labor que podría ser la razón por la cual habría sido asesinado a manos de los grupos armados que hacen presencia en esta zona como el ELN, reductos del EPL o  disidencias de las Farc. ]

[Datos suministrados por el informe del **Sistema Integrado de Monitoreo de Cultivos Ilícitos SIMCI, de la ONU, revelan que en 2017 el número de hectáreas cultivadas con coca en Norte de Santander llegó a 28.244, **ocupando el tercer lugar en la medición solo detrás de Nariño y Putumayo.[(Lea también: Tumaco, municipio con mayor número de líderes asesinados desde 2016).](https://archivo.contagioradio.com/cauca-narino/)  
  
Según el diario de Cúcuta, La Opinión, allegados a Contreras le habían advertido que fuera cauteloso al tratar el tema de la sustitución de cultivos y aunque al respecto siempre se hablaba de frente junto a otros integrantes de la comunidad, el líder social no había recibido amenazas.]

[Según los familiares de la víctima, la camioneta fue abandonada en un sector conocido como La Esperanza, sin embargo el cuerpo de ‘Paramillo’ fue encontrado hasta el 18 de enero cuando fue trasladado a Cúcuta y puesto a disposición del Instituto de Medicina Legal para hacer la investigación correspondiente.]

###### Reciba toda la información de Contagio Radio en [[su correo]
