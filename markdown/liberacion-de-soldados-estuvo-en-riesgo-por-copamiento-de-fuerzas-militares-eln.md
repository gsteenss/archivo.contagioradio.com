Title: Liberación de soldados estuvo en riesgo por "copamiento de Fuerzas Militares": ELN
Date: 2015-11-16 19:49
Category: Nacional, Paz
Tags: ELN, liberación soldados ELN, Paz ELN
Slug: liberacion-de-soldados-estuvo-en-riesgo-por-copamiento-de-fuerzas-militares-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/f11897158a54237acf66986f8c4ae813.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [[  
](https://archivo.contagioradio.com/liberacion-de-soldados-estuvo-en-riesgo-por-copamiento-de-fuerzas-militares-eln/comunicado-2/)Foto: elespectador.com] 

##### 16 nov 2015

De acuerdo con el comunicado publicado a través de la cuenta de Twitter @ELN\_RANPAL, durante la liberación de los soldados retenidos  Andrés Felipe Perez y Kleider Antonio Rodríguez ocurrida al medio de día de este lunes en zona rural del departamento de Arauca, estuvo en riesgo por el "total copamiento territorial de las fuerzas estatales".

Según señala el ELN, la guerrilla "logro burlar todos los cercos y desembarcos" hasta poner a buen resguardo y brindarle cobertura a los soldados prisioneros" cuestionando además el accionar del Estado, que a juicio del grupo guerrillero considera "carne de cañon, objetos desechables que se remplazan con nuevos reclutamientos".

En el comunicado firmado por el frente de guerra oriental, se hace énfasis además en el "alto grado de militarización y atropellos cometidos por la fuerza pública contra la población en la región" refiriéndose al Oriente colombiano, asegurando que con estas acciones el gobierno son "para que empresas multinacionales sigan saqueando todas nuestras riquezas naturales".

La liberación de los uniformados retenidos el pasado 26 de Octubre en el municipio de Guican, Boyacá, se da 14 días después del anuncio dado por el ELN, quienes agradecieron a la Comisión humanitaria por sus "buenos oficios" para permitir la liberación de los soldados, quienes de acuerdo con el reporte de la Cruz Roja se encontraban en "condiciones físicas aptas para el viaje" hacia Bogotá, a donde llegaron en horas de la tarde.

Al cierre del comunicado, el ELN aseguro que las liberaciones son una "muestra de su política humanitaria" encamina en su "propósito de avanzar en la construcción de una paz verdadera con justicia social y bienestar para el pueblo colombiano".

Este es el comunicado emitido la tarde del lunes.

![comunicado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/comunicado.jpg){.aligncenter .size-full .wp-image-17262 width="599" height="378"}
