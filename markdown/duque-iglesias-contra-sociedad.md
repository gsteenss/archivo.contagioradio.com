Title: Discurso de Duque ante iglesias atenta contra la sociedad moderna
Date: 2019-07-07 15:20
Author: CtgAdm
Category: Comunidad, Entrevistas
Tags: Culto, familia, Fe, Religión
Slug: duque-iglesias-contra-sociedad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Discurso-de-Duque-ante-iglesias.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @IvanDuque  
] 

En el marco del **Día Nacional de Libertad Religiosa y de Culto, celebrado el pasado 4 de julio,** el presidente Iván Duque pidió a las iglesias luchar por la familia 'tradicional' y contra el consumo de sustancias psicoactivas. La petición del Presidente fue vista como un desafío a las Cortes, que se han pronunciado sobre estos temas, así como a la sociedad liberal moderna que promueve valores de respeto hacía la diferencia y las formas en que las personas se relacionan en la actualidad.

El pastor **Milton Mejía, integrante del Diálogo Intereclesial por la Paz de Colombia (DiPaz)** y quien participó en el encuentro en la Casa de Nariño, declaró que el Presidente dejó ver su fe cristiana, e hizo evidente mediante en su oración y sus acciones que es un cristiano católico que seguía por las doctrinas clásicas tradicionales. Para el Pastor, "eso es bueno porque todos tenemos derecho a vivir nuestra fe, pero es interesante que en un día donde había tantas expresiones de diversas religiones", el Presidente se decante por solo una de esas opciones.

### **Duque pidió defender el diseño de familia 'original'** 

> [\#Bogotá](https://twitter.com/hashtag/Bogot%C3%A1?src=hash&ref_src=twsrc%5Etfw) Consideramos importante volver a la conversación sobre la familia, por ser el núcleo de la sociedad. Debemos reflexionar sobre cómo se están formando nuestras familias y cómo están creciendo nuestros niños. [\#LibertadReligiosa](https://twitter.com/hashtag/LibertadReligiosa?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/QmLXjwPJKv](https://t.co/QmLXjwPJKv)
>
> — Iván Duque (@IvanDuque) [4 de julio de 2019](https://twitter.com/IvanDuque/status/1146816024853065729?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
En su discurso, Duque le pidió a las iglesias que defendieran la familia, entendida como la unión de un hombre y una mujer, en la que también hay hijos. Respecto a esta petición, el Pastor afirmó que no había nada nuevo, porque es lo mismo que se viene proclamando por parte de "sectores católicos e incluso algunos sectores cristianos no católicos" que hablan de un diseño original de familia, "como algo que Dios hubiera creado". (Le puede interesar: "Familia[54 mil familias serían afectadas por reanudación de fumigación aérea"](https://archivo.contagioradio.com/fumigacion-aerea-destruiria-proyecto-de-vida-de-54-000-familias/))

No obstante, Mejía aseguró que "alguien que tenga un conocimiento básico de teología de biblia sabe que no hay un diseño original de la familia como dicen ellos (...) **si se habla de que Dios diseñó una familia, podríamos decir que es la familia humana"**. En ese sentido, sostuvo que la familia humana es muy diversa, no se limita a padre, madre e hijo; y concluyó que "las familias son relaciones amplias, son acuerdos de grupos entre personas que deciden unirse por amor, para construir sociedad".

### **Duque desafió a las Cortes, y a un proyecto de sociedad liberal moderna**

> [\#Bogotá](https://twitter.com/hashtag/Bogot%C3%A1?src=hash&ref_src=twsrc%5Etfw) Por nuestra juventud no vamos a dejar de luchar contra la amenaza de la droga en las calles. Invito a todas las iglesias y organizaciones religiosas a que nos acompañen en esta tarea en el ámbito del diálogo con la familia y con la comunidad. [\#LibertadReligiosa](https://twitter.com/hashtag/LibertadReligiosa?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/NpJjrNRi5k](https://t.co/NpJjrNRi5k)
>
> — Iván Duque (@IvanDuque) [4 de julio de 2019](https://twitter.com/IvanDuque/status/1146814687553773568?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Además de la familia, Duque pidió a los 117 líderes religiosos que estuvieron en el encuentro que lucharan contra "la amenaza de la droga en las calles"; hecho que llamó la atención, porque recientemente la Corte Constitucional eliminó la norma que castigaba el consumo de sustancias en parques y plazas públicas. Para el Integrante de DiPaz, esto podría interpretarse como un encontrón con la Corte; pero también es una forma de oponerse al pensamiento liberal moderno.

El Pastor explicó que su declaración fue **"contra el proyecto de un gran grupo que pide una sociedad moderna, de personas que pedimos relaciones entre los seres humanos más libres de desarrollo"** y que permita, a través de la educación crítica, que las personas decidan cómo vivir. En resúmen, Mejía declaró que Duque enfrentó "a las cortes, y a un gran sector de la sociedad que vemos de otra forma la fe y las relaciones humanas". (Le puede interesar: ["Decisión de C. Constitucional sobre consumo de sustancias en espacios públicos fue sensata"](https://archivo.contagioradio.com/decision-constitucional-consumo-de-sustancias-sensata/))

### **El tema que no apareció en la reunión: La Paz y la reconciliación**

El Pastor criticó que no hubo posibilidad de reaccionar a las peticiones del Presidente, porque tras su discurso, distribuyeron a los integrantes de las iglesias en distintas mesas, en las que hacía presencia un funcionario del Gobierno. **En la mesa del Pastor Mejía quedó el alto comisionado para la paz Miguel Ceballos, "fue bueno porque nunca habíamos podido hablar con él", comentó el Pastor.**

A pesar de la coincidencia en la mesa, "nosotros insistimos en el tema de paz que no salió en el discurso, como tampoco salió la palabra reconciliación", denunció Mejía; al tiempo que comentó que un discurso como el pronunciado por el Presidente había sido posible "porque las iglesias están muy apegadas a la agenda del Gobierno". (Le puede interesar: ["Académicos del mundo piden medidas a Duque políticas serias de DD.HH."](https://archivo.contagioradio.com/academicos-mundiales-critican-a-duque-por-ola-de-violencia-contra-lideres/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38150911" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38150911_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
