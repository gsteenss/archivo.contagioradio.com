Title: La justicia espectáculo y la inquisición
Date: 2015-07-13 12:46
Category: Camilo, Opinion
Tags: atentados Bogota, congreso de los pueblos, Doctrina de la Seguridad, falso positivo judicial, Juan Manuel Santos, marcha patriotica, Movimiento social
Slug: la-justicia-espectaculo-y-la-inquisicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/11752526_475971242570234_6611217422223694166_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Congreso de los Pueblos] 

#### [Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [~~@~~CamilodCasas](https://twitter.com/CamilodCasas) 

###### [12 Jul 2015] 

Juan Manuel Santos celebró desde su cuenta de twitter en pasados días la privación de la libertad de integrantes del Congreso de los Pueblos. “Capturadas 11 personas del ELN responsables de petardos en Bogotá. Felicitaciones a @PoliciaColombia y @FiscaliaCo -Pagarán por atentados!”. Evidentemente, el primero en hacerlo fue el general Palomino, quien en su cuenta de la misma red social expresó: “Valoro inmensamente el trabajo metódico de investigadores de la Policía que permitieron la captura de responsables de los atentados en Btá” (sic)

Así Santos, a los 11 integrantes del Congreso de los Pueblos, los vinculó por arte de magia como integrantes del ELN y responsables de los ataques con explosivos el pasado 2 de julio contra las sedes de la empresa Porvenir, que maneja fondos de pensiones del banquero Sarmiento Angulo.

En las expresiones oficiales ni siquiera usaron una calificación matizada el presumible. En tuiter, se les usó, se les marcó, se les condenó, ese fue el juicio sumario, mediático y social. Nada distante de los frecuentes espectáculos uribistas bajo los cuales millares de personas fueron detenidas arbitrariamente y como desde la época del Estado de Sitio o del Estatuto de Seguridad Democrática se ha usado por parte de diversos gobiernos para lograr el control social y la desarticulación del movimiento social y la oposición política.

De fondo en este caso, como en muchos, de nada sirve que el Fiscal a cargo del caso haya negado que se les esté acusando por los hechos del 2 de julio. Es el mismo valor inicial judicial que se le concede a las afirmaciones del rector de la Universidad Pedagógica Nacional quien afirmó que algunos de los estudiantes detenidos estaban en una actividad académica en esa fecha fuera de la ciudad de Bogotá, y por tanto, no son explosivistas, y también, que los demás acusados demuestran que no tienen nada que ver con las circunstancias, de modo, de tiempo y de lugar por lo que se les privó de la libertad. Así de claro, de poco sirve ante las afirmaciones presidenciales que son falsas, la verdad de estas víctimas es nada ante el poder fáctico del ejecutivo, y de la policía, o mejor, sirve como, otra constancia de la injusticia, pero poco para lograr las libertades.

Aquí se trata de un efecto simbólico judicial para demostrar la diligencia de la seguridad, para generar la sensación de una eficacia de inteligencia y de investigación que no ha existido. Por eso, las imputaciones y alegaciones se cambian, ya no son los responsables de los hechos del 2 de julio, son los responsables de otros hechos anteriores. La pregunta es, ¿por qué no los detuvieron antes?, y si ¿ahora?.

Todo tiene un tufillo de chivo expiatorio y una pretensión de aleccionamiento colectivo. Los chivos expiatorios para mostrar eficacia por un hecho que generó zozobra, pero ante lo cual no hay respuestas ciertas, aquí se trata de dar seguridades psicológicas a los ciudadanos y de mostrar una autoridades legítimas.

Y el aleccionamiento colectivo a los jóvenes, aunque los detenidos no sean los responsables es la eficacia simbólica y psicológica contra  esos mismos sectores  que se inspiran en el corazón y las ideas de cambios y transformaciones sociales para otra democracia.

El llamado falso positivo judicial es un aleccionamiento a esas nuevas identidades juveniles, que nacen en las universidades públicas y privadas, que nacen con pretensiones de creatividad, aunque a veces padezcan los [clichés ]{.b}de las gramáticas de los adultos.

Lo que hoy lamentablemente ocurre con el Congreso de los Pueblos, ocurrió y ocurre con Marcha Patriótica; sucedió con la Unión Patriótica y A Luchar, con Colombia Unida, con FIRMES, con el Partido Comunista

Sin resolver ese asunto de las mentalidades basadas en la Doctrina de la Seguridad Nacional poco avanzaremos en la construcción de una democracia distinta, pues lo que se penaliza y criminaliza es todo aquello que no opera en las lógicas de consentimiento al poder político, al poder financiero y empresarial dominante.

El derecho a la paz será real solo con una transformación de la mentalidad de los organismos de inteligencia, que construyen a veces partes o todo el acervo probatorio que criminaliza lo que no corresponde a sus categorías doctrinales. Leer lo distinto a lo permitido en nuestra cultura política maniquea, católica fundamentalista, sancionatoria, culposa e inquisitoria, será siempre criminalizado; expresarse y organizarse para disentir sería siempre malo y pecado.

Santos debería rectificar, pero no lo va hacer. Así de claro. Ya sentenció y no se echará atrás. Las víctimas inocentes seguirán hasta tanto esa mentalidad no se transforme, y esta no cambiara hasta que el poder no se transforme, es decir, hasta que no exista otra cultura política

Esperamos la pronta libertad de los integrantes del Congreso de los Pueblos, y de las decenas de líderes sociales de Marcha Patriótica, y otros movimientos sociales, víctimas del uso del aparato judicial como máquina de guerra, el justiciero que opera como guerrero, y el policía que actúa como represor, no como disuador.
