Title: ¿Qué podría pasar con la Ley Estatutaria de la JEP en el Congreso?
Date: 2019-03-11 12:06
Author: CtgAdm
Category: Paz, Política
Tags: "La injerencia de Estados Unidos, Iván Duque, JEP, uribismo
Slug: que-podria-pasar-con-la-jep-en-el-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/CONGRESO-e1483381822139.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón] 

###### [11 Mar 2019]

Con las objeciones del presidente Iván Duque frente a 6 artículos de la Ley Estatutaria de la Jurisdicción Especial para la Paz, el tramite tendrá que regresar al Congreso. Sin embargo, senadores han manifestado que esto podría recaer en un prevaricato, **porque no podrían sancionar sobre algo ya revisado y legislado por la Corte Constitucional.**

El senador Iván Cepeda, del Polo Democrático, señaló que el Congreso de la República ya había debatido cada uno de los artículos de la Ley Estatutaria, incluidos los que el presidente de la República objetó, razón por la cual afirmó que los puntos que se quieren volver a pasar a este espacio deberán tener una evaluación previa que asegure que los congresistas no están cometiendo prevaricato, por **"hacer un debate sobre algo que ya ha sido objeto de una sentencia constitucional". **(Le puede interesar:["Presidente Iván Duque objeta seis puntos de la Ley Estatutaria de la JEP"](https://archivo.contagioradio.com/presidente-ivan-duque-objeta-seis-puntos-la-ley-estatutaria-la-jep/))

"Lo que el presidente presentó al país, como unas objeciones de inconveniencia son un burdo intento por revivir asuntos que la Corte Constitucional ya tocó de una manera bastante clara, en su sentencia con relación a la Ley Estatutaria de la JEP" expresó Cepeda y agregó que, "estamos diseñando una estrategia en el Congreso y vamos a actuar de forma muy decidida en el momento en el que se lleven a discusión, a ese escenario, todos esos asuntos".

### **¿Qué hay detrás de las objeciones de Duque a la JEP?** 

Cepeda manifestó que las intenciones para frenar a la JEP que están detrás de las objeciones de Duque, tienen que ver en alguna medida con "rodear a su jefe de una total impunidad y evitar cualquier tipo de trabajos y labores del Sistema Integral de Verdad Justicia y Reparación, **que pueda esclarecer al país las responsabilidades políticas por tantos crímenes que se han cometido en Colombia"**.

En ese sentido Cepeda celebró las diversas acciones que se han convocado desde la ciudadanía para proteger la JEP y afirmó que es importante recordar que "el Sistema de Verdad, Justicia Reparación y no Repetición es el que va a garantizar que millones de víctimas del conflicto armado puedan tener acceso a la verdad, a la justicia y a una reparación digna".

<iframe id="audio_33278932" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33278932_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
