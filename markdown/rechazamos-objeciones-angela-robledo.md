Title: Ganaremos quienes rechazamos las objeciones de Duque a la JEP: Angela María Robledo
Date: 2019-04-05 16:15
Author: AdminContagio
Category: Paz, Política
Tags: Cambio Radical, ELN, Objeciones, paz
Slug: rechazamos-objeciones-angela-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Los-que-quieren-la-paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @angelamrobledo] 

###### [5 Abr 2019] 

El representante a la Cámara León Fredy Muñoz denunció que el secretario general de la presidencia, Jorge Mario Eastman, estaría llamando a congresistas para pedirles que no asistan a la votación en plenaria de las objeciones a la Jurisdicción Especial para la Paz (JEP) que se realizarán el próximo lunes. Pese a que está acción rompería con la división de poderes establecida, no sorprende a la oposición, que estima tener los votos suficientes para rechazar las objeciones.

> Que poco respeto a la independencia del Congreso y que falta de ética la de [@jorgemeastman](https://twitter.com/jorgemeastman?ref_src=twsrc%5Etfw) Secretario General de la Presidencia. Este funcionario público viene realizando llamadas a Congresistas de distintos partidos para que no asistan a votar las objeciones de la [\#JEP](https://twitter.com/hashtag/JEP?src=hash&ref_src=twsrc%5Etfw). [pic.twitter.com/doxUGXeJl1](https://t.co/doxUGXeJl1)
>
> — León Fredy Muñoz (@LeonFredyM) [4 de abril de 2019](https://twitter.com/LeonFredyM/status/1113901152691720194?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
Como lo relata la Representante a la Cámara Ángela María Robledo, desde la noche del jueves ha circulado información sin confirmar según la cual, Eastman estaría buscando desarmar el quorum en la corporación para evitar que se rechacen las objeciones a la JEP; sin embargo, la acción no resulta extraña si se tiene en cuenta que "el embajador de los Estados Unidos se atrevió, en un desayuno, a casi exigirle" a algunos congresistas que aceptaran las objeciones.

</p>
La Parlamentaria recordó que esta es una práctica común del Gobierno, mediante la cual presiona al Legislativo para obtener resultados que le son favorables. Una muestra de ello fue lo ocurrido con el Plan Nacional de Desarrollo, que fue votado favorablemente gracias a congresistas que, según denunció Germán Vargas Lleras, recibieron 'mermelada' para asistir al debate.   Para la Representante ambos hechos mostrarían la ruptura del compromiso hecho por el Gobierno de mantener la separación de poderes y respetar la autonomía del Congreso, "recurriendo  al todo vale como en la época del patrón de Iván Duque".

No obstante, Robledo confía en que las reuniones internas que han tenido Cambio Radical, el Partido de la U y los liberales logren "neutralizar esta acción del Gobierno".   En ese sentido, la Representante por la Colombia Humana concluyó que el Gobierno probablemente acude a estos mecanismos como forma desesperada ante la posibilidad de que ganen "quienes queremos rechazar las objeciones de Duque a la JEP" por todas sus nocivas implicaciones. (Le puede interesar: ["El PND empobrecería los colombianos: Congresistas de bancada alternativa"](https://archivo.contagioradio.com/pnd-emprobeceria-los-colombianos-congresistas-bancada-alternativa/))

### **Duque logró unir las diferentes  fuerzas de oposición e independientes**

Sobre la "nueva aplanadora legislativa", como se ha denominado a la alianza entre Cambio Radical, el Partido Liberal y el Partido de la U, la Representante manifestó que primero era necesario analizar como funcionaría ese bloque, y a partir de la defensa por la paz encontrar otras iniciativas que los unan. (Le puede interesar:["Adhesión de liberales a Duque es un 'suicidio asistido' del partido"](https://archivo.contagioradio.com/adhesion-de-liberales-a-duque-es-un-suicidio-asistido-del-partido/))

Adicionalmente, Robledo concluyó que es bueno ver al estatuto de la oposición rindiendo frutos ante un "Gobierno débil y autoritario", que dadas sus características, logró lo que hasta hace unos meses no se podía imaginar: Unir a las fuerzas de oposición e indipendientes. (Le puede interesar: ["La iniciativa ciudadana que invita a que \#DefendamosLaPaz"](https://archivo.contagioradio.com/iniciativa-ciudadana-defendamoslapaz/))

### **Seguiremos insistiendo en la necesidad de una paz completa** 

Este viernes se conoció una carta enviada por la iniciativa Defendamos La Paz, en la que pedían al Ejército de Liberación Nacional (ELN) decretar un cese unilateral al fuego a partir del próximo 9 de abril, para demostrar su voluntad de buscar una salida negociada al conflicto. (Le puede interesar:["Comunidades del norte de Antioquia  piden reanudar diálogos Gobierno-ELN"](https://archivo.contagioradio.com/comunidades-del-norte-de-antioquia-piden-reanudar-dialogos-gobierno-eln/))

Ángela Robledo, como integrante de esta iniciativa recalcó que la misma tenía la particularidad de ser una red en la que se reúnen sectores muy diversos, cuyo propósito es buscar una paz completa porque creen que "una negociación es éticamente superior que una relación de vencedores y vencidos".   Por lo tanto, y recordando que los ceses al fuego unilaterales por parte de las FARC fueron claves para destrabar las negociaciones en momentos complicados, Robledo sostuvo que apelaban a "la sensatez y la voluntad de dialogo del ELN" para que den esta muestra de voluntad, al tiempo que es una forma de mostrar que hay un gran sector de la ciudadanía dispuesta a apoyar el proceso.

> Iniciativa [\#DefendamosLaPaz](https://twitter.com/hashtag/DefendamosLaPaz?src=hash&ref_src=twsrc%5Etfw) envía carta al ELN encabezada por negociadores y facilitadores en los diálogos con esa guerrilla. Solicitamos cese unilateral al fuego y de hostilidades lo más amplio posible [\#PorUnaPazCompleta](https://twitter.com/hashtag/PorUnaPazCompleta?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/DEV8Jp5AaS](https://t.co/DEV8Jp5AaS) — Iván Cepeda Castro (@IvanCepedaCast) [5 de abril de 2019](https://twitter.com/IvanCepedaCast/status/1114113372843384833?ref_src=twsrc%5Etfw)

<p>
<iframe id="audio_34129958" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34129958_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
