Title: En 27 de los 32 departamentos de Colombia existe presencia paramilitar
Date: 2016-04-06 14:18
Category: Nacional, Paz
Tags: FARC, Ministro de defensa, Paramilitarismo, proceso de paz
Slug: es-el-paramilitarismo-un-fantasma
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Paramilitarismo1-e1459970325314.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Portal del Sur] 

###### [6 Abr 2016]

Pese a que la propia **Defensoría del Pueblo ha asegurado en 27 de los 32 departamentos de Colombia existe presencia paramilitar,** el Ministro de Defensa, Luis Carlos Villegas aseguró que este fenómeno es un fantasma inventado por las Farc para dilatar el proceso de paz, a lo que esa guerrilla respondió que se trata de “una realidad fehaciente que acaba de lanzar el guante al rostro del pueblo de Colombia”.

A su vez, este martes, [ante la Corte Interamericana de Derechos Humanos, organizaciones sociales evidenciaron la vigencia y fortalecimiento de estructuras paramilitares.](https://archivo.contagioradio.com/ante-la-cidh-se-exige-al-gobierno-enfrentar-fenomeno-paramilitar/) Danilo Rueda, integrante de la Comisión de Justicia y Paz, reseñó por ejemplo, las acciones violentas que se dieron en el marco del reciente [paro armado](https://archivo.contagioradio.com/comunidades-atemorizadas-por-paro-de-autodefensas-gaitanistas/) decretado por las Autodefensas Gaitanistas, el control paramilitar de 82 barrios de [Buenaventura](https://archivo.contagioradio.com/alertan-aumento-de-la-desaparicion-forzada-y-paramilitarismo-en-buenaventura/) pese a la constante militarización del municipio, la presencia de más de 300 paramilitares armados en el [Bajo Atrato](https://archivo.contagioradio.com/?s=Bajo+Atrato), y la libre movilidad de por lo menos 200 hombres armados y vestidos de camuflado en [Putumayo](https://archivo.contagioradio.com/?s=putumayo) a escasos metros de una base militar financiada por los Estados Unidos.

Según la Defensoría, en Cauca, Santander, Putumayo, Antioquia, Arauca, Atlántico, Chocó, Córdoba, Cundinamarca, Meta, Norte de Santander, Sucre, Sur de Bolívar y Buenaventura, se han presentado las acciones de violencia paramilitar durante los tres primeros meses de 2016. Regiones que **coinciden con los departamentos y municipios claves para la implementación de los acuerdos de paz**.

La delegación de paz de las FARC, ha respondido también al ministro de defensa, que esas actuaciones violentas del paramilitarismo tienen un mensaje claro: “intimidar a los amigos de la solución política, mostrarles el país que pretenden una vez desaparezca la insurgencia armada” y citan al informe oficial Basta Ya, donde se cuenta que entre 2003 y 2012, cuando supuestamente ya se habían desmovilizado estos grupos, 2.7 millones de colombianas y colombianos fueron desplazados y expropiados de sus tierras.

Por otra parte, [campesinos reclamantes del tierras en el Magdalena Medio](https://archivo.contagioradio.com/grupos-paramilitares-amenazan-a-reclamantes-de-tierras-en-magdalena-medio/), denuncian que se encuentran atemorizados por la persistencia de grupos paramilitares que se oponen a la restitución de tierras y afirman que la situación ha empeorado desde que **César Augusto Castro Pacheco , alias ‘Tuto Castro’ ex jefe paramilitar, permanece en retención domiciliaria**.

Frente a ese contexto, no solo evidenciado por la guerrilla de las FARC sino por organizaciones sociales, la Defensoría del Pueblo y el Centro de Memoria Histórica, la delegación de paz de la guerrilla señala como una “afrenta” que el Ministro Villegas señale como un “fantasma” la vigencia de grupos paramilitares, “no puede ser aceptada de manera pasiva por ninguna persona decente que habite en el territorio nacional”, dicen las FARC.

Finlamente el comunicado de la guerrilla concluye indicando la importancia de que en la  Mesa de La Habana se logre **concretar el acuerdo sobre  Paramilitarismo y Garantías de Seguridad.** “El mensaje ha de ser claro ante el país y el mundo. Con grupos paramilitares, con crímenes y atentados, con amenazas y terror no puede materializarse la paz. No se trata de tácticas dilatorias como aseguró el Ministro de Defensa, se trata de construir por fin un país distinto, democrático y justo”, finaliza el comunicado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
