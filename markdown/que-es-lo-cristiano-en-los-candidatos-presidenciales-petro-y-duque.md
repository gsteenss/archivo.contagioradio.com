Title: ¿Qué es lo cristiano en los candidatos presidenciales Petro y Duque?
Date: 2018-05-11 10:20
Category: Abilio, Opinion
Tags: Candidatos a la presidencia, Duque, Petro
Slug: que-es-lo-cristiano-en-los-candidatos-presidenciales-petro-y-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/fe-y-politica.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 11 May 2018 

[Franz Hinkelammert ha llamado la atención sobre la ambivalencia de la figura del “creyente en Dios” en razón que la confesión de una fe determinada, no revela realmente en lo que se cree, aludiendo a que son las prácticas, en consecuencia, las que dan cuenta de la fidelidad o no a la creencia.]

[Por lo general,  las grandes tradiciones religiosas se refieren a la importancia de las obras de misericordia que hacen justicia a los más débiles, sin embargo, en la historia se han calificado de cristianas prácticas que van en clara contravía de los evangelios: las cruzadas, la colonización con cruz y espada en América, la persecución a movimientos que han trabajado por construir la justicia social, el respaldo a regímenes dictatoriales responsables de graves violaciones a los derechos humanos.]

[El llamado a las obras, en el caso de la tradición cristiana, está muy presente en los Evangelios:  “por sus frutos los conocerán” (Mat 7, 20), “no solo el que diga el señor entrará en el Reino de los cielos, sino el que haga la voluntad de mi padre celestial” (Mateo 7, 21), y en la figura del juicio apocalíptico definitivo de Mateo 25, 31-46 muestra que quien cree verdaderamente en Dios es  quien da de comer al hambriento, de vestir al desnudo, posada al sin techo, y visita a los presos.]

[En el  Antiguo Testamento, la tradición profética da cuenta de la repugnancia que le produce a  Yaveh el culto externo que está lejos de la justicia, “sus manos están de sangre llenas, límpiense, lávense, quiten sus fechorías delante de mi vista, desistan de hacer el mal, aprendan a hacer el bien, busquen lo justo, den sus derechos al oprimido, hagan justicia al huérfano, aboguen por la viuda” (Isaías 1, 15-17).]

[La actual carrera a la presidencia en Colombia, antes que reflejar fidelidad a la tradición de justicia del Antiguo y Nuevo Testamento,  se ha centrado en la reproducción de mensajes para provocar sentimientos adversos contra la “amenaza de la izquierda” que representa Gustavo Petro.]

[Este discurso  recuerda los días previos a la elección del referendo en la que difuminaron por redes sociales informaciones falsas sobre el acuerdo de paz, que después se supo, por confesión del  gerente de la campaña uribista por el No, Juan Carlos Vélez, respondió a una estrategia engañosa en la que buscó “[que la gente se fuera a votar berraca](https://www.elespectador.com/noticias/politica/cuestionable-estrategia-de-campana-del-no-articulo-658862)”. ]

[Se trata de la estrategia de la “clínica del rumor” practicada  por el asesor electoral JJ. Rendón, quien recientemente dijo que “[haré todo lo posible para que Petro pierda](http://caracol.com.co/radio/2018/04/12/politica/1523543938_732550.html)”.]

[Recientemente  un video se lanzó al aire con  el título “[Con Dios o con el Demonio](https://www.facebook.com/groups/362331653888388/permalink/1639252766196264/)” en el que el centro de los ataques es Petro, pero que también va contra los demás candidatos, exceptuando a Duque.  Repite que  “no tiene temor de Dios”. Como  pruebas de un “pacto satánico de Petro” afirma que su esposa “usa prendas Wuayu”, que tiene “mirada reptiliana” y que habla parecido a Chávez.]

[Sería  una broma  si otro video  titulado “Petro es un demonio jóvenes con Duque iglesia cristiana Barranquilla” no hubiese circulado  en las redes sociales y si Viviane Morales proclamándose cristiana, al igual que algunos líderes del partido Mira y Colombia Justa Libres,  no se hubiesen adherido al candidato de la extrema derecha.]

[En éste segundo  [video](https://www.youtube.com/watch?v=GPXIl_PkdwU), un grupo de jóvenes y adultos  con camisetas de “Duque presidente”  hacen afirmaciones cargadas de animadversión por el candidato de la Colombia Humana, carentes  de todo soporte fáctico y evangélico:]

[“(…)el candidato que haya apoyado el proceso de la Habana está apoyando cosas que no son de Dios… cosas que son totalmente diabólicas”, Petro y los demás candidatos, exceptuando a Duque, “prometen la igualdad social que es anti bíblica, la eliminación de clases va en contra de Dios” “Petro es socio de las Farc y del narcotráfico… y la izquierda es completamente del diablo”, “nosotros estamos a favor de la pena de muerte por que está instituida en la biblia… Petro es un demonio, va a acabar con Colombia”,  “Petro promueve la brujería, promueve la santería promueve el ateísmo”, “estamos con Duque para contrarrestar ese comunismo del diablo”, “no se puede ser cristiano y apoyar la izquierda”, “si tú crees en dios no puedes votar por Petro”, “no votaría por Petro porque se declara plenamente ateo, porque promueve el pecado”, “Petro representa el comunismo, un sistema satánico”.]

[Da esperanza que no todos los  líderes de [los partidos llamados cristianos  se hayan sumado a Duque](https://www.semana.com/elecciones-presidenciales-2018/noticias/division-en-colombia-justa-libres-por-adhesion-a-ivan-duque-566653), quizás por no compartir este tipo de prácticas de deslegitimación que  van en contravía de los propios postulados del Evangelio () y que[Petro afirme  que lo suyo es la practica el cristianismo](https://www.youtube.com/watch?v=uSAdPyCbVFo), de la opción preferencial por el pobre.]

[El llamado es a discernir  por el tipo de opción política de los cristianos en contiendas electorales como la que estamos viviendo en Colombia, teniendo en cuenta  el seguimiento de Jesús en la dirección del bien común, tal como lo afirma el Consejo Mundial de Iglesias en su llamado a “[una revolución política y social” para la  eliminación de la pobreza](https://www.oikoumene.org/es/resources/documents/general-secretary/wider-ecumenical-movement-incl-wcc/ending-extreme-poverty-a-moral-and-spiritual-imperative)“   y el papa Francisco en reunión con jóvenes jesuitas: "[Debemos inmiscuirnos en la política porque la política](http://es.catholic.net/op/articulos/19680/cat/763/es-deber-del-cristiano-involucrarse-en-politica-aunque-sea-demasiado-sucia-asegura-el-papa.html#) es una de las formas más altas de la caridad, porque busca el bien común. Y los laicos cristianos deben trabajar en política".]

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio
