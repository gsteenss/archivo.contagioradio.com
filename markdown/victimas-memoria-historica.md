Title: La ruptura de confianza entre la víctimas y el Centro Nacional de Memoria Histórica
Date: 2019-02-20 14:17
Author: AdminContagio
Category: Memoria, Paz
Tags: Centro Nacional de Memoria Histórica, Dario Acevedo, Ruben Dario Acevedo
Slug: victimas-memoria-historica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Diseño-sin-título3-e1550689696519-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Centro Nacional de Memoria Histórica] 

###### [20 Feb 2019] 

Desde la llegada a la presidencia de Iván Duque, organizaciones sociales y activistas han señalado su preocupación sobre el papel que jugará este Gobierno en la construcción e implementación del proceso de paz; hasta el momento, los cambios que ha realizado el Presidente en torno a las instituciones relacionadas con este tema, el nombramiento de funcionarios y la eliminación de palabras como Posconflicto del discurso oficial comienza a dar sentido a esta preocupación.

Uno de los hechos más recientes tiene que ver con el nombramiento de **Darío Acevedo en el Centro Nacional de Memoria Histórica (CNMH)**, quien en repetidas ocasiones se ha negado a aceptar que exista el conflicto armado en Colombia, similar al discurso de posesión realizado por Ernesto Macías, presidente del Senado, en agosto del año pasado. Esta situación causó la **pérdida de confianza en el Centro de Memoria por parte de víctimas de la masacre de Bojayá, quienes decidieron retirar sus archivos de la Institución**.

### **Lo que pierde el CNMH con la llegada de Acevedo** 

Como lo relata **Leyner Palacios, integrante de iniciativas de memoria del Pacífico**, la relación de las y los bojayaseños con las instituciones del Estado fue siempre muy difícil, puesto que la masacre fue producto también de la omisión de Colombia, en sus organizaciones civiles y militares para cuidar la vida de quienes habitan en Bojayá. Sin embargo, **sus habitantes optaron por trabajar junto al CNMH y confiar en sus funcionarios para hacer memoria sobre lo ocurrido en ese Municipio del Chocó.**

Dicha confianza se ha traducido en la realización de informes, la toma de testimonios sobre lo ocurrido y el desarrollo de una buena relación, que **hasta el momento, había permitido construir una propuesta para la adecuación y organización de un lugar de memoria para Bojayá.** (Le puede interesar: ["Ruben Acevedo, nuevo director del CNMH"](https://archivo.contagioradio.com/ruben-acevedo-es-el-nuevo-director-del-centro-nacional-de-memoria-historica/))

No obstante la confianza construida, a la dirección del Centro llegó oficialmente el martes una persona que niega la existencia del conflicto, hecho que resulta en una incoherencia, porque como explica Palacios **"nos cuesta creer que una persona que desconoce la existencia de un conflicto armado, dirija un centro que buscar construir y guardar la memoria sobre ese conflicto armado"**.

Ante la controversia entre quienes han padecido el conflicto en sus familias y en sus propios cuerpos, y quien llega a dirigir el Centro asumiendo que el conflicto no existió, los primeros han decidido que sus testimonios y vivencias, consignados en archivos que guardaba el CNMH **salgan de allí, previendo que puedan ser usados "para caminar en una dirección guerrerista".** (Le puede interesar: ["Tras 16 años, Bojayá no ha logrado cerrar el capítulo del dolor"](https://archivo.contagioradio.com/bojaya-cerrar-dolor/))

En ese mismo sentido han actuado **las víctimas del genocidio de la Unión Patriótica, quienes también anunciaron el retiro de sus archivos del Centro**; acción a la que podrían sumarse otras organizaciones que trabajan con la Institución.

### **La esperanza depositada en la Comisión para el Esclarecimiento de la Verdad** 

Aunque la mayoría de víctimas de la organización en la que trabaja Palacios sienten que **"la institucionalidad colombiana, con estos nombramientos está cambiando su dirección"** en un sentido que no pueden acompañar, reconocen la necesidad de construir la paz y continuar contribuyendo en la reconciliación y la convivencia; razón por la que tienen puestas sus esperanzas en el Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR).

En una carta firmada por diferentes espacios de memoria en Colombia, las organizaciones señalaban que **los funcionarios relacionados con estas iniciativas debían tener una visión imparcial sobre el conflicto, preparación académica suficiente y un enfoque humanista para relacionarse con las víctimas;** condiciones que, de acuerdo a Palacios, cumplen quienes integran el SIVJRNR. (Le puede interesar: ["Organizaciones piden que el Estado no monopolice la memoria"](https://archivo.contagioradio.com/organizaciones-piden-que-estado-no-monopolice-memoria-historica/))

Por lo tanto las víctimas han tenido reuniones con la Unidad de Búsqueda de Personas Dadas por Desaparecidas (UBPDD) y la Comisión para el Esclarecimiento de la Verdad (CEV) para lograr un marco de entendimiento, adicionalmente **tienen grandes expectativas sobre la Jurisdicción Especial para Paz (JEP)** esperando que la actuación del Sistema en conjunto les permita seguir construyendo la confianza en las instituciones del Estado, y viendo resarcidos sus derechos a la verdad, la justicia y la reparación.

<iframe id="audio_32713103" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32713103_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
