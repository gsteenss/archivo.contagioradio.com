Title: Crónica de una disolución anunciada
Date: 2020-01-28 13:22
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: 21n, Lucha social, Paro Nacional, Paro Nacional Colombia, Paro21E, Protesta social
Slug: cronica-de-una-disolucion-anunciada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/84288655_1308314789354925_1599123002698498048_o.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/record20200127094820.mp3" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/83190368_1308314732688264_820488497570775040_o.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto por: Andres Zea/ Contagio Radio] 

<!-- wp:paragraph {"align":"justify"} -->

El 21 de noviembre de 2019, fue una fecha histórica para la lucha social en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Millones de personas salieron a las calles no solo por un motivo en específico, sino por cien o quizás mil motivos que manifestaban su rechazo general a la forma como Colombia está siendo gobernada, lo que generó una única consigna unificada los primeros 3 días:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

***“que renuncie el presidente de la república y se convoquen elecciones”.*** Consigna corta, certera y aglutinante; en todo sentido un objetivo político.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El 21, 22 y 23 de noviembre fueron las fechas determinantes y más álgidas de aquella situación, pues las masivas marchas de millones de personas, los enfrentamientos en todas las calles de las principales ciudades colombianas tenían en un atolladero a todo el gabinete presidencial y sus fuerzas militares y de policía.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### “Se nos sublevó la gente ¿qué hacemos señor presidente?”

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La respuesta meditada y estratégica del uribismo con pleno conocimiento del tipo de sociedad en la que estamos fue apelar a la represión violenta durante esos tres días, fue engañar sistemáticamente a los habitantes de conjuntos residenciales el 21 de noviembre en Cali y el 22 de noviembre en Bogotá y finalmente ofrecer condiciones (como el uso de recalzadas) para el asesinato de **Dilan Cruz** a manos del **Esmad**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Caracol Televisión alguien salió diciendo *“con que maten a uno eso se calman”*, un alguien que profetizó una táctica que al parecer funcionó.  
Después de la muerte de Dilan, se puso a prueba la moral, la conciencia y el grado de sacrificio de la sociedad que se manifestaba. Prontamente comenzaron a tener más fuerza manifestaciones pacíficas que bajaron la tensión nacional e internacional para el gobierno y lograron estabilizarlo para repensar su estrategia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Claro, un mal gobierno se estabiliza cuando nadie lo ataca contundentemente. Quizás con una moral de batalla diferente, la gente hubiese subido el nivel, es solo que a veces el pensamiento, una obra y gracia de la cultura, también resulta ser una cárcel.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posterior al 23 de noviembre y hasta enero del 2020, las cosas han tomado rumbos eclécticos entre la beligerancia solitaria y descoordinada hasta los intentos de organización popular. Se ha logrado como nunca se logró en la historia, consolidar cuerpos de ciudadanos dispuestos a organizarse para contestar a lo que fue la estrategia rastrera del gobierno: montar una mesa de discusión con el comité nacional de paro, mientras de espaldas al clamor nacional avanzó y aprobó sus proyectos bandera.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### ¿Qué ocurrió?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Se esperó que se consolidara un objetivo político de ese movimiento ciudadano nacido el 21 de noviembre, pero a la fecha aún no parece tenerlo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### ¿Qué ha pasado?

<!-- /wp:heading -->

<!-- wp:paragraph -->

No como conclusión política sino más como actitud (emotiva), se ha gestado la idea de que no exista representación, que nadie sea líder y que no hay que “politizar los espacios”, lo que dificulta la coordinación. Los últimos resultados de esto fueron, por un lado, un bloqueo en la carrera 30 en Bogotá donde los manifestantes, no supieron qué hacer política y tácticamente ante la aparición del secretario de gobierno distrital, quien en un acto proselitista quería negociar con los manifestantes, a sabiendas que esa no es su potestad, **ni el problema por ahora es con el nuevo gobierno distrital, sino con el gobierno nacional**. Y por otro lado, el supuesto paro del 21 de enero, que terminó en una farra-protesta de puta madre en el planetario y algunas escaramuzas en los barrios mientras en Antioquia quitaban las cabezas de las estacas y le daban sepultura a los decapitados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy se piensa siempre en dar argumentos para estar en la calle, antes que responder a las razones políticas para actuar de todas las maneras posibles ante la barbarie que atraviesa el país sea o no en las calles. Se prefiere estudiar hechos que causas de los hechos y en vez de analizar los mecanismos ideológicos del gobierno, se piensa en quedar bien ante los medios de información. No, no se pueden hacer cambios y a la vez caerle bien a todo el mundo. Como le escribiría Silvio Rodríguez a Rubén Blades, es mejor tratar de entender las revoluciones de la historia, no las que soñamos para tranquilizarnos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con línea editorial clara, los periodistas se fijan en preguntar por los argumentos para bloquear, y ponen en duda si en un país como Colombia donde aún decapitan gente es necesario protestar; el caso es que entre unos y otros tal vez no perciben que están varados en un flujo de ideas y respuestas que son necesarias, pero no prioritarias.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### ¿Qué es prioritario?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Constituir un objetivo político, es decir, responder a la pregunta **¿qué equivalencia existe en toda esta diversidad de demandas populares?**  
El movimiento del 21N debería consolidar mediante diálogo ciudadano un objetivo claro y no un memorial de ciento y pico de agravios.  
Como si fuera poco, los manifestantes desde el 23 de noviembre se enfrascaron en debates sobre la violencia y la no violencia antes de preocuparse para qué la violencia, o para qué la no violencia; permitieron que sus debates fueran organizados y dirigidos por lo que decían los medios de información, demeritando que cuando estuvieron las dos opciones sobre la mesa (violencia y no violencia) en conjunto con una única solicitud (la renuncia) fue cuando el gobierno envió tropas del ejército a las ciudades, hubo toque de queda, los medios internacionales tenían en la mira al uribismo, las organizaciones internacionales comenzaron a voltear sus ojos sobre Colombia y en definitiva, **Duque y por lo tanto el uribismo estaban asfixiándose.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Después del 23 de noviembre de 2019, se vieron en las calles acciones artísticas y simbólicas, gente emborrachándose y drogándose para luego gritar en la madrugada *“viva el paro nacional”* y una primera línea que no estudió a Alejandro Magno por lo que fue coherentemente atropellada por el Esmad en su primera aparición; todas, acciones claramente diferentes en la intención, pero iguales en algo:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Bajaron la beligerancia, dejaron respirar al gobierno uribista.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy es claro que se necesita un objetivo político. Es decir, encontrar una equivalencia entre toda la diversidad de las demandas, una consigna contundente, bastante seria, que tenga implicación nacional e internacional y que convoque a los manifestantes no solo a expresar una emoción de rabia o rechazo al gobierno, sino a estudiar y evaluar las posibilidades de acción política.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Será natural que, en el tránsito por la consecución del objetivo político, muchas personas queden en el camino, personas que querían divertirse un rato, personas que en realidad no quieren que nada en sus vidas cambie, quedarán en el camino filántropos, intelectuales de sillón, politiqueros ortodoxos, pesudo-anarquistas, y gente cool anti-gobierno que en realidad no estaría dispuesta más que a fumarse un porro o tomarse una polita en busca de “la libertad”; quedarán en el camino aquellos que no sean capaces de sacrificar un poquito de su individualidad perpetua por la posibilidad de construir un proyecto político colectivo. Lo anterior, no aislará a los que queden, sino que los distinguirá y el apoyo popular comenzará a aumentar ante la claridad del objetivo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un movimiento social sin objetivo político puede tener un apoyo multitudinario pero efímero, ya que en muchas ocasiones se ha generado un fenómeno sociológico que podría llamarse “el consumo de la desobediencia”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Con un objetivo político, puede haber apoyo paulatino y en crecimiento pero consciente, determinado y duradero.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin el objetivo político, el gobierno ya ha visto que un movimiento segmentado no le hará ni un poquito de daño y de continuar así, por ejemplo, el riesgo de una disolución definitiva del movimiento del 21N es alto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin objetivo político, el movimiento del 21N andará más preocupado porque sus integrantes “no se aburran”, que por estudiar y analizar las estrategias que arrojarán a todos a compartir discursos, actitudes y acciones en pro de lo acordado, no por gusto, sino por convicción política.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin el objetivo político, tanto las acciones pacíficas como las acciones contundentes no tendrán un apoyo popular; se verán sectarias, necias y le darán créditos por ejemplo al gobierno distrital para que diga “¿si ve? yo quise hablar”, cuando el problema fundamental no es con el distrito.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin el objetivo político se consignará un memorial de agravios, surgirán opiniones diversas (importantes o irrelevantes) pero no se logrará sacar un solo comunicado que contenga elementos discursivos que como mínimo ajusten la seriedad de lo que se está haciendo y el esfuerzo histórico de muchas personas que se han reunido espontáneamente en las localidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin el objetivo político, la estrategia ideológica del gobierno triunfará, porque acaparará la atención del ciudadano que observa la contienda y sustentará que los manifestantes no tienen argumentos para acciones inconexas y aisladas, que solo quieren “joder a la ciudadanía”. Porque recordemos: son millones los que observaron la contienda, son muchos y no se trata de que no les guste la violencia o de que no les guste el pacifismo a la hora de protestar, lo que no le gusta a la gente es no ganar nada protestando por cualquiera de las dos formas y en cambio sí resulten ganando más cuando aceptan la injustica del país, cuando callan y aceptan las condiciones inequitativas en las que viven, cuando callan la impunidad o cuando escogen el “menos peor”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Que no vengan los pacifistas moralistas a decir que la sociedad colombiana rechaza la violencia, cuando tienen metida en las venas la cultura traqueta, veneran las series de narcos, o al reggaetonero que más mercantiliza a la humanidad con sus letras y su estética. Que no vengan los pacifistas de derecha con sus cuentos de la no violencia cuando aceptan legítima y legalmente el asesinato de unos, pero se revuelcan en el piso de indignación cuando les tronchan los dedos a otros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En fin, sin el objetivo político, se disolverá una oportunidad histórica y eso es grave.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: [El salto ideológico](https://archivo.contagioradio.com/el-salto-ideologico/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy la línea editorial de los medios informativos quiere proponer discursivamente la masacre de personas que continúa en Colombia como una “gran pena” “como una situación triste” (muy emotiva) pero nada más. No quieren otorgarle la gravedad política nacional e internacional porque eso desestabiliza al gobierno uribista. Si semejante barbarie estuviera ocurriendo bajo los ojos de gobiernos de izquierda, sería mostrada día y noche como la catástrofe más hijueputa. ¿quieren cantar cancioncitas para que paramilitares que decapitan personas pagados por cárteles mexicanos y con el permiso institucional de algunos organismos del Estado al mando de políticos y empresarios, dejen de hacerlo? ¡adelante! Buena suerte con ello.  
Por el momento diré que es grave pero predecible que se disuelva ese movimiento nacido el 21N. Es grave porque grave es la situación que sigue viviendo el país… es predecible, porque hay gente que solo quiere divertirse y otros que su individualidad ocupa más espacio que 5 litros de sangre.

<!-- /wp:paragraph -->

<!-- wp:audio {"id":79924,"align":"center"} -->

<figure class="wp-block-audio aligncenter">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/record20200127094820.mp3">
</audio>
</figure>
<!-- /wp:audio -->
