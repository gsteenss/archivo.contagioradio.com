Title: Tras 27 años, Masacre La Rochela continúa en la impunidad
Date: 2016-01-19 17:39
Category: Judicial, Nacional
Tags: Masacre La Rochela, Paramilitarismo
Slug: masacre-la-rochela-continua-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/rochela.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.elmercuriodigital.net]

<iframe src="http://www.ivoox.com/player_ek_10139149_2_1.html?data=kpWelZ6VeJqhhpywj5aUaZS1kZaah5yncZOhhpywj5WRaZi3jpWah5yncbXmwtiYlJyPpYa3lIqmk9TXaZO3jNLO1cbHtsafzcaY1NTHrMbgwpDQ0dPYrc%2BZpJiSm6bFb8bijNHOjc7RtNbijoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Yomary Ortegón, CAJAR] 

###### [19 Enero 2015]

Este lunes se cumplieron 27 años de la masacre de la Rochela, en el departamento de Santander, cuando el 18 de enero de 1989, **12 de 15 funcionarios judiciales fueron asesinados a manos de un grupo paramilitar junto a miembros del Ejército Nacional.**

En el marco de la conmemoración de esta masacre, el Colectivo de abogados José Alvear Restrepo y el Centro por la Justicia y el Derecho Internacional, presentaron  un informe ante la Corte Interamericana de Derechos Humanos, con sus observaciones y seguimiento al caso, teniendo en cuenta que han sido muy pocos los avances del Estado colombiano para llegar a la verdad, justicia y reparación de las víctimas.

Según el informe, “9 años después de la sentencia de la CIDH que condenó al Estado por estos hechos, **no hay avances significativos en materia de justicia, mientras que en materia de reparación integral medidas como becas estudiantiles, oportunidades laborales y atención en salud para las familias de las víctimas no se han cumplido”.**

La justicia es uno de los ejes de la sentencia de la CIDH que más ha incumplido el Estado, pues la masacre fue cometida **por un grupo de 40 paramilitares, conocidos como “Los Masetos” que actuaron en asocio con agentes estatales,** sin embargo la única condena por estos hechos es la que cumplió a mediados de 2015, el paramilitar Alonso de Jesús Baquero, Alias “el negro Vladimir”, quien señaló a los militares Farouk Yanine Díaz, Juan Salcedo Lora, Carlos Julio Gil Colorado y Alfonso Vacca, y al ex congresista Tiberio Villarreal Ramos, de quienes aún no se tienen investigaciones concretas para ser acusados.

Así mismo, continúa reinando la impunidad en el caso, frente a los procesos contra el comandante de las AUC, **Ramón Isaza Arango,** comandante de las AUC, quien ha aceptando algún tipo de responsabilidad por cadena de mando, y también contra el exjefe paramilitar **Iván Roberto Duque**, que ha solicitado cesación del proceso penal por vencimiento de términos, cuya solicitud se encuentra pendiente por resolver.

Otra de las observaciones que se hacen a la CIDH, tiene que ver con el riesgo en el que se encuentran los familiares y los testigos del caso, ya que l**a Unidad Nacional de Protección no ha cumplido con los compromisos en materia de protección** ordenadas por el Tribunal Interamericano.

A su vez, aseguran que el Estado no ha cumplido con las becas educativas, oportunidades laborales y otras medidas que reparaban económicamente a las esposas e hijos de los funcionarios judicializados.

Finalmente, “se solicita a la Corte Interamericana mantener la supervisión de las medidas de reparación ordenadas en la sentencia de 2007 y requerir al Estado colombiano información detallada en relación con **su obligación de investigar, juzgar y sancionar los hechos conforme a lo ordenado en la sentencia**”, dice el informe.

[Masacre de La Rochela - Observaciones Informe Estatal 150115](https://es.scribd.com/doc/296018854/Masacre-de-La-Rochela-Observaciones-Informe-Estatal-150115 "View Masacre de La Rochela - Observaciones Informe Estatal 150115 on Scribd")

<iframe id="doc_66540" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/296018854/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>  
Reciba toda la información de Contagio Radio en su correo <http://bit.ly/1nvAO4u> o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio.
