Title: Caravanas de estudiantes avanzan hacia gran movilización por la educación
Date: 2018-11-23 17:42
Author: AdminContagio
Category: Educación, Movilización
Tags: Caminantes por la Educación, Paro Nacional, UNEES
Slug: caravanas-de-estudiantes-avanzan-hacia-gran-movilizacion-por-la-educacion
Status: published

###### [Foto: Contagio Radio] 

###### [23 Nov 2018]

Otras dos **caravanas de estudiantes se dirigen hacia la capital desde los departamentos de Santander y Antioquia**, con la intención de sumarse a los más de 80 "caminantes" que ya se encuentran en Bogotá, provenientes de las ciudades de Ibague, Villavicencio y Florencia, para hacer parte de la gran movilización del Paro Nacional el próximo 28 de noviembre.

En total son 320 personas que partieron en 6 delegaciones las que se espera que lleguen el próximo lunes a la capital. Algunos de los estudiantes hacen parte de universidades como la Industrial de Santander, la Universidad de Antioquia, entre otras. (Le puede interesar: ["Estudiantes de Uni.Cauca están acordonados por más de 150 integrantes del ESMAD")](https://archivo.contagioradio.com/estudiantes-de-uni-cauca-estaria-acordonados-por-mas-de-150-integrantes-del-esmad/)

**Los obstáculos que han tenido que superar los "Caminantes" por la Educación**

De acuerdo con Cristian Guzmán, vocero de la UNEES, las personas que han hecho parte de estas movilizaciones nacionales han tenido que afrontar varias adversidades, **entre ellas están las denuncias que hacen de seguimientos por parte de la Fuerza Pública.**

Asimismo, las y los estudiantes que llegaron a Bogotá han tenido dificultades de salud como deshidratación, insolación y cansancio extremo, producto de las largas caminatas que han emprendido. Razón por la cual la UNEES adelanta una "donatón" para recolectar elementos de aseo, alimentación, ropa y cobijas que permitan atender de mejor forma tanto a las personas que ya están acá como a los que vienen.

### **¿Qué ha pasado con la mesa de conversaciones?** 

Frente a el avance de la mesa de conversaciones con el gobierno Nacional, Guzmán afirmó que aún falta mucha voluntad y que las propuestas que salen desde Duque, como el hecho de que las personas que quieran realicen donaciones a la Educación en su declaración de renta, continúan siendo dañinas para la clase media, **en vez de cobrar mayores impuestos a multinacionales, empresas o personas naturales con mayor capacidad adquisitiva**.

De igual forma señaló que el próximo 2 de diciembre se está citando a una asamblea de urgencia del UNEES para diálogar con todo el estudiantado sobre lo que ha pasado en la mesa de conversaciones y de esa forma tomar decisiones sobre las propuestas que realice el gobierno y el paro Nacional. (Le puede interesar:["Las 3 condiciones de los estudiantes para levantar el paro nacional"](https://archivo.contagioradio.com/condiciones-levantar-paro-nacional/))

###### Reciba toda la información de Contagio Radio en [[su correo]
