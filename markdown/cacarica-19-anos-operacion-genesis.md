Title: CAVIDA, 19 años de resistencia, memoria y dignidad
Date: 2016-03-03 18:22
Category: Otra Mirada, Reportajes
Tags: cacarica, cavida, Operacion genesis
Slug: cacarica-19-anos-operacion-genesis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Caminata-por-la-paz-66.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotos: Gabriel Galindo, Contagio Radio ] 

###### [3 Mar 2016 ] 

“Nos sacaron a la fuerza con bombas y metralletas” entona Augusto, “la gente se levantaba pa’ sus tareas cotidianas, cuando en el cielo escuchamos ruidos de los pájaros metales que venían a su destino a sacar al campesino”, continúa, toma un poco de aire y sigue cantando... “Ya siendo la hora siete, esto si me dolió a mí, cuando enseguida explotaban las bombas en el Salaquí de los aviones Kfir que allá rápido volaban cumpliendo con su misión, pa’ acabar la población”.

Hace 19 años llegó la muerte al territorio colectivo del Cacarica, ubicado en el departamento del Chocó, limites con Panamá. La operación génesis, planeada por paramilitares del bloque Elmer Cárdenas, de las autodefensas campesinas comandado por “El alemán”, y la brigada 17 del ejército, al mando Rito Alejo del Río, obligó a que más de 5000 afrodescendientes tuvieran que abandonar su territorio entre el 24 y el 28 de febrero de 1997.

Durante estos años las comunidades negras del Bajo Atrato, como las de Cacarica han aprendido que la fuerza también está en la solidaridad, por eso, han convocado a cientos de personas provenientes de España, Canadá, Alemania, Estados Unidos, Suiza, Chile, Bogotá, Buenaventura y Medellín a que los acompañen, a la peregrinación por la Paz con Justicia Socio Ambiental con la que se conmemora no sólo esta trágica fecha, sino también la resistencia de las comunidades, materializada en sus apuestas de construcción de paz.

Así fue la peregrinación...

<iframe src="http://co.ivoox.com/es/player_ej_10657192_2_1.html?data=kpWjl5yVfZOhhpywj5acaZS1lJaah5yncZOhhpywj5WRaZi3jpWah5yncaS1t66xo4qWh4ylmpDOh6iXaaOl0NiYxsqPtsbnytjhx9PHrcKZk6iYz8rRs9PdwpDmjcnNq8%2FdxcbRj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

\
