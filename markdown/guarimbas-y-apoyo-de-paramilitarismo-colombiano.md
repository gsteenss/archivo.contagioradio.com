Title: Guarimbas y apoyo de paramilitarismo colombiano
Date: 2014-12-17 16:53
Author: CtgAdm
Category: Hablemos alguito
Tags: Guarimbas, Venezuela
Slug: guarimbas-y-apoyo-de-paramilitarismo-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/footer-logo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fscJix7T.mp3)

Conversamos con las defensoras de derechos humanos Marieva Caguaripano y Ana Barrios activistas del colectivo SURDH. El colectivo nace de la preocupación de miembros de las tradicionales organizaciones de DDHH venezolanas e internacionales por la cobertura de la violencia política desatada a partir de Febrero de 2014, totalmente parcializada, distorsionada e instrumentalizada a favor de la opción política que buscaba la salida del presidente electo. Desde el colectivo opinan que los derechos humanos son de por sí un concepto ampliamente político. Así se posicionan como organización de izquierdas y con un carácter crítico respecto a la temática.  
Contextualizandonos el transcurso de la crisis vivida, nos describen como las violaciones de DDHH han sido sistemáticas por ambas partes, pero puntualizan como la información ha sido en muchas ocasiones manipulada en contra del gobierno. Por otro lado conversamos sobre la situación más grave, es decir la del estado Táchira en el que se encuentra la ciudad fronteriza con Colombia de San Cristóbal, donde se han producido los peores enfrentamientos. También investigamos sobre la colaboración del paramilitarismo colombiano en las ¨Guarimbas¨. Por último realizamos conjuntamente un análisis de medios con el objetivo de valorar la influencia de estos en la opinión dentro y fuera del país.

\[tags\]
