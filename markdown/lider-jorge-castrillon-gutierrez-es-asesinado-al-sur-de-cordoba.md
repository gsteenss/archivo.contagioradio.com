Title: Líder social Jorge Castrillón Gutiérrez es asesinado al sur de Córdoba
Date: 2019-02-01 17:30
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: asesinato de líderes sociales, Sur de Córdoba
Slug: lider-jorge-castrillon-gutierrez-es-asesinado-al-sur-de-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Jorge-Herney-Castrillón-Gutiérrez-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Facebook 

###### 1 Feb 2019 

El pasado 31 de enero fue asesinado el líder social y político Jorge Herney Castrillón Gutiérrez, precandidato a la Alcaldía de San José de Uré, al sur de Córdoba, una región donde la confrontación de grupos armados irregulares como el Cartel de Sinaloa y el Clan del Golfo han dividido el territorio y ahora podrían interferir en los comicios. Con su muerte  la cifra de líderes asesinados entre 2016 y 2019 en el sur de Córdoba asciende a 28.

Según la información recolectada, el líder social desapareció desde el día miércoles cuando se desplazaba hacia una vereda cercana a la cabecera municipal. Su cuerpo fue hallado, con varios impactos de arma de fuego, al día siguiente en un potrero del corregimiento de Buenavista a tres o cuatro horas de San José de Uré.

### **Organizaciones sociales ya habían advertido de amenazas a líderes de la región** 

**Andrés Chica, integrante de la Asociación Campesina para el desarrollo del Alto Sinú ASODECAS** explica que incluso al lanzarse cinco alertas tempanas sobre amenazas contra líderes sociales y políticos desde 2018 en la región, la respuesta del Gobierno se ha limitado a aumentar el número de uniformados en la zona, “más pie de fuerza nos pone en más riesgo, y nos da más miedo porque siempre al ver a un soldado vemos a alguien que nos amenaza en el territorio por todo lo que ha pasado” afirma.

Chica explica que el sur de Córdoba está rodeado de grupos armados y que en particular las veredas están confinadas pues existen fronteras invisibles a raíz del conflicto **entre estructuras neoparamilitares como los Caparrapos, el Clan del Golfo, una creciente fuerza armada que se autodenomina el Nuevo Frente 18 y la continua presencia del Cartel de Sinaloa**, todos con intereses en el narcotráfico y la minería ilegal.

Aunque dichas organizaciones nunca cesaron sus actividades delictivas, el integrante de ASODECAS asegura que su accionar disminuyó durante el proceso de Paz con las FARC, pero que desde enero de este año la violencia ha alcanzado un punto crítico, una situación que se evidencia **en municipios como Montelíbano donde “están asesinado a dos personas diarias de manera selectiva”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
