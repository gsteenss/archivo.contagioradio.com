Title: Plazas Vega habría llamado a oficiales en retiro a ganar la "Guerra política"
Date: 2018-02-13 17:07
Category: Nacional, Política
Tags: ACORE, plazas vega
Slug: plazas-vega-elecciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/plazas_vega_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Caracol 

###### 13 Feb 2018 

En un audio, conocido por Contagio Radio, se escucha lo que habría sido el discurso que el Coronel (R) Alfonso Plazas Vega, a un grupo de cerca de 60 oficiales retirados, en el que **hace un llamado efusivo a ganar lo que él denomina “Guerra política”**  que de perderse amenazaría las garantías para el retiro de los uniformados y la estabilidad del país.

“Si no ganamos la guerra política miren a ver que taxi compran” asegurando que los militares **perderán sus derechos adquiridos por que los consiguieron bajo un sistema democrático**, sentenciando que “en el momento en el que el sistema cambie y pasemos a un sistema comunista los derechos adquiridos ya no existen” y que se acabará el sueldo de retiro de manera paulatina a punta de impuestos.

Plazas Vega asegura que habrá en el país un nuevo ejército que “estará en manos, como en El Salvador, de la guerrilla” afirmando que serán los guerrilleros quienes ocupen el lugar en el que están ubicados los oficiales; y que  se equivocan aquellos que no asistieron a su convocatoria al pensar que el país "no caerá en manos de las FARC" por los hechos en los que algunos candidatos de ese partido fueron agredidos en las recientes semanas. **Según Plazas Vega el verdadero candidato de la izquierda es Gustavo Petro** advirtiendo que llenó la plaza  de Popayán "hasta las banderas (...) de personas como las que no vinieron aquí"

De manera enfática **calificó a los militares que no hacen parte de la guerra política como “Apátridas**” asegurando que están faltando su juramento de bandera. Y afirma que los oficiales en retiro no deben olvidar lo que ocurre con los militares y policías activos en el servicio, para los cuales **afirma el gobierno Santos no ha construido bases sino cárceles.** (Le puede interesar:[Ejército deberá responder por torturas a 11 personas durante retoma del Palacio de Justicia](https://archivo.contagioradio.com/torturas_palacio_justicia_retoma/))

“Si nosotros perdemos las elecciones el próximo 11 de marzo Colombia se pierde, estamos colgados de la brocha como dice el refrán popular, si nosotros no ganamos las elecciones ampliamente en el mes de marzo, que eso si nos va a levantar la moral para tener fuerza para el mes de mayo, **créanme que esto corre el riesgo de perderse**” advirtió el militar.

Por último hace un llamado para apoyar un referendo que acabe con la JEP amenazando con que es “**la que nos va a meter presos a todos los que estamos aquí, todos aquí serán víctimas de la JEP** (...) ninguno de ustedes están exentos ni sus hijos, **para los comunistas el enemigo no solamente son ustedes que los enfrentaron, los enemigos también son nuestras mujeres y nuestros hijos**, ¿quieren luchar por el futuro de sus hijos?, empiecen por ahí porque los van a perder si no ganamos.

<iframe id="audio_23745277" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23745277_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
