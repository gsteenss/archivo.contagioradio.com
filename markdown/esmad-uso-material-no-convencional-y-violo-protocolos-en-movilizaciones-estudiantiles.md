Title: ESMAD usó material no convencional y violó protocolos en movilizaciones estudiantiles
Date: 2019-10-18 13:01
Author: CtgAdm
Category: Estudiantes, Movilización
Tags: ESMAD, estudiantes, heridos, Universidades
Slug: esmad-uso-material-no-convencional-y-violo-protocolos-en-movilizaciones-estudiantiles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-18-at-12.46.29-PM-e1571421549155.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Nuevamente, estudiantes denunciaron que durante las jornadas de protestas del pasado jueves, se presentó uso excesivo de la fuerza por parte del Escuadrón Móvil Antidisturbios (ESMAD). Según defensores de derechos humanos, **solo en Bogotá se registraron cerca de 10 personas heridas y 16 detenidas de forma arbitraria**, mientras en la Universidad del Atlántico se reportó un nuevo herido en su ojo, producto de la acción del Escuadrón.

### **Las cifras de capturas y heridos en las diferentes Universidades** 

A raíz de la aprobación del artículo 44 en el próximo Presupuesto General, según el cuál "las universidades estatales pagarán las sentencias o fallos proferidos en contra de la Nación con los recursos asignados por parte de esta", estudiantes en diferentes ciudades capitales decidieron salir en una jornada de protesta, tras las cuales se presentaron nuevas acciones violentas por parte del ESMAD. (Le puede interesar: ["Más de 25 heridos tras la intervención del Escuadrón Móvil en la movilización estudiantil"](https://archivo.contagioradio.com/heridos-intervencion-esmad-movilizacion-estudiantil/))

> ?URGENTE? [\#DDHH](https://twitter.com/hashtag/DDHH?src=hash&ref_src=twsrc%5Etfw)  
> Ayer durante la protesta pacífica realizada en la UCundinamarca sede Fusagasugá, una estudiante resultó herida debido a la detonación de una granada aturdidora en su rostro
>
> Exigimos al gobierno nacional las respectivas respuestas [\#DuqueRespeteLasUniversidades](https://twitter.com/hashtag/DuqueRespeteLasUniversidades?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/1wOdgrshjX](https://t.co/1wOdgrshjX)
>
> — Colectivo José Martí (@cjosemarti) [October 18, 2019](https://twitter.com/cjosemarti/status/1185221932670750720?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Según **Valentina Ávila, integrante de la Red Universitaria Distrital de Derechos Humanos**, las protestas se vieron nuevamente agredidas y manejadas de forma irresponsable por parte de la Policía, "que nuevamente realizaron procedimientos irregulares, que no están cumpliendo con los protocolos de intervención, son injustificados y hacen uso de material no convencional". Respecto de esta última denuncia, Ávila afirmó que en la intervención contra la Universidad Distrital, en la sede de la Calle 40, el Escuadrón usó "recalzadas, granadas aturdidoras que contenían pilas, tornillos y canicas", además de ser apuntadas y disparadas de forma directa a la cara y cuerpo de los estudiantes.

> [\#SeValeProtestar](https://twitter.com/hashtag/SeValeProtestar?src=hash&ref_src=twsrc%5Etfw) // [\#Denuncia](https://twitter.com/hashtag/Denuncia?src=hash&ref_src=twsrc%5Etfw) [\#Bogotá](https://twitter.com/hashtag/Bogot%C3%A1?src=hash&ref_src=twsrc%5Etfw) Estudiantes de la ASAB son fuertemente reprimidos por el ESMAD. Uso desmedido de la fuerza. [pic.twitter.com/Z8XQRIs5fB](https://t.co/Z8XQRIs5fB)
>
> — ContraPortada (@ContraPortadaCA) [October 17, 2019](https://twitter.com/ContraPortadaCA/status/1184948376539992071?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Producto de esta situación, se presentaron 16 detenidos arbitrariamente, de los cuales 4 eran menores de edad y 1 defensor de DD.HH. Asimismo, las jornadas dejaron como saldo a 10 personas heridas, entre ellas, 1 integrante de la Red Distrital que fue arrollado por una camioneta mientras acompañaba la movilización de la Universidad Nacional de Colombia, sede Bogotá. Por su parte, en Barranquilla, estudiantes de la Universidad del Atlántico denunciaron que mientras los estudiantes se manifestaban, el ESMAD atacó a un joven, causándole graves heridas en uno de sus ojos.

> [\#SeValeProtestar](https://twitter.com/hashtag/SeValeProtestar?src=hash&ref_src=twsrc%5Etfw) // [\#Barranquilla](https://twitter.com/hashtag/Barranquilla?src=hash&ref_src=twsrc%5Etfw) En medio del plantón pacífico que desarrollaban los estudiantes de la Universidad del Atlántico, resultó herido un estudiante luego de los indiscriminados ataques del ESMAD. [pic.twitter.com/ERDiNcP5TZ](https://t.co/ERDiNcP5TZ)
>
> — Andy Kaoz Ph (@AndyKaozPh) [October 17, 2019](https://twitter.com/AndyKaozPh/status/1184930657509826562?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **El ESMAD no actúa respetando los DD.HH., ¿quién lo controla?** 

Ávila aseguró que en este año, sobre todo en los últimos meses, se ha visto un recrudecimiento de la violencia y represión por parte de la fuerza pública contra lo estudiantes, que han insistido en que el ESMAD no tiene la formación en DD.HH., como debería tenerla para actuar de forma adecuada en el marco de la protesta social. No obstante, recordó que este cuerpo recibe directrices de los gobernantes, por lo que valdría la pena preguntarse si en el caso de Bogotá, la secretaría de seguridad no ha querido dar la orden para que se hagan procedimientos rigurosos y respetuosos de la dignidad de las personas.

La defensora de DD.HH. afirmó que tampoco han recibido una respuesta adecuada por parte de los entes de control, como la Procuraduría o la Defensoría del Pueblo, encargados de velar por los derechos de la ciudadanía. De hecho, resaltó que en muchas ocasiones han tenido que acudir al llamado de la ONU, como única entidad con capacidad (y voluntad) de velar por la protección de los manifestantes. (Le puede interesar: ["Como falsos positivos judiciales, califican capturas de estudiantes en Pereira"](https://archivo.contagioradio.com/como-falsos-positivos-judiciales-califican-capturas-de-estudiantes-en-pereira/))

Por último, la estudiante señaló que la **Personería sí ha realizado oficios en los que asegura que los uniformados no están cumpliendo con su reglamento y fueron enviados al comandante de Policía en Bogotá, Hoover Penilla, pero no han recibido respuesta**. Ello quiere decir, que "ni siquiera la incidencia del ministerio público está siendo atendida por parte de la Fuerza Pública", lo que resulta grave porque "lo que está en riesgo en realidad es la vida misma de los estudiantes". (Le puede interesar: ["Unidades antidisturbios de países democráticos no pueden patear la constitución: Monedero"](https://archivo.contagioradio.com/unidades-antidisturbios-de-paises-democraticos-no-pueden-patear-la-constitucion-monedero/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_43274808" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_43274808_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
