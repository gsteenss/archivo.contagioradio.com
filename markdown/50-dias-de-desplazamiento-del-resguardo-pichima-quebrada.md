Title: 50 días de desplazamiento del resguardo Pichimá Quebrada
Date: 2019-07-23 16:38
Author: CtgAdm
Category: Comunidad, Nacional
Tags: comunidades, conflicto armado, Desplazamiento, Líderes indígenas, pichima quebrada, Resguardo
Slug: 50-dias-de-desplazamiento-del-resguardo-pichima-quebrada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB8780.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

 

Se han cumplido 50 días desde que 417 personas salieron desplazadas del resguardo indígena Pichimá Quebrada, en Litoral del San Juan (Sur Chocó). Este segundo desplazamiento que sufren los Wounaan, fue producto de los enfrentamientos entre los grupos armados que se encuentran en la zona, ELN y disidencias de las FARC. (Le puede interesar: **[Pichima Quebrada, dos veces desterrada por la guerra](https://archivo.contagioradio.com/pichima-dos-veces-desterrados-por-la-guerra/)**)

> ** “Estamos ahí por el conflicto armado, por el enfrentamiento que hubo por los dos grupos armados, no tuvimos otra opción que salir”**

A partir de ese momento, las personas se han asentado en la cabecera municipal de Santa Genoveva de Docordó, en dos albergues brindados por parte de la Alcaldía y uno por la Casa Comunitaria que no cuentan con las condiciones necesarias para ser habitados.

Según dos voceros de los Wounaan, los habitantes de Docordó les han manifestado que no los quieren en sus territorios, sin embargo no encuentran otras soluciones por parte de las autoridades, “en el momento no nos hemos sentido bien, siempre hemos estado atrás de la administración para que nos arregle las cocinas, el techo… las casas que nos han dado están en malas condiciones, no tiene baterías sanitarias… por lo que tenemos que acudir al río".

Asimismo, a la fecha no tienen ningún plan de retorno a su resguardo, debido a que no cuentan con las garantías de seguridad para regresar, sin volver a ser víctimas de las confrontaciones armadas de los grupos que permanecen en el lugar.

En este sentido, la comunidad de Pichimá Quebrada, el 4 de junio, realizó la declaración acerca de su situación de desplazamiento ante la Personaría Municipal y el Ministerio Público, pero, desde entonces, no se han tomado medidas. Los líderes expresaron  que lo difícil para ellos "es la situación de la cocina para la preparación de alimentos, y los niños que en el territorio ellos tenían espacio para desplazarse…”.

Por tanto, a partir del 16 de julio representantes de la comunidad Wounaan llegaron a Bogotá para exponer su situación ante la Defensoría del Pueblo, la Personería y la Unidad de Víctimas, entre otras. En primera medida, para visibilizar su situación de desplazamiento, y evidenciar las condiciones actuales en la cabecera municipal donde están ubicados.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F493416528096212%2F&amp;show_text=0&amp;width=560" width="560" height="308" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Asimismo, pretenden mostrar su Plan de Retorno ante el Gobierno Nacional, el cual incluye a grandes rasgos temas de salud, educación y vivienda. La comunidad espera que las entidades estatales asuman responsabilidad en su plan de retorno, y sobre todo garantías de seguridad y no repetición de este tipo de violencia.

> ** “Cada vez que nos preguntan cuándo vamos a regresar al territorio, tenemos como respuesta que no lo sabemos, porque no sabemos qué entidades nos van a garantizar el regreso”**

### **Primer hecho de desplazamiento que enfrentó el resguardo indígena** 

Los voceron Wounnan ya habían sido víctimas de desplazamiento, "ya tenemos esa experiencia del 2016 donde estuvimos desplazados en la cabecera municipal, y la comunidad, dada las precarias condiciones, tuvieron la voluntad de regresar al territorio, pero sin ninguna garantía”, por tanto, el temor de los habitantes es la repetición de estos hechos. (Le puede interesar : **[500 indígenas Wounaan del resguardo Pichimá permanecen confinados por enfrentamientos armados](https://archivo.contagioradio.com/indigenas-wounan-del-resguardo-pichima-permanecen-confinados-por-enfrentamientos-armados/)**)

Este desplazamiento sucedió en el 2016, aproximadamente 136 personas debieron dirigirse y asentarse en la cabecera municipal, otras 162 se quedaron en el territorio en medio de enfrentamientos entre un grupo armado y el Ejército Nacional. El hecho fue denunciado por la Organización Nacional Indígena de Colombia en conjunto con la consejería de los Derechos de los Pueblos indígenas.

Los Wounnan esperan que no se repitan estos hechos, donde sin ninguna garantía para su seguridad los habitantes regresaron al territorio, y esperan que el Gobierno Nacional, junto con las  autoridades locales cumplan con el Plan de Retorno que plantearon.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38879453" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38879453_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
