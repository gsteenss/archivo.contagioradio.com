Title: La matriarca afro de las selvas colombianas
Date: 2017-07-25 15:51
Category: Mujer, Nacional
Tags: Defensa de derechos de las mujeres, Mujer negra
Slug: la-matriarca-afro-de-las-selvas-colombianas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Ligia-Chaverra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo/Contagio Radio 

#### **Por Mónica Lozano ** 

###### 25 Jul 2017 

[Han dicho de ella que es guerrillera, la han querido judicializar, asesinar y desplazar. Lo último lo lograron y en ese hecho se ha centrado su lucha y resistencia. Con 75 años de edad, Ligia María Chaverra, conocida como la matriarca de la cuenca del Curvaradó, ubicada en las selvas chocoanas de Colombia, le ha puesto la cara a los paramilitares, al gobierno, a los empresarios, pero también a embajadores de todo el mundo. Desde las rústicas casas en medio de la selva, se ha desplazado hasta Costa Rica, México, Canadá, España, Suiza y Bélgica para denunciar que en 1997 las fuerzas militares en concordancia con estructuras paramilitares desplazaron y masacraron a los pobladores de Jiguamiandó y Curvaradó, departamento del Chocó.]

[“Dicen que soy guerrillera, pero yo he criado a ocho hijos y a 44 nietos. ¿Creen que he tenido tiempo para hacer la guerra?”, dice la matriarca. Para la comunidad, María Ligia no es otra cosa que una mujer que ha entregado su vida a la defensa del  territorio, exigiendo igualdad de derechos para todas las personas sin distinción de género, raza o religión. Enseñando a niños, niñas, jóvenes y adultos la importancia de defender sus tierras fértiles  de intereses económicos y políticos.]

[Hace 58 años llegó al Curvaradó donde se casó con Celedonio Martínez con quien tuvo 8 hijos. La vida parecía tranquila hasta que la Operación militar Septiembre Negro, diseñada por la Brigada 17 del Ejército colombiano en conjunto con paramilitares, que tuvo como epicentro el Curvaradó, la desplazó a ella, a su familia, y a 1500 pobladores. Luego empezó la militarización de la zona y Chaverra fue testigo de más de 70 crímenes entre asesinatos y desapariciones forzadas. Vio morir a vecinos, amigos y familiares.]

[Tras esa escalada de la violencia, vivió 6 meses escondida con sus nietos en las montañas chocoanas, donde la desnudez, el hambre, el frío pero sobre todo el miedo fueron las constantes. Cuando sonaban los disparos de un lado, debían desplazarse hacia otro lugar y así, sin ningún tipo de estabilidad vivió esos días con su familia. Al principio no sabía muy bien por qué los perseguían, “decían que se trataba de operaciones contra la guerrilla de las FARC”, pero cuando los pobladores volvieron al Curvaradó, el monocultivo de palma de aceite y de banano sembrados en lo que todavía para ellos era su hogar y sus tierras, explicó tal situación. “Por la palma fue nuestro despojo. La sangre de nuestros amigos y de nuestros hermanos ha abonado esa palma”, expresa incansablemente la matriarca.]

[La fe y creencias religiosas de esta lideresa la mantuvieron en pie y le permitieron seguir adelante en medio de la adversidad. Quienes la conocen, describen a Ligia como una persona religiosa, también como la expresión rural y afrodescendiente que mezcla discursos que practica en su propia vida.  Puede hablar de gozarse una verbena, pero al tiempo guarda un fervor muy especial por la virgen María, y a su vez por el libertador Simón Bolívar. Su tabaco en la boca y su andar un poco cabizbaja han acompañado los pasos de quien ha liderado la permanencia en el territorio de las comunidades afro y mestizas en medio del conflicto armado colombiano.]

[Poco a poco su voz fue ganando espacios en la comunidad y esa fortaleza y credibilidad hizo que la población la escogiera como represente de las víctimas de esa operación militar, junto a la ONG, la Comisión Intereclesial de Justicia y Paz. Juntos han denunciado ante la Corte Interamericana de Derechos Humanos, el drama en el que han vivido las comunidades del Chocó por el actuar gubernamental y paramilitar.]

[De ese trabajo, se constituyó Camelias, lugar blindado por medidas de la Corte Interamericana de Derechos Humanos, que entre otras cosas, condenó al Estado colombiano por su responsabilidad en las masacres que generaron el desplazamiento de Ligia María y de miles de familias afro y mestizas. Desde entonces, antes de ingresar al territorio donde habita Chaverra, se lee en un cartel, ‘Zona humanitaria Las Camelias, es tesoro, lugar exclusivo de población civil. Prohibido el ingreso de actores armados’.]

[En medio de su labor como ama de casa y agricultora, ha continuado luchando para que el gobierno le cumpla a las comunidades, en medio de los señalamientos que dicen que  Ligia María es integrante de la guerrilla de las FARC. Y es que sus conocidos aseguran que en diversas ocasiones esta líder afro ha sido perseguida y estigmatizada. Nunca ha negado la presencia de esa guerrilla en Curvaradó antes del desplazamiento, como tampoco ha dejado de plantear su posición política progresista, aunque eso no significa que piense igual  a ellos.]

[En medio de la selva permanece custodiada con un esquema de protección que el gobierno fue obligado a brindarle. Sabe que es más que posible que su cabeza tenga precio, pero a pesar de su edad sigue potenciando sus valores al servicio de su familia y su comunidad. “Si me matan por decir la verdad, nada puedo hacer, quedarán mis hijos y mis nietos. Yo sólo he abierto camino para que otros sigan esta lucha”.]

[Hace unos meses, en la Zona Humanitaria Las Camelias se realizó una jornada cultural durante la semana de clases del Colegio  de la comunidad. Allí se juntaron manos jóvenes negras y mestizas para rendir un homenaje a las enseñanzas, sabiduría y lucha de Ligia María Chaverra. Hoy un mural de colores con la palabra resistencia, evidencia el legado que ha dejado la matriarca del Curvaradó.]

\[caption id="attachment\_27506" align="aligncenter" width="841"\]![Mural en homenaje a la lideresa Maria Ligia Chaverra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/mural-ligia-maria-chaverra6.jpg){.size-full .wp-image-27506 width="841" height="540"} Mural en homenaje a la lideresa Maria Ligia Chaverra\[/caption\]
