Title: Líder social Álvaro Paul Gómez habría sido asesinado por el Ejército en Cauca
Date: 2018-11-13 14:40
Author: AdminContagio
Category: Entrevistas, Movilización
Tags: Cauca, ejercito, lideres sociales
Slug: lider-social-alvaro-paul-gomez-habria-sido-asesinado-por-el-ejercito-en-cauca
Status: published

###### Foto: Archivo 

###### 13 Nov 2018 

A través de un comunicado, la Red de Derechos Humanos del Suroccidente Colombiano Nacional denunció el asesinato del líder comunal **Álvaro Paul Gómez Garzón** quien habría sido ejecutado extrajudicialmente por integrantes del Ejército el pasado 11 de noviembre en horas de la noche cuando se desplazaba en un vehículo automotor  por la vía interveredal de **San Alfonso-Balboa-Argelia.**

Seis miembros del **Batallón de Infantería No 56, General Francisco Javier González**, ubicados en un puesto móvil de control detuvieron el vehículo para requisar  al líder quien también fue parte de la **Asociación Nacional de Zonas de Reserva Campesina (ANZORC) y de la Coordinación Social y Política Marcha Patriótica Cauca.**

Según los dos testigos del suceso, Gómez Garzón regresó a su vehículo y avanzó unos metros antes que el mismo grupo de militares le pidiera que se bajara nuevamente del vehículo, el líder social continuó conduciendo lo que habría ocasionado que los militares abrieran fuego, la ráfaga de fusil impactó en la cabeza del líder comunal ocasionándole la muerte de forma inmediata, lo que impidió proveerle la ayuda necesaria. Lea también ([Eladio Posso, otro lider social asesinado en el Bajo Cauca Antioqueño)](https://archivo.contagioradio.com/eladio-posso-lider-social-asesinado-bajo-cauca-antioqueno/)

El también líder social, Erlery Balboa afirma que “el Ejército Nacional quiere tapar su error“ al argumentar que abrieron fuego al ver que Gómez no detuvo su vehículo, a pesar de haber requisado con anterioridad al líder social y constatar que no portaba armas de fuego ni representaba amenaza alguna, “se harán las respectivas investigaciones pues existen videos en los que se reconoce que se trató de un error militar” concluye.

Este asesinato se suma a más denuncias vinculadas al **retén de San Alfonso** donde el narcotráfico exigiría dinero para poder cruzar por la vía.

###### Reciba toda la información de Contagio Radio en [[su correo]
