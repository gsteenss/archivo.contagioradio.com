Title: Comunidades campesinas e indígenas viven una creciente ola de violencia en Tumaco, Nariño
Date: 2020-11-17 11:42
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Asesinatos de indígenas, Desplazamiento forzado, nariño, Tumaco
Slug: comunidades-campesinas-e-indigenas-viven-una-creciente-ola-de-violencia-en-tumaco-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Tumaco, Nariño - @FTC\_HERCULES

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El [Comité de Solidaridad con los Presos Políticos](https://twitter.com/CSPP_) es una de las organizaciones que ha alertado sobre la delicada situación de DD.HH que vive Tumaco, Nariño donde sus habitantes denuncian que en la cabecera municipal la ocurrencia de homicidios es casi diaria; tan solo en octubre existen registros de más de 20 asesinatos. Hechos que se suman a desapariciones, desplazamientos masivos y casos de reclutamiento forzado que viven las zonas rurales del municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El más reciente de de estos actos de violencia se registró el pasado 11 de noviembre en horas de la tarde cuando **fue asesinado Eibar Angulo Segura habitante del Consejo Comunitario La Nupa**, en la vereda Porvenir.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma en el corregimiento de **Llorente y las veredas Guayacana y Candelillas**, en medio de la disputa por el territorio de los diferentes grupos que operan en la zona, continúan las vulneraciones a los DD.HH que se dan en una zona con alta presencia del Ejército y de la Fuerza de Tarea Conjunta Hércules.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

También es necesario resaltar la situación que vive el pueblo indígena Awá, con especialidad el Resguardo indígena Piguambí Palangala que **vive en una situación de **confinamiento, restricciones a la movilidad y escasez de alimentos**** como resultado de un enfrentamiento entre múltiples grupos al margen de la ley. [(Lea también: Pueblo Awa en alto riesgo por amenazas, desplazamientos y asesinatos)](https://archivo.contagioradio.com/pueblo-awa-en-alto-riesgo-por-amenazas-desplazamientos-y-asesinatos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Casi por concluir el 2020, ya son más de 17 comuneros indígenas asesinados en varios sectores rurales del pacífico en Nariño. Una de las comunidades más afectadas ha sido la del guardia indígena **Euliquio Pascal Rodríguez**, habitante del resguardo Inda Sabaleta quien fue asesinado el pasado 7 de octubre. [(Lea también: Es asesinado Euloquio Pascal líder Awá, en Nariño)](https://archivo.contagioradio.com/es-asesinado-euloquio-pascal-lider-awa-en-narino/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tres semanas antes de la muerte de Euliquio, también fue asesinado al líder social y profesor **indígena Juan Pablo Prada** en el resguardo Piguambi Palangala, corregimiento de Llorente y cinco indígenas más pertenecientes al resguardo de Sabaleta resultaron muertos, **tres desaparecidos y 500 personas desplazadas.**  [(Le recomendamos leer: Tres masacres en una semana, seis jóvenes fueron asesinados en Tumaco, Nariño)](https://archivo.contagioradio.com/tres-masacres-en-una-semana-seis-jovenes-fueron-asesinados-en-tumaco-narino/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En aquel mismo lugar, en el mes de julio, en cercanías al corregimiento de Llorente fue asesinado Rodrigo Salazar, de 44 años quien se desempeñaba como gobernador suplente del Resguardo Indígena, un territorio que resulta valioso para los grupos armados ilegales que lo usan como corredor estratégico para la producción y exportación de coca que es conducida no solo a puertos naturales, sino a la frontera con Ecuador. [(Lea también: Rodrigo Salazar, reconocido líder del Pueblo Awá fue asesinado en Llorente)](https://archivo.contagioradio.com/rodrigo-salazar-reconocido-lider-del-pueblo-awa-fue-asesinado-en-llorente/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Acuerdos de sustitución voluntaria son ignorados en Nariño

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Los sucesos de violencia generados por la violencia de grupos ilegales se suman a las erradicaciones llevadas a cabo por el Gobierno que les ha dado prioridad por encima de la erradicación voluntaria, no solo yendo en contra vía del Acuerdo de la Habana sino desconociendo los Acuerdos de Sustitución Voluntaria de Cultivos de Uso Ilícitos firmados por las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sus pobladores además denuncian que en el marco de la pandemia se ha abierto la puerta a las **fumigaciones terrestres con glifosato en los territorios de los Consejos Comunitarios y de los resguardos indígenas.** Ante las múltiples formas de violencia generadas tantos por actores legales como ilegales, las comunidades han alzado la voz exigiendo al Gobierno el cumplimiento del Acuerdo de Paz y una presencia estatal que trascienda del incremento de la Fuerza Pública . [(Le recomendamos leer: Tras tres años de impunidad, la revictimización persiste en El Tandil)](https://archivo.contagioradio.com/tras-tres-anos-de-impunidad-la-revictimizacion-persiste-en-el-tandil/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
