Title: Festival Petronio Álvarez en Contagio Radio
Date: 2019-08-13 12:06
Author: CtgAdm
Slug: festival-petronio-alvarez-en-contagio-radio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/DSC9106-copia.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/galeria158810.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/galeria156648.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/ODRADL0.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/DSC9106.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/DSC9106-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/CONTAGIO-RADIO-EN-EL-PETRONIO.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/CONTAGIO-RADIO-EN-EL-PETRONIO-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/marimba.mp4" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

FESTIVAL PETRONIO ÁLVAREZ
=========================

Del 14 al 19 de agosto, tal y como es tradición desde 1996, llega a Cali la edición XXIII del **Festival de Música del Pacífico Petronio Álvarez**, una celebración que reúne anualmente a más de 600.000 personas alrededor de la música, el baile, la gastronomía, las artesanías, los saberes ancestrales y sobretodo de todo el calor de la gente que, a través de la cultura, comparte con el país y el mundo, toda la esencia de su herencia afrocolombiana.

EN VIVO DESDE EL PETRONIO ÁLVAREZ
---------------------------------

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FRevistaArcadia%2Fvideos%2F490294788181527%2F&amp;width=800&amp;show_text=false&amp;appId=1237871699583688&amp;height=413" width="800" height="613" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>

CONTAGIO RADIO EN EL PETRONIO
=============================

En alianza con la Revista Arcadia y de la mano de la Comisión de la Verdad, Contagio Radio, emprende una nueva historia para conocer más a fondo la tradición del Pacífico, dialogar con sus protagonistas, aprender de sus conocimientos y narrar las historias, del que es considerado el más grande de los festivales afro de América Latina.

### [Programación](https://petronio.cali.gov.co/wp-content/uploads/2019/07/Folleto-Programacio%C3%ACn-Petronio-x-pags-baja_compressed.pdf)

### [Playlist Petronio Álvarez](#playlist)

### [Artículos](#ARTICULOS)

MÚSICA

PLAYLIST PETRONIO ÁLVAREZ
=========================

Vamos prendiendo los motores con la música del Pacífico, María Camila Salazar realizó esta playlist que contiene más de 60 canciones que te pondrán en modo Petronio.

<iframe src="https://open.spotify.com/embed/playlist/2cnUCZIKU35QiKokBXe7M3" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

PODCAST:
--------

**Nidia Góngora y el Dj británico Quantic**

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FRevistaArcadia%2Fvideos%2F2566114720307092%2F&amp;width=800&amp;show_text=false&amp;appId=1237871699583688&amp;height=513" width="800" height="513" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>

**Cultura de Paz con Pablo Moreno e Isdalia Ortega**

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F497509591015600%2F&amp;width=800&amp;show_text=false&amp;appId=1237871699583688&amp;height=513" width="800" height="513" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>

ARTÍCULOS PETRONIO ÁLVAREZ

</i>","nextArrow":"","autoplay":false,"autoplaySpeed":5000,"rtl":false}' dir="ltr"&gt;  
[historia](https://archivo.contagioradio.com/tag/historia/)  
Historias del Petronio: Próceres del Pacífico En el Petronio Álvarez también se conocen las historias y...  
[](https://archivo.contagioradio.com/historias-del-petronio-proceres-del-pacifico/)  
[Fotografia](https://archivo.contagioradio.com/tag/fotografia/)  
Historias del Petronio: 25 años retratando el poder, la riqueza y el color del Pacífico Esta es la historia de Jorge Idarraga, un fotógrafo de...  
[](https://archivo.contagioradio.com/historias-del-petronio-25-anos-retratando-el-poder-la-riqueza-y-el-color-del-pacifico/)  
[Cultura](https://archivo.contagioradio.com/tag/cultura/)  
Petronio Álvarez: La música como bien universal, la música de todas y todos A propósito del lanzamiento del Festival Petronio Álvarez y la...  
[](https://archivo.contagioradio.com/petronio-alvarez-la-musica-como-bien-universal-la-musica-de-todas-y-todos/)

#### "La música es un bien universal, y como es universal es de todos. Nos ayuda a decir una verdad de lo que estoy viviendo y sintiendo sin señalar, para también decir una verdad"

**Ángela Salazar, Comisionada Comisión de la Verdad**

[  
Facebook  
](https://www.facebook.com/contagioradio/)  
[  
Twitter  
](https://twitter.com/Contagioradio1)  
[  
Instagram  
](https://www.instagram.com/contagioradio/)
