Title: "Condiciones en las cárceles Colombianas son preocupantes" Alan Duncan
Date: 2016-12-20 11:08
Category: DDHH, Nacional
Tags: cárceles colombia, Centros penitenciarios, presos politicos
Slug: condiciones-en-las-carceles-colombianas-son-preocupantes-alan-duncan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/carcel_colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [20 Dic 2016] 

El Ministro Británico de Estado de Asuntos Exteriores Alan Duncan expresó su **preocupación frente a las condiciones que atraviesan las cárceles en Colombia,** específicamente frente a la sobrepoblación que existe, el trato que se les da a los presos y la falta de asistencia médica en estos lugares.  A este hecho se suman  los diferentes llamados de atención que han hecho diversas organizaciones sociales al Estado, frente a las condiciones inhumanas en las que se encuentran los reclusos.

De acuerdo con Duncan, “**las condiciones de las cárceles en Colombia, incluyendo la sobrepoblación y el trato a los presos, son particularmente preocupantes”**, situación que previamente el Ministro de Justicia había declarado como estado emergencia para los centros penitenciarios, esto debido a la falta de personal médico que pudiese atender a los presos.

De igual forma el Ministro Británico también señaló que el acceso a la justicia y al debido proceso en Colombia sigue siendo un punto álgido, sobre todo frente a los casos de defensores de derechos humanos que se encuentran en la cárcel “**La detención de defensores de derechos humanos y los activistas en Colombia, a menudo sin juicio ni acceso a representación legal, sigue siendo un motivo de preocupación”**. Esto debido a que la embajada Británica se ha dedicado a realizar el seguimiento de los casos de presos políticos como Húber Ballesteros y David Ravelo.

Entre tanto, la última emergencia de la que se tiene conocimiento, fue la que se reportó en la Cárcel de Cómbita, Boyacá, en donde al parecer más de 100 reclusos estaría enfermos y presentando diarrea, fuertes dolores estomacales e infecciones intestinales. Le puede interesar:["Grave situación de salud de presos en Cárcel de Cómbita, Boyacá"](https://archivo.contagioradio.com/grave-situacion-de-salud-de-presos-en-carcel-de-combita/)

###### Reciba toda la información de Contagio Radio en [[su correo]
