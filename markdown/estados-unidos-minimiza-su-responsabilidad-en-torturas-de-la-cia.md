Title: Estados Unidos minimiza su responsabilidad en torturas de la CIA
Date: 2014-11-24 17:04
Author: CtgAdm
Category: DDHH, Política
Tags: CIA, EEUU, Torturas
Slug: estados-unidos-minimiza-su-responsabilidad-en-torturas-de-la-cia
Status: published

###### Foto: laprensa.hn 

La técnica conocida como el **“Submarino”**, ha sido una de las más usadas por la **CIA** contra **sospechosos** de terrorismo, y consiste en el **ahogamiento al punto de la muerte** para luego intentar sacar respuestas.

Sin embargo existen otras prácticas, señaladas en el informe como la administración de medicamentos para **impedir el sueño**, o para provocar daños estomacales, e incluso **medicamentos que borran la memoria**, son algunas de las denuncias de las víctimas.

Lastimosamente, según varios analistas, las consecuencias de estos actos no llegarán más allá de un escándalo mediático de 2 semanas como lo señala el analista Cesar Torres.

Torres afirma que ya desde el **2001 hubo voces que denunciaban las torturas por parte de la CIA** y que invitaban a abolir esta práctica de la llamada “lucha contra el terrorismo” sin embargo, desde ese mismo momento las denuncias han sido **ignoradas sistemáticamente.**

Lo más preocupante es el hecho de que muchos estados y gobiernos conocen de estas prácticas desde hace mucho tiempo y **han hecho muy poco para ocultarlas y mucho menos prevenirlas**, lo que permite prever que no habrá un alcance de responsabilidad penal o política por el resumen del informe publicado hoy.
