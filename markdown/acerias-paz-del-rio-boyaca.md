Title: Acerías Paz del Río sigue causando estragos en Boyacá
Date: 2016-12-11 22:30
Category: Ambiente, Nacional
Tags: Acerías Paz del Río, Gran Minería en Colombia
Slug: acerias-paz-del-rio-boyaca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/WhatsApp-Image-2016-12-07-at-7.25.16-AM-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 Dic 2016 ] 

[Habitantes del municipio de Paz del Río denuncian que la empresa Acerías Paz del Río realizó un desvío de aguas hacia la zona de Paz Vieja, ocasionando **la remoción en masa de 7m2 de tierra en el derrumbe El Salitre y otros graves daños ambientales.**]

[ La comunidad asegura que aunque la empresa cuenta con título minero otorgado por el Estado y licencia ambiental de Corpoboyaca, **"no cuenta con un plan de manejo de aguas y no ha realizado consultas previas con la población".**]

[Elver Lizarazo líder de la comunidad, aseguró que entidades territoriales **ya habían advertido a la siderúrgica sobre los riesgos que podía generar** la ejecución de los desvíos sin tener un plan de contingencia adecuado.]

[Lizarazo añade que “en 2011 la empresa destruyó la zona de El Salitre y la convirtió en el derrumbe que ahora es, se habían comprometido a reconstruirlo pero ya han pasado 5 años y eso no ha pasado (...) **llevamos esos mismos 5 años pidiendo un plan de manejo de aguas y Acerías no lo ha hecho”.**]

[Por otra parte, el líder comunitario señaló que seguirán exigiendo una respuesta y soluciones por parte de la empresa y las instituciones del Estado, pues aparte de los daños recientes, “desde la llegada de Acerías Paz del Río **se han secado**]**entre 22 a 30 nacimientos de agua y si continúa la actividad minera,10 nacimientos más podrían correr con la misma suerte”.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
