Title: EPM "perdió el control sobre Hidroituango": Contraloría
Date: 2018-08-28 17:27
Category: DDHH, Nacional
Tags: ANLA, Antioquia, EPM, Hidroituango, Rios Vivos
Slug: epm-perdio-el-control-sobre-hidroituango-contraloria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/De4luDzXkAA2qVR-e1528309998898.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Julieta Duque] 

###### [28 Ago 2018] 

Duros cuestionamientos hizo la Contraloría General de la Nación a Hidroituango, a través de un informe, en el que afirma que el proceso de obtención de la licencia de funcionamiento estuvo “plagado de errores”, Asimismo, manifestó que “EPM tomó decisiones sin sustento técnico”.  **Sobre el proyecto, perdió el control hidráulico del mismo y el plan de contingencias que realizó no tenía previsto el riesgo catastrófico ocurrido en abril de este año**.

### **La licencia de Hidroituango** 

La Contraloría señaló que uno de los primeros errores consistió en que el Ministerio de Ambiente expidió una licencia ambiental para Hidroituango, “a sabiendas de que el lugar presentaba una cantidad de fallas geológicas, ampliamente conocidas y registros de derrumbes frecuentes”. Hecho que ha provocado, según el ente regulador, daños sociales y ambientales incalculables. (Le puede interesar: ["Hidroituango no acata las medidas ambientales")](https://archivo.contagioradio.com/hidroituango-no-cumple-con-medidas-ambientales/)

De igual forma, expresó que EPM ocultó información a la Autoridad Nacional de Licencias Ambientales (ANLA)y avanzó en la construcción de las obras sin la licencia requerida, mientras que, tanto las comunidades como las gobernaciones y alcaldías, **tampoco habrían contado con la información suficiente sobre los riesgos del proyecto.**

Referente el papel de control que ha tenido la ANLA en el proyecto Hidroituango, la Contraloría aseveró que ha sido permisivo, ya que no se explica cómo existiendo tantas irregularidades, no se ha generado ni una sanción a EPM y manifestó que “las compensaciones, la protección de especies amenazadas y la reforestación, entre otras obligaciones, llevan mucho tiempo de retraso y la ANLA en vez de hacer uso de su competencia sancionatoria, cada vez fija nuevos plazos para su cumplimiento, en detrimento de las condiciones ambientales de las zonas afectadas por el proyecto”.

### **Los daños de Hidroituango** 

La Contraloría declaró que la ausencia de un plan de contigencias ajustado a los riesgos reales del proyecto, ocasionó perjuicios en parte a más de **25.000 personas de los 14 municipios afectados por la emergencia**. Asimismo, señaló que el riesgo de este proyecto es sistemático y permanente, y además incierto, sometiendo a las comunidades a no tener definiciones o claridades sobre el devenir en sus territorios.

De igual forma, Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos, que agremia a las víctimas del proyecto Hidroituango, afirmó que en la auditoría realizada por la Contraloría también se evidencia que **existieron fallas en el censo de población afectada durante el mes de abril** y que aún está la alerta roja para las comunidades río arriba del Cauca.

“Lo que esperamos en lo inmediato es que se de comida, albergue, se atienda la situación. A las personas que perdieron su vivienda se les empiece a construir un barrio, un municipio o lo que se tenga que hacer, porque las comunidades de Puerto Valdivia, hasta Tarazá está sin saber hacia dónde ir” afirmó Zuleta. (Le puede interesar: ["EPM demanda a víctimas de Hidroituango: Ríos Vivos"](https://archivo.contagioradio.com/epm-demanda-a-victimas-de-hidroituango/))

Esta auditoría de la Contraloría ya se encuentra en manos de la Procuraduría General de la Nación y de la Fiscalía, quienes serán los entes que tomen decisiones sancionatorias sobre el proyecto Hidroituango, el accionar de EPM y de la ANLA. Además, la Contraloría afirmó que encontró 35 hallazgos, de los cuales 9 tuvieron presunta incidencia disciplinaria y 8 penal.

<iframe id="audio_28154510" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28154510_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio.
