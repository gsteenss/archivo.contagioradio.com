Title: Líder del PKK, en Turquía, anuncia intención de abandonar la lucha armada
Date: 2015-03-03 21:17
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: conflicto armado entre PKK y Turquía, Negociación entre PKK y Turquía, PKK abandona las armas
Slug: lider-del-pkk-en-turquia-anuncia-intencion-de-abandonar-la-lucha-armada
Status: published

###### Foto:Tribunainterpretativa.org 

El líder del PKK (**Partido de los trabajadores del Kurdistán**) **Abdalá Ocalan** ha exigido al brazo militar del partido que celebre un congreso en primavera con el objetivo de abandonar la lucha armada. Con estas declaraciones pretende mostrar que el "proceso de paz" continua en buen camino y que desde el 2012 hay avances significativos.

Para el PKK *“Esta declaración de buena voluntad supone una base muy importante y una oportunidad de democratizar el Estado y el Gobierno y de resolver la cuestión kurda y los problemas básicos de Turquía”* señala el partido. El comunicado del líder fue leído conjuntamente entre miembros del HDP y del gobierno de Ankara, en un gesto con miras a un proceso de paz.

Desde que en 2012 iniciaran conversaciones secretas entre el **PKK**, el **gobierno turco** y los nacionalistas kurdos representados por el **Partido de la Democracia de los Pueblos (HDP),** no han parado de realizarse encuentros para establecer una agenda de paz.

**Abdalá Ocalan** condenado a cadena perpetua en la isla de Imrali, en 2013 **declaró el cese unilateral al fuego y retiró a sus milicianos de territorio turco.**

A pesar de lo positivo de las declaraciones las divisiones continúan presentes entre los kurdos, de hecho el coopresidente del HDP Selahattin Demirtas ha arremetido contra el ejecutivo turco acusándolo de crear "falsas expectativas" y de no generar un clima de dialogo.

**El conflicto turco-kurdo inició en 1984 y se ha cobrado ya más de 40.000 víctimas**. El PKK es la principal organización armada dentro de los rebeldes kurdos, que reclaman la independencia de Turquía o mayor autonomía y la formación de un estado Kurdo junto con las regiones de Siria, Irak e Irán.

El PKK ha combatido al ejercito turco utilizando como base la región fronteriza de Irak, el partido está ilegalizado y considerado como organización terrorista por la EU, EEUU y el gobierno turco.
