Title: Atentan contra lideresa Maidany Salcedo de Piamonte, Cauca
Date: 2017-09-19 14:52
Category: DDHH, Nacional
Tags: Atentado, Cauca, lideres sociales
Slug: atentados_cauca_maidany_salcedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/informe_derechos_onu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: MOVICE 

###### [19 Sep 2017] 

Según la denuncia de la Asociación de Trabajadores de Piamonte, Cauca, el atentado contra Maidany Salcedo se presentó **el pasado 2 de septiembre cuando la líder campesina se desplazaba en un bote sobre el río Inchiyaco,** hacia la zona urbana del municipio luego de realizar una reunión de socialización sobre el acuerdo regional de sustitución de cultivos de uso ilícito, en el marco de la implementación del acuerdo de paz con las FARC.

El relato señala que el bote en que se movilizaba la líder campesina fue atacado por desconocidos que dispararon contra la embarcación pero **ninguna de las balas impactó a alguna persona dada la reacción del escolta de Maidany** y el conductor del bote que maniobró la embarcación para evadir el ataque. La lideresa y sus acompañantes lograron escapar de los agresores se refugiándose en uno de los caseríos en las laderas del río.

Aunque los hechos se produjeron hace dos semanas, solamente hasta este martes se puso establecer establecer la información para realizar la denuncia pública e intentar impedir más riesgos contra la vida de la líder comunitaria que ya había sido víctima de una amenaza el pasado mes de junio. Según la denuncia **el pasado 28 de ese mes, la líder campesina fue abordada por sujetos que la amenazaron de muerte** si seguía hablando de sustitución de cultivos de usos ilícito.

Para la Asociación de Trabajadores de Piamonte, esta situación se ve agravada por la ausencia de medidas de seguridad efectivas dado que, aunque el territorio cuenta con alta presencia de efectivos militares, la presencia de actores armados no se ha reducido. En cambio la participación de las FFMM en la erradicación forzada de cultivos ha incrementado la conflictividad en la zona **“la fuerte presencia militar del ejército y la policía, quienes en vez de garantizar protección,** participan de las acciones de erradicación forzada aumentando la conflictividad y descontento social”.

Ante ese hecho organizaciones sociales exigen al Estado colombiano medidas efectivas de seguridad para la lideresa, pero además que se investigue estos y otros hechos que se han presentado en la región contra defensoras y defensores de derechos humanos que se están viendo afectados por las acciones de grupos armados que atemorizan y generan zozobra en la zona.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
