Title: FlamenBo: un homenaje a la esencia del flamenco
Date: 2017-07-05 15:16
Category: eventos
Tags: Bogotá, Cultura, Festivales, flamenco
Slug: ii-festival-de-flamenco-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/DG3R8029.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: medios y artes 

###### 06 Jul 2017 

Como un homenaje al origen y esencia flamenca, vuelve a Bogotá en su segunda edición el Festival FlamenBo-Raíces 2017, un evento que busca fomentar el conocimiento, respeto y la creación, recordando que entre las raíces diversas y complejas de los pueblos latinoamericanos se encuentra el flamenco como herencia cultural.

La programación que se extiende del 6 al 15 de julio, incluye talleres de formación con maestros nacionales e internacionales, tablaos nacionales, muestra de estudiantes, actividades académicas, exposiciones, instalaciones, cineforo flamenco, el estreno de la obra colombiana “Reflejos” y la participación de la compañía española “La Bambina” con su obra "Donna"

Los escenarios que hacen parte de esta versión son: La Factoría L´Explose, Teatros de Villa Mayor y Kennedy, Teatro Mayor Julio Mario Santo Domingo, El Planetario Distrital, Trementina Artes, Biblioteca Nacional y la Casona de la Danza (Le puede interesar: [Territorio, resistencia y cine en el Distrito de Aguablanca](https://archivo.contagioradio.com/42904/))

FlamenBo es organizado por la Casa de Flamenco bajo la dirección de la bailaora Marcela Hormaza, con el apoyo de la Gerencia de Danza de IDARTES y La Red de Festivales de Danzas del Mundo: EVOÉ.

<iframe src="https://www.youtube.com/embed/Mz1xnboijDU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

** PROGRAMACION 2017**

**“REFLEJOS”**  
Jueves 6, viernes 7, sábado 8 y viernes 14 de julio a las 8:00 p.m.  
Lugar: Sala de Teatro La Factoría L´Explose Cra 25 \# 50 - 35 Galerías  
Bono de apoyo: General \$30.000= Estudiantes: \$25.000=  
Dirección: Casa Flamenco - Colombia

**FUNCIÓN DE GALA: “DONNA”**  
9 de julio a las 8:00 p.m.  
Lugar: Teatro Mayor Julio Mario Santo Domingo Calle 170 \# 67 – 51  
Bono de apoyo: Platea \$50.000 - 1 balcón: \$40.000 -2 balcón \$20.000=  
Compañía La Bambina – España

**TABLAO FLAMENCO**  
12 de julio a las 8:00 p.m.  
Lugar: Trementina Artes Av Cra 24 \# 37 -44 (Park Way).  
Bono de apoyo: General \$30.000= / Estudiantes: \$25.000  
Baile: La Bambina - Marcela Hormaza - Cante: David Bastidas  
Guitarra: Sergio Gómez

**MUESTRA ESTUDIANTES CASA FLAMENCO**  
13 de julio a las 8:00 p.m.  
Lugar: Sala de Teatro La Factoría L´Explose / Cra 25 \# 50 - 35 Galerías  
Bono de apoyo: General \$30.000= / Estudiantes: \$25.000

**TABLAO FLAMENCO EN LAS LOCALIDADES – CULTURA EN COMÚN**  
Localidad Kennedy, 21 de Julio a las 10:00 a.m  
Localidad Villa Mayor 22 de julio a las 3:00 p.m.  
Entrada Libre

**FUNCIÓN DE CLAUSURA**  
Tablao Artistas Internacionales, equipo docente de Casa Flamenco y Compañía La Bambina  
15 de Julio 2017 a las 8:00 p.m.  
Lugar: Sala de teatro La Factoría L´Explose Cra 25 \# 50 - 35 Galerías  
Bono de apoyo: General \$30.000= / Estudiantes \$25.000=

**EXPOSICIÓN FOTOGRÁFICA FLAMENCA**  
Desde el 6 de julio  
Lugar: Sala de teatro La Factoría L´Explose Cra 25 \# 50 - 35 Galerías  
Entrada Libre  
Artista invitado: Carlos Mario Lema  
Muestra fotográfica basada en el tema “Flamenco en el escenario”, captura de imágenes en movimiento escénico, donde se destaca la fuerza y pureza del movimiento.

**CINE FLAMENCO**  
Jueves 6 de julio a las 6:00 p.m.  
**“Camarón”** del director: Javier Cahavarri  
Planetario Distrital Calle 26 \# 5 – 93  
Viernes 7 de julio a las 6:00 p.m.  
**"Herencia Flamenca”** Un Film De Mitchael Meert  
Planetario Distrital Calle 26 \# 5 – 93  
Jueves 13 de julio a las 6:00 p.m  
**“Morente Sueña La Alhambra”** Una película José Sánchez-Montes  
Biblioteca Nacional Calle 24 \# 5 – 60  
Viernes 14 de Julio a las 6:00 p.m.  
**“La Sombra De Las Cuerdas”** un documental del Niño Miguel  
Biblioteca Nacional Calle 24 \# 5 – 60  
ENTRADA LIBRE

**CLASES MAESTRAS**  
Clases maestras nivel iniciación  
Horario: 2:00 a 3:30 p.m. del 10 al 13 de julio de 2017 en la Casona de la Danza Calle 18 \#1– 05 Este. ENTRADA LIBRE  
CLÁSICO ESPAÑOL – Cesar Guerrero (lunes)  
SEVILLANAS – Marcela Hormaza (martes)  
CASTAÑUELAS – Bernardo Cardona (miércoles)  
FLAMENCO – Diego Fetecua (jueves)

**TALLERES CON MAESTROS INTERNACIONALES**  
CASA FLAMENCO  
Cra 20 \# 50 – 59 galerías  
Información de valores e inscripciones www.casaflamencobogota.com  
GUITARRA – Sergio Gómez  
CANTE – David Bastidas  
INICIACIÓN AL COMPÁS – Sergio Gómez  
BAILE – Danila Scarlino (Nivel básico, Nivel intermedio 1 y 2)

 
