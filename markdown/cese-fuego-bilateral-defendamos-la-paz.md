Title: Cese al fuego bilateral una oportunidad para combatir el Covid-19: Defendamos la Paz
Date: 2020-03-27 18:51
Author: CtgAdm
Category: Actualidad, Paz
Tags: Cese al fuego bilateral, Defendamos la Paz
Slug: cese-fuego-bilateral-defendamos-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Defendamos la Paz

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mediante un comunicado, la plataforma Defendamos la Paz, con el fin de hacer frente al Covid-19, exhortó **a todos los grupos armados que operan en Colombia a cesar sus ataques, y a que las fuerzas militares suspendan sus operaciones ofensivas** en un momento que llama a plasmar con hechos la protección de los territorios más vulnerables a la violencia y la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En el caso particular de Colombia, señalan, la llegada del Covid-19 es una oportunidad para cuestionarse "la inutilidad del conflicto armado, de la violencia omnipresente a lo largo de la historia" del país y de promover la cooperación para resolver los problemas que genera esta crisis en la frontera entre Colombia y Venezuela. [Promover la paz en los territorios: el desafío de Defendamos la Paz para el 2020](https://archivo.contagioradio.com/promover-la-paz-en-los-territorios-el-desafio-de-defendamos-la-paz-para-el-2020/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta petición se une al llamado del secretario general de la ONU, Antonio Guterres, **con el fin de decretar un alto al fuego en todo el mundo,** una petición que ya ha tenido resultados en naciones como Filipinas donde existe una tregua bilateral o Camerún donde las milicias rebeldes han declarado un alto al fuego; se espera que los grupos armados respondan al llamado y el Ejército adopte la medida [(Lea también: Unión Europea reafirma su apoyo a los liderazgos en Colombia)](https://archivo.contagioradio.com/union-europea-reafirma-su-apoyo-a-los-liderazgos-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La paz debe garantizar la vida de los líderes sociales

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

[La plataforma](https://twitter.com/DefendamosPaz)Defendamos la Paz condenó los hechos que se han presentado en departamentos como Cauca, Meta y Bolívar donde aprovechando la actual situación de emergencia, se siga asesinando a lideresas y líderes sociales, así como a excombatientes. [(Lea también: Antonio Gallego Mesa, exguerrillero de las FARC es asesinado en La Macarena, Meta)](https://archivo.contagioradio.com/antonio-gallego-mesa-exguerrillero-de-las-farc-es-asesinado-en-la-macarena/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Solidaridad humana y el cuidado de la vida y la salud por encima de cualquier consideración política, económica o militar”** es el llamado que también realizó la Comisión de la Verdad sumándose a esta petición, invitando a todas las organizaciones armadas al margen de la ley que operan en el Colombia a cesar sus actividades,

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
