Title: A 35 años persiste "pacto de silencio" frente a desaparecidos del Palacio de Justicia
Date: 2020-11-06 08:56
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Crimen de Estado, Desaparecidos del Palacio de Justicia, Palacio de Justicia, Verdad historica
Slug: 35-anos-memoria-construyen-victimas-palacio-justicia-no-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/MG_0547.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Víctimas del Palacio de Justicia / Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Estos 6 y 7 de noviembre, se conmemoran 35 años de la toma y retoma del Palacio de Justicia, un suceso que concluyó en la muerte de 98 personas y la desaparición de otras 11 en medio de sucesos que incluyeron torturas y asesinatos por parte del Ejército Nacional. Hoy, para los familiares de las víctimas y los abogados que litigan el caso, pervive en la impunidad de la rama judicial y la ausencia de reconocimiento por parte de los responsables que impiden sanar las heridas que requieren de la verdad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Pilar Navarrete, esposa de Héctor Jaime Beltrán, empleado de la cafetería del Palacio** y cuyos restos fueron hallados en 2017 en la tumba que contenía los supuestos restos del también desaparecido magistrado auxiliar Julio César Andrade, considera que en los últimos años no ha existido ningún tipo de avance, por el contrario, se han ignorado los fallos a favor de las víctimas, incluida la sentencia de la Corte Interamericana que para 2014 declaró responsable al Estado colombiano por violaciones de derechos humanos” ocurridas en el marco de la toma y retoma y que exigía entre otras cosas, efectuar una búsqueda rigurosa para determinar el paradero de las 11 víctimas aún desaparecidas y establecer la verdad de los hechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Para Helena Urán Bidegain, hija del magistrado auxiliar del Consejo de Estado Carlos Horacio Urán,** sacado con vida del Palacio, torturado y asesinado por el Ejército afirma que los últimos 35 años se pueden dividir en dos momentos, el primero en el que creían que su papá había muerto al interior del Palacio y el segundo en el que se reveló que su billetera fue hallada en una bóveda secreta del Cantón Norte del Ejército, en Bogotá.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hoy el caso que se adelantaba para esclarecer los hechos que llevaron al asesinato del magistrado permanecen paralizado. Al respecto, Urán Bidagain considera que **tiene que existir un aparato detrás, de otro modo, no sería posible esconder durante 35 años estos crímenes**, "ahí tienen que confluir muchas energías que lo hacen posible, son muchos intentando proteger a otros". Agrega que las amenazas que sufrió su familia solo tuvieron sentido hasta el momento en que supieron que su padre había sido víctima de una ejecución extrajudicial lo que las llevo a ella y a su familia al exilio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Académicos, defensores de DD.HH. y algunos políticos han señalado que en el país primó un gran pacto de silencio nacional del establecimiento, incluidos sectores políticos y sectores de la fuerza pública que impidieron se adelantara la resolución del caso y que las dudas respecto a los cambios en la vigilancia del Palacio, el accionar del Ejército, la producción del incendio, la violencia contra los rehenes y la desaparición de personas. [(Lea también: "El pacto del silencio" tras los 32 años de la toma y retoma del Palacio de Justicia)](https://archivo.contagioradio.com/49372-2/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> La pregunta que nos tenemos que hacer es quién se beneficia de estas mentiras, de la impunidad, quien tiene miedo a la verdad y de ahí cada uno puede sacar sus conclusiones
>
> <cite>Helena Urán Bidegain</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Eduardo Carreño, abogado al frente del caso, señala que es necesario profundizar en aspectos que son indiscutibles como la responsabilidad del entonces presidente Belisario Betancur quien expresó que todas las acciones ocurridas en aquellos dos fatídicos días fueron conocidas y ordenadas por él

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"En este tipo de procesos no se puede aceptar que cuando se ordena la investigación empiece la tergiversación de lo que se hizo, había una comunicación directa de radio con quienes estuvieron al frente de la retoma", expresa el abogado, quien recuerda, también debe responsabilizarse a los miembros del gabinete de ministros que hicieron parte de la toma de decisiones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las dudas que surgen ante mecanismos de la JEP

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En ese sentido cabe señalar que la única sentencia que ha surgido de esta caso fue la que se dio contra el general (r) Jesús Armando Arias Cabrales quien como Comandante de la Brigada XIII fue responsable de la desaparición de 11 personas entre ellas **Carlos Augusto Rodríguez, Bernando Beltrán, Luz Mary Portela, David Suspes Celis e Irma Franco**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que la condena establecía que Arias Cabrales debía cumplir 35 años de cárcel, desde mayo de 2020, como parte de su sometimiento a la [Jurisdicción Especial de Paz (JEP)](https://www.jep.gov.co/Paginas/Inicio.aspx) obtuvo el beneficio de libertad transitoria, condicionada y anticipada, aún cuando la Corte Interamericana prohibía que responsables de los hechos ingresaran a algún tipo de justicia especial.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La decisión, rechazada por los familiares de las víctimas quienes han expresado que los aportes del militar a la justicia ordinaria han sido nulos y que hasta el momento han sido iguales tras su ingreso a la JEP, ha ocasionado que existan dudas sobre el proceso que hoy adelanta la jurisdicción. [(Lea también: Víctimas del Palacio de Justicia temen que JEP sea escenario de impunidad para Arias Cabrales)](https://archivo.contagioradio.com/victimas-del-palacio-de-justicia-temen-que-jep-sea-escenario-de-impunidad-para-arias-cabrales/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Era lo poco que se había alcanzado dentro de la justicia ordinaria, después la JEP le da esa libertad condicional sin que aportara un mínimo de verdad" expresa Helena Urán quien pese a la circunstancia señala que existen funcionarios que adelantan labores junto al Sistema Integral de Justicia, "no quiero ser pesimista pero tampoco podemos ser tan ingenuos", agrega.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además de Arias Cabrales, otros militares implicados en las desapariciones incluidos el Mayor General (r) Iván Ramírez, el mayor (r) Óscar William Vásquez y el sargento mayor (r) Gustavo Arévalo Moreno se han acogido a la JEP junto al sargento (r) Bernardo Garzón Garzón quien realizó una petición adicional. En la actualidad la JEP decretó la acumulación de casos relacionados con los acontecimientos del 6 y 7 de noviembre de 1985, su llegada al igual que la de Arias Cabrales es de interés paras las víctimas que aguardan con expectativa la verdad que pueda surgir de este escenario.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué existió detrás de las ordenes dadas en el Palacio de Justicia?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte el abogado Germán Romero expresa que lo más preocupante no es el poco avance en los casos, sino todo lo que ha implicado para familiares y funcionarios quienes han sido perseguidos, **como es el caso de la jueza María Stella Jara, quien recibió en 2008 el proceso contra el coronel (r) Luis Alfonso Plazas Vega, Angela María Buitrago exfiscal del proceso Plazas Vega** quien fue absuelto por la Corte Suprema de Justicia en 2015. .

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"El militarismo en Colombia es un mal que pareciera imposible de superar" señala el abogado quien afirma, existe un alto grado de militarización en la justicia nacional y que la intención de los magistrados y miembros de las Cortes que fallecieron en medio de la toma y retoma era precisamente el evitar que la rama judicial fuera "subyugada a este poder".[(Le recomendamos leer: 34 años del Palacio de Justicia: Para nunca olvidar, para no repetir)](https://archivo.contagioradio.com/34-anos-del-palacio-de-justicia-para-nunca-olvidar-para-no-repetir/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para Helena, no se habla de esa verdad que permanece oculta con relación a los más de 100 casos que eran investigados en aquel entonces y que vinculaban a las Fuerzas Armadas con violaciones a los DD.HH, "la gente no tiene ni idea de lo que pasó, solo conoce una versión y todos repiten que el narcotráfico financió a la guerrilla, pero rara vez mencionan la otra aparte de la historia".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El abogado Carreño coincide en que el asesinato de los magistrados en medio de los hechos no fue casual y fue estudiado, **"ahí se sabia a quien había que asesinar y tenían toda la información de inteligencia",** por lo que señala esas son las respuestas de fondo que deben darse, ¿por qué se asesinó específicamente a los magistrados de la Sala Penal, el Consejo de Estado o la Sala Constitucional, sin embargo el resolver esas preguntas de fondo implicar revelar verdades que involucran al alto Gobierno y señala, ahí es donde se debe seguir presionando.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La rama judicial está al servicio de una justicia de clases"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Carreño concuerda además en que la administración de la justicia en Colombia es cuestionable, al considerar que se trata de una "justicia de clase" que únicamente funciona para la persecución de dirigentes sindicales o comunales pero no de igual forma cuando se trata de crímenes de Estado, tanto así que resalta, existen sectores que en la actualidad buscan modificar la JEP.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"No quieren que la poca verdad que se ha ganado en la jurisdicción ordinaria se ratifique en la JEP y se amplíe, quieren echar para atrás eso, aquí estamos defendiendo lo precario de la administración de justicia que hay"**, agrega Carreño. A su vez, Romero advierte que en algunos de los casos se ha cerrado la posibilidad de obtener reparaciones simbólicas y materiales lo que ha obstruido aún más los procesos en el caso del Palacio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Carreño señala que estos mismos problemas se ven en el Instituto de Medicina Legal, organismo que se comprometió a entregar para julio de 2020 la totalidad de los resultados forenses que están vinculados al caso, "el retardo injustificado tiene implícita esa política de impunidad", señala el abogado quien advierte, es una situación que se agrava con la politización e incidencia del presidente Duque en otras instituciones que deben estar al servicio de las víctimas y la ciudadanía como la Defensoría del Pueblo o la Procuraduría.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La memoria histórica también está en disputa en el caso del Palacio

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Romero señala que otra de las mayores preocupaciones que existen es que se está asistiendo a un " escenario de revisionismo histórico" en el que la impunidad militar busca obstaculizar los avances sustanciales que han existido en materia de verdad en el caso y aunque de a poco los y las colombianas han conocido lo ocurrido en el Palacio de Justicia desde otras lógicas y el silencio no sea tan generalizado aún existe trabajo por hacer.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Parte de ese negacionismo, señala Helena no solo se evidencia en el ámbito judicial, sino en la sociedad mismo, su indolencia e incluso en las mismos lugares donde ocurrieron los hechos y que no conservan nada de lo ocurrido en 1985 como el actual Palacio de Justicia **cuya reconstrucción inicial tenía un crédito extraordinario de 400 millones y para 1998, ascendía a los 63.000 millones de peso**s o la Casa del Florero, lugar que guarda en sus paredes las torturas y violaciones a los DD.HH. que fueron cometidas en su interior contra las personas que fueron sacadas vivas del Palacio y llevadas hasta aquel lugar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El abogado Carreño señala que la pelea que debe seguirse dando ante las afirmaciones de que no existieron desaparecidos, la negación de las decisiones de la Corte Suprema o los fallos internacionales, es el no bajar las banderas de las víctimas y continuar luchando por que existan responsabilidad penales, "necesitamos tener certeza de qué pasó, qué les hicieron, dónde están y qué les hicieron ese día, es importante que todas las generaciones sepan lo que pasó", concluye Pilar Navarrete. [(Lea también: «¡Sí existen desaparecidos! familiares de las víctimas del Palacio de Justicia»](https://archivo.contagioradio.com/existen-desaparecidos-l-palacio-de-justicia/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
