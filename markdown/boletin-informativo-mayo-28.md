Title: Boletín Informativo Mayo 28
Date: 2015-05-28 17:30
Category: datos, Uncategorized
Slug: boletin-informativo-mayo-28
Status: published

[*Noticias del Día:*]

<iframe src="http://www.ivoox.com/player_ek_4564563_2_1.html?data=lZqjlpqad46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhtDgxtnW0JDNssfj09LO1s7as4zBwt7cjZeccYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-El **escalamiento de la guerra** está llevando al **proceso de conversaciones de paz de las FARC con el gobierno colombiano** **a un punto difícil de superar** por ello es necesario que **la movilización social se haga sentir**, afirma **Alfredo Molano**, sociólogo, periodista e integrante de la **Comisión de Memoria Histórica del conflicto y sus víctimas.**

-**Palestina y su federación de fútbol**, junto con organizaciones internacionales y personalidades de carácter público a nivel mundial, han firmado un documento **exigiéndole a la FIFA** **la expulsión de Israel** por las reiteradas violaciones de los DDHH contra el deporte y la selección de fútbol de Palestina.

-Continuan los **desalojos forzosos de las familias más pobres de Medellin**. Esta vez se trata del **barrio Villa Café**, donde desde las 6:30 de esta mañana, **1500 personas de estratos 0 y 1 han sido desalojados a manos de 1000 agentes de la policía**, que han usado la violencia para lograr desplazarlos. **Walter Caicedo, líder comunitario de Villa Café**.

-A partir de este 28 de mayo, **una comisión de concejales de Bogotá** empezó a analizar el texto sobre la **consulta popular para acabar con las corridas de toros en la ciudad**, radicado este martes por las **organizaciones que hacen parte del colectivo "Bogotá sin toreo"**.

-Después que el ex vicepresidente de España , ex presidente del FMI y ex presidente de Bankia, **Rodrigo Rato**, fuera **detenido** durante el registro que efectuó en su casa de Madrid la fiscalía anticorrupción, **por los delitos de blanqueo, fraude y alzamiento de capitales.** Hoy ha sido **acusado formalmente de defraudar 2,5 millones de euros** a Hacienda.

-Desde esta mañana, **280 campesinos y campesinas provenientes del Catatumbo**, Norte de Santander, **se tomaron la entrada de la Defensoría del Pueblo en Bogotá**, con el objetivo de pedir la presencia de Jorge Armando Otálora, defensor del pueblo para que **el gobierno cumpla los acuerdos pactados anteriormente en la mesa de negociación**. Habla **Olga Quintero**, vocera de la **Asociación Campesina del Catatumbo, ASCAMCAT.**

-Este miércoles la **Unión Europea** ha puesto en funcionamiento la **medida presentada** la semana pasada para r**epatriar un total de 40.000 solicitantes de asilo** pertenecientes a **Siria y a Eritrea**, que actualmente están provocando una **crisis humanitaria en Grecia e Italia.**
