Title: Reserva forestal Van Der Hammen en peligro por planes de Peñalosa
Date: 2016-01-04 15:21
Category: Ambiente, Nacional
Tags: Enrique Peñalosa, Van der Hammen
Slug: reserva-forestal-van-der-hammen-en-peligro-por-planes-de-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/thomas4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [cop20.bogota.gov.co]

En los planes de la administración del anterior alcalde de Bogotá, Gustavo Petro, estaba hacer de la reserva Van der Hammen, **el bosque urbano más grande del mundo**, teniendo en cuenta que estos terrenos fueron declarados en 2011 por la Corporación Autónoma Regional de Cundinamarca, CAR, como Área de Reserva Forestal Productora Regional del Norte, lo que significa que tienen un fin forestal que debe conservarse y protegerse.

Sin embargo, durante el discurso de su posesión como alcalde de Bogotá, Enrique Peñalosa, hizo algunos comentarios que dejan en el limbo esta reserva forestal, cuyos terrenos estarían destinados a la urbanización y expansión de la ciudad, según las propuestas del actual alcalde.

**“La ciudad necesita crecer en ubicaciones optimas,** cada kilometro demás que tengan que recorrer los ciudadanos en la ciudad, todavía por construir, tiene enormes costos ambientales, económicos y sociales, vamos a aprovechar los terrenos que todavía hay en Bogotá”, dijo Peñalosa en su discurso este 1 de enero, quien además ha señalado que “el norte de Bogotá no es el norte de la ciudad. **El norte de la ciudad es Tocancipá, es Chía, es Cajicá”,** por lo que se ha referido explícitamente a que los terrenos de la Thomas Van Der Hammen son netos potreros sobre los que se puede construir.

Lo anterior contrasta con el objetivo por el cual fue creada la reserva, ya que representa una franja de conexión entre los cerros orientales y el río Bogotá, creada con el objetivo de impedir la urbanización entre Chía y la capital, para servir como escenario de recreación, conexión, protección y restauración de la principal estructura ecológica de Bogotá.

“El extremo norte del Distrito Capital constituye un área importante y única del patrimonio ecológico histórico y cultural. **El ecosistema Torca - Conejera, contiene características geológicas, climáticas, edafológicas, de vegetación y fauna que no existen en el resto del territorio del Distrito y que son muy escasas en el resto de la Sabana”,** fue lo que concluyó en el año 2000, un estudio realizado por una misión de expertos, cuando en su anterior administración, Enrique Peñalosa también había pretendido hacer de la reserva una zona urbanizada.

Es decir, que la propuesta de Enrique Peñalosa, estaría acarreando graves consecuencia para los **22 mil árboles que se han sembrado en el bosque Las Mercedes,** y además pone en riesgo lo que sería el segundo gran pulmón de la ciudad después de los Cerros Orientales y una joya ambiental para el mundo entero ya que contribuye a la conservación de la estructura ecológica principal de la capital que cuenta con **zonas montaña, piedemonte, humedales, altiplanicie y valle inundable**, y no solo potreros como lo ha planteado el alcalde.
