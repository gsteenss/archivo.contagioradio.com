Title: Campesinos de San Pablo confirman que Alvaro Rincón fue asesinado por el ejército
Date: 2016-09-13 17:02
Category: DDHH, Nacional
Tags: Alvaro Rincxon, asesinato, falsos positivos, Fiscalía, Fuerzas militares, Sur de Bolivar
Slug: campesinos-del-patio-bonito-confirman-que-alvaro-rincon-fue-asesinado-por-el-ejercito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/alvaro-rincon-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [13 Sep 2016]

Según el informe divulgado hoy por la Federación Agrominera del Sur de Bolívar, FEDEAGROMISBOL, el señor Alvaro Rincón fue asesinado en medio de un desembarco de tropas del Batallón Nueva Granada en la vereda Patio Bonito del municipio de San Pablo. La denuncia relata que **hubo disparos indiscriminados que alcanzaron a Don Álvaro en su propia vivienda y luego fue “rematado” por los propios militares.**

Ante la indignación que provocó el hecho pobladores de la vereda Patio Bonito y de veredas aledañas formaron un cordón humano **para evitar un levantamiento del cadáver sin la presencia de las autoridades de fiscalía** que debe realizarlo y que pueden adelantar una investigación, sin embargo, ante dicha reacción se movilizó el **ESMAD que dispersó a la comunidad y realizó el levantamiento** sin los procedimientos legales.

Se espera que en las próximas horas se conozca el resultado de una verificación que se está realizando con personal de la alcaldía de San Pablo, la Defensoría del Pueblo y algunos integrantes de las organizaciones sociales, ya que el **[ESMAD y las FFMM no permiten la entrada de campesinos a la casa de Alvaro Rincón](https://archivo.contagioradio.com/comunidad-asegura-que-ejercito-nacional-asesino-a-lider-campesino/).**

Este es el comunicado de FEDEAGROMISBOL

##### "La Federación Agrominera del Sur de Bolívar – FEDEAGROMISBOL y la Corporación Sembrar denuncian los siguientes hechos:

1.  ##### Según versiones de la comunidad, a las 5 de la tarde del día domingo 11 de septiembre, un helicóptero artillado realizó un desembarco de tropa, perteneciente al Batallón Nueva Granada, en el municipio de San Pablo, vereda Patio Bonito.

2.  ##### Al momento del desembarco se presentaron ametrallamientos indiscriminados, en estos ametrallamientos fue alcanzada la vivienda del señor Alvaro Rincón, afiliado a la junta de Acción Comunal de la vereda, quien resultó herido y posteriormente rematado, por uno de los militares dentro de su propia vivienda, según lo relataron los pobladores de la zona, quienes afirmaron que el señor Rincón se abrazo a su hijo pidiendo que no lo asesinaran.

3.  ##### Las comunidades vecinas de la vereda Patio Bonito al percatarse de lo ocurrido, se reunieron en la finca para ayudar la familia de la víctima, mientras llegaban pobladores de los corregimientos vecinos un poco mas retirados, como Vallecitó y Diamante; con el fin de no dejar levantar el cadáver hasta no esclarecer lo sucedido.

4.  ##### La fuerza publica al ver la reacción de la comunidad llevo hasta la zona miembros del Escuadrón Móvil Antidisturbios – ESMAD, quienes quemaron una moto y arremetieron contra los pobladores hiriendo a varios de ellos y retirando el cuerpo del señor Alvaro Rincón.

5.  ##### La comunidad fue dispersada por el ESMAD y no se les permite acercarse al lugar de los hechos.

##### En el día de hoy una comisión integrada por la defensoría del Pueblo y delegados de la alcaldía del municipio de San Pablo, junto con líderes de la comunidad, viajaron hacia la zona con el fin de verificar lo sucedido en la vereda Patio Bonito.

##### Solicitamos:

##### Que se investigue y sancione a los responsables de los hechos ocurridos en los que fue asesinado el señor Alvaro Rincón, y resultaron varias personas heridas.

##### Garantías y protección a la vida e integridad física de las comunidades agromineras del Sur de Bolívar."
