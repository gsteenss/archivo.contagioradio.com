Title: "Condenamos asesinato de mi hermana del alma" amiga de Berta Cáceres
Date: 2016-03-03 19:28
Category: DDHH, Entrevistas
Tags: Berta Cáceres, Copinh, honduras
Slug: condenamos-asesinato-de-mi-hermana-del-alma-amiga-de-berta-caceres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Berta-Caceres-1-e1457038761300.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Goldman Price 

<iframe src="http://co.ivoox.com/es/player_ek_10672030_2_1.html?data=kpWjmZeUd5Ghhpywj5eWaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5yncYamk6jc0MnJssLh0NiYw9jJt8riwtncjcnJb87djM3S1NLFssKfxcrZjcbQscKZk5eYw9LNq8KfxcqYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Miriam Miranda] 

###### [3 Mar 2016] 

“Rechazamos y condenamos este asesinato de mi compañera, de mi hermana del alma”, expresa con la voz entre cortada Miriam Miranda, defensora de derechos humanos y amiga de Berta Cáceres. Ella fue una de las primera personas que se enteró del asesinato de la líder indígena, hecho que para Miriam representa “un golpe fuerte para la vida de los pueblos indígenas y negros de Honduras, y un golpe muy duro para la democracia”.

Miriam y Berta estuvieron de gira el año pasado en diferentes países de América compartiendo su experiencia de lucha frente a los proyectos extractivistas. Miriam relata que pese a las amenazas, las múltiples denuncias a las autoridades y la medida cautelares ordenadas por la CIDH, el Estado hondureño no hizo nada por proteger la vida de la ambientalista. Pero ni eso, ni **las amenazas de agredirla sexualmente, de violentar a su madre, secuestrar a sus hijas y tampoco el asesinato de sus compañeros** de lucha, hicieron que Berta dejara de enfrentarse con empresas y la represión estatal.

En abril del 2015 recibió el premio Goldman, un prestigioso reconocimiento para las personas del mundo que dedican su vida a la defensa del ambiente, cuya lucha  logró que la compañía China Sinohydro, e incluso, el Banco Mundial renunciaran a continuar apoyando el proyecto hidroeléctrico en el Río Gualcarque, sitio sagrado para la etnia lenca.

"Las amenazas en mi contra siempre han sido por defensa del territorio y por el hecho de ser mujer, se me prohibió salir del país, querían prohibirme mi actividad política y organizativa" dijo hace algunos meses Berta en una entrevista a Contagio Radio.

Su amiga Miriam, recuerda a Berta como una mujer que luchaba para que “pudiéramos vivir mejor”, junto a ella, la también defensora de derechos humanos camino muchos años. “Hemos caminado juntas por la ratificación del Convenio 169 de la OIT para exigir el derecho a la consulta previa, caminamos con 10 mil indígenas exigiendo respuestas  a las problemáticas de las comunidades…  Hemos perdido una hermana del mundo”, dice Miriam.

De acuerdo con la compañera de Berta, las autoridades ya manejan una hipótesis de robo, lo que para ella “es una constante en América Latina donde se acalla la vida de los defensores de los derechos humanos”, y resalta que las personas desconocidas que ingresaron al apartamento de Berta no se robaron nada, pero aclara que la responsabilidad del gobierno es contundente, pues en Honduras “Los grupos del poder económico definen la agenda del estado”.
