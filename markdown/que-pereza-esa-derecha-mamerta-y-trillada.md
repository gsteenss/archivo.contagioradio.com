Title: ¡Que pereza esa derecha mamerta y trillada!
Date: 2016-05-16 12:15
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Centro Democrático, uribe, Uribistas
Slug: que-pereza-esa-derecha-mamerta-y-trillada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Uribistas-en-congreso.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: risaraldahoy] 

#### **Por [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 16 May 2016

[Pero ¡que mamertos! todos los del Centro Democrático con sus discursos derechosos trillados; parecen hordas desesperadas cargadas de argumentos inverosímiles, juicios de clase, uso de conceptos que ni conocen; incluso en ocasiones, a más de uno le da por convertirse en “esperpento de experto” cuando explican con cara de analistas, cómo la izquierda se desmorona en el continente, pero temen aceptar que la derecha siempre se ha desmoronado y sólo se ha mantenido a punta de corrupción y guerra sucia .]

[Hace poco, yo creo que el ex militante sindical, ex miembro del EPL y actual propagandista del uribismo señor Obdulio Gaviria, de seguro invitó a su partido a buscar en los métodos y conceptos de izquierda, la salida a la crisis y al fracaso tan grande que han demostrado ser, como derechosos y como partido.]

[Es que no hay nada más vergonzoso que una ultraderecha derecha fracasada como la del Centro Democrático, tratando de subir rating utilizando conceptos como]*[resistencia civil]*[,]*[castrochavismo]*[,]*[golpe de Estado a la democracia]*[, para acusar al gobierno Santos, a la izquierda local, regional y en general a cualquiera que no le haga venias a su patrón.]

[Esta gente, ante el vacío absoluto de argumentos ideológicos, coherentes y éticos, no le queda de otra que elaborar debates fantasiosos, ocultar toda la corrupción y falsedad que se vivieron durante el mandato de su patrón, generar adeptos con falacias o mejor “idiotas útiles” y finalmente contar siempre con un as bajo la manga: sus adorados RCN y NTN24.]

[Esos canales son todo un lío, pues le celebran a esta derecha refrita, todos los días y sobre todo ¡todas las noches! la mamertiadera. Sin esos canales, los derechosos estarían enterrados hace mucho, pero mucho tiempo, ¡ellos lo saben! si esos canales dejaran de existir, de seguro a la directora de noticias, le tocaría hacer lo mismo que a Alfredo Rangel, cuando abandonó su rol de “analista político”, y pasó a posesionarse coherentemente como senador del Centro Democrático; pues hoy por hoy, las posiciones editoriales de esos canales no son fieles a una profesión, sino más bien a una ideología derechista mamerta y trillada.]

[¿Por qué esa derecha mamerta construye toda esta falacia discursiva? Bueno, primero que todo, es totalmente imposible, que por conclusión política, académica, ética y lógica una persona se convierta en un mamerto derechoso; el uribismo (representante por antonomasia de la derecha mamerta) ha demostrado ser nada más que un acto de fe.  Por eso, la única razón por la cual los del Centro Democrático siguen hablando tantas sandeces, es porque como no pueden convencer ética, académica o políticamente, no les queda de otra sino seguir protagonizando ridículos a punta de golpes de opinión; es que en serio, no tienen nada más…]

[No obstante, en medio de todo, está sucediendo algo grave; me refiero específicamente al momento en que esa derecha fracasada, deja de ser mamerta y pasa a convertirse en una infame, al utilizar a las madres de los soldados caídos para hacer política.]

[Eso es rastrero, eso es ser mercantilista del dolor, muchos soldados han muerto, y esos muchachos no son mejores o peores, no los devolverán a los brazos de sus seres queridos porque un presidente les ponga una medalla en su ataúd. Si Uribe no quiso la paz y prefirió sustituir a los soldados muertos con más muchachos “frescos” y Santos ha preferido apostarle al fin del conflicto para que precisamente no mueran los mismos de siempre, eso debería alejarse del manoseo absurdo que han hecho los uribistas con las madres de los soldados muertos, que como todas las madres, que han perdido hijos e hijas de cualquier bando en esta penosa guerra, ¡son víctimas! no de primera o segunda categoría,  sino que son VICTIMAS y merecen respeto sin distinción ideológica o partidista; esa es la mínima condición para la paz, pero claro, un mamerto no puede comprenderlo, y un infame, no respeta el dolor de una muerte, sino que saca provecho politiquero.   ]

[En conclusión, la ultraderecha en Colombia hoy, como un discurso serio está mandada a recoger, ha demostrado estar cada vez más sola, cada vez más aislada, y con un único as bajo la manga: sus adorados canales, que, por radio, televisión e internet, hacen las de escuderos de la infamia, el cinismo y la pendejada.]

[No me canso de repetirlo, por el momento la derecha en Colombia ha sido, es y seguirá siendo un completo y rotundo fracaso; se ha demostrado que su estancia en el poder no ha sido producto de la política, sino de la corrupción y la criminalidad, ¡y bueno! esa es harina de otro costal, pues para nadie es un secreto que, a la par de una derecha mamerta y desesperada en campaña contra el proceso de paz, tristemente se pronostica el surgimiento de una segunda unificación paramilitar nacional (la primera fueron las AUC)… casualidad o no, los mamertos de derecha están iracundos porque no logran convencer, y el paramilitarismo es delincuencia en su más alta expresión, que apunta sus cañones específicamente contra la base social de la izquierda. Pero bueno, para no extenderme, esa es “harina de otro costal” …]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
