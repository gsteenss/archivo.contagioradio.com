Title: ¿Cómo afecta la calidad del aire a los bogotanos?
Date: 2019-09-20 15:55
Author: CtgAdm
Category: Ambiente
Tags: Alerta, ambiente Bogotá, contaminación de aire, oxigeno
Slug: aire-de-bogota-en-alerta-roja
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/23347680711_6a8983c791_b.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/bogota1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [[Foto:[Semanario Voz](http://semanariovoz.com/author/vozdigital/)]] 

Irritación ocular, congestión, fatiga y gripas continuas; son algunos de las afectaciones que viven las y los bogotanos debido al elevado nivel de contaminación que se ha registrado en la ciudad en los últimos días, a lo que se suman incendios y acumulación de CO2  que están provocando graves daños ambientales, y especialmente en la calidad del aire de la  capital. (Le puede interesar:[Biciusuario entutela Bogotá por contaminación de aire](https://archivo.contagioradio.com/biciusuario-entutela-bogota-por-contaminacion-de-aire/))

Según Camilo Prieto, Integrante del Movimiento Ambientalista Colombiano, los indicadores aire en rojo y naranja que se encuentran en el sitio aqicn.org (*Air Pollution: Real- time Air Quality Index*por por sus siglas en Ingles), evidencian que el aire  en **varias zonas de Bogotá,** como Fontibón, Kennedy , Puente Aranda y las salidas de la capital, como Autopista Norte y Sevilla, tendrían un alto **riesgo para la salud de niños y  adultos mayores, mientras que el resto de la ciudad se encuentra en riesgo intermedio.**

Asimismo, Prieto aseguró que las personas que van transitando por la circunvalar o por las vías altas de la ciudad pueden notar fácilmente una capa de *smog*. Este fenómeno climatológico es básicamente un producto de la disminución de las lluvias y el aumento de las temperaturas, que favorecen el surgimiento de una situación conocida como inversión térmica.(Le puede interesar:[Peñalosa no atiende recomendaciones académicas sobre calidad del aire en Bogotá](https://archivo.contagioradio.com/penalosa-no-atiende-recomendaciones-academicas-calidad-del-aire-bogota/))

De igual forma, un estudio que se realizó el año pasado, reveló que las personas que viajan en el Sistema Integrado de Trasporte Público (SITP) especialmente en los buses azules, respiran once veces más contaminación que una persona que está afuera de estos articulados.

“**Es necesario que las personas tomen medidas de protección por la calle**, si van a salir de sus casas y sobretodo si van a utilizar el servicio publico, hagan uso de un tapabocas M95 que ayude a disminuir las partículas y gases nocivos que entran a nuestros sistemas respiratorios” aconsejó finalmente el activista.

### **El nivel del aire en cifras** 

Un hecho que llama la atención es que en Colombia solamente dos ciudades manejan datos abiertos sobre la calidad del aire: Bogotá y Medellín. En ninguna otra localidad de Colombia se puede realizar una verificación sobre la condición del aire. (Le puede interesar:[Calidad del aire en Bogotá y licitación de nuevos buses de Transmilenio](https://archivo.contagioradio.com/55845-2/))

Prieto declaró que “se pueden encontrar datos donde el 88% de las estaciones de medición registran una buena calidad de aire en el país, pero cuando se comparan estos datos con sugerencias que da la organización de la salud desde el año 2005, resulta que el 76%  de las estaciones no cumple con los estándares; esto quiere decir que tres cuartas **partes del país , respira un aire con un nivel peligroso**”.

Para poder enfrentar este asunto, el ambientalista señaló que **son necesarias políticas publicas adecuadas y una movilidad más limpia y sostenible que permitan el decrecimiento de las emisiones de CO2 y,** pese a que en algunas ciudades del país como Medellín ya se han puesto en marcha sistemas de transporte limpios con buses eléctricos, en Bogotá este tipo de licitaciones continúan lejanas, mientras que las afectaciones al sistema respiratorio siguen en aumento con un total de 705.756 personas atendidas por enfermedades provocadas por la contaminación.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
