Title: Carabinero a prisión por arrojar a joven al río en Chile
Date: 2020-10-05 21:18
Author: AdminContagio
Category: El mundo
Tags: brutalidad policial, Carabineros, Chile
Slug: carabinero-a-prision-por-arrojar-a-joven-al-rio-en-chile
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/captura-de-pantalla-Chile.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Captura de pantalla

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este sábado 3 de octubre, la Fiscalía Centro Norte de Chile detuvo al **carabinero que habría arrojado a un joven de 16 años al río Mapocho, durante las manifestaciones que se desarrollaron el pasado viernes en Santiago.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/lemonnsweetie/status/1312460837550878721?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1312460837550878721%7Ctwgr%5Eshare_3\u0026amp;ref_url=https%3A%2F%2Fwww.publimetro.co%2Fco%2Fnoticias%2F2020%2F10%2F05%2Faterrador-video-policia-arrojo-joven-puente-plenas-manifestaciones.html","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/lemonnsweetie/status/1312460837550878721?ref\_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1312460837550878721%7Ctwgr%5Eshare\_3&ref\_url=https%3A%2F%2Fwww.publimetro.co%2Fco%2Fnoticias%2F2020%2F10%2F05%2Faterrador-video-policia-arrojo-joven-puente-plenas-manifestaciones.html

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Después de que se dieran a conocer los hechos, la Institución de Carabineros rápidamente negó el acto. Sin embargo, el sábado reconoció la participación del policía, que fue retirado de sus funciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente, el juez de Garantías de Santiago, determinó este domingo, sobre las 11 de la mañana, prisión preventiva contra el carabinero identificado como Sebastián Zamora Soto de 22 años por los delitos de “presunto homicidio frustrado” y omisión de socorro. (Le puede interesar: [El aparato del Estado se revistió de legalidad para afectar la Movilización: Análisis](https://archivo.contagioradio.com/aparato-estado-revistio-legalidad-afectar-movilizacion/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FRCentroNorte/status/1312857501784838144","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FRCentroNorte/status/1312857501784838144

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El hecho se dio a conocer por medio de un [video publicado](https://twitter.com/teleSURtv/status/1312377996796407808)por el canal de televisión Telesur, en el que se evidencia como el carabinero hace caer al joven a una distancia de siete metros, causándole traumatismo encéfalo craneano cerrado y fractura bilateral de muñeca. (Le puede interesar: [La violencia policial es la nueva pandemia: Casos a nivel mundial](https://archivo.contagioradio.com/la-violencia-policial-es-la-nueva-pandemia-casos-a-nivel-mundial/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al caer el joven, los rescatistas de la brigada de salud conformada por estudiantes de medicina lo asistieron y fueron quienes lo llevaron a la Clínica Santa María, donde hoy se recupera.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/alejandramatus/status/1312789451370790912","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/alejandramatus/status/1312789451370790912

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Después de conocerse los hechos, chilenos volvieron a salir a las calles, y a la Clínica Santa María con carteles que rezaban *"No se cayó, lo lanzaron"* para rechazar la violencia con la que la institución arremete contra los manifestantes. En estas movilizaciones, 20 personas fueron detenidas, según reportó la policía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta nueva jornada de protesta se da a solo tres semanas de realizarse el plebiscito que definirá si Chile entra o no en un proceso constituyente y en medio de la solicitud de renuncia del director general de Carabineros, Mario Rozas, por las violaciones a los derechos humanos desde el estallido social del 18 de octubre de 2019.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras tanto, el representante en América del Sur de la Alta Comisionada de las Naciones Unidas para los Derechos Humanos, señaló que es necesario “investigar y, si procede, juzgar y sancionar” al agente directamente involucrado, sino también “la eventual responsabilidad de los mandos a cargo de la operación”.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
