Title: La noche universitaria
Date: 2020-07-31 01:25
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: columnistas, Universidad Nacional de Colombia, Universidades, Universidades Privadas, Universidades públicas en Colombia, universitarios
Slug: la-noche-universitaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/estudiantes_2_0.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/grabacion-johan.mp3" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:audio {"id":87674} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/grabacion-johan.mp3">
</audio>
  

<figcaption>
Escuche aquí la nota completa

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Decía Gramsci que el poder está dado por la hegemonía cultural que logran las clases dominantes por medio del control del sistema educativo, las instituciones religiosas y los medios de comunicación de masas; si esto es así, la educación puede ser considerada tanto como un elemento liberador o democratizador de la sociedad, como también un medio de control.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Es decir… con pena hoy hay que decir que ser universitario no significa ser un sujeto liberado o “menos bruto”.  

<!-- /wp:heading -->

<!-- wp:paragraph -->

Pero eso sí, cuando la educación se constituye en un medio de control, es posible que las formas de la crítica que deberían emergen en los profesores o en los estudiantes queden asfixiadas por la eficacia, y el tecnicismo que solicita un sistema educativo para el trabajo y al que no le conviene un sistema escolarizado que contiene un proceso teórico, ético y humanista amplio. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Por qué damos por hecho que la educación para el trabajo es más importante que la educación universitaria (universal)? el sentido común diría, “porque el trabajo es más importante”. Esto podría hacernos formular preguntas tales como ¿qué clase de trabajo? ¿trabajo a cualquier precio? ¿acaso no se debería ganar la vida honradamente haciendo lo que se quiere y no lo que toca? 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para nadie es un secreto que, en muchas ocasiones, los estudiantes estudian sus carreras en detrimento de aspiraciones y ambiciones diferentes de las que se hallan comúnmente estereotipadas; hay gente estudiando x carrera, habiendo soñado estudiar y.   

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ¿pero y qué de las ambiciones o aspiraciones personales? ¿son realmente auténticas? 

<!-- /wp:heading -->

<!-- wp:paragraph -->

El sentido crítico del sujeto aislado, de ese sujeto que ahora, o incluso antes de la pandemia andaba en la casa es tan bajo, que él mismo se vuelve parte de una masa compuesta por sujetos aislados que pierden simultánea y aisladamente la capacidad crítica por causa de la estimulación excitante y placentera de información, series, imágenes, memes, micro videos, que los dopan durante horas en soledad. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ahora bien, esto que llega en forma de consumo informativo y de entretenimiento, tiene contenido hegemónico, lo que desarrolla en ese sujeto dopado todos los días con lo mismo, una naturalización de ese contenido hegemónico. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Naturalizar el contenido hegemónico significa convertirlo en sentido común, y eso en esencia es una la alucinación colectiva que pasa a ser la forma exclusiva de la realidad. Por eso, la personalidad de los sujetos, que parece ser tan auténtica, en realidad está planificada y construida sobre la base del control del placer que permite generar las bases de una dominación pasiva pero muy eficaz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### ¿Pero cómo funciona esa vaina de la alucinación colectiva? 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Gustave Le Bon el mecanismo de alucinación colectiva, parte de una masa en atención expectante. ¿Cuál masa? ¡pues usted, yo, el vecino! Tengamos o no tengamos títulos universitarios. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nosotros siempre esperamos que algo suceda, pues la vida monótona suscita una constante expectativa. Entonces, si nos sugestionan masivamente por las redes comunicativas, desde mensajes formales o informales, se logra la deformación y el contagio de ideas que surgen desde la hegemonía. Es allí, cuando hay deformación, que los hechos reales o la realidad es sustituida por la alucinación. Si se suman a esto las acciones que puedan derivarse, entonces se compone un hecho colectivo, un sentido común, que pervive cuando agrega o quita cosas a la realidad, es decir, deformándola.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Apliquemos este planteamiento al campo educativo: 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Si la alucinación colectiva, (aceptada como sentido común) es la educación para el trabajo, entonces, la educación universitaria (universal), comenzará a sufrir un desgaste del pilar más importante, que en este caso son las cosas que la gente cree sobre la educación. Porque la creencia sobre las cosas puede superar a la realidad misma de las cosas, nos cueste entenderlo o no. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fíjese usted. En Colombia, tres generaciones atrás, abuelos y abuelas daban su vida entera para que sus hijos estudiaran en una Universidad. El trabajo y el dinero se conseguían, pero dar estudio universitario a los hijos en Colombia hace 30 o 40 años era el prestigio más alto que podía tener una familia. Hoy más parece un requisito y me atrevería a especular, que ya ni requisito.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El valor se ha perdido no porque la educación se más o menos buena (aunque esto sería otro debate) sino porque la gente cree que el valor educativo ya no es tan importante como otras cosas. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por ello para un estudiante, atravesar tantos semestres con materias que supuestamente no otorgan satisfacciones respecto a las costumbres habituales de consumirlo todo, se convierte en una aparente tortura. El nuevo sentido común consiste en creer que estudiar ya no es tan importante, y eso mina y destruye el sentido crítico de un sujeto al que se le evapora el propósito mismo de permanecer 5 años en una universidad; sencillamente no lo halla y le gustaría que fuera más fácil, más rápido, como un paquete de algún producto comestible, algo “abre fácil”. Es que educarse no es un trabajo fácil, ¡es todo un anti-placer! lleva tiempo, implica sacrificio, desgaste, etc.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero esta larga noche universitaria está permitiendo preguntas tales como ¿para qué leer cientos de páginas sobre historia de Latinoamérica si se puede ver la historia de Latinoamérica contada en 3 minutos por Youtube?  Con preguntas así, el sujeto ha permitido la pérdida del sentido real de la educación y ha aceptado la alucinación colectiva como sentido común. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A cambio de seguir inconsciente en un mundo prediseñado, el sujeto crítico debería plantearse nuevos mundos posibles de educación, de placer, de liberación política, de liberación del tiempo libre, porque no hay cosa más presa hoy que nuestro tiempo libre, porque no hay ambiente en el que nos comportemos más esclavos, como en el del tiempo libre. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta lucha por los nuevos mundos posibles no es dolorosa, pero sí genera resistencia porque el poder de las masas que viven la alucinación y de la que el sujeto es preso, no es racional, es decir, el sujeto crítico puede ser apartado de forma cruel y traumática. El típico ejemplo de esto siempre fueron el “ñoño” la “ñoña”, esos personajes que son rechazados por la masa, por ser los que asumen profundamente el no-placer que implica educarse (en sentido real). 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### ¿Pero cómo lucha un sujeto contra toda la fuerza del sentido común impuesto?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cuando el sujeto no desarrolla una lectura crítica de la información que se le está presentando a través de los mass-media, esto lo conduce a la composición de la idea de la realidad a partir de una sola narrativa que podría ser hegemónica.  Siendo así, el sujeto es más maleable, intempestivo o desinteresado frente a los temas político-culturales, y aparece presentándose en la práctica cotidiana, sea virtual o presencial, como “a-político” o “ni esto ni aquello”; cosa que es simplemente un sofisma simbólico de su propia subjetividad que se halla dirigida desde marcos hegemónicos que no se ha tomado el trabajo educativo y crítico de comprender.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Entonces, el oscurantismo contemporáneo está cargado de luces pero que te dejan ciego.

<!-- /wp:heading -->

<!-- wp:paragraph -->

No hay nada más que ruido en la noche universitaria, egos pendejos de profesores pendejos, egos pendejos de estudiantes pendejos, porque el ego es el refugio más seguro que tienen aquellos que temen reconocer el engaño en el que viven; la noche universitaria seguirá siendo larga mientras, la universidad y todos sus habitantes, no tengan las capacidades para enfrentarse a lo que el nuevo paradigma “educación para el trabajo” está haciendo de ella… pero, es que es tremendamente jodido hacer propio y abrazar un anti-placer como resulta ser la educación realmente universitaria. Las cartas están sobre la mesa, que comience el semestre. 

<!-- /wp:paragraph -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:heading {"level":6} -->

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Vea m](https://archivo.contagioradio.com/opinion/columnistas/)[á](https://archivo.contagioradio.com/opinion/columnistas/)[s: Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
