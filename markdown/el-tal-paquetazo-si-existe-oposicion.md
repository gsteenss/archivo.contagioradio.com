Title: El tal paquetazo sí existe: Oposición
Date: 2019-11-15 10:15
Author: CtgAdm
Category: Movilización, Nacional, Paro Nacional
Tags: 21 de noviembre, Gobierno, paquetazo, Paro Nacional
Slug: el-tal-paquetazo-si-existe-oposicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Marcha-apoyo-campesinos082813017.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Ministros y el presidente Duque han señalado en medios de comunicación que la reforma laboral y pensional no existen, y por lo tanto, no existe el 'paquetazo' que convoca al paro nacional del próximo 21 de noviembre. Sin embargo, desde la banca de oposición respondieron diciendo que ambas reformas sí se han anunciado en cabeza de los Ministros de Hacienda y Trabajo, y que otras razones para una masiva movilización son el aumento de la violencia en los territorios, los bombardeos a niños y el asesinato de líderes sociales.

### **¿Gobierno niega las razones para convocar al paro?  
** 

En declaraciones a medios de comunicación, el Ministro de Hacienda Alberto Carrasquilla, la Ministra de Trabajo Alicia Arango y el mismo presidente Duque, han señalado que no existe el 'paquetazo', **aduciendo que no hay proyectos de Ley en el Congreso que contengan dichas medidas económicas y por lo tanto, no hay razones para convocar a un paro nacional.** Para el senador por el Polo, **Wilson Arias,** "ni siquiera en eso acierta el Gobierno", porque los motivos del paro son más que las reformas pensional, laboral y salarial, que de una u otra forma se han anunciado.

En primer lugar, Arias resaltó que el Gobierno solo ha intentado desmentir dos temas: La reforma laboral y la reforma pensional, pero aclaró que son modificaciones que la misma Ministra de Trabajo y Ministro de Hacienda han señalado que deben hacerse de una u otra manera, en consonancia con declaraciones del Fondo Monetario Internacional (FMI), la Organización para la Cooperación y el Desarrollo Económico (OCDE) y distintos gremios empresariales.

**Carrasquilla ha dicho en diferentes entrevistas que las personas se deben pensionar según su capacidad de ahorro,** lo que significa, entre líneas, que se acaba Colpensiones o que quedará solo con el régimen de ahorro individual y no el de prima media. Por otra parte, **la ministra Arango ha expresado la necesidad de 'flexibilizar' la jornada laboral** para aumentar las cifras de empleo, lo que para Arias significa la precarización del trabajo. (Le puede interesar: ["¡Que nadie se quede en casa! 21 de noviembre, paro nacional"](https://archivo.contagioradio.com/que-nadie-se-quede-en-casa-21-de-noviembre-paro-nacional/))

En ese sentido, el Senador recordó que en el **artículo 193 del Plan Nacional de Desarrollo (PND)** se incluyó el piso de protección social en el que "dice claramente: Usted puede tener una contratación de tiempo parcial y le pagan proporcionalmente al tiempo trabajado, por eso, el empleador no aporta al sistema de seguridad social, sino a los Beneficios Económicos Periódicos (BEP's)". (Le puede interesar: ["De 6 países que privatizaron su régimen pensional, 4 revirtieron la decisión: Wilson Arias"](https://archivo.contagioradio.com/de-6-paises-que-privatizaron-su-regimen-pensional-4-revirtieron-la-desicion-wilson-arias/))

### **El bombardeo a niños, el asesinato de líderes sociales, la violencia en Cauca...** 

Para Arias, además  del 'paquetazo', hay razones genuinas para manifestarse pidiendo modificaciones a las políticas de Duque, entre ellas, al PND que "incluye una reforma ambiental minera". De acuerdo al Senador, hay una reforma más que sí está en el Congreso, la "Ley de Financiamiento que es odiosamente pro ricos, porque les concede beneficios", y "está el tema de asesinato de líderes sociales". (Le puede interesar:["Duque llevará al Congreso la misma Ley de Financiamiento que le devolvieron"](https://archivo.contagioradio.com/duque-llevara-congreso-misma-ley-de-financiamiento/))

En tal sentido, el Congresista recordó que también se ha aumentado la violencia en diferentes territorios ante la aparente incapacidad del Gobierno de solucionar la situación, se ha incumplido los acuerdos pactados con los profesores y los estudiantes de educación superior, y el Ministro de Defensa renunció a su cargo luego que se hiciera un debate de control político en el que se reveló que las fuerzas militares bombardearon un campamento con niños. (Le puede interesar:["Botero debe asumir responsabilidad política y penal por bombardeo a niños"](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/))

### **"No es por 30\$, es por 30 años"** 

El senador Arias sostuvo que en una ocasión, el mismo senador Uribe dijo que en Colombia se ha aplicado gradualmente el 'paquetazo', es decir, que las reformas laborales, salariales y tributarias no se han pasado como un conjunto de medidas que deben ser aprobadas al tiempo, sino como leyes que son aprobadas con meses de diferencia. Por lo tanto, señaló que Colombia se parecía a Chile, porque la frase de "No es por 30 pesos, es por los 30 años" de reformas neoliberales, se puede trasladar al contexto nacional.

En consecuencia, Arias concluyó que **la suma de estos años de reformas ha hecho que los colombianos tomen conciencia de lo que han vivido y salgan a manifestarse** el próximo 21 de noviembre, y dijo que en la calle se estaba sintiendo esa "voz de solidaridad y acompañamiento" con el paro. (Le puede interesar: ["La unión del pueblo y la persistencia: claves en la movilización de Chile"](https://archivo.contagioradio.com/la-union-del-pueblo-y-la-persistencia-claves-en-la-movilizacion-de-chile/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44419980" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44419980_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
