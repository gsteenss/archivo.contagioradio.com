Title: Condenan a 72 meses de prisión a ex director de inteligencia del DAS por tortura a Claudia Julieta Duque
Date: 2015-10-01 15:23
Category: Judicial, Nacional
Tags: Actividades ilegales del DAS, Alvaro Uribe, Carlos Alberto Arzayús, Claudia Julieta Duque, das, Fiscalía General de la Nación, Jorge Noguera
Slug: condenan-a-72-meses-de-prision-a-ex-director-de-inteligencia-del-das-por-tortura-a-claudia-julieta-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/DAS_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: radiosantafe 

###### [1 Oct 2015]

El exdirector de Inteligencia del DAS y exjefe de la Oficina de Control Interno del DAS, **Carlos Alberto Arzayús Guerrero, acaba de ser condenado por el Juzgado Segundo Penal Especializado a 72 meses de prisión** por la tortura psicológica cometida contra la periodista Claudia Julieta Duque.

Arzayús aceptó cargos el pasado mes de Marzo y en diciembre fue condenado Hugo Daney Ortiz, exsubdirector de **Operaciones de Inteligencia del DAS**, y en junio el Tribunal Superior de Bogotá modificó y aumentó la condena contra Jorge Armando Rubiano, exsubdirector de Desarrollo Tecnológico del DAS.

La periodista también denunció que en los últimos meses su situación de seguridad ha venido empeorando ya que se ha percatado de seguimientos, intimidaciones e incluso un intento de entrar en su apartamento ubicado en la ciudad de Bogotá. Según Duque el **incremento de incidentes de seguridad ha estado directamente relacionado con el avance en los procesos judiciales** en contra de sus atacantes. Vea también [Se incrementan segumientos contra Claudia Julieta Duque](https://archivo.contagioradio.com/se-incrementan-hostigamientos-seguimientos-y-tortura-contra-claudia-julieta-duque-y-su-familia/).

Tanto la misma Claudia Julieta Duque como su abogado defensor **han solicitado la vinculación de otros 19 funcionarios del DAS**, varios de los cuales continúan trabajando en diferentes instituciones del Estado, y se espera que la Fiscalía tome decisiones a este respecto en los próximos días.
