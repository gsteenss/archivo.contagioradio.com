Title: 4 campesinos detenidos en desalojo a predios en Puerto Lleras
Date: 2016-12-13 16:17
Category: DDHH, Nacional
Tags: Desalojo, Meta, Palmas de Ariari, Puerto Lleras
Slug: campesinos-desalojo-puerto-lleras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/ESMAD-Caquetá.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Desplazados-e1481538333709.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Archivo Contagio Radio 

##### 13 Dic 2016 

A pesar de haber sido notificados de la medida provisional, solicitada mediante acción de tutela por la comunidad, el Escuadrón Móvil Anti disturbios ESMAD, continuó la tarde de este lunes con [el desalojo de 83 familias que habitan en zona rural del municipio de Puerto Lleras Meta](https://archivo.contagioradio.com/desalojo-familias-puerto-lleras-meta/), registrando este martes **la detención de 4 campesinos, habitantes de esos predios que serían judicializados en las próximas horas.**

**La medida que ordena la suspensión de la diligencia de desalojo**, le fue notificada al alcalde de la población, sin embargo **el ESMAD derribó durante la tarde del lunes dos viviendas más y sus respectivos cultivos de pan coger**, con lo que son ya 7 las familias que han perdido sus hogares y que actualmente se encuentran donde algunos vecinos que les han tendido la mano.

Para el abogado Francisco Henao, de la Corporación Jurídica Yira Castro, es preocupante que aun conociendo la medida que ordenaba frenar el desalojo, "**no se explicó por qué inmediatamente no se suspedió la acción**" y resulta muy extraño que **los objetos personales y enseres de las viviendas derribadas se encuentren en posesión de Palmas de Ariari S.A**., [empresa que interpuso querella por la supuesta propiedad de los predios](https://archivo.contagioradio.com/campesinos-desalojo-puerto-lleras-meta/).

"Es irregular que los campesinos deban ir a reclamarlos allá, violando los derechos de las personas que trataron de impedir el lanzamiento" asegura el defensor quien añade que, según información directa, **el abogado de la compañía palmera era quien dirigía desde tractores la destrucción de las viviendas y de los cultivos**.

L**a orden de desalojo estará suspendida por el momento mientras termina el trámite**. El tribunal del Meta tiene 10 días hábiles para responder a la acción de tutela, resolviendo las inquietudes existentes sobre la propiedad de los predios de los que no se encuentran registros posteriores a la querella presentada y buscar **se garanticen los derechos de la comunidad, quienes son sujetos de Reforma Agraria cobijados por la ley 160**.

Adicionalmente y luego de garantizar los derechos de los habitantes, el abogado asegura **se deberia investigar a los funcionarios municipales quienes han hecho caso omiso a las advertencias sobre las irregularidades en el proceso**. Los pobladores reunidos en ASOCAMPROVI se reunieron este martes con el fin de revisar las pruebas del lanzamiento, fotos, videos que sirvan para entablar denuncias correspondientes.

<iframe id="audio_14941254" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14941254_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
