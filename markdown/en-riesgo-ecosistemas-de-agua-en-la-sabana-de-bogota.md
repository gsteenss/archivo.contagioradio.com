Title: En riesgo ecosistemas de agua en la Sabana de Bogotá
Date: 2016-12-22 18:00
Category: Ambiente, Nacional
Tags: defensa ambiental, Mineria, sabana de Bogotá
Slug: en-riesgo-ecosistemas-de-agua-en-la-sabana-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/sabana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Panoramio] 

###### [22 Dic 2016] 

El pasado miércoles, el Ministerio de Ambiente aumentó la cantidad de zonas compatibles para minería en la sabana de Bogotá, que con el aval **pasaron de abarcar 11.000 a 18.000 hectáreas. Hasta el momento la magistrada Nelly Villamizar suspendió la decisión,** pero defensores ambientales temen que en cualquier momento se reactive poniendo en peligro ecosistemas hídricos vitales.

El abogado y defensor ambiental Rodrigo Negrete aseguró que el fallo da dos mensajes a la ciudadanía, “el primero es un parte de tranquilidad en cuanto a que el Tribunal Administrativo de Cundinamarca está atento al fallo sobre el río Bogotá y el segundo las obligaciones que emanan de ese fallo”.

### **Un complot del Estado contra la ciudadanía y el ambiente** 

El abogado añade que "hay muchas empresas mineras en Bogotá que tienen títulos en estas zonas y la ampliación de las **7.000 hectáreas benefician a unas pocas empresas y afectan gravemente a las comunidades y el ambiente".**

Por otra parte, Negrete denunció que "**los defensores ambientales estamos en el papel de idiotas útiles sin que se tomen en cuenta las investigaciones sobre impactos** en la sabana de Bogotá, parece que al Ministerio no le importan las recomendaciones o lo que digamos frente a lo que implica ampliar esa zona de explotación minera".

![Sabana de Bogotá](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Captura-de-pantalla-2016-12-22-a-las-15.30.27.png){.size-full .wp-image-34017 .aligncenter width="397" height="594"}

El defensor señaló que “han identificado que se vienen dando licencias para explotación minera donde no es permitido (…) hay muchas leyes y decretos de protección ambiental que no se están cumpliendo, como la **Ley 99 de 1993 la cual declara la Sabana de Bogotá y sus páramos, aguas, valles aledaños, cerros circundantes y sistemas montañosos como de interés ecológico nacional".**

Por último, Rodrigo Negrete manifestó que este tipo de decisiones arbitrarias hacen pensar que **"el Estado hace un complot en contra de la ciudadanía y el ambiente"** e invita a organizaciones ambientalistas y en general a toda la comunidad a fortalecer la movilización social y la defensa del agua.

<iframe id="audio_15321751" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15321751_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
