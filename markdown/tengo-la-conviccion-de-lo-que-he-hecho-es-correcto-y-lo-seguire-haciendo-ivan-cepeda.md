Title: "Tengo la convicción de lo que he hecho es correcto, y lo seguiré haciendo." Iván Cepeda
Date: 2015-02-23 16:58
Author: CtgAdm
Category: Judicial, Nacional
Tags: Alvaro Uribe, Consejo de Estado, Iván Cepeda, Paramilitarismo
Slug: tengo-la-conviccion-de-lo-que-he-hecho-es-correcto-y-lo-seguire-haciendo-ivan-cepeda
Status: published

###### Foto: editorialamanzonico.com 

<iframe src="http://www.ivoox.com/player_ek_4122798_2_1.html?data=lZaflJydfI6ZmKiak5WJd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjK7jh6iXaaKlz5Cwx9XJqMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Senador Iván Cepeda] 

Para el 24 de febrero está citada la audiencia en la investigación que realiza el **Consejo de Estado contra el senador Iván Cepeda** por la supuesta inhabilidad que tenía Cepeda para realizar el debate de **control político por paramilitarismo** en el mes de Septiembre de 2014. Según los demandantes del Centro Democrático, el senador Cepeda no podía realizar el debate puesto que fue demandado por injuria y calumnia por el senador Alvaro Uribe.

Ivan Cepeda afirma que el punto de discusión es si **estaba o no inhabilitado para realizar el debate**, pero reitera que los congresistas tienen el deber de realizar los debates que crean necesarios y además tienen derecho a la libertad de expresión, y ese es el derecho que afirma ejerció durante el debate.

Cepeda resalta que no había ningún impedimento para realizar dicho debate, y recuerda que todas las razones que se expusieron de manera previa fueron discutidas por su equipo y no se presentó ningún impedimento. El senador recuerda que **ha actuado en derecho y en la misma línea espera que se le respeten sus derechos.**

En la diligencia ente el Consejo de Estado se espera que el Senador Cepeda se ponga en conocimiento de las pruebas que habría en su contra y así mismo presente los elementos generales de su defensa. En caso de prosperar esta demanda Cepeda podría verse abocado a la **pérdida de la investidura como Senador de la República**.

El debate, por el cual Cepeda es investigado, se realizó [el **17 de septiembre de 2014** en el congreso de la república](https://www.youtube.com/watch?v=vXUp6W9Yb9o), durante la exposición de Cepeda el senador Uribe no estuvo presente puesto que previamente afirmó que estaría en la Fiscalía radicando una serie de denuncias contra el senador del Polo Democrático.

Casi al finalizar la intervención de Cepeda arribó nuevamente el senador Uribe, quien expuso, entre otras, algunos documentos que supuestamente **probarían la responsabilidad de Cepeda en delitos relacionados con la presión a testigos**. Sin embargo, el senador Uribe no respondió ninguna de las preguntas planteadas por Cepeda y se retiró de nuevo del recinto ante la indignación de los congresistas presentes que esperaban que se pudiera dar un debate serio.

Tras la salida de Uribe, tanto el presidente de la sesión como el mismo ministro del interior Juan Fernando Cristo y la senadora Claudia López, rechazaron de manera vehemente la ausencia del senador del Centro Democrático, sin embargo, realizaron sus intervenciones y respaldaron la necesidad de discutir sobre el paramilitarismo para esclarecer todas las responsabilidades políticas y penales que tiene este fenómeno.

A continuación la intervención de Iván Cepeda en el debate por paramilitarismo

\[embed\]https://www.youtube.com/watch?v=vXUp6W9Yb9o\[/embed\]
