Title: Los derechos de los animales también se defienden en Contagio Radio
Date: 2014-12-11 15:33
Author: CtgAdm
Category: Voces de la Tierra
Slug: los-derechos-de-los-animales-tambien-se-defienden-en-contagio-radio
Status: published

Desde este sábado llega a Contagio Radio la Onda Animalista, que se emitirá a partir de las 10 am todos los sábados.

Onda Animalista es un programa encaminado a la defensa de los derechos de los animales. Natalia Parra, directora del espacio afirma que “es necesario que la gente separa los pasos que se van dando en la defensa de los derechos de los animales en Colombia”.

Los invitamos a todos y todas a que escuchen la nueva propuesta que busca darle voz a esos seres que no tiene la posibilidad de expresar lo que sienten.
