Title: El delito político y el Acuerdo de Paz
Date: 2018-04-06 17:10
Category: Expreso Libertad
Tags: colombia, delito, político
Slug: delito-politico-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/presos-farc.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alexander Esbobar] 

###### [27 Feb 2018] 

La posibilidad de un acuerdo de paz entre el gobierno nacional y la entonces guerrilla de las FARC, fue la primera oportunidad para hablar en Colombia sobre los prisioneros políticos que hay en las diferentes cárceles del país y la violación a los derechos humanos de los que han sido víctimas. Escuche en los micrófonos del Expreso Libertad, cómo este Acuerdo, podría cuestionar la violación a derechos humanos en las cárceles y prácticas como la tortura en contra de prisioneras y prisioneros políticos.

<iframe id="audio_25008937" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25008937_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
