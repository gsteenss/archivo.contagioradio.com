Title: Colombianos en el exterior buscan salvaguardar acuerdo de paz
Date: 2016-10-04 14:01
Category: Nacional, Paz
Tags: Apoyo al proceso de paz, Colombianos en el exterior, plebiscito por la paz
Slug: colombianos-en-el-exterior-buscan-salvaguardar-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/colombianos-e1475607598350.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agenda Internacional de Iniciativas Ciudadanas] 

###### [4 Oct 2016] 

Después de los resultados del plebiscito por la paz, colombianos, algunos de ellos exiliados, que se encuentran en el exterior han decidido **activar herramientas e iniciativas que re tomen el debate de la paz**, motivo por el cual la Agenda Internacional de Iniciativas Ciudadanas, y otras organizaciones políticas como la bancada Die Linke, en Alemania, **se preparan para coordinar estrategias que salvaguarden los acuerdos de paz.**

Para Miryam Ojeda miembro de la Agenda Internacional de Iniciativas Ciudadanas, organización que surge en países como México, Estados Unidos, Reino Unido, Canadá, Argentina, entre otros, para apoyar al plebiscito por la paz, “**las elecciones reflejan un país urbano que no entiende al país rural"** y que hace necesario retomar el trabajo con estrategia más contundentes en el exterior.

El paso a seguir desde la Agenda  será utilizar las plataformas mediáticas, en sus respectivas ciudades, para posicionar el debate, igualmente intentarán apelar a todos los congresistas y parlamentarios que han apoyado la paz en Colombia para lograr una **presión desde el exterior**, mañana saldrá la ruta de actividades que coordinara a todos los países en donde esta red tiene presencia.

De igual forma, organizaciones en Alemania también han comenzado a reunirse para generar iniciativas que salvaguarden el proceso de paz, uno de ellos es la Bancada de Die Linke [que ha expresado durante los años de negociación su apoyo  al proceso y que ya habían planteado una reunión para analizar el estado de Colombia](https://archivo.contagioradio.com/parlamento-aleman-expresa-su-apoyo-al-proceso-de-paz-en-colombia/), de este evento se espera que surjan una serie de iniciativas que de acuerdo con Carlos Heinsfruth, miembro de la bancada, **puedan aportar a la construcción de una paz para la sociedad entera. **

Con 1.372  mesas instaladas, fueron **599.026 personas las habilitadas para votar en el exterior** de las que solo participó el **13,85%  de la población**, y en donde se impuso el **sí con un 54,13%**  frente sobre al no con un 45,86%.  No obstante estas votaciones no ayudaron a que en Colombia la mayoría eligiera aprobar los acuerdos de paz.

<iframe id="audio_13180037" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13180037_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_13180047" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13180047_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
