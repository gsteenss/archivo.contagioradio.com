Title: Comunidades de Cacarica conmemoraron 20 años de la Operación Génesis
Date: 2017-03-21 13:24
Category: Nacional, Reportajes
Tags: 20 años, cacarica, Chocó, Operacion genesis, Soy génesis
Slug: 20-anos-renaciendo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/SOY-GENESIS-290.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo-Contagio Radio 

 

Caminando por calles asfaltadas, terrenos polvosos y trochas selváticas, las comunidades que habitan la cuenca del río Cacarica conmemoraron 20 años de la Operación Génesis, operativo militar y paramilitar ejecutado entre el 24 y el 27 de febrero de 1997 en el Bajo Atrato chocoano. Durante 3 días, hombres, mujeres, niños y ancianos marcharon una vez más, como tantas veces tuvieron que hacerlo para exigir la restauración de sus derechos, en Colombia y diferentes partes del mundo.

**Día 1: Marcha del silencio en Turbo**

![sin olvido](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/sin-olvido.jpg){.alignnone .size-full .wp-image-37827 width="1000" height="667"}

Entre luces, cánticos y espacios para la memoria, decenas de personas marcharon, primero por las calles de Turbo, puerto sobre el pacífico que los vio llegar hace 20 años en embarcaciones que cargadas de hambre y terror, arribaban sin tener claro cuál sería su destino. Con el atardecer cayendo sobre el mar como telón, fueron encendiéndose faroles artesanales que cada uno llevaba en su mano, símbolo de luz y verdad, esa que les ha sido negada por tanto años.

Ante la mirada sorprendida de los habitantes del puerto, quienes se preparaban para disfrutar de las actividades propias de un viernes, los afrodescendientes que vivieron el desplazamiento, caminaron acompañados por campesinos e indígenas de otros procesos sociales en el país. Con letras en sus manos que formaba la palabra Sin Olvido daban cada pasó por las calles del puerto, expresando como lo han hecho durante estas dos décadas la importancia de la memoria, y su reclamo  por el derecho a la verdad, justicia y a la vida digna.

Pausada pero firmemente, caminaron cerca de cien personas rumbo al desvencijado coliseo “Bruno Martínez”, espacio que durante 4 años se convirtió en su hogar. Atravesar la reja es volver en el tiempo, al recuerdo de días de poca comida, baños colectivos y mucho sufrimiento. A pesar del dolor y los malos momentos, con sus propias manos en ese mismo lugar levantaron un monumento de cuatro caras en cemento, en el que recogen a manera de friso la memoria de lo ocurrido.

Para conmemorar dos décadas de lucha y resistencia, el monumento fue adornado con una serie de murales en los que plasmaron el antes, durante y después del desplazamiento. Frente a un pequeño enrejado construido de manera similar a un corral para salvaguardar el espacio de memoria, Jeison Mena, autor de algunas de las pinturas y quien para la fecha contaba apenas con 10 años de edad, da la Bienvenida a las delegaciones invitándolos a regalar algunas reflexiones. Se escucharon poemas, alabaos y canciones que contaban los años de sufrimiento e invocaban la presencia de los que hoy ya no están.

**Día 2: Llegada a Cacarica y marcha de antorchas**

\[caption id="attachment\_37461" align="alignnone" width="1100"\]![En la noche las comunidades caminaron del Limón hasta la Z.H Nueva Esperanza alumbrando el camino con antorchas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Soy-genesis-45.jpg){.size-full .wp-image-37461 width="1100" height="733"} En la noche las comunidades caminaron del Limón hasta la Z.H Nueva Esperanza alumbrando el camino con antorchas\[/caption\]

Al día siguiente, saliendo temprano en la mañana las delegaciones se embarcan rumbo a Cacarica. Cinco embarcaciones rápidas conocidas como pangas, emprenden el viaje por el océano pacífico para luego conectar con el río Atrato. Luego de la parada de rigor para comer pescado con patacón como desayuno, el viaje de aproximadamente 3 horas continúa desde Bocas del Atrato hasta la Tapa, punto hasta el que se puede acceder en bote durante el verano.

Faltando poco menos de la mitad del camino, al pasar el punto conocido como "Puente América", un bote de la Armada nacional detuvo a las embarcaciones, alertando de una situación de combates que se presentaban en la zona. Por unos minutos la zozobra volvió al corazón de quienes no querían volver a repetir la misma historia. Tras una orden superior, se permitió que la caravana continuara su trayecto "bajo su propio riesgo".

La inclemencia del verano que azota a la región desde hace más de dos meses, hace que para llegar a la zona humanitaria “Nueva esperanza en Dios”, lugar escogido para el desarrollo de las actividades conmemorativos, se necesite caminar más de hora y media bajo el incontenible sol. A lomo de mula son transportados los alimentos que van a servir para atender el casi centenar de bocas que visitan la comunidad. Al atravesar el pórtico que da entrada al caserío, ni el cansancio físico que amenazaba con robarse hasta el último aliento fue impedimento para que los brazos de viajantes y sus anfitriones se entrecruzaran con el afecto de quienes compartieron solidariamente sueños y luchas y no se encuentran durante largo tiempo.

Luego de tomar los alimentos e hidratarse adecuadamente, una voz por autoparlante convidó a todos los presentes a unirse a la marcha de antorchas. La noche se iba posicionando poco a poco ocultado la figura de los árboles, las casas de madera y los caminos. Por orden de tallas fueron repartidas camisetas y pañoletas con el emblema y consigna de la conmemoración: Soy Génesis, símbolo de un nuevo nacimiento y reivindicación del nombre bíblico que por años no significo más que dolor y sufrimiento.

Con la indicación de tomar antorchas artesanales cargadas de cascarilla de arroz mezclada con ACPM, los caminantes dispusieron su cuerpo y corazón para desplazarse desde la Zona humanitaria hasta la comunidad del Limón, punto inicial de una serie de estaciones demarcadas por varias palabras representativas del antes, durante y después del desplazamiento: Zona de pesca y transporte, educación propia, fortalecimiento organizativo, libertad, paz, verdad, libertad, justicia, solidaridad y fraternidad.

El recorrido cubrió el territorio hasta llegar al centro de la zona humanitaria, en la cual se dispuso un espacio donde fue depositado el fuego de cada antorcha logrando de manera colectiva una gran hoguera que ilumino la oscura noche chocoana, culminando con una danza colectiva donde los presentes bailaron y festejaron con regocijo el nuevo comienzo, a pesar de los temores y las amenazas de los violentos de volver a sus territorios.

**Día 3: Jornada pedagógica, cultural y deportiva**

\[caption id="attachment\_37467" align="alignnone" width="1100"\]![Con baile](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Soy-genesis-62.jpg){.size-full .wp-image-37467 width="1100" height="733"} Con baile y dando un nuevo significado a al palabra "Génesis" se vivió el 3er día de conmemoración\[/caption\]

El domingo desde la madrugada podían escucharse los primeros movimientos de la comunidad. Como si se tratara de cualquier día de la semana los campesinos iniciaron sus labores de duro trabajo mientras los participantes de la conmemoración, plato y vaso en mano, hacían la respectiva fila para tomar el primer café y comida del día. La primera actividad estaba enfocada a la integración con la presentación de las diferentes delegaciones invitadas, participación que se realizó de manera aleatoria.

Al frente del micrófono Yahaira Salazar, compartió uno de sus poemas con los asistentes para luego entonar, liderados por algunas matriarcas de la comunidad, uno de los cánticos representativos de su sentir "Oyeme Chocó, oye por favor, tu no tienes porque estar sufriendo así". El espacio se presentó como una buena oportunidad para presentar la novela "Contra la corriente" de Alexandra Huck, una historia inspirada en las experiencias de las víctimas de la operación Génesis. La autora leyó algunos pasajes del texto mientras los asistentes escuchaban con atención.

Una vez terminado el relato, fue el momento para reflexionar, pensar y buscar caminos para emprender los nuevo retos que vienen para procesos organizativos como el de CAVIDA y otros de los presentes en el mismo espacio ante el nuevo panorama que se avecina en tiempos de post acuerdos. Compartiendo puntos de vista y propuestas, por grupos presentaron lo que avisoran como posibilidades que se abren para buscar un buen existir y encontrar la bella locura que los ha unido durante tiempos menos esperanzadores.

Con la expectativa que surge al pensar como será el futuro y los pies sobre la tierra muy en el presente y dispuestos para patear el balón, hombres y mujeres conformaron equipos para disfrutar el infaltable partido de fútbol, deporte que disfrutan de manera común la mayor parte de los convocados. Con un circulo en el centro del campo y listos para tomar la partida, se disponían los combinados a escuchar las indicaciones del juego, cuando un sonido de ráfaga retumbó desde los árboles, !Se metieron, se metieron! alguien grito mientras los más pequeños y algunos de los mayores corrieron atemorizados. Quienes estaban al tanto que se trataba de una representación, parte de los actos de memoria, rieron apenas salieron a la luz algunos de los jóvenes de la comunidad, que cargando palos a manera de armas, recrearon la entrada de los hombres que irrumpieron hace 20 años.

El sentido de hacer un acto de ese tipo previo a la actividad deportiva, tenia toda su razón de ser. La puesta en escena recordaba la memoria de Marino López, símbolo encarnado de la infamia cometida contra las comunidades y por el cual la nación, en la personas del General Rito Alejo del Río fue condenada. Ese día su cabeza rodó, ahora, antes de caer el sol de la tarde el balón corrió y tras el una mezcla de identidades, colores y esperanzas, todos en procura de anotar el anhelado gol.

Como un manto oscuro la noche hizo presencia, y con ella el fin de la jornada. En el kiosko comunitario de "Chocó producciones", fueron congregados todos y todas, para contemplar algunas presentaciones artísticas. Se escucharon improvisaciones de Hip hop por parte de los Renacientes y algunas danzas a cargo de los más pequeños de la zona humanitaria. Cuando ya finalizaban los actos, el profesor Henry hizo un llamado a los asistentes para mostrarles una sorpresa. Atravesando el campo de fútbol, una valla en tela señalaba que en ese lugar próximamente se levantará la sede de la Universidad de Paz Marino López, un centro para la educación propia que permita continuar formando jóvenes profesionales en la comunidad. Las manifestaciones de alegría y jubilo no se hicieron esperar, se trata de la materialización de un sueño por el que hay mucho trabajo que realizar.

Cuando el cansancio agotaba las últimas reservas de energía, en un momento especial de remembranza, hombres y mujeres pudieron identificar a los amigos y familiares, incluso a ellos mismos en un video grabado hace 16 años en Bahía Cupica, lugar al que algunos miembros de la comunidad fueron enviados y separados de sus familiares que habian llegado a Turbo. Rostros y figuras más delgadas, la imagen de aquellos que partieron, hicieron parte de ese momento de memoria.

\
