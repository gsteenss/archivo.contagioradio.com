Title: Radican demanda contra Ley ZIDRES
Date: 2016-03-04 15:14
Category: Economía, Nacional
Tags: César Jeréz, Ley de Zidres
Slug: polo-partido-verde-y-cumbre-agraria-radican-demanda-contra-zidres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Demanda-ZIDRES1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alberto Castilla ] 

<iframe src="http://co.ivoox.com/es/player_ek_10677442_2_1.html?data=kpWjmZyYeJOhhpywj5WbaZS1lpeah5yncZOhhpywj5WRaZi3jpWah5yncbHjzdSSlKiPlMLm1c7R0ZC6qdPYxpDmjajZscPmxpCuydfFtsrVjNfOxs7Hpc%2Bfxcraw9PIpYzX0JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Cesar Jeréz, ANZORC] 

###### [4 Mar 2016] 

Este viernes Congresistas del Polo Democrático y el Partido Verde, junto a organizaciones de la Cumbre Agraria **radican demanda por vicios de fondo y forma contra varias disposiciones de la Ley ZIDRES**. De acuerdo con César Jeréz, vocero de la ANZORC, el principal argumento para demandar es la ilegalidad de este proyecto que desconoce la Ley de Baldíos y viola el régimen de las unidades agrícolas familiares, "una burla al país, a la sociedad colombiana y a los millones de campesinos con poca tierra que están a la espera que se solucione esta problemática en el país", afirma.

Durante los dos últimos gobiernos se ha buscado implementar reformas legales sobre los baldíos, y según afirma Jeréz la Ley ZIDRES sería el sexto intento, los demás se han sido retirados al percatarse de la ilegalidad que se estaba cometiendo. “Lo que está en el fondo es una lucha por la tierra” asegura el líder, pues **lo que se busca es beneficiar a “las familias colombianas terratenientes con enorme poder político y económico”** que aumentan su capital a través de la especulación y venta de tierras a multinacionales.

Las organizaciones y Congresistas demandantes esperan que la Ley ZIDRES caiga por vía constitucional “**es insostenible normativamente, es regresiva y va contra los acuerdos de La Habana**: solucionar problema de acceso a la tierra, formalizar la propiedad de fincas campesinas y llevar proyectos de inversión a los campos” asevera el vocero, su ejecución impediría la existencia de las Zonas de Reserva Campesina, agrega e insiste en que tras esta ley se esconde **“un despojo de tierras sin precedentes en el país”**. Sólo en Vichada podrían expropiarse 10 millones de hectáreas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
