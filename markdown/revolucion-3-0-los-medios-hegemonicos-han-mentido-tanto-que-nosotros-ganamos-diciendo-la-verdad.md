Title: Revolución 3.0: “Los medios hegemónicos han mentido tanto, que nosotros ganamos diciendo la verdad”
Date: 2014-12-11 15:28
Author: CtgAdm
Category: El mundo
Slug: revolucion-3-0-los-medios-hegemonicos-han-mentido-tanto-que-nosotros-ganamos-diciendo-la-verdad
Status: published

A través de las redes sociales, generando contenidos prácticos, concretos, sencillos, videos cortos, canciones, fotografías, declaraciones en audio y transcritas, jóvenes que promedian los 24 han generado el espacio comunicativo “Revolución 3.0”, para democratizar la información y fomentar la libre expresión en un país que, como México, ha visto controlados sus medios por los conglomerados económicos y la hegemonía política.
