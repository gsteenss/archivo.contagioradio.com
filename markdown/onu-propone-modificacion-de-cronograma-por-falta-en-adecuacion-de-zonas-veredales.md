Title: ONU propone "recalendarizar" dejación de armas de FARC-EP
Date: 2017-02-18 08:40
Category: Nacional, Paz
Tags: FARC, MMV, ONU, Zonas Veredales
Slug: onu-propone-modificacion-de-cronograma-por-falta-en-adecuacion-de-zonas-veredales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Arnault-ONU.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto] 

###### [18 Feb 2017]

En una carta dirigida a los integrantes de la comisión de impulso y seguimiento al acuerdo de paz, Jean Arnault, jefe de la Misión de la ONU, señaló que la falta de adecuación de las zonas veredales en las que se alojan las FARC traería consecuencias graves como incidentes en torno al cese al fuego, el cronograma de dejación de armas, incluso plantearon que se debe revisar la amnistía para que el MMV funcione adecuadamente.

Luego de la finalización del traslado de los integrantes de las FARC a las Zonas Veredales Transitorias y Zonas Campamentarias, el jefe de la misión de la ONU, Jean Arnault, señaló que existen 3 consideraciones, una en torno a la adecuación de las Zonas Veredales que incluye su delimitación, las preocupaciones de los integrantes de las FARC y el siguiente mandato de esa misión. ([Lea también Inicia camapaa de donaciones a guerrilleros en zonas veredales](https://archivo.contagioradio.com/inicia-campana-de-donaciones-para-guerrilleros-de-las-zonas-veredales/))

### **La adecuación de las Zonas Veredales y el cronograma de dejación de armas** 

En este punto Arnault señala que se debe aclarar la delimitación de las Zonas Veredales para poder vigilar si hay salidas o no de los límites establecidos para las FARC y evitar incidentes del Cese al fuego, además recordó que hay ambigüedad en el punto en que los integrantes de la guerrilla tendrían “permisos especiales” dado que tampoco está concluido el tema de la amnistía.

“Queremos asegurarnos de que no existen ambigüedades sobre el estatus jurídico de los miembros de las FARC-EP, en particular sobre el ámbito de aplicación de los levantamientos de las órdenes de captura” y por ello sería necesario “aclarar el régimen de salidas” señala la carta. ([Le puede interesar: Los incumplimientos del gobierno en Zonas Veredales](https://archivo.contagioradio.com/continuan-incumplimientos-del-gobierno-en-zonas-veredales/))

Por otro lado la ONU señala que en cuanto a la dejación de armas y según la información que tienen, solamente hasta finales de Marzo estarían listas las Zonas Veredales, incluyendo los sitios donde se monitoreará el almacenamiento de las armas de las FARC “¿Estarían de acuerdo las partes con la recalendarizar el inicio de la recepción escalonada por la misión del armamento hasta esa fecha?”

### **Las preocupaciones de las FARC que también preocupan a la ONU** 

Según la carta, que también menciona a integrantes de la misión en terreno las preocupaciones de las FARC van en tres sentidos: la seguridad jurídica de cara a la amnistía, seguridad socioeconómica y la seguridad física una vez dejadas las armas. ([Lea también: Zonas veredales bajo la sombra paramilitar](https://archivo.contagioradio.com/zonas-veredales-bajo-la-sombra-paramilitar/))

“nos parece indispensable que la marcha del proceso de paz hacia la dejación de armas deba acompañarse de avances tangibles en los ámbitos jurídicos, socioeconómicos y de seguridad que alivien las preocupaciones de los miembros de las FARC –EP” y pidieron a la Comisión un informe sobre los avances específicos en esos temas.

### **El mandato actual de la misión actual termina en Mayo** 

Por último, para Arnault sería necesario iniciar rápidamente con las gestiones para que el mandato de la ONU que inicia en Mayo, al terminar la dejación de armas, dado que se requieren tiempos prudenciales para la adopción de medidas y el establecimiento de los requerimientos que conciernen a esa situación.

[MisiónONU-CartaCSIVI-170217](https://www.scribd.com/document/339663929/Misio-nONU-CartaCSIVI-170217#from_embed "View MisiónONU-CartaCSIVI-170217 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_58375" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/339663929/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-lD3uJIOi6LIqejqqUrQM&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
