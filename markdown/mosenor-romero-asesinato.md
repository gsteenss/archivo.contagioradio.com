Title: 37 años después piden investigar asesinato de Monseñor Romero
Date: 2017-03-24 12:05
Category: Otra Mirada
Tags: 37 años, crimen, Investigación, Monseñor Romero
Slug: mosenor-romero-asesinato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/foto010.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:El Salvador.com 

###### 24 Mar 2017 

Defensores de Derechos Humanos y organizaciones sociales en el Salvador, solicitaron a un tribunal de justicia de ese país **investigar y procesar a los autores materiales e intelectuales del asesinato de monseñor Oscar Arnulfo Romero**, ocurrido el 24 de marzo de 1980.

La petición, presentada por los abogados de la oficina de Tutela legal María Julia Hernández y de la Fundación de Estudios para la Aplicación de Derechos, **añadieron a su petición las investigaciones adelantadas por la Comisión de la Verdad de las Naciones Unidas y las sentencias de la Corte Interamericana de Derechos Humanos frente al caso**.

**El Estado Salvadoreño había sido condenado por la el tribunal internacional por su responsabilidad  en el crimen**, sentencia que incluyó la orden de re abrir la causa judicial y aplicar acciones de reparación. En marzo de 2010, cuando se cumplieron 30 años del homicidio, el entonces presidente **Mauricio Funes reconoció la responsabilidad estatal y pidió perdón por el magnicidio** que atribuyó en su momento a "la violencia ilegal que perpetró un escuadrón de la muerte".

Sin embargo por una amnistía declarada en 1993 por el gobierno derechista de la Alianza Republicana Nacionalista, **los autores materiales del crimen no fueron procesados judicialmente**. Ley que años más tarde fue derogada por la Corte Suprema del país, una decisión que  deja sin impedimentos y excusas la continuidad de la investigación sobre delitos de lesa humanidad y crímenes de guerra ocurridos por cuenta de la represión militar durante los años 70 y parte de los 80.

**[En mayo de 2015 monseñor Romero fue declarado beato](https://archivo.contagioradio.com/beato-monsenor-romero-dignifica-los-martires-en-america-latina/)**. Los obispos de el Salvador se encuentran actualmente en Roma para pedir al papa Francisco que este año pueda darse **su canonización**, una solicitud que el sumo pontífice ha visto con buenos ojos y que **podría hacerse realidad el 15 de agosto cuando se cumplirá un siglo del nacimiento del religioso** recordado por ser "La voz de los sin voz".

**Actividades de conmemoración.**

Varias actividades están programadas para recordar la memoria de Monseñor Oscar Romero en el Salvador a 37 años de su homicidio. El Comité Nacional Monseñor Romero ha dispuesto que la fecha comience con un acto litúrgico desde las 7:30 de la mañana, el cual se llevará a cabo en la capilla del Hospital La Divina Providencia, lugar donde fue asesinado por el disparo de un francotirador el ahora beato.

Al finalizar el servicio religioso, se realizará una movilización que partirá desde el llamado "Hospitalito" hasta la Catedral Metropolitana, en un recorrido que abarcará tres estaciones: en el monumento a monseñor Romero, en el parque Cuscatlán y en las gradas de la catedral capitalina. Las actividades en el templo incluyen actos culturales con música y cantos en honor al mártir salvadoreño.

Por su parte, el papa Franciso dedico una homilía a la memoria de Monseñor Romero en la capilla de la Casa de Santa Marta. Allí el sumo pontífice hizo hincapié en la importancia de ser conscientes de la maravilla que Dios realiza en nosotros y con misericordia, para que ejercerla con los demás poniendo atención ante la hipocresía de “robar un falso perdón” en el confesionario.
