Title: Las reformas de Rafael Correa para redistribuir la riqueza
Date: 2015-06-16 13:55
Category: El mundo, Otra Mirada
Tags: domingo negro, ecuador, impuesto, ley de herencia, plusvalía, Rafael Correa, revolución ciudadana, riqueza
Slug: las-reformas-de-rafael-correa-para-distribuir-el-46-del-pib
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Ecuador-Revolucion.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Telesur 

<iframe src="http://www.ivoox.com/player_ek_4648719_2_1.html?data=lZuhmpyVfY6ZmKiak5qJd6Kol5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMLnjNfSyNTWscLnjMnSjbfFqsLZzZCw0dfWqcKf0cbfw5DIrdTo087P187Wb8bgjJmjh5eZb8XZjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align: center;">

[Juan Paz y Miño, Historiador]

</div>

<div style="text-align: center;">

</div>

###### [16 jun 2015] 

Desde el año 2007 en que Alianza País llegó a la presidencia con la candidatura de Rafael Correa, Ecuador ha intentado abandonar el esquema de economía neoliberal a favor de un nuevo tipo de modelo económico que trate de redistribuir la riqueza. Sin embargo, para el historiador Juan Paz y Miño, "pese a los cambios que se han hecho, la concentración de la riqueza en el Ecuador es todavía abismal".

En un intento por redistribuir el 46% del PIB que, según un estudio de la Universidad Católica de Quito, aún se encuentra en manos del 0.2% de los contribuyentes, el gobierno de Correa promueve la Ley de Herencia y Plusvalía, un impuesto encaminado a cobrar hasta un 47% de las transacciones significación económica relacionadas con venta de terrenos, edificios y casas. Según el presidente Correa, cerca del 98% de los ecuatorianos no tendrían que pagar este impuesto, que sólo afectaría a 115 consorcios empresariales, cuya composición es normalmente familiar, y que controla cerca del 80% de la economía del país.

El Proyecto de Ley ha provocado polarización, y movilizaciones cada vez más violentas a lo largo de las últimas semanas por parte de capas altas y medias de la población: la primera que se verá evidentemente afectada por la reforma, y la segunda, según Paz y Miño, ante la desinformación que han generado los medios, que esgrimen argumentos como que el gobierno quiere acabar con la propiedad privada, y quitar a las familias la posibilidad de heredar. "Esa es una manipulación grave que se ha hecho en el país, y que ha logrado que se movilicen las capas medias que defienden su patrimonio, su familia y su heredad", afirma el profesor universitario.

Para el historiador, "tratar de democratizar esta situación no es fácil en el Ecuador ni en ninguna parte de América Latina, puesto que la tesis de redistribuir la riqueza -como afecta a los sectores mas pudientes- siempre ha ocasionado en distintos momentos de la historia feroces resistencias de estos sectores, que creen por lo demás que su riqueza viene dada por su trabajo personal, cuando en sociología, economía e historia sabemos que la concentración de la riqueza se da por mecanismo sociales, entre los que se incluyen la explotación de la fuerza de trabajo". En el caso del Ecuador, la valorización de los inmuebles sobre los cuales se cobraría el impuesto, se habría dado como consecuencia de obras públicas aledañas, como valor socialmente creado, y no por el trabajo personal de las personas y familias de capas altas del país. Según el estudio de la Universidad Católica, además, la acumulación de capital de estos sectores se habría dado por una histórica evasión tributaria.

La polarización a mantenido movilizada a la ciudadanía ecuatoriana: Mientras la oposición convocó a una caravana de automóviles en lo que se denominó el "domingo negro"; los sectores afines a la reforma salieron a las calles el lunes 15 de junio bajo la consigna \#YoDefiendoMiRevolución.

El presidente Rafael Correa retiró de manera momentánea el Proyecto de Ley, según declaró, para que se realice un gran debate nacional en torno a las reformas propuestas. Para Juan Paz y Miño, esta posición devela un triple reto del gobierno hacia la oposición: por una parte, desmoviliza a los sectores que estaban generando hechos violentos durante las manifestaciones; reta a la oposición a debatir con argumentos reales su rechazo a la reforma; y desmonta la posibilidad de una revocatoria vía movilización, como ocurriera en el Ecuador en la década de los 90. Según afirma Correa, existen mecanismos de revocatoria a través de la consulta popular, y si la oposición busca ese fin puede hacerlo por vías democráticas.

En cualquier caso, se viven circunstancias que exigen a la ciudadanía ecuatoriana tomar su papel en el esclarecimiento y debate de las propuestas que se encuentran sobre la mesa.

 
