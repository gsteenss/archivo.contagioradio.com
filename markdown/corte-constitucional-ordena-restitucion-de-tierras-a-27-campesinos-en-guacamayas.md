Title: Corte Constitucional ordena restitución de tierras a 27 familias en Guacamayas
Date: 2017-11-09 13:14
Category: DDHH, Nacional
Tags: Corte Constitucional, reclamantes de tierras, Restitución de tierras, víctimas, víctimas del conflicto armado
Slug: corte-constitucional-ordena-restitucion-de-tierras-a-27-campesinos-en-guacamayas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Restitución-de-Tierras-e1510251256744.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Informador] 

###### [09 Nov 2017] 

La Corte Constitucional le dio la razón a 27 campesinos reclamantes de tierra de la vereda Guacamayas, corregimiento de Belén de Bajirá, Antioquia a quienes **se les había negado la restitución de sus predios.** Con esto, falla en contra de la decisión de la Corte Suprema de Justicia y el Tribunal de Justicia y Paz de Medellín por lo que se reconoce que hubo violencia generalizada en la zona.

De acuerdo con la agencia de prensa del Instituto de Popular de Capacitación, la decisión proferida en la sentencia **facilita  las acciones para que las víctimas** del conflicto armado obtengan una reparación integral.

### **Campesinos debieron vender sus tierras por incursión paramilitar** 

En el [comunicado no. 53](http://www.corteconstitucional.gov.co/comunicados/No.%2053%20comunicado%2018%20y%2019%20de%20octubre%20de%202017.pdf.pdf)de la Corte Constitucional, estas personas argumentaron que **“se vieron forzados a vender sus predios** en el departamento de Antioquia, como consecuencia de la incursión paramilitar del bloque comandado por Arlex Hurtado de las Autodefensas Campesinas de Córdoba y Urabá (ACCU), lideradas por Raúl Emilio Hasbún Mendoza.”

Además, los inmuebles “se encuentran bajo la titularidad de la sociedad La Guacamayas Ltda, que se **aprovechó de las condiciones de violencia** “y de la incursión paramilitar en la región, pagando precios irrisorios, acumulando propiedades que habían sido adjudicadas por el INCORA, e incumpliendo con su lugar de verificar la existencia de condiciones aptas de seguridad para adquirir estas tierras”. (Le puede interesar:["Cinco propuestas para evitar el fracaso total de la ley de restitución de tierras"](https://archivo.contagioradio.com/42667/))

Con esto en mente, la Corte manifestó que las pretensiones de restitución **fueron negadas por la Sala de Justicia y Paz del Tribunal Superior de Medellín** y por la Corte Suprema de Justicia “quienes sostuvieron que no se demostró el nexo causal entre la venta de los predios y las supuestas intimidaciones ejercidas por los miembros del grupo armado con presencia en la zona”.

Sin embargo, argumentó que no se tuvo en cuenta durante el proceso **la protección de los derechos de las víctimas** en calidad de reclamantes de tierra por lo que la Corte Suprema “prefirió seguir el parámetro, menos protector de los derechos constitucionales de las víctimas, que se encontraba establecido en normas previas, de carácter reglamentario y que, en la actualidad, se encuentran derogadas”.

Por lo tanto, a las 27 familias **se les deberá garantizar la restitución de sus predios** y para organizaciones como Forjando Futuros e IPC, “el fallo de la Corte Constitucional se convierte en un precedente de aplicación de la ley más favorable a las víctimas que buscan la restitución de sus tierras”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
