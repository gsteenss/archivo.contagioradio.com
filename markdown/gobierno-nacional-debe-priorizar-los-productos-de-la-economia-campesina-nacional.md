Title: Gobierno debe priorizar  productos campesinos antes que importar alimentos e insumos
Date: 2020-04-17 18:10
Author: AdminContagio
Category: yoreporto
Tags: campesinas, campesinos, economía campesina, Gobierno Nacional
Slug: gobierno-nacional-debe-priorizar-los-productos-de-la-economia-campesina-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/buhola-colombia-realizara-mayor-proceso-de-formalizacion-rural-de-su-historia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":4} -->

#### 

<!-- /wp:heading -->

<!-- wp:paragraph -->

*En medio de la reunión virtual realizada el día de hoy en la **Comisión VII de senado**, donde participó el Ministro de Agricultura y el presidente de **Fedepalma**, el senador **Alberto Castilla** recalcó la importancia de priorizar y respaldar la producción de alimentos que asegura el campesinado como única forma de superar la crisis y llamó a llevar pruebas de testeo rápido de Covid-19 a las zonas rurales.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego de iniciadas las sesiones plenarias del Senado de la República de manera virtual el pasado lunes 14 de Abril, las comisiones constitucionales dieron inicio a sus deliberaciones. Hoy en la comisión VII se realizó una reunión con el presidente de Fedepalma, Jets Mesa, y el Ministro de Agricultura, Rodolfo Enrique Zea Navarro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante la reunión virtual, el  
Senador Alberto Castilla denunció el desplazamiento de 25 familias en el  
Departamento del Cauca por parte de actores armados y lamentó que se sigan  
presentando estos hechos que atentan contra la vida de los líderes sociales en  
medio de una pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Castilla indicó que las comunidades campesinas en las zonas rurales, donde actualmente se encuentra, están acatando las medidas de aislamiento y manteniendo la producción agrícola. Por esta razón insistió que “la única forma de superar esta crisis en términos alimenticios es reconocer al campesinado políticamente y brindarle las ayudas necesarias para que su producción se priorice y se suspenda la importación de alimentos”

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Actualmente en el país se está importando 14 millones de alimentos que se podrían producir en el país.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Adicionalmente llamó la atención sobre el **decreto 523** del 15 de abril “Por el cual se modifica parcialmente el Arancel de Aduanas en relación con la importación de materias primas como el maíz amarillo duro, el sorgo, la soya y la torta de soya", expedido en el marco de la emergencia sanitaria producida por el Coronavirus, el cual reduce a 0 el arancel sobre ciertos productos como el maíz amarillo, lo que podría conllevar a poner en riesgo la producción nacional pues “el país produce cerca de 1 millón de toneladas, suficiente para abastecer al país”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente el senador llamó a que el Ministerio de Agricultura implemente cuanto antes la realización de testeo rápido en las zonas rurales, pues el abandono histórico y el déficit para la atención en salud en estas zonas puede poner en riesgo a la población campesina ante posibles contagios de Coronavirus.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También llamó al Gobierno Nacional a que incluya la economía campesina dentro de las medidas que se están tomando en medio de la emergencia sanitaria y que el estado priorice la compra de alimentos a este sector de la población.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **Video de la  
intervención completa del senador:**

<!-- /wp:heading -->

<!-- wp:embed {"url":"https://www.youtube.com/embed/CNHK_wSINUg"} -->

<figure class="wp-block-embed">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/embed/CNHK\_wSINUg

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

[Mas de Yo Reporto](https://archivo.contagioradio.com/?s=yo+reporto)

<!-- /wp:paragraph -->
