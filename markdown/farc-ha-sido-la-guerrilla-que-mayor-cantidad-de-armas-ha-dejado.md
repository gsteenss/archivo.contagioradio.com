Title: Balance a un año de la paz: FARC, la Guerrilla que mayor cantidad de armas ha dejado
Date: 2017-11-23 16:32
Category: Nacional, Paz
Tags: acuerdos de paz, Dejación de armas, FARC, Paz y reconciliación
Slug: farc-ha-sido-la-guerrilla-que-mayor-cantidad-de-armas-ha-dejado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/dejacion-de-armas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: NC Noticias] 

###### [23 Nov 2017] 

La Fundación Paz y Reconciliación, la Corporación Vivamos Humanos, la Red ProdePaz y la Iniciativa Unión por la Paz, presentaron el tercer informe “Como va la Paz, terminó la guerra, el posconflicto está en riesgo. A un año de paz”. Allí, alertaron que la implementación de lo acordado en la Habana, **no va por buen camino** y hace falta poner en marcha estrategias que ayuden a reducir la violencia focalizada que, como fenómeno, está incrementando a pesar de haberse firmado la paz.

El informe está compuesto **por 5 capítulos** en donde se habla del balance de la dejación de armas, el proceso de la reincorporación, el cumplimiento del acuerdo sobre las víctimas, la legislatura de la paz y el enfoque de género y el medio ambiente.

### **La dejación de las armas de las FARC se ha cumplido a cabalidad**

De acuerdo con el primer capítulo del informe, el proceso de dejación de las armas por parte de las FARC **no tiene precedentes**. Indica que “la desmovilización paramilitar arrojó un promedio de 0.6 armas por hombre” lo que quiere decir que casi la mitad de los paramilitares no entregaron las armas. Para ilustrar esto, indican que de los 868 desmovilizados del bloque Cacique Nutibara, solo 497 entregaron armas.

En comparación con el nivel internacional, **“no existe nada aproximado a lo que sucedió con el proceso de paz entre el Gobierno de Colombiano y las FARC”**. Ponen el ejemplo de Afganistán donde en el proceso de proceso de paz se desmovilizaron 63.000 personas y se entregaron 47.575 armas que corresponden a un 0.76 armas por desmovilizado.

Para el caso de las FARC la relación fue de **1.3 armas por excombatiente** donde los 6.800 desmovilizados entregaron 7132 armas en las 26 zonas veredales. Además, las FARC entregaron las coordenadas de las 1027 caletas en donde las Fuerzas Militares deberán extraer 277. (Le puede interesar: ["Que la finalización de la dejación de armas, no sea el fin de los Acuerdos de Paz"](https://archivo.contagioradio.com/finalizacion-de-la-dejacion-de-arma/))

Con este proceso de dejación, la violencia en los más de 200 municipios en los que tenía presencia las FARC **ha disminuido sustancialmente**. “Los homicidios han reducido drásticamente, los secuestros están cerca de desaparecer, los heridos producto de las minas antipersona llegan casi a cero y los desplazamientos disminuyeron”.

### **En muchos territorios no hay control del Estado y por ello proliferan nuevos actores** 

Sin embargo, las organizaciones hacen la precisión en que en algunos territorios, el **incremento de la violencia se debe a la presencia de nuevos actores** armados que aprovechan que tampoco hay presencia del Estado para hacerse con el control del territorio.

A pesar de esto, “la posibilidad de una paz completa **se ve amenazada por la desidia política,** la insuficiente capacidad institucional para implementar lo pactado y los intereses económicos de grupos criminales”. Además, el Estado no ha tenido la capacidad de proteger la vida de los excombatientes de las FARC y de sus familias en la medida que, “hasta el momento han sido asesinados 18 personas en proceso de reincorporación y 11 familiares”.

### **Fortalecimiento de la disidencia está directamente relacionado con  fracaso en la implementación** 

Estas problemáticas, han causado que **cerca del 10% de los excombatientes deserten** y que los que están en proceso de reincorporación, vivan en constante incertidumbre. Para Paz y Reconciliación, “todo indica que el fortalecimiento de la disidencia está directamente relacionado con el evidente fracaso de lo que hasta ahora se ha implementado”. (Le puede interesar: ["Con la dejación de armas se está cerrando la página de la guerra en Colombia"](https://archivo.contagioradio.com/dejacion-armas-farc/))

Si bien el fenómeno de la disidencia ha tendido a aumentar, la Fundación explica que esto **“no se trata de un plan B de las FARC”** y no hay una relación entre los integrantes de las FARC que están en proceso de transición y las disidencias. Esto se contrasta con las afirmaciones de los integrantes del partido político FARC que, en repetidas ocasiones han manifestado que “las llamadas disidencias se alejaron del proceso de paz, de los intereses de la agrupación y que sus intereses son meramente económicos”.

###### Reciba toda la información de Contagio Radio en [[su correo]
