Title: ¿Por qué revocaron al Alcalde de Tasco, Boyacá?
Date: 2018-07-30 11:57
Category: Nacional, Política
Tags: Boyacá, Nelson García, Pedro Castañeda, Tasco
Slug: por-que-revocaron-al-alcalde-de-tasco-en-boyaca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/tascoboyaca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mapio] 

###### [30 Jul 2018] 

“Final Feliz” así calificó Pedro Castañeda, impulsor de la revocatoria al alcalde de Tasco, Boyaca, al desenlace de este proceso democrático en ese municipio, luego de que el 90% de la comunidad decidiera sacar a Nelson García de este cargo. Según Castañeda, fueron los incumplimientos al plan de gobierno y la falta de gestión las que culminaron en este hecho, histórico en el país.

De acuerdo con Castañeda, el alcalde, integrante de la Alianza Verde, no atendió los llamados que hicieron, en varias ocasiones los campesinos, frente a que pusiera en marcha su plan de gobierno, razón por la que fue elegido. Además, habría manifestado que **él no estaba llamado a cumplir el plan de gobierno, y que este solo era una formalidad para ser elegido**.

"El no atendió a los campesinos, dijo que no había dinero y nunca tuvo la gentileza de recibir a la gente del campo en sus despacho, sino que tuvo una forma muy arrogante y prepotente; y los campesinos, nosotros que lo elegimos con el voto, no era para que tuvieron ese desprecio" afirmó Castañeda.

El total de votos fueron** 1.609 votos de los cuales el 97% manifestó estar a favor de la revocatoria mientras que solo el 3% se opuso**. En los próximos 30 días, las autoridades tendrán que convocar a unas nuevas elecciones para la Alcaldía, de una terna escogida por el partido Alianza Verde. Sin embargo, este periodo electoral solo contaría con 5 meses más.

Para Castañeda, se espera que, en ese lapso de tiempo, el alcalde entrante logre poner en marcha algunas de las propuestas del plan de gobierno y así dejarlas listas para que continúen en la siguiente alcaldía de 2019. (Le pude interesar:[ "Tricunal de Boyacá ordena suspender explotación minera en Tasco"](https://archivo.contagioradio.com/tribunal-de-boyaca-ordena-suspender-explotacion-minera-en-tasco/))

Nelson García aseguró que ha cumplido con el 70% de gestión durante su candidatura y aseguró que espera que la comunidad no se arrepienta más adelante de esta decisión. Asimismo manifestó que es necesaria una revisión de la ley que permite las revocatorias porque podría generar "ingobernabilidad".

<iframe id="audio_27331750" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27331750_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
