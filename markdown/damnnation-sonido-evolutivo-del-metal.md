Title: "DamnNation" sonido evolutivo del Metal
Date: 2015-09-17 11:48
Category: Sonidos Urbanos
Tags: Damnation banda bogotana, Death- Groove, Metal bogotano, Metal evolutivo
Slug: damnnation-sonido-evolutivo-del-metal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/IMG-20150917-WA0001-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [<iframe src="http://www.ivoox.com/player_ek_8436934_2_1.html?data=mZmgmJ6XeI6ZmKiakpaJd6Kkl4qgo5mZcYarpJKfj4qbh46kjoqkpZKUcYarpJKyzpDXs8%2FdxdSYpsrFuMmhjKzf0dTaqYzYxpCSlJeopc7ir8bhy9TSaZOmjJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>]

###### [10 Sep 2015]

"**DamnNation**", es una banda Death-Groove de Bogotá, surge a partir de la fusión de dos proyectos para dar al metal tradicional un sonido evolutivo. En actividad desde octubre de 2014, la banda ha ganado reconocimiento gracias a los diversos toques y Festivales en los que ha participado, recibiendo gran acogida por parte del público.

\[caption id="attachment\_14158" align="aligncenter" width="1600"\][![Damnation](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/IMG-20150903-WA0005.jpg){.wp-image-14158 .size-full width="1600" height="960"}](https://archivo.contagioradio.com/damnnation-sonido-evolutivo-del-metal/img-20150903-wa0005/) Damnation en "A la Calle"\[/caption\]

"**DamnNation**" esta conformada por Gustavo Rivera en la batería, Andrés Morales en la guitarra lider, Javier Piñeros en la guitarra ritmica, Andrés Hernández bajo y voz, y Wilber Rodríguez en la voz principal, quienes están próximos a publicar su Ep: "Slow Damn.1a"
