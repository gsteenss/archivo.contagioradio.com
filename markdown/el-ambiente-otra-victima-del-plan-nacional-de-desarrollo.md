Title: El ambiente, otra víctima del Plan Nacional de Desarrollo
Date: 2019-05-07 16:38
Author: CtgAdm
Category: Ambiente, Política
Tags: Alianza Colombia Libre, deforestación, fracking, Plan Nacional de Desarrollo, Puerto de Tribugá
Slug: el-ambiente-otra-victima-del-plan-nacional-de-desarrollo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-62.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ministerio de Ambiente] 

En medio de polémicos debates, el Congreso aprobó el pasado 2 de mayo al Plan Nacional de Desarrollo (PND), una hoja de ruta que el Presidente Iván Duque usará los siguientes tres años para guiar su gobierno. Esto a pesar de las fuertes críticas que ha recibido por profundizar en políticas extractivistas y otras que ocasionarán fuertes impactos ambientales.

Entre los 337 artículos aprobados, algunos de los que más preocupación generan son la construcción de un puerto en Chocó, el avance de proyectos piloto de fracking, el aumento en la meta de deforestación en el país a más de 1.000.000 de hectáreas en tres años y la disminución en las regalías  que aportan empresas mineras.

### Puerto de Tribugá 

El **artículo 78** en el PND da prioridad al desarrollo de infraestructura portuaria y de transporte que conecte los accesos marítimos al resto del país, hecho que impulsaría la construcción del **Puerto de Tribugá**, en el municipio de Nuquí, en Chocó, así como **la conexión vial Las Ánimas - Nuquí**, dos megaproyectos que habían quedado dilatadas desde los años 90.

Como lo denunció [Luis Alberto Angulo, coordinador de comunicaciones del Consejo Comunitario Los Riscales, la construcción de un puerto, que sería más grande incluso que el Puerto de Buenaventura, amenaza ecosistemas enteros, incluyendo flora y fauna endémica de la zona. Además perjudicaría a las comunidades quienes dependen de la pesca artesanal y el ecoturismo para su sustento. ]

Cabe resaltar que la realización de este proyecto pasaría por encima de la voluntad de las comunidades, quienes a través de una consulta previa establecieron esta zona como un **Distrito Regional de Manejo Integral** para la conservación de la diversidad biológica de esta área. (Le puede interesar: "[El artículo del PND que provocaría la destrucción de 917 hectáreas de manglares de Tribugá](https://archivo.contagioradio.com/el-articulo-del-pnd-que-provocaria-la-destruccion-de-917-hectareas-de-manglares-en-tribuga/?fbclid=IwAR1za6k5AE8WJ7Wz1rNTI5yR13UcOTDpaFhx55zr7GVk5PNKvgElnpMZNGA)")

### Un sí al fracking

Durante su campaña presidencial, el primer mandatario prometió que no implementaría el uso de la técnica de fracturación hidráulica conocida como fracking, dadas las consecuencias nocivas que podría tener para el agua y el ambiente. Sin embargo, en el PND quedó plasmado **dos apartados que hablan sobre la viabilidad del fracking y la hoja de ruta que se debería seguir para su implementación**.

Según Carlos Andrés Santiago, integrante de la Alianza Colombia Libre del Fracking, más de 40 congresistas presentaron una contrapropuesta para que se eliminaran estos apartados de las bases del plan, no obstante, representantes del Partido de la U, el Cambio Radical, el Partido Conservador y el Centro Democrático activaron su maquinaría para hundir la iniciativa en la Cámara.

Asimismo Santiago señala que la inclusión del fracking en el Plan Nacional de Desarrollo es otro respaldo del Gobierno a las empresas petroleras, como Ecopetrol, ConocoPhillips, ExxonMobile, Parex, Canacol y Drummond, que insisten en desarrollar proyectos usando esta técnica de extracción no convencional. Esto a pesar de las advertencias de organizaciones sociales, el Comité de Expertos y la Contraloría sobre la falta de la capacidad del Gobierno para hacerle seguimiento y control a este tipo de proyectos. (Le puede interesar: "[Comunidades rechazan el avance de proyectos piloto de fracking](https://archivo.contagioradio.com/comunidades-rechazan-avance-proyectos-piloto-fracking/)")

A pesar de ello, Santiago resalta que no se podrá proceder con los proyectos piloto hasta que el Consejo de Estado levante su moratoria, que suspende provisionalmente el uso del fracking. Situación que solo se puede dar en el momento en que el Alto Tribunal decide sobre la demanda de nulidad que interpuso el grupo de litigio de interés pública de la Universidad del Norte, trámite que podrá demorar meses o incluso años.

### ¿Colombia sin bosques?

Tras conocer las bases del Plan Nacional de Desarrollo, que permitiría aproximadamente la  meta de deforestación de 220.000 hectáreas  anualmente, es decir, 880.000 hectáreas durante el mandato del Presidente Iván Duque, ambientalistas prendieron las alarmas sobre este hecho.

Razón por la cual organizaciones como Dejusticia entregaron una petición de más de 90.000 firmas al Congreso para recordarle al Primer Mandatario su compromiso, bajo acuerdos internacionales, de reducir la tasa de deforestación a cero para el año 2020.

En cambio algunos congresistas modificaron el PND de tal manera que el Gobierno ahora permitiría **la tala de 252.000** **hectáreas de bosque al año, es decir, 1.100.000 hectáreas de bosque** durante la presidencia de Duque. Esta decisión solo reducirá la deforestación en Colombia en un 30% basado en las proyecciones del Instituto de Hidrología, Meteorología y Estudios Ambientales (Ideam).

Camilo Prieto, vocero del Movimiento Ambientalista Colombiano, calificó esta política del Gobierno como una "claudicación ante las mafias de la deforestación y por otro lado, una decisión irresponsable que no está comprendiendo que la el mayor problema ambiental que tiene Colombia en este instante es justamente la deforestación". De acuerdo con estimaciones del Ministro de Ambiente Ricardo Lozano, en el 2018 se talaron **alrededor de 280.000 hectáreas**.

### Menos regalías que pagar para las empresas

Finalmente, un gran mico que se aprobó en el PND es la reducción del porcentaje de regalías que aportarían las empresas mineras. Según las bases, [la **explotación de carbón a cielo abierto pasa del 5 al 3,5 % y la de oro y plata veta solo pagará 0,4% en regalías**. Cabe recordar que el dinero que se recoge de estos impuestos sirve para financiar obras de infraestructura en las regiones, educación, desarrollo y la recuperación del pasivo ambiental.]

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
