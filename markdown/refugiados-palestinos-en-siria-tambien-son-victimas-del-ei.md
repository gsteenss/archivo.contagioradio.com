Title: Palestinos refugiados en Siria también son víctimas del EI
Date: 2015-04-07 15:48
Author: CtgAdm
Category: El mundo, Política
Tags: Al Yarmuk, Campo de refugiados Palestino Siria, Frente Al Nusra, Jaled Abdul Mayid, refugiados palestino, Siria
Slug: refugiados-palestinos-en-siria-tambien-son-victimas-del-ei
Status: published

###### Foto:Huffingtonpost.com 

###### **Entrevista con [Leila Nachawati], activista y analista política hispanosiria:** 

<iframe src="http://www.ivoox.com/player_ek_4324933_2_1.html?data=lZiflp6Xd46ZmKiakpuJd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiaqfwtnOxcaPp8Lh0dSYxsqPtsba1szWw8nTt4zkwtHS1dnNstCfxtOYtc7WrcKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El campo de refugiados palestino **Al Yarmuk** cercano a la capital de Siria fue creado en 1957 para albergar a miles de ciudadanos palestinos que huían de las políticas de despojo de tierras y expulsión por parte de Israel. Actualmente es blanco de **ataques de los grupos extremistas** de carácter islamista, Estado **Islámico y Frente Al Nusra**, provocando una **emergencia humanitaria**.

El miércoles **1 de abril**, miembros del EI, ocuparon violentamente el campo, dejando un saldo de **15 muertos y más de 70 heridos**. Está no ha sido la única incursión, ya que en **2012 fue ocupado y casi controlado por los extremistas.**

Actualmente el campo alberga a más de **18.000 palestinos** que no han podido regresar a sus tierras. Según organismos internacionales como la Umrwa (Agencia de la ONU para los refugiados en Palestina) la situación es muy precaria debido a la escasez de agua, medicinas y alimentos.

La ONU, y el gobierno sirio han denunciado públicamente la situación en el campo de refugiados exigiendo el cese de la cooperación de países como Arabia Saudí con los grupos extremistas y demandando ayuda a la comunidad internacional para la protección del mismo.

Distintos **portavoces palestinos** han denunciado también la situación calificándola de **“catastrófica”.**

**Jaled Abdul Mayid** representante del **Frente de luchas Palestinas** señaló que “Los **terroristas** del EI y del Frente al Nusra **secuestraron a decenas de vecinos** del campamento, cortaron las cabezas de cinco personas y mataron a tiros a otros dos”.

Para el partido político palestino **Hamas**, la situación es cada vez peor y también la considera como una **“catástrofe”.**

Según **Fayez Abu Eid**, portavoz del **Grupo de Acción para los Palestinos en Siria** “no hay agua ni comida y todavía quedan muchos civiles que no han podido huir”.

En total en lo que va de año el **EI ha asesinado en Siria a más de 2 mil personas**, los bombardeos de la coalición encabezada por EEUU no han conseguido frenarlo y su avance es debido en parte  la financiación de países como Arabia Saudí.

Según **Leila Nachawati, activista y analista hispanosiria**, el campo de refugiados de Yarmuk esta siendo **hostigado** por parte de los extremistas como camino para la toma de la capital, pero **también por el régimen sirio, ya que cuando el levantamiento los palestinos también apoyaron a los rebeldes**, de tal manera la emergencia humanitaria es mayo por la desatencion gubernamental y por la amenaza del EI.
