Title: Mensaje de navidad para un Estado secuestrado
Date: 2018-12-19 06:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Cabal, ESMAD, Fiscal General, Mensaje de navidad
Slug: mensaje-de-navidad-para-un-estado-secuestrado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Mensaje-de-navidad-para-un-Estado-secuestrado-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)**

###### **19 Dic 2018**

[Pronto comenzará la psicosis decembrina en la que luego de paros, protestas, escándalos y crisis, todo parece detenerse para dar paso a las fiestas, a la familia o a la nostalgia. Donde literalmente todo se “desconecta” e incluso, salir a dar un recorrido en automóvil por una ciudad como Bogotá se convierte en un extraño evento placentero. Las energías se renuevan y cuando más estamos entregados a nuestras intimidades, diciembre es un mes en el que por lo general el gobierno de turno, mediante mecanismos bastante hábiles como sesiones nocturnas o extraordinarias en días festivos, hace de las suyas y aprueba aquello que en un clima de agitación política generaría controversia.  ]

[Incluso aún si estar distraído con uno que otro escándalo, por ahora, eso no se puede evitar, pues]**el Estado se encuentra secuestrado.**[ La institucionalidad se halla rígida y asegurada por aquellos que irónicamente también desde la institucionalidad, mediante mecanismos de cooptación, cupos indicativos, el computador de Palacio, primas del silencio etc. etc. etc. entablan una relación directa con el delito, con el robo del dinero público y cosas (como ya sabemos) aún peores, haciendo que incluso las atrocidades sean defendibles a costa de investiduras públicas, empresariales, familiares, de influencias partidistas y bla bla bla.]

[Reflexionar más no podemos. La situación ha sido trágica por la cantidad de gente asesinada de enero a diciembre de 2018. Ha sido indignante ver casos como el del fiscal Martínez, ha sido descarado ver el caso Carrasquilla, a Duque, a Uribe, en fin, ¿acaso los que deberían reflexionar no son ellos que causan tanto mal al país pero siguen con sus conciencias intactas?  ¿qué les limpia su conciencia? ¿El convencimiento propio de que han hecho “el bien”? ¿un par de plegarias decembrinas? ¿o el espaldarazo del ignorante (con títulos o sin títulos) que cree que “han hecho el bien” y los sigue apoyando? ¡que reflexionen ellos!]

[Claro, diciembre significa un bajón para la actividad política popular, pero eso no traducirá en una reducción en los ánimos de lucha.]

[Mensaje de navidad para todas aquellas y aquellos que están superando la rabia, el mal genio, el empute, el odio etc. contra el sistema, y lo están transformando, lo están canalizando en vías para la organización política de base y popular, más seria, menos efímera que una simple emoción y más prolongada o parecida a una convicción histórica.]

[Mensaje de navidad para todos los campesinos e indígenas que luchan por resolver la impunidad en los asesinatos de sus compañeros, asesinatos que aunque no reciben las extensas primeras planas como cuando se cubre el escándalo de cualquier funcionario público; a los campesinos que siguen produciendo y tecnificándose autogestionadamente. Por ahora, la situación del campo sigue siendo la muestra de que la exclusión política, la inequitativa tenencia de la tierra, el caciquismo y en general, todo el sistema clientelar que sostiene el secuestro del Estado se fundamenta en la violencia y la injusticia rurales. Es en la arena rural y no en las ciudades, donde nace el poder del régimen que existe en Colombia.]

[Mensaje de navidad para todos los estudiantes, docentes, trabajadoras, obreros, conductores, que marcharon por las calles colombianas desafiando al régimen, que superaron a aquellos voceros de la cultura hegemónica que critican las movilizaciones, que pasaron por el abuso extremo de la policía, y soportaron la respuesta déspota de los que se supone deberían gobernar y solucionar. Si el Esmad no tuviera permiso legal para usar las armas casi letales que usa y que están prohibidas en Europa y otros países, de seguro el caso francés con los chalecos amarillos se hubiese replicado aquí donde necesitamos tanto.]

[Mensaje de navidad por la fortaleza de los pelaos y peladas que marcharon caminando hacia Bogotá, los que aguantaron lluvia y calor, hambre y frío, ¡no desfallezcan! La universidad es solo un paso, los que luchan se conocen luego de su paso por las universidades. Un proceso de cambio es largo, pero su tiempo disminuye en la medida en que logramos organizar, conocer, planificar, cómo responder a un régimen tan antiguo, a mecanismos tan perversos como los que existen en Colombia.]

**¿Y la otra parte?...**

[A los que tienen secuestrado el Estado, a esos funcionarios públicos que recibieron el cargo como pago de un favor de campaña; a esos funcionarios que reciben prebendas de grupos empresariales para favorecer sus intereses; a esos funcionarios públicos que favorecen primero a multinacionales y después a la gente, los del favor familiar.]

[A esos supuestos periodistas que hablaron mucho más de los errores de uno que de los errores del otro, que llamaron a la guerra desde sus micrófonos, (como fue el caso de Arizmendi atizando una intervención militar en Venezuela, como si sus hijos, nietos o su casa fueran los que se afectarían con la muerte y los bombardeos) a esos “periodistas” que fueron compinches de los secuestradores del Estado y que tienen el descaro de molestarse cuando alguien los acusa de parcializados.]

[A los contratistas que desangraron los recursos del país gastándose los anticipos, dejando calles incompletas o mal construidas, permitiendo que se cayeran puentes etc.; a esa gente como Paloma, como Cabal, que han convertido el mamertismo derechoso en una bandera popular entre una parte de la clase media que me avergüenza, porque tiene todo para cambiar la situación, pero lo desprecia por su fastidiosa servidumbre cultural, intelectual y política.]

[A esos catedráticos que no perciben el suelo que pisan, que observan la realidad a través del libro así ésta les este mendigando a sus pies y que inundan la academia de una objetividad que hoy es el mayor y más peligroso de los sesgos que les impide debatir sin ofender al contrario, que les impide llegar a conclusiones que sirvan a alguien más que a su ego.]

[A esa gente, que tiene secuestrado al Estado Colombiano y que participa de los mecanismos de ese secuestro, un solo mensaje de navidad:]

*[El poder no dura para siempre, tarde o temprano cae. Por cada líder social asesinado están naciendo 10 más. El inconformismo de millones ya no será más una molestia, sino que será aquello que no podrán detener. Si nos quitan la universidad, si secuestran la crítica, entonces nos educaremos por nuestra cuenta… ya lo estamos haciendo.  Si su moral no sede, si esa moral que nos inculcan desde sus instituciones no abandona la hipocresía, entonces la destruiremos donde no pueden defenderla: la destruiremos en nuestro interior. Las marchas serán más frecuentes, pasarán por los barrios y sectores donde residen los que tienen el poder, aunque sea solamente para que vean la cara de nuestra determinación. Pronto los manifestantes mirarán de frente al Esmad, porque, aunque la represión y la violencia sean su única propuesta, nuestros muertos nos mantienen vivas y con la moral más alta que la de todo su ejército. La lucha social se librará en el campo, en la ciudad, en las aulas, en las redes, en el campo simbólico, en el del discurso, en todas las benditas y malditas partes. No parece una metáfora, pero si los poderosos son la corona, para que el Estado sea libre, para que se acabe este secuestro en que tienen a la institucionalidad, habremos de ver caer esa pesada corona, tarde, temprano, de día o de noche, un inicio o un final de año…la veremos caer.]*

*[Feliz navidad, les desea la causa popular.]*

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
