Title: La Silla Vacía, Semana y El Tiempo entutelados por el investigador Renán Vega
Date: 2016-04-14 06:00
Category: Judicial, Nacional
Tags: dea en colombia, renán vega cantor
Slug: la-silla-vacia-semana-y-el-tiempo-entutelados-por-estigmatizar-a-renan-vega
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Renán-Vega.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [13 Abril 2016 ]

Este miércoles el profesor Renán Vega Cantor acudió a los juzgados de Paloquemao para interponer tres acciones de tutela para que se garantice la protección de sus **derechos fundamentales, que fueron vulnerados por El Tiempo, Semana y La Silla Vacía** quienes lo acusaron de adelantar investigaciones desde un sesgo ideológico de extrema izquierda, como intelectual adscrito a las FARC-EP; así mismo, aseveraron que su informe para la Comisión Histórica del Conflicto contiene información falsa, inventada y sacada de la televisión.

De acuerdo con Vega, en el informe que presentó a la Comisión hay un apartado titulado 'Imperialismo sexual' en el que refirió **54 hechos de abuso sexual ocurridos en Melgar y Girardot entre 2003 y 2007**, información que provino de una investigación rigurosa y que tuvo relevancia en el informe que se elaboró en EEUU sobre "las tropelías de la DEA en Colombia", y que fue presentado por la Oficina del Inspector General del Departamento de Justicia de Estados Unidos en marzo de 2015.

Sin embargo, **según La Silla Vacía el profesor se "pifió"**, porque no tendría cómo sustentar la existencia de los 54 casos mencionados en el informe, versión que fue replicada por las columnas de Alfonso Cuellar, en Semana y el blog de Andrés Sánchez, en El Tiempo y que vendrían a reforzar la "pifia periodística", como lo calificó Vega en una detallada réplica que elaboró y que circuló por varios medios.

"La peor ofensa que se le puede decir a un historiador es que se está inventando las fuentes, que no tiene bases para decir lo que está diciendo, y esa es la principal calumnia que se ha dirigido contra mí, y a raíz de ello no sólo se ha mancillado mi respetabilidad intelectual y política sino que además se han desprendido una serie de **señalamientos y de calumnias directas de tipo político**", afirma Vega.

De acuerdo con el profesor, estos señalamientos representan la **imposibilidad de abordar otras formas de comprender la historia contemporánea colombiana, **teniendo en cuenta que la Comisión Histórica surgió con el objetivo de "poner sobre la mesa esas otras miradas" del conflicto, aquellas que nunca se han escuchado, porque sobre sus orígenes "ha habido una versión que se ha impuesto a rajatabla".

Con estas acciones de tutela Renán Vega exige a los medios en mención **reconozcan su error, rectifiquen lo publicado y retiren los comentarios ofensivos de lectores** que atentan contra su honra y buen nombre, teniendo en cuenta su dedicación a la investigación crítica durante 30 años, que le ha valido importantes reconocimientos en Colombia y en el exterior.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
