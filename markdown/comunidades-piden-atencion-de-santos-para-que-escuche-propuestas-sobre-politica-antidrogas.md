Title: Comunidades piden atención de Santos para que escuche propuestas sobre política antidrogas
Date: 2015-07-07 16:01
Category: Comunidad, Nacional
Tags: Amapola, Amapola y Marihuana, coca, Constituyente Nacional de cultivadores de Coca, Cultivos de uso ilícito, fumigaciones con glifosato, Marihuana, Mesa regional de Putumayo, política antidrogas, Putumayo
Slug: comunidades-piden-atencion-de-santos-para-que-escuche-propuestas-sobre-politica-antidrogas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Sin-título.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [ANZORC]

##### <iframe src="http://www.ivoox.com/player_ek_4733456_2_1.html?data=lZyglZmZeo6ZmKiak5iJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8af0c7Rx5DFsIzk08rgy8nJstXZjLjO0NnTt4zl1sqY0tfTtNbZ1NnO1ZDIqYzgwtiYxdTRuY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Marcela Muñoz, Mesa Regional de Organizaciones sociales de Putumayo] 

Del 4 al 6 de julio, se realizó la Constituyente Nacional de cultivadores de coca, marihuana y amapola, cuyas conclusiones estuvieron enmarcadas en la necesidad de generar una nueva política desde los  cultivadores, productores y consumidores. A partir de ese escenario, **se llamó la atención del presidente Juan Manuel Santos, para que escuche las propuestas de política antidrogas que se plantean desde las diferentes comunidades** del país.

Durante la constituyente se habló sobre la reparación de las víctimas de la política antidrogas, teniendo en cuenta la necesidad de entender que “**el campesino cultiva coca, no porque quiera, sino por la desatención por parte del Estado”**, ya que la única atención del gobierno “**llega en una avioneta llena de veneno**”, dice Marcela Muñoz, integrante de las Mesa Regional de Organizaciones Sociales del Putumayo, refiriéndose a las fumigaciones con glifosato, que por estos días tienen en crisis humanitaria a los campesinos e indígenas del Putumayo.

**"El cultivo de coca se debe entender como un cultivo que se puede agro-transformar,** lo que se debe poner en discusión es el uso ilícito”, dice Muñoz, quien añade que la política antidrogas del gobierno nacional se da desde “el enfoque equivocado, un  enfoque prohibicionista y no un tratamiento desde la salud pública o los derechos humanos”, por lo que desde la constituyente se le pidió al gobierno que se discuta una política de cultivos de uso ilícito en el marco de la sustitución gradual y voluntaria y no en el marco de la erradicación.

En este encuentro nacional, también se tuvo la posibilidad de compartir los saberes ancestrales alrededor de la mata de coca. Las comunidades indígenas tuvieron una participación importante en este evento resaltando la responsabilidad de defender el cultivo de coca como un cultivo ancestral.

Respecto al proceso de paz, las organizaciones sociales saludan los avances sobre los cultivos de uso ilícito, pero creen que **hace falta la participación de las comunidades afectadas por la política antidrogas,** es por eso que se debe tener en cuenta las propuestas de las comunidades entendidas como apuestas de paz con justicia social.

Cabe resaltar que en la constituyente se realizó un acto simbólico en torno al cese al fuego bilateral y la paz con justicia social.
