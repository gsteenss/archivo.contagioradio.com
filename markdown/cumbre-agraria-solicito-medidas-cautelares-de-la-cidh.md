Title: Cumbre agraria solicitó medidas cautelares de la CIDH
Date: 2016-06-07 14:54
Category: Movilización, Paro Nacional
Slug: cumbre-agraria-solicito-medidas-cautelares-de-la-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/la-minga-es-alegria-7-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: congresodelospueblos-cumbreagraria] 

###### [07 Jun 2016] 

Las organizaciones que hacen parte de le Cumbre Agraria, **solicitaron a la Comisión Interamericana de Derechos Humanos que dicte medidas cautelares** para resguardar a miles de personas que han salido a exigir el cumplimiento de los acuerdos del gobierno, según la Cumbre Agraria la represión ha alcanzado límites insospechados y pone en riesgo la vida de los manifestantes, y el señalamiento por parte del Min Defensa sobre infiltraciones del ELN pueden generar más y más agresiones.

Los fuertes operativos policiales y militares dejan una cifra de **203 personas heridas, 171 personas detenidas y 104 judicializados, así como [8 amenazas](https://archivo.contagioradio.com/aguilas-negras-amenazan-a-lideres-de-la-cumbre-agraria-prensa-rural-y-guardia-indigena/) y 3 asesinatos**. ([Balance de DDHH](https://archivo.contagioradio.com/aumenta-a-152-el-numero-de-heridos-y-a-145-los-detenidos-en-la-minga-nacional/)) Las cuentas más críticas se registran en el departamento del Cauca y Valle del Cauca, así como en Antioquia por el caso de las amenazas de paramilitares al Movimiento Ríos Vivos.

Desde el inicio de las protestas la semana pasada, convocadas por la Cumbre Agraria, se mantienen miles de personas en 100 puntos del país, con bloqueos esporádicos en vías como la panamericana, las vías hacia Quibdó o algunas alternas en Santander y Norte de Santander, también en Antioquia e Cesar.

En el comunicado de las organizaciones sociales y campesinas se afirma que han “t**enido un trato de guerra y un sin número de amenazas de grupos paramilitares**” por lo cual la solicitud de medidas cautelares. Además exigieron una rectificación por parte del Ministro de Defensa ante lo que se considera una estigmatización de las protestas sociales. Además reafirman que hasta que se retiren las FFMM y se brinden garantías de DDHH no habrá conversaciones.

**Desde el pasado Sábado los voceros de la Cumbre Agraria se encuentran deliberando en el resguardo “La María” en el municipio de Santander de Quilichao** en el departamento del Cauca y aspiran a que las conversaciones con el gobierno nacional se realicen allá para contar con la presencia de la mayor parte de las organizaciones que han hecho parte de esta protesta.

[![CkXscD\_WYAEFOvf](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/CkXscD_WYAEFOvf.jpg){.aligncenter .wp-image-25066 width="502" height="764"}](https://archivo.contagioradio.com/cumbre-agraria-solicito-medidas-cautelares-de-la-cidh/ckxscd_wyaefovf/)
