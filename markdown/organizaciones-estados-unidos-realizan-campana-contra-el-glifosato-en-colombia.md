Title: Organizaciones en Estados Unidos realizan campaña contra el Glifosato en Colombia
Date: 2015-05-07 16:00
Author: CtgAdm
Category: DDHH, Nacional
Tags: cultivos de hoja de coca, Estados Unidos, Glifosato, Latin American Working Group, Lisa Haugart, Lucha anti drogas en Colombia, Organización Mundial de la Salud, organizaciones de DDHH, propuestas de erradicación de las comunidades, Washington Office on Latin America, Washington Office on Latin America (WOLA); Asociación Interamericana para la Defensa del Ambiente
Slug: organizaciones-estados-unidos-realizan-campana-contra-el-glifosato-en-colombia
Status: published

###### Foto: colombiaantidrogas.co 

<iframe src="http://www.ivoox.com/player_ek_4462786_2_1.html?data=lZmjlJyceo6ZmKiakpyJd6KmmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRk9PbwtPW3MbHrdDixtiYp9jYpcXj1JDC0M7Is9Sf08rOzs7epc%2BfxMba0saJh5SZo5bOjcjTstXmwpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Lisa Haugart Latin American Working Group] 

Además de los efectos nocivos para la salud humana por el uso del **glifosato** en aspersiones aéreas, los daños ambientales por mutaciones genéticas causadas en la vegetación, la contaminación de las fuentes hídricas en varias regiones de Colombia, las **organizaciones de DDHH** y civiles en **Estados Unidos** tienen como argumento que las fumigaciones no han alcanzado su objetivo de reducir el número de las plantaciones de hoja de coca.

Según un reciente estudio de agentes estatales en Estados Unidos los **cultivos de hoja de coca** aumentaron cerca de un 35% en Colombia, lo que significa claramente que no sirve la estrategia anti drogas que se está usando y que mantiene como uno de sus pilares el combate contra las plantaciones y contra los pequeños cultivadores. Pero además el desplazamiento forzado por razones de salud o económicas también es evidente.

Según Lisa Haugart, directora de **Latin Américan Working Group**, organización que se ha dedicado a denunciar las violaciones a los DDHH en Colombia y a denunciar las complicidades empresariales o legales desde el propio EEUU, afirma que el uso del glifosato se está denunciando como nocivo desde hace más de 10 años, pero que sólo hasta la publicación del informe de la **OMS** se ha podido hacer eco de las múltiples denuncias existentes por el uso del herbicida en la lucha antidrogas.

Haugart también señala que la lucha anti narcóticos debe hacerse teniendo en cuenta las **propuestas de erradicación de las comunidades con sustitución de cultivos**, así como tener en cuenta que la mesa de conversaciones de la Habana se ha acordado que la fumigación será el último recurso para el combate a las drogas y no el primero como se realiza ahora.

Se espera que estas jornadas de presión política tanto en el congreso de EEUU como en otros escenarios internacionales rinda sus frutos y se prevengan los efectos dañinos sobre la salud humana y el ambiente.

**El uso del glifosato viene siendo criticado en desde hace varios años**, pero tomó fuerza cuando la República del Ecuador denunció a Colombia ante tribunales internacionales. En esa ocasión en 2013 Colombia debió pagar una suma cercana a los 17.000 millones de dólares por los efectos nocivos del uso del glifosato en aspersiones aéreas que penetraban a territorio ecuatoriano causando graves afecciones sobre la salud humana y el ambiente.

Algunas de las organizaciones que hacen parte de esta campaña son Latin America Working Group (LAWG);  Washington Office on Latin America (WOLA); Asociación Interamericana para la Defensa del Ambiente (AIDA); Oxfam Environmental Investigation Agency Center for International Policy (CIP) entre otras.
