Title: El choque de barbaries
Date: 2015-11-18 08:43
Category: Cesar, Opinion
Tags: Ataques en Paris EI, Daech, Estado Islámico, Fundamentalismo religioso, Siria, Unión Europea apoya ataque
Slug: el-choque-de-barbaries
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/3C8.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Latuff] 

#### **[Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)**

##### [18 Nov 2015]

De uno de los libros (2007) del profesor libanés Gilbert Achcar, quien imparte cátedra en Londres, hemos tomado su título para esta columna. El atentado terrorista del “Estado Islámico” en París, con más de 130 muertos, evidencia el fundamentalismo religioso, el integrismo, de los actores no estatales; el Estado de Excepción, los bombardeos y la guerra civil mundial es la otra faz, la de los Estados de Occidente, la Unión Europea y Estados Unidos. Dos barbaries.

Insistimos en la naturaleza terrorista del Daech (Isis, Estado Islámico), conservadora, reaccionaria; no se trata, lo decimos con firmeza, de una “izquierda antiimperialista en proceso de reconversión cultural” como lo afirman despistados sectores académicos y políticos en Colombia. Sus orígenes son diversos, como lo son las distintas culturas, religiones e historias de la región asiática y, en particular, del próximo y del medio Oriente, e indudablemente tienen que ver con las resistencias populares a las intervenciones imperialistas europeas (“civilizatorias” las siguen llamando los señores de la guerra), el declive de los regímenes nacionalistas árabes (Nasser) y la ocupación militar de la entonces Unión Soviética en Afganistán (1979-1989). Los sectores integristasradicalizados fueron financiados, incluidas las armas, por las monarquías de la región,Estados Unidos y Europa.

Así mismo, Occidente y sus Estados han declarado la guerra en nombre de la “humanidad”. Pero es su guerra contra la humanidad. Desde octubre de 2001, en respuesta a los atentados de Al-Qaeda contra Estados Unidos, George Bush hijo decretó el Estado de Excepción, canceló las libertades civiles y asumió la guerra contra el terrorismo como una “cruzada” mundial. De entonces a hoy la coalición con la OTAN y la Unión Europea se ha impuesto y los resultados los conocemos: bombardeos, presos “no-personas”, Abu Ghraib y Guantánamo, interceptación de comunicaciones, vigilancia satelital, construcción de Muros, segregaciones colonizadoras (Israel en Palestina). Una espiral de violencia; dos barbaries.

Ahora bien, el terrorismo del Estado Islámico tiene una ubicación precisa: Siria. Es allí en donde la guerra ha asumido proporciones significativas y en donde Francia ha intensificado sus acciones militares con bombardeos que hoy mediáticamente se proyectan como “respuesta” a los ataques en París. En Siria se vive una guerra civil desde hace 4 años; el régimen de Bashar Al-Assad es el responsable directo por los desplazamientos, el gaseamiento de la población civil, el genocidio de los kurdos, la huida de los hoy refugiados y la terrible crueldad; pero es apoyado por Rusia - que considera “legítimo” al gobierno - e Irán con el visto bueno de Irak y Egipto; Alemania no interviene militarmente pero está involucrada. Las operaciones de guerra son encabezadas por Estados Unidos y Francia, ambos partidarios de una retirada de Al-Assad para una solución transitoria a la crisis y controlada por ellos, frente a Rusia; pero tales acciones eran principalmente efectuadas contra la oposición, diferente al Estado Islámico, que enfrenta al régimen sirio (a la cual no se ha querido entregar armamento).

Por supuesto, el atentado terrorista en Paris modifica las circunstancias. Ahora François Hollande lidera la causa de la “libertad, igualdad y fraternidad” y las bombas son arrojadas directamente en la zona controlada por el Isis. Sin embargo, el estribillo del “Todos somos Paris” es engañoso: el rechazo al terrorismo se ha evidenciado - y con razón - en la solidaridad espontánea del mundo con el pueblo francés y en el repudio a la masacre; pero ello no debe ser entendido como apoyo al gobierno de Hollande ni al Estado colonialista (recordemos a Argelia - África - y al Caribe de “Nuestra América”).

Tampoco dicha solidaridad puede convertirse en la firma de un documento en blanco que le conceda patente de corso a Hollande para cancelar o disminuir las libertades civiles y las garantías democráticas con base en el Estado de Excepción. Mucho menos la solidaridad debe ser entendida como la puerta abierta para nacionalismos extremos y discursos tipo Le Pen; ni tampoco para aceptar que las limitadas políticas hacia los refugiados que huyen de la guerra en Siria, y que se aumentarán por los bombardeos, se cancelen.
