Title: Denuncian cuatro asesinatos simultáneos en el sur de Córdoba
Date: 2017-01-20 13:31
Category: DDHH, Nacional
Tags: paramilitares, violencia contra defensores de derechos humanos
Slug: atencion-denuncian-cuatro-asesinatos-simultaneos-en-el-sur-de-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/movimienot-campesino-e1478190781832.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Macondo ] 

###### [20 Ene 2017] 

La **Asociación de Campesinos del Sur de Córdoba ASCSUCOR** denunció en un comunicado, publicado este 20 de enero, que el 19 de enero se **presentaron cuatro asesinatos en esa región** en la que se ha denunciado el reforzamiento de la presencia paramilitar desde el preagrupamiento de la guerrilla de las FARC-EP.

Un primer hecho tiene que ver con el **asesinato de Hernán Enrique Agamez,** en la vereda San Cipriano en el corregimiento tierra adentro de Montelibano Córdoba. **Hernán era integrante del comité cocalero y dinamizaba la implementación del punto 4 del acuerdo de paz en esa vereda**, además era integrante de ASCSUCOR.

El segundo hecho fue  hacia las 7 de la noche, en la vía que conduce desde La Rica, Puerto Libertador, a Juan José, en el mismo corregimiento. Los campesinos denuncian que **hayaron el cuerpo sin vida de Marcelino Pastrana Fernández.**

**Las otras dos personas asesinadas fueron Everto Julio Quiñones y Fredys Cogoyo**, el hecho sucedió en la vía que conduce desde La Rica, Puerto Libertados, al corregimiento Tierra Adentro en Montelibano. Fredys Cogoyo era el hijo del presidente de la junta de acción comunal de la vereda El Salado.

ASCSUCOR ha denunciado que desde finales del 2014, se ha incrementado la presencia paramilitar en el territorio y además se ha hecho **caso omiso por parte de las autoridades correspondientes, a las alertas tempranas y los informes de riesgo** emitidos por la Defensoría del Pueblo.

Debido a la gravedad de los hechos la Asociación Campesina hace  un llamado al defensor del Pueblo y la comunidad Nacional e Internacional para que se preserven las libertades, la justicia y la soberanía colombiana, ya que las **Fuerza Pública no ha reaccionado** ante las denuncias y solicitaron el apoyo para impedir que más hechos como este se sigan presentando.

Comunicado de Prensa

****LA ASOCIACIÓN DE CAMPESINOS DEL SUR DE CORDOBA****  
****“ASCSUCOR”****  
Nit. ****900.575.888-5****  
*Integrante de la* *****COORDINACIÓN AGROMINERA DEL NOROCCIDENTE Y EL MAGDALENA MEDIO COLOMBIANO*****  
*Filial al ****MOVIMIENTO POLÍTICO Y SOCIAL MARCHA PATRIÓTICA*****  
*Miembro de la ****Asociación Nacional de Zonas de Reserva Campesina ANZORC*****  
****EMITE****  
****DENUNCIA PÚBLICA****  
****ANTE LA COMUNIDAD NACIONAL E INTERNACIONAL****  
Nro. ****<u>063</u>****  
****Enero 20 de 2.017****

*Cuatro Asesinatos simultáneos ocurrieron el día de ayer 19 de enero del presente año en el Sur de Córdoba en inmediaciones rurales de los municipios Puerto Libertador y Montelibano (Córdoba).*

****HECHOS:****

1.  En horas de la tarde de ayer 19 de enero del presente año, la comunidad campesina del corregimiento Juan José (Puerto Libertador – Córdoba) conoció del asesinato del campesino****HERNAN ENRIQUE AGAMEZ FLOREZ****, ocurrido en la vereda San Ciprian del Corregimiento Tierradentro (Montelibano – Córdoba), el fallecido era residente de Juan José en donde vivía con su familia; Hizo parte del Comité Cocalero de dicha zona y que dinamizaba políticamente la preparación para la implementación del acuerdo cuatro, eje uno en cohesión al acuerdo de reforma rural integral, impulsándose en el territorio desde la Asociación de Campesinos del Sur de Córdoba (ASCSUCOR), organización campesina filial a la Marcha Patriótica – Córdoba. El cuerpo del campesino ****HERNAN ENRIQUE AGAMEZ FLOREZ**** fue trasladado desde la vereda San Ciprian del Corregimiento Tierradentro (Montelibano – Córdoba) hasta el corregimiento Juan José (Puerto Libertador – Córdoba) en hamaca por campesinos y la guardia indígena zenú san jorge, llegaron hasta dicho corregimiento aproximadamente a las 10 de la noche, desde allí fue trasladado a la morgue del municipio de Puerto Libertador (Córdoba).
2.  Un segundo hecho victimizante ocurrió entrada la noche en la vía que del corregimiento la Rica Puerto Libertador (Córdoba) conduce al corregimiento Juan José (Puerto Libertador – Córdoba), sobre la vida de la persona ****MARCELINO PASTRANA FERNANDEZ.**** Su cuerpo fue llevado hasta la morgue del municipio de Puerto Libertador (Córdoba) en la misma noche de ayer.
3.  Un tercer y cuarto hecho aconteció contra la vida de ****EVERTO JULIO QUIÑONEZ MIRANDA**** y****FREDYS COGOLLO MORA**** sucedido en la vía que del corregimiento la Rica Puerto Libertador (Córdoba) conduce al corregimiento Tierradentro (Montelibano) a la altura de la finca la cumbre. El cuarto mencionado es hijo del presidente de la Junta de Acción Comunal de la vereda el Salao del corregimiento la Rica Puerto Libertador (Córdoba).

****ANTECEDENTES:****

La Asociación de Campesinos del Sur de Córdoba (ASCSUCOR), que desde 2012 adelanta múltiples escenarios de reivindicación de los derechos del campesinado surcordobes y que resisten en los municipios de Montelibano, Puerto Libertador y San José de Uré aplicando la consigna “Por la dignidad de nuestras vidas y territorios” ha venido denunciando, alertando y comunicando sistemáticas y de antaño violaciones a los derechos humanos y al derecho internacional humanitario; así como movilizaciones sociales y mesas de concertación con el gobierno nacional, departamental y municipales.

Desde finales de 2014 hemos venido denunciando el incremento de la presencia paramilitar en el territorio, la inexistencia de la fuerza pública y el latente riesgo en el que viven los campesinos y campesinas de esta región.

La Defensoría del Pueblo, mediante el Sistema de Alertas Tempranas (SAT) ha venido generando inminentes llamados al gobierno colombiano para que conjure la posibilidad de graves hechos contra la vida, integridad, seguridad y bienes de la población civil; entre los que se puede apreciar:[Informe de Riesgo N° 015-13 2013](https://drive.google.com/file/d/0B3ioaATX6HtGVG5Tb3NwQXV6eVU/view?usp=sharing) - [Informe de riesgo N° 034-14 de inminencia1 2014](https://drive.google.com/file/d/0B3ioaATX6HtGdTFaVU1qV3h4Mnc/view?usp=sharing) - [Informe de Riesgo Nº 030-16A.I. de Inminencia1 2016](https://drive.google.com/file/d/0B3ioaATX6HtGYldjajNSOGV4am8/view?usp=sharing).

Tras el pre-agrupamiento de los frentes de las farc que hacían presencia en esta vasta región, el incremento, presencia y control irregular por parte de grupos paramilitares ha sido inminente. Hoy por hoy, estas zonas poblacionales que organizadas mediante la ASCSUCOR se preparan para la implementación de los acuerdos sobrevenidos entre el gobierno del presidente Santos y las Farc ven como una amenaza e imposibilidad consecuente de ello que estructuras provistas de la re-ingeniería paramilitar cada día cobren mayor presencia en el territorio.

****ANTE ESTO:****  
****EXIGIMOS:****

1.  A la Fiscalía General de la Nación y la Procuraduría General de la Nación se investiguen estos dolorosos hechos, ya que generan miedo y zozobra a las comunidades campesinas de la región, y se convierten en actos determinadores de agresiones a las organizaciones sociales, sus integrantes y comunidades en general.

<!-- -->

2.  Al Gobierno Nacional tomar las medidas pertinentes que brinden efectivas garantías y goce de los Derechos Humanos, en consecuencia: El Derecho a la Vida, la integridad personal, la seguridad, la organización y la libre movilidad de los líderes de ASCSUCOR.

**** ****  
****SOLICITAMOS:****

1.  Al Defensor del Pueblo Nacional, al Coordinador Residente y Humanitario de la Oficina de las Naciones Unidas en Colombia, al Procurador disciplinario delegado para los Derechos Humanos, al Fiscal General de la Nación, a la Consejera Presidencial de DD.HH y DIH:

Instar de manera urgente a quienes competa implementar las medidas necesarias a fin de preservar las libertades y el orden, la justicia y soberanía Colombiana que se han desvirtuado en la consecución de derechos de los ciudadanos y ciudadanas en el marco de las declaraciones de los derechos humanos, los derechos de los pueblos, afianzado en la constitución política colombiana y demás actos de ley, protocolos y tratados internacionales

1.        A la comunidad Nacional e Internacional, a las organizaciones defensoras de Derechos Humanos, el acompañamiento y apoyo ante esta vulneración de los derechos humanos fundamentales que concurrieron en la relatoría de estos hechos y a difundir públicamente esta denuncia.

   
****RESPONSABILIZAMOS:****

-   Al Estado Colombiano, en cabeza del presidente ****Juan Manuel Santos Calderón****: Recordando que la responsabilidad primaria de protección de los derechos humanos recae de acuerdo con los principios constitucionales y el articulado de los tratados y Convenios Internacionales sobre el Estado colombiano, al ser éste el sujeto responsable de velar por el cumplimiento de lo consagrado en la Carta Política y por ser el que adquiere los compromisos en materia de derechos humanos y de derecho internacional humanitario ante la comunidad internacional. Por tal razón, su mayor deber jurídico es el de prevenir las violaciones frente a estos derechos y tomar las medidas necesarias para investigar, identificar, juzgar y sancionar a los responsables por su acción u omisión, o tomar las medidas correctivas de carácter administrativo o político que impida que hechos como este se sigan perpetrando.

###### Reciba toda la información de Contagio Radio en [[su correo]
