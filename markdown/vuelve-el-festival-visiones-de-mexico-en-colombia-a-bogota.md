Title: Vuelve el Festival Visiones de México en Colombia a Bogotá
Date: 2016-09-02 12:00
Category: Cultura, eventos
Tags: colombia, Festival visiones de México en Colombia, Fondo de cultura Económica, mexico
Slug: vuelve-el-festival-visiones-de-mexico-en-colombia-a-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/banner-twitter-e1472767221427.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  Foto: Fondo de cultura económica 

##### 2 Sep 2016 

En su sexta versión vuelve a Bogotá el **Festival Visiones de México en Colombia**, un evento que tiene como escenario el Centro Cultural Gabriel García Márquez, sede del Fondo de Cultura Económica en el país.

Para este año, el evento incluye varias actividades entre ellas tres exposiciones de arte. La primera “**Alas para Volar**” donde 35 pintores mexicanos y un colombiano le rendirán homenaje a una de las grandes pintoras, **Frida Kahlo**, la segunda titulada “Vive la Lucha Mexicana”, que expone la Lucha libre AAA y los grandes luchadores que han subido a los ring y la última “Pliegues de Memoria”, una presentación basada en los trabajos de encuadernación de Dulce María Luna.

En cuanto a los invitados, la muestra presentará a **Dorian Roldan Peña**, vicepresidente de la Lucha libre AAA mexicana, **Christopher Domínguez** crítico literario y novelista, **Alejandro Magallanes**, diseñador gráfico, **Jeannette L. Clariond**, poeta y traductora.

Entre los invitados nacionales estarán: **Héctor Abad Faciolince**, **Juan Esteban Constaín, Alberto Salcedo Ramos, Susana Carey**, artista y escenógrafa y los poetas **Rómulo Bustos** y **Darío Jaramillo Agudelo**, entre otros.

En cuanto a lo musical, la programación incluye un concierto del Grupo de Cobres y la Orquesta Sinfónica del Conservatorio de Música, ambas de la Universidad Nacional, este último cerrará el Festival acompañado de la **venta nocturna tradicional de libros FCE**, con descuentos especiales desde las seis hasta las diez de la noche.

El Festival Visiones de México en Colombia, estará partir del **2 hasta el 17 de septiembre**, los eventos son totalmente gratis y podrá disfrutar de igual forma de la variada comida Mexicana. Si desea consultar más puede ingresar aquí.
