Title: La carta de Piedad Córdoba a Vladdo
Date: 2016-04-19 10:29
Category: Nacional, Política
Tags: Brasil, Dilma Rouseff, piedad cordoba, Vladdo
Slug: la-carta-de-piedad-cordoba-a-vladdo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/vladdo-y-piedad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [19 Abril 2016]

La ex congresista colombiana Piedad Córdoba escribió al caricaturista Vladdo una respuesta a sus trinos en torno a la votación que se realizó en la cámara baja del parlamento de Brasil y que resultó con la aprobación del pedido de [Impechment](https://archivo.contagioradio.com/?s=impeachmen) a la presidenta [Dilma Rouseff.](https://archivo.contagioradio.com/multitudinarias-movilizaciones-en-brasil-rechazan-intentos-de-golpe-de-estado/)

Luego de una serie de respuestas del carticaturista, Piedad Córdoba decidió escribir sus argumentos en un texto más largo que un tuit, hasta el momento se desconoce la respuesta de Vladdo.

Este es el texto de la carta

*“Al respecto, me permito responderle lo siguiente:*

*A Dilma Rousseff no se la acusa por el escandaloso caso de los sobornos en Petrobras, (y aquí varios medios importantes lo informan así) es más, ni siquiera ha sido investigada y menos acusada por ello. Por otro lado, el tema Petrobras ha salpicado no sólo a miembros del Gobierno, sino también a muchos políticos y funcionarios de otros partidos y desde hace casi dos décadas.*

*A Dilma se la acusa de presunto "crimen fiscal" por una maniobra contable que llevan haciendo los gobiernos locales, regionales y nacionales de Brasil por mucho tiempo, en otras palabras, si se juzga a Dilma por ello, deberían también hacerlo a centenares de gobernantes en todo Brasil.*

*Por otro lado Vladdo, si la juzgan para derrocarla por un supuesto crimen que en realidad no existe, entonces ¿qué es? Pues un golpe de estado, ¿o no?*

*Vladdo, no es del todo cierto que la gente no se siente defraudada de Dilma, millones de brasileros han salido a respaldarla a las calles, lo que pasa es aquí en Colombia SOLO le hacen cubrimiento y despliegue a las marchas opositoras que también son numerosas, pero medio Brasil sino más, está con Dilma.*

*Vladdo, en Brasil TODOS los medios descaradamente están en contra de Dilma. No me extraña su baja imagen así como pongo en remojo los resultados de las encuestas, que tú bien sabes hoy son una herramienta electoral y política, el caso Dilma tiene micho de un linchamiento mediático bien orquestado.*

*Vladdo, es muy injusto que se hable de los problemas del Gobierno de Dilma, que los tiene, sin mencionar el enorme legado social de sus gobiernos y el de Lula Da Silva que por ejemplo, en 12 años hicieron más universidades que en toda la historias de Brasil y sacaron de la pobreza a 40 millones de personas, logro que fue reconocido por la misma ONU. ¿Se busca invisibilizar esto?*

*Vladdo no niego que el Gobierno de Brasil tenga problemas de corrupción que deben ser investigados hasta el fondo y coincido contigo en que se debe castigar a los responsables, pero tras de esto, lo que en verdad se esconde es una estrategia para sacar a Dilma del poder, por parte de quienes perdieron la pasadas elecciones.*

*¿Sabías por ejemplo que más de la mitad de los diputados que votaban ayer tienen problemas de corrupción PEORES que los que le pretenden imputar a Dilma?*

*¿Sabías que el 30% del congreso que es evangélico le está cobrando a Dilma que se haya declarado atea?*

*¿Sabías que un diputado dijo ayer al votar: “voto por mi familia y porque en Brasil no haya educación sexual?”*

*Y lo que es peor: ¿sabías que el diputado Jair Bolsonaro voto contra Dilma y en ese momento dijo públicamente que lo hacía en nombre del Coronel Ustra quien la violó y torturó cuando estuvo presa?*

*Vladdo, ¿no te das cuenta que quienes quieren tumbar a Dilma y con un argumento polémico, son un congreso más corrupto que el de Colombia, (que ya es mucho decir), un grupo de fanáticos religiosos y un grupo de ultraderechistas militaristas?*

*Vladdo, como demócrata que sé que eres, que has cuestionado con dureza los cinismos de la derecha colombiana, ¿no te das cuenta que es su equivalente en Brasil quienes buscan derrocar a Dilma para volver al poder y poder privatizar Petrobras? Recuerda que esta empresa es hoy un activo geoestratégico global luego de hallazgos que permiten suponer que Brasil tendría reservas de petróleo entre 30.000 y 50.000 millones de barriles.*

*¿No será Vladdo, que detrás de semejante montaje de “juicio democrático” lo que hay es un contubernio entre políticos y empresarios con enormes intereses económicos?*

*Vladdo, no te pido que respaldes a Dilma, tienes todo el derecho en profesar tus posturas, derecho que además respeto. Lo que te reclamo es que hagas un juicio ponderado de la situación de Brasil.*

*Con aprecio,*

*Piedad Córdoba Ruiz”*
