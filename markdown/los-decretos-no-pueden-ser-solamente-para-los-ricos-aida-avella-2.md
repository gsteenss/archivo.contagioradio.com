Title: "Los decretos no pueden ser solamente para los ricos": Aída Avella
Date: 2020-03-25 12:45
Author: CtgAdm
Category: Actualidad, Economía
Tags: Carrasquilla, decreto 444, decretos, empresas
Slug: los-decretos-no-pueden-ser-solamente-para-los-ricos-aida-avella-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Diseño-sin-título-4.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Los más de [20 Decretos](https://dapre.presidencia.gov.co/normativa/decretos-2020/decretos-marzo-2020)por parte de la presidencia para abordar la crisis que ha generado el coronavirus en Colombia, varias son las reacciones, críticas y preguntas, pero el que más generó polémica fue el **Decreto 444 del 21 de marzo**, por contener varias medidas que según varios sectores sociales del congreso, de organizaciones de DDHH y sindicatos, entre otros, favorecen a los más ricos y a las grandes empresas presentes en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La congresista Aida Avella, de la Unión Patriótica conversó con Contagio Radio sobre las preguntas que genera este decreto y otros decretos , que deberían hacerse para hacer de estas medidas, unas más equitativas y que favorezcan a la mayoría de los colombianos y colombianas que, por cuenta del coronavirus podrían verse más afectados, tanto económica, como socialmente.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AidaAvellaE/status/1242186439779065856","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AidaAvellaE/status/1242186439779065856

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Entre varios puntos la Senadora afirmó que el Decreto tiene inconsistencias enormes, al proponer atender la emergencia del [Covid-19](https://archivo.contagioradio.com/hay-negligencia-de-estado-contra-reclusos-que-protestaba-por-riesgo-de-covid/), *"finalmente su único propósito es acatar y salvaguardar a los bancos con transferencias monetarias que salen de los recursos públicos ahorrados por la nación, es decir con los impuestos que pagamos todos los colombianos"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo añadió que este es una reglamentación *"indolente e inverosímil en un contexto, donde millones de nuestros compatriotas no tienen asegurado el acceso a los alimentos o lo básico para sobrevivir; en donde disponen de los dineros públicos para intereses privados, **en vez de buscar una manera de llegar a quienes lo necesitan de tal manera que no tenga que salir arriesgar su vida al exponerse al contagio del coronavirus**"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y ante la responsabilidad que asume el Ministerio de Hacienda de manejar este fondo dijo, *"el papel que ha cumplido Carrasquilla no ha sido otro más que ayudar a sus amigos banqueros, fomentando así que los que más tienen plata, y que además tiene poder sobre los [fondos de pensiones](https://archivo.contagioradio.com/fondos-de-pensiones-publicos-son-la-mejor-opcion-para-los-colombianos/)privados y públicos siga haciendo creciendo su bolsillo"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que es necesario que los ciudadanos se hagan preguntas ante los decretos presentados, que se informen y sepan como se está organizando el país, *"yo tomo vocería y me cuestiono, por eso hago todas esas críticas y estoy escribo como Senadora una carta al Presidente Duque y al Ministro Carrasquilla, solicitando que me las aclaren, q****ué está pasando, qué es lo que va a pasar; porque el dinero no puede ser solamente para los ricos y a los pobres dejar las moronas"*.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AidaAvellaE/status/1242508092920782848","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AidaAvellaE/status/1242508092920782848

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Lo que se puede identificar acerca de los decretos

<!-- /wp:heading -->

<!-- wp:paragraph -->

De las cinco partes que componen el Decreto 444, más de la mitad son cuestionadas. La primera reflejada en el Artículo 4 que afirma que este fondo se usará para atender las necesidades generadas por parte de las entidades que hacen parte del presupuesto general de la nación; es decir, un dinero distribuido en ministerios, agencias o entidades públicas que no se puedan solventar con el presupuesto general; pero deja en interrogativa la correspondencia de estos a las entidades de la salud.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otro punto cuestionado es el apartado tres, el cual se refiere la liquidez transitoria del sector financiero, distribuida a través de transferencias temporales de valores depósitos, plazo, entre otros; acción que varios sectores han interpretado como una inyección de capital a las entidades financieras para que estás de alguna manera puedan seguir ofreciendo sus servicios y productos, como por ejemplo préstamos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente el punto cuatro, hace referencia a las **empresas privadas públicas o mixtas con actividades de desarrollo internacional** que cuenten con acciones o bonos de deudas, los cuales pueden ser entregados al Estado a cambio de dinero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La crítica en este último por los analistas, es que se privilegia a las grandes empresas con acciones de millones, dejando de lado a hospitales, EPS y a las medianas y pequeñas empresas, entidades que no cuentan con un respaldo de interés para el Estado, y que pierden la posibilidad de solicitar un dinero vital para el pago de sus sueldos, deudas o demás gastos empresariales.

<!-- /wp:paragraph -->
