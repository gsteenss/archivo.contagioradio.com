Title: 3 producciones colombianas estarán en el Festival de Cannes
Date: 2015-04-25 10:14
Author: CtgAdm
Category: 24 Cuadros, eventos
Tags: Alias María, Cesar Augusto Acevedo, Cine Colombiano, el abrazo de la serpiente, Festival de Cannes, Festival Internacional de Cine de Cartagena, José Luis Rugeles, La tierra y la sombra
Slug: 3-producciones-colombianas-estaran-en-el-festival-de-cannes
Status: published

*El Festival más importante del cine Francés contará con 3 películas producidas en Colombia.*

La edición 68 del **Festival Internacional de Cine de Cannes**, una de las citas anuales de mayor prestigio e importancia para la cinematografía mundial, contará con una nutrida participación de producciones colombianas convirtiéndose así en el país Latinoamericano con mayor presencia en toda la selección del Festival, con tres largometrajes. Chile, Argentina y México, participan con una película cada uno.

Se trata de las cintas “**El abrazo de la Serpiente**” del director Ciro Guerra, “**La tierra y la Sombra**” de Cesar Acevedo y “**Alias María**” de José Luis Rugeles, quienes desfilaran por la alfombra roja entre el 13 y el 24 de Mayo.

**[El abrazo de la Serpiente]**

**[[![el abrazoimg](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/el-abrazoimg.jpg)**

**"El abrazo de la serpiente"** es la única película de ficción de América Latina seleccionada para participar en **la Quincena de los Realizadores**, escogida dentro de una convocatoria de 3.365 películas entre cortos y largometrajes de todo el mundo. Será además el estreno mundial de la película del director cesarense **Ciro Guerra** (La sombra del caminante, 2005, Los viajes del viento, 2009) y la productora **Cristina Gallego**.

Es sin duda el reconocimiento a la proeza de rodar durante siete semanas en la Amazonía colombiana, primera en más de 30 años, enfrentando dificultades propias de la geografía y el clima de Mitú (Vaupés) y Puerto Inírida (Guainía), donde el espectador es el ganador por la cantidad de  de parajes fantásticos, imponentes y arriesgados que aportaron todo su dramatismo al filme.

Inspirada en los diarios de los primeros exploradores que recorrieron la Amazonía Colombiana, Theodor Koch-Grunberg y Richard Evan Schultes. Cuenta la épica historia del primer contacto, encuentro, acercamiento, traición y posible amistad que trasciende la vida, entre Karamakate, un Chamán Amazónico, último sobreviviente de su tribu, y dos científicos (interpretados por el actor belga Jan Bijvoet (Borgman) y el estadounidense Brionne Davis) que con cuarenta años de diferencia recorren el Amazonas en busca de una planta sagrada que podría curar sus males.

La cinta que llegará a las salas colombianas el próximo 21 de mayo, fue pre- estrenada el pasado 22 de abril en Bogotá, con participación especial del embajador de Francia Sr. Jean-Marc Laforêt, Claudia Triana, directora de Proimágenes; y los protagonistas nativos Tafillama (Antonio Bolívar), Nilbio Torres (Karamakate Jóven y viejo respectivamente) y Yauenkü Migue (Miguel Dionisio Ramos).

La película es co- producida por Ciudad Lunar Producciones con NorteSur de Venezuela;  MC Producciones y Buffalo Films de Argentina y en asocio con Caracol Televisión y Dago García Producciones.

**[La tierra y la sombra]**

[![11146284\_837684092984354\_5889877120333562555\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/11146284_837684092984354_5889877120333562555_n.jpg){.alignleft .wp-image-7677 width="390" height="211"}](https://archivo.contagioradio.com/?attachment_id=7677)

La ópera prima de **Cesar Augusto Acevedo,** producida por Burning Blue en co-producción con Topkapi (Holanda), Rampante Cine (Chile), Preta Portê (Brasil), competirá en la **Semana de la Crítica**, sección paralela del Festival, que busca "impulsos renovadores en el cine". La tierra y la sombra, es uno de los 11 largometrajes, siete de ellos en competencia, que fueron seleccionados entre 1750 películas provenientes de diferentes rincones del mundo.

Filmada en el Valle del Cauca con un reparto de actores locales, la cinta está ambientada en una plantación de caña de azúcar y el negocio alrededor de esta y cuenta la historia de Afonso, un viejo campesino que regresa a su casa tras una ausencia de 17 años. Al llegar descubre que todo lo que alguna vez conoció ya no existe, todo ha cambiado, la destrucción de la región y su familia que ya no espera verlo; lo único que permanece casi intacto en su memoria: su antiguo hogar.

En el reparto están: Haimer Leal como Alfonso; Hilda Ruiz como Alicia; Edison Raigosa como Gerardo, el hijo de Alfonso y Alicia; Marleyda Soto como Esperanza, la esposa de Gerardo; y Felipe Cárdenas como Manuel el hijo de Gerardo y Esperanza. Marleyda Soto ya había debutado en el cine con “Doctor Alemán”.

Durante su proceso de realización, **“La tierra y la sombra"** contó con el apoyo de varias instituciones incluyendo al Fondo para el Desarrollo Cinematográfico (FDC) de Colombia, el Hubert Bals + de Holanda y de los festivales de San Sebastián y La Habana.

La representación de Latinoamérica dentro de la categoría se complementa con la película argentina **“Paulina”** de **Santiago Mitre** (El estudiante, 2011), en la sección reservada a primeros y segundos filmes que incluye además cuatro fuera de competencia y una decena de cortos. Un jurado presidido por la actriz y realizadora israelí Ronit Elkabetz atribuirá varios premios al final del certamen.

**[Alias María]**

[![alias ma](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/alias-ma.jpg){.alignleft .wp-image-7678 width="640" height="237"}](https://archivo.contagioradio.com/?attachment_id=7678)

El filme que dió apertura a la versión 55 del FICC Festival Internacional de Cine de Cartagena de Indias, sigue dando de qué hablar. **“Alias María”** de **José Luis Rugeles**, producido por Rhayuela Cine y escrita por **Diego Vivanco**, representará a Colombia en **Un Certain Regard** (Una Cierta Mirada) del Festival de Cannes, convirtiéndose en la cuarta película colombiana que logra llegar a esta sección, después de **Cóndores no entierran todos los días** de **Francisco Norden** (1984), **Los viajes del viento** de **Ciro Guerra** (2009) y **La Playa D.C.** de **Juan Andrés Arango** (2012).

Con **“Alias María”**, **Rugeles** regresa a la pantalla grande, luego de **García** (2010), y cuenta la experiencia de María, una guerrillera de 13 años que observa el parto secreto del hijo del comandante, mientras trata de decidir qué hará con su propio embarazo. Es una invitación a la reflexión sobre **los absurdos de la guerra y el conflicto en nuestro país desde una mirada personal y social.**

Rodada en el Magdalena Medio, escenario real del conflicto colombiano, cuenta con un reparto principal de 5 actores naturales seleccionados entre 1800 aspirantes: Karen Torres, Anderson Gómez, Erik Ruiz, Neiver Agudelo y Deivis Sánchez, junto a un grupo de actores provenientes del teatro como Carlos Clavijo, la argentina Lola Lagos, Fabio Velasco, Julio Pachón y Carmenza González.

La película contó con el apoyo de Fondo para el Desarrollo Cinematográfico FDC de Colombia, Ibermedia, The Global Film Initiative, INCAA, OIM, USAID. La producción además es una propuesta multiplataforma, compuesta de una serie de productos y actividades profundamente ligadas a la temática de la historia y que se llevarán a cabo en diferentes zonas del país, como talleres de contenidos audiovisuales en zonas con altos índices de reclutamiento y utilización de jóvenes, transformando a través de las cámaras su visión del mundo.

Este año el jurado de Un Certain Regard será presidido por la actriz y directora italo-americana Isabella Rossellini, un espacio en el que cada año se presentan una serie de películas con diferentes tipos de visiones y estilos que buscan el reconocimiento internacional.
