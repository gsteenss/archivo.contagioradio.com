Title: 106 Madres Comunitarias serían indemnizadas
Date: 2017-02-27 16:55
Category: Mujer, Nacional
Tags: ICBF, Madres Comunitarias, SINTRACIHOBI
Slug: 106-madres-comunitarias-serian-indemnizadas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/MadresComunitarias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Villanueva 24H] 

###### [27 Feb 2017] 

Después de años de espera e incertidumbre, la tutela interpuesta por la defensa de las Madres Comunitarias ante la Corte Constitucional, fue aprobada, mediante la sentencia   **T-480 de 2016 se ordena al Gobierno Nacional reconocer los derechos laborales a 106 madres comunitarias** que se le habían vulnerado sus derechos fundamentales en materia laboral, sin embargo, ellas dicen que la medida no resuelve la situación de todas las madres y podrían irse a paro indefinido.

Olinda García, líder del sindicato de madres comunitarias –SINTRACIHOBI–, manifestó que el Estado “nunca ha sido claro en la creación de políticas públicas sobre el trabajo de nosotras” y que ha dejado en manos de terceros la estructuración del programa, **“la situación está así porque la mayoría de las ONG que contratan, son de funcionarios y exfuncionarios del ICBF y otras de políticos de turno”**, lo que ha sido causa de las irregularidades en los pagos y contrataciones.

###  

Sin embargo, a pesar del fallo de la Corte, que hará un ajuste a los pagos que se adeudan a estas madres, hace algunas semanas el presidente Santos objetó una ley que permitía a las madres comunitarias contar con todas las condiciones laborales asegurando su derecho a la estabilidad laboral, las razones que señaló Juan Manuel Santos tienen que ver con la inconstitucionalidad de la norma, por considerar que esta **“atentaba contra la sostenibilidad fiscal del país”, ya que la atención implicaría \$769.000 millones al año,** “afectando el equilibrio macroeconómico” puntualizó el mandatario.

Frente a ello, las madres comunitarias y el grupo de abogados defensores, señalan que si “¿cuando se habla de sostenibilidad fiscal, también se está pensando en el manejo eficiente de los recursos?”, indican que el anuncio del presidente Santos **“no tiene en cuenta los recientes escándalos y déficit fiscales” ocasionados por contratos multimillonarios con filiales como Odebrecht,** y los paraísos fiscales.

Blanca Albarracín otra de las madres comunitarias, comentó que si hoy Colombia está hablando de la construcción de paz desde los territorios, se deben crear las condiciones para que mujeres, como las madres comunitarias, **“que desempeñan un papel fundamental en la construcción del tejido social”** cuenten con las condiciones laborales mínimas para realizar su labor. ([Le puede interesar: 65.000 madres comunitarias irían a paro indefinido](https://archivo.contagioradio.com/65-000-mil-madres-comunitarias-irian-a-paro-indefinido/))

Por último, las lideresas aseveraron que **“al presidente Santos y a la directora del ICBF se les olvida que tienen una deuda social con las madres comunitarias”**, revelaron que se unirán a las actividades del Paro Internacional de Mujeres, a la movilización que se realizará en Bogotá y aseguraron que de no tener una respuesta efectiva a la situación de todas las madres comunitarias, 65.000 en todo el territorio nacional, entrarían en un paro nacional indefinido.

###### Reciba toda la información de Contagio Radio en [[su correo]
