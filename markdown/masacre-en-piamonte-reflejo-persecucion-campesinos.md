Title: Masacre en Piamonte, un reflejo de la persecución a campesinos
Date: 2020-04-06 19:26
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Asesinato de campesinos, Cauca, Piamonte, Sustitución de cultivos de uso ilícito
Slug: masacre-en-piamonte-reflejo-persecucion-campesinos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/familia-asesinada.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto:

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/maidani-salcedo-sobre-asesinato-hamilton-piamonte_md_49748464_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Maidany Salcedo, Lideresa Social (Piamonte - Cauca)

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

Con el asesinato de **Hamilton Gasca Ortega y sus dos hijos de 10 y 7 años,** integrante de la Asociación de Trabajadores Campesinos de Piamonte ocurrido el pasado 3 de abril, quedan en evidencia los riesgos y amenazas que enfrenta una organización que promueve la sustitución voluntaria en Cauca, la última de estas ocurrida el 5 de marzo cuando los integrantes de esta organización fueron amenazados de muerte por grupos armados presentes en la zona.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**ASINTRACAMPIC** es una organización que nació en el 2012 en Piamonte y que impulsó el Acuerdo de Paz desde que se negoció en La Habana hasta su posterior firma e implementación, realizando la pedagogía y la socialización en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La lideresa Maidany Salcedo relata que al ser firmada la paz, sus integrantes fueron incluidos en el proceso de sustitución voluntaria, siendo un total de 945 familias las que participaron del Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito (PNIS), es a partir de aquel momento en que comienzan las amenazas. [(Lea también: Presidente Duque, fumigando no se acaba con el narcotráfico: lideresa Maydany Salcedo)](https://archivo.contagioradio.com/presidente-duque-fumigando-no-se-acaba-con-el-narcotrafico-lideresa-maydany-salcedo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"**Conservamos 1380 hectáreas de montaña en conjunto con 130 familias"** que están siendo beneficiadas por diversos proyectos que promueven la protección de la Amazonía. Sus integrantes, que hacen parte de Fensuagro y a Marcha Patriótica, continúan sin comprender por qué se les ataca, "no estamos haciendo nada malo, simplememente buscamos el beneficio para nuestro campesinado".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Son 5 los integrantes de la Asociación de Trabajadores Campesinos de Piamonte asesinados

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La lideresa advierte que el asesinato de Hamilton no es el primero ocurrido contra integrantes de la Asociación de Trabajadores Campesinos de Piamonte, Cauca, a la fecha han sido un total de cinco personas parte de la asociación asesinadas sin incluir a los dos menores que también fueron atacados durante el pasado fin de semana. [(Le recomendamos leer: ¿Qué está pasando en Cauca? análisis del líder campesino Óscar Salazar)](https://archivo.contagioradio.com/que-esta-pasando-en-el-cauca-un-analisis-del-lider-campesino-oscar-salazar/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Uno de los homicidios más recientes ocurrió en agosto de 2017 cuando Fernando Asprilla, integrante de la Asociación, fue asesinado en la vereda La Tigra, donde residía y había ocupado el cargo de presidente de la Junta de Acción Comunal. [(Lea también: Más de 200 integrantes de Marcha Patriótica han sido asesinados entre 2011 y 2020)](https://archivo.contagioradio.com/asesinatos-marcha-patriotica-2011-2020/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Los ataques, explica Maidany, también incluyen falsos positivos judiciales contra miembros de la organización y amenazas contra la junta directiva**, incluida la dirigente quien junto a su familia ha tenido que desplazarse del municipio para proteger su vida. [(Le puede interesar: Asesinan a Hamilton Gasca Ortega y sus dos hijos en Piamonte, Cauca)](https://archivo.contagioradio.com/asesinan-a-lider-hamilton-gasca-ortega-y-sus-tres-hijos-en-piamonte-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Afirma que **ASINTRACAMPIC ha sido declarada como objetivo militar por grupos como el Cartel de Sinaloa, 'La Constru' o las disidencias de las FARC,** además de las denuncias que ha hecho la [Comisión de Justicia Paz](https://twitter.com/Justiciaypazcol)sobre grupos como 'La Mafia' quienes también delinquen en el municipio, "creeríamos que es porque le decimos no a las multinacionales, no a la extracción de petroleo, no a la deforestación". [(Lea también: Denuncian nuevas amenazas contra Maydany Salcedo, lideresa de Cauca)](https://archivo.contagioradio.com/amenazas-maydanysalcedo-lideresa-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"A pesar que se están acatado los protocolos de cuarentena nos siguen matando, allá se están peleando el territorio por el narcotráfico, es un municipio entre Putumayo y Caquetá donde nunca pasa nada. Enterramos nuestros muertos y salen los desplazados, pero nunca pasa nada".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
