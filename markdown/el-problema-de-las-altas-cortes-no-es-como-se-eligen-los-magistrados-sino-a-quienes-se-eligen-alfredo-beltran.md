Title: “El problema de las altas cortes no es de quién elige los magistrados, sino a quienes se elige” Alfredo Beltrán
Date: 2015-03-13 22:12
Author: CtgAdm
Category: Entrevistas, Política
Tags: Alfredo Beltran, Asamblea Constituyente, Corte Constitucioinal
Slug: el-problema-de-las-altas-cortes-no-es-como-se-eligen-los-magistrados-sino-a-quienes-se-eligen-alfredo-beltran
Status: published

###### Foto: cumbreporlapaz.blogspot 

<iframe src="http://www.ivoox.com/player_ek_4212674_2_1.html?data=lZeelJubeI6ZmKiak5WJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56nic2f0dfcxNHJscKfxcqYzsbXb8Lg1cbgjcjTttXZ1JDb0ZDJt4zXhqigh6eXsdCf1MqYx9HNq8ahhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alfredo Beltrán, Ex magistrado de la Corte Constitucioal] 

Según el ex magistrado de la Corte Constitucional, en la situación actual de la Corte Constitucional, es necesario ir más allá de la situación episódica en que está envuelto el magistrado Pretet, que es una situación individual, hay que mirar si lo que hay es el reflejo de una crisis de valores o de principios no solo en la Corte sino en el conjunto de la sociedad colombiana.

Sobre la licencia solicitada por Pretet, Beltrán afirma que si la licencia fuera suficiente para conjurar la crisis no estaríamos en crisis. El Ex magistrado cita a Hamlet cuando dice que “algo podrido  anda en Dinamarca”, de manera que no es suficiente con el retiro temporal y por ello Pretet debería retirarse del cargo.

Sobre la posibilidad de la constitución Beltrán afirma que es necesario estudiar el tema con detenimiento porque hay quienes aprovecharían la oportunidad que da la crisis. Seguramente “no todos los que llegaran a esa constituyente, tendrían la intensión de impulsar la democracia” como aquellos que afirman que la tutela es un instrumento maldito.
