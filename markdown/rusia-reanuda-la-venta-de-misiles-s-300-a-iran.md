Title: Rusia reanuda la venta de misiles S-300 a Irán
Date: 2015-04-14 12:51
Author: CtgAdm
Category: El mundo, Política
Tags: Acuerdo Irán con G5+1, misiles S-300, Rusia reanuda la venta de misiles S-300 a Irán, Rusia vende misiles a Irán
Slug: rusia-reanuda-la-venta-de-misiles-s-300-a-iran
Status: published

###### Foto:Andina.com.pe 

En 2007 Rusia e Irán firmaron un acuerdo para la venta de **misiles S-300** por un total de 800 millones de dólares. En **2010** el presidente Ruso, siguiendo las directrices de la ONU, prohibió su venta a Irán, ahora **Vladimir Putin**, en consonancia con el acuerdo nuclear entre Irán y el G5+1, **ha levantado la prohibición** para poder continuar con la venta de misiles antiaéreos.

Después de la prohibición de **Dimitri Medvedev** en 2010, Teherán se vio obligado a llevar a Rusia a la Corte Internacional de Arbitraje en Ginebra (Suiza), para reclamar a Moscú 4.000 millones de dólares de indemnización por el contrato incumplido.

Los misiles S-300 tienen la tecnología suficiente para** **alcanzar a aviones o misiles lo que suponen un que Iran cuenta con un **sistema de defensa antiaéreo** que lo pone en el mismo nivel de otros Estados que ya tienen sistemas similares.

El contrato se cerró debido a las presiones por parte de la **ONU** cuando se establecieron las sanciones a Irán por su programa nuclear. Dichas sanciones provenían de la UE y de EEUU, lo que obligó a Rusia a cumplir con el mandato de Naciones Unidas y suspender la venta de misiles.

**Rusia e Irán, han sido** **afectados por las sanciones impuestas por la UE y EEUU,** uno por el programa nuclear y el otro por su implicación en la guerra en Ucrania, lo que ha hecho que económicamente establezcan relaciones bilaterales.

Alta Representante de Política Exterior y de Seguridad Común de la UE**, Federica Mogherini** expresó su preocupación en la **venta de misiles** a Irán porque en su opinión contribuye a desestabilizar la región, pero añadió que **"…Pensamos que no afecta a las negociaciones en curso sobe el programa nuclear iraní…"**.

El acuerdo llegado el 2 de Abril entre Irán y el G5+1, queda pendiente de ser ratificado y se espera que para Junio este ya firmado.
