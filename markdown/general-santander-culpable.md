Title: Acusado de atentado a la General Santander denuncia presiones para declararse culpable
Date: 2019-01-24 21:57
Author: AdminContagio
Category: DDHH, Nacional
Tags: Atentado, ELN, General Santander, Humana Radio
Slug: general-santander-culpable
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Diseño-sin-título-3-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [ 24 Ene 2019] 

Este jueves Humana Radio hizo pública una carta en la que **Ricardo Andrés Carvajal Salgar, acusado de ser parte de la red que puso la bomba en la Escuela** de Cadetes de Policía General Santander, afirma ser inocente de esta acusación; y sostiene que está recibiendo presiones para declarar su culpabilidad en el caso.

El documento es una transcripción de una carta que envió Carvajal desde el patio de máxima seguridad de La Picota, y allí, él asegura que el audio en el que se lo diciéndole a un amigo que ya habían puesto la bomba y les tocaba "guardarse" es real, pero **obedece a una "broma de mal gusto"**. (Le puede interesar: ["Violar protocolo establecido con el ELN sería un irrespeto a la Comunidad Internacional"](https://archivo.contagioradio.com/protocolo-eln/))

En su relato, Carvajal cuenta la vida de un hombre que vive de la construcción, y que aparentemente está en la cárcel por un chiste. El hombre señala haber sido grabado por cámaras de seguridad en una panadería lejos de la Escuela General Santander, en momentos en que se cometía el atentado. (Le puede interesar:["Colombia no puede omitir el DIH ni violar los protocolos de la mesa con el ELN"](https://archivo.contagioradio.com/colombia-no-puede-omitir-el-d-i-h-ni-violar-los-protocolos-de-la-mesa-con-eln/))

Adicionalmente, explica que las personas dueñas de la bodega en la que se guardó la camioneta y que lo identifican junto al vehículo en realidad no lo conocen, y su testimonio e identificación puede deberse a que su rostro fue expuesto por los medios de comunicación empresariales tras conocerse su captura. Por último, Carvajal relata que **durante las audiencias de legalización de captura ha recibido presiones de hombres para que declare su culpabilidad**, mientras sus hijas afirman que han visto movimientos de personas extrañas cerca a su lugar de residencia.

Por su parte, el amigo con el que habló Carvajal acudió a la Fiscalía para asegurar que sí se realizó la llamada y la misma no era una broma. El ente acusador ha señalado que la conversación la obtuvo de sus sistema de vigilancia telefónica a personas sospechosas, **aunque no ha aclarado por qué tenía interceptado al amigo de Carvajal**.

![Ricardo Andrés Carvajal Salgar](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-24-at-10.11.05-554x800.jpeg){.alignnone .size-medium .wp-image-60543 width="554" height="800"} ![WhatsApp Image 2019-01-24 at 10.11.05 (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-24-at-10.11.05-1-491x800.jpeg){.alignnone .size-medium .wp-image-60544 width="491" height="800"}

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
