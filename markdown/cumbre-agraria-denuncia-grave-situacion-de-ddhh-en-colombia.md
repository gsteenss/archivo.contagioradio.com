Title: En el 2015 indígenas colombianos han recibido 2.373 agresiones
Date: 2015-03-11 01:39
Author: CtgAdm
Category: Otra Mirada, Paz
Tags: Aida Avella, alirio uribe, Angela Maria Robledo, Cumbre Agraria, Derechos Humanos, DIH, Ministerio del Interior, Ministro de defensa, proceso de paz
Slug: cumbre-agraria-denuncia-grave-situacion-de-ddhh-en-colombia
Status: published

###### Foto: Contagio Radio 

La grave situación que amenaza a los líderes y lideresas de organizaciones que hacen parte de la Cumbre Agraria, Campesina, Étnica y Popular,  se evidencia en el informe presentado este martes  en la sede de la ONIC, denominado **“Balance Situación de  Vulneración de DDHH e Infracciones al DIH”.**

A través de la Subcomisión de Derechos Humanos, Garantías y Paz, se denuncia “un plan de exterminio en contra del Movimiento Social y Popular en el País” según indica el informe. Voceros de diferentes partes del país y miembros del MOVICE, la ANZORC, ANAFRO, Marcha Patriótica, el Congreso de los Pueblos entre otros, acompañados por el vocero de la Representante a la Cámara, Ángela María Robledo, y el senador Alirio Uribe, denunciaron las constantes arremetidas, intimidaciones, bombardeos, fumigaciones, falsos positivos, en los departamentos del **Tolima, Caquetá, Putumayo, Arauca, Meta, Casanare, Antioquia, Norte de Santander, Atlántico, Cauca,  Bolívar y Bogotá; por parte del ESMAD y el ejército.**

En el informe se destacan las violaciones de derechos humanos durante el 2015, de manera colectiva e individual, contra campesinos y campesinas, indígenas y afrodescendientes. Respecto a la situación de las comunidades indígenas, el Consejero Mayor del la ONIC, Luis Fernando Arias, enumeró **“110 acciones bélicas, 16 amenazas colectivas, 35 amenazas individuales, 2 atentados individuales, 2 atentados colectivos, 1500 acciones de confinamiento, 671 víctimas de desplazamiento forzado, 20 detenciones arbitrarias, 15 asesinatos selectivos, una ejecución extrajudicial y un allanamiento ilegal”.**

En el Norte del Cauca, una de las zonas más afectadas,  se habla sobre **8 ejecuciones extrajudiciales, 4 amenazas colectivas, 4 penas o tratos crueles inhumanos y degradantes, 2 casos de tortura, 4 desplazamientos forzados colectivos y 4 ataques ilegales a la honra y la reputación,** contra habitantes del Municipio de Caloto por parte de “Las Águilas Negras”. Además, se detallan uno a uno los casos y las circunstancias de amenazas, asesinatos y agresiones contra líderes y lideresas sociales.

Durante la rueda de prensa, los voceros de la cumbre resaltan que **no es posible que mientras a la Habana se habla de paz, los movimientos populares sean víctimas de la guerra,** de manera que rechazaron los comunicados del Ministerio del Interior donde se falta la verdad y exigieron al Gobierno del Presidente Santos, que se tomen medidas encaminadas a proteger la vida de los movimientos populares.

Se trata de “**un plan sistemático muy grande desde la institución, todos los días hay amenazas y crímenes”**, denuncia la presidenta de la Unión Patriótica, Aída Avella, y agregó que el Ministro de Defensa es un **“ministro de guerra”**. Por su parte, el vocero de la representante Ángela María Robledo, se unió a esta afirmación y dijo que se ha encontrado un alto hostigamiento por parte de las Fuerzas militares contra las comunidades y los colectivos populares. Así mismo, se reitera que la Fiscalía no investiga ni acelera los procesos cuando se trata de casos de asesinatos contra líderes y lideresas sociales.

[Balance situación de vulneración de Derechos Humanos e Infracciones al Derecho Internacional Humanitario – marzo 2015  
](https://archivo.contagioradio.com/actualidad/cumbre-agraria-denuncia-grave-situacion-de-ddhh-en-colombia/attachment/balance-situacion-de-vulneracion-de-derechos-humanos-e-infracciones-al-derecho-internacional-humanitario-marzo-2015/)

\[embed\]https://www.youtube.com/watch?v=nIvxCgcgMwE\[/embed\]

[ ](https://archivo.contagioradio.com/actualidad/cumbre-agraria-denuncia-grave-situacion-de-ddhh-en-colombia/attachment/balance-situacion-de-vulneracion-de-derechos-humanos-e-infracciones-al-derecho-internacional-humanitario-marzo-2015/)
