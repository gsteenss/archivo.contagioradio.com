Title: Un atentado contra la solidaridad internacional
Date: 2015-07-22 11:19
Category: Eleuterio, Opinion
Tags: Al qaeda, Alan, Daesh, Eleuterio Gabon, Kobani, Siria, Suruç
Slug: un-atentado-contra-la-solidaridad-internacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/fdfgdgd.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Albertanonews] 

#### **[[Eleuterio Gabón](https://archivo.contagioradio.com/eleuterio-gabon/) ]** 

###### [22 Jul 2015] 

Suruç,  ciudad turca en la frontera con Siria, fue el lugar elegido por el Estado Islámico, Daesh, para realizar su primer antentado en Turquía. Hubo al menos 30 muertos y más de 100 heridos, todos ellos jóvenes comunistas, anarquistas y afines que se concentraban a favor de la resistencia del pueblo kurdo.

Cuando meses atrás entrevistamos a Alan Kanjo, realizador kurdo-catalán, para que nos contara sobre la situación en Kobani, ciudad kurda en la provincia de Rojava al norte de Siria de donde es originaria su familia, nos habló de Suruç de donde había regresado y a donde se disponía a volver de nuevo. *"La ciudad estaba totalmente desbordada y su población se ha duplicado de 100 a 200 mil personas debido a la llegada masiva de refugiados."* La decisión del ISIS de ocupar militarmente el Kobani en una ofensiva total, produjo la huida de miles de personas hacia Suruç en septiembre de 2014, entre los que se encontraban los familiares de Alan.

Alan también nos contó sobre el pueblo kurdo, un pueblo de 50 millones de personas repartidas en los territorios de cuatro estados diferentes; Turquía, Siria, Irak e Irán. Un pueblo sin estado, de tradición nómada, descendiente de los persas y de marcado carácter laico en una zona de predominancia islámica.  El estallido de la guerra en Siria, tras la terrible represión por parte del gobierno a su propio pueblo que había iniciado una revolución, dio pie a que los kurdos de Siria se lanzaran también a realizar su propio proceso revolucionario "*Repartieron el poder por cantones con independencia administrativa, formaron asambleas públicas y crearon una escolarización en kurdo a partir de 2012. En plena guerra Habían perdido el suministro de agua y electricidad sin embargo se autogestionan construyendo pozos y comprando generadores para tener unas horas de luz.”*

Lo que más llamó la atención de todo este proceso revolucionario del pueblo kurdo fue el papel que sus mejeres desempeñaban en él. El rol de la mujer fue siempre importante en la cultura kurda y en la nueva situación de emergencia, esa importancia quedó manifiesta. En el Kobani las mujeres representan un 40% en las milicias del YPG(Unidades de Protección Popular). “*Tienen muy claro que su papel en la resistencia es fundamental, están dispuestas a romper tabús y acabar con la imagen tradicional de la mujer. Este proceso supone un paso por la liberación de la mujer nacido de la propia cultura kurda. No se daría sin el consentimiento y la complicidad de los hombres. Por mucho que otros casos similares se intenten en otras partes de Oriente Medio, los hombres siempre dicen no. Todo esto va más allá del terreno militar y trasciende a otros ámbitos sociales. Muchas otras mujeres están en la misma onda que sus compañeras milicianas y trasmiten valores que se están generalizando en las familias y en la sociedad kurda en general. Es un comienzo pero este proceso tiene también una importancia crucial para la valoración de la mujer en el ámbito de Oriente Medio."*

En el lado opuesto a esta revolución aparece la reacción del Estado Islámico o Daesh, un grupo que tras desvincularse de Al-qaeda en Irak y aprovechando la guerra en Siria se introdujo en el país para imponer la ley islámica a sangre y fuego.

Hace apenas un mes Alan relataba através de las redes sociales la masacre que Daesh había provocado en Kobani. A medida que las noticias iban llegando, Alan relataba la destrucción de las ciudades y los asesinatos de sus más cercanos familiares a manos de este grupo fascista de ideología islamista.

La comunidad internacional ha dado la espalda al pueblo kurdo por pretender una revolución realmente transformadora y democrática en Oriente Medio, dejandolo a merced del fanatismo de Daesh. En Suruç, quienes se concentraban a favor del pueblo kurdo el pasado lunes, eran jóvenes solidarios que se disponían a llegar a Rojava para trabajar en su reconstrucción.
