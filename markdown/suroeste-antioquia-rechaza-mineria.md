Title: ¿Por qué los municipios del suroeste de Antioquia rechazan la minería?
Date: 2019-04-29 11:57
Author: CtgAdm
Category: Voces de la Tierra
Tags: consultas populares, Jericó Antioquía, Mineria
Slug: suroeste-antioquia-rechaza-mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/33081661_10155900278132772_7763543078492700672_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mesa Ambiental 

¿Por qué **13 municipios del suroeste de Antioquia no quieren minería de metales en su territorio**? ¿Qué impacto tendría sobre ecosistemas de municipios como **Jericó y Támesis**?. Las comunidades afectadas se declaran en "rebeldia" frente a la locomotora minera.

Nos acompañaron en Voces de la Tierra **Rodrigo Negrete** abogado consultor ambiental; **Luis Álvaro Pardo**, economista especialista en derecho minero y Constitucional y **Fernando Jaramillo,** Coordinador de la mesa ambiental de Jericó. Además, abordamos el tema de las 5 coadyuvancias al fracking en el Congreso y las [filtraciones en vídeo por el caso Minesa](https://archivo.contagioradio.com/filtran-video-que-evidencia-campana-de-estigmatizacion-de-minesa-contra-ambientalistas/).

<iframe id="audio_35093247" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35093247_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información ambiental en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en Contagio Radio 
