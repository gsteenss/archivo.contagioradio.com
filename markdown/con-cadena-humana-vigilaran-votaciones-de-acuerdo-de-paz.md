Title: Con cadena humana vigilarán votación de Acuerdo de Paz
Date: 2016-11-28 15:34
Category: Movilización, Nacional
Tags: acuerdo de paz, Cadena Humana
Slug: con-cadena-humana-vigilaran-votaciones-de-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Plaza-de-B.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Minube] 

###### [28 Nov. 2016] 

La sociedad civil sigue movilizándose para exigir la refrendación y la implementación pronta de los Acuerdos de Paz, **este martes 28 de Noviembre en la Plaza de Bolívar ubicada en la ciudad de Bogotá, se darán cita cerca de 500 personas quienes a través de una intervención que han dado a conocer como “cordón humano” realizarán un ejercicio de control y veeduría ciudadana.**

**La idea es que estas personas puedan rodear 102 siluetas que serán los rostros de cada uno de los senadores que votará la refrendación del Acuerdo de Paz**. Además de las personas que estarán en la Plaza, habrán cerca de 200 más dentro del Congreso quienes informarán en tiempo real cómo y quiénes han votado.

“Las personas que están al interior del Congreso informarán en tiempo real al equipo que estará en la plaza cómo ha votado cada uno de los congresistas, quiénes no han llegado y quiénes se han salido. Esto, para que sobre las siluetas se ponga un sticker que dirá "SÍ", "NO", "NO LLEGÓ" o "SE SALIÓ" reza una comunicación dada a conocer a propósito del evento.

Esta actividad es convocada por los movimientos AcuerdoYa, Paziempre y la Ong Sinestesia, quienes han asegurado que de ser necesario estarán miércoles y jueves realizando la misma actividad para las votaciones que se darán en Cámara sobre los Acuerdos de Paz. **La actividad pretende ser transmitida en tiempo real.**

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
