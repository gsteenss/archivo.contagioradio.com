Title: 342 periodistas han sido asesinados entre 2006 y 2015 en América Latina
Date: 2015-12-23 18:31
Category: DDHH, El mundo
Tags: Asesinatos de periodistas en 2015, Periodistas asesinados en América Latina
Slug: 342-periodistas-han-sido-asesinados-entre-2006-y-2015-en-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Asesinato-de-periodistas1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Voa Noticias ] 

###### [23 Dic 2015 ] 

[De acuerdo con el último informe de la ‘Comisión de investigación de Atentados a Periodistas’ CIAP, de la ‘Federación Latinoamericana de Periodistas’ FELAP, **en los últimos 10 años ha aumentado en 60% el índice de asesinatos de comunicadores en América Latina**. Los deshonrosos primeros lugares son ocupados por México con 15 casos, seguido de Honduras con 10, Brasil con 8, Colombia con 5 y Guatemala con 3 en lo que va corrido de este 2015 que en total reportaría 43 asesinatos.]

[**Desde 2006 hasta la fecha han sido asesinados en total 342 periodistas en Latinoamérica**, 87% de ellos eran reporteros locales y más de la mitad trabajaban online. Entre los países de América Latina y el Caribe en los que no se registran episodios de este tipo se destacan Antigua y Barbuda, Bahamas, Barbados, Belice, Costa Rica, Cuba, Chile, República Dominicana, Granada, Jamaica, San Cristóbal y Nieves, San Vicente y Las Granadinas, Santa Lucia, Surinam, Trinidad Tobago y Uruguay.]

[El informe concluye llamando la atención en el aumento de asesinatos de periodistas en la región, que son **impulsados por altos niveles de poder político vinculados a la corrupción** imperante en los niveles locales, municipales, departamentales y estatales. Así mismo, asegura no contemplar casos de desaparición forzada, por la dificultad en su seguimiento debido a los **temores de las víctimas y sus familias de interponer la respectiva denuncia**.  ]
