Title: Fallo sobre ilegalidad de huelga de pilotos golpea la libertad sindical
Date: 2017-11-30 13:10
Category: Movilización, Nacional
Tags: ACDAC, Avianca, Huelga de pilotos, Pilotos, Sindicalismo
Slug: fallo-sobre-ilegalidad-de-huelga-de-pilotos-golpea-la-libertad-sindical
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/DKmPaWSWAAoR06c-e1506535314979.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  CUT] 

###### [30 Nov 2017] 

En un comunicado de 11 puntos, los pilotos que trabajan para la compañía Avianca y que se encuentran agremiados en la Asociación Colombiana de Aviadores Civiles (ACDAC), rechazaron la decisión de la Corte Suprema de Justicia, quien declaró ilegal huelga de 51 días porque, supuestamente, se trata de un servicio público esencial. Los pilotos indicaron que la justicia en Colombia **“le da un duro golpe” al movimiento sindical del país**. Además, aseguran que habrá despidos masivos y criminalización del conflicto.

El 29 de noviembre, la Sala de Casación Laboral de la Corte Suprema de Justicia, ratificó la decisión del Tribunal Superior de Bogotá de **declarar ilegal el cese de actividades por los pilotos del sindicato**, quienes solicitaban mejores condiciones laborales por parte de Avianca. La huelga, se llevó a cabo entre el 20 de septiembre y el 9 de noviembre de 2017 y no logró que la empresa se sentara en la mesa de negociación con los aviadores.

### **Fallo de la Corte, desconoce los derechos de la libertad sindical** 

Con esta decisión, los pilotos indicaron que se desconocieron los convenios de la Organización Internacional del Trabajo que tienen que ver con la libertad sindical y la protección a los derechos de sindicalización. Argumentaron que “La Corte desconoció y **permitió la burla** de las recomendaciones del Consejo de Administración y del Comité de Libertad Sindical de la OIT que son vinculantes para el Estado y deben ser atendidas obligatoriamente”. (Le puede interesar: ["En Colombia hay un déficit de 300 controladores aéreos"](https://archivo.contagioradio.com/en-colombia-hay-un-deficit-de-300-controladores-aereos/))

Adicionalmente, la Corte Suprema de Justicia **“sepulta el derecho de asociación sindical** y de negociación colectiva, y a la huelga como herramienta para concertar las relaciones laborales entre los trabajadores y sus empleadores”.  Dicen que no solo el movimiento sindical se afecta con esta decisión, sino que también lo hacen los defensores de derechos humanos y a los trabajadores en general pues “cercena la protesta social y le arrebata a los trabajadores el derecho constitucional a la concertación de las relaciones laborales”.

### **Pilotos piden garantías para evitar despidos masivos** 

La decisión de ilegalidad, le dio a Avianca **la libertad para despedir a los pilotos que hicieron parte de la huelga**. De hecho, algunos que ya están de vuelta en su trabajo, han denunciado que la empresa le ha puesto memorandos y perciben actitudes represivas por haber participado en la huelga. (Le puede interesar: ["Hay un déficit de 300 controladores aéreos en Colombia"](https://archivo.contagioradio.com/controladores-aereos-inician-operacion-reglamento-en-colombia/))

Por esto, le han pedido a la Ministra de Trabajo y al Gobierno en general que **les brinde garantías** para proteger su derecho al trabajo. Indicaron que “se vendrán los despidos masivos y la criminalización del conflicto. Está en juego la estabilidad y futuro del sector aéreo y la integridad y vida de miles de tripulantes y de millones de pasajeros”.

Finalmente, indicaron que continuarán prestando sus servicios como aviadores en miras que contribuir con la seguridad de las operaciones aéreas y ACDAC mantendrá su agenda administrativa, jurídica, legislativa y gremial. “**Seguiremos trabajando por el fin de la discriminación por parte de Avianca Holding**, la defensa del derecho de asociación y de negociación colectiva, y el respeto por las Convenciones Colectivas en todas las aerolíneas”.

###### Reciba toda la información de Contagio Radio en [[su correo]

 
