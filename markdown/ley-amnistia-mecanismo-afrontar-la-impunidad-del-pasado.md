Title: Ley de Amnistía, un mecanismo para afrontar la impunidad del pasado
Date: 2016-12-27 20:55
Category: Entrevistas, Paz
Tags: Ejecuciones Extrajudiciales, implementación acuerdos de paz, Ley de Amnistia
Slug: ley-amnistia-mecanismo-afrontar-la-impunidad-del-pasado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/amnistía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Lalibertad] 

###### [27 Dic 2016] 

Frente a las inconsistencias de la ley de amnistía señaladas por Human Rights Watch, Diego Martínez secretario ejecutivo del Comité Permanente por la Defensa de los Derechos Humanos, señaló que la ley "**no es un mecanismo de impunidad sino un mecanismo para poder afrontar el pasado,** delitos como la sustracción de menores y ejecuciones extrajudiciales serán incorporados a la ley colombiana".

La HRW resaltó que “el proyecto de ley menciona las ‘ejecuciones extrajudiciales’, pero el derecho colombiano no tipifica ningún delito bajo este nombre (…) De manera similar, el proyecto se refiere a la ‘sustracción de menores’, aunque no existe el delito de ‘sustracción’ de personas en el Código Penal de Colombia”. Le puede interesar: [Preocupaciones de HRW sobre](https://archivo.contagioradio.com/ley-amnistia-colombia-paz-hrw/)ley de Amnistía.

Diego Martínez aseguró que el tema conceptual de los delitos de “ejecución extrajudicial” y “sustracción de menores” se resuelven en la próxima etapa que es la incorporación plena de estos contenidos por medio del acto legislativo.

Distintos sectores han manifestado su preocupación frente a una posible situación de impunidad, sobre ello Martínez indica que **“delitos como genocidio, crímenes de guerra o de lesa humanidad no pueden ser amnistiados** ni para integrantes de la guerrilla ni para agentes del Estado”.

Por otro lado, en respuesta a la misiva de la HRW, el Ministro de Defensa Jorge Londoño aseveró que “está absolutamente claro que **los guerrilleros que hayan cometido delitos de lesa humanidad irán a las zonas veredales** y lo propio pues es con los **agentes del Estado que no hayan cumplido cinco años, quienes irán a las guarniciones correspondientes”.**

Por último, el abogado Diego Martínez dijo que “la aprobación de la ley de amnistía permite que ese mismo concepto se amplíe y **haya lugar para el esclarecimiento y la justicia restaurativa.**

###### Reciba toda la información de Contagio Radio en [[su correo]
