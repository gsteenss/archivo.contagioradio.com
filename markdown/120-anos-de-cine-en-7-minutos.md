Title: 120 años de cine en 7 minutos
Date: 2015-12-28 11:47
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Cultura, Día mundial del cine
Slug: 120-anos-de-cine-en-7-minutos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/imagen11.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [28 Dic 2015]

120 años han pasado desde la primer vez que 33 espectadores pagaron 1 franco en la entrada para ver una película, ocasión que representaría el nacimiento del cine tal como lo conocemos hoy y que daría lugar a la conmemoración de su Día mundial.

El 28 de diciembre de 1895,  los hermanos Luis y Jean Lumiere, inventores del proyector cinematográfico, presentaron en el "Salon indien du Grand Café del Boulevard des Capucines" de París, la que sería su ópera prima y del cine "Los trabajadores saliendo de la fábrica", seguido por la célebre "Llegada de un tren a la estación de la Ciotat" y "El regador regado", primer corto de comedia con el jardinero Jean-François Clerc.

Aunque varias de las cintas habían sido presentadas con anterioridad en sociedades científicas, en la Universidad de la Sorbona y en Bruselas, entre otros lugares, y que el invento de los Lumiere había sido patentado diez meses atrás, fue sólo hasta ese día que la ciencia se convertiría en entretenimiento y el cine en una industria persistente en el tiempo.

A pesar que los Lumiere, por su poco interés en la explotación comercial de su invención, no detectaron del potencial que representaba, es gracias a su trabajo que a lo largo de un siglo millones de personas en todo el mundo han conocido historias maravillosas, algunas de las que aparecen resumidas en el siguiente video realizado por Joris Faucon Grimaud en su canal de youtube.

<iframe src="https://www.youtube.com/embed/i2QGCcrPnNk" width="660" height="415" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
