Title: Cinco razones para que EE.UU. apoye la consolidación de la paz según Wola
Date: 2017-02-01 16:05
Category: Nacional, Paz
Tags: Estados Unidos, paz, plan Colombia, WOLA
Slug: estas-son-5-razones-por-las-que-eeuu-debe-apoyar-a-consolidar-la-paz-segun-wola
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/paz-col.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [1 Feb. 2017] 

Esta semana el mundo está a la expectativa de la confirmación de Rex Tillerson como Secretario de Estado de Estados Unidos, designación que ha preocupado a organizaciones sociales como Wola, dado que Tillerson “revisaría los detalles del reciente acuerdo de paz de Colombia y que determinaría hasta qué punto los Estados Unidos debería seguir apoyándolo.”

Gimena Sánchez, integrante de Wola manifestó **que preocupa el interés de Tillerson por revisar la ayuda que puede dar Estados Unidos a Colombia** “es cierto que hay cosas que no han funcionado como el Plan Colombia, pero lo que sí ha funcionado es en mejorar la seguridad de Colombia y llegar a que se desmovilicen la más grande guerrilla con este acuerdo de paz” indicó. Le puede interesar: [Un futuro incierto para el 'Plan Paz Colombia'](https://archivo.contagioradio.com/los-verdaderos-alcances-del-plan-paz-colombia/)

**Wola da a conocer 5 razones por las cuáles Estados Unidos debe ayudar a consolidar la paz en Colombia.**

### 1.**Consolidar avances en seguridad requiere de una inversión continua** 

Según  Sánchez la primera razón por la cual se debe seguir ayudando  Colombia es porque aunque el acuerdo ya se firmó, ahora se está tramitando toda la legislación lo que se traduce en que “**este es el momento en el que Estados Unidos tiene que fortalecer más los mecanismos y donar más plata para asegurar que la paz se consolide en Colombia.** De lo contrario la situación puede ser muy frágil”.

### 2. **Se necesitan más esfuerzos integrados a largo plazo para abordar el tráfico de drogas** 

La segunda razón expuesta a través de una comunicación, es que **es necesario continuar aunando esfuerzos para que de esta manera, a largo plazo, el tráfico de drogas sea abordado y trabajado de manera eficaz** “hay que mirar a las regiones y ver las soluciones que tienen los campesinos más allá de la coca. Si eso no se da y sigue así de lento como hemos visto, tendremos un aumento de coca” puntualizó Gimena.

### 3. **La paz puede prevenir la proliferación de grupos armados ilegales** 

Para Wola, la tercera razón es que **seguir ayudando a Colombia puede contribuir a que la proliferación de grupos armados ilegales no aumente en el país** “Estados Unidos tiene un papel en asegurar que las conversaciones con el Ejército de Liberación Nacional (ELN) avancen y que las redes criminales y los actores neo-paramilitares en todo el país no llenen el vacío dejado por las FARC” comentó. Le puede interesar: [En EE.UU ven el paramilitarismo como un riesgo para el proceso de paz](https://archivo.contagioradio.com/en-ee-uu-ven-el-paramilitarismo-como-un-riesgo-para-el-proceso-de-paz/)

### 4. **Apoyar la paz en Colombia es una política bipartidista** 

Otro punto que destaca Gimena, es que al apoyar la paz en Colombia, Estados Unidos estaría mostrándole al mundo que los legisladores Demócratas y republicanos “siguen comprometidos en trabajar juntos para avanzar los objetivos de la política exterior de los Estados Unidos”.

### 5. **Apoyo de los Estados Unidos a poblaciones vulnerables es clave para profundizar la democracia colombiana** 

Por último, Gimena destaca que la quinta razón es que el apoyo de Estados Unidos a poblaciones vulnerables se convierte en un tema clave para poder **ayudar a Colombia en una transición que llega después del conflicto y que se traduce en la consolidación de una democracia** que redunde en "que no haya amenazas contra líderes y organizaciones sociales". Le puede interesar: [Las verdades ocultas del Plan Colombia](https://archivo.contagioradio.com/las-verdades-ocultas-del-plan-colombia/)

De igual modo, Gimena recalcó que en la actualidad están realizando un trabajo de dar información a todos los integrantes del Congreso “especialmente con los republicanos para entender mejor sus inquietudes y **ayudar a clarificar información errada que están recibiendo de otras fuentes como de los uribistas**, para asegurar que haya la información correcta y así tomar las decisiones”.

<iframe id="audio_16780776" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16780776_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
