Title: Así operan las estructuras paramilitares de Israel en medio de las protestas palestinas
Date: 2015-10-08 14:48
Category: DDHH, El mundo
Tags: Abdel al-Rahham Shadi Abdala Obeidallah, Cisjordania, Franja de Gaza, Hammas, Mahmud Abás, ONU, Palestina, Paramilitares en Israel, Plazoleta de las Mezquitas
Slug: asi-operan-las-estructuras-paramilitares-de-israel-en-medio-de-las-protestas-palestinas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/paramilitares_israel_paestina_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: revolutionnews 

###### [8 Oct 2015]

En un video publicado por el portal Revolution News, de la mañana de este jueves se observan los mecanismos de operación de las FFMM de Israel que infiltran a sus efectivos en las movilizaciones de palestinos. El video fue grabado por periodistas que cubrían la **movilización por el entierro de Abdel al-Rahham Shadi Abdala Obeidallah**, de 13 años, asesinado en Cisjordania hace 3 días.

En el video se puede observar como un grupo de hombres inicia una reacción a los disparos de las FFMM de Israel con piedras, varios de ellos, sin previo aviso se mueven hacia los soldados, se voltean, sacan de sus pantalones armas cortas y disparan contra quienes antes estaban a su lado. **Lo aterrador de la filmación radica también en la forma brutal y despiadada como son golpeados los jóvenes que no alcanzaron a huir de los infiltrados.**

\[embed\]https://www.youtube.com/watch?t=2&v=55Cpmy9xrSU\[/embed\]

También se observa como uno de los jóvenes capturados habría recibido disparos de quienes creía eran compañeros de indignación, es arrastrado por soldados que luego lo tiran al piso y lo **fotografían con su rostro ensangrentado y en estado de inconciencia por los disparos y los brutales golpes que recibe**.

La publicación que ha sido vista más cerca de 1 millón de veces y compartida por cerca de 30.000 personas es una evidencia más de las **estrategias de represión usadas por las FFMM de Israel** y que se repiten a diario en las calles de Belen, en los territorios ocupados ilegalemente por los colonos israelís.

Hace apenas una semana la bandera de Palestina ondeaba por primera vez en la sede de la Organización de las Naciones Unidas que mantiene su mirada pasiva ante las repetidas violaciones de DDHH de las que son víctimas los habitantes de Palestina tanto en la Franja de Gaza como en Cisjordania y los territorios ocupados ilegalemente por colonos de Israel.
