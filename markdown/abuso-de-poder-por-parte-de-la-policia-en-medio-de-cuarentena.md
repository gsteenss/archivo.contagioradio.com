Title: Denuncian abuso de poder de la Policía en medio de cuarentena
Date: 2020-04-07 15:23
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Fuerza Pública
Slug: abuso-de-poder-por-parte-de-la-policia-en-medio-de-cuarentena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/policia-alcaldia-1200x800-e1507224817473.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Desde hace tres semanas en Colombia se declaró la cuarentena obligatoria para la ciudadanía, colocando sanciones muy altas a quienes la incumplan. Sin embargo, se han viralizado vídeos de la ciudadanía en los que denuncian varias situaciones de abuso de poder por parte de la Policía**, imponiendo multas de forma arbitraria e intimidando a la población.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre los casos se encuentran la mujer que fue arrestada en Bogotá, en el Portal Tunal o la familia en Calí que fue intimidada por un policía que les apunto con su arma de dotación.

<!-- /wp:paragraph -->

<!-- wp:heading -->

"Me cobraron un a multa por intentar estudiar"
----------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esteban Velasquez es un estudiante de sociología que el pasado 4 de abril, tras salir a llevar a que arreglaran su computador y hacer mercado, fue sancionado con un comparendo por **32 salarios mínimos diarios, que equivaldrían a \$936,323**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según su relato, tras dejar su herramienta de estudio y trabajo en casa de un conocido y dirigirse a un supermercado, integrantes de la Policía metropolitana lo interceptaron, le pidieron papeles y a pesar de explicar el motivo para está en la calle recibió el comparendo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Yo debía llevar mi computador a arreglar porque es **mi única herramienta de trabajo para realizar mis estudios** y recibir las clases virtuales. Además vivo en una residencia universitaria y necesitaba hacer el mercado de víveres" señala Velásquez. (Le puede interesar: " [Policiales desconocen medidas comunitarias de prevención COVID19](https://www.justiciaypazcolombia.com/policiales-desconocen-medidas-comunitarias-de-prevencion-covid19/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, el estudiante afirma que esta acción por parte de los policías no solo fue desproporcional sino también irracional. "Tienen que tener en cuenta que **hay circunstancias de fuerza mayor por las cuales uno debe salir en este momento de excepción**. Yo en este momento ni siquiera cuento con el dinero del arriendo, no sé cómo pagaré la multa." asevera.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como Esteban otras personas manifiestan haber sido víctimas de multas injustificadas y excesivas por parte de la Policía en medio de la cuarentena. Todas ellas podrían estarce configurando como abusos de poder.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Los casos de violencia sexual cometidos por integrantes de la Policía
---------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

A estos hechos se suman las denuncias de mujeres que afirman ser víctimas de violencia sexual por parte de integrantes de la Policía. Entre los hechos se encuentra el testimonio de una mujer en la localidad de Bosa, en Bogotá. Ella asegura que fue obligada a **desnudarse en un bus oficial, mientras que otro policía le insinuó que tuvieran relaciones sexuales**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Cali, otra mujer fue víctima de acoso sexual, cuando una patrulla motorizada la hostigó mientras paseaba a su mascota durante los 20 minutos permitidos. Los dos hombres increparon a la mujer por estar sola en la calle. Cuando ella intentó alejarse de estos, **los hombres se abalanzaron sobre ella con la moto**. (Le puede interesar:["En cuarentena mujeres temen ser víctimas de violación por parte de la Policía"](https://archivo.contagioradio.com/en-medio-de-cuarentena-mujeres-temen-ser-victimas-de-violacion-por-parte-de-la-policia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con Andriana Benjumeda, directora de la Corporación Humanas, «es más grave que te viole un policía a que te viole un particular porque ese policía, **no solo no podía cometer ese delito, sino que tenía la obligación de cuidarte de que alguien más lo cometiera»** .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En caso de estar siendo víctima de abusos por parte de la Fuerza Pública puede contactarse a la línea 123. Las multas pueden ser apeladas en los siguientes tres días hábiles y puede contactarse con la campaña Defender la Libertad.

<!-- /wp:paragraph -->
