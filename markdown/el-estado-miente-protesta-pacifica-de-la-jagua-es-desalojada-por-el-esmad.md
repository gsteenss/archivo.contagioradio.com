Title: Protesta pacífica de La Jagua es desalojada por el ESMAD
Date: 2014-12-26 16:21
Author: CtgAdm
Category: Otra Mirada
Slug: el-estado-miente-protesta-pacifica-de-la-jagua-es-desalojada-por-el-esmad
Status: published

###### Foto: Juntando Alternativas 

<div class="dialog dialog_autosize ui-dialog-content ui-widget-content">

</div>

<div class="ui-resizable-handle ui-resizable-n">

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsce7SQD.mp3)

</div>

Integrantes de ASOQUIMBO denuncian que la tarde del viernes 21 de noviembre, en un Consejo de Seguridad realizado en el Municipio de Garzón, se dió a conocer la orden de desalojar a la comunidad que protestaba en La Jagua.

Este desalojo, asegura Juan Pablo Soler, no cumple con los requerimientos de la Ley. Por una parte, la Sentencia de la Corte especifica que el desalojo no podría llevarse a cabo hasta que la empresa EMGESA no llevara a cabo el censo requerido. Por otra parte, el desalojo no cuenta con la presencia del Alcalde ni la Procuraduría, el primero, quien firma la misma orden.

Las 300 personas concentradas llevaron a cabo un registro audiovisual de los hechos, para demostrar ante estaentos internacionales que el gobierno colombiano les incumple, y no genera los escenarios de diálogo que pacíficamente reclamaba la comunidad hace 17 días.

 
