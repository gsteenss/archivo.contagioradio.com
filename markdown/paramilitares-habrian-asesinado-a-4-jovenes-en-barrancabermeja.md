Title: Paramilitares habrían asesinado a 4 jóvenes en Barrancabermeja
Date: 2016-06-15 15:43
Category: DDHH, Nacional
Tags: Barrancabermeja, juventud rebelde
Slug: paramilitares-habrian-asesinado-a-4-jovenes-en-barrancabermeja
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Juventud-rebelde.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Juventud Rebelde 

###### [15 Jun 2016]

Juventud Rebelde-Magdalena Medio denuncia que **entre los días 11 y 13 de junio fueron asesinados cuatro jóvenes en la ciudad de Barrancabermeja.** Según denuncian, **estos crímenes habrían sido cometidos por estructuras paramilitares** de la zona que vienen amenazando y generando el desplazamiento de entre 20 y 30 jóvenes cada día.

Melkin Castrillón, secretario de Derechos Humanos de Juventud Rebelde, asegura que con el pretexto de la limpieza social, las estructuras paramilitares de la zona, han asesinado a estos jóvenes conocidos como  **Francisco Javier Medina Cañas, de 30 años, William Enrique López Gómez de 27 años, Jhonnys Matínez de 19 años y Óscar que al parecer tendría entre 20 y 27 años.**

Según el encargado de DDHH, los paramilitares son quienes reclutan a los jóvenes, los vuelven drogadictos, los utilizan para que atraigan a otras personas al consumo de drogas y luego los asesinan. Pero además, aseguran que también **miembros de Juventud Rebelde han sido asesinados por estos grupos, debido a su trabajo en la construcción de paz y las denuncias en torno a los vínculos entre estas estructuras y la policía y las fuerzas militares**.

De acuerdo con Castrillón, desde su organización se han hecho las respectivas denuncias sobre esta situación, sin embargo, la respuesta de las autoridades, específicamente del **alcalde y el comandante la policía ha sido negar la presencia paramilitar,** y no seguir con las investigaciones pertinentes para atacar la corrupción al interior del Ejército y la Policía, y lograr el desmonte del paramilitarismo.

“Como organización juvenil, le exigimos al gobierno nacional y local generar condiciones de protección y preservación de la vida de los jóvenes, su bienestar y su realización como personas. Exigimos el desmantelamiento de las estructuras paramilitares causantes de desplazamiento, asesinatos y amenazas hacia nuestros jóvenes. También le hacemos el llamado a la comunidad en general y a las diferentes organizaciones sociales para que cerremos filas ante el avance de los grupos paramilitares y no permitamos que estos actos atroces queden impunes”, dice la denuncia de Juventud Rebelde.

<iframe src="http://co.ivoox.com/es/player_ej_11914972_2_1.html?data=kpamk5mde5Ohhpywj5aWaZS1lZWah5yncZOhhpywj5WRaZi3jpWah5ynca7ZzdDW0JCnpdTo087Zzoqnd4a2lNOYj5CuudfZz9nixpC2qcPZzcnSj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
