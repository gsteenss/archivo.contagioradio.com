Title: Hace falta la decisión del Senado para que se prohíban las corridas de toros en Colombia
Date: 2018-03-22 14:11
Category: Animales, Voces de la Tierra
Tags: Cámara de Representantes, Congreso de la República, corridas de toros, prohibición de corridas de toros, Senado
Slug: hace-falta-la-decision-del-senado-para-que-prohiban-las-corridas-de-toros-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/toros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Cristian Garavito - El Espectador] 

###### [22 Mar 2018] 

[Luego de que en segundo debate la plenaria de la Cámara de Representantes aprobara el proyecto de ley que busca **prohibir las corridas de toros en el país,** los ambientalistas esperan que su trámite en el senado sea satisfactorio y se protejan los derechos de los animales. Al proyecto le quedan dos debates en el senado para convertirse en ley de la República.]

[De acuerdo con Natalia Parra, integrante de la plataforma que defiende los derechos de los animales, ALTO, este proyecto de ley **es uno de los intentos por terminar con las corridas de toros** desde el poder legislativo e indicó que por definición de la Corte Constitucional,  le otorga las facultades únicamente al Congreso como el único que puede tramitar esta clase de iniciativas]

### **El legislar es el único que puede decidir sobre la materia** 

[Recordó que fue la Corte la que manifestó que no tienen facultad para definir esta prohibición **“ni los alcaldes o las consultas populares”**. En esa medida, los animalistas han tratado por todas las vías posible detener las corridas de toros y con este proyecto esperan que se pueda frenar el sufrimiento de los animales. (Le puede interesar:["En manos del Congreso quedó la prohibición de las corridas de toros"](https://archivo.contagioradio.com/prohibicion_corridas_toros_congreso/))]

[Este es un proyecto que viene desde el ejecutivo pues ex vice ministro del interior, Luis Ernesto Gómez, presentó una iniciativa de ley para prohibir las corridas de toros con el apoyo de diferentes sectores del animalismo y tuvo un curso exitoso en la Cámara de Representantes. La propuesta** tuvo 75 votos a favor** y será el senado quien defina el futuro de las corridas de toros.]

### **Animalistas son conscientes de que aún faltan retos por superar**

### [Teniendo en cuenta el llamado que le hizo la Corte Constitucional al Congreso de la República para que legislara sobre el tema, Parra manifestó que los Congresistas están trabajando para que el **proyecto siga su curso.** Sin embargo, es consciente de que en el senado la iniciativa debe enfrentar varios retos.] 

[Entre ellos se encuentran las posiciones **de algunos senadores** que han manifestado estar a favor de las corridas de toros y las novilladas. Desde ALTO saben que en la Comisión Sexta del Senado se deberán enfrentar con la posición taurina de senadores como Álvaro Uribe Vélez. (Le puede interesar:["Centro Democrático busca volver patrimonio cultural las corridas de toros"](https://archivo.contagioradio.com/centro_democratico_corridas_toros/))]

[Por esto quieren que, cuando el debate llegue a la plenaria del Senado, el proyecto pueda continuar con su trámite por lo que le han pedido a la ciudadanía **que presione a los legisladores** para que lleven a cabo este debate. Parra indicó que es importante que el trámite se realice antes de que termine la legislatura en junio y así no afectar el resultado del mismo.]

<iframe id="audio_24746160" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24746160_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
