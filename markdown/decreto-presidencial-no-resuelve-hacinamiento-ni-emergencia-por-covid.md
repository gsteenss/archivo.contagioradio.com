Title: Decreto presidencial no resuelve  hacinamiento ni emergencia por COVID
Date: 2020-04-15 14:45
Author: CtgAdm
Category: Actualidad, DDHH
Tags: carceles, crisis, INPEC, Ministra de Justicia
Slug: decreto-presidencial-no-resuelve-hacinamiento-ni-emergencia-por-covid
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/crisis-carcelaria1-e1463780527863.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El decreto 546 expedido por el gobierno nacional, con el que se buscaba controlar la crisis del Covid 19 dentro de los centros de reclusión, solo beneficiaría al **5% de la población privada de la libertad**. Organizaciones defensoras de derechos humanos afirman que esta no es una medida efectiva y no atiende a los llamados del movimiento carcelario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La abogada Lorena Medina, defensora de derechos humanos, afirma que este decreto es **"nefasto"** porque beneficia a muy poca población reclusa. Estimada en cerca de 121.000 personas en 132 cárceles que solamente tienen una capacidad aproximada para albergar a 82.000.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además incluyen medidas que ya están consagradas en otros decretos, que tampoco se hacen efectivos producto de la ineficiencia del sistema judicial. (Le puede interesar:" [A las cárceles llega el covid 19 por omisión de gobierno Duque](https://archivo.contagioradio.com/a-las-carceles-llega-el-covid-19-por-omision-de-gobierno-duque/)")

<!-- /wp:paragraph -->

<!-- wp:heading -->

¿Para qué sirve este decreto?
-----------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El decreto 546 expresa que la población reclusa que podrá recibir una prisión domiciliaria son las **personas mayores de 60 años**, que según Medina, ya estaría indicada en la Ley 600 de 2000.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo podrán salir mujeres en estado de gestación, quienes hayan cumplido más del 40% de su condena y personas condenada a 5 años o menos. Es decir, que solo podría salir el **5% de la población, señala la defensora. Hecho que en cárceles que llegan al 48%** de hacinamiento representa una ínfima solución para evitar el contagio y la propagación del virus.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, el decreto tiene un artículo en donde especifica que personas condenadas por delitos sexuales, de lesa humanidad, cobijados por justicia transicional o imputados por perteneces a Grupos Armados delincuenciales no serán incluidos en esta medida.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Prisioneros políticos excluidos del decreto
-------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El abogado Uldarico Florez asegura que frente a quienes se acogieron al Acuerdo de La Habana, la Jurisdicción Especial para la Paz podría tomar medidas. Sin embargo, el tribunal manifestó que al ser medidas excepcionales y humanitarias son competencia del gobierno nacional. (Le puede interesar: «[Todos tenemos derechos, ellos también-2](https://www.justiciaypazcolombia.com/todos-tenemos-derechos-ellos-tambien-2/)«)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La defensora de derechos humanos Lorena Medina, asevera que en el decreto **"hay una estigmatización y se están vulnerando los derechos de los prisioneros políticos"**. Razón por la cual expone que las intensiones de ese mecanismo no son salvaguardar la vida de la población reclusa.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Ya hay 15 personas contagiados de Covid 19 en carcel de Villavicencio
---------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Brigada Jurídica Eduardo Umañana señala en un comunicado de prensa que este decreto será demandado. En primer lugar porque no es una medida que busque salvar la vida de las personas privadas de la libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, el decreto tampoco aporta elementos como lo indica la Comisión Interamericada de los Derechos Humanos, con excarcelaciones masivas, en procura de la protección de poblaciones vulnerables como la carcelaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En segundo lugar, tampoco se han especificado protocolos para la mitigación de la pandemia. Por ejemplo; en la cárcel de Villavicencio, en donde hay 15 personas infectadas, entre reclusos y guardias, no se están tomando medidas en contra del contagio, según denuncias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente el abogado afirma que tanto el Movimiento carcelario como la población civil, organizaciones defensoras de derechos humanos y la comunidad internacional deberán continuar en acciones de presión para evitar una tragedia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
