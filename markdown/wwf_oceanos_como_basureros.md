Title: "La humanidad ha hecho del océano un basurero": WWF
Date: 2017-09-08 18:18
Category: Ambiente, Voces de la Tierra
Tags: contaminación, Donald Trump, océanos, WWF
Slug: wwf_oceanos_como_basureros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/isladeplastico3-e1504912216701.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: greenarea.me] 

###### [8 Sep 2017] 

“El cambio climático y su impacto en el mar, la sobreexplotación de los recursos marinos y la contaminación de los océanos por desechos urbanos e industriales** han hecho del océano el basurero de la humanidad”,** es lo que ha señalado a EFE la presidenta del Fondo Mundial para la Naturaleza (WWF), Yolanda Kakabadse en el marco del Cuarto Congreso Internacional de Áreas Marinas Protegidas (IMPAC4), en Chile.

De esta forma es como Kakabadse alerta al mundo de que **“sin el océano no hay humanos”. **Asimismo, la presidenta ha señalado que la crítica situación de los océanos es responsabilidad de la sociedad, y por tanto precisa que es "Es imperativo reconocer que si no emprendemos acciones en estos momentos para recupera la salud del Océano, la humanidad seguirá en problemas".

Es esa medida desde la WWF se asegura que para enfrentar tal situación de crisis ambiental que viven los océanos es necesario que se fortalezcan **las buenas prácticas sobre el manejo y gestión de los ecosistemas marinos.** Además, resalta que se trata de una tarea que solo puede llevarse acabo una vez las naciones se unan para trabajar en favor de la naturaleza.

Y es que cabe recordar que de acuerdo con una investigación publicada en la revista «Science» con apoyo de la fundación de la navegadora Ellen MacArthur, para el 2050 los mares y océanos tendrán más desechos plásticos que peces. Ese informe señala que si el mundo continúa con el ritmo actual de consumo, en 10 años habrá** un residuo de plástico en el mar por cada pez, lo que significa que en el 2050 **el peso de los residuos plásticos será mayor al peso de todos los peces en el mundo.

### Sobre Donald Trump 

En una entrevista exclusiva que tuvo el medio Prensa Latina, la presidenta de la WWF se refirió también a las decisiones del presidente de Estados Unidos Donald Trump en contra del ambiente. Frente a ello asegura que "Definitivamente vamos a triunfar en esta cruzada. Trump se sale del Acuerdo de París sobre Cambio Climático y los grupos sociales reaccionan de otra manera".

Y agrega que "la sociedad norteamericana, el empresariado y gobiernos locales, respondieron no, nosotros no nos retiramos. Y al contrario, fortalecieron sus compromisos en la lucha del pacto del calentamiento global de la atmósfera".

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

 
