Title: Movimiento carcelario denuncia riesgo de contagio de Covid-19 por requisas de INPEC
Date: 2020-12-03 21:51
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Cárcel, #INPEC, #MovimientoCarcelario #Covid-19 #
Slug: movimiento-carcelario-denuncia-riesgo-de-contagio-de-covid-19-por-requisas-de-inpec
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/jhon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

En un reciente comunicado de prensa, el movimiento carcelario denunció un conjunto de operaciones de requisas por parte del Instituto Nacional Penitenciario y Carcelario (INPEC), que pondría en riesgo la vida de la población privada de la libertad, debido al posible contagio de Covid-19. Estos operativos **no se estarían realizando bajo medidas de bioseguridad**, ni respetando el distanciamiento para evitar la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En la misiva se indica que estos operativos fueron ordenados por el Ministerio de Justicia, en todas las cárceles del país. Dichas acciones las estarían realizando grupos del Gaula, CRI y el CTI. (Le puede interesar:["Denuncia Movimiento Carcelario"](file:///C:/Users/CONTAGIO%20RADIO/Downloads/DENUNCIA%20MNC.pdf))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Estas requisas no están garantizando el distanciamiento social, toda vez que vienen sacando a los presos de sus celdas y agrupándolos en espacios muy reducidos", afirman en el documento. Sumado a ello, denuncian que los guardias han tenido **contacto directo con las pertenencias directas de la población reclusa**.

<!-- /wp:paragraph -->

<!-- wp:heading -->

El contagio de Covid 19 en las cárceles
---------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El mayor temor de la población reclusa recae en los altos contagios que ya sufrieron, debido a que **17.000 personas padecieron Covid 19.** Un contexto que evidenció la ausencia total de atención por parte del Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, la primera ola de contagio fue provocada por el INPEC, ya que eran las únicas personas que ingresaron o salieron de las cárceles. Actualmente, y debido a que nunca se ordenó un acuartelamiento, continúan saliendo y entrando de estos lugares.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Las violaciones a DD.HH que vive la población privada de la libertad
--------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el comunicado, el movimiento carcelario asevera que durante 9 meses han sido privados del derecho a la visita, sin que se establezcan medidas para hacerla con protocolos de bioseguridad. **Igualmente se les estaría cobrando un IVA desde este 2 de diciembre**; desconociendo que al ser una población vulnerable son excluidos por ley del pago de este impuesto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De otro lado, los internos de la cárcel la Picota denunciaron la pésima calidad de comida que les servían, señalando un posible hecho de corrupción en la prestación del servicio de alimentación que hace el correspondiente contratista.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente recordaron la masacre perpetrada por guardias del INPEC el pasado 21 de marzo, exaltando la impunidad absoluta frente a los responsables de estas acciones que cobraron la vida de 24 personas y dejaron un saldo de 90 heridos. (Le puede interesar: "[Reclusos de cárcel La Modelo tienen miedo de denunciar masacre del 21 de marzo](https://archivo.contagioradio.com/reclusos-de-carcel-la-modelo-tienen-miedo-de-denunciar-masacre-del-21-de-marzo/)")

<!-- /wp:paragraph -->

<!-- wp:heading -->

Las exigencias del movimiento carcelario
----------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Frente a estas vulneraciones la población reclusa exige la reducción del 20% de sus condenas, como indemnización a los tratos crueles e inhumanos de los que han sido víctimas. Asimismo, solicitan que se normalice el ingreso de visitas bajo medidas de bioseguridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esa misma vía, están pidiendo al Congreso de la República que realice un debate de control político sobre el accionar de los funcionarios del INPEC y su accionar el 21 de marzo. Por último están convocando a todas las organizaciones sociales a que les acompañen en la lucha por garantizar sus derechos. (Le puede interesar: "[Todos tenemos derechos, ellos también](https://www.justiciaypazcolombia.com/todos-tenemos-derechos-ellos-tambien/)")

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
