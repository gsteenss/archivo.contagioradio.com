Title: Comunidades del Cauca realizarán Marcha de la Comida tras proceso de liberación de la madre tierra
Date: 2018-03-21 13:28
Category: Comunidad, Nacional
Tags: Cauca, Indigenas Nasa, liberación de la madre tierra, marcha de la comida
Slug: comunidades-del-cauca-realizaran-marcha-de-la-comida-tras-proceso-de-liberacion-de-la-madre-tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/AFICHE-FINAL-pño-1024x746-e1521654821946.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Proceso de liberación de la madre tierra] 

###### [21 Mar 2018] 

[Luego de haber realizado el proceso de liberación de la madre tierra, las comunidades del Cauca recogieron las cosechas de diferentes alimentos. A modo de celebración y como un proceso de dignificación, realizarán la Marcha de la Comida **hasta la ciudad de Cali en el Valle del Cauca** los días 23 y 24 de marzo. La comida será repartida a las comunidades más vulnerables de Cali.]

[De acuerdo con uno de los integrantes del proceso de liberación de la madre tierra en ese departamento, “las comunidades del pueblo indígena NASA **llevan tres años realizando la liberación de la madre tierra**”. Esto teniendo en cuenta que en el norte del Cauca, “la tierra está concentrada por pocos propietarios para el cultivo de caña y el agro combustible”.]

### **Proceso de liberación ha permitido la siembra de alimentos** 

[De acuerdo con el integrante de este proceso, la forma de tratar la tierra por parte de las empresas y latifundistas que actualmente la tienen, se convierte en una forma de esclavizar la vida y **“por lo tanto hay que liberarla”**. Durante ese proceso, la comunidad “ha cortado la caña y ha sembrado comida pero en ocasiones el Estado a dañado los cultivos de plátano, fríjol o yuca”.]

[Sin embargo, gracias a la persistencia de las comunidades, han logrado mantener las siembras **“que están produciendo alimentos, agua y aire”**. Razón por la cual la comida se convierte el fruto de la libertad, “a la comida la vamos a poner a marchar para que sea compartida y entregada a diferentes habitantes, comunidades y organizaciones de Cali”afirmó el integrante y ratificó que las cosechas, que han durado años produciendo alimentos, no se habría logrado si no se realiza el proceso de liberación.]

### **Indígenas recuperaron su territorio mediante la liberación de la madre tierra** 

[Desde los años 70 y con la construcción del Consejo Regional Indígena del Cauca, los indígenas empezaron a realizar el proceso de **recuperación de la madre tierra** como una medida para recuperar sus territorios y ampliar los resguardos indígenas. Es por esto que en 2005, los indígenas amplían el concepto para no sólo recuperar la tierra sino para liberarla de la producción agro industrial de la caña. (Le puede interesar:["Indígenas del Cauca lograron acuerdo con el Ministerio de Educación"](https://archivo.contagioradio.com/indigenas-del-cauca-logran-acuerdo-con-ministerio-de-educacion/))]

[Con esto en mente, inicia un proceso de liberación de la tierra por parte de los indígenas, quienes **nunca contaron con el apoyo del Estado colombiano**. Así que, las comunidades llevaron a cabo acciones para que les fuera devuelto el territorio teniendo en cuenta que el pueblo Nasa es su dueño legítimo.]

[Finalmente, los indígenas y las comunidades invitaron a las personas a que hagan parte de la Marcha de la Comida. En Cali, llegarán a la Universidad del Valle donde harán entrega de alimentos **a diferentes familias** para que continúe el ciclo de producción de alimentos. Presentarán de dónde viene la comida que están consumiendo los colombianos y explicarán cómo ]hacen las comunidades para resistir en la producción de alimentos.

<iframe id="audio_24725576" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24725576_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
