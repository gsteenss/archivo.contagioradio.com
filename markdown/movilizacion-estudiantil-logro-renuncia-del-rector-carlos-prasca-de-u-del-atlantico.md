Title: Movilización estudiantil logró renuncia del rector Carlos Prasca de U. del Atlántico
Date: 2019-11-01 17:28
Author: CtgAdm
Category: Estudiantes, Movilización
Tags: Carlos Prasca, estudiantes, paro, Universidad del Atlántico
Slug: movilizacion-estudiantil-logro-renuncia-del-rector-carlos-prasca-de-u-del-atlantico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Uni.-Atlántico.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Universidad.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Redes Sociales  
] 

El pasado 31 de octubre se vió en Barranquilla, Atlántico, una marcha concurrida como pocas se han visto en la ciudad, cientos de estudiantes se manifestaron contra las actuaciones violentas del ESMAD, la corrupción en las universidades públicas y pidiendo la renuncia del rector de la Universidad del Atlántico, Carlos Prasca. En medio de la marcha, se supo que Prasca había presentado su renuncia, no obstante, los jóvenes están analizando la situación para evaluar los siguientes pasos a seguir.

### Una marcha masiva que logra la renuncia de Carlos Prasca

Fabián Salcedo, estudiante de la Universidad del Atlántico afirmó que estaban felices por la convocatoria de la marcha porque contó con una asistencia masiva y logró el principal objetivo: Presionar por la salida de Prasca. Sin embargo, Salcedo explicó que respecto a sus renuncia hay distintas críticas, porque si bien es un logro, el que su salida no se produzca de inmediato puede derivar en que no salga de la rectoría y sea solo una forma de calmar la situación.

De acuerdo a Salcedo, Prasca dijo que renunciaría pero el Consejo Superior Universitaria le permitió que la renuncia se hiciera efectiva después de retornar de sus vacaciones. Por esa razón, los estudiantes convocaron para hoy a las 2 de la tarde un plantón frente a la Gobernación del Atlántico para pedir que la salida del Rector se produzca de inmediato. (Le puede interesar: ["¿Por qué pidió un descanso no remunerado Prasca, rector de la U. del Atlántico?"](https://archivo.contagioradio.com/carlos-prasca-rector/))

### El paro se mantiene por la búsqueda de democracia universitaria

El estudiante afirmó que la próxima semana se realizará una asamblea universitaria para definir nuevas actividades de movilización y otros eventos que les permitan visibilizar las problemáticas de la Universidad. Adicionalmente, Salcedo manifestó que el paro que inició hace una semana persistirá hasta que en la Institución haya una reforma al estatuto general que permita una asamblea constituyente para generar espacios de participación real de los estudiantes en las decisiones importantes.

De esta forma, los estudiantes buscan garantizar tener voz y voto en las próximas elecciones para la rectoría, así como poder ejercer un mayor control a las administraciones de la Universidad. Por su parte, los profesores respaldaron la movilización de los estudiantes, y pidieron al Consejo Superior Universitario aceptar de inmediato la renuncia de Carlos Prasca. (Le puede interesar: ["Universidad del Atlántico de nuevo bajo la mira del terror"](https://archivo.contagioradio.com/universidad-del-atlantico-mira-terror/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44241992" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44241992_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
