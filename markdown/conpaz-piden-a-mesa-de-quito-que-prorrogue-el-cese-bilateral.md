Title: Comunidades piden a mesa de Quito que prorrogue el cese bilateral
Date: 2017-12-22 16:48
Category: DDHH, Nacional, Paz
Tags: ELN, lideres sociales, Proceso de paz en Quito
Slug: conpaz-piden-a-mesa-de-quito-que-prorrogue-el-cese-bilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/CONPAZ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CONPAZ] 

###### [22 Dic 2017] 

A través de un comunicado de prensa las Comunidades construyendo paz en los territorios, **CONPAZ, le solicitaron tanto al ELN como al gobierno Nacional que prorroguen el cese bilateral** al fuego luego del 9 de enero, debido a que esto trae para las comunidades la seguridad y el ejercicio de la democracia en los territorios.

En la misiva de este xxxx, CONPAZ afirmó que darse el cese bilateral se podrían adelantar la exploración de desminado como ya ocurrió **“por expresión de acatamiento de la voluntad comunitaria”** del ELN en Jiguamiando, en Chocó, que debería extenderse a otros territorios del Bajo Atrato. (Le puede interesar: ["Los retos para los nuevos equipos en la mesa de diálogos entre gobierno y ELN"](https://archivo.contagioradio.com/gustavo-bell-y-antonio-garcia-nuevos-aires-para-la-mesa-en-quito/))

De igual forma en el texto expresan que la participación, que se ha manifestado desde un inicio de los diálogos como el centro del proceso, debe ser un mecanismo real para **construir apuestas de paz completa**, por lo tanto, uno de los primeros pasos debe ser que las comunidades estén incluidas en las agendas humanitarias del Bajo Atrato.

Así mismo señalaron que a medida que se avance en el proceso urge un acompañamiento de dinámicas humanitarias, razón por la cual afirmaron que **“sus vidas siguen en riesgo por la muerte con las balas, la exclusión social y ambiental”** mientras que la “justificación de los enemigos está justificando vehementemente la muerte de unos y de otros”.

Finalmente afirmaron que el asesinato de los líderes en el país, lleva las Comunidades construyendo Paz a continuar en la exigencia de la verdad, “**mucho más cuando se ofende a las víctimas señalando que sus vidas cegadas son por líos de faldas o de lindero**s” como lo manifestó el ministro de Defensa Luís Carlos Villegas.

###### Reciba toda la información de Contagio Radio en [[su correo]
