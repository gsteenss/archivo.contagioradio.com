Title: Congresistas de EE.UU. piden al gobierno Trump interceder por el acuerdo de paz en Colombia
Date: 2019-05-24 16:50
Author: CtgAdm
Category: Paz, Política
Tags: Congresistas, Estados Unidos
Slug: congresistas-ee-uu-piden-interceder-acuerdo-de-paz-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Congresistas-de-Estados-Unidos-e1558733904675.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Aciprensa] 

Cerca de **80 congresistas de EE.UU., integrantes de todos los partidos, enviaron este viernes una comunicación a Mike Pompeo, secretario de Estado del país**, para solicitar que el Acuerdo de Paz en Colombia sea aplicado por el gobierno de Iván Duque. En el documento los parlamentarios añaden que “la lenta implementación del acuerdo está afectando de forma significativa y adversa a las personas defensoras de derechos humanos y líderes y lideresas sociales que lideran la recuperación en zonas asoladas por la guerra”.

Según la comunicación, el recurrente asesinato de líderes sociales, la falta de desarrollo integral en las regiones y el “clima de incertidumbre” que posa sobre el apoyo al Sistema Integral de Verdad, Justicia, Reparación y No Repetición deben ser una prioridad para el Gobierno colombiano. En consecuencia, los líderes políticos recalcan que el Gobierno norteamericano apoye a su par, para que los objetivos del acuerdo se cumplan. (Le puede interesar: ["La multinacional Chiquita Brands podría afrontar juicio en Estados Unidos"](https://archivo.contagioradio.com/la-multinacional-chiquita-brands-podria-afrontar-juicio-en-estados-unidos/))

Sobre este tema, recomiendan al Secretario de Estado que **“comunique el apoyo inquebrantable de Estados Unidos a los tres mecanismos del sistema de justicia transicional** – la Jurisdicción Especial para la Paz (JEP), la Comisión para el Esclarecimiento de la Verdad (CEV) y la Unidad de Búsqueda de Personas dadas por Desaparecida (UBPD)”. (Le puede interesar: ["Comisión para el Esclarecimiento de la Verdad expresó su respaldo a lideres sociales de Cauca"](https://archivo.contagioradio.com/comision-de-la-verdad-expreso-su-respaldo-a-lideres-sociales-del-cauca/))

Respecto a los asesinatos de líderes sociales, los Congresistas de EE.UU. pidieron que se desarrollen los planes necesarios para proteger a personas defensoras de derechos humanos mediante la concertación de planes de protección, la puesta en acción (con “suficientes poderes”) de la Comisión Nacional de Garantías de Seguridad y el desmantelamiento de grupos sucesores del paramilitarismo y grupos de crimen organizado.

Para concluir, en la comunicación solicitaron a Pompeo que, “reconociendo que los intereses a largo plazo de los Estados Unidos son mejor logrados por medio de una Colombia estable y respetuosa de los derechos”, **inste al Gobierno colombiano a implementar de forma integral el Acuerdo de Paz de 2016**, así como los capítulos sobre desarrollo rural y cultivos de uso ilícito, cuyo efecto sería la reducción en la actividad de grupos armados, así como en la “producción ilícita de drogas”.

> Carta de congresistas demócratas pidiéndole a Mike Pompeo, Secretario de Estado de EEUU, para que su gobierno se pronuncie a favor de la implementación del Acuerdo de Paz en Colombia ¡Importantísimo! [pic.twitter.com/0n50I0vKEs](https://t.co/0n50I0vKEs)
>
> — Iván Marulanda (@ivanmarulanda) [24 de mayo de 2019](https://twitter.com/ivanmarulanda/status/1132005194244530176?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
