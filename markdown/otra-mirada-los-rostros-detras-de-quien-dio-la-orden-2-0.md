Title: Otra Mirada: Los rostros detrás de “¿Quién dio la orden? 2.0”
Date: 2020-10-03 19:07
Author: AdminContagio
Category: Nacional
Slug: otra-mirada-los-rostros-detras-de-quien-dio-la-orden-2-0
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Mural-Quien-dio-la-orden-.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado miércoles se realizó el lanzamiento del mural «¿Quién dio la orden? 2.0» por parte de organizaciones de víctimas pertenecientes a la Campaña por la Verdad. En este se reúnen más rostros de presuntos victimarios y responsables de ejecuciones extrajudiciales como Mario Montoya Uribe y Publio Hernán Mejía, a quienes las víctimas exigen se les expulse de la JEP, pues no han hecho aportes a la verdad. (Le puede interesar: [Se instaló el mural «¿Quién dio la orden? 2.0»](https://archivo.contagioradio.com/se-instalo-el-mural-quien-dio-la-orden-2-0/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el programa de Otra Mirada sobre los rostros que están detrás del nuevo mural y de las exigencias de las víctimas, estuvieron como invitados **Natalia Herrera, abogada de la Corporación Jurídica Yira Castro, Fernando Rodríguez, abogado del Comité de Solidaridad con los Presos Políticos, Luis Alfonso Castillo, abogado de la Corporación Jurídica Libertad.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el programa, los invitados explicaron por qué se le solicito a la Jurisdicción Especial para la Paz la apertura de incidente de verificación de Mario Montoya Uribe y Publio Hernán Mejía y cómo avanzan los casos de estos y los otros militares en la jurisdicción. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, explican qué mensaje se está enviando a través del nuevo muro y a quién va dirigido, y describen qué agrupa la Campaña por la Verdad, por qué surge y cuál es su objetivo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, comentan cómo afectan los procesos de las víctimas de conocer la verdad cuando los perpetradores no contribuyen aun cuando se comprometieron a hacerlo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, comparten cómo seguir el camino a la justicia que se basa en la esperanza de las víctimas por conocer la verdad. (Si desea escuchar el programa del 30 de septiembre: [Otra Mirada: Los riesgos de reclamar tierras en Colombia](https://www.facebook.com/contagioradio/videos/3171911616368687)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el análisis completo: [Haga click aquí](https://www.facebook.com/contagioradio/videos/4394601837281190)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
