Title: Videos
Date: 2014-12-16 15:31
Author: AdminContagio
Slug: videos
Status: published

VIDEOS
------

https://www.youtube.com/watch?v=xxvu2N\_2wxU

#### La Roja, la cerveza de la paz

En Icononzo, Tolima, un grupo de excombatientes le apuestan a la paz a través del proyecto productivo La Roja, una cerveza que nace como una propuesta ecónomica y de vida para hombres y mujeres que sueñan con un país donde se pueda dialogar y brindar desde la diferencia.

#### Festival De las Memorias | Alto Guayabal

El segundo Festival de las Memorias se realizó con las comunidades indígenas emberá de Jiguamiandó. El Festival Dayira Cricha (Nuestro Pensamiento desde el Corazón) en homenaje a todos los desaparecidos y desaparecidas se desarrollará en el Resguardo Ambiental Humanitario So Bia Drua, Jiguamiandó entre el viernes 17 de mayo y el domingo 19 de mayo.

https://www.youtube.com/watch?v=qQUr5NVd0gU  
https://www.youtube.com/watch?v=4J2lzRX74T0

#### Proyectos productivos en la Uribe, Meta

En San José de León, un grupo de aproximadamente 60 excombatientes junto a sus familias han formado un proyecto productivo con sus propios recursos buscando construir la paz que tanto necesita Colombia.

#### María Tila Uribe sobre las Masacre de las Bananeras

Durante más de 30 años María Tila ha investigado y recogido testimonios que la motivaron para escribir el libro “... les regalamos el minuto que falta” reflexiones sobre la masacre de las Bananeras que espera llevar a las nuevas generaciones para evitar que el sangriento capitulo en la historia de Colombia caiga en el olvido.

https://youtu.be/A1atGO1Le7w  
https://www.youtube.com/watch?v=A-2T0vrWlzA

#### Proyectos productivos en la Uribe, Meta

En el 2018 La Uribe (Meta) quedo en el tercer puesto de los municipios donde más se ha deforestado, producto de la ganadería extensiva en Colombia. Un grupo de excombatientes intentan cambiar esta realidad a través de un proyecto productivo que pueda mejorar sus vidas y la de las comunidades aledañas.

#### Ecoturismo para la paz

Ecoturismo para la paz: desde hace más de un año, luego de la firma del Acuerdo de Paz, un grupo de excombatientes trabaja arduamente para que los estigmas que se tiene sobre este municipio se transformen.

https://www.youtube.com/watch?v=KRRh01xRwA0  
https://www.youtube.com/watch?v=HmijBxj9KGQ

#### Conmemoración Francisco Montoya - Comunidad de Vida y Trabajo la Balsita

Luego de 21 años del crimen de Francisco Montoya, líder social, la Comunidad de Vida y Trabajo la Balsita, conmemoró esta fecha con actividades culturales y de memoria.

#### Casa de la memoria San Antonio, Inza

Luz Marina Cuchumbe y Basilio Pillimue, perdieron a sus hijos, ellos a través de esta casa de la memoria, trabajan por cumplir los sueños de sus seres queridos. 

https://www.youtube.com/watch?v=nuHaQmZcnyo  
https://youtu.be/TXajQCROOWk

#### Samuel David fruto de la paz, víctima de la guerra

Hombres armados acabaron con la vida de Samuel David, niño de siete meses de nacido, hijo de Carlos Enrique González, excombatiente de las FARC y Sandra Pushaina. 

#### ¡Defendamos la JEP, defendamos la paz!  

Salieron a la calle con un propósito, defender la JEP!

https://www.youtube.com/watch?v=5PxPPzE8aNchttps://www.youtube.com/watch?v=N-0K98lhwL4

#### ¿Qué hay detrás de las objeciones de Duque a la JEP?  

¿Qué se esconde detrás de las objeciones de Duque y qué sigue para el ley estatutaria de la JEP? Aquí se lo contamos.

#### ¡No están solos!  

Un grito por la vida de nuestros líderes sociales

https://www.youtube.com/watch?v=1CS4WtjJqds  
[  
Haz clic aquí  
](#)  
https://www.youtube.com/watch?v=3S2WIxTKWlo

#### Presentan a la JEP informe que vincula a comandante de la cúpula militar con falsos positivos  

Organizaciones de DD.HH. presentaron ante la Jurisdicción Especial para la Paz el informe ¿Qué futuro nos espera?, que recopila evidencia sobre la responsabilidad del Teniente Coronel Adolfo León Hernández, en 23 casos de ejecuciones extrajudiciales, ocurridas bajo su comandancia. Actualmente, el teniente está al mando del Comando de Transformación del Ejército.

#### Jóvenes de Curbaradó le cantan al colegio AFLICOC

Canción escrita y cantada por los estudiantes del colegio AFLICOC en la zona humanitaria Las Camelias en Curvaradó, Chocó.

https://www.youtube.com/watch?v=CGmhYhQTo\_c  
https://youtu.be/PQkw9ty\_wWY

#### 29 AÑOS ¿DÓNDE ESTÁN LOS DESAPARECIDOS?

Los días 6 y 7 de noviembre se conmemora la desaparición de 12 personas, durante la toma y la retoma del Palacio de Justicia en 1985. Los familiares nos cuentan cómo empezó la búsqueda conjunta de hermanas, hijas, madres y esposos; las conmemoraciones a lo largo de estos 29 años, y la búsqueda de verdad y justicia, en el marco de un nuevo proceso de paz en Colombia.

#### UN RETORNO EN LA ESPERANZA

Fue el 12 octubre un día de alegría para 20 familias de la vereda La madre Unión, Curvaradó, quienes retornan finalmente a sus tierras luego de estar  17 años fuera de sus tierras por causa del desplazamiento y la violencia.

https://youtu.be/s6lTJNdtl2A  
https://youtu.be/pFuzDKPxC44

#### CRÍMENES DE ESTADO DESDE LA MEMORIA DE LAS MUJERES

Delegadas de todos los capítulos regionales e integrantes del Movimiento Nacional de Víctimas de Crímenes de Estado (Movice) se reunirán en el Primer Encuentro Nacional de Mujeres del Movice, por la memoria y contra la impunidad, que se realizó en el Centro de Memoria, Paz y Reconciliación de Bogotá.

El Encuentro tuvo como objetivo conocer los hallazgos de la investigación Los crímenes de Estado desde la memoria de las mujeres y aportar recomendaciones, que generen acciones para fortalecer la perspectiva de género de las mujeres víctimas organizadas en el Movice.

#### ESTUDIANTES COLOMBIANOS SE MOVILIZAN CONTRA EL ACUERDO 2034  

El jueves 16 de octubre se llevaron a cabo movilizaciones en las principales ciudades del país, para rechazar el Acuerdo 2034, exigir el pago de 12,5 billones de pesos que adeuda el Estado a las universidades públicas, y garantías a la autonomía y democracia universitaria.

El Encuentro tuvo como objetivo conocer los hallazgos de la investigación Los crímenes de Estado desde la memoria de las mujeres y aportar recomendaciones, que generen acciones para fortalecer la perspectiva de género de las mujeres víctimas organizadas en el Movice.

https://youtu.be/JXnbs4Vk3i4
