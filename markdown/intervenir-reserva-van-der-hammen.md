Title: ¿Es conveniente intervenir la Reserva Van der Hammen?
Date: 2018-11-02 16:45
Author: AdminContagio
Category: Voces de la Tierra
Tags: Peñalosa, reserva Van der Hammen
Slug: intervenir-reserva-van-der-hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Diseño-sin-título-1-e1522792225586.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 25 Oct 2018 

La propuesta de la Alcaldía de Bogotá, con la que pretende sustraer, realinderar y recategorizar parte de la reserva Thomas Van der Hammen, vuelve poner en el debate público la viabilidad de intervenir este ecosistema protegido que abarca 1395 hectáreas en el noroccidente de la capital.

Para hablar de las afectaciones que tendría el proyecto de la administración Peñalosa, nos acompañó en Voces de la Tierra, María Mercedes Maldonado, académica y exsecretaria de planeación y hábitat de Bogotá, mientras que Diego Molano Aponte, Concejal de Bogotá por el Centro Democrático ex Director de Acción social, presenta los beneficios que traería la intervención.

<iframe id="audio_29801817" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29801817_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
