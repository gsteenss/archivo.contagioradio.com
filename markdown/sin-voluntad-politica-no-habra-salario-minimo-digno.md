Title: Sin voluntad política no habrá salario mínimo digno
Date: 2016-12-15 16:49
Category: Economía, Nacional
Tags: CUT, Ministerio de hacienda, Ministerio de Trabajo, Salario Mínimo en Colombia, SIndicatos
Slug: sin-voluntad-politica-no-habra-salario-minimo-digno
Status: published

###### [Foto: Contagio Radio] 

###### [15 Dic 2016] 

La Ministra de Trabajo Clara López y el Ministro de Hacienda Mauricio Cárdenas, dieron inicio desde el 12 de Diciembre a la Comisión Permanente de Concertación de Políticas Salariales y Laborales con los sindicatos de centrales obreras del país para **construir y negociar una agenda conjunta sobre el ajuste al salario mínimo para el 2017.**

Clara López Obregón, anunció que la próxima semana el Gobierno Nacional presentará una propuesta de salario mínimo tanto a los **representantes de las centrales obreras como a los empresarios con el propósito de buscar la concertación entre las partes.**

Uno de los integrantes de la Central Unitaria de Trabajadores –CUT–, aseguró que se han visto algunos avances en las negociaciones pero **“creemos que no hay totalmente una voluntad política,** en la reunión del 15 de Diciembre, Mauricio Cárdenas habló algo sobre la postura de su cartera y a los 10 minutos se retiró de la reunión, vimos eso como irrespetuoso”.

Por último, el integrante de la CUT manifestó que estarán trabajando en articular y fortalecer las propuestas de otros sindicatos y **“esperamos que la reunión de la próxima semana arroje resultados más concretos”. **Le puede interesar: [Gobierno se debe poner en los zapatos de los trabajadores y aumentar 14%](https://archivo.contagioradio.com/gobierno-se-debe-poner-en-los-zapatos-de-los-trabajadores-y-aumentar-14-salario-minimo/)salario mínimo.

###### Reciba toda la información de Contagio Radio en [[su correo]
