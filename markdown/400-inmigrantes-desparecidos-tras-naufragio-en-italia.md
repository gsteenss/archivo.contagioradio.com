Title: 400 inmigrantes desparecidos tras naufragio en Italia
Date: 2015-04-15 15:12
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: 400 inmigrantes desaparecidos naufragio Italia, Inmigración Italia, naufragio inmigrantes Italia, Save the children 5600 inmigrantes en 3 dias en Italia
Slug: 400-inmigrantes-desparecidos-tras-naufragio-en-italia
Status: published

###### Foto:Publimetro.com 

En las horas de la tarde de ayer 14 de Abril **naufragó una barcaza con más de 550 inmigrantes** ilegales procedentes de **Libia**, que intentaban llegar a **Italia**. Del total han **sobrevivido 150 y más de 400 continúan desaparecidos**.

Según la ONG **Save the Children** y los testimonios de los supervivientes de entre los desaparecidos "...hay muchos jóvenes y niños...", en total serían **317 niños que no iban acompañados de su padres.**

La organización también informó que en **tres días** (11, 12 y 13 de Abril)  han llegado a **aguas italianas 5.100 inmigrantes** que han sido interceptados en aguas de las islas de Lampedusa, Sicilia y las regiones del sur de Calabria y Puglia.

En total desde **el viernes pasado han llegado 8.500 inmigrantes**, todos en una situación de gravedad para su salud y en condiciones infrahumanas.

Todos los inmigrantes han sido realojados en los distintos Centros de Internamiento de Extranjeros (CIE) de todo el país.

La avalancha de extranjeros que intenta entrar a través de las **costas de Libia tiene que pagar altos precios a mafias establecidas** que los transportan en barcazas de extrema peligrosidad y hacinamiento. Además la situación en Libia es extremadamente grave debido a la violencia entre facciones y el creciente conflicto armado.

La organización no gubernamental afirmó que "...el creciente número de muertes en el Mediterráneo plantea, no sólo a Italia, sino a toda la Unión Europea y a sus miembros, el deber de responder con un dispositivo de búsqueda y rescate en el mar capaz de lidiar con esta situación....".

Por último la procedencia de los inmigrantes, casi en su totalidad subsahariano, hace que la inmigración adquiera un carácter económico pero también humanitario, debido a los conflictos armados que se están viviendo en la región. Esto implicaría que muchos de los inmigrantes serían potenciales solicitantes de asilo político y refugio.
