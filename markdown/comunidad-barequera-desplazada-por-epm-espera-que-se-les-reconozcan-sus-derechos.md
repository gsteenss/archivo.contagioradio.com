Title: Comunidad desalojada por HidroItuango se declara víctima de Megaproyectos
Date: 2015-03-30 22:10
Author: CtgAdm
Category: DDHH, Otra Mirada
Tags: Antioquia, DDHH, Derechos Humanos, desalojo forzoso, desplzamiento, Empresas Públicas de Medellín, EPM, ESMAD, Hidroelectrica, Hidroituango, Medellin, Movimiento Ríos Vivos
Slug: comunidad-barequera-desplazada-por-epm-espera-que-se-les-reconozcan-sus-derechos
Status: published

##### Movimiento Ríos Vivos 

<iframe src="http://www.ivoox.com/player_ek_4285975_2_1.html?data=lZell56beY6ZmKiakp2Jd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibHBjN6Yp7ixhaWfxcrgw9HTrsLm0NOYyNTWvsLYwtLS0NnJb8Kfw8bfx9bZqdPj1JCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Humberto Pino, vocero de la comunidad barequera ] 

Luego de que EPM (Empresas públicas de Medellín) y el ESMAD, desplazaran forzadamente a 81 personas, por la hidroeléctrica Hidrituango, en la Playa La Arenera, al Norte de Antioquia, ahora, la comunidad barequera, espera ayudas humanitarias, pero sobre todo una respuesta por parte del gobierno y la empresa, **para que no se les siga revictimizando.**

Fue el pasado viernes 27 de marzo, cuando los habitantes de esa zona se despertaron rodeados de agentes del ESMAD, quienes desalojaron a los pobladores, que llegaron a las dos de la tarde al Puente Pescador, allí, EPM y la policía se habían comprometido a elaborar una acta en compañía de la comunidad sobre el desalojo, sin embargo, de acuerdo a Humberto Pino, vocero de los barequeros “**no se logró nada, porque querían hacer el acta de acuerdo ellos y no a la comunidad”.**

**“No estábamos pidiendo limosna sino que se nos reconocieran nuestros derechos** y se nos restableciera nuestra forma de vida”, dice Pino, quien añade que la comunidad decidió no ir al campamento de EPM, ya que sería contradictorio para ellos, pero sobre todo humillante, dada la situación de revictimización por la que están pasando.

Este lunes, la comunidad barequera se reunió para realizar la declaración de desplazamiento forzoso por parte de Empresas Públicas de Medellín y la fuerza pública, y esta será entregada al personero de Ituango quien está en la obligación de recibirla.

Por el momento, las 81 personas afectadas han recibido **ayudas de parte de la alcaldía municipal** y se están refugiando en casas de personas conocidas de Ituango.

Así mismo, se denuncia que los funcionarios y la policía que realizaron el desalojo, le prometían a los niños que **les daban juguetes y hasta "otros papas",** con el objetivo de que los hijos de las familias les fueran arrebatados.

##### **Declaraciones de los niños y niñas víctimas del desalojo ** 

<iframe src="http://www.ivoox.com/player_ek_4286182_2_1.html?data=lZelmJacdo6ZmKiakpWJd6Kmk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibHBjN6Yp7ixhaWfxcrgw9HTrsLijMaYxdTRuc%2FdxcbRjcfFtsbl1srfw5CWcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Vea también** [https://archivo.contagioradio.com/nuevamente-epm-y-esmad-realizan-desalojo-forzoso-por-hidroituango/]
