Title: Los retos humanitarios para 2018 según el CICR
Date: 2018-03-01 14:04
Category: DDHH, Nacional
Tags: balance humanitario, CICR, Comité Internacional de la Cruz Roja, situación humanitaria, Unidad de Búsqueda de Personas Desaparecidas
Slug: los-retos-humanitarios-para-2018-segun-el-cicr
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/DXOMDQ-X0AEbkjX-e1519931043296.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CICR Colombia] 

###### [01 Mar 2018] 

La delegación del Comité Internacional de la Cruz Roja en Colombia presentó el balance sobre los retos en atención humanitaria del país para el 2018 a la vez que realizó llamados a la acción de manera puntual. **El retraso en la implementación del Acuerdo de Paz,** la persistencia de la guerra en algunos territorios, la puesta en marcha de la Unidad de Búsqueda de personas desaparecidas y la crisis humanitaria en las cárceles son algunas de las preocupaciones que plantea el organismo.

En lo que tiene que ver con la implementación del Acuerdo de Paz, el Comité indicó que aún hace falta compromiso y voluntad política para que se cumpla con lo que se acordó en la Habana. Manifestó que **“las víctimas merecen mucho más de lo que hemos visto ahora”** y hay una preocupación especial por las poblaciones más apartadas donde el Estado no ha hecho presencia con oportunidades de desarrollo y servicio básicos.

### **Por primera vez el CICR realizó llamados a la acción** 

El CICR hizo un llamado al Gobierno Nacional para que se prioricen los programas políticos que le **den respuesta humanitaria a las víctimas del conflicto armado**. Indicó que es necesario que los dirigentes políticos de los próximos años se comprometan a respetar “las dimensiones humanitarias del Acuerdo de Paz y a garantizar los recursos para su implementación”.

Además, afirmó que “es imperativo que en los acercamientos entre el Gobierno y el ELN se **adopten acuerdos humanitarios** que tengan un impacto real en la vida de las personas que más sufren por el conflicto” y reiteraron el llamado a la Fuerza Pública y todos los grupos armados para respetar el Derecho Internacional Humanitario. (Le puede interesar:["Luz verde para funcionamiento de la Unidad de Búsqueda de Personas Desaparecidas"](https://archivo.contagioradio.com/luz-verde-para-funcionamiento-de-la-unidad-de-busqueda-de-desaparecidos/))

Recalcaron la obligación del Estado colombiano para **limitar el uso de la fuerza** y que asegure la presencia estable en las regiones más apartadas del país “con énfasis en la reconstrucción social” en la medida en que no se puede restringir al control territorial o policial. Ratificó que es necesario que el Estado responda con rapidez a las necesidades de las comunidades que “se ven afectadas por nuevos hechos de violencia armada, incluidas las zonas más vulnerables de los centros urbanos”.

### **El conflicto armado aún continua en Colombia** 

Si bien el Comité reconoce la importancia de la disminución de las acciones armadas en varias regiones del país, en 2017 dieron seguimiento a 550 casos de violaciones a los derechos humanos por lo que“aún queda un camino largo y complejo para que podamos hablar de una **Colombia que ha superado la guerra**”. Enfatizó en que el conflicto armado entre diferentes grupos continúa en departamentos como Chocó, Nariño, Norte de Santander, Cuca, Guaviare, Antioquia, Arauca y Caquetá.

Los grupos armados a los que hace referencia el balance son el **EPL, las AGC, el ELN** y las estructuras de las FARC del antiguo Bloque Oriental que no se acogieron al proceso de paz. Todo esto se suma a las expectativas que hay en el país por lo que ocurra con el proceso de negociación con el ELN en Quito, Ecuador y las múltiples necesidades que requieren las víctimas pues aún persisten los casos de desaparición, amenazas, homicidios selectivos, violencia sexual y los desplazamientos masivos.

### **Es urgente la puesta en marcha de la Unidad de Búsqueda de Personas Desaparecidas** 

Una de las mayores preocupaciones del Comité es “el drama de las desapariciones”. Frente a esto, uno de los grandes retos es la puesta en marcha de manera efectiva de la Unidad de Búsqueda de Personas Desaparecidas que tiene carácter extrajudicial y humanitario. Según cifras, “entre 2015 y 2017 se registró un aumento del **133%** en los casos de desaparición abiertos por el CICR”.

Además, de las 91.187 personas que continúan desaparecidas en el país, cifra que maneja el Comité desde 1970 y el 17 de enero de 2018, **26.608 fueron desapariciones forzadas** y sólo en 2017 el Comité recibió 748 peticiones para “orientar, apoyar o indagar sobre personas desaparecidas”. A pesar de esto, el organismo logró que 271 personas recibieran noticias sobre sus familiares donde 133 establecieron contacto con ellas y “138 fueron notificadas de la muerte de su ser querido”.

Christopher Harnisch, jefe de la delegación del CICR para Colombia, le dijo a Contagio Radio que se necesita una **base legal clara que reconozca el carácter extrajudicial** y humanitario de la Unidad de Búsqueda de Personas Desaparecidas que ya cuenta con el visto bueno para empezar a trabajar. Dijo que es necesario que los familiares de los desaparecidos participen en este proceso de manera activa y que la Unidad tenga un apoyo político y económico de manera constante.

Clarificó que las familias deben tener las garantías necesarias para **contar que sus familiares están desaparecidos** en la medida en que “en las zonas donde hay presencia de grupos armados las personas tienen miedo de contar lo que les ha pasado”. Por esto, tanto las organizaciones como el Estado deben generar las condiciones de confianza que requieren seguimiento y respuestas para que los familiares estén seguros de que van a encontrar a sus seres queridos. (Le puede interesar:["Las recomendaciones de las víctimas para la Unidad de Búsqueda de Desaparecidos"](https://archivo.contagioradio.com/las-recomendaciones-de-las-victimas-para-el-funcionamiento-eficaz-de-la-uniddad-de-busqueda-de-desparecidos/))

### **Colombia está fallando en la atención de la crisis carcelaria** 

Otra preocupación del Comité es la crisis carcelaria que se vive en el país. Por un lado, “hay un consenso entre las instituciones colombianas en considerar que la política criminal del país es **incoherente e ineficaz**” por lo que “la Corte Constitucional ha hecho hincapié en su aspecto excesivamente punitivo y en que las condiciones actuales violan la dignidad humana y los derechos de los detenidos”.

De acuerdo con las cifras del INPEC, hay un hacinamiento del **46% en las cárceles** del país por lo que el CICR indicó que hay una sobrepoblación de más de 36.400 personas. Esto, además de la ineficiente política criminal, genera problemas humanitarios como la deficiente atención médica y la falta de condiciones sanitarias que acelera la propagación de enfermedades.

Como lo recalca en el balance, las instituciones han venido tratando de reducir los delitos utilizando equívocamente **el uso de la privación de la libertad** y “no existe evidencia que demuestre que un endurecimiento de las penas haya mejorado los índices de seguridad ciudadana”. Con esto en mente, el Comité en Colombia logró que 1.280 detenidos obtuvieran mejoras en sus condiciones de saneamiento y salud al igual que en las visitas de sus familiares.

<iframe src="https://www.youtube.com/embed/rhodDj-dBh4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
