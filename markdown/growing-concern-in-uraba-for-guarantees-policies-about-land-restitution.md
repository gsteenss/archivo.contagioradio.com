Title: Growing concern in Urabá for guarantees policies about land restitution
Date: 2019-04-30 15:24
Author: CtgAdm
Category: English
Tags: Land Restitution
Slug: growing-concern-in-uraba-for-guarantees-policies-about-land-restitution
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Restitucion-de-Tierras-Urabá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

The National Police would be stop the accompaniment of Commission on Restitution of Land Rights in the sub-region of Urabá after the attack suffered by a police unit causing six injured on 11^th^  April. This has stated by six human rights organizations, who represent **21 rural families** who hoped to return to the village of Guacamayas. They have been displaced by the United Self-Defense Forces of Colombia between 1996 and 1997 and from that moment they couldn’t  no longer return to their territories. According to Gerardo Vega, director of the Forjando Futuros Foundation, the return of 1,000 hectares is scheduled for May 14^th^ , 15^th^  and 16^th^ , in compliance with the Constitutional Court that not succeeded in 2017. However, the National Police sent an official letter last week in which express concern about this process and the difficulties implied.

The police officers announced to the judges working in the Commission on Restitution in Urabá that they would cancel the security measures provided due to the uncertain events occurred. These measures would be evaluated in a meeting on **the next 3^rd^ May** by the Ministry of the Interior to determine if they would continue.

These changes occurred after a unit was attacked in a blitz offensive on April 11^th^  by members of illegal armed groups while the police was providing protection to the rural population during a study of a property. "If the police can not comply, the main issue is: **who is  going to be in charge for citizen’s security in the Urabá region?**" Asked Vega, during a press conference . For its part, the Urabá Police Department denied the denunciation of the six organizations and assured that is going to maintain the accompaniment of land restitution in the municipalities located between the departments of Antioquia and Chocó.

\[caption id="attachment\_66056" align="alignleft" width="238"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-29-at-11.56.40-AM-225x300.jpeg){.wp-image-66056 width="238" height="317"} Restitución de tierras\[/caption\]

Vega affirmed that paramilitary groups which are operating in the area, are serving landowners who seek to impede this restitution. Only 163 of 6,824 land claims have been resolved since 2011 that is only 4%. In addition, **20 local peasant have been killed since 2008**. Only in two of these cases, the Prosecutor's Office arrested the perpetrators while the others have remained unpunished.

 

 

 

 

 

For this reason, Nely Osorno president of the “Instituto Popular de Capacitación” (an institution of rural promotion linked with several sectors of society which carries out research, training, public education, advisory, consulting, accompaniment) said that the “insecurity issue”has been used as an excuse for not advancing the delivery of these lands. "These processes remain paralyzed until the law expires," recalled the Land Restitution Unit  which has short time (until 2021) to deliver these two farms to these peasant families.

 
