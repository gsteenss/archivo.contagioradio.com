Title: 7 datos que evidencian el deterioro del planeta tierra
Date: 2017-04-22 10:36
Category: Ambiente, Voces de la Tierra
Tags: cambio climatico, Día de la tierra, planeta tierra
Slug: 7-datos-evidencian-deterioro-del-planeta-tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/planeta-tierra-e1492875332583.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ChristianChan/iStock 

###### [22 Abr 2017]

El Día de la Tierra fue declarado en 1970 por las Naciones Unidas, reconociendo que el planeta y sus ecosistemas son el hogar de la humanidad y de miles de especies de fauna y flora. Un día en el que se busca llamar la atención de los gobiernos y de los ciudadanos del mundo sobre la importancia de conseguir un equilibrio entre las necesidades económicas, sociales y ambientales de las generaciones presentes y futuras. Sin embargo, aunque se visibilizan esfuerzos por parte de algunos sectores, las actividades humanas siguen siendo las causantes de la actual situación en riesgo en la que se encuentra el planeta. Para evidenciar esa problemática en Contagio Radio recogimos 8 datos claves sobre las condiciones actuales del planeta.

### 1.Pérdida de la vida silvestre 

El informe, “**Los años más cálidos de la historia. ¿Por qué se calienta nuestro planeta?”, realizado en Madrid** evidencia **entre 1970 y 2012, se produjo una disminución del 58% en las poblaciones de vertebrados, peces, aves, mamíferos, reptiles y anfibios, en todo el mundo. **De seguir con la tendencia actual "se calcula que en cuatro años, viviremos la desaparición de dos tercios de la vida silvestre en el mundo". Por su arte, Latinoamérica en los últimos 40 años  perdió el 83% de las poblaciones de peces, aves, mamíferos, anfibios y reptiles hechos que deben preocupar no solo a los ecologistas, pues dichas especies garantizan el tejido de los ecosistemas que sustentan la vida humana en la Tierra.

### 2. Las polinizadoras están extinguiéndose 

Las abejas son ahora una especie en peligro de extinción, y sin estas polinizadoras, la vida en el plantea también podría extinguirse. Diversos estudios han evidenciado que los monocultivos y los pesticidas serían los principales causantes de esta situación, pese a que de las **100 especies de vegetales que proveen el 90% de los alimentos en 146 países, 71 son polinizadas por abejas,** como lo señala la Organización de las Naciones Unidas para la Agricultura y la Alimentación, FAO.

### 3. el 50%  de los arrecifes de coral han desaparecido 

La WWF advierte que el planeta  ha perdido casi la mitad de sus arrecifes de coral en los últimos 30 años. La situación se ha dejado avanzar a tal punto que aunque se lograra frenar el calentamiento global,** los científicos creen que  más del 90% de los corales morirán antes del 2050.**

### 4. El planeta ha perdido un 64 % de sus humedales 

La Convención de Ramsar, ha llamado la atención a 168 países, debido a que en los últimos **100 años **el planeta ha perdido un** 64 % de sus humedales**, y además la población de especies de estos ecosistemas **se ha reducido un 66%.** Su degradación, además, avanza a un ritmo del 1,5% anual, arrasando miles de especies que se nutren de su entorno.

### 5. El cambio climático afecta el 80%  las actividades de la tierra 

**Una** investigación conjunta de la Universidad de Florida, el Centro ARC de Excelencia para Estudios de Arrecifes de Coral y la Universidad de Queensland, de Australia, que concluye que en el 80% de los casos estudiados, el calentamiento global ha provocado cambios. Se trata de un análisis realizado por científicos de 10 países, que indican que **las pulgas de agua, las salamandras, las panteras, pingüinos y aves canadienses,** además de otras[ especies de fauna](https://archivo.contagioradio.com/la-tercera-parte-de-las-especies-animales-en-el-mundo-se-extinguiran-por-el-cambio-climatico/) y flora cambiaron su tamaño, color o población por las altas temperaturas que hoy padece la tierra.

### 6. El impacto de la agroindustria 

De acuerdo con la FAO Tras varias décadas de industrialización desenfrenada de la agricultura la evidencia acerca de los impactos ambientales que genera es abrumadora. La agricultura industrial es responsable del **70% de la deforestación en América Latina.** Frente a esta situación, amigos de la Tierra publica un informe en el que demuestra que, tras cuatro décadas de pruebas científicas, la agricultura agroecológica es la respuesta más efectiva para combatir los retos ambientales que amenazan al abastecimiento de alimentos: el cambio climático, la erosión del suelo, la escasez de agua o la pérdida de biodiversidad.

### 7. Cifras record en el deshielo del polo norte 

El Ártico ha vuelto a batir un récord. Este 2017,  se registró la **mayor pérdida de hielo** **de la historia.** El total de glaciar marino solo ocupó una superficie de 14 millones de kilómetros cuadrados, en su **momento más álgido.** Es decir que ha registrado un deshielo equivalente a un área casi igual de grande a Bolivia, estableciendo un máximo récord por tercer año consecutivo, así lo informó el Centro Nacional de Datos sobre Hielo y Nieve de Estados Unidos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
