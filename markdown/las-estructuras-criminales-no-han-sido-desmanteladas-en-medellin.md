Title: Las estructuras criminales no han sido desmanteladas en Medellín
Date: 2015-11-05 18:22
Category: DDHH, Nacional
Tags: CORPADES, Los chata, Los Pacheli, Oficina del doce de octubre, Paramilitarismo, Urabeños, Violencia en Medellín
Slug: las-estructuras-criminales-no-han-sido-desmanteladas-en-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/laS2ORILLAS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:las2orillas.com 

<iframe src="http://www.ivoox.com/player_ek_9273499_2_1.html?data=mpeklZmdfY6ZmKiak5mJd6Kkl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmsrjzcrbxc7Fb8bijLLSxsrQsIa3lIquptOPusKfxtOYw9rRqc%2Fo0JDQ0dPYtsLmytSYw5DIqcTgwteah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Fernando Quijano, CORPADES 

###### [3 de noviembre 2015] 

Contrario a las declaraciones institucionales en las que se  presenta una reducción del 26% en los índices de violencia registrados en la ciudad de Medellín, gracias a la acción de las fuerzas policíales, para Fernando Quijano, director de CORPADES, la realidad que vive la capital antioqueña es muy diferente.

Quijano argumenta que las versiones presentadas por parte, entre otros, del mayor general Mendoza Guzmán, deben ser tema de aclaración ya que en distintas zonas de la ciudad, **las desapariciones, las guerras entre combos, los asesinatos, vacunas, control territorial de las bandas, injerencia de los urabeños en los barrios y otras acciones** continúan vigentes hasta el día de hoy.

Frente al desmonte de organizaciones armadas ilegales, el defensor de Derechos Humanos asegura que “**aquí no se desmantelan bandas**” y solo se realiza “la captura, decomiso y control” a las mismas, por lo que hay una visión “muy positiva de la institucionalidad, pero la realidad de varios corregimientos muestran otra realidad”.

De acuerdo con investigaciones adelantadas, asegura Quijano que se han encontrado 3 bandas muy poderosas “**articuladas a los urabeños**” como son “**los Chata**”, que están metidos en la guerra con Barbosa quienes tienen “toda la hegemonía del poder militar” del Valle de Aburrá, que siguen amparados por un sector de la institucionalidad con protección especial.

Otra de las bandas se conoce con el nombre de "**Los Pacheli" **que tienen incidencia en Bello y que participaron “indirectamente en las elecciones que se dieron recientemente en el municipio de Bello”, ellos están inmiscuidos con el tema del tráfico de drogas, prostitución, explotación infantil y tienen laboratorios de drogas.

Por otro lado está la “**Oficina del doce de octubre”** al mando de alias ”Cataño” quienes son “amos absolutos” del Nuevo Jerusalén, organización que no fue desmantelada por estos generales y que “a todos sus habitantes, que son más de 5000 familias le están cobrado \$32.000 mensuales por agua y luz”.

Así funcionan tres estructuras criminales y las autoridades no hacen nada e incluso “**las tres pagan una parte a un sector de la policía, a un sector de la institucionalidad**”, por lo cual se han presentado capturas de funcionarios y miembros del cuerpo oficial en Bello y en muchos sectores del Valle de Aburrá según denuncia Quijano, situación que espera el nuevo alcalde  asuma y “ponga a funcionar a la fuerza pública”con “respaldo institucional” y "jefes comprometidos".
