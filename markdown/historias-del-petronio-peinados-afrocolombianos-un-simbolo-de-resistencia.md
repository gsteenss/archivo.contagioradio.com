Title: Historias del Petronio: Peinados afrocolombianos, un símbolo de resistencia
Date: 2019-08-17 12:00
Author: CtgAdm
Category: Cultura
Tags: Cultura, Estética Afro, Petronio Álvarez
Slug: historias-del-petronio-peinados-afrocolombianos-un-simbolo-de-resistencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Emilia-Petronio.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Petronio-Peinados.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

La cultura del Petronio Álvarez no solo tiene sus cimientos en la música y la danza, una parte vital de este festival también lo son la estética y los múltiples estilos que hacen parte de los peinados afro, de los que desde su experticia en el tema, **Emilia Eneyda Valencia**, investigadora, presidenta de la Asociación de Mujeres Afrocolombianas (Amafrocol) y fundadora del encuentro de peinadoras 'Tejiendo Esperanzas', resalta como una forma de resistencia desde sus raíces afrícanas.

Pese a que en décadas pasadas la mayoría de mujeres negras usaban un estilo colonial, adoptando el peinado de las mujeres blancas,  Emilia entró en contacto con la estética afro después de ver a unas atletas kenianas a través de la televisión llamando su atención de inmediato, y aunque en su casa y en general alrededor suyo, nadie sabía cómo realizar esos peinados, finalmente y gracias a una de sus amigas, "en cinco minutos, a la orilla del río Condoto", aprendió, recuerda la docente.

Su conocimiento, sumado a su talento, le permitió a Emilia continuar sus estudios repartiendo su tiempo entre la universidad, un trabajo de contabilidad los días sábado, y las citas agendadas con todas aquellas mujeres que le pedían, adornar su cabello.

### Apuesta política en el Petronio

Para Emilia, el cabello afro y los peinados de origen africano tienen una fuerte sentir político, pues aunque en África este tipo de trenzado era muy común, al llegar a América se convierte en un símbolo de resistencia para quienes se negaban a que les fuera impuesta la cultura occidental, algo que adquirió más fuerza con el Movimiento por los derechos civiles de Estados Unidos, **"reinvindicando la bebelleza de la estética de la gente negra".**

 

\[caption id="attachment\_72291" align="aligncenter" width="1024"\]![Petronio Emilia Eneyda Valencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Emilia-Petronio-1024x575.jpg){.size-large .wp-image-72291 width="1024" height="575"} Foto: Contagio Radio\[/caption\]

El cabello afro, no es solo un símbolo político, también es una muestra de la fuerza de la mujer, pues anteriormente en África, relata Emilia, existieron numerosos matriarcados que se vieron reducidos con la influencia europea que cambió la dinámica  que ya traía el pueblo africano.

Esta mujer, referente de los procesos afro, afirma que el auge de la cultura negra, es una oportunidad para deconstruir imaginarios a través de una tradición africana que data de milenios atrás y que ahora se comparte con toda la sociedad. [(Le puede interesar: Historias del  Petronio: Próceres del Pacífico)](https://archivo.contagioradio.com/historias-del-petronio-proceres-del-pacifico/)

Y aunque resalta que no es molesto que otras culturas hagan uso de las diversos y coloridos trenzados, propios de sus raíces, advierte que no deben apropiarse de su autoría, "estas no son las trenza de las Kardashian, son las trenzas de las comunidades negras".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F379002389483837%2F&amp;tabs=timeline&amp;width=0&amp;height=0&amp;small_header=false&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=true&amp;appId=894195857389402" width="0" height="0" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
