Title: No hay garantías para la vida de los firmantes del Acuerdo de paz
Date: 2020-11-17 14:07
Author: CtgAdm
Category: Expreso Libertad
Tags: #AcuerdoDePaz, #Farc, #Firmantes, #Genocidio
Slug: no-hay-garantias-para-la-vida-de-los-firmantes-del-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/partido_farc_afp_12_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Desde la firma del Acuerdo de Paz a la fecha, en Colombia han sido asesinado**s 241 excombatientes**. Una cifra alarmante que de acuerdo con las y los firmantes evidencian el riesgo que afrontan las personas que construyen una apuesta de paz en el país y un camino hacia el genocidio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa del Expreso Libertad, Martín Batalla, firmante del Acuerdo, y Laura Villa, integrante del Consejo Nacional de Reincorporación, señalan las causas detrás de los asesinatos a las y los excombatientes en los distintos territorios del país. (Le puede interesar: ["Tres firmantes de paz asesinados en Chocó, Caquetá y Putumayo"](https://archivo.contagioradio.com/tres-firmantes-de-paz-asesinados-en-choco-caqueta-y-putumayo/))

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_60531442" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_60531442_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Asimismo, aseguran que si bien, hay un marco de 10 años para la implementación del Acuerdo de Paz y muchas de las apuestas productivas que permiten la reincorporación están andando, **si no hay una garantía a la vida, este proceso se ve en vilo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, tanto Martín Batalla como Villa, aseguran que si bien hay un panorama violento, es importante que tanto la ciudadanía, como la comunidad internacional continúen apoyando la implementación del Acuerdo de Paz y las apuestas de reincorporación que generan las y los firmantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
