Title: Listado de posibles militares beneficiados con la JEP
Date: 2017-03-17 18:40
Category: Nacional, Paz
Slug: min-defensa-entrego-listado-de-817-militares-que-quedaran-a-la-espera-de-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/LISTA-DE-MILITARES-JEP.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Medios] 

###### [17 Mar 2017] 

El Ministro de Defensa Luís Carlos Villegas **firmó una resolución en la que solicita la libertad de 817 militares miembros de la Fuerza Pública, como parte de los Acuerdo de Paz**, que corresponderían al 55% de los beneficiarios de la Justicia Transicional. Entre este listado estarían los nombres del General Rito Alejo del Río, el General Jaime Uscátegui y el Mayor Cesar Alonso Maldonado.

**Los integrantes de la Fuerza Pública que aparezcan en la resolución seguirán vinculados a procesos penales**, y sí están sentenciados continúan con sus penas hasta que vayan a la Jurisdicción Especial de Paz. Le puede interesar: ["Tres generales serían los primeros en acogerse a Jurisdicción de Paz"](https://archivo.contagioradio.com/generales-acogidos-jurisdiccion-de-paz/)

Contagio Radio pudo establecer  una lista de más de 150 integrantes de la fuerza pública que podrían ser beneficiados con estás  medidas.

[Lista de Militares que quedarán a la espera de JEP](https://www.scribd.com/document/342228496/Lista-de-Militares-que-quedaran-a-la-espera-de-JEP#from_embed "View Lista de Militares que quedarán a la espera de JEP on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_6813" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/342228496/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-7t82WAQxSnsHfjLD4YXY&amp;show_recommendations=true" width="600" height="800" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
