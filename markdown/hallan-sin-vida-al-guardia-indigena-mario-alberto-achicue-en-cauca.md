Title: Hallan sin vida al guardia indígena Mario Alberto Achicué en Cauca
Date: 2019-09-10 11:07
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cauca, CRIC, guardia indígena
Slug: hallan-sin-vida-al-guardia-indigena-mario-alberto-achicue-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/CRIC-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

El pasado 8 de septiembre fue asesinado **Mario Alberto Achicué**, guardia indígena de 26 años en la vereda de Agua bendita – Resguardo Indígena de Avirama, en Páez-Belalcazar  al oriente del Cauca, según la información preliminar de la autoridad tradicional, el guardia fue hallado al interior de una habitación con golpes en el sector temporal del cráneo derecho, además de otro golpe que afectó su boca.

A través de su comunicado, el CRIC señala que debido  a negligencias de la administración municipal y la falta de apoyo por la institucionalidad",  no fue posible realizar  una necropsia de forma minuciosa, sin embargo las autoridades indígenas ya están trabajando para investigar las causas y responsables de los hechos.  [(Le puede interesar: Asesinan a María Nelly Bernal, guardia indígena del cabildo Pueblo Pastos en Nariño)](https://archivo.contagioradio.com/asesinan-a-maria-nelly-bernal-guardia-indigena-del-cabildo-pueblo-pastos-en-narino/)

Mario Alberto Achicué era un integrante activo de la Guardia Indígena del resguardo de Lame-norte de Páez, de donde era natal, adicionalmente trabajaba como guardia en actividades comunitarias en el resguardo de Avirama donde vivía actualmente. [(Le puede interesar: Asesinan a Gersain Yatacue, coordinador y guardia indígena de Toribio, Cauca)](https://archivo.contagioradio.com/asesinan-a-gersain-yatacue-coordinador-y-guardia-indigena-de-toribio-cauca/)

Noticia en desarrollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no">[﻿]</iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
