Title: Lo que EPM no ha dicho sobre apertura del vertedero en Hidroituango
Date: 2018-11-06 16:02
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Cauca, EPM, Hidroituango, Movimiento Ríos Vivos
Slug: apertura-vertedero-hidroituango
Status: published

###### [Foto: @MonoMeLlaman] 

###### [6 Nov 2018] 

El pasado domingo, **Empresas Públicas de Medellín (EPM) hizo oficial la llegada del proyecto Hidroituango a su cota 405**, lo que significa que el nivel del agua en el embalse alcanzó 405 metros sobre el nivel del mar, y por ende, entraría en funcionamiento el vertedero que permite evacuar las aguas del río Cauca a su cauce normal. Sin embargo, **la noticia que fue presentada como un éxito por parte de la empresa, para las comunidades es la continuación de una alerta de riesgo que no ha sido debidamente atendida**.

**Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos Antioquia,** expuso que **debido a errores de diseño en el proyecto, Hidroituango no tiene compuertas de descarga**, que son los lugares por donde debería fluir normalmente el agua a través de la represa, sino que tiene un vertedero, compuesto por 4 compuertas. (Le puede interesar: ["EPM perdió el control sobre Hidroituango: Contraloría"](https://archivo.contagioradio.com/epm-perdio-el-control-sobre-hidroituango-contraloria/))

El éxito que para EPM representó la operatividad de una de esas compuertas, es para Zuleta un riesgo, en tanto **los vertederos de las hidroeléctricas** tienen la función de prevenir que el agua rebase los muros de las mismas, es decir, **se usan en caso de emergencia**. (Le puede interesar: ["Después de 5 meses, ANLA sanciona a EPM por sólo 2.420 millones de pesos"](https://archivo.contagioradio.com/despues-de-5-meses-anla-sanciona-a-epm-por-solo-2-420-millones-de-pesos/))

La vocera del Movimiento, explicó que los vertederos se abren en momentos específicos, cortos y con la finalidad exclusiva de desalojar el agua que puede afectar la integridad estructural del embalse, por lo tanto, **lo que la empresa muestra como un hito es para las comunidades cercanas al río Cauca un riesgo sobre el que no existen claridades.**

En ese sentido, **el peligro se centraría en los riesgos que conllevaría para el macizo rocoso la caída del agua que sale del vertedero,** así como las advertencias anteriores que se han emitido, incluso desde la Agencia Nacional de Licencias Ambientales (ANLA) y la Contraloría General de la Nación. (Le puede interesar: ["Alcaldía ordena desalojo de 22 familias víctimas de Hidroituango"](https://archivo.contagioradio.com/desalojo-victimas-hidroituango/))

### **Apertura de vertedero es una amenaza más** 

Zuleta denunció que actualmente hay mensajes contradictorios dirigidos hacía la población porque mientas EPM afirma que se puede regresar a los lugares desalojados, el Departamento Administrativo del Sistema de Prevención, Atención y Recuperación de Desastres **(DAPARD), sostiene que las comunidades no deben estar cerca a la ribera del río.**

Por estas razones, las personas aún no tiene claridad sobre el estado real de la obra y la amenaza que representa la misma para sus comunidades. Adicionalmente, al riesgo de la apertura del vertedero, se suman los siguientes pasos que se tomarían para poner en funcionamiento Hidroituango, entre ellos, **el taponamiento de la Casa de Maquinas y de los túneles de desviación que aún están abiertos, lo que podría afectar la estabilidad del macizo rocoso que rodea el proyecto**.

Finalmente, Zuleta concluyó manifestando que no hay un canal de comunicación entre los directivos de EPM, los mandatarios locales y regionales, y las personas afectadas por Hidroituango (calculadas por la Contraloría en más de 26 mil); razón por la que se seguiría viendo un doble discurso, en el que **mientras los encargados de la obra se preocupan únicamente por la viabilidad de la misma, las comunidades temen por los riesgos que atentarían contra su vida**.

<iframe id="audio_29873128" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29873128_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
