Title: Reforma hospitalaria del distrito sería un golpe fatal al derecho a la Salud
Date: 2016-03-10 15:27
Category: DDHH, Nacional
Tags: Alcaldía de Bogotá, crisis de la salud, Luis González, secretaria de salud
Slug: reforma-hospitalaria-del-distrito-seria-el-paso-final-para-privatizacion-del-servicio-medico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Crisis-de-la-salud.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Yo Reporto Colombia 

<iframe src="http://co.ivoox.com/es/player_ek_10753705_2_1.html?data=kpWkl5ibdJahhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncbPZx9Tfz8aPrNDn0c7hw9HFtsrVjMnSzpDIrdTo087h0ZDXqdOZpJiSo6nFb8bgjNXO1dSPqsriwtGYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [María Doris González, SINDESS] 

###### [10 Mar 2016] 

La posible reforma a la Red Pública Hospitalaria de Bogotá anunciada esta semana por el Secretario de Salud, Luís Gonzáles Morales, **sería el paso final para la [privatización del derecho a la salud](https://archivo.contagioradio.com/?s=crisis+de+la+salud) en la capital y posiblemente en el país**, es lo que se ha asegurado desde el Sindicado Nacional de La Salud y Seguridad Social, SINDESS.

De acuerdo con María Doris González, presidenta de SINDESS, se trata de una reforma que [afecta directamente la estructura de la red hospitalaria](https://archivo.contagioradio.com/40-recorte-presupuestal-bogota-hospitales/), la especialización de los servicios ¿y la movilidad de los usuarios. Además, **la reforma no  plantea soluciones referentes a las deudas de las EPS con los hospitales.**

La propuesta consiste en reagrupar a las entidades de salud pública en 4 redes especializadas ubicadas geográficamente en el norte, sur, suroccidente y centro – oriente de la ciudad y dos centrales de urgencias ubicadas en las localidades de Kennedy y Usaquén. Cada una de estas redes tendría servicios especializados, lo que significa que las personas deberán ir a los hospitales, no por la cercanía y facilidad de movilidad vial, sino por las características de la urgencia que tengan.

“Lo primero que va a suceder, es que el distrito tendrá un año de plazo para re organizar los distintos hospitales en cuatro redes, esas cuatro redes estarán especializadas, **por ejemplo si una persona está en el Hospital La Victoria y va a tener un bebe deberá dirigirse al hospital El Tintal,** porque esa será la georeferencia en todo Bogotá para Obstétricas y Pediátricas, esto sería una violación a la Ley Estatutaria que dictamina que la salud es un derecho y que deben atender a las personas donde estas necesite la atención y no donde la administración diga que debe asistir”, explica María Doris Gonzáles.

Según la presidenta del SIDNESS, es una propuesta que nació con varios vicios. “Nosotros como trabajadores y como organizaciones sindicales estamos en contra de este proyecto porque es el paso para la privatización de los servicios de salud, el Secretario no ha tenido en cuenta nuestras recomendaciones” enfatiza María Doris Gonzales, quien agrega que **“es una reforma que no es buena ni para la comunidad ni para los trabajadores del sector".**

Además María Doris plantea que este proyecto no tiene estudios de movilidad, ni de las necesidades que afronta la ciudadanía y que la creación de una comisión asesora encargada de administrar la [venta de medicamentos](https://archivo.contagioradio.com/el-negocio-de-los-medicamentos-otro-ataque-al-derecho-a-la-salud/) y de talento humano, tendría un carácter lucrativo y de control sobre las entidades públicas de salud, dando paso a la liquidación de la Administración Pública Operativa.

Frente a esa situación, la medida que han tomado diferentes asociaciones sindicales es **presentar un documento a todos los concejales explicando los vicios del proyecto y la no viabilidad del mismo para una ciudad como Bogotá,** argumentando la cantidad de vacíos que existen, y la falta de estudios e investigación que tiene este proyecto. Así mismo, se vienen realizando plantones de los trabajadores del sector de la salud frente al Concejo de Bogotá.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
