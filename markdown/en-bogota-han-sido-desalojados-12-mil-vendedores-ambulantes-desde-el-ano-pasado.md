Title: En Bogotá han sido desalojados 12 mil vendedores y solo 500 han sido reubicados
Date: 2017-07-14 15:54
Category: Economía, Nacional
Tags: Bogotá, Desalojos, Vendedores informales
Slug: en-bogota-han-sido-desalojados-12-mil-vendedores-ambulantes-desde-el-ano-pasado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/marcha-vendedores-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Jul 2017]

Frente al sistemático maltrato y persecución que sufren los vendedores informales en Bogotá, ellos y ellas han decidido hacer un plantón para que no se les vulnere sus derechos. En el portal el Dorado de Transmilenio, **las y los vendedores han rechazado los desalojos y decomiso de mercancía que ha venido haciendo la Policía** sin ninguna explicación y sin garantías de reubicación.

Para John Rivera, presidente de la Asociación de Trabajadores Independientes, “estamos exigiendo que se cumpla el debido proceso de reubicación establecido en la sentencia C211 de 2017”. De igual forma hizo énfasis en que “**en esta nueva administración han desalojado a 12 mil vendedores informales** y menos de 500 han accedido a las ofertas institucionales de reubicación laboral”. (Le puede interesar: ["Admnistración de Peñalosa ha desalojado a 640 vendedores informales"](https://archivo.contagioradio.com/administracion-de-penalosa-ha-desalojado-640-vendedores-informales-en-los-ultimos-dias/))

En la sentencia, la Corte Constitucional manifestó que “la preservación del espacio público no es incompatible con la protección (…) a las personas, que bajo el principio de la buena fe, **se han dedicado a actividades informales en zonas consideradas como espacio público**”.

Adicionalmente, la sentencia establece que “los miembros de este sector de la población, que estén en condiciones de vulnerabilidad y amparados por el principio de confianza legítima, **no serán afectados con multas o decomisos hasta tanto las autoridades competentes hayan ofrecido programas de reubicación** o alternativas de trabajo formal”.

Rivera manifestó que “llegamos a un acuerdo con la alcaldía de Engativá para hacer una mesa de concertación para solucionar el problema y **reclamar que se haga una oferta institucional que se ajuste a nuestras necesidades**”. Mientras esto sucede, ellos y ellas decidieron retomar sus lugares de trabajo como forma de protesta. (Le puede interesar: ["Vendedores informales exigen ser incluidos en Plan de Desarrollo de Bogotá"](https://archivo.contagioradio.com/vendedores-informales-exigen-ser-incluidos-en-el-plan-de-desarrollo-de-bogota/))

### **El desalojo de los vendedores del portal "El dorado" en Bogotá** 

Frente al desalojo que ocurrió con los vendedores de la zona del portal El Dorado el día de ayer, Rivera manifestó que **“los Policías llegaron diciendo que nos iban a recoger las cosas y nos las iban a quitar”**. Ellos y ellas le manifestaron al capitán de la Policía presente que “la administración está actuando de una forma que no debe y no están cumpliendo con las exigencias de la sentencia”.

Finalmente exigieron que el sector de **los vendedores informales sea tratado con respeto** y que se construyan mecanismos que les garantice el derecho al trabajo. Rivera recalcó que la informalidad en el país “es del 48% y nosotros producimos el 47.7% del pib de la nación”.

<iframe id="audio_19806130" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19806130_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

######  
