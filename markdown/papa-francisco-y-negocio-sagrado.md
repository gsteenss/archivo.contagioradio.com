Title: Papa Francisco  y  negocio sagrado
Date: 2015-02-04 16:21
Author: CtgAdm
Category: Abilio, Opinion
Tags: Papa Francisco, teologia de la liberación, teologia en colombia
Slug: papa-francisco-y-negocio-sagrado
Status: published

Por **[[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/)]**

En diciembre un sacerdote de Belén de Bajirá llegó a bautizar  a cinco niñas y niños, confirmar y dar la primera comunión a ocho y casar a una pareja, en  una comunidad rural de la cuenca del Curvaradó,  en el Chocó.

Su secretaria hacía las partidas a los sacramentos correspondientes,  mientras el sacerdote  ordenaba  a una  de las catequistas cobrar  veinte mil pesos por bautismo,  diez mil por confirmación y primera comunión y doscientos cincuenta mil por el   matrimonio.

Dos días atrás la pareja que quería casarse,  pobre y  víctima de desplazamiento forzado,  supo del alto costo de su matrimonio y tuvo que desistir del sacramento por carecer de recursos.   Pero ese mismo día tuvo la suerte  de recibir  una ayuda humanitaria del gobierno por cuatrocientos  mil pesos, en razón de su condición de víctima, retomando su ilusión de celebrar su amor con el sacramento.

El día  de la celebración, ya congregados los contrayentes,  la secretaria del párroco  gritó “quien no pague por adelantado no  recibe el sacramento, no se fía”, y dejó el dinero encima del altar para que el padre lo depositara en su bolso.

Solo un mes atrás el papa Francisco puso un ejemplo de sus primeros días de sacerdocio. Contó que una pareja para casarse con misa, como querían,  tuvo que pagar dos turnos, pues un pago solo alcanzaba para veinte minutos y el párroco se negaba a extenderlo argumentando que la celebración no podría sobrepasar ese tiempo. El Papa, además de afirmar que eso era pecado recomendó,  ante situaciones similares a sus fieles que  “tengan el valor de decírselo en la cara al párroco”.

En nuestro caso, los fieles del Curvaradó han tenido el valor de denunciar, aún en su propia cara, a paramilitares, militares, empresarios beneficiarios del despojo, a cortar palma aceitera sembrada en sus tierras, a desalojar a empresarios, a sacar ganado, pero en esta oportunidad, ante un representante de lo sagrado, solo guardaron el escandaloso acto en su corazón, y   contaron   con indignación el suceso, cuando se presentó la oportunidad.

Las palabras del papa Francisco diciendo que la salvación no se compra, aún no llega a los oídos de muchas parroquias  y poco o nada se conoce de avances frente al tráfico  de la salvación. Al sacerdote mercader, la pobreza de sus feligreses, su condición de víctimas y su extrema pobreza, no le tocaron el corazón.

Como en esta manifestación específica, para sorpresa de muchos de nosotros, el papa Francisco ha motivado procesos también de gran significación como la constitución de  la red Pan Amazónica para la defensa de los territorios de esa área del planeta y la presentación de casos ante el Sistema Interamericano de Derechos Humanos por las afectaciones a los territorios de las comunidades por la gran minería.

A buena hora la denuncia profética a los sacramentos como negocio y de la mercantilización de territorios especialmente estratégicos para la conservación de la vida, hace parte de la palabra y la acción del hombre más importante de la catolicidad. Confiamos en que mujeres y hombres de iglesia se dejen tocar  por éste llamado, de lo contrario,  su  credibilidad  seguirá  resquebrajada.
