Title: FARC alerta sobre irregularidades en el proceso de reincorporación
Date: 2018-04-09 13:33
Category: Otra Mirada, Paz
Tags: acuerdo de paz, ex combatientes, FARC, gobierno colombiano, paz, reincorporación
Slug: farc-alerta-sobre-problemas-en-el-proceso-de-reincorporacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/ultima-marcha-de-la-guerrillas-de-las-farc-6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: NC Noticias ] 

###### [09 Abr 2018  ] 

[Luego del encuentro en Bogotá de 140 delegados y delegadas de los Espacios Territoriales de Capacitación y Reincorporación y de nuevos puntos de reagrupamiento, **los integrantes del partido político FARC encontraron varias irregularidades** respecto a la reincorporación de los ex combatientes a la vida civil.  Argumentaron que no han sido suficientes las respuestas del Estado para este proceso que atraviesa por falta de recursos y precariedad en las condiciones de vida.]

### **Irregularidades encontradas por el partido político** 

[La primera de ellas, de acuerdo con la Fuerza Alternativa Revolucionaria del Común (FARC), es que la respuesta del Estado frente a la reincorporación “**transita los caminos de la burocratización** excesiva, la ineficiencia, la indolencia, las trabas recurrentes, la falta de voluntad política y la muy notoria insuficiente capacidad institucional”. Esto debido a que el proceso de reincorporación no se está llevando a cabo de una manera justa y transparente.]

[En segundo lugar, el partido FARC manifestó que en los **espacios territoriales **no se cuenta con las condiciones dignas para la vida como lo son las garantías a la salud, la vivienda e incluso los mínimos en servicios públicos. Además, recordaron las problemáticas en cuanto a los procesos de documentación. ]

Como una tercera irregularidad nombra las problemáticas referente a los recursos para la reincorporación. En el encuentro concluyeron que “todos los **dineros** del posacuerdo y particularmente los **destinados a la reincorporación deben asignarse de manera directa y sin intermediación** alguna a los Espacios Territoriales, las comunidades de ex guerrilleros y ex guerrilleras que los habitan o a sus organizaciones solidarias.”

Por último, encontraron un problema en la acreditación de pertenencia de los ex combatientes "al partido político, al sistema financiero, obstáculos para la organización de cooperativas y **bloqueos en el sistema bancario**".  Además insisten en el incumplimiento por parte del Estado sobre la liberación de los presos políticos, de los cuales aún continúan 623 en centros de reclusión.

### **Recomendaciones para la ejecución del proceso de reincorporación ** 

Ante dicha situación, la FARC plantea un **“plan de emergencia”** para darle solución efectiva a los problemas más urgentes al proceso de reincorporación. En la reunión plantearon que "es necesario la elaboración y puesta en marcha del Plan Nacional de Reincorporación" que tenga en cuenta el acceso a la tierra.

Sin embargo desde el partido Fuerza Alternativa Revolucionaria del Común reconocieron que los avances en el ámbito de la educación han sido el resultado "del esfuerzo propio y de la voluntad férrea que tienen los excombatientes de las FARC desde los tiempos de la guerra”. (Le puede interesar: "[Los puntos en el congelador de los acuerdos de paz de la Habana](https://archivo.contagioradio.com/los-puntos-en-el-congelador-de-los-acuerdos-de-paz-habana/))

Finalmente, el partido agradeció el apoyo de la **comunidad internacional**, la ONU, la Unión Europea y en particular a los Gobiernos de Cuba y de Noruega, "quienes muestran un interés por la reincorporación de los excombatientes".

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
