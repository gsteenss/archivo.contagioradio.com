Title: Marcha Mundial de Mujeres lanza campaña de solidaridad internacional en Tunez
Date: 2015-03-26 18:23
Author: CtgAdm
Category: El mundo, Mujer
Tags: Foro social mundial de Túnez, Marcha mundial por las mujeres
Slug: marcha-mundial-de-mujeres-lanza-campana-de-solidaridad-internacional-en-tunez
Status: published

###### Foto:PoemarioSaharalibre.blogspot.com 

##### **Entrevista a[ Grazazama]:** 

<iframe src="http://www.ivoox.com/player_ek_4268660_2_1.html?data=lZejmpuadI6ZmKiakpyJd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLmxM3OjdLZssXdwtGY0tTWb83V1JDa18%2FJtsbnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La **Marcha Mundial de las Mujeres**, es un movimiento **anticapitalista, antipatriarcal y anticolonialista** de carácter **internacional**. El movimiento lucha por la liberación de las mujeres y trabaja en la creación de alternativas para mejorar la vida de las mismas.

Según **Grazazama**, activista del movimiento, este año han decidido **participar en el Foro Social Mundial de Túnez** con el objetivo de **solidarizarse** y de apoyar la **lucha de las mujeres en el mundo árabe** y en todo el continente africano.

Cada **cinco años la organización convoca un acto de solidaridad en alguna parte del mundo.** Así, siendo conscientes de la situación de violación de DDHH de las mujeres en contextos militaristas en el continente africano y en los **países árabes**, este año van a trabajar en la creación de **agendas de lucha** en dicho contexto.

La activista afirma que el trabajo se va a realizar en torno a **4 problemáticas,** la primera hace referencia la **defensa de las mujeres contra la violencia sobre ellas** a nivel estructural y cultural, la segunda sobre la resistencia al militarismo, la tercera con el objetivo de crear alternativas económicas para **resistir al control del capitalismo sobre el trabajo y el cuerpo de las mujeres** y la cuarta por la **defensa** del territorio, la **naturaleza** y el derecho de las mujeres a la utilización de **los recursos naturales.**
