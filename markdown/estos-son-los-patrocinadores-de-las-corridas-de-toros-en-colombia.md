Title: Estos son los patrocinadores de las corridas de toros en Colombia
Date: 2017-02-05 14:24
Category: Animales, Voces de la Tierra
Tags: corridas de toros, Maltrato animal
Slug: estos-son-los-patrocinadores-de-las-corridas-de-toros-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/corridas-de-toros-e1486324945371.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [AnimaNaturalis]

###### [5 Feb 2017] 

En medio del debate que se desarrolla en estos días en torno a las corridas de toros, en Contagio Radio hemos hecho un listado de las empresas que apoyan las **fiestas taurinas en Bogotá, Manizales y Cali.**

Mientras para unos, este tipo de actividades les sigue pareciendo cultura, tradición y arte, a los defensores de la vida de los toros les parece un acto cruel. Lo cierto es que son varias las empresas que se lucran de este espectáculo, y este punto es precisamente una de las preocupaciones por las cuales se busca que se mantenga viva la denominada fiesta brava.

El rechazo hacia esta actividad ha generado que varias empresas hayan decidido quitar sus publicidades de la temporada taurina de Bogotá, hoy son 5 las empresas que publicitan en la Plaza La Santa María, entre los que se encuentran medios como **RCN Radio, CM& y Caracol Radio. Además, ‘Tu boleta’,** ha sido clave para la organización del evento aunque técnicamente no sea patrocinador.

Por su parte, en Manizales, son 21 las empresas que patrocinan la fiesta brava, entre los que se encuentran **el Banco BBVA, La Cruz Roja** y cinco medios de comunicación, en los que repiten **RCN y Caracol Radio**. Finalmente, en Cali, son 6 compañías las patrocinadoras, con la revista **Cromos** entre ellas.

![patrocinadores corridas de toros](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/infografia_patrocinadores_corridas.png){.wp-image-35785 .aligncenter width="405" height="1013"}
