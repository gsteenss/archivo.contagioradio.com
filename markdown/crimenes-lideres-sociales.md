Title: Crímenes contra líderes sociales en 2016 y 2017 son superiores a las cifras registradas
Date: 2018-10-03 17:45
Author: AdminContagio
Category: DDHH, Nacional
Tags: Defensoría del Pueblo, Dejusticia, Human Rights, lideres sociales, Somos defensores
Slug: crimenes-lideres-sociales
Status: published

###### [Foto: El Sonajero] 

###### [3 Oct 2018] 

Una investigación realizada por  las organizaciones **Human Rights Data Analysis Group y Dejusticia** reveló que en Colombia **podría haber un error en las cifras de líderes sociales asesinados durante 2016 y 2017;** hecho que podría afectar la creación e implementación de políticas públicas que contrarresten la violencia sufrida por quienes defienden los derechos humanos en el país.

En el documento, los investigadores analizaron los datos de líderes sociales asesinados reportados por **Front Line Defenders, INDEPAZ, Somos Defensores, Cumbre Agraria, la Defensoría del Pueblo** y la **Oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos (OACNUDH)**; que registraron en 2016, 160 asesinatos y en 2017, 172. (Le puede interesar: ["77 defensores de DD.HH. han sido asesinados en el primer semestre de 2018"](https://archivo.contagioradio.com/77-defensores-asesinados/))

\[caption id="attachment\_57278" align="aligncenter" width="413"\][![Líderes Sociales Asesinados en 2016 y 2017](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/Captura-de-pantalla-2018-10-03-a-las-3.05.00-p.m.-413x163.png){.wp-image-57278 .size-medium width="413" height="163"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/Captura-de-pantalla-2018-10-03-a-las-3.05.00-p.m..png) Foto: Asesinatos de líderes sociales en Colombia en 2016–2017: una estimación del universo\[/caption\]

### **"Estimamos que en 2016 fueron asesinados 166 líderes, mientras que esta cifra fue de 185 para 2017"** 

El estudio usó el método bayesiano no paramétrico de clase latente y captura-recaptura (LCMCR por sus siglas en inglés), para determinar, a partir de las coincidencias y diferencias en el número de casos registrados, si podría haber errores en el total de asesinatos. Gracias a este método, los investigadores concluyeron que **en 2016 fueron asesinados 166 líderes sociales en Colombia, mientras que esta cifra fue de 185 para 2017.**

La variación en las cifras, según la investigación, se produce por dos factores: Es más probable que se reporten asesinatos "a plena luz del día de un reconocido líder social en una capital" a que se reporten aquellos que ocurren en zonas rurales de difícil acceso; además, porque es probable que "las personas reporten los casos a organizaciones afines a su pensamiento político, pero no a aquellas que tengan un pensamiento distinto".

Entre las conclusiones del estudio, las organizaciones destacan que **hubo más casos que no fueron documentados por ninguna de las 6 organizaciones en 2017 respecto a 2016**; y afirmaron que la violencia contra líderes sociales aumentó entre un año y otro, "probablemente  en un 10% o más". (Le puede interesar: ["Siguen los atentados en contra de líderes de la comunidad Wayuú"](https://archivo.contagioradio.com/siguen-los-atentados-en-contra-de-los-lideres-de-la-comunidad-wayuu/))

Con el [documento](https://cdn.dejusticia.org/wp-content/uploads/2018/09/Asesinatos-de-l%C3%ADderes-sociales-en-Colombia-en-2016-2017-una-estimaci%C3%B3n-del-universo.pdf), los investigadores desearon aportar a la discusión sobre el número total de líderes asesinados, no para tratarlos como una cifra, sino para **aportar a la discusión sobre las deficiencias y fortalezas en las medidas de protección de defensores de derechos humanos en riesgo**, para que se hagan los cambios necesarios para frenar los ataques perpetrados en su contra. (Le puede interesar: ["Nuevas capturas contra líderes en el oriente del país"](https://archivo.contagioradio.com/captura-lideres-sociales-oriente/))

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
