Title: “Nosotros somos el presente no el futuro” Foro Nuevas Ciudadanías
Date: 2016-04-14 18:30
Category: Nacional, Política
Tags: Ambientalismo, animalismo, jovenes, nuevas tecnologías, paz
Slug: nosotros-somos-el-presente-no-el-futuro-foro-nuevas-ciudadanias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/colegiosfa-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alejnadra Rojas] 

###### [14 Abril 2016]

Más de 400 jóvenes se reúnen hoy en el Colegio San Francisco de Asís en el Foro “Nuevas Ciudadanías” que pretende generar preguntas en torno a las responsabilidades que tienen los y las jóvenes de cara al país. **“Muchas veces nos venden la idea de que somos el futuro, pero somos el presente, tenemos que actuar ahora”** afirma Alejandra Rojas, estudiante de grado 11 y organizadora del evento.

El evento continuará con una serie de talleres en los que se trabajarán diversos temas como el **ambientalismo, las nuevas tecnologías, las identidades sexuales y de género, el animalismo, nuevas expresiones artísticas, espiritualidad más allá de las religiones y el compromiso de los y las jóvenes frente a la paz.**

Dentro de los retos que se plantean está la manera en que los jóvenes deben asumir los procesos políticos de creación de leyes y de iniciativas ciudadanas en cada uno de los temas mencionados, y sobre todo la necesidad de asumir las tareas que la realidad está planteando. Todas estas reflexiones se realizan en medio de talleres y actividades culturales.

Al final del evento habrá una conferencia a cargo de Santiago Rivas, presentador del programa “Los Puros Criollos” del que esperan “nos antoje un poquito de las ganas de trabajar por el país” afirma Rojas.

Uno de los elementos de resaltar de en este tipo de iniciativas estudiantiles es precisamente que los jóvenes se asuman como parte de la solución a los problemas que plantea la sociedad de hoy, y es muy importante que desde esos escenarios, muchas veces cerrados al entorno, se abran horizontes para la reflexión y la acción, plantean algunos profesores y estudiantes.

<iframe src="http://co.ivoox.com/es/player_ej_11169181_2_1.html?data=kpaemJ6VfJKhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaLgxs%2FO0MnWpYzG0M%2FO1ZCRb6TjzZDAw9OPitPVz8jW1cjTb8XZjKbgy9iRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
