Title: Colombia está de fiesta con "La paz sí es contigo"
Date: 2016-07-15 14:33
Category: Movilización, Nacional
Tags: LaPazSiesContigo
Slug: colombia-esta-de-fiesta-con-la-paz-si-es-contigo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/pitalito8-e1468604108800.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [\15 de Jul] 

Desde muy temprano iniciaron las diferentes actividades culturales que hacen parte de la campaña "La paz sí es contigo", una apuesta de diversos sectores del movimiento social del país que busca, a partir de herramientas pedagógicas y culturales, **aumentar la recepción de la ciudadanía hacía los acuerdos de paz de la Habana y obtener una mayoría de votación al sí en el plebiscito por la paz**.

Ciudades como **Cúcuta,** **Medellin**, **Villavicencio**, **Cali**, **Neiva**, **Manizales**, **Pasto**, **Popayán**, entre otras ya registran movilizaciones en sus calles. Algunas de ellas finalizaran con eventos culturales como conciertos, presentaciones de obras de teatro o batucadas.

\  
 
