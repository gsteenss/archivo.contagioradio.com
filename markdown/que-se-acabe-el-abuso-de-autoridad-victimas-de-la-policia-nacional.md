Title: "Que se acabe el abuso de autoridad" Víctimas de la Policía Nacional
Date: 2016-08-09 13:19
Category: DDHH, Nacional
Tags: abuso de autoridad, Abuso de fuerza Policía
Slug: que-se-acabe-el-abuso-de-autoridad-victimas-de-la-policia-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/policia-agresion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ernesto Che] 

###### [9 de Agost] 

[Víctimas de la Policía Nacional enviaron una carta al director de esa institución, General Jorge Nieto, con la  finalidad de **exigirle que cesen los abusos de fuerza hacía la población civil, y a su vez, que haya un acción de disculpas públicas** frente a todos los crímenes que han cometido en contra la ciudadanía, que en la mayoría de los casos han quedado en la impunidad.]

En el documento se expone como **la Policía se ha convertido en un ente de represión y corrupción**, que a través del tiempo ha [cometido crímenes en contra de la población civil](https://archivo.contagioradio.com/el-prontuario-del-esmad/)y de miembros de la misma institución. Por otro lado, se presentan algunos de los nombres de las víctimas que fueron asesinadas por miembros de la institución cuyos casos como el de **Sandra Vásquez o Héctor Martinez, continúan sin avances en la investigación.**

Dentro de las exigencias en la carta, esta como primera medida [que se desmonte gradualmente el ESMAD](https://archivo.contagioradio.com/este-codigo-de-policia-es-para-la-dictadura-no-para-el-posconflicto-alirio-uribe/), que se abra una veeduría ciudadana en donde se sigan los procesos disciplinarios abiertos a miembros de la Policía, que se mejoren los procesos de selección e ingreso de personas a la institución y que se **mejoren los entrenamientos de la Policía sobre el uso de la fuerza y el Derecho Internacional Humanitario para que no haya uso indebido de la misma.**

De acuerdo con **Gustavo Trejos**, padre de Diego Felipe Becerra, grafitero asesinado por integrantes de la policía, "**lograr justicia en este país en contra de los uniformados que abusan de poder o que cometen homicidios, es una tarea casi que imposible**, nosotros hemos avanzado mucho y sabemos que lo vamos a lograr, pero es muy difícil porque el Ministro de defensa y algunos senadores piensan que la institucionalidad se mantiene con base en el silencio cómplice".

A su vez, Trejos afirma que la única forma para que la Policía se convierta en una autoridad respetuosa e integra es que deje de lado sus actos represivos y de hostigamiento en contra de la población y por el contrario que **se convierta en una institución que pueda dialogar con la ciudadanía, que trabaje con la comunidad** y realice actividades lúdicas y culturales, sobretodo en un momento de posconflicto como el que se prevé para Colombia.

<iframe src="http://co.ivoox.com/es/player_ej_12491526_2_1.html?data=kpehm5aZdpehhpywj5aaaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncajp1NnO2NSPmNPZy9TgjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
