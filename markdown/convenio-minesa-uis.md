Title: Convenio de Empresa de Emiratos Arabes MINESA con UIS levanta sospechas
Date: 2017-06-21 13:55
Category: Ambiente, Voces de la Tierra
Tags: medio ambiente, Mineria, Minesa, Páramo de Santurbán, uis
Slug: convenio-minesa-uis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Páramo-de-Santurbán-foto-de-Jorge-William-Sánchez-Latorre-e1498070474455.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Conciencia Ciudadana] 

###### [21 Jun 2017] 

Son tres** los convenios que firmó la Universidad Industrial de Santander (UIS) con la empresa árabe MINESA que despiertan las sospechas de la comunidad estudiantil**. Según lo acordado se especifica que la universidad pagará cerca de 300 millones de pesos más que la empresa y hay cláusulas de confidencialidad que expresan que la multinacional podrá modificar su nombre para no afectar sus intereses.

Según los estudiantes de la Universidad Industrial de Santander y algunos docentes, los acuerdos entre la entidad universitaria y la empresa Minesa S.A.S, **ponen en riesgo la estabilidad del páramo de Santurbán**. puesto que los estudios están orientados a identificar la riqueza en materia de metales preciosos presentes en el ecosistema.

Esto pues la empresa quiere realizar proyectos de minería que, por los explosivos utilizados tanto abajo como encima de la montaña, ponen en riesgo 14 ríos que surten agua a 3 millones de santandereanos.  (Le puede interesar: ["A salvo páramo de Santurbán de Eco Oro Minerals")](https://archivo.contagioradio.com/a-salvo-paramo-de-santurban-eco-oro-minerals/)

El objetivo de la empresa Minesa en los acuerdos es “generar confianza con la población y los vecinos del páramo y desarrollo en la región”.  Sin embargo, **la comunidad académica cuestionó estos objetivos en la medida que hay cláusulas que ponen por encima los intereses de la empresa** y ponen en riesgo la autonomía de la Universidad.

Seún la ambientalista Dayana Corzo, integrante del Comité por Santurbán, ante esto los estudiantes se preguntan  "¿Cómo es que MINESA busca generar confianza en la comunidad y alguno de los resultados de los estudios que realice la UIS pueden llegar a afectar los intereses de la empresa?”.

De igual forma, los estudiantes manifestaron que los 3 convenios firmados entre la UIS y Minesa **tienen un valor de 2´457.320.000 pesos donde la Universidad aporta 1´437.320.000** y la empresa tan solo 1´020.000.000 pesos. Según la comunidad estudiantil, "si la Universidad aporta a un privado para investigaciones más dinero del que aporta la misma empresa, se crea un déficit".

Además, "la universidad va a aportar su capital humano y de infraestructura para desarrollar proyectos mineros y no estamos de acuerdo en eso".  (Le puede interesar: ["Minería en páramo de Santurbán no contó con estudios sobre impactos socio ambientales"](https://archivo.contagioradio.com/mineria-en-paramo-de-santurban-no-conto-con-estudios-sobre-impactos-socio-ambientales/))

El Comité para la defensa del agua y el páramo de Santurbán manifestó que se movilizarán en contra de las intenciones de Minesa S.AS. de **“afectar nuestras riquezas y de dejar nuestros pueblos llenos de miseria, prostitución, delincuencia y muertos de sed”.** Manifestaron también que “nunca hemos estado en contra del desarrollo de nuestros pueblos y del fortalecimiento de la economía”.

Por esto manifiestan que la comunidad santandereana “ ha sido engañada por la empresa que dice buscar mejorar la calidad de vida para la comunidad y **sólo se evidencian problemas ambientales y sociales asociados a los procesos mineros**".

De igual forma, los estudiantes han denunciado en varias ocasiones que Minesa **"se registró como una sociedad minera de Santander engañando a la población** pues es una filial de la empresa de Emiratos Árabes Unidos Mubadala". Minesa llegó a Colombia para realizar actividades mineras de oro y actualmente se encuentra realizando actividades de exploración en el páramo de Santurbán.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
