Title: 17 frases por cada año sin Jaime
Date: 2016-08-13 10:05
Category: Otra Mirada
Tags: Asesinato Jaime Garzón, Frases de Jaime Garzón
Slug: 17-frases-por-cada-ano-sin-jaime
Status: published

##### [13 Agos 2016]

Se cumple un año más del homicidio de **Jaime Hernando Garzón Forero**, ocurrido la madrugada del 13 de Agosto de 1999, cuando iba camino a su trabajo en Radionet dos sicarios que se transportaban en una motocicleta de alto cilindraje con placas ocultas, lo interceptaron y le dispararon en 5 ocasiones causándole la muerte.

Hoy recordamos 17 frases, una por cada año de ausencia material, que se mantienen vigentes en un país que hoy busca caminos para encontrar paz con dignidad, esa misma a la que Jaime se refería y que termino costandole la vida.

Lo que nos enseñan a los colombianos no tiene nada que ver con las necesidades que tenemos los colombianos.

Ser colombiano para nosotros significa tener una astucia, casi siempre mal usada.

Hay algo que existe en los colombianos: no perdemos la esperanza de hacerlo cada vez mejor.

En Colombia hay mucho talento, pero la educación nos ha llevado a que el talento sea  desperdiciado. Se tuerce.

¿Es posible que la solución de la crisis nacional venga de allá mismo, de los que la han provocado?

General, no busque enemigos entre los colombianos que arriesgamos la vida a diario por construir una patria digna, grande y en paz, como la que quiero yo y por la que lucha usted.

Yo no hago humor político, hago crítica política

En este país no llamamos las cosas de verdad, nunca!... Ni en política, ni en la vida real

“El problema de los colombianos es que no tenemos una conciencia colectiva. Tenemos una posición cómoda e individual ante la vida”

Este país entiende que el desarrollo es cemento. ¿Para qué queremos ocho vías si nos agredimos?

Duermo para olvidar que soy un bufón en esta comedia de sociedad

El Estado existe para cobrarle a uno impuestos, IVA, valorización; pero no existe para darle seguridad social, empleo, seguro social, seguro médico, nada

En Colombia yo creo que el pueblo recobra la capacidad de poder si se organiza, en términos civiles, porque el conflicto armado ya no tiene razón de ser.

El presidente nunca se dirige al país, se digiere al país

A Alvaro (Uribe) le cabe el país en la cabeza. El vislumbra todo este gran país como una zona de orden público total”, Godofredo Cínico Caspa

Si ustedes no reaccionan y hacen uso del voto, vuelven y nos imponen otro tipo

Los colombianos no respetamos, nos le metemos al rancho a todo el mundo... Respetemos, que cada uno sea
