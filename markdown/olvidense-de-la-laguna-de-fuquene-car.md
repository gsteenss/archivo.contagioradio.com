Title: "Olvídense de la Laguna de Fúquene": CAR
Date: 2016-04-26 17:25
Category: Ambiente, Entrevistas
Tags: CAR, chiquinquira, laguna fuquene
Slug: olvidense-de-la-laguna-de-fuquene-car
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Laguna-Fúquene.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Boyacá Radio ] 

###### [26 Abril 2016 ]

Por lo menos diez activistas ambientales, entre ellos Fabián Arévalo, marcharon desde Chiquinquirá hasta Bogotá, para exigir a las directivas de la CAR la preservación de la Laguna de Fúquene que enfrenta 25 años de deterioro ambiental. Exigencia frente a la que Néstor Franco, director de la entidad, respondió con que de los 48 km de extensión de la Laguna iban a quedar 300 has en un **embalse en la vereda San José, municipio de Carupa, cuya construcción desplazaría a 187 familias campesinas**.

Según Arévalo, la CAR argumenta que es casi imposible reunir el billón y medio necesario para salvar la Laguna, "para ellos es más viable construir un embalse pequeño y cortar uno de los afluentes principales del río San José", que vierte sus aguas en la Laguna, la cual "automáticamente se perdería", **beneficiando a los terratenientes que han estado detrás de la implementación de los programas impulsados por la CAR desde 1967 para secarla**.

La CAR está empecinada en secar la Laguna, lleva diez años sin implementar el CONPES establecido para salvarla, sin que se vean reflejado el presupuesto que ya fue asignado, expresa Arévalo, e insiste en que de no preservarse el cuerpo hídrico, **podría aumentar la temperatura en la región 2°**, y lo anterior sumado a la plantación de eucalipto, pino y acacia promovida por la CAR, convertiría a parte del altiplano cundiboyacense en un desierto en menos de 10 años.

Debido al problema invernal de 2011, el río Suárez fue drenado, generando la reducción de un metro de profundidad de la Laguna, ocasionando una emergencia sanitaria que ya cumple más de tres meses, en los que los pobladores de Chiquinquirá no han contado con el suministro de agua, deben comprarla aún sin ser potable en carrotanques y por \$50 mil. **Actualmente 1 de cada 5 habitantes presenta problemas estomacales y epidérmicos por el consumo de esta agua**.

Exigimos al "gobierno que nos atienda, porque **prácticamente nos estamos muriendo de sed**" y el alcalde de Chiquinquirá "esta empeñado en construir el embalse", afirma Arévalo, y agrega que continúan la movilización que ya completa un mes. Este martes los campesinos se reúnen con las plenarias de la Cámara y el Senado, exigiendo que sus demandas sean escuchadas.

<iframe src="http://co.ivoox.com/es/player_ej_11315023_2_1.html?data=kpagk5qUdpShhpywj5aWaZS1kZiah5yncZOhhpywj5WRaZi3jpWah5yncafVw87O0JCltsbqwtHcjZKPhc7Wysrb1sbQrdTowpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]

   
 
