Title: Las estrategias de las Comisiones de Paz para el posconflicto
Date: 2016-01-29 13:14
Category: Nacional, Paz
Tags: Angela Robledo, Comisiones de Paz, Congreso, Iván Cepeda, Senado
Slug: las-estrategias-de-las-comisiones-de-paz-para-el-posconflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/DSC05443.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio.  ] 

###### [29 Ene 2016. ] 

Durante esta semana las Comisiones de paz de Cámara y Senado se reunieron con el fin de concertar algunas **propuestas para fortalecer la búsqueda de paz en esta nueva etapa de los Diálogos en La Habana**, en la que se concretarán los mecanismos de implementación de los acuerdos a los que lleguen las partes.

En rueda de prensa, Ángela María Robledo, Representante a la Cámara por Bogotá, aseveró que las Comisiones se reestructurarán para **brindar acompañamiento a la implementación de los eventuales acuerdos**, en tal sentido, fortalecerán su interlocución con el Consejo Nacional de Paz para desarrrollar en las regiones más afectadas por la dinámica del conflicto armado, **Consejos Departamentales y Municipales de Paz**, que buscaran garantizar el efectivo cumplimiento de los acuerdos en los territorios.

<iframe src="http://www.ivoox.com/player_ek_10251111_2_1.html?data=kpWfl5aVdZKhhpywj5WUaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5ynca3V1JDS1dnWpdXZyM7O1ZDIqYzgwtiYpdTRrdTd0NPS1ZDIqYzEwt%2BY0sbWpYzZzZDd0djHs8%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Por su parte Horacio Serpa, Senador del Partido Liberal, afirmó que desde las Comisiones de Paz se exhortará tanto al Gobierno nacional como a la comandancia del ELN para que se **formalice la mesa de conversaciones, teniendo en cuenta los avances de la actual fase exploratoria** y** **con el fin de consolidar la paz con ambas guerrillas.

<iframe src="http://www.ivoox.com/player_ek_10251145_2_1.html?data=kpWfl5aVeJahhpywj5WUaZS1lpyah5yncZOhhpywj5WRaZi3jpWah5ynca3V1JDS1dnWpdXZyM7O1ZDIqYzgwtiYpdTRrdTd0NPS1ZDIqYzEwt%2BY0sbWpYzZzZDd0djHs8%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El Senador del Polo Democrático, Iván Cepeda, afirmó que las Comisiones de Paz buscarán concertar reuniones con Rafael Pardo, actual Ministro del Posconflicto, para asegurar que el Estado colombiano se comprometa a disponer todos sus esfuerzos en **la planificación  estructural que requiere la fase de implementación de los acuerdos**.

<iframe src="http://www.ivoox.com/player_ek_10251164_2_1.html?data=kpWfl5aVepWhhpywj5WVaZS1kZqah5yncZOhhpywj5WRaZi3jpWah5ynca3V1JDS1dnWpdXZyM7O1ZDIqYzgwtiYpdTRrdTd0NPS1ZDIqYzEwt%2BY0sbWpYzZzZDd0djHs8%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Roy Barreras, Senador del Partido de la U, aseguró que una de las primeras iniciativas será enviar una carta al Centro Democrático para que acoja la **invitación de hacer parte de las Comisiones de Paz con el fin de lograr consensos** que nutran el proceso de Diálogos con la guerrilla de las FARC-EP.

<iframe src="http://www.ivoox.com/player_ek_10251169_2_1.html?data=kpWfl5aVepqhhpywj5WUaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5ynca3V1JDS1dnWpdXZyM7O1ZDIqYzgwtiYpdTRrdTd0NPS1ZDIqYzEwt%2BY0sbWpYzZzZDd0djHs8%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ] 
