Title: En las cárceles de Colombia existe pico y placa para poder dormir
Date: 2017-04-21 15:03
Category: DDHH, Nacional
Tags: carceles, ley de jubileo, presos, situación carcelaria
Slug: en-las-carceles-de-colombia-existe-pico-y-placa-para-poder-dormir
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/carce.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: primiciadiario.com] 

###### [21 Abr. 2017] 

Familiares de los internos de las cárceles de Colombia están convocando a una movilización este 29 de abril para hacer un llamado al Congreso Nacional, luego de la radicación de **dos iniciativas que buscan la reducción de penas a reclusos a propósito de la visita del Papa Francisco a Colombia en septiembre**. Proyectos que según las familias, pretenden impedir la salida de algunos internos “evitando recibir algún beneficio como la ley de jubileo”.

**Según Sandra Aguilar, una de las convocantes a la movilización** esta “pretende que el Congreso nos escuche. Hoy 70% de la población carcelaria tienen penas muy altas y estamos convencidos que la cárcel no resocializa”.

Además pretenden mostrar todas las realidades inhumanas a las que se ve enfrentada a diario la población carcelaria, en donde según ella varias **no cuentan con la posibilidad de acceso al agua, los cobros para acceder a alimentos son exorbitantes e incluso existe “pico y placa” para poder dormir acostados.**

"Lo que vemos ahora es que tener un preso se volvió un negocio. **Tienen que pagar para entrar al baño, pagar \$1.200.000 si quieren una celda sola** o una buena celda (…) hay pico y placa en las cárceles para dormir. Si eres número par duermes el lunes, miércoles y viernes" aseveró Sandra

Según lo denuncia Sandra **en cárceles como la de Valledupar no hay camas suficientes, razón por la cual instauraron el pico y placa.**

Otra de las situaciones actuales, es el tema de la alimentación en las cárceles, por la cual, están cobrando y los precios son irrisorios **“normalmente les colocan 2 o 3 cucharadas de arroz, si quieres una porción adicional tienes que pagar 10 mil pesos** en la semana para que te den el doble (…) a veces también escasea la comida, no entiendo por qué”.

Sumado a estas condiciones el agua no es la excepción, relata Sandra que un ejemplo es la cárcel La Modelo en Bogotá, en donde después de las 7 a.m. no funciona en este lugar, incluyendo los baños y en la **Cárcel El Buen Pastor las mujeres deben pagar después de haber entrado dos veces a hacer uso de los sanitarios.**

“Para poder bañarte de primeras debes pagar, primero entran los Plumas, que son los que manejan los patios, porque eso es mentira que manda el INPEC, y después si pueden entrar los demás” agrega Sandra.

**Los Plumas, relata Sandra en la denuncia, son quienes están atentos del funcionamiento de los patios,** vigilan quiénes ingresan, que esté limpio todo, que no hayan peleas ni drogas y son ellos quienes tienen los  privilegios “en la Cárcel La Picota estamos hablando del patio 9 y 11 y en la cárcel El Buen Pastor del patio 11 y 16”.

**En la actualidad se conoce que han sido radicados 3 proyectos que pretenderían la reducción de penas para la población carcelaria** que cumpla algunas condiciones como: no haber cometido delitos de lesa humanidad, feminicidio, crímenes atroces, entre otros. Proyectos propuestos por el Partido de la U, el Gobierno Nacional y el Partido Centro Democrático.

“Démosles una segunda oportunidad, a que sus hijos crezcan en ese vínculo de familia (…) ojalá el Congreso y el Gobierno sienta y escuche otro punto de vista que las cosas no son solamente como ellos las dicen, sino que hay otros puntos de vista”.

Al final, los familiares esperan que a los presos les hagan reducciones de penas y si no logran los acuerdos a través de leyes en el Congreso, **sugieren que sea el Papa quien tomé la decisión de hacer el descuento.**

La movilización partirá de los juzgados de Paloquemao en Bogotá a las 7 a.m. para llegar a la Plaza de Bolívar, en la que darán a conocer su propuesta que entre otros temas pretende hacer entender a la sociedad y al Congreso que **la población carcelaria merece una segunda oportunidad.**

<iframe id="audio_18273882" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18273882_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
