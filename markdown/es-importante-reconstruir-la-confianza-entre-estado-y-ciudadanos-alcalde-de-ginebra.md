Title: Es importante reconstruir la confianza entre Estado y ciudadanos: Alcalde de Ginebra, Suiza
Date: 2017-08-16 13:13
Category: Judicial, Nacional, Paz
Tags: alcaldía de ginebra, implementación de los acuerdos, proceso de paz, suiza, veeduria internacional
Slug: es-importante-reconstruir-la-confianza-entre-estado-y-ciudadanos-alcalde-de-ginebra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/suiza-e1502909501993.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Ago 2017] 

El alcalde de Ginebra en Suiza, Rémy Pagani, culminó su recorrido por el país realizando veeduría internacional de la implementación de los acuerdos de paz. En rueda de prensa manifestó su preocupación por **la situación de seguridad de los líderes sociales, por el fenómeno del paramilitarismo** y la lentitud del gobierno en los procesos de implementación de los acuerdos.

El Alcalde de esta ciudad fue invitado a Colombia por la Comisión de Paz del Senado para que realizara veeduría internacional y para fortalecer el apoyo al proceso de paz. El alcalde y su delegación se reunió con diferentes **organizaciones sociales, con funcionarios públicos, comunidades de diferentes territorios, víctimas del conflicto armado**, presos políticos de la cárcel la Picota, miembros de las FARC en la Zona Veredal de Icononzo en el Tolima y los gestores de paz de ELN.

**Preocupaciones de la delegación de Ginebra**

El alcalde Rémy Pagani, manifestó que, una de las mayores preocupaciones es **la demora del Gobierno de Colombia para cumplir con lo pactado en los acuerdos de paz**. “Las FARC cumplieron de manera impresionante con la dejación de armas y es hora de que el Gobierno haga su parte”. Fue enfático en que existe una preocupación por los procesos burocráticos para cumplir con lo acordado, pero “cuando se trata de enviar al ESMAD no hay demoras”. (Le puede interesar: ["Gobierno le hace "conejo" a las FARC: Andrés Paris"](https://archivo.contagioradio.com/gobierno-le-hace-conejo-a-las-farc/))

Igualmente dijo que la mesa de negociación del ELN está comprometida con llevar a cabo las negociaciones de paz, pero, están viendo de cerca el proceso de implementación con las FARC. **“Es necesario que se cumplan los acuerdos para que se evidencie un ejemplo para la impulsar a las negociaciones de la mesa de Quito”**.

Indicó además que el proceso de restitución de tierras “hace parte de las prioridades para la paz y **no se está cumpliendo con las sentencias que hay para proteger los derechos de las comunidades** para su retorno a la tierra”. Ante esto, llamó a que el Estado cumpla con sus deberes para cumplir con lo que ordenan los tribunales.

Una preocupación adicional es la **situación de inseguridad y violencia que viven los líderes sociales en diferentes territorios del país.** Pagani aseguró que “la vida de los defensores de derechos humanos se debe garantizar en la medida que son aliados importantes para la implementación de los acuerdos”. Por esto, le recordó al Gobierno que uno de sus deberes es proteger a todos los ciudadanos colombianos y en especial a los líderes sociales. (Le puede interesar: ["Paramilitares amenazan a líder por reunirse con organismos internacionales"](https://archivo.contagioradio.com/paramilitares-amenazan-a-lider/))

El alcalde hizo énfasis en que **el problema del paramilitarismo debe ser una prioridad para el Estado.** “Me sorprende que el término del paramilitarismo no se pueda usar, hablando con las comunidades y los líderes sociales pude constatar que son grupos neoparamiliatres responsables de asesinatos y amenazas”. Agregó que “no usar esta palabra demuestra falta de voluntad y protección hacia esos grupos”.

Finalmete, Pagani manifestó que **“es importante que se construya una confianza recíproca entre los ciudadanos y el Estado.** En Colombia se da prioridad a las empresas que pasan por encima de los derechos de los ciudadanos”. Manifestó que el apoyo del gobierno Suizo y la alcaldía de Ginebra continuará a pesar de quien sea el próximo presidente para que “no haya un paso atrás en el proceso de paz”.

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
