Title: "Todos tenemos que cambiar"
Date: 2017-02-17 08:55
Category: Camilo, Opinion
Tags: Pablo Beltrán, proceso de paz eln
Slug: pablo-beltran-proceso-de-paz-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Pablo-beltran.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cancilleria de Ecuador 

#### **Por[Camilo de Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)** 

###### 17 Feb 2017 

Así se expresó Pablo Beltrán, el pasado 7 de febrero en Quito, en el inicio de la fase pública de la mesa entre la guerrilla del ELN que él representa y el gobierno de Juan Manuel Santos que encabeza en la mesa Juan Camilo Restrepo.

Los cambios de las élites y de su establecimiento son mínimos y son poco esperables, espero que sí, de los que creemos en un país distinto. Que los poderosos se hagan a lo suyo, lo nuestro es lo que nos corresponde cambiar para que lo nuevo pueda surgir, la apertura mental, metodológica y el cese del vanguardismo y el basismo y el sectarismo para que lo llamado alternativo sea la paz con justicia socio ambiental.

El discurso  del ELN expresa el horizonte de su posición en la mesa e implícitamente el reconocimiento del momento político que vivimos, la posibilidad de la ruptura o la continuidad, la participación es para cambios, las conversaciones son para lograr cambios territoriales.

Este proceso con una fase exploratoria de tres años, que hasta ahora logra abrirse paso públicamente se dilató debido al desconocimiento o menosprecio de la identidad y de la historia de esta agrupación guerrillera en los cálculos del estratega de la Pax Neoliberal, Sergio Jaramillo..Para él simplemente se trataba de la  suma de un uno más un uno, FARC + ELN. El estratega se dio cuenta, tal vez tarde, que las diferencias con las FARC en métodos y percepciones, no eran solucionables con reuniones en La Habana entre las dos guerrillas.

El momento histórico que se abrió con la apuesta por la Paz cuenta en las más diversas reacciones expresadas en las calles, en el pre y posplebiscito, en las tensiones políticas en los partidos tradicionales y en los propios sectores populares por el Acuerdo Final con las FARC EP y ahora en la mesa con el ELN, la continuidad del modelo neoliberal y la altísima corrupción.

La construcción del proceso hacia la paz con la base institucional cuenta con menos oxígeno, un presidente con baja popularidad, una bancada legislativa de gobierno fragmentada, una reforma tributaria que protege el capital,  un escenario preelectoral que desconecta los escándalos de la corrupción de la paz y que ubica la paz como un distractor de la corrupción, una ebullición de nuevas formas de paramilitarismo y de nueva criminalidad es poco favorable para la discusión política que debería habilitar la mesa de Quito.

El mundo considera que la guerra interna en Colombia terminó, salvo sectores especializados que saben que no es así, por lo que la atención internacional considera que Colombia se hizo un país viable. La sociedad de los medios hace verosimil  a los ciudadanos-consumidores que el conflicto armado cesó y si hay grupos armados ilegales son los que siguen vinculados con el tráfico de drogas. Así también contribuyen a despolitizar la identidad del ELN, como lo hicieron con las FARC EP, banalizando las razones políticas de sus actuaciones.

Es preciso que todos debemos cambiar, como lo expresó Pablo Beltrán. En medio de un cambio histórico que puede nacer con vigor, en medio de la inflexibilidad del gobierno,  ese cambio posible y real tiene oportunidades sobre la base del poder conciente o la construcción de la democracia profunda distinta al establecimiento.

La democracia profunda se basa en el ser participante deliberante, que reconoce  la propia sensibilidad y la de otros, la sensibilidad inconciente que lleva a amores ciegos por nobles causas, ocultando los autoritarismos, los victimismos, el vanguardismos o basismos, falsas lealtades a presupuestos ideologizados, y descubre a los otros como seres humanos, en apuestas organizativas transformantes desde dentro y no simplemente en epidermis que se cocen con el mismo sol cómo si este no fuera distinto cada día.

Quizás lo menos esperado, y dependera de las dos guerrillas y del movimiento social que cree en la paz, estamos a puertas que se abran paso por fin al agenciamiento social de la paz con justicia socio ambiental distinta a la Pax Neoliberal.

Esta el Acuerdo Final con las FARC EP y las posibilidades de participación que este ofrece y lo que es conquistable en el proceso con el ELN en profundización participante. Creo que ambos procesos ya perciben que los cambios no vendrán de lo firmado, sí de la ciudadanía amplia y diversa, más allá de los sectores en que ambas fuerzas inciden, justo porque la justicia solo nace en las entrañas de una nueva sociedad en construcción. Continuar en los sesgos doctrinarios de unos y otros es imposibilitar la sociedad con el proyecto de país que nos merecemos.

#### [**Leer más columnas de opinión de  Camilo de las Casas**](https://archivo.contagioradio.com/camilo-de-las-casas/) 

------------------------------------------------------------------------

####  

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
