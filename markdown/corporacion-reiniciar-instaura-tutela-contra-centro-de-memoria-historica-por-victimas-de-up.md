Title: Corporación Reiniciar instaura tutela contra Centro de Memoria Histórica por víctimas de UP
Date: 2018-08-02 13:19
Category: Movilización, Nacional
Tags: Centro de Memoria Histórica, Corporación Reiniciar, Genocidio de la UP, Unión Patriótica
Slug: corporacion-reiniciar-instaura-tutela-contra-centro-de-memoria-historica-por-victimas-de-up
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/union-patriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unión Patrióticoa] 

###### [02 Agosto 2018] 

[El Juzgado 43 Penal del Circuito especializado Bogotá admitió la acción de tutela que presentó la Unión Patriótica contra el Centro Nacional de Memoria Histórica, referente al informe “Todo pasó ante nuestros ojos - el Genocidio de la Unión Patriótica 1984-2002”, **que de acuerdo con las víctimas, no fue discutidos ni concertado con ellos**. ]

En el comunicado que sale desde la organización, se afirma que "han solicitado la protección a los derechos a la verdad, la memoria y la participación de las víctimas de la Unión Patriótica, en la construcción de medidas de satisfacción y reparación" frente a la publicación del informe por parte del Centro Nacional de Memoria Histórica.

Razón por la cual señalan que es **"irrespetuoso"** que la institución demandada haya publicado la historia del Genocidio de la UP "sin que las víctimas hubiesen conocido antes cual fue el tratamiento que se le dio a los testimonios, entrevistas, documentos y otros aportes a la versión final". (Le puede interesar: ["Genocidio de la UP irá a la Corte Interamericana de DD.HH"](https://archivo.contagioradio.com/genocidio-de-la-up-ira-a-la-corte-interamericana-de-ddhh/))

De igual forma, aseguran que ese documento del Centro de Memoria será presentado al Sistema Integral de Verdad Justicia Reparación y garantías de no Repetición , **sin que previamente se hubiesen atendido los llamados que se le hicieron a la organización para "materializar el necesario diálogo**" con las víctimas.

José Antequera, quién hizo parte de la organización Hijos e Hijas, que también respalda la tutela, manifestó que "hay que abordar ese diálogo con seriedad y tiene que existir una clarificación frente a qué es lo que el Centro Nacional de Memoria Histórica ha rechazado o esta siendo problemático, más allá de la participación en abstracto, y qué es lo que las víctimas del genocidio de la Unión Patriótica están reclamando".

Asimismo aseveró que el hecho de que se haya publicado el informe sin que previamente se hayan resuelto estos inconvenientes, **"es una mala noticia"** que no permite comprender por qué una institución pública, a pesar de conocer el rechazo de las víctimas continúa haciendo visible el documento.

<iframe id="audio_27550600" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27550600_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
