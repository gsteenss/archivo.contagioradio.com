Title: Piden declarar emergencia humanitaria por asesinatos de habitantes de calle en Bogotá
Date: 2018-01-25 12:50
Category: DDHH, Nacional
Tags: asesinatos de habitantes de calle, Bogotá, Habitantes de calle, operativos Policía, policia
Slug: policia-realizo-operativo-contra-habitantes-de-calle-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Habitantes-de-calle..jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Alirio Uribe] 

###### [25 Ene 2018] 

La Red de Trabajo para la Habitabilidad en Calle denunció que en la noche de 24 de enero se realizó un operativo policial contra los habitantes de calle en Bogotá. Le pidieron a las autoridades que presente el informe sobre el operativo y que además **se declare la emergencia humanitaria** teniendo en cuenta que en lo que va del 2018 han sido asesinadas 5 personas de esta población.

De acuerdo con **Fray Gabriel Gutiérrez**, sacerdote franciscano que ha trabajado por los derechos de los habitantes de calle, “anoche hacia las 10:23 pm en el caño de la calle sexta, donde se han vuelto a organizar hace 6 meses los que hemos llamado los sobrevivientes del Bronx, la Policía intervino este lugar”.

Indicó que el operativo tenía como objetivo desmantelar una banda criminal y **“siempre hacen relación de los habitantes de calle con la criminalidad”**. Del operativo “intervinieron a 200 habitantes que se encontraban en sus asuntos”. Sin embargo, no hubo violencia pero ellos, al ver a la Policía, “se asustaron, corrieron y algunos fueron llevados a IDIPRON”. (Le puede interesar: ["Aumentan asesinatos y desapariciones de habitantes de calle en Bogotá"](https://archivo.contagioradio.com/aumentan-asesinatos-y-desapariciones-de-habitantes-de-calle-en-bogota/))

### **Acciones violentas y operativos policiales continúan** 

En varias ocasiones, quienes acompañan a los habitantes de calle, han denunciado que los controles de la Policía no se han detenido y **se han hecho de manera violenta**. Esto preocupa en la medida que no se han creado propuestas alternativas para la atención de esta población que “sigue siendo perseguida”.

Tras los operativos realizados, los habitantes de calle **son llevados a los centros de atención** como el Bacatá. Allí les hacen ofertas de los programas que tiene el Distrito, algunos ingresan a los centros para iniciar un proceso, pero otros, son atendidos en hogares de paso donde pueden estar máximo 6 horas.

### **Es necesario que se decrete emergencia sanitaria** 

Fray Gabriel recordó que el llamado a decretar la emergencia sanitaria es algo que se ha venido haciendo **desde hace varios años.** Esto servirá para poder escuchar a los habitantes de calle y así conocer sus necesidades reales para “buscar salidas que no sean de represión sino salidas humanitarias”. (Le puede interesar:["Desalojos del Bronx sólo favoreció a empresas privadas"](https://archivo.contagioradio.com/a-un-ano-de-la-retoma-del-bronx-se-evidencia-la-improvisacion-del-desalojo/))

Indicó que estas personas **necesitan ser vistas como humanos** y necesitan ser atendidas de forma diferente. “En estos últimos dos años son muchos los operativos violentos porque los quieren obligar a la fuerza a que participen de los centros del Estado”. Hay que tener en cuenta que la Corte Constitucional se pronunció sobre el derecho que tienen a habitar la calle, por esto es necesario que las calles se humanicen y haya una manera más humana de atender a esta población.

<iframe id="audio_23363192" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23363192_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
