Title: Para proteger a los líderes hay que pasar de voluntad a hechos: Diakonia
Date: 2020-02-04 17:22
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: cooperación internacional, Diakonia, Estados Unidos, lideres sociales
Slug: para-proteger-a-los-lideres-hay-que-pasar-de-voluntad-a-hechos-diakonia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/10jpg.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Archivo

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/cesar-grajales-director-nacional-premios-diakonia_md_47314061_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Cesar Grajales | Premios Diakonia

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Las y los ganadores del premio a la defensa de los DDHH que entrega Diakonía cada año, realizarán una gira de incidencia por Estados Unidos del 17 al 24 de febrero con el fin de buscar que políticos y organizaciones de ese país que exijan al Gobierno colombiano garantías para la el ejercicio de defender los DDHH y los liderazgos sociales en Colombia. (Le puede interesar: <https://archivo.contagioradio.com/tras-firma-del-acuerdo-se-han-agudizado-actos-violentos-contra-lideres/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La comisión estará integrada por los y las ganadoras del **Premio Nacional a la Defensa de los Derechos** Humanos en 2019, como la **Asociación Campesina del Valle del Río de Cimitarra**, representantes de la Consultoría para los Derechos Humanos y el Desplazamiento (**CODHES**), **Clemencia Carabalí**, presidenta de la Asociación de Mujeres Afrodescendientes del Norte de Cauca (**ASOM)**, y ** Ricardo Esquivia** quien recibió el **reconocimiento a «Toda una Vida»**.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Defensores y defensoras tendrán una agenda enfocada en la paz y las garantías de DDHH

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según **Cesar Grajales, director de Diakonia en Colombia**, la organización hará acompañamiento a los ganadores del premio en dos agendas, la primera en Washington, *"allí nos enfocaremos en los generadores de decisiones en el gobierno de Estados Unidos y consolidar su apoyo"*, y la segunda en New York donde se reunirán con instituciones de las Naciones Unidas y con las redes de iglesias y organizaciones que trabajan por los derechos humanos y la paz en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Grajales reconoció que *"el Congreso de Estados Unidos es un actor en la política internacional muy importante, especialmente en este lado del mundo"*, razón por la que según él, las voces de miembros del Congreso y funcionarios del Departamento de Estado, *"harán presión al Gobierno Colombiano para que se apliquen en serio los temas de seguridad, protección y legitimidad para los líderes, lideresas y defensores de DDHH"* .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo agregó que la agenda de Derechos Humanos es un tema también internacional, *"esto no solo compete la vida doméstica de cada país, es un asunto que las sociedades del mundo tienen derecho a saber, opinar y hacer pedidos a las autoridades"*. (Le puede interesar: <https://archivo.contagioradio.com/la-llegada-de-un-camaleon-rabioso-a-la-fiscalia/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así como la gestión de acciones de cooperación que se logren las cuales según **Cesar Grajales**, serán el objetivo de esta gira que tendrá como primer punto pedir a las autoridades de los Estados Unidos *"que sigan haciendo una clara y legítima presión al Gobierno de Colombia para que hagan acciones reales y concretas de protección*", y segundo solicitar seguimiento y control hacia las organizaciones responsables de la violencia en los territorios del país. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto según el Director de Diakonia Colombia a la luz de que los defensores en su relato y peticiones siempre piden a los gobiernos hacer esfuerzos mucho más exigentes para que las autoridades colombianas, ***"pasen de solo la preocupación y los mensajes con voluntad de proteger a los líderes, a acciones concretas y reales**". * (Le puede interesar:[](https://archivo.contagioradio.com/el-uribismo-sigue-culpando-a-las-farc-de-lo-que-pasa-en-colombia/)<https://www.justiciaypazcolombia.com/estado-general-de-la-implementacion-del-acuerdo-de-paz-en-colombia/>)  

<!-- /wp:paragraph -->

<!--EndFragment-->

<!-- wp:block {"ref":78955} /-->
