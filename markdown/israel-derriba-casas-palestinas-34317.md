Title: Israel derriba casas y desplaza familias palestinas
Date: 2017-01-05 14:00
Category: DDHH, El mundo
Tags: Demoliciones, Desplazamiento, Israel, Palestina
Slug: israel-derriba-casas-palestinas-34317
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/palestina-casas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mentiras Sionistas Wordpress] 

###### [5 Enero 2017] 

En una reciente alocución el primer ministro pelestino, Rami Hamdala, manifestó que el **Gobierno de Israel continua realizando desplazamientos forzosos de Palestinos.** Además culpó a dicho país de desplazar en menos de 72 horas a cerca de 150 palestinos en los territorios de Cisjordania y Jerusalén. Le puede interear: [ONU declara ilegales las colonias israelíes en territorio palestino](https://archivo.contagioradio.com/onu-declara-ilegales-las-colonias-israelies-territorio-palestino/)

Según el mandatario palestino, **las demoliciones de estructuras que realiza Israel "dañan a las comunidades más vulnerables y violan derechos básicos".** Así también lo ha asegurado la Oficina de la ONU para Asuntos Humanitarios (OCHA) quien manifestó que para el año anterior se demolieron cerca de 1.089 casas palestinas, lo que se tradujó en el desplazamiento de 1539 familias. Le puede interesar: [Palestina “ha escogido buscar justicia y no venganza”](https://archivo.contagioradio.com/palestina-ha-escogido-buscar-justicia-y-no-venganza/)

Hasta ahora se conoce que el Ejército de Israel ha demolido 60 casas en menos de 72 horas, lo que obligo a más de 150 personas ha abandonar estas zonas para salvaguardar sus vidas. **Para Hamdala, esta situación es grave y cree que "las demoliciones y desplazamientos incrementarán para el 2017".** Le puede interesar: [Solidaridad con Palestina debe expresarse en el fin de la ocupación](https://archivo.contagioradio.com/solidaridad-con-palestina-expresarse-en-fin-de-ocupacion/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
