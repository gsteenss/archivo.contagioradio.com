Title: Gobierno insiste en vender ISAGEN y dejar de recibir $460 mil millones anuales
Date: 2016-01-06 13:54
Category: Economía, Nacional
Tags: ISAGEN, Justicia Tributaria, Plan Nacional de Desarrollo 2014 - 2018, presupuesto colombiano
Slug: gobierno-insiste-en-vender-isagen-y-dejar-de-recibir-460-mil-millones-anuales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/isagen_1_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eluniversal 

###### [6 Ene 2016] 

Según José Roberto Acosta, integrante de Justicia Tributaria, el decreto 1609 de 2013 que autorizó la venta **viola el artículo cuarto de la Ley 2226 de 1995** que obliga a que dichos recursos se destinen al Presupuesto General de la Nación, lo que implicaría que la subasta violaría los derechos fundamentales de todos los colombianos y por ello convocó a una “tutelatón” como último recurso para salvar el bien común del país.

La venta de ISAGEN no solamente representaría la [descapitalización del Estado con un recurso que costaría cerca de 5.2 billones de pesos](https://archivo.contagioradio.com/?s=isagen). Además la venta de la empresa de energía más importante del país significaría **renunciar a la posibilidad de regular el mercado de los servicios públicos**, que en manos de las empresas privadas y extranjeras se han convertido en un negocio, según lo señala la organización Justicia Tributaria.

A pesar de la insistencia del gobierno, en **septiembre del 2014 se logró frenar la venta de la empresa** en razón de una demanda de los trabajadores y ex trabajadores que consideraban esa posibilidad como fuera de la ley y porque el argumento de la regla fiscal no sería válido, sin embargo el Consejo de Estado falló a favor de los intereses del gobierno en ese entonces y dio vía libre la subasta que se realizaría el próximo 13 de Enero.

### **Congresistas se oponen a la venta de ISAGEN** 

En las últimas semanas de Diciembre de 2015, 40 senadores colombianos enviaron una carta  a los directores ejecutivos y a **las juntas directivas de Colbún (Chile), GDF Suez (Francia) y Brookfield (Canadá)**, empresas que están inscritas en la subasta para plantear que son más de 80 los congresistas que han firmado un documento en oposición a la venta de la empresa y han interpuesto diversas acciones para oponerse a dicho procedimiento.

“Isagén es un activo estratégico del Estado colombiano, por lo cual hemos usado y seguiremos utilizando todas las herramientas legales a nuestro alcance para evitar que la privatización prospere” explica Atonio Navarro Wolf, senador del partido Verde y promotor de la iniciativa parlamentaria.

[Congresistas firman contra la venta de ISAGEN](https://es.scribd.com/doc/294754442/Congresistas-firman-contra-la-venta-de-ISAGEN "View Congresistas firman contra la venta de ISAGEN on Scribd")

<iframe id="doc_42380" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/294754442/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="70%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
