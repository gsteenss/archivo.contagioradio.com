Title: Ejército francés abusó sexualmente de niños en la República Centroafricana: ONU
Date: 2015-04-30 14:39
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Aids-Free World, Anders Kompass, Identificados soldados franceses abuso sexual, Informe ONU abusos sexuales niños en República Centroafricana, soldados franceses abuso sexual niños África
Slug: informe-de-la-onu-acusa-a-ejercito-frances-de-abusos-sexuales-en-africa
Status: published

###### Fotos:europapress.es 

Un total de **12 niños han sido abusados sexualmente por soldados franceses** y de otras nacionalidades en la **República Centroafricana,** en medio de una operación de pacificación del ejército francés en el transcurso de la **operación Sangaris.** Los** **soldados franceses abusaron a los niños a cambio de comida o incentivos monetarios en el campo de refugiados cercano al aeropuerto de la capital Bangui entre 2013 y 2014.

Para el **obispo de Bangui Dieudonné Nzapalainga es evidente que uno o más soldados están implicados**, también para la ONG **Aids-Free World** , que ha aportado parte de la investigación sobre el suceso, también hay evidencias claras de que alguno de los niños haya sido violado.

El **informe realizado por la ONU,** fue presentado al presidente de Francia en Julio de 2014, pero solo se ha conocido esta semana, gracias a Anders Kompass, miembro sueco de la Organización de las Naciones Unidas, suspendido de trabajo, quién filtró el documento por la "inoperancia" de sus superiores.

Actualmente ya han sido identificados varios soldados por la fiscalía gala que lleva el caso, en total podrían estar implicados hasta 14 de ellos. Esta mañana el presidente francés **François Hollande** declaraba:

"...*Si algunos militares se han comportado mal, **habrá sanciones a la altura** de la confianza que nosotros depositamos en el conjunto de nuestras fuerzas armadas. Porque yo estoy orgulloso de nuestro ejército y, por tanto, seré implacable con quienes se hayan comportado mal, si ha sido el caso en Centroáfrica...*”.

El suceso supone un duro revés para el ejército francés acostumbrado a intervenir en África. Muchas de las **operaciones sin consentimiento de la comunidad Internacional** o de los organismos Africanos, casos como el de Somalia, Ruanda, Congo o la ocupación militar de **Libia**.
