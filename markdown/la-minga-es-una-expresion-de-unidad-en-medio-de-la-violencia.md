Title: "La Minga es una expresión de unidad en medio de la violencia"
Date: 2020-10-17 14:29
Author: AdminContagio
Category: Actualidad, Movilización
Tags: Bogotá, campesinos, Minga, Movilzación estudiantil
Slug: la-minga-es-una-expresion-de-unidad-en-medio-de-la-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Minga-indigena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego de que este 12 de octubre La Minga, ingresara a la Ciudad de Cali con el fin de tener una reunión con el presidente Ivan Duque, **cita que fue incumplida y qué llevo a que miles de personas tomarán la decisión de movilizarse hacia Bogotá en búsqueda de una respuesta** directa por parte del Gobierno Nacional.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1316779358040842242","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1316779358040842242

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El arribo de más de 10.000 personas, luego de una recorrido durante 5 días desde el occidente del país hasta la capital colombiana, se espera que sea este lunes 19 de octubre. ([Avanza la Minga en defensa de la vida](https://archivo.contagioradio.com/la-minga-sera-en-defensa-de-la-vida-el-territorio-la-democracia-y-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este recorrido pretende lograr en la ciudad de Bogotá una reunión con el presidente Iván Duque, para exigir que se cumplan los acuerdos firmados durante el 2019 entorno a las garantías de vida y territorio para comunidades indígenas, afro y campesinas; así como el cumplimiento del [Acuerdo de Paz firmado](https://archivo.contagioradio.com/lets-defend-colombia-una-agenda-comun-de-movilizacion-social/) en el 2016.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Ante el incumplimiento histórico, La Minga resiste

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Edwin Capaz, Coordinador de Derechos Humanos de la Asociación de Cabildos Indígenas del Norte del Cauca (ACIN)**, señaló que pese a que han recibido el respaldo por parte de diferentes sectores sociales, también **es un reto no solamente movilizar de manera segura a la cantidad de personas que conforman esta Minga , sino también afrontar *"la estigmatización y criminalización*** *que atraviesa la protesta social y especialmente está que es encabezada por las comunidades indígenas".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Situación que no es ajena para el movimiento indígena, ni tampoco para muchos sectores sociales, "*siempre se ha tratado de patrones de conducta institucional por parte del Estado, con el objetivo de estigmatizar o* ***crear escenarios que les puedan ser útiles para la represión violenta hacia las movilizaciones***", destacó Capaz

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Capaz agreó también que estos patrones se dividen en diferentes puntos, el primero según el defensor es el *"**movimiento estratégico que inician en medios de comunicación,** en donde diferentes mandos militares y funcionarios del Gobierno crean unas cortinas mediáticas"*, el caso más reciente giró entorno a infiltraciones en la Minga de grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En segundo lugar señaló que parte de las judicializaciones o implicaciones legales en contra de la Minga han sido montajes, "***conocemos casos en donde militares intentan ingresar a esos escenarios de movilización social armamentos de manera camuflada**, eso ya lo hemos vivido y la justicia indígena ha podido salir de este tipo de implicaciones".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y destacó que el tercer punto es, *"la estrategia de crear por medio de la justicia ordinaria, escenarios en donde se filtran audios o videos entorno a la creación de escenarios de justificación de los actos de violencia y represión por parte principalmente del Esmad"* ([Tras tres años de impunidad, la revictimización persiste en El Tandil](https://archivo.contagioradio.com/tras-tres-anos-de-impunidad-la-revictimizacion-persiste-en-el-tandil/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte Joe Sauca, coordinador de Derechos Humanos del Consejo Regional Indígena del Cauca ([CRIC](https://www.cric-colombia.org/portal/la-palabra-de-la-minga-fortalece-la-resistencia/)), señaló que la respuesta que han tenido por parte del gobierno desde que esperaba la asistencia de este en la Ciudad de Cali es *"la dinámica del gobierno que no se presta para el diálogo, un gobierno que siempre ha tenido los intereses parcializados hacia gobernar un sector de la sociedad olvidando las realidades territoriales".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **"*Nos señala cómo grupos al margen de la ley, generando polarización qué distraiga la atención de los reclamos justos que estamos haciendo desde las comunidades".***
>
> <cite>Joe Sauca , Coordinador de Derechos Humanos del CRIC</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### La Minga es de todos y todas, no sólo de los indígenas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Al mismo tiempo Sauca qué resaltó que, **rechazan lo que está pasando en entorno a la lectura mediática y por parte del Gobierno de lo que significa la Minga,** *"nosotros hemos sido claros en los espacios públicos, resaltando las razones por las cuales se han llevado a cabo las diferentes Mingas y especialmente esta, que es por la defensa de la vida y la paz, y por ello se se nutre de diferentes sectores sociales, populares, universitarios y sindicales"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo el vocero de la ACIN, **Edwin Capaz**, destacó que **la Minga es el reflejo de una realidad que cruza al país, frente al nivel de asesinatos que ocurren,** y "*el nivel de desconexión que hay entre el Gobierno y el cumplimiento de los Derechos, la toma de poderes en todas las instancias del Estado y el nivel de incumplimiento del Acuerdo de Paz"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ademas de hechos cómo, la represión estatal a las movilizaciones sociales estudiantiles y de sectores sociales entre otros contextos que no son solamente indígenas sino urbanos y de diferentes sectores son los que según Capaz, *"los nos unen en esta Miga".* ([Gobierno gastará \$13.800 millones en ESMAD y Policía](https://archivo.contagioradio.com/gobierno-gastara-13-800-millones-en-esmad-y-policia/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El riesgo de defender la vida

<!-- /wp:heading -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/113348540811/videos/690720165113515","type":"rich","providerNameSlug":"embed-handler","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-rich is-provider-embed-handler">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/113348540811/videos/690720165113515

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

Por otro lado **Milton Conda, delegado de la Comisión Nacional de Territorios Indígenas,** destacó que el ejercicio en defensa de la vida la madre tierray la exigencia colectiva de los derechos no solamente de los pueblos indígenas sino de todos los sectores sociales

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hablamos de sectores sociales porque cuando se afecta la vida se afecta también un colectivo Y eso es lo que nosotros estamos defendiendo

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

y agregó que pese a los diferentes de riesgos que representan no solamente salir a movilizarse fuera de su territorio si no existiera desde sus resguardos Y es de diferentes comunidades el cumplimiento de los acuerdos pactados históricamente, resaltó que esta lucha no es silenciar mucho menos termina cuando se presentan amenazas estigmatizaciones e incluso cuando se apaga la vida de quienes luchan por ella, "nuestra lucha No termina con la oleada de violencia que continúa en Colombia escuda en la pandemia para poder continuar todos los proyectos extractivos en los territorios"

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Día a día matan compañeros y compañeras y no sabemos cuál es la realidad más allá específicamente pero si algo muy claro Y es que avanza el tema de la extracción el despojo y de invisibilizar una realidad en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Más de 1300 acuerdos pactados con el gobierno serán exigidos este lunes

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "En la Minga todos y todas nos cuidamos"

<!-- /wp:heading -->

<!-- wp:paragraph -->

En este aspecto Edwin Capaz, resaltó que el aprendizaje que hoy han logrado luego de las diferentes Mingas realizadas les ha costado bastante, *"estos aprendizajes, especialmente **en los últimos 10 años en este accionar de declamación derecho nos ha dejado ver no solamente la estrategia del Estado en negar y ocultar las razones por las que reclamamos**".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También, destaca que años de movilización les ha entregando como aprendizaje, *"l**a unidad comunitaria, el trabajo entre organizaciones sociales y reencontrarnos con los diferentes sectores del país que pese a su situación particular vive en contextos similares** al nuestro"* ([Contra viento y marea la minga sigue exigiendo diálogo con Duque en Bogotá](https://archivo.contagioradio.com/7-000-indigenas-de-la-minga-buscan-un-dialogo-con-duque-en-bogota/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**Esta movilización lo que nos ha permitido es conocer al país y escuchar a todos lo que se suman de manera directa o indirecta a la Minga**, y esta lucha en rechazo a el sinnúmero de cosas que están mal en Colombia y qué depende un cambio de una constante de reclamación por parte de nosotros y nosotras"*
>
> <cite>**Edwin Capaz, Coordinador de Derechos Humanos de la ACIN**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Finalmente los dos voceros coincidieron en que **ésta no va a ser la última movilización encabezada por el movimiento indígena**, y que además se recogerán en las diferentes acciones sociales en las calles que se están organizando para los próximos meses. ([Otra Mirada: Se reactiva la movilización social en Colombia](https://archivo.contagioradio.com/otra-mirada-se-reactiva-la-movilizacion-social-en-colombia/))

<!-- /wp:paragraph -->
