Title: Se agrava crisis de derechos humanos en el Cauca
Date: 2016-02-19 18:39
Category: DDHH, Nacional
Tags: Cauca, Fuerzas militares, marcha patriotica, Red de DDHH Francisco Isaías Cifuentes, Situación de DDHH
Slug: se-agrava-crisis-de-derechos-humanos-en-el-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/IMG_0383.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[Foto: Remap Valle]

[12 Feb 2016]

[La crisis de derechos humanos en el departamento del Cauca](https://archivo.contagioradio.com/mineria-amenaza-en-cauca/) sigue siendo alarmante y motivo de denuncia por parte de diferentes organizaciones de la sociedad civil como la Red de Derechos Humanos del Suroccidente Colombiano “Francisco Isaías Cifuentes” y la Comisión de Derechos Humanos del Movimiento Político y Social Marcha Patriótica.

**Según las denuncias los hechos registrados en varios municipios del Cauca involucran al Ejército Nacional**, quién ha desplegado una serie de acciones irregulares en la zona en contra de la comunidad y los procesos organizativos que allí se desarrollan. También se ha evidenciado la[presencia de grupos paramilitares por medio de panfletos amenazantes](https://archivo.contagioradio.com/de-cada-100-defensores-de-ddhh-rurales-86-han-sido-agredidos/) que ponen en riesgo la vida, la integridad y la seguridad de la población.

La Red de DDHH indica que el 31 de enero en el municipio de Guapi, corregimiento del Bajo Guapi, vereda de Calle Honda, miembros del Batallón Fluvial de Infantería de Marina No.42 interceptaron a **Perfecto Anchico Orobio** cuando éste se disponía a regresar a su casa luego de una jornada de trabajo; le fueron propinados **tres impactos de arma, causándole la muerte.**

El 9 de febrero se registraron amenazas de muerte mediante panfletos suscritos por [el grupo paramilitar “Los Urabeños”](https://archivo.contagioradio.com/panfleto-amenazante-anuncia-limpieza-social-turbo-antioquia/) contra los habitantes del municipio de El Bordo Patía. El mismo día se presentan hostigamientos y rondas irregulares de un **integrante de la red de informantes del Ejército Nacional, quién a su vez ha sido señalado de cometer hurtos, extorsiones, amenazas, torturas, homicidios y violaciones**. Dicho hecho pone en riesgo la vida, la integridad y la seguridad de Gerardo y James Barona Avirama, habitantes del corregimiento e integrantes de varias asociaciones de El Palo, Municipio de Caloto.

La última acción irregular denunciada por la población civil hasta la fecha ocurrió el día 11 de febrero en el municipio de Cajibio donde militares del Batallón de Infantería No.7 “Gr. José Hilario López” realizaron un [proceso de erradicación forzada de cultivos de uso ilícito](https://archivo.contagioradio.com/campesinos-del-catatumbo-denuncian-nuevos-incumplimientos-del-gobierno/), en lo que fue calificado por los propios campesinos como una inminente vulneración de Derechos Humanos en contra diferentes asociaciones que lideran procesos en la región.
