Title: Compitamos... pero bajo las mismas reglas
Date: 2016-06-01 10:21
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: transporte, Uber
Slug: compitamos-pero-bajo-las-mismas-reglas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/no-mas-uber.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Fernando Vergara 

#### **Por [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 1 Jun 2016 

[Los debates entre ciudadanos sobre el tema de UBER y taxistas, me hacen recordar algo simple, triste pero cierto: la violencia comienza ante los vacíos que generan las discusiones por temas diferentes.]

[Bueno… tenemos por un lado una gran cantidad de personas alegando por el servicio, otros alegando por el tema legal, otros alegando por el problema de la permisividad a las multinacionales y otros alegando por el incremento de la violencia de parte y parte. Este debate, muy mal planteado ¡como siempre! desde los medios masivos.]

[Este tema (como todo en Colombia) tiene tres caras, ocho brazos, y múltiples respuestas.  El litigio entre taxistas y UBER ya ha comenzado a generar violencia, UBER tiene en sus manos, las voces justas de miles de usuarios que piden un mejor servicio, pero con esto está sacando un provecho que supera el tema de un buen servicio y se mete en el campo del desafío al sistema legal del Estado colombiano y además entra en la misma lógica en la que opera cualquier multinacional, la lógica de las utilidades por encima de todo.  ]

[Lo grave es que pronto ocurrirán tragedias, por una parte, entre taxistas que cansados de pagar mucho más impuestos que un carro afiliado a UBER, han comenzado a realizar ataques violentos contra los carros blancos, y, de otra parte, los conductores de Uber que también han comenzado a atacar carros amarillos ante la violencia que se les ha venido encima.]

[Si por lo menos nos damos a la tarea de comprender el fenómeno con algo más que frases de cajón o sentimientos de odio porque un día “el taxista no me quiso llevar” tal vez podamos aportar a la desmitificación de un escenario para la violencia, porque en serio ¡qué mamera! en Colombia no salimos de una para meternos en otra, y nos gusta criticar, pero nos cuesta mucho comprender tanto lío.]

[El tema de UBER y taxistas, tiene tres vías de comprensión: 1) el servicio 2) la legalidad y 3) el modelo económico transnacional.]

[Frente a la primera vía. Mire, la verdad y sin generalizar lo que voy a escribir, puesto que hay taxistas con carro amarillo en verdad amables y admirables por su paciencia con la locura del tráfico y la indecencia de algunos clientes; en varias ocasiones muchos deciden no realizar la carrera, en ocasiones preguntan para dónde va el pasajero, algunos son agresivos, pelan en montonera y otros ni cuidan el estado de los vehículos. ¿Qué eso es real? Si ¿Qué son todos? No.  El problema del servicio, es el campo lícito de una competencia empresarial. Los usuarios, sin duda alguna, tenemos derecho a escoger un mejor servicio.]

[Frente a la segunda vía. UBER no es una plataforma tecnológica. UBER es una empresa multinacional, que, por medio de la plataforma tecnológica, inscribe vehículos que prestan servicio de transporte público. UBER se lava las manos diciendo que ellos no son empresa de transporte, creyendo que la “astucia” de maquillar una razón social no sería develada por nadie. UBER tiene todo el derecho de competir con un mejor servicio, pero debemos tener claro que esta multinacional no está compitiendo ofreciendo un servicio de “plataforma tecnológica” sino efectivamente compite ¡ofreciendo transporte de personas! si UBER no trasportara personas, su plataforma no generaría riqueza. Las personas escogen UBER no por usar una plataforma, sino por la necesidad del servicio de transporte, la necesidad de que un carro las transporte sin que el conductor diga “por allá no voy”, por eso la gente escoge UBER, por eso su razón social real, es la del servicio de transporte.]

[Por tal motivo, UBER constituye una ilegalidad, no como plataforma, sino como prestadora de servicio de transporte no constituida. La gente usa UBER para transportarse, no para usar una plataforma tecnológica. Las personas que debaten en este campo, están en todo el meollo del asunto, pues esto es lo que genera inequidad legal en las formas de competencia; los cupos, los impuestos, los seguros para los taxis amarillos son tremendamente altos frente a los de los carros inscritos en UBER. El tema puede tener solución: ajustando la norma y los impuestos a todos por igual. O UBER se constituye bajo las normas del transporte, o bien, easy taxi, tapsi y demás aplicaciones, que se dediquen a hacer lo mismo, que les rebajen los cupos a los amarillos a precio de UBER ¡y listo!... Que compitan legalmente servicio contra servicio.]

[Frente al tercer tema. Creo que la ideología neoliberal, también tiene velas en este entierro. Con el tema del emprendimiento, de ser atractivos para la inversión internacional, de modernizar con nuevas tecnologías, de seguir ese discurso trasnochado del “progreso” o de que la ley sí debe flexibilizarse con las multinacionales, pero endurecerse para los nacionales, no podemos derogar los compromisos que una empresa transnacional debería cumplir (como todas las empresas colombianas) con el Estado y su sistema fiscal aduanero.]

[Permitirle a UBER que funcione con una razón social errada en el nombre del progreso y el emprendimiento, es ser ciegos ante la realidad del país, es olvidar que en el nombre del progreso, se quebró el campo colombiano, que en el nombre del emprendimiento la entrega del dinero público a las empresas privadas ha terminado en los escándalos más vergonzosos de corrupción; es darle la razón a Echeverry para que con pataletas groseras, demerite la apreciación del ambientalismo técnico sobre el tema de la explotación de petróleo…]

[¿Acaso sabemos todos los impuestos que tiene que pagar un empresario colombiano para poder competir dignamente contra las multinacionales? Si no lo sabe, deje de ser escudero de UBER y pregúntese y ayude a pensar cómo, no sólo para una multinacional, sino PARA TODOS, las cosas podrían facilitarse de una manera más equitativa… y sabe qué ¡¡¡deje pelear con su vecino, más bien salgan al trabajo en bicicleta!!!]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
