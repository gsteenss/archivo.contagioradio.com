Title: 3 meses de violenta represión contra la movilización en Francia
Date: 2016-06-15 13:01
Category: El mundo, Movilización
Tags: abuso policial Francia, rechazo a reforma laboral francia, reforma laboral Francia, represión contra sindicatos Francia
Slug: 3-meses-de-violenta-represion-contra-la-movilizacion-en-francia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Protestas-Francia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Telesur ] 

###### [15 Junio 2016 ] 

Desde principios de marzo, sindicatos y organizaciones sociales de Francia se movilizan en rechazo a la reforma laboral que actualmente está en discusión en el parlamento y que implicaría **incrementos en la jornada laboral de entre 25 y 30 horas, no pagos de las horas extras,** **despidos directos de trabajadores, reducción de permisos y tiempos vacacionales**, y desconocimiento de las garantías conquistadas en el sector laboral.

Durante las jornadas de movilización que ya completan su novena versión, los ciudadanos han sido violentamente reprimidos por la fuerza pública, el más reciente hecho ocurrió el pasado martes, cuando **por lo menos 1 millón de personas que participaban de una concentración en la Plaza Italia de París, fueron atacadas por la Policía con granadas, bolillos y gases lacrimógenos**, por lo menos 40 franceses resultaron heridos y 44 detenidos.

Las protestas se han realizado **en París y en otras 200 ciudades francesas en las que también ha habido represión por parte de la Policía**, y pese** **a que François Hollande, anunció este miércoles que prohibirá estas manifestaciones por no garantizar "la preservación de los bienes, de las personas y de los bienes públicos", las organizaciones sindicales aseguran que se mantendrán en las calles ante un [[gobierno aparentemente de izquierda pero con grandes contradicciones](https://archivo.contagioradio.com/persiste-movilizacion-contra-la-reforma-laboral-en-francia/)].

\

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

 
