Title: "Marginal Compaz", hip hop Bogotano en "A la Calle"
Date: 2015-06-08 09:00
Category: Sonidos Urbanos
Slug: marginal-compaz-hip-hop-bogotano-en-a-la-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/10556300_263873363813307_7253008460408336854_n-e1433534230308.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### <iframe src="http://www.ivoox.com/player_ek_4601849_2_1.html?data=lZudk52YfY6ZmKiakpaJd6Kkk4qgo5mWcYarpJKfj4qbh46kjoqkpZKUcYarpJK6w9fLrc%2FVzZDQ0dLUpduhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Marginal Compaz, invitados en "A la Calle"] 

Marginal Compaz es una agrupación bogotana de hip hop que tiene su cuna en el  **colectivo "Rapjudezco**". Expresan una postura crítica en sus letras y rimas exponiendo las diferentes problemáticas actuales e históricas de la capital y del país. Se caracterizan por la **fuerza, la unión, y el respeto**. Uno de sus objetivos es **cambiar el concepto de los "barrios bajos"** incentivando además a los niños interesados en la música haciéndolos participes de sus eventos.

\[caption id="attachment\_9823" align="aligncenter" width="1238"\][![IMG-20150605-WA0000](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/IMG-20150605-WA0000-e1433533766105.jpg){.wp-image-9823 .size-full width="1238" height="787"}](https://archivo.contagioradio.com/?attachment_id=9823) Marginal Compaz con Angel y Kevin de "A la Calle"\[/caption\]
