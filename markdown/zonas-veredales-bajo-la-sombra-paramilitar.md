Title: Zonas Veredales bajo la sombra paramilitar
Date: 2017-01-11 15:27
Category: Otra Mirada, Paz
Tags: Autodefensas Gaitanistas, Implementación de Acuerdos, Presencia Paramilitar, Zonas Veredales Transitorias de Normalización
Slug: zonas-veredales-bajo-la-sombra-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/zona-veredal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia] 

###### [11 Enero 2017] 

Organizaciones defensoras de derechos humanos, comunidades indígenas, afro, campesinas, entidades territoriales y la delegación de las FARC, han denunciado que la presencia de grupos paramilitares en cercanías a las Zonas de Preagrupamiento y Zonas Veredales Transitorias –ZVTN– **pone en grave riesgo la vida e integridad de los combatientes y las comunidades aledañas, además de la plena implementación de los acuerdos.**

Las delegaciones de paz, han informado que en los departamentos donde estarán ubicadas las ZVTN y los campamentos serán en los departamentos de Antioquia, Cesar, Norte de Santander, Tolima, Arauca, Chocó, Córdoba, Cauca, Guaviare, Putumayo, Guainía, Meta, Caquetá y Nariño. Antioquia, Chocó, Cauca, Nariño y Guaviare son algunos de los departamentos donde se han presentado públicamente las denuncias de presencia paramilitar.

Algunos guerrilleros han manifestado que temen por su vida, la de sus familiares e integrantes de las comunidades, por lo que en la ZVTN de Monteloro en Antioquia, ya se presentó la deserción de 4 integrantes de las FARC.

Además, el IPC advirtió que “hay una estrategia de reocupación de estructuras paramilitares en esas zonas que coindicen con las zonas veredales transitorias, eso pone en alto riesgo al proceso y a las comunidades y líderes que están entorno de los puntos campamentarios”.

### **Antioquia** 

La Gobernación de Antioquia realizó una visita a las ZVTN en la vereda Llanogrande, de Dabeiba, así como a los puntos de preagrupamiento temporal de Monteloro, Alto de Urama y Camparrusia. El posterior informe, reveló que hay una **falta de presencia de Policía y Ejército para la vigilancia en los sitios y la creciente zozobra de los guerrilleros** por posibles ataques paramilitares.

En la ZVTN de Llanogrande se espera tener concentrados a 200 guerrilleros al mando del comandante Isaías, 82 de los cuales están preagrupados en Murindó. El informe señala que en el sitio “faltan equipos de comunicación, no hay señal de internet ni sistema de abastecimiento eléctrico, pero sí agua potable y vía de acceso.

Además, resalta que **“debería haber dormitorios para 30 personas y solo hay para 12** (…) y el contrato de arrendamiento para construir la sede de las Farc no se ha formalizado.

En la de Monteloro, hay concentrados 45 gruerrilleros, hay 4 menores y 3 lactantes, además de varios lesionados de guerra. Fue en esta Zona donde se registraron las cuatro deserciones y no hay Policía ni Ejército, **“el peor enemigo de la paz es el tiempo, porque puede significar una alta deserción”**, advierte la gobernación en el informe.

En el Alto de Urama, se encuentran preagrupados 62 guerrilleros, 16 mujeres y 46 hombres, entre ellos hay cuatro menores de edad que esperan ser entregados al ICBF. Indica el informe que “durante el día se dedican a estudiar el acuerdo de paz, realizan actividades de alfabetización y cuidan su propia granja”. También destaca la ausencia de Ejército y Policía en el área cercana y de igual forma los miembros de las Farc expresan temor de ser agredidos por paramilitares.

### **Cauca** 

En este departamento, Consejos Comunitarios, Resguardos Indígenas y mandos guerrilleros han denunciado hostigamientos de grupos paramilitares a través de pintas y panfletos puestos en zonas aledañas a las ZVTN.

Además, guerrilleros denuncian que hasta el momento no ha sido adecuada la ZVTN que estaría ubicada en la vereda La Elvira en el municipio de Buenos Aires.

### **Córdoba** 

** **Andrés Chica vocero de la Asociación de Campesinos del Sur de Córdoba ASCSUCOR, ha manifestado que “con la ausencia de instituciones estatales y el pre agrupamiento de las FARC en el territorio se ha aumentado la presencia de paramilitares (…) **en una semana pasaron de ser 160 hombres a 400 paramilitares que andan atemorizando y rondando** por Tierralta, Tierradentro, Puerto Libertador y Montelibano”.

Además, el vocero dijo que “los paramilitares nunca han salido de Córdoba, por el contrario, se han fortalecido y todo ante los ojos del ejército y las instituciones del Estado, el campesinado está sólo y ¿quien va a contrarrestar a esos grupos?**”.**

### **Chocó** 

A través de un comunicado entregado por la Comisión Intereclesial de Justicia y Paz, se dio a conocer que en Diciembre de 2016 en las comunidades de Balgasira y Bocachica, territorio colectivo de Cacarica en el Chocó, **fue registrado un grupo de aproximadamente 300 paramilitares de las Autodefensas Gaitanistas de Colombia.**

En los últimos meses, comunidades de la cuenca del Río Salaquí y de la cuenca del Río Cacarica en el Bajo Atrato Chocoano, han denunciado el “crecimiento de la presencia por parte de paramilitares, así como las constantes amenazas de ingresar las Zonas Humanitarias ubicadas en las comunidades de Nueva Vida y Nueva Esperanza en la cuenca del Cacarica”.

![INFOGRAFIA CORRECION](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/INFOGRAFIA-CORRECION.jpg){.alignnone .size-full .wp-image-34725 width="620" height="848"}

### **Guaviare** 

En este departamento al sur del país, las FARC denunció la presencia de paramilitares en el municipio de El Retorno. “Con preocupación hemos venido recibiendo informes de las comunidades de las veredas Santa Bárbara, Mirolindo, El Unilla, La Cristalina, San Lucas, Pueblo Seco, La Reforma y San Miguel sobre **la presencia de un grupo de 60 paramilitares de Los Urabeños, al mando de Arnuvio Casanova”**, aseguraron miembros de las Farc en un comunicado.

En el informe, las FARC destacan a dos paramilitares conocidos con los alias de ‘el Costeño’ y ‘Florida’ quienes afirman públicamente que **“el objetivo es mantener el registro de todas las personas que entran o salen de la zona** y que se dirigen hacia el punto de preagrupamiento de San Miguel”

### **Nariño** 

En zonas cercanas al sector de El Playón, uno de los puntos de preagrupamiento en Tumaco, comunidades han denunciado la presencia de tres grupos paramilitares.

Frente a ello **el ministro de Defensa Luis Carlos Villegas, durante una rueda de prensa, negó las denuncias e indicó que “allí solo existe una lucha del Estado en contra el delito”.**

La denuncia de la comunidad asegura que un grupo de entre 50 y 80 paramilitares actualmente opera en las veredas Pital y el Firme de Tumaco, han dicho que portan armas largas y camuflado, y están al mando de alias Cuzumbo, 'Olindillo' y 'Titano'.

Agregan que "otro grupo de 40 paramilitares de los Urabeños se encuentra en la vereda el Seivito, a cinco minutos de la vía Tumaco-Pasto (…) también otro grupo que se hace llamar 'Los Negritos', compuesto por unos 30 hombres, se ubica en la vereda el Descolgadero sobre las bocanas del río Mira".

En dicha ZVTN estarían concentrados cerca 160 guerrilleros de la columna móvil Daniel Aldana y Mariscal Sucre.

Por ultimo, sobre los recientes casos de amenazas, hostigamientos y asesinatos, que organizaciones de derechos humanos y comunidades han señalado fueron a manos de paramilitares, el ministro de defensa dijo que **"en Colombia no hay paramilitarismo. Decir que en Colombia hay paramilitarismo significaría otorgar un reconocimiento político a unos bandidos** dedicados a la delincuencia común u organizada".

###### Reciba toda la información de Contagio Radio en [[su correo]
