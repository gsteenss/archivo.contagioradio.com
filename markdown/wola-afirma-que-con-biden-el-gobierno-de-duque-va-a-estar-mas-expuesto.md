Title: WOLA afirma que con Biden el gobierno de Duque va a estar más expuesto
Date: 2020-11-13 18:13
Author: AdminContagio
Category: Actualidad, El mundo, Otra Mirada
Tags: acuerdo de paz, Biden, Duque
Slug: wola-afirma-que-con-biden-el-gobierno-de-duque-va-a-estar-mas-expuesto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Duque-y-Biden-foto-Presidencia.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*FOTO: @infopresidencia*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras en Estados Unidos se define la ruta que tomará la presidencia del demócrata Joe Biden juntos a la vicepresidencia de Kamala Harris, **en Colombia se hace una primera lectura de esta decisión como un posible aliado para la continuidad y respaldo internacional del Acuerdo de Paz**, un accionar que va desde lo conceptual a lo territorial.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/angelamrobledo/status/1325139218385940482","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/angelamrobledo/status/1325139218385940482

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El cambio de Gobierno en EE.UU se da luego **años en donde el país ha sido víctima de amenazas para detener la cooperación, en medio de una lluvia de críticas ante el proceso fallido de lucha en contra del narcotráfico**, así como otros puntos que han puesto en riesgo las relaciones bilaterales entre Estados Unidos y Colombia. ([En dos años del Gobierno Duque se ha derrumbado institucionalidad](https://archivo.contagioradio.com/en-dos-anos-del-gobierno-duque-se-ha-derrumbado-institucionalidad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Situación que abre la puerta a diferentes preguntas sobre el impacto del gobierno de Joe Biden en Colombia , **luego de que la continuación de administraciones como la de Trump y Duque, dejarán grandes impactos en las comunidades rurales y con una amplia vulneración a los Derechos Humanos**. ([“36 niños muertos por balas oficiales”: Roy Barreras](https://archivo.contagioradio.com/36-ninos-muertos-por-balas-oficiales-roy-barreras/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**[Adam Isacson,](http://wola.org/es/programa/colombia/(abre%20en%20una%20nueva%20pestaña)) Director para Veeduría de Defensa en la Oficina de Washington para Asuntos Latinoamericanos (WOLA),** señala que pese al *"sistema democrático raquítico que tiene Estados Unidos, este aún funciona para poder llevar a cabo acciones que dignifiquen la vida no solamente al interior del país sino en materia de relaciones exteriores"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por tanto este cambio permite, según Isacson, abrir la oportunidad a cuatro años donde se espera no solamente *"responder a las promesas incumplidas desde el gobierno de Obama sino fortalecer la democracia al interior de los Estados Unidos".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Principales retos de Biden en Estados Unidos

<!-- /wp:heading -->

<!-- wp:paragraph -->

Teniendo presente que los cambios se deben construir al interior para poder llevar al exterior, Adam Isacson señala que los puntos claves están en pasos como, **fortalecer el sistema de asilo a migrantes y refugiados**, trabajar por acabar con el **racismo sistemático, que se venía dando desde el 2014** y se incrementó durante el gobierno de Donald Trump. ([George Floyd y los tres detonantes de las protestas en EE.UU](https://archivo.contagioradio.com/george-floyd-y-los-tres-detonantes-de-las-protestas-en-ee-uu/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así como el fortalecimiento del sistema Nacional de Salud, *"el gobierno saliente no tuvo una política de salud en torno a la pandemia por tanto Estados Unidos iba con un rumbo como Brasil con la administración de bolsonaro".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "En Colombia Biden no dejará pasar tantas cosas como Trump, exigirá respuestas": **Adam Isacson**, WOLA

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En cuanto a la incidencia en Colombia Isacson indicó que, el tema con mayores pendientes y que **decayó durante el gobierno de Trump, en materia de acciones diplomáticas y en ocasiones presupuestales, fue el Acuerdo de Paz** y su implementación.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"El gobierno de Trump dejó de lado ese tema, dejó de presionar al gobierno de Duque por el ritmo de la implementación"*
>
> <cite>**Adam Isacson, Director para Veeduría de Defensa en WOLA**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por otro lado mencionó que un tema como las intervenciones ilegales a periodistas, defensores de Derechos Humanos y representantes políticos, así como las carpetas secretas en el Ejército, ***"hace parte del patrón de deterioro al interior del Ejército de Colombia con la llegada del gobierno de Duque"*.** ([Policía, paramilitares y ejército son los mayores violadores de DDHH según el CINEP](https://archivo.contagioradio.com/primer-semestre-2020-mayores-violaciones-dd-hh-cometidas-policia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un accionar que según el representante WOLA, por parte de Trump se mantuvo en un total silencio, *"un silencio que se extiende a las masacres, asesinatos de excombatientes por parte de la fuerza pública y violaciones de mujeres. **Claros ejemplos de la ausencia de Trump ante este evidente cambio de rumbo de las fuerzas militares en Colombia".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Llamo también la atención que desde WOLA han visto como en los últimos meses desde adaptando a La retórica de campaña que adoptó de los uribistas empezó a hablar de un nefasto pacto de santos obana con los narcotraficantes criticando de manera directa el mismo pueblo"

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y enfatizó en que organizaciones como WOLA esperan que el gobierno de Biden , no mantenga tanto silencio, *"eso va a ser un tema clave en la relación militar qué existe entre los dos países, este expresar de una opinión más crítica del rumbo del Ejército y seguramente sobre algunos oficiales en especial"*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Si hubiese ganado Trump, veríamos en Washington un gobierno con tinte uribista tratando de hacer trizas el Acuerdo de Paz"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Isacson, agregó que más allá de la omisión ante el actual militar en Colombia adoptada por Estados Unidos, están las campañas uribistas que se dieron al interior, como fue el caso del estado de Florida, ***"en donde se transmitían mensajes políticos que fueron adoptados por Trump y los candidatos al congreso".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En donde ligaban a los demócratas con el comunismo y el castrochavismo,"una retórica que tiene su origen en Bogotá, y es el resultado de que los republicanos arrasaran en el sur de la Florida, a tal punto que **uno de los candidatos agradeció en su discurso de triunfo a Álvaro Uribe**".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la participación del uribismo en las elecciones de Estados Unidos, el vocero de WOLA señala que a pesar de que las relaciones tanto militares como diplomáticas entre los dos países se van a mantener, el gobierno actual no será tan flexible, y *"**van a hacer todo lo posible para estorbar a los uribistas".***

<!-- /wp:paragraph -->

<!-- wp:heading -->

La incursión de tropas estadounidenses no se detendrá en Colombia: WOLA
-----------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Finalmente añadió que así como existirán claros cambios hay puntos que no se van a modificar, uno de ellos es la presencia del ejército estadounidense en Colombia, **"es claro que van a haber cambios de tono en Colombia con la llegada de Biden, pero en frente a las actividades militares o accionar que trascienda lo ideológica no veremos mucho"**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y recordó que el tema de la cooperación internacional es algo que siempre ha apoyado Biden , ejemplo de ello es el reconocimiento en su apoyo incondicional a Colombia, durante lo 40 años en el senado, pero también **fue un gran impulsor del [Plan Colombia](https://archivo.contagioradio.com/la-deuda-historica-con-sus-hijos-e-hijas-desparecidos-as/), con la incursión de tropas norteamericanas en Colombia**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo las actividades de la DEA, que no detendrán sus investigaciones, o en casos mas alarmantes la continuación de las acciones de erradicación forzada, *"lo mas probable es que estas continúen, hace algunos años habían cerca de 24 avionetas operando en Colombia, hoy hay 8 esperando despegar bajo las ordenes de la Corte Constitucional, **al Gobierno de Biden no lo veo deteniendo esto, y si el país quiere fumigar desde Estados Unidos se le apoyará".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pesé a todo este panorama señaló que **un gran reto va a ser que los oficiales que ingresen al país especialmente a los territorios**, tendrán que tener claridad para ejecutar estos planes de políticas antidrogas, *"que deben ir de la mano con la opinión de las comunidades y no como algo impuesto desde un escritorio en Bogotá*"

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En los próximos meses el demócrata Joe Biden de 77 años, junto a su fórmula presidencial Kamala Harris, se enfrentarán a un gobierno con múltiples retractores, en un momento en el que también Estados Unidos atraviesa una crisis económica y de salud, junto a grandes expectativas de múltiples países que como Colombia esperan que haya un cambio en materia de la defensa de los Derechos Humanos y no solamente de palabras sino en acciónes.

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://youtu.be/5OiRfOwsXi8","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://youtu.be/5OiRfOwsXi8

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
