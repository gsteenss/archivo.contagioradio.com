Title: Continúa persecución política contra la UP
Date: 2015-08-23 00:42
Category: Nacional, Política
Tags: Aida Avella, Elecciones 2015, Falsos Positivos Judiciales, montajes judiciales, persecución política, Unión Patriótica
Slug: continua-la-persecucion-politica-contra-la-up
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/foto2653.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [vozpueblocom.wordpress.com]

###### [22 Ago 2015] 

A través de un comunicado, la **Unión Patriótica denuncia que continúa la persecución política contra su movimiento**, esta vez se trata de **Marino Greuso,** quien es integrante de la UP y candidato  a la Alcaldía de Guapi, Cauca.

De acuerdo con la denuncia, el pasado 20 de agosto, funcionarios de la Fiscalía y miembros de la fuerza pública ingresaron a la casa de Greuso para allanarla, y al no encontrar al candidato, se llevaron a su padre, **Plutarco Greuso de 72 años de edad, a quien le imputan el delito de “porte ilegal de armas”.**

Así mismo había sucedido con Jesús Greuso, hermano del integrante de la UP, quien también fue víctima de un montaje judicial como lo afirma el comunicado, donde se añade que eso mismo se está haciendo con Marino.

Tras esos hechos y los constantes señalamientos que le han venido haciendo a la UP a través de la **Emisora Mariana Estéreo, de la Armada Nacional,** el movimiento hace un llamado de atención al gobierno nacional para que detengan estos ataques de persecución política a través de montajes judiciales y se respete a todas y todos los candidatos de la Unión Patriótica en la actual campaña electoral.
