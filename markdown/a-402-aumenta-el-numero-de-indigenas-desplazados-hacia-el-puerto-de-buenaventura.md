Title: A 402 aumenta el número de indígenas desplazados hacia el puerto de Buenaventura
Date: 2014-12-11 15:31
Author: CtgAdm
Category: DDHH
Slug: a-402-aumenta-el-numero-de-indigenas-desplazados-hacia-el-puerto-de-buenaventura
Status: published

Las primeras familias de los resguardos indígenas de los resguardos de Aguas claras y Balsaito llegaron a Buenaventura el 28 de noviembre, como consecuencia de la presencia paramilitar y guerrillera en sus territorios.

**Denuncian que el Alcalde les ha preguntado cuando retornaran al territorio, pero no se ha preocupado por brindarles condiciones de salubridad y alimentación para su permanencia en el Coliseo de Buenaventura**.
