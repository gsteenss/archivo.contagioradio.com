Title: Pobrecita la clase media…
Date: 2017-07-25 08:43
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Campañas presidenciales, partidos politicos
Slug: pobrecita-la-clase-media
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/la-clase-media-en-las-elecciones.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: commandeleven 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 25 Jul 2017 

[Próximamente comenzarán con todas sus fuerzas las campañas políticas para la presidencia de la república de Colombia en el 2018 y como es habitual, los que definirán quién gane, será la clase media, pues la clase baja sencillamente estará trabajando y no tendrá tiempo ni para votar (a menos que lo paguen bien). Por tanto, para muchos la historia no contará, la política no contará, la comprensión teórica de los problemas no contará, el enredo saturadísimo de información nos volverá aguerridos]*[opinistas]*[ pero completos tarados para explicar por qué… y el marketing político será el que triunfe, no por ser la tendencia, sino precisamente porque las condiciones para que sea una tendencia no se han transformado. ¿Cuáles son esas condiciones para que hoy triunfe el marketing político por sobre la política? una clase media empobrecida; económica e intelectualmente empobrecida.]

[Por supuesto los que hacen fila en Crepes, o fila en BW o fila en un “VVC”, o los que hacen memes contra el comunismo dirán que no están empobrecidos y que son capitalistas porque su salario les alcanza para un par de deudas más. Tal vez pensarán que son notablemente diferentes de los pobres que les toca hacer fila en transmi, o tan diferentes de los que hacen fila para comprar en el madrugón en San Victorino.]

[Ni que hablar de los que creen que por comprar en “SARA” o en las ofertas de “FALAVELLLA” se diferencian si acaso en algo de los que corren a San José a los gangazos… por supuesto, en términos estructurales, los banqueros, que son los mismos de las inmobiliarias, es decir los mismos que suben a políticos que por lo general son amigos de la familia, se burlan de esas aparentes diferencias, pues al final, mientras la clase baja les prepara y les sirve la comida, la clase media creyendo que la habían invitado y que podía sentarse en la mesa a comer con ellos, siempre la dejan en ridículo, la sientan lejitos y le preguntan por lo general “¿a cuántas cuotas?”…]

[Pobrecita esa clase media que se fue del país con un grado de resentimiento, el ego que le queda, lo gasta hablando pestes para tapar las propias. Porque al final las propias pestes, siempre olerán fuerte, viajemos los kilómetros que viajemos.]

[Pobrecita la clase media, que comió cuento y creyó que la técnica podía reemplazar a la teoría, que paga todo a crédito y se cree capitalista. Que contrató al marketing como su coach, y ahora no se puede quejar en público porque “qué oso”… no obstante, en privado, pasa saliva lentamente luego de tener que vender el carro, luego de tener que ir al “D. One” oooooobivamente por la calidad, no por su precio, pues agunos de sus representantes son tan petulantes que si se les diera la gana podrían endeudarse comprando todos los días en “Caruya”…]

[Pobrecita la clase media que envía a sus hijos a estudiar a las mejores universidades para que finalmente allí les enseñen a ser los mejores vigilantes del sistema. Pobrecita la clase media que mantiene una amplia capa de gente muy inteligente, bien preparada, pero que no ha podido abandonar la casa de sus padres ante unos salarios incongruentes con la inversión en su formación profesional.  ]

[Pobrecita esa porción de la clase media que nunca es capaz de pagarse una sesión de psicoanálisis porque el coach le “echa un mejor carretazo”…  pobrecita la clase media que lo único que quiere es vivir en paz con lo que tiene, pero no comprende por qué terminó en una deuda más por algo que realmente no necesitaba.]

[Pobrecita la clase media colombiana, que temerosa de ser consciente y reflexiva, prefiere ser hiriente y agresiva…. va tan solita en su carrito soportando trancones de aquí pallá de allá pacá y más pallá, soñando con que algún día le abran paso como a esas caravanas de políticos ladrones…tal vez por eso, con alegría ve las series de narcotraficantes… para sublimar el deseo de ser un amo, aunque sea en la casa… por la que paga arriendo… o por la que le faltan 20 años de cuotas.  ]

[Pobrecita la clase media que, al ser inconsciente de su ontología revolucionaria por antonomasia, no comprende el poder que tiene. Está empobrecida económicamente porque sostiene al sistema con su trabajo, pero lo venera con sus compras demenciales.]

[La clase media perdió el poder sin resistirse… no es tan guachesita como para salir a la calle a machar por sus derechos (pues no los conoce) pero eso sí, es bastante atarvana y agresiva cuando de pelear contra otro semejante o “inferior” se trata. La clase media perdió el poder cuando aun teniendo la oportunidad de entrar a una universidad, prefirió ser tecnócrata, déspota con la clase baja, y admiradora de una clase alta que siempre la deja en ridículo con sus mercancías inalcanzables.]

[Los integrantes de la clase media perdieron el poder cuando diluyeron en el líquido del marketing, la noción del tiempo y la comprehensión de la historia, siendo paulatinamente guiados, por modelos educativos de muy buena fachada pero huecos intelectualmente, a creer que el mundo había comenzado el día en que nacieron.]

[Pobre clase media, que pelea y se jala de las mechas siempre por uno y por otro, pero nunca por sí misma… es tan individualista, que no sabe proteger sus propios intereses. Tan instrumentalizada que se patió la última peleíta mediática y se puso a favor de Álvaro Uribe o a favor de Daniel Samper, se tiró un par de almuerzos por andar peleando, y no se dio cuenta que nunca ha sido ni terrateniente ni oligarca.    ]

[Y este cuento termina en un baile, un baile que le pega la clase alta a la clase media, porque, aunque pobrecita siempre corre detrás, nunca le coge el paso y termina echándole la culpa a todo, eso sí, menos a la falta de consciencia que le permitiría reconocer el potencial de transformación política, económica y cultural que en ella reside.]

[Pobrecita la clase media, que siempre está presionada por no caer, y auto-presionada por subir a donde nunca podrá… si, auto-presionada… he allí la clave de su desgracia, ¡¡pero menos mal!! he allí la clave de su liberación, una liberación que no llegará humillando al de abajo o lamiendo al de arriba… una liberación que llegará derrumbando falsas consciencias y reconociendo su lugar en el sistema, una liberación que llegará siendo luz para los de abajo, a la vez que verbo y fuego contra los de arriba según sea necesario.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
