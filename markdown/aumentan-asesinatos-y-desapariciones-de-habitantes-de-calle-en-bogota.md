Title: Aumentan asesinatos y desapariciones de habitantes de calle en Bogotá
Date: 2018-01-19 09:42
Category: DDHH, Nacional
Tags: asesinatos de habitantes de calle, Bogotá, Bronx, Habitantes de calle
Slug: aumentan-asesinatos-y-desapariciones-de-habitantes-de-calle-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/habitantes-de-calle-e1516372912796.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Blog Luz para los Habitantes de calle] 

###### [19 Ene 2018] 

La Red de Trabajo para la Habitabilidad en Calle denunció un aumento en el número de asesinatos y desapariciones de personas de esta población. Desde 2010 hasta 2016 han sido asesinados **474 habitantes de calle en Bogotá**, sólo en enero de 2018 han sido asesinadas 5 personas.

Hani Mon, integrante de la Red manifestó que hay una necesidad por atender, humanizar, dignificar y **restituir los derechos de los habitantes de calle de la ciudad**. Dijo que los medios de comunicación han presentado los asesinatos asociándolos a riñas entre ellos mismos, sin embargo, no hay investigaciones ni judicializaciones por parte de las autoridades desprotegiendo así la vida de estas personas.

Adicionalmente, informó que se está presentando **una muerte por semana,** tanto de habitantes de calle, como de vendedores ambulantes, trabajadoras sexuales, integrantes de la población LGBTI, raperos y grafiteros. Explicó que es alarmante la posible existencia de grupos de exterminio y que luego de la intervención del Bronx aumentó la vulnerabilidad para estas poblaciones.

**En Bogotá hay aproximadamente 14 mil habitantes de calle**

<iframe src="https://co.ivoox.com/es/player_ek_23291042_2_1.html?data=k5ifm5aUeJOhhpywj5WaaZS1kZyah5yncZOhhpywj5WRaZi3jpWah5yncaLgw8rf1tSPkIa3lIqvldXJvozYxpC6x9jFaZO3jMjO0MnNqMLo0JDOjdHFb6SZpJiSo5bRpdPVjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El candidato a la cámara por Bogotá y ex habitante de calle Alberto López de Mesa indicó que el censo de 2011, que registró 9 mil habitantes de calle, se hizo sin entrar al Bronx por lo que la cifra **puede superar los 14 mil**. (Le puede interesar: ["Desalojos del Bronx sólo favoreció a empresas privadas"](https://archivo.contagioradio.com/a-un-ano-de-la-retoma-del-bronx-se-evidencia-la-improvisacion-del-desalojo/))

De igual forma, el número de personas que salen re habilitados de los Centros de Atención del Distrito es casi nulo. Por esto, “con el desalojo del Bronx, la **diáspora se ha visto en toda la ciudad** y son personas que están desamparadas”. En un día los centros atienden sólo a 2 mil personas de forma rotativa por lo que sin atención diaria se quedan cerca de 12 mil personas.

**“La sociedad debe entender que se trata de seres humanos”**

El candidato enfatizó en que los habitantes de calle son excluidos por parte de la sociedad y **se olvida que son seres humanos** “tan diversos, hay de todo en la habitabilidad de calle y de diferentes estratos”. Manifiesta que “merecen la misma atención que cualquier otro ciudadano”.

Para atender esta situación, propuso que las autoridades, en términos de seguridad, **brinden atención con ambulancias** que ronden los parques y que haya un cuerpo de seguridad dedicado a la protección que evite las riñas para crear cultura de sana convivencia entre una población que está viviendo condiciones de vulnerabilidad extrema.

**Centros de Atención del distrito no son suficientes para todos los habitantes de calle**

<iframe src="https://co.ivoox.com/es/player_ek_23291062_2_1.html?data=k5ifm5aUepOhhpywj5WXaZS1lJWah5yncZOhhpywj5WRaZi3jpWah5yncanZxNnc1JCzuMbm0IqfpZCspcPd1cbb1sqPqMafxMbZzsqRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Héctor Otero fue habitante de calle y lleva 15 días de proceso en el Centro de Atención Bacatá. Denunció que hay **abusos constantes por parte de la Policía** y “de asesinos que no se sabe de donde vienen”. (Le puede interesar: ["Desalojo del Bronx no tuvo una planeación integral"](https://archivo.contagioradio.com/desalojo-del-bronx-no-tuvo-una-planeacion-integral/))

Arturo, quien también es habitante de calle denunció que "la alcaldía de Enrique Peñalosa, cuando desbarató la L, prometió que todos los habitantes de calle irían a lugares de paso". Sin embargo, en los hogares de paso no alcanzan a caber el **10% de las personas**.

Manifestó que en Bacatá, el centro de atención más grande de la ciudad, solamente caben **400 personas** y "a las 4 0 5 de la tarde ya no hay cupo". En ocasiones Arturo ha tratado de llegar en la noche para conseguir un cupo y no encuentra uno. Dice que "las comidas sobran y las camas están desocupadas". Además, "la atención es mala y pretenden dejar amarrada a las personas para que no las vean en la calle".

**Los habitantes de calle deben buscar cupo todo los días**

Arturo comentó que la personas que llegan a los centros de atención reciben un desayuno y **deben volver a la noche a buscar un nuevo cupo**. Dijo que “en la mañana salimos, si se desayuna más de dos veces lo sancionan y hay que salir a buscar comida para volver a entrar a almorzar”.

La situación se agrava en la medida en que no todos los habitantes asisten a los centros de atención y cuando “la Policía comienza a cumplir el nuevo código ¿dónde meten a las personas?”. Frente a las actividades de capacitación, indica que **no son suficientes** y dan talleres que no potencializan sus habilidades, “sólo los que hacen proceso los capacitan para que pasen de la vida de indigencia a la ciudadanía normal”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
