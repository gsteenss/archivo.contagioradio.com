Title: Pase lo que pase, lo nuevo está creciendo
Date: 2018-06-15 20:07
Author: ContagioRadio
Category: Camilo, Opinion
Tags: Petro
Slug: pase-lo-pase-lo-nuevo-esta-creciendo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/marcha-de-las-flores101316_160.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Junio 14 de 2018

Por **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)**

La Colombia de hoy es otra, diferente a la de estos últimos 70 años atravesada por la violencia socio política, la represión legal e ilegal (entre ellas, el paramilitarismo), los desenfoques del alzamiento armado en el ejercicio del derecho a la rebelión,  la tenencia de la tierra en pocas manos, la exclusión, el clasismo, el negacionismo, la manipulación rampante.

Hoy gracias a hitos como la dejación de armas de las FARC, en medio de la Pax Neoliberal, entre otros, el propio establecimiento se ha ido fragmentando, así la mayoría de la clase política se encuentre cobijada en el candidato de Uribe. El bipartidismo que comprende el conjunto de los matices de los partidos tradicionales que con diversos disfraces han estado usufructuando el poder político: liberales, conservadores, la U, Cambio Radical, Centro Democrático; tiene miedo, a ese despertar ciudadano, al brote consciente de un nuevo tipo de poder en construcción

Entre el odio uribista a Santos se expresa la mezquindad de la clase política, que vela por intereses propios. El transfuguismo de líderes políticos de las agrupaciones políticas tradicionales de estar con la paz de Santos, ahora arropados en el proyecto político contra los acuerdos de paz no es nuevo. A ellos, les rige es el interés propio distante del bien común porque les es ganancioso.

En el candidato de Uribe, se expresa lo turbio, la creación de odios para asegurar la impunidad social, política y hasta religiosa. El maquillaje para que la fetidez se continúe ocultando o reconociendo tácticamente para dilatar transformaciones profundas. Ellos lo que allí se congregan no son mejores ni peores, son la expresión de un sentimiento político, de la ceguera irracional o de la idolatría a un hombre que expresa un sector del país.

El sector que usa de la guerra como medio para garantizar negocios y acumular riqueza. El sector que crea demonios para mostrar sus postulados como los de la alta moral y la bendición evangélica o divina. Allí se arropan unos y otros como amebas para mantenerse en el poder, sin escrúpulo alguno. La política para ellos un negocio, como lo es la guerra o lo es la pax. Una lealtad inquebrantable a la muerte, disfrazada de vida.

Allí en el candidato de Uribe se expresa el país de los corruptos. El país que compra líderes locales, sean afros, negros, indígenas, y urbanos con algo de dinero para asegurar votos.  Clientelas que suman a la nueva esclavitud disfrazada de oportunidad democrática. Ahí se arropan los beneficiados  y testaferros del tráfico de drogas que pagan esos mismos votos, que pagan en la Registraduría regional y nacional para asegurar el fraude.

Ahí están los que se reparten los recursos públicos para sus propios negocios o para lavar el dinero de la droga que  implica a connotados empresarios, a reconocidos políticos, a militares y a policías de alto rango, mientras los armados que les aseguran sus rutas son negados, son de tercera, se les usa y se les tira, como ocurrió con los paramilitares extraditados y los dejados a su suerte a través de la ley 975, mal llamada de “justicia y paz”.

Esos mismos dineros entre mezclados de legales e ilegales con los que se paga a periodistas o pastores, da lo mismo, para mantener la lealtad a la podredumbre que recibe tratamiento profilácticos, o como símbolo de poder real que imposibilita la libertad de expresión, el debate de las ideas. Ese mismo amor al poder que hace que religiosos legitimen la continuidad de la violencia  de la exclusión, el irrespeto a la diferencia.

Ahí se expresan los sectores editoriales y empresariales que pretenden seguir desarrollando oligopolios en los sectores alimentarios, energéticos, informativos, carreteables, infraestructura, extracción petrolera y carbonífera, desconociendo los principios internacionales de derechos humanos y empresas, los principios de precaución y de prevención, de mitigación y de resarcimiento, y por supuesto, los derechos laborales. Para esos sectores solo la acumulación sin más

Ahí se expresan los que no disponen de sus hijos para ir a la guerra. Aquellos que difícilmente tiene hijos mutilados, desplazados, desarraigados, heridos o muertos en combate, quizás sí, afectados por el terrible drama del secuestro. Pero aun así, a pesar de haber padecido esos horrores, insistiendo en el lenguaje bélico, y hemos de decir, del otro lado en que está lo nuevo, un lenguaje y unas actitudes que a veces le hacen juego.

Y más allá de esa realidad politiquera, la gran pregunta, es cómo logramos comprender que muchos electores se muevan en ese sentimiento idolátrico, ¿qué mueve a esos electores a creer, a asumir  una defensa ciega del uribismo? ¿Qué es esa fe? ¿Por qué?

Un momento histórico nuevo se expresa en lo electoral. Sea quien sea el presidente no podrá ejercer el poder desconociendo lo que se manifiesta en Colombia  tendrá una expresión ciudadana critica, el poder consciente. El crecimiento de una postura independiente en la toma de decisiones, el resurgir de nuevos liderazgos juveniles, la creación de nuevos lenguajes distantes del odio. La discusión sana política que logra responder quiénes son esos otros que los mueve ciegamente, para intentar responder el inconsciente colectivo que  está en el alma de eso electores.

Ese poder consciente ante  la fragmentación y el ocaso de los partidos tradicionales,  ante las cuestionadas corruptas de los de abajo y arriba y entre los de arriba es el gran resultado de la historia de esa alma ciudadana. Poder de lo nuevo ante aquellos que  propiciaron la lucha de clases, poder consciente ante las consecuencias que evidencia un modelo de desarrollo que destruye las fuentes de vida, la de los seres humanos y nuevas generaciones, las aguas, los bosques,los animales.

Colombia, está dejando de ser la misma de ayer.

#### [Leer más columnas de opinión de  Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)
