Title: Más de 600 niños y niñas en Ciudad Bolívar cabalgan por la paz
Date: 2016-09-06 14:20
Category: Nacional, Paz
Tags: Diálogos de paz en Colombia, pedagogía de paz, semana por la paz
Slug: mas-de-600-ninos-y-ninas-en-ciudad-bolivar-cabalgan-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Caballitos-por-la-paz-e1473186445131.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Nadia Suarez ] 

###### [6 Sept 2016] 

Este martes 650 niños y niñas del Colegio Arborizadora Alta de Ciudad Bolívar salieron de sus casas en los caballitos de palo con los que juegan todos los días para cabalgar en favor de la paz, como lo vienen haciendo desde hace 8 años, gritando de calle en calle que **la paz significa poder cumplir todos sus sueños, sonreír todos los días** **y gozar de los espacios de la localidad**.

Nadia Suarez, una de las docentes que lidera esta iniciativa, asegura que la jornada se suma al proceso que vienen desarrollando desde hace ya varios años en el que vinculan a niños, niñas y jóvenes a **actividades de reflexión y sensibilización sobre la reconciliación y el perdón**, su necesidad e implicaciones** **en [[una de las localidades más impactadas por la violencia](https://archivo.contagioradio.com/la-limpieza-social-una-violencia-mal-nombrada-informe-del-cnmh/)], particularmente la dirigida hacia la infancia.

En los últimos años estas actividades se han orientado a la comprensión del momento histórico que vive el país, sin dejar de lado la reflexión sobre la v**iolencia que se manifiesta en las cotidianidades de los hogares**, por eso han contado con la participación de madres y padres de familia, quienes también hicieron parte de la cabalgata de caballitos de palo por la paz.

Durante estos ocho años el equipo docente de la institución se ha encargado de adecuar las aulas y transformar las prácticas pedagógicas para que los niños y las niñas disfruten las clases y se encuentren con otras personas, sobre la base del buen trato y el uso frecuente del diálogo, **rompiendo con las dinámicas de conflicto en sus casas y transformando golpes en abrazos**.

Los niños y las niñas de este colegio se sienten muy agradados de ir a clases, al punto de que cómo lo afirma la profesora Nadia, se entristecen los días que no tienen que ir, porque allí han logrado aprender a reconciliarse consigo mismos y con los demás, como **una posibilidad de reparación colectiva en la que han buscado ser felices** y vincular a las personas adultas para que les ayuden a cumplir la tarea.

Esta actividad se realiza en el marco de la [[Semana por la Paz](https://archivo.contagioradio.com/piero-se-une-a-la-celebracion-de-la-paz-en-colombia/)] que se conmemora desde hace 29 años y que en esta nueva versión incluirá **más de 3 mil actividades para la sensibilización en torno al proceso de paz**, que se realizarán entre el 4 y el 11 de septiembre en diversas regiones del país.

<iframe src="https://co.ivoox.com/es/player_ej_12803731_2_1.html?data=kpelkpibd5Khhpywj5WbaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5ynca_Vxc7OjbjZaaSnhqae1MreaZO3jKjczsrLrdCfotfP0dfNvsLY0NfOjabQuMKfxcqYpc7ZqMLYjKfczoqnd46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
