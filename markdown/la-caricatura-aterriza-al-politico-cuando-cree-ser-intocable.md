Title: “La caricatura aterriza al político cuando cree ser intocable”
Date: 2020-04-27 14:03
Author: AdminContagio
Category: yoreporto
Tags: Caricaturista, entrevista, Humorista gráfico, Política colombiana
Slug: la-caricatura-aterriza-al-politico-cuando-cree-ser-intocable
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/000JhorManr-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":5} -->

##### Por: Sasha Yumbila Paz

<!-- /wp:heading -->

<!-- wp:heading {"align":"right","level":5} -->

##### Entrevista a Jhor Manr, Psicólogo y Caricaturista  
{#entrevista-a-jhor-manr-psicólogo-y-caricaturista .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Posiblemente usted no conozca al Psicólogo e Ilustrador digital colombiano Jhorman Rodríguez, pero sí al humorista gráfico bogotano de agudeza provocadora y de gran valor político, quien firma sus obras como Jhor Manr.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con 29  años de edad este caricaturista tiene en sus redes sociales ([Facebook](https://www.facebook.com/jrcaricaturas/), [Twitter](https://twitter.com/jrcaricaturas) e [Instagram](https://www.instagram.com/jrcaricaturas/?hl=es)) una compilación de obras la cual nos aporta una idea bastante exacta de los principales conflictos y debates actuales de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tuve el gusto de entrevistarlo y esto fue lo que me dijo:

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Sasha Yumbila : **¿Cómo llegó a la idea de hacer caricaturas?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Jhor Manr**: Desde niño, tuve mucha preferencia por el arte y siempre me ha gustado dibujar. Hace 6 años comencé a utilizar mis dibujos como medio de protesta social y crítica política. Desde entonces comencé a compartir mis dibujos en redes sociales. He recibido apoyo de la gente, gracias a eso he podido continuar y dibujar para ellos se ha convertido en un reto muy importante y satisfactorio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### S.Y: **¿Cuáles son las razones que lo estimulan para hacer caricaturas?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J.M:** Primero, el intento de aportar algo a nuestra sociedad, no solo mostrar un punto de vista, sino también denunciar y hacer más visibles las verdaderas problemáticas de nuestro país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Segundo, la mediocridad y la falta de integridad que se percibe en la mayoría de los medios de comunicación nacionales, hace que muchas personas del común, como yo, tratemos de opinar al respecto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tercero, el sueño, que por momento parece inalcanzable, de cambiar el rumbo de este país tan corrupto y desigual.

<!-- /wp:paragraph -->

<!-- wp:image -->

<figure class="wp-block-image">
![](https://lh4.googleusercontent.com/LaQdzW5bHmXuKUsL22U4VcLtROlzdIAq0u7diiUwAX_1wjnMneS5SpXMA1bq358HAY17GqVr1Y7bcvsx8GgDUm3U2sgzvayrhIlbfgucoOemJobk0Ae7kx7Ys4KKgAC6BeMU0xIWUEEjj8P_Bg)

</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":5} -->

##### S.Y: **¿Es humor o política?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J.M:** Es humor, y es un humor crudo y negro. Es intentar mostrar con sátira y sarcasmo las situaciones más desagradables y lamentables de la política y de nuestra sociedad. Y cómo lo pueden ver, Colombia es el escenario perfecto para visualizar lo tan asquerosa que puede llegar a ser la política en una nación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### S.Y: **¿Por qué utilizar la caricatura como medio de expresión crítica?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J.M:** Primero, porque con una imagen puedes decirlo todo sin poner una sola letra, ni mencionar una sola palabra. Puedes darle a la mente un simple dibujo para que lo interprete y saque sus propias conclusiones. Si la imagen es la adecuada, habrás puesto a pensar a muchas personas sobre una problemática que merece dicha atención. Es lo que menos le conviene a un gobernante o líder político que no está haciendo lo correcto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Segundo, la caricatura siempre ha reflejado la idea popular, permite que aquel que tiene el poder, sepa cómo lo está observando parte de ese pueblo que gobierna. Aterriza al político, cuando cree ser inalcanzable o intocable.

<!-- /wp:paragraph -->

<!-- wp:image -->

<figure class="wp-block-image">
![](https://lh3.googleusercontent.com/GiOlFo3szSD7l2I-x6A9_vxbqv4prcU5Gr60_y-ajIMPl4Xli39UOVrVQDUPamFT1T3HPqqUYKiTVtG8F2zu85OQwhsa5Ea28d3DXUCjPkejKgW82Qy3M2ODkabtXUJ1hczHhdnBWFUyUeCYMQ)

</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":5} -->

##### S.Y: **¿Por qué Uribe y Duque en sus caricaturas?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

J.M: Uribe porque desde el 2002, ha sido el máximo representante de la derecha corrupta de este país, al conocer los principales problemas sociales en Colombia, es sorprendente ver que Uribe está involucrado en casi todos, por eso aparece en muchos de mis dibujos. Me indigna que haya logrado construir esa burbuja mafiosa que impide que se conozca toda la verdad y que lo investiguen completamente. Eso pienso de él.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Duque, por ser uno más de los que Uribe pone en el poder, yo no sabía que él existía hasta que Uribe lo postuló, lo bueno de Duque es que brinda material de sobra para dibujar. Hay que ver algo bueno.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### S.Y: **Se percibe en sus caricaturas un país narco fascista ¿Qué tiene para decir al respecto?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J.M:** Que estoy dibujando bien.

<!-- /wp:paragraph -->

<!-- wp:image -->

<figure class="wp-block-image">
![](https://lh5.googleusercontent.com/iuIhFxzcW2jBGdANr3IfAbfnWDLiuBYgcc9NBDTHTQ6qUQX0CnmQknbOcmIo8hlNzibwc0mAGN9vxTXKwrTQoWVIVq17BGxqJIZUiJwKqwWQkqXwxw3LfTzMGiR8LnT1ASVZP6s0sYc4wFUYYA)

</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":5} -->

##### S.Y: **¿Algún público en especial?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J.M:** No sé, me gusta que mi público sea variado, obviamente hay personas que tienen posiciones contrarias y me lo hacen saber de forma muy respetuosa y valiosa, pero me agrada poder conversar con ellos y discutir ciertos temas. Considero que es importante fomentar estos espacios de discusión sana y pacífica, sean los ideales y perspectivas que sean.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### S.Y: **En cuanto al arte y la técnica gráfica ¿Qué lo diferencia de los demás caricaturistas?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J.M:** Sinceramente no considero que mi técnica gráfica sea especial, el estilo de todos los caricaturista es único. A mi me gusta mucho darle profundidad a las formas, destino mucho tiempo en el color y las sombras de mis matachos. En cuanto al mensaje del dibujo si es todo lo contrario, trato de ser lo más directo y básico posible, siempre es mi reto principal.

<!-- /wp:paragraph -->

<!-- wp:image -->

<figure class="wp-block-image">
![](https://lh3.googleusercontent.com/qbi19gEQLbN6UTv6zf8hwYEMZsr5gqMOqxuhQrTwwZ1ICpnm-RgGygt1IsNzdUJqRGrKeMDP9bDMk7jq-wkrmtMg-SZosUtsMoZAb0HZrhc3l2CFz2_qzH77uuglDs7c2nh_oOniLnUwwhOQOw)

</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":5} -->

##### S.Y: **¿Qué piensa en general de nuestros humoristas gráficos? (Matador... etc) ¿Algún referente es especial?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J.M:** Pienso qué hay demasiado talento, correspondiente a todo lo desagradable que hay que visualizar. Por eso ningún caricaturista se detiene, hay mucho por dibujar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La caricatura mezcla dos aspectos primordiales. La idea y el arte. En ideas, admiro a Matador, y en arte admiro a Bacteria.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### S.Y: **Cree qué se debe conformar  un gran movimiento nacional de caricaturistas para tener mayor repercusión en la denuncia y poder visibilizar nuevas ideas de cambio social o como la mayoría en el mundo del arte, que siga cada uno por su lado?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J.M:** Yo en eso no me he involucrado mucho, admiro a todos los que se atreven a dibujar la realidad, y más en este país en donde incluso la vida se pone en juego, sin embargo y personalmente me gusta que mi labor como caricaturista, sea lo más independiente posible.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### S.Y: **¿Qué aporte hacen sus caricaturas a la tranformación política y cultural de un país en la que se ha posicionado una “cultura mafiosa”?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J.M:** No estoy seguro del aporte que hacen mis caricaturas al país, sin embargo espero estar aportando al arte Colombiano que es inmenso y a la libertad de expresión que en este país es tan restringida, ese aporte solo sabremos si existió o no, con el pasar de los años. En cuanto a nuestra cultura mafiosa, solo desaparecerá cuando nuestro país invierta más dinero en educación, que en armas y guerra.

<!-- /wp:paragraph -->

<!-- wp:image -->

<figure class="wp-block-image">
![](https://lh5.googleusercontent.com/hODdUbx2JL9Zxt2qLb5GeAocHroB-V4CsgF5pxAyulOvLG-Um7h2TP8TzwiMRss6aEnmSV1Y65mDfoiZsiD1tqFNV9Cn2zUD6swRvdzD-k1rZ74hrQ56V9zj-aEHr5p187Swg_eEGKVwZXkp-Q)

</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":5} -->

##### S.Y: **¿Hay algun medio diferente de sus redes sociales donde publiquen sus caricaturas?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J.M:** Si, apoyo a varios medios sin ánimo de lucro, como el Periódico comunitario «El Hormiguero» de Bogotá y la ONG «Soy Campesino» de Boyacá. Y algunos medios digitales también publican mis dibujos como La Oreja Roja, 180 grados Quindío, Prensa Social Colombia, entre otros.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### S.Y: **En su experiencia personal, ¿qué le han aportado las publicaciones de sus caricaturas?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J:M**: Lo que más considero gratificante y que me aporta es el aprecio y el valor que la gente le ha dado a mis dibujos. Cada dibujo y las reacciones de la gente son un indicador de cómo estoy haciendo mi trabajo. Creo que el apoyo de todos ellos es el motor fundamental de cada caricatura.

<!-- /wp:paragraph -->

<!-- wp:image -->

<figure class="wp-block-image">
![](https://lh4.googleusercontent.com/5uUcLrLOixwC-2Ii6fyEOWIegbTWtHDxWYbouadrMnugYouYRh9ULfKhuPxh6vJ4r3PTNxGp2mA82aIgZJVy7MbgHoXOu0Y-jqppMr7YdhN6Qr_sKQKXvcGG0YnloLkJy04xmZxoM2zaDDRPaQ)

</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":5} -->

##### S.Y: **¿Trabaja en algún proyecto?  ¿Te pagan por las caricaturas que hace al día?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J.M:** No recibo remuneración alguna por mis caricaturas, tampoco lo espero. Trabajo actualmente en asesorías y consultas a compañías en otros temas, dicho trabajo me permite tener tiempo para dibujar y escribir.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### S.Y: **Hacer burla del poder arbitrario es un deber, pero además del pensamiento crítico, ¿no considera necesario alimentar la esperanza del cambio en quienes disfrutan sus caricaturas?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J.M:** Esa parte la veo difícil, considero que en este país las personas que tienen el don y las facultades para generar esperanza de ese tan anhelado cambio de inmediato son estigmatizadas por la misma fuerza política y los medios de comunicación de este país. Si recuerdas, por ejemplo, Garzón, Pizarro, Galán, fueron asesinados y hoy se les recuerda con mucho valor y aprecio, por que representaban el cambio, pero en la época fueron fuertemente criticados y estigmatizados, los mismos medios y políticos que en su momento los hicieron ver como amenaza, incluso quienes estuvieron de acuerdo con sus asesinatos, hoy en día les hacen homenajes, así funciona.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### S.Y: **Finalmente ¿Algún mensaje para las y los colombianos?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**J.M:** Que no coman cuento entero a lo que muestran los medios tradicionales, que estamos entrando en una era que facilita la información que antes era tan restringida, que si se toman el mínimo esfuerzo por conocer la verdad, la van a encontrar. Y que gracias por tanto apoyo.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar:[Yo Reporto](https://archivo.contagioradio.com/?s=yo+reporto)

<!-- /wp:paragraph -->
