Title: En riesgo el derecho a la consulta previa
Date: 2016-11-23 13:46
Category: DDHH, Nacional
Tags: Consulta Previa, Derecho, indígenas, territorio
Slug: proyecto-de-ley-para-reglamentar-consulta-pretende-desconocer-las-comunidades-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Indigenas-Vaarios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Emaze] 

###### [23 Nov. 2016] 

**Diversas organizaciones indígenas han manifestado que podría ser exterminados de manera acelerada de ser aprobado el proyecto de reglamentación del derecho fundamental a la consulta** y al consentimiento previo, libre e informado radicado ante la Mesa Permanente de Concertación por el Gobierno Nacional.

Y han denunciado que **el contenido del proyecto de ley propuesto por el Gobierno “acelera el exterminio y la extinción física y cultural de nuestros pueblos, para el beneficio de las empresas privadas y multinacionales**, que responde a intereses económicos, políticos y geoestratégicos que atentan contra los territorios”

Según el comunicado, **dicho proyecto de ley recorta los derechos de los pueblos indígenas, afrodescendientes entre otros que habían ido siendo ganados en lo que respecta a la consulta previa,** toda vez que dentro de sus artículos propone que solamente aquellos pueblos que sean certificados podrán ser consultados.

Destacan que hay que tener en cuenta que **en Colombia existe un subregistro de las comunidades indígenas y étnicas** que ocupan los territorios en el país y en los casos que han optado por llevar a cabo dicha certificación, llevan años esperando respuesta del ente encargado.

**Por su parte manifiestan que dicho proyecto de ley iría en contravía de los avances logrados con la Sentencia T-129 de 2011,** puesto que manifiesta que solo serán tenidos en cuenta los Proyectos, Obras o Actividades (POA) a gran escala y que tengan mayor impacto dentro del territorio. **Así como el desconocimiento del Convenio 169 de la OIT, ratificado por Colombia mediante la Ley 21 de 1991. **Contenido relacionado: [Autoridad afrocolombiana denuncia negación de consulta previa sobre plan de desarrollo](https://archivo.contagioradio.com/autoridad-afrocolombiana-denuncia-negacion-de-consulta-previa-sobre-plan-de-desarrollo/)

De igual manera, **dicho proyecto de ley manifiesta** **que la consulta previa estará supeditada a la disponibilidad presupuestal que tengan las entidades encargadas** lo que estaría condicionando la realización o no de dicho procedimiento a los rubros que destinen los entes.

En suma, según el comunicado de estas organizaciones, **este proyecto de ley estaría reduciendo cualquier campo de acción que puedan tener las comunidades indígenas, afrodescendientes entre otras para poder negarse a que algunos proyectos lleguen a sus territorios.**

**El desconocimiento de los impactos sociales, culturales y religiosos serían las consecuencias que podría tener para las comunidades, de aprobarse este proyecto de ley.**

Por último, **las organizaciones han exigido al Gobierno nacional el respeto del territorio, como fuente primordial de subsistencia de los Pueblos Indígenas y han rechazado que dicho proyecto de ley no haya sido consultado con ellos. **Le puede interesar: [Anla y Ministerio del Interior adelantaban consulta previa ilegal en parque Tayrona](https://archivo.contagioradio.com/anla-y-ministerio-del-interior-adelantaban-consulta-previa-ilegal-en-parque-tayrona/)

Las organizaciones firmantes son: los pueblos y organizaciones indígenas de Colombia representados en sus principales organizaciones de carácter nacional con asiento en la Mesa Permanente de Concertación (MPC), integrada por la Organización Nacional Indígena de Colombia (ONIC), la Organización de los Pueblos Indígenas de la Amazonía Colombiana (OPIAC), la Confederación indígena Tayrona (CIT), Autoridades Indígenas de Colombia (AICO) y Autoridades Tradicionales – Gobierno Mayor.

Leer comunicado completo [aquí](http://www.onic.org.co/comunicados-onic/1553-una-declaracion-de-muerte-en-tiempos-de-paz-propuesta-de-reglamentacion-del-derecho-fundamental-a-la-consulta-y-al-consentimiento-previo-libre-e-informado).

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

[proyecto de reglamentación del derecho fundamental a la consulta y al consentimiento previo, libre e inform...](https://www.scribd.com/document/332092677/proyecto-de-reglamentacion-del-derecho-fundamental-a-la-consulta-y-al-consentimiento-previo-libre-e-informado-radicado-ante-la-MPC#from_embed "View proyecto de reglamentación del derecho fundamental a la consulta y al consentimiento previo, libre e informado radicado ante la MPC on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

</div>

<iframe id="doc_88952" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/332092677/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-RCPrzE9HnX1E62lBdzC7&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
