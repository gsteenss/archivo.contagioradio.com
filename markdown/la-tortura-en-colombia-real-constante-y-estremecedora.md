Title: La tortura en Colombia: real, constante y estremecedora
Date: 2015-05-05 09:16
Author: CtgAdm
Category: Carolina, Opinion
Tags: Carolina Garzón Díaz, Coalición Colombiana contra la Tortura, Comité contra la Tortura de las Naciones Unidas, informe contra la tortura, LGBTI, paramilitares, UNCAT
Slug: la-tortura-en-colombia-real-constante-y-estremecedora
Status: published

###### Foto: Amnistia Internacional 

[Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/) (@E\_vinna)

¿Sabías que la esterilización forzada e involuntaria de personas con discapacidad es una práctica reiterada? ¿Tenías idea que desde el 2001 hasta julio de 2014 ha aumentado el reclutamiento indiscriminado e ilegal de jóvenes en las “batidas”? ¿Te imaginas que durante los últimos cinco años, las cifras de hacinamiento carcelario han pasado del 27.8% al 58.5%? ¿Conocías que entre 2010 y 2012 se denunciaron 295 ataques con agentes químicos? Todas estas son formas de tortura y cada una de estas cifras corresponde a un país: Colombia.

En Colombia muchas personas creen que las torturas no existen y esto sucede porque se asocia a escenas hollywoodenses creyendo, entonces, que la tortura se restringe únicamente a lo físico, a lo individual y a prácticas clandestinas de mercenarios. Pero esta visión reducida nos impide ver la cotidianidad de la tortura y su conexidad con otros delitos y prácticas que se viven diariamente en nuestro país.

Hace pocos días en Ginebra, Suiza, la Coalición Colombiana contra la Tortura (CCCT) presentó al Comité contra la Tortura de las Naciones Unidas (UNCAT) un documentado informe sobre la situación de “Tortura y tratos o penas crueles, inhumanos o degradantes en Colombia” ocurridas entre 2009 y 2014. El UNCAT recibió este insumo alterno al informe oficial presentado por el Gobierno colombiano y realizó el examen sobre la situación de tortura en el país. El Informe Alterno presentado por la CCCT reveló que la tortura en Colombia se presenta como forma de discriminación, persecución política, sometimiento de la población y represión de la protesta.

Como siempre, las víctimas de la tortura son las poblaciones que han sido históricamente vulneradas: las poblaciones indígenas, afrodescendientes y campesinas; las mujeres, los niños, las personas de la comunidad LGBTI, la población carcelaria, las personas con discapacidad y los defensores de DDHH. Aquí vale la pena mencionar que los casos de tortura se agravan a causa del conflicto armado, como expone el Informe Alterno, pero no únicamente se comenten en este marco. Los ataques con agentes químicos, los malos tratos en las cárceles, la esterilización de personas en situación de discapacidad y la tortura durante las protestas sociales son prácticas que ocurren con independencia del conflicto armado interno.

¿Y quiénes son los responsables? Según información recogida por la CCCT “La Fuerza Pública es registrada como el principal perpetrador de estos actos, seguido por los grupos paramilitares posdesmovilización.” A ellos se atribuyen torturas como la violencia sexual contra mujeres y niños, el reclutamiento forzado, las “batidas”, los casos de desaparición forzada, las ejecuciones extrajudiciales y el funcionamiento de las “casas de pique”, entre otras.

Estas prácticas de tortura fueron puestas en conocimiento de los Expertos del Comité contra la Tortura, quienes se mostraron receptivos ante la grave situación de Colombia. Indagaron con consternación por los casos documentados en el Informe Alterno de la CCCT y preguntaron a la delegación del estado colombiano por cada una de estas denuncias. Como era de esperarse, la nutrida comitiva del gobierno no logró responder satisfactoriamente a las preguntas del Comité y siguieron repitiendo su discurso aprendido. Pura demagogia.

Este 15 de mayo el UNCAT hará públicas sus recomendaciones a Colombia, que seguramente se parecerán las anteriores hechas en 2009 y las cuales el Estado no cumplió. Sin embargo, con anhelo, espero que todo lo ocurrido en estas semanas contribuya a nuestra conciencia sobre la existencia de la tortura en Colombia, nos ayude a solidarizarnos con las víctimas de estos flagelos y a exigirle de forma informada al Estado colombiano que cumpla con las recomendaciones del Comité, porque en Colombia la tortura sí existe, es real, constante y estremecedora.
