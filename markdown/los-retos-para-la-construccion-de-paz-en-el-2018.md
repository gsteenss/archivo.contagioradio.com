Title: Los retos para la construcción de paz en el 2018
Date: 2018-01-16 17:20
Category: Nacional, Paz
Tags: ELN, FARC, Juan Manuel Santos, paz
Slug: los-retos-para-la-construccion-de-paz-en-el-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/paz-en-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo/Contagio Radio] 

###### [16 Ene 2018] 

La implementación de los Acuerdos de Paz y con ella la puesta en marcha de los tribunales de la JEP, la mesa de conversaciones entre el gobierno y el ELN, el control que adquieren grupos armados en el país y las elecciones a Congreso y presidencia, **son algunos de los retos que tendrá que presenta el 2018 para avanzar en la construcción de paz en el país.**

### **Las elecciones una disputa para la paz**

<iframe src="https://co.ivoox.com/es/player_ek_23186112_2_1.html?data=k5iempuVdZOhhpywj5aXaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Z0ZCrs8_uhqigh6aVsMbnjpCxy9ePqMafqrOxp7Wlno6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con el director de Indepaz, Camilo González Posso, las elecciones al Congreso y a la presidencia, son el escenario más “crítico” porque del mismo se definirá las voluntades que existan en la rama legislativa para avanzar en la implementación de los Acuerdos de Paz e insistir en los diálogos de paz con el ELN.

“**Es un año político, el eje de todo está en la política**, lo que está en juego es si se van a implementar los Acuerdos y a ponerle el énfasis a la situación negociada o si va a haber una lucha por la renegociación de los mismos para hacerlos trizas” afirmó Gonzáles Posso.

### **Los Acuerdos de Paz de La Habana** 

Para Camilo González Posso, el gobierno en cabeza del presidente Juan Manuel Santos, aún tiene varios días para garantizar la implementación de puntos importantes de los Acuerdos, por lo tanto, lo que se necesita es la voluntad de que se pongan en marcha esos mecanismos que dejen la implementación en un punto irreversible.

“Todavía hay herramientas administrativas y legislativas, como recursos para garantizar los acuerdos, **desahuciar la implementación me parece prematuro, lo que hay que hacer es poner el acelerador”** expresó González Posso y agregó que también debe existir una voluntad y una disposición por cumplir lo que ya se ha pactado como los acuerdos de sustitución de cultivos ilícitos.

<iframe src="https://co.ivoox.com/es/player_ek_23186140_2_1.html?data=k5iempuVeJGhhpywj5acaZS1kZaah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZCxqcXdz8aYqcbQsMbb0IqfpZDFp8LYhqigh6adscrX0JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El profesor  y analista Carlos Medina, señaló que espera que con la reciente visita que hiso Antonio Guterrez, secretario general de la ONU, se realicen los ajustes necesarios para que haya un mayor cumplimiento en los acuerdos por parte del gobierno y aseguro que este proceso sirve como referencia para el avance de los diálogos en Quito.

### **Si hay alternativas para superar crisis mesa ELN-Gobierno** 

Referente al estado actual de la mesa entre el ELN y el Gobierno, ambos analistas aseveraron que hay alternativas para superar el estado de crisis. De acuerdo González, el primer paso debe ser sentarse en la mesa y tramitar los temas sobre el cese bilateral, el desescalamiento del conflicto armado y las acciones humanitarias.

“La mesa tiene que ponerle sustancia a los temas, sin pensar que se va a llegar al acuerdo final, pero si se podría llegar a acuerdos de aplicación inmediata alrededor de los planes de enfoque territorial, **garantías democráticas a la vida, de seguridad a las comunidades y los líderes y en alivios y acciones humanitarias”** señaló González.

Para Medina, el primer paso que se debe dar en la retoma de los diálogos en Quito, es que el presidente Juan Manuel Santos de la orden de la reanudación de las conversaciones, que se pacte un nuevo cese bilateral al fuego con **“objetividad y realismo”** y avanzar en las metodologías de participación de la sociedad. (Le puede interesar: ["La agenda que debería adoptar el gobierno para facilitar conversaciones con el ELN"](https://archivo.contagioradio.com/la-agenda-que-deberia-adoptar-el-gobierno-para-facilitar-conversaciones-de-paz-con-eln/))

### **Las estructuras armadas continúan la guerra en Colombia** 

Sobre las operaciones que se han intensificado en todo el territorio, por parte de estructuras armadas paramilitares, que buscan control sobre las comunidades y rutas del narcotráfico, González Posso aseguró que están dadas las condiciones para que se avance en la exploración de anuncios que han hecho grupos armados como las **Autodefensas Gaitanistas de Colombia sobre ceses bilaterales que permiten un diálogo con el gobierno**.

Del otro lado Carlos Medina afirmó que es importante que **el gobierno brinde garantías de seguridad a las comunidades y los líderes sociales**, al mismo tiempo que pone en marcha estrategias y acciones para el desmonte de las estructuras paramilitares.

“No se puede construir una paz en los territorios cuando existe la presencia de grupos paramilitares en connivencia con las Fuerzas Militares, **se debe desparamilitarizar la Fuerza Pública**” manifestó Medina. (Le puede interesar: ["4 porpuestas para destrabar la Mesa ELN-Gobierno y reanudar el cese bilateral"](https://archivo.contagioradio.com/4-propuestas-para-destrabar-la-mesa-eln-gobierno-y-reanudar-cese-bilateral/))

###### Reciba toda la información de Contagio Radio en [[su correo]
