Title: Comunidades del Cauca no van a dejar que el terror se apodere de sus territorios
Date: 2019-09-03 11:27
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Amenaza a líderes sociales, asesinato, Cauca, comunidades
Slug: comunidades-cauca-no-terror
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-03-at-10.18.13.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Después del asesinato de Karina García Sierra, candidata a la alcaldía de Suárez, Cauca, distintos movimientos y comunidades expresan su dolor y ponen de manifiesto **la violencia que sufren y el abandono del Estado en sus territorios**, sin embargo aseguran que seguirán trabajando por la paz de manera interal.

Karina era la primera mujer candidata a la alcaldía en el municipio de Suárez, en Cauca. Antes de su asesinato, pidió públicamente  "garantías para todos los candidatos, para que no hayan hechos que se puedan lamentar”. Poco después, ocurrió la masacre en la que ella fue asesinada junto con el presidente de la Junta de Acción Comunal del barrio Las Brisas en Suárez, Héctor González; el representante de las víctimas del conflicto armado de la región, Aidé Trochez; Lavedis Ramos, parte del equipo de campaña y Yeison Obando, aspirante al consejo municipal.

Francia Márquez, lideresa del Proceso de Comunidades Negras (PCN) en Suárez, en declaraciones a Contagio Radio denuncia que “**las amenazas no paran contra los líderes sociales. Los compañeros y compañeras siguen siendo amenazados** (…) es una situación que ya desborda los límites de la cordura”.

Ante esta grave situación, el Gobierno opta por una militarización de esos territorios, la cual no parece dar una respuesta efectiva: “Suárez se está militarizando y aun así no para la violencia. No creo que la militarización sea la solución (...)". La presencia del Ejército en el departamento del Cauca es notable pues se estiman al menos siete batallones de alta montaña, tres de ellos en el norte del Cauca, con 13.000 efectivos.

### **El abandono institucional en Cauca**

[Además, "hay un abandono del Estado en términos de inversión social, de salud, educación, de mejora de la calidad de vida” lamenta Francia. Los datos del DANE muestran que Cauca es uno de los departamentos con mayor pobreza y desigualdad del país.]Mientras que a nivel nacional la incidencia de la pobreza supone un 26,9%, en esta región asciende hasta un 50,7%, según el Boletín Técnico de la Pobreza Monetaria en Cauca de DANE de 2017. En esa misma línea, la pobreza extrema es del 20,8% y, a nivel nacional, no llega al 8%. “No hay programas económicos que generen condiciones de vida digna a la gente.

En cuanto a la falta de inversión educativa, la lideresa relata que "en zonas alejadas los niños han dejado de ir a las escuelas y se ponen a raspar coca porque no tienen en su institución un maestro”. (Le puede interesar: ["Asesinato del profesor Orlando Gómez en C. es un atentado contra la educación"](https://archivo.contagioradio.com/orlando-gomez-atentado-educacion/))

César Cerón, candidato a la Alcaldía de Suárez, quién conocía a Karina García, insiste en la necesidad de una intervención por parte del Estado. “Nosotros teníamos una esperanza con los Acuerdos de Paz pero hoy nuevamente se produce guerra en estos territorios”. (Le puede interesar: ["Preocupante situación de seguridad en Suárez, Norte del C."](https://archivo.contagioradio.com/seguridad-norte-del-cauca/))

Estos ataques sistemáticos hacia los líderes sociales, así como la falta de actuación por parte del Gobierno en esos territorios minan el ánimo de participar en las elecciones y de creer en la política. Sin embargo, sus líderes y lideresas siguen realizando acciones para parar la violencia que se apodera de sus comunidades y apoyar la Paz. Cerón declara: “vamos a hacer una gran rueda de prensa en donde podamos expresar todo lo que viene sucediendo en este rincón del Cauca. Hacer unas manifestaciones religiosas y **una marcha por la vida y por la paz”**.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_40885026" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_40885026_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
