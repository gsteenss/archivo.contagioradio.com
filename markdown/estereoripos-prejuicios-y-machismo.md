Title: Estereoripos, prejuicios y machismo
Date: 2015-04-27 16:24
Author: CtgAdm
Category: closet
Tags: Adopción igualitaria, Closet Abierto, LGBTI, Se puede ser, Sub Secretaría LGBTI
Slug: estereoripos-prejuicios-y-machismo
Status: published

<iframe src="http://www.ivoox.com/player_ek_4415290_2_1.html?data=lZmel5eddI6ZmKiakpeJd6Kkloqgo5aZcYarpJKfj4qbh46kjoqkpZKUcYarpJKy1dnJtsbj087d0diJdqSf0dfSzNrNp8rj1JDmjdLFp8nd1NLcj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Programa 13 de Abril] 

Closet Abierto debate en todos los contextos que están ligados a la comunidad LGBTI, después de hablar de términos religiosos y políticos nos centramos en la sociedad, que se ve influenciada por otros contextos alternos que afectan la aceptación y el rechazo hacia la comunidad LGBTI.

Las afectaciones sociales para la comunidad son bastante altas y nos demuestran que el retroceso en Colombia parte desde la heteronormatividad que se ha impuesto desde generaciones pasadas, y de la misma manera se ve reflejada en las decisiones legislativas que se toman para decidir de alguna manera el futuro de las personas homosexuales.

La sociedad colombiana se torna machista a tal punto de no permitir que un hombre de su familia tenga una orientación sexual diferente a la heterosexual, por el simple hecho de que ya no será el hombre de la familia, pues todavía se impone el modelo de familia en el que el hombre es el que tiene la responsabilidad económica y la mujer es quien cumple con las responsabilidades del hogar.

A pesar de que las nuevas generaciones han empezado a transformar su mentalidad, todavía existen muchos prejuicios, los invitados Andrea Medina, Jefferson López y Álvaro Baquero se encargan de exponer diferentes puntos de vista, desde su posición heterosexual y su cotidianidad, buscando dar espacio a diversidad de opiniones.
