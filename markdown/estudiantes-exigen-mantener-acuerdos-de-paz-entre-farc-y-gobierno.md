Title: Estudiantes exigen mantener acuerdos de paz entre FARC y Gobierno
Date: 2016-10-18 13:52
Category: Nacional, Paz
Tags: #AcuerdosYA, El acuerdo se mantiene, Movimiento estudiantil Colombia
Slug: estudiantes-exigen-mantener-acuerdos-de-paz-entre-farc-y-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz64.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 de Oct 2016] 

Más de 160 estudiantes se reunieron este fin de semana en Bogotá, para hablar de las propuestas e iniciativas que tienen, como movimiento estudiantil, **en respaldo a mantener los acuerdos de paz en la Habana y el inicio de la mesa de conversaciones con el ELN**, en este mismo espacio, anunciaron que se unirán a la movilización convocada este próximo 20 de octubre por la paz y que se realizarán en diferentes ciudades de Colombia y el mundo.

En el escenario participaron estudiantes de aproximadamente 40 universidades públicas y privadas de diferentes regiones del país y como resultado del encuentro realizaron una carta dirigida tanto a la clase política del país, como a los medios de información y la sociedad. En esta misiva le **piden al gobierno que las propuestas que se hagan en torno a los acuerdos de la Habana sean de carácter adicional, sin modificar ninguno de los puntos ya acordados.**

De acuerdo con Camilo Rodríguez, estudiante de economía de la Universidad Nacional, el punto más importante de la carta **“sobre mantener el acuerdo, que solo debe ser modificado para bien y no para dañar, avances como el punto 5 cinco de víctimas” **Lea también:["Comunidad Estudiantil se suma a los esfuerzos por acuerdos ya"](https://archivo.contagioradio.com/comunidad-estudiantil-se-suma-a-los-esfuerzos-por-acuerdos-ya/)

A los políticos del país les exigen **no continuar haciendo campañas que manipulen el contenido de los acuerdos y evitar dilatar el proceso**, mientras que a los medios de información les piden hacer un manejo responsable de la información, de igual forma extienden un llamado a la sociedad para que se posibiliten más espacios de debate que superen la polarización.

**Al igual que la iniciativa Paz a la Calle, el espacio se desarrollará de forma horizontal sin voceros** y se espera que la carta recolecte un determinado número de firmas para ser enviada a la mesa de diálogos en la Habana.

### Texto carta "Suscribamos esta carta que contiene la voz de los estudiantes por la paz" 

Paz: una palabra ajena, lejana y fuera del léxico de un país abordado por la violencia desde hace más de cincuenta años. Si nos preguntáramos como sociedad dónde inició esta pugna interna tal vez tendríamos que remitirnos a una historia más profunda, a la colonización misma. Acuerdos y negociaciones han sido la forma hasta el momento de encerrar nuevas guerras y frustraciones, nuevas constituciones y eliminación de la postura diferente. Sin embargo hoy, creemos firmemente que estamos ante la posibilidad de que un acuerdo sea, al fin, un paso para construir caminos hacia una Colombia con mayores garantías para la libre expresión y organización de amplios sectores. La pregunta es ¿Estamos listos para avanzar como sociedad?

Sabemos que los resultados del 2 de octubre opacaron por un momento esta posibilidad, en medio de una altísima abstención y la polarización del país. A pesar de los sentimientos de tristeza que nos embargaron, pronto entendimos que el resultado de esta primera batalla nos obligaba a trabajar con más ahínco en la dirección de nuestras convicciones, y por ello pusimos manos a la obra. Muy rápidamente las voces de la sociedad, en especial las de jóvenes y estudiantes, se levantaron clamando por una pronta solución a la situación de incertidumbre política generada tras el resultado del plebiscito. Hemos decidido como pueblo, como jóvenes y como estudiantes, encaminar todos nuestros esfuerzos en la construcción de la paz y en hacerle saber a la sociedad colombiana, al Gobierno, a la insurgencia, a los del "Sí”, los del “No" y a la abstención: ¡La paz se queda!

Un amplio y diverso movimiento social se ha tejido estos días: se crearon comités de apoyo, se fortaleció la movilización en las calles, nuevos actores subieron a las tablas, se generaron nuevos espacios de debate y decisión ciudadana; tomamos la calle y nos escuchamos, de esa manera comprendimos que nos une la voluntad de ver materializados los acuerdos como una oportunidad para aclarar y cerrar las heridas que ha dejado en nuestra gente la dinámica prácticamente ininterrumpida de confrontación bélica que ha tenido lugar en Colombia.

Sabemos que son muchos los problemas de nuestro país, que difícilmente podrán resolverse en un acuerdo, pero entendemos que se abre un nuevo y promisorio capítulo de trabajo y lucha por construir los cambios que la gente sencilla necesita y reclama, entre ellos la necesidad de construir una educación superior como derecho fundamental, bien común y al servicio de la gente, que se contraponga a un modelo que ha mostrado su fracaso y agotamiento para responder a un nuevo momento como sociedad.

Es así como estudiantes de alrededor de 40 universidades de todo el país, reunidos el día 15 de octubre en la Universidad Nacional de Colombia-sede Bogotá, enviamos los siguientes mensajes:

**A la mesa de diálogos:**

-       La mesa de negociación sigue con su legitimidad y escuchamos su voz. Es la mesa quien decidirá sobre qué hacer con los acuerdos hasta el momento pactados. Hacemos un llamado a que se llegue a un Acuerdo YA.

-       Consideramos sustanciales los avances en materia de verdad, justicia, reparación y garantías de no repetición, en enfoque de género, en la pretensión de cerrar la brecha entre el campo y la ciudad y generar una apertura democrática en el país. Los acuerdos ya firmados tienen que ser la base de la discusión: cualquier modificación que la mesa decida hacer, deben ser elementos adicionales bajo el espíritu de los acuerdos y no aspectos que retrocedan o anulen lo ya establecido.

-       La ONU continúa con su papel de monitoreo y verificación y es garante de la vida de los guerrilleros.

-       El cese al fuego y de hostilidades bilateral y definitivo debe mantenerse.

-       No queremos ser un impedimento, ni un elemento de dilación de la mesa, pero en dado caso de que ésta decida abrir la participación, consideramos que la voz de las y los estudiantes, en su diversidad, deberá ser escuchada. Esta misma voz debe ser tenida en cuenta para el proceso de implementación y verificación.

-       Teniendo en cuenta la dinámica participativa que es transversal a los diálogos entre el Gobierno Nacional y el ELN, llamamos a sumar la participación de las y los estudiantes en la construcción de éste escenario.

 **A los sectores políticos:**

 -       No más manipulación a la sociedad colombiana por medio de mentiras y engaños.

-       No más dilación del proceso anclado a intereses políticos y electorales particulares.

-       La Corte Constitucional y el Congreso de la República deben ponerse en función de construir alternativas de la mano de la mesa de diálogos para resolver el escenario de implementación.

-       Brindar todas las garantías para la participación y la movilización de toda la ciudadanía.

**A los medios de comunicación:**

 -       Exigimos un manejo responsable de la información.

-       Es necesario el uso de un lenguaje que posibilite una nueva realidad de reconciliación y paz en el país.

**A la sociedad colombiana:**

 -       Llamamos a que se genere una apertura al diálogo social respetuoso de las diferentes ideas y que lleve a una despolarización del país.

-       La movilización ciudadana desde todos los sectores debe mantenerse y profundizarse desde la unidad y la articulación.

-       Saludamos las múltiples iniciativas que a lo largo y ancho de la geografía nacional se han desarrollado en favor de la paz y nos comprometemos a acompañarlas y fortalecerlas.

 **A las y los estudiantes:**

 -       Invitamos a la articulación activa y creativa con los distintos sectores sociales en la defensa y construcción de la paz y a la articulación misma de las iniciativas estudiantiles a nivel nacional. La unidad es una premisa fundamental en esta etapa de movilización en calles y universidades del país.

-       Velar porque nuestras universidades e Instituciones educativas sean territorios de paz y puedan constituirse en sujetos de reparación colectiva, debido al ejercicio de la violencia en su interior, es fundamental en este escenario.

-       Se hace necesario iniciar un diálogo nacional que proyecte las propuestas y los retos para la construcción de una educación como derecho hacia la edificación de la paz.

 **A la guerra le decimos: ¡Nunca Más!**

**¡Somos la generación de la esperanza!**

**¡Vamos por la paz!**

<iframe id="audio_13373996" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13373996_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
