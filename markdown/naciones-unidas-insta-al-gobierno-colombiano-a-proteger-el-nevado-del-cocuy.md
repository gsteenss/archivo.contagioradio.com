Title: Naciones Unidas insta al gobierno colombiano a proteger el Nevado del Cocuy
Date: 2016-09-07 16:02
Category: Comunidad, Nacional
Tags: ministro de Ambiente, Nación Uwa, Nevado del Cocuy, Parques Naturales, pueblos indígenas
Slug: naciones-unidas-insta-al-gobierno-colombiano-a-proteger-el-nevado-del-cocuy
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Zizuma-2-e1473281874409.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Kinorama 

###### [7 Sep 2016] 

Durante el **Congreso Mundial por la Naturaleza que se realiza en Hawai del primero al 10 de septiembre,** representantes de la Nación U’wa de Colombia, lograron que el tema de la conservación del nevado del Cocuy fuera parte de uno de los informes de Naciones Unidas, por medio del cual se insta al gobierno a tomar las medidas necesarias para proteger esta zona del país en concordancia con la garantía de los derechos de la comunidad indígena.

El Informe de la Relatora Especial para los derechos de los pueblos indígenas, Victoria Tauli-Corpuz incluyó el tema de la conservación del Nevado del Cocouy, o el Zizuma como le llama la comunidad indígena. **Allí se exhorta a los estados a reconocer la sabiduría ancestral y con ello, garantizar la participación plena de los pueblos indígenas**, para de esta manera adoptar todas las medidas normativas, jurídicas y administrativas necesarias para reconocer los derechos de los indígenas en el marco de la delimitación de los Parques Naturales.

Lo anterior, teniendo en cuenta que desde hace 6 meses la Nación Uwá, a través del Movimiento Político de Masas del Centro Oriente de Colombia, se encuentran en Acción Colectiva Pacifica, ejerciendo control territorial a través de la Guardia indígena exigiendo respeto a sus sitios sagrados y el cumplimiento a los acuerdos pactados en 2014.  En ese sentido, **la comunidad ha exigido a Parques Nacionales la resolución suspendiendo el eco-turismo en el parque el cocuy** mientras se realiza el estudio de impactos, permitiendo que la comunidad indígena de a conocer su cosmovisión y de elementos que permitan la verdadera protección de Zizuma.

Sin embargo este **avance se encuentra amenazado por las últimas declaraciones del Ministro de Ambiente, Luis Murillo** “quien nunca ha participado en la mesa de dialogo expone que es orden presidencial de reabrir el ecoturismo en el Cocuy, esto es una claro incumplimiento del gobierno a respetar lo acordado y pasa por encima de los derechos de nuestro pueblo, ante lo cual la Nación U´wa dejamos claro que continuaremos en movilización”, dice la comunidad.

<iframe src="https://co.ivoox.com/es/player_ej_12817522_2_1.html?data=kpelk5yZdpOhhpywj5WdaZS1lJWah5yncZOhhpywj5WRaZi3jpWah5yncaLp08aYtsrLtsrVhpewjbPFp8qZpJiSpJjSb7brhqigh6aVcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
