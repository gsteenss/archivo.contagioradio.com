Title: Mapuches marcharon para exigir el reconocimiento de sus derechos como pueblos indígenas
Date: 2015-10-13 15:31
Category: El mundo, Entrevistas
Tags: 12 de octubre, Alameda, Chile, Día de la raza, día de la resistencia indígena, Palacio de la Moneda, Plaza Italia, Pueblo Mapuche, Santiago de Chile
Slug: mapuches-marcharon-para-exigir-el-reconocimiento-de-sus-derechos-como-pueblos-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/12-de-octubre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### actualidad.rt.com 

###### [13 oct 2015 ] 

Tras cumplirse **523 años de lucha y resistencia de las comunidades indígenas** en América Latina, en Santiago de Chile miles de personas salieron a las calles a respaldar al **pueblo Mapuche,** quienes en el marco de la conmemoración del 12 de octubre se manifestaron para **exigir el reconocimiento de sus derechos como pueblos originarios. **

Los indígenas chilenos solicitaron que el 12 de octubre sea reconocido como el día en que se inició el exterminio de los pueblos originarios de América Latina, teniendo en cuenta que han sido discriminados históricamente en su país, donde han mantenido la resistencia Mapuche que alberga diferentes comunidades indígenas que luchan permanentemente por la defensa de sus territorios desde los 90.

Es por eso que la convocatoria en Facebook para la marcha expresaban, “**Nuestro pueblo mapuche continua resistiendo dignamente el colonialismo opresor del Estado chileno y el capitalismo destructivo instaurado en nuestro territorio".**

La marcha se inició en la Plaza Italia, luego se dirigieron por la Alamed (principal avenida de Santiago de Chile) para llegar al Palacio de la Moneda donde se produjeron enfrentamientos entre manifestantes y la Policía, mientras los indígenas reclamaban los derechos sobre sus tierras y la libertad de los presos políticos.
