Title: Especiales Contagio Radio
Date: 2020-01-09 11:01
Author: CtgAdm
Slug: especiales-contagio-radio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ESPECIALES-CONTAGIO-RADIO.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/familia.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/DSC3271.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/GAB0592.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/especiales-contagio-radio.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### MULTIMEDIA

ESPECIALES CONTAGIO RADIO
-------------------------

Especiales que narran las historias que se viven en los territorios de Colombia, propuestas que nacen a pesar de la guerra. Apuestas por la vida, la memoria y el territorio. 

[RETORNÓ LA VIDA A PICHIMÁ QUEBRADA](https://archivo.contagioradio.com/retorno-la-vida-a-pichima-quebrada-2/)
=========================================================================================================

[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Google-plus  
]()

Con banderas blancas en cada embarcación inició el retorno, más de 90 familias Wounaan decidieron enfrentar el silencio abrumador del Estado y retornaron a su comunidad, sin garantías, pero con el alma feliz porque retornaban a su tierra

[  
VER  
](https://archivo.contagioradio.com/retorno-la-vida-a-pichima-quebrada-2/)

DIC 2019

[LAS MEMORIAS DE DABEIBA:  
EN BUSCA DE LA VERDAD](https://archivo.contagioradio.com/retorno-la-vida-a-pichima-quebrada-2/)
===========================================================================================

[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Google-plus  
]()

En la Zona Humanitaria de la Balsita, en la vereda Caracolón, municipio de Dabeiba, se realizó el cuarto Festival por las Memorias, un encuentro por la paz y la reconcialiación.

[  
VER  
](https://archivo.contagioradio.com/las-memorias-de-dabeiba-en-busca-de-la-verdad/)

DIC 2019

[EL PATO, EL RÍO QUE CAMBIÓ PARA LA PAZ](https://archivo.contagioradio.com/retorno-la-vida-a-pichima-quebrada-2/)
=============================================================================================================

[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Google-plus  
]()

El Río Pato en Caquetá,  fue la sede del Campeonato Nacional de Rafting Remando por la Paz, un escenario de reconciliación con  la participación de deportistas, comunidad campesina, excombatientes y Fuerza Pública

[  
VER  
](https://archivo.contagioradio.com/el-pato-el-rio-que-cambio-para-la-paz-2/)

NOV 2019

[DIAAG MAAG, LA ESPERANZA DEL PUEBLO WOUNAAN](https://archivo.contagioradio.com/diaag-maag-la-esperanza-del-pueblo-wounaan/)
========================================================================================================================

[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Google-plus  
]()

Los Wounnan se exponen a ser revictimizados por un mundo desequilibrado espiritualmente, que obligó a Ewandan a bajar de sus montañas para atraparlo en un frío albergue…

[  
VER  
](https://archivo.contagioradio.com/diaag-maag-la-esperanza-del-pueblo-wounaan/)

SEP 2019

[LA GABARRA, 20 AÑOS DESPUÉS](https://archivo.contagioradio.com/la-gabarra-20-anos-despues/)
========================================================================================

[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Google-plus  
]()

La localidad del Norte de Santander conmemoró el vigésimo aniversario de la masacre paramilitar sin haber sido reparada y obligada a seguir viviendo de la coca como forma de subsistencia.

[  
VER  
](https://archivo.contagioradio.com/la-gabarra-20-anos-despues/)

AGO 2019

[PICHIMA QUEBRADA, DOS VECES DESTERRADA POR LA GUERRA](https://archivo.contagioradio.com/pichima-dos-veces-desterrados-por-la-guerra/)
==================================================================================================================================

[  
Facebook  
]()  
[  
Twitter  
]()  
[  
Google-plus  
]()

Por segunda vez la comunidad indígena de Pichima Quebrada ha tenido que salir de su territorio huyendo de la guerra, más de 90 familias completan 30 días desplazadas.

[  
VER  
](https://archivo.contagioradio.com/pichima-dos-veces-desterrados-por-la-guerra/)

AGO 2019

[  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/logo-contagio-radio-web-1.png){width="201" height="76"}](https://archivo.contagioradio.com/)

##### [OTRA FORMA DE ENCONTRARNOS](https://archivo.contagioradio.com/)
