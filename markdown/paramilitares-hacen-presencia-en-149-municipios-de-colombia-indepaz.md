Title: Paramilitares hacen presencia en 149 municipios de Colombia: Indepaz
Date: 2016-04-22 15:00
Category: Nacional
Tags: CIDH, INDEPAZ, Paramilitarismo
Slug: paramilitares-hacen-presencia-en-149-municipios-de-colombia-indepaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Paramilitarismo2-e1461348491716.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Universidad del Atlántico 

###### [22 Abr 2016]

El más reciente informe del Instituto de Estudios para el Desarrollo y la Paz, Indepaz, sobre la presencia de grupos narcoparamilitares durante los 3 primeros meses de 2016, evidencia que en el país hay 149 municipios y 22 departamentos afectados por las actuaciones de 14 organizaciones paramilitares.

[Antioquia, Meta, Chocó y Córdoba son los departamentos donde se evidencia mayor control paramilitar,](https://archivo.contagioradio.com/es-el-paramilitarismo-un-fantasma/) que de acuerdo con Leonardo González, coordinador de la Unidad Investigativa de Indepaz, se trata de una verdadera amenaza para los procesos de paz con las FARC y el ELN.

“**Hoy los acuerdos de paz que se realicen tendrán una dificultad mayor para su implementación de existir en el territorio grupos paramilitares** y no solo por la seguridad de quienes hayan decidido dejar las armas, sino especialmente para las comunidades y poblaciones donde se vayan a poner en marcha los planes de implementación en la etapa de transición a la paz”, dice González.

En su análisis, el investigador de Indepaz plantea que tras la supuesta desmovilización paramilitar durante el gobierno de Álvaro Uribe Vélez, el Estado se ha dedicado a ocultar este fenómeno sin reconocerlo, lo que ha generado ambigüedad por parte de las autoridades para enfrentarlos, pues no se tiene en cuenta que se trata de **“un fenómeno político, económico y militar, de una múltiple alianza”**.

Eso a su vez, evidencia la clara **relación que existe entre estas estructuras y agentes estatales.** Desde Indepaz, se señala que algunos grupos siguen operando de forma tradicional como lo hace la estructura ‘Águilas Negras’. Asimismo, se ha evidenciado que aún existe una relación militar entre estos grupos neoparamilitares con la fuerza pública.

[Ante la Comisión Interamericana de Derechos Humanos se había evidenciado esa situación](http://Danilo%20Rueda,%20abogado%20integrante%20de%20la%20Comisión%20de%20Justicia%20y%20Paz,%20citó%20como%20ejemplos%20las%20acciones%20violentas%20que%20se%20dieron%20en%20el%20marco%20del%20reciente%20paro%20armado%20decretado%20por%20las%20Autodefensas%20Gaitanistas;%20así%20como%20el%20control%20paramilitar%20de%2082%20barrios%20de%20Buenaventura,%20pese%20a%20la%20constante%20militarización%20del%20municipio;%20la%20presencia%20de%20más%20de%20300%20paramilitares%20armados%20en%20el%20Bajo%20Atrato,%20quienes%20se%20movilizaron%20pasando%20por%20retenes%20militares;%20y%20la%20libre%20movilidad%20de%20por%20lo%20menos%20200%20hombres%20armados%20y%20vestidos%20de%20camuflado%20en%20Putumayo,%20a%20escasos%20metros%20de%20una%20base%20militar%20financiada%20por%20los%20Estados%20Unidos.). Por ejemplo en Buenaventura, hay control paramilitar en 82 barrios pese a la constante militarización del municipio; también se ha evidenciado la presencia de más de 300 paramilitares armados en el [Bajo Atrato](https://archivo.contagioradio.com/?s=Bajo+Atrato), pese a los retenes militares; y la libre movilidad de por lo menos 200 hombres armados y vestidos de camuflado en [Putumayo](https://archivo.contagioradio.com/?s=putumayo), a escasos metros de una base militar financiada por los Estados Unidos.

Es por ello, que el análisis de González, concluye con la necesidad de que el Estado genere estrategias efectivas de desmantelamiento de los grupos paramilitares y de todas sus redes políticas, militares y cadenas de negocios, a partir de “**un componente social para proteger a la población de la guerra y así aislar estos factores mafiosos narcoparamilitares**”.

<p>
<script id="infogram_0_C2hSyuIyk61xmYwW" title="Presencia narcoparamilitares en los primeros tres meses de 2016" src="//e.infogr.am/js/embed.js?bzv" type="text/javascript"></script>
</p>
<div style="padding: 8px 0; font-family: Arial!important; font-size: 13px!important; line-height: 15px!important; text-align: center; border-top: 1px solid #dadada; margin: 0 30px;">

[Presencia narcoparamilitares en los primeros tres meses de 2016](https://infogr.am/C2hSyuIyk61xmYwW)  
[Create your own infographics](https://infogr.am)

</div>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
