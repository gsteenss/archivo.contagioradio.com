Title: Futuro de la negociación con el ELN dependerá del nuevo presidente: Víctor de Currea
Date: 2018-06-12 16:05
Category: Nacional, Política
Tags: Cese al fuego, cese unilateral, ELN, proceso de paz
Slug: futuro-de-la-negociacion-con-el-eln-dependera-del-nuevo-presidente-victor-de-currea
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/98c5ba09-bb46-4488-8881-91d39d3798c8.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tomás García Laviana] 

###### [12 Jun 2018] 

Tras el anuncio del cese al fuego unilateral para la jornada electoral decretado por el ELN, analistas políticos recordaron que **no es la primera vez que esto ocurre** y afirmaron que este tipo de prácticas favorecen los procesos electorales. Adicionalmente, el proceso de negociación en la Habana se mantiene en búsqueda de un cese bilateral.

De acuerdo con el profesor y analista Víctor de Currea Lugo, el ELN ha venido realizando cese al fuego de manera unilateral desde las votaciones para la refrendación del **plebiscito** sobre el Acuerdo de Paz entre el Gobierno y las FARC. Afirmó que la suspensión de actividades militares con el fin de que se realicen las elecciones de manera tranquila, evidencia que el ELN “no impone candidatos y tampoco los impide”.

### **Proceso de paz con el ELN continúa avanzando** 

Adicionalmente, indicó que el proceso de paz con esa guerrilla se “sigue trabajando” donde los temas más importantes son el cese al fuego bilateral y la **definición de los mecanismos de participación de la sociedad civil.** Aclaró que esto no da garantías de una continuación y “lo que suceda en la mesa dependerá de quien sea el nuevo presidente”. (Le puede interesar:["ELN decreta cese al fuego unilateral durante jornada electoral"](https://archivo.contagioradio.com/eln-decreta-cese-al-fuego-unilateral-durante-jornada-electoral/))

El profesor enfatizó en que la llegada del nuevo presidente no debería afectar el proceso de negociación con el ELN en la medida en que “a ellos les da igual con quien deban negociar”. Sin embargo, afirmó que son preocupantes los anuncios del candidato **Iván Duque** en lo relacionado con la continuidad de la negociación únicamente con condiciones unilaterales de concentración de las fuerzas del ELN.

De Currea recordó que el ELN ha rechazado esta idea de concentración por lo que es posible que haya algún tipo de tensión en una mesa de diálogo **“de por sí frágil”**. Agregó que nada está escrito en los procesos de paz teniendo como evidencia el Acuerdo de Paz con las FARC en donde este ha sido “torpedeado”.

<iframe id="audio_26499578" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26499578_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
