Title: Es necesario actuar ya para salvar al Arroyo Bruno en la Guajira
Date: 2019-07-12 18:17
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: arroyo Bruno, Cerrejón, Guajira
Slug: necesario-actuar-salvar-arroyo-bruno-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/bruno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

###### [Foto: Archivo Contagio Radio ] 

La comunidad de La Guajira clama porque sean retirados los tapones que desvían el cauce natural del Arroyo Bruno; pese a la sentencia SU 698 de 2017 de la Corte Constitucional,  el impacto ambiental que implica y las consecuencias que trae a los Wayúu y a los afro que sobreviven con su afluente, pues si el Arroyo Bruno desaparece ellos y ellas también estarían condenados a desaparecer por la falta del agua en el único bosque seco de Colombia.

La abogada Miladys Giovanetti, habitante de la región,  relata que “el lunes 8 de julio, en compañía de la Contraloría, Defensoría del pueblo, Corpoguajira y la Autoridad Nacional de Licencias Ambientales (ANLA), se evidenció  el daño ambiental que ha causado la minería y la desviación del cauce natural del afluente”. (Le puede interesar: ["Comunidades de la Guajira fueron excluidas de discusión sobre el futuro del Arroyo Bruno"](https://archivo.contagioradio.com/arroyo-bruno-afecta-guajira/))

### **También la fauna corre peligro si se secan las aguas del Arroyo Bruno**

Además del evidente deterioro del Arroyo Bruno, encontraron especies como un tigrillo e iguanas muertas, lo que podría estar ligado al desvío del acuífero no solo desapareciendo al arroyo, sino también afectando la fauna y la flora. (Le puede interesar ["Comunidades de la Guajira demandaron a Cerrejón y piden anular licencia ambiental"](https://archivo.contagioradio.com/comunidades-la-guajira-presentan-demanda-cerrejon-estado/))

Giovanetti expresa que “de nada sirven las las compensaciones que mencionan las empresas y el Estado como lo son pozos o trampas de agua si nos quedamos sin acuíferos” la condición ambiental que enfrenta la comunidad es lamentable, pues el estado no atiende sus súplicas. La continuidad de los tapones también genera que las poblaciones de  Alabania, Barrancas y Hatonuevo consuman agua contaminada, y no cuenten con abastecimiento suficiente.

### **Cerrejón se impone e incumple sentencia de la Corte Constitucional**

El 9 de Julio se desarrolló en el Centro Cultural de Riohacha la audiencia regional,  en la que estuvieron presentes la ANLA, el Instituto de Hidrología y Estudios Ambientales (IDEAM), según Giovanetti se ve la omisión del Estado pues se niega a atender la sentencia unificada 698 de 2017 de la Corte Constitucional, en la que se dispone el riesgo que representa para las comunidades el desvío del Arroyo Bruno.

Uno de los asistentes a la audiencia comentó  que "el viceministro de Relaciones Políticas se expresó de forma grosera con el público; igualmente, el Presidente de SINTRACARBÓN fue grabado de forma intimidante por un miembro de la empresa minera". A esto se suman las amenazas en contra del Expresidente del sindicato. (Le puede interesar: ["¿Por qué el 98% de los trabajadores del Cerrejón están dispuestos a iniciar una huelga?"](https://archivo.contagioradio.com/por-que-el-98-de-los-trabajadores-del-cerrejon-estan-dispuestos-a-iniciar-una-huelga/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38331301" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38331301_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
