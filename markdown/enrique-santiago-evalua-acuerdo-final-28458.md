Title: “No puedo hacer algo más satisfactorio que contribuir a acabar con el conflicto” Enrique Santiago
Date: 2016-08-26 17:01
Category: Entrevistas, Paz
Tags: acuerdo final, Conversaciones de paz de la habana, enrique santiago, FARC
Slug: enrique-santiago-evalua-acuerdo-final-28458
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Enrique-Santiago.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [26 Ago 2016]

El abogado [Enrique Santiago](https://archivo.contagioradio.com/?s=enrique+santiago), uno de los artífices del acuerdo final de paz, que acaba con más de 50 años de conflicto en Colombia, **asegura que ha sido uno de los trabajos más satisfactorios de su vida**, además agrega que la decisión de acabar con la violencia fue la verdadera fuerza del acuerdo, así como el trabajo colectivo que se realizó durante estos 4 años de conversaciones de paz. “ojalá sea así y se cierre ese ciclo de violencia” resalta.

**“La voluntad de acabar con la violencia ha sido lo que logró el acuerdo final”** reconoce el abogado, asesor del proceso de paz y uno de los principales artífices de la J[urisdicción Especial de Paz](https://archivo.contagioradio.com/victimas-y-jurisdiccion-especial-de-paz/), uno de los más avanzados del mundo en materia de acuerdos de paz. Respecto de la JEP han asegurado diversas entidades nacionales e internacionales que se logra permitir el a[vance del derecho hacia un derecho basado en la posibilidad de restaurar los daños provocados](https://archivo.contagioradio.com/entrevista-con-enrique-santiago-justicia-en-el-proceso-de-paz/) por una actividad fuera de la ley.

Enrique Santiago afirmó que frente a la implementación, le corresponde a los colombianos y señala que es necesario que se haga un “relevo a la mesa de conversaciones”, según el abogado ahora todo el mundo “ se remangará” y comenzará a trabajar para la implementación de esos acuerdos. Agrega que e**spera que ahora se dejen de invertir recursos en construir y se comience a construir.**

Respecto a la amnistía afirma que el procedimiento está inscrito en el trámite del procedimiento urgente del acto legislativo para la paz, es decir, al día siguiente del plebiscito, siempre y cuando la voluntad expresada sea favorable al tratado. Antes del marco jurídico de paz se ha acordado **un mecanismo inmediato de excarcelación para personas condenadas o acusadas de pertenecer a la insurgencia y para personas que fueron acusadas pero no se reconocen como tal**. Esas medidas se aplicaran al mismo tiempo del inicio del proceso de dejación de armas.

Además hay una medida muy importante que tiene que ver con el tratamiento diferenciado a las mujeres en situación de pobreza que tienen cargas familiares y que se vieron obligadas a cometer delitos menores relacionados con el narcotráfico, en ese sentido resalta que este colectivo de mujeres y **los campesinos y campesinas, detenidos en el ejercicio de la protesta socia**l y acusados de rebelión tienen un punto de ganancia en ese acuerdo.

Por último el abogado resalta que estos cuatro años de trabajo han sido muy duros y el papel de los **[medios de comunicación distorsionaron](https://archivo.contagioradio.com/abogado-enrique-santiago-a-bluradio/) los mensajes de lo que pasaba en la mesa de conversaciones y por ello el trabajo de los medios de comunicación que han asumido la defensa del derecho a la paz** ha sido también crucial en alcanzar el acuerdo final.

<iframe src="http://co.ivoox.com/es/player_ej_12678198_2_1.html?data=kpejmZ2VfZmhhpywj5abaZS1k5uah5yncZOhhpywj5WRaZi3jpWah5yncabi087e18qPl8Li1c7OydSJdqSfwtjS1dTWb8vp04qwlYqliMXdxNSYstfTp8bn0JDRx5DUpduhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
