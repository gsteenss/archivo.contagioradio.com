Title: Esta semana se fortalecen las movilizaciones por la paz
Date: 2017-10-02 13:29
Category: Otra Mirada, Paz
Tags: FARC, Gobierno, Implementación, JEP, Movilizaciones por la paz, voces de paz
Slug: 47453-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [02 Oct 2017]

Cuando se cumple un año de la votación del plebiscito y también la gran marcha por la paz que colmó la Plaza de Bolívar y las calles aledañas exigiendo la implementación, diversas **organizaciones sociales y políticas están volviendo a convocar movilizaciones por la paz, puesto que según muchas de ellas la implementación está en riesgo**, dadas las demoras en el congreso y en la implementación de garantías de seguridad y económicas para integrantes de las FARC.

Una de las primeras acciones es la audiencia pública que se desarrolla a esta hora en el congreso de la república en la que se espera que se definan rutas y propuestas para que se implemente de manera eficaz el acuerdo en el congreso, pero también para **acelerar el cumplimiento de las garantías de seguridad en las zonas veredales, así como las garantías económicas que permitan la reincorporación de los integrantes de las FARC.**

Otra de las actividades convocadas es a las afueras del Congreso de la República en la que participarían diversas organizaciones con la idea de **motivar a los y las congresistas encargados de viabilizar los acuerdos, especialmente la ley estatutaria de la JEP** que tendría trámite esta semana. Además publican las listas de cuenta de Twitter de los congresistas para que a través de redes sociales también se de la acción en favor de la implementación. [Lea también: la ciudadanía es veedora de la JEP](https://archivo.contagioradio.com/la-ciudadania-debera-ser-veedora-de-la-jurisdiccion-especial-de-paz/)

### **Estos son los senadores:** 

@motoasenador, @German\_VaronC, @JaimeAminH, @JOSEOBDULIO, @AlRangelS, @PalomaValenciaL, @doriscvega, @AndradeSenador, @RoyBarreras  @AABenedetti, @juanmanuelgalan, @robgerlein, @MoralesViviane, @HoracioSerpa, @ClaudiaLopez, @AlexLopezMaya, @RoosveltRodri

### **Estos son los representantes a la cámara** 

@norbeymm, @CLARAROJASG, @polosuarezmelo, @SILVIOJCT, @16oscarsanchez, @JulianBedoya107, @AlbeiroVanegas, @NeftaliSantosRa, @MiguelPintoH1, @HarryGonzalez, @carlosacorream, @BernerZambrano, @CarlosEdward\_O, @HernanPenagos, @josecaicedos, @elbertdiaz103, @JaimeBuenahora, @AngelicaLozanoC, @GNavasTalero, @SHOYOS, @sanvalgo, @ALVAROHPRADA, @MariaFdaCabal, @EdwardR\_CD, @HRoaSarmiento, @teleC10, @unnortejoven, @PEDRITOPEREIRA1, @hesanabria, @oscarbravo101, @Rodrigo\_Lara\_, @JorgeERozo, @abrahamcongreso, @jomolinaf, @Fernandodlp2014

Este martes 3 de Octubre se realizará un conversatorio en la sede de Casa Ensamble en la que se espera que por parte de los grupos de jóvenes que impulsaron las marchas de 2016, definan una ruta de movilización que nuevamente deje caro el mensaje de que es necesario que implementen los acuerdos de manera urgente y eficaz. [Lea también: La paz necesita de la ciudadanía](https://archivo.contagioradio.com/la-jep-necesita-a-la-ciudadania-en-movilizacion-para-defenderla/)

Según Jairo Rivera de Voces de Paz e Iván Cepeda, Senador, se espera que a partir de estas jornadas que podrían llamarse de sensibilización frente a la situación del acuerdo y **es probable que se vuelva a convocar a las calles este 5 de Octubre.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
