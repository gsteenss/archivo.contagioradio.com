Title: Las marchas estudiantiles seguirán hasta que haya mesa de concertación con el Gobierno
Date: 2018-10-25 22:55
Author: AdminContagio
Category: Educación, Movilización
Tags: Educación Superior, estudiantes, Marchas, Mesa de Concertación, UNEES
Slug: marchas-mesa-de-concertacion
Status: published

###### [Foto: @UNEES] 

###### [25 Oct 2018] 

15 días después de la gran marcha en la que estudiantes se tomaron el país para reclamar mejores condiciones para la educación superior pública, asambleas permanentes, paros y huelgas de hambre realizadas en diferentes universidades han logrado que el Gobierno abra las puertas para la negociación; sin embargo, **estudiantes advierten que hasta no tener algunas garantías, las movilizaciones se mantendrán.**

<iframe src="https://co.ivoox.com/es/player_ek_29636816_2_1.html?data=k56jlZucdZehhpywj5aUaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncavZ1IqwlYqmhdSfrcrc0MbWqNCfutTZxs6JdqSfxtjh18nNpc_oxpDCsrmnb8bijM3ix9HLpYzYxpDVw9KRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Uno de los manifestantes es **Jesús Leonardo Yoldi, estudiante de la Universidad Pedagógica y Tecnológica de Colombia (UPTC)**, y quien lleva 10 días de huelga de hambre. Aunque, Yoldi afirmó que sus compañeros, administrativos de la Universidad, profesores y hasta senadores lo han visitado, indicó que no levantará la huelga hasta no lograr una mesa de concertación con el Gobierno.

El estudiante sostuvo que sí hay recursos en la educación y podrían buscarse en los dineros que pierde el país por culpa de la corrupción. Por eso, es posible pensar que el Gobierno puede comprometerse en hechos concretos; con ese objetivo, tanto **Yoldi como los profesores y otros universitarios que mantienen las huelgas de hambre esperarán para terminar su ayuno.** (Le puede interesar:["Profesores de universidades públicas protestan con huelga de hambre"](https://archivo.contagioradio.com/profesores-huelga-de-hambre/))

Adicionalmente, la necesidad de llegar a una mesa de concertaciones en la que se acuerden puntos específicos para sacar a la educación pública de la crisis estructural que vive, tiene que ver con la **garantía de que esta situación no se repetirá,** porque de otra forma, los estudiantes tendrían que volver a paro en dos o tres años, como lo han hecho los profesores, los campesinos o los transportadores.

### **\#SinProfesNoSeNegocia**

<iframe src="https://co.ivoox.com/es/player_ek_29636830_2_1.html?data=k56jlZucd5Ghhpywj5WXaZS1lJ6ah5yncZOhhpywj5WRaZi3jpWah5yncbTVz9nWw8zTb6iZpJiSpJjRqduZk6jj0cjJttCfxcqYzsaPmc_d18rf1c7IpcWfxcqYo9PYrdDl1s7OjcaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Santiago Gómez,vocero de la Universidad de Antioquia ante la UNEES**, sostuvo que el Gobierno hizo el llamado para que el Ministerio de Educación Nacional (MEN) se reuniera con los estudiantes; sin embargo, de la reunión fueron excluidos los representantes de posgrados y los docentes, por eso, el llamado es a que se integren en la mesa estos sectores de la educación.

En ese sentido, Gómez aseguró que sería ideal llegar a un mínimo de entendimientos con el MEN, y de igual forma, recalcó la importancia de "unificar conceptos básicos" con los docentes, para **acordar soluciones concretas frente al problema de la educación superior pública que vive el país**. (Le puede interesar: ["Generación E: ¿El nuevo Ser Pilo Paga?"](https://archivo.contagioradio.com/generacion-e/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
