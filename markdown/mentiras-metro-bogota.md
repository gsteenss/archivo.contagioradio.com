Title: Irregularidades y mentiras sobre el Proyecto Metro de Bogotá
Date: 2018-09-28 11:11
Author: AdminContagio
Category: Nacional, Política
Tags: Hollman Morris, Juan Pablo Bocarejo, Metro Bogotá, Peñalosa
Slug: mentiras-metro-bogota
Status: published

###### [Foto: @Bogotá] 

###### [28 Sep 2018] 

En el debate de control político sobre el Proyecto Metro de Bogotá, **la bancada de oposición presentó la que sería una prueba irrefutable de que la alcaldía de Enrique Peñalosa mintió sobre los estudios de factibilidad de la iniciativa,** hecho que constituye una ilegalidad en el trámite de la misma, y un riesgo económico que tendrían que asumir los capitalinos y que podría ser una catástrofe financiera parecida a la de Reficar.

<iframe src="https://co.ivoox.com/es/player_ek_28958452_2_1.html?data=k52ml52YeZOhhpywj5WbaZS1k52ah5yncZOhhpywj5WRaZi3jpWah5ynca7Vz9rSzpC3pdPhysrb1tSJdqSfpNTbxcrOpc2fsdTZ0ZDIqc7jxNeSpZiJhZLoysjcj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **¿Qué pasó con el CONPES del Metro?** 

El Concejal Manuel Sarmiento explicó que el proceso para construir el Metro inició gracias a la Ley 310 de 1996 en la que se señala que la nación debe cofinanciar los proyectos de transporte masivo; y para obtener esos recursos, el distrito debió hacer un documento CONPES (que es una aprobación de los planes económicos y de inversión a nivel nacional). Un requisito para obtener dicho documento, es que **la alcaldía de Bogotá presente un estudio de factibilidad del proyecto**, que es la investigación sobre cómo se realizarían las obras.

El CONPES del metro de Bogotá se aprobó en septiembre del año pasado, sin embargo, la bancada de oposición del Concejo dijo que **ese documento no tenía estudios de factibilidad**, porque de los 23 productos que hacían parte del oficio, sólo 1 estaba listo. En ese momento, la Administración Distrital se defendió diciendo que los estudios estaban tan adelantados que tenían factibilidad.

Pero ahora, l**a Financiera de Desarrollo Nacional envió un oficio** a Andrés Escobar, gerente de la empresa Metro, en el que **señalaba que los estudios con los que se aprobó el CONPES son una versión preliminar, cuya publicación final se concluyó en agosto de este año**. Para Sarmiento, el hecho prueba que la Alcaldía mintió al mostrar un CONPES sin estudios de factibilidad, situación que conlleva a un riesgo financiero y de corrupción sobre el Proyecto.

> El gerente Andrés Escobar dijo que ninguna ley exigía un estudios de factibilidad terminado. ¡FALSO! La ley 310 de 1996 lo señala claramente, exige un estudio completo, no a medias ni una versión preliminar: [\#AUnMetroDeLaCatástrofe](https://twitter.com/hashtag/AUnMetroDeLaCat%C3%A1strofe?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/23jookHxLH](https://t.co/23jookHxLH)
>
> — Manuel Sarmiento (@mjsarmientoa) [27 de septiembre de 2018](https://twitter.com/mjsarmientoa/status/1045410035998896128?ref_src=twsrc%5Etfw)
>
> \[caption id="attachment\_57130" align="aligncenter" width="445"\][![Proyecto Metro de Bogotá](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/Captura-de-pantalla-2018-09-28-a-las-11.19.41-a.m.-445x572.png){.wp-image-57130 .size-medium width="445" height="572"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/Captura-de-pantalla-2018-09-28-a-las-11.19.41-a.m..png) Foto: @mjsarmientoa\[/caption\]

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
El Concejal sostuvo que los efectos de esa irregularidad los pagarán los bogotanos, porque es el Distrito el que tiene que cubrir los sobre costos; y recordó que en el pasado debate de control político sobre el tema, Escobar había aceptado que el valor del metro ya no era de **12,9 billones sino otra cifra más grande**, aunque no precisó cual. (Le puede interesar: ["El proyecto elevado de Peñalosa es el mejor ejemplo de corrupción sistémica: Hollman Morris"](https://archivo.contagioradio.com/metro-elevado-corrupcion-sistemica/))

### **Debemos reestructurar el proyecto del metro para Bogotá** 

Sarmiento afirmó que en el debate hubo dos cosas por discutir: Si el metro que se está proponiendo es adecuado para Bogotá, y la respuesta es que no; y si está bien planteado, y la respuesta es que tampoco. En primer lugar, porque la iniciativa que se está presentando es un "metrico" alimentador de transmilenio; y en segundo lugar, porque tiene irregularidades con las que se puede caer en sobre costos.

El Concejal por el Polo aseguró que los próximos pasos a seguir deberían ser reestructurar el proyecto, tumbar el CONPES aprobado y hacer uno nuevo, que "basado en los estudios que se han hecho y las cifras de los diferentes análisis, señalan con toda contundencia que el metro que le sirve a Bogotá es subterráneo, convirtiéndolo en la columna vertebral de la movilidad en la capital".

<iframe src="https://co.ivoox.com/es/player_ek_28958420_2_1.html?data=k52ml52YdpGhhpywj5WaaZS1lZiah5yncZOhhpywj5WRaZi3jpWah5yncanjzdHaw9OPkdDm087gh5enb9Tjw9fSjdHFt4zhxtPhy9fFt4zZjM7f1MrLuc3V087Rw8nJt4zk0JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Por su parte, el también **concejal Hollman Morris** anuncio  que radicó una **denuncia penal contra la directora del Idu, Janeth Mantilla, y el secretario de movilidad de Bogotá, Juan Pablo Bocarejo,** por la presunta comisión de 3 delitos que tienen que ver con el Metro elevado: 1. El cambio de un objeto de un convenio, porque Mantilla cambió la razón del convenio que dio nacimiento al proyecto del metro subterráneo por un metro elevado; 2. Sacar adelante la iniciativa sin los requisitos de Ley; y 3. Por fraude, al mentir sobre los estudios de factibilidad.

###### [Reciba toda la información de Contagio Radio en ][su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
