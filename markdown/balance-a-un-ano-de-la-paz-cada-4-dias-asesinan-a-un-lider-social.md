Title: Balance a un año de la paz: Cada 4 días asesinan a un defensor de DDHH
Date: 2017-11-24 17:29
Category: Nacional, Paz
Tags: acuerdo de paz, Agresiones contra defensores de DDHH, defensores de derechos humanos, lideres sociales
Slug: balance-a-un-ano-de-la-paz-cada-4-dias-asesinan-a-un-lider-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/lideres-asesonados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Nov 2017]

La situación de las y los líderes sociales, a un año de la firma de los Acuerdos de Paz, es apremiante en la medida que, desde la firma del mismo, hasta septiembre de 2017, **ha habido 270 agresiones contra líderes sociales**.

La Fundación Paz y Reconciliación indicó que ha habido 2 agresiones sexuales, 150 amenazas, 29 atentados, 2 casos de desaparición forzada, 84 homicidios y 3 secuestros. Para la Fundación, “hasta el 1 de octubre (2017) **se cometió una agresión por día**”. Además, en el país, desde la firma del Acuerdo de Paz, asesinan a un líder social cada 4 días. (Le puede interesar: ["Balance a un año de la paz: Cuatro aspectos de la crisis en la reincorporación de FARC"](https://archivo.contagioradio.com/balance-a-un-ano-de-la-paz-en-crisis-reincorporacion-de-las-farc-a-la-vida-civil/))

### **Impunidad desbordada en los crímenes contra defensores** 

De acuerdo con el reporte, los departamentos más afectados son **Cauca, Valle del Cauca, Norte de Santander y Tolima.** Allí y en el resto del país, ha sido muy difícil “rastrear e identificar los actores intelectuales de los hechos victimizantes” en la medida que la red criminal de las organizaciones armadas “cuenta con sicarios que son los principales perpetradores materiales de las múltiples violaciones a los derechos humanos”.

Esto, para Paz y Reconciliación, es una de las razones por las cuales la tasa de impunidad frente a los responsables de los hechos violentos **es cada vez más alta.** Adicionalmente, “El mayor número de hechos han sido cometidos por actores aún sin identificar, presentando una gran dificultad para la investigación y la judicialización de los responsables de las agresiones”.

### **Asesinatos se cometen en razón de la actividad social que realizan los defensores** 

Si bien el Estado y especialmente la Fiscalía General de la Nación, ha manifestado que no existe una sistematicidad en los homicidios de líderes sociales y que están relacionados con situaciones personales, el informe indica que **“las victimizaciones contra líderes sociales se cometen en razón de sus actividades sociales**, de sus luchas reivindicativas o de su apoyo al proceso de paz”. (Le puede interesar: ["Balance a un año de la paz: FARC, la guerrilla que mayor cantidad de armas ha dejado"](https://archivo.contagioradio.com/farc-ha-sido-la-guerrilla-que-mayor-cantidad-de-armas-ha-dejado/))

Adicional a esto, hay tres tipos de liderazgo que ha generado la mayor cantidad de víctimas. El primero está relacionado con las actividades de los defensores contra **economía ilícitas y la defensa del medio ambiente**. La segunda, está relacionada con las víctimas del conflicto armado en la reclamación de tierras, restitución de sus derechos y lucha por la verdad. Por último, los defensores son asesinados por tener perspectivas para la participación política.

Finalmente, el informe indica que en el país hay **8.532.636 víctimas individuales** inscritas en el Registro Único de Víctimas de las cuáles, a diciembre de 2016, se han reparado 125.801 con indemnización económica. A julio de 2017, 940.335 han recibido un mensaje estatal de dignificación. A esa misma fecha, ha habido 2.243 procesos de entrega de cadáveres. Igualmente, 234.818 hogares han sido acompañados en el proceso de retorno o reubicación territorial t 583.235 víctimas han sido remitidas al Programa de Atención Psicosocial y Salud Integral para las Víctimas.

###### Reciba toda la información de Contagio Radio en [[su correo]

 
