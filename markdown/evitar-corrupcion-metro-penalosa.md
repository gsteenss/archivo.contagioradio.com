Title: Concejales piden medidas cautelares para evitar corrupción en Metro de Peñalosa
Date: 2019-07-05 17:38
Author: CtgAdm
Category: Nacional, Política
Tags: Alacaldía, Bogotá, medidas cautelares, Metro, Peñalosa
Slug: evitar-corrupcion-metro-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Enrique-Peñalosa.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @enriquepenalosal  
] 

La bancada de concejales del Polo Democrático **solicitó al Consejo de Estado que imponga medidas cautelares urgentes para suspender la licitación de la ejecución del tramo 1 de la primera línea del Metro en Bogotá**, que está impulsando el alcalde Enrique Peñalosa, debido a que no se ha resuelto una tutela en donde se denuncia la falta de estudios de factibilidad para este proyecto, hecho que podría desembocar en actos de corrupción.

Según el concejal **Manuel Sarmiento,** los estudios de factibilidad son un requerimiento obligatorio de la Ley 310 de 1996 que se tramitan cuando hay construcciones de infraestructuras como la del Metro que está impulsando Peñalosa. Sin embargo, la aprobación de esta iniciativa se habría hecho solamente con la versión preliminar del estudio de factibilidad y no con la definitiva.

"Por esta falta de estudios se esta violando el principio de planeación que debe regir los procesos de contratación estatal, y por ende estamos solicitando que se suspenda la licitación de este proyecto" señaló Sarmiento, quien agregó que **en un plazo de 15 días se conocerá la respuesta del Consejo de Estado frente a esta petición**. (Le puede interesar: ["En render se quedó la Alcaldía de Bogotá"](https://archivo.contagioradio.com/render-quedo-bogota-penalosa/))

### **¿Qué pasó con los estudios de factibilidad que envió Enrique Peñalosa?** 

Sarmiento aseguró que la aprobación de la construcción del Metro de Bogotá sin los estudios necesarios, fue producto de la "actitud del alcalde Peñalosa y alcahuetería del Gobierno Nacional, primero de Juan Manuel Santos y ahora de Iván Duque, de pasarse por encima de la Ley". (Le puede interesar: ["Irregularidades y mentiras sobre el Proyecto Metro de Bogotá"](https://archivo.contagioradio.com/mentiras-metro-bogota/))

Hecho que provocó que estas instituciones dejaran de lado las exigencias legales, y que podría ocasionar a futuro situaciones catastróficas como la de Hidroituango. Por tal razón Sarmiento señaló que **ya existen los estudios de ingeniería para una primera línea del metro subterraneo,** a la que solo le hace falta la estructuración financiera del proyecto para entrar a la etapa de licitación.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38148755" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38148755_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
