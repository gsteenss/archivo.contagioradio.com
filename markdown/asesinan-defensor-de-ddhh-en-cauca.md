Title: Defensor de DDHH se recupera después de intento de homicidio
Date: 2016-10-20 17:23
Category: DDHH, Nacional
Tags: Agresiones contra defensoras, ANZORC, Cauca
Slug: asesinan-defensor-de-ddhh-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/defesnor-de-derechos-humanos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [20 de Oct 2016]

**Rectificación **

El 19 de Octubre, en Corinto-Cauca, **fue atacado el defensor de Derechos Humanos Esneider González, miembro de ANZORC**, después de que un sicario le propinara 8 impactos de bala, afuera de su casa. González había participado de las visitas de la comisión del MM&V a la zona en la que se proyectaba como Punto Transitorio de Normalización en la Vereda la Cominera en el marco de los acuerdos de paz entre el gobierno nacional y las FARC-EP.

 Los hechos se registraron sobre las 8:00pm mientras González se encontraba en su casa con su compañera cuando golpearon a la puerta, uno de sus hijos atendió al llamado, Esneider salió de su hogar y hablo con una joven, posteriormente el **sicario en moto, le dispararon hiriéndolo en la espalda y la cabeza**,  de inmediato el defensor de derechos humanos fue remitido a un centro hospitalario en la ciudad de Cali, en donde fue atendido, pese a la gravedad de las heridas González se encuentra estable.

El defensor de derechos humanos es integrante de la Asociación de Victimas Arte Paz y Vida ARPASAVI, de la Asociación de Trabajadores Campesinos de la Zona de Reserva Campesina del Municipio de Corinto - ASTRAZONAC, filial de FENSUAGRO CUT, de la Asociación Nacional de Zonas de Reservas Campesinas-ANZORC, del Proceso de Unidad Popular de Suroccidente Colombiano-PUPSOC y del Movimiento Político Y social Marcha Patriótica en el Departamento del Cauca. Le puede interesar:["Paramilitares amenazan a organizaciones de Derechos Humanos del Valle del Cauca2](https://archivo.contagioradio.com/organizaciones-ddhh-del-valle-del-cauca-amenazadas-por-paramilitares/)

Con este hecho son **8 los defensores de derechos humanos asesinados en el Cauca** por su labor y trabajo social, en lo corrido del año, de igual forma se han registrado 63 situaciones de riesgo, de las que 38 han sido amenazas de muerte, a su vez el pasado 16 de octubre, **fue asesinado Yimer Chávez, líder campesino y coordinador de la Guardia Campesina y Popular de la vereda el Frotino, en el municipio La Sierra**. Le puede interesar:["Asesinan a Yimer Chávez, líder campesino del Cauca"](https://archivo.contagioradio.com/asesinan-a-yimer-chavez-lider-campesino-del-cauca/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
