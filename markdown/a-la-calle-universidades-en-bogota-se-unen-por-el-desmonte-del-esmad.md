Title: A la calle, universidades en Bogotá se unen por el desmonte del ESMAD
Date: 2019-09-27 10:20
Author: CtgAdm
Category: Estudiantes, Movilización
Tags: ESMAD, Movilización
Slug: a-la-calle-universidades-en-bogota-se-unen-por-el-desmonte-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-27-at-09.54.56.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-27-at-09.54.58.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-27-at-09.54.59.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-27-at-09.54.59-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-27-at-09.55.00.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/MARCHA-ESMAD.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

En Bogotá las universidades públicas y privadas se unen para realizar un gran plantón en rechazo a la actuación del Escuadrón Móvil Antidisturbios (ESMAD) contra el movimiento estudiantil, el llamado a movilizarse se da luego de tres días de represión por parte de esta unidad de la policía. Las universidades han citado a concentraciones desde las 10:30 de la mañana, todas marcharán hasta encontrarse en la Carrera 7ma con calle 40 (frente a la Universidad Distrital) sobre las 12:30 del medio día, y posiblemente después se dirigirán hasta el centro de Bogotá. (Le puede interesar: ["Estudiantes de la Universidad Distrital tienen razones de sobra para protestar"](https://archivo.contagioradio.com/estudiantes-de-la-universidad-distrital-tienen-razones-de-sobra-para-protestar/))

### **En contexto: Financiación y autonomía universitaria**

En los meses finales de 2018 la Unión Nacional de Estudiantes por la Educación Superior (UNEES) en conjunto con otras plataoformas estudiantiles lograron unir a más de 60 instituciones de educación superior en torno a la defensa de este derecho. El principal logro de esta movilización fue la inyección de recursos para evitar la crisis económica de muchas de las instituciones, sin embargo, los estudiantes advirtieron que sería infrutuoso recibir más dinero si todos los integrantes de la comunidad académica no podían participar en la distribución de dichos recursos.

Uno de los hechos que les dió la razón a los estudiantes ocurrió en la Universidad Distrital, dónde la Procuraduría inició una investigación contra el exdirector del Instituto de Extensión, William Muñoz, por el presunto uso de más de 10 mil millones de pesos de recursos públicos en beneficio personal y de terceros. Ante estos hechos, la comunidad académica inicó esta semana una jornada de protestas exigiendo respuestas a los directivos, y pidiendo la profundización de la democracia universitaria para permitir que los estudiantes incidan en el presupuesto de la Universidad.

En el marco de dichas protestas, el pasado miércoles la Pontificia Universidad Javeriana (PUJ) decidió apoyar la manifestación de la Universidad Distrital, y mientras esto ocurría, el ESMAD atacó a estudiantes de ambas instituciones. En el mismo día, en medio de enfrentamientos en la Universidad Pedagógica Nacional (UPN), un artefacto explosivo afecto las instalaciones de la Universidad dejando a 5 personas heridas. (Le puede interesar:["Nuevamente el ESMAD olvida que tiene que cumplir protocolos de intervención"](https://archivo.contagioradio.com/esmad-olvida-que-tiene-protocolos-intervencion/))

Luego de estos hechos, a través de redes sociales se denunció el ingreso de la Fuerza Pública al campus de la UPN sin previa autorización de rectoría; ante lo cual, los estudiantes tuvieron nuevas razones para exigir el respeto de la autonomía universitaria. (Le puede interesar: ["Explosión en la Pedagógica habría sido por artefactos lanzados desde afuera"](https://archivo.contagioradio.com/explosion-en-la-pedagogica-habria-sido-por-artefactos-lanzados-desde-afuera/))

### **Las universidades se unen por el desmonte del ESMAD y la educación superior pública**

En una nueva jornada de protesta, la Universidad Colegio Mayor de Cundinamarca realizó un plantón el pasado jueves en apoyo a la Universidad Distrital, de igual forma lo hizo la Universidad Nacional de Colombia; en ambos casos, el ESMAD reprimió con gases a los estudiantes. Por esa razón, este viernes otras instituciones públicas y privadas se están sumando a una movilización para pedir el desmonte del ESMAD y el respeto por la democracia y autonomía universitaria.

Para Wendy Ayala, estudiante de la Universidad Distrital, esta será una movilización importante porque es la muestra de que tanto estudiantes de universidades públicas como privadas, están demostrando que les interesa la lucha por la educación, y entienden la trascendencia de defender lo público como un espacio para garantizar este derecho a todas las personas. Así mismo, las muestras recientes de violencia del ESMAD han servido para que se plantee un cuerpo de seguridad de la Policía que respete el derecho a la protesta, garantice la dignidad de quienes participan en ella y esté en capacidad de responder, siguiendo protocolos de acción, cuando se lo requiera.

Para concluir, Ayala recordó algunas medidas de autoprotección que podrían tomarse durante las protestas de hoy, cómo salir en grupos, o 'triadas'; tener el número celular de todas las personas con las que se marcha; tener la batería de los celulares cargados en caso de emergencia; e identificar a las personas encargadas de derechos humanos para acudir a ellos en caso de emergencia. (Le puede interesar: ["Garantizar la protesta social es función de un Estado democrático"](https://archivo.contagioradio.com/garantizar-la-protesta-social-es-funcion-de-un-estado-democratico/))

 

\

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
