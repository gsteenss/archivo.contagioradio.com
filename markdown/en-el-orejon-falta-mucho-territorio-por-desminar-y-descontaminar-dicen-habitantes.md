Title: En el orejón falta mucho territorio por desminar y descontaminar dicen habitantes
Date: 2015-11-26 17:36
Category: DDHH, Paz
Tags: Desminado en el Orejón, desminado humanitario, Movimiento Ríos Vivos, Proceso de paz en la Habana
Slug: en-el-orejon-falta-mucho-territorio-por-desminar-y-descontaminar-dicen-habitantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/desminado_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: septimadivisionmil.co 

<iframe src="http://www.ivoox.com/player_ek_9520727_2_1.html?data=mpqfkpyWe46ZmKiak5aJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BfxtGY0dfJroa3lIqvldOPqsLg1caYz9rHrNCf1crf1M7Ys9Pd0JDd0dePqMbnzs7bw9ePvYzYxtiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Fabio Muñoz, Ríos Vivos] 

##### [26 Nov 2015]

Pese a que en distintos medios de comunicación se ha afirmado que el proceso de desminado en El Orejón ha llegado a su fase final, las comunidades denuncian que esta descontaminación no debe terminar pues sus caminos de transito continúan minados, aún se registran sitios en los que hay campamentos de guerra y la presencia de distintos actores armados al margen de la ley en sus territorios persiste.

Fabio Muñoz, habitante de la región e integrante del Movimiento Ríos Vivos, asegura que este proceso de desminado no debe concluir, hace 2 días le manifestaron a la Delegación de Paz en La Habana que el Orejón todavía no está completamente descontaminado, por lo que exigen que haya una segunda fase de desminado que sea continua pues no puede quedar a medias.

“Los que más se han beneficiado hasta el momento con el desminado han sido directamente los terrenos de EPM”, afirma Muños, indicando que estos territorios fueron adquiridos para la implementación del proyecto Hidroituango y que los caminos que ellos transitan no se han acabado de desminar “por eso la solicitud humanitaria de que hay que continuar el desminado en El Orejón”.

Este líder agrega que la parte desminada denominada "El Alto del Capitán", de propiedad de la comunidad, “es un punto estratégico para los grupos armados y nosotros pensamos que si lo desminaron es porque las Fuerzas Militares lo van a necesitar para hacer sus bases o para tener un punto de referencia de toda la región”.

En relación con los proyectos sociales que fueron anunciados a la comunidad, Muñoz indica que  “han quedado en infraestructura y estudios y otros han querido iniciar pero no como nosotros habíamos propuesto” para la protección de sus derechos humanos, así como tampoco han recibido respuestas efectivas por parte del Gobierno frente a sus denuncias de presencia de varios grupos armados en la zona.

Tras una evaluación rigurosa de la comunidad se evidenció que la cantidad de minas en la zona no era tan grande como en su momento lo anunciaron los medios, “que aquí habían más minas que habitantes pues es completamente falso… si es verdad que hay minas, si es verdad que han desactivado minas pero la cantidad no está por el orden de 800 o 3000 como habrían afirmado” concluyó Muñoz.
