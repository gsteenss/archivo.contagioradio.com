Title: La radiografía del derecho fundamental al agua en Colombia
Date: 2017-03-22 08:53
Category: Ambiente, Voces de la Tierra
Tags: Agua, Día mundial del agua
Slug: la-radiografia-del-derecho-fundamental-al-agua-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Agua-e1458146372649.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Taringa] 

###### [22 Mar 2017] 

Colombia posee el mayor volumen de agua después de la Unión Soviética, Canadá y Brasil, con al menos 737.000 cuerpos de agua. Su ubicación estratégica geográficamente, sus tres cordilleras, dos océanos, bosques tropicales, [precipitaciones anuales y]{lang="EN"}el contar con el 50% de los páramos del mundo, harían creer  que el agua en el territorio colombiano nunca se acabará. Sin embargo, la situación es otra. Expertos advierten que para el 2050 el 60% de los páramos del país habrá desaparecido, y solo por dar unos ejemplos del drama hídrico y con ello social, a causa de esta situación más de 4 mil niñas y niños de La Guajira han fallecido por desnutrición y falta de agua potable en la última década, y el 58% de la población rural del país no tiene acceso al líquido vital de manera apta para el consumo humano. **¿Por qué sucede esto en uno de los países con mayor riqueza hídrica?** En Contagio  Radio hemos identificado 6 principales causas.

### **[Falta de control estatal]** 

La ola de consultas populares en diferentes puntos del territorio colombiano se viene desatando debido a la falta de control estatal por parte de las entidades gubernamentales que deberían garantizar que, por ejemplo, las empresas que desarrollan proyectos mineroenergéticos, cumplan con sus planes ambientales y garanticen el derecho a un ambiente sano para las comunidades. Pero esto no ha sido así.  Además de que en muchos lugares intervenidos por este tipo de proyectos, han perdido importantes fuentes de agua, las poblaciones vienen siendo víctimas de la estigmatización,  desplazamientos forzados, y asesinatos de quienes lideran estas luchas.

Sumado a ello, es importante destacar dos situaciones recientes que se derivan de la falta de compromiso estatal para garantizar el derecho al líquido vital para los colombianos. El primero: a finales del 2016, intereses económicos y políticos, como se denunció en ese momento, [**hundieron el proyecto de Ley que pretendía que el agua se considerará como derecho Fundamental** en la Constitución. La iniciativa que pasaría a séptimo debate y que contaba con aprobación total, hasta ese momento, no fue tramitada a tiempo y por lo tanto no pasó por el debate en el Congreso.]{lang="ES"}

[El segundo: a inicios de febrero de este año la **Contraloría General de la Nación, hizo un llamado a las corporaciones ambientales regionales y al Ministerio de Ambiente**, debido a que encontró que el fallo de la Corte Constitucional que prohibió la explotación minera en los páramos no ha sido debidamente acatado en los casos particulares del páramo de Santurbán y el páramo de Miraflores. La auditoría del organismo señala que el 71,48% de las superficies de ecosistemas de páramo aún no se encuentran delimitadas. De estas superficies el 60,45% corresponde a áreas totalmente desprotegidas, lo que implica que no tienen ningún tipo de instrumento de protección ambiental dentro del Sistema Nacional de Áreas Protegidas. [(Le puede interesar: Contraloría advierte que fallo contra minería en páramos no está siendo acatado) ](https://archivo.contagioradio.com/36172/)]{lang="ES"}

### **[Ganadería]

El Instituto Geográfico Agustín Codazzi (IGAC), indica que de los 114 millones de hectáreas que tiene el país, 26 millones tienen vocación agrícola, pero solo se usa 6,3 millones de hectáreas para ese fin. En cambio, **mientras que solo 8 millones de hectáreas tienen vocación ganadera,** [**se usan 38 millones para ese fin.** Y es que esta actividad conlleva graves problemas para el ambiente. P]{lang="EN"}[ara criar una vaca se utiliza una superficie 28 veces más amplia que la que se necesita para producir huevos, además se necesita 11 veces más agua para regar los campos donde crece el ganado.]{lang="ES"}

[Cifras del estudio “Role of dairy in the carbon footprint of US beef”, de la Universidad de Bard College, explican que la producción de carne implica el uso de una gran extensión de tierras, deforestación de las mismas, contaminación del suelo y de las fuentes de agua. Solo por dar un ejemplo de las repercusiones de esta actividad en las aguas, el complejo de humedales más importante del país, la Ciénaga Grande de Santa Marta,  se encuentra a punto de desaparecer por las actividades de la compañía agropecuaria RHC, que tras el desplazamiento de las comunidades a manos de la violencia paramilitar, empezó a realizar, entre otras actividades, ganadería extensiva, poniendo en riesgo el régimen hídrico al producir distintas transformaciones a nivel ecológico y ocasionando el secamiento de la ciénaga.[(Le pude interesar: Compañía RHC tiene en riesgo a la Ciénaga Grande de Santa Marta) ](https://archivo.contagioradio.com/cienaga-grande-de-santa-marta-en-peligro-de-extincion-por-compania-rhc/)]{lang="ES"}

\[caption id="attachment\_6560" align="aligncenter" width="600"\]![Estado actual de la Ciénaga Gran de Santa Marta. Foto: El Tiempo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/cienaga-contagio-radio-600x450.jpg){.wp-image-6560 .size-large width="600" height="450"} Estado actual de la Ciénaga Gran de Santa Marta. Foto: El Tiempo\[/caption\]

### **[Monocultivos]

[Las consecuencias de esta práctica enmarcadas en lo que se denomina el "capitalismo verde", afectan de manera directa tanto a la naturaleza como a las diferentes comunidades. Según el Instituto Alexander Von Humboldt, los monocultivos causan efectos nocivos como la alteración del ciclo hidrológico en los territorios. El Estudio Nacional del Agua, del IDEAM, explica la huella hídrica azul del sector agrícola, acarrea la apropiación humana de ríos, lagos y acuíferos que no retorna a sus fuentes, **Casi la mitad de esa demanda de agua, se concentra en cultivos de caña y palma de aceite.** Precisamente, en zonas muy golpeadas por la sequía como el Valle del Cauca, las concesiones superficiales y subterráneas de agua están concentradas en el sector cañero con un 64.1% y 87.8% en el monocultivo de palma de aceite.]{lang="ES"}

[El ejemplo más reciente está en Mapiripán. De acuerdo con la Comisión de Justicia y Paz, en este municipio del Meta, la empresa italiana Poligrow consume cerca de 454 millones de litros de agua por los cuales paga 6 millones de pesos en un contrato de 5 años. La organización defensora de derechos humanos asegura que esta multinacional tiene permisos de captación de agua más alta que petroleras, pues **usa más de 1'700.000 litros diarios en Mapiripán.** Debido a la plantación de 15 mil hectáreas de palma aceitera, en la zona 13 fuentes de agua se han secado. [(Le puede interesar: Entre el agua y el aceite)](https://archivo.contagioradio.com/audiencia-en-congreso-de-la-republica-por-accion-de-poligrow-en-mapiripan/)]{lang="ES"}

https://www.youtube.com/watch?v=waW6p98GWto&t=2s

### **[Hidroeléctricas]

[El Estudio Nacional del Agua del 2014, señala que del total de la demanda hídrica en Colombia, las hidroeléctricas ocupan el segundo lugar del uso del agua con el 21.5%. Aunque se ha querido vender la idea de que las hidroeléctricas no representan ningún inconveniente en la disponibilidad de agua, ya que supuestamente no afectan los caudales, organizaciones ambientalistas, denuncian que las represas  provocan una gran conflictividad, entre otras cosas por el acaparamiento y concentración del agua, destruyendo el ciclo natural del río y  la pesca. Esto conlleva afectaciones a la calidad de las aguas al generar turbidez, concentración de sedimentos y nutrientes, pérdida de oxígeno disuelto, bioacumulación de mercurio y eutrofización. Factores que impiden que las aguas dejen de estar disponibles para el consumo humano. De hecho, el llenado y la puesta en marcha de dos grandes y nuevas represas, como Hidrosogamoso y El Quimbo, ponen en evidencia esta situación. Precisamente, un estudio de la Universidad Santo Tomas de Bucaramanga, liderado por el investigador Jaime Puentes, asegura que la calidad del agua en el Río Sogamoso aguas abajo de la represa, no es apta ni para consumo doméstico ni para cultivos.]{lang="ES"}

\[caption id="attachment\_2715" align="aligncenter" width="600"\]![Orilla de río Sogamoso afectado por represa. Foto: Movimiento Ríos Vivos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/podrido-hidrosogamoso-600x403.jpg){.wp-image-2715 .size-large width="600" height="403"} Orilla de río Sogamoso afectado por represa. Foto: Movimiento Ríos Vivos\[/caption\]

### **[Minería]

[Los páramos son productores de agua. Sin embargo, entre los años 2002 y 2009, el gobierno entregó títulos mineros sin obligar a las empresas nacionales y multinacionales a cumplir mayores requisitos. Durante esos años se pasó de 1,1 a 8,4 millones de hectáreas tituladas, entregándose 391 títulos mineros en 108.000 hectáreas de páramos. Hoy, con el pie en el acelerador de la locomotora minero energética, la situación es aún más alarmante. **Hay concesiones mineras en 177.207 hectáreas de páramo.** Este hecho, ocasiona que el país con mayor número de páramos, los tenga en riesgo por la erosión, el retroceso de los hielos, y la sequía de la vegetación necesaria para preservar el agua. [(Le puede interesar: El Cerrejón ha secado 26 fuentes de agua en La Guajira)](https://archivo.contagioradio.com/con-el-arroyo-bruno-serian-27-las-fuentes-de-agua-que-el-cerrejon-ha-secado-en-la-guajira/)]{lang="ES"}

\[caption id="attachment\_17865" align="aligncenter" width="600"\]![Así se ve la mina a cielo abierto de El Cerrejón en La Guajira. Foto: laurismu](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/el-cerrejon-600x450.jpg){.wp-image-17865 .size-large width="600" height="450"} Así se ve la mina a cielo abierto de El Cerrejón en La Guajira. Foto: laurismu\[/caption\]

### **[Extracción petrolera]

[**Parte de la región amazónica comprende el 50% del territorio colombiano, una zona clave para la producción de agua**, pues allí, junto a la región pacífica se produce el 77% del agua del país. El departamento del Caquetá y de Putumayo hacen parte de esa región. Por tal motivo son]{lang="ES"} lugares esenciales para la vida, pero a su vez representan la cuenca hidrocarburífera llamada Caguán-Putumayo que tiene un potencial de reservas estimadas en 6.000 millones de barriles de petróleo según el departamento de Geociencias de la Universidad Nacional de Colombia.

Según el Estudio Nacional del Agua, el 1,6 % de la demanda de agua en Colombia, corresponde a la explotación de crudo. De manera que el uso del líquido vital, no es el principal problema de esta actividad, sino más bien los daños a las fuentes de agua en medio del proceso de extracción.

Cuando se produjo la gran sequía en el departamento del Casanare en el año 215, la revista Semana, consultó a la Sociedad Colombiana de Ingenieros (SCI) en Casanare. Esta entidad explica que aunque las petroleras no consumen mucha agua sí profundizan e impiden su uso. La densidad del crudo es tal, que lo que deben hacer las petroleras es hacer pozos de reinyección de agua al lado de donde sacan el crudo para que el esta empuje el petróleo y de esa manera pueda ser extraído. “El problema es que esta agua se obtiene de las capas superficiales y queda atrapada en las más profundas, lo que hace que los animales y los humanos pierdan el líquido”, manifiesta  la SCI.

Pese a ello, [un estudio de 2012 de la universidad estadounidense, la RAISG, indica que los países con mayores proporciones de su territorio amazónico dedicado a la actividad de hidrocarburos son: Perú, con el 84%, y **en segundo lugar Colombia con el 40%. El país, tiene el mayor número de bloques destinados para la actividad petrolera en la región amazónica occidental con 102.**]{lang="ES"}

\[caption id="attachment\_11053" align="alignnone" width="800"\]![Derrame de petróleo en Putumayo. Foto: Archivo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/petroleo-putumayo-contagio-radio.jpg){.size-full .wp-image-11053 width="800" height="533"} Derrame de petróleo en Putumayo. Foto: Archivo\[/caption\]

[Finalmente, frente a este panorama, es necesario resaltar, que además de esto, Colombia camina hacia el fracking. Una actividad contra la que ya se están manifestando las comunidades. Además de la la posible contaminación de las aguas con gases tóxicos, esta actividad]

![día mundial del agua (6)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/día-mundial-del-agua-61-e1490266883461.jpg){.wp-image-38118 .aligncenter width="514" height="727"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
