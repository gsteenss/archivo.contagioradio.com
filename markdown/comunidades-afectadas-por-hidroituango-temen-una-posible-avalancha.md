Title: Comunidades afectadas por Hidroituango temen posible avalancha
Date: 2018-05-08 14:55
Category: DDHH, Movilización
Tags: Antioquia, comunidades afectadas por hidroituango, Hidroituango, Movimiento Ríos Vivos
Slug: comunidades-afectadas-por-hidroituango-temen-una-posible-avalancha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DcpT75PXcAEDk30-e1525800304474.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Claudia Ortiz] 

###### [08 May 2018] 

Luego de 10 días que completa la tragedia por el taponamiento de uno de los túneles de descarga que hace parte del proyecto hidroeléctrico Hidroituango en Antioquia, las comunidades continúan en **completa zozobra** temiendo que se pueda presentar una avalancha. Han pedido a Empresas Públicas de Medellín que establezca un plan de contingencia debido al riesgo en el que se encuentran.

De acuerdo con el Movimiento Ríos Vivos, **el río Cauca ha bajado considerablemente su caudal** en la zona baja debido a un represamiento de la misma en la parte alta. Esto ha generado una alerta para las comunidades que viven a las riberas del río y del cual dependen del barequeo y la pesca para su diario vivir.

### **EPM ha confundido a las comunidades** 

Isabel Cristina Zuleta, integrante de Ríos Vivos indicó que **“la situación real es de caos y angustia”**. Afirmó que EPM implementó un frente de trabajo “con personas que desconocen la información y que no tienen respuestas para las comunidades”. Además, argumentó que EPM ha visitado las diferentes comunidades diciéndoles que deben estar tranquilas y “hoy ante la magnitud de la tragedia tienen a la gente confundida”.

Adicional a esto, Zuleta criticó la postura de la empresa que afirmó que, por seguridad, las comunidades **no se deben acercar a las riberas del río** “desconociendo que las comunidades viven ahí y toda la infraestructura de municipios como Puerto Valdivia y Caucasia está alrededor del río Cauca”. (Le puede interesar:["15 familias afectadas dejó la inundación producto del taponamiento de un túnel para Hidroituango"](https://archivo.contagioradio.com/15-familias-afectadas-dejo-la-inundacion-producto-del-taponamiento-de-un-tunel-para-hidroituango/))

### **Autoridades aseguran que no hay riesgo en la zona** 

Así mismo, Cecilia Muriel, lideresa de Puerto Valdivia y una de las afectadas por el proyecto hidroeléctrico, aseguró que **“el río ya no tiene el caudal que tenía y está estancado”**. Afirmó que las comunidades se han movilizado pidiendo ayuda debido a que no han dormido temiendo que en cualquier momento se presente una avalancha.

Después de haber emitido un sin fin de peticiones a la empresa para garantizar el bienestar y la seguridad de las comunidades, “la empresa no ha respondido y sentimos que las peticiones han sido en vano”. Aseguró que las autoridades, como la Policía Nacional, les han dicho que **no provoquen pánico en las mismas comunidades** “cuando no estamos diciendo una mentira”. (Le puede interesar:["Es indignante posición de EPM frente a tragedia por Hidroituango": Ríos Vivos"](https://archivo.contagioradio.com/es-indignante-posicion-de-epm-frente-a-tragedia-por-hidroituango-rios-vivos/))

Finalmente, EPM informó que han estado trabajando para que el agua pueda fluir de manera normal por medio de los **llenados prioritarios desde la presa del proyecto**. Sin embargo, recordó que aún no tienen una respuesta para hacer frente a problemas como la mortandad de los peces e indicó que la crisis se presentó debido a una falla geológica “inesperada”.

\  
<iframe id="audio_25861175" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25861175_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
