Title: Profesores de cátedra y monitores de la Universidad de La Guajira llevan un año sin salario
Date: 2019-10-21 13:00
Author: CtgAdm
Category: Educación, Movilización
Tags: ESMAD, estudiantes, Rioacha, Universidad de La Guajira
Slug: profesores-de-catedra-y-monitores-de-la-universidad-de-la-guajira-llevan-un-ano-sin-salario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-08-at-10.03.01-AM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/70748473_399749010919467_3654908158331060224_o-e1570553478735.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Uniguajira resiste  
] 

En la Universidad de La Guajira llevan más de dos semanas en paro por cuenta de los problemas financieros que enfrenta la Institución, y **ha generado la deuda en salarios de monitores y profesores catedráticos hace un año.** Adicionalmente, los estudiantes reclaman que el gobierno departamental, los entes de control y el Ministerio de Educación Nacional (MEN) no han hecho presencia en el lugar para encontrar soluciones a la situación económica. (Le puede interesar: ["Las cuatro exigencias del paro cívico en La Guajira"](https://archivo.contagioradio.com/exigencias-paro-civico-guajira/))

### **"El Gobierno debe la suma de 150 mil millones de pesos"  
**

Hammer Solano, estudiante de la Universidad de La Guajira, explicó que desde el 27 de septiembre los integrantes de esa institución entraron en paro, exigiendo el derecho a la educación y el respeto de las ordenanzas 214 de 2007 y 232 de 2008 y la Ley 30 de 1992, que reglamentan los recursos económicos que se deben brindar a las Instituciones de Educación Superior (IES) públicas. Según Solano, por el incumplimiento de estas normativas**, la deuda del Estado con la Universidad alcanza los 90 mil millones de pesos,** "y ante eso, ni el gobernador encargado (Jhon Fuentes Medina) ha dado la cara".

Frente a los recursos logrados por el movimiento estudiantil de 2018, Solano dijo que habían sido destinados a cubrir el pago de personal de planta e infraestructura de la Universidad, puesto que cerca de 87% de los funcionarios obedecen a ese tipo de contrato. No obstante, los profesores que tienen contrato de cátedra y los monitores no han recibido sus sueldos desde hace un año, lo que significa que "en ocasiones no tienen ni como tomar un bus para llegar a trabajar".

Adicionalmente, los estudiantes señalan que **ninguno de los entes de control llamados a verificar la situación financiera de la Universidad se han hecho presentes para dialogar con los estudiantes y encontrar soluciones.** Por el contrario, los jóvenes denunciaron que el pasado lunes se reunieron a puerta cerrada delegados del MEN, el rector de la Universidad y el vicerector financiero, "pero no tenemos ni un comunicado oficial de lo que pasó". (Le puede interesar: ["Gobierno y estudiantes logran acuerdo en mesa de negociación"](https://archivo.contagioradio.com/estudiantes-gobierno-acuerdo/))

### **El ESMAD actuó como 'de costumbre'**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ffermin.r.moreno%2Fvideos%2F10156792387687965%2F&amp;show_text=0&amp;width=267" width="267" height="476" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En cambio, Solano afirmó que la institución que sí se ha hecho presente es el Escuadrón Móvil Antidisturbios (ESMAD), que tras la toma realizada por parte de estudiantes a la Gobernación de La Guajira, agredió a los manifestantes "e incluso tenemos un compañero en el hospital por un fuerte golpe que recibió en la cabeza". Asimismo, el pasado lunes, estudiantes de la sede Maicao se tomaron una vía en ese municipio, y tras la acción del ESMAD se presentó la captura de una de las manifestantes, que luego de intervención de los entes de control fue liberada, presentando multiples golpes y raspaduras que serán verificadas en medicina legal.

\[caption id="attachment\_74752" align="aligncenter" width="341"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-08-at-10.03.01-AM.jpeg){.wp-image-74752 width="341" height="607"} Agresión a estudiante de la U. de La Guajira\[/caption\]

<iframe id="audio_42879074" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_42879074_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **Actualización: 4 semanas sin clase, sin apoyo y sin soluciones**

Este lunes 21 de octubre, mientras 9 Instituciones de Educación Superior públicas entran en paro respondiendo el llamado a la Semana por la Indignación por los abusos del ESMAD, estudiantes de la Universidad de La Guajira completaron 4 semanas con cese de actividades. En entrevista con Contagio Radio, el estudiante Hammer Solano dijo que hasta la semana pasada el Consejo Superior Universitaria había apoyado la protesta de los jóvenes, pero el pasado 16 de octubre decidió suspender el semestre académico y reiniciar actividades el viernes.

La decisión no fue consultada ni acordada con los estudiantes, que siguen exigiendo el pago de la deuda que tiene la Universidad y asciende a más de los 90 mil millones de pesos. Por esta razón, los jóvenes decidieron mantenerse en paro, y en sintonía con las demás universidades del país, manifestarse contra la violencia ejercida por la Policía y el ESMAD, que en el caso de La Guajira, dejó como resultado a un estudiante herido en su cara, y debió ser tratado quirúrgicamente.

Adicionalmente, Solano manifestó que la solución que se ha ofrecido por parte de las directivas de la Institución es que los jóvenes asuman pagos cercanos a 2 millones de pesos, con el fin de solventar la deuda. Es decir, que los cerca de 100 mil estudiantes de la Institución tendrían que cubrir con su dinero la deuda que tiene el Departamento con la Universidad, y que afecta a profesores catedráticos, monitores, administrativos y personas que trabajan en servicios generales.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]

 
