Title: Cambio climático afecta el 80% de las funciones de la tierra
Date: 2016-11-15 13:03
Category: Ambiente, Animales, El mundo, Nacional
Tags: Animales, Calentamiento global, cambio climatico
Slug: cambio-climatico-afecta-80-las-funciones-la-tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/aves-migratorias-e1479232476933.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### [15 Nov 2016] 

**Animales más pequeños, aumento de plagas, disminución de especies de fauna y flora,** son algunas de las consecuencias que ya se evidencian por el cambio climático que actualmente altera casi todos los procesos de la vida del planeta.

Así lo afirma una investigación conjunta de la Universidad de Florida, el Centro ARC de Excelencia para Estudios de Arrecifes de Coral y la Universidad de Queensland, de Australia, que concluye que en el 80% de los casos estudiados, el calentamiento global ha provocado cambios. Se trata de un análisis realizado por científicos de 10 países, que indican que **las pulgas de agua, las salamandras, las panteras, pingüinos y aves canadienses,** además de otras[especies de fauna](https://archivo.contagioradio.com/la-tercera-parte-de-las-especies-animales-en-el-mundo-se-extinguiran-por-el-cambio-climatico/) y flora cambiaron su tamaño, color o población por las altas temperaturas que hoy padece la tierra.

En el estudio se analizó a la pulga de agua, una especie cuya reproducción depende de la temperatura. "Ahora sabemos que el cambio climático está afectando su genética, su fisiología, su distribución y las comunidades de las que forma parte. Este ejemplo ofrece la prueba más completa de cómo el cambio climático puede alterar todos los procesos que rigen la vida del planeta", dice Brett Scheffer, biólogo de la Universidad de Florida, en una entrevista realizada por el periódico El País de España.

### Las consecuencias 

Respecto a la vida en el mar, aunque el 52% de las especies adaptadas a aguas calurosas han crecido, las de aguas frías han disminuido en ese mismo porcentaje. Asimismo se resalta que, en la tierra, **el 50% de las especies de vertebrados se extinguieron en los últimos 40 años. **

Por otra parte se ha logrado evidenciar que la primavera se está adelantando, lo que ha afectado la conducta de diversos tipos de aves, generando hay **cambios en los patrones de llegadas y partidas de aves migratorias.** En Canadá, los bosques boreales cada vez avanzan más hacia el norte y especies como la marta americana y la marmota de vientre amarillo están aumentando de tamaño.

**"Los genes están cambiando la fisiología de las especies y características físicas como el tamaño corporal están modificándose.** Las especies se están moviendo y observamos claros signos de ecosistemas enteros bajo estrés, todos ellos en respuesta a[los cambios en el clima en tierra o en mar](https://archivo.contagioradio.com/acuerdos-paris-cambio-climatico/)", explica Scheffers.

Cabe recordar que este fenómeno continuará presentándose ya que **el 2016 será el año más cálido de todos los registrados,** con temperaturas que estarán 1,2 grados por encima de los niveles pre-industriales según un informe de la Organización Meteorológica Mundial.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
