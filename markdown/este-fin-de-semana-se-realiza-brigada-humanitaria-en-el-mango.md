Title: Este fin de semana se realizó brigada humanitaria en “el Mango”
Date: 2015-08-14 13:24
Category: Movilización, Nacional
Slug: este-fin-de-semana-se-realiza-brigada-humanitaria-en-el-mango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/el_mango_cauca_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elpueblo.com 

<iframe src="http://www.ivoox.com/player_ek_6734464_2_1.html?data=l5yglpmaeI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhtPdyMbRw5DMuc7Vz87hw9fNpYzgzcrU0ZDJt9XZjNvWx9fSqdSfwtGYh6qWaZmkhp6wr8bSq9CZppeSmpWJfaWfpJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Willie Monsalve, Secretario Gral. Juventud Comunista] 

###### [14 Ago 2015] 

Durante este fin de semana se realizará la **segunda brigada humanitaria en el corregimiento “El Mango”** del municipio de Argelia, Cauca. Una iniciativa que convoca a cerca de 1200 jóvenes pertenecientes a diversas organizaciones sociales, nacionales e internacionales, para intercambiar reflexiones en torno a la crisis humanitaria que vive la región.

Willie Monsalve, secretario general de la Juventud Comunista, extiende la invitación a participar en la jornada que se llevará a cabo desde este viernes 14 de agosto hasta el próximo lunes 18, constituída como un espacio de **denuncia, acción y movilización, para exigir el cese bilateral al fuego y el respeto por los Derechos Humanos.**

Según señala Monsalve, se busca que la misión tenga **proyección en una audiencia con el Congreso de la República, la Comisión de Paz del Senado y Organizaciones de Derechos Humanos**, para que las comunidades involucradas, cuenten con acompañamiento y se visibilicen sus denuncias y propuestas.

Argelia, es un municipio que se ha caracterizado por la continua movilización social en rechazo a la presencia de actores armados, frente a los hostigamientos y la amenaza que representa la guerra para la población civil, al habitar territorios que son **escenario de confrontaciones y que también son objetivo de intereses económicos de multinacionales.**
