Title: Colombianos en Nueva York piden implementación de Acuerdo de Paz
Date: 2019-06-20 20:12
Author: CtgAdm
Category: Paz, Política
Tags: Francisco Santos, Nueva York
Slug: colombianos-en-nueva-york-piden-implementacion-de-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-20-at-7.14.26-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: ] 

A propósito de la visita del Embajador Francisco Santos a Nueva York, colombianos defensores de derechos humanos le pidieron a sus representantes su apoyo a la causa de la paz en Colombia y a mantener una posición crítica en su reunión con el funcionario respecto a la efectiva implementación del Acuerdo y la construcción de una paz estable y duradera.

Las senadoras del estado de Nueva York, Jessica Ramos, Catalina Cruz y Nathalie Fernández, tenían planeado para el día de hoy un Facebook Live con el funcionario, sin embargo tras recibir las solicitudes de este red de activistas, las senadoras Catalina Cruz y Nathalie Fernández decidieron cancelar su participación en este encuentro. A última hora, el consulado finalmente canceló el evento. (Le puede interesar: "[Llaman al gobierno alemán a fijar postura sobre asesinatos de líderes sociales en Colombia](https://archivo.contagioradio.com/llaman-gobierno-aleman-fijar-postura-sobre-asesinatos-colombia/)")

Según Elizabeth Castañeda, defensora de derechos humanos radicada en Nueva York, organizaciones sociales enviaron a las senadoras información sobre la postura del embajador, especialmente en temas como las amenazas en contra de la justicia transicional, las preocupaciones sobre las directrices del Ejército como la ha reportado The New York Times y la implementación de los acuerdos de paz.

Castañeda manifestó que la cancelación del evento se pudo dar por las presiones que han ocasionado manifestaciones en contra del Gobierno del Presidente Iván Duque en Estados Unidos, Londres y Francia. Ayer, colombianos se reunieron afuera del Consulado de Colombia en Nueva York para exigir el respeto del Gobierno a la Jurisdicción Especial para la Paz, los líderes sociales y el proceso de paz.

A pesar de la cancelación del encuentro, Castañeda insiste que los colombianos en el exterior tienen un deber en presionar a los funcionarios de su país natal a cumplir con sus compromisos a la paz. Como ejemplo, recordó que lograrón que 79 congresistas de Estados Unidos enviaran una carta al Secretario de Estado Mike Pompeo, por medio del cual pidieron que el Gobierno colombiano respete los derechos humanos para recibir recursos del plan "Paz Colombia".

<iframe id="audio_37377281" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37377281_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
