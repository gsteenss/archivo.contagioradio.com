Title: Campaña "Defendamos la Vida" seguirá respaldando el trabajo de los líderes sociales
Date: 2020-06-16 22:00
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: Comunidad Internacional, Defensa de líderes sociales, implementación del Acuerdo de Paz
Slug: campana-defendamos-la-vida-seguira-respaldando-el-trabajo-de-los-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/EaPSR4OXQAAvhUo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: líderes sociales/ Federación luterana

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A un año de lanzarse la campaña Defendamos La Vida iniciativa con el propósito de visibilizar el gran trabajo de las y los líderes sociales en Colombia, integrantes de la Unión Europea, creadora de la campaña destacaron su preocupación por el aumento de la violencia en contra de ellos y ellas pero a su vez resaltaron los procesos locales de resistencia y su compromiso desde la comunidad internacional en la defensa de la vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El balance no es alentador. Para abril, el accionar de grupos armados legales e ilegales había dejado un saldo de 84 líderes sociales asesinados, **al menos 26 en el departamento de Cauca, para mediados de mayo la cifra de líderes asesinados en 2020 ascendió a 100,** y tan solo en el mes de junio han sido asesinados ocho líderes sociales nuevos. [(Lea también: Durante 2020 se han registrado 115 amenazas y 47 asesinatos contra defensores de DD.HH.)](https://archivo.contagioradio.com/durante-2020-se-han-registrado-115-amenazas-y-47-asesinatos-contra-defensores-de-dd-hh/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, Maydany Salcedo presidenta de la Asociación de Trabajadores Campesinos de Piamonte Cauca (ASINTRACAMPIC), organización que trabaja por la conservación de 1670 hectáreas de bosque primario y la sustitución voluntaria, afirma que pese a la presencia de actores armados que se disputan el territorio y la ausencia del Estado que hasta el momento no ha entregado los proyectos productivos, lideresas y líderes continúan en su función defendiendo los derechos ambientales y de las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Si ustedes algún día nos dejan solos, a todos nosotros nos matan, hoy día agradecemos que el Acuerdo de Paz se sostenga y seguimos con vida gracias a ustedes", expresó la lideresa que coordina el proceso de 736 familias que ingresaron al Plan Nacional de Sustitución de Cultivos de Uso Ilícito (PNIS) . [(Lea también: "No nos dejen solos", la petición de líderes sociales del Chocó ante amenazas)](https://archivo.contagioradio.com/no-dejen-solos-lideres-sociales-choco/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El Estado tiene las herramientas para la paz pero no las usa

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Diana Sánchez, directora de la Asociación Minga quien ha acompañado, procesos y comunidades a través de su experiencia señala que desde las organizaciones sociales y defensoras de DD.HH. se esperaba que con el Acuerdo de Paz se detuviera la violencia sin embargo, el tiempo ha demostrado que se ha incrementado y en ello el Estado tiene responsabilidad directa. [(Lea también: "Incumplir acuerdos internacionales trae consecuencias": Eurodiputados sobre Colombia)](https://archivo.contagioradio.com/incumplir-acuerdos-internacionales-trae-consecuencias-eurodiputados-sobre-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Está claro que el Gobierno desechó el acuerdo" redujeron instancias acordadas en La Habana como la Comisión Nacional de Garantías de Seguridad donde se crearía la política del desmonte del paramilitarismo y sin embargo esto no ha sucedido, de hecho resalta Sánchez, que en medio de la cuarentena han surgido más grupos armados ilegales, **"aquí el Estado no le da valor a la sociedad, no echa manos de esas herramientas, las desecha viendo que las comunidades y las bases sociales están en riesgo"**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a la **"ceguera y negacionismo del Esta**do" y la ausencia de acciones de otras instituciones como la Fiscalía y a la Procuraduría, la defensora destacó la labor local que se viene realizando en los territorios en medio de las dificultades, ejemplo de ello son la recolección de alimentos de las comunidades indígenas del Cauca y las acciones de control y sanidad por parte de las guardias cimarronas, indígenas y campesinas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La comunidad internacional reitera su apoyo a líderes

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Jeroen Roodenburg, embajador de los Países Bajos señaló que es evidente el incremento en amenazas y asesinatos de líderes sociales por lo que es necesario buscar más soluciones con el Estado, "quizá es peor que antes pero es una dinámica que ha exisitido por mucho tiempo en los territorios". [(Le puede interesar: Eurodiputados piden respuestas a gobierno Duque sobre política de DDHH)](https://archivo.contagioradio.com/eurodiputados-piden-respuestas-a-gobierno-duque-sobre-situacion-de-dd-hh/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Señaló que la transformación que se requiere para la implementación del Acuerdo en los territorios debe ser fruto "de un esfuerzo conjunto en el que toda la sociedad colombiana esté y superar la polarización".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esta apreciación se da días después que un grupo de 28 eurodiputados enviara una carta al Gobierno de Iván Duque expresando su preocupación frente al incremento de asesinatos de líderes sociales y excombatientes y el recrudecimiento de la violencia que se vive en los territorios donde aún no llega la implementación del Acuerdo de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, Martin Sjorgen, representante de la [Embajada de Suecia](https://twitter.com/SwedeninCOL)reafirma que la campaña continúa y que **el verdadero desafío del Estado está en hacer una verdadera "presencia estatal que ha sido históricamente débil" en las zonas afectadas por el conflicto"**, algo que no puede resolverse solo desde un sector de la sociedad y requiere de una sinergia que involucre las iniciativas locales".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Hay fuerzas muy poderosas que no quieren que sus lideres hagan su trabajo y enfrentar esas fuerzas requiere voluntad política, con recursos y contudencia, pero el estado tiene una responsabilidad muy grande y continuaremos dialogando con gobiernos nacionales y locales - Jeroen Roodenburg,

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

El embajador de Países Bajos destacó el trabajo que se realiza en términos de cooperación internacional junto a población afro y resguardos indígenas, derechos de la comunidad LGBTI y apoyando alternativas de libertad de prensa y el desarrollo del agro y el campo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma desde ambas embajadas se resaltó las reuniones con nuevos gobernadores y alcaldes para destacar el trabajo de lideres y lideresas y lo importante que es incluirlos en sus planes de desarrollo su protección y de igual forma buscar alternativas para resolver brechas entre la ciudad y el campo, la situación de desigualdad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Con líderes como ustedes el proceso de paz no puede fallar, estoy convencido de su resiliencia, queda mucho camino por recorrer pero tengan la certeza de que como Unión Europea seguiremos comprometidos con la implementación del acuerdo de paz"**, expresó el diplomático holandés. [(Le recomendamos leer: Unión Europea reafirma su apoyo a los liderazgos en Colombia)](https://archivo.contagioradio.com/union-europea-reafirma-su-apoyo-a-los-liderazgos-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
