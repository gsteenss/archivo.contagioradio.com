Title: Gobiernos del mundo indiferentes ante la violación de DD.HH. por parte de empresas
Date: 2019-11-25 18:08
Author: CtgAdm
Category: DDHH, Nacional
Tags: Agresiones contra defensores de DDHH, DDHH, empresas extractivas, Naciones Unidas
Slug: gobiernos-del-mundo-indiferentes-ante-la-violacion-de-dd-hh-por-parte-de-empresas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/empresas-y-violacion-a-derechos-humanos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Más de 2000 expertos, funcionarios de gobiernos,  profesionales, académicos, sociedad civil, defensores de DD.HH y líderes de todo el mundo se reúnen del 25 al 27 de noviembre en el **Foro de las Naciones unidas sobre empresas y derechos humanos**, este año enfocado en incrementar la responsabilidad de gobiernos y compañías en la protección de los líderazgos sociales y profundizar en los desafíos y prácticas encaminadas a  la prevención y el tratamiento de los impactos sobre la violación de DD.HH.

El evento tiene como tema central a los gobiernos "como catalizadores del respeto de los derechos humanos por parte de las empresas", aunque existen avances jurídicos en algunos países que elaboran planes de acción nacionales sobre las empresas y DD.HH, es necesario revisar si son eficaces los esfuerzos y acciones  actuales, que en ocasiones se ven reflejadas en una ausencia de liderazgo gubernamental. [(Le sugerimos leer: Tribunal confirma sentencia contra 16 empresarios y paramilitares por despojo de tierras) ](https://archivo.contagioradio.com/tribunal-confirma-sentencia-contra-16-empresarios-y-paramilitares-por-despojo-de-tierras/)

Para Ana Zbona, administradora del proyecto de Libertades Civiles y Defensores de Derechos Humanos del Centro de Información sobre Empresas y Derechos Humanos,  existe una constante en cada uno de los países que hacen parte de este foro y es que en la actualidad, los Gobiernos de estas naciones "no están haciendo lo suficiente para cumplir con su deber de proteger los derechos humanos relacionados con las empresas".

Por tal razón explica Zbona, el foro y el mensaje que se transmitirá están enfocados en busca alternativas para que los líderes de Estado y sus administraciones intensifiquen su accionar en la protección de defensores de DD.HH. quienes continúan siendo víctimas de opresión y ataques sin que existan garantías. ["Los empresarios son los que están detrás de los asesinatos": líderes del Bajo Atrato y Urabá](https://archivo.contagioradio.com/hasta-cuando-y-cuantos-mas-continuan-la-violencia-contra-lideres-sociales-del-bajo-atrato-y-uraba/)

En Colombia, cabe mencionar que tan solo en 35 sentencias que han emitido desde los estrados de Justicia y Paz se mencionan a 439 empresas y empresarios que participaron directa o indirectamente financiando estructuras armadas ilegales, una investigación que continúa en construcción y que busca que tenga mayor desarrollo en el  trabajo de la Comisión de la Verdad y la JEP.

Como antecedente, de 39 comisiones de la verdad que se crearon en 30 diferentes países, 56 % (22) hallaron patrones de participación de actores económicos en violaciones de DD.HH, un panorama que en Colombia ha sido evidenciado a través de la relación existente entre actores armados, poderes económicos, élites locales con intereses políticos y económicos como la explotación, producción y distribución.

### ¿Qué es la iniciativa Tolerancia Cero?

Al respecto, la administradora del proyecto de Libertades Civiles se refirió a Tolerancia Cero, iniciativa que busca hacer frente a la violencia e intimidación contra los pueblos indígenas y defensores de DD.HH. en las cadenas de suministro mundiales y que en asociación con diversas comunidades busca poner sobre la mesa y en la agenda de las empresas la violencia ligada a la producción y comercio de productos básicos.

**"Nuestra organización ha documentado más de 2.000 ataques contra defensores y defensoras de DD.HH en esas cadenas de suministros mundiales, es una situación muy grave así que buscamos poner esta problemática en el centro del discurso sobre empresas y derechos humanos"**, manifestó Ana Zbona. [(Lea también: En Colombia el 62% del agua concesionada está en manos de un 1% de empresarios)](https://archivo.contagioradio.com/en-colombia-el-62-del-agua-concesionada-esta-en-manos-de-un-1-de-empresarios/)

A futuro y con relación a Tolerancia Cero, indicó que se busca difundir la declaración a través de la cual, los representantes de las organizaciones y comunidades en defensa de los DD.HH buscan que empresas e inversionistas se comprometan a cumplir una serie de compromisos para proteger a comunidades indígenas y defensores de derechos y se declaren parte de esta iniciativa y comiencen a realizar acciones que ejerzan un verdadero impacto en la reducción de violencia ni ataques en sus cadenas de suministro.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
