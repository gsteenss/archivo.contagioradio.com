Title: "Por cumplir con una fecha no voy a firmar un mal acuerdo”, presidente Santos
Date: 2016-03-09 14:23
Category: Nacional, Paz
Tags: FARC, Juan Manuel Santos, paz, proceso de paz
Slug: por-cumplir-con-una-fecha-no-voy-a-firmar-un-mal-acuerdo-presidente-santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/santos-2-e1457551354187.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las 2 Orillas 

###### [9 Mar 2016]

En las últimas horas, el presidente Juan Manuel Santos dijo que cumplirá y firmará con la guerrilla de las Farc un acuerdo que **"sea bueno para los colombianos"**, y por tanto, señaló que la firma del proceso de paz no se dará de forma acelerada, es decir, que no sería para el próximo 23 de marzo como se había anunciado.

Durante la clausura de la  63 Asamblea Anual de la Confederación Evangélica de Colombia, Santos dijo “Algo que si quiero dejar en claro: yo por cumplir con una fecha no voy a firmar un mal acuerdo”.

Alejo Vargas, director del ‘Centro de Pensamiento y Seguimiento al Diálogo de Paz de la Universidad Nacional’, asegura que no es realista pensar que el 23 marzo se de la firma del acuerdo, teniendo en cuenta que pues los temas pendientes son álgidos y “**lo menos conveniente en este tipo de procesos de terminación de conflictos armados es poner fechas**, son procesos que merecen su tiempo, el tema de la fecha es secundario”.

El analista, considera más acertado pensar en que para mayo o junio de este año la Mesa de Conversaciones haya concluido la discusión de los temas agendados para este ciclo: el fin del conflicto armado; la implementación, verificación y refrendación de los acuerdos y las salvedades que han quedado pendientes en los 3 puntos ya acordados, pues “sin duda es muy probable que en este primer semestre lleguemos a la firma del acuerdo final de paz, **lo fundamental y que debemos tener en cuenta todos los colombianos es que estamos en la fase final y la terminación del conflicto ya se avizora**”.

Cabe recodar que la canciller María Ángela Holguín indicó en su visita a España que la fecha se mantiene, pero se abre la puerta a que no se firme el acuerdo definitivo entre el Gobierno y las FARC, **sino un acuerdo parcial, que incluiría el cese al fuego bilateral.**

Por su parte, la guerrilla de las FARC insistió en la necesidad de seguir haciendo pedagogía por la paz de la manera más amplia posible y reafirmaron que quienes mejor conocen los acuerdos son lo más autorizados, resaltando que la pedagogía no la puede hacer una de las partes. También **señalaron que se está agotando el tiempo para definir de manera bilateral los mecanismos de refrendación de los acuerdos.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
