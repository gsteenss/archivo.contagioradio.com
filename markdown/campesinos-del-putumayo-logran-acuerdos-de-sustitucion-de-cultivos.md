Title: Campesinos del Putumayo logran acuerdos de sustitución de cultivos
Date: 2017-07-27 12:52
Category: Ambiente, Movilización
Tags: acuerdos de paz, Sustitución de cultivos de uso ilícito
Slug: campesinos-del-putumayo-logran-acuerdos-de-sustitucion-de-cultivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/coca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Federación de Periodistas de Perú] 

###### [27 Jul 2017] 

Campesinos de la Perla Amazónica y la baja bota Caucana, lograron llegar ayer a un acuerdo con el gobierno, para que se cumpla con el punto 4 de los acuerdos de paz, **no haya más dilaciones por parte de los representantes del gobierno y se inicie la sustitución de cultivos** de uso ilícito sin que exista erradicación forzada en los territorios por parte de la Fuerza Pública.

### **Los acuerdos en el Putumayo para la sustitución de cultivos ilícitos** 

Jani Silva, lideresa de ANZORC, manifestó que, pese a que fue una reunión extenuante que duro tres días, alcanzaron acuerdos muy importantes, entre ellos que, **si el gobierno llega a incumplir lo pactado, los campesinos retomarán la movilización y convocarán a paro.  **(Le puede interesar:["En Putumayo el Estado ha dilatado plan de sustitución de cultivos de uso ilícito"](https://archivo.contagioradio.com/en-putumayo-el-estado-ha-dilatado-el-plan-de-sustitucion-de-cultivos-de-uso-ilicito/))

Otros de los compromisos pactados entre ambas partes fueron que una vez el campesino reciba el subsidio de **2 millones de pesos tendrá dos meses para erradicar el 100% de la hoja de coca, podrá acceder a un millón ochocientos para iniciar su proyecto productivo** y se le dará un millón de pesos cada dos meses durante un año, para financiar su economía. Además, se lograron acuerdos para mejorar la infraestructura vial de estos municipios, que a su vez son necesarias para que se pueda realizar la implementación de los Planes de Enfoque Territorial.

### **Las preocupaciones de los campesinos** 

“**Hay mucha alegría, por una parte, pero también mucha expectativa con respecto a sí el gobierno cumple o no cumple**, porque esta mañana recibimos un comunicado de la Baja Bota Caucana donde denuncian que el ejército sigue erradicando” afirmó Silva.

Durante la reunión los campesinos aseguraron que están dispuestos a la sustitución de cultivos de uso ilícito, y de acuerdo con Hanny ahora el proceso que se inicia **es poner en conocimiento de todos los campesinos de la región los acuerdos para que se acojan** al plan de sustitución. (Le puede interesar: ["Plan de sustitución de cultivos de uso ilícito debe ser integral o no durará: COCCAM"](https://archivo.contagioradio.com/sustitucion-cultivos-ilicitos-cocam/))

### **Las alternativas de los campesinos para la sustitución** 

Jani Silva, señaló que las comunidades han empezado a generar proyectos de soberanía alimentaria para realizar la sustitución de los cultivos de uso ilícito y a fortalecer el comercio, sin embargo, **reiteró que para que esto se lleve a cabo debe darse cumplimiento a los acuerdos de paz**.

“Estamos pensando en un conjunto de cultivos que nos den la conservación necesaria de la tierra y que necesitamos y promover la economía del campo es decir la economía agrícola” agregó Silva. (Le puede interesar: ["Mujeres cocaleras entregan 14 puntos mínimos para sustitución de cultivos ilícitos"](https://archivo.contagioradio.com/mujeres-cocaleras-entregan-14-puntos-minimos-para-sustitucion-de-cultivo-de-coca/))

<iframe id="audio_20032546" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20032546_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
