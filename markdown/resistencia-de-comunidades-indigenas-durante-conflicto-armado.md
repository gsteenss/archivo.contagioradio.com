Title: Resistencia de comunidades indígenas durante conflicto armado
Date: 2020-10-23 09:20
Author: AdminContagio
Category: Actualidad, Nacional
Tags: #LaVerdadIndígena, Comisión de la Verdad, conflicto armado en Colombia, pueblos indígenas
Slug: resistencia-de-comunidades-indigenas-durante-conflicto-armado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/abre-nota-ruta-indig_eda323a9ef4e37c66241972455aec880.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Unidad de Víctimas

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En un viaje de casi 10 días, la Minga se movilizó este lunes 19 de octubre, a la Plaza de Bolívar en Bogotá exigiendo el fin de la violencia que ha perjudicado los procesos de las comunidades indígenas, así como la implementación de los acuerdos de paz y la protección de sus territorios. (Le puede interesar: [Avanza la Minga en defensa de la vida](https://archivo.contagioradio.com/la-minga-sera-en-defensa-de-la-vida-el-territorio-la-democracia-y-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esta movilización se suman los cinco preencuentros regionales realizados por l[a Comisión de la Verdad](https://comisiondelaverdad.co/actualidad/noticias/la-ruta-hacia-el-encuentro-por-la-verdad-laverdadindigena) denominados **La ruta hacia el Encuentro por la Verdad Indígena**, **como una preparación para el acto de reconocimiento nacional de este 23 de octubre.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estos cinco encuentros permitieron dar a conocer, de la voz de los mismos pueblos originarios, cómo ha afectado el conflicto armado interno a las comunidades indígenas de la Amazonía, del Caribe, de la región Andina, de la Orinoquía y Norte de Santander.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En uno de los encuentros, Sonia Londoño, directora de la Dirección de Pueblos Étnicos de la Comisión de la Verdad señaló que se busca **«reconocer la dignidad de los pueblos indígenas, las violaciones cometidas en su contra en el contexto del conflicto armado y sus impactos en términos del exterminio físico y cultural a que esto ha conllevado, igualmente, el reconocimiento a su resistencia y a la construcción de paz**».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar de ser un espacio para evidenciar el impacto que ha tenido el conflicto armado, cada uno de los testimonios fueron un recordatorio de su resistencia y resiliencia **«**a través de la práctica de la espiritualidad, la medicina tradicional, la economía propia y su lengua**»**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En cada uno de los encuentros, pueblos contaron al país cómo sus comunidades han sido víctimas de reclutamiento forzado, despojo y desplazamiento. Además, relataron las afectaciones en su territorio por la explotación de los recursos naturales y el narcotráfico que contribuyeron a la contaminación y la desaparición del ecosistema, y en consecuencia, a que cerca de 68 pueblos estén en riesgo de exterminio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También narraron las afectaciones ambientales, políticas, sociales, culturales y comunitarias a causa de empresas que se encuentran en los territorios como El Cerrejón, en La Guajira. **Jakeline Romero Epiayú, de la organización Fuerza de Mujeres Wayuu, resaltó que la militarización y la entrega de títulos mineros por parte del Estado «se fue reduciendo mucha parte del territorio ancestral Wayuu».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, se hizo un llamado en contra de la estigmatización, el racismo y la discriminación de la que han sido blanco cuando se movilizan para visibilizar las afectaciones en sus territorios, tal como sucedió con la Minga de este 2020. (Le puede interesar: [Minga del 10 de Octubre avanza a pesar de la estigmatización](https://archivo.contagioradio.com/minga-del-10-de-octubre-avanza-a-pesar-de-la-estigmatizacion/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También hicieron un llamado para que se implemente el acuerdo de paz, afirmando que es «la principal garantía para la no repetición», así como exigir que que los responsables en el marco del conflicto armado aporten a la verdad y a la construcción de paz.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, llamaron a la sociedad colombiana a contribuir para la protección del territorio y a rechazar las violaciones cometidas en contra de los pueblos y comunidades indígenas y el exterminio físico y cultural del que continúan siendo víctimas.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
