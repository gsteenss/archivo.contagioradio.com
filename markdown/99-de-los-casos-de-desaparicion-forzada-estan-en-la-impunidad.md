Title: 99% de los casos de desaparición forzada están en la impunidad
Date: 2016-08-30 14:06
Category: DDHH, Nacional
Tags: Derechos Humanos, Desaparición forzada, impunidad, paz
Slug: 99-de-los-casos-de-desaparicion-forzada-estan-en-la-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/jj1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [30 Ago 2016] 

Este 30 de agosto se conmemora el Día Internacional del Detenido Desaparecido decretado por la Asamblea General de las Naciones Unidas, que ha catalogado la práctica de desaparición forzada como “**violación múltiple y compleja de los derechos humanos”** en la que se atenta contra el derecho a la vida, la libertad, la integridad personal y el reconocimiento a la personalidad según la Corte Interamericana de Derechos Humanos. Sumado a ello la violación del derecho a la verdad por parte de las consideradas también víctimas, es decir, de los familiares de los desaparecidos.

En Colombia, **las primeras luchas por parte de familiares de los detenidos desaparecidos tienen lugar en febrero de 1983,** cuando en un acto de denuncia pública y reclamo ante la negligencia e incumplimiento del Estado en su deber de “investigar los hechos y sancionar adecuadamente a los responsables”, se congregan en las calles familiares y amigos de los desaparecidos, dando lugar a la creación de la Asociación de Familiares de Detenidos- Desparecidos, ASFADDES.

Según Gloria Gómez, coordinadora de ASFADDES, en Colombia la desaparición forzada ha sido empleada como mecanismo de terror, “neutralización de fuerzas”  y eliminación de la oposición política, ejercida de manera sistemática por funcionarios o agentes del Estado, grupos paramilitares, en múltiples ocasiones con la complacencia del Estado, y por grupos insurgentes. Sumado a ello las fallas institucionales en cuanto a los procesos de búsqueda, exhumación y entrega de cadáveres que aún hoy siguen vigentes y los altos índices de impunidad.

En la audiencia sobre desaparición forzada realizada el año pasado, por Iván Cepeda, Alirio Uribe y Ángela María Robledo se indicó para ese entonces existían más de 122 mil personas desaparecidas según cifras oficiales, además hay 90 mil investigaciones en curso, y únicamente hay 105 sentencias, lo que significa que el **“99% de los casos están en la impunidad”.**

Por tal motivo se considera que una de las condiciones necesarias para avanzar en materia del esclarecimiento de los hechos, la memoria, la no repetición y la verdad, tiene que ver con el establecimiento de acciones conjuntas por parte de las víctimas y las diferentes instituciones teniendo en cuenta los mecanismos ya establecidos; como es el caso de la ley 589 del 2000 en la que se tipifican y se establecen los mecanismos vigentes, como también el vincular activamente  a las  organizaciones de víctimas desde su carácter propositivo en la elaboración y ejecución de los proyectos acordados.

### Desapariciones forzadas en el contexto y a razón del conflicto armado: Acuerdo conjunto N° 62 y Mesa de Trabajo Sobre Desapariciones Forzadas 

En la actualidad en el marco del acuerdo conjunto N° 62 por parte del Gobierno Nacional y las FARC-EP presentado el 17 de octubre del 2015 desde la Habana, la Mesa de Trabajo Sobre Desaparición Forzada (MDTF), de la Coordinación Colombia, Europa y Estados Unidos presentó a la mesa de diálogo de la Habana una serie de recomendaciones (111 en total) sobre la desaparición forzada en Colombia.

Los tres puntos centrales del informe de la MDTF tienen que ver; primero con una serie de recomendaciones relacionadas a las medidas inmediatas a implementar para la búsqueda, ubicación, identificación y entrega digna de los restos; segundo con el fortalecimiento institucional y la participación activa de las víctimas en los procesos;  y tercero en lo que respecta el  fortalecimiento de la  UBPD  **(Unidad Especial para la Búsqueda de Personas Desaparecidas)”.**

Las propuestas tocan aspectos claves como son: la agilización en los procesos de entrega de información por parte de las FARC-EP, el Estado colombiano y los diferentes grupos paramilitares; la creación del universo de personas desaparecidas en contexto y a razón del conflicto armado; la participación activa de las víctimas (familiares) de desaparición forzada y las diferentes organizaciones de derechos humanos, fuentes de financiación, mecanismos de veeduría ciudadana a los procesos, y el fortalecimiento de la UBPD, la cual según la Mesa de Trabajo  funcionará como mínimo en un periodo de “10 años, prorrogable previo concepto del Comité Veedor, una vez se haya evaluado la eficacia de la Unidad y se emitan recomendaciones para su continuidad”.

Por otro lado se establecieron los dos puntos claves en materia de desaparición forzada en Colombia en razón del conflicto armado; en el primer punto se establece **“poner en marcha unas primeras medidas inmediatas humanitarias de búsqueda, ubicación, identificación y entrega digna de restos de personas dadas por desaparecidas”**. Algunos de los aspectos que engloban dicho punto son:

-   la solicitud del apoyo por parte de la Cruz Roja internacional en el “diseño y puesta en marcha de planes especiales humanitarios para la búsqueda, ubicación, identificación y entrega digna de los restos a sus familiares”

<!-- -->

-   la cooperación por parte del Gobierno Nacional y las FARC-EP en la entrega de información al Comité Internacional de la Cruz Roja (CICR), en consonancia con la “información brindada por parte de las organizaciones de víctimas” con el ánimo de “facilitar la ejecución de los planes humanitarios”

<!-- -->

-   y el compromiso por parte del Gobierno Nacional en “acelerar los procesos de identificación y entrega digna de los restos de las víctimas y de quienes hayan muerto en desarrollo de operaciones de la Fuerza Pública inhumanos como N.N en cementerios ubicados en las zonas más afectadas por el conflicto”.

El segundo punto trata sobre  la creación de una Unidad Especial para la Búsqueda de Personas Desaparecidas (UBPD), la **creación de dicha Unidad Especial será de carácter “transitorio y excepcional” con amplia participación por parte de la víctimas y hará parte del Sistema Integral de Verdad, Justicia, Reparación y no Repetición.**  En este punto se busca establecer lo sucedido a las personas “dadas por desaparecidas como resultado de acciones de Agentes del Estado, de integrantes de las FARC-EP, o de cualquier organización que haya participado en el conflicto”.

Dentro de las funciones de la UBPD están: (1) la “recolección de la información con el fin de establecer el universo de personas desaparecidas en el contexto y a razón del conflicto armado”; (2) la agilización en los procesos de identificación a cargo del Instituto Nacional de Medicina Legal y las Ciencias Forenses”;(3) libre acceso a las bases de datos oficiales y suscribir convenios con las organizaciones de víctimas y de Derechos Humanos; (4) “garantizar la participación por parte de los familiares de las víctimas en los procesos de  búsqueda, identificación, localización y entrega digna de los restos”.

<iframe id="doc_57593" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322588732/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
