Title: Con panfletos difunden amenazas de 'limpieza social' en Cauca
Date: 2018-06-28 14:55
Category: DDHH, Nacional
Tags: Comisión de Justicia y Paz, Limpieza social, Panfleto, Sustitución Voluntaria
Slug: panfleto-limpieza-social-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/amenaza-argelia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 Jun 2018] 

El autodenominado grupo Comando Popular de Limpieza (CPL), amenazó a través de un panfleto que realizará una limpieza social en el municipio de Argelia, Cauca. Los panfletos fueron encontrados por los pobladores del caserío El Mango, el domingo 24 de junio y en ellos se afirma que se acabará con la vida quienes estén relacionados de cualquier forma con el expendió y consumo de drogas, así como contra personas que ellos consideren como 'malas'.

Según la [Comisión Intereclesial de Justicia y Paz](https://www.justiciaypazcolombia.com/limpieza-social-y-amenazas-en-el-municipio-de-argelia/), aunque el proceso de dejación de armas de las FARC se cumplió, la reincorporación de los ex-combatientes ha avanzado lentamente, lo que posibilita el regreso de algunos de ellos a las armas. Adicionalmente, el ELN ha aumentado sus operaciones en la zona, y el panfleto ha sido comprendido por la comunidad como una forma de control territorial a las formas de organización diversas que hay en la zona.

De igual forma, la Comisión de Justicia y Paz señala que la iniciativa de sustitución voluntaria de cultivos de uso ilícito avanza lentamente, y la siembra de cultivos de pan coger se dificulta por las condiciones de infraestructura, que imposibilitan la salida de las cosechas. Por estas razones, la situación en Argelia es delicada y debe ser resuelta con un enfoque que responda a las violencias estructurales que se viven en la zona. (Le puede interesar: ["Incursión de tipo paramilitar deja tres muertos y nueve heridos en Argelia, Cauca"](https://archivo.contagioradio.com/incursion_armada_paramilitar_argelia_cauca/))

###### ![Panfleto de Amenaza en argelia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/WhatsApp-Image-2018-06-28-at-10.17.41-AM-600x800.jpeg)

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)] 
