Title: Ana Lucía Fernández lleva 4 días desaparecida en Medellín
Date: 2020-09-24 19:45
Author: CtgAdm
Category: Actualidad, Nacional
Tags: Ana Lucía Fernández, desaparecida, Medellin, mujeres
Slug: 4-dias-lleva-desaparecida-ana-lucia-fernandez-en-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/BuscarlasHastaEncontrarlas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Desde el pasado 21 de septiembre se reporta la desaparición de **Ana Lucía Fernández**, en Medellín, hija de la directora regional de la [Comisión de la Verdad](http://comisiondelaverdad.co) en esta ciudad.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Estamos_Listas/status/1309131950830030855","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Estamos\_Listas/status/1309131950830030855

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Ana Lucía** Fernández tiene 17 años de edad, y desde las lunes a las 7:00 pm no se conoce nada de ella, luego de ser vista por última vez en **el sector de La Aguacatala de Medellín** junto a su compañero sentimental. Quién indica que luego de despedircen a las 6:30 pm, ella tomó un taxi.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de ser un hecho que enciende las alarmas de las organizaciones feministas al tratarse de una desaparición en la segunda ciudad con la taza más alta de[violencia contra la mujer](https://archivo.contagioradio.com/otra-mirada-las-caras-de-la-crisis-economica/), 4.809 agresiones en el 2020 según Medicina Legal; **sus padres son reconocidos defensores de la paz en Colombia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por un lado los papas de Ana Lucia Fernández, son Marta Villa, directora regional de la Comisión de la Verdad en Antioquia, y **Rubén Fernández Andrade, es subdirector del Centro de Fe y Culturas de Medellín.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
