Title: Estudiantes de la Universidad Pedagógica llaman a un nuevo paro nacional estudiantil
Date: 2017-10-17 16:36
Category: Educación, Nacional
Tags: Movimiento estudiantil, Universidad Pedagógica Nacional
Slug: estudiantes-de-la-universidad-pedagogica-llaman-a-un-nuevo-paro-nacional-estudiantil
Status: published

###### [Foto: Contagio Radio] 

###### [17 Oct 2017] 

Estudiantes de la Universidad Pedagógica Nacional que iniciaron hoy un paro de actividades, exigien al gobierno presupuesto para cubrir el déficit de infraestructura y de planta docente de **un billón de pesos, el aumento del rubro para la educación pública que supere el déficit de los 13 billones** y la finalización de la política pública de “Ser Pilo Paga”, invitaron a la realización de un paro nacional para discutir el futuro de la educación pública.

De acuerdo con Amalfi Bocanegra, representante de los estudiantes ante el Consejo Superior de la Universidad, hace un mes radicaron una carta ante el Ministerio de Educación, el Ministerio de Hacienda y el Congreso de la República con 5 puntos. El **primero de ellos consiste en aumentar el presupuesto general para la educación, que deberían estar por encima de los 4 puntos del IPC**. (Le puede interesar: ["Universidad Distrtial dejará de recibir más de 7 mil millones de pesos al año"](https://archivo.contagioradio.com/universidad-distrital-dejara-de-recibir-mas-de-7-mil-millones-de-pesos-al-ano/))

Específicamente los estudiantes están proponiendo que el dinero que ingresa para financiar a ICETEX entre a cada una de las universidades públicas. Según Bocanegra la **deuda actual que tiene el gobierno con las instituciones es de 13 billones** de pesos y con su propuesta, los centros educativos podrían recibir cada una un billón de pesos. Además, exigen que se aumente en un 40% el presupuesto para COLCIENCIAS.

### **La Universidad Pedagógica no tiene docentes** 

De acuerdo con la representante estudiantil, la crisis de la Universidad Pedagógica Nacional se evidencia en la infraestructura de la institución y en la planta docente, “no tenemos como ampliar la plata doncente, **tenemos solo el 17% como profesores de planta, el resto están contratados por horas de clase o por meses**, situación que no genera garantías para la investigación o para que haya una contratación digna”.

Por este motivo, estudiantes, docentes y algunos sindicatos de trabajadores decidieron hacer el llamado a **paro que permita construir una propuesta en conjunto que logre que la Universidad Pedagógica Nacional supere la crisis que afronta**. (Le puede interesar:["Paro estudiantil de la UPTC logra acuerdo definición en costo de matrículas"](https://archivo.contagioradio.com/paro-estudiantil-de-la-uptc-acuerdo-sobre-definicion-en-costo-de-matriculas/))

De igual forma, los estudiantes manifestaron que es contradictorio que el Ministerio de Educación exija a las universidades públicas **acreditarse como de alta calidad, cuando ello no se ve reflejado en mejores laboratorios, entre otros**.

Durante la jornada de hoy se desarrollará una asamblea multiestamentaria, en donde se espera que se llegue a un acuerdo de todos los sectores que hacen parte de esta comunidad académica para establecer la hoja de ruta del paro.

### **Estudiantes se preparan para un paro nacional** 

Amalfi Bocanegra señaló que los estudiantes de la Universidad Pedagógica Nacional están haciendo un llamado a un paro Nacional “**todas las universidades públicas están afrontando problemáticas presupuestales, ahora tienen que vender servicios de extensión o aumentar las matrículas para sobrevivir**”.

Por esta razón los estudiantes están convocando a una reunión de carácter distrital, en la que no solo participen las universidades públicas, sino también las privadas, **el próximo viernes para construir en conjunto un pliego de exigencias** frente a la crisis de la educación en el país. (Le puede interesar: ["Así fue la movilización en defensa de la Educación"](https://archivo.contagioradio.com/estar-seran-las-rutas-de-movilizacion-de-la-marcha-por-la-educacion/))

Otras universidades se han movilizado recientemente lo que podría facilitar un proceso de unidad dado que los problemas de infraestructura, planta docente, ser pilo paga y presupuesto para la investigación son ejes comunes de las protestas recientes en diversos claustros.

###### Reciba toda la información de Contagio Radio en [[su correo]
