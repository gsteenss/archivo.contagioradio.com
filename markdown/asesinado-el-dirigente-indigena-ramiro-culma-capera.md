Title: Asesinado el dirigente indígena Ramiro Culma Capera
Date: 2016-09-21 13:18
Category: DDHH, Nacional
Slug: asesinado-el-dirigente-indigena-ramiro-culma-capera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Cabildo-Coyaima.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Por La Tierra ] 

###### [21 Sept 2016 ] 

En el municipio de Coyaima, departamento del Tolima, fue **asesinado el líder indígena Ramiro Culma Capera**, quien era fundador del cabildo El Rosario, miembro de la Asociación de Cabildos Indígenas del Tolima y militante del Partido Comunista y de la [[Unión Patriótica](https://archivo.contagioradio.com/son-fundamentales-las-garantias-de-no-repeticion-aida-avella/)]. El hecho ocurrió el pasado sábado en horas de la noche, cuando él se dirigía a su casa y fue atacado con una ametralladora.

El líder indígena de 40 años de edad y padre de dos hijos, **venía trabajando en actividades de pedagogía de paz**, para [[promover el sí en el plebiscito](https://archivo.contagioradio.com/crecen-seguidores-de-campanas-como-sumate-al-si-en-colombia/)], pues según se conoce, consideraba que estaba ante la posibilidad de abrir espacios en la región para la consolidación de la paz con justicia social, por lo que los **pobladores aseguran estar consternados con lo sucedido** y agregan que "era un hombre bueno, trabajador y emprendedor".

El hecho enciende alarmas en todo el departamento, pues pese al miedo, las comunidades vienen denunciando la alta militarización del territorio y la [[presencia de paramilitares](https://archivo.contagioradio.com/es-fantasioso-asegurar-que-el-paramilitarismo-no-sigue-vigente/)] que continúan extorsionando a los pobladores. La familia y allegados al líder indígena esperan que las autoridades pongan en marcha una **exhaustiva investigación que dé cuenta de los los responsables materiales e intelectuales**, para que sean sancionados.

Vea también: [[En menos de un mes han sido asesinados 11 líderes sociales en Colombia](https://archivo.contagioradio.com/en-menos-de-un-mes-han-sido-asesinados-11-lideres-sociales-en-colombia/)]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
