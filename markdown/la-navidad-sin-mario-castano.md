Title: La Navidad sin Mario Castaño
Date: 2017-12-03 07:42
Category: Abilio, Opinion
Tags: Curvarado, Líderes asesinados, Mario Castaño
Slug: la-navidad-sin-mario-castano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/mario-castano-bravo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión Justicia y Paz 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 3 Dic 2017 

[El crimen de Mario Castaño el pasado 26 de noviembre en el Bajo Atrato es una nueva herida a la esperanza de  paz.]

[En Colombia  los deseos,  la alegría, paz y prosperidad,  propios de estas épocas decembrinas ha tenido un referente tangible en los diálogos con las FARC,  en los consiguientes acuerdos, en  un cese bilateral con el ELN y en  una mesa de diálogos andando. Sin embargo  crímenes como el de Mario, la colcha de retazos en que quedó convertido el acuerdo de paz primero por la responsabilidad del propio nobel de paz y luego por el congreso de la república, los aprietos por los que pasa la mesa de Quito, nos ponen de nuevo ante la pregunta por las razones para tener esperanza de paz en el país, más allá del silenciamiento de los fusiles.]

[La vida de Mario, como la de otros líderes del Curvaradó,  que han sido sistemáticamente asesinados en la defensa de sus  territorios, da luces sobre el sentido real de la Esperanza. Puede estar  en la verdad que contienen sus testimonios  y que,  así haya llegado a algunos expedientes judiciales, no  fue  tenida en cuenta para sus fallos por los  aparatos de justicia. Se esperaría  que,  al menos, sea recogida en la Comisión de Esclarecimiento de la verdad, pues abundan informes  suscritos por organizaciones nacionales e internacionales que dan cuenta del contexto empresarial de la región de Urabá y Bajo Atrato en donde se debe indagar sobre los verdaderos responsables  del despojo y asesinatos en ésta región del país. Al menos la verdad, en este mar de impunidad que todo lo inunda.]

[Antes de Mario,  perdió la vida  Orlando Valencia quien se aprestaba a viajar a Chicago a denunciar la ocupación empresarial y paramilitar de las tierras del Curvaradó,  asesinado al regresar a su casa, luego de que la embajada de Estados Unidos le negara la visa. Los responsables estaban a la vuelta de la esquina en la sede de la empresa palmera Urapalma en Bajirá, apoyada con subsidios del gobierno, más el único condenado fue uno de los funcionarios medios de la compañía, quedando militares, policías y miembros  del gobierno, amparados en la más absoluta impunidad.]

[Otra de las víctimas  en  Caño Manso,  muy cerca del lugar en que mataron a Mario, fue  Ualberto Hoyos, un campesino soñador, investigador, veedor, obsesionado por la verdad y la justicia, quien declaró  ante todas las instancias,  cuantas veces se lo pidieron. Sobrevivió a un atentado y luego le quitaron la vida en las propias tierras que defendía contra la ocupación que hiciera el empresario, ex militar, tío de capo del narcotráfico, miembro del Centro Democrático, miembro de Acore, vinculado con la masacre de Mapiripán,  el coronel (r) Luis Felipe Molano. Los serios indicios que conducían a la responsabilidad de su estructura y la presión internacional,  llevaron a que la fiscalía interceptara alguno de sus teléfonos, pero hasta ahí llegaron las “investigaciones exhaustivas” y el caso está en la impunidad. Un año antes había aparecido muerto, con golpe en la cabeza,  el adulto mayor Benjamín Gómez, quien no cedió a las presiones de los trabajadores del empresario por quedarse con sus tierras, hecho que nunca se esclareció.]

[Con valentía  las familias de Caño Manso, en una acción noviolenta, desalojaron al  empresario, pues ni siquiera la orden, que para ese fin   emitió  la Corte Constitucional,  fue acatada por las autoridades  del actual gobierno.]

[Luego vinieron los  crímenes de  Argénito Díaz, de Manuel Ruiz y Samir Ruiz, del sector de Llano Rico, también en Curvaradó. Ellos lideraban la reclamación de tierras frente a los poderosos empresarios Lopera, Ríos y  Montoya. En tierras expropiadas a los campesinos  de los presuntos responsables de los crímenes,  el ejército instaló el Batallón 54 de Selva, respaldando las ocupaciones ilegales. Quienes asesinaron a  Argénito no aparecen. Sólo los directos responsables de asesinar a Manuel y Samir están procesados, más los determinadores amparados en la impunidad de la justicia penal y ahora de la JEP, siguen campantes ocupando los territorios de las comunidades. Se teme  ahora por la vida de Enrique Cabezas, otro líder que  ha sido capaz  de decir lo que ha visto y vivido, que padeció un exilio forzado y que permanece en los territorios.]

[Y ahora el crimen de Mario.  El  conoció muy de cerca a los despojadores, empresarios del banano, de la ganadería extensiva a quienes, como voz que grita en el desierto, denunció con desespero. Igual que  Ualberto, llenó de expedientes todas las instancias de esta erosionada institucionalidad, de los que cargaba copias que casi no cabían en su maletín.  Hablaba con nombres propios, por ejemplo del empresario Francisco  “Pacho” Castaño quien ocupó parte de la finca de su propiedad, pero también de los bananeros Restrepo Girona y sus socios los Gaviria Correa que extendieron sus negocios  de banano a las tierras de las comunidades en Pedeguita y Mancilla, de la mano del intocable Juan  Guillermo Gonzales, que luego de una corta temporada en prisión, recuperó su libertad para seguir ejerciendo el control de parte de ese extenso territorio.]

[A Mario  lo escuchamos decir en la asamblea  de Conpaz, en el Centro de Memoria Histórica de Bogotá, que “¿se desarma la guerrilla, pero qué pasa con los paramilitares?” Y justos fueron ellos los que  segaron su vida, a un año de firmado el acuerdo del Teatro Colón.  Esos paramilitares  que se reciclaron, que se amparan en la acción u omisión de la fuerza pública y que obedecen a los “terceros responsables”, es  decir, en estos casos, a los empresarios que se apropiaron de  tierras en Curvaradó, Pedeguita y Mancilla, parte del Jiguamiandó y La Larga Tumaradó. Aquellos a los que  se refirió en carta pastoral  el obispo católico  de Apartadó, pidiendo llamar por su propio nombre.]

[Esta temporada navideña sin Mario, será de todos modos oportunidad para  reencuentros familiares,   diálogos con amigxs y para conmemorar  un nacimiento. Puede ser un momento para  intentar resignificar la esperanza en la vida, en medio de tanta muerte  y para repensar las dimensiones de una paz y prosperidad  que vaya más allá del importante  logro ya alcanzado del silenciamiento de los fusiles entre dos partes.  Nada fácil el desafío.]

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio
