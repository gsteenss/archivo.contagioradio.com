Title: Defender a los defensores
Date: 2017-03-07 12:19
Category: Ambiente y Sociedad, Opinion
Tags: Agua, ambientalistas, Ambiente, defensores de derechos humanos
Slug: defender-a-los-defensores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/5482206494_7cf1c48046_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Gert Steenssens 

#### [**Por: [Natalia Gómez Peña/Democracia Ambiental/ Asociación Ambiente y Socieda](https://archivo.contagioradio.com/margarita-florez/)d **] 

###### 7 Mar 2017 

Esta semana los relatores especiales de Naciones Unidas hicieron un llamado al gobierno de Kenia para proteger a cuatro defensores de derechos humanos ambientales que están siendo atacados y amenazados de muerte por haber presentado una demanda contra una fundidora de plomo que estaba contaminando una comunidad. Los cuatro defensores están ahora escondidos a la espera de que su gobierno les brinde las debidas medidas de protección, mientras la comunidad internacional se moviliza en torno a ellos para salvar sus vidas.

Mientras tanto en Colombia la cifra de defensores de derechos humanos asesinados, entre el 1 de diciembre y lo que va corrido del 2017, ya  llega a 23[\[1\]](#_ftn1){#_ftnref1}. Un número alarmante, y frente al cual el gobierno nacional aun niega un patrón de sistematicidad en estos asesinatos. Varios de los defensores de Derechos Humanos asesinados este año eran también defensores ambientales. Líderes comunales que buscaban la protección de sus comunidades y se oponían al desarrollo de megaproyectos con altos impactos sociales y ambientales en sus territorios.

Vemos como el costo de defender el ambiente es bastante alto en todo el mundo. Ellos, los defensores del ambiente buscan defender lo que es tanto suyo como mío. El agua que llega a nuestras casas, el aire que respiramos. Sin embargo, son constantemente atacados, perseguidos, señalados.  Y en  países como Colombia, con un contexto de conflicto armado y violencia, ese costo para los defensores puede llegar a representar hasta perder la vida.

Colombia está iniciando una nueva etapa de post-conflicto y construcción de paz en la que la protección de los defensores de derechos humanos debe ser un elemento primordial. En ese sentido el acuerdo de paz contempla varias provisiones importantes para hacer frente a la grave situación que viven los defensores. Especialmente en la sección uno sobre reforma agraria y la sección dos sobre participación política el acuerdo de paz desarrollo diferentes aspectos que pueden ser herramientas para los defensores de derechos humanos ambientales y que giran en torno a la participación ciudadana como eje articulador.  Así mismo, el acuerdo de paz llama a la revisión del marco institucional actual, de manera que se adopten mejores herramientas de protección para prevenir ataques a los defensores de derechos humanos y garantizar que se investiguen y castiguen estos hechos.

La falta de efectividad de los mecanismos de participación ciudadana en nuestro país ha desencadenado numerosos conflictos socio-ambientales relacionados con la explotación de recursos naturales. Las dificultades en la recopilación y el acceso a la información ambiental hacen que las comunidades no tengan las herramientas necesarias para ejercer una participación informada y oportuna. El difícil acceso a la justicia pone a las comunidades en situación de desigualdad frente a los grandes intereses  económicos que quieren pasar por encima de sus territorios.

En la semana del 20 al 24 de marzo se llevará a cabo en Brasil la sexta reunión del Comité de Negociación del convenio sobre los derechos de acceso a la información, participación y justicia ambiental para América Latina y el Caribe. Esta es una oportunidad histórica para que el gobierno colombiano de pasos hacia la implementación de los acuerdos de paz y tome medidas concretas para la protección de nuestros defensores ambientales. Por la construcción de paz y la protección de nuestro ambiente llego la hora de que defendamos a nuestros defensores.

###### [\[1\]](#_ftnref1)
