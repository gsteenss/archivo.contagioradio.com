Title: Ciclo 37 de negociaciones de paz inicia con cambios que demuestran avances
Date: 2015-05-21 11:55
Author: CtgAdm
Category: Nacional, Paz
Tags: ciclo, dialogos, FARC, Gobierno, habana, jorge restrepo, Luis Eduardo Celis, maria angela holguin, paz, pinzon, villegaz
Slug: ciclo-37-de-negociaciones-de-paz-inicia-con-cambios-que-demuestran-avances
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/PAZ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ElPais 

#####  

<iframe src="http://www.ivoox.com/player_ek_4528708_2_1.html?data=lZqfmpyUfI6ZmKiakpuJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8rXzdSYlZyPqMafz8rU0cjNpcTd0NPS1ZDIqYzkwt%2BYy9PNp8rVjMjc0JDHpc7WytTgjdbZqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Eduardo Celis, analista] 

 

###### [21 may 2015] 

Inicia ciclo 37 de negociaciones entre el gobierno y guerrilla de las FARC en La Habana, con cambios en el equipo que lidera Humberto de la Calle; la incorporación de la canciller Maria Ángela Holguín y el empresario Gonzalo Restrepo.

A la maniobra del presidente Santos también se sumó, esta semana, el cambio en el gabinete de Defensa, de Juan Carlos Pinzón quien pasará a ocupar la embajada de EEUU, por Luis Carlos Villegas quien ocupará el puesto como Ministro de Defensa.

Para el columnista Luis Eduardo Celis, los cambios propuestos por Juan Manuel Santos son naturales en un proceso que se ajusta y avanza.

En lo concerniente al cambio en el Ministerio de Defensa, Celis evalúa el nombramiento de Villegas como "una buena noticia". "Se espera un talante más dialogante, más abierto a entender que las FARC transiten de ser 'enemigos del Estado' a ser contradictores en la política", declara el analista. "Esperamos de Villegas un tono más tranquilo de Villegas que se diferencie del tono tendencioso de Pinzón".

Ante el temor que pudiera generar este cambio al interior de las Fuerzas Armadas, el analista segura que es necesario que se entienda que "Las fuerzas armadas tienen que acatar el mandato ciudadano que ha recibido el presidente Juan Manuel Santos, que es avanzar hacia una paz negociada". Por otra parte, si las Fuerzas Armadas han sido protagonistas del conflicto colombiano, tienen responsabilidades en el reconocimiento, la contribución de la verdad y la rectificación de los errores cometidos, y que es tarea del ministro entrante liderar ese proceso de reconocimiento.

En cuanto a la vinculación de la cancillería Holguín y el empresario Restrepo en el equipo negociador del gobierno, Luis Eduardo Celis asegura que pueden ser positivas en la medida en que se fortalezca el compromiso de la comunidad internacional con el proceso, y se facilite el tratamiento de temas que les conciernen, como la extradición.

"Esperamos que del ciclo 37 salgan avances importantes, por ejemplo en algo que las FARC han insistido en llamar la "Comisión de esclarecimiento" y el gobierno "Comisión de la verdad", concluyó el columnista.
