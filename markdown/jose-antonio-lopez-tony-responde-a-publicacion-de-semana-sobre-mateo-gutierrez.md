Title: José Antonio López "Tony" responde a publicación de Revista Semana sobre Mateo Gutiérrez
Date: 2017-11-16 18:09
Category: Nacional
Tags: Mateo Gutierrez, MRP, Revista Semana
Slug: jose-antonio-lopez-tony-responde-a-publicacion-de-semana-sobre-mateo-gutierrez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/mateo-gutierrez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [16 Nov 2017] 

Con el título "Basta ya: respuesta a una infame calumnia", José Antonio López Rodríguez, de 76 años de edad, respondió contundentemente al artículo de la Revista Semana, en el que supuestamente se mostraría que Mateo Gutiérrez, señalado de integrar el MRP, habría recibido entrenamiento como explosivista, durante una visita a Cuba en la que se hospedó en la casa del exdiplomático.

López Rodríguez, asegura que **la publicación fue realizada con "gran despliegue y amarillista cintillo",** y afirmó que Mateo y su mamá, Aracelis León se hospedaron en el apartamento que rentan a turistas extranjeros, del 12 al 20 de enero de 2017, y recomendados por un amigo común en Colombia, y no porque hubiera entablado una relación con la familia durante su pertenencia **al servicio diplomático durante 2000 y 2005,  cuando apenas Mateo tenía cerca de unos  años.**

El exdiplomático jubilado hace ocho años asegura que la publicación busca "sembrar la matriz de opinión" y que el artículo quiere hacer creer "que lo publicado se ajusta a la verdad y que en Cuba se entrenan jóvenes para realizar actos terroristas".

Además resalta que un señalamiento como este debe ser probado ante los tribunales, "si fuera cierto que la Fiscalía, lo cual dudo, hizo tal señalamiento, tiene que tener en su poder contundentes pruebas y presentarlas", y cierra afirmando que **"es vergonzoso que tales artículos sean publicados con fines aviesos** y mucho menos en una revista que cuenta con un equipo profesional y periodistas altamente calificados".

Este es el texto de la carta

*La Habana 15 de Noviembre del 2017.*  
*“Año 59 de La Revolución”*

*Sr. Alejandro Santos Rubino*  
*Director de la Revista Semana. Bogotá, Colombia.*  
*Basta Ya: Respuesta a una infame calumnia.*

*Mi nombre es José Antonio “TONY” López Rodríguez. En la edición 1854 del 12 al 19 de noviembre del 2017 con gran despliegue y amarillista cintillo se publicó el artículo titulado Cartas Explosivas. Según el artículo en un registro en la cárcel donde guarda prisión el joven Mateo Gutiérrez León, le ocuparon una carta dirigida a mi persona, en la cual él menciona, que en una conversación en mi casa en La Habana le aconsejé que no regresara a Colombia, a partir de allí comienzan a tejer una serie de historias que nada tiene que ver con la verdad, todas traídas por los pelos, que evidencia un gran montaje de quienes en Colombia son enemigos de la paz.*

*Mateo y su señora madre, la doctora Aracelys León Medina, se hospedaron en el apartamento que rentamos a turistas extranjeros del 12 al 20 de enero del 2017, vinieron recomendados por un amigo colombiano nombrado Luis Eduardo Celis, quien en otras oportunidades y con criterio solidario con mi esposa y conmigo, nos ha enviado sus amistades a hospedarse en el apartamento que alquilamos.*  
*Para mi sorpresa el artículo, por cierto muy mal redactado, propio de un libelo de quinta categoría, trata con intencionalidad y perversa motivación política, señalar que la Fiscalía, en uno de los viajes de Mateo a Cuba, en el 2013 y 2017 “pudo haber recibido instrucción en el uso de explosivo”. La palabra “pudo haber” niega que lo afirme, es un recurso idiomático para salvar la ropa, porque son absolutamente conscientes de la mentira y están incurriendo en un delito. Si fuera cierto que la Fiscalía, lo cual dudo, hizo tal señalamiento, tiene que tener en su poder contundentes pruebas y presentarlas.*

*El autor de este canallesco artículo, va tejiendo este novelón, cuando hace la siguiente afirmación “No obstante la carta a Tony, abre una interrogante, especialmente, por el destinatario” la interrogación entra en el campo de la especulación y qué tiene de especial el destinatario, esa razón especial, es sencillamente fortalecido con la siguiente alegación “en los noventa y durante algún tiempo formó parte de la representación diplomática de la isla en Colombia” y termina de rematar, “durante muchos años las agencias nacionales y extranjeras lo estuvieron vigilando porque aseguraban que pertenecía al G-2 – Inteligencia Cubana.*

*Todo lo dicho arriba es una pura falsedad, está montado con intencionalidad y motivaciones políticas de dañar las relaciones con Cuba y contradicen las manifestaciones del alto gobierno y el pueblo colombiano que ha reconocido el papel jugado por Cuba a favor de la paz.*

*Cumplí misión diplomática en Colombia desde abril del 2000 hasta enero del 2005 y no en los años 90. Mantuve relaciones con partidos políticos, organizaciones sociales, religiosos, académicos, periodistas, personalidades de la cultura y la ciencia, congresistas, con el Alto Comisionado de Paz y fui, junto a nuestro embajador, delegado de Cuba en el Grupo de países Facilitadores de Paz, durante los diálogos con las FARC en San Vicente del Caguan y del Grupo de Amigos en el proceso de paz con el ELN, tanto en el gobierno de Pastrana, como en los años 2006 y 2007 en el gobierno de Álvaro Uribe Vélez, llevado a cabo en La Habana, Cuba. De allí las relaciones mantenidas con el Secretariado y con el COCE. No en los años 90 como apunta el artículo.*

*En 1966, hace 51 años que fui elegido para integrar la comisión de Relaciones Exteriores de la Unión de Jóvenes Comunistas y unos años después fui promovido como funcionario del Departamento América del Comité Central del Partido. Departamento encargado de las relaciones con América Latina y el Caribe en esa época. No sé de dónde sacan la historia de que en esta época era miembro del G-2 Inteligencia Cubana.*

*Estoy jubilado hace 8 años y no 2 como apunta el articulo, otra inexactitud, absolutamente desvinculado de cualquier función pública, dedicado a trabajar mi tesis para el doctorado a mis 76 años y ejerciendo mi vieja profesión de periodista, para un diario en Méjico.*

*En fin la idea es sembrar la matriz de opinión, “instrucción en explosivo, carta a alguien especial, que era diplomático en los 90 en la embajada cubana y sospechado de ser miembro de la inteligencia cubana, que era vigilado por las agencias nacionales e internacionales”. Todo eso unido, arroja lo que quieren que el lector concluya, que lo publicado se ajusta a la verdad y que en Cuba se entrenan jóvenes para realizar actos terroristas y de paso culpar a Mateo Gutiérrez León,*

*Sin ningún tapujo confiesan que las agencias de inteligencia internacionales, pueden actuar en Colombia, vigilando a diplomáticos de un país con el cual mantienen relaciones. Es evidente que no es una conducta honorable, porque es ceder independencia y soberanía a un país extranjero. Que los lectores juzguen.*

*Es vergonzoso que tales artículos sean publicados con fines aviesos y mucho menos en una Revista que cuenta con un equipo profesional y periodistas altamente calificados, algunos de los cuales son viejos amigos, por lo que colijo que ninguno de ellos se prestó a este lamentable y despreciable show.*

*Para salvar tan lamentable suceso, le pido que con igual destaque publique esta carta abierta y pública, pues debo no solo rechazar y condenar este hecho, porque atenta contra el buen nombre de Cuba, país que ha sido desde hace muchos años solidario con Colombia y con un gran compromiso en la búsqueda de la paz, lo cual quedó demostrado con los Acuerdos de La Habana.*

*También dejar muy clara mi postura hacia Colombia y su pueblo al que respeto y admiro. No se puede, perversamente, tratar de mancillar a un país y a sus funcionarios, no es esa la noble e inteligente y veraz labor del periodismo.*  
*A la espera de su atención y que quede subsanado este doloroso pasaje.*

*Atentamente,.*  
*José Antonio “TONY” López Rodríguez.*  
*La Habana. Cuba.*
