Title: Hay una ruptura entre el poder y la autoridad: Dario Monsalve
Date: 2020-09-15 10:23
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Arquidiócesis de Cali, Pacto por la Vida y la Paz, Policía Nacional
Slug: hay-una-ruptura-entre-el-poder-y-la-autoridad-dario-monsalve
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Dario-Monsalve.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: monseñor Dario Monsalve /@OArquidiocesis

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tras una semana en la que fueron asesinadas 13 personas a manos de la Policía Nacional, la constante violencia en los territorios que ha dejado el saldo de 55 masacres en diversas zonas del país y tras la firma de un pacto por las paz en regiones como el pacífico y el suroccidente del país, cuyas comunidad claman al Gobierno soluciones, sectores religiosos como la Arquidiocesis de Cali reiteran la necesidad de buscar una salida negociada al conflicto.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de un panorama en que las víctimas fatales de las masacres han ascendido a 218, los defensores de DD.H.H. asesinados han llegado a los 214 y los excombatientes que han perdido la vida alcanzan las 43 personas, según el Instituto de estudios para el desarrollo y la paz (Indepaz), monseñor Darío Monsalve, [hizo un llamado](https://www.elespectador.com/colombia2020/pais/el-poder-no-puede-generar-orden-cuando-es-abusivo-monsenor-monsalve/) al gobierno para que este "recupere su legitimidad". [(Lea también: En dos años del Gobierno Duque se ha derrumbado institucionalidad)](https://archivo.contagioradio.com/en-dos-anos-del-gobierno-duque-se-ha-derrumbado-institucionalidad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En ocasiones anteriores, monseñor Monsalve ha señalado que si bien, múltiples sectores de la sociedad han expresado su anhelo de un cambio significativo, sin embargo atribuye la responsabilidad a un Gobierno Nacional caracterizado por una "actitud “arrogante”, al no asumir un problema y las propuestas de solución de una manera eficaz.[(Le recomendamos leer: El Gobierno está del lado contrario de la ciudadanía: Arzobispo de Cali)](https://archivo.contagioradio.com/el-gobierno-esta-del-lado-contrario-de-la-ciudadania-arzobispo-de-cali/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para monseñor, el camino que había emprendido el país hacia una salida política al conflicto armado, se detuvo ante un "desaliento institucional y un desaliento social" reflejado en lo que denominó **un "gran vacío de lo estatal" reflejado en un Estado desdibujado y una sociedad fragmentada.** [(Le puede interesar: Es hora de que el Gobierno deje esa actitud “arrogante” frente al asesinato de líderes)](https://archivo.contagioradio.com/gobierno-deje-actitud-arrogante-frente-asesinato-lideres/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicho desaliento, como lo expresó Darío Monsalve se evidencia en una discontinuidad en la implementación del Acuerdo de Paz a raíz de la presión de partidos políticos que van en contravía de lo pactado en La Habana, lo que señala tiene consecuencias en una fase tan temprana del proceso, por lo que señaló se requiere "fundamentalmente una voluntad de paz, que no sea engañosa, sino clara y firme".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La Policía no está hecha para reprimir" señala Monsalve

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito de la ausencia de tal institucionalidad y de los hechos de violencia en los que la Policía Nacional participó, Monsalve también la atribuye a la ausencia de equilibrio entre autoridad y poder, evidenciando una "ruptura entre la autoridad, que es lo que legitima realmente, y el poder, que tiene que estar al servicio de la autoridad" y sin embargo hoy, advierte, es un poder abusivo y carente de autoridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, expresó que, en medio de un contexto de implementación del acuerdo, la Policía debe estar enfocado en preservar la vida y "el cuidado de la dignidad del ser humano. Agregó que la Policía no debería acudir a la represión y mucho menos tener en su poder armas letales y al usarlas existe una "desproporción, una desigualdad, una opresión y es pisotear la dignidad y menospreciar la vida de las personas".

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/arzobispodecali/status/1303819120455094294","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/arzobispodecali/status/1303819120455094294

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Los sucesos de los recientes días llevan a concluir al arzobispo que debe reflexionase el cómo incluso la violencia permea "la conciencia nacional y las instituciones". Con anterioridad, Monsalve también había manifestado que el país cayó en una mentalidad de "aniquilar al contrario", por lo que explicó **"se necesita hacer una revolución ética, psicológica y moral para que el derecho a la vida sea el pilar de nuestra sociedad".** [(Lea también: Cinco normativas que deben cambiarse para frenar delitos de la Policía)](https://archivo.contagioradio.com/cinco-normativas-que-deben-cambiarse-para-frenar-delitos-de-la-policia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Monseñor cuestionó de igual forma, la formación que **está recibiendo la Fuerza Pública enfocada en la represión como la solución más efectiva, lo que ha generado una pérdida en "la confianza en la autoridad y en la institucionalidad",** estimulando una respuesta negativa desde la ciudadanía, y aunque señala que no se debe responder a la violencia con más violencia, es necesario exigir al Estado que "se desarme de esa violencia mental, emocional, ideológica, autoritaria" [(Lea también: "La iglesia no puede guardar silencio frente a la situación del país")](https://archivo.contagioradio.com/la-iglesia-no-puede-guardar-silencio-frente-a-la-situacion-del-pais/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las comunidades proponen la paz

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la Arquidiocesis de Cali, una de las organizaciones que expresaron su apoyo al Pacto por la Vida y por la paz, firmado por 140 organizaciones del país, es una forma de anteponer la razón a la fuerza. Una propuesta que para las comunidades es una forma de responder a "las víctimas que sufren los estrago de actores armados y que también están sufriendo los impactos desproporcionados del abandono histórico"; así lo expresó el líder social Leyner Palacios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"El pacto es un grito que quiere eliminar la sordera que ha tenido el gobierno nacional y los actores armados en confrontación de no escucha a una salida negociada al conflicto armado y debe romper los tímpanos de los actores y las barreras que existen para la negociación"**, agregó.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicho problema, **monseñor lo atribuye a una pérdida de interlocución con los múltiples actores en el territorio lo que tiene consecuencias negativas directamente en los territorios,** "un país con estas masacres tendría que estar volcado con todos sus instrumentos a controlar y llegar a todas las poblaciones, ha faltado esa decisión de fondo que diga aquí hay un Estado y una sociedad que repudia esto".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Finalmente Monsalve señaló que en el contexto de la pandemia, es apremiante continuar los diálogos con grupos armados, aún más después del llamado a un cese al fuego global hecho por la ONU, sin embargo es enfático en que este debe empezar desde el Gobierno, uno que señala, únicamente reconoce la existencia del conflicto armado cuando es de su conveniencia. [(Lea también: Comunidades que han vivido la guerra en sus territorios respaldan la voz de Dario Monsalve)](https://archivo.contagioradio.com/comunidades-que-han-vivido-la-guerra-en-sus-territorios-respaldan-la-voz-de-dario-monsalve/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
