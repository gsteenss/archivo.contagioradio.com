Title: Persiste la presencia de paramilitares sobre el Río Tamboral en el Chocó
Date: 2017-03-14 16:47
Category: DDHH, Nacional
Tags: Amenazas, Chocó, indígenas, paramilitares
Slug: persiste-la-presencia-de-estructuras-neoparamilitares-sobre-el-rio-tamboral-en-el-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/documento-choco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [14 Mar. 2017] 

Integrantes de la comunidad indígena de Playa Bonita y La Dominga sobre el Río Tamboral, en el municipio de Bajo Baudó, departamento del Chocó, denunciaron que este sábado, observaron cómo **varios hombres, quienes se autodenominaron como “Autodefensas Gaitanistas de Colombia” (AGC) y que iban vestidos de camuflado, intimidaron a una persona** y “le ordenaron no denunciar los hechos”.

En la comunicación conocida por Contagio Radio, agregan que ante los atropellos cometidos por los paramilitares, éstos le dijeron al poblador que **“ya sabían que venían los militares y nada iba a pasar (…)  y que se iban a quedar, que ya habían arreglado todo”**. A pesar de portar insignias y brazaletes de las AGC, los armados le dijeron al indígena que eran de la guerrilla del ELN. Le puede interesar: [Paramilitares se toman caserío de Domingodó en el Chocó](https://archivo.contagioradio.com/paramilitares-se-toman-caserio-de-domingodo-en-el-choco/)

**Los integrantes de este mismo grupo fueron vistos durante varias horas de ese mismo día,** desde las 9 de la mañana hasta las 4 de la tarde en las comunidades indígenas en Playa Bonita, y hacia las 6 de la tarde se desplazaron hasta La Dominga. Le puede interesar: [Comunidades indígenas de Jiguamiando, Chocó asediadas por paramilitares](https://archivo.contagioradio.com/pparamilitares_jiguamiando_choco/)

**Para el día domingo, estas personas armadas fueron vistas en Urada** y según los testimonios de los pobladores se dirigieron hacia el caserío de Puerto Lleras. Le puede interesar: [Paramilitares extorsionan y amenazan a pobladores de Murindó, Chocó](https://archivo.contagioradio.com/paramilitares_amenazan_extorsionan_pobladores_choco/)

Las comunidades manifestaron que ya han puesto en conocimiento a las autoridades de estos hechos, sin haber recibido ninguna respuesta hasta el momento. Le puede interesar: [Paramilitares arremeten en diferentes regiones del país](https://archivo.contagioradio.com/paramilitares-arremeten-contra-diferentes-regiones-del-pais/)

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
