Title: Bogotá marchó contra la homofobia
Date: 2015-05-17 22:04
Author: CtgAdm
Category: LGBTI, Nacional
Tags: Batucada lesbica, Bogotá, Comunidad LGBTI, homofobia, marcha contra la homofobia
Slug: bogota-marcho-contra-la-homofobia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/DSC4401-800x534.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

En Bogotá, la comunidad LGBTI salió a las calles a manifestar de forma pacifica su rechazo a la homofobia, entre gritos y arengas y la compañia de la batucada "Toque Lésbico", la comunidad caminó desde el Planetario Distrital hasta la Plaza de Bolívar.

El recorrido de la marcha pasó frente a la Procuraduría General donde con gritos los asistentes criticaron  el papel del  Procurador Alejandro Ordoñez contra la comunidad LGBTI, también hicieron un minuto de silencio por las víctimas de la homofobia en Colombia. Entre los nombres de las víctimas se recordaba el caso del [estudiante Sergio Urrego](https://archivo.contagioradio.com/continua-imputacion-de-cargos-por-caso-sergio-urrego/) del  gimnasio ¨El Castillo campestre¨

Cada 17 de mayo en el mundo se celebra el día internacional contra la homofobia, éste se instauró luego que la Asamblea General de la Organizaciones Mundial de la Salud eliminara la homosexualidad de la lista de enfermedades mentales.

\
