Title: Fuerte respuesta de jóvenes becados en Cuba a María Fernanda Cabal
Date: 2020-02-14 15:41
Author: CtgAdm
Category: Estudiantes, Política
Tags: Cuba, Educación Superior, Implementación del Acuerdo, Medicina
Slug: fuerte-respuesta-de-jovenes-becados-en-cuba-a-maria-fernanda-cabal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/WhatsApp-Image-2020-02-14-at-11.34.47-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto Cuba: Cortesía

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través de una carta, **600 estudiantes favorecidos con las 1.000 becas ofrecidas por Cuba para estudiar medicina,** como un aporte a la construcción de la paz, respondieron a los señalamientos de la senadora María Fernanda Cabal quien se pronunció en contra de esta oportunidad ofrecida por el país garante justificando un no cumplimiento del Acuerdo. Sin embargo ha sido la misma implementación la que ha permitido el acceso a la educación superior a colombianos que en su propio país no podrían hacerlo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La senadora del Centro Democrático expresó que el otorgamiento de 1.000 becas, incluidos excombatientes de FARC que se encuentran en proceso de reincorporación es una "actitud de peligrosa ingenuidad" apelando a un incumplimiento por parte de los firmantes de la paz y refiriéndose de forma negativa hacia el acuerdo. [(Lea también: No se puede menosprecia el rol de Cuba en los procesos de paz del mundo)](https://archivo.contagioradio.com/no-se-puede-menosprecia-el-rol-de-cuba-en-los-procesos-de-paz-del-mundo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En respuesta, los estudiantes resaltaron que el cumplimiento por parte de los firmantes de paz ha sido evidenciado y que esta oportunidad ofrecida por Cuba hace parte de las contribuciones realizadas a la implementación del Acuerdo, beneficiando no solo a excombatientes, sino en general a colombianos que fueron desplazados y afectados por la guerra.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Es decir, en la [Escuela Latino Americana de Medicina](https://instituciones.sld.cu/elam/), **son en la actualidad 38 excombatientes, 21 familiares de miembros de la Fuerza Pública y 520 jóvenes  colombianos de diversas comunidades**, organizaciones, indígenas y afrodescendientes que señalan, deseaban cursar la carrera de medicina en Colombia sin embargo resulta "inasequible para los pobres, como lo es la educación superior". [(Lea tambié: Académicos del mundo piden a la ONU exigir cumplimiento de protocolos con el ELN)](https://archivo.contagioradio.com/academicos-del-mundo-piden-a-la-onu-exigir-cumplimiento-de-protocolos-con-el-eln/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Las becas son para poder construir paz en nuestro país"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Adriana Guerrero, integrante del consejo estudiantil de Colombia en Escuela Latinoamericana de Medicina afirma que la beca cubre su vivienda, uniformes, alimentación, libros, transportes, salud, "derechos fundamentales de todo ser humano que en Colombia son negados" .

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que con estas acusaciones que hace la senadora Cabal expone a una generación consciente de la necesidad de la implementación del Acuerdo de paz, "gracias a los acuerdos de paz estamos estudiando medicina, aquí estudiamos excombatientes, hijos e hijas, familiares de militares y jamás existe un problema entre nosotros, no interfiere para nada nuestra procedencia".

<!-- /wp:paragraph -->

<!-- wp:quote -->

>   Todas y todos los que nos encontramos acá convivimos fraternalmente sin distinción de nuestra procedencia, compartimos aula y sueños de una Colombia mejor.

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

La estudiante destaca el intercambio cultural que se vive en la academia y cómo el ver a colombianos afectados por la guerra, hoy estudiando y desarrollando fraternidad resulta conmovedor, "nunca más los jóvenes para la guerra, ¿cuántas generaciones se han perdido en nuestro país por el conflicto?".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Estudiar medicina en Cuba frente al reto de hacerlo en Colombia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Estudiar medicina en Colombia es un desafío, actualmente en las universidades publicas se presentan más de 1.000 estudiantes, de los cuales en promedio solo son admitidos 90, mientras el costo de los estudios de las **Universidades Privadas oscilan entre los \$14.100. 000 y los \$ 25.568.000 millones por semestre.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante esta realidad, es evidente que existe un muro para poder acceder a la educación superior en Colombia, en particular hacia la medicina, carrera que en Cuba, goza de un gran prestigio, siendo el primer país en erradicar la transmisión del VIH de madre a hijo; desarrollar una vacuna para prevenir el cáncer de pulmón y ofrecer desde 2006 un tratamiento para úlceras diabéticas entre otros aportes a la humanidad.

<!-- /wp:paragraph -->

<!-- wp:audio -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/s_p2_168710_1.html">
</audio>
</figure>
<!-- /wp:audio -->

<!-- wp:core-embed/scribd {"url":"https://www.scribd.com/document/447081047/Respuesta-de-jovenes-becados-en-Cuba-a-Maria-Fernanda-Cabal-febrero-2020","type":"rich","providerNameSlug":"scribd","className":"wp-embed-aspect-9-16 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-scribd wp-block-embed is-type-rich is-provider-scribd wp-embed-aspect-9-16 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.scribd.com/document/447081047/Respuesta-de-jovenes-becados-en-Cuba-a-Maria-Fernanda-Cabal-febrero-2020

</div>

</figure>
<!-- /wp:core-embed/scribd -->

<!-- wp:block {"ref":78955} /-->
