Title: Las tres modificaciones de la Corte Constitucional a la sentencia contra Cerro Matoso
Date: 2018-09-25 16:13
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Cerro Matoso, cordoba, Corte Constitucional
Slug: las-tres-modificaciones-de-la-corte-constitucional-a-la-sentencia-contra-cerro-matoso
Status: published

###### [Foto: La Razón] 

###### [25 sept 2018] 

Son tres grandes modificaciones que hace la Corte Constitucional a la sentencia sobre la actividad de Cerro Matoso, en Córdoba: **anula la indemnización que la empresa tendría que hacer a las víctimas por más de 400 millones de dólares**, tumba la creación del fondo especial de etnodesarrollo y ordena que se inicie un tramite nuevo de licenciamiento ambiental.

### **Las víctimas de Cerro Matoso** 

Las comunidades que denuncian haber sido afectadas por Cerro Matoso son Torno Rojo, Bocas de Uré, Puerto Colombia, Unión Matoso en Puerto Flecha, Guacarí en la Odisea, Centro América y Puente Uré y el Consejo Comunitario de las Comunidades Negras de San José de Uré; además se ha calculado que podrían haber más de** 3.000 personas afectadas**, en el departamento de Córdoba.

En esa medida, la sentencia T-733 del año pasado obligaba a la empresa a dar una  indemnización por 400 millones de dólares a las víctimas. Sin embargo en un nuevo fallo, la Corte **considera que su jurisprudencia no es generar indemnizaciones a partir del mecanismo de la tutela**.

Álvaro Pardo, economista y ambientalista quien ha denunciado los abusos de Cerro Matoso sobre el territorio y la comunidad, afirma que el hecho de que no se pague la multa atenta contra una acción mínima de reparación "esto es apenas lo razonable para restablecer años de sufrimiento".

Referente a la creación de Fondo de Etnodesarrollo, que buscaba una reparación desde una perspectiva colectiva y étnica, **la Corte Constitucional argumenta que en el anterior fallo esta medida fue genérica por tal razón la elimina. **(Le puede interesar:["Cerro Matoso tendrá que reparar a comunidades por vulneración a derechos humanos y ambientales"](https://archivo.contagioradio.com/cerro-matoso-tendra-que-reparar-a-comunidades-por-vulneracion-a-derecho-humanos-y-ambientales/))

### **La reparación Ambiental** 

Otra de las modificaciones que ordena el nuevo fallo, tiene que ver con una solicitud de licenciamiento ambiental por parte de Cerro Matoso, ya que la empresa venía ejecutando labores en el territorio con un contrato de aporte antiguo, razón por la cual **sus permisos ambientales no cumplen con los nuevos requerimientos ante la ley para obtener la licencia ambiental.**

Asimismo, la Corte indica que Cerro Matoso deberá realizar procesos de consulta previa para saber si las comunidades quieren o no que este proyecto de explotación de níquel continúe con esa labor en el territorio.

Para Pardo, esta nueva orden de la Corte Constitucional hace parte de una serie de medidas que evidencian que no es garante de los **derechos de los ciudadanos y mucho menos de las personas más débiles y que podría echar para atrás lo alcanzado en materia de derechos fundamentales**.

"Hoy una compañía sigue operando, puede causar los problemas que cause y la Corte Constitucional le puede eliminar cualquier obligación que tenga de reparar a las víctimas. Pueden hacer lo que quieran estas compañías y este es un muy mal precedente para el tema de salud, ambiental de las comunidades que viven en las áreas de influencia de las compañías mineras y de hidrocarburos" asevera Pardo.

<iframe id="audio_28866031" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28866031_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
