Title: Plan Nacional de Desarrollo tiene un déficit de $58 billones  
Date: 2015-02-04 20:04
Author: CtgAdm
Category: Economía, Política
Tags: colombia, economia, Gasto público, Plan Nacional de Desarrollo
Slug: plan-nacional-de-desarrollo-tiene-un-deficit-de-58-billones
Status: published

##### Foto: [blog.chlewey.net]

<iframe src="http://www.ivoox.com/player_ek_4038936_2_1.html?data=lZWgmp6Xeo6ZmKiak5uJd6Kpk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMrWwtfR0ZC3pdPhysrb1tSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Libardo Sarmiento] 

El Plan Nacional de Desarrollo (PND) 2014-2018, ‘Todos por un nuevo país’, necesita 790 billones de pesos para ser financiado, pero de acuerdo a Libardo Sarmiento, analista económico de Le Monde Diplomatique en Colombia, **hay un déficit de 58 billones de pesos, lo que equivale a que el Gobierno deberia hacer cuatro reformas tributarias, una por año.**

Pese a que según el gobierno, el PND se basa en la paz, la equidad y la educación, lo cierto es que **los mayores gastos para el país son utilizados en pago de la deuda externa y la nómina y mantenimiento de las  fuerzas militares,** lo que genera que el monto de inversión sea mínimo.

Por un lado, 1/4 del presupuesto va dirigido al pago de la deuda externa. Por otro lado, 2/4 del presupuesto nacional van dirigidos a la burocracia y el gasto militar, teniendo en cuenta que de **2 millones de empleados estatales en Colombia, 600 mil son militares.** Además, según Sarmiento, en los últimos 20 años el gobierno subió el **gasto militar del 4% al 9%; de ahí que únicamente quede el 26% para inversión.**

Uno de los mayores problemas, es que el gobierno proyectó una inversión para cuatro años sobre la base de un precio del **barril de petróleo cercano a los US\$90 por barril, pero los precios del petróleo internacional están bajo los US\$ 50,** es decir, hay un déficit de \$58 billones para financiar el PND.

Sarmiento, afirma que **el gobierno no está mirando detalladamente los problemas estructurales**, por ejemplo, una estrategia para solucionar los problemas de financiación del PND, de acuerdo al analista, sería **reducir el gasto militar**; evaluar la reforma **tributaria, debido a que las anteriores han generado grandes efectos negativos en la clase media del país**; y fortalecer la **política de exportaciones,** debido a que el país está importando todo y en cambio, las exportaciones van en déficit.

Este miércoles el Plan Nacional de Desarrollo pasa a evaluación por parte del Consejo Nacional de Política Económica y Social, y el viernes será radicado ante el Congreso de la República para que las bancadas hagan sus ajustes, de acuerdo a sus intereses regionales, lo que le aumentará problemas a los rubros de inversión.

 
