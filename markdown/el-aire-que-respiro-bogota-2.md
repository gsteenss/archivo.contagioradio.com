Title: Con documental ciudadanos exponen su preocupación por calidad del aire en Bogotá
Date: 2018-02-15 13:32
Category: Ambiente, eventos
Tags: Bogotá, calidad aire, Documental
Slug: el-aire-que-respiro-bogota-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/DOCUMENTAL-6_0-e1544725462844.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mesa Técnica por la calidad del aire 

###### 15 Feb 2019 

La preocupación por la baja calidad del aire en Bogotá, ha motivado a ciudadanos comunes a **emprender acciones para visibilizar las afectaciones producen en la salud y el ambiente de la capital**, lo que contrarresta con decisiones como la tomada por la administración de mantener vehículos públicos que funcionan a partir de diesel.

Una de esas iniciativas es la producción del documental **"El Aire que Respiro, Caso Bogotá"**, una pieza audiovisual realizada por la Mesa Técnica Ciudadana por la calidad del aire de la capital, producciones EnAction y Studio 93 que **se estrenó oficialmente este jueves 13 de diciembre**.

El largometraje, **producido con el apoyo de la Fundación Heinrich Boll Colombia** busca sensibilizar a los espectadores sobre la situación actual de la ciudad y los factores que inciden en el deterioro de la calidad del aire; **exponiendo simultáneamente el trabajo que la ciudadanía realiza por garantizar su derecho a un ambiente sano y aun aire limpio.**

<iframe src="https://www.youtube.com/embed/v_gxCdbYT0E" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
