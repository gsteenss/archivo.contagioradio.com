Title: Policía sigue actuando “de manera criminal” en Tumaco
Date: 2017-10-10 12:42
Category: DDHH, Nacional
Tags: Fuerza Pública, Masacre de Tumaco
Slug: policia-sigue-actuando-de-manera-criminal-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/14.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 Oct 2017] 

La actuación de la policía en contra de las comunidades que se oponen a la erradicación forzada de cultivos de uso ilícito en la zona rural de Tumaco no solamente se limitó a disparar a la multitud. Gracias a la labor periodística y la Misión Humanitaria del domingo, se han conocido más detalles de una actuación calificada como **criminal por parte de varios sectores sociales que urgen por que se conozca toda la versas y se haga justicia por esta masacre**.

Durante el corto recorrido de la Misión Humanitaria que se encuentra en terreno desde el domingo **se pudieron observar dos comportamientos, por lo menos irregularidades, por parte de la fuerza pública** que permanece en el sitio y que ha construido una base, justo en el sitio en que se perpetró la masacre.

### **Alteración de la escena del Crimen** 

Según la información de las personas que participaron en la Misión se ha podido observar que varios árboles permanecen con las marcas de las balas, en los troncos se pueden observar las trayectorias de los proyectiles. Sin embargo, se verificó que **hay muchos árboles que han sido derribados tratando de ocultar las pruebas** que serían contundentes para establecer lo que sucedió y judicializar a los responsables.

La misión también observó que en varios de los sitios en los que se presentó la masacre la tierra ha sido removida, **incluso se han construido trochas falsas para simular las que los campesinos han utilizado y utilizaron el 5 de Octubre**. (Le puede interesar: ["Entre los policías que atacaron la Misión Humanitaria "estaban los que nos mataron""](https://archivo.contagioradio.com/los-policias-que-nos-mataron-tumaco/))

Además el vice defensor del Pueblo realizó un recorrido en el que no observó ninguno de los supuestos cráteres que habrían dejado los cilindros bomba. Extrañamente en algunos medios de información se han compartido imágenes de **un cráter que habría sido producto de estas explosiones**. Sin embargo este cráter no estaba en el momento de la visita del vice defensor del Pueblo.

### **Invasión y destrucción de propiedades civiles** 

Uno de los testimonios recogidos por la comunidad y la Misión Humanitaria da cuenta de una actuación criminal por parte de la policía antinarcóticos el 5 de Octubre. Uno de los testigos señala que un policía remató a un campesino que le pidió ayuda desde el suelo. Lo que pudieron observar es que **sin mediar palabra el policía que llegó hasta el sitio y le propino otro disparo al herido.**

Por otra parte, el 5 de Octubre la familia de uno de los habitantes de la región tuvo la mala suerte de tener su vivienda cerca del sitio de la masacre. Según la denuncia del habitante la policía llegó y prácticamente destruyó las pertenencias de las 4 personas que habitaban la casa, regaron el mercado y rasgaron las prendas de vestir. De la casa se apropió la policía que hasta el momento, ha impedido que sus dueños regresen a sacar lo poco que les queda **“llevo 4 días con la misma ropa”** resaltó en una entrevista el padre de familia.

### **Policía sigue amenazando a campesinos** 

Otra de las denuncias tiene que ver con que los policías, muchos de los que estuvieron el 5 de Octubre en la masacre, permanecen en el sitio de los hechos. En los videos de la Misión Humanitaria **se nota como varios de los policías son señalados por los campesinos como “ustedes fueron los que nos mataron”**. (Le puede interesar: ["Informe forense contradice versión del gobierno en masacre de Tumaco"](https://archivo.contagioradio.com/informe-forense-contradice-version-del-gobierno-en-masacre-de-tumaco/))

Pero la situación no ara allí, varias de las familias que habitan el sector han denunciado que la policía antinarcóticos les ha impedido el tránsito por algunos de los caminos, incluso **ha prohibido el paso a las familias que tienen sus viviendas cerca**. Además han amenazado con el uso de las armas contra quienes se opongan a las restricciones que están imponiendo.

###### Reciba toda la información de Contagio Radio en [[su correo]
