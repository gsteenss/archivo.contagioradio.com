Title: Vivimos la guerra, construimos la paz: La historia de lucha por los derechos humanos de las mujeres rurales colombianas
Date: 2020-03-05 14:55
Author: AdminContagio
Category: yoreporto
Tags: Defensa de derechos de las mujeres, derechos de las mujeres, Derechos Humanos, mujeres, mujeres rurales, Mujeres y Paz
Slug: la-historia-de-lucha-por-los-derechos-humanos-de-las-mujeres-rurales-colombianas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/PODCAST-VERSIÓN-FINAL-CONTAGIO-RADIO.mp3" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/mujercampo.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":6} -->

###### por: Melissa Silva Franco / Marta Saiz

<!-- /wp:heading -->

<!-- wp:paragraph -->

La construcción de paz en Colombia es un camino largo, dificultoso y exigente. Un camino que se traza desde hace más de 50 años y por el que han andado, y siguen andando miles de mujeres rurales que se organizan, tanto para esta lucha, como para el reconocimiento de sus derechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Norma Villarreal y Leonora Castaño** son férreas defensoras de los derechos de las mujeres rurales colombianas. Villarreal desde la institucionalidad y la academia, y Castaño desde el terreno, fueron impulsoras de **ANMUCIC** (Asociación Nacional de Mujeres Campesinas, Negras e Indígenas de Colombia), cuyo trabajo en la tenencia de la tierra y los derechos de la mujer rural fueron, y siguen siendo clave a la hora del empoderamiento y respeto de la exigencia de las mujeres campesinas. Esta labor llevó a Castaño a ser la presidenta de la organización hasta 2003, año en el que las amenazas contra su vida conllevaron al exilio de ésta y su familia a la ciudad española de Alicante. Desde allí continúa con la defensa de los derechos humanos y de las mujeres desde la Colectiva de Mujeres Refugiadas, Exiliadas y Migradas, y el Foro Internacional de Víctimas.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Un exilio del que la lideresa no ha podido retornar.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Diecisiete años en los que la situación de la organización y las condiciones no han sido favorables para su regreso. Años dónde ha sufrido las secuelas del exilio. “Los y las exiliadas pasamos mucho tiempo en los países que nos acogen… con el cuerpo aquí, pero con la mente allá”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero la distancia física no ha sido un impedimento para que Castaño, así como el resto de las víctimas exiliadas, siguieran con el trabajo político e hicieran llegar sus propuestas a la mesa de diálogo de los acuerdos de paz en La Habana. Especialmente de la Colectiva de Mujeres a la subcomisión de género. Propuestas que para Villarreal son el cúmulo de las exigencias de las mujeres rurales y de organizaciones como **ANMUCIC**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, a pesar de los avances de la subcomisión de género, de estos 130 compromisos con perspectiva de género logrados en los acuerdos, **el 42% todavía no han comenzado a implementarse**. Y así ocurre con todo el acuerdo de paz. Este incumplimiento por parte del gobierno de Iván Duque, sumado al asesinato de más de 700 defensores y defensoras desde 2016, ha provocado las protestas a nivel nacional del movimiento social, indígena, estudiantil y de mujeres. Hecho que para Castaño es muy reparador, pues es ver como la sociedad colombiana se está despertando. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero también, esta situación de vulneración a los derechos humanos en Colombia hace que el exilio vuelva a las cifras históricas de los años más duros del conflicto armado. El [Ministerio del Interior](http://www.interior.gob.es/documents/642012/9911687/Nota_avance_mensual_datos_proteccion_internacional_2019_12_31.pdf/2cf48c9d-6378-4899-91dc-c7281c04dd46) español y la [Comisión Española de Ayuda al Refugiado (CEAR)](http://masquecifras.org/) apuntan que en 2019 ha habido 29.363 solicitudes de asilo por parte de la población colombiana. Sin embargo, el Estado español considera que Colombia es un país seguro. Solo 10 solicitudes fueron aprobadas por razones humanitarias. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y dentro de las amenazas al acuerdo de paz, Villarreal afirma que todavía hay mujeres a las que su labor política las pone en peligro. Por ello, la socióloga concluye que Colombia no caminará hacia la paz hasta que haya un profundo reconocimiento y respeto por los derechos de las mujeres colombianas y su trabajo, especialmente, de las mujeres rurales.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este podcast forma parte del proyecto *Vivimos la guerra, construimos la paz*, de la Asociación Acción Internacional por la Paz / **IAP** Catalunya, que ha recibido el apoyo de la Beca DevReporter 2019, impulsada con la financiación del proyecto *Frame, Voice, Report!*, de la Unión Europea, la Agencia Catalana de Cooperación al Desarrollo (**ACCD**) y el Ayuntamiento de Barcelona.

<!-- /wp:paragraph -->

<!-- wp:audio {"id":81696} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/PODCAST-VERSIÓN-FINAL-CONTAGIO-RADIO.mp3">
</audio>
  

<figcaption>
\*El contenido de este podcast es responsabilidad de sus autoras y no refleja necesariamente la posición de la Unión Europea.

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

[Le puede interesar: Yo Reporto](https://archivo.contagioradio.com/?s=yo+reporto)

<!-- /wp:paragraph -->
