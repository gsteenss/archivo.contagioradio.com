Title: Construcción de carretera daña reserva en Sierra Nevada de Santa Marta
Date: 2015-04-09 14:49
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, Corpamag, Corporación Autónoma Regional del Magdalena, especies endémicas, Finca La Victoria, Michael Ernest Erwin Weber Oswald, Proaves, reserva natural de las Aves El Dorado, Santa Marta, Sierra Nevada de Santa Marta
Slug: construccion-ilegal-de-carretera-dana-reserva-de-la-sierra-nevada-de-santa-marta
Status: published

##### Foto: Fundación Proaves 

<iframe src="http://www.ivoox.com/player_ek_4325113_2_1.html?data=lZifl5aVd46ZmKiak5eJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRdpGfxtjdx8jNqdSfxtPRh6iXaaKtzs7Qw9iPqMafwtvS1ZDIqYzgwpDAy8rWtsKfr8rjw8nFb8XZjLiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ivonne Alzate, Fundación Proaves] 

El pasado 30 de marzo, se trasladaron más de cuarenta personas de la fundación ProAves y de la comunidad residente de la Sierra Nevada de Santa Marta, con el objetivo de **detener tres máquinas excavadoras** con las que se está abriendo paso a una carretera de aproximadamente **8 kilómetros de extensión**, que atraviesa la reserva de la Fundación Proaves cuenta con **la reserva natural de las Aves El Dorado.**

De acuerdo a Ivonne Alzate, integrante de Proaves, el **daño que se le hace al ecosistema con esta construcción es “muy grave”**, debido a que la reserva natural por la que pasa la carretera,  fue creada para conservar uno de los sitios más importantes de Colombia donde habita un gran número de especies animales y vegetales únicas.

Según Alzate, se está poniendo en **peligro** **veinte especies endémicas de aves,** además se vería afectada la **cuenca del río Gaira,** ubicado en la Cuchilla de San Lorenzo, en una zona que corresponde a una transición de bosques muy húmedos subtropical y bosques húmedos montañosos bajos.

**Proaves, había realizado la denuncia ante la Corporación Autónoma Regional del Magdalena,** Corpamag, el pasado 5 de junio del 2014, cuando señalaron como causante del daño ambiental, al ciudadano alemán Michael Ernest Erwin Weber Oswald, quien al parecer habría obtenido un permiso para talar únicamente cinco árboles y no una extensión tan grande como para abrir ocho kilómetros de carretera, para la **venta ilegal de lotes para casas vacacionales.**

Corpamag, anunció que no se había dado ningún permiso al extranjero para construir vías en la Finca La Victoria, en la Sierra Nevada de Santa Marta. Es por eso que en este momento **hay un proceso ante la Fiscalía y los demás entes ambientales,** para evitar que nuevamente las máquinas regresen. “La idea es que la corporación realice una sanción para que no vuelva a suceder algo así debido a la importancia biológica y ecológico de la Sierra”.

Proaves hace un llamado a la comunidad samaria, para estar atentos al sonido de las retroexcavadoras y no permitir que nuevamente avancen, pues como lo afirma Ivonne Alzate, “**la Sierra Nevada no tiene valor económico”**.

[![carretera3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/carretera3-1024x768.jpg){.wp-image-6901 .size-large .aligncenter width="604" height="453"}](https://archivo.contagioradio.com/construccion-ilegal-de-carretera-dana-reserva-de-la-sierra-nevada-de-santa-marta/carretera3/)

[![carretera](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/carretera-1024x768.jpg){.wp-image-6899 .size-large .aligncenter width="604" height="453"}](https://archivo.contagioradio.com/construccion-ilegal-de-carretera-dana-reserva-de-la-sierra-nevada-de-santa-marta/carretera/)

[![carretera2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/carretera2-1024x768.jpg){.wp-image-6900 .size-large .aligncenter width="604" height="453"}](https://archivo.contagioradio.com/construccion-ilegal-de-carretera-dana-reserva-de-la-sierra-nevada-de-santa-marta/carretera2/)
