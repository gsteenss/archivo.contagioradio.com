Title: Giovany Oliveros concejal de Cunday, Tolima,  es asesinado
Date: 2020-02-12 14:43
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato, Cunday, Giovany Oliveros, líder político, Tolima
Slug: giovany-oliveros-concejal-de-cunday-tolima-es-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Diseño-sin-título-9.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Archivo

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En horas de la noche del martes 11 de febrero en el municipio de Cunday Tolima fue asesinado el concejal Giovany Oliveros Mayorga, según testigos, los responsables de este hecho son hombres armados que intentaron robar su vivienda. (Le puede interesar: <https://archivo.contagioradio.com/es-declarada-crisis-humanitaria-en-tres-regiones-del-pais/>)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Ricardo_Orozv/status/1227424201977552896","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Ricardo\_Orozv/status/1227424201977552896

</div>

<figcaption>
*Twitter: Ricardo Orozco -Gobernador del Departamento del Tolima*

</figcaption>
</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Martín Cadena, allegado al Concejal, señaló que los hechos ocurrieron luego de que el funcionario saliera de una reunión en la Contraloría y llegara a su casa, *"mientras el se bañaba dos hombres armados ingresaron cerca de las 9:00 pm a su vivienda, donde tiene una panadería, los hombres entraron a robar seguramente el producido del día"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aseguró también que le quitaron el celular a él y a su esposa, mientras lo despojaban del dinero, *"en medio del robo uno de estos hombres disparó contra Giovany entres tres oportunidades, uno pegó al aire, otro en la pierna y el otro en el pecho, ese fue el mas grave".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Su esposa resulto ilesa e inmediatamente los agresores huyeron buscó ayuda en sus vecinos, quienes transportaron a Oliveros en un vehículo particular, *"los vecinos lo montaron a la camioneta, pero la herida del pecho era muy grave, llegó casi sin signos vitales al hospital, por eso ahí no pudieron hacer mucho para salvarlo"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Concejal Giovany Oliveros y su apuesta por el deporte

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Cadena, Giovany tenía casi 50 años de edad, dos hijas y un nieto; había dedicado su vida al oficio de la panadería hasta que decidió incursionar en la política, *"se metió en la política debido a su interés por lo jóvenes deportistas en Cunday, y así promover los escenarios deportivos"*, por último afirmó, *"no es de extrañar que al conocer la noticia de su muerte la comunidad se acercara al hospital exigiendo claridad sobre los hechos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En las últimas horas el partido Alianza Verde del cual hacia parte el Concejal Oliveros, a través de sus redes sociales manifestó: *"Hacemos un llamado al Gobierno nacional, departamental y local para que garanticen la seguridad de todos nuestros líderes y lideresas sociales"*. Le puede interesar: ( <https://www.justiciaypazcolombia.com/incursion-paramilitar-de-las-agc-en-nuevo-canaveral/> )

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte el alcalde de Cunday Luis Gabriel Pérez afirmó, *"no hay razón para pensar que esto se deba a una persecución política, de hecho, en Cunday estos actos no son frecuentes hace mucho no pasaba nada así, tenemos indice de cero violencia hace más de 2 años"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que ante el asesinato del Concejal, se han activado todos los protocolos de seguridad, *"todos los concejales están dentro del Plan Padrino, donde un agente de policía verifica que se cumplan los protocolos de seguridad"*, adicional señaló que desde la Alcaldía y la Policía han garantizado seguridad a los miembros de la familia Oliveros, así como también a los miembros de la comunidad.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
