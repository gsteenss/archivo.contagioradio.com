Title: Otra Mirada: Procuraduría general ¿otro fortín político?
Date: 2020-08-14 19:58
Author: AdminContagio
Category: Nacional, Otra Mirada, Programas
Tags: Procurador, Procuraduría General de la Nación
Slug: otra-mirada-procuraduria-general-otro-fortin-politico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/procuraduría.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @PGN\_COL

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pronto se hará la elección de quien reemplazará a Fernando Carrillo y será el nuevo procurador General de la Nación durante los próximos cuatro años. Este martes fue ternado Juan Carlos Cortes y una vez se presenten los otros candidatos, el Senado deberá elegir al próximo procurador o procuradora.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Quien quede a cargo de la procuraduría debe garantizar la vida y los derechos de las comunidades, sin embargo, preocupa cómo estos cargos podrían estar permeados por conveniencias o por intereses políticos y económicos. (Le puede interesar: [Defensoría del Pueblo será repartida como cuota política del gobierno Duque](https://archivo.contagioradio.com/defensoria-del-pueblo-cuota-politica/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Haciendo un análisis de lo que se puede esperar estuvieron Diana Sánchez, vocera de la CCEEU y Asociación Minga, Ana María Rodríguez, sub directora de la Comisión Colombiana de Juristas y Daniela Gómez, coordinadora de Democracia y Gobernabilidad de la Fundación Paz y Reconciliación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las invitadas explican cómo ha estado funcionando la Procuraduría con Fernando Carillo en frente y de esta manera, qué se está esperando de las nuevas elecciones y si los candidatos cumplen con el perfil que todo procurador debe tener.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Abordan el tema de la equidad de género, **la falsa integración de las mujeres en estos modelos de elección** y hacen propuestas de cambio para ser más inclusivos no solo con las mujeres sino también con los diferentes sectores del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez, agregan cuáles son los riesgos en materia de derechos humanos al nombrar a personas que tienen cargos tan vitales como el de procurador y que no responden a los intereses de los colombianos sino a intereses políticos o económicos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, comentan cómo va la veeduría ciudadana y hacen un llamado para que quienes están a cargo de elegir al nuevo o nueva procurador sean conscientes de la decisión, teniendo en mente la función que debe cumplir el mismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el programa del 11 de agosto: [Otra Mirada: El legado de Ángela Salazar. ](https://www.facebook.com/contagioradio/videos/302878174479596) 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el análisis completo: [Haga click aquí](https://www.facebook.com/watch/live/?v=297309114909299&ref=notif&notif_id=1597251655496944&notif_t=live_video_explicit)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
