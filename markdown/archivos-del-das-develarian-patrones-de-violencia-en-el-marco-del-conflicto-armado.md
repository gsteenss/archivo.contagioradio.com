Title: Archivos del DAS develarían patrones de violencia en el marco del conflicto armado
Date: 2018-11-07 15:59
Author: AdminContagio
Category: DDHH, Nacional
Tags: comision de la verdad, crímenes de estado, JEP
Slug: archivos-del-das-develarian-patrones-de-violencia-en-el-marco-del-conflicto-armado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Paramilitarismo1-e1459970325314.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [07 Nov 2018] 

El director de la Comisión para el Esclarecimiento de la Verdad, Francisco de Roux señaló que la acción por parte de la Jurisdicción Especial para la Paz, de preservar los archivos del DAS, ha sido recibida con agrado y servirá para **"leer la verdad histórica, la verdad social y la verdad humana"** de lo que sucedió durante el conflicto armado en Colombia.

Asimismo, afirmó que con estos archivos, en los que se contiene información principalmente del accionar del Estado, se buscará encontrar patrones, es decir la combinación de variables que estuvieron detrás de cada una de las grandes violaciones de derechos humanos. (Le puede interesar:["JEP preservará los archivos del DAS](https://archivo.contagioradio.com/jep-preservara-los-archivos-del-das/)")

"Muy posiblemente serán patrones regionales, diferentes de acuerdo a los momentos del tiempo" afirmó de Roux, quien explicó que entre algunas de estas variables, se podrán encontrar elementos políticos, institucionales o culturales que conforman un comportamiento de **actores identificados y que culminan en hechos como las masacres, los desplazamientos masivos y las desapariciones forzadas**.

Con la recopilación de esta información, el director de la Comisión de la Verdad, aseguró que será posible dar el paso a la no repetición en Colombia y analizar "cómo se van a desmontar esos patrones y a proponer unos humanitarios, de verdadera democracia y reconciliación".

<iframe id="audio_29926988" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29926988_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
