Title: Este Cese Bilateral es como "volver a nacer": Carlos Lozano
Date: 2016-06-22 15:28
Category: Nacional, Paz
Tags: Cese al fuego bilateral, Mesa de conversaciones de paz de la habana, proceso de paz Colombia
Slug: establecer-una-fecha-para-la-firma-puede-generar-frustraciones-carlos-lozano
Status: published

###### [Foto: Archivo] 

###### [22 Junio 2016 ] 

Carlos Lozano Guillén, director del Semanario Voz, califica como histórico y trascendental el anuncio hecho desde La Habana, sobre la firma del cese al fuego bilateral y definitivo. "Éste es el comienzo del fin del conflicto armado en Colombia", se evidencia la voluntad decida de las FARC-EP y del Gobierno, se demuestra que sí es posible llegar a un acuerdo por la vía del diálogo; sin embargo, **establecer una fecha no es lo más acertado, puede generar frustraciones e incomprensiones** de sectores de la opinión pública, agrega Lozano.

"Para quienes nacimos en la guerra este anuncio es como volver a nacer", sí se tiene en cuenta que se vivieron décadas de intolerancia y sectarismo, en las que sectores como la iglesia católica tuvieron gran responsabilidad. "Por supuesto que la guerrilla va abandonar la vida armada para hacer tránsito a la lucha política, pero **el Estado debe cambiar sus métodos y adecuarse a las nuevas condiciones democráticas** que demandará el posconflicto, deberá aceptar la oposición, el cuestionamiento al statu quo por las vías constitucionales y abandonar el ejercicio del poder mediante la violencia", afirma Lozano.

Lozano asevera que éste es un acuerdo que compromete a todas las partes, "pese a que la oligarquía se ha acostumbrado a la unilateralidad y el odio"; ahora, uno de los grandes problemas es la reconciliación que será más difícil que la implementación, pues exige que haya **un ejercicio ciudadano de verificación para que los acuerdos sean implementados conforme fueron acordados**.

"Tiene que acabarse con la venganza y con la retaliación", asegura Lozano e insiste en que las víctimas nos dan una esperanza de vida, construyendo la paz sobre otras bases y otros sentimientos y de allí la necesidad de que **cada quién asuma su responsabilidad de forma autocrítica como garantía de no repetición**, pues "sí vamos a seguir con una clase dominante que cree que nunca se equivocó, que los demás fueron los equivocados, que todos tienen que arrepentirse menos ellos, así no va a funcionar esto, aquí tiene que haber la responsabilidad histórica de quienes detentaron el poder a lo largo de la historia".

"Lo que nos está enseñando el proceso de La Habana es que con persistencia, con voluntad política, pese a las debilidades, a las ambigüedades, a los problemas, a los inconvenientes que se presentaron, se puede llegar a un buen logro, **en esa misma dirección se puede lograr con el ELN**, por eso es tan desafortunado lo que [[está ocurriendo con Carlos Velandía", concluye Lozano](https://archivo.contagioradio.com/proceso-contra-carlos-velandia-entra-en-etapa-de-juicio-afectando-su-derecho-a-la-defensa/)].

<iframe id="audio_11993375" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11993375_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
