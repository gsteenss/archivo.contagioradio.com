Title: Habitantes de Bogotá si tienen propuestas de ciudad
Date: 2015-06-24 16:02
Category: Ambiente, eventos
Tags: Alcaldía, atanasova, Bogotá, ciudad, cumbre, donka, san juan de dios, urbano
Slug: habitantes-de-bogota-si-tienen-propuestas-de-ciudad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pulzo 

#####  

<iframe src="http://www.ivoox.com/player_ek_4683598_2_1.html?data=lZullZqdfI6ZmKiakpyJd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjMLWytnO0NnJt4zYxpCv0czTuIa3lIquk5DXrYzoysrbx5DUttDk1srg1sbXb8XZjMjW18nFqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Donka Atanasova] 

###### [24 jun 2015] 

Organizaciones urbanas y barriales de Bogotá se reunirán durante el viernes 26 y sábado 27 de junio en la Cumbre Urbana y Popular, para compartir experiencias sobre el Derecho a la ciudad, el hábitat y la convivencia, y para fortalecer y proyectar las apuestas de las comunidades urbanas sobre la ciudad.

<div>

El Hospital San Juan de Dios, en la capital, será el escenario para compartir y hacer comunes las experiencias y conflictividades, que como aprendizajes, sirvan de insumo para la construcción de planes de vida de las comunidades urbanas, e incluso como propuestas de política pública.

El trabajo en ejes como derechos sociales e inclusión, modelo de ciudad, seguridad y participación, entre otros, daría como resultado planes colectivos para la construcción de ciudad.

Las personas interesadas en participar, pueden encontrar más información en el blog https://cumbrepopularurbana.wordpress.com/

</div>
