Title: Defensoría del Pueblo advierte sobre violaciones a DDHH contra comunidades de Putumayo
Date: 2019-10-01 13:02
Author: CtgAdm
Category: Comunidad, Nacional
Tags: confinamiento, Desplazamiento forzado, Disidencias de las FARC, La Mafia, Putumayo
Slug: defensoria-del-pueblo-advierte-sobre-violaciones-a-ddhh-contra-comunidades-de-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Comunidades.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de Justicia y Paz ] 

La Defensoría del Pueblo emitió la alerta temprana Nº 040-19, de inminencia, advirtiendo sobre la  situación de riesgo que enfrentan los 4.000 habitantes de cerca de **20 comunidades campesinas, afrocolombianas e indígenas del corregimiento de Piñuña Blanco**, en Puerto Asís, Putumayo, quienes manifestaron que si el Estado no actúa de forma oportuna frente a los constantes enfrentamientos de grupos armados en su territorio, se verán forzados a desplazarse con el fin de proteger su vida y la integridad de las familias.

Dicho riesgo al que se han visto expuestas las poblaciones, radica en la presencia y disputa del territorio de las disidencias de los **Frentes 1°y 48 de las FARC-EP y la ‘La Mafia**’, estructura armada ilegal que viene incrementando su accionar, derivando en el recrudecimiento de amenazas e intimidaciones contra las comunidades campesinas, indígenas y afrocolombianas que habitan en las zonas mencionadas. [(Lea también: Continúa la crisis humanitaria en la Zona de Reserva Campesina en Puerto Asís, Putumayo)](https://archivo.contagioradio.com/continua-la-crisis-humanitaria-en-la-zona-de-reserva-campesina-en-puerto-asis-putumayo/)

### Más de 20 comunidades de Putumayo están en riesgo 

La Defensoría también menciona que en el territorio se han presentado homicidios de personas protegidas, confinamientos y desplazamientos de la población civil, que representan violaciones **"a los derechos a la vida, libertad, integridad y seguridad de la población"**, así como infracciones a las normas y principios del Derecho Internacional Humanitario (DIH).

La alerta, que cobija a las veredas de Canacas, Monteverde, Peneya, Monte Bello, El Silencio, Puerto Bello, Campo Ají, San Ignacio, Alto Piñuña, Bocana de Piñuña Blanco, Campo Sol, Santa Helena, Lisveria, Dos Quebradas y los territorios colectivos: Resguardo Siona Buena Vista y cabildos Indígenas Siona Santa Cruz de Piñuña Blanco, Indígena Bajo Santa Helena, Nasakiwe de Chorro Largo y el Consejo Comunitario La Chilpa,  establece que debe ser el Estado colombiano quien responda y garantice la integridad de sus habitantes.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]

 
