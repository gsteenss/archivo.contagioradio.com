Title: Circusncripciones de paz deben respetar liderazgos de jóvenes, mujeres y afros
Date: 2017-09-20 13:15
Category: Nacional, Paz
Tags: Circunscripciones de paz, Fast Track
Slug: circusncripciones-de-paz-deben-respetar-liderazgos-de-jovenes-mujeres-y-afros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Paz-e1499290199642.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [20 Sept 2017] 

La representante ante la Cámara del Congreso, Ángela María Robledo, ha venido denunciado el peligro que afrontan las circunscripciones de paz, con **las estructuras paramilitares que están en los territorios y que podrían intervenir en estas elecciones** para continuar la relación entre la política y el control territorial.

Otro de los peligros es la lentitud con la que el debate de las circunscripciones se ha llevado en la comisión primera de la Cámara, que de acuerdo con la representante, ha sido empujada por Rodrigo Lara, presidente de esta instancia “**hay una lentitud inexplicable, nosotros apoyamos la candidatura de Rodrigo Lara**, pero con un compromiso de parte del representante y era mover y darle celeridad a un proceso que se llama Fast Track” afirmó Robledo. (Le puede interesar: ["Cambio Radical y el Centro Democrático se le atraviesan al Fast Track"](https://archivo.contagioradio.com/cambio-radical-y-centro-democratico-se-atraviesan-al-fast-track/))

Frente al riesgo de que las cisrcunscripciones sean coptadas por los partidos políticos tradicionales o por los grupos paramilitares que operan en los territorios, Ángela María Robledo señaló que se debe garantizar que se respeten los liderazgos de mujeres, jóvenes, indígenas, afros “**que por la guerra no han tenido voz en la democracia representativa**”.

Para disminuir los peligros a las circunscripciones, tanto Ángela María Robledo como Alirio Uribe, propusieron un artículo que blinde estos escenarios “hay que evitar que se presenten allí **personas que se han presentado por otros partidos**, hay que tener una enorme claridad de que quienes lleguen allí nunca hayan tenido vínculos con la parapolítica”.

Además, manifestó que tiene que existir una veeduría ciudadana por parte de las comunidades en donde se encuentran las 16 circunscripciones y una gran presión social, para que se cumpla lo pactado en los Acuerdos de paz. (Le puede interesar: ["Acuerdos de paz podrían perder el blindaje jurídico"](https://archivo.contagioradio.com/acuerdos-de-paz-podrian-perder-su-blindaje-juridico/))

<iframe id="audio_21001571" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21001571_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
