Title: ¿Dónde estaba la policía de Buenaventura o el ESMAD durante saqueos?
Date: 2017-05-22 13:08
Category: DDHH, Nacional
Tags: ESMAD, Movilizaciones, Paro Buenaventura
Slug: confirman-que-arremetida-del-esmad-provoco-disturbios-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/buenaventura-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alvaro Miguel] 

###### [22 may 2017] 

Luego de los desmanes que ocurrieron en Buenaventura, el viernes 19 de mayo, los coordinadores del paro denuncian ataques violentos por parte del Esmad. **Un bebé murió tras la explosión de una granada en una vivienda y 7 personas resultaron heridas** con las balas de goma que utiliza el Esmad.

Para Javier Torres, miembro del comité ejecutivo del paro de Buenaventura, “las movilizaciones que empezaron el 16 de mayo habían sido pacíficas porque no hubo intervención de las autoridades. **El viernes, el uso desmedido de la fuerza del Esmad, ocasionó los hechos que conocemos”.** Le puede interesar: ["Buenaventura sigue en paro pese a represión, militarización y toque de queda".](https://archivo.contagioradio.com/buenaventura-sigue-en-paro-a-pesar-de-la-represion-la-militarizacion-y-el-toque-de-queda/)

Según Torres, el Esmad llegó a puntos de concentración como la Delfina y el Piñal para arremeter contra las actividades lúdicas que estaban realizando la población. Torres Afirmó que, **“el Esmad llega y las personas apenas los ven levantan las manos, les dejan el camino libre y los agentes le echan gases lacrimógenos ante la indefensión de ellos”.**

La llegada del Esmad generó inconformidad entre los pobladores, quienes cuestionaron el uso de la fuerza en una movilización que se ha caracterizado por ser pacífica. **Los líderes del paro han rechazado los desmanes** del viernes pues aseguran que no hacen parte de la movilización pacífica. Le puede interesar: ["Pescadores de Buenaventura que están en paro cívico son amenazados por paramilitares"](https://archivo.contagioradio.com/pescadores-de-buenaventura-son-amenazados-por-paramilitares/)

Según Torres, el gobierno en cabeza del Secretario General de la Presidencia y el Ministro de Medio Ambiente, había manifestado que “no iba a haber intervención de la fuerza pública. Una vez se terminó la reunión, el gobierno se retiró, llegó el Esmad y **engañaron al pueblo de Buenaventura”.**

[**¿QUÉ SIGUE EN EL PARO?**]

El comité ejecutivo del paro está esperando que hoy el **gobierno les dé información sobre la visita del Presidente de la República a Buenaventura** para llegar a acuerdos reales. De igual manera, le han pedido a los bonaverenses que se abastezcan de alimentos para poder continuar con el paro. Le puede interesar: ["Si no cerramos el puerto el gobierno no nos presta atención": Habitantes de Buenaventura"](https://archivo.contagioradio.com/si-no-cerramos-el-puerto-el-gobierno-no-nos-presta-atencion-habitantes-de-buenaventura/)

Para esto, **le han sugerido al comercio que permita la entrada de alimentos** y que los camiones surtan de víveres a las plazas de mercado y los supermercados. Igualmente, les han solicitado a las empresas de transporte que movilicen a las personas entre las 6 de la mañana hasta las 6 de la tarde cuando inicia el toque de queda.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FJorgeRobledoPresidente%2Fvideos%2F1364105906987439%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_18830308" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18830308_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
