Title: Así fue la instalación de la Cumbre Agraria en Bogotá
Date: 2015-08-31 18:34
Category: Movilización
Tags: ASCAMCAT, Cumbre Agraria, gloria florez, Olga Quintero, ONIC, Paro Agrario
Slug: asi-fue-la-instalacion-de-la-cumbre-agraria-en-bogota
Status: published

###### Fotos: Contagio Radio 

Con gran energía inició la jornada de movilización pacífica en Bogotá de la Cumbre Agraria Campesina, Étnica y Popular, que llegó a la capital para visibilizar los dos años de incumplimientos del gobierno Nacional con el campo colombiano

**Cinco mil personas de comunidades campesinas, negras, indígenas y urbanas llegaron a la capital** con diferentes apuestas culturales que se llevaron a cabo en el Coliseo Camacho El Campín, donde se realizó la instalación oficial de la Cumbre Agraria con la participación de los voceros y voceras de las 13 organizaciones.

**Gloria Flórez, la secretaria de gobierno de Bogotá,** hizo parte de este evento, allí resaltó la necesidad de establecer un modelo económico de desarrollo distinto al capitalista que ha acabado la madre naturaleza y ha generado la situación actual de crisis del campo y las comunidades que lo habitan.

Por otra parte, Juvenal Arrieta, Consejero Secretario General de la Organización Nacional Indígena de Colombia, ONIC, demostró su rechazo a la política de Estado y los incumplimientos del mismo.

“Pasamos de un gobierno de represión a un gobierno de acuerdos que no se cumplen, el problema ya no es que no se firmen acuerdos, el problema es que nada de lo que se firma se cumple… El primer acuerdo es que no va haber más acuerdos, no nos vamos de Bogotá hasta que no nos cumplan lo pactado”, aseguró Arrieta, quien agregó que la jornada de este año fortalece la unidad popular para que **se empiece a preparar el gran paro nacional del 2016.**

Desde hoy hasta el próximo 5 de septiembre la Cumbre Agraria estará en Bogotá, sin embargo, es probable que la jornada de movilización se extienda de acuerdo a las respuestas del gobierno.

**“Aquí estamos para decir ya no más montajes judiciales, ya no más agresiones en nuestros territorios, ya no más falsedades e incumplimiento del estado colombiano**, queremos llegarle al pueblo ciudadano”, expresó la lideresa social y dirigente de ASCAMCAT, Olga Quintero.

 
