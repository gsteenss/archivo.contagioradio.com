Title: El racismo en EE.UU más grande de lo imaginado
Date: 2015-03-05 21:33
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Amy Velez Balcazar, indignados de Wall Street, Indignados Wall street, justicia en Ferguson
Slug: abusuelven-policia-que-asesino-a-michael-brown-en-ferguson
Status: published

##### Foto:NBCnews.com 

###### **Entrevista con [Amy Velez Balcazar]:**  
<iframe src="http://www.ivoox.com/player_ek_4171650_2_1.html?data=lZakk5uZdI6ZmKiak5eJd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjKba25C6qc3Z25Cvw9HHpdvV05DRx5CtssXdyNPOxtTXb8XZjLzOztGPt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

**Darren Willson, el policía que asesino al joven negro Michael Brown** en un suburbio de Ferguson ha sido declarado **inocente** por el Departamento de Justicia. El asesinato del joven desarmado provocó semanas de disturbios y el malestar de la comunidad negra en dicho estado.

Por otro lado esta semana ha sido presentado el **informe** de la Corte de **Justicia** que confirma que tanto las autoridades **policiales como la justicia en Ferguson actúa con un patrón racista.**

Entrevistamos a **Amy Velez Balcazar del colectivo de indignados de Wall Street**, para poder comprender la situación actual de violación sistemática de los DDHH de la comunidad afroamericana y los últimos sucesos al respecto.

Amy Velez nos relata  como en **el país los negros son minoría pero mayoría en las cárceles**, así nos describe imágenes cotidianas de racismo, que atestiguan que la segregación continua a pesar según sus palabras de tener a un negro sentado en la Casa Blanca.

Una sociedad donde **no existen las mismas oportunidades para blancos que para negros**, a pesar de las medidas que ha tomado el gobierno de Obama.

La activista nos explica que **el informe** publicado por la Justicia estadounidense al respecto del racismo en Ferguson, **es solo la punta del iceberg** y que a penas se ha comenzado a trabajar en monitorizar el racismo y las violaciones de DDHH relacionadas.

Un **ejemplo** de esta situación es la educación pública de quién Amy fue parte de la junta de la escuela de un barrio deprimido, donde se puede ver la **diferencia entre las escuelas** en barrios de vivienda pública, donde habita la comunidad afroamericana sin ningún tipo de recursos y con niveles altos de pobreza o la de barrios donde la vivienda es privada y donde la mayoría es blanca con escuelas totalmente equipadas.

Por último indica que quién más está difundiendo las **ideas racistas** criminalizando y estigmatizando a la población negra son los **medios de comunicación de masas**. De tal forma las organizaciones sociales de DDHH tienen que trabajar para contrarrestar la difusión del racismo a través de la política y de los medios de comunicación de masas.
