Title: Prisioneras políticas del Buen Pastor denuncian el posible cierre de su patio
Date: 2018-11-14 18:24
Author: AdminContagio
Category: DDHH, Mujer
Tags: Bogotá, Cárcel El buen Pastor, Prisioneras políticas
Slug: prisioneras-politicas-del-buen-pastor-denuncian-el-posible-cierre-de-su-patio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/250dd3e87b831a36ce01d2e283b58d40.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo END ] 

###### [14 Nov 2018] 

Las prisioneras políticas del pabellón sexto de la  Cárcel El Buen Pastor entraron en huelga de hambre desde el pasado 10 de noviembre, en rechazo al traslado a un pasillo con celdas al interior del centro penitenciario. Según ellas, **este hecho violaría la legislación del sistema carcelario y  dejaría sin garantías a las prisioneras políticas**.

En total son 23 mujeres y 5 lactantes las que se encuentran en este pabellón y aseguran que con esta iniciativa, se les violarían los derechos a la **vida digna, salud, al libre desarrollo de la personalidad**, la reducción de la pena y a la educación, porque serían trasladadas a un espacio mucho más reducido que solo cuenta con celdas y baños.

Actualmente, las reclusas se encuentran en un espacio amplio, que cuenta con comedor, cancha y salón de máquinas, debido a que por ser consideradas como personas privadas de la libertad de seguridad especial, **no pueden acceder a ninguna zona de la cárcel fuera de su patio. **

Además, señalaron que la gran mayoría de las reclusas están condenadas a penas altas, por lo que necesitan "espacios que permitan una vida digna" sin que se les perjudique su seguridad. De igual forma, este pabellón, que cumpliría más de 20 años, **también ha sido reconocido como un escenario libre de consumo de alucinógenos, riñas y hurtos.**

De acuerdo con la administración del centro de reclusión, el espacio sería utilizado para trasladar a las madres gestantes, sin embargo, la presas políticas afirman, que el patio se puede disponer de otras formas, de manera tal que no sean trasladadas y que las madres gestantes tenga su lugar. (Le puede interesar: ["Prisioneros políticos denuncian violaciones de DD.HH desde directivas de La Picota"](https://archivo.contagioradio.com/prisioneros-politicos-denuncian-violacion-de-dd-hh-desde-directivas-picota/))

Finalmente las reclusas afirmaron que con este hecho, se pasaría por alto el artículo 2 de la Ley 1709 de 2014, en donde, "sin discriminar diferencias entre la población carcelaria, se reconocen las distinciones" y se agrupa a las internas por edad, género, religión, orientación sexual, raza, etnia y situación de discapacidad", todo ello con la finalidad de lograr la resocialización.

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
