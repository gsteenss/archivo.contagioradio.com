Title: Ferias y fiestas siguen siendo escenarios de crueldad animal
Date: 2015-01-19 21:46
Author: CtgAdm
Category: Animales, Paz
Tags: Corralejas, derechos de los animales, Maltrato animal, Petro, Sucre, Toros, Turbaco
Slug: ferias-y-fiestas-siguen-siendo-escenarios-de-crueldad-animal
Status: published

#### Foto: Carolyn Cole 

El brutal asesinato de un toro en Turbaco, Bolívar y las carreras de gatos en Tuluá, Valle; evidencian casos de crueldad animal que han generado indignación en diversos sectores de la sociedad colombiana.

Sin embargo, pese a que el general de la sociedad colombiana se ha mostrado en contra de este tipo de hechos, **las ferias y fiestas siguen siendo escenario para realizar actos crueles contra los animales por cuenta de las tradiciones culturales.**

Esta vez en el marco de las de corralejas en Buenavista, Sucre, la víctima fue un caballo que había sido embestido por un toro y enseguida un grupo de hombres descuartizó al animal que aun se encontraba con vida. El hecho fue registrado el 2 de enero, mediante un video grabado por Orlando Serpa, habitante del municipio (Ver video, imágenes fuertes).

Hasta el momento se tiene identificado al hombre de Buenavista, que se ve descuartizando al animal en el video. De acuerdo a las autoridades se trata de un ciudadano de un barrio marginal.

Por su parte, Quintiliano Tapia Rodríguez, alcalde de Buenavista o “Tierra de paz” (como la llaman sus habitantes), solo se enteró del terrible hecho 16 días después, y ahora afirma que se judicializará a las personas responsables de desmembrar al caballo. De acuerdo al alcalde, se tomaron todas las medidas necesarias, para que precisamente este tipo de actos no sucedieran.

Pese a la indignación de varios sectores sociales, entre ellos los defensores y defensoras de los animales, Semana.com dio a conocer que el  magistrado **Mauricio González Cuervo**, presentó una ponencia para que las corridas de toros vuelvan a “La Santamaría”, en contra vía de las políticas animalistas del gobierno actual de la ciudad.

**Las ferias y fiestas con las que se inició el 2015  han evidenciado la necesidad de que se sancione una ley contra el maltrato animal**, y con ello, se prohiban todo tipo de actividades crueles con animales.

Desde Onda Animalista, creemos que es contradictorio que en un país donde se está hablando de paz, niños y niñas sean espectadores de estos actos violentos.
