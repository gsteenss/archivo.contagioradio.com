Title: MOE alerta sobre porcentajes atípicos de inscripción de cédulas en 28 municipios
Date: 2018-01-15 16:32
Category: Nacional, Política
Tags: elecciones marzo 2018, inscripción de cédulas, MOE, REgistraduría, trasteo de votos
Slug: inscripcion_cedulas_elecciones_marzo_moe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/votacion-e1516038805803.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa CNE] 

###### [15 Ene 2018] 

Un total de os **1.821.823 ciudadanos cambiaron su puesto de votación o inscribieron su cédula en el territorio colombiano, **lo que representa una tasa promedio nacional de 30,6 inscritos por cada mil habitantes. Por su parte, un total de **88.876 ciudadanos inscribieron la cédula fuera del país.**

De acuerdo con cifras de la Registraduría, **en comparación con la campaña electoral del 2014, hubo un aumento del 24% (352.084) de inscritos frente a las elecciones nacionales de 2014.**

El Registrador Nacional, Juan Carlos Galindo, señaló que la persona que no pudo inscribirse para Congreso, ya no podrá participar en las elecciones del Congreso, pero recordó que las inscripciones se seguirán realizando para que las personas puedan votar en las elecciones presidenciales, cuya primera vuelta será en el mes de mayo.

"Hay un reto y un llamado para que las personas se acerquen a las urnas a ejercer su derecho al voto", señala Diego Rubiano, integrante del observatorio de democracia de la Misión de Observación Electoral, quien agrega que en términos generales los niveles de participación en Colombia oscilan entre el 30 y 40%. (Le puede interesar: [Acciones de pre campaña son el ingrediente perfecto para la corrupción: MOE)](https://archivo.contagioradio.com/?p=49605)

\[caption id="attachment\_50508" align="aligncenter" width="721"\][![Fuente: RNEC, cálculos MOE](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/elecciones-721x418.png){.size-medium .wp-image-50508 width="721" height="418"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/elecciones.png) Fuente: RNEC, cálculos MOE\[/caption\]

### **Lugares en riesgo** 

Según explica Rubiano, todavía no se puede hablar de fraude en inscripción de cédulas en elecciones de autoridades nacionales, sin embargo desde la MOE se señala el riesgo de que se produzca trashumancia histórica, teniendo en cuenta que tras los procesos de inscripción de los últimos años, actualmente 82 municipios del país tienen más censo electoral que población.

Puntualmente**, la MOE llama la atención sobre 28 municipios donde la tasa de inscripción de cédulas es tres veces mayor al promedio nacional**, lo que podría sugerir casos de trashumancia. De ellos, los que más preocupa, son los municipios de Nariño, del departamento de Nariño que tiene un tasa total de inscritos de 311,19%, Puerto Gaitán, Meta (292,81 %) y Carmen del Darién, Chocó (175,74%).

Rubiano, explica que la exagerada inscripción de cédulas en dichos municipios tiene un patrón común, que consiste en que las economías de esos lugares se basan especialmente en actividades extractivas, legales e ilegales, lo que genera que personas de otros lugares del país se trasladen allí en búsqueda de trabajo. No obstante, la MOE ha alertado a la Fiscalía sobre esas cifras para que se investigue si puede haber indicios o no de que se trate del delito de conocido como “trashumancia electoral”.

Por otra parte, en 20 departamentos la tasa de inscripción de cédulas supera el promedio nacional, entre los que se encuentra Atlántico, Meta y Vichada. (Le puede interesar:[ Los cuestionamientos a las candidaturas presidenciales por firmas según la MOE)](https://archivo.contagioradio.com/?p=45986)

\[caption id="attachment\_50510" align="aligncenter" width="800"\][![Fuente: RNEC, cálculos MOE](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/municipios_moe1-800x726.png){.size-medium .wp-image-50510 width="800" height="726"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/municipios_moe1.png) Fuente: RNEC, cálculos MOE\[/caption\]

###  **¿Qué es la “trashumancia electoral”?** 

Al realizar la inscripción de cédulas, los ciudadanos declaran bajo juramento que habitan en el municipio, corregimiento o inspección de policía correspondiente. La inscripción sólo la puede hacer el titular de la cédula de ciudadanía debido a que se debe dar una impresión de la huella dactilar. Igualmente, la inscripción sólo se puede realizar presentando la cédula.

La MOE, explica que el delito de fraude en inscripción de cédulas, conocido popularmente como “trashumancia electoral” o “trasteo de votos” consiste en la a**cción de lograr que un ciudadano inscriba la cédula de ciudadanía para votar en un lugar distinto al lugar de residencia**, y está tipificado como una conducta punible por el artículo 389 del Código Penal, con pena de 4 a 9 años de prisión.

Igualmente, quien incurra en el delito de trashumancia también incurre en el delito de falso testimonio ya que al inscribir su cédula declara bajo juramento que efectivamente habita en la ciudad, corregimiento o 6 inspección de policía correspondiente. El delito de falso testimonio es sancionado por el artículo 442 del citado código con pena de prisión de 6 a 12 años.

<iframe id="audio_23161014" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23161014_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
