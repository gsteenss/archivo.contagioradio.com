Title: Todavía habrá Álvaro Uribe para rato
Date: 2015-02-10 12:52
Author: CtgAdm
Category: Camilo, Opinion
Tags: Alvaro Uribe, Bloque Metro, los doce apostoles, Maria del Pilar Hurtado
Slug: todavia-habra-alvaro-uribe-para-rato
Status: published

###### [Foto: Confidencial Colombia] 

#### Por **[Camilo De Las Casas  ]** 

No comparto la apreciación según la cual el expresidente se encuentra asustado, temeroso frente a la entrega de María del Pilar Hurtado para responder por el desarrollo de operaciones ofensivas ilegales desde la agencia de seguridad DAS, dependiente del primer mandatario entre 2002 y 2010.

Sus decenas de trinos defendiendo su cuestionada y repudiada política de la seguridad, en la que se cometieron escuchas ilegales, montajes judiciales, ejecuciones extrajudiciales, torturas, reflejan su personalidad. Es un hombre que simultáneamente miente, niega, oculta, transfiere

Es una operación reactiva astuta, que sus críticos no siempre saben leer y que le posibilitan mantener su popularidad en cerca del 50% de los colombianos, según la última encuesta de esta semana.

Uribe ha sobreaguado a las acusaciones penales por su participación en la creación del Bloque Metro, las operaciones paramilitares en La Mundial, Guacharacas; los “12 Apóstoles”; El Aro, Oro Bajo, La Juntas, Playón, La Granja, Génesis, Septiembre Negro, entre otras, en las que fueron parte estructuras armadas regulares.

Son más de 250 acusaciones. Todas duermen en  la injusticia-Pasan de fiscal en fiscal, o de Comisión de Acusaciones en Comisión de Acusaciones, de Salas de la Corte Suprema de Justicia, sin que existan avances significativos. Aún ni las dos sentencias de Tribunales de Antioquia que ordenan investigar a Uribe por sus responsabilidades penales logran remover la sospechosa o mejor esperada parsimonia del aparato judicial.

¿Pero porque aún Uribe se mantiene en libertad, por qué las investigaciones no avanzan? Varias son las razones o sinrazones.

El aparato judicial colombiano nunca ha hecho justicia cuando de sectores de poder político se trata, las pocas sanciones a sectores de poder han recaído en algunos militares, y en menos del 10% de los vinculados con la llamada parapolítica. Los sacrificados son eses. Aún los casos de corrupción y fraude de poderosos sectores empresariales se encuentran en impunidad. El caso de Uribe no es la excepción. El aparato de justicia en Colombia es la justicia de los poderosos.

En el caso de Uribe, hay elementos adicionales. Uribe cuenta con el respaldo del Departamento de Estado, los secretos de Uribe, el número 82, allí se conservan. Las declaraciones e informes de exnarcoparamilitares, algunos que hoy son personas libres y protegidas en ese país, las compartidas a agencias de seguridad, a fiscales, siguen allá dormidas. Si existiera una decisión desde el norte, los parlamentarios Republicanos y los sectores demócratas que lo respaldan, le darían la espalda. Del mismo modo, la justicia colombiana actuaría con celeridad. Esa decisión de Washington de irse contra Uribe está lejos de producirse, y la dependencia colombiana solo reproduce ese implícito respaldo.

Uribe es tan astuto en su fórmula que los propios medios reproducen sus mentiras, sus trinos, le abren el espacio. El supuesto principio de la pluralidad informativa le posibilita que reitere sus mentiras haciéndola verosímil. No responde a lo que se pregunta, evade y oculta, pareciendo dar una respuesta coherente; y finalmente al poner a sus opositores como los victimarios, éste se convierte en víctima. Esa legitimidad social que logra incide en los llamados administradores de justicia, en los propios medios de información. Uribe es un hombre que despierta una lealtad ciega, a pesar de las evidencias.

No creo por lo escrito arriba, que Uribe éste aún asustado, ni temeroso. Su defensa ha sido un ataque reactivo, provocador, apasionado. Amplios sectores militares, policiales, políticos incluyendo los de la coalición de gobierno, empresariales y de iglesias le siguen, se identifican con su falsa moral y sus mentiras.

Un nuevo momento nacerá si se despersonaliza la judicialización a Uribe. El asunto del paramilitarismo es más que Uribe, esto no significa abandonar la denuncia penal contra éste. Se trata de una modificación en la estrategia mediática que ha ido acompañando algunas de las denuncias, y desarrollar una estrategia para desmoronar el respaldo que aún tiene en los Estados Unidos. Y en todo este entramado hay que identificar una estrategia de presión ética al séquito de sus prófugos Luis Carlos Restrepo, Andrés Arias, Luis Hoyos, y de sus guardianes ex militares y ex policías , más los que están activos aquí en Colombia.

La verdad tiene una oportunidad si hay creatividad.

Y  unas preguntas más. Si se llega a un Acuerdo con las FARC EP y el ELN, y se habilitará una Comisión de la Verdad, cómo se lograría que Uribe estuviera dispuesto a comparecer? Y ante una eventual reforma constitucional para cimentar el proceso hacia una sociedad en paz, ¿estarán hoy dispuestos todos a contar con Uribe?

-   Qué mal éste país. La matanza de los niños horroriza y debería movilizarnos. Policía omisiva y corrupta y detrás el asunto de las tierras.

