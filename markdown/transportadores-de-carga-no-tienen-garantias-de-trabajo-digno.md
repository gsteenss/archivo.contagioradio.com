Title: Camioneros reclaman una política integral para el transporte de carga
Date: 2015-02-05 19:46
Author: CtgAdm
Category: Movilización, Nacional
Tags: Asociación Colombiana de Camioneros, Enrique Virviescas, paro camionero en colombia, paro de camiones
Slug: transportadores-de-carga-no-tienen-garantias-de-trabajo-digno
Status: published

###### Foto: PubliCamión 

<iframe src="http://www.ivoox.com/player_ek_4043898_2_1.html?data=lZWhlZ2dfI6ZmKiak5eJd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dg0NLPy8aPp8LmxsjSjcnJb9biwpDd0dGJh5SZoqnhy8jFb8ri1crU1MbQb9HV08aYx9GPuNPVz9iah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Enrique Virviescas,  Asociación Colombiana de Camioneros] 

El paro de caminioneros que paralizó Bogotá en febrero del 2011, que culminó con el establecimiento de una mesa de negociación entre los sindicatos de transportadores de carga y el gobierno nacional, llegó a algunos acuerdos frente a la **problemática de fletes, chatarrización, precio del combustible y regulación de la carga**. Sin embargo, para los representantes de este sector, el gobierno ha incumplido los acuerdos, y la **crisis del sistema de transporte de carga se mantiene en Colombia.**

"La política del gobierno es equivocada, **es una política del poder abusivo que explota a los trabajadores**" señaló Enrique Virviescas, directivo de la Asociación Colombiana de Camioneros. Los representantes del sector consideran elevados los costos de la operación, del proceso de modernización de un parque automotor y del combustible, además, exigen del Estado una regulación integral de la carga, y que no se trasnacionalice el sistema.

La solución a la crisis es, para el directivo, la **construcción de una política integral para el transporte de carga** que tenga en cuenta a los trabajadores del sector, y no únicamente a los empresarios.

Mientras la **Asociación de Transportadores de Carga (ATC), ha llamado a sus afiliados a entrar a paro el 23 de febrero**, la Federación Colombiana de Transportadores de Carga por Carretera -Colfecar-, y la Asociación Colombiana de Camioneros consideran que no es el momento indicado para una acción de ese tipo.

 
