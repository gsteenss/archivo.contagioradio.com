Title: La justicia imperfecta en el camino hacia la paz
Date: 2016-03-22 09:00
Author: AdminContagio
Category: Cindy, Opinion
Tags: carceles, justicia transicional, proceso de paz
Slug: la-justicia-imperfecta-en-el-camino-hacia-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/justicia-incorrecta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: primiciadiario 

#### **[Cindy Espitia Murcia](https://archivo.contagioradio.com/cindy-espitia/) - [@CindyEspitiaM](https://twitter.com/CindyEspitiaM)** 

###### [21 Mar 2016] 

[Ante la eventual terminación del conflicto armado con las Farc, se ha planteado en la opinión pública un debate, realmente necesario, frente al límite de las concesiones que puede realizar el Estado en el propósito de armonizar la búsqueda de la paz con su obligación de garantizar la justicia.]

[Los opositores del proceso de paz han afirmado que el Estado no puede renunciar a su deber de investigar y ]**encarcelar a cada uno**[ de los responsables de cometer violaciones a los derechos humanos, porque de lo contrario se patrocinaría un contexto de impunidad y se invitaría a las personas a tomar las armas ya que, en todo caso y sin importar lo que hagan, recibirán un beneficio.]

[Esta posición resulta muy atractiva. Especialmente para una país como el nuestro que históricamente ha]**asociado la cárcel con la justicia**[ e, incluso, ha desconocido la existencia de un conflicto armado, limitándolo exclusivamente a actos terroristas– concepto aún vacío de contenido e instrumentalizado por intereses políticos-.]

[No obstante, más allá de lo políticamente correcta que suene, esta postura no tiene una aplicación práctica pues difícilmente puede utilizarse el mismo estándar de una situación tradicional – como lo es la comisión de un delito ordinario – a un contexto extraordinario en el que, por un lado, hay miles de víctimas y victimarios y, por el otro, hay un sistema judicial que por más que realice su mayor esfuerzo, no contará con la capacidad material para abordar caso a caso los delitos cometidos en la atrocidad de la guerra.]

[Vale la pena aclarar, que éste no es un problema propio de Colombia. Todos los países que han padecido un conflicto armado y han llegado a una solución negociada – como Irlanda del Norte, Sudáfrica, Sudán, Croacia, Camboya, Macedonia, entre otros – se han enfrentado a tal realidad.]

[Ahora bien,]**la experiencia de Colombia también ha demostrado que esta postura desborda la capacidad judicial y consolida un escenario de impunidad generalizada**[. Con la desmovilización de los paramilitares, en el Gobierno de Álvaro Uribe, se estaba ante el desafío de investigar a 4.490 personas por 427.628 delitos. El camino que se tomó fue investigar cada caso de forma aislada y juzgar y sancionar, con penas menores, a quienes entregaron las armas. Por supuesto, el resultado fue desalentador; al 2011, es decir 6 años después de promulgada la Ley de Justicia y Paz, sólo se habían emitido 10 sentencias.]

[En el 2012, al observar la inoperancia y falta de efectividad del paradigma, propuesto actualmente por la oposición, se implementó i) la metodología de investigación por patrones que no analiza los casos de forma aislada, sino que agrupa aquellos que compartan un modus operandi para, además de garantizar la justicia de una colectividad, develar la estructura y forma de operación de los grupos ilegales; ii) el mecanismo de selección y priorización de casos que busca  establecer criterios objetivos para escoger los casos que serán investigados y su orden de prioridad y iii) la persecución de los máximos responsables que, por lo tanto, no propenderá por juzgar a cada uno de los miembros de una estructura criminal, sino a aquellos que fungieron un papel determinante en la comisión de los delitos y cuya aprehensión es necesaria para evitar la continuidad de los ilícitos.]

[Bajo este paradigma – que es el propuesto para aplicarse en el proceso con las FARC – se han promulgado 29 sentencias en total, en las que se ha condenado a 117 postulados; identificado 18.990 hechos contra 20.614 víctimas; hallado 4.519 fosas y exhumando 5.817 cuerpos.]

[Esto no significa que el modelo de justicia transicional propuesto por el Gobierno en el marco de las negociaciones con las Farc ofrezca una justicia perfecta. Todo lo contrario, seleccionar y priorizar casos y perseguir a los máximos responsables,]**constituye per sé una imperfección connatural pero necesaria** [pues es el precio de caminar hacia la paz, principal garantía de no repetición, con el mayor grado de justicia posible.]

[E conclusión: Pretender perseguir a cada uno de los miembros de un grupo armado tras la finalización de un conflicto es un propósito loable pero inalcanzable. Aplicar esta postura desconoce la naturaleza de la realidad colombiana y no lleva más que a consolidar un verdadero escenario de impunidad extendida que, en palabras de la Corte Constitucional,]**“al buscar perseguir a todos, no se logra perseguir a nadie”.**
