Title: Minería en territorio Wiwa pone en riesgo a 250 personas
Date: 2020-05-04 16:19
Author: CtgAdm
Category: Actualidad, Ambiente
Tags: defensa de la vida, indigenas, Minería ilegal
Slug: mineria-ilegal-en-territorio-indigena-pone-en-riesgo-a-250-personas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/a1aa9461-e9bc-4c51-8363-2db20731830a.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/232d6c67-d3f4-4d3e-a89f-4eed1066143e.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En medio del Estado de Emergencia decretada por el Gobierno, la comunidad indígena Wiwa de Arimaka, ubicada en el municipio de Dibulla departamento de La Guajira; denuncia el riesgo al que se enfrentan por la [actividad de minería](https://archivo.contagioradio.com/en-cauca-toda-la-comunidad-se-siente-en-riesgo/), así como agresiones por parte de los mineros hacia la comunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según **José Martín Barros Gil**, Secretario de la comunidad de Arimaka, el pasado 30 de abril sobre las 6:00 am la comunidad procedió a retirar de su territorio todo el material relacionado a la actividad minera.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Decidimos sacar todo elemento que apoye esta actividad ilegal y ponga en riesgo la seguridad de nuestra comunidad, pero a pesar de ello, ahora **tememos por las represalias que pongan en riego la vida de las más de 250 personas del territorio**"*, agregó el Secretario.

<!-- /wp:quote -->

<!-- wp:image {"id":83920,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![comunidad wiwa indigena](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/a1aa9461-e9bc-4c51-8363-2db20731830a-1024x768.jpg){.wp-image-83920}  

<figcaption>
Foto : Integrantes de la comunidad

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

La ubicación de esta comunidad indígena se encuentra entre el **territorio  
Kogui y territorio Wiwa de la Sierra Nevada de Santa Marta y comprende cerca de de 85 familias y 450 personas**, entre niños, mujeres, hombres, mamos, autoridades y líderes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Familias que llevan cerca de 7 años de resistencia ante hechos que vulneran su territorio ancestral, según un [comunicado](file:///C:/Users/Usuario/Downloads/ACCIO%CC%81N%20URGENTE%20POR%20LA%20VIDA%20DE%20LA%20COMUNIDAD%20WIWA%20ARIMAKA.ABRIL2020.pdf) entregado por el Colectivo de Abogados José Alvear Restrepo (**CAJAR**), la Organización Nacional Indígena Colombiana (**ONIC**), y el Centro de Investigación y Educación Popular (**CINEP**).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Allí, las organizaciones aseguran que *"**desde el 2014 la comunidad ha tenido que enfrentar una serie de situaciones que vulneran sus derechos y afectan su territorio ancestral**, en ocasión a la minería ilegal de oro por un grupo de aproximadamente 100 trabajadores, (...) algunos de ellos armados"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Personas que explotan cerca de 15 lugares dentro de ***"el predio que le pertenece a la comunidad y el [territorio ancestral](https://www.justiciaypazcolombia.com/convocatoria-la-alimentacion-de-todos-los-colombianos-y-colombianas-esta-en-las-manos-de-los-campesinos-y-campesinas-de-colombia/)el cual protegen desde su cosmovisión como pueblo Wiwa"***, y que ha puesto en riesgo la vida de la comunidad al impedir el ingreso de las personas pese a las amenazas y sobornos por parte de los jefes de este grupo minero.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Las personas que se identifican como jefes y como propietario de la mina junto a un grupo de hombres sin identificar, **han hecho uso de sobornos, amenazas e intimidaciones para que la comunidad wiwa, sus lideres y autoridades espirituales accedan a aceptar la actividad ilegal"***
>
> <cite>Afirma el Comunicado</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### "Rechazamos totalmente la minería en nuestros territorios ancestrales"

<!-- /wp:heading -->

<!-- wp:paragraph -->

De igual forma en el comunicado agregan que en el mismo año 2014 estos autodenominados jefes de la mina, para facilitar su trabajo en el territorio *"**ofrecieron mercados y dinero, sin embargo, la comunidad mantuvo  
su rotunda decisión de no aceptar este tipo de sobornos y en negarse ante cualquier actividad***".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Esta negativa provocó amenazas para los lideres de la comunidad Wiwa**, y posteriormente una apropiación ilegal del territorio del cual hoy extraen oro mediante el uso de elementos como la dinamita, que no solo pone en riesgo la vida de las comunidades, sino atenta gravemente contra los ecosistemas.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "*La minería afecta nuestros territorios, **desconociendo nuestros sitios sagrados y poniéndolos en manos de personas irresponsables que aumentan la contaminación** al desechar basura, hierros, metales y elementos tóxicos que propagan las enfermedades*".
>
> <cite>José Martín Barros Gil| Secretario de la comunidad de Arimaka</cite>

<!-- /wp:quote -->

<!-- wp:image {"id":83921,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/232d6c67-d3f4-4d3e-a89f-4eed1066143e-1024x768.jpg){.wp-image-83921}  

<figcaption>
Foto: Personas de la comunidad en el lugar donde se desarrolla parte los trabajos de minería ilegal

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Finalmente Barrio señaló que de manera pacifica acordaron con la minera, **un plazo de 8 días para que estos retiren la maquinaria y elementos del territorio,** *"sabiendo que ingresaron elementos no convencionales, ellos deben ser quienes los saquen de manera segura de acá"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo hicieron un llamado al Gobierno y la Fuerza Publica a respaldar esta acción, garantizando su derecho a la protección en medio del Estado de Emergía y su soberanía ancestral sobre el territorio. (Le puede interesar: <https://archivo.contagioradio.com/en-cauca-toda-la-comunidad-se-siente-en-riesgo/>)

<!-- /wp:paragraph -->
