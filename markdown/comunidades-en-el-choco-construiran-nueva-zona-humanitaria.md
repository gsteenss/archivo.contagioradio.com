Title: Comunidades en el Chocó declaran nueva Zona Humanitaria
Date: 2017-11-10 15:45
Category: DDHH, Nacional
Tags: Chocó, comunidades, Justicia y Paz, paz, Zona humanitaria
Slug: comunidades-en-el-choco-construiran-nueva-zona-humanitaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/zona-humanitaria-e1510344628718.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Colectivo Sur Cacarica] 

###### [10 Nov 2017] 

Las comunidades campesinas y afrodescendientes que habitan en el corregimiento San Pedro de Ingará, en el municipio de San José del Palmar en Chocó, realizarán el Festival por la Vida y por la Paz donde instalarán** sus territorios como Zona Humanitaria**.

De acuerdo con la Comisión Intereclesial de Justicia y Paz, estas comunidades transformarán sus territorios en **“Zona Humanitaria Intercultural, Humanitario y Ambiental** como un espacio de paz con justicia socioambiental”. Con esto, ratifican su compromiso con las acciones que han venido desarrollando las diferentes comunidades del país con la paz. (Le puede interesar:["Al aire" emisora comunitaria en Zona Humanitaria de Curvaradó"](https://archivo.contagioradio.com/al-aire-emisora-comunitaria-en-zona-humanitaria-de-curvarado/))

Para esto, los días 11 y 12 de noviembre van a desarrollar el Festival por la Vida y la Paz en donde **habrá actividades culturales, lúdicas, deportivas y ambientales**. Además, realizarán un conversatorio sobre implementación de los acuerdos de paz y las negociaciones que se llevan a cabo en Quito entre el Gobierno Nacional y el ELN.

Finalmente, invitaron a delegaciones y personas que quieran participar del festival para que **lleven semillas, mensajes de paz** y expresiones artísticas y culturales propias de cada región.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
