Title: Piden restricción para evitar que Coronel (r) Plazas Vega salga del país
Date: 2015-12-17 11:47
Category: Judicial, Nacional
Tags: Coronel Plazas Vega, holocausto del Palacio de Justicia
Slug: plazas-vega-no-puede-salir-del-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/bomba-palacio-de-justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: archivo 

###### [17 Dic 2015]

La defensa de víctimas del Palacio de Justicia, aseguró que al conocerse que se aproximaba la absolución del Coronel (r) Alfonso Plazas Vega, le han pedido a la Fiscal Cuarta Delegada, Yenny Claudia Almeida, que ordene la detención preventiva del coronel, con el fin de **impedir que este salga del país entorpeciendo el caso que se adelanta en su contra**.

"**Sin duda Plazas Vega tuvo un papel protagónico en la retoma del Palacio de Justicia"** señala Liliana Ávila, una de las abogada de los familiares de las víctimas del Palacio de la Comisión de Justicia y Paz, quien agrega que la Corte Suprema  realizó una indebida interpretación de las pruebas.

Así mismo, para el abogado del Colectivo de Abogados José Alvear Restrepo, Luis Guillermo Pérez, la decisión de la Corte Suprema de Justicia, **“muestra un gran desconocimiento y el desacato a las órdenes internacionales”,** teniendo en cuenta que el propio expresidente Belisario Betancurt, en su momento reconoció el papel del coronel durante la retoma del Palacio de Justicia, pues era quien tenía al mando la casa del florero, donde habrían sido torturadas las víctimas.

“Él sabe quiénes fueron los que torturaron y ejecutaron, pero dice que los nombres no los da a la opinión pública, s**i ha ocultado los nombres es cómplice de los responsables, además es responsable de otros hechos**”, expresa el abogado Pérez, afirmando que esta situación en el caso de los desaparecidos del Palacio de Justicia es “una nueva ofensa a los familiares, limitando el impacto del acto de reconocimiento de responsabilidad".

Pérez, también dijo "Nosotros no nos hemos inventado las pruebas de responsabilidad (...)[Los agentes del Estado y los miembros del ejército han comprometido a Plazas Vega", concluye  se trata de **"es un fallo vergonzoso", que demuestra que **]**la justicia colombiana vive uno de los peores momentos.**

\[embed\]https://www.youtube.com/watch?v=QiUkAvu1dS4&feature=youtu.be\[/embed\]
