Title: ¡La Orinoquía en Emergencía!
Date: 2018-11-30 12:08
Author: AdminContagio
Category: DDHH, Nacional
Tags: Claretianos, Corpoclaretiana, indígenas, Orinoquía, Riesgo
Slug: orinoquia-en-emergencia
Status: published

###### [Foto: @Corpoclaretiana] 

###### [20 Nov 2018] 

Este viernes en la tarde comunidades indígenas y campesinas de la Orinoquía presentarán las razones por las que esta región necesita mayor atención por parte del Estado y la sociedad colombiana; la exposición de estas situaciones se realizará en el edificio Gerardo Arango, de la Universidad Javeriana de Bogotá, a las 4 de la tarde.

Según Viviana Rodríguez, integrante de la Corporación Claretiana Norman Perez Bello, Meta, Casanare y Vichada, son departamentos que están en emergencia porque durante mucho tiempo se ha dicho que esa región es "tierra de nadie", desconociendo las comunidades campesinas e indígenas que allí tienen sus territorios.  Por esta negación de las comunidades, durante la década de los 90's se produjo una incursión paramilitar, a la que se sumó el ingreso de grandes multinacionales en la zona.

Ambos hechos causaron grandes afectaciones a las poblaciones humanas que sufrieron la violencia, y por efectos de las grandes multinacionales, vieron afectado el ambiente en sus territorios. A esto se suma el desconocimiento que sigue existiendo sobre las poblaciones que habitan la Orinoquía, situación que sigue afectando a las comunidades que allí habitan, y que en el caso de los grupos indígenas, los mantiene al borde de la extinción.

El objetivo del foro será dar a conocer estas problemáticas; sin embargo, serán las voces del resguardo Caño Mochuelo y el Trompillo, así como de las comunidades campesinas de Vichada y de la rivera del Río Meta, quienes darán cuenta de los hechos. El foro se realizará en el Estudio 4 del Edificio Gerardo Arango, en la sede de la Universidad Javeriana de Bogotá, y la inscripción se puede realizar a través de [internet](https://docs.google.com/forms/d/e/1FAIpQLScz3U3EvmJ5w33FZl-BgYlYOnnpRuOCd4VZLxLWyLJ1mwd8Gw/viewform), o en las instalaciones de la Universidad.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
