Title: En Guatemala crece el escepticismo frente a segunda vuelta presidencial
Date: 2015-10-23 15:43
Category: El mundo, Otra Mirada
Tags: Corrupción en Guatemala, Derechos Humanos, Elecciones 25 octubre Guatemala, Elecciones en Guatemala, Elecciones presidenciales guatemala, Emisora derechos humanos, Jimmy morales, Movilización social Guatemala, Sandra Torres
Slug: en-guatemala-crece-el-escepticismo-frente-a-segunda-vuelta-presidencial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/elecciones-en-guatemala-1_995x560.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:elheraldodesaltillo.mx 

<iframe src="http://www.ivoox.com/player_ek_9144386_2_1.html?data=mpahlpiceo6ZmKialJWJd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8rijMzO1MbSuIa3lIqupsbXb9HV08aYx9HJp8Td0NPS1ZDIqc2fxdTay9PLs4y71sbhx9LFsMKf0c6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Andrea Ixtziu, periodista] 

###### [23 oct 2015]

Ad portas de la **segunda vuelta de las elecciones presidenciales en Guatemala ,** que se llevarán a cabo el 25 de octubre**, **el candidato por el Partido Nación, **Jimmy Morales, tiene un 67.9%**, tiene la ventaja sobre la candidata **Sandra Torres** del partido Unidad Nacional de la Esperanza quien **tiene un 32.1%.**

Andrea Ixtziu indica que desde la primera vuelta presidencial, la movilización social vinculada con los **grupos estudiantiles y las juventudes se han organizado** “entorno a plataformas que buscan consolidarse a largo plazo”, han mantenido reuniones permanentes y han tenido en una “constante reflexión” sobre lo que ”ocurre y respecto hacia dónde queremos llegar”, **planificando a “largo plazo”** .

Frente a este gobierno la periodista guatemalteca, afrima que cualquiera de los dos candidatos que sea elegido, **será vigilado por la ciudadanía** la cual “no va a dejar pasar desapercibida, ni mucho menos ignorar cualquier señal que halla de corrupción o violación a derechos humanos que haya”.

En la primera vuelta de nuevos diputados, alcaldes y poder local y presidencia el voto mayoritario lo tuvo el **abstencionismo con un 40%, lo cual “va a incrementarse** en la segunda vuelta” porque muchos ciudadanos no creen en los candidatos. Morales relacionado con la cúpula militar de la dictadura y Torres relacionada con los mismos escándalos de corrupción que tumbaron a Otto Perez.

Según Ixtziu la gran apuesta de los ciudadanos es “**empujar hacia la consolidación de una propuesta de reformas a la ley electoral y de partidos políticos**”, con la esperanza de “depurar el congreso de la república”, “legislar y vigilar el trabajo hecho por los diputados” y generar más “control” y “auditoría” por parte de los ciudadanos frente al gasto público.

Por ende el presidente que sea electo se encontrará con **una Guatemala con muchos problemas fiscales y sociales**, pero también con una ciudadanía activa que buscará "la firma de una asamblea constituyente", que incluye un "pacto político" respecto a que “los pueblos indígenas son mayoría de población y que sean reconocidos constitucionalmente” además el respeto a mecanismos de consulta comunitaria “mecanismos de explotación y uso del suelo”, entre otras exigencias.

Ante el eventual triunfo de Jimmy Morales, s**e ve con preocupación la cúpula militar que lo rodea ya que es “retrógrada”** y “violadora de derechos humanos”, lo cual representaría un “retroceso en materia de derechos humanos” y en cuanto a los “espacios de diálogos con el gobierno” y además una “posibilidad de creación de estrategias de guerra” para responder a demandas y movilizaciones de la ciudadanía, lo cuál genera un espacio de "bastante incertidumbre".

Las comunidades han llamado al abstencionismo y al voto en blanco, ya que no ven la presidencia “con mayor importancia”, pero **ven en la “estrategia local de toma del poder político” una salida**, que ha ayudado al “crecimiento de la auditoria social”, lo cuál ha dado al entorno rural más eficacia “para la fiscalización del poder político local”, con lo cual estos políticos han entendido que “no son intocables” y “las posiciones ciudadanas se han fortalecido”.

**Guatemala a futuro**

Frente a los casos de corrupción e impunidad abiertos que tiene la Fiscalía, la oficina de corrupción y de impunidad, y la CICIG, afirma que  "va a ser un 2016 lleno de capturas que incluso al inicio del año no podrán ejercer sus cargos por señalamientos jurídicos”, y que “los tiempos jurídicos decidirán a futuro”, Ixtziu indica que una de las alarmas es que esos cargos que queden vacios se llenen con “personas igual de corruptas” y por esto “la emergencia de la ley electoral y partidos políticos”, para impedir que la corrupción vuelva a la dirigencia.

En las medidas a largo plazo, Ixtziu indica que hay un pequeño bloque liderado por seis diputados que tienen propuestas menos criticadas que las demás que se espera que “**en estos años representen una voz de la ciudadanía en el congreso de la república**” y de cara a las elecciones del 2019, se están “consolidando movimientos”, que “buscan constituirse en una propuesta política”, además que los movimientos estudiantiles han tomado fuerza y esto “da bastante esperanza” y fortalece el camino para que en las próximas elecciones “ vamos a tener por lo menos unas opciones distintas”.
