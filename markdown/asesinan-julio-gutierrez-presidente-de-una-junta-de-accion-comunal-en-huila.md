Title: Asesinan a Julio Gutiérrez, presidente de una Junta de Acción Comunal en Huila
Date: 2020-03-02 22:49
Author: AdminContagio
Category: Actualidad, Líderes sociales
Tags: Campoalegre, junta de acción comunal, Lider social
Slug: asesinan-julio-gutierrez-presidente-de-una-junta-de-accion-comunal-en-huila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Líder-Julio-Rodríguez.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este lunes 2 de marzo se denunció el asesinato de Julio Gutiérrez, presidente de la Junta de Acción Comunal (JAC) de la vereda El Esmero en Campoalegre, Huila. Según las primeras informaciones, el líder fue interceptado sobre las 5 de la tarde cerca a su casa por los armados, que dispararon contra su humanidad, apagando su vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo a la información de medios locales, el hombre era un reconocido líder en la zona que había fundado la Asociación de Trabajadores del Campo de Campoalegre (ATC) y también hacía parte del Grupo Asociativo El Esmero. De igual forma,[se destacó](https://www.centronoticias.co/asesinado-lider-social-en-campoalegre/) que Julio Gutiérrez fue la persona que dio aviso temprano sobre la avalancha de 2017 en el Río Frío, lo que permitió la evacuación de casas que estaban en riesgo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Julio es el primer líder social asesinado en marzo, y el segundo en Huila en 2020. La primera líder asesinada en el departamento fue Mireya Hernández Guevara, que era representante comunitaria de la vereda La Despensa y tesorera de la Junta de Acción Comunal de Algeciras. (Le puede interesar:["Colombia inicia el año con aumento de violencia contra líderes sociales"](https://archivo.contagioradio.com/colombia-inicia-el-ano-con-aumento-de-violencia-contra-lideres-sociales/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar: [Durante festejo de navidad fue asesinado Reinaldo Carrillo, líder social en Huila](https://archivo.contagioradio.com/durante-festejo-de-navidad-fue-asesinado-reinaldo-carrillo-lider-social-en-huila/).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FunSumapaz/status/1234638960065904640","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FunSumapaz/status/1234638960065904640

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78980} /-->
