Title: Dos docentes fueron asesinados este fin de semana
Date: 2020-11-23 22:19
Author: AdminContagio
Category: Actualidad, DDHH
Tags: asesinato de docente, fecode, nariño, risaralda, Sindicato del Magisterio de Nariño
Slug: dos-docentes-fueron-asesinados-este-fin-de-semana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Asesinan-a-Deimer-Alberto-Lucas.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este domingo 22 de noviembre, se conoció del asesinato de los dos docentes, Byron Alirio Revelo Insuasty y Douglas Cortés Mosquera, en los municipios de Tumaco y Risaralda, respectivamente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

E[l Sindicato del Magisterio de Nariño -Simana-](http://www.simana.org.co/) denunció el asesinato del docente y líder sindical, Byron Alirio Revelo Insuasty en el municipio de Tumaco, departamento de Nariño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El docente se encontraba en el municipio, junto a otro miembro del sindicato el 12 de noviembre para cumplir con una tarea sindical y al día siguiente fueron secuestrados; el 15 de noviembre, el otro directivo fue dejado en libertad, sin embargo, ocho días después se conoció del asesinato del docente. (Le puede interesar: [Se registran dos masacres en menos de 12 horas en Cauca y Antioquia](https://archivo.contagioradio.com/se-registran-dos-masacres-en-menos-de-12-horas-en-cauca-y-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/cutcolombia/status/1330655653296300035","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/cutcolombia/status/1330655653296300035

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

De acuerdo al comunicado, la Junta Directiva de Simana y la Federación Colombiana de Trabajadores de la Educación -Fecode- habían alertado a las autoridades de la desaparición de los dos miembros «guardando la esperanza de encontrar un desenlace positivo tanto para la familia como para todo el magisterio de Nariño. Sin embargo, los enemigos de la vida y la paz, vilmente lo asesinaron».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Byron Alirio Revelo Insuasty, quien había dedicado su vida a educar niños, niñas y jóvenes, fue elegido como Directivo Departamental de Simana y se desempeñaba como secretario de Vivienda, Medio Ambiente y Bienestar Social, cargo con el que defendía la educación pública y los derechos sociales.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Asesinato de docente en Risaralda
---------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El mismo domingo, Fecode denunció el asesinato del docente Douglas Cortés Mosquera, en un local de comidas del barrio San Antonio del municipio de La Virginia, en Risaralda.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/fecode/status/1330673714355187713","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/fecode/status/1330673714355187713

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El docente fue impactado con arma de fuego en las afueras del municipio donde residía y fue trasladado al hospital San Pedro y San Pablo y luego a un centro médico de Pereira, donde falleció. (Le puede interesar: [Jonny Walter Castro, líder social es asesinado en Nariño](https://archivo.contagioradio.com/jonny-walter-castro-lider-social-es-asesinado-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Douglas Cortés era miembro del Sindicato de Educadores de Risaralda -SER- y actualmente se desempeñaba como docente en un colegio de Risaralda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante estos hechos que acabaron con la vida de los dos docentes, el sindicato y Fecode piden al gobierno investigar los hechos y dar con los culpables. Además, exigen que se detenga la «estigmatización contra el magisterio colombiano y su federación» y se garantice el derecho para el ejercicio sindical.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según El Instituto de estudios para el desarrollo y la paz - Indepaz-, con Cortés, ya serían 258 los líderes sociales y defensores de DD.HH. asesinados durante este año 2020.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
