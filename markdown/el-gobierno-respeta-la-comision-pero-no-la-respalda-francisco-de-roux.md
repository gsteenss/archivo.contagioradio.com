Title: El Gobierno respeta la Comisión, pero no la respalda: Francisco de Roux
Date: 2020-02-11 17:34
Author: AdminContagio
Category: Nacional, Paz
Tags: Conflicto armado en Colombi, Sistema Integral de Justicia
Slug: el-gobierno-respeta-la-comision-pero-no-la-respalda-francisco-de-roux
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/padre-francsico-de-Roux.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La **Comisión para el Esclarecimiento de la Verdad (CEV)** cumplió un año de trabajo formal, durante el que han tenido la oportunidad de percibir las realidades más hondas del conflicto armado. Su director, el padre Francisco de Roux hace un balance de los retos y oportunidades que este organismo parte del SIVJRNR encuentra al esclarecer las causas, responsables y consecuencias del conflicto armado en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tras un año de esfuerzo, la Comisión de la Verdad ha recolectado cerca de 12.000 testimonios gracias al trabajo de 600 personas que se han sumado a esta misión que concluirá en 2021, **"todos se están jugando todo por la verdad de este país con una conciencia muy clara de la responsabilidad tan compleja de esclarecer qué fue lo que pasó en esta tragedia humana"**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a los numerosos testimonios reunidos, el padre de Roux señala la necesidad de concientizar a un país de la gravedad de la guerra, que ha cobrado cerca de ocho millones de vidas y que se intensificó durante los últimos 24 años; entidades como el Observatorio de Memoria y Conflicto del Centro Nacional de Memoria Histórica hablan sobre el asesinato selectivo de 28.823 personas entre 1984-201511 28.823.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Este país tiene rabia por lo que paso en el conflicto, pero no tiene dolor, no ha sentido la profundidad del sufrimiento" se cuestiona de Roux, **"estoy convencido que la paz únicamente se conseguirá cuando comprendamos que todos somos seres iguales en este país con la misma dignidad y que matar a cualquier colombiano es una tragedia".**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las múltiples presiones que enfrenta la Comisión de la Verdad

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para el presidente de la [Comisión](https://comisiondelaverdad.co/), la presión de entregar una verdad que cobije a todos proviene de todos lados, "estuvimos con los campesinos en el Sumapaz y acababa de morir Alfredo Molano, lo que para nosotros fue una gran perdida, y de inmediato el clamor de los campesinos fue **¿quién va a garantizar el lugar de los campesinos en la Comisión y si él falta quién lo va a hacer?"** [(Lea también: Alfredo Molano un caminante de la verdad](https://archivo.contagioradio.com/alfredo-molano-un-caminante-de-la-verdad/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Hay gente que nos dice es que ustedes son la única esperanza que tenemos en la transición en la paz, es que ya no quedan si no ustedes”.

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Así mismo, Francisco de Roux explica que hay presión del Ejército, preocupado por la verdad que va a aparecer aquí, las preocupaciones de los empresarios, la presión de grupos como el MOVICE, de las familias que fueron secuestradas y que esperan conocer la verdadera responsabilidad de las FARC, todas, son preguntas que recaen sobre la Comisión que como institución de la transición que se vive en Colombia debe jugar un rol trascendental.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> La presión del Ejército que está preocupado por cual va a ser la verdad que va a aparecer aquí hasta dónde se va a proteger la legitimidad de las Fuerza Armadas

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### El Gobierno respeta a la Comisión, pero no la respalda

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El padre Francisco destaca la autonomía de la CEV en comparación con otras instituciones como el Centro Nacional de Memoria Histórica o el Congreso, a su vez resalta el apoyo que han recibido de organismos como la Procuraduría, la Defensoría y un sector del Congreso que blindó a la JEP y permitió que la CEV tuviera acceso a información reservada para el desarrollo de su trabajo, sin embargo ese mismo respaldo no se ha visto reflejado por parte del presidente Duque, "nos respeta pero nunca lo he visto a decir, nosotros estamos apoyando completamente a la CEV y nos la jugamos por la Comisión, eso no lo tenemos".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Nos respeta pero nunca lo he visto a decir, nosotros estamos apoyando completamente a la CEV y nos la jugamos por la Comisión, eso no lo tenemos": Francisco de Roux sobre el presidente Duque

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Frente a los problemas de la implementación del acuerdo, de Roux admite que incluso desde el Gobierno pasado ya existían falencias pues la administración Santos tuvo al menos año y medio para ser más eficaz, sin embargo a los territorios que dejaron las FARC nunca llegó el Estado. De igual forma, el presupuesto para la Comisión tampoco fue el esperado, algo que se repitió para este 2020, "el Gobierno insiste en que ha tenido que recortar los recursos de todas las instituciones públicas, pero estamos navegando con lo que tenemos y queremos decir que la cooperación internacional nos ha ayudado sensiblemente a recortar ese déficit".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La verdad necesaria para Francisco de Roux

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Al final de los tres años de mandato de la Comisión, de Roux espera poder presentar "un relato comprensible" que permita conocer lo que sucedió, y establecer afirmaciones de fondo sobre algunos de los problemas más profundo del país. [(Lea también: Diálogo entre antiguos actores armados abrió la puerta a la verdad y la reconciliación)](https://archivo.contagioradio.com/dialogo-entre-antiguos-actores-armados-abrio-la-puerta-a-la-verdad-y-la-reconciliacion/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Nuestra medida es la verdad humana, no es una verdad que se construye, es una verdad que se evidencia y cuando ese tipo de verdad aparece, uno no se puede quedar callado y aunque se le venga todo el mundo encima tiene la obligación de decirla, estamos convencidos que al país hay que conmoverlo y en ocasiones sacudirlo, se trata de no tolerar lo intolerable" - Padre Francisco de Roux

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el presidente de la Comisión es consciente que el trabajo de la entidad es limitado, su tiempo de existencia debe ser suficiente para poner en evidencia un conjunto de testimonios que produzcan un efecto de reconocimiento de responsabilidad antes las víctimas, de construcción junto a la sociedad y que permita dejar un legado en manos de académicos, comunicadores sociales, y organizaciones sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Tratamos de construir confianza entre todos, en casos hay personas que nos buscan y que tratan de establecer relaciones de amistad pero siempre les digo, acuérdense que en cualesquier caso somos más amigos de la verdad que de ustedes, lo que nos importa es la verdad" afirma Francisco de Roux quien es enfático en que aunque habrá momentos en que la verdad sea sorprendente y quizá contradiga lo que se está buscando, "frente a la verdad uno tiene que decir, está es la realidad de lo que ocurrió".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

<!-- /wp:paragraph -->
