Title: Financiación, localización y verificación: Puntos claves de un Cese Bilateral ELN y gobierno
Date: 2017-06-22 11:34
Category: Nacional, Paz
Tags: Carlos Velandia, cese bilateral, ELN
Slug: cese-bilateral-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/cese-bilateral-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cancillería Ecuador] 

###### [22 Jun 2017] 

Luego de que el pasado 13 de Junio varios obispos católicos presentaran una comunicación dirigida a las delegaciones de paz para que se acelere la posibilidad de pactar un cese bilateral con el ELN, tanto esa guerrilla como el gobierno afirmaron que hay un ambiente positivo y propicio para que se hable del tema teniendo en cuenta la próxima visita del Papa Francisco a Colombia.

Para Carlos Velandia, gestor de Paz es una muy buena señal que hayan comunicaciones en el sentido de comenzar a preparar las condiciones para el cese bilateral y de hostilidades, sin embargo, uno de los puntos álgidos en la discusión es el tema de los civiles afectados, no solamente por las acciones de la guerrilla sino por los procesos judiciales contra integrantes de organizaciones sociales que actúan en sitios en donde el ELN también tiene presencia. [Lea también: ELN y gobierno incluyen el Cese Biltareal en los puntos de la agenda](https://archivo.contagioradio.com/eln-y-gobierno-incluyen-el-cese-al-fuego-en-la-mesa-de-conversaciones/).

Además, según Velandia, para poder pactar un cese bilateral y de hostilidades tendrían que cumplirse por lo menos tres puntos, que si tienen un desarrollo ágil, podrían reducir los tiempos en los que se defina esa medida y podrían garantizar que el Cese Bilateral sea de largo plazo. [Lea también: ELN está listo para el cese bilateral.](https://archivo.contagioradio.com/42291/)

### **Financiación del ELN ** 

Según Velandia, para pactar un Cese Bilateral hay que tener en cuenta que la actividad armada del ELN es lo que posibilita que las necesidades económicas se suplan, los cobros por “impuestos de guerra” u otras actividades. Para ello se podría abrir la posibilidad de un modelo de financiación similar al aplicado con las FARC – EP en las Zonas Veredales.

### **Localización ** 

El gestor de Paz señala que para poder verificar un cese de hostilidades y de fuego es necesario que el ELN fije unas posiciones sobre las cuales se pueda hacer una veeduría eficaz, sobre todo porque los poderes locales siguen en disputa y en varias regiones, además del ELN también hay presencia paramilitar.

### **Verificación nacional e internacional** 

Por otra parte, Velandia señala que si se hace la petición a la ONU para que verifique el cese bilateral es muy probable que se acepte también por parte de ese organismo hacer ese acompañamiento porque el mandato que rige en este momento para el cese bilateral y la dejación de armas de las FARC-EP es amplio y es respaldado por el Consejo de Seguridad, un organismo que cuenta con amplio poder y capacidad de acción.

Además se podría contar con el respaldo de la Iglesia Católica y otras organizaciones sociales que han manifestado su disponibilidad en el sentido de acompañar y verificar un cese bilateral ELN y gobierno.

Adicionalmente se pueden dar las condiciones para que ese cese bilateral sea de larga duración, incluso definitivo. Según el Gestor de Paz, el único “candado” para sellar el cese bilateral es el avance ágil de las conversaciones y el cumplimiento de los aspectos que se vayan pactando, además de garantizar la participación amplia y efectiva de la sociedad civil, uno de los puntos firmes que ha planteado el ELN.

<iframe id="audio_19414605" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19414605_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
