Title: El enfoque étnico y territorial para la implementación de los acuerdos de paz
Date: 2016-08-11 12:28
Category: Nacional, Paz
Tags: afro, indígenas, La Habana, paz
Slug: las-propuestas-que-los-pueblos-indigenas-y-afro-envian-a-la-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/CplnRu0W8AADcPu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ONIC 

###### [11 Ago 2016] 

Hoy desde la Comisión Étnica de Paz para la Defensa Territorial, se entregó la propuesta del Capítulo especial del Enfoque Étnico, a **Jean Arnault, Jefe de la Misión Política de ONU** para la verificación de los acuerdos de paz, y que más adelante será entregada a las delegaciones del gobierno y de las FARC.

Desde hace más tres años los pueblos étnicos de Colombia vienen exigiendo su participación en la Mesa de conversaciones de paz de La Habana, con el fin de aportar con sus propuestas a la búsqueda de soluciones al conflicto armado, fue por ello que en marzo de este año diversas organizaciones que representan las diferentes poblaciones del país crearon la Comisión Étnica para la Paz y Defensa de los Derechos Territoriales.

A través de esa comisión el** pasado 26 y 27 de junio, las comunidades étnicas lograron ser escuchas de forma oficial por las partes en La Habana,** allí se propuso darle enfoque étnico territorial a los acuerdos de paz, con mecanismos especiales, proporcionales y diferenciales de participación.

En ese sentido, ante la Misión Política de ONU, se solicitó la creación de una mesa de trabajo para abordar los temas urgentes y construir los insumos para el Capítulo Étnico en los Acuerdos de Paz, en esa medida se propuso **la conformación de una Comisión Técnica tripartita, donde se cuente con la participación de delegados de los pueblos étnicos.**

Así mismo, solicitan que se asegure la incorporación del capítulo étnico especial,  que se espera sea incluido en el punto 6 del Acuerdo General para la terminación del conflicto, el cual contiene los principios, salvaguardas, garantías, mecanismos de seguimiento,  perspectiva de género, mujer, familia y generaciones  de los pueblos étnicos en los acuerdos de paz, en razón de que  el conflicto ha tenido un impacto mayor para las poblaciones afro e indígenas, como la misma Corte Constitucional lo ha establecido a través de diferentes fallos.

Finalmente, a partir del capítulo étnico propuesto y en el marco del ‘Acuerdo General para la terminación del conflicto  y la construcción de una paz estable y duradera’, exigen que **“se incorpore acciones y medidas  tendientes a  generar garantías de no repetición  y la construcción de una paz desde la diversidad, los derechos colectivos y las reparaciones integrales y diferenciales”**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
