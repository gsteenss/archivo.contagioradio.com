Title: Internos de La Picota completan 24 días exigiendo atención médica
Date: 2016-08-11 12:03
Category: DDHH, Nacional
Tags: Crisis carcelaria en Colombia, crisis de salud en las cárceles, protestas en la picota
Slug: internos-de-la-picota-completan-24-dias-exigiendo-atencion-medica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Reclusos-Picota..jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Internos Picota ] 

###### [11 Ago 2016] 

Desde hace 24 días los internos de La Picota adelantan una movilización con el fin de exigir atención integral en salud para toda la población presa en Colombia, ante la crisis que afrontan en los centros carcelarios, producto de un modelo que como ellos aseguran comercia con la vida de los seres humanos. Razón por la que exigen al Ministro de  Justicia hacer todas las gestiones para que se decrete la **Emergencia Social y Humanitaria en las cárceles y penitenciarias colombianas**.

Durante estos días, los internos han implementado varias formas de protesta pacífica incluso se han llegado a coser las bocas, para demandar el **suministro de medicamentos, atención especializada a cada paciente y el respeto por los derechos humanos**. Insisten al Ministro de Salud que [[asuma su responsabilidad](https://archivo.contagioradio.com/internos-de-la-picota-completan-una-semana-de-desobediencia-civil/)] pues son seres humanos los que "están padeciendo las inclemencias de la prisión con el agravante de la desatención en salud".

Exigen al gerente de Fiduprevisora que suministre la totalidad de medicamentos, que los **pacientes que requieran tratamientos con especialistas sean remitidos de manera urgente** y que la Unidad de Servicios Penitenciarios y Carcelarios coordinen las acciones para el equipamiento y la adecuación de las áreas de sanidad y odontología del penal.

[![Reclusos Picota](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Reclusos-Picota.jpg){.aligncenter .size-full .wp-image-27727 width="1280" height="960"}](https://archivo.contagioradio.com/internos-de-la-picota-completan-24-dias-exigiendo-atencion-medica/reclusos-picota/)

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
