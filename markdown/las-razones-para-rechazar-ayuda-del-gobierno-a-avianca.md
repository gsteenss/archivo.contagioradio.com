Title: Avianca ni ayuda ni es colombiana: Protesta ciudadana
Date: 2020-04-30 18:38
Author: CtgAdm
Category: Actualidad, Economía
Tags: Avianca, Gobierno, Vía al Llano
Slug: las-razones-para-rechazar-ayuda-del-gobierno-a-avianca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/N693AV_Avianca_Airbus_A321-231_s-n_6002_36946011121.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Avianca /Wiki Commons

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para palear la crisis financiera que se vive en el país y en el mundo a raíz del Covid 19, la aerolínea Avianca pidió al Gobierno una financiación que le permita sobrevivir en el mercado. Al respecto, **la ciudadanía se ha mostrado en contra de la petición pues señalan que el dinero público y el Estado deben estar al servicio de otros sectores mucho más vulnerables** como la pequeña y mediana empresa y no ser el salvavidas de empresas extranjeras.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Porrtales financieros como Bloomberg advierten que aunque aún no hay una decisión al respecto, el Gobierno se plantearía comprar acciones de la aerolínea que mantiene un estado financiero negativo desde 2019 cuando registró pérdidas de US\$893 millones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a ello, la aerolínea ha logrado reducir dicha carga por medio de una reestructuración financiera como un desembolso por parte de United Airlines y del fondo Kingsland Holdings de un préstamo de 250 millones de dólares, esta última es accionista de Avianca Holdings, mientras que United Airlines tiene una participación en la empresa gracias a otro préstamo de 456 millones de dólares hecho por BRW Aviation, compañía de Sinergy, accionista mayoritaria de Avianca de origen brasileño.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras algunos sectores políticos y medios corporativos afirman que rescatar Avianca representa impulsar a una marca colombiana, otros resaltan que aunque Avianca fue una empresa constituida en Barranquilla en 1919, **actualmente pertenece a capital extranjero por lo que no es ya una empresa nacional.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, en las redes sociales las personas criticaron la petición de la aerolínea cuando esta ha llegado a cobrar cuantiosas sumas de dinero por tiquetes locales, ejemplos específicos cuando la vía al Llano permaneció cerrada y los vuelos de Bogotá a Villavicencio alcanzaron la cifra de 700.000 pesos, mientras que cuando la vía Panamericana también presentó dificultades los tiquetes a Nariño superaron el millón de pesos. [(Lea también: Cierre de la vía al Llano ha dejado \$2 billones de pesos en perdidas)](https://archivo.contagioradio.com/afectaciones-cierre-kilometro58-via-llano/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/cortesjaco/status/1255565630385815554","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/cortesjaco/status/1255565630385815554

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Otros, recordaron los sucesos ocurridos durante el paro de pilotos de 2017 cuando cerca de 700 funcionarios de la Asociación Colombiana de Aviadores Civiles **exigían a la aerolínea mejoras económicas y de seguridad aérea,** durante aquel tiempo, la Aeronáutica Civil autorizó a Avianca a emplear pilotos extranjeros como medida de contingencia. Además varios de los participantes de la protesta fueron despedidos. [(Le puede interesar: WOLA alerta sobre violación a los derechos laborales de pilotos de ACDAC)](https://archivo.contagioradio.com/wola-alerta-sobre-violacion-a-los-derechos-laborales-de-pilotos-de-acdac/)  
  
Avianca vuela a 27 países de América y Europa, sin embargo sus ingresos provienen principalmente de vuelos nacionales con más de 700 vuelos diarios a toda Colombia. En medio de la pandemia, el presidente de la compañía afirma que 140 aviones de su flota permanecen quietos por lo que financiar al sector aéreo en general y a Avianca debería ser una prioridad, pese a la austeridad que debería existir en un momento de crisis en el país, "tenemos la responsabilidad de velar por 21.000 empleados y sus familias", afirmó el directivo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Personas como la alcaldesa Claudia López resaltaron la necesidad de dar estos ingresos a las pequeñas y medianas empresas mientras otras opinan que el impulso debe ser dado a SATENA, considerada la aerolínea de Colombia y que **en sus rutas incluye las zonas más vulnerables y lejanas del país.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ClaudiaLopez/status/1255492636531740672","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ClaudiaLopez/status/1255492636531740672

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la [aerolínea](https://twitter.com/Avianca)afirman que retomarán los vuelos a partir del 11 de mayo, cuando la medida de aislamiento obligatorio en el país finalice, pese a ello, desde el Gobierno han señalado que no se permitirán los vuelos nacionales e internacionales hasta que termine la emergencia declarada desde el mes de marzo, y aunque aún no es claro si reactivarían sus actividades, en buscadores de vuelos, ya se puede ver un aumento significativo en los precios de los vuelos que realizaría la aerolínea.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
