Title: La homofobia contraataca
Date: 2016-08-10 06:00
Category: Javier Ruiz, Opinion
Tags: Identidad de Género, LGBTI, Plebiscito
Slug: la-homofobia-contraataca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/MG_9327.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

##### Por: **[Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/) - [@contragodarria](https://twitter.com/ContraGodarria)** 

###### 10 Ago 2016 

#### **El "NO" a la diferencia ** 

[Estamos ad portas de terminar un largo conflicto armado de más de 60 años que solo nos ha traído dolor, muerte, desgracia, exilio, desplazamiento forzado, desapariciones y otras tragedias por parte de los diferentes actores del conflicto. Además, una de las causas del mismo y de los diferentes actos de violencia es la intolerancia de muchos colombianos a la diferencia.]

Y ahora que se avecina un plebiscito, el cual los colombianos debemos respaldar para darle un SI definitivo al fin de la guerra y mejorar como país, existen colombianos opositores a la paz. Es de no creer que cierto sector crea que la paz no es la vía y recurren al engaño, al miedo y a la irracionalidad para decir que la única vía de lograr la paz es con más guerra y con más exclusión.

Quienes apoyan el NO en el plebiscito son las mismas personas que no toleran las diferencias de raza, religión, de sexo y de política. Estas personas desde sus posturas irracionales y engañosas envían mensajes cargados de odio con el único objetivo de lograr su cometido de que el país siga sumido en la guerra y en la intolerancia, donde un sector que se considera privilegiado y hegemónico saque provecho mientras los demás colombianos sigan derramando sangre.

En estos días quienes han mostrado su apoyo explicito al NO están atacando a la comunidad LGBTI, con engaños y mentiras. Los medios de comunicación le dan primera plana a los ataques de los sectores intolerantes que en su discurso falaz dicen que Colombia va a sucumbir ante una “dictadura gay” por culpa de la “ideología de genero” pervirtiendo el lenguaje para confundir y meter miedo a la gente y sobre todo a los padres de familia que se dejan influenciar de estas tendencias ultra conservadoras.

Muchos políticos creen tener el poder de tomar decisiones sobre las cuestiones privadas e intimas de las personas imponiendo sus creencias por encima de las demás creencias. Hemos visto los ataques del Procurador Ordóñez, del mal llamado “concejal de la familia” y pastor cristiano Marco Fidel Ramírez, de la senadora liberal (menuda contradicción) Viviane Morales que propone un referendo donde busca que gane la “tiranía de la mayoría” y de la diputada de Santander, Ángela Hernández, que sin rubor y con falsedades mete miedo acusando a la ministra de educación Gina Parody de imponer la “dictadura gay” en los colegios.

Para completar el circulo de los ataques a la comunidad LGBTI el expresidente Álvaro Uribe, acorde a su intolerancia y a su autoritarismo extremo, se une a la campaña de ataques diciendo que familia solo hay una y que no deben cuestionar lo “natural”. El mismo líder del NO que sin vergüenza pide más guerra para Colombia con su discurso de miedo deja ver su cara excluyente y estigmatizadora de la diferencia. No hay nada diferente entre el discurso homofóbico y el discurso guerrerista.

[Y no hay diferencia tanto en el discurso como en la manera de actuar porque su objetivo es jugar sucio y lo han logrado. Hace poco en las redes sociales se difundió una cartilla falsa de educación sexual, donde había contenido pornográfico gay explícito y los opositores a la idea de una educación sexual basada en la igualdad encendieron la polémica y sin pensar creyeron que eso era verdad, cuando en realidad fue un burdo rumor con el objetivo de atacar la posibilidad de lograr que en Colombia exista la educación sexual, asignatura pendiente para evitar la discriminación dentro de los Colegios.]

Para lograr la paz es necesario aceptar la diferencia y respetar las orientaciones sexuales de las personas porque pertenecen a su fuero interno. Si queremos paz no debemos excluir y es de cuestionar fuertemente a estos sectores que supuestamente aplican los preceptos de amor al prójimo del catolicismo o del cristianismo cuando en realidad usan los argumentos religiosos para generar más odio.

Por ultimo, es lamentable que la Iglesia Católica diga SI a la paz mientras pide la discriminación a las personas por su orientación sexual. Como siempre la jerarquía católica del país en contra de los desfavorecidos.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
