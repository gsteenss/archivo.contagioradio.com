Title: Cese bilateral y participación de la sociedad: temas claves entre Gobierno y ELN
Date: 2018-03-13 14:28
Category: Otra Mirada, Paz
Tags: ELN, Gustavo Bell, Juan Manuel Santos, Mesa en Quito
Slug: cese_bilateral_gobierno_eln_mesa_quito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/eln_0-e1517506384192.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [13 Mar 2018] 

[Tras el anuncio de Juan Manuel Santos frente a la reanudación de la negociación entre ELN y gobierno Colombiano, la Delegación encabezada por el ex vicepresidente Gustavo Bell, viaja hoy a Quito para reactivar las conversaciones en donde se espera que **se trabajen dos mesas con temas centrales: el cese bilateral y la participación de la sociedad civil.**]

De acuerdo con el analista Luis Eduardo Celis, el cese bilateral de fuegos será uno de los ejes de la mesa teniendo en cuenta que ese fue uno de los llamados que hizo el ELN, tras conocer la decisión de Santos. Esa guerrilla expresó que tienen “la convicción que es mejor hacer el diálogo en medio de un cese bilateral, y que la agenda pactada hay que desarrollarla con rigurosidad y celeridad".

Asimismo lo manifestó el mandatario colombiano en su alocución de este lunes, cuando dio instrucciones a Gustavo Bell, para reactivar los diálogos “**con el objetivo de lograr avanzar en los puntos de la agenda y lograr un nuevo cese al fuego y de hostilidades amplio y verificable”.**

Otro de los puntos importantes para este ciclo, será el de la participación de la sociedad civil, punto clave en las conversaciones con la guerrilla, y uno de los temas que ha llevado más tiempo y **que seguramente puede significar hasta 6 meses de trabajo para lograr un acuerdo, así como puede suceder respecto un nuevo cese bilateral,** según cree Celis.

### **Conversaciones se reanudan tras un largo periodo de suspensión** 

Las conversaciones se reinician luego de que, según el ELN, “el Gobierno se retirara de la Mesa”.  Estas conversaciones fueron aplazadas desde el pasado 9 de enero, fecha en la que** culminó el cese bilateral al fuego** y después de que el grupo armado realizara un cese unilateral con el motivo de la jornada electoral del 11 de marzo. Indicó el grupo que las negociaciones inician “el mismo día que terminó el histórico cese al fuego bilateral de 101 días de duración”.

De acuerdo con el ELN, el retiro de la mesa de conversación por parte del Gobierno Nacional se dio **“para satisfacer a los sectores más extremos de la derecha colombiana”** teniendo en cuenta que “la lógica de los sectores ultraderechistas (..) aplauden el retiro de la Delegación gubernamental de la Mesa de conversaciones”.

"Esperamos que las partes asuman un esfuerzo de desescalamiento del conflicto, pero aún en medio de la confrontación la mesa debe funcionar ", concluye el analista.[(Le puede interesar: Gobierno y ELN reanudan diálogos en Quito)](https://archivo.contagioradio.com/gobierno-y-eln-reanudan-mesa-de-negociacion-en-quito/)

<iframe id="audio_24393446" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24393446_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
