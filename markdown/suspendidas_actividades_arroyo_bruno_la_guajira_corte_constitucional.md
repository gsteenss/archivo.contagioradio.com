Title: Suspendidas las operaciones de Cerrejón para desviar el Arroyo Bruno en La Guajira
Date: 2017-08-14 16:56
Category: Ambiente, Nacional
Tags: El Cerrejón, La Guajira, Mineria
Slug: suspendidas_actividades_arroyo_bruno_la_guajira_corte_constitucional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Arroyo-Bruno2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Censat Agua Viva] 

###### [14 Ago 2017] 

La presión jurídica por parte de las comunidades de La Guajira hizo efecto, y logró que la empresa Carbones El Cerrejón no pueda continuar con su proyecto de desviar el Arroyo Bruno. Así lo ha establecido la Sala Plena de la Corte Constitucional, que, **pese a no tener aún un fallo sobre las demandas interpuestas por las comunidades** **La Horqueta, La Gran Parada y Paradero**, ha decidido ponerle freno a las actividades de la minera, hasta tanto no se verifique la situación de los derechos humanos de la población indígena y afro.

Se trata de una decisión por la que llevan luchando más de un año las comunidades, tras conocer que El Cerrejón buscaba desviar una de las pocas fuentes de agua que les quedan para su supervivencia, y que se podría convertir en una de los tantos afluentes que ha secado la empresa, como lo han denunciado hace años las comunidades.

Particularmente, la notificación del alto tribunal se refiere a la suspensión de las actividades referentes al '**Tajo minero La Puente" "hacia el área del cause natural del Arroyo Bruno, incluyendo actividades como la remoción de capa vegetal del cause natural, así como la del acuífero aledaño y aluvial del mismo cauce".**

La noticia significa un respiro para las comunidades que temen las consecuencias de este nuevo proyecto de la empresa. Entre otras, se teme por la posible sequía de una de las pocas fuentes de agua, los daños ambientales que podrían ocasionarse y el desplazamiento forzado de 27 familias. ([Le puede interesar: Razones para no desviar el Arroyo Bruno)](https://archivo.contagioradio.com/13-razones-para-no-desviar-el-arroyo-bruno/)

La compañía argumenta cumplir con todos los requerimientos legales, ya que había logrado obtener el permiso para intervenir el Arroyo Bruno, gracias a que en noviembre de 2014, **la Autoridad Nacional de Licencias Ambientales, ANLA, permitió al Cerrejón la modificación del plan de manejo ambiental.** Un procedimiento para el cual CORPOGUAJIRA se declaró sin posibilidad de emitir permisos por incapacidad técnica, por lo que la ANLA, que ya había avalado el desvío, fue quien terminó realizando “estudios amañados”, que dieron paso a la ampliación de la mina en el Tajo La Puente, como se ha asegurado desde la organización Censat Agua Viva que acompaña a las comunidades.

### Fallos anteriores 

Cabe recodar que hace más de un año, el Consejo de Estado ratificó que debía seguir suspendida la desviación del arroyo Bruno en la Guajira. Ese tribunal ordenó que para este proyecto la multinacional** Cerrejón debe realizar una consulta previa a las comunidades Wayúu que habitan esos territorios.**

Además, para mayo de 2016 el Tribunal Contencioso Administrativo de La Guajira, resolvió un fallo de tutela, con el que se ordenó la suspensión durante un mes de los actos administrativos que autorizaban la intervención del [arroyo Bruno](https://archivo.contagioradio.com/con-el-arroyo-bruno-serian-27-las-fuentes-de-agua-que-el-cerrejon-ha-secado-en-la-guajira/). La decisión se había tomado tras la demandas interpuesta por Marcela Lorenza Gil Pushaina, una autoridad tradicional del asentamiento ancestral La Horqueta, a través de la cual se buscaba la reivindicación de los derechos humanos de los pueblos indígenas y afro de la región.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
