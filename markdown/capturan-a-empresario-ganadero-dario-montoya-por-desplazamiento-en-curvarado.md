Title: Capturan a empresario Darío Montoya por desplazamiento en Curvaradó
Date: 2017-02-15 11:56
Category: Judicial, Nacional
Slug: capturan-a-empresario-ganadero-dario-montoya-por-desplazamiento-en-curvarado
Status: published

###### [Foto: contagioradio] 

###### [15 Feb 2016]

El empresario ganadero Darío Montoya fue detenido la noche de este 15 de Febrero en el marco del proceso contra más de 90 personas por violaciones a los derechos humanos en la cuenca del río Curvaradó, territorio colectivo de comunidades negras, proceso por el que ya fueron condenadas 29 personas. **El empresario sería vinculado por los delitos de desplazamiento forzado y concierto para delinquir.**

La fiscalía investiga si Montoya estaría vinculado con una serie de negocios que conllevaron al desplazamiento forzado de las comunidades de esa cuenca en territorio chocoano desde 1996 hasta 2004 y la posterior apropiación ilegal de esos territorios por empresas palmeras y ganaderas entre las que figuran **Urapalma S.A, Palmura, Palmas de Curvaradó, Palmas S.A, Palmadó y La Tukeka, entre otras**. ([Lea también 600 paramailitares se asentarían en territorios de comunidades negras](https://archivo.contagioradio.com/al-menos-600-paramilitares-se-asientan-en-territorio-colectivo-del-curvarado-en-choco/))

Ya en 2015 habían sido condenados 16 empresarios por los delitos de concierto para delinquir agravado, desplazamiento forzado e invasión de áreas de especial importancia ecológica, fueron condenados a penas entre 7 y 10 años de prisión. No obstante las comunidades han denunciado que **persiste el control paramilitar y las amenazas contra ellos y ellas. **[Le puede interesar empresarios amenazan a reclamantes de tierras](https://archivo.contagioradio.com/?s=curvarad%C3%B3)

Una de las situaciones que genera dudas es que Montoya sería defendido por Hipólito Mendoza, un abogado reconocido en la región por ser también el gerente de la Asociación de Propietarios Tierra, Paz y Futuro, que ha sido cuestionada por sostener que los ocupantes del territorio, luego del desplazamiento, son ocupantes de buena fe, desconociendo la historia de control paramilitar y usurpación de tierras en la región.

###### Reciba toda la información de Contagio Radio en [[su correo]
