Title: Con estos argumentos Ángela Robledo defiende su curul
Date: 2019-06-28 17:15
Author: CtgAdm
Category: Judicial, Política
Tags: Angela Maria Robledo, Corte Constitucional, Curul, tutela
Slug: angela-robledo-defendera-curul
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Angela-Robledo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: Cablenoticias  
] 

**Este viernes, Ángela Robledo presentó una tutela en el Palacio de Justicia para defender su curul en la Cámara de Representantes,** a la que llegó gracias al Estatuto de la Oposición. En el documento su defensa señala los errores que se cometieron por parte del Consejo de Estado, al tomar la determinación de despojarla de su curul, por cuenta de una acusación de doble militancia en la que habría incurrido para ser candidata a vicepresidenta por la Colombia Humana.

### **Se violaron muchos derechos fiundamentales, pero en especial el artículo 112: Ángela Robledo  
**

Robledo recordó que cuando salió la decisión del Consejo de Estado, ella dijo que acudiría a la tutela para proteger la curul de ocho millones de personas que votaron por la Colombia Humana, y que gracias al artículo 112 de la Constitución (que consagra los derechos de la oposición) y la Ley 1909 de 2018 con la que se creó el Estatuto de la Oposición.  Para realizar esta demanda, la acompañará el expresidente de la Corte Constitucinoal, Jorge Iván Palacio, quien argumentará sobre estor derechos.

La tésis central de la defensa estará fundamentada en que Robledo no fue elegida para ser representante a la cámara y por lo tanto no aplicaba el concepto de doble militancia, "que solo palica para partidos políticos alcaldías gobernaciones y Congreso". En cambio**, el régimen de inhabilidades para presidencia y vicepresidencia es cerrado, y no contempla la doble militancia.** (Le puede interesar:["Ángela María Robledo se hechará al hombro la construcción de la Colombia Humana"](https://archivo.contagioradio.com/angela-maria-robledo-se-echara-al-hombro-la-construccion-de-la-colombia-humana/))

La Activista afirmó que el proceso podría ser largo, y ella esperaría que en cinco días se resolviera el destino de la tutela, pues tomando en cuenta la relevacia de la misma, la Corte Constitucional podría darle prioridad. No obstante, sostuvo que tendrían que esperar con serenidad, y confiando en una resolución favorable, "porque lo que está en juego es la democracia y el repseto a la opocisión". (Le puede interesar: ["Pérdida de curul de Ángela María Robledo es un golpe a la oposición"](https://archivo.contagioradio.com/perdida-de-curul-de-angela-maria-robledo-es-un-golpe-a-la-oposicion/))

### **¿Acudirán a otras instancias para proteger "la curul de la paz"?**

Robledo aseveró que ella siempre ha confiado en las instituciones, y por eso confía que la Corte Constitucional tome una decisión que tenga en cuenta que **la doble militancia no aplicaba para Marta Lucía Ramírez ni para ella**. Por el contrario, también podría reconocerse que hubo presiones políticas sobre el Consejo de Estado para sacarla del Congreso. (Le puede interesar: ["Razones a favor y en contra de la curul de Ángela María Robledo"](https://archivo.contagioradio.com/razones-curul-angela-maria-robledo/))

La Líder política concluyó que estas semanas fuera del congreso ha sentido impotencia y dolor, porque ha tenido que cesar su actividad en el escenario en cuyo primer acto fue un debate sobre el asesinato de líderes sociales; sin embargo, ratificó que **el regreso a su curul significará un espacio para pensar en un país diferente, que no obligue al país a seguir atado al odio.**

<iframe id="audio_37724768" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37724768_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
