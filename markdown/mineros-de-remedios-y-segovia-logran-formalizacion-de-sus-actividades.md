Title: Estos fueron los acuerdos que levantaron la protesta de mineros en Segovia y Remedios
Date: 2017-09-04 19:26
Category: Movilización, Nacional
Tags: Antioquia, Mineria, paro minero, Remedios y Segovia
Slug: mineros-de-remedios-y-segovia-logran-formalizacion-de-sus-actividades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/segovia-e1504575019543.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Mineroambientalsegovia] 

###### [04 Sept 2017] 

Tras haber llegado a un acuerdo con la empresa multinacional Grand Colombia Gold y con el Gobierno Nacional, los Mineros ancestrales de Remedios y Segovia en Antioquia, **esperan que haya un seguimiento efectivo al cumplimiento de los acuerdos** mientras se continúan con las investigaciones de la violación a los derechos humanos tras represiones del ESMAD.

Jaime Gallego, miembro de la Mesa Minera, aseguró que, con el Gobierno Nacional, **acordaron la creación de una oficina de formalización para que se caracterice y reconozca** **a los mineros ancestrales.** Igualmente, acordaron reformar el código minero para que se les tenga en cuenta dentro de los procesos que de allí se derivan. (Le puede interesar: ["Informe ratifica grave situación de DDHH en Segovia y Remedios"](https://archivo.contagioradio.com/situacion-de-ddhh-en-segovia-y-remedios/))

También dijo que **esperan contar con el apoyo de las instituciones gubernamentales para poder trabajar con una tecnología más limpia** que vaya en concordancia con el plan de ordenamiento territorial de cada municipio. Esto quiere decir que los acuerdos logrados por la Mesa Minera no solamente cobija los mineros de Remedios y Segovia sino a los de los demás municipios y territorios donde se desarrolle minería ancestral.

En lo que tiene que ver con los acuerdos con la multinacional Grand Colombia Gold, Gallego indicó que **lograron establecer contratos con vigencia de 5 años** como también la creación de una tabla equitativa de sostenimiento de la cadena productiva. (Le puede interesar: ["Policía usa francotiradores para reprimir mineros en Remedios y Segovia"](https://archivo.contagioradio.com/mineros-de-remedios-y-segovia-denuncian-presencia-de-francotiradores-en-el-paro/))

Él indicó que los mineros esperan que se haga un **reconocimiento acompañado de un censo para la población minera y así poder formalizarlos.** “Ya no es que el minero va a estar preocupado de que lo saquen de la mina, ya hay una formula equitativa con contratos a 5 años”.

Finalmente, afirmó que le han pedido a la Defensoría del Pueblo y la Procuraduría **“que las investigaciones de los detenidos en el paro sea transparente”**. Por su parte, las investigaciones jurídicas a los atropellos contra la población cometidos por el ESMAD seguirán adelante y los mineros planean aportar más pruebas.

<iframe id="audio_20682210" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20682210_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
