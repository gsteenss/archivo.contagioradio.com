Title: Estado no mostró voluntad política en la CIDH frente a desaparición forzada: CCJ
Date: 2016-12-07 13:37
Category: DDHH, Nacional
Tags: CCJ, CIDH, Desaparición forzada, estado
Slug: estado-no-muestra-voluntad-politica-en-el-tema-de-desaparicion-forzada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Audiencia-CIDH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CIDH] 

###### [7 Dic. 2016] 

El pasado 6 de Diciembre durante la audiencia temática en la CIDH sobre los derechos de las víctimas y sus familiares de desaparición forzada, diversas organizaciones sociales como la Comisión Colombiana de Juristas, la Fundación Nydia Erika Bautista, el Equipo Colombiano Interdisciplinario de Trabajo Forense y Asistencia Psicosocial (EQUITAS) y la Fundación Progresar, **denunciaron la falta de voluntad política del Gobierno Nacional para elevar a rango constitucional a la Unidad de Búsqueda de Personas dadas por Desaparecidas (UNBPD) y la Comisión para el Esclarecimiento de la Verdad.**

**Dicha audiencia se solicitó a propósito del punto 5 pactado en Acuerdo de Paz suscrito entre el Gobierno y las Farc**. Alejandro Malambo abogado de la Comisión Colombiana de Juristas manifestó “solicitamos la audiencia para hablar sobre los hechos de las víctimas de desaparición forzada en relación con los Acuerdos de Paz”.

Es que **según cifras oficiales son más de 60.000 personas desaparecidas a propósito del conflicto armado que ha vivido Colombia hace más de 52 años.**

La principal intención de solicitar esta audiencia era que la CIDH instara al Estado a dar respuestas frente al tema de desaparición forzada y que éste lo hiciera de manera pública. Disipando las dudas que rodean a las víctimas y organizaciones sociales, con respecto a la creación de la Unidad Búsqueda de Personas dadas por Desaparecidas  con rasgo constitucional.

“No dotar de rango constitucional a dicha institución llevará a profundizar y replicar los problemas de ineficiencia y falta de resultados de la normatividad colombiana actual, que ha sido insuficiente para responder a la grave tragedia de nuestro país de más de 60.000 personas desaparecidas” agregó Malambo. Le puede interesar: [Fiscalía ha suspendido 35 mil investigaciones sobre desaparición forzada](https://archivo.contagioradio.com/fiscalia-ha-suspendido-35-mil-investigaciones-sobre-desaparicion-forzada/)

**Contrario a lo que hubiesen esperado las organizaciones sociales, el Estado dejó más dudas que certezas, durante la audiencia se limitó a manifestar que presentarían un proyecto de ley pero no se clarificó ni se manifestó lo que querían oír las víctimas** “cuando se hicieron preguntas directas, concisas frente a la naturaleza legal de la Unidad Búsqueda de Personas dadas por Desaparecidas  no hubo una respuesta clara de parte del Estado, no hay voluntad política” aseveró Malambo.

Además **Malambo aseguró que lo que pasó en esta audiencia fue un “trago amargo” al ver que “no se hace una decidida defensa de los acuerdos por parte del Estado en esta materia y entonces quedaremos al vaivén de la voluntad política de querer combatir este delito con el rango constitucional”.**

Durante la audiencia las víctimas y ONGs presentaron además los problemas estructurales de impunidad, ausencia de una política pública de búsqueda y pidieron a la CIDH realizar una supervisión a la aplicación del rango constitucional de la UNBPD concertado en los Acuerdos de Paz, vigilar la grave situación de niñas y mujeres desaparecidas por dentro y fuera del conflicto armado, ratificar plenamente los tratados internacionales sobre la materia y realizar una visita in loco a la frontera colombo venezolana, entre otros temas. Le puede interesar: [99% de los casos de desaparición forzada están en la impunidad](https://archivo.contagioradio.com/99-de-los-casos-de-desaparicion-forzada-estan-en-la-impunidad/)

<iframe id="audio_14711853" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14711853_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
