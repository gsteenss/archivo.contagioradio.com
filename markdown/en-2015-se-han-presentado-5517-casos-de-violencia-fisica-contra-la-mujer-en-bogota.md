Title: En 2015 se han presentado 5517 casos de violencia física contra la mujer en Bogotá
Date: 2015-11-25 15:14
Category: Mujer, Nacional
Tags: 25 de Noviembre, Derechos Humanos, dia de la no violencia contra la mujer, Radio derechos Humanos, Secretaria Distrital de la Mujer
Slug: en-2015-se-han-presentado-5517-casos-de-violencia-fisica-contra-la-mujer-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/violencia_mujer_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: yolandario 

<iframe src="http://www.ivoox.com/player_ek_9508435_2_1.html?data=mpqdmpmXeY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRic%2Bfk5Wel5DXqYzcwtOY0tfJt8bi1cbR0ZCZeZKrjMjO1dTXb8XZjNvW0dHJssTdwpDTh6iXaaK41M6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Sandra Mujica, Secretaria Distrital para la mujer] 

###### [25 Nov 2015] 

Según cifras de la **Secretaría Distital para la mujer de enero a julio de 2015 han sido asesinadas 68 mujeres, se han presentado 5.517 casos de violencia física de pareja y 2.174 casos de presunto delito sexual.** Hoy en el día de la No violencia contra la mujer se han programado distintas actividades para unirse a la movilización en esta fecha con la cual se **busca la concientización y toma de medidas frente al maltrat**o, que pese a que en los últimos años ha disminuido continúa permeando todas las capas sociales.

Pese a que en los últimos años se ha generado un fortalecimiento institucional que ha generado un descenso en los casos de maltrato contra la mujer y se han tomado nuevas formas para denunciar, los indicadores siguen siendo fuertes **“cada tres días en Bogotá es asesinada una mujer”**, **cada hora una mujer está siendo maltratada por su pareja y diariamente se presentan 23 quejas diarias, **afirma Sandra Mujica, encargada de la Secretaría Distrital para la mujer, cifras que refleja que hay que seguir trabajando para transformar esta situación.

Sobre las causas de esta violencia, Mujica valora que s**on “estructurales y simbólicas” ya que “se sigue pensando que las mujeres somos propiedad”**, además que esa **violencia está presente en otros ámbitos como el educativo y laboral** dificultando el acceso a ellos, por esto Mujica llama la atención respecto a que “la violencia contra las mujeres es problema de todos y de todas” y que **desde “la formación de maestros y maestras, de niños y niñas”** , se puede minimizar esta problemática.

Sandra Mujica afirma que **el maltrato hacia la mujer “no reconoce estrato social” y lo que sucede es que “cambian las expresiones “** y la aparición en medios de comunicación, por esto “hay que hacer incluso un ejercicio muy fuerte de reconocimiento de esas violencias”, ya que “aunque parezcan sutiles dejan huellas sicológicas muy fuertes” y le compete a la sociedad en conjunto.

De esta forma la Secretaría Distrital para la mujer afirma que las medidas de protección han aumentado para los casos que son denunciados e invita a denunciar en la **línea púrpura de la mujer 018000 112137 y en la página web www.sdmujer.com**.

### **Estas son las actividades del día de hoy:** 

[![Agenda dia no violencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Agenda-dia-no-violencia.jpg){.aligncenter .size-full .wp-image-17819 width="599" height="565"}](https://archivo.contagioradio.com/en-2015-se-han-presentado-5517-casos-de-violencia-fisica-contra-la-mujer-en-bogota/agenda-dia-no-violencia/)
