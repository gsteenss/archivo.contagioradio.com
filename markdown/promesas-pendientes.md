Title: Promesas pendientes.
Date: 2020-04-04 17:15
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: cotidianidad, covi-19, promesas, virtualidad
Slug: promesas-pendientes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/promesas-hijos-700x467-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":6,"customTextColor":"#637379"} -->

###### Por Ángela Galvis Ardila. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

*“En verdad, en ningún lugar del mundo se termina la historia. El día que las personas dejen de hacer historia, se habrá jubilado el ser humano. Y, entonces, le convendría yacer honestamente bajo tierra”* - Eduardo Galeano.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*“Cuando todo esto pase”* se nos volvió una frase casi mántrica en estos tiempos difíciles en los que un visitante *non grato* se adueñó, sin pedir permiso, de nuestra cotidianidad, de nuestro tiempo, de nuestro espacio, de nuestros sueños, de nuestra vida y también de nuestra muerte.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Proponemos planes para “cuando todo esto pase”, prometemos abrazos, visitas, tiempo, afecto, emociones, y mientras tanto buscamos formas nuevas de estar juntos, de relacionarnos desde la virtualidad, visualizamos un futuro cercano igual o mejor que el pasado mediato que teníamos, nos urge la necesidad de pensarnos libres de eso que nos menguó la capacidad básica de tomar decisiones y que nos lleva a que cada noche nos acostemos abrazando el miedo y despertemos cobijados por la incertidumbre.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Queremos parar esa lectura de la vida desde la vulnerabilidad propia y, peor aún, desde la vulnerabilidad de nuestros seres queridos; queremos olvidarnos de que nuestros cuerpos se volvieron de la noche a la mañana objetos porosos y adherentes; queremos parar esa carrera de obstáculos que se nos volvió subsistir; nos rige una pulsión de seguir vivos, de seguir amando, de seguir resistiendo, de que si nos dan otra oportunidad mostraremos nuestra mejor versión, de pasar la hoja y volver a lo que llamábamos normalidad, aun sabiendo que la vida que dejamos en pausa allá afuera no nos recibirá igual.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque puede ser que la ilusión de volver sea la que nos está haciendo repensarnos desde el deseo, y que la fragilidad en la que hoy nos sentimos nos esté llevando a pensar en colectivo, en un nosotros en lugar de un yo, en un sentido de manada, pero que llegado el momento de sentir que salimos de esto todo ese sentimiento se diluya y las grietas que como sociedad teníamos vuelvan a brotar y esta vez con más hondura y más dolor.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Puede ser que esa relación con los demás vuelva a tomar su cauce y sigamos siendo los mismos y las mismas que se repliegan en sus espacios íntimos sin importarle el de al lado, con la mentalidad egoísta del sálvese quien pueda, olvidándonos de las redes de reciprocidad y ayuda que han surgido pero que, perfectamente, pueden ser solo la respuesta a las voces internas que nos pusieron de frente al miedo y a la culpa.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Me prometí pensar con optimismo en esta coyuntura global y quiero asumir que lo que acabo de decir no pasará, que ningún esfuerzo para mejorar como sociedad puede ser en vano, que toda forma de solidaridad deja huella, que nuestras realidades personales y sociales van a pasar por una relación cambiante con el entorno, y que en tiempos de crisis no quiero economizar mi risa ni quiero guardarla para después, quiero invertir en palabras cuando sean necesarias, pero también en silencios, quiero ofrecer presencia, pero, sobre todo, quiero ser generosa con la esperanza, porque, aunque parece que la muerte maneja su propia lógica, me consuela creer que aún queda camino por recorrer “cuando todo esto pase”.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Nuestros Columnistas](https://archivo.contagioradio.com/opinion/columnistas/)  

<!-- /wp:paragraph -->
