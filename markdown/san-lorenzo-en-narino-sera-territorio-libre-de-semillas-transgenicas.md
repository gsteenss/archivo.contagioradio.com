Title: San Lorenzo en Nariño será territorio libre de semillas transgénicas
Date: 2018-03-23 12:23
Category: Ambiente, Nacional
Tags: nariño, San Lorenzo, semillas, soberanía alimentaria, transgénico
Slug: san-lorenzo-en-narino-sera-territorio-libre-de-semillas-transgenicas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/semillas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La República] 

###### [23 Mar 2018] 

[El Concejo Municipal de San Lorenzo en Nariño, aceptó la petición de más de **1.500 personas** para declarar al municipio como un territorio libre de semillas transgénicas. Los campesinos y habitantes del municipio celebraron la declaración e indicaron que es un gran paso para defender los cultivos y las semillas nativas que se producen allí.]

[De acuerdo con Aura Domínguez, una de las guardianas de las semillas allí, “este es un sueño que tenemos **desde el 2012** cuando analizamos la problemática ambiental y la pérdida de la soberanía alimentaria en el país”. Por esto, creen que el territorio, al estar libre de semillas transgénicas, puede contribuir con la superación de estos problemas.]

### **El territorio libre de transgénicos protege las semillas nativas** 

[Indicó que las organizaciones de guardianes de las semillas, tomaron los ejemplos de otros territorios allí mismo en Nariño que se han declarado libres de transgénicos en donde se **ha protegido las semillas nativas** de productos como el maíz. Por esto, desde San Lorenzo, en 2012 iniciaron las conversaciones con la comunidad para dar a conocer la problemática. (Le puede interesar:["Reforma rural debe proteger semillas nativas"](https://archivo.contagioradio.com/reforma-rural-integral-debe-proteger-semillas-nativas/))]

[Luego de esto, lograron que las autoridades municipales incluyeran la iniciativa dentro del plan de Gobierno por lo que “con el alcalde **se lideró la iniciativa** que tiene un alcance no sólo para San Lorenzo sino para todo el país”. Recalcó que es importante que desde la ciudad se tenga en cuenta la importancia del campo pues “sin campo no hay ciudad”.]

Finalmente, Domínguez recalcó que esta iniciativa **no busca estar en contra de empresas** o personas de la industria transgénica sino que creen que es una forma para proteger sus semillas que están en peligro. Además, creen que es importante prevenir problemas de salud y ambientales por medio de una siembra sana que pueda garantizar la soberanía alimentaria.

<iframe id="audio_24767370" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24767370_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

 
