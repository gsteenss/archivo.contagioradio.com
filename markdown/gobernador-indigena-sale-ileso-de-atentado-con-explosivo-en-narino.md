Title: Gobernador indígena sale ileso de atentado con explosivo en Nariño
Date: 2016-11-29 11:49
Category: DDHH, Nacional
Tags: Agresiones contra defensores de DDHH, persecución a Marcha Patriótica
Slug: gobernador-indigena-sale-ileso-de-atentado-con-explosivo-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/dia-de-la-raza-indigenas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [29 Nov 2016] 

El pasado lunes en la madrugada el Gobernador indígena del Gran Cumbal Jorge Humerto Chiran Chiran fue víctima de un atentado. Mientras se encontraba en su residencia un **artefacto explosivo detonó en inmediaciones de su hogar**. Pese al acto de violencia ni el gobernador ni su familia resultaron heridos, sin embargo la onda explosiva dejó como consecuencias daños materiales.

No obstante este no es el primer hecho que amenaza la vida del Gobernador, el pasado 3 de noviembre, Chiran **recibió un panfleto en su casa firmado por un grupo autodenominado “Bloque Militar del Pacífico (sic) Sur Occidente de Nariño”**, en donde era objeto de señalamientos, extorsión y amedrentamientos. Después de la explosión del artefacto la vivienda tuvo afectaciones en su fachada, grietas en las principales puertas y destrucción de todas las ventanas del hogar.

El Gobernador Chiran es un dirigente destacado al interior de su comunidad que **ha trabajado en la defensa del proceso de paz y respaldando la firma e implementación del acuerdo de paz**, firmado el pasado  24 de noviembre en Bogotá. En este sentido Chiran se encuentra articulando trabajo con la Coordinación Nacional de Pueblos Indígenas (CONPI) y la Coordinación del Sector Indígena en el movimiento político Marcha Patriótica.

Con este atentado ya son 6 los hechos reportados desde el sábado en contra de la vida de defensores de derechos humanos o líderes del movimiento social en Colombia, sin que se activen mecanismos que garanticen la vida de estas personas, por parte del gobierno. Le puede interesar:["Dirigente social de la ANUC sale ileso de intento de homicidio en Pitalito"](https://archivo.contagioradio.com/dirigente-social-de-la-anuc-sale-ileso-de-intento-de-homicidio-en-pitalito/)

######  Reciba toda la información de Contagio Radio en [[su correo]
