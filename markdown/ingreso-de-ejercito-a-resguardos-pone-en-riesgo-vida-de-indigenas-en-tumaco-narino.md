Title: Ingreso de Ejército a resguardos pone en riesgo vida de indígenas en Tumaco, Nariño
Date: 2018-04-25 16:20
Category: DDHH, Nacional
Tags: Awá-Unipa, ejercito, ELN
Slug: ingreso-de-ejercito-a-resguardos-pone-en-riesgo-vida-de-indigenas-en-tumaco-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Militaress-Boyaca-e1524691374383.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [25 Abr 2018] 

Las comunidades indígenas pertenecientes al pueblo Awá-Unipa, ubicadas en los resguardos de Piedra Sellada, Quejuambi Feliciana, Inda Sabaleta, Hojal la Turiba y Chinguirito Mira, ubicados en Tumaco - Nariño, afirmaron que se encuentran en una crisis humanitaria, debido a la constante presencia de diferentes actores armados ilegales y del Ejército que viola sus derechos al ingresas sin autorización a los territorios. **Esta situación, podría causar el desplazamiento de más de 900 familias indígenas.**

### **Ejército ingresa sin autorización a resguardos indígenas** 

De acuerdo con la denuncia, actualmente en el territorio hacen presencia más de 13 grupos armados, entre los que se encuentran las Fuerza Militares de Colombia y Ecuador. El primer hecho se presentó el pasado 1 de abril, en el resguardo Piedra Sellada, ubicado en Tumaco, en la frontera con Ecuador; **cuando un gran número de integrantes del Ejército Colombiano se instaló al interior del resguardo**.

Situación que atenta contra la autonomía de los pueblos indígenas y que de acuerdo con la comunidad Awá-Unipa, se ha hecho más recurrente. A esto se suma la presencia del Ejército Ecuatoriano que estaría realizando operativos en inmediaciones al resguardo. De otro lado, **estructuras armadas también estarían haciendo controles de movilidad en el territorio,** provocando confinamiento o que las familias se desplacen forzadamente al Centro Educativo Quebrada la Hondita.

Asimismo, en el reguardo de Chiguirito Mira, la comunidad denunció el ingreso de manera inconsulta, por parte del Ejército y afirmó que el pasado 9 de abril, la Fuerza Pública retuvo **de manera abusiva a varios indígenas, a, intimidándolos y haciendo que se arrodillaran**, además les exigieron información sobre grupos armados ilegales, manteniéndolos reducidos y en condiciones denigrantes.

Al día siguiente, se produjo la retención de un joven integrante del resguardo, que de igual forma, fue sometido a largos interrogatorios. En vista del accionar ilegal del Ejército, autoridades del resguardo les exigieron la liberación del indígena, quien posteriormente fue liberado. (Le puede interesar: ["Organizaciones exigen justicia sobre corrupción al interior de las Fuerzas Militares"](https://archivo.contagioradio.com/organizaciones-exigen-justicia-sobre-corrupcion-al-interior-de-las-fuerza-militares/))

Las intromisiones del Ejercito también se han presentado en los resguardos de Tigrillo Chiquito, Quejuambi Feliciana y en Hojal la Turbia, en donde han venido instalado sus campamentos. De acuerdo con los indígenas, estas acciones por parte de la Fuerza Pública tienen la intención de buscar información sobre grupos armados ilegales, sin embargo, **afirman que ponen en máximo riesgo la vida y garantías a los derechos humanos de las comunidades indígenas**.

### **Los enfrentamientos** 

Otras de las acciones que han atemorizado a la población indígena tiene que ver con los diferentes enfrentamientos que se han provocado desde el pasado 1 de abril, en esa ocasión diferentes viviendas de los habitantes del resguardo Inda Sabaleta resultaron afectadas con impactos de bala. Posteriormente el 17 de abril, hostigamientos por parte de un grupo ilegal a la base de la Policia Guayacan, **provocó el desplazamiento de varias familias indígenas a otros reguardos**.

Frente a la delicada situación, los indígenas del pueblo Awá-Unipa, declararon la crisis humanitaria, a su vez, están exigiendo medidas urgentes por parte del Estado Colombiano para que se garanticen los derechos de los habitantes del territorio y cese el conflicto armado. De igual forma, instaron a la comunidad internacional, encargada de velar y defender los derechos humanos, a estar alerta frente a las posibles acciones violentas que pongan en riesgo la vida de los indígenas.

[Comunicado 006 - 2018 Crisis Humanitaria Resguardos Indigenas Awá Ubicados en El Municipio de Tumaco](https://www.scribd.com/document/377406902/Comunicado-006-2018-Crisis-Humanitaria-Resguardos-Indigenas-Awa-Ubicados-en-El-Municipio-de-Tumaco#from_embed "View Comunicado 006 - 2018 Crisis Humanitaria Resguardos Indigenas Awá Ubicados en El Municipio de Tumaco on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_57234" class="scribd_iframe_embed" title="Comunicado 006 - 2018 Crisis Humanitaria Resguardos Indigenas Awá Ubicados en El Municipio de Tumaco" src="https://www.scribd.com/embeds/377406902/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-plYlOhxRSpsVWSSQO3I1&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
