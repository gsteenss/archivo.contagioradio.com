Title: Se conmemoran 40 años de la toma de Saigón y derrota de EEUU en Vietnam
Date: 2015-04-30 14:45
Author: CtgAdm
Category: DDHH, El mundo, Otra Mirada
Tags: 40 aniversario toma de Saigón, Bombardeos Napalm Vietnam, Frente Nacional de Liberación de Vietnam, Ho Chi Min, Vietcong toma Saigón, Vietnam
Slug: se-conmemoran-40-anos-de-la-toma-de-saigon-y-derrota-de-eeuu-en-vietnam
Status: published

###### Foto:Latrinchera.com 

Miles de vietnamitas han conmemorado el **40 aniversario de la toma de Saigón** por parte del **Frente Nacional de Liberación de Vietnam**, conocido como Vietcong, y liberación del país de las tropas invasoras Estadounidenses.

**Saigón,** actualmente rebautizada con el nombre de **Ho Chi Min** (líder de la revolución socialista) ha sido escenario de un gran desfile donde han participado miles de niños pertenecientes a distintos centros educativos del país, así como el ejército y altos mandatarios de países asiáticos.

El **30 de Abril de 1975 fue liberada la capital por efectivos del Vietcong**, dando paso a un gobierno de transición y al año siguiente se consumó la unificación del país entre el norte y el sur.

La **guerra librada entre 1954 y 1975** entre el Norte gobernado por los comunistas, el Vietcong y las tropas irregulares del sur apoyadas por EEUU acabó con el triunfo de la revolución socialista y la reunificación de ambos países.

Estadounidenses huyendo después de la toma de Saigón:

[![1272646985441](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/1272646985441.jpg){.alignleft .size-full .wp-image-7877 width="720" height="472"}](https://archivo.contagioradio.com/se-conmemoran-40-anos-de-la-toma-de-saigon-y-derrota-de-eeuu-en-vietnam/attachment/1272646985441/)

En la contienda **EEUU combatió al gobierno del Norte e intervino militarmente** sin consentimiento de la Comunidad Internacional. Pero en el transcurso de la guerra las filas Estadounidenses sufrieron grandes perdidas, llevándoles a cambiar de estrategia armando a las milicias del sur.

EEUU utilizó Napalm y Agente naranja provocando **6.000 víctimas directas y más de 4 millones de muertos**, quedando totalmente en la impunidad, en lo que se considera una de su peores derrotas política, militar y social.

Foto que conmovió al mundo de una niña afectada por agente naranja:

\[caption id="attachment\_7878" align="alignleft" width="1280"\][![radioboston.com](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/AP7206081767.jpg){.size-full .wp-image-7878 width="1280" height="964"}](https://archivo.contagioradio.com/se-conmemoran-40-anos-de-la-toma-de-saigon-y-derrota-de-eeuu-en-vietnam/ap7206081767/) radioboston.com\[/caption\]  
 

En el seno de la **sociedad Estadounidense causó gran rechazo y fuertes movilizaciones** en contra de la guerra de Vietnam, alimentando a movimientos sociales de carácter pacifista y cultural como el conocido como **"Hippies"**.

Actualmente **Vietnam es una república socialista gobernada desde la centralidad estatista por el partido comunista de Vietnam**, mantiene relaciones internacionales con la gran mayoría de países asiáticos y es reconocida por la totalidad de la Comunidad Internacional.
