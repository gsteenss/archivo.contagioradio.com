Title: Amenazan nuevamente a opositores del proyecto minero La Colosa
Date: 2015-07-07 13:19
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Anglogold Ashanti, Cajamarca, CEDINS, Comité Ambiental y Campesino de Cajamarca y Anaime, Consejo de Estado, Hermanos Gallón Henao, La Colosa, lista clinton, Marcha Carnaval en defensa de la vida, San Roque Antioquia, Tolima
Slug: amenazan-nuevamente-a-opositores-del-proyecto-minero-la-colosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/110603-carnaval-la-colosa-12.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [notiagen.wordpress.com]

<iframe src="http://www.ivoox.com/player_ek_4732805_2_1.html?data=lZyglJ2UeY6ZmKiak5qJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Lh0crgy9PTt4ztjNGSpZiJhaXYxtfS1ZDTtNDnytnc1MrXb8KfrcaYpdTQs9TVjNjc0JDaaaSnhqaxxdnNcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Camila Méndez, Comité Ambiental y Campesino de Cajamarca y Anaime] 

###### [7 de Julio 2015]

Líderes comunitarios del **Comité Ambiental y Campesino de Cajamarca y Anaime**, denuncian que en las últimas semanas han sido **víctimas de intimidaciones y amenazas** mientras se adelantan acciones por parte de la comunidad en contra del proyecto minero La Colosa, de Anglogold Ashanti.

El pasado 29 de junio, **Robinson Arley**, integrante del Colectivo Socio-Ambiental Juvenil de Cajamarca, fue abordado por un hombre desconocido, quién le insinúo que no siguiera realizando actividades de denuncia o podría sucederle algo.

Otro de los hechos se presentó el 30 de junio cuando el antiguo presidente de la Junta de Acción Comunal de la Vereda Las Hormas de Cajamarca, recibió una llamada de un **supuesto funcionario de la SIJIN de Cajamarca, pidiéndole datos de los líderes que están en contra del proyecto minero** y la instalación de la red WIFI para los colegios por ser patrocinada por Anglogold Ashanti.

Igualmente, el 2 julio, **Isabela Cristina Pardo,** quien es integrante del Comité de Solidaridad con los Presos Políticos y apoya las acciones en contra de La Colosa, fue intimidada por dos sujetos que amenazaron con **“quemarla”.**

Las amenazas se dan luego de  la realización de la **Marcha Carnaval en Defensa de la Vida**, la **suspensión del decreto 2691** de 2014 por el Consejo de Estado, la **solicitud de suspensión de títulos mineros de Anglogold relacionados con los hermanos Gallón Henao, incluidos en la lista Clinton** por el proyecto gramalote en San Roque Antioquia y el resultado de la investigación del CEDINS [“Actividades de Anglogold Ashanti y El Bloque Central Bolívar: ¿Sólo son coincidencias?](http://cedins.org/index.php/proyectos-mainmenu-50/recursos-naturales-mainmenu-66/595-actividades-de-anglogold-ashanti-y-bloque-central-bolivar-solo-coincidencias)”.

A lo anterior, se le suma el trabajo que desarrolla la comunidad y el Comité sobre una consulta popular, donde los pobladores expresarían su rechazo al proyecto minero.

Camila Méndez, integrante del colectivo, asegura que pese a las denuncias correspondientes, **“no hay intensión de las autoridades en avanzar en las investigaciones”, **un** **ejemplo, es el  asesinato de Juan Pinto, Pedro García, José Ramírez y Daniel Sánchez que al día de hoy continúan en la impunidad, según Camila  siempre se buscan excusas a sus muertes, sin tener en cuenta el trabajo que cada uno de ellos realizaba en contra de Anglogold Ashanti.

Cabe recordar que tras la puesta en marcha de La Colosa, se incrementaron las violaciones a derechos humanos y se conoce que **veinte familias han tenido que desplazarse** sin ningún tipo de garantía de vivienda.

Méndez, asegura que **así continúen las intimidaciones, el movimiento en contra de la realización de La Colosa** **seguirá en su lucha.**
