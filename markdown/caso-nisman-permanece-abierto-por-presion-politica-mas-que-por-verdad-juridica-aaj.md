Title: Caso Nisman permanece abierto por “presión política” más que por verdad jurídica: AAJ
Date: 2015-02-16 22:12
Author: CtgAdm
Category: Entrevistas, Judicial
Slug: caso-nisman-permanece-abierto-por-presion-politica-mas-que-por-verdad-juridica-aaj
Status: published

###### Foto: ahorasanluis.com 

Según Ernesto Julio Moureo, integrante de la junta directiva de la Asociación Americana de Juristas, el caso por el suicidio del fiscal Alberto Nisman, **ya estaría cerrado de no estar siendo utilizado como mecanismo de presión política**, en el marco de un golpe institucional gestado desde sectores de derecha en Argentina, vinculados a la Mossad y la CIA.

Según el jurista, este caso ya estaría cerrado de no tener la presión política que tiene por el uso que se pretende dar para **gestar un golpe institucional al gobierno actual**. Según Moureo, los informes citados por el grupo empresarial “Clarín” obedecen más a intereses políticos que a la realidad.

En su versión de internet, el periódico “clarín” afirma que la trayectoria de la bala y la ausencia de pólvora en la mano de Nisman, refutaría la hipótesis del suicidio, sin embargo Moureo, de la **Asociación Americana de Jusristas**, todas las evidencias y la seriedad de la autopsia dan resultados irrefutables de la comisión del suicidio.

Moureo puntualiza que Alberto Nisman, pidió un arma prestada que le entregaron la tarde anterior al suicidio, su apartamento estaba cerrado y está ubicado en un edificio altamente custodiado, el cadáver de Nisman quedó bloqueando la puerta del baño y la **trayectoria de la bala corresponde inequívocamente al 98% de los casos de suicidio** con arma de fuego. Todo ello, lo reseña Moureo de acuerdo al informe de fiscalía en el lugar de los hechos y a la autopsia posterior.

El próximo 18 de Febrero está convocada una movilización para respaldar Nisman, sin embargo, esta actividad ha sido convocada por grupos minoritarios a los que se ha sumado la presión mediática ejercida por diversos grupos que además están **ligados a la dictadura militar, como lo fue en vida el propio Alberto Nisman.**
