Title: Colombia en deuda con las políticas migratorias
Date: 2016-08-03 13:45
Category: DDHH
Tags: deportación cubanos
Slug: colombia-en-deuda-con-las-politicas-migratorias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/cuba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Semana] 

###### [3 de Agos]

Aproximadamente **1.273 son los cubanos que se encuentran en el municipio de Turbo**, en el departamento de Antioquia esperando ayuda por parte del gobierno colombiano para ser enviados de forma humanitaria a México o Estados Unidos. Sin embargo, el presidente Juan Manuel Santos decidió llevar a cabo la deportación de los mismos.

De acuerdo con la especialista en migración e investigadora Alexandra Castro, esta situación no es novedosa y se ha venido presentando cada vez con mayor frecuencia en las fronteras de Colombia, que no están vigiladas y por lo cual **se han establecido como corredores de narcotrafrico, contrabando y trata de personas**.

Frente a la deportación de los cubanos la investigadora expresó que es importante que Colombia realice las deportaciones caso por caso y no una deportación colectiva, debido a que esto va en contravía de principios y acuerdos internacionales de derechos humanos, además indicó que **el país no cuenta con un lugar adecuado para recibir a estas personas, ni una institución encargada de seguir los casos individualmente con atención especializada**.

Las rutas ilegales de migración representan altos costos que deben pagan las personas para llegar a sus destinos y las diferentes violaciones de derechos humanos a las que son sometidas. Según Alexandra Castro, entre más difícil sea llegar al lugar, más costoso será el viaje, cuyas **rutas son controladas por bandas criminales que se lucran por dejar pasar a las personas** y que evidencia la falta de una política de control de las rutas de tránsito migratorias.

"El tráfico de migrantes y el vínculo con la trata de personas se hace más latente si el país  organizado tenga un país el tránsito en su territorio, **si se planean rutas migratorias seguras se evita el tráfico de migrantes**" aseguro la especialista.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

   
 
