Title: Ciudadanía bogotana marchó por los animales y contra la "Estocada a la democracia"
Date: 2015-10-19 11:24
Category: Animales, Voces de la Tierra
Tags: 18 de octubre, Consulta Antitaurina, consulta popular, Estocada a la democracia, Maltrato animal, Marcha mundial por los derechos de los animales, Natalia Parra, paz, Plataforma Alto, Plaza de Bolívar, VII encuentro de los Pueblos Indígenas del Distrito Capital
Slug: ciudadania-bogotana-marcho-por-los-animales-y-por-la-estocada-a-la-democracia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/12167353_10153517610005020_2077378838_n1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [19 Oct 2015] 

En el marco de la VII marcha mundial por los derechos de los animales, este 18 de octubre **miles de ciudadanos y ciudadanas, marcharon desde la Plaza la Santamaría hasta la Plaza de Bolívar,** para expresar su rechazo al maltrato animal y a la "estocada a la democracia" tras la decisión del Consejo de Estado al suspender la consulta antitaurina.

La parte de atrás de la movilización se caracterizó por ser un acto fúnebre, donde la democracia se encontraba en ataúd, como símbolo de la decisión del Consejo de Estado, violando los derechos a la participación política de la ciudadanía. Este acto contrastaba con la alegría y el carnaval de la parte de adelante de la marcha, donde las personas **celebraban la vida, la continuación de la lucha por la defensa de los animales y con ello, la búsqueda de la abolición de las corridas de toros.**

Al llegar a la Plaza de Bolívar, la movilización se unió a los actos de la  ***‘*VII encuentro de los Pueblos Indígenas del Distrito Capital’,** donde indígenas y animalistas, se unieron para defender el derecho a la vida digna. Allí, en su discurso, Natalia Parra, directora de la Plataforma ALTO señaló, "en este país nos han vendido que tenemos muchas formas democracias, pero siempre las han tumbado", así mismo, añadió que las mismas personas poderosas que le han robado la tierras a los indígenas y campesinos, son los mismos que impidieron que la consulta antitaurina se realizara.

**"Los ciudadanos necesitamos que nos respeten, los animalistas estamos más fuertes que nunca, tarde o temprano lograremos tumbar las corridas de toros",** concluyó Parra, también integrante del programa Onda Animalista de Contagio Radio.

\
