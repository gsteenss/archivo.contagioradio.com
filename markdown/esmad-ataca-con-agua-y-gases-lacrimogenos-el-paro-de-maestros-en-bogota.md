Title: ESMAD ataca el paro de maestros en Bogotá
Date: 2017-06-09 15:15
Category: Nacional
Tags: ESMAD, Paro de maestros
Slug: esmad-ataca-con-agua-y-gases-lacrimogenos-el-paro-de-maestros-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/ESMAD-ataca-marchas-de-profesores.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Quedobatenta 

###### 9 Jun 2017

Hacia las 2 de la tarde, cuando una marcha de cerca de 20.000 maestros y maestras se dirigían hacia el aeropuerto por la Calle 26, a la altura de la Avenida Ciudad de Cali, al occidente de la capital, un contingente de integrantes del ESMAD arremetió contra la movilización que se realizaba de manera pacífica, dejando como saldo un profesor herido en una pierna por el impacto de una granada de gas lacrimógeno y varios afectados por la inhalación de los gases.

Varios videos y fotografías circularon a través de redes sociales en los que se da cuenta de la desmedida agresión por parte de los efectivos de la policía contra los maestros y maestras. Situación que elevó la indignación de miles de personas, puesto que los 30 días de paro del magisterio se han caracterizado por ser pacíficos y con un alto contenido pedagógico.

El profesor Fernando Triana aseguró que lo más grave de la represión es que el 70% del magisterio colombiano está conformado por mujeres, algunas de ellas embarazadas y acompañadas por sus hijos. Según lo que se pudo observar hasta el sitio llegaron 2 ambulancias para evacuar los heridos. “No hemos hecho ningún acto de violencia, solo estamos denunciando los incumplimientos del Estado”

Esta situación se presenta en medio de la decisión de FECODE de reafirmar la movilización ante lo que ellos consideraron “amenazas del gobierno” tras las declaraciones del presidente Juan Manuel Santos quien resaltó que no se pagarán los días a quienes no los trabajen. Según los profesores, la idea de reponer las jornadas nunca ha estado en cuestión y su movilización no es solamente por las deudas salariales del Estado.

“Estamos luchando es por la dignidad de los estudiantes, hay aulas con más de 40 estudiantes, los refrigerios y la alimentación no cumplen con las condiciones necesarias, hay establecimientos con más de 4000 estudiantes que no cuentan con servicio médico” además agrega que la situación de los maestros no es diferente “estoy haciendo un doctorado y debo 45 millones, de dónde los pago si no gano sino 2 millones y medio”.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F10154521942845812%2F&amp;show_text=0&amp;width=560" width="560" height="309" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

> Profesor herido por el ESMAD, esto esta pasando en Colombia. Así no se ataca a quienes luchan por sus derechos [@AlirioUribeMuoz](https://twitter.com/AlirioUribeMuoz) [@Ccajar](https://twitter.com/Ccajar) [pic.twitter.com/DmW1G60hYY](https://t.co/DmW1G60hYY)
>
> — Yessika Hoyos (@YessikaHoyos) [9 de junio de 2017](https://twitter.com/YessikaHoyos/status/873260667326914561)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>

