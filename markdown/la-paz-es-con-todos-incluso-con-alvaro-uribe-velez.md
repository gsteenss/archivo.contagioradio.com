Title: La paz es con todos, incluso con Álvaro Uribe Vélez
Date: 2015-04-09 08:37
Author: CtgAdm
Category: Fernando Q, Opinion
Tags: Centro Democrático, Fernando Quijano, paz, uribe
Slug: la-paz-es-con-todos-incluso-con-alvaro-uribe-velez
Status: published

#### Por  [**Fernando Quijano**] - [~~@~~FdoQuijano](https://twitter.com/FdoQuijano)

Estamos llegando al momento de las decisiones finales, hay enormes posibilidades de alcanzar la salida negociada al conflicto armado, político y social; firmar los Acuerdos de Paz y echarlos andar e iniciar la etapa del posconflicto, aunque esta no se librará de ser un poco traumática.

Mi optimismo radica en los vientos de reconciliación que hoy llegan desde muchas partes. Por ejemplo, de los cuarteles militares, veteranos combatientes como el exgeneral Mora Rangel, soplan: “En la historia encontramos que ningún conflicto es eterno, todos, sin excepción, tarde o temprano llegan a su fin y la inmensa mayoría de ellos lo hacen mediante un proceso de conversaciones. Son muy escasos los que terminan con la desaparición total o el sometimiento absoluto del enemigo”. Según él, el proceso de paz es respaldado con “profesionalismo y abundante estoicismo” por las fuerzas armadas del Estado. ¡En hora buena regresa Mora a la mesa!

Sumado a esto, los aires positivos también llegan desde las montañas de Colombia y desde su región del Catatumbo: del primer lugar llegan las propuestas de reconciliación y negociación por parte del ELN; del segundo, la guerrilla del EPL, anuncia su intención de trasegar el camino de la paz. La insurgencia no está descalificando el proceso en La Habana, demostrando una creciente madurez política frente al fin de la guerra ¡Bienvenidas sean también a la Mesa!

Los 50 años de confrontación armada que marcaron la historia de este país parece que pronto van a llegar a su fin. Se seguirá presentando un tira afloje aunque natural en este tipo de procesos; ligerezas verbales por parte de funcionarios públicos escépticos, en su mayoría de la boca ligera del Procurador General; y una que otra imprudencia de voceros de las FARC y del gobierno, de los militares activos y retirados, y hasta del mismo presidente Juan Manuel Santos, a quien le ha dado por insinuar que sus enemigos (léase Farc) “[parecían brutos](http://www.radiosantafe.com/2015/04/07/las-farc-parecen-brutos-presidente-santos/)”, básicamente, por no firmar prontamente los acuerdos.

El mandatario también manifestó que para él sería muy fácil retirarse de la mesa en la Habana: “Si al final me toca correrme, me corro, así de sencillo”, y aunque lamento escuchar eso -casi indicando que poco le importa  desechar más de dos años de negociación-, aun así no arruino mi optimismo, porque soy perfectamente consciente de lo que olvida el presidente: **la consecución de la paz obedece a un interés público y no al interés privado del mandatario.**

Estas declaraciones de uno y otro bando no me causan incertidumbre frente al proceso, pero hay un asunto que sí me preocupa: a la mesa le siguen faltando algunas patas.

Si llegamos a firmar los acuerdos, cómo vamos a resolver el asunto de las estructuras paramafiosas aún vigentes y que no están en negociaciones. Qué hacer con Urabeños, Rastrojos, Oficina y sus colaboradores y auspiciadores, que anualmente reclutan miles de personas, en su mayoría jóvenes, convirtiéndolos en máquinas para matar y perpetuando así la guerra. Esta es la primera pata que falta en esa mesa.

El sometimiento a la Justicia con dignidad y sin impunidad podría ser la salida. El Estado colombiano y el gobierno saben que este es un problema serio que atormenta, infunde pánico y genera zozobra en ciudades como Cúcuta, Bogotá, Cali y Medellín, así como en el puerto de buenaventura y la región del Urabá. En la capital antioqueña se pueden encontrar, si no son más, 350 grupos armados (eufemísticamente llamados Bacrim) que hacen parte del engranaje criminal y obedecen a dos estructuras criminales –Oficina o Urabeños-, los cuales cuentan con más de 13.500 personas reclutadas.

La otra pata que hace falta la tienen algunos protagonistas de Colombia, unos son detractores del proceso; otros, declarados enemigos de la paz con la insurgencia.  El sueño de la paz puede ser posible cuando todos empecemos a creer en ella. Coincido con la exsenadora Piedad Córdoba: la Asociación Colombiana de Oficiales en Retiro de las Fuerzas Militares (Acore), el Centro Democrático y hasta Álvaro Uribe Vélez, así como el resto de detractores deberían acercarse a la mesa e integrarse al proceso, con sus reclamos y sugerencias, porque, en últimas, la paz se firma entre enemigos, y esto incluye a opositores y contradictores.

**Nota:** Hoy, 9 de abril, día de las víctimas, salgamos a marchar por la paz con justicia social, el momento histórico lo amerita, vamos de frente y sin miedo por la salida negociada al conflicto. **Expresidente Uribe, por qué no aporta la pata que hace falta para que la mesa no cojee, empiece por marchar con nosotros.**
