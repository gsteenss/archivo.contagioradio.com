Title: Ditirambo Teatro, 3 décadas de lucha y resistencia
Date: 2018-09-20 17:05
Author: AdminContagio
Category: Cultura
Tags: Bogotá, ditirambo, teatro
Slug: ditirambo-teatro-lucha-y-resistencia
Status: published

###### Foto: Ditirambo Teatro 

###### 20 Sep 2018 

Hace 30 años, el director, dramaturgo y actor **Rodrigo Rodríguez** apostó por un sueño que se materializó en **Ditirambo Teatro**, una de las compañías con mayor trayectoria en Bogotá, con más de **50 obras** llevadas a escena en **8000 funciones**, **dos salas y varios premios nacionales e internacionales**. En razón de su aniversario han preparado una temporada especial para los meses de septiembre y octubre.

Con una selección de obras y lecturas dramáticas, la compañía compartirá con el público **tres décadas haciendo del teatro una herramienta de transformación social y de denuncia** frente a la explotación, el abuso y la corrupción, entre otros temas, y de retratar las tragicomedias que se viven en una sociedad como la colombiana.

El repertorio esta conformado por los monólogos de **Margarita Rosa Gallardo** "El Yuri", la historia de un travesti de la calle 22 de Bogotá y su fatal amor; "La Profesora Rosalba Scholásticus", que reflexiona sobre el poder, el premio y el castigo; y el clásico, "Ni Mierda pal perro", una tragicomedia que narra la historia de Gilma Tocarruncho, una mujer humilde que sufre los embates del cambio, al trasladarse del campo a la ciudad detrás de un sueño que nunca cumple.

Por su parte, **Ricardo Peñuela** se suma con sus interpretaciones en **"Teseo"** y Simón Bolívar en **"Carta A Jamaica"**, estreno de la temporada. Mientras que la actriz Nancy Medina presenta **"Maté a mi madre a cacerolazos"** donde una angustiada actriz, quien fuera en épocas pasadas reconocida y admirada, es abandonada por todos, y "La Caída de La Pantera", donde cuatro mujeres al borde de un abismo reflejan sus temores, angustias, motivos y la profunda psicología de quien está a punto de lanzarse al vacío.

Adicionalmente, **Rodrigo** se integrará al escenario en la obra **"El Ángel De La Culpa"**, la historia de Charly, un viejo detective que tiene que atender un extraño asesinato cometido en un lujoso apartamento, un crimen, aparentemente pasional, entre un jovencito y su amante. Pieza orientada al público adulto, escrita por el dramaturgo chileno Marco Antonio de la Parra, que apela a la doble moral, la hipocresía y la intolerancia.

Bono de apoyo: Boletería: General \$30000/ Estudiantes y 3ra edad: \$20.000

##### **PROGRAMACION: TRES DECADAS DE LUCHA Y RESISTENCIA** 

##### **SEDE PALERMO (Calle 45 A No. 14-37)** 

##### OBRA: El Yury - Martes 18 y 25 de septiembre a las 7:30 p.m. 

##### OBRA: Carta a Jamaica (ESTRENO PARA IDARTES)- Miércoles 19 de septiembre a las 7:30 p.m. 

##### OBRA: La profesora Rosalba Scholásticus- Jueves 20 de septiembre a las 7:30 p.m.- 18, 19 y 20 de octubre a las 7:30 p.m. 

##### OBRA: Maté a mi madre a cacerolazos- Viernes 21 de septiembre   a las 7:30 p.m. 

##### OBRA: Teseo- Sábado 22 de septiembre a las 6:00 p.m. 

##### OBRA: Ni mierda p\`al perro- Sábado 22 de septiembre a las 7:30 Pm 

##### OBRA: La Caída de la pantera- 11, 12, 13, 25, 26 y 27de octubre a las 7:30 p.m. 

#####  Lecturas dramáticas amores de trompetica y violincito- 29 de septiembre, 4, 5 y 6 de octubre a las 6:00 p.m.  y 7:30 p.m. 

##### [[**SEDE [GALERIAS (Cra. 23 No. 50-66](https://maps.google.com/?q=GALERIAS+(Cra.+23+No.+50-66&entry=gmail&source=g))**]] 

##### OBRA: El ángel de la culpa - 20, 21 y 22 de septiembre a las 7:30 p.m. 

##### OBRA: Ni mierda p\`al perro- 4,5, 6,11,12 y 13 de octubre a las 7:30 Pm 
