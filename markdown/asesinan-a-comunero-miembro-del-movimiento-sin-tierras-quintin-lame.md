Title: Asesinan a Comunero miembro del Movimiento Sin Tierras Quintín Lame
Date: 2017-01-10 11:34
Category: DDHH, Nacional
Tags: asesinato indígena, Movimiento Sin Tierras Manuel Quintín Lame
Slug: asesinan-a-comunero-miembro-del-movimiento-sin-tierras-quintin-lame
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Indígenas-del-Norte-del-Cauca-e1464380469118.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Los Mundos de Hachero] 

###### [10 Enero 2017] 

**El pasado 6 de enero fue asesinado en el corregimiento El Palo, en Cauca, Olmedo Pinto García, comunero e indígena Nas**a, que hacía parte del Movimiento de Los Sin Tierra Nietos de Manuel Quintín Lame. Los hechos se dieron sobre las 11:30pm, cuando Olmedo se dirigía hacía su casa y fue abordado por desconocidos que le propiciaron 3 heridas con arma blanca.

El indígena falleció mientras era trasladado al Hospital de Caloto debido a la gravedad de las heridas. De acuerdo con miembros del Movimiento de Los Sin Tierra Nietos de Manuel Quintín Lame, no se tenía conocimiento de alguna amenaza en contra de Olmedo, sin embargo, afirman que **este hecho puede hacer parte de la persecución y estigmatización a esta organización, que ya ha cobrado la vida de varios miembros.**

A su vez, en un comunicado de prensa emitido por el Movimiento de Los Sin Tierra Nietos de Manuel Quintín Lame, señalan  “No descartamos que este hecho haga parte de una estrategia de exterminio selectivo a líderes sociales y lo hagan pasar como un caso re riñas y robos”, agregando que “**crecen los temores por los casos ocurridos en el Norte del Cauca en donde hay atentados de muerte, amenazan a los líderes y dirigentes sociales pertenecientes a Marcha Patriótica**”. Le puede interesar: ["Aldemar Parra, líder social del Hatillo, en Cesar, fue asesinado](https://archivo.contagioradio.com/34435/)"

Olmedo Pinto era comunero Nasa del resguardo indígena de Huellas, en Caloto Cauca. Además de pertenecer al Movimiento de Los Sin Tierra Nietos de Manuel Quintín Lame, hacía parte de la Coordinadora Nacional de Pueblos Indígenas (CONPI) y del Movimiento Marcha Patriótica.

Miembros del resguardo Huellas y del MST Nietos de Manuel Quintín Lame, exigen que este hecho no quede en la impunidad, que se encuentre a los culpables de la muerte de Olmedo Pinto y que las respectivas autoridades hagan una investigación profunda sobre este asesinato. Le puede interesar: ["Colombia reportó 85 asesinatos contra defensores de DDHH en el 2016"](https://archivo.contagioradio.com/colombia-reporto-85-asesinatos-contra-defensores-de-ddhh-en-el-2016/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
