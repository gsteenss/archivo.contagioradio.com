Title: Grandes retos para mesa ELN-Gobierno en sexto ciclo de conversaciones
Date: 2018-07-03 16:35
Category: Nacional, Paz
Tags: ELN, Gobierno, Iván Duque, La Habana, Mesa de Conversaciones
Slug: grandes-retos-para-mesa-eln-gobierno-en-sexto-ciclo-de-conversaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/98c5ba09-bb46-4488-8881-91d39d3798c8.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tomas García Laviana] 

###### [03 Jul 2018] 

Inicia el sexto ciclo de conversaciones entre el ELN y el gobierno nacional con diferencias muy marcadas, tras el empalme que se hizo de los gobiernos Santos-Duque. Un escenario que, de acuerdo a Jaime Díaz, director de la Corporación Podion, solo superará las adversidades si el presidente electo garantiza **condiciones para que la mesa continúe y comprende que no se puede “atar la soga” al cuello del ELN imponiendo condiciones**.

De acuerdo con el analista, tanto el ELN como el gobierno de Santos, demostraron las voluntades por alcanzar máximos en la mesa de La Habana, sin embargo, debido a las dilataciones de ambas partes la mesa quedo en condiciones difíciles, incluso paras que se reafirmen la continuidad de la misma.

### **Lo que Duque debe entender de la Mesa de La Habana** 

Jaime Díaz aseguró que la lectura que hace Iván Duque es “desafortunada” al imponer condiciones a la guerrilla del ELN, hecho que evidencia que no existe una voluntad por dialogar sino de someter a una guerrilla **“derrotada”. Premisa que para el analista es falsa y deja de lado las intenciones que ha tenido ELN por continuar en el proceso de paz**.

“Hay que ceder de lado y lado en beneficio del país. Lo que se busca con esto no es el beneficio del ELN ni estrictamente el del gobierno, es el beneficio para todas y todos los colombianos” afirmó Díaz. (Le puede interesar: ["Incertidumbre en la mesa ELN-Gobierno con la presidencia de Iván Duque"](https://archivo.contagioradio.com/se-sentara-duque-a-dialogar-en-la-mesa-con-eln/))

En un comunicado de prensa el ELN afirmó que aún hay diferencias que impiden que se geste una firma al cese bilateral y aseguró que si bien han existido avances en la creación de los mecanismos de participación social, aún hay dificultades sobre la amplitud de la participación de diversos territorios y sectores sociales.

Además, Jaime Díaz aseguró que la participación social que ha tenido **la ciudadanía en este proceso de paz y sobretodo las comunidades más afectadas por el conflicto armado**, no puede ser dejada de lado ni ignorada por parte del entrante presidente. En ese mismo sentido el analista hizo un llamado al ELN para que aumente las acciones humanitarias, como al gobierno para que genere mayor confianza en la mesa de conversaciones.

<iframe id="audio_26876930" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26876930_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
