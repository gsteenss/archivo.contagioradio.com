Title: Militares detenidos respaldan el proceso y la Jurisdicción Especial de Paz
Date: 2016-10-10 12:34
Category: Judicial, Nacional
Tags: Acuerdo de paz de la habana, FARC, Juan Manuel Santos, Jurisdicción Especial de Paz
Slug: militares-detenidos-respaldan-el-proceso-y-la-jurisdiccion-especial-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/carcel-militar-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elpais] 

###### [10 Oct 2016]

Un grupo de integrantes del Ejército Nacional detenidos por diversos hechos, emitió un comunicado en el que afirman su respaldo al proceso de conversaciones de paz y recalcaron que están dispuestos a contribuir con el esclarecimiento de los hechos por los cuales fueron procesados, juzgados y condenados en el marco de la **[Jurisdicción Especial de Paz](https://archivo.contagioradio.com/?s=jurisdicci%C3%B3n+especial+de+paz)**. Además manifestaron que no son representados por quienes dicen que hablan en su favor.

El comunicado, que está acompañado de **41 firmas y respaldado por más de 800 militares condenados por varios delitos** entre los que se encuentran las ejecuciones extrajudiciales, mal llamadas “falsos positivos” también da una voz de respaldo al presidente Santos y reconoce al [General Mora Rangel](https://archivo.contagioradio.com/renuncia-de-asesores-de-mora-rangel-no-tiene-fundamento-real-analistas/) como su representante en la mesa de conversaciones, afirman que “lo reconocemos como nuestro legítimo representante”.

El comunicado además desautoriza a quienes en este contexto dicen estar hablando por ellos. En ese sentido afirman que el objetivo de ese comunicado es expresar a los colombianos su respaldo al [proceso de paz](https://archivo.contagioradio.com/?s=acuerdo+final) y **evitar que las organizaciones y personas que no los representan sigan confundiendo a la opinión pública** “q*ue sea esta la oportunidad para evitar que todas esas personas y organizaciones políticas que no nos representan continúen confundiendo a la opinión pública acerca de nuestros intereses*”.

Este documento se dio a conocer luego de las propuestas hechas por el senador Uribe Vélez, quien desde el principio manifestó su desacuerdo con el mecanismo de Justicia plasmado en los acuerdos y además pidió alivios judiciales para los militares, lo cual, para muchas víctimas, **significa impunidad para los crímenes cometidos por los militares y la imposibilidad de esclarecer los hechos que rodearon estos mismos**, incluso conociendo los móviles y el contexto en el que se cometieron.

[Militares Presos Respaldan El Acuerdo Entre Gobierno y Las Farc](https://www.scribd.com/document/327082422/Militares-Presos-Respaldan-El-Acuerdo-Entre-Gobierno-y-Las-Farc#from_embed "View Militares Presos Respaldan El Acuerdo Entre Gobierno y Las Farc on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_98137" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/327082422/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Pdnfd594PiP0Hu1xQy2n&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6579139314369074"></iframe>
