Title: Con marcha, paro estudiantil conmemora 90 años de masacre de las bananeras
Date: 2018-12-06 13:17
Author: AdminContagio
Category: Educación, Movilización
Tags: Bananeras, educacion, marcha estudiantil, Paro Estudiantil
Slug: masacre-de-las-bananeras
Status: published

###### [Foto: Contagio Radio] 

###### [6 Dic 2018] 

Con el marco de la conmemoración de la masacre de las bananeras, ocurrida hace 90 años, y los 55 días del paro nacional, los estudiantes universitarios marcharán nuevamente en rechazo a la aprobación de la **Ley Bienal de Garantías, así como la Ley de Financiamiento**. En conjunto con otros sectores, llegarán hasta la Plaza de Bolívar dónde se realizará la lectura de una declaración política y un pequeño concierto.

Los estudiantes rechazaron la aprobación de la Ley Bienal de Garantías, en la que se dispone que las Instituciones de Educación Superior públicas podrían recibir hasta un billón de pesos. Aunque muchos calificaron la ley como un avance, congresistas y activistas estudiantiles han señalado que esos recursos no están asegurados porque dependen de la voluntad de Alcaldes y Gobernadores para ser entregados.

> Eso no es cierto.  
> Esos dos billones no son seguros, porque todo proyecto de inversión con regalías debe ser aprobado en los OCAD (altamente cuestionados) y nada garantiza que los proyectos de educación sean aprobados.  
> Toca modificar la ley estatutaria para que así sea. <https://t.co/nQDwt6AIcd>
>
> — David Racero (@DavidRacero) [6 de diciembre de 2018](https://twitter.com/DavidRacero/status/1070642934515683329?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
**Natalia Ramírez, una de las voceras nacionales de la Unión Nacional de Estudiantes por la Educación Superior (UNEES)**, afirma que con esos recursos la educación superior compite con las regiones; de forma que el Gobierno traslada a los entes regionales y locales la responsabilidad del Estado de garantizar la educación. (Le puede interesar: ["Con novenas y villancicos paro estudiantil se mantiene en diciembre"](https://archivo.contagioradio.com/paro-estudiantil-diciembre/))

### **Por los 90 años desde las Bananeras y los 55 días del paro estudiantil** 

Ramírez recordó que este **5 y 6 de diciembre se conmemoran 90 años de la masacre de las bananeras, así como 55 días del paro estudiantil**, razón que motivó a los estudiantes a reivindicar la lucha por los derechos laborales y humanos, a lo que se suma la situación de la Mesa de Concertación con el Gobierno, que según la integrante de la UNEES, no tiene grandes avances, porque “la voluntad del Gobierno para llegar a acuerdos con los estudiantes es mínima”.

La vocera de la UNEES sostuvo que hasta el día miércoles, la filarmónica de la Universidad Nacional de Colombia, Doctor Krapula y 1280 almas habían confirmado su participación en un concierto que se desarrollaría en la Plaza de Bolívar, sin embargo, **la secretaría de Gobierno de Bogotá no dio la autorización para el desarrollo de la actividad**. Pese a esta situación, los estudiantes realizarán un encuentro, en el que leerán una declaración política y tocará la orquesta de la Universidad Nacional.

<iframe id="audio_30609132" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30609132_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
