Title: ESMAD los dejo sin "Casa de los sueños"
Date: 2015-07-17 14:51
Category: Comunidad, Nacional
Tags: Abuso de poder, Casa Cultural 18 de Diciembre, desalojo forzoso, ESMAD, marcha patriotica, Suba, Unidad de Procesos Populares de Bogotá
Slug: esmad-los-dejo-sin-casa-de-los-suenos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Casa-18-dic.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Edith Parada 

<iframe src="http://www.ivoox.com/player_ek_4944869_2_1.html?data=lZ6hlp2afY6ZmKiak52Jd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fxcrgw9HTrtCfxcqYzsaPaZOmxMbgw5DIqYzg0NiY1drJaaSnhqee0diJdpOZk6iYzsaPh8LnwpCw19HYudOhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Guillermo Pérez, Unidad de Procesos Populares de Bogotá] 

###### [17, Julio, 2015] 

“El desastre que armó ayer el ESMAD no tiene palabras", expresa con indignación, Guillermo Pérez, integrante de la Unidad de Procesos Populares de Bogotá, refiriéndose al desalojo adelantado este jueves, sin previo aviso, sobre la Casa Cultural 18 de Diciembre en Suba, la misma que el joven acompaña desde hace un tiempo.

Con fotos y haciendo indagaciones sobre cuántas personas estaban en la casa, desde el día anterior, agentes de la policía inspeccionaron el lugar y realizaron las labores de inteligencia para planear el desalojo.

Al día siguiente, las tanquetas del ESMAD llegaron para acordonar la zona desde las 6:30 de la mañana, dos horas después se empezó el operativo. Primero rompieron ventanas y luego destruyeron las puertas de la casa para poder ingresar al lugar donde niños y niñas de la localidad de Suba asistían para realizar talleres de arte y construcción de paz.

[![Casa de los sueños 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Casa-de-los-sueños-1.png){.wp-image-11405 .size-full .alignleft width="271" height="424"}](https://archivo.contagioradio.com/el-esmad-los-dejo-sin-la-casa-de-los-suenos/casa-de-los-suenos-1/)

La dueña de la casa, una mujer mayor que acaba de salir de graves problemas de salud, alcanzó a salir de su hogar antes de que empezara el violento operativo del ESMAD. Once jóvenes se quedaron a resguardar la casa y hacer un acción de resistencia de forma pacífica, sin embargo, todos salieron golpeados y agredidos, situación que luego fue registrada en Medicina Legal.

“El abuso de fuerza fue desmedido, eran once jóvenes entre ellas dos mujeres, contra dos escuadrones del ESMAD”, relata Guillermo. Estos jóvenes, son líderes comunitarios que trabajan de la mano de niños, niñas y personas de la tercera edad que encontraron en esa humilde casa esquinera un espacio donde se construyen sueños.

Nora Moreno, es la dueña de la casa desde 1996, para poder pagar la deuda de su inmueble  pidió un préstamo de 4 millones de pesos, de los cuales ha pagado 23 millones, el problema, según cuenta su hijo Antonio Torres (quien ahora se encuentra retenido), es que el inmueble habría sido vendido a otras personas, tras el presunto no pago del préstamo con el cual se compró la casa, es por eso que desde 2009 se empieza todas las acciones para desalojar el predio al ser declarada como un bien a embargar.

“No es posible que usureros que quitan casas a familias sean avalados por el Estado”, dice el integrante la Unidad de Procesos Populares de Bogotá. Él resalta que no hubo agresión a la Fuerza Pública, aun así, los jóvenes son acusados de “agresión a servidor público”, ahora están en los juzgados de Paloquemao para que se les impute cargos y se legalice su captura como si fueran  viles delincuentes, como algunos medios han reseñado, señala Guillermo.

\[caption id="attachment\_11381" align="aligncenter" width="500"\][![Desalojan Casa Cultural 18 de Diciembre en Suba, Corinto](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/11722256_1053047288038716_4906329120469884496_o1.jpg){.wp-image-11381 width="500" height="375"}](https://archivo.contagioradio.com/10-jovenes-detenidos-y-detenidas-tras-desalojo-de-casa-cultural-18-de-diciembre-en-suba/11722256_1053047288038716_4906329120469884496_o-2/) Desalojan Casa Cultural 18 de Diciembre en Suba, Corinto\[/caption\]

El operativo del ESMAD dejó una familia en la calle, los muebles de la casa han sido repartidos como pudieron en diferentes hogares del barrio. Mientras tanto en los juzgados de Paloquemao, los once jóvenes son apoyados por su comunidad y otras organizaciones sociales, exigiendo garantías para los capturados que se suman a los 17 líderes sociales, defensores de derechos humanos, periodistas, profesores y constructores de paz de otros movimientos sociales que  también fueron retenidos.

Por el momento, las clases de arte, canto y pintura, de la casa de los sueños quedarán estancados.
