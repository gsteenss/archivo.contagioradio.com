Title: Curando las taras del periodísmo
Date: 2015-09-03 19:00
Category: Opinion, Ricardo
Tags: Manipulacion mediatica, Monopolio de medios, Prensa parcial
Slug: curando-las-taras-del-periodismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/mafada.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Quino 

##### **[Ricardo Ferrer Espinosa](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/) - @ferr\_es**

###### [03 Sep 2015]

No nos digamos mentiras: HOY PREDOMINA EL PERIODISMO AHISTÓRICO: El despliegue mediático sobre la crisis de inmigrantes en Europa omite señalar la responsabilidad histórica de “occidente” en la espantosa crisis humanitaria. Fue la “Alianza Atlántica” la que decidió disolver países enteros como Afganistán, Irak, Libia, Siria, Yemen. Luego irán por Egipto. De nuevo, como en 1886, los “cultos” europeos trazan las nuevas fronteras de los Estados. El vocero militar y mediático aplaude la invasión de países y luego sigue el saqueo de sus recursos ante el silencio de la ONU. Mientras la prensa y las bolsas de valores bendicen a los gobiernos títeres, la población civil es desechada. Familias enteras quedan a su suerte, sin otra alternativa que lanzarse al mar y cruzar fronteras. Paradójicamente buscan asilo en los países que les destruyeron el propio.

En Colombia, un ejercicio de memoria, desde el periodismo, es necesario y urgente. Si usted le pregunta a un adolescente quién fue Jorge Eliécer Gaitán, muy pocos acertarán con la respuesta. Las propuestas sociales de Gaitán no son insumo de la prensa y menos ahora, cuando la materia de “Historia” desaparece en casi todos los programas académicos. La prensa y la educación nos están quitando los referentes de lo que pudo ser una nación diferente. La crónica histórica de Colombia tiene interesantes contextos y muy pocos redactores \[1\].

LOS TEMAS DE PLANEACIÓN LOS MONOPOLIZA UNA MINORÍA: En la prensa colombiana, nunca encontraremos una invitación a toda la comunidad para que se implique masivamente en la toma de las decisiones cruciales. Es una minoría la que decide por nosotros y ese abuso lo aceptamos con resignación. En vez de incitarnos a participar, la prensa nos muestra, en cambio, a los líderes, a los caudillos, la gente inspirada “que si sabe del tema”. Usted que aspira a tener una pensión justa por su trabajo, no es la persona más idónea para opinar sobre el tema. Mientras se desecha la opinión del trabajador, ellos nos “orientan” desde la prensa económica con la entrevista pagada por el gerente de un fondo de pensiones privado.

Votamos cada cuatro años y creemos que eso es democracia, Delegamos la iniciativa, el debate, la aprobación y ejecución de los proyectos, en la “clase política”, de la cual renegamos, pero la seguimos aupando. Ya es hora de hurgar en la verdadera agenda de los temas que son cruciales en la construcción del país, una visión de futuro ajena a la voracidad del empresario venal y sobre todo, es hora de recuperar el liderazgo colectivo. Nuestro futuro es un tema tan serio que no debe quedar en manos de los bancos, las empresas y mucho menos como proyecto de las empresas mediáticas. Como ejemplo, basta ver el laberinto de información que debe digerir una familia que busca vivienda, hasta que por fin accede a un techo propio.

El ordenamiento territorial, el gasto público, la seguridad social, los planes de desarrollo, las políticas de defensa y protección civil, están en agenda nacional. Pero necesitan más presencia de los vecinos, profesores curiosos, conductores de buses y comunidades, músicos callejeros y amas de casa, porque el país les pertenece.

Justo ahora, cuando nos endulzan los oídos con promesas de cambio y “un nuevo país”, debería surgir un periodismo más prospectivo, constructor de prójimos y conciudadanos. Será hora de plantar cara a los “tanques de pensamiento”, fundaciones y caudillos que únicamente nos ofrecen la agenda para esclavos.

Criados como borregos, por los medios y la escuela, los ciudadanos somos más vulnerables ante los depredadores. Informados, unidos en resistencia y memoria, podemos revertir el fatalismo de la historia que nos han vendido.

\[1\] Alfredo Molano Bravo: libros, obra y biografía.  
http://www.lecturalia.com/autor/17852/alfredo-molano-bravo
