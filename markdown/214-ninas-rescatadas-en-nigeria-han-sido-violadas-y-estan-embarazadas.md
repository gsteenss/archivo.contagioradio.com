Title: 214 niñas rescatadas en Nigeria han sido violadas y están embarazadas
Date: 2015-05-05 16:58
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: 214 niñas rescatad en Nigeria están embarazadas, 293 niñas rescatas en Nigeria, Boko Haram, Fondo para la Población de Naciones Unidas, Lami Musa, Nigeria
Slug: 214-ninas-rescatadas-en-nigeria-han-sido-violadas-y-estan-embarazadas
Status: published

###### Foto:Huffingtonpost.com 

De las 293 niñas y mujeres rescatadas por Nigeria, **214 están embarazadas y la gran mayoría han sufrido violaciones y abusos sexuales**, en prácticas referidas a la esclavitud sexual.

Ninguna de ellas pertenece al grupo de las 219 niñas secuestradas en la escuela de Chibok, hecho que conmocionó al mundo y marco las elecciones del país subsahariano debido al poco interés de su propio presidente en el momento del suceso.

Las mujeres rescatadas responden a la **ofensiva** que está iniciando **Nigeria en colaboración con Níger, Chad y Camerún** en contra de los bastiones del grupo armado islamista **Boko Haram** en la región de Sambisa.

Para **Amnistía Internacional** desde 2014 han sido **secuestradas más de 2.000 mujeres** y niñas, y en total en la última ofensiva han sido 700 las liberadas, casi la totalidad ha sufrido explotación sexual y violaciones sistemáticas por parte de sus captores.

Testimonios como el de **Asabe Aliyu** de 23 años o el de **Lami Musa de 19 años**, relatan como las habían convertido en **esclavas sexuales de los combatientes**, “…Me convirtieron en un objeto sexual. Hacían turnos para acostarse conmigo. Ahora estoy embarazada y no sé quién es el padre…”, “*…Cada día moría alguna de nosotras y sólo esperábamos que llegara nuestro turno…*”.

Según el médico **Babatunde Osotimehin del Fondo para la Población de Naciones Unidas**, la gran mayoría de mujeres y niñas han dado positivo en las pruebas del embarazo, y muchas de ellas ya tienen signos visibles de su embarazo fruto de las constantes violaciones.

**Boko Haram e**s una milicia islamista radical**,** cuyo principal objetivo es la imposición de la Sharia o ley islámica, que es considerada en el norte como justicia informal, pero rechazada frontalmente por los estados del sur de mayoría cristiana.

Aunque la **Sharia es considerada como la ley islámica,** es más un modo de vida interpretable, que es tomada como patrón de grupos extremistas para así, crear del código de conducta, un dogma sobre lo que se puede o no hacer en la vida.

La milicia se crea en 2002, tomando posiciones en el norte del país. Su acción mas sonada fue en abril del 2014 cuando tomaron una escuela secuestrando a mas de 200 niñas con el objetivo de denunciar la política educativa occidental que se aplica en dichas escuelas.
