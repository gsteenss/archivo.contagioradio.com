Title: Por crisis carcelaria piden medidas cautelares ante CIDH
Date: 2020-04-22 14:48
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Covid-19
Slug: por-crisis-carcelaria-piden-medidas-cautelares-ante-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/carcel-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Archivo {#foto-archivo .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Fundación Comité de Solidaridad con los Presos Políticos (FCSPP) y el senador Iván Cepeda solicitaron medidas cautelares a la Comisión Interamericana de Derechos Humanos (CIDH) para proteger la vida de las personas recluidas en las cárceles de Colombia. La solicitud espera cobijar 10 centros penitenciarios del país con 14 medidas específicas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La población vulnerable y de alto riesgo privada de la libertad asciende a 16,265 personas

<!-- /wp:heading -->

<!-- wp:paragraph -->

El abogado Óscar Ramírez de la Fundación recuerda que debido al riesgo que supone el hacinamiento en las cárceles y la ausencia de condiciones para enfrentar el Covid 19, el pasado 13 de marzo hubo una jornada nacional de protesta que culminó con la masacre de por lo menos 23 personas en La Modelo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Más de un mes después del hecho, de acuerdo a Ramírez, más de 40 casos del virus se han presentado en varias cárceles del país dando como resultado la muerte de 3 internos hasta el momento. Adicionalmente, se siguen denunciando la ausencia de condiciones para atender la emergencia sanitaria al interior de los penales. (Le puede interesar: ["Ninguna Cárcel en Colombia está lista para afrontar Covid 19"](https://archivo.contagioradio.com/ninguna-carcel-en-colombia-esta-lista-para-afrontar-covid-19-porque/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo a cifras recogidas por la FCSPP del Ministerio de Justicia, la población vulnerable y de alto riesgo que actualmente está privada de la libertad es de 16.265 personas; cifras que toman en cuenta a adultos mayores de 60 años, madres gestantes, población con patologías mentales, con VIH-SIDA, cáncer, diabetes o hipertensión arterial.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A ello se suman las críticas al Decreto 546 de 2020, con el que se esperaban medidas para reducir el hacinamiento y permitir que la población vulnerable enfrente la contingencia desde sus casas, pero que solo beneficiaría a unas 4 mil personas respecto de "las 39 mil personas pribadas de la libertad en sobrecupo", según cifras de la Fundación.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IPCoficial/status/1252754783456419840","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IPCoficial/status/1252754783456419840

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Medidas para proteger la salud de los internos

<!-- /wp:heading -->

<!-- wp:paragraph -->

Algunas de las medidas solicitadas a la CIDH sobre diez centros de reclusión en el país incluyen evaluar el sistema de exclusiones estipuladas por el mencionado decreto de presidencia para lograr mayor impacto respecto del hacinamiento, así como que se adopten medidas para fortalecer las capacidades operativas del Instituto Nacional Penitenciario Colombiano (INPEC), encargado de de los penales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, se solicita suministrar elementos de primera necesidad, higiene y alimentación para que los internos enfrenten la pandemia, y en la misma medida, que se dote a los penales con elementos de bioseguridad necesarios para evitar contagios. Igualmente, piden que se habiliten canales de comunicación que permitan a las personas privadas de la libertad estar en contacto con sus seres queridos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, la Fundación y el senador solicitan el establecimiento de una mesa o sesiones de trabajo virtual a nivel nacional con personas privadas de la libertad para evitar protestas en las cárceles, mejorar la comunicación con ellos y ellas, así como evitar la desinformación y el miedo al interior de los espacios de reclusión.

<!-- /wp:paragraph -->

<!-- wp:heading -->

¿Cuándo se espera una decisión de la CIDH?
------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El abogado de la [Fundación](http://www.comitedesolidaridad.com/es/content/solicitan-medidas-cautelares-la-cidh-en-favor-de-las-personas-privadas-de-la-libertad) señala que la CIDH tendrá que revisar esta petición así como otras que se han hecho sobre el sistema carcelario, pero la decisión sobre la solicitud de medidas cautelares taradaría por lo menos un mes en resolverse. No obstante, sostiene que la situación generada por el Covid-19 es probable que también duré más que ese tiempo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En consecuencia, espera que la Comisión acepte las medidas, o pida información al Estado respecto a las razones expuestas en la solicitud. Esta opción, sería igualmente válida dado que Ramírez argumenta que el INPEC ni si quiera tienen un cuadro epidemiológico claro con las enfermedades preexistentes en las cárceles. (Le puede interesar: ["Prisionero político relata el drama de vivir con el COVID en la cárcel de Villavicencio"](https://archivo.contagioradio.com/prisionero-politico-relata-el-drama-de-vivir-con-el-covid-en-la-carcel-de-villavicencio/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
