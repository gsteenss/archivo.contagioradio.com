Title: Integrantes de las FFMM habrían intentado abusar sexualmente de 2 niñas en el Mango Cauca
Date: 2015-12-02 09:18
Category: DDHH, Nacional
Tags: Abuso sexual, Cauca, FFMM, Juan Manuel Santos, mango
Slug: integrantes-de-las-ffmm-habrian-intentado-abusar-sexualmente-de-2-ninas-en-el-mango-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/el-mango-cauca-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elpais 

###### [2 Dic 2015]

[Según la información de la comunidad este Martes en horas de la noche, **3 militares habrían intentado violar a una joven de 20 años y a una niña de 13 años de edad**, cuando se dirigían a su lugar de habitación en el sector de Bujio, ubicado a 10 minutos del casco urbano del corregimiento el Mango en Argelia Cauca.]

[La denuncia es clara en afirmar que por lo menos 3 militares, fueron los que intentaron arrinconar a las niñas en el sector conocido como “la piscina” entre el Mango y la vereda el Bujio, afortunadamente un conductor que transitaba por el sector se percató de la situación e impidió que se concretara un abuso sexual. Hasta el momento las niñas atacadas no han podido narrar los hechos por la fuerte afectación de la que fueron víctimas.]

[Una vez conocida la situación la comunidad intentó llegar hasta el lugar de campamento de los militares pero fueron atacados con disparos que impactaron uno de los vehículos en el que se movilizaban los campesinos.]

[Para la comunidad es habitual que los militares ronden el pueblo armados y sin uniformes por lo que fue fácil que las personas afectadas y los testigos lograran identificarlos. El relato del niño que acompañaba las jóvenes y una persona que transitaba por el lugar en una camioneta lograron frustrar el intento de violación.]

[Una comisión de campesinos habitantes del corregimiento se dirigirán hacia Campo Alegre aproximadamente en una hora para intentar establecer la identidad de los responsables del ataque y exigir que se asuma la responsabilidad y se haga justicia por este ataque.]

[A esta hora las menores de edad están recibiendo atención médica y psicológica.  ]
