Title: Animalistas no desistirán de su causa pese a amenaza de "Águilas Negras"
Date: 2015-03-12 21:04
Author: CtgAdm
Category: Animales, DDHH, Nacional
Tags: Aguilas Negras, Amenazas, Animales, Animalistas, Gustavo Petro, Natalia Parra
Slug: animalistas-no-desistiran-de-su-causa-pese-a-amenaza-de-aguilas-negras
Status: published

##### Foto: noticiasrcn.com 

<iframe src="http://www.ivoox.com/player_ek_4207106_2_1.html?data=lZedmZaUeo6ZmKiak5aJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc%2FdzsbZy9jYpdSfz9SYxsrXrdToyteSpZiJhZLijMnSjdjZb8TV1tjOjdXJt8afwpDOz8rSpdvV1JCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Natalia Parra, Plataforma ALTO] 

“Quienes enviaron el panfleto buscan generar terror y que dejemos de luchar por los animales, pero ese es nuestro sentido de vida y no tenemos tranquilidad sino intentamos hacer algo por los animales… **este país tiene un déficit moral con ellos, y por lo tanto no podemos desfallecer, así nos lleguen amenazas**”, es la respuesta de Natalia Parra, directora de la Plataforma ALTO e integrante del programa Onda Animalista de Contagio Radio, y además, es una de las personas amenazadas por el panfleto firmado por las Águilas Negras,[(Ver nota relacionada)](https://archivo.contagioradio.com/animales/animalistas-ahora-son-blanco-de-amenazas-por-parte-de-aguilas-negras/)

Además de ella, Carlos Crespo, Batman Camargo, Juan Parraga, Jesús Merchán, Andrea Padilla, fueron los líderes y lideresas animalistas que se encuentran amenazados. Ellos ya se reunieron e hicieron público el siguiente comunicado frente a su situación.

[![comunicado animalistas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/comunicado-animalistas.jpg){.aligncenter .wp-image-5903 width="533" height="426"}](https://archivo.contagioradio.com/animales/animalistas-no-desistiran-de-su-causa-pese-a-amenaza-de-aguilas-negras/attachment/comunicado-animalistas/)

“Ya estamos muy tranquilos, pese a ser una situación a la que no están acostumbrados”, afirma Parra, quien enumeró tres elementos que tiene en común las personas amenazadas: el primero es el **enfoque por la lucha antitaurina**, respecto a ese tema, la animalista señala que “se sabe la molestia que eso genera en ciertos sectores económicos del país, amparados por  las bandas criminales”.

Por otro lado, el segundo punto que tiene en común, es el **respaldo al alcalde Gustavo Petro, debido a su compromiso con  los animales**, por su política “Bogotá humana con la fauna”.

Otro de los elementos comunes que señala la defensora de los animales, es su **apoyo al proceso de paz,** “hemos apoyado la paz, movilizándonos en marchas, usando el lema de que los animales también son víctimas del conflicto”.

Pese a los problemas de seguridad que esto les significa a los animalistas, Natalia Parra resalta que la lucha por los animales se fortalece y al contrario, ven la necesidad de hacer públicos estos hechos para contrarrestar las amenazas.
