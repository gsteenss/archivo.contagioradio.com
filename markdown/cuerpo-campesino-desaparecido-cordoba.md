Title: Denuncian desplazamiento de por lo menos 200 familias al sur de Córdoba
Date: 2019-03-25 18:16
Author: CtgAdm
Category: DDHH, Nacional
Tags: Armados, cordoba, desaparición, desplazados
Slug: cuerpo-campesino-desaparecido-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Córdoba.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  El Heraldo] 

###### [25 Mar 2019] 

El pasado domingo 24 de marzo fue encontrado el cuerpo de un campesino habitante del corregimiento de Versalles, municipio de San José de Uré (Córdoba), con él otros dos labriegos habrían sido desaparecidos desde el viernes 22 de marzo por hombres armados aún sin identificar. La denuncia fue emitida por la Fundación Cordoberxia, que desde el viernes ha advertido desplazamientos forzados en el territorio, situación que se repitió el domingo, con cerca de 200 familias.

Según la Fundación, hombres armados estarían amenazando las familias para abandonar sus predios desde el viernes; adicionalmente, los violentos habrían extraído de sus viviendas a tres campesinos del corregimiento de Versalles. El domingo en horas de la tarde fue encontrado el cuerpo sin vida de uno de los trabajadores rurales, siendo sus restos trasladados a las entidades oficiales en el municipio de Montelibano.

Los otros dos trabajadores aún no han sido encontrados pero se sospecha que también fueron asesinados; situaciones que fueron denunciadas por la Fundación, y ante las cuales denuncian que se han realizado las respectivas acciones de "búsqueda y rescate por parte del Estado y los órganos competentes".

### **Desplazamientos y conflictividad en el sur de Córdoba**

De igual forma, desde el viernes pasado la Fundación ha advertido sobre desplazamientos de cerca de 160 familias campesinas en el cañón del Río San Jorge, sur de Córdoba, flagelo al que se han sumado la totalidad de la población de las veredas  Río sucio, Soledad y Rogero en la misma región.  (Le puede interesar: ["Denuncian desplazamiento forzado de 160 familias en C."](https://archivo.contagioradio.com/desplazamiento-forzado-familias-cordoba/))

Por esta razón diferentes organizaciones defensoras de derechos humanos han pedido a la Procuraduría y Denfensoría del Pueblo hacer presencia en el territorio para constatar la situación, evitar nuevos desplazamientos y buscar a las personas desaparecidas; de igual forma, piden a la Fiscalía iniciar las investigaciones para dar con los responsables de los hechos.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
