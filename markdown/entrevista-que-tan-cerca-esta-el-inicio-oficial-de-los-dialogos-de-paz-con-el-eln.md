Title: Entrevista: ¿Qué tan cerca está el inicio oficial de los diálogos de paz con el ELN?
Date: 2015-09-09 15:52
Category: Paz
Tags: Canal Capital, conversaciones de paz con el ELN, Gabino, Nicolás Rodrígeuz Bautista, proceso de paz
Slug: entrevista-que-tan-cerca-esta-el-inicio-oficial-de-los-dialogos-de-paz-con-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/gabino.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

En exclusiva para Canal Capital, la televisión pública de Bogotá, y Contagio Radio, expresión radial con enfoque en Derechos Humanos, el máximo comandante del Ejército de Liberación Nacional (ELN) entregará detalles del proceso de paz con el gobierno y los avances de la que sería la formalización de la mesa de conversaciones. Vea el especial completo este miércoles 9 de septiembre durante la emisión nocturna de Noticias Capital.

Igualmente, Gabino abordará temas ambientales y sentará la posición de la guerrilla que comanda sobre el cuidado de la tierra, la relación de la paz con la naturaleza, las corridas de toros e incluso los daños ambientales que algunos sectores atribuyen al accionar subversivo.

https://youtu.be/3sYD4pmZOL0

¿Qué tan cerca está el establecimiento de una mesa de diálogos entre el Gobierno y el ELN? ¿Coincidirán en algún momento ambos proceso de paz con las guerrillas? ¿Qué país sería eventualmente la sede de los diálogos con el ELN?, estos y otros temas serán abordados en la entrevista exclusiva de Canal Capital y Contagio Radio con Nicolás Rodríguez Bautista, “Gabino”.

Aquí podrá ver la señal desde las 8 p.m

<iframe style="border: 0; outline: 0;" src="http://cdn.livestream.com/embed/contagioradioytv?layout=4&amp;height=340&amp;width=560&amp;autoplay=false" width="560" height="340" frameborder="0" scrolling="no"></iframe>

<div style="font-size: 11px; padding-top: 10px; text-align: center; width: 560px;">

[contagioradioytv](http://original.livestream.com/contagioradioytv?utm_source=lsplayer&utm_medium=embed&utm_campaign=footerlinks "Watch contagioradioytv") on livestream.com. [Broadcast Live Free](http://original.livestream.com/?utm_source=lsplayer&utm_medium=embed&utm_campaign=footerlinks "Broadcast Live Free")

</div>

 

Para seguir en vivo la entrevista ingrese a [Contagio Radio](https://archivo.contagioradio.com/envivo.html)
