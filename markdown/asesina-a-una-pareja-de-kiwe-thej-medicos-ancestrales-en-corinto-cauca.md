Title: Asesinan a una pareja de kiwe thë’j (médicos ancestrales) en Corinto, Cauca
Date: 2020-05-29 14:28
Author: CtgAdm
Category: Actualidad, Nacional
Tags: Cauca, Corinto
Slug: asesina-a-una-pareja-de-kiwe-thej-medicos-ancestrales-en-corinto-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/asesinato-de-pareja-de-sabedores-de-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El **Tejido** **de** **Defensa** **de** **la** **Vida** **y** **los** **Derechos** **Humanos** **de** **la** Asociación **de** **Cabildos Indígenas del Norte del Cauca** (ACIN) reportó hace algunas horas el **asesinato de dos** ***kiwe thë’j** (*médicos tradicionales) en el resguardo indígena Páez de Corinto.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CNTI_Indigena/status/1266444484952444934","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CNTI\_Indigena/status/1266444484952444934

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Los **médicos ancestrales** fueron identificados como **María Nelly Cuetia Dagua, de 55 años**, y **Pedro Ángel María Tróchez Medina, de 58 años**; según la comunidad los mayores **fueron sacados en la noche del 28 de mayo de su vivienda en la vereda Los An**des.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente en la mañana de este viernes **sus cuerpos sin vida fueron encontrados con múltiples impacto de arma de fuego** en la vereda Pueblo Nuevo, corregimiento de Río Negro. (Le puede interesar: <https://archivo.contagioradio.com/asesinan-a-edwin-acosta-lider-social-y-minero-de-tiquisio-bolivar/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"***El asesinato de mayores es un acto de destrucción de la memoria** y el conocimiento propios, es la consumación del proyecto de exterminio que pretende arrasar el territorio*", señaló la Asociación de Cabildos (ACIN). (Le puede interesar: <https://archivo.contagioradio.com/a-pocos-kilometros-de-base-militar-binacional-funcionaria-una-pista-del-narcotrafico-en-choco/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar de que hasta el momento se desconocen los responsables de hecho, la Asociación instó a las comunidades indígenas a permanecer en alerta máxima, "**es claro que a medida que se [militariza e](https://www.justiciaypazcolombia.com/wola-congresista-mcgovern-y-lideres-colombianos-de-derechos-humanos-piden-una-revision-de-la-ayuda-militar-ee-uu-a-colombia/)l territorio, más muerte y destrucción se instala en nuestros hogares.** *Vivimos una arremetida contra nuestros proyectos de vida y nuestra pervivencia en el territorio*"

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
