Title: Premios expresarte, por la diversidad y la Inclusión
Date: 2017-12-14 13:27
Category: eventos
Tags: arte, Bogotá, Inclusión, lgtbi
Slug: premios-expresarte-diversidad-arte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/image001-6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Premios expresarte 

###### 14 Dic 2017 

Llega a Bogotá la tercera gala de los **Premios Expresarte**, un espacio cultural que reconoce las expresiones artísticas con contenidos de **diversidad sexual e inclusión**.

En el evento se reconocerán **seis creaciones artísticas inéditas** en literatura, música, danza, artes plásticas, visuales, arte dramático y audiovisual, que fueron seleccionadas por el jurado, tras evaluar **85 propuestas inscritas** a través de la convocatoria realizada en octubre; propuestas que hablan y reflejan la no discriminación en razón a las orientaciones sexuales e identidades de género o diversas.

Durante la gala de premiación se presentarán la imitadora de Gloria Trevi del programa Yo me llamo, Adela Ferrer, Ariana Fallacy, Charlotte Callejas, Grace Kelly, Madorilyn Crawford, Roxana Miranda y Saenz Saens, siete mujeres trans de trayectoria en el arte transformista de Bogotá, y la agrupación las Tupamaras, grupo artístico que fusiona el merengue y la electrónica para ser una plataforma en la que los ritmos periféricos, los bailes extravagantes y las libertades sexuales, tengan cabida en una escena local.

Los Premios Expresarte 2017 tendrán lugar el **jueves 14 de diciembre a las 5:30 p.m.** en el Teatro Jorge Eliecer Gaitán, la entrada es totalmente gratis hasta completar aforo.
