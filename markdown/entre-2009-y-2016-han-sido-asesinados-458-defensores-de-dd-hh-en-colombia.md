Title: Entre 2009 y 2016 han sido asesinados 458 defensores de DD.HH en Colombia
Date: 2017-09-12 19:17
Category: DDHH, Nacional
Tags: colombia, Somos defensores
Slug: entre-2009-y-2016-han-sido-asesinados-458-defensores-de-dd-hh-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/victimas-movice-en-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Sept 2017] 

La organización Somos Defensores dio a conocer su informe “Stop Wars, paren la guerra contra defensores” una recopilación de cifras que dan cuentan del estado actual de la impunidad en Colombia que prevalece para los crímenes cometidos en contra de defensores de derechos humanos y líderes sociales en el país, **y que desde el 2009 hasta el 2016 ha cobrado la vida de 458 defensores**.

### **¿A quiénes asesinan en Colombia?** 

De acuerdo con la información recolectada, en Colombia, los departamentos en donde más se ha asesinado a defensores y líderes sociales son **Cauca con 88 personas, seguido de Antioquia con 74 y Valle del Cauca con 32**, mientras que la población más afectada y vulnerada son las comunidades indígenas a quienes les han asesinado 116 líderes y defensores. (Le puede interesar: ["Víctimas de la masacre de Mapiripan denuncian amenazas de muerte"](https://archivo.contagioradio.com/victimas-de-desplazamiento-en-mapiripan-denuncian-amenazas-contra-su-vida/))

En ese mismo sentido otros de los liderazgos más agredidos en el país son los líderes comunales con **96 asesinatos, los líderes campesinos o agrarios con 50 asesinatos**, los líderes de víctimas con 41 personas asesinadas y los líderes comunitarios y sindicales cada uno con 34 asesinatos.

Frente a las responsabilidades de estos crímenes, Somos Defensores pudo establecer que 306 de los casos fueron cometidos por desconocidos, **95 por paramilitares, 30 por las FARC-EP, 21 por la Fuerza Pública y 6 por el ELN.**

Cifras que, para Somos Defensores, permite establecer que los asesinatos siguen presentándose en territorios donde el conflicto armado todavía existe y sobretodo que las muertes se encuentran ligadas a comunidades rurales.

### **Patrones y responsabilidades** 

Somos Defensores intento establecer un Modus Operandi en los asesinatos a los 458 defensores y líderes sociales, sin embargo encontró que existen grandes vacíos en la información recolectada por parte de la Fiscalía, como que solo en **94 de los 458 casos, se tiene documentación del arma que se utilizó**, mientras que en otros hace falta información relevante como hora de muerte, zona, lugar y afectaciones a terceros.

Sin embargo y pese a los vacíos, el informe señaló que la gran mayoría de asesinatos se realizaron en horas de la mañana o de la noche y en cercanías a sus casas, lo que evidencia que a los líderes sociales y defensores los persiguen en sus cotidianidades para determinar momentos de vulnerabilidad. (Le puede interesar: ["Impunidad en casos de desaparición forzada es casi del 100%: ASFADDES](https://archivo.contagioradio.com/impunidad-en-casos-de-desaparicion-forzada-es-casi-del-100-asfaddes/)")

A su vez se pudo establecer que el **54% de los homicidios se realizaron en zona rural, es decir en donde no hay presencia del Estado y en 120 de los casos** presentados se afectó a un tercero, de igual forma en 322 casos se usó un arma de fuego para acabar con la vida de los defensores de derechos humanos.

### **La impunidad contraataca** 

El informe expone finalmente en su capítulo la Impunidad contraataca, como el 87% De los casos continúa sin ningún tipo de avance, hasta el momento y de acuerdo con la información que se pudo recolectar 283 casos están en indagación, 15 en investigación, 7 en proceso de imputación, 26 en juicio, 4 han tenido condenas, **22 están en ejecución de penas, 2 en terminación anticipada, 44 archivado y 53 casos continúan sin información**.

De igual forma, el informe pudo establecer que el Estado contaba con información suficiente para determinar que defensores y líderes sociales se encuentran en riesgo y se hace urgente activar mecanismos en su protección, muestra de ello es la documentación de 116 homicidios **en 21 departamentos advertidos por el Sistema de Alertas Tempranas entre el 2009 al 2016**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
