Title: Víctimas exigen a Congreso mantener esencia de los Acuerdos de Paz
Date: 2017-05-23 15:18
Category: DDHH, Nacional
Tags: acuerdo de paz, Congreso de la República, víctimas
Slug: victimas-exigen-a-congreso-mantener-esencia-de-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/víctimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corporación Yira Castro] 

###### [23 May 2017] 

Víctimas, organizaciones de víctimas y defensoras de derechos humanos, hicieron un llamado urgente al Congreso de la República para que se “mantenga a la altura del compromiso histórico y político”, tras la decisión de la Corte Constitucional y frente a los Acuerdos de Paz y de esta forma, **priorice el trámite de las leyes que aseguran la implementación**, manteniendo la esencia de lo pactado en el Teatro Colón y la centralidad de las víctimas.

Durante una rueda de prensa las víctimas exigieron que se expida la reforma a la Ley de Víctimas y Restitución de Tierras, conforme a lo que se había pactado en el Acuerdo de Paz y que incluya las propuestas que ya habían realizado en términos de reparación a las víctimas. Le puede interesar: ["Ahora Congreso de la República podrá re negociar los Acuerdos de Paz"](https://archivo.contagioradio.com/jairo-estrada/)

### **Ley de Víctimas y reparación colectiva** 

Las víctimas le exigieron al gobierno que se reconozca a **las víctimas que se encuentran en el exterior y se reglamenten los derechos para esta población**, así como los procedimientos para su atención, asistencia, reparación integral y retorno voluntario al país.

A su vez piden fortalecer el “**enfoque diferencial” incluyendo un enfoque territorial, para que en conjunto con el enfoque de género** se apliquen las medidas de atención y reparación. Además, afirmaron que es importante que se incluya el principio de No regresividad, que significa que los derechos de las víctimas son intangibles.

Referente a las garantías de no repetición, las víctimas señalaron que estas deben ser incluidas en el acuerdo como **“un compromiso real por parte del gobierno”, a través de implementación de medida concretas** de transformación de las condiciones que han permitido la victimización. Le puede interesar: ["Hay que desclasificar los archivos de seguridad para conocer la verdad"](https://archivo.contagioradio.com/hay-que-descalsificar-los-archivos-de-suguridad-para-cononcer-la-verdad/)

De igual modo las víctimas reiteraron la necesidad de que se cree una Política Pública de Atención Psicosocial y de Salud integral para víctimas, que se **reabra del Registro Único de Víctimas por un periodo de tiempo de 5 años** y que se disponga la reglamentación del Programa de Reparación Colectiva con la participación de las víctimas. Le puede interesar: ["Organizaciones Sociales proponen reforma a la Ley de Víctimas y Restitución de Tierras"](https://archivo.contagioradio.com/organizaciones-sociales-proponen-reforma-a-ley-de-victimas-y-restitucion-de-tierras/)

### **Las entidades y sus garantías para las víctimas** 

Uno de los reparos que han hecho las víctimas y organizaciones de víctimas es la falta de una estructura institucional que atienda las necesidades de las mismas, por esta razón con la reforma a la ley 1448, solicitaron que se reglamenten las competencias de cada una de las entidades que conforman el Sistema Nacional de Atención y Reparación a las Víctimas, estableciendo sus deberes de priorizar el cumplimiento de los derechos de las víctimas en todas las instituciones.

Sin embargo, afirmaron que para que este sistema funcione es necesario un presupuesto adecuado que garantice el cumplimiento del Sistema**. En este sentido piden una nueva formulación del CONPES**.

Otro de los reparos que realizaron las víctimas es la importancia de que se genere una **Política Pública de Generación de Ingresos y Empleo para las víctimas** que atienda el enfoque diferencial y de género. Le puede interesar: ["Piden re abrir el Registro Único de víctimas"](https://archivo.contagioradio.com/piden-reabrir-el-registro-unico-de-victimas/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
