Title: "La cámara es mi arma": periodista palestina de 10 años
Date: 2016-05-10 16:11
Category: El mundo
Tags: Cisjordania, Gaza, Ocupación israelí, Palestina
Slug: la-camara-es-mi-arma-es-mas-fuerte-que-una-pistola-periodista-palestina-de-10-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Janna-Jihad-Ayyad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Independt ] 

###### [10 Mayo 2016 ]

Para Janna Jihad Ayyad, la pequeña palestina que desde sus siete años documenta las agresiones que día a día comete el régimen israelí contra el pueblo palestino, **las cámaras son armas más fuertes que las pistolas** porque le permiten enviar el mensaje de Palestina al mundo y mostrar **lo que Israel pone en marcha para obligarlos a dejar sus tierras**.

Los vídeos que graba con el celular de su madre mientras caminan por Cisjordania, Jerusalén, Hebrón, Nablus y Jordán, muestran **la ocupación, los abusos de los soldados, la presencia constante de cañones, policías y puestos de control**, pero también las acciones de resistencia, en las que participan niños y niñas de su misma edad.

Nawal Tamini, madre de la menor, asegura estar orgullosa de su hija, de que comparta con el mundo sus miedos y esperanzas, así como los problemas a los que se enfrenta para ir a la escuela. La llenan de temor los continuos ataques que perpetúa el Ejército israelí contra su vivienda, **"en medio de la noche lanza gases lacrimógenos a nuestra casa"**.

La muerte de su primo, Mustafa Tamimi y de su tío, Rushdie Tamimi, fue lo que animó a la pequeña Janna a documentar todo lo que sucedía en la aldea Nabi Saleh de Cisjordania. **El primero fue asesinado por un bote de gas lacrimógeno y Rushdie fue fatalmente baleado en su riñón**.

Otra de las inspiraciones de Janna fue su tío Bilal Tamimi, que a través de fotografías ha documentado la violencia de los soldados israelíes, y quien afirma que el activismo de la niña tiene que ver con la enseñanza que han compartido entre su familia, de **no aceptar la humillación, no ser cobardes, no quedarse en silenció y luchar por la libertad**.

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
