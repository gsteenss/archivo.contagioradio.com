Title: Resistencia contra la represa del Quimbo
Date: 2015-02-20 21:34
Author: CtgAdm
Category: Hablemos alguito, Programas
Tags: Hidroelectrica, Hidroelectrica Quimbo, Represa del Quimbo, Resitencia comunidades Quimbo
Slug: resistencia-contra-la-represa-del-quimbo
Status: published

###### **Foto:Reexistencia.wordpress.com** 

##### **Entrevista:** 

<iframe src="http://www.ivoox.com/player_ek_4112452_2_1.html?data=lZaelJmZdo6ZmKialpmJd6KklpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fstrWz8fTb9PZ1M7g1sqPpc2fxt3h1MbHuMrqytja0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**La represa del Quimbo es quizás uno de los macroproyectos con más afectación construidos en Colombia**. Pasando por los daños ambientales y arqueológicos, las consecuencias de su construcción se derivan en violencia como desplazamiento forzado y represión contra las comunidades afectadas.

Para Samuel Pinedo, del vicariato apostólico Tierra Adentro del Huila y Carlos Humberto Cuello Barrrea  médico bioenergético y propietario de una reserva forestal en Viche, también en el Huila, lo que esta sucediendo tanto en la represa del Quimbo y los territorios afectados por la megaminería en el Cauca, **hacen parte de un laboratorio de la violencia**.

**Es decir son zonas** pobres y totalmente desprovistas de servicios, como si nunca hubiera existido el estado, donde el presupuesto a penas da para satisfacer las mínimas necesidades de los habitantes. **el estado solo hace presencia cuando aplica un macroproyecto extractivo desplazando a los moradores del lugar**.

La vulnerabilidad en la que se encuentran las comunidades como consecuencia de dichos proyectos realizados por multinacionales en colaboración con el gobierno ha obligado a organizarse y  resistir a las comunidades afectadas.

La represión no se ha hecho esperar, asesinatos, **paramilitarismo, desplazamiento forzado y desaparición forzada como estrategia estatal, victimizando a todo el que lucha por el respeto a los derechos humanos**.

Por último **el sentimiento de desatención**, engaño y desprotección por parte de los afectados hacia sus propios gobernantes, **crea un clima de desconfianza**, pero también permite organizarse autónomamente creando formas  de vida paralelas al estado.

megaproyectos
