Title: "Irán ha puesto en jaque a sus enemigos mortales"
Date: 2015-07-14 18:19
Category: El mundo, Política
Tags: acuerdo nuclear, Acuerdo nuclear Iran y G5+1, Armas Nucleares, César Torres Del Río, China, Estados Unidos, Iran, Israel, Rusia
Slug: iran-ha-puesto-en-jaque-a-sus-enemigos-mortales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/acuerdo-nuclear-iran-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: telam.com.ar 

<iframe src="http://www.ivoox.com/player_ek_4805296_2_1.html?data=lZ2dl5edeo6ZmKiak5mJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmqtfO0JDMpYzk1srg1tSPqc%2Bfy8be18qPpYzn1tiYx9PJscrb0NiYz9TWuMLgxtiSlJeRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Cesar Torres, Analista político] 

###### [14 Jul 2015] 

En una alocución simultánea, **Estados Unidos e Irán anunciaron la definición de un acuerdo nuclear después de 2 años de negociaciones** de Iran con el G5+1 (Estados Unidos, China, Rusia, Reino Unido y Alemania). El acuerdo incluye levantamiento de las sanciones económicas interpuestas por la Unión Europea contra esa nación, anulación del embargo de armas y reemplazado por acuerdo gradual y además continúa el programa nuclear con fines energéticos.

Para Cesar Torres del Río, analista político e internacionalista, “*puede decirse con toda certeza que Irán ha ganado o al menos ha puesto en jaque a sus enemigos mortales y ha obtenido un importante triunfo en materia diplomática y de relaciones internacionales*”, según el analista solamente le hecho de que se retires las sanciones **permite oxigenación interna pero también para sus relaciones en la región y en el mundo**.

De esta manera, también Irán se convierte en una potencia regional, postura “inaceptable” para Israel, porque así las cosas Irán deja de ser uno de los países llamados del “eje del mal” para convertirse en **una potencia económica, comercial y militar regional** y que tiene influencia en el mundo por los acuerdos comerciales con diversas naciones, entre ellas Venezuela.

En materia económica y comercial Cesar Torres agrega que ganan todos, las empresas multinacionales los países que han firmado tratados comerciales y Estados Unidos. Para el efecto de este acuerdo y de la geopolítica del Medio Oriente, Irán no solamente es Irán, afirma Torres, sino que “tiene una cola” de países como Rusia o China, que ubican a Israel como el gran perdedor, sin embargo se espera que **Israel busque fortalecerse política y militarmente.**

Aunque con este acuerdo “ganan todos” **una de las derrotadas es la humanidad por el peligro que representan las armas nucleares.** “*lo ético nunca ha existido para las potencias que dominan el planeta… la humanidad poco cuenta para este grupo de países que tienen la capacidad de destruir el planeta y está vigente la consigna que dice que toda planta nuclear es una amenaza nuclear*”.

Otro de los problemas graves es que, con este acuerdo, “*un grupo de países se autoproclama como el único racional, democrático y con posibilidades de salvar el planeta y al capital…, los otros no tienen derecho*” situación que pone de presente que este tipo de acuerdos, aunque para nuestro continente no tenga en apariencia, implicaciones directas.
