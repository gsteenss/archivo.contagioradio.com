Title: Choque entre instituciones impide avances en justicia en el posconflicto
Date: 2018-11-07 16:07
Author: AdminContagio
Category: Judicial, Nacional
Tags: Corte Penal Internacional, Fiscalía, ICTJ, JEP, justicia
Slug: instituciones-justicia-posconflicto
Status: published

###### [Foto: Contagio Radio] 

###### [7 Nov 2018] 

Luego del foro sobre la complementariedad de los tribunales nacionales con la **Corte Penal Internacional (CPI)** para la investigación y juzgamiento de crímenes, sus organizadores integrantes del **Centro Internacional para la Justicia Trasicional (ICTJ**) evaluaron los **retos que enfrentará la justicia colombiana con la implementación del Acuerdo de Paz.**

**La directora del organismo en Colombia, Maria Camila Moreno**, aportó un balance positivo sobre el evento, por la gran asistencia y el interés suscitado en parte gracias a la participación del **Vicefiscal de la CPI James Stewart.** (Le puede interesar: ["Cuatro preocupaciones de la Corte Penal Internacional sobre cambios a la JEP"](https://archivo.contagioradio.com/cuatro-preocupaciones-corte-penal-internacional-cambios-jep/))

En su intervención, Stewart se refirió al articulo 28 del Estatuto de Roma (referente a la responsabilidad en el mando de uniformados) y sus alcances, en una semana que estuvo marcada por la aprobación en el Congreso, de un Proyecto de Acto Legislativo que modificaría el juzgamiento para militares en la **Jurisdicción Especial para la Paz (JEP).**

Adicionalmente, en el foro participaron integrantes de la JEP, sociedad civil, academia y la Vicefiscal General de la Nación **Maria Paulina Riveros; quien dijo que esta entidad estaba en toda disposición para apoyar a las instituciones del Sistema** Integral de Verdad, Justicia, Reparación y No Repetición, entre ellas la Jurisdicción de Paz.

### **El gran reto es que la justicia transicional, ordinaria e internacional cooperen** 

Moreno indicó que para la institucionalidad colombiana será un gran desafío adaptarse a las nuevas condiciones, entender su rol y lograr una relación armónica, porque lo que se requiere la construcción de **"una política de Estado que fortalezca su capacidad de investigar, juzgar y sancionar los delitos que podrían ser competencia de la CPI"**, y aclaró que finalmente de eso se trata la complementariedad y cooperación entre la justicia de paz, la justicia ordinaria y los tribunales internacionales.

Aunque la directora de la ICTJ aseguró que esta adaptación tomara tiempo para asimilarse, en términos de las competencias y limites de cada una de las instituciones de justicia que hay en el país, **su operatividad debería llevar a una capacidad de juzgamiento que evite la apertura de casos en Colombia por parte de la CPI**. (Le puede interesar: ["Organizaciones de DD.HH y Corte Pena Internacional temen impunidad por modificaciones a la JEP"](https://archivo.contagioradio.com/organizaciones-de-dd-hh-y-la-corte-penal-internacional-temen-impunidad-por-modificaciones-a-la-jep/))

### **Modificaciones a la JEP no contribuyen a la justicia ni a la reparación de las víctimas**

Sobre la modificación que se aprobó en la Comisión Primera del Senado al Acto Legislativo 01 de 2017, con el que se introdujo la JEP al ordenamiento jurídico, y que incorpora la elección de 14 nuevos magistrados para juzgar a agentes del Estado, Moreno aseguró que **es un Proyecto que restaba legitimidad a la Jurisdicción y no contribuye a la justicia ni a la reparación de las víctimas**.

Para la experta, la aprobación de este Proyecto de Acto Legislativo significaría aceptar que hay unos magistrados en la Jurisdicción que no son suficientemente parciales mientras hay otros, elegidos por parte de organizaciones nacionales, que sí son parciales; hecho que "**sería como si la JEP tuviese dos cabezas y dos corazones**". (Le puede interesar:["Las razones de las víctimas para oponerse a modificación de la JEP"](https://archivo.contagioradio.com/las-razones-de-las-victimas-para-oponerse-a-modificacion-de-la-jep/))

Sin embargo, resaltó que **existirían dos revisiones por parte de la Corte Constitucional que lo imposibilitarían**: La primera revisión, por cuenta de una demanda que cursa en la Corte contra el artículo 75 de la Ley de Procedimiento de la JEP, que suspendía la comparecencia de uniformados ante ese mecanismo por 18 meses, y en caso de que se falle en su contra, se cerraría cualquier posibilidad de modificar el interior de este mecanismo; y, la revisión por parte de la Corte del Proyecto de Acto Legislativo que hoy hace su curso en el Congreso.

Por lo tanto, Moreno concluyó que **la Corte, siendo consecuente con su propia jurisprudencia, negaría estos cambios de fondo al Acuerdo de Paz y al Acto Legislativo 01 de 2017**, lo que imposibilitaría la inclusión de los 14 nuevos magistrados. (Le puede interesar:["Modificaciones a la JEP serían inconstitucionales: Gustavo Gallón"](https://archivo.contagioradio.com/modificacion-a-tribunales-de-la-jep-es-inconstitucional-gustavo-gallon/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
