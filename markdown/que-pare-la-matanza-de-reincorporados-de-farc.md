Title: ¡Que pare la matanza de reincorporados de FARC!
Date: 2020-04-04 13:28
Author: AdminContagio
Category: Actualidad, Paz
Tags: asesinato, FARC, Tolima
Slug: que-pare-la-matanza-de-reincorporados-de-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Asesinan-a-Edwin-de-Jesús-Carrascal-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @SandraFARC {#foto-sandrafarc .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aunque las cifras son odiosas, los números de las muertes de reincorporados de las FARC son necesarios si se quiere ver la magnitud de lo que está sucediendo con quienes le apostaron a la paz. Según el propio partido Fuerza Alternativa Revolucionaria del Común, desde la firma del acuerdo de paz han sido asesinadas 194 personas en casi todos los territorios del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El último hecho se presentó en el departamento del Tolima en  
contra El excombatiente en proceso de reincorporación, Carlos Alberto Castillo,  
este viernes 3 de Abril en su casa a eso de las 10 de la mañana. Algunos  
testigos afirmaron que por los menos 5 hombre fuertemente armados llegaron  
hasta su vivienda, lo sacaron y en una zona boscosa cercana le propinaron 3  
impacto de bala en la cabeza.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Carlos Alberto, quién se había acogido al proceso de paz, estaba inscrito en el Espacio Territorial de Capacitación y Reincorporación (ETCR) de Miranda, en el departamento del Cauca, pero se había radicado hace unos meses en la vereda del Berlín del municipio de Atacó, en límites con Planadas y Río Blanco (Tolima), donde vivía con su esposa, también reincorporada de FARC.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las múltiples responsabilidades en asesinatos de  
reincorporados de FARC

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aunque, según algunos medios de información, los  
responsables de este asesinato harían parte de la columna Dagoberto Ramos de  
las nuevas FARC, no se tiene información oficial de la operación de esta  
estructura en el departamento del Tolima, sin embargo, dada la nula o muy poca  
investigación de la fiscalía u otros entes del Estado, no se puede descartar  
ninguna versión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo cierto es que los reincorporados de FARC están expuestos  
y expuestas a múltiples amenazas, una de ellas derivada de la situación en los  
territorios tras la desaparición de FARC como una guerrilla. El control de las  
rutas del narcotráfico, el control territorial en medio de la lucha entre  
paramilitares y la presencia de disidencias son todos factores de riesgo. Sin  
embargo la responsabilidad recae en manos del Estado.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Van 20 asesinatos de reincorporados en lo corrido del 2020

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el Instituto de Estudios para la Paz, INDEPAZ, entre los  
casos más recientes de asesinatos de reincorporados de FARC más recientes,  
antes del cometido este 3 de Abril, figuran el de Alveiro Antonio Gallego  
ocurrido el 21 de marzo en La Macarena (Meta); el de José Isidro Cuestas Ricas,  
el 29 de marzo en Jiguamiandó (Chocó); y el de Juan Carlos Castillo Certijama,  
el 30 de marzo en Puerto Asís (Putumayo). 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

INDEPAZ, FARC, congresistas y otras organizaciones de DDHH han señalado que el Estado debe implementar planes integrales, no se trata de poner un esquema de protección a cada integrante de FARC, pero si es necesario que se selle el manto de impunidad, y el Estado haga presencia integral en el país, sobre todo aquellos más agobiados por las múltiples violencias. La implementación de los PDET´s, la presencia de la defensoría del pueblo, la inversión de recursos en salud y educación serían una de las salidas a este luto permanente al que se enfrentan FARC y el país en general.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar: [La paz no la detiene nadie, afirman excombatientes de FARC](https://archivo.contagioradio.com/la-paz-no-la-detiene-nadie-afirman-excombatientes-de-farc/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
