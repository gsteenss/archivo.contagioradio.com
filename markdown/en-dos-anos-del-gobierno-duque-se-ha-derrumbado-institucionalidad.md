Title: En dos años del Gobierno Duque se ha derrumbado institucionalidad
Date: 2020-08-14 20:07
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Iván Duque
Slug: en-dos-anos-del-gobierno-duque-se-ha-derrumbado-institucionalidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Presidente-Duque.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Presidencia /Iván Duque

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 7 de agosto se marcaron dos años desde la posesión de Iván Duque como presidente de la República. A propósito del cumplimiento mitad de su mandato, comunidades, organizaciones defensoras de DD.HH. y sectores del Congreso advierten sobre la ausencia de voluntad política para la implementación del Acuerdo y un retroceso en materia de DD.HH. y garantías para las poblaciones y un pérdida de credibilidad de las instituciones comenzando por las FFMM y de policía. [(Lea también: A 16 años del Embrujo Autoritario llega Iván, el aprendiz)](https://archivo.contagioradio.com/a-16-anos-del-embrujo-autoritario-llega-ivan-el-aprendiz/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La realidad en las regiones no ha cambiado

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Zulma Ulcue, consejera del pueblo Nasa desde Putumayo** afirma que su departamento ha sido históricamente uno de los más golpeados por la violencia, y aunque con la firma del Acuerdo de Paz durante el gobierno de Juan Manuel Santos, al principio se disminuyó la violencia, con la llegada del presidente Duque esta se ha agudizado y se ve en el fortalecimiento de estructuras y grupos al margen de la ley, como los grupos paramilitares y las disidencias del frente Carolina Ramirez.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre este incremento de la violencia, **Lourdes Castro, coordinadora del programa Somos Defensores** señala tras la firma de un Acuerdo de Paz, este debía ser un gobierno que tenía que sentar las bases para una transición en la implementación sin embargo no se ha visto una voluntad política lo que se ve con claridad en el aumento de las violaciones a los DD.HH.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Somos Defensores, aunque se está en proceso de verificación, por lo menos se han cometido hasta julio de 2020 475 asesinatos contra liderazgos sociales, de los que el 55% han sucedido durante este gobierno, resalta además que también otras agresiones que "ya no estaban en nuestra retina como masacres, desplazamientos y agresiones en los procesos de erradicación forzada".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Estamos ante la ausencia de una política de garantía para la defensa de los DD.HH."

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Y aunque a departamentos como Putumayo los han caracterizado las luchas de las comunidades indígenas y campesinas, hoy ven con preocupación el aumento de amenazas a líderes, el desplazamiento colectivo y un incremento en los asesinatos en lugares como **Puerto Guzmán, Puerto Asís, Villa Garzón y Puerto Caicedo,** "hacer procesos de liderazgos políticos es acercarnos más rápido a la muerte" afirma Ulcue.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la consejera nasa señala que se debe fortalecer la autonomía de las comunidades indígenas, la incursión militar en los territorio genera afectaciones en la armonía de sus territorios, agregando que la fuerte presencia de las Fuerzas Militares en los territorios están en función de proteger los intereses de las empresas y multinacionales, más que de proteger a la gente.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Gran parte de lo que hemos podido verificar es que todo tiene que ver con las multinacionales que hacen presencia en el departamento, la violencia no es ajena a la explotación, todo va ligado y de alguna forma busca que la población salga para que sean intervenidos y rompe el tejido de las comunidades". [(Lea también: 167 líderes indígenas han sido asesinados durante la presidencia de Iván Duque : Indepaz)](https://archivo.contagioradio.com/167-lideres-indigenas-han-sido-asesinados-durante-la-presidencia-de-ivan-duque-indepaz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Zulma también advierte que las erradicaciones forzadas han expuesto a las comunidades que temen el regreso de la fumigación aérea con glifosato y los daños que podrían causar, contaminación ambiental, nuevas enfermedades, y un daño irreparable a terrenos que en el pasado eran aptos para la agricultura y que actualmente no se pueden utilizar. Eso sin mencionar que las sustitución voluntaria no ha llegado al departamento donde 20.331 familias se acogieron al Plan de Sustitución erradicando 9.276 hectáreas de forma voluntaria hasta diciembre de 2019 según la [Oficina de las Naciones Unidas contra la droga y el delito (UNODC)](https://www.unodc.org/documents/colombia/2020/Febrero/INFORME_EJECUTIVO_PNIS_No._19.pdf)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Gobierno Duque, "muchos anuncios pero cero eficiencia"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, **el representante a la Cámara por el Partido Verde, Mauricio Toro** señala que desde el Gobierno no ha existido una agenda legislativa que refleje los cambios que prometió en campaña, "se le cayó una reforma tributaria que ya venía mal, además presentó una ley TIC que prometía conectar a más de 3.000 poblaciones, sin embargo hoy nuestra población está desconectada y nuestros niños tienen problemas de acceso a la educación".  [(Lea también: Sin luz ni internet no puede haber educación virtual de calidad)](https://archivo.contagioradio.com/a-colombia-llego-la-educacion-virtual-mientras-el-20-de-los-estudiantes-no-tenia-ni-luz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Toro además hizo referencia a las objeciones que presentó el Gobierno a la ley estatutaria de la Jurisdicción Especial para la Paz y que finalmente fueron rechazadas por la Corte Constitucional, lo que para el representante simboliza "una falta de pericia, pero no por falta de mermelada en el Congreso, por que lo ha hecho es quitarse ese manto y entregar los puestos y los ministerios, hoy lo estamos viendo con la Defensoría del Pueblo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Debilitamiento del Estado Social de Derecho

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para Lourdes Castro, uno de los elementos que ha caracterizado este Gobierno es un fortalecimiento del presidencialismo y que ha derivado en debilitamiento del Estado social de derecho, que ha ido en deterioro aun más en épocas de pandemia, donde tan solo en el mes de abril se expidieron 46 decretos de carácter legislativo y ejecutivo, que señala la defensora de DD.HH. muchos no están ligados a la situación de la Covid-19 y que requieren un control.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resalta que para julio, 6 de 48 decretos presidenciales fueron declarados inconstitucionales por la Corte Constitucional, mientras otros 67 decretos hasta aquel entonces estaba siendo revisados por el alto tribunal. [(Lea también: El 70% de municipios dependen de una red de salud pública ilíquida y en pandemia)](https://archivo.contagioradio.com/el-70-de-municipios-dependen-de-una-red-de-salud-publica-iliquida-y-en-pandemia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente a ello, Toro advierte que desde el Gobierno se busca causar un desequilibrio de poderes, tal como se evidenció en las reacciones del presidente Duque a la decisión de la Corte Suprema de Justicia frente al caso que involucra al senador Álvaro Uribe, "que el presidente de la República haya tomado partido frente a este anuncio no demuestra la dignidad del cargo presidencial que debe ser neutra y respetuosa con todas las y los ciudadanos y no con un partido político". [(Le recomendamos leer: El fracaso de la política exterior de Duque a 18 meses de su Presidencia)](https://archivo.contagioradio.com/el-fracaso-de-la-politica-exterior-de-duque-a-18-meses-de-su-presidencia/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Hasta que Iván Duque entienda que él es el presidente de casi 50 millones de colombianos y no de un partido político esto no va a ir en mejor camino"
>
> <cite>Representante Mauricio Toro</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, **Laura Vega representante del Consejo Nacional de Reincorporación (CNR) del Partido FARC en Tolima** también recordó cómo se autorizó el ingreso de tropas de Estados Unidos al país, desconociendo al Congreso que debía debatir sobre esta decisión, "la paz con legalidad es una falacia no se entiende cómo se habla de la paz pero se refuerza el aparato militar y se traen tropas estadounidenses para asesorar temas de guerra". [(Operaciones militares de EE.UU. en Colombia deben ser autorizadas por el Congreso)](https://archivo.contagioradio.com/operaciones-militares-de-eeuu-en-colombia-deben-ser-autorizadas-por-el-congreso/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Políticas de guerra caracterizan al gobierno Duque

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito del apoyo que se ha dado a las Fuerzas Militares y que ha caracterizado estos dos años de Gobierno, Toro llamó la atención sobre las mociones de censura contra el exministro de defensa Guillermo Botero y las directrices al interior del Ejército de duplicar la cantidad de muertes en combate incluso si eso significa no exigir exactitud en sus operaciones, una orden que implicaría la reducción de protección a la población civil y que a principios de la década de los 2000 resultó en más 5.000 ejecuciones extrajudiciales. [(Duplicar resultados a toda costa, la orden a militares en el gobierno Duque)](https://archivo.contagioradio.com/duplicar-resultados-a-toda-costa-la-orden-a-militares-en-el-gobierno-duque/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Estamos viviendo una re edición de la política de seguridad democrática que se vivió durante los dos gobiernos del expresidente Álvaro Uribe, lo que implica una mayor militarización en los territorios que contrasta con un aumento de los grupos armado, sucesores del paramilitarismo, grupos ligados al cartel de la droga y las disidencias", agrega Lourdes Castro, desde Somos Defensores.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El representante concluye que esta forma de actuar también se ha evidenciado en la ausencia de diálogos con el ELN, lo que lo lleva a concluir que ha sido "un Gobierno errático con una política guerrerista".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Una lenta implementación del Acuerdo de Paz

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo, Laura Vega destaca que Duque "ha sido consistente en su anunció de hacer trizas el acuerdo", señalando que existe una política de ralentizar la aprobación de proyectos productivos, y aunque se supone que se debían aprobar una serie de propuestas que a futuro garantizaran las condiciones económicas de excombatientes, en la actualidad, para los Espacios Territoriales de Icononzo y Planadas, además de las 12 nuevas áreas de reincorporación - que no son reconocidas del todo por el Gobierno - solo se han aprobado cuatro proyectos en todo el Tolima, por solo mencionar uno de los departamentos donde se lleva a cabo el regreso a la vida civil de los firmantes de la paz. [(Lea también: Gobierno apoya los proyectos individuales pero no los colectivos en los ETCR)](https://archivo.contagioradio.com/gobierno-apoya-los-proyectos-individuales-pero-no-los-colectivos-en-los-etcr/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sumado a ello resalta que ya son 223 excombatientes asesinados desde la firma del Acuerdo por lo que es difícil ejercer la lucha política sin garantías para hacerlo y aunque se establecieron programas con la Unidad Nacional de Protección, "cada día son más las trabas y recortes para la implementación de seguridad, prueba de ello es el reciente desplazamiento forzado de al menos 45 familias y 100 personas del espacio territorial de Ituango y su éxodo hacia Mutatá. [(Lea también: Esperamos que en Mutatá encontremos la paz: excombatientes de Ituango)](https://archivo.contagioradio.com/esperamos-que-en-mutata-encontremos-la-paz-excombatientes-de-ituango/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Lourdes Castro concluye que la denominada Paz con Legalidad, bandera del gobierno Duque es una falacia en un momento donde el país **"no vive en paz ni existe legalidad"**, afirma haciendo referencia a los escándalos que han vinculado al presidente Duque con la compra de votos y a figuras como el ganadero Ñeñe Hernández en La Guajira o a investigaciones al Ejército que han revelado el perfilamiento de personas incluidas periodistas, políticos, defensores de DD.HH. y miembros de las altas cortes.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
