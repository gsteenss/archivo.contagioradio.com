Title: Si no se acaba con el paramilitarismo "yo creo que se va a repetir el caso de la Union Patriotica"
Date: 2015-05-10 21:03
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Comisión de Historia del conflicto y sus víctimas Colombia, Conversaciones de paz en Colombia, FARC, Padre Javier Giraldo
Slug: el-paramilitarismo-tiene-una-fuerza-tan-grande-que-yo-creo-que-se-va-a-repetir-el-caso-de-la-union-patriotica
Status: published

###### 10 May 2015 

Entrevista especial con el Padre Javier Giraldo, Sacerdote Jesuita, defensor de Derechos Humanos y miembro de la Comisión Histórica del Conflicto y sus Víctimas.

#### **Escuchar entrevista con el P. Javier Giraldo** 

<iframe src="http://www.ivoox.com/player_ek_4496730_2_1.html?data=lZmmmJyXdI6ZmKial5mJd6Kkl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYx9GPtMLY08qYrMbarcbmjKzW1MbQqNChhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[**Contagio Radio**:] ¿Está de acuerdo en que debe abrirse una comisión de esclarecimiento específica, para el tema del paramilitarismo en Colombia?

[**Javier Giraldo**]: Sí, eso es super urgente porque lo que ha pretendido esta negociación entre el gobierno y las FARC, como uno lo puede deducir de todos los discursos incluso del mismo gobierno, es que las FARC puedan pasar a una actividad legal y a una actividad política legal, pero si esto se da en esta situación en que el paramilitarismo tiene una fuerza tan grande, yo creo que se va a repetir el caso de la Union Patriótica. Es decir, si se crea una nueva fuerza política en la que puedan actuar o participar los desmovilizados de las FARC, yo creo que se va a repetir el genocidio.

[**C.R**:] En las condiciones actuales, con la vigencia del paramilitarismo...

[**J.G**:] Exacto, porque el paramilitasimo está muy activo, muy fuerte, y lo que pasó en el año 86 fue eso, el paramilitarismo estaba muy fuerte y cuando se creó la Unión Patriótica, que era una alternativa política, pues, vino el genocidio. Entonces yo me temo, y no es un temor infundado e imaginado; ya van más de 80 integrantes de la Unión Patriótica que han sido asesinados. Entonces, eso muestra que ya está en marcha ese procedimiento. No es algo que uno se imagine, eso ya se está produciendo.

Entonces, yo veo muy difícil que se llegue realmente a un acuerdo de paz en esas condiciones; primero, yo creo que debe haber una comisión de investigación del paramilitarismo, eso no se hace  de la noche a la mañana,  investigar un fenómeno de estos tiene es una tarea muy concreta; ubicar dónde están actuando, cómo están actuando, ubicar cuáles son los factores que están permitiendo la actuación de ellos. Eso no es una investigación histórica, es una investigación actual.

Entonces, si después de esa investigación hay realmente voluntad de actuar sobre los factores que están permitiendo ese desarrollo del paramilitarismo, eso es otra cosa que lleva tiempo, eso no es fácil. Esos factores están muy relacionados con políticas de larga trayectoria del gobierno y de los últimos gobiernos. Entonces eso lleva mucho tiempo y no se ha comenzado a hacer.

[**CR**:] ¿Usted ve en el gobierno o en su equipo de paz, alguna disposición para empezar a trabajar en este sentido?

[**J.G**:] Desgraciadamente es lo que uno no ve, porque, por ejemplo, desde el trabajo de la Comisión histórica del conflicto, e incluso en nuestro trabajo,  pedimos que se abrieran ciertos archivos, se entregaran ciertos documentos y no fue posible. Entonces esa disposición del gobierno de permitir que se investigue un fenómeno como estos, exige que la apertura de muchos archivos y de muchos expedientes y de muchas informaciones que se consideran como reservadas, y uno no ve hasta el momento una voluntad del gobierno de hacer eso.

[**C.R**]: ¿Qué piensa usted de lo hecho hasta ahora con el informe de la CMHCV?

[**J.G**]: El gobierno había adquirido un compromiso en la misma mesa de negociaciones de publicar inmediatamente este trabajo y de difundirlo ampliamente. Ya van muchos meses y no se ha cumplido con eso y parece que no hay voluntad de hacerlo. En varios comunicados de las FARC y de la mesa de negociaciones, ellos mismos se han quejado de que el gobierno parece querer sepultar ese informe  y han ido reclamando de una manera muy persistente que se cumpla ese compromiso.

Últimamente ha habido una polémica nacional sobre la importancia o la incidencia de ese informe. Algunos dicen que eso no sirvió para nada porque no señaló personas completas, y en ese sentido no hubo reacciones fuertes y se le ha silenciado, prácticamente, y yo creo que no se trataba de señalar personas concretas con nombres propios. Las tres preguntas que nos hicieron a los investigadores eran, primero: señale cuál es el origen del conflicto, segundo: cuáles son los factores que han contribuido a la persistencia del conflicto, y tercero: cuál cree que es el impacto que ha tenido el conflicto en la sociedad; y yo creo que a pesar de que habían posiciones muy contradictorias, sin embargo todos tratamos de responder a esas tres preguntas, y se trataba de ubicar el origen del conflicto y la persistencia del conflicto. Hay personas que se han dedicado a analizar a fondo, a leer a fondo esos trabajos y a sistematizarlos, y me han dicho que el 70% de las opiniones allí coinciden en que el origen del conflicto está en unas estructuras terriblemente injustas de esta sociedad, y una minoría opina que no hay factores objetivos sino que fue un grupo de personas un poco desquiciadas que les dio por declararle la guerra al Estado sin ningún fundamento y ese es el origen del conflicto. Pero la inmensa mayoría del grupo opinamos, y con elementos de juicio muy fundados en estudios, en análisis económicos, sociales, que el origen del conflicto hay que buscarlo en unas estructuras injustas.

[**C.R**]: Y por último, ¿Cómo empezar a caminar para romper esas estructuras injustas?

**[J.G]:** Bueno, hay algo que a mi me ha preocupado, y es que en toda esta negociación el gobierno ha rechazado muchisimas propuesta interesantes y bien fundamentadas que han hecho las FARC para solucionar algunos de estos temas que están en la agenda y el eslogan del gobierno siempre ha sido: "el modelo no se puede tocar", el modelo económico, político, no se puede tocar, y por eso ellos han rechazado muchas propuestas que uno las ve bien fundamentadas y urgentes, pero el gobierno siempre dice: "no estamos discutiendo el modelo económico y político". Entonces yo veo ahí un obstáculo bastante grande para que el proceso de paz; realmente de frutos, en el sentido en que vayan a incidir en factores muy importantes de la violencia.

Entonces yo me he preguntado qué hacer, y yo veo que si en las mesas de negociaciones no se permite tocar el modelo y estamos muy convencidos de que el modelo es la raíz más importante de la violencia, si eso no se toca, pues la violencia no va a cambiar.

Entonces me parece que es el momento en que los movimientos sociales, que sí pueden tocar el modelo, se unifiquen alrededor de unos puntos fundamentales para incidir en las causas de la violencia, y que formen un bloque común para abrir otro espacio de negociación que es la sociedad civil y sobretodo los movimientos sociales.
