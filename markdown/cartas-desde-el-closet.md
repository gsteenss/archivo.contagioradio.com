Title: "Querida Abuela" carta desde el closet
Date: 2017-06-22 15:18
Category: Cartas desde el Closet, Opinion
Tags: LGBTI
Slug: cartas-desde-el-closet
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/cartas-desde-el-closet.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/cartas-desde-el-closet.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Blogger 

Junio de 2017

**Querida abuelita**

[Un fuerte abrazo]

Hace días quería escribirte, pero me daba miedo, no encontrara las palabras adecuadas.

No te alcanzas a imaginar cuanto te quiero, desde niña fuiste mi heroína, tu cariño, tu manera de ser, la comidas, las historias y los paseos han sido la fuerza para mi vida en los momentos de dolor, de soledad, de incertidumbre, en los tragos amargos.

Me haces falta, extraño tus abrazos, tus caricias, tus palabras, tus consejos, tus oraciones y tus regaños.

No me alcanzo a imaginar lo que significó para ti saber que yo era lesbiana, es decir, homosexual y me imagino que te duele, que te rompe el corazón.

He querido llamarte y no me he atrevido, tengo miedo de herirte o a que me hieras; no sé qué puedo decirte o qué me podrás decir, este silencio me atormenta mucho.

[Sé que para ti es muy difícil aceptar que la nieta de la cual te sentías orgullosa es lesbiana, escuchar los comentarios tan duros de miembros de la familia, de tus amigas y de la Iglesia contra los homosexuales, ver el dolor de mi papa, de mi mamá y no atreverte a hablar del tema con mis hermanos te hace daño, como me hace daño a mí.]

Te cuento que he vivido un infierno por dentro, sola, sin atreverme a hablar, sintiéndome mala y rara desde niña; sí abuelita, desde niña, porque siempre he sido así y no elegí serlo.

[Para ti la religión es muy importante y estás preocupada por mi salvación, yo antes también me preocupaba, porque pensaba que Dios me iba a mandar al infierno como castigo por mi condición sexual, pero hace un tiempo leí que Dios es amor y si es así, no podrá condenarme por una condición sexual que no elegí, que nací con ella y para la que no tengo explicación porque nuestra familia, “es una familia muy normal”.]

[Espero que esta carta nos dé la posibilidad de volver a hablar como antes.]

Con el alma en las manos

**Tu nieta -** **Parche por la Vida**

------------------------------------------------------------------------

###### **Este texto hace parte de Cartas desde el Closet, escritos anónimos de personas con orientaciones de género diversas que no han podido compartir públicamente sus pensamientos e ideas por su orientación sexual. Parche por la Vida ** 
