Title: Popayán  no tiene un plan para enfrentar la crisis  del Covid– 19
Date: 2020-04-07 16:43
Author: AdminContagio
Category: Nacional, yoreporto
Tags: alcaldia de popayán, Coronavirus, Covid-19, Crisis Sanitaria, Popayán
Slug: popayan-no-tiene-un-plan-para-enfrentar-la-crisis-del-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/5e749e10a3a6e.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"customTextColor":"#5f6d73"} -->

###### Juan Carlos López, alcalde de Popayán - Foto de El Tiempo 

<!-- /wp:heading -->

<!-- wp:list -->

-   Ciudad en Movimiento denuncia las medidas improvisadas de la Alcaldía de Popayán para enfrentar el Coronavirus.
-   Los estudiantes de la Universidad del Cauca de la plataforma Unees impulsaron un proceso de recolección de alimentos para ser distribuidos entre las familias que más lo necesitan, acción solidaria que fue interrumpida y frustrada por la Policía Nacional, la cual no permitió su entrega.

<!-- /wp:list -->

<!-- wp:heading {"level":5} -->

##### **Popayán, Cauca. Abril 07 de 2020.**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La organización **Ciudad en Movimiento** denunció a través de un vídeo en redes sociales las escasas e improvisadas iniciativas que ha puesto en marcha la Alcaldía de Popayán para prevenir una crisis sanitaria a causa del Coronavirus.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En dicho video señalan que la capital del Cauca  
cuenta con un sistema de salud muy precario, esto se suma a la denuncia que  
también fue realizada por los trabajadores del Hospital Universitario San José  
en La W, quienes explicaron que este centro medico no esta preparado para  
afrontar la crisis del COVID-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los integrantes de Ciudad en Movimiento de Popayán además cuentan con preocupación las condiciones de pobreza que hay en la ciudad, donde 3 de cada 10 habitantes son pobres y más de la mitad de la población vive en la informalidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### *“El 31,4% de la población payanesa vive en condiciones de pobreza, el 8,5% en extrema pobreza, y cerca del 60% de la fuerza laboral vive de las economías informales”*, explicó la organización.

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con lo anterior, cumplir con la cuarentena se vuelve un *“privilegio de clases”* ya que para las personas más vulnerables escaseará la comida durante este aislamiento preventivo de 19 días. Frente a dicha situación los estudiantes de la Universidad del Cauca de la plataforma **Unees** impulsaron un proceso de recolección de alimentos para ser distribuidos entre las familias que más lo necesitan, acción solidaria que fue interrumpida y frustrada por la Policía Nacional, la cual no permitió su entrega.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### *“El 25 de marzo durante su trayectoria de  
recolección fueron abordados por la Policía quienes bajo ordenes del teniente  
Jhon Falla impidieron que se continuara con esta actividad, pese a los acuerdos  
previos que existía con la alcaldía y siguiendo los protocolos de  
bioseguridad”,* señaló  
Ciudad en Movimiento Popayán. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Pero acá no acaban los problemas en esta  
ciudad, el Banco de Alimentos que está impulsando la alcaldía carece de  
recursos, depende de la solidaridad de las personas para su funcionamiento y no  
cuenta con una ruta de acción estratégica y no se sabe claramente quiénes serán  
sus beneficiarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, el gobierno local se ha quedado solo  
con las medidas ridículas que ha tomado el Gobierno Nacional como por ejemplo  
el diferir a 36 cuotas el pago de servicios públicos del mes de abril para los  
estratos 1 y 2; y el no congelar el pago de los arriendos, lo cual afectará a  
millones de personas en el país que no cuentan con vivienda propia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este panorama es reflejo de la irresponsabilidad y falta de liderazgo del alcalde de Popayán, Juan Carlos López, quien además es investigado por la Fiscalía por mentir en el formulario de Migración Colombia al decir que no había viajado recientemente fuera del país, cuando días antes había estado en Marruecos e hizo escala en España, uno de los países más golpeados por el COVID-19.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/ciudadesmovimiento/videos/1287625714756339/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/ciudadesmovimiento/videos/1287625714756339/

</div>

<figcaption>
Vídeo de Ciudad en Movimiento en Facebook

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

Vea mas de[Yo Reporto](https://archivo.contagioradio.com/?s=yo+reporto)

<!-- /wp:paragraph -->
