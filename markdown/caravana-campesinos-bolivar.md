Title: Marchando campesinos de Bolívar exigen condiciones dignas de vida
Date: 2018-10-30 11:16
Author: AdminContagio
Category: Movilización, Nacional
Tags: bolivar, marcha, marialabaja
Slug: caravana-campesinos-bolivar
Status: published

###### Foto:Julio Castaño 

###### 30 oct 2018 

Bajo la consigna **"Por la vida digna caminaremos hasta la gobernación de Bolívar"**  campesinos salieron este lunes 29 de octubre de Maríalabaja rumbo a Turbaco, en busca de que se **cumplan los acuerdos que desde hace varios años han pactado con diferentes instancias gubernamentales**.

La motivación para recorrer los **más de 60km que comprende el trazado**, esta sustentada en algunas de las demandas de los pueblos rurales de Montes de María que integran la caravana, como **la falta de garantías de acceso al agua potable, fluido eléctrico, a vías en buenas condiciones, educación y salud** entre otras necesidades prioritarias no atendidas.

Además en su condición de víctimas del conflicto **exigen que los planes de reparación colectiva sean implementados**, como quedó firmado en el pacto regional con los Programas de Desarrollo con Enfoque Territorial PEDET, y que **se de claridad sobre los recursos para llevar a cabo las propuestas que las comunidades rurales acordaron**.

Hernando González, vocero de los marchantes e integrante del Comité de desarrollo campesino de Santo Domingo en Carmen de Bolívar asegura que "hemos decidido **caminar para reclamar esos derechos que históricamente las autoridades y los gobiernos nos han negado**" señalando la responsabilidad de los gobernadores y las alcaldías de Maríalabaja, El Carmen y San Jacinto.

Por su parte, el docente Rafael Pérez, manifestó que "**estamos muy contentos con el respaldo de los compañeros marchantes de toda la comunidad campesina**, cada una de las veredas que fueron invitadas del corregimiento y la gente esta muy optimista" asegurando que **los recursos para el desplazamiento provinieron de sus propios productos agrícolas**.

A pesar de la difícil situación que afrontan debido a la falta de una presencia institucional del estado que les represente mejores condiciones de vida, Pérez asegura que **el objetivo es regresar de la gobernación con soluciones**  "lo vamos a conseguir porque creemos en la valentía de nuestra gente".

Jesús Pérez, líder de Marialabaja, segura que con la manifestación buscan "**demostrar que los habitantes de los Montes de María quieren permanecer en el territorio** pero con una vivienda digna, con unas vías de acceso para transportar nuestros productos agrícolas que es la existencia nuestra como comunidad negra y como campesinos".

Los participantes pasaron la noche en el Colegio Nuestra Señora del Carmen en Turbaco, para **en horas de la mañana del martes arribar a la sede de la gobernación departamental**. (Le puede interesar: [Nominación a 'Nobel de DDHH' es un llamado a proteger líderes sociales](https://archivo.contagioradio.com/nominacion-marino-cordoba-martin-ennals/))
