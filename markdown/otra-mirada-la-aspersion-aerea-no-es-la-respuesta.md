Title: Otra Mirada: La aspersión aérea no es la respuesta
Date: 2020-08-27 10:15
Author: AdminContagio
Category: Otra Mirada, Programas
Tags: Aspersión Aérea con Glifosato, programas de sustitución de cultivos
Slug: otra-mirada-la-aspersion-aerea-no-es-la-respuesta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/aspersion-aerea-de-cultivos-ilicitos.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: El Tiempo

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Carlos Trujillo, ministro de Defensa, señaló que Colombia se encuentra lista para reiniciar la aspersión con glifosato para la erradicación de cultivos de uso ilícito. Además indicó que la presencia de cultivos de uso ilícito tiene relación directa con la violencia que actualmente vive el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, son muchos los sectores que cuestionan si el regreso de la aspersión con glifosato es realmente el camino para terminar con la violencia, incluso han señalado que una política para evitar las masacres no debe estar relacionada con más violencia para los territorios y sus pobladores.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Analizando esta posibilidad, estuvieron en el programa de Otra Mirada Jhenifer Mojica, abogada en la Comisión Nacional de Territorios Indígenas, Franklin Cortés, vocero nacional de la Coccam, Alirio Uribe Muñoz, abogado defensor de derechos humanos y  Ricardo Vargas, investigador asociado del Instituto Transnacional - TNI.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Quienes integraron este panel comparten su opinión al gobierno querer escudarse en las masacres para justificar el reinicio de las aspersiones.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Es lamentable que se tienda una cortina de humo sobre las fumigaciones para atender la crisis humanitaria que se ha generado en torno a las masacres en distintas partes del país”
>
> <cite>Jhenifer Mojica</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Siendo el Programa de Sustitución de Cultivos Ilícitos un punto acordado en el acuerdo de paz, señalan que el trabajo del gobierno en implementarlo ha sido deficiente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, cuando se reinició la conversación sobre la aspersión se mencionó que se realizaron una serie de modificaciones para evitar afectar a las comunidades y al ambiente; sin embargo, los invitados ponen en tela de juicio la efectivamente de estas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aún así, **el abogado Alirio aclara que por el momento el gobierno no puede poner en marcha la aspersión** pues hay ciertos requisitos sin cumplir. (Le puede interesar: [Comunidades le recuerdan al Gobierno que acudir a aspersión aérea sería ilegal](https://archivo.contagioradio.com/comunidades-le-recuerdan-al-gobierno-que-acudir-a-aspersion-aerea-seria-ilegal/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, comparten cuáles son las acciones que desde las organizaciones se pueden hacer para que no se desconozcan los pronunciamientos y las decisiones judiciales. (Si desea escuchar el programa del 24 de agosto: [Otra Mirada: Ante la violencia: Reactivar la movilización social](https://www.facebook.com/contagioradio/videos/400188804279393))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el programa completo [Haga click aquí](https://www.facebook.com/contagioradio/videos/786884488783449)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
