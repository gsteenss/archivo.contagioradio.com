Title: Vicente y la olla de oro
Date: 2016-08-19 09:37
Category: Comunidad, Cultura
Tags: Cultura, indígenas, Nasas
Slug: vicente-y-la-olla-de-oro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/LA-OLLA-DE-ORO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Taringa 

#### **Dicen los mayores que es malo adueñarse de los entierros sagrado de los nasas**. 

“Vicente era un hombre ambicioso, atrevido y los espíritus lo castigaron con la muerte”

Vicente no era pobre, tenía buenos caballos, buenas reses y tierra abundante. Cuentan que un día andaba en su rocería sembrando maíz con un cuñado y sus dos hijos cuando de pronto su macana de sembrar se hundió más de lo normal. Por su malicia, dejo marcado el sitio pero no le contó nada a nadie, en primer lugar porque no estaba seguro de haber encontrado una guaca, pues a veces se confunden con nidos de hormigas y en segundo lugar no querían que los acompañantes se dieran cuenta del hallazgo para evitar compartirla y también dicen que las guacas son celosas, esos eran los pensamientos de Vicente.

[En la tarde tan pronto se despidió de su cuñado, Vicente sin decirle nada a su mujer ni mucho menos a sus  hijos, volvió a la roza y en el sitio que antes había señalado empezó a cavar.]

[Fue cavando poco a poco la tierra, en la cual encontró piedras de moler que tapaba la entrada a una tumba, donde halló una calavera y muchos huesos, al lado de la calavera, halló una olla de barro con tres patas  adornadas con figuras de animales.]

[Después que la saco, y sin protección alguna, la destapo y vio que tenía unas pepitas parecidas a las uvillas, era de oro puro que los familiares del difunto le habían dejado a lado de su cabecera. Muy contento la llevo para su casa y la guardo en el cielo raso.]

[Esa noche Vicente en medio de muchos pensamientos de ambición, fue cerrando sus ojos para descansar, pero una horrible pesadilla lo aquejo, soñó con un hombre negro muy alto que lo quería ahorcar, lleno de pánico pujo tanto que su mujer le toco despertarlo echándole agua fría en la cara.]

[Cuando despertó contó lo que había soñado, sin embargo Vicente desde ese día no pudo volver a dormir, pues ya no solo lo miraba dormido, también lo miraba despierto y siempre se sentía perseguido, asustado gritaba y llamaba a su mujer pero ella entraba y no observaba nada.]

[Dominga, la esposa de Vicente, decidido salir a buscar ayuda con los vecinos, mas cercanos les relato lo sucedido, entonces fueron a verlo y se asustaron de su comportamiento, y como es costumbre de los comuneros buscaron al médico tradicional “The wala” pues afirmaban que Vicente tenía un maleficio]

[El medico tradicional lo atendió y cuando le hizo un soplo con hiervas frescas para calmar el mal, sintió que Vicente no tenía ningún maleficio sino que había cometido una falta muy grave.]

[Y por eso estaban enojados los espíritus cuidadores de la tierra, y las señas le indicaban al mayor. “The Wala” que iba a pasar algo muy grave.]

[A petición del mayor fue necesario la ayuda de dos médicos “The Wala” mas, se sentaron tres noches noches seguidas para coger candelillas, y así calmar halos espíritus que estaban volviendo loco a Vicente.]

[En la tercera noche los médicos que no le cedían espacio halos espíritus enojados, muy cansados los tres se durmieron, y fue en ese momento cuando los espíritus que cuidan la tierra se volvieron más fuertes.]

[Eran como las tres de la mañana, cuando Vicente rompió los lasos que lo tenían atado para poderlo controlar, salió corriendo perseguido por el hombre negro que lo atormentaba y en la oscuridad, todavía loco no sabia para dónde coger.]

[Cuando el día aclaro, lo fueron a buscar y lo encontraron desnucado en un despeñadero donde lo había llevado el hombre negro y alto. Los médicos se lamentaron porque no consiguieron que los espíritus cuidadores de la tierra, le perdonaran la falta a Vicente.]

[Más la causa del problema todavía permanecía en el cielo raso, de la casa del difunto y había riesgo de que otro habitante de la casa también se volviera loco y le sucediera lo mismo que al difunto Vicente,]

[Con plantas frescas decidieron retirar la olla del lugar donde estaba, y cuando la vieron ya no había oro, sino una culebra verde enroscada dentro de la olla,  mediante rituales sacaron la olla para devolverla de nuevo al mudo que le pertenecía.]

[Los médicos se pusieron de acuerdo porque las señas así le indicaban, la olla debía ser tirada al río, para que nadie más tuviera el infortunio de hacer enojar a los espíritus.]

[Cuando los mayores llegaron al río colocaron la olla encima de unas piedras y uno de ellos dijo, vuelve tranquila al mundo donde perteneces; al momento se oyó un trueno y la olla se fue agua arriba contra la corriente.]

[Desde entonces los The Walas siempre recomiendan que por ningún motivo debemos hacer enojar a los espíritus cuidadores de la tierra, porque así lo han deseado nuestros mayores.]

[Relato contado por: Mayor “The wuala Avelina Campo - Comunera: margarita conda.]

[Municipio de Orito - Departamento: putumayo.]

[Pueblo Nasa. - Cabildo: Kima  The we’x asentamiento nasa cha cxa V/ bellavista.]

[[![Víctor Hugo Baltazar](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Víctor-Hugo-Baltazar--150x150.jpg)

[Pueblo Nasa Putumayo kwe´sx ksxaw.]

------------------------------------------------------------------------

###### *Como parte del trabajo de formación de Contagio Radio con comunidades afros, indígenas, mestizas y campesinas en toda Colombia, un grupo de comunicadores realizó una serie de trabajos periodísticos recogiendo las apuestas de construcción de paz desde las comunidades y los territorios. Nos alegra compartirlas con nuestros y nuestras lectoras.* 
