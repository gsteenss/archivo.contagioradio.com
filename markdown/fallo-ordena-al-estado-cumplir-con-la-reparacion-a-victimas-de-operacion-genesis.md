Title: Tutela ordena al Estado cumplirle a víctimas de Operación Génesis
Date: 2018-12-21 17:26
Author: AdminContagio
Category: DDHH, Nacional
Tags: Corte Interamericana de Derechos Humanos, estado, Operacion genesis, rito alejo del rio
Slug: fallo-ordena-al-estado-cumplir-con-la-reparacion-a-victimas-de-operacion-genesis
Status: published

###### [Foto: Comisión J y P] 

###### [21 Dic 2018] 

Un fallo de tutela de la Sala penal del Tribunal Superior de Bogotá, protegió los derechos de las comunidades de Autodeterminación Vida y Dignidad, CAVIDA, beneficiarias de la sentencia "Marino López vs Colombia" de la Corte Interamericana de Derechos Humanos, al señalar que las entidades llamadas a **cumplir con cada una de las órdenes del fallo internacional no fueron acatadas en más de 5 años**.

### **Los incumplimientos a las víctimas de la Operación Génesis**

Este proceso inició en noviembre del 2013, cuando la Corte Interamericana de Derechos Humanos falló en favor de las comunidades de Autodeterminación Vida y Dignidad, CAVIDA, víctimas de la operación Génesis en 1997, bajo el mando del General Rito Alejo del Río.

En ese momento la Corte ordenó al Estado Colombiano reparar a las víctimas mediante las disposiciones normativas internas, siempre y cuando esas medidas cumplieran los principios de: **inclusión social, razonabilidad y proporcionalidad de las medidas pecuniarias o indemnizaciones colectivas e individuales, enfoque de género y diferencial entre otros.**

Mandatos que, de acuerdo con la abogada Diana Muriel, de la Comisión de Justicia y Paz, representante de las víctimas, hasta el momento tienen el cero por ciento de ejecución. Ejemplo de ello es que **ni siquiera se ha hecho el acto de reconocimiento de responsabilidad por parte del Estado** y petición de perdón a las comunidades.

El Estado tampoco ha cumplido nada en  cuanto a derechos fundamentales como la salud, que sigue sin ser garantizada,  ya que el centro de atención más cercano queda a 3 o 4 horas vía fluvial y no es de carácter gratuito como lo ordenado por la Corte Interamericana. (Le puede interesar:["Neoparamilitares amenazan la Zona Humanitaria de Nueva Vida en Cacarica Chocó")](https://archivo.contagioradio.com/neoparamilitares-amanezan-la-zona-humanitaria-de-nueva-vida-en-cacarica-choco/)

En cuanto a las indemnizaciones, Muriel manifestó que el Estado tampoco esta cumpliendo debido a que, de acuerdo con lo ordenado se señalaba que estos montos tendrían que ser acordados con las víctimas o comunidades, ya que se darían reparaciones colectivas o individuales, y lo que han recibido las personas son indemnizaciones inconsultas, sin reconocer el enfoque diferencial y que no son los montos indicados para los tipos de hechos victimizantes.

"Hace poco se realizó una audiencia de seguimiento en el que se evidencio ante la Corte la falta de cumplimiento y el no avance, **más allá de tener unas reuniones en las que el Estado afirma tener la voluntad y el compromiso, pero no llega a más de eso"** afirmó la abogada.

Asimismo Muriel manifestó que otra de las ordenes de la sentencia tenían que ver con garantizar el goce efectivo de los derechos fundamentales, que actualmente se están viendo vulnerados por la presencia de paramilitares que amedrantan y hostigan a las comunidades.

### **Tutela sobre operación Génesis es **

Este fallo de tutela, representa un avance tanto en el caso en concreto como en la jurisprudencia nacional, dado que una acción de tutela nunca antes favoreció el pago de indemnizaciones o entrega de beneficios directos sino que se limitaba a que el Estado desarrollara acciones en favor de una persona o un grupo.

Sin embargo, el juez constitucional valoró que debido  al reiterado incumplimiento por parte de la Unidad de Víctimas, al negarse a ejecutar las indemnizaciones individuales de acuerdo a lo definido bajo el Decreto Ley 4635, se hace procedente la acción de tutela y le ordena de manera especifica a la institución ceñirse estrictamente a lo dispuesto en la norma.

Por ende, el procedimiento, la forma y los montos para indemnizar a las víctimas deberá ser concertado con los beneficiarios, teniendo como parámetros los definidos por el Tribunal Internacional en la sentencia, esto es que sea proporcional y racional, que permita la inclusión social, que atienda el enfoque diferencial y de genero.

Finalmente el juez constitucional ordenó que en **un plazo de dos meses el Estado debe presentar un programa con fechas definidas para el cumplimiento de cada uno de los mandatos**, teniendo un año a partir de la concreción de este para el cumplimiento.

<iframe id="audio_30964597" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30964597_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
