Title: La iglesia no es neutral, le apuesta a la paz: Monseñor Monsalve
Date: 2018-05-02 14:47
Category: Paz, Política
Tags: ecuador, iglesia católica, Monseñor Monsalve
Slug: avanzan-esfuerzos-para-recuperar-cuerpos-de-trabajadores-de
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Paz-e1499290199642.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [02 Mayo 2018] 

Durante tres días, en la ciudad de Cali, se llevó a cabo el encuentro “Iglesia y construcción de paz hoy en Colombia”, un escenario que permitió hacer un balance sobre la implementación de los Acuerdos de paz de La Habana, el estado actual de la mesa con el ELN y **plantear una coordinación que permita agilizar la entrega de los cuerpos de los trabajadores ecuatorianos del diario El Comercio**.

Para Monseñor Monsalve, Arzobispo de la Diócesis de Cali, uno de los resultados que arrojó este encuentro es priorizar el acompañamiento regional sobre todo en el Pacífico, desde Urabá hasta Tumaco, debido a los cuadros “aterradores” que existen sobre recrudecimiento del conflicto armado. De **igual forma se acordó la construcción de una red de operadores de paz, provenientes de las iglesias**.

“Vimos que era necesario vincular a muchas personas, trabajar mucho más directamente con los párrocos, para que los territorios de las parroquias, de alguna manera, sirvan de espacio para la reconciliación que estamos buscando” señaló Monseñor.

### **Entrega de los cuerpos de los periodistas ecuatorianos** 

Otra de las labores que en este momento lleva la iglesia, es el acompañamiento humanitario en la tarea de la devolución de los cuerpos de los 3 trabajadores ecuatorianos que hacían parte del Diario El Comercio.

“Esa labor la está acompañando la Cruz Roja Internacional, se esta a la espera de que se le responda a alias “Guacho” la petición de un **corredor humanitario que permita distencionar militarmente**, con el gobierno del Ecuador y el gobierno de Colombia, el espacio territorial necesario para que entren los organismos y recojan los cadáveres” expresó Monseñor Monsalve.

### **Una apuesta por la construcción de paz desde la espiritualidad** 

De acuerdo con Monseñor Darío Monsalve, este encuentro fue suscitado por la preocupación que existe frente a conflictos que están surgiendo en las diferentes fronteras del país, casos concretos como la migración venezolana, el conflicto en la frontera con Ecuador y el deterioro que se está viviendo en los esfuerzos por construir paz en las ciudades. Hechos frente a los cuales, aseguro que la "iglesia no es neutral y tiene un compromiso claro por la reconciliación de los colombianos y por las víctimas del país".

El Arzobispo afirmó que el encuentro permitió “entender cómo están las cosas hoy, cómo se hará la transición política que vive Colombia, salvaguardado lo construido hasta hoy y cómo va a ser el planteamiento de la iglesia en los próximos años”. (Le puede interesar: ["Solicitan medidas cautelares para proteger a los líderes sociales"](https://archivo.contagioradio.com/solicitan-medidas-cautelares-a-cidh-para-proteger-a-los-lideres-sociales/))

<iframe id="audio_25749681" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25749681_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
