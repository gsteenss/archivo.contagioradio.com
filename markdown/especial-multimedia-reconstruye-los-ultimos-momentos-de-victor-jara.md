Title: Especial multimedia reconstruye los últimos momentos de Victor Jara
Date: 2016-09-16 13:40
Category: Otra Mirada
Tags: Dictadura chilena, Documentales, Victor Jara
Slug: especial-multimedia-reconstruye-los-ultimos-momentos-de-victor-jara
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/jara.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotograma: Los últimos días de Victor Jara 

###### [16 Sep 2016] 

La cadena pública de televisión chilena TVN, transmitió este jueves un especial dedicado a la conmemoración de los 43 años de la tortura y asesinato del cantautor **Victor Jara**, ocurrido el 15 de septiembre de 1973, durante los primeros días de la dictadura militar de Alfonso Pinochet. (Lea también [Familia de Victor Jara exige justicia a 40 años de su asesinato](https://archivo.contagioradio.com/familia-de-victor-jara-exige-justicia-a-40-anos-del-asesinato-del-cantautor/))

La dirección de arte y producción del especial, estuvo a cargo de **Christian Rojas**, quien junto con su equipo de trabajo se encargó de reconstruir los últimos momentos de vida del artista, intercalando locaciones y testimonios reales con imágenes incorporadas a través de técnicas de animación digital y análoga.

El trabajo de producción está solventado en datos del **expediente judicial** y complementado con las **versiones de algunos de los detenidos** que compartieron con Jara reclusión en el Estadio Nacional, hoy renombrado con su nombre, construyendo un relato estremecedor dividido en cuatro momentos clave: "El día del golpe de estado", "Detenido en el Estadio de chile", "La muerte" y "El rescate de su cuerpo"

<iframe src="https://player.vimeo.com/video/182784989" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>  
El especial multimedia completo se encuentra disponible [aquí](http://www.24horas.cl/victor-jara/index.html).
