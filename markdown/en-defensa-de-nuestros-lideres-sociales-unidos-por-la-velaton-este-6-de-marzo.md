Title: ¡En defensa de nuestros líderes sociales! Unidos por la Velatón este 6 de marzo
Date: 2019-03-05 12:51
Author: AdminContagio
Category: DDHH, Líderes sociales
Tags: lideres sociales, MOVICE, Velatón
Slug: en-defensa-de-nuestros-lideres-sociales-unidos-por-la-velaton-este-6-de-marzo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/20180706_203927.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Archivo 

######  5 Mar 2019

Más de 127 organizaciones de víctimas, sociales y de derechos humanos, invitan a la ciudadanía a la Velatón nacional el próximo 6 de marzo en defensa de la vida de los líderes y lideresas del país. El evento se realizará simultáneamente en ciudades como Bogotá, Barranquilla, Medellín, Cali, Villavicencio, Manizales, Ibagué entre otras.

**Luz Marina Hache, vocera del Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE)** indica que en Bogotá esperan realizar actividades desde las 2:00 pm, al comenzar con la elaboración de un mandala en homenaje a las víctimas de crímenes de Estado. A las 4:00 pm se realizará un performance de mujeres en escena a cargo de Patricia Ariza y a las 5:00 pm habrá un ejercicio de memoria para reivindicar a líderes sociales a cargo de integrantes de diferentes organizaciones.

A las 5:30 pm la mesa ecuménica realizará una vigilia por la paz y la velatón comenzaría a partir de las 6:00 pm. Tal como informa la vocera del MOVICE se realizará un duelo nacional "porque la vida de la gente es de todos, debemos defenderla y exigirle al Estado que asuma las medidas y el cuidado de los líderes, tanto sociales como de todos los defensores de DD.HH.". [(Le puede interesar Unas velas, el poder de lo nuevo) ](https://archivo.contagioradio.com/unas-velas-el-poder-de-lo-nuevo/)

### **"Hoy como ayer el Estado colombiano tiene una responsabilidad en estos delitos"** 

La vocera agrega que desde el MOVICE se han venido cuestionando las políticas de seguridad del actual Gobierno, pues "es un abierto desconocimiento a lo pactado en el proceso de paz con las Farc",  una razón más según Hache, para exigirle al Gobierno que asuma y garantice la vida de las y los defensores de derechos y de todos los colombianos, y así evitar que la historia de violencia en el país se repita.[(Lea también: Plazas de Colombia y el mundo encienden una luz por los líderes sociales ) ](https://archivo.contagioradio.com/plazas-colombia-encienden-luz-por-lideres-sociales/)

Con la Velatón, señala Luz Marina Hache, esperan que la sociedad acompañe la movilización y que tenga una participación similar a la del 2018, además junto a las organizaciones firmarán una carta dirigida al Consejo de seguridad de la ONU pidiéndole vigilen la situación que se está viviendo en Colombia.

<iframe id="audio_33091480" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33091480_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
