Title: No fue un "desastre natural" la tragedia en Mocoa
Date: 2017-04-02 14:29
Category: Ambiente, Voces de la Tierra
Tags: desastre natural, Mocoa, Putumayo
Slug: no-fue-desastre-natural-lo-ocurrido-mocoa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/mocoa-1-e1491162828390.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión de Justicia y Paz 

###### [2 Abr 2017] 

La tragedia que hoy enluta a Colombia, por la muerte de **más de 200 personas a causa de una avalancha en Mocoa, Putumayo,** no necesariamente puede ser tildada de manera literal como un desastre natural. Según diversas versiones de expertos el dramático panorama del que hoy es testigo el país era una tragedia anunciada, que se deriva del equivocado uso del suelo en un departamento que hace parte de la región amazónica.

Aunque suele ser normal las intensas temporadas de lluvias en esta zona del país, esta vez, la situación fue tal que en unas cuantas horas cayeron más de **600 militros de agua. Una situación que provocó** **el desbordamiento de los ríos Mocoa, Sangoyaco y Mulato.** Aguas que acabaron con todo lo que estaba a su paso generando una avalancha que arrasó con varios barrios de Mocoa, generando que el Gobierno declarara el estado de calamidad pública, pues además de las vidas perdidas, ** hay  213 heridos, un número aún indeterminado de desaparecidos, entre los que [se encuentran 34 indígenas Nasa,](http://www.justiciaypazcolombia.com/)adicionalmente no hay luz, agua ni gas.**

“**En Colombia y en el mundo se ha avanzado en rescatar a los náufragos pero muy poco en evitar los naufragios**", dijo en su cuenta en Twitter, el ambientalista Wilches Chaux tras conocerse la situación que acontecía en Putumayo. Además en un reciente artículo que había escrito el experto refiriéndose a una tragedia similar que hace algunos días vivió Perú, señala que cuando ese tipo de acontecimientos suceden, se suele hablar de ‘desastres naturales’, “como si fuera la naturaleza la que los produce y no fueran nuestras decisiones u omisiones las que generan las condiciones que hacen inevitable que se produzca un desastre", y añade que al catalogar esos hechos de esa forma, se impide identificar y dar solución adecuada a las causas del trauma.

Y es que el origen del desastre en Mocoa no es “natural”. Luis Alexander Mejía,  director de la Corporación para el Desarrollo Sostenible del Sur de la Amazonía, Corpoamazonía, explica en una entrevista de Semana Sostenible que este acontecimiento estaba advertido y era perfectamente evitable gracias a estudios que se habían dado a conocer **hace nueve meses cuando se hizo un taller con el Servicio Geológico Colombiano "donde advertimos que esto podía pasar por el uso inadecuado de los suelos** que agrava este tipo de eventos. Además, indicamos que varios municipios amazónicos, incluido **Mocoa**,** no habían actualizado su Plan de Ordenamiento Territorial (POT)**. Por eso ha sido complejo concertar e implementar los determinantes ambientales en esta zona”.

### Ganadería, minería y construcción de vías 

Putumayo es el quinto departamento de Colombia con mayores índices de deforestación. **Más de 9 mil hectáreas del departamento se han convertido en potreros**, zonas usadas ya sea para la ganadería extensiva o  los cultivos de uso ilícito. Para el director de Coporamazonía, esta es una de las principales causas de lo ocurrido ya que “Cuando quitamos la cobertura vegetal, la tierra, que está compuesta en gran parte por ceniza volcánica, se impregna de humedad y con el agua de lluvia se causan estragos”. De manera que el acabar con los bosques facilita las remociones en masa.

El mal manejo del uso del suelo en este departamento se da también por los impactos indeterminados en la construcción de **la variante San Francisco - Mocoa (VSFM),** en particular al aporte de sedimentos de los taludes intervenidos por la Variante a las microcuencas del río Mocoa, siendo esta otra de las posibles causas como lo indica la Revista Semana en una entrevista con el ambientalista Rodrigo Botero.

Por su parte, la Comisión Intereclesial de Justicia y Paz,  agrega que el **desarrollo de proyectos de explotación minera, han contribuido al aumento de la vulnerabilidad de unas condiciones geológicas** que han desembocado en este tipo de tragedias. “Más allá de ser un desastre natural, la calamidad que hoy enfrenta el departamento de Putumayo, es producto de la falta de previsión del riesgo, planificación y ordenamiento territorial, así mismo, las condiciones de exclusión e inequidad que obliga a los habitantes empobrecidos a ubicar sus viviendas en zonas de alto riesgo”, manifiesta la ONG.

Esta situación, entonces es el reflejo de la deficiente protección ambiental sumada a los vacíos existentes en la gestión del riesgo, que para Wilches Chaux son dos maneras de decir lo mismo. **“Una buena gestión ambiental es la mejor gestión del riesgo”.**

\

###### Reciba toda la información de Contagio Radio en [[su correo]
