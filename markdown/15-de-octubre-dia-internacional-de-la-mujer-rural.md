Title: 15 de octubre día internacional de la mujer rural
Date: 2016-10-15 09:57
Category: Mujer, Nacional
Tags: campesinos, Mujer campesina
Slug: 15-de-octubre-dia-internacional-de-la-mujer-rural
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/mujer-campesina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [15 de Oct 2016] 

De acuerdo a lo establecido por la Asamblea General de la ONU en la resolución 62/136 de 2007, **desde 2008 cada 15 de octubre se celebra el día internacional de las mujeres rurales.** Para 2016 estas mujeres representan una cuarta parte de la población mundial, de las cuales, el 43% se encuentran en los llamados países en vía de desarrollo desempeñando las labores de producción, procesamiento y la preparación de gran parte de los alimentos.

De acuerdo al informe presentado por el Semillero de Investigación en Desarrollo Rural de la Universidad Nacional, en América Latina y el Caribe las mujeres constituyen casi la mitad de la población en áreas rurales. En el caso de Colombia, s**on más de 5 millones las mujeres rurales** en su diversidad: afrocolombianas, indígenas, campesinas y jóvenes las cuales representan un 46% de las mujeres colombianas.

Además de esto, el Centro de Investigación y Educación Popular (CINEP), señaló para 2016 que e**l 19,7% no reciben ningún tipo de educación**; 1**2,6% mayores de 15 años no saben leer ni escribir**; 50% de los hogares desplazados tienen una jefatura femenina; y del total de **11.954 solicitudes de ingreso al registro de las tierras despojadas y abandonadas forzosamente 4.389 son de mujeres.**

La situación en torno a las mujeres y el campo deja en evidencia las desigualdades históricas en el reconocimiento del rol que estas desempeñan como sujetas claves para el desarrollo social. **El trabajo históricamente asignado como es el de la alimentación y el cuidado de los menores, adultos y adultos mayores no es considerado como tal y tampoco remunerado.**

Aun cuando estas participan activamente en el mantenimiento familiar y en las primeras etapas del ciclo agropecuario, el derecho sobre la tierra, la falta de oportunidades económicas, el no ocupar lugares de poder y el bajo acceso a la educación y a los recursos productivos impide el avance de las mujeres en materia de sus derechos.

En marzo del 2016, la *Mesa de Incidencia Política de las Mujeres Rurales Colombianas*, la cual congrega a 150 organizaciones, llevó a cabo una audiencia pública que tenía como objetivo analizar la situación actual del campo y las mujeres, además de analizar los avances o estancos en la aplicación y cumplimiento de la ley 731 de 2002 en la que se busca garantizar los derechos de las mismas en el ámbito rural.

El balance realizado por las organizaciones en esta audiencia pública no fue nada satisfactorio, **aún existen problemáticas relacionadas con la capacidad presupuestal dirigida a los proyectos, titulación de tierras, capacitación y seguimiento en los procesos.**

En 2014, la Confederación Internacional OXFAM señaló que sólo el **0.8% de las mujeres se habían beneficiado del Programa Rural para Mujeres implementado desde el 2011**, mientras que el Programa de las Naciones Unidas para el Desarrollo (PNUD) señalaba la aún existente triple discriminación de la mujer en el campo; por su condición de ser mujer en un mundo rural con oportunidades restringidas; la estructura patriarcal; y los altos niveles de violencia tanto intrafamiliar, como la generada por la guerra a la cual están constantemente expuestas.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU)
