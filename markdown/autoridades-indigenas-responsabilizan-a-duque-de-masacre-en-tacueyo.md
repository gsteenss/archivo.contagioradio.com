Title: Autoridades indígenas responsabilizan a Duque de masacre en Tacueyó
Date: 2019-10-30 18:08
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asesinato de indígenas, Cauca, tacueyo
Slug: autoridades-indigenas-responsabilizan-a-duque-de-masacre-en-tacueyo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/CRIC-CAUCA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

Tras el asesinato de cinco integrantes de las comunidades indígenas en la vereda La Luz, en Tacueyó, Cauca, las autoridades indígenas afirman que esta es una consecuencia del no cumplimiento del Acuerdo de Paz y señalan como responsable al Gobierno Duque, aseverando que desde su nombramiento **han sido asesinados 125 integrantes de los pueblos ancestrales.**

**Rubén Escué, integrante de la Organización Nacional Indígena de Colombia (ONIC), quien estuvo presente en el lugar de los hechos** relata que, como parte del ejercicio de control territorial y ante la llegada de una [caravana de vehículos blindados con personal armado, se activaron los puntos de control con camionetas al servicio de la guardia y se les impidió el paso lo que ocasionó la arremetida con ráfagas de armas de fuego en contra la guardia indígena y las personas presentes.]

[Organizaciones como la **Asociación de Cabildos Indígenas del Norte del Cauca (ACIN)** ACIN señalan que los atacantes, a quienes se ha ligado con disidencias de las FARC-EP habrían atacado también las ambulancias que se desplazaron hasta el sector a atender a los heridos.]

En medio de este ataque, fueron asesinados la autoridad Nasa, Cristina Bautista y los guardias Eliodoro Uniscue,  Asdruval Cayapu, José Gerardo Soto y James Wilfredo Soto, mientras que también resultaron heridos Matías Montaño Noscué, José Norman Montano, Dora Rut Mesa Peteche, Rogelio Tasquinas, Alver Cayapú y Crescencio Peteche, este último se encuentra en grave estado de salud.

"En este momento hay una situación de incertidumbre, unos están muy ofendidos, otros nos sentimos impotentes, porque no somos grupos armados y nos tratan de una forma militar" manifestó Rubén Escué.  [(Le puede interesar: Cinco personas asesinadas y varios heridos deja atentado contra comunidad indígena de Tacueyó)](https://archivo.contagioradio.com/dos-personas-asesinadas-y-varios-heridos-deja-atentado-contra-comunidad-indigena-de-tacueyo/)

### ¿Cuál fue el rol de la Fuerza Pública en Tacueyó? 

Rubén, señaló que el Ejército únicamente movilizó tanquetas hasta la cabecera municipal de Tacueyó y desmintió las afirmaciones de las Fuerzas Militares quienes argumentaron mediante un comunicado, que el ataque pudo haberse suscitado tras retener a integrantes de este grupo armado, **"que la Guardia haya capturado a alguno de ellos y luego hayan arremetido es mentira, eso no sucedió, en ese momento había un control territorial pero no se había detenido a nadie, no había una justificación"** .

De igual forma, las comunidades indígenas desmintieron que se haya contactado o coordinado acciones con la Fuerza Pública, ya que su accionar fue independiente y no respondió eficazmente a la gravedad de los hechos. La ONIC aseguró que se ha intentado coordinar la erradicación de cultivos de uso ilícito presentes en los territorios y que han derivado en una mayor actividad de los grupos armados en la zona.

### Comunidades indígenas exigen la renuncia de Nancy Patricia Gutiérrez y Guillermo Botero

Este 30 de octubre, la ONIC citó a una rueda de prensa para dar respuesta a la ola de violencia que vive el departamento del Cauca, en la que Luis Kankui, consejero mayor responsabilizó al Gobierno de Iván Duque **"de la masacre y el genocidio al que están sometidos los Pueblos Indígenas en Colombia"**.

Acompañando a las autoridades indígenas se encontraban varios congresistas, entre ellos María José Pizarro quien exigió la renuncia del ministro de Defensa, Guillermo Botero y de la ministra del Interior, Nancy Patricia Gutiérrez esto debido a  la forma ineficaz en que se ha hecho frente a la crisis del departamento.

A este llamado se sumó el senador Alexander López, quien reiteró que desde el Congreso se mantendrá la moción de censura para los ministros de Defensa e Interior por ser "cómplices y sujetos pasivos a la hora de garantizar la vida de nuestros pueblos indígenas como lo que ocurrió ayer en Cauca".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
