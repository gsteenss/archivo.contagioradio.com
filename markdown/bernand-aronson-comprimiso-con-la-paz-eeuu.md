Title: Nombramiento de Bernad Aronson concretaría el compromiso de EEUU con el proceso de paz en Colombia
Date: 2015-03-03 17:52
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Bernard Aronson, Conversaciones de paz en Colombia, ELN, FARC, Responsabilidad de Estados Unidos en la guerra en Colombia, WOLA
Slug: bernand-aronson-comprimiso-con-la-paz-eeuu
Status: published

###### Foto: lavoz.com.ar 

<iframe src="http://www.ivoox.com/player_ek_4159893_2_1.html?data=lZaim52dd46ZmKiak5uJd6KnmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKf08rg0tTSt8LWytHWxsbIb8XZjKqyt7qPqc%2BfzcaYydrJttPVjMrbjajTsNDhw87Oj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Entrevista a Gimena Sánhez de WOLA] 

Según Gimena Sánchez integrante de la organización WOLA, es muy **simbólico que el nombramiento se haya dado después de la visita del Senador Uribe** a ese país para sabotear el proceso de paz. Básicamente Uribe estuvo abogando porque la administración de Obama no libere a integrantes de las FARC presos en ese país, que no paren las fumigaciones y que se mantenga la extradición.

Según Sánchez, Aronson tiene una historia de **mediación en el proceso de paz en El Salvador** y fue quien pudo convencer a los sectores más conservadores sobre la conveniencia de la paz en ese país y la necesidad de apoyarlo, así las cosas Bernard será un personaje que muchas personas escucharán.

Sin embargo, Sánchez señala que se conocen algunos **vínculos empresariales de Aronson en el departamento del Meta y el Putumayo.**

Respecto a la responsabilidad de EEUU en el conflicto colombiano, Sánchez señala que es muy importante puesto que ese país es el **donante más importante en cuanto a política de lucha contra las drogas** y en el marco del proceso hay un acuerdo al que EEUU tendrá que adaptarse. También resalta los condicionantes en cuanto a **Derechos Humanos** que tiene la ayuda militar y ese aspecto tiene mucho que ver el tema de la justicia transicional.

Estados Unidos es un país que “por bien o mal” Colombia escucha mucho, así que su aporte puede ser fundamental en materia de **trabajar contra las voces y los sectores militares que se oponen al proceso de conversaciones de paz.**

La delegación de paz de las FARC se reunió en dos ocasiones con Aronson, pero se desconocen los temas tocados en este encuentro, entre ello, no se sabe si uno de los puntos del encuentro fue la posibilidad de que **“Simón Trinidad” o “Sonia”** sean repatriados a Colombia.

En Estados Unidos hay debate en torno a la posibilidad de cese al fuego, señala Sánchez, sin embargo, esta posibilidad se aleja por la continuidad de las fumigaciones, lo importante es que ese es tema de debate al cual se está buscando una salida desde el año 2000.

Frente al derecho a la verdad Gimena Sánchez resalta que es necesario que los archivos clasificados de los **organismos de seguridad tendrían que salir a la luz y desclasificarse para que se abran las puertas a la verdad en Colombia.** Por otra parte es preocupante que no haya un nivel de debate abierto en torno a las conversaciones con el **ELN.**
