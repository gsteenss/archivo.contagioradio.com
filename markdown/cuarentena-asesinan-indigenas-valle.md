Title: Acatando cuarentena en su hogar, asesinan a dos líderes indígenas en Valle del Cauca
Date: 2020-03-24 17:28
Author: CtgAdm
Category: Actualidad, DDHH
Tags: ONIC, Valle del Cauca
Slug: cuarentena-asesinan-indigenas-valle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/asesinato-de-lider-indigena-en-tacueyó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: indígenas / CRIC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En la noche de este 23 de marzo, en hechos ocurridos en el corregimiento de Naranjal en Bolívar, Valle del Cauca fueron asesinados los líderes indígenas Omar y Ernesto Guasiruma del pueblo Embera, mientras permanecían en su hogar como parte de la cuarentena decretada por el Gobierno como respuesta al Covid 19. **Al menos 66 indígenas colombianos fueron asesinados en 2019, según la Oficina de la Alta Comisionada de la ONU para Derechos Humanos (ACNUDH).**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Luis Fernando Árias, consejero mayor de la [Organización Nacional Indígena de Colombia (ONIC),](https://twitter.com/ONIC_Colombia) **los líderes del resguardo del Cañón de Las Garrapatas fueron atacados por dos hombres sin identificar que se movilizaban en una moto** y que dispararon contra las personas que se encontraban al interior de la vivienda. [(Lea también; ONIC: 40 años de resistencia histórica)](https://archivo.contagioradio.com/onic-40-anos-trabando-por-la-vida-y-los-territorios-de-los-pueblos-indigenas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el líder indígena, el hecho de que Omar y Ernesto Guasiruma fueran atacados mientras permanecían en su hogar evidencia que existía un plan de seguimiento y de inteligencia, "ni estando en casa estamos seguros, parece que vamos a ser presa más fácil para los actores armados". Al menos 66 indígenas colombianos fueron asesinados en 2019, según la ONU

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Luis Fernando Árias resalta que la vivienda no se encontraba en un lugar apartado sino que hacía parte del corregimiento de Naranjal, lugar del que fueron evacuadas José y Víctor Guasiruma hermanos de las víctimas quienes resultaron heridos y que fueron atendidos en Cali y Tuluá. [(Lea también: Un adulto y dos menores indígenas, víctimas fatales de minas en Antioquia y Chocó)](https://archivo.contagioradio.com/un-adulto-y-dos-menores-indigenas-victimas-fatales-de-minas-en-antioquia-y-choco/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Comunidades indígenas toman medidas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Como medida de seguridad, Árias expresó que **se restringirá el paso de cualquier persona que salga o ingrese del resguardo mientras la Guardia Indígena continuará en puntos de vigilancia,** "nuestro propósito es evitar que tanto la criminalidad como esta enfermedad llegue a los territorios indígenas de Colombia", concluyo. [(Le recomendamos leer: Asesinan a tres integrantes de guardias campesina e indígena durante fin de semana en Cauca)](https://archivo.contagioradio.com/campesinos-indigenas-en-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dichas medidas de aislamiento no solo han sido abordadas por las comunidades indígenas embera, las autoridades indígenas del Pueblo Arhuaco de los centros de Yewrwa y Simunurwa en la Sierra Nevada de Santa Marta anunciaron que ejercerán control territorial para prevenir el Covid 19 mientras el Pueblo Wayúú ha solicitado al Gobierno ayuda humanitaria para sumarse a la cuarentena, pues no existe alimentación ni agua para evadir riesgos de contagio

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
