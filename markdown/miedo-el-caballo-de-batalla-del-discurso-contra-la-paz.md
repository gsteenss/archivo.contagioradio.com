Title: Miedo: El caballo de batalla del discurso contra la paz
Date: 2016-06-15 10:15
Author: AdminContagio
Category: Cindy, Opinion
Tags: Centro Democrático, proceso de paz
Slug: miedo-el-caballo-de-batalla-del-discurso-contra-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/EL-MIEDO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Roto 

#### **[Cindy Espitia Murcia](https://archivo.contagioradio.com/cindy-espitia/) - [@CindyEspitiaM](https://twitter.com/CindyEspitiaM)** 

###### 15 Jun 2016

[El ser humano actúa, principalmente – ojo, no exclusivamente -, por las emociones y los expertos en comunicación política sí que lo tienen claro. Apelar a la emoción más que al intelecto ha sido una estrategia quizá tan antigua como la humanidad, pero hoy, tal vez, es más evidente.]

[En la práctica se puede apuntar a generar emociones positivas, como la confianza, orgullo y esperanza, y negativas como la ira, la incertidumbre y]**el MIEDO.** [Sí. Pongo la palabra]**MIEDO** [en mayúscula y negrilla porque ha sido una de las principales herramientas discursivas utilizadas a lo largo de la historia que ha garantizado el acceso al poder de muchos y ha justificado ciertas actuaciones, algunas de ellas reprochables. Por ejemplo, el miedo fundamentó la campaña de Macri en Argentina, limitó la oposición en Venezuela en el gobierno de Chávez,  consolidó la campaña de Uribe en el 2002 y lo mantuvo en los niveles más altos de favorabilidad por ocho años, motivó la guerra de Estados Unidos contra Irak en el 2003 y ha tratado de justificar la aplicación de la ‘legítima defensa preventiva’ a nivel internacional.]

En el marco del proceso de paz en Colombia, los opositores, con su iniciativa de ‘resistencia civil’ (que, por cierto, su nombre es aterrador en sí mismo) han acudido a interesantes símbolos y recursos retóricos para posicionar su mensaje. En su discurso es común escuchar frases como “Santos le ha entregado el país al terrorismo”, “el acuerdo es un golpe de Estado”, “la paz de Santos provoca guerra a machete entre campesinos”, “la paz de Santos es el exterminio de las FFAA”, “no es una negociación sino una claudicación”,  “la impunidad de La Habana ha multiplicado por 5 a las bandas criminales” e incluso se alcanzado a hablar de “la primavera árabe en Colombia”.

Aunque la imprecisión reina en estas frases, se observa, por un lado, el uso reiterado de palabras fuertes y alarmantes, por supuesto para aumentar el impacto del mensaje y, por el otro, es claro que se busca generar la sensación de que ante la firma del acuerdo de paz se entrará en un estado de incertidumbre y de caos que amenazará, no sólo nuestra seguridad, sino el fundamento democrático de nuestro país.

¿Aterrador? En efecto lo es. Especialmente para un país que ha sufrido por más de 50 años la guerra, cuyos niños han crecido en medio de las balas y cuyas madres han llorado la muerte sus hijos. Por supuesto que es aterrador imaginar que un país que ha vivido con miedo por cincos década por causa de la guerra pueda vivir circunstancias aún peores.

Ahora bien, el miedo no es en sí mismo malo. Pero lo es, cuando es creado, como en este caso, por mentiras, falacias e incluso medias verdades;  lo es cuando por medio de éste se busca obstruir un fin legítimo como la paz.

Parece entonces que el eventual proceso de refrendación de las acuerdos de paz estará enmarcado en una batalla retórica entre el discurso del miedo, promovido por los opositores de la paz, y el discurso de la esperanza, liderado y apoyado por los que creemos en la salida pacífica del conflicto y en una Colombia sin guerra.

Yo no tengo miedo. Yo creo, voto y votaré por la esperanza y desde ya le digo \#SiALaPaz ¿Y usted?

Espero que en esta lucha, la esperanza, de una vez por todas, sea la vencedora.

[Pdta: Como buena fanática de la película de los Juegos del Hambre, debo terminar esta columna con la siguiente frase: “El miedo es efectivo. Pero hay algo más poderoso que el miedo. Y es la]**esperanza”.**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
