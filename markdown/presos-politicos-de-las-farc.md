Title: Estado sigue sin cumplir ley de amnistía a 22 días de huelga de hambre de presos políticos de las FARC
Date: 2017-07-18 13:22
Category: DDHH, Política
Tags: FARC, Huelga de hambre, Ley de Amnistia, presos políticos de las FARC
Slug: presos-politicos-de-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/presos-politicos-e1498597659899.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/presos-politicos-e1498597659899.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/libertad-para-los-presos-politicos.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Univisión] 

###### [18 Jul 2017] 

Los presos políticos de las FARC continúan protestando **por el cumplimiento de la liberación de los miembros de esa guerrilla** que están detenidos en las diferentes cárceles del país. Jesús Santrich, quien aún se encuentra en huelga de hambre, está en recuperación en la clínica Shaio de Bogotá.

Andrés Paris, miembro del estado mayor de las FARC, manifestó que la salud de Santrich está mejorando debido a una recaída que sufrió en días pasados. Paris afirmó que “él persiste con la **protesta para exigir el rápido cumplimiento de los compromisos de amnistía por parte del gobierno**”. Aseguró que el incumplimiento de la ejecución de la ley de amnistía e indulto, “es una muestra de una estrategia política que busca entorpecer los acuerdos de paz”. (Le puede interesar: "M[ás de 14oo presos políticos de las FARC continúan en huelga de hambre"](https://archivo.contagioradio.com/presos-politicos-de-las-farc-mantienen-la-huelga-de-hambre/))

Frente al estado de salud de los prisioneros políticos Paris comentó que **“hay una crisis humanitaria severa** y algunos han sido heridos y maltratados en cárceles como la de Bella Vista de Cali”. Sin embargo, dijo que ellos y ellas continuarán con la desobediencia civil hasta que los jueces y el gobierno les solucione la situación jurídica y sean liberados cada uno de ellos.

De igual forma Paris recordó que los anuncios del Alto Comisionado para la Paz, Sergio Jaramillo, de **liberar a 800 prisioneros en este mes no se han hecho efectivos**. “Hasta el momento solo han salido grupos pequeños de 20 prisioneros que no corresponden con el compromiso de salida que había manifestado el gobierno”. Dijo que no puede suceder que “cuando se trata de que la insurgencia cumpla con lo pactado hay una presión mediática, pero hay una lentitud en el cumplimiento de las exigencias por parte del gobierno”. (Le puede interesar: ["Presos políticos de las FARC exigen que se cumpla ley de amnistía pactada"](https://archivo.contagioradio.com/presos-politicos-de-las-farc-exigen-que-se-cumpla-la-ley-de-amnistia-de-los-acuerdos/))

Paris manifestó que las **organizaciones internacionales y civiles en el país se han unido a la protesta de los presos políticos** y han manifestado su solidaridad. Ellos y ellas han hecho una denuncia para generar un movimiento que abogue por el cumplimiento de los acuerdos por parte del gobierno colombiano. Mientras tanto, siguen en huelga de hambre más de 1300 presos en los diferentes centros penitenciarios del país.

<iframe id="audio_19868604" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19868604_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
