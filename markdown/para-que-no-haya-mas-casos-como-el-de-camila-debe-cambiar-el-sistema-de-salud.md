Title: Para que no haya más casos como el de Camila debe cambiar el sistema de salud
Date: 2015-02-25 21:03
Author: CtgAdm
Category: Economía, Nacional
Tags: Camila Abuabara, Ley estatutaria de la salud, Román Vega, sistema
Slug: para-que-no-haya-mas-casos-como-el-de-camila-debe-cambiar-el-sistema-de-salud
Status: published

###### Foto: Carmela María 

##### <iframe src="http://www.ivoox.com/player_ek_4134317_2_1.html?data=lZaglpiVe46ZmKiak5uJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMLmwpDe18qPstCfycbmw5DRaaSnhqae1ZDHpdTj1JDQ0dLTb8bgjMnSjajFscrgwpDRx8fJb8TVzseah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Román Vega, Ex-Secretario de Salud] 

 

Camila Abuabara, la joven bumanguesa de 25 años de edad, murió la noche del martes 24 de febrero, tras seis años de resistir a un cáncer de médula ósea. El caso de Camila, sin embargo, no es excepcional. Ocurre en nuestro país con más regularidad de la señalada por los medios de información.

La lucha de Camila durante 6 años, y la de miles de colombianos y colombianas, ha sido contra un sistema de salud que les ha negado la posibilidad de atender sus enfermedad de manera oportuna, y la posibilidad de evitar -en muchos casos- que las personas pierdas su calidad de vida o mueran.

Para el ex-Secretario de Salud y profesor de la Universidad Javeriana, Doctor Román Vega, en el caso de Camila, la EPS no respondió de manera oportuna a las necesidades de tratamiento, y esto redujo drásticamente sus posibilidades de vivir. Incluso la opción tardía de tratarla en los Estados Unidos fue negada por la EPS Sanitas, y solo le fue transplantada la médula ósea que podría salvarla, cuando una sentencia de la Corte Constitucional obligó a la Entidad a efectuarla hace sólo un par de meses, dando como resultado el desenlace que hoy se conoce.

El problema radica, según lo señalado por el Doctor Vega, en que el sistema de salud está concebido en una lógica de mercado, en que las EPS obtienen rentas al reducir el gasto en la atención en salud, poniendo todo tipo de barreras, como el control a las prescripciones médicas y al tratamiento de enfermedades. "Es la contradicción entre el *valor de uso* del derecho a la salud, y el *valor de cambio* que quieren generar los intereses privados para obtener ganancias. Es el modelo capitalista en la salud", declaró.

Lejos de pensar, como señala el Ministro de Educación, que el problema se resuelve con la implementación de la Ley Estatutaria de la Salud, para el ex-Secretario, el núcleo de la contradicción se mantiene en la medida en que -replicando los errores de la constitución de 1991- la Ley Estatutaria plantea la Salud como un derecho fundamental mientras mantiene la intermediación financiera. Un agridulce sin salida.

Mientras el ministro Gaviria repite que, frente al tratamiento de las enfermedades, no todos los colombianos "pueden comer langosta", para Román Vega el presupuesto de la salud es efectivamente insuficiente en la medida en que el gasto público se va en guerra y en el pago de la deuda externa; mientras el poco dinero que queda para el derecho a la salud es mal asignado, quedando en manos de los negociantes.

Lo que hace falta para garantizar el derecho a la vida y a la salud es, para el profesor universitario, no solo garantizar que se traten las enfermedades de manera oportuna, sino que se trate la salud de manera integral, incluyendo la posibilidad de la prevención. El Plan Nacional de Desarrollo tampoco encamina recursos en ese sentido, ni crea un ambiente garante de condiciones que impactan en el individuo, en los grupos familiares y en la sociedad: alimentación adecuada, medio ambiente, vivienda y trabajo de calidad, que eviten los innumerables problemas de salud de los colombianos y las colombianas. Por el contrario, si la Ley Estatutaria y el PND hablan de la salud como un derecho fundamental, lo hacen con la intensión de "limitar el derecho, restringirlo en el marco de la regla fiscal".

Por lo tanto, en Colombia, continuaría vigente la discusión y la lucha de Camila, y de miles de víctimas fatales del modelo, por un cambio en el modelo de sistema de salud. Advierte Román Vega que el gobierno actual no está dispuesto a hacerlo, por lo que se requeriría en Colombia un nuevo gobierno democrático, que acabe con las contradicciones de la Constitución de 1991, planteando una nueva carta de Derechos, y un nuevo sistema de salud.

 
