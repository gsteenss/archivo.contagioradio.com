Title: Presentarán firmas al presidente Santos exigiendo respeto a consultas populares
Date: 2017-07-19 17:16
Category: Ambiente, Nacional
Tags: consultas populares, Juan Manuel Santos, Mineria
Slug: presentaran-firmas-al-presidente-santos-exigiendo-respeto-a-consultas-populares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/consulta-popular.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comité Ambiental 

###### [19 Jul 2017] 

Desde el Comité Ambiental en Defensa de la Vida y la Red Internacional de Derechos Humanos (RIDH), se está recolectando una serie de firmas que el próximo 22 de julio, serán entregadas al gobierno nacional y específicamente al presidente Juan Manuel Santos, con las que se pide que **se respeten los resultados de la consultas populares que se han realizado, donde las comunidades han demostrado su rechazo a las actividades extractivas.**

Las firmas que serán entregadas en el marco del Día Mundial Contra la Minería a Cielo Abierto, y se recolectan a través de una petición en la página web de **La Red Internacional para los Derechos Económicos, Sociales y Culturales** que conecta a más de 280 ONG, movimientos sociales y activistas de 75 países. Una iniciativa mediante la cual se busca construir un movimiento global en defensa de los derechos humanos, y por tanto al ambiente.

Jaime Tocora, Integrante del Comité Ambiental, señala que la idea nace al observar lo que ha sido la respuesta del gobierno nacional negando los resultados  de las recientes consultas populares en donde, como mínimo, **el 96% de los habitantes de los municipios han dicho NO  a la puesta en marcha o la continuidad de proyectos minero-energéticos.**

“Es una imposición la del gobierno nacional, por eso estamos recogiendo estas firmas de diferentes lugares y organizaciones para que respete la  libertad popular”, expresa Tocora, y agrega “No se puede hablar de paz si se declara la guerra a la naturaleza y a  las comunidades (…) lo que se está dando es una nueva colonización desde el gobierno de la mano de las multinacionales”.

### Construcción de alternativas 

En medio de la disyuntiva que está planteando el gobierno, las comunidades y organizaciones ambientalistas se están uniendo y fortaleciendo para dar soluciones alternativas al extractivismo, que traigan un desarrollo social, ambiental y económico a los territorios.

Una de esas propuestas es el fortalecimiento del sector agropecuario, de gran potencial en todo el país, y que genera mayores tasas de empleo que el sector minero.

Y es que de acuerdo con un estudio desarrollado por Guillermo Rudas, experto en el tema, el aporte en la generación de empleo del sector de minería e hidrocarburos es escaso. **Entre 2001 y 2012 el sector agropecuario ha generado 223.000 nuevos empleos, la industria manufacturera 517.000 y la minera apenas 81.000”.**

En ese marco, las comunidades plantean aprovechar el potencial de más de 50 millones de hectáreas que tiene el país para actividades agropecuarias, razón por la que incluso la Organización de las Naciones Unidas para la Alimentación y la Agricultura, FAO ha indicado que **Colombia podría contribuir significativamente a la alimentación del mundo**, y por tanto podría encaminarse a tener una soberanía alimentaria.

Finalmente también se propone apoyar a los mineros artesanales, siempre y cuando desarrollen sus actividades de forma sostenible, sin ningún tipo de químicos que afecten la naturaleza.

<iframe id="audio_20028145" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20028145_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
