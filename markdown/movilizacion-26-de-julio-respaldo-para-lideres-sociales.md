Title: Movilización de este 26 de Julio, un respaldo para líderes sociales
Date: 2019-07-25 17:59
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: 26 de julio, Lideres, Movilización
Slug: movilizacion-26-de-julio-respaldo-para-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/lideres-sociales-marcha-el-grito.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

[La movilización de este 26 de Julio es una voz de protesta para la protección de la vida de las lideresas y los  líderes  sociales, que se convierte en un motivante para que ellas y ellos sigan ejerciendo su labor como defensores de sus territorios y comunidades.]

[ (Le puede interesar: [""El Grito" por la vida, resonará en más de 50 ciudades de todo el mundo"](https://archivo.contagioradio.com/el-grito-por-la-vida-resonara-en-mas-de-50-ciudades-de-todo-el-mundo/))]

[Según el Instituto de Estudios para el Desarrollo y la Paz (Indepaz), desde la firma del Acuerdo en 2016 hasta mayo de este año, las voces de 837 lideresas y líderes sociales han sido silenciadas,  236 desde el inicio del gobierno  Duque, elementos detonantes para la multitudinaria movilización.]

### **Razones para marchar este 26 de julio** 

[Según Luz Marina Cuchumbe, lideresa social de Inzá, Cauca,  la movilización se realiza por la protección de la  vida de los líderes sociales en Colombia, quienes son las voces de sus comunidades y son amenazados, vulnerados y asesinados.]

[A su vez Olga Quintero, lideresa de la Asociación Campesina del Catatumbo (ASCAMCAT), asegura que “en Colombia se necesitan garantías para la paz; además, se requiere proteger la vida de los defensores y defensoras de los derechos humanos”.]

[A este sentir, se une el líder social Yeison Mosquera, integrante de Tierra y Vida, quien indicó que es "una obligación que tiene Colombia de centrarse en la paz y olvidar la guerra". (Le puede interesar ["La MOE ha registrado 228 hechos de violencia contra líderes sociales en los últimos 8 meses"](https://archivo.contagioradio.com/moe-228-hechos-violencia-lideres/))]

[Ariel Palacios, integrante del Consejo de Paz Afrocolombiano, indica que tras "los acuerdos de paz, el país tiene la oportunidad de acercarse a mandatos internacionales en virtud de los derechos humanos". Palacios menciona que “muchos de los líderes asesinados pertenecen a étnias, y esta segmentación se relaciona con un modelo económico que está detrás de la extracción de las  riquezas de estas poblaciones”.]

### **¿Qué impacto tiene la movilización para lideresas y líderes sociales?** 

[Palacios, Quintero y Cuchumbe, concuerdan en que la marcha es un símbolo de respaldo para las defensoras y defensores de derechos humanos en el país. Palacios declara que “liderar causas colectivas es riesgoso y por ello, ver estos actos se convierte en una esperanza, y traduce que la sociedad ve con buenos ojos el trabajo de los líderes sociales”.]

[De acuerdo a Olga Quintero, la marcha es una manifestación para que se defienda el papel que cumplen como defensores de la vida, el territorio y los derechos fundamentales que son vulnerados a diario. Cuchumbe señala que “la movilización es muy importante para los líderes sociales porque somos voceros de comunidades muy lejanas, donde no hay protección para nosotros”. ]

[Mosquera explica que “la movilización del 26 de julio es un motivante para que los líderes sigan trabajando con las comunidades y puedan trabajar para sacar los territorios adelante”. A este grito se han sumado más de cincuenta ciudades en el mundo, donde se realizarán diversas actividades y también distintas organizaciones, partidos políticos y la sociedad en general.]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
