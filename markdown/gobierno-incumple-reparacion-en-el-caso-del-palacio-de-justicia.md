Title: Gobierno incumple reparación en el caso del Palacio de Justicia
Date: 2016-12-09 17:13
Category: DDHH, Nacional
Tags: Caso del palacio de Justicia, Comisión Interamericana de Derechos Humanos, Reparación a las víctimas, Víctimas del Palacio de justicia
Slug: gobierno-incumple-reparacion-en-el-caso-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/palacio-de-justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [9 Dic 2016] 

Familiares de los desaparecidos del Palacio de Justicia acudieron a la Comisión Interamericana de Derechos Humanos para **denunciar irregularidades por parte del Estado colombiano en la implementación de las medidas de reparación**, los familiares recibirían un monto de 2 a 3 salarios mínimos y algunos hasta quedarían debiendo.

Francisco Lanao esposo de Gloria Anzola, explicó que Luis Carlos Villegas, está haciendo una mala interpretación de lo dispuesto en el fallo de la CIDH, el cual en el artículo 603, dice textualmente que **“se estima pertinente fijar, en equidad, la cantidad de US\$100 mil, US\$40 mil a favor de los hermanos de las víctimas y US\$80 mil a favor de las madres, padres, hijos, cónyuges y compañeros permanentes”.**

Lanao comenta que “el problema radica en que se está interpretando ese párrafo como si el monto de dinero se tuviera que repartir entre todos y no uno para esposa, hijo, mamá o hermano de cada familia (…) **esto es pasar por encima de la dignidad de las víctimas y revivir el sufrimiento de todas en este caso”.**

El esposo de Gloria Anzola y otros familiares de los once desaparecidos, aseguran que la posición actual del Ministerio de Defensa “devuelve la sensación de la negligencia, falta de voluntad y consideración del Gobierno, **deja la sensación de un irrespeto total a la justicia colombiana y de interpretar, como si fuera burla, la sentencia de la Corte IDH”.**

Por último, Lanao señala que **el Estado colombiano tiene plazo hasta el próximo 10 de Diciembre para hacer efectivas las medidas de reparación,** de igual modo la CIDH dijo a las víctimas que daría una respuesta aclaratoria antes de esa fecha, los familiares aún están a la espera de poder resolver dicha situación.

[Nota 265 de la Corte Interamericana de Derechos humanos](https://www.scribd.com/document/333766239/Nota-265-de-la-Corte-Interamericana-de-Derechos-humanos#from_embed "View Nota 265 de la Corte Interamericana de Derechos humanos on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_49238" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/333766239/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-hNb9LGe9Vd6Mos4OPQTA&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
<iframe id="audio_14809929" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14809929_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
