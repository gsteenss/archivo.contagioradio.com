Title: Comunidades CONPAZ evalúan positivamente la propuesta de las “Terrepaz”
Date: 2015-11-27 12:11
Category: Nacional, Paz
Tags: Cese bilateral al fuego, Conpaz, dialogos de paz, Marco León Calarca, Maria Eugenia Mosquera, Zonas Terrepaz
Slug: comunidades-conpaz-evaluan-positivamente-la-propuesta-de-las-terrepaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### 

<iframe src="http://www.ivoox.com/player_ek_9528756_2_1.html?data=mpqfmpyZeo6ZmKiakpmJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIqdSfpLS7sqa%2Bb8bqwtGSpZiJhqLVz5Dd0djNuMrqwtLS0NnJb83VjNXf0dXZqdTowpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Maria Eugenia Mosquera, CONPAZ] 

##### 27 Nov 2015

Marco León Calarca, integrante de la delegación de paz de las FARC, develó la propuesta de esa guerrilla para el punto del fin del conflicto en las conversaciones de paz. Se trata de la conformación de zonas especiales, delimitadas y conservando los principios culturales, políticos y económicos en que ha operado esa guerrilla en territorios específicos, para concentrar a sus integrantes y facilitar tanto la reparación de las víctimas como la dejación de las armas y el cese bilateral definitivo.

Las Comunidades Construyendo Paz, CONPAZ, manifestaron que están de acuerdo con la propuesta puesto que las personas que están en las filas de las guerrillas tienen una relación histórica con los territorios en razón de ser algunos familiares de campesinos, y haber operado en esas regiones.

Maria Eugenia Mosquera, integrante de CONPAZ, afirma que desde hace ya 15 años las comunidades vienen desarrollando propuestas que permitan viabilizar la construcción de la paz, y la posibilidad de que integrantes de las FARC integren las “Terrepaz” en las mismas regiones sería una muestra de que realmente las propias comunidades puedan ejercer soberanía sobre sus territorios.
