Title: Solo se ha cumplido el 18% de los Acuerdos de La Habana
Date: 2017-10-02 12:45
Category: Nacional, Paz
Tags: acuerdos de paz, Incumplimientos
Slug: solo-se-ha-cumplido-el-18-de-los-acuerdos-de-la-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/44df7097-6c8f-4c6d-a6e6-49999afd59ef-e1489618959388.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOVICE] 

###### [02 Oct 2017] 

De acuerdo con el más reciente informe del Observatorio de seguimiento a la Implementación de los Acuerdos de Paz, titulado “¿Es posible una paz estable y duradera sin cumplir los Acuerdos de Paz?” **solo se ha cumplido el 18% de lo pactado en este primer año, lo que significaría que el cumplimiento total de los acuerdos se daría en un plazo de 5** años. De igual forma el documento revela que la mayoría de los incumplimientos son responsabilidad del Gobierno en sus diferentes instituciones como el Congreso o la gestión orgánica.

### **El avance de la implementación de los Acuerdos de Paz** 

De acuerdo con el documento, hasta el momento hay un mayor avance en el punto de reincorporación política y socio económica con un 33,8%, luego víctimas e implementación de la JEP con un 27,6%, el desmonte del paramilitarismo con 24,4%, el punto de étnicas con **23%, el sistema Integral de Seguridad con un 22,7% y la participación política con un 19,3%.**

Entre los puntos en los que hay menos o ningún avance se encuentran las garantías al respeto al DD. HH con un 10%, la reforma Rural Integral y PNIS con un 6,9%, la prevención del consumo de drogas ilícitas con **un 3,8% y la comercialización de cultivos con un 2%. **(Le puede interesar:["La JEP necesita a la ciudadanía en movilización"](https://archivo.contagioradio.com/la-jep-necesita-a-la-ciudadania-en-movilizacion-para-defenderla/))

### **La crítica situación de los Acuerdos en el Congreso** 

El informe también evidencia las dificultades que ha tenido que afrontar el Acuerdo de Paz en el Congreso, de acuerdo con el texto solo hay 4 actos legislativos aprobados, una ley orgánica, una ley estatutaria y una ley ordinaria. Lo que expone que en total haría falta por presentar 11 leyes ordinarias, **18 decretos, 3 leyes estatutarias, 2 leyes orgánicas y 2 actos legislativos.**

Además, el informe señala que desde hace 7 meses fueron radicados en el Congreso de la República los proyectos de ley referentes al Sistema Nacional de Innovación Agropecuaria y la Adecuación de tierras, sin embargo, aún no pasan por los debates en plenaria de Cámara y Senado, **sorprendiendo que aun siendo tramitadas vía Fast Track, su paso por el Congreso es de “plena lentitud**”.

De igual forma el documento manifiesta preocupación sobre el estancamiento de la Reforma Política y del proyecto de ley de las Circunscripciones Especiales de Paz, esta última habría sufrido serias modificaciones que irían en contra de lo pactado en **La Habana y del espíritu del Acuerdo**. (Le puede interesar:["Es urgente aprobar las circunscripciones de paz"](https://archivo.contagioradio.com/comunidades-exigen-aprobacion-pronta-de-las-circunscripciones-especiales-de-paz/))

### **Los incumplimientos a las FARC** 

El informe “¿Es posible una paz estable y duradera sin cumplir los Acuerdos de Paz?”, igualmente recalca que de las 26 zonas veredales, **solo 7 fueron finalizadas y en 19 las obras quedaron inconclusas**. De otro lado, referente al proceso de las amnistías se habría avanzado en un 67% en donde 1.270 excombatientes siguen privados de la libertad.

Sobre las garantías de reincorporación al 30 de septiembre 9.843 integrantes de las FARC están vinculados al sistema de salud, mientras que 3.550 están inscritos a programas académicos, **3.850 fueron afiliados al sistema de pensiones y 10.172 habrían recibido los recursos de la asignación única de normalización de dos millones de pesos.**

**Las víctimas y los Acuerdos de Paz**

“Los anuncios reiterados durante el proceso de paz según los cuales las víctimas constituyen el centro del Acuerdo de Paz, no tiene correspondencia con la implementación del punto cinco”, así lo señala el informe que agrega que tampoco se ha avanzado en la Jurisdicción Especial de Paz que permite la creación del Tribunal de Paz, y que pese a que ya están escogidos los 53 magistrados las acciones de partidos como **Cambio Radical y el Centro Democrático dejan en incertidumbre la aprobación de la misma.**

De igual forma, el documento explica que durante la implementación de los acuerdos se ha mantenido un marco de violencia y amenazas contra líderes sociales y defensores de derechos humanos **“que ponen en entredicho las garantías de participación de las comunidades”**. (Le puede interesar: ["Organizaciones de víctimas y DD.HH califican como positiva selección de magistrados en JEP"](https://archivo.contagioradio.com/47138/))

[OIAP Boletin 004 (1)](https://www.scribd.com/document/360494546/OIAP-Boletin-004-1#from_embed "View OIAP Boletin 004 (1) on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe class="scribd_iframe_embed" title="OIAP Boletin 004 (1)" src="https://www.scribd.com/embeds/360494546/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-8q5mzHH5lKhbvtqcKSIc&amp;show_recommendations=true" data-auto-height="false" data-aspect-ratio="0.7729220222793488" scrolling="no" id="doc_7895" width="100%" height="600" frameborder="0"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
