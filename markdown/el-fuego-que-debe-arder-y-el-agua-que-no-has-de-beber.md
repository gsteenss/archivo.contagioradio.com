Title: El fuego que debe arder y el agua que no has de beber
Date: 2016-02-04 10:05
Category: Camilo Alvarez, Opinion
Tags: 4g, Bogotá, incendios, La Guajira
Slug: el-fuego-que-debe-arder-y-el-agua-que-no-has-de-beber
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/incendios_forestales_colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

#### **[Camilo Álvarez Benítez](https://archivo.contagioradio.com/camilo-alvarez-benitez/)- [@camiloalvarezb](https://twitter.com/CamiloAlvarezB)** 

###### 4 Feb 2016 

A cualquiera con dos centímetros de frente le cabria en la cabeza que los resonados problemas del cambio climático no son debates meramente conceptuales; que se acaba el agua potable, que se deshielan los polos, que el hueco en la capa de ozono, que el efecto invernadero y la emisión de gases y que salvemos las ballenas; que nada de esto se resuelve cargando manillas de colores o pintándose los labios de color morado.  Pero, las mentes de quienes nos llevan donde nos llevan tienen frentes 4G y manejan directo por la ruta del sol, disponen el agua potable para extraer carbón, para lavar oro y para venderlas en botellas de plástico que luego contaminan aún más los afluentes de agua.

[Las imágenes son desgarradoras, el modelo 4G produce grandes desiertos, pueblos como Taganga deben comprar el agua en carro tanque y ni que decir de la guajira donde mueren niños y niñas a diario sin que la gente de bien se inmute. El ya clásico documental FLOW (For Love Of Water) nos muestra que el problema es de rango planetario, pero sobre todo que es responsabilidad de un mismo tipo de gente que también es planetaria o al menos se mueve por todo el planeta comprando y comerciando un bien de toda la humanidad.]

[De este lado del túnel, el reclamo más fuerte siempre será de la madre tierra y frente a ella cuando reacciona nos demuestra lo limitado de nuestra existencia; así, en lo que termina derivando el mal trato del modelo extractivista es en lo que los medios cómodamente se aferran en llamar Tragedias o emergencias, basta googlear la palabra para darse cuenta las dimensiones del asunto; La población actúa siempre de acuerdo al tipo de afectación que tiene, cadenas humanas y solidaridad efectiva casi siempre es la respuesta, desde el valle del Misisipi y el huracán Katrina, el tsunami en el sudeste asiático, los huracanes en Haití o los terremotos en Chile.  ]

[Por estas y otras muy buenas razones Bogotá en su último gobierno recogió los saberes de pobladores y ambientalistas en el llamado de ordenar el territorio a través del Agua, pero el agua no es solo hacer tanques y represas es sobre todo sembrar agua, recuperar los bosques, los humedales, las cuencas, quebradas y ríos; planear el uso consciente frente a las necesidades reales del consumo de quienes habitamos la ciudad y por lo tanto generar cambios culturales que ojala en la mayoría de los casos no llevaran a bañarnos de a dos.]

[Los incendios en los cerros de este comienzo de año son una nueva señal del reclamo de la madre tierra, sobre todo en el sentido de mantener el rumbo que se empezó a implementar en la Bogotá Humana, porque lo fundamental no es hallar un culpable de quien inicio el fuego o llevar mercados y frazadas, sino tener claridad de las políticas y los políticos que permiten las condiciones para que los ecosistemas sean más frágiles y  la responsabilidad política de la institucionalidad que debe cuidar, preservar y responder de manera efectiva en la gestión de riesgos y la mitigación del daño.]

[No podemos decir que Peñalosa inició los incendios, pero si podemos afirmar que con la idea de desmantelar la propuesta de la reserva Van der Hammen, la idea de construir aún más sobre los cerros, el desmantelamiento del mínimo vital, la idea de volver al retardatario sistema de manejo de residuos sólidos todo en aras del lucro está en contravía de los llamados del planeta.]

[Al final y al principio son los pobladores y pobladoras  quienes habitando su territorio lo pueden cuidar y proteger y es con ellos con quien se debe hacer, así como las cadenas humanas de estos días mitigando el fuego. Esos pobladores organizados deberían ser la única llama existente y el cuidado del territorio nuestra única sed; Ese es el sentido y el llamado de estos tiempos, lo demás como las fotos de estos días es una cortina de humo.]
