Title: Diecisiete años de memoria y Resistencia: Páramo de La Sarna
Date: 2018-11-28 12:20
Author: AdminContagio
Category: Sin Olvido
Tags: masacre, paramilitares, paramo de la sarna
Slug: diecisiete-anos-memoria-paramo-la-sarna
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/la-sarna-e1543424210551.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Movice 

###### 29 Nov 2018 

Con una peregrinación y bajo la consigna **"Sin memoria no hay verdad, sin verdad no hay justicia y sin justicia no hay paz"**, este domingo 2 de diciembre se conmemorarán **diecisiete años de la Masacre del Páramo de La Sarna**, cometida por paramilitares, quienes con complicidad estatal, asesinaron a quince personas.

El evento rememora los hechos ocurridos el **1 de diciembre del 2001,** cuando a las 6 de la mañana por la vía que de Sogamoso conduce  a Labrazagrande, fue detenido un autobús de la empresa Cootracero, en el lugar conocido como el Páramo de La Sarna. Allí, **paramilitares de las Autodefensas Campesinas de Casanare bajaron todos los ocupantes y ejecutaron a 15 de ellos, acusándoles de ser miembros de la guerrilla**. De este episodio sobrevivieron tres personas, dos menores de 11 y 8 años y una mujer de 60.

La complicidad del Estado quedo establecida cuando paramilitares, en los tribunales de Justicia y Paz, declararon que **el vehículo fue detenido en busca de un integrante del sindicato de maestros**, acusado de ser miembro del ELN y que, para perpetrar la masacre, **el retén militar que habitualmente operaba a unos kilómetros del sitio donde ocurrieron los hechos, fue retirado**.

A partir de lo ocurrido, los familiares, la comunidad y organizaciones sociales han mantenido viva la memoria de las víctimas, a pesar del dolor y la estigmatización que sufrieron. **Desde hace once años se realizan peregrinaciones para exigir al Estado la verdad, la justicia y la reparación integral **con garantías de no repetición. La peregrinación se convirtió en un símbolo de memoria como una construcción colectiva que ha logrado visibilizar los hechos y reivindicar sus derechos.

Este 2 de diciembre, familiares, amigos y la comunidad en general, están convocados a acompañár la conmemoración que mantendrá viva la memoria de: **Luis Miguel Melo, Hernando Gómez, Herminda Blanco, José Noa Rosas, Jairo Peña, José Monguí, Luis Alejandro Pérez, Abel Rodríguez, Isidro Guío, Tania Correa, Luis Arturo Cárdenas, Mercedes Rivera, Luis Ángel Gil y John Poveda**.

La jornada empezará en la plaza de la Villa con la salida de buses desde las **7.30 de la mañana hacia la Iglesia el Rosario de la Ciudad de Sogamoso**, continuara a las 9.00 am con una caminata que se ha realizado desde el 2007 hacia el sector de La Cabaña en el Páramo de la Sarna, sitio de los hechos. Luego, a las 10.00 am se iniciará la celebración eucarística, a las 11.00 am se realizará un acto de memoria y a las 12.00 pm, finalmente, se hará cierre de la decimoprimera peregrinación.
