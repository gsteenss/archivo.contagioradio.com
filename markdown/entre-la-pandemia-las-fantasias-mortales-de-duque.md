Title: Entre la pandemia las fantasías mortales de Duque
Date: 2020-06-22 10:28
Author: Camilo de las Casas
Category: Camilo, Opinion
Tags: Covid-19, Duque, pandemia, Presidente Ivan Duque
Slug: entre-la-pandemia-las-fantasias-mortales-de-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Diseño-sin-título-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Diseño-sin-título-2.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Sub Duque se la jugó al mejor estilo del populismo está semana para intentar paliar su ausencia de legitimidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

 La bancada uribista que incluye al conservantismo, sectores de Cambio Radical, Colombia Libres aprobaron  la cadena perpetua para quienes asesinen y abusen a niñas y niños. La votación de los senadores fue aplaudida por Duque, en su emisión diaria, fastidiosa y plana sobre el COVID.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Esa misma emisión televisiva preconizo el cumplimiento de su otra bandera de campaña, el día sin impuesto de valor agregado, IVA. Invitando con al consumo ciudadano de bienes en las grandes superficies en medio de la cuarentena.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Y como era de esperarse en un país con más del 90 % de impunidad y ante la obvia indignación que genera a cualquier ciudadano la violación de los derechos de los niños los aplausos de la opinión masiva no se hicieron esperar. Así mismo como borregos lógicos, y recordando la imagen de Tiempos Modernos, mujeres y hombres en masas haciendo filas, aglutinados, atropellándose unos a otros en centros comerciales para comprar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ambas escenas muestran la tragedia de la ausencia del poder conciente o de ciudadanos sometidos al espejismo carcelario y del consumo. Un sector de la sociedad usado irreflexivamente, en la que se potencia su instinto de supervivencia, en que la inteligencia emocional está insanamente adobada con eslogan que ocultan la contradicción y los caminos de libertad y curativos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ese sector parlamentario hoy uribista que se identifica en la construcción de Estado de opinión legisla haciendo creer que más penas, o la cadena perpetua carcelaria, evitan la comisión de un delito. Olvidaron intencionalmente que una condena de 60 años como la que hoy existe por delitos contra las niñas y los niños es de facto una cadena perpetua. Olvidan también intencionalmente que los centros carcelarios hoy son de hacinamiento, y reflejo de una crisis de la sociedad en donde ronda la corrupción, y la diferencia de clase, la discriminación y la negación de derechos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La decisión de aprobación de un proyecto inútil por lo ineficaz y que oculta las raíces profundas de una enfermedad personal, a veces familiar y de la sociedad que estimula determinadas conductas en los individuos contra los niños, ni se previene ni se ataca con normas inocuas.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ese mismo Estado de opinión como salida a la crisis de legitimidad del gobierno se usó en el llamado por la prensa internacional el FridayCOVID. La medición en un día de la capacidad de consumo, y, acá con el sello uribista, de profundización de la contaminación. La publicidad del consumo como ilusión de borrar la rampante desigualdad social.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la vista quedó un amplio sector empresarial y comercial que opera como una horda de vampiros que se beneficiaron en la que felizmente estuvieron sometidos ciudadanos que cuentan para votar o para consumir, es casi lo mismo. El grupúsculo de mucho poder se burló de los protocolos de protección ante la pandemia y especularon días antes. Algunos elevaron los precios días antes de la rebaja al IVA. Una rebaja inane que irresponsablemente motivó a valorar la vida por lo que se tiene y por lo que se endeuda en tiempos de una crisis económica, social y ambiental, que es real y sin precedentes.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El Estado de opinión es el irrespeto a una resquebrajada Constitución y a la condición de exclusión y desigualdad de millones de ciudadanos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

SubDuque ofrece como mercader fantasías. Para que las mismas se hagan para él creíble y  desmanteló una comisión de política criminal que cuestionaba el populismo punitivo que contiene la cadena perpetua.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Animado por el sector empresarial que lo eligió en medio de la pandemia compensándolos con beneficios a través de los decretos por emergencia social COVID19, y, ahora por el sector comercial en medio de la crisis del coronavirus, priorizó esa economía de mercado por la economía de la Vida

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Y seguirá el populismo del Estado de Opinión.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Si la Corte Constitucional en la revisión de lo aprobado en el Congreso se cae dirán: “reforma a la justicia. Hay que acabar con las cortes”. Y si los críticos de la economía de mercado se pronuncian, dirán: “ahí están, imposibilitando que el pueblo tenga bienes para mejorar su calidad de vida”, “Castro chavistas”. Estado de opinión es fantasía, arraigo de impunidad, destrucción de las libertades y de la Vida

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Otras columnas de Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)

<!-- /wp:paragraph -->
