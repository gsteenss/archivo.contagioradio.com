Title: Caravana Humanitaria del Cañón del Micay: “un canto por la vida y la paz del territorio”
Date: 2020-10-29 13:18
Author: AdminContagio
Category: DDHH, Nacional
Tags: Cañón del Micay, Caravana Humanitaria del Cañón del Micay, Cauca
Slug: caravana-humanitaria-del-canon-del-micay-un-canto-por-la-vida-y-la-paz-del-territorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/canon-del-micay.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @CNA\_Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A partir de mañana 29 de octubre y hasta el 2 de noviembre, se llevará a cabo la Caravana Humanitaria del Cañón del Micay denominada **“un canto por la vida y la paz del territorio” en la que los participantes se expresarán en contra de la violencia en el departamento del Cauca.** (Le puede interesar: [Caravana Humanitaria acompañará a comunidad del Cañón del Micay](https://archivo.contagioradio.com/caravana-humanitaria-acompanara-a-comunidad-del-canon-del-micay/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esta movilización, organizaciones, entre ellas la [Red por la Vida y los Derechos Humanos del Cauca ](https://twitter.com/RedVidaDHCauca)y el [Coordinador Nacional Agrario –CNA-,](https://twitter.com/CNA_Colombia) recorrerán los municipios de Argelia y El Tambo para acompañar a las comunidades campesinas, afrocolombianas e indígenas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CNA_Colombia/status/1321217431570927616","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CNA\_Colombia/status/1321217431570927616

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por medio de una alerta temprana, la caravana expresó que decenas de organizaciones de derechos humanos, líderes y lideresas adelantaran la Primera Caravana Humanitaria **«para evidenciar la realidad del territorio, y, visibilizar y acompañar a la población que sufre un incremento de la violencia socio política y de las prácticas genocidas del Estado».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, las organizaciones que convocan esta caravana señalan que hay gran preocupación por la deficiente respuesta de las instituciones y de los organismos multilaterales a solidarizarse con la **«**crítica situación que afrontan las comunidades del Cañón del Micay**»** por lo que alertaron a entidades nacionales y locales a garantizar la seguridad, proteger la vida y la integridad de las comunidades de todo el departamento del Cauca, y a los y las integrantes de la Caravana Humanitaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este mismo día, a las 10 de la mañana, se realizará una rueda de prensa en la ciudad de Popayán, en la cual se anunciarán los diferentes actos políticos, culturales y artísticos que se llevarán a cabo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CNA_Colombia/status/1321217355805036547","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CNA\_Colombia/status/1321217355805036547

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading -->

Razones que incentivan la Caravana Humanitaria
----------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el departamento del Cauca se ha declarado una crisis humanitaria pues, a pesar de la alta militarización, continúan las masacres y desapariciones, así como las disputas entre actores armados por los corredores del narcotráfico; el paramilitarismo y las grandes empresas han causado el desplazamiento de muchas familias y las cifras de asesinatos a líderes y lideresas sociales, defensores de derechos humanos y excombatientes siguen aumentando.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Instituto de Estudios para el Desarrollo y la Paz, (Indepaz), 83 han sido los líderes asesinados en el departamento. El último hecho se presentó el 26 de Octubre con el asesinato del líder social e integrante del CNA, Carlos Navia, **en el corregimiento de El Plateado entre los municipios de Argelia y El Tambo en el departamento del Cauca.** (Le puede interesar: [Carlos Navia líder social de Asocomunal en Cauca, es asesinado](https://archivo.contagioradio.com/carlos-navia-lider-social-de-asocomunal-en-cauca-es-asesinado/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
