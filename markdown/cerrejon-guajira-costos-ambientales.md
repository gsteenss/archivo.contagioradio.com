Title: Costos ambientales de acción de Cerrejón en la Guajira siguen en aumento
Date: 2017-08-29 15:52
Category: Ambiente, Nacional
Tags: Cerrejón, Comunidades Wayúu
Slug: cerrejon-guajira-costos-ambientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Arroyo-Bruno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunidad Guajira] 

###### [29 Ago 2017] 

Las comunidades que habitan en los municipios afectados por el Cerrejón, están haciendo un llamado urgente para que se frene la expansión minera en el territorio y los daños tanto culturales como ambientales que se han provocado en el territorio, por este motivo el próximo 31 de agosto estarán en Bogotá para exigir que se suspenda el desvío del arroyo Bruno y **pedirle a la Corte Constitucional que ordene la recuperación del cauce natural del río.**

El proyecto de expansión del Cerrejón pasaría de explotar 32 millones toneladas de carbón a sacar 40 millones y pasaría de consumir **142 litros de agua a 307 litros, por segundo, comparado al consumo de un habitante de la Guajira que está en promedio de 0,7 litros** diarios, de agua sin tratar. (Le puede interesar: ["El Cerrejón buscaría desalojar a 27 familias Wayuú para desviar el Arroyo Bruno"](https://archivo.contagioradio.com/cerrejon-buscaria-desalojar-27-familias-wayuu-desviar-arroyo-bruno/))

Además, las comunidades han manifestado un incremento en las enfermedades respiratorias que están directamente relacionadas con la explotación del carbón y clanes Wayuú son reiterativos en asegurar que desde que el Cerrejón realiza actividades de explotación, ya no pueden moverse libremente a lo largo de su territorio, **se ha venido restringiendo su acceso al agua y 35 comunidades han sido despojadas de sus territorios**.

### **El arroyo Bruno y bosque seco tropical en riesgo de desaparecer** 

El pasado 14 de este año la Corte Constitucional, a través de un fallo, le ordeno a la multinacional Cerrejón frenar las obras que adelantaba en el arroyo Bruno durante 3 meses, sin embargo, los habitantes denunciaron que el arroyo **ya fue desviado en un tramo de 3.6 km, a pesar de ser uno de los afluentes más importantes del río La Ranchería.**

De igual forma, el arroyo se encuentra ubicado en zona de bosque seco tropical, en peligro de desaparecer, y en toda la actividad de la empresa, en 30 años, se han perdido 12 mil hectáreas de fauna y vegetación que tomará más de 60 años recuperar hasta un 73%. (Le puede interesar:["Suspendidas las operaciones del Cerrejón para desviar el arroyo Bruno, en la Guajira"](https://archivo.contagioradio.com/suspendidas_actividades_arroyo_bruno_la_guajira_corte_constitucional/))

Diferentes líderes sociales e indígenas esperan que, con la actividad del 31 de agosto, se logre llamar la atención de **más sectores de la sociedad para frenar la explotación de carbón del Cerrejón** y con los múltiples daños al ambiente y las comunidades.

<iframe id="audio_20592556" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20592556_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
