Title: Colombia continúa con el segundo lugar por índices de desplazamiento
Date: 2015-06-18 12:18
Category: El mundo, Otra Mirada
Tags: colombia, conflicto armado, Desplazamiento forzado, Informe ACNUR, megaproyectos, política minero energética, Siria, violaciones a los derechos humanos
Slug: colombia-continua-con-el-segundo-lugar-por-mayores-indices-de-desplazamiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Desplazados1-e1498006618645.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.reporterosasociados.com.co]

###### [18 de junio 2015]

Este jueves, se publicó un nuevo informe de la Agencia de la ONU para los refugiados, ACNUR, sobre la situación de desplazamiento forzado en el mundo. De acuerdo a este,  el 2014 fue el año en el que se alcanzó el **más alto nivel de población desplazada con 59,5 millones de víctimas.**

El informe titulado, “Tendencia globales”, demuestra que según cifras, **Siria tiene el primer lugar con 7,6 millones** de personas, **Colombia** ocupa el **según puesto con 6 millones** de desplazados, **Irak, República Democrática del Congo, y Sudán,** ocupan los siguientes lugares, de la lista de los países con mayor desplazamiento masivo del mundo.

De acuerdo a la ACNUR, “La guerra y la persecución”, serían las causas de los desplazamientos masivos, que no solo se da por actores armados y la problemática del conflicto, sino también por persecuciones hacia las comunidades como consecuencia de la política minero energética del gobierno, como lo han denunciado las comunidades en Colombia, porque se promueve los **megaproyectos de empresas nacionales y extranjeras,** donde las multinacionales desalojan a los pobladores de sus regiones, como por ejemplo, **el proyecto Hidroituango que ya deja más de 400 familias desplazadas**, que han sido desalojadas por los mismos funcionario de Empresa Públicas de Medellín.

Una de las cifras más alarmantes que presenta el informe, es que **más de la mitad de la población de refugiados, son niños y niñas.** Además se registra que en 2014, unos 13,9 millones de personas se convirtieron en nuevos desplazados, es decir, cuatro veces más que en 2010.

Por otra parte, también se registró que durante 2014, en todo el mundo hubo **19,5 millones de refugiados, 38,2 millones de desplazados internos y 1,8 millones de personas** a la espera de la resolución de sus solicitudes de asilo.

Finalmente, cabe resaltar que desde 2011, se presentó la mayor aceleración en el número de personas refugiadas tras la guerra en Siria, que ha generado el mayor desplazamiento de población en todo el mundo.

Según cifras, el año pasado, cada día una media de 42.500 personas se convirtieron en refugiadas, solicitantes de asilo o desplazadas internas, lo que significa que esa cifra se ha cuadruplicado en los últimos cuatro años. Así mismo, a nivel global, **una de cada 122 personas es refugiada, desplazada interna o solicitante de asilo**. Eso quiere decir, que con ese número de personas se podría conformar un país como Italia o el Reino Unido.

[Tendencias Globales 2014](https://www.scribd.com/doc/269053870/Tendencias-Globales-2014 "View Tendencias Globales 2014 on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_21418" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/269053870/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-PQPLF4luWD5hRDqqKDSU&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>
