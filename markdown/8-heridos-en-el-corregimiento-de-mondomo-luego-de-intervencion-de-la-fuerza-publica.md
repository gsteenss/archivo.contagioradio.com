Title: 8 heridos en el corregimiento de Mondomo luego de intervención de la Fuerza Pública
Date: 2020-07-28 11:03
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: campesino heridos, Cauca, cultivos de uso ilicito, Erradicación Forzada, Mondomo
Slug: 8-heridos-en-el-corregimiento-de-mondomo-luego-de-intervencion-de-la-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/agresiones-ejercito-cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 27 de julio en horas de la mañana se registró **un nuevo caso de abuso de fuerza por parte de integrantes del[Ejército Nacional](https://archivo.contagioradio.com/amenazas-a-artistas-que-cuestionan-con-musica-el-honor-de-las-ffmm/)**, la acción se evidenció en el **corregimiento de Mondom**o, zona rural del municipio de Santander de Quilichao, Cauca.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El hecho quedó registrado en un vídeo grabado por campesinos y campesinas de la zona que se se oponían a las acciones de [erradicación forzada](https://www.justiciaypazcolombia.com/colombia-decision-de-erradicar-forzosamente-cultivos-ilicitos-podria-generar-violaciones-de-derechos-humanos/) de cultivos de uso ilícito, y **en el que se evidencia cómo los uniformados realizan varios disparos al aire con sus armas de dotación.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/posts/10157368550695812","type":"rich","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-rich is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/posts/10157368550695812

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

La acción fue acompañada con insultos y cruce de palabras entre los integrantes del Ejército y algunos campesinos en el momento que los militares eran expulsados por la comunidad, en lo que califican los pobladores como **una acción en defensa del territorio, y ante los incumplimiento de los acuerdos entorno a la erradicación** en Mondomo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a esto, algunas horas después, la comunidad volvió a denunciar nuevos altercados entre la Fuerza Pública, y los habitantes del **corregimiento de Mondom**o. Esta vez con el Escuadrón Móvil Antidisturbios (Esmad), y nuevos miembros del Ejército.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Situación que según la comunidad causó lesiones a por lo menos ocho **personas, cuatro de estas fueron llevadas al Hospital Francisco de Paula Santander** debido a la gravedad de sus heridas. Uno de estos se presume que fue impactado con arma de fuego en su cuerpo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/posts/10157367422400812","type":"rich","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-rich is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/posts/10157367422400812

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte la Tercera División del Ejército señaló por medio de un comunicado, que sus tropas trabajan solo en las veredas de El Turco, Tres Quebradas, Las Vueltas, Agua Blanca y Nueva Colombia desde el primero de julio del 2020; además aclararon que ese tipo de intervenciones no se hacen en coordinación con la Alcaldía Municipal.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde que el Gobierno dio inicio a las acciones de erradicación forzada en los territorios la violencia que incluye agresiones, hostigamientos e incluso asesinatos de miembros de la comunidad rural ha aumentado, situación que se dio en aumento **desde el inicio del aislamiento preventivo obligatorio, con por lo menos 15 eventos de erradicación forzada.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los departamentos con mayores casos son; Antioquia, Caquetá, Córdoba, Nariño, Norte de Santander, Putumayo, Valle del Cauca. Y ha encendido las alarma de la comunidad internacional, una de ellas de la **Amnistía Internacional (AI), quien indicó que este tipo de operativos son “una sentencia de muerte para las comunidades".**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
