Title: Asesinan a Luz Jenny Montaño, lideresa comunitaria en Tumaco
Date: 2017-11-15 11:39
Category: DDHH, Nacional
Tags: asesinatos de líderes sociales, Tumaco
Slug: asesinan-a-luz-jenny-montano-lideresa-comunidaria-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/luz_assinada_tumaco-e1510763805820.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Generación Paz] 

###### [14 Nov 2017] 

Luz Jenny Montaño, de 48 años de edad, es la nueva víctima de la situación que se vive actualmente en Tumaco. En la tarde del pasado domingo 12 de noviembre, desconocidos le **dispararon en su vivienda, ubicada en el barrio Viento Libre en zona urbana del municipio.**

Montaño, hacía parte de la junta de acción comunal de su sector e integraba las asociaciones religiosas del Divino Niño y Jesús Nazareno, desde donde se estaba exigiendo la protección a los líderes sociales del país, sin embargo, a menos de un mes del asesinato de José Jair, se da el asesinato de esta **lideresa conocida por su espíritu religioso y entusiasmo en el quehacer comunitario. **

En ese marco de asesinatos sistemáticos en las regiones del país, las comunidades y organizaciones sociales han denunciado los señalamientos por parte de **Rodrigo Lara, Presidente de la Cámara de Representante, quien dijo que las Juntas de Acción Comunal son la Red de la FARC EP.** También aseguran que **José Felix Lafaurie**, quien afirmó en medios de comunicación que los Comunales del Caquetá tienen nexos con la FARC, poniendo ne riesgo la vida de quienes hacen parte de dichos consejos comunitarios.

Ante el hecho, las organizaciones sociales de Tumaco exigen al Gobierno Nacional y a la Comunidad Internacional acciones inmediatas para que se nos proteja la vida y que tanto los entes de investigación como los analistas de riesgos no minimicen las amenazas con auto amenazas.

###### Reciba toda la información de Contagio Radio en [[su correo]
