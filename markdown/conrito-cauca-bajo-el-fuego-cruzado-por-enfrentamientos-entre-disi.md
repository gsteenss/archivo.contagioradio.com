Title: Corinto bajo el fuego cruzado de EPL y disidentes de las FARC
Date: 2019-01-28 14:08
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: Cauca, comunidades indígenas, Corinto, desplazamientos forzados
Slug: conrito-cauca-bajo-el-fuego-cruzado-por-enfrentamientos-entre-disi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/corinto-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Kien y Ke] 

###### [28 Ene 2019] 

Las comunidades indígenas en el municipio de Corinto, Cauca, **denunciaron el secuestro de la vicepresidente de la Junta de Acción Comunal de la vereda El Crucero, María Evelin Chate Yunda, desplazamientos y desaparición forzada** producto de combates entre disidencias de las FARC y guerrilla del EPL, en los que han resultado dos civiles muertos.

Las autoridades locales de ese municipio confirmaron el secuestro de Chate la noche del pasado domingo 27 de enero, cuando al parecer disidentes de las FARC llegaron hasta la población, preguntando por el presidente de la Junta de la Acción Comunal y al no encontrarlo, la raptaron a ella.

Gonzálo Guetio, dinamizador de la Guardia Indígena, asegura que el mismo 27 de enero en horas de la noche, **hombres intentaron sacar de su vivienda a un miembro de la comunidad**,  acción que se evitó gracias a la rápida acción del resto de habitantes. Además expresó que desde el pasado 25 de enero en horas de la noche, iniciaron las confrontaciones que duraron más de 24 horas.

Para Guetio, el aumento de las confrontaciones y de los desplazamientos **tiene que ver con la** **disputa territorial entre los grupos armados para controlar esta zona**, "están afectado a nuestros niños, **no dejan transitar libremente como lo estábamos haciendo**", afirmó.

**El accionar de la Fuerza Pública**

Frente a la presencia de la Fuerza Pública en el territorio, el integrante de la Guardia Indígena aseguró que **en Corinto, hay un grupo de uniformados en la parte alta del municipio, en donde también se encontrarían los grupos armados, pero "no hacen nada**".

"Uno no entiende la Fuerza Pública para qué esta ahí, porque **están como a unos 10 minutos de en donde están combatiendo los otros y no han hecho nada**" aseveró Guetio. (Le puede interesar:["Águilas Negras amenazan de muerte a Héctor Carabalí, líder social en Buenos Aires, Cauca"](https://archivo.contagioradio.com/amenazan-de-muerte-a-lider-social-en-buenos-aires-cauca/))

En consecuencia los resguardos indígenas le **están pidiendo al Estado garantías y protección de los derechos humanos**, asumir su responsabilidad en el resurgimiento de los grupos armados y cumplir su deber de proteger a las comunidades a partir del ejercicio que debe tener la Fuerza Pública en el territorio.

<iframe id="audio_31881231" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31881231_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
