Title: Presidente Duque sanciona Ley Estatutaria de la JEP
Date: 2019-06-06 18:25
Author: CtgAdm
Category: Paz, Política
Tags: jurisdicción especial para la paz, Ley estatutaria, Presidente Ivan Duque
Slug: presidente-duque-sanciona-ley-estatutaria-de-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/acuerdo-nacional-duque-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

A pesar de sus esfuerzos de modificar el proyecto de ley, el Presidente Iván Duque sancionó este jueves la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP) sin incluir cambios a los seis puntos del proyecto que objetó el pasado 10 de marzo.

Esto se dio después de que la Corte Constitucional ordenara al primer mandatario, firmar la Ley Estatutaria tras ratificar que el Congreso había efectivamente hundido las objeciones. La Ley 1957 del 6 de junio también fue firmada por el presidente del Senado, Ernesto Macías; por el de la Cámara, Alejandro Chacón y por todos los ministros del Gobierno. (Le puede interesar: "[Presidente Iván Duque objeta seis puntos de la Ley Estatutaria de la JEP](https://archivo.contagioradio.com/presidente-ivan-duque-objeta-seis-puntos-la-ley-estatutaria-la-jep/)")

"A partir de esta decisión, la JEP cuenta con todos los instrumentos constitucionales y legales para aplicar criterios y concentrar así el ejercicio de la acción penal respecto a quienes tuvieron participación determinante en los hechos más graves y representativos del conflicto armado", afirmó la justicia transicional en un comunicado.

Además, la Ley Estatutaria fija parámetros claros para la atención de las víctimas y afianza la seguridad jurídica para 11.805 comparecientes que están sometidos a este modelo de justicia.

Noticia en desarrollo...

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
