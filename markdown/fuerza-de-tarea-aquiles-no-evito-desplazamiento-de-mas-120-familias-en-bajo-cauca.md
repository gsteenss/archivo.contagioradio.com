Title: Fuerza de Tarea Aquiles no evitó desplazamiento de más 120 familias en Bajo Cauca
Date: 2019-04-15 22:49
Author: CtgAdm
Category: Comunidad, Entrevistas
Tags: Antioquia, Desplazamientos Forzosos, La Caucana, Plan de Sustitución de Cultivos de Uso ilícito, Tarazá
Slug: fuerza-de-tarea-aquiles-no-evito-desplazamiento-de-mas-120-familias-en-bajo-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/desplazamiento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo, imagen de referencia] 

Desde el 14 de abril se ha conocido que grupos paramilitares han dado la orden a cerca de 120 familias campesinas del corregimiento de La Caucana en Tarazá, Antioquia de abandonar el territorio, esto a pesar de que hay más de 6000 efectivos del Ejército que hacen parte de la **Fuerza de Tarea Conjunta Aquiles** estén presentes en la región y sin embargo no logren proteger a las comunidades que ante las continúas amenazas han tenido que desplazarse a Medellín.

**Según Óscar Yesid Zapata, integrante Coordinación Colombia Europa Estados Unidos** a pesar de las alertas que han sido emitidas sobre la situación que se vive en el Bajo Cauca la violencia sistemática no ha cesado contra 175 familias de los municipios de Cáceres, Tarazá, Caucasia y El Bagre y otros 41 hogares provenientes de Montelíbano, Córdoba que se vieron obligadas a dejar su territorio.

### **¿Qué interés existe sobre La Caucana?**

La confrontación por el poder que existe en Tarazá y en particular sobre el corregimiento de La Caucana se ha presentado entre las autodenominadas **Autodefensas Gaitanistas de Colombia y Los Caparrapos** quienes ven las rutas de**l Nudo de Paramillo**, **el sur de Córdoba, la troncal que va de Medellín** **hacia la Costa Atlántica**, una de las mas fuentes más grandes de financiamiento relacionado a cultivos de uso ilícito.

Zapata explica que gran parte de las poblaciones campesinas que están siendo desplazadas accedieron a programas de sustitución voluntaria de cultivos de uso ilícito, por lo que **"estas estructuras paramilitares verían en estas comunidades un peligro para sus fines económicos"**, lo que sería un indicador de que el Estado incumplió en materias de garantías a estas poblaciones.

Esta afirmación concuerda con estadísticas que documentan el asesinato de 30 personas entre 2017 y 2018 quienes hacían parte o lideraban programas de sustitución en zonas como **Tarazá, Caucasia o Cáceres**, mientras que "gran parte de la población que tiene acceso a la bancarización por los PNIS  están siendo extorsionados", denuncia Zapata.

### Falta de capacidad del Estado

A pesar que es de conocimiento público y que las autoridades han sido anunciadas de estas intimidaciones y  se haya activado una ruta, "ésta es demasiado débil" pues los homicidios selectivos, desapariciones forzadas y desplazamientos continúan, "para nosotros en nada ha cambiado el territorio con la presencia de la Fuerza de Tarea Aquiles" y que en ocasiones pareciera que el Estado "está a merced de la presencia de estas estructuras".

Zapata concluye que otras zonas como **Ituango o Valdivia** podrían ser objeto de desplazamientos a futuro debido a la presencia de estructuras de las AGC, disidencias  e incluso de otras organizaciones criminales que vienen desde Medellín como Los Pachelly. [(Le puede interesar: Se libra una guerra pr tomar el control el control del Nudo de Paramillo)](https://archivo.contagioradio.com/guerra-control-nudo-paramillo/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
