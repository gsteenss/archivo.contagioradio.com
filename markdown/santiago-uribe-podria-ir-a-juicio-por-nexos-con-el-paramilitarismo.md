Title: Santiago Uribe iría a juicio por creación de paramilitares de los 12 apóstoles
Date: 2016-08-18 09:52
Category: DDHH, Nacional
Slug: santiago-uribe-podria-ir-a-juicio-por-nexos-con-el-paramilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/santiago-uribe-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 18 Ago 2016 

**Fiscalía General de la Nación,** concluyó el proceso de investigación en **contra del ganadero Santiago Uribe Vélez**, hermano menor del expresidente Álvaro Uribe Vélez, por su presunta participación en la creación del grupo paramilitar 'Los 12 Apóstoles'. Según información conocida por Contagio Radio en 10 días habría resolución de acusación y se abriría la posibilidad de juicio contra Santiago Uribe quien se encuentra recluido en una guarnición militar de Rionegro.

En desarrollo...

 
