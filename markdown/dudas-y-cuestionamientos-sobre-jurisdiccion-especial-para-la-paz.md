Title: Víctimas piden desmantelar el paramilitarismo como garantía de la JEP
Date: 2015-09-26 00:13
Category: DDHH, Nacional
Tags: acuerdo de justicia en la Habana, acuerdo de paz, Conpaz, Cuba, ELN, FARC, Gobierno Santos, impunidad, Ley 975, Ley de justicia y paz, paramilitarismo en Colombia, proceso de paz
Slug: dudas-y-cuestionamientos-sobre-jurisdiccion-especial-para-la-paz
Status: published

###### Foto: Contagio Radio 

###### [25 Sept 2015] 

Por medio de una carta dirigida a las FARC, ELN y al presidente Juan Manuel Santos, las víctimas integrantes de la red CONPAZ (Comunidades Construyendo Paz desde los Territorios), manifestaron la necesidad de **desarticular el paramilitarimo en el país, y también cuestionaron si existe o no la voluntad para que políticos, militares y empersarios commparescan ante la Jurisdicción Especial para la Paz (JEP).**

Luego de conocerse el comunicado conjunto de las delegaciones del Gobierno y las FARC, sobre el acuerdo de justicia, denominado “Sistema Integral de Verdad, Reconciliación y no repetición”, por medio del cual se crea un Tribunal para la Paz, la red CONPAZ,  expresó su **sentimiento de esperanza frente al acuerdo y así mismo, dieron a conocer sus inquietudes,** pero sobre todo sus requerimientos para que existan garantías reales para las víctimas de todos los actores del conflicto armado.

**“...Nos llena de profunda alegría.** Este es un avance sustancial al pasar de un modelo eminentemente punitivo a uno restaurativo, en el que hemos creído y formulado propuestas nacidas desde nuestras comunidades en lo local y lo regional, y que aún siguen sin conocerse por ausencia de escenarios para tal efecto”, dice la carta.

Allí también señalan que se ha demostrado mayor posibilidad de reconocer y reparar los hechos cometidos por las guerrillas del ELN y las FARC, que lo que ha sucedido con las **responsabilidades que deberían asumirse desde el Estado,** teniendo en cuenta que este ha sido uno de los grandes impulsores de los más de 50 años de guerra en el país.

“Esta disposición no la hemos percibido de la misma manera por parte del Estado, se enuncian los reconocimientos de responsabilidad derivados de Sentencias internas o las del Sistema Interamericano, como expresiones en este sentido, pero las víctimas tenemos que decir, que esos reconocimientos no han sido reales, pues las prácticas estatales siguen siendo despectivas y contradictorias con los propósitos reparatorios”.

Frente a ese panorama y la evidencia actual de que el paramilitarismo continúa activo y atemorizando a las comunidades, CONPAZ exige que se les permita participar de ese sistema de justicia, y que se generen garantías a las víctimas como:

\* Tribunal idóneo, independiente y eficaz en el que sus integrantes sean examinados con lupa y cuenten con algún tipo de aval de las víctimas y de organismos intergubernamentales de derechos humanos

\* Cese de la persecución judicial con montajes contra nuestras organizaciones

\* Desmonte del paramilitarismo.

\*Comparecencia de operadores judiciales que actuaron como parte de un sistema penal corroído como un mecanismo de guerra bajo la concepción de enemigo interno a los pobladores y organizaciones civiles

Así mismo, por medio la carta se afirma afirma que **”la cárcel no ha posibilitado ni la verdad, ni la restauración ni la humanización del responsable de la violación ni la de nosotros las víctimas, si hay una verdad profunda se posibilita el encuentro entre los responsables y las víctimas,** así como en la creación de mecanismos novedosos de reparación como se están trabajando en el caso de Bojayá, proceso del que somos testigos”.

Finalmente, se realiza una serie de cuestiones y dudas frente a la forma como se procederá con el sistema de justicia acordado en la Habana.

1.  ¿De qué manera se va a lograr la participación de nuestras organizaciones en la elección o composición de los integrantes de las Salas de la JEP,  en las leyes estatutarias? ¿Cómo se van a asumir nuestras propuestas de derecho reparativo?
2.  ¿Cómo se van a tratar las más 45 mil desapariciones forzadas para lograr verdad con hallazgos de los restos de nuestras víctimas en el JEP?
3.  ¿Qué va a suceder con las personas privadas de la libertad y falsamente acusadas de ser parte de las guerrillas? ¿Qué va a suceder con presos sociales?
4.  ¿Cuáles son los criterios para definir los casos más graves y representativos, y      cómo asegurar que no haya exclusión? ¿Cómo asegurar, qué está vez no habrá impunidad, ante el poder real económico, el poder real político, el poder real militar que no quiere decir la verdad?. ¿Cómo lograr que no haya una nueva burla a las víctimas, y qué sin entregar verdad plena, los que saben, los que han sido responsables de graves crímenes sean silenciados para proteger a los beneficiarios e instigadores de la criminalidad estatal?
5.  ¿Cómo asegurar que los archivos de inteligencia, los expedientes de 975, los existentes en Corte Suprema de Justicia y ordinarios sean conocidos integralmente por las víctimas y sus organizaciones acompañantes?
6.  Han existido señales del eventual inicio de la mesa de conversaciones entre el gobierno que preside el doctor Juan Manuel Santos y ELN. ¿Lo aquí acordado respecto a la justicia como parte del Sistema Integral cómo es valorado en la fase exploratoria en su aplicación o no con el ELN? ¿Cuál es la opinión del ELN respecto a este Acuerdo? ¿Cómo se van armonizar los procesos de conversaciones con las FARC y el ELN para lograr hablar del fin del conflicto armado?
7.  Uno de los factores que incluso, está incidiendo negativamente, en el proceso electoral y en el cese unilateral del fuego, es el neo paramilitarismo, ¿cómo garantizar nuestra participación sin que este factor de violencia se haya acabado?

Para ver la carta completa haga clic [aquí.](https://comunidadesconpaz.wordpress.com/2015/09/24/jurisdiccion-especial-para-la-paz-algunas-de-nuestras-primeras-preguntas-e-inquietudes-en-esperanza/)
