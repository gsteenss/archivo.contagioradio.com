Title: Cumbre climática en Colombia con la deforestación en su máxima cumbre
Date: 2019-05-18 18:22
Author: Censat
Category: CENSAT, Opinion
Tags: Amazonía, deforestación, Grupo de Trabajo de Gobernadores sobre Clima y Bosques
Slug: cumbre-climatica-en-colombia-con-la-deforestacion-en-su-maxima-cumbre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/censat-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Comité de Comunicaciones del Foro Social Panamazónico 

[Resulta irónico que el país cuente en estos momentos con compromisos internacionales, sentencias judiciales de la Corte suprema, programas para reducir a cero la deforestación neta, millonaria inversión de cooperación para atender el problema, y al mismo tiempo se esté presentando la mayor tasa de deforestación hasta el momento: 219.973 hectáreas entre 2016 y 2017, principalmente en la Amazonía. El departamento del Quindío tiene una extensión de 185.000 hectáreas.]

[¿Qué está fallando? ¿Por qué las acciones y medidas que se proponen no están funcionando? ¿Por qué se está profundizando la deforestación? La respuesta pasa por giros equivocados en la comprensión de la crisis ambiental, olvidando que ésta se enraiza en las relaciones entre sociedades-naturalezas y privilegiando un abordaje exclusivamente ecológico que elude responsabilidades políticas y sociales en la crisis ecológica actual.]

[En esta senda la naturaleza se valora a partir de sus funciones ecosistémicas, y entre ellas cobra un papel preponderante la referida a la regulación climática,  es decir a las emisiones de gases de efecto invernadero (GEI), tranzándolos en una equivalencia de Dióxido de carbono (CO2), apenas uno de ellos. Por este motivo bosques y selvas son contabilizados como sumideros de carbono, y las comunidades que  habitan estos territorios son tratadas como guardianes del CO2.]

[Con gran parafernalia mediática se promocionan soluciones “curalotodo” en escenarios multilaterales enmarcados en esta reducción al absurdo de la problemática ambiental, lo cual contribuye a profundizar los problemas que dicen atacar. Tal es el caso de la Cumbre Global de Gobernadores por el Clima y los Bosques, que tuvo su undécima versión en Florencia, Caquetá, del 30 de abril al 3 de mayo del presente año, luego de que Colombia se integrara a este espacio a finales del año 2016, a través de la gobernación de Caquetá; adquiriendo entre otros compromisos, la firma de un acuerdo de cero deforestación.]

[Participan en esta iniciativa 38 gobiernos regionales de España, Indonesia, Costa de Marfil, Nigeria, Brasil, México, Perú, Ecuador, Estados Unidos y Colombia agrupados con la promesa de avanzar en la reducción de la deforestación, lo cual viabilizan a través de la mercantilización de las emisiones de GEI (comercializadas como carbono equivalente). De acuerdo a datos provenientes de Naciones Unidas, cuatro de los cinco países que mayor tasa de deforestación tuvieron en 2018 a nivel mundial fueron justamente Indonesia, Brasil, Perú y Colombia.]

[Las gobernaciones de los dos departamentos con mayor deforestación en Perú para 2018, Uyacali y Madre de Dios, están en este acuerdo desde hace varios años, igualmente los estados de Matto Grosso, Pará y Rondônia, los tres más deforestados en Brasil para el mismo período. En estos casos, con la incursión de proyectos de Reducción de Emisiones por Degradación y Deforestación (REDD), la deforestación en vez de disminuir, ha aumentado.]

[Éstas deberían ser pistas para que el Estado colombiano comience a replantear los mecanismos Redd como estrategia para reducir la deforestación, pero sobre todo para que con soluciones reales dimensione el problema. Los entes gubernamentales parecen no tomarse en serio este asunto, tanto así que en el Plan Nacional de Desarrollo 2018-2022 la meta es evitar que la deforestación no supere su nivel más alto, el citado para 2017, manteniendo esta tasa de deforestación en lugar de reducirla. En otras palabras, la ambición del ejecutivo actual representa la pérdida de cerca de 900.000 ha de selvas en el período gubernamental.]

[Las propuestas REDD de El Grupo de Trabajo de Gobernadores sobre Clima y Bosques (GCF) han tenido fuertes cuestionamientos en sus versiones anteriores, llegando a suscitarse protestas multitudinarias en el marco de estos encuentros, como ocurrió en California en 2018. Voces críticas han señalado a este enfoque como una falsa solución a la crisis climática y ambiental, así como una amenaza a la reproducción de los medios y modos de vida de las comunidades que dicen beneficiar a través de negocios con el CO2.]

[Por otra parte, comunidades campesinas e indígenas de los estados de Acre, California y Chiapas han identificado alteraciones de sus proyectos de vida colectiva, producto de programas REDD: División comunitaria, pérdida de identidad, militarización y engaños han emergido como denuncias recurrentes. Para el caso de Chiapas, la selva Lacandona es fuertemente custodiada por actores armados y cámaras de vigilancia  para controlar y evitar actividades como la pesca o la agricultura por parte de los pobladores locales; en Brasil, se han dado unas rupturas -hasta el momento irresolubles-, que han desintegrado al movimiento indígena regional y sus luchas históricas; en California organizaciones sociales rechazan y denuncian masivamente la ilegitimidad de este tipo de proyectos.]

[Escenarios como esta cumbre no pretenden reconocer las responsabilidades diferenciadas de sectores económicos y países en las emisiones históricas de CO2, ni cuestionar los motores de deforestación; en esa medida  dejan intactas las bases sociopolíticas que las posibilitan, pero promocionando la discursividad de estar actuando. De forma más amplia, continuar con un tratamiento de la crisis ambiental centrado en el carbono es tomar un camino peligroso, consistente en dividir la naturaleza hasta mínimas expresiones para permitir su apropiación y posterior mercantilización, cuando respuestas realistas deben partir de la conciencia de los orígenes de la crisis en un sistema sociopolítico que todo ha querido mercantilizar.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
