Title: La Pax como guerra preventiva contra lo alternativo
Date: 2016-02-03 11:48
Category: Camilo, Opinion
Tags: ELN, Frank Pearl, paz, proceso de paz eln
Slug: frank-pearl-la-pax-neoliberal-la-division-estrategica-de-lo-nuevo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/conversacion-eln-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Google 

#### **[Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [@camilodcasas](https://twitter.com/CamilodCasas)** 

###### 3 Feb  2016 

Iba a escribir sobre la importancia del horizonte de la paz como justicia ambiental ante la manera tan olímpica como Santos maneja la crisis del Niño, la no previsión sobre la Nina, las ZIDRES y el modelo de recuperación mercantil del Magdalena.

La Vida en su conjunto se va haciendo trizas de país sin futuro, el discurso de la prosperidad es la forma publicitaria para negar la carroña en que todo se viene convirtiendo por esa obsesiva operación de los chulos que aseguran su inversión, aquí de lo que se trataría, si de la paz integral, habláramos es de la  necesidad de la conversión o reconversión básica de la relación del hombre con  la producción de capital extranjero.

Sin embargo, al escuchar está mañana las declaraciones de Frank Pearl sobre  las conversaciones con el ELN y su respuesta a la disposición de esa guerrilla de iniciar la mesa y dejar en claro que desde noviembre están listos, quedan muchas dudas sobre quién en realidad o no, tiene la disposición de avanzar en la mesa  Más allá del tono arrogante, provocador, displicente del empresario Pearl que nos recuerda el tono del ex Ministro de Defensa, Juan Carlos Pinzón, qué hay detrás del tonito: tres conjeturas.

Primero se trata de un asunto estratégico. La Paz territorial planteada desde la seguridad democrática en el primer documento del gobierno de Uribe, firmado por la Ministra de Defensa Martha Lucía Ramírez y asesor en el Ministerio de Defensa del hoy, Alto Comisionado, Sergio Jaramillo. Esa apuesta estratégica mercantil  es la mismo que hay detrás de la pax neoliberal de Santos, asegurar la inversión privada en territorios rurales donde hay gente de carne y hueso, la mayoría de ellos excluidos, afrocolombianos, afros mestizos, indígenas, mulatos, y en muchos casos las guerrillas de las FARC EP y del ELN, justo allí en dónde se encuentra la riqueza biológica, los suelos y subsuelos, para producción intensiva de alimentos, las aguas, las minas vírgenes.

Para los alzados en armas hoy se ofrecen conversaciones y para los civiles políticas de seguridad represivas. Esa polaridad ha marcado todo el proceso con las FARC EP, que han logrado en medio de la lógica de sometimiento del establecimiento algunos avances formales y reales en el escenario político y ética. Al tiempo, la ZIDRES, la licencia exprés, el régimen económico para los privados sin que sus consecuencias para lo acordado importe. Divididas las guerrillas en ese escenario territorial una nueva tensión delicada se va a generar

Segundo, desde el comienzo la forma como el gobierno de Santos proyecta el proceso de salida al conflicto armado visualiza esa división.  La idea de dos mesas imposibilita la convergencia estratégica. La situación ha colocado en dilemas y tensiones a los sectores de influencia de las guerrillas, y un panorama adverso para el movimiento social que proyecta otro tipo de país, esto con independencia de las diferencias de método e ideológicas de estas.

Tercero. Esa dualidad pensada y proyectada sobre las conversaciones pretende hoy lograr un cierre de las conversaciones con las FARC EP, y dejar al garete al ELN para que así, la posibilidad de juego político con un proyecto convergente se dificulte, se generen nuevas desconfianzas y distancias y se dejé expuesto al movimiento revolucionario expresado en las guerrillas, como los responsables de imposibilitar la Paz, en realidad la Pax Neoliberal.

De seguir así, demonizado falsamente el ELN, como se logró con las FARC EP, y sin posibilidad de intentar simultaneidad en asuntos estratégicos por parte de las guerrillas, antes de que se cierre una mesa y se abra la otra,  está cercenada la posibilidad de construir desde ya una alternativa de país, al proyecto político dominante. Eso lo saben,  los estrategas de la guerra y sus beneficiados. La división es ganancia para el establecimiento. Hoy está asegurada la continuidad, la imposibilidad de los cambios desde un proyecto de país, nacido con sus errores y horrores, de lo que algunos llaman la izquierda y con otros sectores democráticos.

Es probable que de llegar a un Acuerdo con las FARC EP, el gobierno que no pretende en realidad cumplir más allá del respeto a la vida y algunos asuntos sociales, defina su Pax Territorial en el Catatumbo para en escenario resquebrar el movimiento social, como ha ocurrido en el pasado, modelo que se extendería en esa Pax Territorial a Arauca, Valle, Cauca, Nariño. Así es la clase dirigente colombiano, absolutamente perversa, sin el mínimo de sensibilidad para ceder a unos privilegios, injustos.

Para eso es el Plan Colombia II, con otro nombre, pero con el objetivo en la Pax Neoliberal de asegurar el poder político de los de siempre, dividiendo a las guerrillas y asegurando a través de ese establecimiento la privatización de los territorios
