Title: Suspenden Consulta Minera pero no el trabajo de ambientalistas en Ibagué
Date: 2016-10-13 16:19
Category: Ambiente, Nacional
Tags: consulta popular, defensa ambiental, Gran Minería en Colombia
Slug: suspenden-consulta-minera-pero-no-el-trabajo-de-ambientalistas-en-ibague
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/ibague.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Kavilando] 

###### [13 Oct 2016] 

A pesar de la suspensión de la Consulta Popular que se realizaría este 30 de Octubre en Ibagué, el **Comité Ambiental y las comunidades siguen trabajando en torno a la defensa del agua, la vida y el territorio.** Las actividades de movilización, información y pedagogía, a cargo del Comité, continúan en distintas comunas de la ciudad.

Esta es la tercera vez que se presenta una dilación para el proceso de esta Consulta Popular, Jaime Tocora, integrante del Comité Ambiental, señaló que si bien no se esperaban la negativa a la consulta popular minera, “se sabia que los opositores a la consulta y sectores que creían se verían afectados reaccionarían, **lastimosamente fue muy cercano a la fecha de la consulta, pero el trabajo con las comunidades no se suspende”**.

La sentencia emitida el pasado miércoles, por la sección cuarta del Consejo de Estado, en respuesta a una acción de tutela, **ordena la suspensión temporal del acto de Consulta Popular, lo que no implica que haya sido derogada.** Alberto Enrique Cruz Tello, ingeniero representante de una empresa minera y de hidrocarburos, según informó Tocora, fue quien interpuso la acción de tutela al Tribunal Administrativo del Tolima, alegando que con dicha Consulta se estaría violando su derecho al trabajo.

Tocora, resaltó que con esta acción colectiva no se busca quitar el trabajo a nadie, por el contrario, **se quiere frenar las licencias de explotación que han sido otorgadas hasta el momento para megaproyectos extractivos, “37% del cerro volcán el Machín y 33% del cañón de Combeima,** han sido otorgados (…) están solicitando 30% más aparte del 40% que ya tienen concedido en Ibagué, en total son 99 títulos”. Le puede interesar: [210 cuerpos de agua cerca al lago de tota desaparecieron por actividad petrolera](https://archivo.contagioradio.com/210-cuerpos-de-agua-cercanos-al-lago-de-tota-desaparecieron-por-actividad-petrolera/)

Además, este defensor ambiental, enunció que se sienten indignados no con la persona que interpuso la tutela, sino **“con un Estado que prioriza los derechos individuales por encima de los colectivos”**.  Añade que la organización Dejusticia y gente cercana a esta causa brindará apoyo en la parte jurídica para dar continuidad a la Consulta.

Para sacar 1 gramo de oro, se requieren 910 kilogramos de nitrato de amonio, explosivo más conocido como ANFO, y 1.060 litros de agua, además de remociones en masa. Lo que busca el Comité es **“proteger el ambiente de todos, y proteger al pequeño minero,** vincularlo al proceso de la consulta porque es claro que a futuro las grandes mineras progresivamente van a desplazar a estos mineros artesanales” señala Tocora. Le puede interesar: [Cambios climáticos afectan con mayor fuerza a la población vulnerable del mundo.](https://archivo.contagioradio.com/cambios-climaticos-afectan-con-mayor-fuerza-a-la-poblacion-vulnerable-del-mundo/)

Por encima del derecho particular, debe estar el derecho de todas las comunidades a un ambiente sano y mejores condiciones de vida. Tocora denuncia que “la Anglo Gold Ashanti es campeona en violar la ley colombiana, pero le decimos a esta empresa que tenemos un **Alcalde que puede decidir sobre el uso del suelo y el subsuelo, donde está el mineral y el hidrocarburo que ellos quieren"**.

Anuncia que así se haya suspendido la consulta, **“los ejercicios de democracia deben continuar, seguirán las movilizaciones para liberar a nuestro territorio de la minería contaminante”**. Y esperan que también se suspenda cualquier actividad por parte de la empresa, “no deben continuar sus tramites para las licencias mineras”.

Concluye que la defensa del agua, la vida y los territorios es fundamental en la construcción de Paz, pero asevera que **“mientras el Estado habla de paz, le declara la guerra al movimiento ambiental”. **

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
