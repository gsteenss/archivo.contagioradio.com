Title: "EPM sigue brindando información confusa a raíz de los derrumbes en Hidroituango" Ríos Vivos
Date: 2018-05-29 13:27
Category: Ambiente, Nacional
Tags: Bajo Cauca, EPM, Hidroituango, Movimiento Ríos Vivos
Slug: epm-sigue-brindando-informacion-confusa-raiz-los-derrumbes-hidroituango-rios-vivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/hidroituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Svenswikipedia] 

###### [29 May 2018] 

Una nueva alerta de evacuación fue emitida por Empresas Públicas de Medellín en la zona de afectación del proyecto hidroeléctrico **Hidroituango** debido a deslizamientos de las montañas. El Movimiento Ríos Vivos había alertado esta situación y denunció que las vías de algunos municipios se están agrietando y los habitantes de Ituango están incomunicados debido a que el puente Pescadero está inundado.

De acuerdo con las comunicaciones de EPM, los habitantes de Tarazá, **“se encuentran en aislamiento para evacuación inmediata”** por lo que sugirieron que “estén preparados para una evacuación”. Adicionalmente, ordenaron la evacuación de los trabajadores de la empresa en la presa.

### **“Información de EPM no es clara”** 

Según Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos, las comunicaciones de EPM continúan siendo confusas en la medida en que no hay información en torno a los avances o retrocesos en la **construcción de la hidroeléctrica**. Afirmó que la información que les ha llegado sobre la evacuación de las personas no ha sido clara y aún desconocen la situación de los derrumbes de las montañas.

\[caption id="attachment\_53602" align="alignnone" width="800"\][![Foto: Ríos Vivos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DeXT6mfXkAAe6Lb-800x600.jpg){.size-medium .wp-image-53602 width="800" height="600"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DeXT6mfXkAAe6Lb-e1527611566341.jpg) Grietas en la montaña del proyecto hidroeléctrico - Foto: Ríos Vivos\[/caption\]

Adicional a esto, Zuleta denunció que al Movimiento le habían permitido la entrada al Puesto de Mando Unificado. Sin embargo, cundo llegaron allí **no los dejaron entrar** argumentando que sólo podían hacer presencia las juntas de acción comunal. Esto fue catalogado como “un acto de discriminación” y no pudieron expresar sus preocupaciones. (Le puede interesar:["Ríos Vivos denuncia proselitismo del Centro Democrático en medio de crisis por Hidroituango"](https://archivo.contagioradio.com/rios-vivos-denuncia-proselitismo-de-centro-democratico-en-medio-de-crisis-de-hidroituango/))

### **Emergencia humanitaria está ocasionando brotes de diarrea en los niños y niñas** 

Dentro de las preocupaciones que ha venido manifestando el Movimiento se encuentra la precaria atención de niños y mujeres que ya empiezan a presentar “brotes de diarrea”. Esto debido a la ausencia de **agua potable** y la descomposición del material vegetal que se encuentra en el río.

Teniendo en cuenta la época electoral, Zuleta recalcó que hubo manejo político de la emergencia que se vive en el Bajo Cauca en aras de que las personas retornaran a sus hogares y pudieran sufragar. Sin embargo, esto genero confusión entre la población **“a la que se le dijo que no había riesgo”** y el domingo 27 de mayo, “el abstencionismo en toda la zona fue más alto de normal”.

Adicionalmente, enfatizó en que el censo de la población afectada **se detuvo** el fin de semana de las elecciones por lo que no se sabe el número de personas que requieren de atención humanitaria. El Movimiento hizo énfasis en que la alerta de derrumbe de las montañas se había hecho y no se tuvo en cuenta por parte de la empresa. (Le puede interesar: ["Alerta naranja no reduce riesgo de una avalancha en Hidroituango: Ríos Vivos"](https://archivo.contagioradio.com/alerta-naranja-no-reduce-el-riesgo-de-una-avalancha-en-hidroituango-rios-vivos/))

> Señores [@EPMestamosahi](https://twitter.com/EPMestamosahi?ref_src=twsrc%5Etfw) cuiden a sus héroes de [@hidroituango](https://twitter.com/hidroituango?ref_src=twsrc%5Etfw) Cuéntennos en qué condiciones y cómo se encuentran los trabajadores [pic.twitter.com/G50x3C1vcE](https://t.co/G50x3C1vcE)
>
> — ClaudiaJulietaDuque (@JulieDuque1) [27 de mayo de 2018](https://twitter.com/JulieDuque1/status/1000528423633203200?ref_src=twsrc%5Etfw)

### **Organizaciones internacionales pidieron que se proteja la vida de las comunidades** 

Por su parte, organizaciones sociales que hacen parte de la comunidad internacional manifestaron a través de una carta **la necesidad de proteger** a los integrantes del Movimiento Ríos Vivos que se encuentran amenazados. Pidieron que se implemente un plan de prevención y protección para las comunidades afectadas por Hidroituango teniendo en cuenta los riesgos que implica la construcción de la presa.

<iframe id="audio_26244592" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26244592_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
