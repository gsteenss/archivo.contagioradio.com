Title: Paramilitares amenazan con tomar el control del Bajo Atrato chocoano
Date: 2015-09-09 15:31
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas, Bajo Atrato chocoano, Batallón Fluvial, Bocas del Atrato, Chocó, Comisión de Justicia, El Tigre, Fuerzas militares, La Honda, paramilitares, Riosucio, Salaquí, Tumaradó
Slug: paramilitares-amenazan-con-tomar-el-control-del-bajo-atrato-chocoano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [www.vivelohoy.com] 

###### [9 Sept 2015] 

[El pasado lunes 7 de septiembre, **paramilitares pertenecientes a las Autodefensas Gaitanistas, obligaron a integrantes de los Consejos Comunitarios de Salaquí a participar de una reunión** en la que manifestaron que venían a implementar proyectos de desarrollo y a **enfrentarse con la guerrilla, con el fin de retomar el control del Bajo Atrato chocoano**.]

[De acuerdo con la Comisión de Justicia y Paz, en esta reunión **los paramilitares afirmaron que enviarían una misión a Cacarica, con apoyo de la policía y los militares de Turbo y Riosucio**. Anunciaron también, que estas reuniones iban a repetirlas en la comunidad de Clavellino, Salaquí, con los Consejos Comunitarios de Truandó, Curvaradó, Cacarica y Jiguamiandó.]

[El hecho ocurrió con **complicidad de las fuerzas militares y policiales**, pues se sabe que los armados ingresaron a la región en embarcaciones rápidas por el municipio de Riosucio, continuaron por el río Atrato, pasando por Tumaradó, El Tigre, Bocas del Atrato y La Honda, zonas con presencia permanente de unidades del batallón fluvial así como de la policía.]
