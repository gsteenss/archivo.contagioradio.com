Title: Bogotanos se manifiestan contra la venta de la ETB
Date: 2016-05-30 15:45
Category: Nacional, Paro Nacional
Tags: ETB, paro nacional bogotá
Slug: bogotanos-se-manifestaron-contra-la-venta-de-la-etb-frente-al-concejo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Manifestación-contra-venta-ETB.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [30 Mayo 2016]

Sobre las 10 de la mañana, desde la Avenida Boyacá con 1° de mayo, inició la movilización de movimientos estudiantiles y sindicatos para exigir al Concejo de Bogotá votar en contra del Plan Distrital de Desarrollo, pues varios de sus puntos permiten la enajenación de empresas públicas, desalojos injustificados y urbanizaciones en reservas forestales, según afirma Alejandra Wilches del Sindicato de Técnicos de la ETB.

<iframe src="http://co.ivoox.com/es/player_ek_11717102_2_1.html?data=kpakk5yVdJOhhpywj5WWaZS1lZWah5yncZOhhpywj5WRaZi3jpWah5yncaLgxs%2FO0MnWpYzLytHQysrXb46fsdfS1c7Iqc%2FowpCutqqwh6Khhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Al medio día el ESMAD y la Polícia cerraron las vías aledañas al Concejo de Bogotá, a las que llegaron los ciudadanos para presionar que no se permita la venta de la ETB, pues como asegura Camila Mahecha de la organización 'Identidad Estudiantil', la privatización afecta el patrimonio capitalino y las finanzas de la Universidad Distrital. Demandas que se unen a las exigencias que se presentan en el marco de la actual Minga Agraria, frente a los incumplimientos del gobierno nacional, y a la violación a derechos humanos de la que han venido siendo víctimas el movimiento social y estudiantil en su conjunto.

<iframe src="http://co.ivoox.com/es/player_ek_11717118_2_1.html?data=kpakk5yVdZmhhpywj5WXaZS1kZ2ah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Zw5CxpcnZxM3OjZKPjcXZz9nWxsbIb6bn1drRy8bSuMrgjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con María Alejandra Rojas de la 'Federación de Estudiantes Universitarios', si se está recuperando Bogotá pero para los empresarios, afectando a los sectores de la salud, de las telecomunicaciones y de la educación pública, y avanzando hacia la privatización. Por lo que se adelanta la recolección de 60 mil firmas para convocar un cabildo abierto que presione la no venta de la ETB y el pago de la deuda estatal con las universidades públicas colombianas, según asevera Katherine Duque, vocera de 'Comuna Universitaria'.

<iframe src="http://co.ivoox.com/es/player_ek_11717140_2_1.html?data=kpakk5yVeJGhhpywj5WWaZS1k5yah5yncZOhhpywj5WRaZi3jpWah5yncaLgxs%2FO0MnWpYzG0M%2FO1ZCRb6fZxcrfw8jNaaSnhqeg0JCns82fxcqYp9jYucXdwtPhx9iRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La agenda de movilización continúa con una lunada que se llevará a cabo este martes en la sede 'Macarena' de la UDistrital y una marcha que liderarán los docentes de Fecode este miércoles, para exigir el cumplimiento de los acuerdos pactados con el Gobierno.

 

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
