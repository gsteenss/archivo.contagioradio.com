Title: Acerca de las últimas elecciones en Venezuela
Date: 2015-12-09 11:56
Category: Javier Ruiz, Opinion
Tags: Derecha venezolana, Elecciones parlamentarias en Venezuela, MUD, Nicolas Maduro, Oposición Venezuela
Slug: acerca-de-las-ultimas-elecciones-en-venezuela
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/elecciones-en-venezuela-2010.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Por [Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/)- [@ContraGodarria ](http://contragodarria/)** 

##### [9 Dic 2015] 

“Fraude”, “nos van a robar las elecciones”, “no nos dan libertad de expresarnos” “todos los días cierran medios de comunicación, “todos los días nos meten presos”, “es una dictadura”, etc.

Así gritaba la oposición venezolana días antes de las elecciones y horas antes de conocerse los resultados definitivos de cómo iba a quedar conformada la Asamblea Nacional cuyas votaciones fueron el día domingo 6 de diciembre. Esta oposición venezolana ya estaba indisponiendo el ambiente y, en caso de perder las elecciones, gritar fraude y sabotear con disturbios y muertes nuevamente al país vecino.

Esta oposición se ha caracterizado por no tener un líder claro y con muchas divisiones internas, aunque aparenten ser una unidad (al menos en el nombre de su partido político), les ha tocado recurrir a líderes de derechas y extremas derechas internacionales como Alvaro Uribe, Andrés Pastrana, Jorge Quiroga, Sebastián Piñera, Mauricio Macri, José María Aznar, etc.

Siendo irónico que las cabezas visibles de la oposición venezolana no sean venezolanos y que los supuestos líderes venezolanos han sido cuestionados por su actuar antidemocrático como María Corina Machado o presos por el asesinato de gente inocente por no estar de acuerdo con las elecciones anteriores que perdían, como es el caso de Leopoldo López y Antonio Ledezma.

Si, ese 6 de diciembre los ojos estaban puestos en Venezuela porque ese país, al decir de la derecha y extrema derecha internacional y venezolana, es un país desobediente por decir que es de izquierda y socialista. Por ser de izquierda y socialista debe ser considerado un país inepto en su gestión pública y en su manejo de la economía y por ser de izquierda es un país dictatorial donde las libertades son escazas. Según lo anterior, Venezuela necesita un cambio “democrático” porque no se aguantaban más las “injusticias” del gobierno de Maduro.

Si es de injusticias estas han provenido de la oposición venezolana porque los hijos y herederos de los “adecos” y “copeyanos” hicieron un golpe de Estado en 2002 tumbando a un gobierno elegido democráticamente con muertes de por medio y muchos de esos golpistas indultados por el “dictador” Chavez. Esos herederos también han provocado un sabotaje petrolero y cada vez que perdían elecciones provocaban “guarimbas” (disturbios) para intentar el derrocamiento del chavismo y desconocer resultados electorales.

El argumento predilecto de esta extrema derecha venezolana y de la extrema derecha internacional es la supuesta escasez de libertad y los medios de comunicación, que sus dueños son afines a esas derechas y extremas derechas, han puesto su parte para desinformar sobre los que pasa en Venezuela. Ya tenían listos los titulares de “FRAUDE” o “DICTADURA” y que no creían en el árbitro que era el Consejo Nacional Electoral hablando mal de este y cuestionando el sistema electoral venezolano.

Se revelaron los resultados y la oposición gano la mayoría. De repente el discurso cambió y Venezuela dejo de ser esa despiadada dictadura donde las libertades para los opositores eran nulas. Dejaron de hablar mal del árbitro y dijeron que Venezuela tiene el mejor sistema electoral del mundo y la guarimba quedo para otra ocasión. Ahora esa oposición hará “cambios” y ya es explícito que ese “cambio” viene en forma de paquetazo neoliberal y en buscar como recortar los espacios democráticos para que los chavistas no participen. Llego el “gran cambio democrático” para quedarse.

Venezuela es un país democrático y, obvio, más democrático que Colombia. Venezuela es una “dictadura” muy rara en donde opositores ganan y en donde los medios de comunicación sin censuras piden que derroquen y maten al presidente Maduro.
