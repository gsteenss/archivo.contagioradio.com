Title: En Colombia una comunidad campesina ha luchado más de 40 años por el derecho a la tierra
Date: 2019-07-18 13:21
Author: JUSTAPAZ
Category: Opinion
Tags: afrodescendientes, agencia Nacional de Tierras, campesinas, comunidades, indígenas, Manuel Enrique Barreto, tierras
Slug: el-garzal-una-comunidad-campesina-ha-luchado-mas-de-40-anos-por-el-derecho-a-la-tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/En-Colombia-una-comunidad-campesina-ha-luchado-más-de-40-años-por-el-derecho-a-la-tierra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Justapaz 

##### **Por: Wendy Ramos – JUSTAPAZ. ** 

[La propiedad de la tierra y su concentración han hecho parte de la historia del conflicto armado colombiano. Históricamente las comunidades campesinas, indígenas y afrodescendientes han denunciado la distribución inequitativa de la tierra, el despojo y han pedido por el acceso digno a la tierra.]

[Diferentes intentos de reforma agraria han tratado -sin mucho éxito- de redistribuir la propiedad rural que se ha concentrado en pocos sectores de la sociedad colombiana. En el entre tanto, actores del conflicto armado, se han apropiado de tierras aumentando la concentración en unos cuantos y dejando millones de víctimas en ese camino. ]

[Muchas comunidades rurales en nuestro país han tenido que sufrir a la violencia, la corrupción, la indiferencia y el abandono estatal. En el Sur de Bolívar, en el municipio de Simití, se encuentra una de ellas: la **comunidad de El Garzal,** que en la actualidad acoge a más de 300 familias y quienes continúan reclamando la propiedad sobre tierras del Estado que han venido trabajado por más de dos generaciones.]

> [Los campesinos y campesinas de **El Garzal** se han visto afectados por diversas dinámicas del conflicto armado y el narcotráfico.]

[Durante los años 80 y la mitad de los 90, el ELN y las FARC-EP tuvieron el control total del sur de Bolívar. En este contexto llega a la región Manuel Enrique Barreto, quien posteriormente fue vinculado con las actividades ilícitas del Bloque Central Bolívar de las AUC y quién tenía interés en obtener el pleno control y dominio sobre estas tierras.  
]

[Esta comunidad ha sido víctima de desplazamiento forzado, extorsión y amenazas con el objetivo de despojarles la tierra que se ha dedicado por más de medio siglo a la producción agrícola, pecuaria y la sostenibilidad alimentaria familiar, para beneficiar intereses económicos como la ganadería extensiva, los monocultivos y el extractivismo. Sin embargo, estos no han sido los únicos medios para lograr dicho objetivo, pues desde 2004, Barreto y sus sucesores, han interpuesto diversas acciones judiciales y administrativas con la finalidad de impedir la adjudicación de la tierra a favor de la comunidad de El Garzal.]

[Tras años de resistencia pacífica y como una luz de esperanza ante la inacción por tantos años del Estado, la Corte Constitucional revisará una acción de tutela interpuesta por la comunidad en 2018, que pide que se ampararen sus derechos a la tierra y el territorio, a la dignidad humana, al trabajo, a presentar peticiones, al acceso a la información y al debido proceso administrativo. ]

[Se espera que la Corte Constitucional proteja los derechos de la comunidad y ordene a la Agencia Nacional de Tierras (ANT) dar respuesta a sus reivindicaciones por la tierra, dentro de un plazo razonable y sin demoras injustificadas. ]

[Mientras tanto, con la fuerza e imponencia del río Magdalena, esta comunidad organizada y pacífica continuará con el ejercicio de resistencia desde la fe, la exigencia y la reivindicación de sus derechos.  El Garzal, al igual que cientos de comunidades campesinas y étnicas, seguirá exigiendo el respeto por la vida y la garantía de los derechos fundamentales como herramientas para la construcción de una cultura de paz en Colombia. ]
