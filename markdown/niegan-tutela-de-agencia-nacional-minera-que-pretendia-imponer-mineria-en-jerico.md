Title: Con tutela, Agencia Nacional de Minería defiende multinacional: abogado ambiental
Date: 2019-02-18 15:02
Author: AdminContagio
Category: Ambiente, Judicial
Tags: acuerdo municipal, Agencia Nacional de Minería, Antioquia, Jericó, La Quebradona
Slug: niegan-tutela-de-agencia-nacional-minera-que-pretendia-imponer-mineria-en-jerico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/33081661_10155900278132772_7763543078492700672_n-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [18 Feb 2019] 

[El juzgado promiscuo municipal de Jericó, Antioquia, rechazó el pasado 13 de febrero una tutela de la Agencia Nacional de Minería (ANM) en contra del municipio antioqueño, que **solicitaba invalidar una medida preventiva de suspensión de actividad minera**, emitida por el Alcalde Jorge Pérez Hernández.]

Según el texto del fallo, la **tutela argumentaba que el Municipio de Jericó violó el derecho de la autoridad minera** al debido proceso por prohibir, a través de un acuerdo municipal, el desarrollo de actividad minera de metálicos y de gran y mediana minería de minerales, el cual fue **amparado luego en un acto administrativo el pasado 26 de enero.** (Le puede interesar: [Alcalde de Jericó exige a AngloGold Ashanti suspender actividad minera](https://archivo.contagioradio.com/alcalde-jerico-exige-anglogold-ashanti-suspender-actividad-minera/)")

Sin embargo, el juez José Andrés Gallego Restrepo r**esolvió negar esta tutela al considerarla improcedente**, argumentando que la acción cuestionaba la legalidad del acuerdo municipal, no el acto administrativo. **El juez sostuvo que el juzgado no puede revisar este acuerdo porque la jurisprudencia le corresponde al Tribunal Administrativo de Antioquia**.

Por otro lado, consideró que **la Agencia Nacional de Minería no está directamente afectada por los hechos y que para proceder la tutela tendría que ser interpuesta por AngloGold Ashanti** a razón que esta multinacional impulsa un proyecto de extracción de cobre, La Quebradona, en el territorio. A este punto, el abogado ambientalista Rodrigo Negrete agregó que el hecho de que la autoridad minera radicara esta tutela es preocupante pues **demuestra como el sector privado esta cooptando las funciones del Estado** para su propio beneficio.

Según el abogado, este proceso legal **podría proceder a un juez promiscuo del circuito de Jericó si es impugnado por el accionante** y posiblemente, llegar a la Corte Constitucional si el alto tribunal decidiera aceptar este caso. Actualmente, **se encuentra suspendida toda actividad de exploración en La Quebradona.**

<iframe id="audio_32652996" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32652996_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
