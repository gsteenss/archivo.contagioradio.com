Title: Así transcurrieron los 4 meses del cese bilateral al fuego entre el ELN y el Gobierno
Date: 2018-01-09 10:37
Category: Nacional, Paz
Tags: cese bilateral, ELN, Mesa Quito
Slug: asi-transcurrieron-los-4-meses-del-cese-bilateral-al-fuego-entre-el-eln-y-el-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [09 Ene 2018] 

Este 9 de enero finaliza el cese bilateral pactado entre la guerrilla del Ejército de Liberación Nacional (ELN), y el gobierno del presidente Juan Manuel Santos, que duró 4 meses y que deja como **balance cero confrontaciones entre ambos actores del conflicto**, incidentes en los que se vio involucrada la sociedad civil y avances en los puntos de participación social y acciones humanitarias que conlleven al desescalamiento del conflicto armado.

### 

### **Incidentes durante el cese bilateral** 

De acuerdo con el balance realizado por la ONU, si bien el balance es positivo, en términos del cese de enfrentamientos y ataques a infraestructuras de oleoductos, como se había establecido en el acuerdo del 1 de octubre, la sociedad civil se vio involucrada en hechos de violencia, en territorio rural, en este periodo de tiempo.

Uno de esos incidentes se presentó el pasado 25 de octubre, cuando el ELN se atribuyó el asesinato del líder indígena Aulio Isaráma, perteneciente al resguardo indígena Catru Dubasa Ancoso, ubicado en el municipio del Alto Baudó, en el departamento del Chocó. A través de un comunicado de prensa de esta guerrilla, informaron que el hecho se dio después de la retención al líder indígena, cuando Isaráma intenta escapar y es asesinado en medio de un forcejeo.

En la misma misiva el **ELN pidió perdón por el hecho y asevero que los integrantes de esta guerrilla no tenían autorización para acabar con la vida del Aulio Isaráma**.

El segundo incidente se registró el 27 de noviembre, en Magüi, Payán, departamento de Nariño, luego de que el ELN asesinara a 13 personas, hecho que de acuerdo con la ONU, produjo el desplazamiento de más de 138 personas de este territorio. De igual forma, el ELN aceptó su responsabilidad y aseguró que los asesinatos se cometieron en medio de una confrontación entre esta guerrilla y grupos disidentes de la FARC.

Así mismo, las comunidades del Bajo Atrato y del litoral del Bajo San Juan, han denunciado, en diferentes ocasiones, el peligro que viven al quedar inmersas en las confrontaciones entre el ELN, grupos neoparamilitares y disidencias de la FARC, **que han provocado el desplazamiento y confinamiento de familias en el departamento del Chocó**. (Le puede interesar: ["Combates entre el ELN y AGC provocan el desplazamiento de 12 familias en Jiguamiandó, Chocó"](https://archivo.contagioradio.com/combates_eln_agc_desplazamiento_jiguamiando_choco/))

### **Las violaciones por parte de la Fuerza Pública**

Por otro lado el ELN, manifestó que durante este periodo de tiempo, la Fuerza Pública violó el cese bilateral, **con acciones como desembarcos en zonas de influencia de esta guerrilla e incluso en cercanías a campamento**s, en donde se encontraban los guerrilleros para cumplir el cese.

Además informaron que también se realizaron retenes y empadronamientos en las vías de acceso a las zonas conocidas como de influencia, **incursiones militares sin dar previo aviso, como se había acordado, el ataque a las fuerzas del ELN concentradas en el río Truandó**.

### **Avances en la mesa de diálogo** 

Estos 4 meses, también han sido útiles para el avance del punto de participación en la mesa de diálogo de Quito, desde el mes de noviembre y hasta principios de diciembre, se realizaron las audiencias preparatorias **para el escenario de participación, en el que hicieron presencia más de 211 delegaciones y que contó con 10 sesiones**.

Estos espacios tuvieron como finalidad recolectar propuestas sobre mecanismos de participación o experiencias, desde todos los sectores de la sociedad, que serán analizadas por las delegaciones de ambos equipos, para construir o implementar lo que será la metodología de participación de la sociedad en Quito. (Le puede interesar:["Avanzan las audiencias preparatorias para la participación social en diálogos ELN-Gobierno"](https://archivo.contagioradio.com/48655/))

### **Las propuestas para continuar el cese** 

Pese a que el cese bilateral culmina el día de hoy a la media noche, ya diferentes organizaciones y plataformas han expresado so voz de aliento para pedir que se reanude lo más pronto posible y **se han esbozado 3 propuestas que podría tener en cuenta la mesa, para continuar con el cese**.

La primera de ellas es la posibilidad de que se prolongue el cese bilateral unos días, mientras se acomodan los nuevos integrantes de las delegaciones, otra de ellas es que se revisen los protocolos con los que funcionó el cese durante este tiempo para que se blinde mucho mejor y se eviten situaciones de riesgo para la sociedad, y finalmente esta la propuesta de que se reanude de forma inmediata.  (Le puede interesar:["Cientos de personas y organizaciones hacen llamado para que se prolongue el cese bilateral"](https://archivo.contagioradio.com/cientos-de-personas-y-organizaciones-hacen-llamado-para-que-se-prolongue-el-cese-bilateral-entre-gobierno-y-eln/))

###### Reciba toda la información de Contagio Radio en [[su correo]
