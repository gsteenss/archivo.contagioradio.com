Title: "Falsos positivos" no deben pasar a la JEP
Date: 2017-04-06 13:17
Category: DDHH, Nacional
Tags: Ejecuciones Extrajudiciales, falsos positivos, JEP, justicia, militares
Slug: 38886-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [06 Abr. 2017]

Cerca de 33 organizaciones nacionales e internacionales aseguraron que, **la decisión de un juez de Bogotá en la que suspende el proceso penal en  contra de 12 militares** implicados en casos de ejecuciones extrajudiciales, causa un grave daño y evita que los responsables rindan cuentas de graves violaciones de derechos humanos.

El juez se negó a realizar la audiencia de imputación de cargos y solicitud de medida de aseguramiento en contra de 12 miembros del Ejército, incluido el coronel Gabriel de Jesús Amado Rincón, **argumentando que la Jurisdiccion Especial para la Paz -JEP- tendría la competencia para  hacerlo.**

Sin embargo, las organizaciones manifestaron que **"no existe ninguna disposición en el Acuerdo de Paz, o en la legislación vigente en Colombia, que permita suspender procesos penales** contra miembros de la Fuerza Pública presuntamente responsables de atrocidades".

Además, agregaron que **la JEP aún no ha sido puesta en funcionamiento y no está claro cuándo comenzará a operar**, razón por la que podría seguirse configurando la falta de acceso a la verdad y a la justicia para las víctimas. Le puede interesar: [Por décima vez aplazan audiencia por "falsos positivos" de Soacha](https://archivo.contagioradio.com/por-decima-vez-aplazan-audiencia-por-falsos-positivos-de-soacha/)

"La decisión sienta un precedente preocupante que podría **tener como resultado que Colombia viole su obligación de investigar y sancionar** sin demoras indebidas, las graves violaciones a los derechos humanos", señalaron las organizaciones firmantes. Le puede interesar:[ Primer General del Ejército será llamado a juicio por "falsos positivos"](https://archivo.contagioradio.com/general-del-ejercito-llamado-a-juicio35257/)

Para las organizaciones, está decisión del Juzgado 9° Penal de Garantías de Bogotá parece ser inconsistente pues **"de conformidad con el acuerdo, la JEP tendrá competencia para juzgar abusos cometidos por agentes del estado**, si estos están relacionados con el conflicto armado".

Así mismo el acuerdo prevé que será la propia JEP —y no los jueces ordinarios de Colombia— la encargada de determinar cuáles son los casos de su competencia. Le puede interesar: ["Juez noveno de garantías "se burla" de las víctimas de ejecuciones extrajudiciales"](https://archivo.contagioradio.com/juez-noveno-de-garantias-se-burla-de-las-victimas-de-ejecuciones-extrajudiciales/)

Por último, las **organizaciones saludaron la sentencia dictada el 3 de abril, en contra de 21 militares por otro de los casos de Soacha**, por un tribunal diferente.  Le puede interesar:[ "Falsos Positivos" son crímenes de Lesa Humanidad](https://archivo.contagioradio.com/falsos-positivos-son-crimenes-de-lesa-humanidad/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
