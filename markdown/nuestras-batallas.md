Title: Nuestras batallas, los desafíos de ser padre en solitario en tiempos del capitalismo
Date: 2019-05-27 13:49
Author: CtgAdm
Category: 24 Cuadros
Tags: Cine frances, Guillaume Senez, romain duris
Slug: nuestras-batallas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/NRXWE6aA.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nuestras Batallas 

Recordado en Colombia por su interpretación de Xavier en la trilogía de Cédric Klapisch (Una Casa de locos (2002), Las Muñecas rusas (2005) y Nueva vida en Nueva York (2013)), Romain Duris vuelve a encarnar  a un padre de familia en **Nuestras batallas,** un drama francés dirigido por [**Guillaume Senez** que llega a las pantallas colombianas el próximo 30 de mayo, presentado durante la Semana de la Crítica del Festival de Cine de Cannes en 2018.]{.m_-636214433934668756pb .m_-636214433934668756wb}

En esta oportunidad el actor parisino se mete en la piel de **Olivier**, un hombre de edad mediana que ha logrado consolidar cierto liderazgo entre un grupo de empleados que laboran en una factoria, notoriedad que poco a poco le va a encaminando hacia el activismo sindical. La situación para Olivier se vuelve compleja cuando de forma repentina **su esposa Laura (Lucie Debay) decide abandonar el hogar, dejándolo a cargo de sus dos pequeños hijos,** con la gran incertidumbre que significa el no encontrar respuestas a tal decisión.

En Nuestras Batallas, **Duris demuestra una evolución en su actuación** que le permite dar forma y carácter a un personaje que durante el film **busca equilibrar sus responsabilidades como cabeza de un hogar golpeado por el vacío que deja la ausencia de la madre** en lo afectivo, lo emocional y lo económico; y el **mantenerse en lucha por los derechos de sus compañeros a tener condiciones de trabajo más dignas,** evitando replicar la historia de su padre, un hombre que dedicó su vida por la causa dejando a su familia en un segundo plano.

<iframe src="https://www.youtube.com/embed/JxEJe2G5xSU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Para el director la cinta propone una perspectiva sobre el mundo laboral como se presenta en la actualidad bajo las condiciones de una sociedad capitalista, sin tratar de teorizar en el tema, mas bien desde el terreno de la sensibilidad humana **"Quería mostrar a un personaje abandonado y que no logra imaginar cómo puede ayudar a las personas que ama",** así como los aprendizajes propios de la paternidad bajo el contexto en que la debe asumir** "es una película sobre la paternidad: Elliot y Rose son los que harán crecer a Olivier, convertirlo en padre, llevarlo a establecerse y pensar en su vida privada, y, en sus relaciones con los demás y con el mundo"\***

Para el estreno en Colombia de este 30 de mayo se han confirmado la salas en Bogotá de **Cine Colombia Avenida Chile, Cine Tonalá, y la Cinemateca Distrital; en Medellín Cine Colombia Vizcaya, el MAMM y el Colombo Americano y en Cali  en Cine Colombia Unicentro.**

###### \*Entrevista hecha por Olivier Séguret

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
