Title: La importancia y/o minimización de las aseveraciones
Date: 2016-01-25 10:42
Category: Opinion, Tatianna
Tags: Cristina Plazas, Defensor del pueblo, Defensoría del Pueblo
Slug: la-importancia-yo-minimizacion-de-las-aseveraciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/juzgar-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: jaquealtitiritero.] 

#### **[Tatianna Riveros](https://archivo.contagioradio.com/opinion/tatianna-riveros/)- ([@[tatiannarive).]

###### 25 Ene 2016. 

Lanzar juicios de valor se ha convertido el pan de cada día, haciéndonos creer que hay comentarios o hasta denuncias más valiosas que otras, que una es más importante, que una puede ser una chisme y otra no; y es precisamente ese el equívoco en el que hemos caído: nos han hecho creer que algo que no es válido para nosotros no debe serlo para los demás, pero, ¿cuántas veces nos hemos puesto en el lugar serio y crítico de ver una y otra parte, sin perder la objetividad?

Recientemente dos hechos que involucran a funcionarios público circularon por redes sociales, periódicos y noticieros: la tutela de Cristina Plazas contra Gonzalo Guillén y las denuncias hechas por Daniel Coronell y Astrid Cristancho sobre Jorge Otálora e infinidad de comentarios, criticas, juicios, argumentos a favor y en contra, solicitud de renuncias, hasta de censura se ha hablado.

Pues bien, para el caso de Plazas y Guillen tengo que decir que sacar los gustos de la funcionaria por el fútbol y el whisky no hacen menos serias las denuncias del periodista, y que burlarse de una persona por una enfermedad como el alcoholismo (si es que la Directora del ICBF está enferma) tampoco es  que demuestre mucho profesionalismo, o que una tutela pidiendo cerrar una cuenta sea el medio idóneo para reclamar el derecho al buen nombre o solucione la situación de los niños de La Guajira que aún siguen muriendo de hambre.

Ahora, que presuntamente exista un acoso sexual por parte de un superior (porque podría ser cualquiera, el acoso laboral y sexual no es ejercido solamente por un superior, puede ser de superior a subalterno, de subalterno a superior, entre compañeros, el acoso no distingue), no minimiza el hecho de publicar las fotos que éste (el Defensor) le enviaba a su subalterna, ni el hecho de que su subalterna sea bonita, legitima al otro para acosarla; y quiero hacer mención de que la conducta no se agrava porque sea un funcionario público, porque es que en ninguna entidad privada, pública, mixta, grande o pequeña, debe existir el acoso laboral, y en toda entidad privada, publica, mixta grande o pequeña deben existir los mecanismos para prevenirlo.

Lo que si no me cabe en la cabeza es como una abogada, con experiencia, inteligente (porque escuché su entrevista en La W tras la primera denuncia de acoso laboral en la que ella también es la víctima) es que hasta ahora se haya atrevido a denunciar, que haya pasado dos años callada conociendo y sabiendo de las herramientas a su favor; error sería que creyeran que porque es profesional no puede tener miedo, o que el miedo es para los que no estudiaron, no, el miedo es para los que no saben, y el silencio (cuando se sabe) a veces (casi siempre) es complicidad.

Resulta entonces paradójico que ambos temas se hayan reducido  al sexo de una y otra parte, creer que una mujer es víctima porque es mujer, o que un hombre debe ser humillado porque es hombre, o que no le podemos creer a una funcionaria porque le gusta la fiesta y el whisky, creer que porque es periodista y el periodismo está sobrevalorado y acabado en este país no le debemos creer. Llama la atención preguntarnos,  ¿en qué estamos fallando? y la respuesta es sencilla: en creer en que una columna, un tweet, una artículo de opinión como este, son un juicios de valor, en juzgar sin conocer  a profundidad un tema, y en creer que un argumento es válido o es erróneo por x o y motivo, que todas y cada una de las cosas que decimos tienen una responsabilidad más allá de las redes sociales, sí, y es que la tecnología nos ha empujado a eso, a encontrar nuevas formas de divulgación y lapidación, antes, era suficiente con que un rey preguntara al pueblo “¿a quién queréis que libere, a Jesús o a Barrabás?, hoy es un hashtag pidiendo la renuncia, o el nombre de la persona de la noticia convertido en tendencia.

La invitación entonces es a dejar la idea fallida de que el sistema judicial no funciona y que por eso es mejor el escarnio público,  la humillación y la “denuncia” periodística, porque estas últimas aseveran la acusación pero de fondo, no resuelven nada.

###### Leer más columnas de opinión de [Tatianna Riveros ](https://archivo.contagioradio.com/tatianna-riveros-2/) 
