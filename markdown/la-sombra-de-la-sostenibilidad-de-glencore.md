Title: La Sombra de la sostenibilidad de Glencore
Date: 2015-12-01 12:23
Category: Ambiente, Entrevistas
Tags: ejecuciones extrajudiciales en Colombia, falsos positivos en Cesar, Glencore, Glencore en Colombia, Glencore en el Cesar, Prodeco viola derechos humanos
Slug: la-sombra-de-la-sostenibilidad-de-glencore
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Glencore-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Digital Question 

###### [1 Dic 2015 ]

[La Organización Pensamiento y Acción Social PAS, en articulación con el Grupo de Trabajo Suiza-Colombia, lanzan el informe ‘Sombra de Sostenibilidad de las Operaciones de Glencore en Colombia’ una investigación sobre los **impactos tributarios, ambientales, laborales, sociales y en derechos humanos** del accionar de la multinacional Suiza en la región del centro del Cesar.]

[Esta **multinacional opera de manera clandestina** a través de 5 empresas colombianas del grupo Prodeco, implementando contratos de concesión fragmentados con regímenes de pagos de regalías e impuestos diferenciados que han generado **detrimento patrimonial por el orden de más de  20 mil millones de pesos anuales** **en los últimos 5 años**, de acuerdo con la Contraloría y que se suman a los descuentos que el Estado colombiano ha concedido a la empresa en los que **por cada \$100 de su pago en regalías le son descontados** \$124.** **]

[Otro de los hallazgos de este informe, de acuerdo con Rafael Figueroa, abogado integrante de PAS, se relaciona con los **60 procesos sancionatorios** que cursan contra la empresa por cuenta de los impactos negativos de sus actividades en la mina Calenturitas, entre los que se destacan de acuerdo con Figueroa,  la **desviación de ríos y afluentes que proveen agua a más de 20 mil pobladores** del Cesar y la **intervención en áreas de protección especial** como la Serranía del Perijá, frente a los que la multinacional multas impuestas por el Ministerio de Ambiente, sin que esto implique el resarcimiento de los daños causados.]

[En materia de afectación a los derechos humanos se destacan los **convenios que Glencore ha suscrito secretamente con el Ministerio de Defensa** en la última década a través de los que ha pagado **cerca de 180 mil dólares anuales en los últimos 5 años**, para la instalación de de un batallón dentro de la mina 'Calenturitas', en el municipio de la Jagua.]

[ Una unidad militar que desde 2007 se ha visto involucrada en casos de **ejecuciones extrajudiciales** que cursan en investigaciones de la Fiscalía y por los cuales han sido condenados 15 miembros del Ejército, sin que esto haya significado que la empresa reconozca su responsabilidad en esta práctica ni que detenga la financiación a través de dichos convenios.]

[Según este informe más de **50 investigaciones cursan en contra de la multinacional por persecución sindical y las precarias condiciones de seguridad industrial** de sus empleados quienes se enfrentan a tratos inhumanos y a la desatención de las enfermedades que adquieren por cuenta de su empleo, hechos frente a los que Glencore "responde con largas batallas legales para no tener en su contra un fallo definitivo", de acuerdo con Figueroa.]

[El último de los hallazgos del informe tiene que ver con los **reasentamientos involuntarios en el centro del Cesar por cuenta de la contaminación ambiental** que padecen varios municipios de este departamento por cuenta de la extracción de carbón. las comunidades además de verse afectadas por el impacto al medio ambiente no cuentan con garantías de reasentamiento dignas y las indemnizaciones no son suficientes para que **más de 130 familias** de las que han sido afectadas superen los **niveles extremos de pobreza** en los que se encuentran.]

[Conozca el informe: ]

[Informe Sombra de Sostenibilidad Glencore](https://es.scribd.com/doc/291820797/Informe-Sombra-Glencore "View Informe Sombra Glencore on Scribd")

<iframe id="doc_925" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/291820797/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
