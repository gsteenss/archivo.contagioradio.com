Title: Acuerdos deben implementarse con enfoque de género y territorial
Date: 2016-09-22 12:28
Category: Mujer, Paz
Tags: Acuerdos de paz en Colombia, enfoque de género en los acuerdos de paz, mujeres constructoras de paz, Violencia contra las mujeres en Colombia
Slug: acuerdos-deben-implementarse-con-enfoque-de-genero-y-territorial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Cumbre-Mujeres-Paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Casa de La Mujer ] 

###### [22 Sept 2016] 

Este miércoles concluyó la II Cumbre Nacional de Mujeres y Paz, de la que participaron por lo menos 500 integrantes de organizaciones de todo el país, para acordar las líneas de trabajo que permitirán verificar que la implementación de lo pactado en La Habana se de bajo la **perspectiva de la garantía de los derechos de las mujeres y con un enfoque territorial**. En el encuentro las líderes reafirmaron su apoyo a los acuerdos de paz, y llamaron la atención del Gobierno colombiano para que asuma un compromiso real frente al desmonte del paramilitarismo y la [[concreción de la fase pública de negociaciones con el ELN](https://archivo.contagioradio.com/piden-intervencion-de-papa-y-onu-para-iniciar-fase-publica-con-eln/)].

"Para nosotras es muy importante que se acabe el conflicto armado y que podamos avanzar hacia un país en paz que sabemos hay que construirlo (...) tenemos toda la disposición para que se transite hacia un país más incluyente, democrático y que aplique a fondo al acuerdo de paz" asegura Marina Gallego, coordinadora nacional de la Ruta Pacífica de las Mujeres, y agrega que **para garantizar la consolidación de lo acordado con la guerrilla de las FARC-EP es necesario iniciar el proceso de negociación con el ELN,** y que los [[grupos paramilitares se sometan a la justicia](https://archivo.contagioradio.com/con-los-paramilitares-hubo-un-intento-de-justicia-paloma-valencia/)].

Parte de las conclusiones a las que llegaron es que las organizaciones abrirán sus puertas a las militantes de las FARC-EP para que se reintegren a la vida civil en igualdad de beneficios y condiciones que los hombres; pues **las experiencias a nivel mundial han demostrado que las mujeres han quedado relegadas en la implementación** de los procesos de paz, por lo que se constituirán como un puente para la [[reintegración social, política y económica](https://archivo.contagioradio.com/las-mujeres-y-la-defensa-de-los-derechos-humanos-en-colombia/)] de aquellas mujeres que dejen las armas, así como de quienes están privadas de la libertad.

Las líderes reafirmaron su compromiso frente a la lucha por la reparación de las mujeres víctimas, con un enfoque transformador, efectivo e integral; en este sentido plantean que harán visibles sus memorias desde la diversidad de lenguajes y narrativas. Del mismo modo no cederán en su exigencia de participación paritaria en todas las instancias en las que sus organizaciones logran empoderamiento, en lo que incluyen su **rechazo categórico a todas las formas de corrupción y mal manejo de los recursos públicos**, pues el compromiso implica el impulso a todo tipo de procesos [[para que las mujeres alcancen un buen vivir](https://archivo.contagioradio.com/el-logro-historico-de-las-mujeres-en-el-acuero-final-de-paz/)].

<iframe id="audio_13015549" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13015549_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
