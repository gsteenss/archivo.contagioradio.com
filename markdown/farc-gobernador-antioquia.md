Title: FARC desmiente acusaciones del Gobernador de Antioquia
Date: 2016-12-28 11:00
Category: Nacional
Tags: Antioquia, colombia, FARC, zonas de concentración
Slug: farc-gobernador-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/niños-farc-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Cosmovisión 

##### 28 Dic 2016 

Como "**pretensiones para sabotear la paz**" calificaron las FARC EP, las afirmaciones hechas por el Gobernador de Antioquia Luis Pérez, en las que denuncia la supuesta explotación sexual de menores cometidos por guerrilleros en las **zonas veredales de preagrupamiento de San Francisco, Yondó y en Dabeiba** ubicadas en ese departamento.

En un comunicado emitido la mañana de este miércoles, el Estado Mayor Central del grupo insurgente asegura que "**El país conoce que la ONU supervisa los puntos de preagrupamiento de las FARC-EP**" y que con esta falsa acusación "**demuestra otra vez la vocación de guerra del Gobernador**".

"**No aceptamos su visita a nuestros campamentos por que solo llevan odio y rencor**" advierte la guerrilla al Gobernador en su comunicado y aseguran que sus declaraciones  "nada dicen del proceso de reconciliación que vive hoy la familia colombiana y de la que hace parte la guerrillerada fariana, porque su odio no les permite ver eso".

De acuerdo con lo expuesto en la comunicación, el Gobernador confunde las visitas de los familiares de guerrilleros con la ocurrencia de actividades sexuales indebidas, aclarándole que **comunidades como la de San Francisco tienen como norma que "no admiten prostitutas en su región**"

Al tanto de la situación y con la misión de investigar y establecer lo ocurrido en estas zonas, el comité de monitoreo y verificación tripartita ha determinado enviar un grupo experto que pueda visitar este jueves 29 de diciembre las zonas de concentración veredales. Le puede interesar:[Mecanismo de monitoreo pide celeridad en puesta en marcha de zonas y puntos veredales](https://archivo.contagioradio.com/33429/)
