Title: Prográmese con los eventos de paz esta semana
Date: 2016-10-18 16:23
Category: Movilización, Nacional
Tags: #114Razones, #AcuerdosYA, #PazALaCalle
Slug: programese-con-los-eventos-de-paz-esta-semana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/pazcalle.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: PazalaCalle] 

###### [18 de Oct 2016]

Siguen sumándose las iniciativas al movimiento ciudadano Paz a la Calle que para esta semana ya tiene listas actividades en diferentes localidades de Bogotá y Colombia, se espera que la actividad cumbre sea el próximo **20 de octubre en donde se llevará a cabo la gran marcha “Colombia Pacífica”.**

En Bogotá el 18 de octubre se han organizado dos actividades, la primera es el conversatorio de **Paz Territorial que se llevará a cabo en la Plaza de Bolívar a las 7:00pm**, lugar en donde aún continúan el Campamento por la paz. De igual forma desde la comisión de género de Paz a la Calle, se realizará la **twitteraton \#114Razones para defender el enfoque de género y diversidad sexual en el acuerdo de paz**, desde las 6:00pm. **En Medellín, este mismo día se hará la asamblea ciudadana en la escaleras del Museo de Arte Moderno de Medellín (MAMM)**.

El 19 de octubre en Bogotá se realizará una **donatón  como parte de una asamblea previa** a la gran movilización del 20 de octubre, el punto de encuentro será el monumento del Almirante Padilla, en el Park Way, desde las 5:00pm. En el Cesar, este mismo día, se **llevará a cabo la asamblea ciudadana en la Universidad Popular del Cesar, a las 6:00pm**.

El 20 de octubre en **Barranquilla se realizará la Abrazatón por la paz, en la Plaza de la Paz desde las 5:00pm** y se espera que diferentes ciudades se unan a la gran marcha “Colombia Pacífica”.

El 21 de octubre en Bogotá la cita será en la Plaza de Bolívar con el encuentro **“Yoga es Paz”, desde las 4:00pm**, de igual forma se realizará la **asamblea departamental de Antioquia a las 9:00am en el Recinto de la Asamblea Departamental**.

De acuerdo con Laura Rubio, miembro del movimiento cívico Paz a la Calle, lo ideal es que cada nodo que se forme en el mundo establezca sus propios principios y que concuerdan en la defensa de los acuerdos de paz de la Habana.

Si conoce de otros eventos, nos puede informar a contagioradio@contagioradio.com o a nuestras redes sociales: Twitter: [@contagioradio1](https://twitter.com/Contagioradio1) o Facebook: [contagioradio](https://www.facebook.com/contagioradio/)

<iframe id="audio_13374375" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13374375_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
