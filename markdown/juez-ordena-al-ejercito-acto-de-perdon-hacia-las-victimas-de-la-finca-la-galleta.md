Title: Juez ordena al Ejército pedir perdón a las víctimas de la finca La Galleta
Date: 2020-05-12 16:16
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Antioquia, Bananeras, restitución de tierra, Urabá Antioqueño
Slug: juez-ordena-al-ejercito-acto-de-perdon-hacia-las-victimas-de-la-finca-la-galleta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Tierras-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 12 de mayo la **Fundación Forjando Futuros** comunicó la reciente orden del Juzgado Primero Civil del Circuito Especializado de Antioquia, en la que **ordena al Ejército Nacional pedir públicamente perdón a las víctimas de [falsos positivos](https://archivo.contagioradio.com/jep-protege-memoria-de-las-victimas-en-el-cnmh/)y el asesinato de 4 personas en la toma del Ejército Nacional al predio “La Galleta”.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La [orden indica](http://www.forjandofuturos.org/noticia_interior.php?id_ent=788)que por medio de un acto público de reconocimiento y reivindicación por las acciones realizadas, la Cuarta Brigada del Ejército Nacional, debe pedir perdón a las 13 familias víctimas de *"violación de Derechos Humanos, homicidio agravado, secuestro extorsivo, desaparición forzada, desplazamiento y amenazas".*

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/forjandofuturos/status/1260237870314504192","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/forjandofuturos/status/1260237870314504192

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En una rueda de prensa **Julio Cuastumal**, abogado integrante de la Fundación se refirió a los casos de falsos positivos, persecución y homicidio perpetrados por el Ejército en Motebello, Antioquia en el predio "La Galleta" el 23 de enero del año 2000.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Galleta era una finca usada para proyectos productivos y **fuente de empleo de desmovilizados** de la Corriente de Renovación Socialista -CRS- del ELN, quienes conformaban la S**ociedad Agropecuaria Horizontes**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"**Excombatientes que creyeron en un proceso de paz y empezaban acciones legales en este predio fueron acusados de guerrilleros, secuestrados, cuatro de ellos asesinados y desaparecidos por hombres del Ejército**"*, señaló Cuastumal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado agregó, que según testigos los uniformados pintaron las paredes con siglas de las Autodefensas, y por años los integrantes de esta sociedad fueron perseguidos por el Estado y adjudicados a diferentes hechos ilegales, que los llevaron a desplazarse de La Galleta y incluso refugiarse en otros países.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Luego de 20 años por fin hay justicia para las víctimas de La Galleta

<!-- /wp:heading -->

<!-- wp:paragraph -->

La orden condenó también a los militares **Humberto de Jesús Blandón Vargas y Sandro Fernando Barrero**, *"miembros de la Cuarta Brigada del Ejército Nacional, como autores penalmente **responsables de los delitos de homicidio agravado y secuestro extorsivo ambos en concurso homogéneo y sucesivo**"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"El estado tiene que pedir excusas públicas por todos los vejámenes que sufrieron estas personas y especialmente por **el homicidio de los señores José Evelio Gallo Gallo, Uberney Gerardo Castro** y la **desaparición de Gerardo Sánchez Gil**".*
>
> <cite>**Julio Cuastumal** | abogado integrante de la Fundación Forjando Futuros</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

El abogado agregó que también **se ordena al Centro de Memoria Histórica hacer varias publicaciones con el fin de restablecer los derechos de las personas** que fueron presentadas como falsos positivos por parte de la brigada Cuarta del Ejército.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**Este es un acto de reivindicación a todas estas personas que ha sido perseguidas durante estos 20 años,** esperamos que el estado restaure sus derechos y realice las actuaciones pertinentes para pedir perdón a todas estas familias que han sido afectadas".*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Además la sentencia entregada el pasado 7 de mayo, decide restituir las 133 hectáreas y los 5800 metros cuadrados que conforman la finca La Galleta, y *"**reconoce la condición de segundos ocupantes a 17 campesinos a quienes se les permite explotar algunas partes del predio** y adjudicar a otros campesinos víctimas del conflicto las otras partes del mismo".*

<!-- /wp:paragraph -->
