Title: Tres razones para suspender TransMilenio por la 68
Date: 2020-01-23 12:20
Author: CtgAdm
Category: Entrevistas, Política
Tags: Avenida 68, Bogotá, Claudia López, Transmilenio
Slug: tres-razones-para-suspender-transmilenio-por-la-68
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Transmielnio-por-la-Av.-68.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Transmilenio-por-la-68-e1579799848631.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @mjsarmientoa {#foto-mjsarmientoa .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El lunes de esta semana se presentó una protesta al sur de Bogotá contra la construcción de TransMilenio por la Avenida 68. Este es el primer desencuentro de Claudia López con concejales del Polo y la Alianza Verde, que le pidieron detener la licitación de la obra, señalando que es una troncal incompleta y con problemas en la planeación del Proyecto.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Las razones para pedir que se suspenda TransMilenio por la Avenida 68**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según explicó el concejal por el Polo Manuel Sarmiento, esta semana el Comité de No Transmilenio por la 68 convocó una marcha para pedirle a la Alcaldesa Claudia López que suspenda la licitación de la Troncal. En consecuencia, Sarmiento sostuvo que **esperan que López cumpla uno de sus compromisos en campaña, que fue no hacer el Proyecto.**

<!-- /wp:paragraph -->

<!-- wp:image {"id":79622,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Transmielnio-por-la-Av.-68-1024x875.jpg){.wp-image-79622}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

La primera razón para pedir la suspensión del Proyecto tiene que ver con la posición fijada por concejales del Polo como Carlos Carrillo o el mismo Sarmiento respecto a Transmilenio. Ambos Concejales han expresado que **la solución en movilidad para Bogotá no debe centrarse en flotas de buses diésel,** sino en corredores de metro, tranvía o trenes ligeros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En segundo lugar, Sarmiento expresó que de acuerdo con un memorando interno firmado por un funcionario del Instituto de Desarrollo Urbano (IDU), **el proyecto no estaba bien estructurado, ni listo para abrir licitación.** Denuncia que también realizó el exconcejal Hollman Morris respecto al proyecto Metro de Bogotá. (Le puede interesar:["Irregularidades y mentiras sobre el Proyecto Metro de Bogotá"](https://archivo.contagioradio.com/mentiras-metro-bogota/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/SilvanaEFEC/status/1220327376615280640","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/SilvanaEFEC/status/1220327376615280640

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En tercer lugar, el Sarmiento señaló que **el proyecto pasó de costar cerca de 4 billones de pesos a 2,6 por pedido del Gobierno Nacional,** en cabeza de Iván Duque. Tal reducción significó eliminar obras como la intersección de la troncal con la Calle 80 y la autopista norte, o un puente de circulación exclusivo en la Calle 100 con Carrera 15.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el primer caso, la eliminación significa que no habría rutas que continúen desde el inicio de la Troncal en la Autopista Sur hasta los Portales de la 80 o Norte, por ausencia de una intersección como la que existe en la estación Escuela Militar. En el segundo caso, implicaría que articulados circulen en carriles mixtos, generando mayor congestión de la que ya existe sobre el puente de la Calle 100 con Carrera 15.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Ya hay una acción popular interpuesta**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sarmiento dijo que esperan que considerando las razones técnicas y los fallos en la licitación, así como el compromiso hecho en campaña, la Alcaldesa suspenda la licitación. Sin embargo, sostuvo que esperan resoluciones de la justicia, pues en noviembre de 2019 radicaron una acción popular solicitando **medidas cautelares urgentes que detengan la licitación, que se daría este jueves**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:html -->  
<iframe id="audio_46925575" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46925575_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
