Title: 14 Empresas mineras deben al Estado $13.608 millones
Date: 2017-04-05 16:36
Category: Ambiente, Nacional
Tags: Contraloría, Mineria
Slug: 38858-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/mineria-colombia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: confederacionminera.blogspot 

###### [ 5 Abr 2017] 

Las empresas mineras como **AngloGold Ashanti, Eco Oro Minerals y Cemex Colombia le deben al Estado colombiano** **\$13.608 millones en Canon Superficiario.** Una contraprestación que cobra la Agencia Nacional de Minería, sobre la totalidad del área de la concesión durante la exploración y explotación minera.

Así lo evidencia una auditoría de la Contraloría General de la República a la que tuvo acceso Portafolio. Se trata de un análisis de control fiscal en el que se establecieron 44 hallazgos administrativos, 12 presentan presunta incidencia disciplinaria y hay 3 con posible incidencia fiscal por más de  \$17.641 millones, de los cuales **el 77% corresponden al no pago de canon superficiario.**

Un hecho que, según la auditoría de la Contraloría, afecta los intereses de todos los colombianos, entendiendo que se perjudica la  economía estatal. Y es que el informe señala que por parte de las autoridades mineras, ha habido ineficiencia en la supervisión y cobro de los dichos cánones, imposibilitado el recaudo de esos recursos al patrimonio.

La auditoría encuentra que más de \$13.000 millones corresponden a 20 títulos mineros, concesionados a **14 empresas mineras** como AngloGold Ashanti, Gaia Energy Investmens, **Eco Oro Minerals Corp., Minincol y Cemex Colombia.**

La empresa con mayor deuda es la minera canadiense **AngloGold Ashanti que debe en canon superficiario \$4.502 millones**. Por su parte, la multinacional Gaia Energy, es la segunda con mayor deuda, con un monto total de \$2.491 millones.

Esta situación se da debido a que “Las autoridades mineras en su momento no iniciaron las correspondientes acciones de cobro coactivo frente a aquellas obligaciones expresas, claras y exigibles de contenido económico establecidas en los actos administrativos de caducidad de títulos mineros, acuerdos de pago e imposición de multas”, dice el informe de la auditoría.

Pero no es la primera vez que las mineras siguen beneficiándose económicamente, sin retribuir al país. Cabe recordar que ha finales del 2016, el representante a cámara, Víctor Correa denunció que  [las empresas mineras en Colombia pagan cero pesos de impuesto de renta](https://archivo.contagioradio.com/empresas-mineras-no-pagan-impuestos-renta-colombia/), como lo evidencian la declaraciones de renta de multinacionales como **Carbones El Cerrejón en La Guajira; Cerro Matoso en Córdoba, y Drummond en Cesar**. Situaciones que según Correa, "**violan el principio constitucional de progresividad,** según el cual deberían pagar más quienes más tienen”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
