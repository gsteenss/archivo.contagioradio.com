Title: Así fue el Paro de las Mujeres en el mundo
Date: 2017-03-08 13:30
Category: El mundo, Mujer
Tags: 8Marzo, Paro Internacional de Mujeres
Slug: avanza-el-paro-de-las-mujeres-en-el-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/luciano-Munhbauer-e1488994381347.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: luciano Munhbauer] 

###### [08 Mar 2017]

Avanzan las movilizaciones de las mujeres en el mundo. Desde muy temprano en diferentes países como Ecuador ,España y Brasil, las mujeres comenzaron a salir a las calles para exigir sus derechos en este 8 de marzo. Con arengas y consignas como **"Las mujeres movemos el mundo" o "Ni una menos"**, miles de mujeres, denunciaran las diferentes violaciones de derechos de las que son víctimas en sus respectivos países.

<iframe id="audio_17469005" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17469005_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Las plazas de ciudades como Madrid, Bilbao y Alameda, España, se llenaron de miles de mujeres y hombres que salieron a exigir al gobierno de Rajoy, **garantías y cumplimiento de los derechos de las mujeres, justicia en los casos de violencia de género y la creación de políticas de género. **

En Brasil, las mujeres salieron a las calles a reivindicar sus derechos laborales y a exigirle al presidente Temer que frente el avance del desempleo en este país. Mientras que Italia las plazas de Roma escucharon la consigna **"Non una di meno", haciendo referencia a la violencia contra la mujer que cada día cobra más víctimas en este país.**

<iframe src="https://www.google.com/maps/d/embed?mid=1AcjbqhlKK_aiB7EDwQn93zHZzBo" width="640" height="480"></iframe>

En Colombia, actividades como la rodada en bicicleta por la capital ya dio inició, la jornada se desarrollará en otras ciudades como Cali, Barranquilla, Medellin, entre otras. Le puede interesar:["Arranca el Paro Internacional de Mujeres"](https://archivo.contagioradio.com/arranca-el-paro-internacional-de-mujeres/)

\

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)
