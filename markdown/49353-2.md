Title: Jean Arnault expresó su preocupación por el proceso de reincorporación en Colombia
Date: 2017-11-21 20:16
Category: Nacional, Paz
Tags: acuerdos de paz, FARC, paz
Slug: 49353-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/censo-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Nov 2017] 

Jean Arnault, jefe de la misión de la ONU para la reincorporación dio un balance con cifras alarmantes sobre el proceso que se viene adelantando con los integrantes de la entonces guerrilla FARC para que se dé el tránsito a la sociedad civil. Arnault expreso que tan solo hay un **45% de los excombatientes en las zonas de reincorporación y que todavía no existe un plan marco para la reincorporación**.

Fenómenos que para Arnault “no se puede eclipsar” sobre todo si se tiene en cuenta que para mayo de este año eran **8.000 los guerrilleros que se encontraban en las zonas veredales, en las que actualmente hay menos de 5.000**, y agregó que pese a que hay razones para salir de estos lugares como los motivos personales y familiares, la realidad es que el motivo más grande ha sido la falta de credibilidad en el gobierno.

El jefe de la Misión de la ONU señaló que uno de los principales que han tenido las personas que se están reincorporando a la vida civil, es que no existe una actualización en la base de datos de la Fuerza Pública, razón por cual, **todavía se encuentran detenidos, no pueden acceder al sistema bancario o firmar contratos con el Estado**. (Le puede interesar:["A un año de la firma de los Acuerdos del Teatro Colón, son muchos los retos para el 2018"](https://archivo.contagioradio.com/49340/))

**Los proyectos productivos**

Uno de los retos más importantes que debía poner en marcha el Gobierno Nacional tenía que ver con los proyectos productivos en los diferentes espacios territoriales de capacitación, sin embargo, de acuerdo con Arnault, estos proyectos se están llevando con éxito en 10 de las 26 zonas y **específicamente por la colaboración que se ha podido tejer entre integrantes de la FARC y el apoyo de universidades**.

Finalmente el Jefe de la Misión de reincorporación de ONU hizo un llamado al gobierno para que aumente sus esfuerzos y no vislumbrar un fracaso, de cara a que no se repita un fenómeno como el ocurrido con las desmovilizaciones del paramilitarismo en donde las personas terminaron en economías ilegales, motivo por el cual manifestó que el acceso a la tierra debe ser una prioridad.

###### Reciba toda la información de Contagio Radio en [[su correo]
