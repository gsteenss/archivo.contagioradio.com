Title: “Joaco” y Mónica también son colombianos: Paz a la Calle
Date: 2016-11-19 16:51
Category: Nacional, Paz
Tags: Asesinato Joaco y Monica, Cese al fuego, FARC, guerrilla, militares
Slug: joaco-y-monica-tambien-son-colombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/joaco-y-monica-apa_prensa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Blu Radio] 

###### [19 Nov. 2016] 

Ante el asesinato de “Joaco” y Mónica los dos guerrilleros de las Farc del frente 37, sucedido el pasado 16 de noviembre, diversos movimientos sociales y de la ciudadanía como **Paz a la Calle han manifestado recientemente que hechos como ese deben ser repudiados.**

En una comunicación de una página, Paz a la Calle aseguró que “es un hecho confuso y lamentable – el asesinato de los dos guerrilleros de las Farc -” y manifestaron **“no queremos ni una vida más sacrificada para la guerra”.**

Para Jairo Rivera, integrante de Paz a la Calle lo que sucedió con Monica y “Joaco” "nos remite un poco a la producción de sentido común  que impera desde hace muchos años en Colombia, en donde la muerte de un guerrillero no importa (…) y es que eso tiene que ver es con que durante muchísimos años ha habido una producción de un imaginario contrainsurgente en Colombia”.

Por su parte, el Campamento por la Paz ha asegurado en la misiva, que hay que comenzar a entender que debe primar la vida de cualquier persona **“estas dos personas –“Joaco” y Mónica- son colombianas, como los que fueron víctimas de las Farc, como los que fueron víctimas de los paramilitares”.**

Por último han solicitado al Gobierno Nacional, a las Farc y a la Comisión de Verificación Tripartita  se aclaren los hechos y las responsabilidades de este asesinato que cobro la vida de dos personas. Le puede interesa: [Según comunidad, Ejército sí habría violado cese bilateral al fuego](https://archivo.contagioradio.com/segun-comunidad-ejercito-si-habria-violado-cese-bilateral-al-fuego/)

Y concluyen instando a que se implementen los acuerdos ya, para que de esta manera no se pase **“otra navidad en medio de la guerra y en medio de la peor que es la que se vive a través de la incertidumbre (…). Navidad sin paz no es navidad”** dijo Rivera.

<iframe id="audio_13821074" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13821074_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
