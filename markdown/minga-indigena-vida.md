Title: Este lunes inicia la Minga nacional indígena por la vida
Date: 2017-10-29 19:00
Category: Movilización, Nacional
Tags: indigena, Minga
Slug: minga-indigena-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/la-minga-es-alegria-3-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [24 Oct 2017] 

La Organización Nacional Indígena de Colombia, ONIC, expresó a través de su vocero, Fernando Arias, que este lunes 30 de octubre se retomara la Minga Nacional Indígena y se realizarán movilizaciones en diferentes departamentos del país, con la finalidad de exigirle al gobierno que cumpla con el capítulo étnico de los Acuerdos de paz, en donde se establecía hacer un proceso de consulta previa a las comunidades.

A estos incumplimientos se suman los decretos de autoridades ambientales y el decreto del Sistema Nacional de Participación que no han sido firmados por el presidente y los incumplimientos en los acuerdos de educación y tierras.

De igual forma, en un comunicado de prensa la ONIC señaló que esta Minga también tendrá como objetivo rechazar los asesinatos a indígenas y campesinos en el país "lo que sucedió y sigue sucediendo a los hermanos Awá, afros y campesinos en Tumaco, al pueblo Kokonuko, a los Wounaan, Sikuanis, a los Pueblos Originarios en sus territorios ancestrales, no puede repetirse en otras regiones del país, y menos en tiempos de Paz”

Se espera que cada guardia indígena del país se encargue de coordinar la movilización en su territorio, al igual que las actividades que van a estar acompañando el proceso de Minga, finalmente Arias afirmó que las comunidades indígenas han estado excluidas de los acuerdos pactados en La Habana y que en ese orden de ideas y sin la inclusión de los indígenas no podrá gestarse una paz en las comunidades.

##### Reciba toda la información de Contagio Radio en [[su correo]
