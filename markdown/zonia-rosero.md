Title: Feminicidios en Putumayo ascienden a más de 13 con asesinato de lideresa Zonia Rosero
Date: 2019-03-05 15:09
Author: CtgAdm
Category: DDHH, Líderes sociales
Tags: asesinato de líderes sociales, feminicidios, Puerto Asís Putumayo
Slug: zonia-rosero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/D04s3E8X4AA3mXq.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: JFColombia 

###### 5 Mar 2019 

Después de haber recibido varios impactos de bala el pasado 25 de febrero y  permanecer 48 horas en pronóstico reservado, falleció de un paro cardíaco **la doctora y lideresa Zonia Rosero,** en un centro asistencial de Pasto el pasado 27 de febrero. Zonia había manifestado su deseo de participar en los comicios por la Alcaldía de Puerto Asís, Putumayo.

**La diputada del Putumayo, Yury Quintero** explica que Zonia Rocero ejercía su liderazgo al interior de las comunidades, tanto campesinas como urbanas, sirviendo a las personas a  través de su profesión, "siempre estaba con los jóvenes, las mujeres y las personas mayores de edad, buscaba que fueran atendidos y escuchados".

La doctora Rosero ya había recibido amenazas en su contra, manifestando estar en peligro, **"eso estaba muy anunciado, pero hasta que no suceden las cosas, nadie hace nada",**  afirma la diputada Quintero, quien también destaca la buena relación que tenía la lideresa con la Fuerza Pública, lo que pudo ponerla en peligro.

### **En Putumayo hoy hay una pelea por territorios** 

En lo que va corrido del año, en la región se han conocido 25 casos de asesinatos. Entre esos la mayoría de muertes han sido mujeres, de las cuales siete fueron víctimas de homicidio durante el fin de semana pasado.

Estas cifras se suman a las amenazas que han recibido las comunidades de excombatientes que habitan en los **ETCR del departamento**. [(Lea también 413 mujeres fueron víctimas de feminicidio en los últimos 3 años)](https://archivo.contagioradio.com/?p=62642&preview=true)

Según la diputada, el narcotráfico en el departamento está cobrando fuerza debido al vacío que se dejó en el territorio después de la dejación de armas con el grupo Farc y el no cumplimiento de los acuerdos,  "estamos volviendo a  la violencia que se vivió durante el 2002 y el 2004"  agrega Quintero, quien destaca la presencia de diferentes sectores armados como **la banda criminal La Constru, el Clan del Golfo, las disidencias y los paramilitares**, además de una posible aparición del ELN en la zona.

Asímismo, Quintero afirma que este enfrentamiento por los territorios se siente aún más en las contiendas electorales y que con la muerte de la doctora Zonia Rosero es posible identificar con más fuerza la complicidad, el silencio y la no atención de los sucesos en la región por parte de las autoridades. Sin embargo también los motiva a "alzar la voz y evidenciar lo que está sucediendo en el departamento".

<iframe id="audio_33088566" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33088566_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
