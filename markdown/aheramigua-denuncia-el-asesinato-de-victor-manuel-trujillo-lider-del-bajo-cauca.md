Title: AHERAMIGUA denuncia el asesinato de Víctor Manuel Trujillo, líder del Bajo Cauca
Date: 2019-01-17 11:42
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: Aheramigua, Amenazas, Bajo Cauca, lideres sociales
Slug: aheramigua-denuncia-el-asesinato-de-victor-manuel-trujillo-lider-del-bajo-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/victor-trujillo-lider-bajo-cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Víctor Trujillo 

###### [17 Ene 2019] 

**La Asociación de Hermandades Agroecológicas y Mineras de Guamocó** (AHERAMIGUA) denunciaron a través de un comunicado de prensa, el asesinato de **Víctor Manuel Trujillo Trujillo**,  según la organización, el crimen habría sido cometido por presuntos miembros del Frente Guillermo Ariza del Ejército de Liberación Nacional -ELN, el pasado 15 de enero en la vereda Puerto Guamo, zona Caribona del municipio de Montecristo, Bolívar.

Trujillo, fue uno de los líderes que impulso el paro Agrario del 2013, además era un artista del género urbano que desarrollo diversas iniciativas culturales enfocadas a la juventud. Además fue miembro de la junta directiva de AHERAMIGUA hasta que decidió retirarse, luego de ser víctima de graves amenazas de grupos paramilitares, persecuciones y detenciones arbitrarias **por parte de la Fuerza Pública** y haber pasado 3 meses en el exilio, sin haber recibido protección estatal luego de su regreso a Colombia.

### **El conflicto armado en Antioquia** 

AHERAMIGUA denunció que a finales del 2018 el ELN cometió una serie de acciones que vulneraron los derechos humanos y el Derecho Internacional Humanitario en la región del Guamocó, jurisdicción del Bagre y Nechí, Antioquia, y Montecristo y Santa Rosa, Bolívar, como la siembra de minas antipersona, **el reclutamiento forzoso de menores de edad, extorsiones y amenazas en contra de los líderes comunitarios.**

Razón por la cual, la organización le exige al Ejército de Liberación Nacional "que cese sus ataques a la población civil", de igual forma le pidieron tanto a la guerrilla como al gobierno nacional, que continúen con la mesa de diálogos y conversaciones para lograr una salida pacífica al conflicto armado.  (Le puede interesar:["Colombia el país con más asesinatos a defensores en el mundo"](https://archivo.contagioradio.com/colombia-pais-mas-asesinatos-defensores-mundo/))

https://youtu.be/aMkc\_JIt-BY

 

###### Reciba toda la información de Contagio Radio en [[su correo]
