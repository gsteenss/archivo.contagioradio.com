Title: Empresarios venezolanos piden derogar leyes de precios justos y del trabajo
Date: 2015-12-09 11:44
Category: El mundo, Otra Mirada
Tags: ANTV, Diosdado Cabello, Ley de precios justos, ley orgánica del trabajo, MUD, Nicolas Maduro, PSUV, Trabajadores de Venezuela, Venezuela, VTV
Slug: empresarios-venezolanos-piden-derogar-leyes-de-precios-justos-y-del-trabajo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/trabajadores_venezuela_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AVN 

###### [9 Dic 2015]

FEDECAMARAS, una de las más importantes agrupaciones de empresarios de Venezuela, pidió que sea **derogada la ley de precios justos**, instituida para evitar la especulación y favorecer el bolsillo de los trabajadores y trabajadoras regulando los precios de los productos de la canasta básica, **y la ley del trabajo que, entre otras cosas, prohíbe la tercerización laboral para proteger el derecho al trabajo.**

En una rueda de prensa ofrecida luego de que el Consejo Nacional Electoral publicara los resultados en los que se confirma que la Mesa de Unidad Democrática tendrá 112 de los 167 escaños en el congreso, la agremiación empresarial afirmó que **es necesario que se deroguen estas leyes para promover la inversión privada y atacar el desabastecimiento** de productos de la canasta básica.

Esta sería una de las primeras acciones que implementaría el nuevo congreso de ese país. Analistas afirma que con este tipo de acciones se **desenmascara la verdadera guerra económica, ya que estas dos leyes y otras que serían reformadas favorecen los intereses económicos de los trabajadores** y no de los empresarios y derogarlas sería facilitar las ganancias que muchas veces superan el 400% en diversos productos.

Así mismo, dirigentes de esa agrupación política (MUD) afirmaron que el canal público de la Asamblea Nacional deberá cambiarse y los trabajadores deberán salir, situación ante la cual los trabajadores de los canales públicos se manifestaron por sus derechos y fueron respaldados por el actual presidente de la AN, **Diosdado Cabello quien afirmó que el canal deberá ser propiedad de los trabajadores.**

A su vez el presidente Nicolás Maduro convocó a una **asamblea extraordinaria de los integrantes del Partido Socialista Unido de Venezuela, PSUV**, que se reunirá a partir de este jueves para hacer una evaluación profunda de los resultados electorales y planificar las acciones de defensa de los derechos adquiridos durante cerca de 20 años de revolución bolivariana.
