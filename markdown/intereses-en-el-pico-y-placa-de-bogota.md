Title: Pagar para contaminar, el decreto de Peñalosa con el pico y placa
Date: 2019-10-31 17:39
Author: CtgAdm
Category: Ambiente, Política
Tags: Bogotá, Movilidad, pico y plaza
Slug: intereses-en-el-pico-y-placa-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/movilidad-n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:][U.Andes] 

Sectores ambientalistas rechazaron el  decreto de la  **Secretaría de Movilidad que permite  el levantamiento del pico y placa en Bogotá a los propietarios de vehículos particulares** (1,8 millones) que paguen **\$2'066.000 cada semestre**, esta medida empezaría a regir desde enero de 2020. (Le puede interesar [¿Cómo afecta la calidad del aire a los bogotanos?](https://archivo.contagioradio.com/aire-de-bogota-en-alerta-roja/) )

Para los defensores del ambiente en Bogotá **este decreto del pico y placa busca obtener recursos adicionales para el Sistema Integrado de Transporte público de la ciudad**, afirmando que tendría  un ingreso de **177.000 millones de pesos** en el próximo año con este cobro, para Camilo Prieto  vocero de la Fundación Movimiento Ambientalista Colombiano "no nos deja de sorprender la **capacidad de improvisación del distrito para sacar fondos**, afirmando de alguna manera [que si usted tiene como pagar, puede contaminar más". ]

En un principio la medida "p[ico y placa" está diseñado para disminuir el flujo vehícular pero también para reducir las emisiones de gases de efecto invernadero y CO2 que disminuyen la calidad del aire, "deben ser conscientes de los riesgos que trae a la población este tipo de políticas, tener claro que **a mayor cantidad de vehículos en Bogotá más tóxico será el aire que respira la** **ciudadanía.** Un ejemplo es, que pasaría en lugares como la Sevillana- Carvajal, las Américas o las autopistas, donde se presentan los niveles más elevados de partículas de carbono asociadas a emisiones vehículares"  añadió Prieto. ]

[Para Prieto una de las soluciones está en estimular las tecnologías limpias, como  los medios de transporte alternativos que no solo son sostenibles con el planeta, también generan rutas alternativas evitando la congestión]vehícular, "**hay que pensar diferente y a futuro, y generar  incentivos  que permitan la migración a carros eléctricos,** mediante la eliminación de impuestos como el iva o el recaudo anual", así mismo Prieto agregó que al reducir[  la emisión de contaminación vehicular, se disminuyen los costos del sistema de salud de enfermedades respiratorias, fondo que podría destinarse a la movilidad.]

> *["es terrorífico pensar que en la búsqueda de métodos de financiamiento para el transporte público, se deba deteriorar la calidad de vida de las personas contaminando la ciudad", Camilo Prieto]*

### ¿Cuál será el futuro del pico y placa con la nueva alcaldesa?

[Dentro del plan de gobierno de la alcaldesa de Bogotá para el periodo 2020- 2023 se resalta ampliamente las políticas ambientales entre ellas la movilidad limpia, por eso **la esperanza de los ambientalistas está en que tras su posicionamiento en enero del próximo año decretos como este sean eliminados**, "el programa ambiental de Claudia López era el mejor estructurado de todos los candidatos, pensando para la descontaminación del río Bogotá, promover la movilidad limpia y generar un mejor ordenamiento que respete la estructura ecológica de nuestra ciudad", resaltó Prieto. ]

Este decreto del pago voluntario para la suspensión de la restricción de movilidad en vehículos públicos, **fue presentada en el 2016  por Enrique Peñalosa** en el Plan Distrital de Desarrollo (art. 73), medida que fue archivada tras opciones de sectores ambientales, sin embargo fue revivida gracias al **Plan Nacional de Desarrollo** del presidente Ivan Duque. (Le pude interesar:[POT de Peñalosa satisface intereses económicos y no las necesidades de Bogotá](https://archivo.contagioradio.com/pot-satisface-intereses-economicos/))

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44016659" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44016659_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
