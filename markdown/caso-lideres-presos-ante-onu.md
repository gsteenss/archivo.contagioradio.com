Title: Caso de líderes ambientales que están en la cárcel llega a la ONU
Date: 2019-09-17 13:25
Author: CtgAdm
Category: Ambiente, Líderes sociales
Tags: ambientalistas, Ambiente, Casanare, Fiscalía, Petroleras, territorio
Slug: caso-lideres-presos-ante-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/San_Luis_de_Palenque_01082.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/lideres-petroleo.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

Este 16 de septiembre, se presentó ante el Grupo del Trabajo sobre la Detención Arbitraria de Naciones Unidas **el caso de ocho líderes ambientales de San Luís de Palenque, Casanare; que fueron capturados el pasado 27 de diciembre de 2017, luego de participar en protestas y denuncias públicas en contra de la empresa de exploración** y producción de gas natural y petróleo, Frontera Energy. (Le puede interesar: [Concesiones petroleras amenazan a 81 resguardos en Amazonía](https://archivo.contagioradio.com/concesiones-petroleras-amenazan-a-81-resguardos-en-amazonia/))

Ferney Salcedo, Yulivel Leal, Jesús Leal Salcedo, Carmen Iraida Salcedo, Miguel Ángel Rincón, Josué Eliecer Rincón, María Teresa Rincón y Salcedo Betancourt, **son los líderes comunitarios y ambientales pertenecientes al mismo núcleo familiar**, acusados por concierto para delinquir, violencia contra servidor público y obstrucción de vías públicas, entre otros delitos, sin que de acuerdo con los abogados defensores se tenga evidencia de estos mismos hechos.

Previo a esta situación, los 8 líderes se encontraban realizando varios denuncias contra la petrolera Energy, una de las más importantes tiene que ver  con la exigencia de una compensación ambiental del 1% de las ganancias de la compañía por la **captación de agua para uso petrolero, deterioro de las vías debido al paso de maquinaria y el pago de la deuda a personas de la comunidad que prestaron servicios de transporte, alimentación y hospedaje a trabajadores de la multinacional. **

Por su parte, la Fiscalía argumentó que estas ocho personas conformaron en 2016 un grupo delictivo organizado que tiene como “fachada la protesta social”. Además, se les acusa de ser los responsables de lesiones ocasionadas a un agente del Escuadrón Movil Antidisturbios (ESMAD) en una protesta, frente a estos cargos la Fiscalía y la Fuerza Pública, han limitado su actuación y no han presentado pruebas que relacionen a estas personas con actividades ilegales.

Según Fabián Laverde, director de COSPACC  en  Casanare, este accionar “**envía un mensaje de miedo, de que todo el que se movilice será procesado**, todo el que se mueva en términos de denuncia social lo vinculan con concierto para delinquir”. (Le puede interesar:[Petróleo por vida, los daños ambientales en la Ciénaga de Palagua](https://archivo.contagioradio.com/petroleo-contaminacion-palagua/))

### El caso de los líderes, una disputa de influencias

El operativo en contra de los líderes contó con más de 200 hombres, entre miembros de la Policía y el Ejército Nacional, dos Helicópteros y la Fiscalía, **“luego de indagar, descubrimos que todo este show mediático en la captura, se da tras la firma de un convenio entre el Ejército y la empresa Energy**, por la suma de \$2.152 millones de pesos , y tres días después de la captura celebran otro contrato, sumando así casi \$4.500 millones de pesos, esto explica porque la Fuerza Pública en especial del Casanare se han convertido en el fortín de seguridad privado de las petroleras”.

> ***“cada vez que arrebatan un líder social a su comunidad duele al interior de las organizaciones sociales, pero, por otro lado, si el mensaje es de tratar de delincuente a quienes protegen el territorio, esto lo que hace es motivar la protesta, y la denuncia por parte de la comunidad” - Fabián Laverde ***

Las organizaciones que presentaron la denuncia en la ONU , resaltaron  las violaciones al debido proceso que se han presentado a partir de un uso indebido del derecho penal, la violación de la libertad personal, el presupuesto de peligrosidad de la labor de defensa de los derechos humanos, al igual que los comprobantes que implican a la empresa canadiense Frontera Energy con estas violaciones.

> *** “esta denuncia es una acción que pocas veces existe la posibilidad de presentarse, y nos satisface que esto ya es de conocimiento de Naciones Unidas, pero también es una acción que puede traer repercusiones, porque muchas veces es más fácil denunciar a un funcionario público que a estas grandes empresas”- Fabián Laverde***

El proceso de liberación de los ocho líderes continúa, al igual que las acciones de la petrolera en este territorio, esta denuncia  hecha ante la ONU por el Comité de Solidaridad de Presos Políticos (CSPP) y la Corporación Social para la Asesoría y Capacitación Comunitaria (COSPACC), es el llamado que hacen para que la voz de los líderes y todos aquellos que exigen el cese inmediato de las actividades de  Frotera Energy, no sea callada y al contrario se puedan hacer publicas todas las afectaciones ambientales y sociales que ha generado es multinacional.

(Le puede interesar: [Ambientalistas denuncian desacato de multinacionales e instituciones sobre el uso de Fracking)](https://archivo.contagioradio.com/ambientalistas-denuncian-desacato-de-multinacionales-e-instituciones-sobre-el-uso-de-fracking/)

   
<iframe id="audio_41571809" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_41571809_4_1.html?c1=ff6600"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
