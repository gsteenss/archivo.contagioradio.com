Title: Colombia es el segundo país más peligroso para defensores del ambiente
Date: 2017-07-13 13:44
Category: DDHH, Entrevistas
Tags: Asesinatos, defensores ambientalistas, Global Witness, Informe Global Witness
Slug: en-colombia-fueron-asesinados-37-ambientalistas-en-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/global-witness-e1499968280737.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Global Witness] 

###### [13 Jul 2017] 

En el informe “Defender la Tierra”, publicado este mes, la Organización Global Witness alertó sobre la grave situación en la que se encuentran las y los defensores de tierra y medio ambiente en el mundo. Colombia, según la Organización, **es el segundo país con el mayor número de asesinatos de defensores del ambiente y la tierra** después de Brasil.

Según el Informe, **en 2016 fueron asesinados en Colombia 37 defensores alcanzando un máximo histórico,** pues hubo un aumento del 40% de los crímenes con respecto al año anterior. ** **Para Billy Kyte, miembro de Global Witness, “los asesinatos en ese país ocurrieron en el contexto del proceso de paz donde las comunidades y las víctimas están volviendo a sus territorios”.

El informe detalla que “**las áreas que estaban bajo control guerrillero ahora son observadas con codicia por compañías extractivistas y paramilitares,** mientras que las comunidades desplazadas que regresan son atacadas por recuperar tierras que les fueron robadas durante medio siglo de conflicto”. (Le puede interesar: ["Seis termoeléctricas afectarían 5 ecosistemas de Boyacá"](https://archivo.contagioradio.com/habitantes-de-boavita-en-boyaca-preocupados-por-proyecto-minero-extractivo/))

De igual forma, Kyte manifestó que, en el país, **“la mayoría de los casos no tienen procesos judiciales exitosos contra los responsables y hay impunidad total”**. Adicionalmente, según él, de los asesinatos cometidos contras los defensores ambientales en Colombia, “22 muertes fueron articuladas por paramilitares, además hay un vacío de poder donde están los recursos naturales y  por último las víctimas del conflicto son la población más afectada y que menos posibilidades tiene para acceder a la justicia”.

### **Latinoamérica es la región donde ocurren el mayor número de asesinatos a defensores de tierra** 

El informe global caracteriza a América Latina como la región donde ocurren la mayor cantidad de asesinatos. Para Kyte “hay pautas en indicios de que los **crímenes son cometidos por intereses económicos y políticos de las empresas** que trabajan en el sector minero por ejemplo”. De hecho, Global Witness destacó que “el sector minero es el más peligroso cuando las personas impiden que se desarrollen estos proyectos”. (Le puede interesar: ["Defensores de Derechos Humanos contra las cuerdas"](https://archivo.contagioradio.com/disminuyeron-las-amenazas-pero-aumentaron-los-asesinatos-somos-defensores/))

Adicionalmente, manifestó que los indígenas son los más afectados y en muchos casos **“las comunidades no son consultadas sobre los procesos que las empresas van a desarrollar** y ocurren crímenes como el de Berta Cáceres en Honduras”. Con este caso, Kyte puso de manifiesto que las personas “se enfrentan contra los intereses de las grandes empresas que entran a sus territorios a realizar proyectos con intereses económicos que dañan los ecosistemas y son asesinadas”.

### **Recomendaciones de Global Witness** 

Como gran recomendación, Kyte manifestó que **“los gobiernos deben proteger a estas personas que son las que están defendiendo el planeta y el medio ambiente**, ellos no pueden seguir siendo presentados como criminales una vez que son asesinados”.

De igual manera, hizo un llamado a que **“los Estados cumplan con sus obligaciones y consulten a las comunidades antes de realizar los proyectos**, deben garantizar la seguridad de los defensores y procesar a los responsables”. (Le puede interesar: ["185 defensores de derechos humanos han sido asesinados en el último año"](https://archivo.contagioradio.com/185-defensores-de-derechos-humanos-han-sido-asesinados/))

Para el caso de Colombia, Kyte manifestó que “el mecanismo de protección que ya existe debe tener más recursos para garantizar la seguridad”. Igualmente, “el Estado debe terminar con la impunidad en las investigaciones y aceptar que **las comunidades tienen el derecho de rechazar** y decirle no a la realización de este tipo de proyectos”.

\[embed\]https://www.youtube.com/watch?v=XuJVXpTxOFs\[/embed\]

###### [Video: Global Witness] 

<iframe id="audio_19786804" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19786804_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
