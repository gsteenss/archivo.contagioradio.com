Title: Hueco fiscal se lo invento el ministro de Hacienda: Eduardo Sarmiento
Date: 2018-11-27 15:13
Author: AdminContagio
Category: Economía, Nacional
Tags: Canasta familiar, iva, Ley de Financiación, Presupuesto General de la Nación
Slug: hueco-fiscal-hacienda
Status: published

###### [Foto: @IvanDuque] 

###### [27 Nov 2018] 

Gobierno y ponentes de Ley de Financiamiento llegaron a un acuerdo preliminar que permitiría recaudar los **\$13,7 billones, para cubrir casi la totalidad del déficit presupuestal del país**, a través de propuestas y ajustes que a pesar de no verse reflejados en el IVA de la canasta familiar, son conductos parecidos que afectarían el bolsillo de los colombianos.

Dentro de las propuestas planteadas por el Congreso el pasado 26 de noviembre, surgen iniciativas como los impuestos al dividendo y al patrimonio, la normalización de los bienes de los colombianos en el exterior, la modificación del IVA a cervezas y bebidas azucaradas y un aumento en la tarifa de renta a las personas con ingreso superiores a los cinco millones de pesos que harían posible tapar el hueco del presupuesto nacional.

### **La reforma, una colcha de retazos**

Para el economista Eduardo Sarmiento, la Ley de Financiamiento en realidad “cambió en la forma, pero sigue teniendo la misma falla inicial” al hacer una modificación que baja los gravámenes a las empresas de manera significativa y lo compensa "a través de impuestos al consumo de los estratos medios y bajos". (Le puede interesar: ["Aumento del IVA: Única propuesta del Gobierno que puso de acuerdo al Congreso"](https://archivo.contagioradio.com/aumento-iva/))

Según el experto, contrario a lo que sucede usualmente con las reformas tributarias en Colombia, las cuales son iniciativas impulsadas por el Gobierno y finalmente aprobadas, en esta ocasión la propuesta del ministro **Alberto Carrasquilla** de gravar con IVA “fue totalmente rechazada y la reforma pasó a manos de un "Congreso aterrado por el hueco fiscal  y a su vez con  grandes divergencias de opiniones”.

Para Sarmiento, las propuestas realizadas por el Congreso son una **"colcha de retazos"** para subir el IVA de forma disfrazada y de enmendar "el fracaso de un hueco fiscal que inventó el ministro de Hacienda al adoptar un gasto excesivo con relación al gasto del año anterior" cuando según Sarmiento debió reconocer que tal cifra no era tan alta y aclarar de cuánto era realmente el déficit.

El economista concluye que **el cambio más drástico de esta reforma se dará en la devolución del IVA por parte de las empresas,** las cuales al tener que pagar un costo adicional, “tratarán de compensarla en un alza de precios para los consumidores", una medida que en su criterio no se sabe si recaudará el dinero necesario o si finalmente resultará viable.

### **Gobierno y Congreso a buscar más soluciones** 

Frente a otra de las propuesta del Gobierno de recaudar 6 billones de pesos al reducir el descuento en el impuesto al valor agregado, del 100% al 90%, **la Comunidad Andina de Naciones**, organismo constituido por Bolivia, Colombia, Ecuador y Perú, y que busca el desarrollo económico, social y cultural a través de la integridad de naciones, le salió al paso y explicó que un acuerdo firmado entre tales países le impediría modificar dicho impuesto.

La organización clarificó que según lo firmado por Colombia en la  Decisión 599 de la Comunidad Andina en su artículo 26 se estableció que "el crédito fiscal originado en la adquisición de bienes o utilización de servicios destinados en su totalidad a operaciones gravadas a la tasa general y a operaciones sujetas a régimen de tasa cero, será descontable en un 100%"  lo que impide al Gobierno realizar modificaciones a dicho impuesto y a seguir buscando nuevos recursos.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
