Title: La conducta guerrerista del senador Uribe
Date: 2019-04-08 20:53
Author: CtgAdm
Category: Nacional, Paz
Tags: entrevista, masacre, Paramilitar, uribe
Slug: conducta-guerrerista-senador-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Tuit-de-ÁUV-1.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/alvaro_uribe_2_0.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/OTRA-MIRADA-12.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/OTRA-MIRADA-13.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/OTRA-MIRADA-14.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El pasado domingo 7 de abril el senador y expresidente Álvaro Uribe Vélez, tuiteo que si la autoridad del Estado requería hacer masacres era porque la protesta social además de violenta, generaba "terror". La afirmación generó revuelo por el sentido mismo de la frase, que justifica el regreso de las masacres al país, así como por la respuesta que tuvo por parte de usuarios y las implicaciones que tendrían para el país este tipo de declaraciones.

A propósito del trino, Contagio Radio habló con el director de la Fundación Paz y Reconciliación, León Valencia Agudelo.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Tuit-de-ÁUV-1-300x159.png){.wp-image-64355 .aligncenter width="411" height="218"}

### **Contagio Radio: ¿Qué opina sobre la declaración del Senador respecto a las Masacres?** 

León Valencia: Esta declaración es muy preocupante y hace parte de una conducta del expresidente Uribe: anteriormente  justificó la existencia del paramilitarismo, diciendo que la guerrilla había atacado a las ganaderos, empresarios del campo... y que ellos se habían visto obligados a defenderse; y le daba un manto de apoyo a este fenómeno paramilitar con declaraciones que fueron reiteradas durante muchos años y nadie objeto esto.

> En esa misma linea está la declaración, no justificando el paramilitarismo, sino sobre una posible agresión de la fuerza pública y la posibilidad de masacres, y dice él, que serían motivadas por la acción de protesta y las infiltraciones de actores ilegales en la protesta. Pero incluso más grave es que no haya una ola de indignación en el país, que no haya una acción de la justicia porque la magnitud de esto es enorme.

El conjunto de las fuerzas del país nadie reacciona a eso, y Uribe lo dice con tranquilidad, porque incluso algunos sectores lo van a aplaudir. Eso duele, duele mucho. (Le puede interesar: ["Magistrada Lombana debería hacerse a un lado en investigación sobre Uribe: Defensa de Cepeda"](https://archivo.contagioradio.com/magistrada-lombana-deberia-hacerse-lado-investigacion-uribe-defensa-cepeda/))

### **CR: ¿No sería esta una oportunidad para que la justicia actuara contra Uribe?** 

LV: Hay muchas cosas que él dice y muestran que no son hechos aislados, o errores humanos sino que hay una justificación de las masacres y una linea de pensamiento de Uribe que siempre ha combinado las formas de lucha: El Estado y las manifestaciones de ilegalidad. Primero, impulsando las Convivir, porque sabemos que cuando las Convivir perdieron la legalidad pasaron a ser grupos paramilitares.Eso Mancuso lo contó bastante bien.

Segundo, porque hubo 1.200 masacres en Colombia entre 1995 y 2005, en esa época paramilitar, documentadas por el Centro Nacional de Memoria Histórica, y hay algunas que lo vinculan a él en expedientes judiciales, no sus detractores, sino en expedientes judiciales. Entonces yo espero que en algún momento de la historia del país actúe la justicia y Uribe tendrá que responder al país por estas cosas.

### **CR: ¿Qué acción judicial se podría emprender contra Uribe por su tweet?** 

LV: Eso es incitación al terrorismo, porque si algo genera terror es una masacre. El terrorismo usa las masacres en todos lados porque es el signo del miedo, y pasa en Estados Unidos o por acción de gente totalmente desequilibrada, porque eso se usa para expandir el miedo en una sociedad, y lo utilizaron enormemente en Colombia para quedarse con las tierras: cada que pasaba una masacre la gente salía corriendo, la consecuencia de eso es el desplazamiento, por eso llegamos a 6 millones de desplazados.

Entonces en principio esto es incitación al terrorismo, y es grave porque aquí siempre se ha vinculado el terrorismo a las manifestaciones de izquierda guerrillera, que sin duda han hecho cosas muy terribles y no tienen justificación alguna, pero no se vincula el terrorismo a la acción de grupos políticos de la derecha también. En este caso es bastante demostrable que hay ahí una acción de terror para la gente, porque para eso se hacen esas manifestaciones: para infundir terror en la sociedad.

> Art 102 Apología del genocidio.  
> "El que por cualquier medio difunda ideas o doctrinas que propicien o justifiquen las conductas constitutivas de genocidio, o pretendan la rehabilitación de regímenes o instituciones que amparen dichas prácticas incurrirá en prisión de 6 a 10 años" [pic.twitter.com/eL0M26g0UP](https://t.co/eL0M26g0UP)
>
> — Sergio Fernández (@SergioFerG) [7 de abril de 2019](https://twitter.com/SergioFerG/status/1114999854143606786?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **CR: ¿Se están alentando a grupos armados para cometer masacres?** 

LV: Brian Winter, el escritor fantasma de Uribe -quién escribió sobre su vida- dijo en una revista americana: Si me preguntaran, si hay que temerle mucho a la llegada de un presidente ligado a Uribe al poder, yo no temería por Uribe pero sí por sus seguidores que van a sentir que llegaron nuevamente al poder y pueden hacer cualquier cosa en Colombia. Si ese escritor que tiene una relación entrañable con Uribe dice que puede ocurrir que mucha gente se sienta empoderada, imagínese qué tanto de realidad puede tener eso.

Yo sí creo que una incitación de esas tiene unas consecuencias enormes, porque los seguidores duros de Uribe son de todas las calidades: hay gente decente, honorable que cree en sus ideas y todo eso; pero hay gente también que ha estado en actividades muy diversas que se siente empoderada por eso y que está dispuesta a cometer delitos en la institucionalidad y por fuera de ella. Entonces es un mensaje muy malo y negativo.

No es lo mismo que lo diga una persona sin ninguna influencia en la sociedad colombiana, en redes sociales se dicen cosas horribles, pero un dirigente de la talla de Uribe si dice eso sí tiene unas consecuencias en la sociedad. (Le puede interesar: ["Caso de falsos testigos abriría la puerta a nuevas investigaciones contra Uribe"](https://archivo.contagioradio.com/caso-de-falsos-testigos-abriria-la-puerta-a-nuevas-investigaciones-contra-uribe/))

### **CR: ¿Cómo se puede manejar esta situación?** 

> LV: Lo importante es la voz de la gente, pongo el ejemplo de fútbol: ¿Por qué los locales tienen ventaja sobre los visitantes? Porque los locales tienen a 40 mil personas gritándoles a favor de un equipo y eso amilana al equipo contrario. Si la sociedad colombiana se indignara, y pusiera una censura social al expresidente Uribe y a sus seguidores para que no digan estas cosas o se retracten, el Uribismo tendría contención.

Pero hay mucha cobardía, no se alza la voz contra estas cosas, hay un miedo generalizado a Uribe y los que decimos nos sometemos a todas las agresiones en redes. Y las personas que hablan contra él en radio y televisión merecen aprecio, porque él es una persona que genera miedo. (Le puede interesar: ["La relación entre Chiquita Brands, Álvaro Uribe y las Convivir"](https://archivo.contagioradio.com/la-relacion-entre-chiquita-brands-alvaro-uribe-y-las-convivir/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
