Title: “En 2014 hubo 131 agresiones directas contra la prensa en Colombia” FLIP
Date: 2015-02-09 18:26
Author: CtgAdm
Category: DDHH, infografia, Paz
Tags: Amenazas a periodistas, Andromeda, das, FLIP, Maria del Pilar Hurtado, Paramilitarismo, Periodistas
Slug: en-2014-hubo-131-agresiones-directas-contra-la-prensa-en-colombia-flip
Status: published

###### Foto: elpais.com.co 

##### [Pedro Vaca, Director de FLIP] 

#####  <iframe src="http://www.ivoox.com/player_ek_4058458_2_1.html?data=lZWimpmZfI6ZmKiak5iJd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMrmxsjh0dePqMafzcaYqLGtlIzExsnf0ZC6pcTVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

En el reciente informe “**60 años de espionaje a la prensa en Colombia**” la Federación para la libertad de prensa, **FLIP**, resalta que actualmente se siguen desarrollando operaciones de inteligencia contra las personas que ejercen la labor periodística, uno de los casos recientes señalados por la FLIP es el de la operación **“Andrómeda”** en la que se registra que por lo menos **30 periodistas son blanco de interceptaciones ilegales**.

Pedro Vaca, director de la FLIP, afirma que en este caso se quiere dar especial relevancia al tema de las interceptaciones puesto que muchas de ellas pueden quedar en la impunidad. Sin embarog, resalta que es probable que se esclarezcan las responsabilidades de funcionarios del Estado si **Maria del Pilar Hurtado** da luces sobre esas mismas responsabilidades.

Sin embargo las interceptaciones ilegales no son la única manera de agredir a los periodistas. La FLIP documenta que durante el **2014 se presentaron 131 agresiones directas en contra de 164 periodistas**, dentro de las cuales se referencian **60 amenazas contra 71 personas**, todas ellas relacionadas con el ejercicio periodístico, bien sea por publicaciones o por investigaciones en curso.

Respecto a las amenazas, Pedro Vaca, afirma que es preocupante que este tipo de agresiones no estén en concordancia con los esquemas de protección que reciben los afectados o afectadas, para ello es necesario que la **Unidad Nacional de Protección**, UNP, responda de manera más eficaz, oportuna y a**decuada a las denuncias y a los niveles de riesgo.**

Por último, el informe señala que “la **pauta publicitaria sigue siendo un tema preocupante para la libertad de expresión**” pues muchas veces los medios regionales, locales o nacionales se ven afectados por la dependencia de la pauta, tanto a nivel público como a nivel privado.

[![libertad-de-prensa-2014-colombia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/02/libertad-de-prensa-2014-colombia.jpg){.aligncenter .wp-image-4576 .size-full width="627" height="807"}](https://archivo.contagioradio.com/ddhh/en-2014-hubo-131-agresiones-directas-contra-la-prensa-en-colombia-flip/attachment/libertad-de-prensa-2014-colombia/)

En cuanto a los retos que enfrentan los periodistas para este 2015, Vaca señala que algunas de las principales tareas tendrán que ver con la aplicación de los eventuales acuerdos de paz a los que se llegue en las **conversaciones con la guerrilla de las FARC y del ELN**. Otra línea estaría orientada a que desde el propio Estado y el congreso de la república se establezcan leyes que permitan que los y las periodistas ejerzan su profesión con las garantías de imparcialidad y sin la dependencia de la financiación pública o privada.

Cabe resaltar que respecto a informes anteriores, la situación de los periodistas en Colombia sigue siendo preocupante puesto que las agresiones han permanecido constantes. El informe de Reporteros Sin Fronteras ubica a **Colombia en el puesto 113 de 180 países en cuanto a garantías  a la prensa.**
