Title: "El principito" para los adultos que olvidaron que un día fueron niños
Date: 2015-11-05 13:12
Author: AdminContagio
Category: 24 Cuadros
Tags: Antoine de Saint-Exupéry, Bogota International Film Festival, El principito, El principito película animada, Mark Osborne
Slug: el-principito-en-las-salas-de-cine
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/el-principito-cover-e1446741967567.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [5 nov 2015]

Tras llevarse el premio del público en el Bogotá International Film Festival, se estrena en las salas colombianas "El principito", animación digital inspirada en el libro del escritor y aviador francés Antoine de Saint-Exupéry, publicado en 1943 y traducido a más de 250 idiomas.

La película que tuvo su premier en la más reciente edición del Festival de cine de Cannes, es dirigida por Mark Osborne, nominado en dos oportunidades al premio Oscar por el cortometraje animado "More" de 1999 y diez años después por la comercialmente exitosa "Kung Fu Panda".

La historia que podrán ver desde hoy los espectadores, combina de manera creativa diferentes técnicas de animación 3d y stop motion, e incluye elementos y personajes nuevos  que interactuán con el universo creado por de Saint-Exupéry en su  texto original gracias a la adaptación realizada por Irena Brignull.

Con las voces de actores y actrices como Rachel McAdams, Benicio Del Toro, Jeff Bridges, Paul Giamatti  y James Franco "El principito" producida por la compañía "On Animation Studio", fundada por los franceses Aton Soumache, Dimitri Rassam y Alexis Vonard, quienes en 2009 recibieron la aprobación para realizar la primera versión animada del libro.

Una cinta para toda la familia, cargada de mensajes para los adultos que han olvidado que un día fueron niños y quieren convertir a sus hijos prematuramente en adultos, ejemplificados en la pequeña niña y su madre, personajes creados para tal fin en la adaptación cinematográfica.

<iframe src="https://www.youtube.com/embed/ptve5e5IreY" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Sinopsis**

Para que todo salga bien en la prestigiosa Academia Werth, la pequeña y su mamá se mudan a una casa nueva. La pequeña es muy seria y madura para su edad y planea estudiar durante las vacaciones siguiendo un estricto programa organizado por su madre, pero sus planes son perturbados por un vecino excéntrico y generoso. Él le enseña un mundo extraordinario en donde todo es posible. Un mundo en el que el Aviador se topó alguna vez con el misterioso Principito.
