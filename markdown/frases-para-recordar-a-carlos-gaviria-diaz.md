Title: Frases para recordar a Carlos Gaviria Díaz
Date: 2016-03-31 12:40
Category: Nacional, Política
Tags: Carlos Gaviria Díaz, Frases Carlos Gaviria Díaz, Polo Democrático Alternativo
Slug: frases-para-recordar-a-carlos-gaviria-diaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/carlos-gaviria-app.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 31 Mar 2017

Se cumplen dos años del fallecimiento del pedagogo, abogado y líder político Carlos Gaviria Díaz, su compromiso por la defensa de los menos favorecidos en el país vive en sus enseñanzas y en el registro existente de sus palabras, vigentes y relevantes para la construcción de una paz democrática.

*“A mí me gusta es la educación y la libertad y a él (el presidente Uribe) le gusta la cárcel”.*

*"En un Estado de derecho a nadie se le puede privar de su libertad porque se fume un pucho de marihuana".*

*"Si la democracia es el gobierno de las mayorías, ¿cómo es posible que las mayorías estén desprotegidas y se encuentren en la pobreza o en la miseria?" *10 de abril de 2006, entrevista con la revista Semana*.*

*"Uno de las grandes obstáculos que ha encontrado la izquierda es la guerrilla, porque cuando uno dice que es de izquierda, lo vinculan con la lucha armada. Es muy importante que la gente se saque de la cabeza que toda propuesta de izquierda tiene que ver con el uso de las armas. Lo he dicho en todos los foros. Ni la ética que yo profeso ni las convicciones filosóficas que tengo son compatibles con la lucha armada".*10 de abril de 2006, entrevista con la revista Semana*.*

*"Que nos investiguen: a mí por nexos con la guerrilla, a Uribe por nexos con ‘paras', a ver quién sale mejor librado"* 30 de octubre de 2007, en respuesta a las palabras del Presidente Uribe en medio de la campaña para elecciones regionales de 2007*.*

*"Soy un defensor a ultranza de la autonomía universitaria" *Audiencia Publica "En Defensa de la Calidad de la Educación Superior", Senado de la República*.*

*"Yo no digo lo que digo por estar en la oposición, sino que estoy en la oposición porque creo en lo que digo"* 9 de mayo de 2006, Medellín*.*

*"Soy un liberal en el sentido más puro de la palabra"*
