Title: En Colombia el acoso sexual es una conducta naturalizada e invisibilizada
Date: 2016-01-28 13:42
Category: DDHH, Mujer
Tags: Astrid Helena Cristancho, Defensoría del Pueblo, Jorge Armando, violencia sexual
Slug: jorge-armando-otalora-no-representa-la-defensa-de-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Plantón-Otálora.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: SISMA MUJER ] 

<iframe src="http://www.ivoox.com/player_ek_10232489_2_1.html?data=kpWflZeYfJqhhpywj5WdaZS1lpyah5yncZOhhpywj5WRaZi3jpWah5yncavj08zSjabWscLixdSYsdmJh5SZopbZ0dfFb8%2FjjNfS0tfJt8bi1caYzsaPqMbaxtPgw5DIqYzg0NiYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Adelaida Palacios, Corporación Humanas] 

###### [28 Ene 2016 ]

El Defensor del Pueblo, Jorge Armando Otálora, en las últimas horas presentó su renuncia, atendiendo a la presión de diferentes organizaciones defensoras de los derechos de las mujeres frente a los **señalamientos por acoso sexual y laboral hechos por varios de sus funcionarios**. Por su parte la Procuraduría General decidió suspenderlo provisionalmente del cargo, mientras se adelanta la investigación disciplinaria.

De acuerdo con Adelaida Palacios, coordinadora jurídica de la ‘Corporación Humanas’, estas decisiones reconocen la exigencia que se ha hecho para que una institución como la Defensoría del Pueblo **no pueda estar a cargo de un funcionario sin legitimación** a la hora de representar los intereses de las mujeres. “Las mujeres que vivimos en un escenario de obstáculos para acceder a la justicia, **no nos sentimos representadas por una persona que atenta contra nuestros derechos**”, asegura.

Según afirma Palacios justificar el que un funcionario público permanezca en su cargo a pesar de graves acusaciones en su contra, reafirma que **en Colombia el acoso sexual es una conducta naturalizada e invisibilizada**, y perpetua los obstáculos en el acceso a la justicia enfrentados por las mujeres y los estereotipos que se activan cuando exigen la defensa de sus derechos. “**A Otálora le deben respetar su debido proceso pero no lo debe hacer desde el ejercicio de su cargo**, debe primar el significado que tiene la Defensoría del Pueblo como institución que se encarga de representar a hombres y mujeres”.

Palacios insiste en que este hecho es un llamado para reflexionar las formas en que la **institucionalidad colombiana tolera y naturaliza las violencias contra las mujeres**, y el lugar que ellas ocupan dentro de las instituciones, pues “pese a que existe una ley de cuotas, las mujeres seguimos estando por fuera de los espacios de decisión y de poder, se ha avanzado, pero todavía vemos que las mujeres seguimos muy **excluidas de los cargos de decisión** y que cuando vamos a participar **pesan sobre nosotras estereotipos y estigmatizaciones**”, agrega.

“**En un sentido simbólico y de reparación consideramos pertinente que la Defensoría sea asumida con un sentido de cambio**, estaríamos muy satisfechas si esta responsabilidad la asume una mujer, también en reconocimiento de que estos espacios nos han sido negados”, asevera Palacios en referencia a la petición que se está impulsando de que la terna para la elección del cargo directivo de la Defensoría del Pueblo esté compuesta por mujeres.

“**Necesitamos cambios estructurales para construir relaciones de género que reconozcan espacios igualitarios** en el escenario de lo público, así como dejar de considerar que los intereses que versan sobre los derechos de las mujeres están únicamente en lo privado. **Hay que exigir debate en los escenarios políticos sobre cómo queremos vivir y relacionarnos**”, afirma Palacios, concluye insistiendo en que es urgente consolidar una institucionalidad comprometida con el respeto de los derechos humanos.

En el marco de la **presentación de la denuncia penal por parte de Astrid Helena Cristancho** por el acoso sexual y laboral del que fue víctima, desde tempranas horas se llevó a cabo un platón frente al bunker de la Fiscalía, promovido por distintas organizaciones defensoras de los derechos de las mujeres para reconocer que esta es una problemática que las mujeres enfrentan”, ante la que **el Estado tiene la obligación de impartir justicia**, teniendo en cuenta que las mujeres no son las únicas obligadas a trabajar en la protección de sus derechos.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ] 
