Title: Con violencia no se acaba la violencia en la Comuna 13
Date: 2018-07-10 16:11
Category: DDHH, Nacional
Tags: Combos, Comuna 13, Derechos Humanos, Medellin
Slug: violencia-no-acaba-violencia-en-comuna-13
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/medellin-comuna-13.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [10 Jul 2018] 

En días recientes, los habitantes de la comuna 13 de Medellín, denunciaron enfrentamientos con armas de fuego entre los denominados 'combos' que operan en la zona. De acuerdo con una líder del lugar, la situación aún es crítica puesto que **la Alcaldía de la ciudad no ha generado un plan para atender estos hechos.**

<iframe src="https://co.ivoox.com/es/player_ek_26990721_2_1.html?data=k5umm5WbdpKhhpywj5WZaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5ynca2ZpJiSo6nIqdOfxM7ixsbIpc_VjMnSjdHFb6Tjztrbw5CVd4zYxpC6x8nJsM2ZpJiSo6nScYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

A pesar que el Secretario de Seguridad de la Ciudad, Andrés Felipe Tobón, ha señalado que no existen denuncias, la habitante de la Comuna asegura que no tienen garantías para denunciar puesto que la información **"se está filtrando de la policía a los combos y por eso hay amenazas de los combos a los habitantes".**

Según la residente de la zona, por este tipo de episodios se puede percibir el miedo de quienes residen en la comuna, por quedar en medio del fuego cruzado entre la fuerza pública y los grupos de crimen organizado.

Adicionalmente, la habitante de la Comuna 13 cree que hay un componente de estigmatización que sufren quienes residen en el sector, realacionada con el territorio bajo militarización, lo que para ella reproduce la idea de que "es una zona de guerra".

### **La Comuna está viviendo un nuevo ciclo de violencia** 

<iframe src="https://co.ivoox.com/es/player_ek_26990749_2_1.html?data=k5umm5WbeJqhhpywj5aVaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncari1crU1MbSuMafxcrZjaiJh5SZo5jay9nJb8XZjLLSz9TWrcKZk6iYpdTRuc_VjJagj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con un Integrante del Comité de Memoria de la Comuna 13 (quien pidió mantener la reserva sobre su nombre por temor a amenazas),  **se presentan situaciones cíclicas de violencia,** "que se repiten en el tiempo de forma recurrente, casi con características parecidas y con una respuesta estatal que sigue siendo la misma de siempre: la confrontación militar".

Los ciclos se producen por varias condiciones como la posición geográfica de la comuna,  ubicada en la ruta que une a la capital de Antioquia con la región Caribe; la situación histórica que vive la comunidad por ser una zona marginal; los efectos y dinámicas propias del microtráfico; y la estigmatización que sufren por cuenta de la respuesta militar con la que se combaten esas olas de violencia.

### **La operación Orión y los desaparecidos de la Escombrera** 

Uno de los hechos claves que señala el integrante del Comité de Memoria, es la reparación integral para las víctimas de la operación Orión. Durante esta acción militar, ocurrida entre los años 2002 y 2004,  se tomaron la comuna haciendo uso de la fuerza para finalmente consolidar el poder paramilitar en el sector, dejando un número cercano al centenar de desaparecidos, cuyos cuerpos podrían estar en la Escombrera.

Según testimonios de paramilitares que se acogieron a la Ley de Justicia y Paz, muchos de los desaparecidos están en la que podría ser la fosa común más grande del mundo; pero como lo afirma el miembro del Comité, la búsqueda de personas desaparecidas se detuvo sin razón aparente, causando una revictimización de los desaparecidos y sus familias. (Le puede interesar: [Así debería ser la búsqueda de personas desaparecidas en la Escombrera"](https://archivo.contagioradio.com/asi-deberia-ser-la-busqueda-de-personas-desaparecidas-en-la-escombrera/))

Sumado a esta exigencia de reparación, los habitantes de la Comuna 13 piden frenar la violencia de todas las partes, **que no se estigmatice el territorio,** que no hayan más políticas guerreristas que dejan es una estela de criminalidad reproducida en el tiempo y que no haya más revictimización contra los habitantes de la Comuna 13.

###  

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU) 
