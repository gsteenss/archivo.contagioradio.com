Title: Exigen presencia de Santos ante derrame de crudo la Lizama
Date: 2018-03-29 13:08
Category: Ambiente, Nacional
Tags: Barrancabermeja, Desastre ambiental, Ecopetrol, Juan Manuel Santos, La Lizama, ministro de Ambiente
Slug: ecopetrol-es-el-estado-en-el-magdalena-medio-oscar-sampayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Diseño-sin-título.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE / Correo del Orinoco] 

###### [28 Mar 2018] 

El desastre ambiental del pozo 158 de la Lizama en Santander, parece no tener fin, según denuncian las comunidades. De acuerdo con Óscar Sampayo, líder ambientalista y habitante de Barrancabermeja, en total serían tres los afloramientos de crudo los que siguen activos, aunque Ecopetrol asegura que la situación está controlada.

Según Sampayo, sobre la 1:00 de la mañana empezó la nueva emergencia que obligó la evacuación del personal debido a la presión del gas. Más tarde, el afloramiento que se encontraba activo desaparece, pero sobre las 8 de la mañana el afloramiento vuelve a aparecer, como dice Ecopetrol, a 60 cm del primero con el que empezó el desastre. Y luego, sobre las 3 de la tarde nuevamente es necesaria otra evacuación de las personas por un nuevo afloramiento.

### **Control  sobre afloramientos que ha anunciado Ecopetrol es débil** 

Aunque la empresa ha afirmado que tiene controlados el afloramiento inicial y los nuevos que han aparecido desde el martes, el líder ambientalista, señala que en total serían tres los afloramiento activos. "Ecopetrol dice que están controlados, pero es un control muy débil de los lodos. Hacen un canal y crean una piscina donde cae todo, pero **no sabemos como se está afectando el subsuelo y qué tipo de tóxicos se están generando.** Los daños en el sector son brutales", asevera Sampayo.

Afirman también que en la vereda Planta Nueva a donde se están llevando los lodos producidos por el desastre, no habría ningún tipo de control ambiental. Además,** las piscinas de Ecopetrol de recuperación de lodos "están al tope",** por lo que han tenido que llevar dichos residuos a Sabana Grande.

### **Ecopetrol impide ingreso de la institucionalidad** 

Según se ha podido conocer a través de diferentes medios y de la voz de la misma comunidad, la empresa petrolera ha impedido el ingreso de la institucionalidad al lugar donde están los afloramientos, incluso no le fue permitido el paso a la contralora municipal cuando este miércoles quiso verificar la situación.

"En Barrancabermeja la institucionalidad es totalmente débil. El alcalde se encuentra detenido y el gobernador no existe. **El poder de la industria extractiva se impone, Ecopetrol es el Estado del Magdalena Medio",** expresa Ócar Sampayo.

En ese sentido se explica las pocas respuestas que ha dado Ecopetrol a la comunidad, pues tras 27 días de la tragedia, aún no se tiene una respuesta de por qué sucedió tal hecho, y peor aún, cuánto tiempo se demoren en controlar el derramamiento de crudo. Lo único a lo que ha hecho referencia la empresa es al plan de contingencia, pero las personas afectadas señalan que necesitan una respuesta sobre qué fue lo que pasó.

"La quebrada la Lizama y el Caño muerto están totalmente afectados. Es la muerte y la destrucción, las palabras no alcanzan a describir lo miserable de la situación". Lo que más preocupa es la falta de caracterización del agua de los lodos del subsuelo que están totalmente contaminados. **"Han pasado 27 días y no hay caracterización de esas aguas, rogamos para que no llueva porque todo puede empeorar",** dice el habitante de Barrancabermeja.

### **Comunidad exige presencia del presidente Santos ** 

La comunidad y los ambientalistas coinciden en señalar que el gobierno ha sido completamente incompetente y blando con Ecopetrol. Exigen que el Ministro de Ambiente, Luis Gilberto Murillo se reúna con la comunidad, y también reclaman la presencia de Juan Manuel Santos. "**El presidente es el que asigna los bloques petroleros, y así como los asigna debe responsabilizarse por lo que ha pasado**", dice Sampayo.

En la mañana de este miércoles cerca de 200 personas se manifestaron frente a las instalaciones de Ecopetrol para exigir medidas eficaces y sanciones a la empresa ante el desastre ambiental ocasionado.

<iframe id="audio_24929754" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24929754_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
