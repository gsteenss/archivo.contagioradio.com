Title: "Estamos más unidos que nunca para defender la paz ": Oposición
Date: 2019-03-13 16:17
Author: CtgAdm
Category: Paz, Política
Tags: Congreso, Goebertus, JEP, oposición, Réplica
Slug: estamos-mas-unidos-nunca-defender-la-paz-oposicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-2019-03-13-a-las-3.27.44-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Partido Alianza Verde] 

###### [13 Mar 2019] 

Este martes, y como respuesta a la alocución presidencial del pasado domingo con la que el presidente Iván Duque presentó seis objeciones al Proyecto de Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP), la bancada de oposición utilizó su derecho a réplica, y con la vocería de **Juanita Gobebertus**, expresó que estos reparos del Presidente dividen a la población y no son de carácter político (por inconvenientes), sino jurídico.

En el documento la Unión Patriótica, Mais, Polo, FARC, Colombia Humana y Alianza Verde señalan que las condiciones de Duque en realidad sí producen un 'choque de trenes', pues aunque fueron tildadas de "inconvenientes", en ningún momento se presentan los argumentos que sustenten económica, social o políticamente esa inconveniencia, mientras se concentra en argumentos jurídicos que ya fueron resueltos por la Corte Constitucional.

Adicionalmente, afirman que las objeciones dividen el país, lo devuelven a la fragmentación vivida en octubre de 2016, al tiempo que evitan el normal funcionamiento que debería tener la Jurisdicción de paz. (Le puede interesar: ["Estatuto de O.: Primer paso en largo camino para la participación"](https://archivo.contagioradio.com/estatuto-de-oposicion-un-primer-paso-en-largo-camino-para-la-participacion/))

### **"Estamos más unidos que nunca para defender la paz y la democracia"** 

Para el **representante a la cámara por la lista de Decentes David Racero**, el derecho a réplica ejercido por los partidos en oposición fue un hecho histórico, posibilitado por el Estatuto de la Oposición, que era una necesidad evidenciada hace mucho tiempo pero que se hizo efectivo gracias al Acuerdo de Paz. (Le puede interesar: ["Seis argumentos cuestionan las objeciones de Duque a la JEP"](https://archivo.contagioradio.com/desmienten-objeciones-duque-jep/))

**"Lo bonito del derecho a réplica es que todos los partidos de oposición se pusieron de acuerdo y dieron la vocería a Juanita Goebertus, diciendo que estamos más unidos que nunca para defender la paz y la democracia"**, sostiene Racero. (Le puede interesar:["Los retos a los que se enfrenta la bancada de O."](https://archivo.contagioradio.com/los-retos-a-los-que-se-enfrenta-la-bancada-de-oposicion/))

A este llamado para defender lo pactado en el Teatro Colón, también se sumó el Partido Liberal que, como la bancada de oposición, anunció que votaría negativamente las 6 objeciones presentadas por el Presidente, lo que garantizaría que en la Cámara de Representantes se rechacen los reparos presidenciales. En el Senado las cosas son diferentes, allí el Gobierno está moviendo fichas para lograr mayoría.

### **Los 3 escenarios de la estatutaria en el Congreso** 

El Congresista explica que Cámara y Senado deben discutir las objeciones separadamente, y si ambas corporaciones niegan las objeciones, el Proyecto de Ley Estatutaria de la JEP tendría que ser sancionada; en caso de que la decisión unánime fuera aprobar las objeciones, estas tendrían que ser resueltas por el Congreso, hacer tránsito por la Corte Constitucional y pasar luego a sanción presidencial.

El tercer escenario contempla que la Cámara niegue las objeciones y el Senado las apruebe, **en cuyo caso se hundiría el Proyecto.** Racero cree que "eso es lo que quiere el Presidente, porque ellos ya están prometiendo pasar un Proyecto de Acto Legislativo con la justicia mínima que quieren", lo que implicaría un enfoque punitivo y de castigo, y no enfocado en la verdad, la restauración y las víctimas como el que actualmente se está desarrollando.

Recientemente la periodista Darcy Quinn afirmó que el presidente Duque estaría intentando 'convencer' a algunos senadores de apoyarlo en las objeciones con el famoso método de la mermelada; razón por la que Racero argumenta que se necesita veeduría y presión ciudadana para que se nieguen las objeciones, al tiempo que resalta la movilización social como forma de apoyar la sanción de la Ley Estatutaria de la JEP.

[Documento respuesta de la oposición a objeciones de la JEP](https://www.scribd.com/document/401830496/Documento-respuesta-de-la-oposicio-n-a-objeciones-de-la-JEP#from_embed "View Documento respuesta de la oposición a objeciones de la JEP on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_4231" class="scribd_iframe_embed" title="Documento respuesta de la oposición a objeciones de la JEP" src="https://es.scribd.com/embeds/401830496/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-SO0JEYonpWXE9DDuIiGU&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
<iframe id="audio_33349427" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33349427_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
