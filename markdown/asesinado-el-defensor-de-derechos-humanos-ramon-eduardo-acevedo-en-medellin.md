Title: Convivir tendrían responsabilidad en el asesinato del defensor Ramón Eduardo Acevedo
Date: 2016-06-08 15:56
Category: DDHH, Nacional
Tags: Asesinatos defensores de derechos humanos, Comuna 10 Medellín, Ramón Eduardo Acevedo
Slug: asesinado-el-defensor-de-derechos-humanos-ramon-eduardo-acevedo-en-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/fotonoticia_20160607061702_1280.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Notiamerica ] 

###### [8 Junio 2016 ]

[El pasado lunes sobre las 3 de la tarde, un joven que se movilizaba en un vehículo por el barrio Chagualo, en la comuna 10 de Medellín, **asesinó con tres disparos al defensor de derechos humanos Ramón Eduardo Acevedo** cuando llegaba a su casa. Según analistas esta comuna es controlada por paramilitares de las Convivir y Acevedo había realizado un trabajo de investigación sobre los mecanismos de extorsión usados por el grupo armado en la zona, por el que había sido amenazado.  ]

Acevedo hacía parte de la Mesa de Derechos Humanos de la Comuna 10 y había fundado la Mesa del Valle de Aburra, en la que **adelantó sus investigaciones sobre las extorsiones de las Convivir**, que de acuerdo con un  grupo de estudios económicos de la Universidad de Antioquia, generaron utilidades de \$9 mil millones a las bandas criminales en la ciudad de Medellín durante 2015.

La Mesa de Derechos Humanos del Valle de Aburra en su más reciente comunicado, aseguró que pese a que el defensor había denunciado amenazas en su contra, nunca se le había otorgado seguridad. Por lo que exhortan a Fedérico Gutierrez, actual alcalde de Medellín a que convoque un C[onsejo de Seguridad en el que se discuta no sólo este caso, sino la **difícil situación que enfrentan los defensores de derechos humanos** contra quienes se cometieron más de 1500 agresiones y 155 homicidios entre 2010 y 2015. ]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
