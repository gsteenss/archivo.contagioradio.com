Title: Ferdinand un símbolo antitaurino que vuelve después de 80 años
Date: 2017-12-21 17:10
Author: AdminContagio
Category: 24 Cuadros, Animales
Tags: 20 century fox, Ferdinand, tauromaquia
Slug: ferdinand-pelicula-tauromaquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Ferdinand.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Blue Sky/Disney 

###### 22 Dic 2017 

Esta semana fue estrenada la película animada **"Olé, el viaje de Ferdinand"**, una producción de 20 Century Fox y los estudios Blue Sky (La era del hielo, Rio), que cuenta la historia de un toro de corazón grande, sensible que ama el aroma de las flores, que no esta dispuesto a ser tomado como un animal de lidia.

<iframe src="https://www.youtube.com/embed/hUfYGmVZXUc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El film esta inspirado en el cortometraje de 1938 **Ferdinand The bull (Ferdinando el Toro)** dirigido por Dick Rickard y producido por Walt Disney, a partir de **´El cuento de Ferdinando´** escrito por Mauro Leaf, que **obtuvo un premio Oscar** de la Academia un año después de su estreno.

La historia del Toro noble, f**ue censurada en su momento por el régimen de Franco**, quién lo consideraba "una sibilina maniobra judeomasónica a favor del pacifismo y de la República", concepto similar al que tuvo **Adolf Hitler para quemar en público varios de los ejemplares** del cuento en el que estaba inspirado.

Pese a que en países como los **Estados Unidos, el cuento fue considerado una exaltación del comunismo y del colectivismo**, activistas y líderes del talante de Gandhi, mostraron su simpatía con la historia, numerándolo como uno de sus textos favoritos. Tensiones que favorecieron sus ventas y atrayendo la atención del importante estudio de animación.

Con el paso del tiempo, se convirtió en un referente para los animalistas antitaurinos y algunas comunidades gay, que encontraban en su mensaje un claro referente contra los estereotipos machistas sobre el comportamiento masculino. Le puede interesar: [Centro Democrático busca volver patrimonio cultural las corridas de toros](https://archivo.contagioradio.com/centro_democratico_corridas_toros/).

<iframe src="https://www.youtube.com/embed/vgAI5VIpyxc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La nueva versión de la historia, dirigida por Carlos Saldanha, cuenta la aventura de Ferdinand por escapar de la Tauromaquia, un destino al que lo quieren obligar, y buscará volver con su familia tarea para la cuál contará con el apoyo de varios animales que serán en sus compañeros inseparables.

 
