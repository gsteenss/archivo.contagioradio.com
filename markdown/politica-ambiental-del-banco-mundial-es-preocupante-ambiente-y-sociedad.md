Title: "Política ambiental del Banco Mundial es preocupante" Ambiente y Sociedad
Date: 2015-02-03 20:12
Author: CtgAdm
Category: Ambiente, Economía
Tags: Ambiente, América Latina, Asociación Ambiente y sociedad, Banco Mundial, política ambiental
Slug: politica-ambiental-del-banco-mundial-es-preocupante-ambiente-y-sociedad
Status: published

##### Foto: [impactoambientales.wordpress.com]

La Asociación Ambiente y Sociedad, afirma que el borrador de la nueva política ambiental evidencia que “**la propuesta del Banco Mundial deja abierto el cumplimiento de los estándares y , no define claramente cuándo ni cómo se debe cumplir con lo establecido en la política”**, es por ello, que ven de gran importancia la participación de la sociedad civil en las consultas regionales que realiza el Banco Mundial para crear el nuevo  marco de salvaguardas Socio-ambientales, consulta que se realizará en Lima, Perú entre el 2 y 4 de febrero.

Para las organizaciones ambientales y defensoras de derechos humanos, es crucial el aporte que puede hacer la sociedad civil sobre el borrador del nuevo marco de salvaguardas socio – ambientales, debido a que es preocupante **la flexibilización de los requerimientos ambientales y sociales que pide la institución financiera a las empresas y Estados para financiar la construcción de obras en América Latina.**

Pese a que el Banco Mundial  hace un llamado a que representantes de la sociedad civil, organizaciones indígenas, empresas e instituciones del Estado hagan parte de esta consulta; para la asociación “**es preocupante el escaso dialogo y poca receptividad frente a los comentarios y observaciones realizadas** en previas oportunidades por parte de la sociedad civil”, según afirman en uno de sus comunicados.

Debido a lo anterior, **Ambiente y Sociedad junto a otras entidades están organizando actividades con el fin de promover espacios de discusión y diálogo** de los diferentes actores sociales que hacen parte de las dinámicas socio-ambientales.

La organización realizará dos talleres para poder concertar nuevas propuestas respecto al nuevo marco de salvaguardas socio-ambientales, con el objetivo de que la política ambiental final proteja los ecosistemas, las comunidades y los trabajadores que puedan verse afectados por los mega proyectos que se quieran desarrollar en los países en vías de desarrollo.
