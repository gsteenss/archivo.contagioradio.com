Title: Ante el Tribunal ético serán presentados casos de mujeres panamazónicas y andinas
Date: 2020-10-26 18:10
Author: AdminContagio
Category: Actualidad, Mujer, Nacional
Tags: Agresiones a mujeres indígenas, Foro social panamazónico, Panamazonía
Slug: ante-el-tribunal-etico-seran-presentados-casosde-mujeres-panamazonicas-y-andinas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/amazonas-e1478294687660.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Albaciudad

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este miércoles 28 de octubre se realizará el **II Tribunal ético de justicia y derechos de las mujeres panamazónicas y andinas**, en el que se presentarán casos de agresiones contra las mujeres de la panamazonía que defienden sus pueblos y territorios. (Le puede interesar: [167.559 familias afectadas por conflictos societerritoriales en la Panamazonía](https://archivo.contagioradio.com/167-559-familias-afectadas-por-conflictos-societerritoriales-en-la-panamazonia/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FospaOficial/status/1320399399357472769","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FospaOficial/status/1320399399357472769

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El Tribunal ético, es impulsado por diferentes organizaciones de mujeres en Latinoamérica que anteriormente realizaron dos webinares donde abordaban temas como el derecho de la mujer indigenas, y la presentación y análisis de varios casos en materia de vulneraciones a sus derechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante este tribunal internacional se presentarán emblemáticos casos de agresiones contra las mujeres de diversos países Panamazónicos, en el marco de la defensa que hacen de sus territorios y sus pueblos.(Le puede interesar: [Resistencia de comunidades indígenas durante conflicto armado](https://archivo.contagioradio.com/resistencia-de-comunidades-indigenas-durante-conflicto-armado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El encuentro «será precedido por reconocidas juezas», quienes emitirán un veredicto que será presentado en el IX Encuentro Internacional virtual del [Foro Social Panamazónico (FOSPA)](http://www.forosocialpanamazonico.com/ya-llega-ii-tribunal-etico-de-justicia-y-derechos-de-las-mujeres-panamazonicas-andinas/)que se llevara a cabo del 12 al 15 de noviembre.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese al contexto producto de la pandemia, el Proceso de Foro Social Panamázonico (FOSPA), **se ha venido fortaleciendo migrando a la virtualidad, con la campaña FOSPA en movimiento, un espacio en donde miércoles se presenta un conversatorio en el que se abordan las realidades amazónicas y las distintas acciones que comunidades y procesos sociales** de los 9 países de la cuenca, adelantan para hacer frente a las distintas afectaciones que padece este importante territorio.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FospaOficial/status/1313225466891952130","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FospaOficial/status/1313225466891952130

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
