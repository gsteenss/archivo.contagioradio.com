Title: Inician campaña de objeción fiscal al gasto militar en España 2015
Date: 2015-05-12 16:58
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Berta Iglesia, Desobediencia civil activa contra gasto militar, Ecologistas en Acción, M.O.C., Objeción fiscal al gasto militar España
Slug: inician-campana-de-objecion-fiscal-al-gasto-militar-en-espana-2015
Status: published

###### Foto:Canariasporlapaz.blogspot.com 

###### Entrevista con [**Berta Iglesias**], activista de la campaña y miembro de Ecologistas en Acción: 

<iframe src="http://www.ivoox.com/player_ek_4484334_2_1.html?data=lZmllpiXeI6ZmKiak5WJd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Lh0caSpZiJhpLVjNTPzMrHrYa3lIqvldOPpc2fyMbg1tSPscrgytnO1JCpt9HVhqigh6eVpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La campaña por la objeción al gasto militar consiste en la **desobediencia civil, social pacífica y activa** en colaborar en los **gastos que conllevan la guerra y sus preparativos.** Se trata de decir "con mi dinero no se financian las guerras".

Para ello se aprovecha la **declaración de la renta** o IRPF, donde se declaran los impuestos y bienes privados anualmente al estado. Cuando la persona realiza la declaración **destinando una parte de los impuestos a un proyecto solidario** a través de políticas de paz y de respeto a los DDHH.

Cualquier **persona puede objetar aunque no tenga que realizar la declaración de la renta** obligatoriamente, independientemente se tenga nómina o no cada persona paga cientos de impuestos al año al comprar bienes de consumo, puedes exigirle al estado que te devuelva lo que ha gastado en el presupuesto militar.

Para ello las webs del ministerio de defensa publican los **datos del gasto militar que para 2014 según Berta Iglesias, activista de Ecologistas en Acción**, una de las organizaciones sociales que participan en la campaña, ha sido de **41.393,28 millones de euros,** que dividiéndolo entre los habitantes censados equivale a **890,87 euros por cada uno**.

Cuando reclamas el dinero después lo desvías a otros proyectos solidarios de tal forma **objetas de participar en contribuir fiscalmente** con el gasto referido a lo militar.

La campaña está enmarcada en la **filosofía de la lucha No Violenta**, que para Berta es”… legitimidad, coherencia entre medios y fines, horizontalidad y respeto máximo a la persona humana sea cual sea el rol social que desempeñe...”.

La **Desobediencia civil** activa es una **herramienta de la lucha no violenta** que consiste en desobedecer públicamente una ley que se considera injusta o no ética.

Este tipo de campaña han funcionado en **otros países como Alemania, Holanda, Canadá, EEUU o Italia** donde se llevan realizando durante años.

Para Berta Iglesias el principal problema de este tipo de campañas es la **dificultad a la hora de hacer seguimiento sobre cuántas personas lo hacen,** puesto que para ello es necesario que se inscriban en la web a parte de realizar la acción.

En todo caso el **objetivo final es terminar con el ejercicio militar y todo lo que conlleva**, como la investigación, el soporte o el propio ejército.
