Title: La deuda de Colombia con la población LGBTI
Date: 2018-05-17 14:19
Category: DDHH, Nacional
Tags: colombia, Colombia Diversa, derechos LGBTI, dia contra la homofobia, LGBTI
Slug: la-deuda-de-colombia-con-la-poblacion-lgbti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/LGBTI.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [17 May 2018] 

Al conmemorarse el día internacional contra la homofobia y la transfobia, Colombia ha avanzado en la construcción de política públicas que garantizan la protección de los derechos de la comunidad LGBTI. Sin embargo, estas políticas **no se han implementado** en su totalidad y aún hay barreras sociales que impiden el goce efectivo de sus derechos. Tan sólo en 2016 fueron asesinados 108 de sus integrantes.

### **Política Pública no ha sido implementada para defender a la población LGBTI** 

De acuerdo con Gustavo Pérez, coordinador del área de derechos humanos de Colombia Diversa, los reconocimientos legales, que están asociados, por ejemplo, a la unión entre parejas del mismo sexo, “no son garantizados en la práctica y no han sido suficientes para **erradicar la discriminación y la violencia contra las personas LGBTI**”. Dijo que “año tras año vemos cómo se siguen presentando los mismos números de hechos de violencia en muchos de los departamentos del país”.

En la práctica, la política pública “busca coordinar las acciones estatales a favor de la población LGBTI” para garantizar **el goce efectivo de sus derechos**. Aquí, entidades como la Defensoría del Pueblo, la Policía, la Fiscalía y las Gobernaciones deberían trabajar conjuntamente en ámbitos educativos, de salud, laborales o en materia de acceso a la justicia.

Sin embargo, los obstáculos que aún persisten en la sociedad y el lento actuar de las instituciones no han permitido que esta política sea implementada. Pérez manifestó que “aún persisten **los prejuicios de parte de la sociedad** en general y por parte de los funcionarios públicos”. (Le puede interesar:["En video quedó registrada agresión contra comunidad LGBTI en la Plaza de Bolívar"](https://archivo.contagioradio.com/video-agresion-comunidad-lgbti/))

**Reconocimiento de los derechos es una tarea de toda la sociedad**

Desde Colombia Diversa indicaron que el reconocimiento de los derechos “muchas personas lo ven como **si nos estuvieran otorgando derechos especiales**”. Además, los problemas de exclusión socio económica y discriminación en ámbitos laborales o educativos, aumentan los riesgos de violencia contra esta población.

Pérez enfatizó en que parte del problema radica en la cantidad de campañas de sensibilización con la sociedad que **carecen de mecanismos** para transformar la discriminación y los prejuicios. Indicó que “se debe pensar en cómo hacer para que las campañas sean efectivas y las personas se apropien de su contenido y se entienda que esto es un problema para toda la sociedad”.

### **Homicidios contra personas LGBTI son constantes en el tiempo** 

Desde Colombia Diversa han venido documentando las agresiones contra los integrantes de la comunidad LGBTI y han visto que las cifras de violencia han sido constantes en el tiempo. Afirmó que en los últimos años se ha reconocido **los derechos que les eran negados**, pero no existe una correlación con la disminución de la violencia. (Le puede interesar:["Cartilla Protegiendo la Diversidad de la Defensoría del Pueblo"](http://www.defensoria.gov.co/es/nube/destacados/7290/Consulte-y-descargue-aqu%C3%AD-la-cartilla-Protegiendo-la-Diversidad-documento-elaborado-por-la-Defensor%C3%ADa-del-Pueblo-con-apoyo-de-ACNUR-lgbti-DIVERSIDAD-acnur-Defensor%C3%ADa-del-Pueblo-derechos.htm))

Pérez Manifestó que sólo en 2016, se registraron 108 homicidios a nivel nacional y la cifra de 2017, que está siendo consolidada por Colombia Diversa, osila entre el mismo número. A esto se suma que la mayoría de los casos **continúan en la impunidad** pese a los esfuerzos de la Fiscalía por impulsar las investigaciones de los crímenes contra estas personas.

### **Grandes ciudades son los lugares más peligrosos para la población LGBTI** 

En los informes de la organización hay regiones en donde hay un mayor número de casos de violencia que son **Antioquia, Valle del Cauca y Bogotá**. Esto se debe en parte a que son los centros más poblados, pero hay casos de agresiones que se presenta en territorios donde hay presencia de grupos armados.

Finalmente, Pérez recordó que el día contra la homofobia y la transofbia debe ser visto “como una oportunidad **para generar una mayor conciencia** sobre los problemas que todavía quedan pendientes para la población LGBTI”. Esto en virtud de entender que la violencia y la discriminación son factores que están arraigados en la sociedad colombiana.

<iframe id="audio_26037280" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26037280_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
