Title: Las luchas de las prisioneras políticas
Date: 2018-03-30 14:33
Category: Expreso Libertad
Tags: colombia, Prisioneras políticas
Slug: las-luchas-las-prisioneras-politicas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/2511_dibujo_mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 27 Mar 2018

La falta de atención médica y  de conocimiento sobre métodos de reproducción y salud sexual, el trato despectivo de la guardia del INPEC frente a las necesidades de las mujeres, el aislamiento y las pocas oportunidades para tener una reincorporación social, son  algunas de las crueles situaciones que deben a travesar las prisioneras políticas en las cárceles de Colombia. Conozca en este programa del Expreso Libertad, las apuestas y los logros del movimiento de mujeres prisioneras políticas en el país, en defensa de sus derechos.

<iframe id="audio_24854930" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24854930_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
