Title: El balón del cese al fuego bilateral está en la cancha del ELN: De Currea
Date: 2018-01-06 20:15
Category: Paz
Tags: Cese al fuego bilateral, ELN, Juan Manuel Santos, Quito
Slug: cese-al-fuego-bilateral-eln
Status: published

###### [Foto: cablenoticias] 

###### [06 Ene 2018]

A tan solo 3 días del fin del cese al fuego bilateral firmado entre el gobierno y el ELN en el marco de las conversaciones de paz que se realizan en Quito y que se reanudarían este lunes, ya son 5 las comunicaciones en las que se pide la prórroga de la medida como la posibilidad de que dichas conversaciones se mantengan en un ambiente favorable.

Este sábado **cinco países acompañantes del proceso de paz, en una carta firmada por sus correspondientes embajadores en Colombia, señalaron que el cese al fuego temporal ha sido un alivio para poblaciones en muchas regiones del país** y que sería muy favorable a las conversaciones que se mantenga esa medida que también ha sido un alivio humanitario.

Los embajadores de Suecia, Suiza, Países Bajos, Italia y Alemania señalaron que no prorrogar el cese al fuego después del 9 de Enero podría representar un obstáculo para el avance de la participación política en el proceso de conversaciones.  “**Una reanudación de las hostilidades entre la Fuerza Pública y el ELN causaría un fuerte impacto humanitario** para la población civil e pondría a riesgo la participación activa de la sociedad civil en las negociaciones.”

Estos países que han hecho parte de llamado Grupo de Países de Apoyo, Acompañamiento y Cooperación a la Mesa de Conversaciones (GPAAC), aseguran que se unen a las voces de organizaciones y comunidades en las que se clama por una prórroga del Cese Bilateral y aseguran estar dispuestos a seguir contribuyendo en que se alcance un fin positivo en este proceso de paz. [Lea también: Cientos de personas piden prorroga del cese al fuego entre gobierno y ELN](https://archivo.contagioradio.com/cientos-de-personas-y-organizaciones-hacen-llamado-para-que-se-prolongue-el-cese-bilateral-entre-gobierno-y-eln/)

### **Cese al fuego bilateral podría ser definitivo para el proceso con el ELN** 

Por su parte, el presidente Juan Manuel Santos señaló que "Estamos más que dispuestos a prorrogar el cese al fuego con el Eln y a renegociar las condiciones de un nuevo cese" lo que abriría la puerta a hablar de las condiciones desfavorables al ese al fuego señaladas por el ELN en su comunicado al final del 2017. Hasta el momento todo está listo para que el próximo lunes se reinicien las conversaciones de paz con la llegada de las delegaciones de paz del gobierno y del ELN.

Para el analista Victor de Currea Lugo, el balón del cese bilaterial está en manos del ELN dado que el presidente Santos se habría manifestado en el sentido de renegociar algunas de las condiciones que el ELN valora como negativas del cese actual. En ese sentido **podría ser viable una prorroga corta del cese actual para darle tiempo a la nueva delegación del gobierno** y negociar las condiciones de un nuevo cese bilateral.

###### Reciba toda la información de Contagio Radio en [[su correo]

 
