Title: Oliverio Conejo, coordinador de salud indígena y su hija fueron asesinados en Cauca
Date: 2020-09-12 10:21
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: Asesinato de indígenas, Cauca, Totoró
Slug: oliverio-conejo-coordinador-de-salud-indigena-y-su-hija-fueron-asesinados-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Indigena-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Oliverio Conejo/ Consejo Regional Indígena del Cauca

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Autoridades del[Consejo Regional Indígena del Cauca](https://twitter.com/CRIC_Cauca)informaron la noche de este 11 de septiembre sobre el asesinato de Oliverio Conejo, coordinador del programa de salud del Pueblo Totoroez y su hija Emily Conejo de 22 años en zona rural de Chuscales en el municipio de Totoró al oriente del departamento en la Transversal del Libertador que comunica Popayán e Inzá.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según relatan habitantes del sector, los hechos sucedieron mientras el coordinador se desplazaba junto a su hija en su automóvil en dirección un ritual de sabiduría ancestral cuando sujetos armados que se desplazaban en una motocicleta los interceptaron y les dispararon con arma de fuego en múltiples ocasiones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Oliverio Conejo era un destacado dirigente del Pueblo Totoroez quien había ocupado diversos cargos tanto a nivel local como zonal y regional, mientras al interior del cabildo se desempeñaba como **coordinador del programa de salud, donde venía liderando iniciativas para proteger a la comunidad indígena de la Covid-19**. [(El silencio frente al exterminio de los líderes sociales en época de pandemia)](https://archivo.contagioradio.com/el-silencio-frente-al-exterminio-de-los-lideres-sociales-en-epoca-de-pandemia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la Guardia Indígena, el programa de Derechos Humanos del CRIC, y comunidades campesinas de la zona ya activaron operativos para recopilar información y localizar a los autores de los hechos, hasta el momento se desconocen los móviles del ataque. [(Lea también: 971 defensores de DD.HH. y líderes han sido asesinados desde la firma del Acuerdo de paz)](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el mes de mayo, desde la Defensoría del Pueblo se había enviado un documento a Jorge Isaacs Hoyos, comandante de la Tercera División del Ejército - **con jurisdicción en Valle del Cauca, Nariño y en específico en Cauca** - solicitando una “intervención urgente ante las autoridades correspondientes" con el fin de proteger a las comunidades indígenas que afirmaban, encontrarse en medio del conflicto armado entre el Ejército y las disidencias de las FARC y diferentes actores armados ilegales.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
