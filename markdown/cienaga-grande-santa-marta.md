Title: Convención Ramsar confirma crisis ecológica de la Ciénaga Grande Santa Marta
Date: 2017-06-06 17:04
Author: ambiente y sociedad
Category: Ambiente, Voces de la Tierra
Tags: Ciénaga Grande de Santa MArtha
Slug: cienaga-grande-santa-marta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/39510_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Tiempo 

###### [6 Jun 2017] 

Las conclusiones del informe de expertos de la Convención Ramsar que visitaron en agosto del año pasado la Ciénaga Grande Santa Marta evidencian que la situación de este importante ecosistema es de agonía. El complejo de humedales más grande del país, se encuentra bajo amenaza grave, lo que significa el secretariado de Ramsar reconoce que **Colombia no ha cumplido su obligación internacional de proteger el humedal.**

Los científicos de la Convención encontraron “cambios fuertes” en las condiciones ecológicas de la Ciénaga, y por ello, recomiendan de manera inmediata que el país inscriba a este ecosistema en el Registro Montreux, donde se alberga a los humedales más amenazados del mundo, y que requieren atención prioritaria. [(Le puede interesar: La última ortunidad de la Ciénaga Grande de Santa Marta) ](https://archivo.contagioradio.com/visita-de-convencion-ramsar-la-ultima-oportunidad-de-la-cienaga-grande-de-santa-marta/)

### "Cambios fuertes" 

Hace un año, la profesora de la Universidad de Magdalena, Sandra Vilardy, aseguraba que la situación de ese lugar es de calamidad, **“la ciénaga está en un colapso ecológico, es un paciente que está en cuidados intensivos”**. Un año después el informe de los expertos lo confirma.

La sobreexplotación y contaminación de parte de las aguas de la Ciénaga, la disminución del agua dulce que recibe debido a la presencia excesiva de sedimentos y a la obstrucción de canales, la “gran pérdida” de bosques de manglar ocasionada por obras viales y de infraestructura que bloquearon flujos de agua, y la disminución de la población de peces, son las principales causas que hacen necesarias acciones de carácter urgente para impedir que la Ciénaga desaparezca.

Mientras esta semana **el consorcio Ribera emitía un reporte en el que da cuenta del avance del 56,4% en las obras de construcción de esa vía La Prosperidad**, que afecta directamente la Ciénaga, el informe de Ramsar, demuestra que los monocultivos de palma y banano, la construcción de esa vía y la ineficiencia de las obras hidráulicas de la vía entre Barranquilla y Santa Marta, tiene en alto riesgo ese ecosistema. [(Le puede interesar: Compañía RHC tiene en riesgo de extinción la Ciénaga Grande de Santa Marta) ](https://archivo.contagioradio.com/cienaga-grande-de-santa-marta-en-peligro-de-extincion-por-compania-rhc/)

### Acciones inmediatas 

“Dados los fuertes cambios en las características ecológicas (…) se requiere la toma de medidas urgentes por parte del Gobierno de Colombia que permitan mantener y recuperar su carácter ecológico y alcanzar su uso racional de acuerdo a los objetivos de la Convención”, dice el documento.

De acuerdo con Juan Pablo Sarmiento Erazo, investigador de la Universidad del Norte, “La inclusión de la Ciénaga en el Registro Montreux puede permitir la obtención de ayuda económica, mediante fondos otorgados por la Ramsar Wetland Conservation Fund, así como de apoyo y asesoría técnica por parte del Grupo de Examen Científico y Técnico de la Convención Ramsar, que puede generar recomendaciones para la recuperación y conservación del ecosistema”.

El informe indica que es importante realizar una limpieza efectiva de acuerdo a un nuevo diseño hidrológico, y su vez, llama la atención del gobierno para que se fortalezca la coordinación interinstitucional para la regeneración ecológica.

Teniendo en cuenta dichas conclusiones la Asociación Interamericana para la Defensa del Ambiente, AIDA, manifiesta que “**Es clave que el Gobierno colombiano siga al pie de la letra las recomendaciones de Ramsa**r y que implemente lo antes posible las mejoras y cambios necesarios en el manejo del sitio”.

Luego de conocerse el informe, **este miércoles desde las 7 de la mañana está citado en la Comisión de Ordenamiento Territorial del Senado un debate de seguimiento** a la grave situación de contaminación en que se encuentra la Ciénaga Grande de Santa Marta, en el que se espera tomar las acciones ara salvar el complejo de humedales. [(Le puede interesar: Ciénaga de San Silvestre, otro complejo de humedales que agoniza)](https://archivo.contagioradio.com/cienaga-grande-de-santa-marta-en-peligro-de-extincion-por-compania-rhc/)

<iframe id="doc_76214" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/350901464/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-aBCJfRIHoWwo5uQSQptQ&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
