Title: Festival Anti Mili Sonoro: Arte, cultura y música
Date: 2015-06-18 11:57
Category: DDHH, eventos
Tags: anti mili, Arenga., Che Guerrero, Frankie ha muerto, La Gleba, Los Vizocios, Mackia, Ministerio de Vgancia, Polikarpa y sus viciosas, Rehén, sonoro
Slug: festivas-anti-mili-sonoro-arte-cultura-y-musica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Captura-de-pantalla-2015-06-18-a-las-11.45.17.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Proceso Distrital de Objeción de Conciencia 

<iframe src="http://www.ivoox.com/player_ek_4658482_2_1.html?data=lZuimpmcdo6ZmKiakpuJd6KnlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRisbn1c7jw9iPhc%2FoypC6y9HNb7Tjz9Tf0YqXhYy109nSh5enb8Tpzdni1MaPvYzhhqigh6elt8rXwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [María Camila, Proceso Distrital de Objeción de Conciencia] 

###### [18 jun 2015] 

En una jornada llena de arte, cultura y música reggae, rap, rock y punk, se llevará acabo este jueves 18 y viernes 19 de junio la 4ta versión del Festival Anti Mili Sonoro en Bogotá, un esfuerzo de organizaciones juveniles, antimilitaristas y objetoras de conciencia por visibilizar los procesos de resistencia contra la guerra y el militarismo en Colombia.

El festival que remonta su origen al año 1989 en Medellín, como una forma de acción directa no-violenta que evidenciara las formas de reclutamiento forzado contra la juventud. Desde el año 2012 las organizaciones juveniles en Medellín articularon iniciativas con los y las jóvenes en Bogotá para dar vía al Festival también en la capital del país.

En su versión del año 2015, el Festival ha contado con escenarios preparatorios de convocatoria desde el 9 de mayo, con foros y conciertos locales en Bosa, Suba, Usme y Chapinero, y tendrá como eventos centrales el foro "[¿Arte y Paz? Charla del papel de la música y el arte en la construcción de paz](https://www.facebook.com/events/1604291093179173/?ref=3&ref_newsfeed_story_type=regular&feed_story_type=17&action_history=null)" el jueves 18 de junio a las 5 pm en el Centro de Memoria, Paz y Reconciliación; y el concierto Anti Mili Sonoro el viernes 19 a partir del medio día en la Plaza de Bolívar.

El Festival contará en esta oportunidad con la participación de Rehén, Polikarpa y sus viciosas, Ministerio de Vgancia, Mackia, Los Vizocios, La Gleba, Frankie ha muerto, Che Guerrero y Arenga.

\[caption id="attachment\_10235" align="aligncenter" width="960"\][![Anti Mili Sonoro 2015](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Anti-Mili-Sonoro.jpg){.size-full .wp-image-10235 width="960" height="355"}](https://archivo.contagioradio.com/festivas-anti-mili-sonoro-arte-cultura-y-musica/anti-mili-sonoro/) Anti Mili Sonoro 2015\[/caption\]
