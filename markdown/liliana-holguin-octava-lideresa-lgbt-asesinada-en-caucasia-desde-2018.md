Title: Liliana Holguín, octava lideresa LGBT asesinada en Caucasia, Antioquia desde 2018
Date: 2019-02-27 17:51
Author: CtgAdm
Category: DDHH, LGBTI, Líderes sociales
Tags: Asesinatos a líderes LGBT, Asesinatos contra población LGBT, Caucasia
Slug: liliana-holguin-octava-lideresa-lgbt-asesinada-en-caucasia-desde-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/9c355a46-525b-45c6-9b06-285a2577e895.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [oto: Caribe Afirmativo] 

###### [27 Feb 2019] 

El pasado 26 de febrero fue asesinada** Liliana Holguín mujer de 40 años  perteneciente a la Mesa LGBTI de Caucasia, **mientras se desplazaba en una motocicleta junto a su sobrino cuando fueron alcanzados por otra moto desde la que dispararon en repetidas ocasiones terminando con la vida de ambos. Liliana era conductora de mototaxi y apoyaba actividades de formación, además de liderar acciones contra el desplazamiento forzoso y en pro del activismo.

**Wilson Castañeda, director de Caribe Afirmativo**, afirma que en repetidas ocasiones han advertido sobre la violencia que existe contra las personas LGBT en el municipio, **"con el asesinato de Liliana ya son ocho los líderes y lideresas activistas LGBT asesinados en el municipio lo que equivale para el director en un "extinción del movimiento social"**,  señala.

Castañeda también denuncia que el municipio es un fuerte corredor del  tráfico de drogas y que todas estas muertes están siendo vinculadas por las autoridades al microtráfico, pues la presión de las bandas sobre la población termina involucrando a sectores vulnerables como el LGBT en estos circuitos del menudeo de la droga.

### **Vincular el microtráfico al sector LGBT, la estrategia de las bandas criminales** 

Tal como indica Castañeda, sobre el homicidio de Liliana, la Policía declaró que existía una presunta relación entre el microtráfico y su muerte, sin embargo Castañeda advierte que se trata de una lideresa visible en su comunidad quien además de hacer parte de la mesa municipial LGBT asistía a los talleres de empoderamiento y de quien nunca se supo hubiera recibido amenazas o restricciones por ejercer su activismo.

Es importante señalar que desde enero de 2018 las personas LGBT de Caucasia han manifestado que no hay condiciones para su activismo, pues han sido recurrentes la circulación de panfletos amenazantes que incluyen nombres y señalamientos hacia ellos,  además diversas bandas como **Los Urabeños, el Clan del Golfo y Los Paisas** les coercionan para depositar y guardar su mercancía bajo la advertencia que si no aceptan serán asesinadas.

Desde 2017 Caribe Afirmativo viene trabajando en el territorio para generar mecanismos de protección  y garantías de derechos, de igual forma frente a los crímenes sucedidos han logrado establecer una investigación junto a la Fiscalía para lograr determinar si efectivamente existe una relación entre los homicidios y la identidad de género de las víctimas.[(Le puede interesar La deuda de Colombia con la población LGBTI)](https://archivo.contagioradio.com/la-deuda-de-colombia-con-la-poblacion-lgbti/)

<iframe id="audio_32924733" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32924733_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]

<iframe id="audio_32924733" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32924733_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
