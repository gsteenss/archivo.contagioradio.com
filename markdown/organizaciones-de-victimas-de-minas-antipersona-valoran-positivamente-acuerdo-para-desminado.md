Title: Organizaciones de víctimas de minas antipersona valoran positivamente acuerdo para desminado
Date: 2015-03-10 18:14
Author: CtgAdm
Category: DDHH, Nacional
Tags: acuerdo de minas, acuerdos mesa de paz, colombia, minas antipersona, Reinel Barbosa
Slug: organizaciones-de-victimas-de-minas-antipersona-valoran-positivamente-acuerdo-para-desminado
Status: published

##### Foto:Kint.com 

##### **Entrevista con[ Reinel Barbosa]:** 

<iframe src="http://www.ivoox.com/player_ek_4195041_2_1.html?data=lZaml5WYdY6ZmKiakp6Jd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYtMrNssbgjKfO1MfTt8Kf14qwlYqliMToytLOjcnJb87dz8bgjcbSuMrkxteah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Organizaciones sociales de **víctimas de minas antipersona valoran positivamente el acuerdo sobre el desminado** entre el gobierno colombiano y la guerrilla de las FARC-EP en el marco de los conversaciones de La Habana.

**Reinel fue afectado en 2008** por la explosión de una mina antipersona, es miembro de la Asociación distrital de sobrevivientes de minas antipersona (ADISMAN), y de la Red nacional de sobrevivientes de minas antipersona "Municiones sin explotar, artefactos explosivos" y de la Asociación de personas víctimas con discapacidad.

Reinel en entrevista con Contagio Radio explica la importancia de este paso en la construcción de paz a ** pesar de no existir  un cese bilateral al fuego, igualmente resalta la importancia de este acuerdo para el desescalonamiento** del conflicto armado y para frenar las víctimas que este produce.

Según él, el **desminado si es posible sin cese bilateral al fuego**, debido a que principalmente se va a realizar en zonas de desarrollo rural, corredores por donde pasan niños para ir a la escuela, zonas de impacto social, como lugares de pesca, caza o de reunión de comunidades indígenas. Es decir no se va a realizar en zonas donde aún se están implementando los artefactos o donde hay confrontación armada.

En total son **600 municipios afectados y el desminado podría tardar más de 8 años** en dar **resultados positivos**, pero ante todo evitaría muertes innecesarias.

Para el **desminado** es importante la colaboración entre guerrilla, y ejército Colombiano, pero sobre todo la **colaboración de la sociedad civil**, que es el grupo social más afectado.
