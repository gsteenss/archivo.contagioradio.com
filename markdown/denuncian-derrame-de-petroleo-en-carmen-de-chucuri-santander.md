Title: Denuncian derrame de petróleo en Carmen de Chucurí, Santander
Date: 2018-04-15 18:50
Category: Ambiente, Nacional
Tags: Carmen de Chucurí, derrame de petróleo, Ecopetrol, Lizama, Santander
Slug: denuncian-derrame-de-petroleo-en-carmen-de-chucuri-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/petroleo_23-e1523835999328.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @valentinacmpm ] 

###### [15 Abr 2018] 

De acuerdo con la denuncia de la comunidad y un comunicado del Equipo Jurídico Pueblo, la vereda Dos Bocas del municipio del Carmen de Chucurí, en Santander la comunidad y la naturaleza es víctima de un nuevo afloramiento, al parecer crudo y gas, debido a las actividades extractivas de Ecopetrol. Según la denuncia, el panorama es "totalmente desolador", debido a la presencia de "una gran mancha pegajosa, oscura, con olor a petróleo que se esparce lentamente, acercándose al Río Cascajales, que a su paso afecta toda forma de vida", dice el comunicado.

Este nueva mancha de crudo está ubicado a 25 minutos del Corregimiento de Yarima; por la vía que conduce al Casco urbano del Carmen de Chucurí. "**El afloramiento se avista a 300 metros aproximadamente de la vía principal y a pocos metros del río Cascajales; muy cerca se encuentran varios pozos de petróleo** “Campo San Luis” que fueron explotados por décadas pero se desconoce si fueron cerrados conforme los protocolos técnicos de sellamiento".

La comunidad ha detectado dos afloramientos, aunque creen que pueden existir más. Afirman que la situación más grave se presenta dentro de un pastizal."A simple vista se advierte que fluye un líquido espeso color oscuro. Permanentemente se observan burbujas que brotan del subsuelo. A diferencia de lo ocurrido en el Pozo 158 Lizama, el fenómeno que ocurre en Dos Bocas es paulatino y progresivo", señala el Equipo Jurídico Pueblos.

### **Ecopetrol no responde** 

Según afirma el comunicado, el propietario del predio donde se evidencia el crudo, ha informado en reiteradas ocasiones a Ecopetrol sobre dicha situación, sin embargo, la empresa de economía mixta lo que afirma es que, “se trata de un manadero de hidrocarburos natural que fue reportado a las autoridades ambientales desde el año 2012. Allí está el Campo San Luis, en donde ha sido reportados cuatro afloramientos más que no están relacionados con pozos petroleros", dijo Adrián Antonio Camargo, líder de producción Gerencia de Mares de Ecopetrol, en una entrevista en para RCN Radio.

Por su parte, la Agencia Nacional de Hidrocarburos, ANH, ya habría visitado la zona, y de tal visita concluyó que, “**en el sitio del afloramiento existen cuatro puntos naturales de rezumideros de hidrocarburos**, documentados en el Plan de Manejo Ambiental”.

Mientras tanto, los pobladores del lugar manifiestan que no ha habido intensión de Ecopetrol de realizar algún tipo de intensión de investigar las causas reales de la presencia de crudo. Asimismo, la comunidad asegura que hace cinco años se realizaron trabajos de sísmica que originó el aumento del vertimiento de fluidos.

### **Las consecuencias ** 

Hasta el momento, la preocupación de los habitantes es la muerte de ganado, "o bien porque quedan atascados en el espeso líquido o por el consumo de pasto contaminado. También han muerto diversas especies de animales silvestres", dice la denuncia.

El comunicado también indica que el profesor **Óscar Vanegas analizó el crudo que aflora, y concluyó que, dichas muestras, son coincidentes con el crudo del pozo petrolero “San Luis 4**” ubicado relativamente cerca de la boca del afloramiento.

"Todo parece indicar -desde ya- que las entidades del Estado, como Ecopetrol, la Agencia Nacional de Hidrocarburos, la Autoridad Nacional de Licencias Ambientales –ANLA-, Ministerio de Ambiente, entre  otras, acudirán al engaño para evadir su responsabilidad en esta nueva tragedia", considera el Equipo Jurídico Pueblos.

<iframe id="audio_25395889" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25395889_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
