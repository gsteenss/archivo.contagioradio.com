Title: Movimiento estudiantil de la UDistrital no cederá ante presiones del Consejo Superior
Date: 2016-05-27 16:37
Category: Movilización, Nacional
Tags: Movimiento estudiantil Colombia, paro universidad distital, Universidad Distrital
Slug: movimiento-estudiantil-de-la-udistrital-no-cedera-ante-presiones-del-consejo-superior
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Estudiantes-UDitrital.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UDistrital ] 

###### [27 Mayo 2016]

De acuerdo con el más reciente Comunicado del Consejo Superior de la Universidad Distrital sí el 10 de junio no se cesa el paro decretado por el movimiento estudiantil, se suspende el semestre académico. Ante esta presión los estudiantes reafirman su posición de mantenerse en cese de actividades, pues las directivas continúan desconociendo las prioridades del pliego de peticiones.

Este pliego contempla la derogación del acuerdo 001 de 2016, la continuación del proceso de reforma universitaria puesto en marcha por la constituyente, la exigencia de garantías académicas para la terminación exitosa del semestre y condiciones de infraestructura adecuadas en cada una de las sedes.

Una de las exigencias que hace el Consejo Superior es que se retire el campamento que los estudiantes tienen en la sede administrativa de la institución; sin embargo, los estudiantes consideran que el bloqueo de este edificio es el mecanismo de presión más alto y dado que no hay garantías para la negociación, mantendrán la toma pacífica.

Vea también: [[movilización Universidad Distrital](https://archivo.contagioradio.com/?s=UNIVERSIDAD+distrital) ]

<iframe src="http://co.ivoox.com/es/player_ej_11692121_2_1.html?data=kpajm5eVdpKhhpywj5WbaZS1kZiah5yncZOhhpywj5WRaZi3jpWah5yncavjydOYsNTLucbmwpCajbqPiMrn1dfW1sbQcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

######  
