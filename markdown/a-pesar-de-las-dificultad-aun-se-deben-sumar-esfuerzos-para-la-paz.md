Title: "A pesar de las dificultad, aún se deben sumar esfuerzos para la paz"
Date: 2020-07-08 19:48
Author: CtgAdm
Category: Actualidad, Paz
Tags: esfuerzos para la paz
Slug: a-pesar-de-las-dificultad-aun-se-deben-sumar-esfuerzos-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 8 de Julio **António Guterres, secretario general de las Naciones Unidas,** señaló en su informe trimestral sobre la Misión de Verificación de la ONU en Colombia que, ***"es un hecho que el coronavirus y el aislamiento han afectado directamente la implementación del Acuerdo de Paz".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Laura Gil, politólogo e internacionalista,** reconoció que hay un panorama bastante negativo hoy en el país, frente a la presencia internacional en Colombia, *"**es sorprendente ver cómo se han desmoronado tan rápido las relaciones internacionales y los logros** que habían obtenido las políticas exterior en Colombia".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y señaló que para el Gobierno Nacional es posible que los resultados de las políticas de [relaciones internacionales](https://archivo.contagioradio.com/no-aparece-el-informe-de-la-comision-presidencial-de-excelencia-militar-human-rights-watch/)se lean como exitosas, *"esto a la luz de qué creen que se han alineado muchos puntos, dejando de lado que **a al largo y mediano plazo esto representa un gran impacto y un gran costo para el país** y a los esfuerzos para la paz".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Por dónde enfocar los esfuerzos para la paz?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Gil señala que el país se encuentra atrapados en el peor escenario para poder reaccionar como comunidad, esto teniendo en cuenta de que no es posible hacer movilización en las calles, ***"no se puede defender la vida poniendo en riesgo la vida, pero esto no significa que no existan otras formas de movilizar, de exigir y actuar".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de ellas es la defensa de la vida de los y las firmantes de paz, *"no olvidemos los ex combatientes, tenemos **215 excombatientes asesinados, esta es la gente que le apostó, firmó y está tratando de superar la ilegalidad, a ellos deberíamos estar recibir con los brazos abiertos**,apoyando, y no simplemente lamentando números"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado destacó que **la prioridad nacional debe ser la vida**, en conjunto con la internacional, *"en una acción concertada de todos los movimientos sociales, para dejar saber que tenemos un Acuerdo de Paz que nos dio dividendos, que no siga dando"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Este es un país muy diferente al que era cuando estábamos en mitad de la guerra, pero que hay un Acuerdo de Paz que se está violando, y que no está garantizan la vida de las personas y a este es el camino al que le debemos trabajar"*
>
> <cite>**Laura Gil| politólogo e internacionalista**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y concluyó diciendo, ***"pareciera que volvimos a un Gobierno como el de Uribe, en donde si se decía desde Estados Unidos que las cosas se ejecutaba, solo así se hacían",*** y agregó que ante esto hay que apostarle a entender que la opinión de la comunidad internacional es valiosa e importante, pero a esta se debe *"hacer un tamizaje y entender que deben conocimiento del proceso que se ha llevado por años en Colombia y respaldarlo".*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Puede profundizar en este tema viendo. Otra Mirada: Colombia y sus relaciones internacionales.*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D282066349783725&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
