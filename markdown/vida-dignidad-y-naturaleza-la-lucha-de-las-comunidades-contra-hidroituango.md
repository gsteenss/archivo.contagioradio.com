Title: Vida, dignidad y naturaleza: La lucha de las comunidades contra Hidroituango
Date: 2015-12-15 21:00
Category: Hablemos alguito
Tags: Movimiento Ríos Vivos, Proyecto Hidroituango
Slug: vida-dignidad-y-naturaleza-la-lucha-de-las-comunidades-contra-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Hidroituango-no-es-paz-e1490827578825.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### <iframe src="http://www.ivoox.com/player_ek_9732541_2_1.html?data=mpyglJqYdY6ZmKiakpaJd6Kmkoqgo5eacYarpJKfj4qbh46kjoqkpZKUcYarpJLDy8nFaZO3jMnWydPNqMLYjN6Y0MbYudPVzcrnw4qXhYzAwpDZ18jMpYzYxpDZw9iPp9Dh1tPWxsbIqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Hablemos Alguito, Hidroituango] 

##### 14 Dic 2015

La construcción del megaproyecto energético Hidroituango por parte de Empresas Públicas de Medellín (EPM), ha traído consecuencias nefastas para los habitantes de 12 municipios del área del norte y occidente de Antioquia, a los que llegó con promesas de bienestar y desarrollo.

Al año 1969 se remontan los primeros estudios adelantados para construír 5 represas en la región, aprovechando las condiciones naturales del cañon del Río Cauca, megaobras que para la época resultaron imposibles de ejecutar por el alto costo que demandaban y las dificultadas técnicas y de acceso propias del terreno.

La presencia de actores armados en la región, sumada a la ausencia de instituciones del Estado, provocaron que grandes expectativas se generaran en la población con la llegada del megaproyecto, vendiendo las ideas de pacificar y traer el desarrollo tan esperado por las comunidades.

Sin embargo la realidad sería bastante diferente. La desaparición del patrimonio cultural, despojos, desplazamientos, masacres y crímenes contra la naturaleza se convertirían en la pesadilla de los pobladores que han emprendido desde entonces procesos de resistencia y lucha por sus derechos contra los poseedores del gran capital y sus ejércitos privados de orígen paramilitar.

Isabel Cristina Zuleta, Integrante movimiento Ríos Vivos y Genaro Graciano, Vicepresidente de la Asociación de víctimas y afectado por megaproyectos, comparten en "Hablemos Alguito" su experiencia de vida, en la lucha por la defensa de los Ríos y el territorio, los derechos de la mujer y  por la presevación de su identidad y patrimonio.
