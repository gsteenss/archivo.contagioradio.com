Title: Misión de la ONU es atacada en Caquetá
Date: 2020-10-08 21:51
Author: AdminContagio
Category: Nacional
Tags: Caquetá, Meta, Misión de Derechos Humanos de la ONU
Slug: mision-de-la-onu-es-atacada-en-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ONU-informe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @MisionONUCol

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este miércoles, en horas de la tarde, la Defensoría del Pueblo denunció que seis hombres armados abordaron el vehículo de la Misión de Derechos Humanos de la ONU haciendo descender a los pasajeros y posteriormente, incinerando el automóvil. Los hechos sucedieron en zona limítrofe de Caquetá y Meta, en el sector de Lozada.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DefensoriaCol/status/1313995723235622912","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DefensoriaCol/status/1313995723235622912

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según la defensoría, **los hombres serían disidentes de las FARC, quienes habrían manifestado que no querían la presencia de este organismo internacional en el sector.** ([Le puede interesar: Parte de verdad en crimen de Álvaro Gómez se ocultaría en la «Masacre de Mondoñedo»](https://archivo.contagioradio.com/parte-de-verdad-en-crimen-de-alvaro-gomez-se-ocultaria-en-la-masacre-de-mondonedo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con la entidad, **la misión de la ONU se encontraba en el sector junto a la Defensoría y la [Comisión para el Esclarecimiento de la Verdad](https://twitter.com/ComisionVerdadC?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1314023919549116418%7Ctwgr%5Eshare_3&ref_url=https%3A%2F%2Fwww.contagioradio.com%2Fwp-admin%2Fpost.php%3Fpost%3D91134action%3Dedit) haciendo parte de una comisión humanitaria.** Asimismo lo indicó la ONU en Colombia, agregando que quienes se transportaban en el vehículo se encuentran bien.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego de los sucesos, el personal se transportó en vehículos de la Comisión de la Verdad y la Defensoría del Pueblo que se transportaban con ellos y ya se encuentran en San Vicente del Caguán.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONUHumanRights/status/1314023919549116418","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONUHumanRights/status/1314023919549116418

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por su parte, [la Cancillería](https://twitter.com/CancilleriaCol/status/1314017667641217025) expresó que «condena y rechaza el incendio del vehículo de la Oficina de la Alta Comisionada de Naciones Unidas para los Derechos Humanos en Caquetá en nombre del Gobierno de Colombia». «Nuestra total solidaridad».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por el momento, las autoridades se encuentran investigando el hecho para dar con los responsables del hecho. (Le puede interesar: [Dos líderes sociales de El Castillo, Meta fueron asesinados](https://archivo.contagioradio.com/lideres-sociales-el-castillo-meta-fueron-asesinados/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Situación en Caquetá y Meta
---------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Así como en departamentos como Antioquía, Cauca y Nariño se siguen resgitrando violaciones de los derechos de las comunidades y habitantes, la violencia también se evidencia en Caquetá y Meta. Según el [Instituto de estudios para el desarrollo y la paz](http://www.indepaz.org.co/lideres/) -Indepaz- **en lo que va del 2020, cuatro líderes y defensores de derechos humanos han sido asesinados en Caquetá y cinco en el Meta.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, desde que se firmó el acuerdo de paz, cerca de 19 firmantes han sido asesinados en los dos departamentos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, uno de los departamentos con mayores casos de violaciones, agresiones, hostigamientos en medio de operativos de erradicación forzada de cultivos ilícitos es Caquetá.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
