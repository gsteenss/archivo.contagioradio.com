Title: Mujeres Rumbo a Gaza son asaltadas por la Armada de Israel
Date: 2016-10-05 14:39
Category: El mundo, Movilización
Tags: Conflicto Israel-Palestina, mujeres, Mujeres Rumbo a Gaza, Palestina
Slug: mujeres-rumbo-a-gaza-asaltadas-por-la-armada-de-israel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/nota_mujeres_rumbo_gaza-e1475691918805.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Rumbo a Gaza] 

###### [5 Oct de 2016] 

El día de hoy **la armada de Israel asaltó de forma ilegal la [Flotilla de Mujeres rumbo a Gaza.](https://archivo.contagioradio.com/ocupacion-palestina-solo-puede-terminar-si-a-nivel-internacional-sumamos-esfuerzos/)** Son 13 mujeres de cinco continentes las tripulantes del velero quienes realizan una acción no violenta de denuncia contra el bloqueo israelí a la Franja de Gaza que se prolonga desde 2007 y que tiene confinadas a cerca de 2 millones de personas en 365 Kilómetros cuadrados.

El velero asaltado zarpó hacia Gaza el 14 de Septiembre desde Barcelona y según informó la organización en su perfil de Twitter "**se perdió el contacto con el velero Zaytouna-Oliva a las 15:58 de hoy". E**n las ultimas horas se informó que las 13 activistas se encuentran retenidas ilegalmente en  Ashdod zona militar de Israel.

Desde el 2010 se han organizado tres flotillas de ayuda humanitaria a la Franja de Gaza y en todos los casos no se llegó a destino, en dos ocasiones fueron frenadas por comandos del Ejercito israelí y la tercera fue abordada por el Gobierno griego que, por presiones de Tel Aviv, impidió salir de sus puertos a los barcos. **Ese año 10 activistas fueron asesinados por fuerzas israelíes en la misma zona militar de Ashdod.**

La organización Rumbo a Gaza afirma que "Las flotillas y otras misiones navales son una denuncia ante la inacción de la comunidad internacional​ frente a este sufrimiento y a la vez un llamamiento a la solidaridad de la sociedad civil con la resistencia palestina."

Frente a esta situación **varios eurodiputados españoles y [organizaciones de Suecia se pronunciaron solicitando](https://archivo.contagioradio.com/mas-de-100-organizaciones-se-solidarizan-con-palestina-en-la-semana-contra-el-apartheid-israeli/) a Federica Mogherini** Alta Representante de la Unión Europea para Asuntos Exteriores, se **"garantice la protección de las integrantes de la Flotilla, ante las amenazas del Gobierno de Israel** y asegure la libre navegación del barco hasta que este alcance su objetivo".

Varias ciudades de España, **organizaron para las 7:00pm de hoy manifestaciones frente a las delegaciones del Gobierno para solicitar protección a las integrantes de la iniciativa Mujeres Rumbo a Gaza.**

Las activistas retenidas son:

1.  Mairead Maguire –Irlanda
2.  Marama Davidson – Nueva Zelanda
3.  Ann Wright – USA
4.  Yudit Barbara Ilany – Israel
5.  Jeannette Escanilla –Suiza
6.  Samira Douaifia –Algeria
7.  Fauziah Hasan – Malasia
8.  Mina Harballou – Reino Unido
9.  Leigh-Ann Naidoo – Sur África
10. Hoda Rakhme – Rusia
11. Sandra Barrilaro – España
12. Synne Sofie Reksten – Noruega
13. Emma Ringqvist - Siuza

**Las exigencias**

En un comunicado oficial la organización Rumbo a Gaza manifiesta lo siguiente:

1.  Denunciamos que Israel siga atacando barcos civiles en aguas internacionales.
2.  Denunciamos que un régimen racista y de apartheid continúe colonizando Palestina.
3.  Denunciamos que gobiernos, como el del Estado español, ni protejan el derecho a la libre navegación, ni condenen los ataques israelíes a barcos civiles, ni hayan llevado a cabo en 68 años acciones y políticas efectivas para poner fin a la ocupación y el bloqueo.
4.  Exigimos protección hacia las pasajeras del Zaytouna, llevadas contra su voluntad a centros de detención israelíes, y su inmediata puesta en libertad.

###### Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU) 
