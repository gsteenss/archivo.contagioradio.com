Title: Isla de Barú vive su peor momento ambiental, social y cultural
Date: 2020-02-11 18:43
Author: CtgAdm
Category: Ambiente, Nacional
Tags: bolivar, Proceso de titulación colectiva
Slug: isla-baru-vive-peor-momento-ambiental-social-cultural
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Barú11.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Barú Javier García/El Heraldo

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/oscar-chavez-sobre-situacion-ambiental-isla-de_md_47665877_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Óscar Chavez | Jurista

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

El más reciente informe del **Observatorio Étnico Campesino de la Universidad Javeriana** evidencia que el deterioro ambiental de la isla de Barú está vinculado al crecimiento acelerado del turismo y amenaza la pervivencia étnica y cultural de los 3.000 habitantes de la comunidad ancestral negra de Barú. Adicionalmente hay un proceso de titulación colectiva que ha sido frenado por la Agencia Nacional de Tierras.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Desconocer un derecho ancestral

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque a la comunidad de Barú le pertenecen 2476 hectáreas de la isla, desde 1851 - pertenencia ratificada en 1957 - en abril de 2019, la Agencia Nacional de Tierras frenó el proceso de titulación colectiva argumentando que en la solicitud no se expresó que los predios eran de carácter privado. Para el abogado, esto es impreciso pues los miembros de la comunidad de Barú pueden ceder sus títulos individuales o familiares para titulación colectiva, además, no se trata de un bien baldío, sino que están solicitando la validación de un título ancestral que data de 1851 y del cual existe registro.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El jurista Óscar Chávez señala que no voluntad política para delimitar los espacios que pertenecen a la comunidad dónde se permita su existencia y recreación, **"entendemos que hay una complejidad, pero ese no debe ser el obstáculo para que se hagan las claridades pertinentes".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"En estos momentos la isla está pasando por su peor momento ambiental, social y cultural a raíz de la dinámica de turismo sin control que hay en la zona" explica el abogado, pues a pesar que desde un punto de vista genera un crecimiento, también ha perjudicado a las comunidades al punto de que hoy están al borde de la extinción, cultural y física". [(Lea también: Zarigüeyas, 'guatines', ardillas e iguanas víctimas del "desarrollo" en Cali)](https://archivo.contagioradio.com/zarigueyas-lechuzas-ardillas-e-iguanas-victimas-del-desarrollo-en-cali/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La vulneración del derecho de la comunidad al territorio colectivo y lo que han denominado "la extinción de su identidad étnica y cultural" se ve reflejada en el desarrollo turístico, portuario e industrial con apropiación irregular de tierras y usos contraproducentes de la misma. Según las comunidades estas prácticas las **marginan de espacios que han constituido su fuente de sustento, como la pesca, la agricultura y las fabricación de artesanías.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Chávez resalta que toda la apertura que se ha hecho, únicamente ha beneficiado a otros sectores y no precisamente a las comunidades que llevan más de 300 años viviendo en esas tierras y que hoy no cuentan con calidad de vida ni condiciones para la preservación de sus ecosistemas, "**hay que garantizar la defensa de la comunidad para que esto sea un atractivo, no nos oponemos al turismo, creemos que debe ser más ordenado e inclusivo para las comunidades"**, explica.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las afectaciones en Barú también se ven en el bosque tropical

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, Johana Herrera directora del Observatorio Étnico Campesino de la Universidad Javeriana y una de las mentes tras el informe 'Barú, territorio colectivo desde 1851' señala que se ha podido evidenciar un aumento en la infraestrucura hotelera impactando directamente en el bosque seco tropical cuya expansión alcanzaba las 1.000 hectáreas en 1987 y en 2017 solo alcanzó 419 hectáreas de cobertura.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Hay un divorcio entre la ocupación empresarial hotelera y las formas de usos de la comunidad" - Johana Herrera

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma, las afectaciones también se han visto en las lagunas costeras, que son fundamentales para estos ecosistemas que tienen una especial dificultad para el acceso al agua dulce, pasando de tener 93 hectáreas a contar solo con 50, lo que lleva a **"escenarios de imposibilidad de recuperación de estas áreas"** que ya han sido reemplazadas en varios casos por malla urbana e infraestructura para el turismo. [(Le puede interesar: En Patía, Cauca, siembran las semillas de la esperanza para salvar el bosque seco tropical)](https://archivo.contagioradio.com/en-patia-cauca-siembran-las-semillas-de-la-esperanza-para-salvar-el-bosque-seco-tropical/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En términos ecológicos, el turismo desmedido pone en riesgo a especies que conviven en el bosque, en particular aves y reptiles, que en otros bosques secos tropicales, se ven amenazadas por la **minería, la ganadería, la ampliación de la frontera agrícola o la existencia de cultivos de uso ilícito, y en el caso de Barú ampliación de vías y malla urbana.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
