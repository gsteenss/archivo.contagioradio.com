Title: Construcción de cinco viviendas de lujo tienen en riesgo cerros orientales de Bogotá
Date: 2016-02-19 08:08
Category: Ambiente, Nacional
Tags: Alcaldía de Bogotá, cerros orientales, construcciones ilegales
Slug: construccion-de-cinco-viviendas-de-lujo-tienen-en-riesgo-cerros-orientales-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Cerros-orientales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Secretaría Ambiente 

<iframe src="http://www.ivoox.com/player_ek_10495287_2_1.html?data=kpWhm5qWfJihhpywj5aUaZS1kZ6ah5yncZOhhpywj5WRaZi3jpWah5yncaTjz9jh1NrHp8qZpJiSpJjSb8XZjMjW0MjTb9fd187S0MnFt4zYxpDZ18%2FTb9XdxtPS0JDJsozmysrgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [18 Feb 2016]

**Cinco viviendas de lujo que se encuentra dentro de la reserva forestal de los Cerros orientales de Bogotá** y que no cuentan con licencia de construcción representan un grave riesgo para la biodiversidad que existe en los cerros de la ciudad.

De acuerdo con Andrés Plazas, líder de la organización Amigos de la Montaña, el pasado 12 de enero el cauce de la quebrada Rosales, en Bagazal, amaneció completamente seca, debido a esa construcción de privados que ni la Secretaría de Ambiente, ni los sellamientos de la CAR y la Alcaldía de Chapinero pudieron evitar.

Según lo denuncia Plazas, **la quebrada habría sido desviada con el propósito de crear dos lagos artificiales para las casas** cuya construcción avanza cada día más. Se trataba de un “desenlace anunciado”, dice el líder de la organización ambientalista.

**La Comercializadora kaysser y la Constructora Imaco**, serían algunos de los responsables del deterioro ambiental al que se ha visto puesto los cerros orientales, y que no han querido acatar las medidas de parte de la CAR y la alcaldía de Chapinero.

En total son seis delitos que se le imputan a los responsables de estas obras **“abiertamente ilegales”, como lo había señalado Susana Muhamad,** anterior secretaria de Ambiente: **"aprovechamiento ilícito de recursos naturales, contaminación ambiental, contaminación ambiental por residuos sólidos peligrosos, daño ambiental, invasión a áreas protegidas y fraude a resolución de autoridad".**

Organizaciones como Amigos de la Montaña, aseguran que estas construcciones impactan gravemente el ecosistema de los Cerros y desvían el tránsito de la Quebrada Rosales, poniendo en peligro el habitad de animales como el pez Capitancito enano, que se encuentra en peligro de extinción.

Tras estos acontecimientos, el pasado 9 de febrero en las instalaciones de la CAR, 31 organizaciones comunitarias se dieron cita para expresar su rechazo a estas construcciones y en defensa a los Cerros Orientales.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
