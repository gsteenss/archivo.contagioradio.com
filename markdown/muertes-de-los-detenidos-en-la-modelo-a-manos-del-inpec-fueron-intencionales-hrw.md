Title: Muertes de los detenidos en "La Modelo" a manos del INPEC fueron intencionales: HRW
Date: 2020-11-24 23:08
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Cárcel La Modelo, Human Rights Watch, INPEC, La Modelo
Slug: muertes-de-los-detenidos-en-la-modelo-a-manos-del-inpec-fueron-intencionales-hrw
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Carcel-La-Modelo-INPEC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Según un estudio publicado por la ONG Human Rights Watch -[HRW](https://www.hrw.org/es/news/2020/11/24/colombia-muertes-de-detenidos-habrian-sido-intencionales)- que contó con la participación de instituciones forenses internacionales, **las muertes de las personas detenidas, durante el motín penitenciario que tuvo lugar en la cárcel La Modelo en marzo de este año “*habrían sido intencionales*”.** (Lea también: [Amotinamiento dejó 23 muertos en cárcel La Modelo](https://archivo.contagioradio.com/amotinamiento-dejo-23-muertos-en-la-carcel-la-modelo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe recordar que el 21 de marzo de este año, **24 internos de la cárcel La Modelo murieron y 76 más resultaron heridos, luego de que se produjera un motín en el centro penitenciario originado por la reacción violenta de los guardias del INPEC,** ante las protestas que iniciaron varios reclusos por la precaria e indigna condición de hacinamiento en la que se encontraban y por el temor a que el virus se propagara al interior de la cárcel.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El estudio publicado contó con la participación del Independent Forensic Expert Group –IFEG- y el International Rehabilitation Council for Torture Victims –RCT-, dos instituciones forenses reconocidas como referentes mundiales en la investigación médico-legal de la tortura, los maltratos y las ejecuciones ilegales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los expertos forenses analizaron los 24 informes de necropsia que emitió el Instituto Nacional de Medicina Legal, concluyendo que **“*la mayoría de las heridas de bala descritas en los informes de necropsia son consistentes con que hayan sido infligidas con intención de matar*”** y que “*los informes de autopsia no registran ningún indicio de heridas de bala que hayan sido efectuadas únicamente con el fin de herir a las personas, en vez de matarlas*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a estos hallazgos, el director para las Américas de HRW, José Miguel Vivanco; expresó que pese a que **“*este informe, basado en documentación oficial, indica que las muertes de los detenidos de La Modelo habrían sido intencionales; hasta ahora no ha habido avances significativos en la investigación penal sobre el caso*”.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JMVivancoHRW/status/1331193764925333505","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JMVivancoHRW/status/1331193764925333505

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según HRW, los 24 informes de autopsia examinados por los expertos internacionales no anexaban fotografías, bosquejos, análisis de balística, ni información de investigaciones en el lugar de los hechos; sin embargo, al ser los reportes oficiales elaborados por Medicina Legal sirvieron de base para realizar el estudio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, **HRW advirtió sobre otro tipo de irregularidades en los informes de autopsia, como por ejemplo que 10 de ellos no estaban firmados y otros 2 estaban marcados con la etiqueta “borrador”.** No obstante, según la ONG, fuentes oficiales indicaron a los expertos forenses que realizaron el estudio, que no se habían presentado actualizaciones, ni revisiones hasta el día 9 de noviembre; por  lo cual los hallazgos fueron publicados ya que no había información nueva.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las investigaciones no han arrojado responsables sobre lo ocurrido en La Modelo

<!-- /wp:heading -->

<!-- wp:paragraph -->

HRW señaló que se ha reunido a través de sus representantes con el Fiscal General, Francisco Barbosa, para indagar sobre el estado de las investigaciones frente a este caso; pero según lo recogido en estos encuentros, informó que **hasta el día 13 de noviembre, aún no se habían presentado imputaciones en relación con las muertes y lesiones que sufrieron los reclusos, es decir que los responsables no han sido individualizados para ser presentados ante la Justicia.** (Lea también: ["Fiscal Barbosa está encubriendo responsables de masacre en cárcel La Modelo" Uldarico Florez](https://archivo.contagioradio.com/fiscal-barbosa-esta-encubriendo-responsables-de-masacre-en-carcel-la-modelo-uldarico-florez/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En relación con esto, José Miguel Vivanco afirmó que: “*Las autoridades de la Fiscalía General de la Nación tienen la obligación de adelantar investigaciones oportunas, imparciales y exhaustivas, y garantizar que los responsables por el uso excesivo e injustificado de fuerza letal respondan por sus actos*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, Human Rights Watch también cuestionó el hecho que Margarita Cabello, entonces Ministra de Justicia, sea quien vaya a asumir las investigaciones ahora en su rol de Procuradora, señalando que **“*existen motivos válidos para temer que haya un conflicto de interés que limite las posibilidades de una investigación oportuna, eficaz e independiente sobre los hechos en La Modelo, dado su anterior cargo como ministra y el hecho de que pareciera tener una opinión formada sobre lo sucedido*”.** 

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/petrogustavo/status/1331234955389788162","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/petrogustavo/status/1331234955389788162

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
