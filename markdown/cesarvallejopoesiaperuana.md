Title: Un siglo y un cuarto de César Vallejo
Date: 2017-03-16 16:58
Category: Viaje Literario
Tags: 125 años, Cesar VAllejo, Latinoamérica, Perú, poesia
Slug: cesarvallejopoesiaperuana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cesar-vallejo2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Poetas del fin del mundo 

###### 16 Mar 2017 

Un día como hoy hace 125 años nació en Santiago de Chuco, uno de los poetas más importantes de las letras peruanas: Cesar Vallejo. De origen provinciano y mestizo fue una de las grandes figuras de la lírica hispanoamericana del siglo XX. Su obra esta cargada de una gran expresividad y desbordada sensibilidad que provenían de sus dolores personales y colectivos, creando un lenguaje muy autentico.

Varias de sus composiciones literarias se caracterizan por el uso de un léxico bíblico y litúrgico, ya que sus padres tenían planeado dedicarlo a la vida del sacerdocio, camino que no seguiría al igual que sus estudios universitarios. En 1915 entro a estudiar en la Universidad de San Marcos (Lima) Derecho y Filosofía y Letras en la Universidad de Trujillo, ambas carreas las dejo para dedicarse a ser maestro en Trujillo.

Cesar Vallejo incursiono en varios campos como: la dramaturgia, los ensayos y la narrativa con una de sus grandes obras “Paco Yunque”, pero inicio en el campo de la poesía y el modernismo con “Los heraldos negros”. Estableció su propio estilo literario con “Trilce”. y con las obras “España, aparta de mi este cáliz” y “Poemas Humanos” encontró la fama. Le puede interesar: [Manuel Cepeda y la memoria en sus poemas](https://archivo.contagioradio.com/manuel-cepeda-y-la-memoria-en-sus-poemas/)

En estos 125 años de historia, Vallejo nos ha dejado varias obras que deberiamos leer más de una vez, pues los sentimientos y anhelos de este gran poeta son una fuente de gran conocimiento e inspiración. Hoy en la fecha de su natalicio sería un gran homenaje empezar a disfrutar alguna de sus obras.

**Poema para ser leído y cantado**

Sé que hay una persona  
que me busca en su mano, día y noche,  
encontrándome, a cada minuto, en su calzado.  
¿Ignora que la noche está enterrada  
con espuelas detrás de la cocina?  
Sé que hay una persona compuesta de mis partes,  
a la que integro cuando va mi talle  
cabalgando en su exacta piedrecilla.  
¿Ignora que a su cofre  
no volverá moneda que salió con su retrato?  
Sé el día,  
pero el sol se me ha escapado;  
sé el acto universal que hizo en su cama  
con ajeno valor y esa agua tibia, cuya  
superficial frecuencia es una mina.  
¿Tan pequeña es, acaso, esa persona,  
que hasta sus propios pies así la pisan?  
Un gato es el lindero entre ella y yo,  
al lado mismo de su tasa de agua.  
La veo en las esquinas, se abre y cierra  
su veste, antes palmera interrogante...  
¿Qué podrá hacer sino cambiar de llanto?  
Pero me busca y busca. ¡Es una historia!
