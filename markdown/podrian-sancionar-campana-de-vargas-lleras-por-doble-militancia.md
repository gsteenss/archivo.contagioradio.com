Title: Podrían sancionar campaña de Vargas Lleras por doble militancia
Date: 2018-04-03 15:41
Category: Nacional, Política
Tags: alirio uribe, Cambio Radical, Vargas Lleras
Slug: podrian-sancionar-campana-de-vargas-lleras-por-doble-militancia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/german_vargas_lleras_5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [03 Abril 2018] 

El Consejo de Estado admitió la demanda interpuesta contra Germán Vargas Lleras para investigar si cometió el delito de doble militancia, debido a que, de acuerdo con Alirio Uribe, representante a la Cámara, **Vargas Lleras se presentó como candidato a partir de la recolección de firmas, y al mismo tiempo actuar como vocero del partido Cambio Radical**.

Según Alirio Uribe se pidió una medida de suspensión temporal para garantizar los derechos de los y las sufragantes, dado que la campaña tendría una irregularidad de comprobarse la razón de la demanda. Sin embargo se negó la medida cautelar, situación que sería violatoria del derecho electoral.

La falta del candidato se habría presentado cuando usó el mecanismo de la recolección de firmas para presentarse a las elecciones presidenciales, debido a que solo los candidatos que no tengan partido u organización política pueden realizarlo, y según Alirio Uribe, la creación de la campaña “Mejor Vargas Lleras”, **lo presentaba como si no tuviera relación alguna con el Partido Cambio Radical, sin aclarar su vinculación con el mismo**.

“Lo que estamos demandando es la resolución del Consejo Electoral, que le otorgó la inscripción de los símbolos” afirmó Alirio Uribe y agregó que “**es totalmente ilegal e inconstitucional”** la acción de respaldo hacia la candidatura de Vargas Lleras. (Le puede interesar: ["Propuesta de Duque tiene similitudes a poderes dictatoriales: Alfredo Beltrán"](https://archivo.contagioradio.com/propuesta-de-ivan-duque-tiene-similitudes-a-poderes-dictatoriales-alfredo-beltran/))

Así mismo, afirmó que se recolectaron pruebas que evidencian la campaña que también realizó Cambio Radical en apoyo a Vargas Lleras e incluso una recolección de firmas que habrían hecho senadores y representantes de este partido para el candidato y que se las habrían entregado en actos públicos. **El Consejo de Estado será quién tome la decisión, sin embargo, no hay un tiempo determinado en el que tenga que pronunciarse**.

<iframe id="audio_25054827" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25054827_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
