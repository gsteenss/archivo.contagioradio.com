Title: ¿Gobierno ambienta la ruptura del proceso de paz?
Date: 2015-07-06 16:59
Category: Otra Mirada, Paz
Tags: Carlos Lozano, Cese Bilateral de Fuego, Conversaciones de paz de la habana, FARC, Frente Amplio por la PAz, Humberto de la Calle
Slug: gobierno-ambienta-la-ruptura-del-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Humberto-de-la-calle-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elpueblo.com 

<iframe src="http://www.ivoox.com/player_ek_4729422_2_1.html?data=lZyfm5mWdo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRaaSmhqez1cqPqdTohqigh6aVb8Lhw87S0NnFssXjjNrbw5DWudHo1tfOjcnJsIzk09TQx9jTb8XZjNXO3IqXio6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Lozano Guillen] 

###### [6 Jul 2015] 

El pasado fin de semana se conoció una entrevista de “**balance del proceso de conversaciones de paz**” con el jefe del equipo de paz del gobierno nacional Humberto de la Calle, en la que se afirma que el proceso de conversaciones de paz está en crisis y a punto de ruptura, igualmente que "le ***queda poco tiempo, para bien o para mal"*** según de la Calle. Por otra parte se conoció  que un grupo de militares, con la justificación de las explosiones en Bogotá, le **pidieron expresamente al presidente Santos que se levantara de la mesa.**

**La “Inusual entrevista”**

En dialogo con el periodista Juan Gossaín , Humberto de la Calle, afirma que este es uno de los peores momentos del proceso de conversaciones y que una de las grandes dificultades es el escenario de guerra que se vive en nuestro país y que se agudiza después de la ruptura del cese unilateral por parte de las FARC. Sin embargo, para algunas analistas, no se ha tenido en cuenta que el gobierno decidió y ha reiterado que nada de lo que sucediera en Colombia afectaría las conversaciones de la Habana.

De la Calle, también afirma que es posible que se dé un cese bilateral de fuegos antes de la firma de los acuerdos y que uno de los principales obstáculos es la justicia, que en tratados internacionales contemplaría, según de la Calle, unas penas mínimas de cárcel para evitar la acción de tribunales internacionales, obviando lo que la propia fiscalía de la CPI ha dicho en relación a los crímenes imputados a las FARC.

**¿Qué hay detrás de las afirmaciones de Humberto de la Calle?**

El analista político, **Carlos Lozano**, afirma: *El proceso de paz pende de un hilo, sin embargo, las razones expresadas por De la Calle no son tan ciertas o profundas como los problemas*. Para Lozano la afirmación que las conversaciones están llegando a un punto de fondo, expresa la inconformidad de algunos sectores de poder que se oponen a perder sus beneficios a costas de los derechos de las víctimas.

Uno de los principales problemas fue el anunció del proceso con una guerrilla supuestamente derrotada, y la evidencia  indica que militarmente ha sido imposible derrotarlas, pero por otra parte el “ultimátum” del gobierno al proceso solo puede entenderse como una evidencia de las múltiples presiones de sectores militares que ven afectados su intereses en los temas que se discuten actualmente, asegura el analista.

``` 
Ultimatum del gobierno al proceso solo puede entenderse como una evidencia de las múltiples presiones de sectores militares que ven afectados su intereses en los temas que se discuten actualmente.
```

Según Lozano, esta “carta del fin del proceso” también la han usado estos sectores de derecha y ultra derecha en otros procesos anteriores tanto en el Caguán, como antes. Sin embargo, la salida que proponen algunos de la posibilidad de la renuncia del presidente, no cabe; se necesita que el presidente Santos cumpla su promesa de paz; que las FARC, no se levanten de la mesa, y que la sociedad colombiana defienda el proceso de paz a través de la movilización.
