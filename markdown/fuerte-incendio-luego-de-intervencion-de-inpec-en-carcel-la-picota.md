Title: Fuerte incendio luego de intervención de INPEC en cárcel La Picota
Date: 2020-03-17 23:13
Author: AdminContagio
Category: Actualidad, DDHH
Tags: carcel, INPEC
Slug: fuerte-incendio-luego-de-intervencion-de-inpec-en-carcel-la-picota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Incendio-en-La-Picota.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

La conflagración se habría iniciado luego de una intervención del Cuerpo de Reacción Inmediata, CRI, del INPEC en el patio UT, usualmente usada como calabozo, de la cárcel Erpon Picota en la ciudad de Bogotá. Según la información allegada a Contagio Radio por parte de los internos del centro penitenciario, el incendio se presenta a partir de las 9:30 pm y podría extenderse a otros patios si no hay intervención inmediata.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de las primeras [informaciones](https://twitter.com/CSPP_/status/1240111989529161733) a través de audios enviados por los internos, relata que se estaba desarrollando una jornada pacífica para evitar un encierro de 24 horas. Según relatan, los propios internos de La Picota decidieron retirarse a a sus celdas de manera pacífica, luego de ello habría ingresado el CRI de manera violenta hiriendo a varios internos, y destrozando sus pertenencias al interior de las celdas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lastimosamente este tipo de procedimientos son reiterados y las denuncias sobre abusos de autoridad y violaciones a los DDHH al interior de las cárceles también son reiterativos. Este tipo de intervenciones sumado a las condiciones de hacinamiento carcelario y las denuncias por desabastecimiento al interior de este centro en el mes de enero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según datos de la Defensoría del Pueblo el sistema penitenciario tiene capacidad para 80.203 reclusos, pero hay registro de 119.842 personas privadas de la libertad, es decir, 39.639 presos más que la capacidad del sistema; elevando las cifras de hacinamiento al 49,42 % hasta diciembre de 2019. (Le puede interesar: ["Prisionero político víctima de torturas en Cárcel La Picota"](https://archivo.contagioradio.com/prisionero-politico-victima-de-torturas-en-carcel-la-picota/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es de anotar que el hacinamiento carcelario es uno de los mayores problemas que afronta Colombia y una de las grietas del Sistema Penal colombiano, que muchas veces opta por la prisión como una medida incial para combatir el delito. Ya varios expertos, penalistas y analistas han señalado que frente a este problema es necesario revisar el funcionamiento del sistema de justicia punitiva que se aplica en Colombia.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
