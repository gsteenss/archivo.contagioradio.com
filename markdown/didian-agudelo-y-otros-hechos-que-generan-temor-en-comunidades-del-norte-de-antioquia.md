Title: Didian Agudelo y otros hechos que generan temor en comunidades del Norte de Antioquia
Date: 2020-03-23 18:36
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Antioquia, ejercito
Slug: didian-agudelo-y-otros-hechos-que-generan-temor-en-comunidades-del-norte-de-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Ejército-en-Anorí-y-caso-de-Didian-González-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @Ejercito\_Div7 {#foto-ejercito_div7 .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Asociación Campesina del Norte de Antioquia (ASCNA) denunció el miércoles 18 de marzo que en desarrollo de una operación del Ejército contra estructuras armadas ilegales en los municipios de Campamento, Angostura, Guadalupe y Anorí, uniformados irrumpieron de forma violenta en propiedades de campesinos y los señalaron de ocultar a cabecillas de los grupos ilegales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la denuncia, los hechos ocurrieron en la vereda La Milagrosa de Angostura, donde militares del Batallón Girardot ingresaron a una de las casas de manera violenta y agredieron a niños que se encontraban en el lugar, mientras les preguntaban a ellos y a un adulto mayor por los mandos de grupos armados ilegales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La acción se volvió a repetir en horas de la noche, en esta ocasión amenazaron a los residentes con expropiar la casa si no les daban información.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo invitamos a consultar: ["Comunidad responsabiliza al Ejército del asesinato y desaparición del líder Didian Agudelo"](https://archivo.contagioradio.com/comunidad-responsabiliza-al-ejercito-del-asesinato-y-desaparicion-del-lider-didian-agudelo/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/coeuropa/status/1240679103382110210","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/coeuropa/status/1240679103382110210

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **Por temor a represalias de los uniformados** campesinos guardan silencio ante abusos

<!-- /wp:heading -->

<!-- wp:paragraph -->

Gabriel Sánchez, encargado de derechos humanos de ASCNA, explicó que los hechos ocurrieron en el marco del Plan Cabuyo, creado por el Ejército para dar con el comandante de las Disidencias del Frente 36 que opera en la zona y lleva ese apodo, 'Cabuyo'. Sánchez afirmó que en el lugar de la denuncia que hizo la Asociación habitan tres familias, y hay una señora mayor de edad en silla de ruedas a la que los soldados iban a sacar, diciendo que estaban escondiendo al líder de las disidencias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el integrante de las ASCNA sostuvo que el Ejército se llevó a dos campesinos de un lugar cercano esta semana, y otros dos jóvenes de La Milagrosa que fueron dejados lejos de su casa. "Nos preocupa que quieren conseguir información a como dé lugar, y esas comunidades están pensando en desplazarse a cascos urbanos porque sigue la militarización" y los malos tratos a la comunidad, aseguró.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sánchez afirmó que estos hechos son constantes en la región, "algunos se han denunciado, pero en otros la población siente miedo de denunciar porque cada vez siente más las represalias de las tropas". (Le puede intereasr:["No convencen explicaciones del Ejército tras intento de retención a líder Nasa"](https://archivo.contagioradio.com/no-convencen-explicaciones-del-ejercito-tras-intento-de-retencion-a-lider-nasa/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Didian Agudelo y los compromisos en materia de DD.HH. en la región**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Contagio Radio se había comunicado recientemente con Sánchez a raíz de una manifestación que se desarrolló en Campamento y tenía por finalidad saber la verdad en el homicidio de Didian Agudelo, un campesino que fue desaparecido y posteriormente encontrado colgado en un árbol, cerca de donde habían tropas del Ejército.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tras la marcha se acordó que los campesinos no culparían al Ejército hasta que no concluyera la investigación de los hechos, que los medios de comunicación no estigmatizarían a la población y que se crearía una mesa con la Defensoría del Pueblo, Personería, integrantes de la Fuerza Pública y presencia de la Gobernación de Antioquia para verificar la situación humanitaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, Sánchez sostuvo que de la investigación sobre Didian no se ha sabido nada, y cuando al Ejército se le cuestiona por las actuaciones de sus hombres "niegan todo, pero la comunidad todos los días vive con miedo por el actuar de las tropas". (Le puede interesar:["Campesinos exigen que Ejército diga la verdad del asesinato de Didian Agudelo"](https://archivo.contagioradio.com/campesinos-exigen-que-ejercito-diga-la-verdad-del-asesinato-de-didian-agudelo/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
