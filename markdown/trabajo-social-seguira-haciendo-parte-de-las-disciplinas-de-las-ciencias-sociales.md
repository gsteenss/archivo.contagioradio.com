Title: Trabajo Social seguirá haciendo parte de las disciplinas de las Ciencias Sociales
Date: 2016-12-02 16:33
Category: Educación, Nacional
Tags: Ministerio de Educación, Movimiento estudiantil, Trabajo Social
Slug: trabajo-social-seguira-haciendo-parte-de-las-disciplinas-de-las-ciencias-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/trabajo-social1-e1477963157978.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Hermano Mayor ] 

###### [2 Dic 2016] 

Después de realizarse una Audiencia pública en donde diferentes sectores que hacen parte del gremio de Trabajo Social, expusieron sus argumentos para que la carrera de Trabajo Social permaneciera como estudio al interior de las Ciencias Sociales, el **Ministerio de Educación le dio una reversa a su decisión** de ubicar esta carrera en el área de Salud y a**cepto dejarla como se encontraba anteriormente, sin modificar su campo de acción.**

El cambio de decisión de acuerdo a los estudiantes, se debe en principio a la capacidad de movilización que tuvieron desde que se dio a conocer la intensión por parte del Ministerio de Educación y a la **presión social y política que se habría generado con acompañamientos como el del representante a la cámara Víctor Correa**. Le puede interesar: ["Trabajo Social apunto de extinguirse como profesión dentro de las Ciencias Sociales"](https://archivo.contagioradio.com/trabajo-social-apunto-de-extinguirse-como-profesion-dentro-de-las-ciencias-sociales/)

De acuerdo con Juan Rodríguez, representante de la carrera Trabajo social en la Universidad Nacional, esta noticia significa una victoria para la organización de los estudiantes **“preparamos todos los escenarios para que esta lucha con el Ministerio y DANE estuviese llena de argumentos”**. Sin embargo Rodríguez afirma que existirían otras áreas y carreras que el Ministerio de Educación querría modificar y sacar de sus disciplinas actuales.

Rodriguez afirma que lastimosamente la movilización en defensa de la carrera de Trabajo Social no tuvo mucha solidaridad por parte de estudiantes de otras carreras, sin embargo expresa que después de haber pasado por esta experiencia están dispuestos a acompañar a sus compañeros en el momento en el que deban movilizarse para defender sus carreras.

<iframe id="audio_14456106" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14456106_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
