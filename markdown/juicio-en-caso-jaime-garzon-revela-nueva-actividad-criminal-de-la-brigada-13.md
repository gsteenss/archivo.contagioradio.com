Title: Juicio en caso Jaime Garzón revela nueva actividad criminal de la Brigada 13
Date: 2019-02-13 14:56
Author: AdminContagio
Category: DDHH, Judicial
Tags: Crimen de Estado, das, Jaime Garzon
Slug: juicio-en-caso-jaime-garzon-revela-nueva-actividad-criminal-de-la-brigada-13
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/5a5e39d83ae58.r_1534284317572.0-403-2005-1406-770x400-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 13 Feb 2019 

**Sebastián Escobar**, abogado de la familia de **Jaime Garzón** se refirió a los avances en el juicio que se adelanta contra el coronel (r) **Jorge Eliecer Plazas Acevedo** implicado en el asesinato del periodista y explicó que a raíz de la investigación han surgido nuevas hipótesis en especial las relacionados a la actividad criminal de la Brigada 13 del Ejército la que mediante su grupo de vigilancia y seguimiento cometió una serie de delitos en la ciudad de Bogotá como preludio al asesinato del también humorista.

**Según el abogado, dichos delitos incluyen varios secuestros cometidos por la fuerza pública**, lo que ha permitido establecer un posible nexo entre la Brigada 13  y las Farc, tal relación habría sido mediada por el Ejército para infiltrarse en el grupo armado. Jaime, quien participó en procesos de liberación de secuestrados, pudo haber conocido el vínculo entre las partes y este habría sido otro de los motivos para acabar con su vida.

Esta hipótesis que involucra a la Brigada 13 ha logrado ser probada en el proceso **al presentar otras pruebas relacionadas al asesinato del empresario Benjamín Khoudari en 1998 y el secuestro de Martha Cecilia Velásquez en 1999**. [(Lea también: Las investigaciones que siguen por el magnicidio de Jaime Garzón) ](https://archivo.contagioradio.com/las-investigaciones-magnicidio-jaime-garzon/)

### **La evolución de un caso de más de 20 años** 

Para el abogado resulta difícil valorar la actuación de la Fiscalía, teniendo en cuenta que el ente de control desvió la investigación y una "actuación  deficiente e incluso en ocasiones cómplice" con el DAS

A pesar de ello rescata que "se ha consolidado una investigación con las dificultades propias de lograr esclarecer unos hechos ocurridos hace 20 años" y que esta tomó otro rumbo cuando fueron señaladas  **diferentes personas vinculadas al establecimiento, como el  exsubdirector del DAS, José Miguel Narváez, condenado  a 30 años de prisión y el coronel (r) Jorge Eliecer Plazas Acevedo contra quien se adelanta un juicio por el asesinato de Jaime.**

[La Fiscalía solicitó la condena de Plazas Acevedo con base en la relación que tuvo con las autodenominadas Autodefensas Unidas de Colombia (AUC), y cómo este puso a disposición de este aparato criminal, la oficina de Inteligencia de la Brigada 13 la cual dirigía en aquel entonces.   
  
Por su parte, la familia del humorista reiteró que este crimen debe ser declarado de lesa humanidad como ya lo han hecho la Fiscalía y el Consejo de Estado, además de solicitar la condena del coronel Plazas Acevedo.]

Escobar precisa que no existe un plazo para emitir un fallo para decidir la culpabilidad de Plazas Acevedo y que en este momento se está discutiendo la competencia de la JEP en el caso sin embargo señala que durante el fallo anterior contra José Miguel Narvaez, el juzgado se tomó casi más de dos años en tomar una decisión, situación que podría repetirse.

### **¿Qué pasa con los demás implicados en el caso Jaime Garzón?** 

En cuanto  a los demás señalados de participar en el crimen,  el abogado señala que algunos de los investigados han solicitado acogerse formalmente a la JEP, condición que puede generar dificultades en el avance de la justicia ordinaria si la decisión es aceptada por la sala de apelaciones de la **Jurisdicción Especial para la Paz.**

Además enfatizó que aunque siempre han considerado que Plazas Acevedo tenía un rol importante dentro de esta estructura conformada por paramilitares e integrantes de la fuerza pública, "nunca ha sido considerado como un integrante determinante" lo que lleva a concluir que aún hay elementos por desentrañar de este aparato criminal.

<iframe id="audio_32518204" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32518204_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
