Title: Universidad de Antioquia entra en paro indefinido
Date: 2015-10-09 18:05
Category: Educación, Movilización
Tags: Cambio de examen de admisión en la universidad de Antioquia, educacion, Mauricio Alviar rector de la universidad de Antioquia, Turbo, Universidad de Antioquia
Slug: universidad-de-antioquia-entra-en-paro-indefinido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/manifestaciones.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### www.Flickr.com 

###### [09 oct 2015] 

En paro indefinido se declaró un sector de estudiantes de la Universidad de Antioquia, ante la negativa de las directivas de derogar el acuerdo 48 del Concejo Académico por medio del cual se modificaba el cambio de examen de admisión a la Institución de educación superior.

La Asamblea Estudiantil asegura  que los argumentos del rector para cambiar el examen no son válidos. Citando un estudio sobre deserción realizado por la Universidad Nacional sede Medellín, afirma que “el problema de **deserción no depende del tipo de prueba de admisión** que se realice a los aspirantes, el estudio muestra que la deserción se presenta uniformemente en todas las instituciones de educación superior del país con una media del 48 por ciento”.

Ante el paro de los estudiantes, el Consejo académico informó mediante un comunicado que “La universidad no quiere el paro, no se somete a él, no está dispuesta a que se le imponga, **los estudiantes no pueden parar indefinidamente. La universidad es de ellos para abrirla, deja de pertenecerles si la bloquean”.**

Por otro lado, estudiantes de la Universidad de Antioquia en Turbo, denuncian que cuando empiezan los aguaceros en su municipio, los alumnos y docentes deben realizar sus clases en medio del agua, que a veces les llega a los talones, pero en otras ocasiones los aseguran que el agua puede llegar hasta las rodillas.

Es por eso que algunos universitarios han decidido reclamar y exigir mejores condiciones en las instalaciones para poder estudiar y aunque se han interpuesto derechos de petición en los que pide explicaciones y soluciones a la universidad, aun no hay respuestas claras.

.
