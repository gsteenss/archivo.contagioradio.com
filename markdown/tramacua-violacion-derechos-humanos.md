Title: La Tramacúa una violación a los derechos humanos
Date: 2018-07-24 13:20
Category: Expreso Libertad
Tags: presos politicos, tramacua
Slug: tramacua-violacion-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/tramacua13.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Pilón 

###### 24 Jul 2018 

En este programa del Expreso Libertad, se conocerán con mayor profundidad las difíciles condiciones de los reclusos de la Cárcel de Valledupar, más conocida como Tramacúa o el Guantánamo de Colombia. Un lugar que de acuerdo con la Corte Constitucional, ha violado los derechos humanos de la población reclusa, sometiéndolos a temperaturas que alcanzan los 40 grados, a un acceso restringido al agua y donde  eran trasladados los prisioneros políticos que defendía derechos humanos en otras cárceles del país.

\[caption id="attachment\_55495" align="alignnone" width="800"\]![infografia carceles](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/tramacua-800x600.png){.size-medium .wp-image-55495 width="800" height="600"} <iframe id="audio_27216824" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27216824_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>\[/caption\]

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
