Title: Quinto capítulo Radionovela Partículas de Libertad
Date: 2020-12-18 14:26
Author: CtgAdm
Category: Expreso Libertad
Tags: #Expresolibertad, #Judicialización, #PresosPolíticos, #Radionovela
Slug: quinto-capitulo-radionovela-particulas-de-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 15 de diciembre, el **Expreso Libertad** lazó el quinto y último capítulo de su radionovela **«Partículas de Libertad»**, una historia que relató el montaje judicial creado por un agente de la policía en contra de 5 estudiantes de universidades públicas.

<!-- /wp:paragraph -->

<!-- wp:html /-->

<!-- wp:paragraph -->

En este capítulo Juan Diego y Felipe deberán investigar qué sucede con su amigo Alberto y las reuniones que mantiene con guardias de la cárcel, mientras que Sofía y Araminta deberán hacer lo posible por intentar develar que hay detrás de su montaje judicial.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el lanzamiento de este programa participaron Érika Aguirre y Xiomara Torres, mujeres víctimas del montaje judicial conocido como el caso Lebrija. Ambas reflexionaron en torno al ejercicio de memoria que hace esta radionovela y la importancia de **contar y visibilizar la situación de las y los prisioneros políticos en Colombia.**

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Ffb.watch%2F2wrAtUOsKG%2F&amp;width=600&amp;show_text=false&amp;height=338&amp;appId" width="600" height="338" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowfullscreen="true"></iframe>  
<!-- /wp:paragraph -->

<iframe id="audio_62515865" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_62515865_4_1.html?c1=ff6600"></iframe>

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Vea más de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
