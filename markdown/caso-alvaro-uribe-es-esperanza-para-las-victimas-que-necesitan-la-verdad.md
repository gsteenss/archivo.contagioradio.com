Title: Caso Álvaro Uribe es esperanza para las víctimas que necesitan la verdad
Date: 2020-08-10 21:56
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Álvaro Uribe Vélez, Corte Suprema de Justicia, Ejecuciones Extrajudiciales, madres de soacha, Operación Orión
Slug: caso-alvaro-uribe-es-esperanza-para-las-victimas-que-necesitan-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Víctimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: víctimas de ejecuciones extrajudiciales /MAFAPO

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La decisión tomada por la Corte Suprema de Justicia al dictar medida de aseguramiento y detención domiciliaria contra el expresidente Álvaro Uribe Vélez, fue recibida de forma positiva por parte de las víctimas de masacres, desapariciones forzadas, desplazamientos y ejecuciones extrajudiciales ocurridas bajo su mandato. Aunque esta es tan solo una parte del proceso, las personas afectadas claman por justicia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre la decisión tomada por la Corte, Carmenza Gómez una de las [Madres de Soacha](https://twitter.com/MAFAPOCOLOMBIA)y madre de Víctor Fernando Guerrero Gómez y John Nilson Gómez, víctimas de ejecución extrajudicial por parte del Ejército señala que el fallo fue recibido con un poco de satisfacción, por lo que espera el proceso siga sin contratiempos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Por una verdad, justicia y garantías de no repetición, no queremos ver más familias llorando, , queremos que la muerte de nuestros hijos no quede en la impunidad, que sea limpiada en nombre de ellos, ellos no eran guerrilleros", afirma Carmenza. [(Le puede interesar: Corte Suprema de Justicia ordena medida de aseguramiento contra Álvaro Uribe Vélez)](https://archivo.contagioradio.com/corte-suprema-de-justicia-ordena-medida-de-aseguramiento-contra-alvaro-uribe-velez/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque no hay una cifra unánime, según el informe “Muertes ilegítimamente presentadas como bajas en combate por agentes del Estado" que la Fiscalía General le entregó a la JEP en agosto de 2018, las víctimas ascienden a 2248,​ para organizaciones sociales como la Coordinación Colombia Europa Estados Unidos **señalan que este registro asciende a las 10.000 víctimas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Estamos peleando con un pez grande, con un tiburón, no solo son los jóvenes de Soacha y Bogotá, sino todos aquellos que fueron muertos en combate, campesinos e indígenas que los sacaba el Ejército y los pasaba como positivismos para cumplirle a los mayores, como Mario Montoya".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Carmenza afirma que fueron nueve años de espera en audiencias en la justicia ordinaria y aunque en 2018 comenzaron a escuchar las verdades de los integrantes del Ejército que se acogieron a la JEP, es necesario que se haga justicia, la que incluya además de los altos comandantes, al expresidente Uribe y demás miembros de su entonces gabinete.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/MAFAPOCOLOMBIA/status/1290865265723531273","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/MAFAPOCOLOMBIA/status/1290865265723531273

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Las víctimas necesitan saber la verdad

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Luz Helena Galeano, integrante de Mujeres Caminando por la Paz y víctima de la Operación Orión,** ocurrida entre el 16 y 18 de octubre del año 2002, cuando 1.500  hombres de la Fuerza Pública, en conjunto con estructuras paramilitares  ingresaron a la Comuna 13 arremetiendo contra la población civil, señala que este fue un día histórico para las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"El pronunciamiento de la Corte lo vemos como una luz de esperanza para que se abran casos que han sido cerrados, hay que decir que toda la Comuna 13 fue víctima indirecta por lo que esperamos que la Corte siga investigando los casos en contra de Alvaro Uribe Vélez" no solo la Comuna 13 sino otros como la Masacre del Aro o La Granja.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe recordar que Orión, operación ocurrida bajo la orientación general Mario Montoya, comandante de la Brigada IV durante el gobierno de Uribe Vélez, comandante en jefe de las Fuerzas Militares dejó un resultado de **17 homicidios cometidos por el Ejército, 71 personas asesinadas por paramilitares, 12 personas torturadas, 370 detenciones arbitrarias y 6 desapariciones forzadas.** [(Lea también: Operación Orión ¡Nunca Más!, 17 años buscando justicia y verdad)](https://archivo.contagioradio.com/victimas-operacion-orion/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el general Mario Montoya como el entonces general de la Policía Metropolitana de Medellín, Leonardo Gallego, son investigados por connivencia con grupos paramilitares, aún no hay avances en términos de justicia, ni tampoco se ha reconocido la responsabilidad del entonces presidente Uribe.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Hay que crear conciencia hacer memoria para que la gente que no lo conoce y no lo ha vivido lo sienta"
>
> <cite>Luz Helena Galeano</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Para **Camilo González Posso, director del Instituto de estudios para el Desarrollo y la Paz,** hay una serie muy grande de procesos que tienen implicaciones para el expresidente Uribe, por lo que el pedido de las víctimas es una "justa aspiración a justicia" sin embargo señala cómo las víctimas logran llegar a la conclusión en común de cómo se asocian los asesinatos al accionar del Estado y a unas políticas definidas desde la cúpula más alta del poder ejecutivo y que ha determinado prácticas concretas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"La autorización de las Convivir, los que auspiciaron el paramilitarismo y los que tomaron la decisión de realizar una guerra con la idea de que todo es legítimo para salvar a la patria, ese discurso se impuso en Colombia pero aquí hay una responsabilidad política

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el analista señala que estos interrogantes no se van a resolver en este proceso, sí r**esalta la responsabilidad que tendrá la Jurisdicción Especial para la Paz (JEP) para llegar a una verdad judicial,** "está muy bien que nos hagamos ilusiones que salgan algunas verdades en un proceso como este, pero esto es un proyecto de más largo aliento, que se va a alimentar de situaciones como estas".[(Le puede interesar: La relación entre Chiquita Brands, Álvaro Uribe y las Convivir)](https://archivo.contagioradio.com/la-relacion-entre-chiquita-brands-alvaro-uribe-y-las-convivir/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
