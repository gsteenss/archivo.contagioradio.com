Title: Alejandro Carvajal, joven de 20 años asesinado por el Ejército en Catatumbo: ASCAMCAT
Date: 2020-03-27 17:38
Author: AdminContagio
Category: Nacional
Slug: alejando-carvajal-joven-de-20-anos-asesinado-por-el-ejercito-en-catatumbo-ascamcat
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/30d8e3ee-92b7-4dc0-ba12-064197cadd15.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El 26 de marzo cerca de las 3 p.m. se denunció el asesinato de Alejandro Carvajal, joven de 20 años, en la vereda Santa Teresita, municipio de Sardinata, Norte de Santander, según la organización ASCAMCAT por integrantes del [Ejercito Nacional](https://archivo.contagioradio.com/piden-cese-de-operaciones-militares-para-frenar-la-propagacion-del-covid-19/).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AscamcatOficia/status/1243385430520676353","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AscamcatOficia/status/1243385430520676353

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según Holman Pérez, integrante de la Asociación Campesina del Catatumbo (ASCAMCAT); **Alejandro Carvajal** fue asesinado en su casa y era sobrino de una dirigente muy reconocida de la región campesina del [Catatumbo,](https://archivo.contagioradio.com/un-pacto-humanitario-lo-minimo-para-proteger-las-vidas-en-catatumbo/) y agregó "*este no es un hecho aislado nosotros habíamos advertido sobre los riesgo inminente que tenían las comunidades en medio de los operativos de erradicación*".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la gravedad de los hechos, la oficina de comunicaciones del Ejercito Nacional, publicó un comunicado donde señalaba que en medio de hechos confusos entre un integrante del batallon N9 de operaciones terrestres había muerto un habitante de la región, documento que según Pérez luego fue eliminado.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**La erradicación no viene sola estos procesos vienen acompañados con una serie de violaciones a los Derechos Humanos**, como capturas desplazamiento y muerte, llenando de miedo a la población"*
>
> <cite> Holman Pérez|ASCAMCAT </cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### "Nosotros anunciamos esta crisis y nadie nos escuchó"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Pérez señaló desde el 11 de marzo se registró el ingreso a la región del Catatumbo, específicamente en el corregimiento las Mercedes, 500 a 600 hombres, entre erradicadores y Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente el 16 de marzo y ante la falta de respuesta por parte del Gobierno a los hechos que ocurrían en el territorio, campesinos bloquearon la vía, Cúcuta-Tibú y Cúcuta-Ocaña, acción que fue detenida por las autoridades regionales al empezar la crisis de [Covid-19](https://www.justiciaypazcolombia.com/emberas-en-crisis-alimentaria-y-de-salud-por-confinamiento-de-agc-y-covid19/), en donde acordaron con los campesinos un cese de todas las actividades, entre ellas la erradicación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"El campesinado se comprometió a regresar a sus territorios para evitar la propagación del virus, ellos cumplieron, pero el Ejército no"*, afirmó el vocero; agregó que ese mismo día se registraron disparos por parte de la Fuerza Pública hacia los campesinos que se movilizaban, y días después, *"el 20 de marzo en el corregimiento de la Merced el Ejército agredió y amenazó a las personas con dispararles".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "El asesinato de Alejando Carvajal hubiéramos podido evitarla si el gobierno nacional hubiese cumplido"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Finalmente el vocero campesino agregó que la muerte de **Alejandro Carvajal**, así como los otros hechos de violencia se hubieran podido evitar si, el Gobierno hubiera cumplido desde un inicio con lo firmado en el Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Frente a la crisis generada por el coronavirus, señaló, "*el Gobierno pide que nos aislemos que detengamos las actividades que estamos haciendo, pero él hace todo lo contrario, **le pedimos días atrás detuviera la erradicación, pero en respuesta nos enviaron 1.000 militares más"***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Afirmó que su exigencia es y siempre será que, "por la vía forzada no vamos a lograr nada, esa es una política ya fracasada, porque la gente siempre vuelve a sembrar, por miedo y necesidad". Leer: ["Con políticas del uribismo seguiremos entregando los territorios a la ilegalidad"](https://archivo.contagioradio.com/con-politicas-del-uribismo-seguiremos-entregando-los-territorios-a-la-ilegalidad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y añadió que hoy la comunidad siente miedo e incertidumbre por la presencia militar y los recientes hechos violentos, y por otro preocupación ante la crisis de la pandemia con un sistema de salud mediocre, y una orden de aislamiento social, con miles de hombres del Ejercito saliendo y entrando sin proteccion de su territorio.

<!-- /wp:paragraph -->
