Title: Oleada de amenazas y homicidios en Colombia es escalofriante: Amnistía Internacional
Date: 2017-03-22 11:25
Category: DDHH, Nacional
Tags: Amnistía Internacional, Asesinados, colombia, paramilitares, paz, San josé de Apartadó
Slug: oleada-de-amenazas-y-homicidios-en-colombia-es-escalofriante-amnistia-internacional
Status: published

###### [Foto: Archivo] 

###### [22 Mar. 2017 ]

**Como “escalofriante” definió este miércoles Amnistía Internacional la oleada de amenazas, homicidios y desplazamientos** que se están recrudeciendo en todo el territorio colombiano. A través de un comunicado Erika Guevara-Rosas, directora para las Américas de Amnistía Internacional, aseveró que “el conflicto armado dista de haber concluido, incluso meses después de la firma del Acuerdo de Paz”.

Además, Guevara-Rosas aseguró que es alarmante ver cómo, **en grandes zonas de Colombia, el conflicto armado está más vivo que nunca** “en todo el país hay cientos de miles de personas que aún no han notado ninguna diferencia en sus vidas desde que se firmaron los acuerdos de paz”.

En la misiva, Amnistía pone de manifiesto tan solo un ejemplo de los múltiples que suceden en Colombia, citan lo que ha sucedido recientemente con **la Comunidad de Paz de San José de Apartado, históricamente asediada por el conflicto armado interno**, pero que también ha trabajado por lograr salvaguardar la vida en el territorio. Le puede interesar:[ Cambios en los Acuerdos de Paz no garantizan derechos de las víctimas: Amnistía](https://archivo.contagioradio.com/amnistia-internacional-reconoce-presencia-paramilitar-en-colombia/)

“La Comunidad de Paz de San José de Apartadó muestra la manera en que, **durante décadas, la población colombiana, prácticamente sola, ha luchado valientemente por la justicia.** Sus miembros son un ejemplo de la lucha para proteger los derechos humanos, tan fundamentales para todas las personas en Colombia” puntualizó la directora.

Así como la Comunidad de Paz de San José de Apartadó, en Colombia existen iniciativas como las **Zonas Humanitarias en el Chocó reconocidas antes la CIDH como mecanismo para salvaguardar la vida de las comunidades y de los territorios.**

Además en la comunicación se ponen de manifiesto los ataques de los cuales son víctimas pobladores de diversos lugares del país dentro de los que se encuentran **“ataques, tortura, abusos sexuales y desplazamiento forzado a manos de todas las partes enfrentadas”** agrega Amnistía. Le puede interesar: [Impunidad, la mayor preocupación de organizaciones de DDHH con la JEP](https://archivo.contagioradio.com/impunidad-mayor-preocupacion-de-organizaciones-37994/)

De igual manera, Amnistía asegura que **persiste la presencia paramilitar en los territorios del país,** recalcando el incremento de la actividad de las autodenominadas Autodefensas Gaitanistas de Colombia en San José de Apartado. Le puede interesar: [En CIDH evidencian tareas pendientes del Estado para el desmonte del paramilitarismo](https://archivo.contagioradio.com/cidhparamilitarismoestadoddhh/)

**“Ya es hora de que las autoridades colombianas se enfrenten a la realidad y reconozcan que el conflicto sigue** causando estragos a cientos de miles de personas vulnerables. Cuanto más se demore la acción, más vidas se perderán”, concluyó Erika Guevara-Rosas. Le puede interesar: [Colombia sin garantías en la defensa de los derechos Humanos](https://archivo.contagioradio.com/colombia-sin-garantias-en-la-defensa-de-los-derechos-humanos/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
