Title: Maestros, trabajadores de la salud y estudiantes se toman Bogotá
Date: 2015-04-27 16:42
Author: CtgAdm
Category: Educación, Nacional
Tags: amaury nuñez, Antoc, educacion, fecode, maestros, magisterio, mane, ministerio, paro, Salud, Universidad Nacional, william agudelo, yesid camacho
Slug: maestros-trabajadores-de-la-salud-y-estudiantes-se-toman-bogota
Status: published

###### Foto: Contagio Radio 

Maestros, trabajadores de la salud y estudiantes se tomaron Bogotá este lunes 27 de abril, en una movilización que partió de 5 puntos: el Parque Nacional, el Ministerio de Salud, la Universidad Nacional, la Universidad Pedagógica Nacional y La Sevillana, hacia el Ministerio de Educación, ubicado en la Calle 26 con 57.

A la convocatoria realizada por Fecode; que viene adelantando un proceso de negociación con el Ministerio de Educación Nacional para exigir el cumplimiento del pliego de exigencias presentado desde hace más de un año al gobierno nacional; se sumaron los estudiantes de la Universidad Pedagógica Nacional, quienes al ser estudiantes y futuros docentes aseguran que esta problemática los afecta en dos vías.

<iframe src="http://www.ivoox.com/player_ek_4415452_2_1.html?data=lZmel5mZdo6ZmKiakpqJd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLZ1Nnf0diJdqSf1dfOxMbOpcXj08rgjcnJb83VjNjOztrIb9qfxtjh18nNpc%2FoxtiY1cqPuNDhjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [William Agudelo, Asociación Distrital de Educadores] 

A la movilización acudieron también los trabajadores de la salud, quienes aseguran que todos los trabajadores estatales son mal pagos en Colombia, que los trabajadores de la salud son los peor pagos y que la lucha fundamental que emprenden estos últimos, es por la existencia de los hospitales públicos, rechazando el modelo de financiación en salud que los está llevando a la quiebra.

<iframe src="http://www.ivoox.com/player_ek_4415464_2_1.html?data=lZmel5maeI6ZmKiakpuJd6KnlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLZ1Nnf0diJdqSf1dfOxMbOpcXj08rgjcnJb83VjNjOztrIb9qfxtjh18nNpc%2FoxtiY1cqPuNDhjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Yesid Camacho, ANTHOC] 

El último sector que se sumó a la Toma de Bogotá surgió en la Universidad Nacional de Colombia, que cumple su 3ra semana en paro por cuenta de las negociaciones entre la administración de la institución y los trabajadores, que exigen también una nivelación salarial equitativa. Estudiantes y profesores han apoyado a los trabajadores, con dinámicas de movilización y anormalidad académica, tanto en la Sede Bogotá como en la Sede Medellín.

<iframe src="http://www.ivoox.com/player_ek_4415469_2_1.html?data=lZmel5mafY6ZmKiakpuJd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLZ1Nnf0diJdqSf1dfOxMbOpcXj08rgjcnJb83VjNjOztrIb9qfxtjh18nNpc%2FoxtiY1cqPuNDhjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Amaury Nuñez, Representante Estudiantil CSU Universidad Nacional] 

La jornada de movilización culminó con un plantón frente a las instalaciones del Ministerio de Educación Nacional, en que las y los maestros ratificaron el paro indefinido, y rechazaron las presiones de la Ministra para regresar a las clases. Aseguran que fueron los incumplimientos del gobierno los que han llevado a que se viva esta situación en Colombia.

\
