Title: Denuncian trasteo de votos en López de Micay, Cauca
Date: 2015-08-21 14:29
Category: Nacional, Política
Tags: 25 de octubre, Cauca, Conpaz, corrupción, elecciones, López de Micai, Partido Conservador, trasteo de votos
Slug: denuncian-trasteo-de-votos-en-lopez-de-micay-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/7341.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.cne.gov.co]

<iframe src="http://www.ivoox.com/player_ek_7291631_2_1.html?data=mJemk5uXdY6ZmKiakp2Jd6KnmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbi1tPQy8bSb9Xmwtjhx9SPqMaf19Th0diPqc%2BfrYqwlYqmd9HZ25DRx5CxrcTV2oqfpZCnpdbXwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Manuel Garcés, candidato a la Alcadía López de Micay CONPAZ] 

###### [21 Ago 2015]

Manuel Garcés, candidato a la alcaldía  del municipio López de Micay, en el departamento del Cauca, denuncia que la campaña política de **Wilmer Elver Riascos, del Partido Conservador** es financiado con dineros del narcotráfico de grupos ilegales. Según su denuncia, **entre 800  y 1000 personas estarían siendo trasladadas de ciudades como Popayán, Cali y Buenaventura,** con el objetivo de ganar las elecciones.

"En las elecciones pasadas  yo ocupé el segundo lugar en las aspiraciones a la  alcaldía, donde nos ganaron por 173 votos, el candidato que quedó de cuarto  ahora está haciendo un trasteo electoral con miras a la alcaldía", de acuerdo con Garcés "**no se entiende como un candidato puede trastear más de 500 personas"**, cuando cada una de ellas podría conllevar un costo de 700 mil pesos.

Pero esa no es la única problemática en el marco de las elecciones de este municipio del país, ya que el integrante de CONPAZ, asegura que el candidato del Partido Conservador, ha amenazado la vida de él otras personas que hacen parte de su grupo de campaña, entre ellos un primo a quien asesinaron.

**"Hemos recibido amenazas a líderes y a los cabildos indígenas,** en mi caso personal me salvé de morir, ya habían pagado 50 millones de anticipo para matarme y habían acordado que era una suma de 100 millones pesos, acabo de recibir un reporte del corregimiento de Suárez del municipio de López de Micay donde le mandan un mensaje a uno de los líderes de mi comunidad que tiene que retirarse de la campaña, en los últimos meses aliados de esa campaña **han asesinado a un primo mío Oscar Garcés y otro líder juveni**l, además uno de mis concejales está desaparecido", expresa Manuel Garcés.

Esta situación fue comunicada a la Misión de Observación electoral, MOE, con el fin de obtener orientaciones jurídicas para instaurar la denuncia, así mismo, el líder social hace un llamado  a la comunidad a que se pronuncie ante la corrupción en medio de las campañas electorales. Por el momento, las autoridades no se han manifestado.
