Title: Histórica legalización de aborto en Argentina
Date: 2020-12-30 16:24
Author: CtgAdm
Category: Actualidad, El mundo, Mujer
Slug: historica-legalizacion-de-aborto-en-argentina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Aborto-legal-pcr.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El [Senado de Argentina](https://twitter.com/SenadoArgentina) legalizó la ley del aborto voluntario con 38 votos a favor y 29 en contra. Con esta decisión el país se suma al pequeño grupo de países donde se permite que las mujeres soliciten el aborto legal, entre los que se encuentran Uruguay, Cuba, Guyana, Guayana Francesa, Puerto Rico y algunas partes de México.     

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El gobierno de Argentina calcula, que hay entre 370.000 y 520.000 abortos clandestinos anuales. De igual manera, estiman que más de 3000 mujeres perdieron la vida por abortos inseguros.

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/SenadoArgentina/status/1344179887515586561","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/SenadoArgentina/status/1344179887515586561

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

La sociedad argentina, se siente esperanzada gracias a que con esta decisión se  podrá tener cifras exactas sobre el aborto en el país, lo que permitirá generar mejores políticas públicas en materia de salud y otorgarle a las mujeres el derecho a decidir si quieren o no ser madres.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Acciones claves para lograr la aprobación de la ley

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las acciones de movilización social por parte del movimiento feminista en argentina, fueron fundamentales. Ya que, en los últimos años la despenalización del aborto se convirtió en la bandera más importante de este grupo, quienes también han apoyado en el pasado iniciativas para la igualdad de derechos y la protección de los niños.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este movimiento ha ganado notoriedad frente al congreso argentino, dada la presión mediática y legislativa que han logrado extender para que sus propuestas y opiniones sean tenidas en cuenta dentro de la agenda legislativa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Uno de los componentes para el éxito de la aprobación de esta ley fue el hecho de que algunos congresistas cambiaran su voto. El cambio de mentalidad que han tenido alguno de estos congresistas fue decisivo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los senadores argumentaron haber cambiado su parecer con argumentos como "***Desde la votación de 2018 he reflexionado, he analizado. La penalización no logra evitar que muchas mujeres realicen esta práctica y más aún en la clandestinidad. En efecto, la intervención del Estado, en este momento, es fundamental***" diputada, Flavia Morales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual manera, el apoyo por parte del presidente le dio peso y notoriedad a la aprobación de la ley.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***&lt;&lt;La criminalización del aborto de nada ha servido, solo ha permitido que los abortos ocurran clandestinamente en cifras preocupantes***&gt;&gt;.
>
> <cite>Presidente argentino, Alberto Fernández</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### En qué consiste la ley del aborto voluntario

<!-- /wp:heading -->

<!-- wp:list -->

-   El aborto se permite hasta la semana catorce. Después de este, solo se podrá acceder al aborto circunstancias de violación o si está en peligro la vida o la salud de la madre.
-   La persona gestante deberá expresar su consentimiento por escrito.
-   Para las menores de 13 años se requiere un consentimiento informado y la asistencia de por lo menos uno de sus progenitores.
-   El profesional de salud tiene el derecho a ejercer la objeción de conciencia.
-   Si una institución privada no cuenta con profesionales para realizar la interrupción del embarazo, dicha institución deberá ayudar en el proceso de comunicación con otra institución.
-   Se deberá mantener la privacidad y el derecho a la intimidad de quienes recurran a las instituciones médicas para solicitar un aborto.

<!-- /wp:list -->

<!-- wp:paragraph -->

Le puede interesar leer: [La lucha feminista sigue por la despenalización del aborto en Colombia](https://archivo.contagioradio.com/la-lucha-feminista-sigue-por-la-despenalizacion-del-aborto-en-colombia/)

<!-- /wp:paragraph -->
