Title: Con Diálogos para la No Repetición, Comisión de la Verdad promoverá defensa de líderes sociales
Date: 2019-06-07 16:32
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: comision de la verdad, Diálogos para la No Repetición, lideres sociales
Slug: con-dialogos-para-la-no-repeticion-comision-de-la-verdad-promovera-defensa-de-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Líder-social.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de la Verdad ] 

Bajo el nombre de **‘Larga vida a los hombres y mujeres líderes sociales, y defensores de derechos humanos'**  la Comisión de la Verdad dará inicio este 11 de junio a los **Diálogos para la No Repetición,** una conversación centrada en los riesgos que viven los líderes y lideresas sociales en Colombia y que busca responder las preguntas que el país no afrontado, relacionadas a las causas y consecuencias del conflicto armado.

A través de este escenario en el que participarán diversos actores sociales e institucionales se busca promover una interlocución que brinde una mayor comprensión y reconocimiento de las violencias que padecen aquellos que defiende la paz en sus territorios.

**El presidente de la Comisión de la Verdad, el padre Francisco de Roux** señaló que han querido centrarse en la situación que viven los líderes sociales pues existe una sensación de que "el problema no ha sido encarado a fondo" y no se han revisado las razones políticas, estructurales y culturales que han ocasionado los ataques contra los líderes sociales incluso, después de finalizado el conflicto armado con las Farc.

**La luchas por la tierra, el problema del narcotráfico y el problema de la defensa del medio ambiente, son algunas de las causas** que han sido consecuencia de los asesinatos que tan solo en el mes de mayo han cobrado la vida de 20 líderes. [(Lea también: Comisión de la Verdad expresó su respaldo a líderes sociales del Cauca)](https://archivo.contagioradio.com/comision-de-la-verdad-expreso-su-respaldo-a-lideres-sociales-del-cauca/)

### La cuota artística rememora a los líderes sociales

Como parte de este dialogo, el 10 de junio, la artista **Doris Salcedo **realizará la obra 'Quebrantos', una acción monumental que honrará la memoria de 165 líderes asesinados, dibujando sus nombres los cuales luego serán  reescritos en vidrio fracturado.

La escultora afirmó que **"los colombianos estamos perdiendo nuestra mejor gente, perdiendo dignidad, perdiendo humanidad y eso tenemos que tratar de ganarlo en la medida que cuidamos a otros líderes"** además añadió que es a través del arte que se puede recuperar la dignidad que se ha perdido en cada hecho violento en Colombia.

También se refirió a los Diálogos para la No Repetición l**a comisionada Patricia Tobón**, quien indicó que han querido dar inicio a esta iniciativa abordando la situación  de los líderes sociales pues dentro del mandato de la Comisión está previsto "contribuir a que la sociedad reflexione cuáles son las causas que en Colombia están generando un reciclaje del conflicto" contra los defensores de derechos humanos en las comunidades.

Este acto simbólico y de memoria en el que serán escritos en vidrio fracturado 162 nombres de los líderes sociales que fueron asesinado desde la firma del Acuerdo Final de Paz contará con la presencia de líderes y lideresas amenazados y también participarán los y las comisionadas, en un acto que dará inicio a las 10 de la mañana de este 11 de junio.

> ¿Qué son los [\#DiálogosParaLaNoRepetición](https://twitter.com/hashtag/Di%C3%A1logosParaLaNoRepetici%C3%B3n?src=hash&ref_src=twsrc%5Etfw)? Responde la comisionada Patricia Tobón Yagarí. El primer Dialogo se realizará el 11 de junio a las 9 de la mañana con trasmisión por la página web de la [@ComisionVerdadC](https://twitter.com/ComisionVerdadC?ref_src=twsrc%5Etfw) [\#LaVerdadEsConLosLíderes](https://twitter.com/hashtag/LaVerdadEsConLosL%C3%ADderes?src=hash&ref_src=twsrc%5Etfw)
>
> Conozca más en ► <https://t.co/oqdl6fNZgP> [pic.twitter.com/zf9QaYr0lb](https://t.co/zf9QaYr0lb)
>
> — Comisión de la Verdad (@ComisionVerdadC) [7 de junio de 2019](https://twitter.com/ComisionVerdadC/status/1136969805792067584?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
