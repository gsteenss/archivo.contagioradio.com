Title: Corte Penal Internacional abriría investigación formal contra generales colombianos
Date: 2018-03-16 16:09
Category: DDHH, Nacional
Tags: Corte Penal Internacional, crímenes de estado, Ejecuciones Extrajudiciales, falsos positivos
Slug: cpi-abriria-investigacion-formal-contra-generales-colombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/FALSOS-POSITIVOS-e1500995272586.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Pilon] 

###### [16 Mar 2018]

**La fiscalía de la Corte Penal Internacional estará **[**nuevamente**]** Colombia,** cabe recordar que la visita anterior fue en Septiembre de 2017, una cita que no fue tan grata, según las declaraciones de la Fiscal Bensouda el Estado colombiano, concretamente la Fiscalía General, no entregó información sobre los avances en  justicia de algunos casos que adelanta la CPI.

Desde el año 2017 este organismo internacional ha puesto gran atención sobre las “ejecuciones extrajudiciales” conocidas como "Falsos Positivos". En total el organismo pedía **que se investigará a 29 integrantes de la fuerza pública, entre ellos 23 generales y seis coroneles** que estarían implicados en cerca de 5000 casos de ejecuciones extrajudiciales según lo recogió la oficina de las Naciones Unidas para los DDHH en Colombia. (Le puede interesar: [Corte Penal Internacional pide investigaciones por ejecuciones extrajudiciales)](https://archivo.contagioradio.com/corte_penal_internacional_falsos_positivos/)

Sin embargo, a pesar de que el número de posibles investigados es alto, las pruebas más contundentes pesarían sobre seis generales, que habrían sido los priorizados por las víctimas y las organizaciones de DDHH para que se adelantarán investigaciones y así entregar a la CPI  la información.

Entre los nombres se encuentran el General (r) Óscar González Peña, comandante del Ejército Nacional de 2008 a 2010; sobre quien pesarían 113 casos de este tipo de delitos. El general Henry Torres Escalante contra quien pesarían otros 113 de los crímenes cometidos entre diciembre de 2005 y junio de 2007, el General Mario Montoya, Jorge Navarrete, Emiro Barrios, Ricardo Bernal, Jorge Salgado también altos mandos de las FFMM. (Le puede interesar: e[l historial que presentaron las víctimas de los falsos positivos)](https://archivo.contagioradio.com/generales-victimas-cpi-falsos-positivos/)

Esta nueva visita de tres delegados de la fiscalía de la **CPI** también **abriría las puertas para que la JEP, recién puesta en funcionamiento, acelere las investigaciones por los crímenes de Estado,** dado que muchas de las personas, integrantes de la Fuerza Pública acusadas o procesadas por los llamados “Falsos Positivos” se han acogido a ese mecanismo con el que se comprometen a contar la verdad de los hechos, las órdenes dadas o recibidas y los beneficiarios de este tipo de crímenes.

Así las cosas, esta visita según las organizaciones de derechos humanos puede ser una oportunidad de oro para las víctimas que desde hace más de una década están buscando verdad y justicia y no han encontrado en los procedimientos regulares, ni celeridad ni interés político para resolver una de las mayores atrocidades de la guerra en Colombia.

###### Reciba toda la información de Contagio Radio en [[su correo]
