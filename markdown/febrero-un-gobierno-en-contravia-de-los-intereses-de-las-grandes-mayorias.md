Title: Febrero: Gobierno en contravía de las grandes mayorías
Date: 2020-12-28 20:42
Author: CtgAdm
Category: Actualidad, Especiales Contagio Radio, Nacional, Resumen del año
Tags: acuerdo de paz, asesinato de excombatientes, Gobierno Nacional
Slug: febrero-un-gobierno-en-contravia-de-los-intereses-de-las-grandes-mayorias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-1-1.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-1-2.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-6.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Ante el aumento de los asesinatos de personas que se acogieron al acuerdo de paz fue dramático. Solamente para Febrero de 2020 se reportaban más de 200 asesinatos lo que motivó una de las primeras protestas masivas en contra de lo que las FARC han llamado *un genocidio*. Al cierre de esta nota se reportan 489 asesinatos desde la firma del Acuerdo de Paz en noviembre de 2016.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En paralelo al aumento de los asesinatos de excombatientes y líderes sociales, las comunidades comenzaron a plantear las estrategias de solución del problema ante la incapacidad o el desinterés del gobierno para responder al derramamiento de sangre. Algunas de estas propuestas tienen que ver con los acuerdos humanitarios regionales que no encontraron tampoco respuesta o respaldo del gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, pero en la misma línea, el **gobierno de Iván Duque parecía estar viviendo y gobernando para una realidad diferente**. La visita del presidente a la región del Urabá estuvo antecedida por una serie de problemas de seguridad para las comunidades y marcada por el asesinato de un líder social a pocos metros del lugar en el que estaba el presidente Duque. Un ejemplo de un gobierno que va por un camino muy diferente al de la mayoría de sus ciudadanos.

<!-- /wp:paragraph -->

<!-- wp:heading -->

25 de febrero: Un cacerolazo para amplificar la voz de casi 200 excombatientes asesinados
-----------------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este 25 de febrero, el partido Fuerza Alternativa Revolucionaria del Común convocó en la ciudad de Bogotá, desde las 6:00 pm, un cacerolazo en el Parque de las periodistas que incluirá una serie de expresiones culturales artísticas y simbólicas en respuesta al silencio del Estado ante la muerte de excombatientes, 12 de los cuales han sido asesinados en lo corrido del 2020, **alertó el último reporte de la Misión de Verificación de la Organización de las Naciones Unidas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver más: ([Un cacerolazo para amplificar la voz de casi 200 excombatientes asesinados](https://archivo.contagioradio.com/cacerolazo-mplificar-voz-casi-200-excombatientes-asesinados/)[– Contagio Radio](https://archivo.contagioradio.com/un-pacto-humanitario-lo-minimo-para-proteger-las-vidas-en-catatumbo/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Un pacto humanitario, lo minimo para proteger las vidas en Catatumbo
--------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tras la misión humanitaria al municipio de El Carmen, en Catatumbo del pasado 22 y 23 de febrero, para verificar la situación de las poblaciones afectadas por el paro armado decretado por el ELN y el EPL en la región, organizaciones señalaron la necesidad urgente de establecer pactos humanitarios que eviten nuevas afectaciones a las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La misión humanitaria estuvo a cargo de organizaciones sociales, defensoras de derechos humanos, entes internacionales y la Defensoría del Pueblo. Gracias al testimonio de los habitantes de El Carmen, la Misión señaló que se han presentado abusos por parte de la Fuerza Pública, confinamientos y desplazamientos de población, así como que se mantiene el estado de zozobra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver más: ([Un pacto humanitario, lo minimo para proteger las vidas en Catatumbo – Contagio Radio](https://archivo.contagioradio.com/un-pacto-humanitario-lo-minimo-para-proteger-las-vidas-en-catatumbo/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Iván Duque visita Apartadó y a tan solo kilómetros es asesinado el líder Amado Torres
-------------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La mañana de este sábado 29 de febrero en San José de Apartadó, Antioquia fue asesinado el líder social y actual tesorero de la Junta de Acción Comunal de la vereda La Miranda, **Amado** **Torres** de 49 años. El suceso, que señalan fue perpetrado por accionar paramilitar ocurrió el mismo día que el presidente Iván Duque visitaba el corregimiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según relatan sus familiares, cerca de las 6:00 am, hombres fuertemente armados con prendas militares llegaron hasta la vivienda del líder y lo sacaron a la fuerza para después disparar contra Amado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver más: ( [Iván Duque visita Apartadó y a tan solo kilómetros es asesinado el líder Amado Torres – Contagio Radio](https://archivo.contagioradio.com/ivan-duque-visita-apartado-y-a-tan-solo-kilometros-es-asesinado-el-lider-amado-torres/))

<!-- /wp:paragraph -->
