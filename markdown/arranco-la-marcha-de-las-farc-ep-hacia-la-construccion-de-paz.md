Title: Arrancó la marcha de las FARC-EP hacia la construcción de paz
Date: 2017-01-31 12:53
Category: Otra Mirada, Paz
Tags: implementación acuerdos de paz, Zonas Veredales
Slug: arranco-la-marcha-de-las-farc-ep-hacia-la-construccion-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/ultima-marcha-de-la-guerrillas-de-las-farc-10.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotos: Nueva Colombia Noticias] 

###### [31 Ene 2017] 

En las filas de la guerrilla de las FARC-EP se vive una mezcla de emociones que, de acuerdo con Marcos Calarcá, delegado para el mecanismo de monitorio y verificación de esta guerrilla, es una **“conjugación de sentires entre los positivos con miradas a la paz y los negativos que ven con afán las amenazas a líderes en Colombia y  las transformaciones que vienen”**

Emociones que surgen entre las últimas marchas de las FARC-EP como guerrilla, pero que para Calarcá significa al mismo tiempo que **“habrán más como movimiento político en la legalidad”**, motivo por el cual, pese las zonas veredales  no estén en  condiciones óptimas para su traslado y permanencia, el delegado informó que  cumplirán con su palabra y se dispondrán a terminar lo que falte en cada uno de los puntos transitorios  para continuar con el proceso de reincorporación y pedagogía de los acuerdos de paz entre los guerrilleros.

Sin embargo, para el delegado de las FARC-EP hay riesgos y temores, el más latente es que no se cumpla con lo pactado - “**los temores se dirigen hacía la no implementación de los acuerdos, la manipulación en cuanto a tiempo y formas**, ese sería el peor escenario en el que podríamos caer”, situación que para Calarcá, puede afrontarse con la presión por parte de la ciudadanía para que no quede un “acuerdo a medias”. Le puede interesar:["Zonas veredales bajo la sombra paramilitar"](https://archivo.contagioradio.com/zonas-veredales-bajo-la-sombra-paramilitar/)

De igual forma, frente a las denuncias hechas por algunas comunidades de presencia paramilitar en cercanías a los puntos transitorios, Calarcá señaló que ya se tuvo una reunión con el gobierno Nacional, en donde este **se comprometió a brindar todas las garantías de seguridad durante el traslado de las unidades a estos lugares.**

Este proceso de movilización que inició el pasado sábado, culminará el día de mañana cuando el Frente Martín Castillo llegue a la zona veredal de Los Pondores, en La Guajira, y finalmente estén los **6.000 integrantes de las FARC-EP en todos los puntos transitorios**. A su vez, Calarcá indicó que se realizará un registro de las armas en las zonas transitorias y habrán cambios en el calendario de la dejación de armas para continuar con el proceso de implementación. Le puede interesar: ["Este fin de mes estaremos en las Zonas Veredales: Jesús Santrich"](https://archivo.contagioradio.com/35424/)

\

<iframe id="audio_16745523" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16745523_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
