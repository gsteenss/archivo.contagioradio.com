Title: El cambio climático lo frenará la sociedad civil y no la COP21
Date: 2015-12-01 15:01
Category: Ambiente, Otra Mirada
Tags: COP 21, Cumbre COP 20 Copenhage, Derechos Humanos, Ecologistas en Acción, Marcha mundial por el clima, Radio derechos Humanos
Slug: el-cambio-climatico-lo-frenara-la-sociedad-civil-y-no-la-oficialidad-de-la-cop21
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/COP21.-Cumbre-Cambio-climático.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.24horas.cl]

###### [1 Dic 2015] 

Cuando en  el 2009  se realizó la cumbre **en Copenhague no se llegó a ningún acuerdo y la ciudadanía dejó de movilizarse**, para esta COP21 la coalición clima 21 de Francia y otras organizaciones afirman que de esta cumbre no saldrá ningún acuerdo y no hay que crear falsas expectativas, sin embargo **no hay que dejar de movilizarse. Por eso las organizaciones apuntan a fortalecer el movimiento por la justicia climática en todos los países**.

Las movilizaciones buscan **concientizar sobre la necesidad de “un cambio de modelo de producto de consumo, una apuesta clara por un nuevo modelo energético que significa,** basado en energías renovables, dejar las reservas de los combustibles fósiles en el subsuelo”, afirma Tom Kucharz, integrante de Ecologistas en Acción, esto para no superar los 2 grados centígrados de temperatura en el 2100 y dejar así la quema de combustibles fósiles que genera el 80% de contaminación, **por esto ecologistas en acción y muchas otras organizaciones se oponen a este acuerdo** que saldría de esta cumbre, ya que no contempla estos temas.

Kucharz resalta que “en vez de prestar tanta atención a las cumbres de las Naciones Unidas que han demostrado después de 20 años ser totalmente cooptados por intereses de las grandes industrias, las empresas petroleras, de los bancos que no quieren que cambie nada, **el nuevo enfoque podrían ser las ciudades, municipios, los gobiernos locales**”, los cuales podrían **generar políticas y modelos urbanísticos, de transporte  y de vida que aporten al medio ambiente**.

**Otro de los pasos que deben seguir las ciudadanías del mundo es fortalecer las luchas** “tenemos que mostrar que la lucha contra la minería, la lucha contra la explotación del agua, la lucha por comunidades y muchas organizaciones sociales por un trabajo digno, por la soberanía alimentaria, pro la reforma alimentaria todo esto son luchas que están encaminadas a cambiar el modelo de producción y consumo” y tendrán mejores resultados que las cumbres de las Naciones Unidas.

De esta forma, **coordinando y organizando los movimientos sociales y organizaciones populares emprender políticas coherentes y justas**, Kucharz valora que “cada vez se engaña menos gente, cada vez más personas en el mundo están conscientes quienes son los culpables y los responsables del desastre” y ven que “las alternativas van a surgir en otros ámbitos como de la sociedad civil”, no vendrán ni de las empresas, ni de los gobiernos, ni siquiera de esta COP21.
