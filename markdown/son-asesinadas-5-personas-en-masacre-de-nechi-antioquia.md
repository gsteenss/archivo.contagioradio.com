Title: Son asesinadas 5 personas en  masacre de Nechí, Antioquia
Date: 2020-11-04 14:41
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Antioquia, bajo cauca antioqueño, masacre, Nechí
Slug: son-asesinadas-5-personas-en-masacre-de-nechi-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Informe-masacres-Indepaz-Nechi.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Masacre-Nechi-Antioquia.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este martes **se registró una nueva masacre en la que fueron asesinadas 5 personas en el municipio de Nechí, ubicado en la región del Bajo Cauca antioqueño.** (Le puede interesar: [Asesinan a Deimer Alberto Lucas, hijo del Gobernador Indígena del pueblo Senú en El Bagre, Antioquia](https://archivo.contagioradio.com/asesinan-a-deimer-alberto-lucas-hijo-del-gobernador-indigena-del-pueblo-senu-en-el-bagre-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El reporte inicial de la comunidad señalaba que en la tarde de este martes 3 de noviembre, 4 personas fueron asesinadas en el corregimiento de Bijagual y otras más, resultaron heridas. (Le puede interesar: [Asesinan al líder Jhon Jairo Guzmán en Tarazá, Antioquia](https://archivo.contagioradio.com/asesinan-al-lider-jhon-jairo-guzman-en-taraza-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según habitantes del municipio hombres armados habrían irrumpido en un billar, asesinando incluso al propietario del establecimiento. **Por otro lado, la comunidad también denunció que varias personas quedaron heridas; especialmente un hombre que no pudo ser trasladado debido a que ninguna autoridad hizo presencia oportuna en el sitio del crimen y murió horas después**, incrementando el número de **víctimas fatales a 5 personas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con la información suministrada por la comunidad, pese a que el hecho se presentó en horas de la tarde; hasta anoche no se tuvo reporte de presencia de las autoridades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según los pobladores del corregimiento de Bijagual, esta es la primera vez que se registra un crimen de estas dimensiones en esa zona.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, **Nechí, como otros municipios del Bajo Cauca antioqueño, viene siendo escenario de un recrudecimiento de la violencia protagonizada principalmente por el Clan del Golfo (autodenominado Autodefensas Gaitanistas de Colombia -AGC-) y los Caparros,** dos grupos paramilitares que ejercen control de la población civil y se disputan las rutas del narcotráfico y la explotación de recursos mediante la minería ilegal; en lo que ha sido considerado una especie de “*reauge paramilitar*” en esa región.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe anotar que la Defensoría del Pueblo ha emitido alertas y llamados previniendo a las autoridades sobre la violencia que viven los pobladores de esta región. **El pasado 31 de agosto La Defensoría emitió un llamado a las autoridades para que hicieran presencia en el Bajo Cauca antioqueño, ya que, grupos armados ilegales como los Caparrapos y las AGC tienen en riesgo a comunidades en Nechí, Cáceres, Caucasia, Tarazá y Zaragoza.** (Lea también: [Otra Mirada: Estas son las características del paramilitarismo después del acuerdo de paz](https://archivo.contagioradio.com/otra-mirada-estas-son-las-caracteristicas-del-paramilitarismo-despues-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a estos llamados, los hechos de violencia se siguen presentando, con todo y que **el Bajo Cauca es la zona más militarizada del departamento de Antioquia con por lo menos 5.000 hombres de Fuerza de Tarea Conjunta Aquiles y 2.000 más de la operación Agamenón 2**, según voceros de [CCEEU](https://coeuropa.org.co/158/) (Nodo Antioquia).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AidaAvellaE/status/1323979763850117121","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AidaAvellaE/status/1323979763850117121

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según el más reciente [informe](http://www.indepaz.org.co/informe-de-masacres-en-colombia-durante-el-2020/) del Instituto de Estudios para el Desarrollo y la Paz –Indepaz-, esta es **la masacre número 71** ocurrida en el país en lo que va del presente año; **siendo Antioquía el departamento más afectado, el cual registra un total de 16 masacres**.

<!-- /wp:paragraph -->

<!-- wp:image {"id":92243,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Informe-masacres-Indepaz-Nechi.png){.wp-image-92243}  

<figcaption>
Fuente: Indepaz

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->
