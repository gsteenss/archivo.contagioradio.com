Title: Las investigaciones que siguen por el magnicidio de Jaime Garzón
Date: 2018-08-16 13:31
Category: DDHH, Entrevistas
Tags: Derechos Humanos, Fiscalía, Jaime Garzon, JEP
Slug: las-investigaciones-magnicidio-jaime-garzon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/marisol-garzon-y-jaime-garzon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo] 

###### [15 Ago 2018] 

Tras la condena de **José Miguel Narváez** por el asesinato de **Jaime Garzón,** **Santiago Escobar, abogado defensor de los familiares del humorista** aseguró que la sentencia sirve para avanzar en el esclarecimiento del asesinato, pero aclaró que aún existen otras investigaciones abiertas contra miembros de las Fuerzas Armadas por su presunta participación en el homicidio del humorista.

A pesar de considerar que este paso es importante en el esclarecimiento de la verdad, el abogado anunció que **apelarán la decisión del juzgado en la que se desconoce el caso como un delito de Lesa Humanidad**, pese a que otras autoridades como la Fiscalía y el Consejo de Estado si lo han hecho. (Le puede interesar: ["30 años de prisión pagará José Miguel Narváez por homicidio de Jaime Garzón"](https://archivo.contagioradio.com/condena-narvaez-homicidio-garzon/))

Además, según Escobar "desde ningún punto de vista se puede considerar que el magnicidio de Jaime Garzón fue aislado, sino parte de un plan mucho más amplió que buscaba atentar contra defensores de derechos humanos, con el mismo modus operandi" y afirmó que **está era la primera condena que se producía tras 19 años del asesinato, puesto que la otra sentencia emitida contra Carlos Castaño, se produjo cuando ya se presumía la muerte del jefe paramilitar.**

Sin embargo, recordó que aún cursa una investigación en la Fiscalía contra el General (r) Rito Alejo del Río para determinar su responsabilidad en el magnicidio del humorista, y aún no concluye el proceso del Coronel (r) Jorge Eliécer Plazas Acevedo por los mismo hechos. (Le puede interesar: ["JEP pide información de 9 brigadas implicadas en ejecuciones extrajudiciales"](https://archivo.contagioradio.com/9-brigadas-implicadas-en-ejecuciones-extrajudiciales/))

### **General (r) Rito Alejo del Río** 

En su [Informe](http://www.cidh.org/countryrep/Colom99sp/indice.htm) sobre la situación de los Derechos Humanos en Colombia en 1999, la **Comisión Interamericana de Derechos Humanos (CIDH)** consideró que " salvo que se esté tratando de una palpable inefectividad y falta de control sobre sus tropas, es claro que el General Rito Alejo del Río debía tener conocimiento (...) de la presencia de los paramilitares en el área (Apartadó, Antioquia) y de la cooperación entre sus hombres y aquellos grupos".

Del Río fue comandante de la Brigada Nº17 con sede en Carepa, Antioquía y tenía bajo su jurisdicción la zona del Urabá. Durante su comandancia entre 1995 y 1997, se desarrollo la **operación "Génesis"**, que tenía como propósito retomar zonas ocupadas por la guerrilla, sin embargo, dejó cerca de 5.000 personas desplazadas y **el asesinato Marino López Mena,** uno de los más estremecedores de la historia. (Le puede interesar: ["Víctimas de Rito Alejo del Río esperan toda la verdad"](https://archivo.contagioradio.com/victimas-rito-alejo-del-rio/))

Sobre el crimen de Marino, se estableció que **el General tenía responsabilidad en su asesinato** porque conocía de la presencia paramilitar en la zona y no hizo nada para evitar la muerte del comunero. El General también es investigado por su presunta participación en la **Masacre de Mapiripán,** Meta; en el homicidio de Álvaro Gómez Hurtado y en el homicidio de Jaime Garzón.

Esta última investigación se produjo por la presunta participación del uniformado en la coordinación de la **logística que permitió a los sicarios asesinar a Garzón**, mientras el investigado era Comandante de la Brigada Nº13, que tiene jurisdicción en Cundinamarca y el Distrito Capital. (Le puede interesar: ["Generales Mauricio Santoyo y Rito Alejo del Río son vinculados al crimen de Jaime Garzón"](https://archivo.contagioradio.com/generales-mauricio-santoyo-y-rito-alejo-del-rio-son-vinculados-a-crimen-de-jaime-garzon/))

### **Coronel (r) Jorge Eliécer Plazas Acevedo** 

El Coronel (r) Jorge Plazas Acevedo fue jefe de inteligencia de la Brigada Nº17 cuando el General (r) Rito Alejo del Río era su comandante. Allí, además de participar en la operación "Génesis", se le sindica de tener parte en la **masacre de Mapiripán,** Meta, cuando presuntamente coordinó el despegue de un avión con 30 paramilitares, que aterrizó en San José del Guaviare, Guaviare, y posteriormente se dirigieron al Municipio del Meta para masacrar a cerca de medio centenar de personas.

Plazas Acevedo también fue acusado de ser coautor del asesinato de Garzón, mientras era jefe de investigación en la Brigada que dirigía del Río. Según la Fiscalía, **desde esa Brigada se hizo el seguimiento al humorista, y Acevedo sería conocido como alias "Don Diego"**, un reconocido paramilitar de Urabá. (Le puede interesar: ["Coronel Plazas Acevedo: De la masacre de Mapiripán al asesinato de Jaime Garzón"](https://archivo.contagioradio.com/coronel-plazas-acevedo-de-la-masacre-de-mapiripan-al-asesinato-de-jaime-garzon/))

### **Tanto Plazas Acevedo como del Río están en proceso de acogerse a la JEP** 

Aunque el proceso de Acevedo se estaba desarrollando en la justicia ordinaria, la firma de sometimiento ante la Jurisdicción Especial para la Paz (JEP) del Coronel retirado hizo que su caso fuera trasladado ante el tribunal de paz, y mientras tanto se encuentra en pausa. De otra parte, el General afirmó que se acogería a la JEP, y ya recibió medida de libertad provisional por parte de la justicia transicional.

Sobre ambos sometimientos, Escobar señaló que espera que los uniformados entreguen información que sea útil para esclarecer los hechos sobre el asesinato de Garzón, porque  hasta ahora **"la lógica de quienes han asistido a la JEP no ha sido la de colaborar con la justicia para esclarecer hechos en los que se ven involucrados".** (Le puede interesar: ["Madres de Soacha rechazan 'verdades a medias' de militares en la JEP"](https://archivo.contagioradio.com/madres-soacha-rechazan-verdades-medias-jep/))

<div class="stream-item-footer">

<iframe id="audio_27886817" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27886817_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU) 

</div>

<div class="osd-sms-wrapper">

</div>
