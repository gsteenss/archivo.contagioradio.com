Title: 66 excombatientes que apostaron a la paz han sido asesinados en 2019
Date: 2019-12-18 17:11
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato de excombatientes, Chocó, Valle del Cauca
Slug: 66-excombatientes-que-apostaron-a-la-paz-han-sido-asesinados-en-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Zonas-veredales-17-scaled.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Para octubre del 2019, la Misión de Verificación de la ONU, confirmó que durante el año se habían cometido **63 homicidios contra excombatientes,** un dato que, sumado a los más de tres homicidios cometidos contra firmantes de la paz registrados hasta mediados de diciembre, representan un incremento en comparación con los 64 reportados durante 2018, revelando la ausencia de garantías para quienes se acogieron al Acuerdo y que buscan retornar a la vida civil.

### Arley Saldaña  y Manuel Perea, víctimas de desaparición 

En menos de una semana, se ha denunciado el asesinato de tres excombatientes quienes cumplían con su proceso de reincorporación. Durante el fin de semana se conoció la muerte de Manuel González en Antioquia, caso a los que se suman los homicidios de Arley Saldaña en Valle del Cauca y Manuel Perea en Chocó.

El pasado 12 de diciembre de 2019 cerca de las 03:40 en el Barrio Mojica de Cali, Valle del Cauca fue hallado sin vida **Arley Mejía Saldaña**, reincorporado de las FARC  a quien se encontró varias heridas de arma blanca.  Su familia, lo buscaba desde el pasado jueves e incluso lo había reportado como desaparecido.

Manifiestan desde el Partido que Arley había solicitado medidas de protección, sin embargo se desconoce si la Unidad Nacional de Protección alcanzó a aportar las medidas de seguridad que había pedido.  [(Le puede interesar: Asesinan a excombatiente de las FARC en el ETCR de Santa Lucía, Ituango)](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-las-farc-en-el-etcr-de-santa-lucia-ituango/)

Al respecto, las autoridades que hallaron el cuerpo de Arley indicaron que hasta el momento no existen personas capturadas ni testigos presenciales de los hechos. Señalan que Arley no pudo ser identificado en su momento debido a que junto a él no se encontró algún documento de identificación, ni información o acompañamiento de familiares que aportaran  datos personales.

Por su parte, en el corregimiento del llano de Bebara, municipio del Medio Atrato en el Chocó, lugar en el que **Manuel Antonio Perea Palacios**, adelantaba su proceso de reincorporación, el hombre fue desaparecido el pasado 13 de diciembre cuando hombres encapuchados sin identificar lo amarraron y sacaron de su residencia. Tres días después, Manuel fue hallado sin vida en las orillas del río Bebara. [(Lea también: En Colombia han sido asesinados 170 excombatientes tras la firma del Acuerdo de Paz)](https://archivo.contagioradio.com/en-colombia-han-sido-asesinados-170-excombatientes-tras-la-firma-del-acuerdo-de-paz/)

### Es necesaria una mayor seguridad para los excombatientes 

Los asesinatos de Arley Mejía y Manuel Perea se dan días después del también homicidio de **Manuel González,** excombatiente habitante del ETCR  de Santa Lucia en Ituango, Antioquia. Al respecto el dirigente del partido FARC, Pastor Alape, manifestó, "en el día de hoy despedimos a Manuel Antonio “Manduco” González, forjador de paz y reconciliación, víctima del odio y la polarización que siguen promoviendo sectores de poder en Colombia".

> [\#AEstaHora](https://twitter.com/hashtag/AEstaHora?src=hash&ref_src=twsrc%5Etfw) con el alma rota, despedimos a nuestro compañero Manuel González, un joven trabajador, comprometido con la paz. Insistimos en la [\#DENUNCIA](https://twitter.com/hashtag/DENUNCIA?src=hash&ref_src=twsrc%5Etfw) de la grave situación que vive el ETCR de Ituango. Presidente [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw), ¿Cuál es su compromiso con la paz? [pic.twitter.com/IL4q7MUs2t](https://t.co/IL4q7MUs2t)
>
> — FARC (@PartidoFARC) [December 17, 2019](https://twitter.com/PartidoFARC/status/1207007737403641865?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
A su vez, Pablo Catatumbo, senador de la misma bancada expresó que hechos como el asesinato de Manuel Antonio y los más de 180 firmantes de la paz que han muerto en un lapso de tres años, son la prueba del genocidio que se está dando contra la población de excombatientes.  [(Le puede interesar: Con Wilson Parra, son 169 excombatientes de FARC asesinados desde la firma del Acuerdo de Paz)](https://archivo.contagioradio.com/con-wilson-parra-son-169-excombatientes-de-farc-asesinados-desde-la-firma-del-acuerdo-de-paz/)

Ante la ausencia de garantías para los cerca de 7.000 personas que dejaron las armas, se ha pedido a la oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos, que haga un acompañamiento a los casos. **Según el Partido Fuerza Alternativa Revolucionaria del Común más de 180 excombatientes han sido asesinados desde la firma del Acuerdo de Paz.**

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
