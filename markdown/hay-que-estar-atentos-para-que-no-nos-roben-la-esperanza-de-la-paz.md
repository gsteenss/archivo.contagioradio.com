Title: No nos robarán la esperanza de la paz, aunque lo intentan
Date: 2017-09-08 06:00
Category: Opinion
Tags: acuerdo farc y gobierno, ELN, Visita de Papa a Colombiia
Slug: hay-que-estar-atentos-para-que-no-nos-roben-la-esperanza-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-4-e1504831395382.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [8 Sep 2017] 

#### **Por [@Kinescribe](https://twitter.com/kinEscribe)** 

La visita del Papa Francisco a Colombia ha sido catalogada por él mismo como un impulso a la paz en nuestro país. Una paz con señales muy positivas y otras muy negativas que no pueden quedarse ocultas, ni pasar a un segundo plano. El Papa en todas sus intervenciones ha hecho un llamado a no dejarnos robar la esperanza de la paz, así en el congreso y otras instancias estén haciendo todo lo posible por arrebatárnosla.

Por un lado la burocracia estatal ha resaltado por el incumplimiento en la construcción de las Zonas Veredales, el retraso en la aplicación de la Amnistía a los presos de esa organización, la falta de atención en salud, y además el asesinato de 14 integrantes de esa guerrilla en diferentes regiones del país.

Ahora, en asuntos estructurales las cosas no están mejor. Marta Lucía Ramírez, precandidata presidencial, recusó a los magistrados** **Antonio José Lizarazo y Cristina Pardo en el crucial debate sobre el blindaje jurídico a los acuerdos de paz que obliga a los gobiernos venideros a actuar coherentemente con el texto del acuerdo de fin del conflicto con las FARC, ambos magistrados podrían haber respaldado dicho blindaje.

El problema es que seguramente se acogerá la medida y quedarán por fuera de la decisión desequilibrando la balanza a favor de los demandantes, el Centro Democrático, y así, si desaparece dicho “blindaje” el acuerdo quedaría en manos de un nuevo gobierno que podría hacerlo trizas en un corto periodo de tiempo.

Por otra parte, en el congreso el Fast Track no está funcionando. Hicieron falta cerca de una decena de debates para que la reforma política, la primera de las grandes apuestas del acuerdo, pasara solamente de la Comisión Primera, las argucias de Cambio Radical y del Centro Democrático no dieron tregua para frenarla, petición de archivo artículo a artículo, modificaciones simples, votación tras votación, fueron solamente algunas de las situaciones que se presentaron. Y falta la revisión constitucional que podría tumbarla pues a las cortes no les ha gustado nunca el “tribunal de aforados”.

Vienen otras leyes gruesas, cerca de 10, entre las que está la ley estatutaria de la Jurisdicción Especial de paz, la reglamentación de la Comisión de la Verdad, las circunscripciones especiales de paz y otras que también podrían enfrentar serios tropiezos en su camino a convertirse en ley si la tendencia continúa.

Sin embargo, pese a esos esfuerzos por frenar la implementación y destruir la paz, son miles y miles las organizaciones sociales que siguen trabajando porque la paz sea una realidad. Por ejemplo, los representantes del capítulo étnico hacen las denuncias a tiempo y seguramente lograrán que no se les excluya de la CSIVI y que los recursos vayan a donde deben ir. Ya lo hicieron una vez y seguramente se repetirá.

La clave es la movilización, que los sectores democráticos en el congreso y la gente en las calles sigan haciendo la tarea juiciosa, sin ausentismo y sin mezquindades, la unidad de cara a las elecciones, el voto consciente en 2018, la construcción de fuerzas políticas sólidas en las regiones, seguramente salvarán la idea de la paz de todos y todas.

El cese bilateral del gobierno y el ELN, los mensajes públicos de las llamadas Autodefensas Gaitanisas de Colombia que quieren contribuir a la construcción de la paz, o el nacimiento de Fuerza Alternativa Revolucionaria del Común, y las palabras del papa, cargadas de alegría, de esperanza y de utopía, como lo mencionó en la casa de Nariño, sin duda aportarán para que crezca el espíritu de paz y la movilización para rodear ese querido bien que se acerca para nuestro país.
