Title: Juguetes cantados: un viaje con humor por ritmos y aires musicales
Date: 2017-04-27 11:00
Category: eventos
Tags: Bogotá, Corporación Colombiana de Teatro, musical, teatro
Slug: juguetes-cantados-teatro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/18155838_1408531369241813_7700804767966409612_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:C.C. de Teatro 

###### 26 Abr 2017 

El dúo Haz de Pares presenta en corta temporada del 27 al 29 de abril "Juguetes cantados", una propuesta escénica musical que incorpora canciones y palabras utilizando el sentido del humor como recurso principal.

Utilizando ponchos, sombreros, pelucas, faldas y juguetes musicales como guitarras, armónicas, bombos, platillos y huevitos sonoros, Ricardo Vélez y Luis Miguel Hurtado Garcia, presentan un recital de 5 canciones del repertorio universal versionadas a su estilo y 9 de su propia autoria, todas con un tono cliché, kitsch y festivo.

A ritmo de joropo rolo, cumbiandina anglolatina, burlería flamenca, merengue atanguizao, jazz swing-gogo entrew otros, Juguetes cantados presenta 3 momentos a los espectadores cuyo eje central consiste en un homenaje a las vocales de la lengua castellana. Le puede interesar: [Indígenas Muinane reconstruyen memoria tras holocausto cauchero](https://archivo.contagioradio.com/muinane-amazonas-libro/).

El par de viajeros ha llevado su propuesta a escenarios en países como Cuba, Argentina, Brasil, Le Méxique, EU, España, Italia, y Colombia, con perspectiva de llevarla en poco tiempo a la India y Pakistán. En Bogotá se presentará de jueves a sábado en la Sala Seki Sano Calle 12 \# 2- 65, desde las 7:30 de la noche. La boletería para estudiantes y adulto mayor tiene un costo de 12.000 pesos y público general a 20.000 pesos.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FCCdeTeatro%2Fvideos%2F1407541232674160%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
