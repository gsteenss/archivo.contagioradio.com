Title: Camioneros del país continúan en protesta pacífica pese a provocaciones de la Fuerza Pública
Date: 2015-03-16 23:06
Author: CtgAdm
Category: Movilización, Nacional
Tags: Asociación Colombiana de Camioneros, ESMAD, IMPALA, Paro Camioneros en Colombia, víctimas ESMAD
Slug: camioneros-del-pais-continuan-en-protesta-pacifica-pese-a-provocaciones-de-la-fuerza-publica
Status: published

###### Foto: asocamion.com 

<iframe src="http://www.ivoox.com/player_ek_4228919_2_1.html?data=lZefmp6VfY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYp8nLpdOft8bfycbXb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### [**Entrevista con Edgar Vargas - ACC**] 

Edgar Vargas Presidente de la Asociación Colombiana de Camioneros en el departamento del Casanare afirma que en el departamento y en todo el país los transportadores han **acogido el llamado a paro nacional por lo menos en un 90%** y además están recibiendo el apoyo de los comerciantes quienes han rodeado la protesta porque han entendido las razones.

Vargas, denuncia que el gobierno nacional no ha querido acoger las exigencias del sector de los transportadores porque ya hay unos compromisos asumidos con empresas como **IMPALA**, entre otras, que tiene concesionados importantes canales como el transporte en el río Magdalena, en el Puerto de Buenaventura y ha construido sus propios puertos en ciudades como Barrancabermeja. Según Vargas son más de **5000 camiones provenientes de Europa los que llegarían a Colombia en los próximos meses.**

Vargas agrega que los transportadores se mantienen en la decisión de mantener la protesta pacífica y que en varias ocasiones **han soportado las provocaciones del ESMAD** para no dar la excusa de la estigmatización al sector del gobierno que prefiere la confrontación al diálogo para acallar la protesta. Además en algunas ocasiones han identificado agentes de las fuerzas de seguridad infiltrados en los puntos de concentración.

Vargas también **rechazó las acusaciones por las cuales se afirma que el Paro de camioneros está infiltrado** por actores ilegales y desmintió las acusaciones por supuestas amenazas contra los transportadores que decidan trabajar.

De este paro hacen parte camioneros de todos los renglones de la economía como los camiones sisterna, los transportadores de ganado y de productos agrícolas.
