Title: Descontento en comunidad estudiantil de la Nacional por reelección de Mantilla
Date: 2015-03-25 22:55
Author: CtgAdm
Category: Educación, Nacional
Tags: educacion, gina parody, Ignacio Mantilla, mane, mario hernandez, Mesa Amplia Nacional Estudiantil, Ministerio de Educación, Reelección, U Nacional, Universidad Nacional
Slug: descontento-en-comunidad-estudiantil-de-la-nacional-por-reeleccion-de-mantilla
Status: published

##### Foto: [prensauniversidad.blogspot.com]

Pese a que el médico y profesor, Mario Hernández, había sido escogido por los estudiantes como rector de la Universidad Nacional, este miércoles el Consejo Superior Universitario reeligió a **Ignacio Mantilla por los próximos tres años**. De inmediato la comunidad estudiantil, mostró su rechazo a la elección del Consejo Superior y salió a las calles en rechazo del nombramiento de Mantilla.

[![Sin título](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/Sin-título.png){.aligncenter .wp-image-6458 width="511" height="475"}](https://archivo.contagioradio.com/descontento-en-comunidad-estudiantil-de-la-nacional-por-reeleccion-de-mantilla/sin-titulo-2/)

Mario Hernández, que había ganado la consulta de opinión con **un total de 7150 votos ponderados**, proponía recuperar la identidad publica de la universidad, “el consejo superior debe entender el mensaje que aporta a respetar la legitimidad y acompañamiento de la comunidad universitaria”, afirmó Hernández.

Sin embargo, el CSU hizo caso omiso de la opinión de la comunidad universitaria y eligió al actual rector, que de acuerdo al vocero de la Federación Medica Colombiana, ha hecho que “**la universidad entra cada vez más en una política de  auto-sostenibilidad y una lógica de mercado** que nos hacer ver como una empresa común y corriente que deberá parecer a la Universidad de los Andes o cualquier otra para sobrevivir”.

Cabe recordar que, esta decisión  la tomó el Consejo Superior, **liderado por la ministra de Educación, Gina Parody,** quien eligió a Mantilla como rector de la universidad.

Se espera que en los próximos días continúen convocándose  **marchas y movilizaciones** en todas las sedes del país de la U. Nacional, como rechazo a la reelección de Mantilla.

[![foto poder popular](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/CA9jOo8VAAAbaMG.jpg){.aligncenter .wp-image-6457 width="511" height="287"}](https://archivo.contagioradio.com/descontento-en-comunidad-estudiantil-de-la-nacional-por-reeleccion-de-mantilla/ca9joo8vaaabamg/)
