Title: Civilización y barbarie
Date: 2018-10-12 07:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: civilizacion y barbarie, Domingo Faustino, lideres sociales
Slug: civilizacion-y-barbarie
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/lideres-sociales-770x400-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### 12 Oct 2018 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

[Domingo Faustino Sarmiento propuso en su obra Civilización y Barbarie, un diálogo entre dos opuestos. De una parte, la civilización representada por la ciudad, lo europeo, lo norteamericano; y de otra parte la barbarie, representada por los pueblos aborígenes, por el campo, por el medio oriente. Ese intento de diálogo entre estos dos opuestos ha sido también, la base constitutiva de una cultura latinoamericana.]

[Civilización y barbarie hoy sobrepasan las intenciones de Domingo Faustino, pero el título de su obra, supera a su obra cuando el lenguaje hace lo suyo y desde la significación castellana, nos hipnotiza, nos convoca a desear que desaparezca la dicotomía, nos convoca a saber por qué… ¡por qué civilización y barbarie! Por qué aún no se agotan y siguen brotando como la sangre y el agua.  ]

[Civilización y barbarie son hoy, no solo en gran parte de nuestra América, sino particularmente en Colombia: el triunfo de una dialéctica que nos guía alegres y nos guía taciturnos.]

[Civilización y barbarie fundamentan todos los intentos por mantener intacto el poder, y todos los intentos por desarticularlo, por democratizarlo, por bajarlo a tierra, por hacerlo descender desde esos cielos pero no solo en forma de lluvia.]

[Civilización y barbarie controlan la génesis de la contradicción y aguardan a la vez, en un sentido multilateral la solución a tanto acierto desgarrado, a tanto desacierto incomprendido.]

Civilización y barbarie no siempre significa estudiados y no estudiados, no siempre significa cultos y no cultos, no siempre significa doctos e ignorantes, no siempre significa bondad y maldad, no siempre significan lo que significan por que un significado sin sentido manifiesto no tiene sentido. Y aquí lo manifiesto son muertos envueltos en papel regalo.

[Civilización y barbarie la que habla de una masacre de personas por razones políticas, en medio de la indiferencia, en medio de esa incapacidad casi biológica de sentir dolor alguno por la barbarie y que se camufla en la retórica tan civilizada.]

[Civilización y barbarie no son dos… es un único camino que, en vez de paseo, parece condena, que, en vez de imprimir esfuerzo, nos deja a la corriente de un devenir distópico, sencillamente porque el miedo al pasado se amplifica con la barbarie del presente.]

Civilización y barbarie significan decir que vamos bien, mientras los zapatos dejan huellas al paso por el charco de sangre que va cambiando de color rojo a color negro. Significa santificar al yo, sin reconocer lo pobrecito que es el yo, sin perspectiva de todo lo que hemos hechos nosotros para que exista.

Civilización y barbarie son 2 y son 3, son 10 y son 100, son 200 y 300, serán pronto más de 400 muertos desde que comenzó el conteo final; un final que siempre comparten la civilización y la barbarie cuando se la pasan teniendo sexo brutal sin más destino que la satisfacción de sus propias tesis.

Civilización y barbarie son el marco que encierra una tragedia en medio de la normalidad. Su efecto más grave, su consecuencia más pegajosa es la naturalización.

La naturalización de la realidad es el efecto de la civilización y la barbarie, porque no queda nada después de saber que matan, porque no queda nada después de creer y concluir racionalmente que no se puede hacer nada. Y es mejor siempre llevar la razón en la mano derecha, porque si hubiese tiempo de reflexionar, la incapacidad de sentir puede ser vista como una barbárica debilidad.

Civilización y barbarie fundamentan la hipocresía mundial ante la tragedia colombiana, que ofrece cuadros y planos desde los cuales se puede escoger un mensaje lo suficientemente imparcial para condenar a unos y salvar la conciencia de muchos, así sean tan culpables como el asesino mismo.

Civilización y barbarie aceptan las palabras, la retórica y la poética, la mezquindad y la pasión, aceptan las normas APA y el plagio, aceptan la mentira y la canción, porque juntas se revuelven pero no se mezclan.

Civilización y barbarie no son solo un título, son una invitación a pensarlas juntas, por separado, desde arriba, desde abajo, desde la izquierda y desde la derecha… no intentar separarlas es la única condición que le propusieron a Latinoamérica, y que lastimosamente hemos aceptado.

Civilización y barbarie, son todos y cada una de las estudiantes universitarios, que no percibieron lo domesticados que estaban, mientras corrieron a los brazos de la primera definición de libertad.

No hay rabieta que las separe, no hay suposición de indiferencia que escape a la dicotomía, ni la mejor de las series, ni el mejor de los memes, son capaces de quebrantar la infinita tristeza que causa la muerte del inocente, que puede simplemente estar oculta en la incapacidad casi biológica de sentir aunque sea un poco.

Por eso, tanto civilizado con la conciencia tranquila, sin preocupación, sin culpa, porque sencillamente no hay razones para sentir. La razón siempre se basta a sí misma, pero cuando intentó civilizadamente eliminar la barbarie, se convirtió en barbarie. No pudo mantener el control, como tampoco pudo la barbarie diferenciar entre aspirar y conspirar.

En medio de los aspectos más civilizados de nuestros territorios, perviven la barbarie de las muertes anunciadas de cientos de personas por causas políticas. Una injusticia a vox populi, una necesidad en las reuniones del poder. Un sin sentir a voz populi, una parte más del paisaje colombiano.

Por todo eso y aún mucho más, desde la distinción de Domingo Faustino hasta esta barbárica paráfrasis que no se hizo para complacer a filósofos o a ignorantes, y que nunca fue un homenaje ni una palabra vacía, siguen allí, Civilización y Barbarie, sentadas en el andén, taciturnas después del éxtasis, viendo pasar la gente, los autos, viendo caer el sol sin saber qué fecha es, esperando el nuevo día, aún sin saber para qué, viendo cómo la cifra aumenta, una cifra de muertos que nacieron, crecieron, lucharon y murieron en medio de la civilización y la barbarie.

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
