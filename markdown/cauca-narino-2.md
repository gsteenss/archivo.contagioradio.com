Title: Tumaco, municipio con mayor número de líderes asesinados desde 2016
Date: 2019-01-15 18:36
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: Amenaza a líderes sociales, Cauca, narcotrafico, nariño
Slug: cauca-narino-2
Status: published

###### Foto: Sonia Gutierrez 

###### 14 Ene 2019 

[Indepaz presentó un nuevo informe titulado **'Cauca - Nariño, Crisis de seguridad en el Posacuerdo'**, el cual revela la difícil situación con la que cerró el año el suroccidente colombiano,  en particular los departamentos de Cauca y Nariño que han sido testigos de un incremento en los índices de homicidio, desplazamiento forzado, desaparición y reclutamiento de menores y afectación a las comunidades.  
  
Aunque se presenta una notable reducción de confrontación armada en el Cauca y en la costa nariñense, en comparación con la que se vivió en la década anterior, también es evidente la aparición de un nuevo ciclo de violencias y la existencia de patrones de criminalidad y victimización contra líderes o activistas por derechos de sus comunidades.]

**Paramilitarismo**[  
]

[Según Indepaz, el accionar de grupos armados ilegales y el narcoparamilitarismo [son los elementos que agudizan la violencia en los municipios y su impacto sobre las comunidades. En Cauca, los municipios más afectados son **Miranda, Corinto, Caloto, Toribio, Santander de Quilichao, El Tambo y Argelia** mientras que en Nariño el municipio de Tumaco presenta una preocupante tendencia en alza en los homicidios los cuales ascendieron a 275.   
  
La presencia de estos grupos se evidencia a través de amenazas con panfletos, sufragios y llamadas, sin embargo no se ha identificado por parte de las FFMM la ubicación de campamentos o movimiento de tropa, lo cual impide un combate efectivo. Dentro de estas organizaciones también se incluyen personas que estuvieron en el proceso de paz pero decidieron volver  a las armas. [(Lea también Paramilitares hacen presencia en 149 municipios de Colombia: Indepaz) ](https://archivo.contagioradio.com/paramilitares-hacen-presencia-en-149-municipios-de-colombia-indepaz/)]  
]

**Minería y cultivos de uso ilícito  
**[  
De igual forma se pone en evidencia los diversos intereses geoestratégicos de movilidad y económicos del departamento del Cauca y Nariño, tanto de proyectos minero energéticos (legales e ilegales de extracción y comercialización) como de plantaciones de cultivos de uso ilícito, tráfico y comercialización.   
  
El informe señala que aunque no existen pruebas que relacionen a las empresas mineras con los actos de grupos paramilitares, es un indicio que “el accionar de estos grupos sean el ataque a los líderes y comunidades” las cuales precisamente se oponen a la megaminería en sus territorios, representando un obstáculo para sus intereses económicos y políticos.  
  
En municipios como Toribío y Corinto, se han ampliado los cultivos de coca y la producción de marihuana, actividades promovidas por agentes del narcotráfico que “han generalizado conflictos con las autoridades indígenas”  y permitido la presencia de grupos armados en el territorio étnico.]

[En Nariño, los territorios étnicos de los Awá y consejos de comunidades negras, han sido ocupados ilegalmente “por parte de colonos respaldados con armas y obligándoles a sembrar coca por orden de agentes del narcotráfico, asimismo las comunidades también deben enfrentar las iniciativas de inversionistas en palma que se disputan su territorio.  
**  
Líderes sociales y defensores de DD.HH, los más afectados**  
]

[[Desde la firma del Acuerdo de paz entre el Gobierno y las Farc se han incrementado las afectaciones a los procesos organizativos de comunidades étnicas y sus líderes. Según estadísticas de Indepaz, Cumbre Agraria y Marcha Patriótica en el Cauca se registraron 119 homicidios en contra de líderes sociales y defensores de derechos humanos entre enero de 2016 y diciembre de 2018 mientras que en Nariño, en este mismo periodo, se presentaron 49 casos.[(Le puede interesar En Colombia hay cinco conflictos armados: CICR)](https://archivo.contagioradio.com/colombia-conflictos-armados-cicr/)]]

[Según cifras de Indepaz, Tumaco es el municipio con mayor número de líderes asesinados desde el 2016: cuatro (4) asesinados en 2016, diecinueve (19) en 2017 y diez (10) en el  2018, los habitantes de las regiones señalan que ante el incumplimiento del acuerdo se ha deslegitimado a los líderes ante sus comunidades y los vuelto vulnerables de la acción de los armados ilegales.]

A pesar que se desconocen con exactitud los autores materiales o intelectuales de los homicidios, el informe hace énfasis en que el patrón de asesinato a líderes es el mismo en todos los casos, lo que evidencia que no se trata de casos aislados, sino que se trata de una sistematicidad en los ataques con el fin de generar “una atmósfera de inseguridad y zozobra generalizadas” y negar la garantía de los derechos fundamentales de las comunidades un escenario de posconflicto.

###### Reciba toda la información de Contagio Radio en [[su correo]
