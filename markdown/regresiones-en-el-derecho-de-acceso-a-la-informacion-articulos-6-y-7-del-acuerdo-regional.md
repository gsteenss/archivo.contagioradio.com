Title: Regresiones en el derecho de acceso a la información
Date: 2017-10-09 11:52
Category: Ambiente y Sociedad, Opinion
Tags: Ambiente
Slug: regresiones-en-el-derecho-de-acceso-a-la-informacion-articulos-6-y-7-del-acuerdo-regional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/paramo-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo 

#### **[Por: Natalia Gomez - Asociación Ambiente y Sociedad ](https://archivo.contagioradio.com/margarita-florez/)** 

[La sociedad civil está extremadamente decepcionada con la naturaleza regresiva de las disposiciones sobre el acceso a la información en una región que cuenta con una de las legislaciones más fuertes sobre el derecho a la información en el mundo. El acuerdo regional es un retroceso en el sentido de que no cumple con los estándares esbozados en la][[Ley Modelo Interamericana de]](https://www.oas.org/dil/esp/CP-CAJP-2840-10_Corr1_esp.pdf)[Acceso a la Información y no proporciona categorías o tipos de información ambiental que por su naturaleza pueda ser declarada como información pública en toda la región.]

[El hecho de no incluir un régimen de excepción desestabiliza el acuerdo, ya que no proporciona ninguna orientación sustantiva sobre las normas en toda la región para la liberación de información ambiental bajo regímenes reactivos. El texto tampoco proporciona un régimen de publicación proactivo obligatorio y progresivo para mejorar el acceso a información crítica, incluida información sobre emisiones, estudios de impacto ambiental, permisos, etc.]

[Las principales regresiones que vemos en los artículos 6 y 7 del acuerdo regional:]

1.  [No existe un estándar regional para un régimen de excepciones]
2.  [No hay ninguna categoría de información ambiental cuyo acceso nunca pueda ser negado por los estados]
3.  [No existe un deber general de los Estados de prestar asistencia a las personas que hacen una solicitud de acceso a información sobre el medio ambiente]
4.  [No existe una lista obligatoria de información que debe incluirse en los sistemas de información ambiental]
5.  [Existe retroceso en la provisión de acceso a documentos públicos que impactan el medio ambiente]

-   ****No se crea un estándar regional en el régimen de excepciones****

[El texto adoptado en el artículo 6 sobre el acceso a la información, aunque consagra el principio de máxima divulgación en el párrafo 1, limita su aplicación al no establecer un sistema de excepciones claro, limitado y estandarizado para toda la región y, en cambio, permite que cada país aplique su régimen domestico de excepciones, así sea mas restrictivo al negar el acceso a la información. El régimen de excepciones es fundamental para el funcionamiento de un sistema eficaz y transparente de acceso a la información ambiental. Para crear un acuerdo regional las partes debían crear una lista de información exenta.]

[La disposición de 6.5. es también incongruente ya que indica que las Partes pueden aplicar las excepciones. En ningún país una parte aplica excepciones, sino que debe ser una autoridad pública la que aplica las excepciones.]

[Un régimen de excepciones claro también proporcionaría orientación clara a los países signatarios que actualmente no tienen legislación sobre acceso a la información como San Kits and Nevis, Dominica, Granada y Santa Lucía.] [La lista de exenciones que existe no cumple con los estándares de la ley modelo en la región y no hubo una respuesta o una discusión sustantiva con dichos países sobre estas disposiciones.]

**Por último,** **el texto no establece un requerimiento sino que sólo alienta a cada Parte a adoptar un régimen de excepción que favorezca la divulgación de información.**

2.  **No se estableció ninguna categoría de información ambiental cuyo acceso nunca pueda ser negado por los estados**

[En el texto compilado sexta versión el articulo 6.7 establecía que la información relativa factores que afectaran negativamente el medio ambiente y la salud y la seguridad humanas no se consideraría exenta de divulgación. Este artículo, que fue borrado por los estados en la negociación en Argentina permitía que información de importancia fundamental para la protección del derecho a la salud y a un medio ambiente sano siempre fuera puesta en conocimiento de los ciudadanos. Artículos similares están incluidos en otros instrumentos de derecho internacional como por ejemplo el Convenio de Aarhus que establece que la información sobre emisiones de contaminantes al medio ambiente siempre deberá ser entregada, y la ley modelo interamericana sobre acceso a la información que establece que en ningún caso se podrá negar el acceso a la información en casos de graves violaciones a los derechos humanos o crímenes contra la humanidad.]

[El ahora artículo 6.6 contiene obligación vaga y sin precedentes de "tener en cuenta" las obligaciones de derechos humanos al aplicar el régimen de excepciones. No existe un estándar significativo en este artículo porque los países ya están obligados a aplicar los instrumentos de derechos humanos de los que son parte. Además, no está claro cómo se espera que funcione en este contexto, ya que no se expresa como una exención al régimen de excepciones y no forma parte de la prueba de interés público. Esto indudablemente creará problemas en la aplicación, ya que es una obligación vinculante que es difícil de interpretar y aplicar. Esto tiene ramificaciones tanto para el público como para las autoridades públicas, ya que las autoridades públicas tendrán dificultades para establecer que han cumplido con este requisito y el público tendrá dificultades para entender el alcance de este requisito en primer lugar.]

3.  **Se excluyo el deber general de los estados de prestar asistencia a las personas que hacen una petición de acceso a la información ambiental**

[El Artículo 6.2. estableció ciertos derechos que los Estados deben garantizar a las personas cuando se hace una solicitud de información ambiental como: hacer una solicitud sin mencionar ningún interés especial o explicar los motivos de la solicitud, y ser informado con prontitud si la autoridad tiene la información solicitada.]

[ Sin embargo, este artículo no incluye el deber general de los Estados de ofrecer asistencia a las personas al hacer una solicitud. El deber de asistencia es importante para asegurar que los peticionarios reciban información de manera oportuna. Cuando las autoridades ayudan a los peticionarios a aclarar sus solicitudes de información, pueden reducir el tiempo dedicado a buscar información. Este deber se encuentra en la Ley Modelo Interamericana de Acceso a la Información Pública de la OEA, s. 25 (2) y legislación sobre acceso a la información en Jamaica y Trinidad y Tobago, entre otros. Aunque el apartado 3 del artículo 5 se refiere a la prestación de asistencia al público en el ejercicio de los derechos de acceso, el texto utilizado en dicho artículo es "se esforzará" y, por lo tanto, no es vinculante.]

4.  **No hay una lista obligatoria de información que se deba divulgar proactivamente en el sistema de información ambiental**

[La falta de un régimen de excepción regional se ve agravada por la negativa de las partes a ponerse de acuerdo sobre un conjunto mínimo de información que debe ser divulgada de forma proactiva al público como un estándar regional. Esto perjudicará la capacidad del público para obtener acceso a información ambiental clave que actualmente no está disponible en muchos países, incluyendo permisos, estudios de impacto ambiental, contratos, informes de monitoreo, información de cumplimiento. Las disposiciones proactivas de acceso a la información ambiental en el acuerdo regional no llevan sustancialmente a la región hacia adelante, requiriendo una mejora progresiva a nivel nacional. Los Estados se comprometieron, en el párrafo 3 del artículo 7, a contar con sistemas de información ambiental, pero no acordaron una lista obligatoria de información que debía incluirse en los sistemas de información ambiental y simplemente incluyeron una lista que establecía ciertas directrices voluntarias.]

5.  **Hay una regresión en el acceso a documentos públicos que impactan el medio ambiente**

[El instrumento regional debería abordar la provisión de acceso a documentos e información en relación con el uso y la explotación de los recursos naturales. El nuevo artículo 7.9 establece que : “Cada Parte promoverá el acceso a la información ambiental contenida en las concesiones, contratos, convenios o autorizaciones otorgadas que involucren el uso de bienes, servicios o recursos públicos, de acuerdo con la legislación nacional.” Este artículo es problemático porque soslaya los estándares de muchos países en cuyas legislaciones ya se consagra que documentos como las concesiones y contratos públicos, y en vez se establece que las partes promoverán el acceso a la información ambiental de dichos documentos, pero no a los documentos como tal. Hay mucha información en estos documentos que a pesar de no ser estrictamente ambiental si impacta el medio ambiente y por ese debe ser de público conocimiento.]

#### **[Conozca aquí  las columnas de opinión de Ambiente y Sociedad](https://archivo.contagioradio.com/margarita-florez/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
