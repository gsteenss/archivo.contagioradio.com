Title: Congreso de los Pueblos denunció intento de Falso Positivo Judicial por parte del ejército
Date: 2018-01-18 20:58
Category: Judicial, Nacional
Tags: congreso de los pueblos, falso positivo judicial
Slug: congreso-de-los-pueblos-falso-positivo-judicial-por-parte-del-ejercito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Ejercito-Popular-de-Liberación-e1527526839809.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [18 Ene 2018]

Según la denuncia del Congreso de los Pueblos publicada este 16 de Enero, una pareja de esposos habría sido víctima de una intento de Falso Positivo Judicial cuando unidades de la Brigada XVI del ejercito que invadieron el patio de su casa, los engañaron para que transportaran material alusivo al ELN. El temor de la organización es que en el camino los hubiesen interceptado e involucrado en hechos delictivos.

El hecho sucedió el pasado 11 de Enero, en la vereda Volcán Blanco, jurisdicción del municipio de Aguazul (Casanare) y en la vivienda de los campesinos Andres Camilo Barrera Patiño y Diana Carolina Saganome Cardenas.

Según el comunicado l**os militares llegaron hasta la propiedad de los campesinos, se instalaron el patio**, se les solicitó retirarse para no poner en riesgo a los civiles dadas las condiciones de la región, sin embargo no se retiraron. Al día siguiente el Capitan Alvaro Arroyo les pidió transportar un paquete hasta la vereda el Charte, sin embargo al revisar el contenido los campesinos encontraron que se trataba de banderas y panfletos del ELN. [Lea también: Denuncian dos falsos positivos judiciales en Antioquia](https://archivo.contagioradio.com/denuncian-dos-nuevos-caso-de-falsos-positivos-judiciales-en-remedios-antioquia/)

Para evitar más acciones irregulares los campesinos convocaron a organizaciones de DDHH, devolvieron el paquete y reclamaron al militar por esa situación. L**a respuesta del funcionario público de las FFAA fue que la comunidad ayudaba más a la guerrilla que a ellos** y que simplemente se tomarían algunas fotografías del material que estaban enviando. [Lea también: El caso de David Ravelo una muestra de las injusticias del sistema judicial colombiano.](https://archivo.contagioradio.com/seguire-trabajando-por-demostrar-que-fui-victima-de-un-falso-positivo-judicial-david-ravelo/)

Según esa organización el riesgo de este tipo de acciones ilegales como judicializaciones, es alto, pues en la región se ha presentado una alta tasa de homicidios conocidos como “Falsos Postivos”, además de presencia de la guerrilla del ELN y actos de sabotaje a la infraestructura petrolera por parte de esa organización armada.

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
