Title: Gobierno ha incumplido 1300 acuerdos a la Minga Indígena
Date: 2017-10-30 12:47
Category: Movilización, Nacional
Tags: acuerdos incumplidos, indígenas, indígenas de Colombia, Minga Indígena, Movilización Indígena
Slug: inicia-minga-indigena-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/mimga-indigena-e1509385601387.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 Oct 2017] 

Los pueblos indígenas de Colombia dieron inicio a las movilizaciones indefinidas para exigir el cumplimiento **de más de 1300 acuerdos** que se han pactado con el Gobierno Nacional. Igualmente, indicaron que la Minga Indígena se realizará para rechazar los más de 40 asesinatos de la comunidad indígena desde la firma de los acuerdos de paz.

En rueda de prensa, diferentes organizaciones indígenas manifestaron que “frente a las sistemáticas violaciones a los DDHH, los derechos de los pueblos indígenas y étnicos, comunidades campesinas y la sociedad civil en general, convocamos al pueblo colombiano a esta **Minga Nacional por la vida, el territorio, la paz** y el cumplimiento de los acuerdos”.

Los puntos del pliego que presentan los indígenas al Gobierno Nacional tienen que ver con **la implementación del acuerdo con las FARC** en lo relacionado con el Capítulo Étnico, las garantías de seguridad para la movilización y para los líderes y el cumplimiento de acuerdos en materia de tierras indígenas. Además cada organización tiene un pliego de exigencias regional que funcionan ligados al pliego nacional. (Le puede interesar:["Ya no estamos para nuevos acuerdos, queremos hechos: indígenas de Kokonuco"](https://archivo.contagioradio.com/comuneros-exigen-al-gobierno-devolucion-de-territorios-ancestrales-en-kokonuko-cauca/))

### **Más de 100 mil indígenas se movilizan en Colombia** 

<iframe src="https://co.ivoox.com/es/player_ek_21774279_2_1.html?data=k5akmZmWe5qhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5ynca3pytiYqMnTb6LmysbgjdjTptPZjMrZjc7SrcTd0JDRx5DQpdSfztTjy9HNvsLXytTbx9iPqc-hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Luis Fernando Arias, consejero de la Organización Nacional Indígena de Colombia-ONIC-, dijo a Contagio Radio, que **más de 100 mil indígenas se movilizarán en 16 departamentos** del país. Afirmó que exigirán ser incluidos en el plan marco de la implementación de los acuerdos de paz como también el cumplimiento de acuerdos en temas de territorialidad, educación, salud y “demás asuntos que conciernen a los pueblos indígenas”.

Indicó que las movilizaciones en los diferentes departamentos se **realizarán de manera pacífica** para presionar al Gobierno a que se instale la mesa de negociaciones. Afirmó que, por primera vez en el país, la movilización reunirá a las 5 organizaciones indígenas “que tienen asiento en la mesa permanente de concertación”. Además, ante la presidencia de la República, radicarán el pliego de exigencias de 5 puntos de carácter regional.

### **En "la Delfina" en el Valle del Cauca se concentran más de mil indígenas** 

<iframe src="https://co.ivoox.com/es/player_ek_21774206_2_1.html?data=k5akmZmWdJehhpywj5WXaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncajpytLax9ePldbd09SSlKiPtsbk0Nfhw5DIqdTYxpDSzpDWqdTb1sbfxtSPqc-frcaYpsrQqsriwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Guimmer Quiro, reportero de CONPAZ en el resguardo de la Delfina vía a Buenaventura, manifestó que hay **más de mil personas presentes en los puntos de encuentro** de la movilización. Allí, 80 comunidades indígenas se unirán a las exigencias nacionales de cumplimiento de los acuerdos y respeto por la vida. (Le puede interesar:["Mujeres indígenas del mundo siguen luchando por sus derechos"](https://archivo.contagioradio.com/mujeres-indigenas-luchan-por-sus-derechos/))

Hacia las 10 de la mañana se presentó un bloqueo por parte de los comuneros y la respuesta del ESMAD dejó un saldo inicial de **dos indígenas heridos** por los disparos realizados por la policía.

### **En la Sierra Nevada se movilizarán más de 2 mil indígenas** 

<iframe src="https://co.ivoox.com/es/player_ek_21774340_2_1.html?data=k5akmZmXeJGhhpywj5WXaZS1kpiah5yncZOhhpywj5WRaZi3jpWah5ynca_j08rmjbLFr9af1NTP1MqPsNDnjM7bxdrRtM3dzs7S0NnTt4zYxtGYqdTGcozCwtGYw5DQs9Shhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con Norey Maku, integrante de la Confederación Indigena Tayrona, el Gobierno Nacional les ha incumplido varios acuerdos como **el referente a la protección del territorio ancestral** de la Sierra Nevada de Santa Marta. Dijo que también está pendiente la ampliación del resguardo de la Sierra, por lo que los territorios que se han incorporado con recursos propios, carecen de protección legal.

Adicionalmente, indicó que la Sierra Nevada **está siendo amenaza por las actividades minero energéticas** pues hay más de 200 títulos mineros que “generan un riesgo para el territorio y la supervivencia de los pueblos indígenas”. Por esto, de los 4 pueblos de la Sierra Nevada, se van a movilizar alrededor de 2 mil indígenas.

### **En el Amazonas exigen el respeto por los pueblos indígenas** 

<iframe src="https://co.ivoox.com/es/player_ek_21774378_2_1.html?data=k5akmZmXe5mhhpywj5WXaZS1lpuah5yncZOhhpywj5WRaZi3jpWah5yncafVz96YrdrNttCf1NTP1MqPsMKfz9SYy9PHsNbnyoqwlYqmd8-fxcqYy9PIaaSnhqaxycrSpdSfxtOYx9GPtI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según Fany Kuiro, integrante de la Organización de los Pueblos Indígenas del Amazonas-OPIAC-, los indígenas de Colombia **no están dentro del plan marco de implementación** en lo que tiene que ver con el Capítulo Étnico del mismo. Dijo que en la Amazonía hay 62 pueblos indígenas y hay 6 departamentos que no están dentro de los sistemas de información de Planeación Nacional, por lo que no tienen figura de entidad territorial. (Le puede interesar: ["Indígenas AWÁ denuncian presencia de grupos armados en sus territorios"](https://archivo.contagioradio.com/indigienas-awa-denuncian-presencia-de-grupos-armados-en-sus-territorios/))

Afirmó que las actividades de movilización se realizarán de manera pacífica en el Amazonas y desde allí, **se unirán a la Minga Indígena de manera permanente**. También indicó que desde el Amazonas han rechazado las acciones de violencia contra los líderes indígenas que han sido amenazados y asesinados en los diferentes territorios del país.

Finalmente, las organizaciones hicieron un llamado a la opinión pública para que los diferentes sectores de la sociedad se unan a la Minga Indígena para exigir el cumplimiento de los acuerdos. Además, le pidieron al Gobierno Nacional **garantías de seguridad para poder ejercer su derecho a la protesta** y a la movilización social.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
