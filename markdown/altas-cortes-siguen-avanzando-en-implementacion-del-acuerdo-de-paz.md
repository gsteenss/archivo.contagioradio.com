Title: Altas Cortes siguen avanzando en implementación del Acuerdo de Paz
Date: 2017-06-14 12:15
Category: Judicial, Nacional, Paz
Tags: Altas Cortes, Cortes, FARC, paz
Slug: altas-cortes-siguen-avanzando-en-implementacion-del-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Corte-constitucional-e1470436473989.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Tnn Políticas 

###### 14 Jun. 2017

A propósito de los avances que continúan dándose en el marco de la implementación de los Acuerdos de Paz, la Corte Constitucional aseguró que en los próximos días se dará el fallo de la ley 1830, por medio de la cual **se podrán designar 3 voceros o voceras de las FARC en cada una de las cámaras del Congreso** para que participen en los debates de los proyectos de reforma constitucional que serán tramitados.

Según la Ley 1830 estas personas **podrán intervenir en los debates, pero no contarán con voto para dichos proyectos.** Es decir, participarán de la misma manera que lo hacen en la actualidad los y las integrantes del movimiento Voces de Paz. Le puede interesar: [Las FARC-EP y sus posibles coaliciones para las elecciones 2018](https://archivo.contagioradio.com/las-farc-ep-y-sus-posibles-coaliciones-para-las-elecciones-2018/)

De esta manera, se siguen dando pasos con miras a entregar todas las posibilidades y garantías para que las FARC puedan comenzar su participación en política.

### **Corte Suprema pide aclarar quiénes pueden hacer parte de la JEP** 

En el marco de la acogida de los integrantes de la guerrilla de las FARC a la Jurisdicción Especial para la Paz – JEP –, el alto tribunal reiteró su llamado a los máximos dirigentes de esa guerrilla, al Alto Comisionada para la Paz y a la Fiscalía General de la Nación para que **se certifique de manera estricta las personas que sean parte de esta jurisdicción.** Le puede interesar: [Ley de Amnistía la manzana de la discordia](https://archivo.contagioradio.com/ley-de-amnistia-la-manzana-de-la-discordia/)

Esta aclaración la hizo al recibir la postulación a la libertad condicionada de Santo Román Narváez Ansazoy quien dice ser integrante de la guerrilla de las FARC, pero a quien no se le ha podido comprobar que sus actividades hayan estado enmarcadas en medio del conflicto armado. Narváez Ansazoy es requerido por la justicia de Estados Unidos en extradición.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="osd-sms-wrapper">

</div>
