Title: Ecuador de nuevo en las calles contra imposiciones de Lenin Moreno
Date: 2020-05-26 21:18
Author: CtgAdm
Category: Nacional
Slug: ecuador-de-nuevo-en-las-calles-contra-imposiciones-de-lenin-moreno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Ecuador.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Ecuador @RiksinakuyEc

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desafiando a las medidas de aislamiento ocasionadas por el Covid-19 en Ecuador, un lugar afectado por al menos 36.000 casos de contagio, más de 2.000 personas pertenecientes a centrales sindicales, organizaciones sociales y estudiantes salieron a marchar el pasado 25 de mayo, en ciudades como Quito y Guayaquilpara manifestarse en contra de las medidas económicas y reformas labores del gobierno de Lenin Moreno que han llevado al cierre de varias empresas públicas y la pérdida de empleos de 150.0000 personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Edison Hurtado, doctor en Sociología de la Facultad Latinoamericana de Ciencias Sociales** y quien ha trabajado en temas como protesta y acción política de Ecuador señala que gran parte del malestar se debe a la Ley de Apoyo Humanitario, aprobada por la Asamblea Nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el experto, esta ley obedece a una estrategia del Gobierno que se implementó desde mayo de 2018 al llegar al Ministerio de Economía y Finanzas, personas como Richard Martínez, dirigente empresarial quien ha implementando un plan que beneficie los intereses de los grupos empresariales más grandes del país. [(Lea también: ¿Por qué Ecuador se vuelca a las calles?)](https://archivo.contagioradio.com/ecuador-tras-protestas/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Educación, empleo y gasto público son las víctimas del gobierno en Ecuador

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Dentro de las medidas anunciadas por Lenin Moreno resaltan los recortes en el gasto público, incluido el cierre de empresas estatales y embajadas, reducción del dinero para las universidades de casi \$100 millones de dólares, lo que podría afectar a cerca de 30 centros educativos, la reducción de horas de trabajo del sector público en un 25% lo que equivale a un recorte salarial del 16%.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Lenin Moreno eliminó por lo menos siete empresas públicas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, son siete las empresas públicas que serán eliminadas incluidas F**errocarriles, Siembra, Medios Públicos, Centro de Entrenamiento para el Alto Rendimiento Ecuador Estratégico, Correos del Ecuador y la Unidad Nacional de Almacenamiento**, además de la liquidación de la aerolínea ecuatoriana Tame, lo que llevaría además al cierre de 3.695 empleos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el profesor se trata de un "neoliberalismo tentacular", que por distintas vías ha querido implementar estas agendas de reformas estructurales y que se alinean con el Fondo Monetario Internacional, y que han sido consideradas inconstitucionales para gremios, sindicatos y colectivos de trabajadores. [(Le puede interesar: ¿Levantar el paquetazo, o buscar la salida de Lenín Moreno en Ecuador?)](https://archivo.contagioradio.com/levantar-el-paquetazo-o-buscar-la-salida-de-lenin-moreno-en-ecuador/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"La gente está sintiendo las medidas, pero en pocas meses cuando no hayan ingresos, será una crisis humanitaria mucho más fuerte que la que hay por el Covid-19, hay una conciencia política muy fuerte, están saliendo arriesgándo su salud pero son medidas que están golpeando a los sectores de forma inhumana".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "En Ecuador, el golpe al empleo se da cerrando las oportunidades laborales y reduciendo los salarios"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Dichas reformas aprobadas permiten unos acuerdos entre empleadores y empleados para renovar y bajar los sueldos, sin embargo advierte el profesor que dicha decisión da mucho poder a las empresas para despedir a sus empelados por fuera de los derechos laborales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"no se permite que se tengan derechos a un seguro de desempleo o a reconocer los años que han aportado a la empresa, simplemente se reducen las horas laborales para bajar los salarios" asegura el experto. [(Lea también: Lenin Moreno anuncia que recuperará el orden en Ecuador, desplegando las FFMM)](https://archivo.contagioradio.com/lenin-moreno-anuncia-que-recuperara-el-orden-en-ecuador-desplegando-las-ffmm/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los demás decretos hablan de la reducción del aparato estatal, ahora se está exigiendo el cierre de empresas públicas y despidos de trabajadores, El Decreto 1053 que modifica la jornada laboral para los servidores públicos, contempla que la medida se aplique por 6 meses o máximo un año y empezaría a regir a partir del 1 de junio. [(Le puede interesar: De Francia a Latinoamérica, la lucha contra la precarización de la vida)](https://archivo.contagioradio.com/de-francia-a-latinoamerica-la-lucha-contra-la-precarizacion-de-la-vida/) 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la movilización del día lunes no contó con la participación de la[Confederación de Nacionalidades Indígenas del Ecuador](https://twitter.com/CONAIE_Ecuador), el profesor advierte que el colectivo junto a otros sectores ya están evaluando la situación para unirse a la movilización, mientras sectores medios, populares y urbanos se han volcado a las calles en rechazo a las medidas tomadas por Lenin Moreno. [(Lea también: Movimiento indígena logra derogar el decreto 883 en Ecuador)](https://archivo.contagioradio.com/movimiento-indigena-logra-derrogar-paquetazo-en-ecuador/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Creo que se va a activar un escenario de conflicto que no va a parar, además se está viviendo un escenario pre electoral, es un gobierno totalmente desligitimado que no tiene nada que perder" afirma Hurtado quien señala la importancia de que exista una coalición que haga frente a un gobierno liderado por el sector bancario, condonando impuestos y permitiendo la salida de capitales. [(Lea también: 2020 una oportunidad para unificar y concentrar las exigencias sociales: ONIC)](https://archivo.contagioradio.com/2020-oportunidad-unificar-exigencias-sociales-onic/)

<!-- /wp:paragraph -->

<!-- wp:quote {"className":"is-style-default"} -->

> El punto aquí no es que el Covid-19 está causando crisis económica, el tema es cómo los gobiernos gestionan la crisis sanitaria para ver quien la paga y en el Gobierno ecuatoriano se ha hecho que toda la crisis la paguen los sectores sociales y obreros sin tocar al sector financiero - Edison Hurtado

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque 2019 fue un año en el que la movilización social se erigió en diferentes países de Latinoamérica incluido no solo Ecuador sino otras naciones como Brasil, Chile y Colombia y en otras latitudes como Haití y China, para el profesor, la coyuntura de la pandemia ha sido la excusa para que los Gobiernos contra los que se ha manifestad un sector de la sociedad, aceleren su agenda lo que ha fijado las condiciones para que los sectores sociales salgan a protestar pues la afectación hacia ellos ha sido directa. [(Lea también: Chile: Los efectos de un modelo económico fallido)](https://archivo.contagioradio.com/chile-los-efectos-de-un-modelo-economico-fallido/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
