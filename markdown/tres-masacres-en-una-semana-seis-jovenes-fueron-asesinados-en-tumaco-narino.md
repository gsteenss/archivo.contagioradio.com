Title: Tres masacres en una semana, seis jóvenes fueron asesinados en Tumaco, Nariño
Date: 2020-08-22 11:48
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Gobierno Duque, nariño, Regreso de las masacres, Tumaco
Slug: tres-masacres-en-una-semana-seis-jovenes-fueron-asesinados-en-tumaco-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Centro.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Tumaco.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Tumaco, Nariño / @FTC\_HERCULES

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A los hechos de violencia ocurridos en el municipio de Samaniego donde fueron asesinados ocho jóvenes y los tres comuneros Awá víctimas de homicidio en zona rural de Ricaurte, **se suma una nueva masacre en el sector La Guayacana, en Tumaco donde hombres armados asesinaron a seis jóvenes y desaparecieron a dos más.** El suceso agudiza la violencia que se vive en el departamento donde grupos armados se disputan el territorio pese a una alta presencia militar incluida La Fuerza de Tarea Conjunta Hércules.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“**Se trata de seis personas jóvenes, que fueron asesinadas en el sector de La Guayacana.** en este momento es materia de investigación", explicó el gobernador del departamento John Rojas**.** Los hechos ocurren a tan solo horas de que se dé un consejo de seguridad al que asistirá el presidente Iván Duque. [(Lea también: Nueva masacre, en Nariño fueron asesinados 9 jóvenes)](https://archivo.contagioradio.com/nueva-masacre-en-narino-fueron-asesinados-9-jovenes/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Parael 16 de agosto [Naciones Unidas](https://twitter.com/ONUHumanRights) **había documentado 33 masacres, sin contar otras 7 que están por ser registradas y otras tres nuevas ocurridas en las últimas horas en los departamentos de Arauca, Cauca y el suceso del que se conoció esta mañana en Nariño.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente a los sucesos de las últimas horas el presidente Duque se pronunció a través de su cuenta en Twitter en el que hizo alusión a la violencia que se está viviendo en los territorios. Al respecto, la ciudadanía a través de redes sociales ha criticado la respuesta del Gobierno ante las masacres consecutivas exaltando que más allá de ser información inexacta, no se debe responder al país a través de estadísticas ante un fenómeno que ha adquirido mayor frecuencia con el paso de los días. [(Le puede interesar: En Gobierno Duque han ocurrido 1.200 violaciones de DDHH contra el pueblo Awá en Nariño)](https://archivo.contagioradio.com/en-gobierno-duque-han-ocurrido-1-200-violaciones-de-ddhh-contra-el-pueblo-awa-en-narino/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanDuque/status/1297151862295199745","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanDuque/status/1297151862295199745

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Contrario a la información entregada por el Gobierno, fuentes como la Oficina del Alto Comisionado para los derechos humanos en Colombia y la Base de datos ¡Basta Ya! del Centro Nacional de Memoria Histórica demuestran cómo **la violencia ha ido en aumento en dos años de la administración. Duque**. [(Lea también: Las razones que explican el regreso de las masacres a Colombia)](https://archivo.contagioradio.com/las-razones-que-explican-el-regreso-de-las-masacres-a-colombia/)

<!-- /wp:paragraph -->

<!-- wp:image {"id":88625,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Centro.png){.wp-image-88625}

</figure>
<!-- /wp:image -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/camilorey/status/1297176098732728321","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/camilorey/status/1297176098732728321

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Nariño un complejo mapa de actores armados

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque autoridades han apuntado a la responsabilidad del ELN, el grupo desmintió las afirmaciones que se han hecho sobre su participación en la masacre de Samaniego ocurrida hace una semana; contrario a ello denunciaron que en esa región existe una alta presencia militar y que además habría una alianza del Ejército y la Policía con organizaciones al servicio del narcotráfico.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Señalaron en particular al grupo de Los Contadores, cuyo origen deriva de alianzas y vínculos con el frente Óliver Sinisterra y las Guerrillas Unidas del Pacífico. Otro grupos presentes en la zona son las autodenominadas Autodefensas Gaitanistas de Colombia (Agc), Los Nuevos Delincuentes, La Gente del Nuevo Orden, Los de Sabalo e incluso facciones del EPL.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
