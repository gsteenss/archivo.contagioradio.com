Title: ASCAMCAT denuncia aumento de amenazas y hostigamientos a sus dirigentes
Date: 2017-01-31 17:55
Category: DDHH, Nacional
Tags: amenazas a líderes, ASCAMCAT, José Gregorio Bayona
Slug: ascamcat-denuncia-aumento-de-amenazas-y-hostigamientos-a-sus-dirigentes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/gregorio-bayona.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ANZORC] 

###### [31 Ene 2017] 

José Gregorio Bayona dirigente de la Asociación de Campesinos del Catatumbo –ASCAMCAT–, fue capturado por agentes de la policía en el municipio del Zulia cuando se movilizaba desde el corregimiento Las Mercedes hacia Cúcuta. La policía asegura que Bayona tiene una orden de captura desde el 2009, por el delito de rebelión, e integrantes de ASCAMCAT  desmienten dicha versión, **afirman que dicha orden no es vigente y que no se ha encontrado prueba alguna de culpabilidad del líder social.**

Johny Abril integrante de ASCAMCAT, manifestó que esta retención arbitraria **hace parte de una serie de “amenazas y persecuciones que vienen desde 2011”**, debido al trabajo organizativo realizado en pro de los derechos de los campesinos “en todo el Catatumbo, y por el trabajo de José Gregorio en la vereda Sardinata, corregimiento de Las Mercedes”.

Abril señala que este es un caso mas donde “se amenaza e intimida al movimiento campesino” y revela que recientemente han denunciado ante la fiscalía y organizaciones de derechos humanos la recepción de **panfletos “firmados por los rastrojos”, donde amenazan con el “exterminio del movimiento social** (…) esto lo sabe el Gobierno, si algo nos pasa es responsabilidad de ellos”.

Este integrante de la asociación campesina, comenta que muchos integrantes de ASCAMCAT han venido sido hostigados, algunos tienen ordenes de captura por delitos de rebelión y otros tienen investigaciones judiciales **“por supuestos delitos que nosotros ni siquiera conocemos”.**

Por último, Abril hace un llamado de atención al Gobierno, pues “tenemos una mesa de interlocución y el Gobierno la tiene paralizada” denuncia que no han recibido respuesta alguna del futuro de José Gregorio Bayona, y **esperan que al estar proscrita la orden de captura del 2009, se esclarezcan los hechos y el dirigente sea liberado el día de hoy.**

<iframe id="audio_16757023" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16757023_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
