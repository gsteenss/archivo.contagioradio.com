Title: Bajo constante amenaza viven líderes sociales en Cauca
Date: 2018-07-25 13:50
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, Cauca, francisco isaias cifuentes, lideres sociales, marcha patriotica, Panfleto
Slug: bajo-amenaza-viven-lideres-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/eurodiputados-lideres-sociales1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [25 Jul 2018] 

La Red de Derechos Humanos “Francisco Isaías Cifuentes”, denunció que diferentes  líderes sociales, afrodescendientes, indígenas y campesinos,  organizaciones sociales, que hacen parte del Proceso de Unidad Popular del Suroccidente Colombiano (PUPSOC) y del Movimiento Marcha Patriótica, recibieron amenazas a través de llamadas y panfletos, en relación al trabajo político y social que hacen en el territorio.

Según la organización defensora de Derechos Humanos, el miércoles 18 de julio, Arbey Gómez, integrante de Marcha Patriótica en Almaguer, Cauca, **recibió una llamada de un hombre que se identificó como integrante del ELN**, cobrando una extorsión y amenazándolo en caso de no pagar el dinero.

Posteriormente, el sábado 21 de julio Gómez recibió otra llamada en la que un hombre lo señala de ser miembro de disidencias de las FARC, advirtiéndoles que de no pagar el dinero de la extorsión, **deberá abandonar la zona o atenerse a ser objeto de un atentado contra su vida.**

### **Hombres armados ingresan a la vivienda de líder en Balboa** 

El mismo sábado, **3 hombres armados ingresaron a la vivienda de Solmey Botina,** líder campesino del PUPSOC y miembro de Marcha Patriótica; preguntando por su paradero en momentos en que se encontraba fuera del municipio. Tras percatarse de la ausencia de Botina, se retiraron de la vivienda con rumbo desconocido. (Le puede interesar: ["Los asesinatos de líderes sociales son sistemáticos: Ángela María Robledo"](https://archivo.contagioradio.com/asesinatos-de-lideres-sociales-son-sistematicos-angela-robledo/))

### **AGC amenazan con panfleto a líderes de Miranda** 

El domingo 22 de julio, Cristobal Guamanga, quien hace parte de la Asociación Pro Constitución de la Zona de Reserva Campesina del Municipio de Miranda, Cauca, miembro del PUPSOC y de Marcha Patriótica; **recibió vía Whatsapp un panfleto amenazante contra él y todos los líderes sociales del territorio** por parte de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC).

Por estas razones, el equipo de la Red Francisco Isaías Cifuentes, pidió al Estado colombiano **que se emprendan las acciones legales y jurídicas pertinentes para investigar las responsabilidades materiales e intelectuales de estas amenazas,** así como tomar las medidas necesarias para proteger a los líderes y comunidades que integran el PUPSOC y el Movimiento Marcha Patriótica, con el fin de garantizar los procesos sociales y la vida digna de las personas en la zona.

\[caption id="attachment\_55075" align="aligncenter" width="609"\][![Captura de pantalla 2018-07-25 a la(s) 11.49.48 a.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-25-a-las-11.49.48-a.m.-609x523.png){.wp-image-55075 .size-medium width="609" height="523"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-25-a-las-11.49.48-a.m..png) Panfleto amenazante de las autodenominadas AGC\[/caption\]

######  

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio](http://bit.ly/1ICYhVU)]
