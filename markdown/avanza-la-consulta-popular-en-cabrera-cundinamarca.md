Title: Avanza la Consulta Popular en Cabrera Cundinamarca
Date: 2017-02-06 14:30
Category: Ambiente, Nacional
Tags: Consulta Popular Minera en Cabrera, EMGESA, Páramo del Sumapáz
Slug: avanza-la-consulta-popular-en-cabrera-cundinamarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Cabrera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Historia Boyacá] 

###### [6 Feb 2017] 

La Consulta Popular Minera que será llevada a cabo el próximo 26 de Febrero en el municipio de Cabrera Cundinamarca, tiene como objetivo no sólo frenar el proyecto ‘El Paso’, que pretende construir **14 microcentrales hidroeléctricas sobre el río Sumapáz**, sino también el avance de exploraciones para los bloques petroleros COR33 y COR 4.

Paola Bolaños vocera e integrante de la Zona de Reserva Campesina de Cabrera, manifestó que de llevarse a cabo dichos proyectos extractivos, otros municipios como **Venecia, Pandi e Icononzo en el Tolima, se verían gravemente afectados.** ([Le puede interesar: Habitantes de cabrera decidirán sobre micro-hidroeléctricas en Sumapáz](https://archivo.contagioradio.com/35010/))

Bolaños reveló que el proyecto hidroeléctrico abarca toda la cuenca media del río Sumapáz, “desde el nacimiento, en la parte alta que es zona de amortiguamiento del páramo, hasta la desembocadura en el Boquerón de Icononzo”.

La vocera también señaló que ese tipo de “actividades nocivas” afectan lo ambiental y también lo cultural, **“la identidad campesina y la vocación agropecuaria de nuestros municipios se está viendo vulnerada”.**

### Las comunidades deciden 

Desde el año 2000, las comunidades campesinas de Cabrera se conformaron como Zona de Reserva Campesina –ZRC–, estas familias, de manera concertada han construido la pregunta que les permitirá echar para atrás el proyecto de EMGESA y ENDESA, ¿Está usted de acuerdo si o no, que en el municipio de cabrera como ZRC, se ejecuten proyectos mineros y/o hidroeléctricos, que transformen o afecten el uso del suelo, el agua o la vocación agropecuaria del municipio?

Paola Bolaños aseguró que en varias ocasiones se han dirigido al Gobierno Nacional para que invierta en proyectos agropecuarios, **“en nuestro plan de desarrollo sostenible y no en estos proyectos que van en contravía** (…) **exigimos respeto por nuestras costumbres, por nuestro trabajo”.**

Por último la vocera extiende la invitación a diferentes organizaciones ambientales y defensoras del agua, la vida y el territorio, para que acompañen **el próximo 26 de febrero la Consulta Popular para decirle “No al Paso”.**

<iframe id="audio_16852294" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16852294_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
