Title: En manos de Lidio García está la decisión de dar vida a las curules de las víctimas
Date: 2019-12-20 19:42
Author: CtgAdm
Category: Nacional
Tags: acuerdo de paz, curules de paz, paz, víctimas
Slug: en-manos-del-presidente-del-senador-esta-la-decision-de-dar-vida-a-las-curules-de-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/curules.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/lidio-garcia-curules-de-las-victimas.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:elgrifonoticias] 

Este 20 de diciembre integrantes de Defendamos la Paz presentaron un solicitud ante el Presidente del Senado Lidio García Turbay, **solicitando que reconsidere la decisión de su predecesor en diciembre del 2017, de negar la representación política de las victimas** en la Cámara de Representantes. (Le puede interesar: [Propuesta del Gobierno sobre Curules de Paz busca desconocer lo acordado en La Habana](https://archivo.contagioradio.com/curules-gobierno-desconocer-acuerdo-habana/))

Solicitan al Presidente del Senado anular acto legislativo para revivir curules de las víctimas

Este acto legislativo fue aprobado en el Senado el 30 de noviembre de 2017, con 50 votos por el si y 7 por el no, a pesar de estos resultados la Mesa Directiva del Congreso se negó a continuar con el trámite; a la luz de esto, el documento destaca que existen dos pronunciamientos  de la Corte Constitucional, organismo encargado de hacer veeduria en el desarrollo de normas con relación al Acuerdo de Paz, uno de ellos indica que la interpretación dada por la Mesa en aquel entonces fue errada, lo que indica que el Acto Legislativo si fue aprobado en ese entonces.

Ante esto Juan Fernando Cristo ex ministro de interior e integrante de Defendamos La Paz, afirmo, "Lo que se hizo fue un maniobra política para debilitar estas curules, y se debe poner nuevamente en vigencia algo que ya fue legal, destacando que este es un claro acto en apoyo y reconocimiento al Proceso de paz, tener representantes que puedan decir de primera mano como abordar el tema del conflicto". (Le puede interesar: [Precandidatos a Curules de paz y víctimas protestan en Bogotá](https://archivo.contagioradio.com/precandidatos-a-curules-de-paz-y-victimas-protestan-en-bogota/))

Así mismo el documento resalta tres punto que apelan a la negación de estas curules, la primera, afirma que este acto va en contra de la Constitución y la ley, la segunda, afirma que ésta no va en coherencia con los intereses públicos y sociales, y por último que destaca "el acto causa agravio injustificado no sólo a una persona sino a las 8.920.473 víctimas inscritas en el Registro Único de Víctimas (RUV)".

### ¿Cómo las víctimas podrán obtener estas curules?

Las curules que se están defendiendo son 16, y tienen como fin otorgar poder y voz en la rama Legislativa a las víctimas del conflicto armado, divididas en  los departamentos que han sufrido mayores indices de conflicto armado como,  Arauca, Antioquia, Bolívar, Cauca, Caquetá, Cesar, Córdoba, Chocó, Meta, Nariño, Norte de Santander, Putumayo y Tolima; los congresistas electos para Cámara de Representantes estarán de manera temporal y durante 2 periodos electorales.

Asimismo según el  Acuerdo de Paz, estos candidatos deben habitar mayormente los territorios o en su defecto que sean desplazados de estos, así como también deben ser integrantes de organizaciones campesinas, de desplazados, desmovilizados o otras que trabajen por la paz. (Le puede interesar: [No fue correcto archivar acto legislativo de las curules de paz: Alfredo Beltrán](https://archivo.contagioradio.com/tribunal_superior_circunscripciones_paz/))

De igual forma Cristo resalto que los representantes deben integrar partidos independientes, "los partidos políticos que cuentan con representación en el Congreso o con personería jurídica, incluido el partido FARC, no podrán inscribir candidatos", y aclaró que en este no se incluyen victimas en ciudades como Bogotá y sus campañas políticas contarán con financiación especial y con acceso a medios regionales.

La respuesta a esta solicitud de Defendamos la Paz  y que ha sido liderada por el senador Roy Barrera, se estará definiendo antes de marzo del 2020 y se verá reflejada de ser aceptada en las elecciones de 2022, cuando se convoque a elecciones del Congreso para ese año. (Le puede interesar: [No podemos hablar de paz sin nuestra participación política: víctimas del conflicto](https://archivo.contagioradio.com/no-podemos-hablar-de-paz-sin-nuestra-participacion-politica-victimas-del-conflicto/))

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" scrolling="no" width="450" height="80" frameborder="0"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
