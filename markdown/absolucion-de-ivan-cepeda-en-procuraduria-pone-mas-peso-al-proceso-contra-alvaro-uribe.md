Title: Absolución de Iván Cepeda en Procuraduría pone más peso al proceso contra  Álvaro Uribe
Date: 2020-08-25 22:33
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Álvaro Uribe, Iván Cepeda, Manipulación de testigos, Procuraduría General de la Nación
Slug: absolucion-de-ivan-cepeda-en-procuraduria-pone-mas-peso-al-proceso-contra-alvaro-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Ivan-Cepeda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este martes, se conoció **la decisión de la Procuraduría General de la Nación -[PGN](https://www.procuraduria.gov.co/portal/)- en la que absuelve al senador Iván Cepeda, en relación con las acusaciones que en su momento hiciere el expresidente Álvaro Uribe Vélez, sobre supuestamente recibir falsos testimonios a cambio de ofrecimientos.**  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El ente de control, ya había emitido una decisión en favor de Cepeda el pasado 30 de julio de 2018; no obstante, la defensa de Uribe instauró un recurso de reposición contra la misma.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la decisión conocida el día de hoy, **la Procuraduría reafirmó las imprecisiones advertidas en la queja que instauró Álvaro Uribe; ratificó la «*inexistencia de la conducta*» acusada por el expresidente; y se mantuvo en su postura de absolver a Iván Cepeda.** (Le puede interesar: [Caso Álvaro Uribe es esperanza para las víctimas que necesitan la verdad](https://archivo.contagioradio.com/caso-alvaro-uribe-es-esperanza-para-las-victimas-que-necesitan-la-verdad/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1298237166255046656","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1298237166255046656

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Cabe recordar que la Corte Suprema adelanta un proceso en contra de Uribe por estos mismos hechos, el cual, tiene cobijado con medida de aseguramiento domiciliaria al exmandatario. (Le puede interesar: [“Responsabilidad de Álvaro Uribe en el caso de Diego Cadena es innegable” - Iván Cepeda](https://archivo.contagioradio.com/alvaro-uribe-tiene-responsabilidad-innegable-en-caso-diego-cadena/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La decisión de la Procuraduría, reafirmaría el mérito que evidenció la Corte para abrir la investigación contra Uribe por los delitos de fraude procesal y soborno a testigos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

«*Esta es una decisión que se une a otras y de un órgano distinto a la Corte Suprema de Justicia. Me parece que es valioso porque ayuda a mostrar que **hay una multiplicidad de decisiones que se están tomando por distintos órganos en distintos momentos. Todas esas decisiones muestran que no existe ningún fundamento para demostrar que en algún momento yo he hecho ofrecimientos a personas a cambio de que declaren en contra de Álvaro Uribe** y su hermano Santiago Uribe*», señaló el senador Cepeda tras conocer la decisión.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1298328054121533441","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1298328054121533441

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Las amenazas en contra de Iván Cepeda y el apoyo internacional que ha recibido

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por otra parte, **Iván Cepeda denunció hace algunos días, que luego de que se oficializara la medida de aseguramiento en contra de Álvaro Uribe, se intensificaron las intimidaciones y amenazas contra su vida** e incluso alertó que sujetos sospechosos merodeaban las inmediaciones de su residencia y tomaban fotografías, lo que lo hacía temer que se estuviese planeando un atentado en contra suya.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto llevó incluso a que parlamentarios europeos, dirigieran una carta al presidente Iván Duque, en señal de apoyo a Cepeda, en la que manifestaron su preocupación por su situación de seguridad y le pidieron al presidente Iván Duque respetar la separación de poderes y no interferir con sus declaraciones en las decisiones de la Corte Suprema. (Le puede interesar: [«Duque y su Partido buscan presionar a la Corte Suprema en caso de Álvaro Uribe»](https://archivo.contagioradio.com/duque-y-su-partido-buscan-presionar-a-la-corte-suprema-en-caso-de-alvaro-uribe/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
