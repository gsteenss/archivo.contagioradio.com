Title: La Memoria de Trípido y los grafiteros fallecidos a través del arte
Date: 2018-08-27 09:39
Category: eventos
Tags: “Trípido”, Bogotá, gafiti
Slug: memoria-tripido-arte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 27 Ago 2018 

En el marco de la conmemoración de los [7 años del asesinato de Diego Felipe Becerra](https://archivo.contagioradio.com/7-anos-sin-tripido/), grafitero bogotano conocido como Trípido, sus familiares y amigos buscan **evocar su memoria y la de otros grafiteros fallecidos a través del arte**.

Por tal razón como parte del homenaje, **abren la invitación para aquellas personas que deseen vincularse con su diseño artístico** a la actividad que tendrá lugar el **22 y 23 de septiembre** en la calle 116 con Av Boyacá, lugar donde fue asesinado.

![tripido memoria](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/39900050_309778166441141_6351640483365650432_n-480x529.jpg){.size-medium .wp-image-55999 .aligncenter width="480" height="529"}

Todas las personas interesadas t**ienen como plazo para enviar los bocetos el próximo 8 de septiembre**, a través del correo electrónico: conmemoración@tripido.com.co

El evento que se desarrollará entre las 9 de la mañana y las 7 de la noche, servirá como **antesala al 'Trípido Fest'**, espacio cultural y artístico que tendrá lugar el **7 de noviembre** en el escenario al aire libre La media Torta de Bogotá.
