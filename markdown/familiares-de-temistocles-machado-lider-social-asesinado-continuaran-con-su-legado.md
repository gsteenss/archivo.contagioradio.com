Title: Familiares del líder Temístocles Machado continuarán con su legado
Date: 2018-04-26 13:35
Category: DDHH, Nacional
Tags: asesinato de líderes sociales, buenaventura, defensores de derechos humanos, lideres sociales, Temístocles Machado
Slug: familiares-de-temistocles-machado-lider-social-asesinado-continuaran-con-su-legado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/temisto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Elizabeth Otálvaro] 

###### [26 Abr 2018] 

Al cumplirse tres meses del asesinato del líder social Temístocles Machado en Buenaventura, su sobrino Cristian Rivera recordó la **lucha social del líder** e indicó que su legado debe permanecer a través de la continuidad de los procesos que adelantaba su tío. Según el último reporte de la organización Somos Defensores, en lo que va de 2018, han sido asesinados 46 defensores de derechos humanos.

Rivera recordó que la familia de Temístocles Machado ha seguido una tradición por la defensa de los derechos humanos. Afirmó que el padre de “don Temis”, Juan Machado,“**empezó a ser líder desde muy temprana edad** cuando llegó a Buenaventura con sus hijos y Temístocles siguió la línea de Juan siendo un líder comunitario de la comuna seis de Buenaventura”.

### **Temístocles Machado fue un líder comprometido con su comunidad** 

El sobrino de Machado, recalcó los valores del líder asesinado en enero y afirmó que su tío estuvo liderando **procesos relacionados con la tenencia de la tierra** porque “hay intereses sobre las tierras por la expansión del puerto de Buenaventura y otros proyectos”. Por esto, Temístocles emprendió una batalla para que las personas no fueran víctimas de despojos y pudieran continuar arraigados a sus territorios.

En lo familiar, Rivera indicó que el asesinato de Machado ocurrió días después de haber asistidos al sepelio del señor Mario Machado, abuelo de Cristian Rivera, en Bogotá. Afirmó que **“fueron dos golpes muy rápidos para la familia,** especialmente para sus hijos, su esposa y las personas de la comuna seis en Buenaventura”. (Le puede interesar:["La lucha por la que habría sido asesinado Temístocles Machado"](https://archivo.contagioradio.com/temistocles_machado_lider_buenaventura_paro_asesinado/))

### **Como un acto de duelo, Rivera le escribió unas palabras a los asesinos de su tío** 

Luego del asesinato, Rivera escribió un texto dirigido a los sicarios que asesinaron al defensor de derechos humanos donde realizó un acto de duelo. Inidicó que le escribió a los perpetradores teniendo en cuenta que “si lo leen **pueden reflexionar** un poco sobre sus acciones que no sólo afectaron a mi tío sino a muchos líderes en el país”.

Adicionalmente, dijo que él se imagina al asesino de su tío como “una persona vulnerable que desafortunadamente **no tuvo oportunidades** y por una u otra razón se vio metido en bandas criminales o grupos armados”. Por esto, dijo que es posible ver a esa persona como una víctima “pero es el clásico ejemplo en el que la víctima se convierte en victimario”. (Le puede interesar:["Roban información relacionada con la labor del líder Temístocles Machado en Buenaventura"](https://archivo.contagioradio.com/roban-informacion-relacionada-con-labor-de-lider-temistocles-machado-en-buenaventura/))

### **Legado de Temístocles Machado va a continuar** 

A pesar del asesinato del líder social, sus familiares han decidio mantener viva la lucha por los derechos humanos. Rivera afirmó que **“la voz de rebeldía”** de la familia Machado se incrementó con este suceso y recordó que está dispuesto a seguir con el legado que dejó en vida Temístocles Machado.

Finalmente, envió un mensaje para que la lucha por la defensa de los derechos humanos continúe en el país como un **camino de** [**resistencia**]** ante las injusticias**. Sin embargo, invitó a que los defensores y líderes sociales del país hagan un proceso de acompañamiento y protección mutua para evitar que continúen los asesinatos en Colombia.

<iframe id="audio_25644851" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25644851_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
