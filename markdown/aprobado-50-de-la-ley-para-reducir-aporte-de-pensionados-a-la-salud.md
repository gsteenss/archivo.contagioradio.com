Title: Pensionados aportarán sólo 4% a la salud sí Senado aprueba Ley
Date: 2016-04-26 11:43
Category: Economía, Nacional
Tags: aporte salud pensionados, Pensionados, proyecto ley 065
Slug: aprobado-50-de-la-ley-para-reducir-aporte-de-pensionados-a-la-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Pensionados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jujogol] 

###### [26 Abril 2016 ]

La Cámara de Representantes aprobó este lunes el Proyecto de Ley 065 que **reduce el aporte de los pensionados al régimen contributivo de salud del 12 al 4%**, iniciativa que ya se había radicado en septiembre de 2015, pero que de acuerdo con el Representante Alirio Uribe Muñoz, al ser promulgada "mal y de mala fe", fue declarada inconstitucional.

Según Uribe, con la presión de por lo menos 400 pensionados, se logró que en la plenaria de la Cámara todas las bancadas **"desobedecieran la línea" del Gobierno de Juan Manuel Santos y del Ministro de Hacienda** y votaran a favor del Proyecto de Ley, que beneficia al 20% de la población en edad de pensión que cuenta con este derecho.

El futuro que le queda al Proyecto es ser aprobado en la Comisión Séptima del Senado, en la que se espera haya presión para que el **compromiso pactado por el actual mandatario durante su campaña**, se cumpla y se elimine la [[gran injusticia](https://archivo.contagioradio.com/proyecto-de-ley-reduciria-aporte-de-pensionados-a-la-salud/)] con los pensionados.

<iframe src="http://co.ivoox.com/es/player_ej_11315232_2_1.html?data=kpagk5qWd5Ohhpywj5WYaZS1k5aah5yncZOhhpywj5WRaZi3jpWah5yncaLgytfW0ZC5tsrWxpCajbXTsNCfpcra0cjWaaSnhqae1s7Hs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
