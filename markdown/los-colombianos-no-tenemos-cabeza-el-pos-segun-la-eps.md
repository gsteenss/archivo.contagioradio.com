Title: Los colombianos no tenemos cabeza,  El Pos según la EPS
Date: 2015-06-01 07:36
Category: Opinion, Ricardo
Tags: colombia, EPS, sistema de salud
Slug: los-colombianos-no-tenemos-cabeza-el-pos-segun-la-eps
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/eps.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto Contagio Radio 

#### [**Por [Ricardo Ferrer Espinosa](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/)  -   @ferr\_es**] 

**En el sistema de salud pública que se aplica en Colombia, lo que afecte a nuestra cabeza no es prioritario...  o es un gran negocio.**

En el actual POS, la atención a nuestros ojos, oídos, nariz, boca y salud mental, es muy básica. Si el usuario requiere mayor atención, las EPS ponen obstáculos ante quienes no tienen capacidad o gentilmente nos abren la vía hacia la medicina privada.

Se supone que el cuerpo es uno. En la práctica, las EPS excluyen muchos procedimientos con la excusa de que no son prioritarios o urgentes. Como si fuésemos zombies descabezados, las EPS nos remiten a la medicina privada y más costosa, para conseguir anteojos, prótesis dentales y audífonos.

Pacientes con problemas nasales han tenido que interponer tutelas para que les autoricen la intervención. Las EPS argumentan que los procedimientos no son prioritarios, entonces pasan al campo de la cirugía estética, es decir, zona de negocios.

Los órganos situados en nuestra cabeza, además de sentir el mundo, son eje de la interacción social, y por esa necesidad de relacionarnos sin limitaciones, los usuarios han sido condicionados a pagar dinero extra. Parte de nuestra proyección se juega en el rostro. Con sus elementos de estética y funcionalidad buscamos la aceptación, la integración con nuestro entorno, y esa necesidad genera un negocio donde la mayoría de los usuarios tienen poca capacidad de pago, el Estado controla muy poco y las empresas solo buscan su rentabilidad. Basta con mirar los sobreprecios en lentes, monturas y exámenes que realizan las ópticas.

**En grupos prioritarios, ha retrocedido la atención infantil:**

- Salud visual: la cobertura sigue siendo deficiente en los escolares de Colombia. Más allá de la atención básica que cubre un médico por cuerpos extraños y conjuntivitis, el sistema limita extremadamente el acceso a profesionales de oftalmología y optometría.

- Salud auditiva: ¿Cuántos niños presentan dificultades escolares por carecer de un diagnóstico adecuado sobre sus órganos y sentidos? El actual sistema de educación no tiene suficiente complemento con el sistema de salud. Existe una carrera de obstáculos para la provisión de audífonos y mientras tanto, los niños se desnivelan en sus estudios.

- Salud oral: con frecuencia se considera que las prótesis y ortodoncia son una elección estética. Lo cierto es que suben la calidad de vida: ayudan a nutrirse bien, mejoran la autoestima, la pronunciación y la comunicación con el entorno. Sin dientes (o prótesis dental) el aspirante difícilmente conseguirá empleo. En la sonrisa  vemos si un niño es cuidado, si está bien calcificado, si se le hacen las ortodoncias necesarias. Al paso que vamos, con la enorme desigualdad del país, seguiremos viendo a nuestra gente desdentada.

- Salud mental: infantes y jóvenes de Colombia habitan un país turbulento y desigual. Entre la población que tiene la suerte de contar con seguro de salud, los tratamientos son reactivos (1).

Los defensores de la Salud Pública siempre nos enseñaron que el dinero que se invierta en la salud de la infancia, muy lejos de ser un gasto, evita costos mayores en la vida del adulto. El ejemplo clásico que escuchamos a Héctor Abad Gómez era el de la nutrición infantil: un niño o adolescente que crezca en carencias sufrirá las consecuencias en toda su vida de adulto.

La salud, un derecho fundamental, está hoy en manos de especuladores. La sigla de las EPS cobra sentido si se rescribe así: Evaden Prestar el Servicio.

###### 1 . <http://www.corteconstitucional.gov.co/relatoria/2011/T-094-11.htm>...“hay eventos en los que es necesario que el juez ordene a la EPS que preste un determinado tratamiento que resulta de vital importancia para el paciente y que no está incluido dentro del Plan Obligatorio de Salud, tal y como lo estableció la jurisprudencia anteriormente citada, que resulta plenamente aplicable a los casos bajo estudio”. 
