Title: En operación militar asesinan a Joel Villamizar líder indigena Uwa
Date: 2020-06-01 18:50
Author: CtgAdm
Category: Actualidad, DDHH
Slug: en-medio-de-operacion-militar-fue-asesinado-el-indigena-joel-villamizar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/EZblFLNWsAEo9kG.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/EZblFLNWsAEo9kG-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 31 de mayo la **Asociación de Autoridades Tradicionales y Cabildos U’was**, (ASOUWA), denunció el **asesinato del dirigente indígena Joel Villamizar en la vereda Rio colorado**, del Municipio de Chitagá, cerca al Resguardo Indígena Unido, en el departamento de Norte de Santander.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ANZORC_OFICIAL/status/1267452234075451394","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ANZORC\_OFICIAL/status/1267452234075451394

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El hecho se presentó sobre las 8:30 am, cuando **integrantes de la Trigésima Brigada, pertenecientes a la Segunda División del Ejército, ingresaron al territorio para ejecutar la captura de alias** ***Marcial,*** líder de la comisión Martha Cecilia Pabón del ELN.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El operativo, según voceros de Asouwa, se desarrolló en zona rural *"poniendo en riesgo la vida de población civil"*, y tuvo como resultado el asesinato de alias *Marcial*, y la captura de 4 hombres que lo acompañaban.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, la Asociación señaló que se ha difundido información errónea que indica que en el operativo también fue *"dado de baja **otro hombre, que podría ser escolta del cabecilla del ELN"***, acusación que fue desmentida por la organización al indicar que se trataba de **Joel Villamizar**.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**No vamos a permitir que esta situación lamentable sea considerada como un falso positivo del Estado colombiano**, ya que el hermano U’wa asesinado nunca estuvo vinculado al grupo insurgente ELN".*
>
> <cite>**Asociación de Autoridades Tradicionales y Cabildos U’was**| (ASOUWA)</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Según directivos de la Asociación, **Villamizar se desempeñaba como directivo de la Asociación U’wa** y recientemente como Coordinador de Educación de la comunidad indígena.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"El jueves 25 de mayo él estuvo trabajando en la oficina, pero **pidió permiso para celebrar su cumpleaños el viernes con su familia en la finca de Chitagá**, sin saber que allí iba a estar Maricial"*, señalaron los directivos U´wa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y afirmaron que el día del operativo, Joel salió a verificar que ocurría y fue herido en dos oportunidades con arma de fuego, *"su esposa y cuñada denunciaron la agresión, **pero pese a las suplicas, el helicóptero del Ejército llegó dos horas después, cuando ya Joel había fallecido**"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posterior a esto, los uniformados se llevaron a los 4 hombres detenidos, el cuerpo del cabecilla y el de Joel Villamizar hacia Cucuta, *"pedimos que así como el Ejercito se lo llevó y lo mató, nos regresen el cuerpo del compañer*o *para hacer su despedida"*, indicó Asouwa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente la Asociación hizo un llamado a organizaciones de Derechos Humanos del Gobierno Nacional, a defensores de Derechos Humanos nacionales e internacionales, **para que se realicen de manera urgente y efectiva acciones que eviten las agresiones contra el pueblo indígena**. (Le puede interesar: [Ejército quería que Ariolfo Sánchez fuera un "falso positivo"](https://archivo.contagioradio.com/ejercito-queria-que-ariolfo-sanchez-fuera-un-falso-positivo-comunidad-de-anori-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

He indicaron que estos hechos son materia de investigación y que habrá un fuerte pronunciamiento ante los entes estatales, *"**por esta muerte de nuestro hermano U’wa; el Estado colombiano y el ejército de Colombia deben responder**"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte la Asociación campesina de Catatumbo (**Asacamcat**), señaló que la responsabilidad de este asesinato recae sobre el comandante de la Segunda División del Ejército, el *Brigadier General Marcos Evangelista Pintos* quien comanda todas las operaciones militares que se desarrollan en la zona.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pintos se encarga de coordinar las tropas presentes en Santander, Norte de Santander, Boyacá, Antioquia, Bolívar y Cesar, **lugares en donde en los últimos días se registraron los asesinatos de [Alejandro Carvajal](https://archivo.contagioradio.com/alejando-carvajal-joven-de-20-anos-asesinado-por-el-ejercito-en-catatumbo-ascamcat/) y [Digno Buendía](https://archivo.contagioradio.com/ejercito-habria-asesinado-a-emerito-digno-buendia-ascamcat/), agresiones vinculadas con hombres del Ejército.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
