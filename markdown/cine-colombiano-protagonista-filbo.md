Title: 3 espacios para el cine colombiano y la literatura en la FILBO
Date: 2019-04-26 15:58
Author: CtgAdm
Category: 24 Cuadros
Tags: Cine Colombiano, filbo
Slug: cine-colombiano-protagonista-filbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Diseño-sin-título-3.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Cine Colombiano 

Con una programación dedicada completamente al cine colombiano se desarrollara **\#FilBoCine,** un espacio dentro de la Feria Internacional del libro de Bogotá,que a partir de 3 programas busca visibilizar los valores y cualidades de las películas hechas en Colombia: **Mujeres de Macondo, Cine y Literatura y Libros de película**.

Entre el **26 de abril y el 6 de mayo**, se proyectarán **27 películas colombianas,** acompañadas por **15 conversatorios** con los realizadores y por primera vez un espacio de negocios entre el sector cinematográfico y editorial; todo esto gracias a la alianza pactada entre la **Academia Colombiana de Artes y Ciencias Cinematográficas y la Cámara Colombiana del Libro**.

##### 3 Espacios para leer el cine 

Por una parte esta **Mujeres de Macondo**, programa diseñado por la Academia con el apoyo del Fondo para el Desarrollo Cinematográfico, en el que se proyectarán películas colombianas acompañadas por conversatorios, dónde las principales protagonistas son las mujeres que hacen cine, en cada uno de los departamentos de las producciones que ellas lideran. Algunas películas de este programa son **Pájaros de Verano** (Cristina Gallego, codirectora), **La cara oculta** (Olga Turrini, Maquilladora), **La tierra y la sombra** (Marcela Gómez, Directora de Arte), **Los Hongos** (Sofía Oggioni, Directora de Fotografía), **X500** (Isabel Torres, Sonidista), **Los nadie** (Isabel Otalvaro, Montaje), **Virus Tropical** (Adriana García, Música) y **Siempreviva** (Ana Piñeres, Productora).

Otro de los espacios es **Cine y Literatura**, un ciclo de charlas y conversatorios donde se explorará la relación entre ambas manifestaciones culturales, con la participación de escritores y cineastas colombianos entorno a una obra en común. Entre los invitados estarán Rosa Ramos, productora, y Carol Ann Figueroa, guionista de **Paciente**; Humberto Dorado actor de **Ilona llega con la lluvia**; Vicky Hernández actriz **La mansión de Araucaima**; **Angélica Blandón** actriz de Paraíso Travel y **Lisandro Duque** director de Milagro en Roma. Como novedad en esta sección, se tendrá el conversatorio Adaptar o morir, **“Pautas para adaptar un libro en cine y televisión”** liderado por la actriz Diana Wiswell y Jerónimo Rivera.

Por último, **Libros de película,** un programa desarrollado con el objetivo de promover nuevas ventanas de encuentro para el sector audiovisual y editorial. Esta iniciativa, inédita en la Feria, busca **identificar libros de autores colombianos para ser adaptados a formatos audiovisuales**. En una primera etapa de la convocatoria, la Cámara Colombiana del Libro invitó a los editores nacionales a postular sus obras, recibiendo 49 títulos que cuentan con los derechos de adaptación. El 29 de abril se anunciarán las obras seleccionadas por el comité para ser sometidas a votación del público y el 5 de mayo  se anunciarán las obras ganadoras y el 6 de mayo, finalizará el proceso en el encuentro profesional entre los editores literarios ganadores y los productores audiovisuales miembros de la Academia.

Esta iniciativa hace parte de la celebración de los 10 años de la Academia, entidad que tiene como finalidad acercar al público a las historias hechas en el país y sus realizadores, reduciendo la brecha entre las audiencias y las producciones nacionales. "Durante estos 10 años hemos logrado consolidarnos como una de las entidades referentes del sector cinematográfico, liderando procesos de formación, promoción e investigación; muestra de ello, es este nuevo espacio en la Feria Internacional del Libro de Bogotá con el que esperamos llegar a nuevas audiencias”, aseguró Consuelo Luzardo, su presidenta.

###### <iframe id="doc_39320" class="scribd_iframe_embed" title="Programación #FILBOCINE 2019" src="https://es.scribd.com/embeds/407737258/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-KdIurQ3uthtxOhqc2s7Z&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7066666666666667"></iframe> 

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
