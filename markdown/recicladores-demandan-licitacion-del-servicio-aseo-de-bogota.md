Title: Recicladores demandan licitación del servicio aseo de Bogotá
Date: 2017-01-04 16:20
Category: Nacional, yoreporto
Tags: Bogotá, Peñalosa, recicladores, reciclaje
Slug: recicladores-demandan-licitacion-del-servicio-aseo-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/nuestro-trabajo-es-un-servicio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:espanol.wiego 

###### 3 Ene 2017 

#### [**Por  Camilo A. Leal\* - Yo Reporto**] 

[**Recicladores del Distrito Capital interpusieron ante el Tribunal Superior de Bogotá** acción de tutela para proteger los derechos al mínimo vital, el trabajo y la igualdad, solicitando suspender el proceso licitatorio de la recolección de residuos no aprovechables, barrido y limpieza RBL, porque dicha licitación no incluye acciones afirmativas ni obligaciones contractuales de los futuros operadores aseo para garantizarles a los recicladores el acceso cierto y seguro a los residuos aprovechables.  ]

### Los rechazos de los recicladores 

Líderes y lideresas del gremio reciclador como Luis Alberto Romero, Gustavo Martínez y Paula Rengifo, entre muchos otros, señalan que **esta licitación vulnera sus derechos porque perpetua la situación de desigualdad entre recicladores y prestadores del RBL**, debido a que le entrega Áreas de Servicio Exclusivo a empresarios del aseo, en el que las empresas de RBL se aseguran monopolios sobre sectores de la ciudad, única manera de asegurar la rentabilidad, 100% de cobertura a los usuarios y el cierre financiero del negocio de aseo, mientras que los recicladores siguen en libre competencia, compitiendo en desigualdad de condiciones, ferozmente en la calle, cuadra a cuadra, entre ellos, y con bodegueros y empresarios privados que se metieron al negocio del reciclaje, lo que ha generado la situación de vulnerabilidad y explotación de la población recicladora y no garantiza la recolección del reciclaje en toda la ciudad.

Por otro lado **denuncian que ellos compiten en precarias condiciones contra los operadores de RBL por acceder a las bolsas de basura** donde se encuentran los materiales aprovechables, “los recicladores para ganarnos nuestro mínimo vital competimos contra el carro compactador, tenemos que buscar en las bolsas de basura rápidamente antes de que los operarios recojan las bolsas y las echen al carro, y así no la pasamos todo el día, todos los días”. Esta situación ha permanecido a pesar de las múltiples ordenes que ha emitido la Corte Constitucional desde la Sentencia T-724 de 2003 para que se construya un esquema de aseo que incluya a los recicladores de oficio a través de sus organizaciones en el servicio de aseo de la ciudad.

Por esta razón los recicladores rechazan enérgicamente la implementación de contenedores en la ciudad, hasta tanto no se articule un esquema de aseo que ponga a las organizaciones de recicladores en igualdad de condiciones técnicas, operativas, administrativas y jurídicas frente a los operadores de RBL. Los recicladores señalan que “la contenerización de la ciudad no mejora la gestión de los residuos, todo lo contrario, si no se articula con los recicladores, y no hay unos mínimos de cultura de separación en la fuente, la cantidad de basura que va a llegar al Relleno Sanitario Doña Juana va a ser mucho mayor porque los operadores de RBL no permiten el acceso de los recicladores a dichos contenedores, y los usuarios depositan toda la basura en ellos… increíblemente la  actual administración prefiere más basura en el relleno que más residuos aprovechados, con tal de ver “limpia” la ciudad de basura y de recicladores”

**Los recicladores también le piden al Tribunal Superior de Bogotá que le ordene al Distrito cumplir con el Plan de Inclusión de la Población Recicladora** que dejó la administración pasada y aprobó la corte, porque necesitan las acciones afirmativas en materia de bodegas, infraestructura, equipamiento, maquinaría y asistencia técnica profesional permanente para formalizarse, si es que la Administración Peñalosa quiere que las organizaciones de recicladores sean empresas formales que cumplan con el Decreto Nacional 596 de 2016. Señalan que si bien han recibido programas sociales y desde el 2013 vienen recibiendo un pago de tarifa por los materiales que recuperan, la recolección de los residuos aprovechables se sigue haciendo en las mismas condiciones indignas -e ineficientes- que motivó a la corte a ordenar su inclusión en el servicio de aseo y la suspensión de la licitación en el año 2011, con el agravante que la Directora de la UAESP Beatriz Cárdenas, sustituyó el PGIRS dejado por la administración Petro, recortando los programas, proyectos y el presupuesto que se habían decretado en el PGIRS de 2015 para la entrada en funcionamiento de Zonas Ecológicas para la operación de las organizaciones y la entrega de bodegas, equipamiento, maquinaría e infraestructura para los recicladores.

Los recicladores esperan que esta acción judicial y otras que están contemplando interponer lleguén hasta la Corte Constitucional para que dicho organismo se exprese sobre el incumplimiento del Distrito Capital en su inclusión.

Pta: En la feria de servicios organizada por la UAESP el 14 de Diciembre en Kennedy, se evidenciaron los sectores que lideran el gremio reciclador, uno liderado por Nohra Padilla y la ARB que protagonizó un incidente en el que la histórica dirigente golpeó a una recicladora de oficio de otra organización. Otro sector tiene un liderazgo colectivo joven en el que figuran recicladores como Luis Alberto Romero de la Asociación EMRS, Paula Rengifo de la organización GER-8 y Gustavo Martínez de ARAUK, entre otros, que vienen liderando una mesa de recicladores de base y afirman que Norha Padilla no los representa.

###### \* Historiador de la Universidad Nacional de Colombia, [Candidato a Magíster en Desarrollo Sustentable y Gestión Ambiental de la Universidad Distrital FJC - Investigador en gestión y aprovechamiento de residuos] 
