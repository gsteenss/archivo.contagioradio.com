Title: 32 líderes sociales han sido asesinados durante 2017 en Cauca
Date: 2017-12-20 11:21
Category: DDHH, Nacional
Tags: amenazas a líderes sociales, asesinatos de líderes sociales, Cauca, defensores de derechos humanos, lideres sociales, Patía
Slug: aumenta-preocupacion-por-asesinato-de-lideres-sociales-en-el-cauca
Status: published

###### [Foto: El Universal.com] 

###### [20 Dic 2017] 

Tras el asesinato de **Alfonso Pérez Mellizo**, presidente de la Junta de Acción Comunal del Corregimiento de Pan de Azúcar, se agudiza la situación de los líderes y defensores de derechos humanos en el departamento del Cauca. En el 2017, han sido asesinados 32 líderes solamente en ese departamento. Además solamente se ha conocido de la detención de los autores materiales de 3 de los casos, es decir 1.2%.

De acuerdo con Deivin Hurtado, integrante de la Red de Derechos Humanos Francisco Isaías Cifuentes, Pérez Mellizo fue visto con vida por última vez el lunes 18 de diciembre y su cuerpo fue encontrado a 400 metros del casco urbano del municipio de Patía en el Cauca y venía desarrollando **diferentes actividades de defensa de los derechos de las comunidades**.

### **Defensores habían alertado sobre panfletos amenazantes** 

Hurtado indicó que los líderes sociales del Patía **habían denunciado la existencia de dos panfletos amenazantes** firmados por el grupo armado ilegal Águilas Negras hace 5 meses. Además, el día del asesinato de Pérez, “hubo otras acciones intimidatorias contra uno de los concejales de Popayán que hace parte de Marcha Patriótica”, haciendo de las amenazas una acción sistemática. (Le puede interesar: ["Masacre en Suárez, Cauca deja seis personas muertas"](https://archivo.contagioradio.com/masacre-en-suarez-cauca-deja-seis-personas-muertas/))

Ante estos hechos y por el asesinato de Pérez **no ha habido acciones por parte de las autoridades**. Sin embargo, los defensores de derechos humanos han descartado que el móvil del asesinato, responda “a situaciones personales, fue un asesinato que responde al ejercicio de la acción política y social que venía realizando el presidente de la Junta de Acción Comunal”.

### **En Cauca ha aumentado la zozobra por presencia de grupos armados** 

Las comunidades han informado que los grupos armados han venido desarrollando acciones **por el control del territorio** “que son complejas y que generan intranquilidad en las comunidades”. Han dicho que, en primer lugar, “los asesinatos de los líderes no tienen un reconocimiento departamental ni nacional”. (Le puede interesar:["Se agrava la situación en el Cauca por asesinato de étno educador"](https://archivo.contagioradio.com/se-agrava-situacion-en-el-cauca-por-asesinato-de-etno-educador/))

En segundo lugar, “**no se ve la fuerza institucional** para tomar los sitios que fueron dejados por las FARC y esto genera violencia por parte de grupos armados que han intentado ingresar a los territorios y que sabemos que son paramilitares”. Por esto, para ellos no es suficiente con que se aumente el pie de fuerza militar pues no genera condiciones de seguridad.

### **Fiscalía General de la Nación se queda en investigar a los autores materiales** 

De los 32 asesinatos de líderes sociales que se han registrado en el Cauca, sólo se ha producido **la captura de los sicarios en 3 casos**. Esto preocupa a los defensores de derechos humanos en la medida en que “sólo se está llegando a la judicialización de los autores materiales y hay que buscar los autores intelectuales que están detrás de los asesinatos”. (Le puede interesar: ["Por ataque del ESMAD muere comunicadora indígena en Kokonuco, Cauca"](https://archivo.contagioradio.com/por-ataque-del-esmad-muere-comunicadora-indigena-en-kokonuko-cauca/))

Hurtado informó que, la Fiscalía les ha dicho que las investigaciones sólo pueden llegar hasta la captura de los autores materiales dejando por fuera a las personas que verdaderamente están poniendo en riesgo a los líderes sociales. Dijo que “los autores intelectuales **les ofrecen dinero a los sicarios** por asesinar o atentar contra las personas”.

Finalmente, las comunidades del Cauca han hecho énfasis en que los actores armados **se han opuesto a que haya programas de sustitución voluntaria** de cultivos ilícitos y “el Gobierno Nacional no ayuda a cumplir lo pactado en los acuerdos de paz”. Esto ha hecho de ese departamento el lugar donde más asesinatos de líderes sociales han ocurrido.

<iframe id="audio_22763273" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22763273_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
