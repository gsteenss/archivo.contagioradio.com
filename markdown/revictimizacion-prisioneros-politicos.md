Title: El flagelo de la revictimización de prisioneras y prisioneros políticos
Date: 2018-08-21 15:48
Category: Expreso Libertad
Tags: Criminalización, Estigmatización, presos politicos
Slug: revictimizacion-prisioneros-politicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/criminalizar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Arquimedios 

###### 21 Ago 2018 

Las diferentes problemáticas que afrontan las cárceles en Colombia, han generado múltiples afectaciones a la población reclusa del país. Sin embargo, las prisioneras y prisioneros políticos denuncian que frente a ellos **hay una serie de situaciones que evidencian una revictimización por su condición de pertenecer a un grupo insurgente o tener un pensamiento crítico**.

**Hernán Tangarife,** asegura que este tipo de situaciones han provocado incluso la muerte de las y los prisioneros políticos, o **los han sometido a tratos inhumanos que se convierten en tortura**. En este Expreso Libertad, escuche las revictimizaciones que viven las y los prisioneros políticos y **cómo han asumido diferentes luchas para afrontarlas, desde la defensa de los derechos humanos**.

<iframe id="audio_30713132" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30713132_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
