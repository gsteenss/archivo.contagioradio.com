Title: Más de 200 integrantes de Marcha Patriótica han sido asesinados entre 2011 y 2020
Date: 2020-01-13 17:40
Author: CtgAdm
Category: Entrevistas, Líderes sociales
Tags: Asesinatos de líderes Marcha Patriótica, marcha patriotica
Slug: asesinatos-marcha-patriotica-2011-2020
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Marcha-patriótica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Marcha Patriótica

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante la violencia sociopolítica de la que ha sido víctima históricamente Marcha Patriótica, sus integrantes han solicitado al presidente Iván Duque, una reunión para buscar garantías efectivas y eficaces para hacer frente a los 222 asesinatos de sus integrantes entre 2011 y 2020, de los que 43 han asesinados desde la llegada de Iván Duque a la Presidencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La agudización de la violencia en los territorios contra miembros de Marcha Patriótica, en lo corrido de enero, se ve reflejada con los homicidios de Nelson Enrique Meneses, Amparo Mejía, Juan Pablo Dicué y John Fredy Álvarez. Crímenes ocurridos en tan solo 48 horas entre el 11 y 12 del presente mes.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Marcha Patriótica identifica patrones

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según Cristian Delgado, vocero de [Marcha Patriótica](http://Amenazan%20a%20lideresa%20de%20Marcha%20Patriótica%20y%20exceso%20de%20autoridad%20bajo%20lucha%20antinarcóticos), se ha podido identificar que la mayoría de dichos homicidios han ocurrido en inmediaciones de los hogares de las víctimas, quienes fueron atacadas con armas de largo alcance por hombres que dispararon contra las víctimas entre ocho y quince oportunidades. [(Le puede interesar: Continúan las amenazas a líderes de Marcha Patriótica)](https://archivo.contagioradio.com/continuan-las-amenazas-a-lideres-de-marcha-patriotica/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Delgado indica que en su mayoría son líderes sociales y políticos que participaron en procesos de movilización como el Paro Nacional o la Minga del suroccidente, para los casos de los líderes asesinados en Caloto y Tibú. Además resalta que la mayoría de los hechos victimizantes se dieron en zonas con fuerte presencia militar.  
  
Uno de los patrones a resaltar para Delgado, es que en tres de los crímenes, los hechos ocurrieron en presencia de las familias de la víctima, a los que usualmente sus atacantes se acercan en motocicleta*,* lo que evidencia que se realizan seguimientos para abordar a las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Atentados, amenazas y un total de 52 homicidios continúan presentándose incluso después que el movimiento solicitará ante la Comisión Interamericana de Derechos Humanos medidas cautelares que les fueron concedidas en mayo de 2018.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Defensa de la vida debe ser transversal en las movilizaciones

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Ad portas de la reanudación del Paro Nacional el 21 de enero y en medio de un aumento de la violencia en las regiones, el integrante de Marcha Patriótica afirma que lo crucial es que los derechos humanos sean un eje transversal del paro, "si bien la consigna del paro es en defensa de la vida, hay que señalar que en la agenda inicial de 13 puntos lo único referido DD.HH. era muy general y solo relacionado al desmonte del ESMAD, por lo que fue necesario fortalecerlo".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Junto a Cumbre Agraria y otros plataformas de DD.HH, Marcha Patriótica señala que se requiere una presencia integral del Estado y no solo enfocada en el pie de fuerza, "es necesaria una presencia que transforme los territorios y garantice la vida", asegura el vocero de Marcha Patriótica. Asimismo agrega que decidieron apelar al presidente debido a que **"el diálogo con las entidades no ha dado frutos, pues se escudan en que son funcionarios".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Insistiremos en el decreto 660 de protección colectiva para las comunidades, en profundizar en el sistema de alertas de la Defensoría y en fortalecer la Comisión Nacional de Garantías" explica Delgado. De igual forma, asevera que deben adelantar un programa especial de protección para Marcha Patriótica en los territorios, y aunque la propuesta fue enviada hace dos años, en los estudios técnicos de la Unidad Nacional de Protección, el proyecto no ha avanzado. [(Lea también: "No les vamos a dar el gusto de desfallecer" Marcha Patriótica)](https://archivo.contagioradio.com/cuanto-mas-podra-resistir-marcha-patriotica/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
