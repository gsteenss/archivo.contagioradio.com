Title: Marabunta en Sonidos Urbanos
Date: 2016-08-24 16:28
Category: Sonidos Urbanos
Tags: Cultura, Música
Slug: marabunta-en-sonidos-urbanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/marabunta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Marabunta 

Con la fuerza del ciclón de las hormigas,  las mezclas de las raíces de Latinoamérica y una alegría que se imprime en cada nota musical llega **Marabunta a Sonidos Urbanos**, una agrupación compuesta por 4 colombianos de diferentes partes del país que juntan en su música e historias con sabor a tierra y rebeldía.

Conversamos con ellos sobre sus inicios en la música, sus aspiraciones como banda y las historia y anécdotas que plasman en sus composiciones. A su vez nos compartieron la importancia de retratar en sus letras el compromiso que tienen con la transformación y construcción de un mundo para todos y todas.

Con canciones como “La Candelita” y “Turin” o “Bosque” que hacen parte de su primer álbum, transcurrió este programa lleno de  energía y unidad.

<iframe src="http://co.ivoox.com/es/player_ej_12651879_2_1.html?data=kpejl5ace5qhhpywj5WVaZS1k5mSlaaZdo6ZmKialJKJe6ShkZKSmaiRdI6ZmKiar8bWpcPpz9nOjcrSb7Tjz87R0diPmdPWwtPc1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
