Title: Ecosistemas en Colombia son víctimas de la intervención humana
Date: 2016-11-01 10:32
Category: Ambiente, Otra Mirada
Tags: Ambiente, colombia, Día mundial de la ecología, Ecosistemas
Slug: dia-de-la-ecologia-ecosistemas-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/dia-de-la-ecologia-1-de-noviembre.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

##### 1 Nov 2016 

A propósito del **Día Mundial de la Ecología**, el equilibrio del planeta se ha visto irreversiblemente alterado por el accionar del ser humano. La desertificación, la erosión de los suelos, la deforestación, la crisis de los recursos hídricos, el calentamiento global y las constantes amenazas a la biodiversidad, agudizan profundamente esta problemática a la que los ecosistemas en Colombia no es ajena.

### Ecosistemas en Colombia 

El modelo económico ha tenido fuertes repercusiones sobre los ecosistemas del pais. **Alrededor del 48% del territorio nacional presenta procesos de desertificación**, lo que quiere decir que, cerca de **49 millones de hectáreas de la superficie del país han sufrido procesos de degradación** convirtiéndose en zonas áridas y semiáridas, con impactos negativos sobre la producción biológica de los ecosistemas y el ciclo hidrológico.

De acuerdo con cifras del Instituto Agustín Codazzi en 2015, **de las 114'174.800 hectáreas del territorio nacional, sólo el 16% de las áreas tenían protección legal**. Entre los departamentos con niveles de desertificación catalogadas como muy alta se encuentran: La Guajira, Cesar y Santander, seguido por Boyacá, Atlántico, Córdoba y Sucre con un nivel alto.

En relación con los  bosques Seco Tropicales, el Instituto Alexander Von Humboldt señala que de los **9 millones de hectáreas con este tipo de ecosistema, en la actualidad subsiste únicamente el 8%.** La intervención agrícola, ganadera, el desarrollo turístico y la minería han tenido incidencia directa en la destrucción de estos bosques, los cuales albergan una gran riqueza en términos de biodiversidad y logran adaptarse a condiciones de sequía. Le puede interesar: [Arte para enfrentar el cambio climático](https://archivo.contagioradio.com/arte-para-enfrentar-el-cambio-climatico/).

### Especies en peligro 

De las **56.343 especies** registradas por el Sistema de Información sobre Biodiversidad en Colombia, **un total de 798 especies de plantas se encuentran amenazadas, 313 animales vertebrados y 74 invertebrados**, de los cuales 264, 50 y 6 respectivamente, se encuentran en peligro crítico. La construcción de megaproyectos y la minería son unas de las principales causantes de la extinción de la fauna y flora silvestre, además de la comercialización y prácticas como la caza indiscriminada de animales.

### El agua en Colombia 

**El 46% de los recursos hídricos son utilizados para el sector agrícola**. De acuerdo con el profesor de la Universidad Nacional, Gabriel Gillot, la “**creencia de que Colombia posee una gran riqueza hídrica está siendo perjudicial para la conservación**”. La contaminación hídrica a razón de los residuos domésticos, residuos industriales, actividades agropecuarias y mineras incide profundamente en el deterioro de los ecosistemas hídricos.

Para el caso de los **ecosistemas de páramo en Colombia**, es fundamental sean aplicadas las medidas acordadas por las Naciones Unidas en el marco de la Cumbre del Clima en las que se establecen la **reducción y eliminación de las prácticas que provocan el cambio climático**, como es el caso de la **deforestación y la reducción de la emisión de CO2**, además de limitar las actividades agropecuarias y comerciales en gran escala que tienen altos impactos negativos en la alta montaña según lo ha señalado la organización mundial Greenpeace.

Según el informe 'Páramos en Peligro', sobre el caso de la Minería de Carbón en Pisba, presentado en diciembre de 2013 por la Organización Greenpeace, **el 99% de los páramos en el mundo se encuentran en la cordillera de los Andes y en la Sierra Nevada de Santa Marta** y Costa rica, Colombia tiene alrededor del 49% de estos ecosistemas en el mundo y, **el 70% de la población colombiana se abastece de agua gracias a los mismos**.

### Defensores del ambiente 

Finalmente en conmemoración del día internacional de la ecología, es oportuno también señalar la crítica situación que viven los defensores del medio ambiente. En 2015, de acuerdo al informe presentado por la ONG Global Witness, de los **185 ambientalistas asesinados en el mundo por defender sus tierras, bosques y ríos, el 66% eran de Latinoamérica**. Es el caso de Brasil con 50 muertes; Filipinas 38; **Colombia 26**, y 12 Perú y Nicaragua respectivamente.
