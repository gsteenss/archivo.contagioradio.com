Title: Campesinos defienden su derecho a la consulta en Cajamarca, Tolima
Date: 2015-02-17 20:01
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Anglogold Ashanti, Cajamarca, consulta popular, Mineria, Tolima
Slug: campesinos-defienden-su-derecho-a-la-consulta-en-cajamarca-tolima
Status: published

##### Foto: Las 2 Orillas 

<iframe src="http://www.ivoox.com/player_ek_4096655_2_1.html?data=lZWmmJuZeY6ZmKiakpqJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRksrZyMbbjcjTstTpzdnOjdXTtNbgwteYw5DMpcPd1cbb1srXb8XZjKjOzMbRpdPXwoqfpZC4s83djoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Renzo García, ambientalista] 

**El Concejo de Cajamarca, Tolima, negó el derecho a la consulta popular,** para que los habitantes de ese municipio dijeran "si" o "no" al proyecto minero que pretende realizar **Anglogold Ashanti**, con el objetivo de extraer alrededor de 29 millones de onzas de oro.

Los ambientalistas y el único cabildante que votó a favor de la consulta minera, se defienden afirmando que **la consulta popular en Cajamarca se había convertido en un compromiso luego de la audiencia pública** que lideró el senador Iván Cepeda, en octubre del 2014.

Además, los habitantes del municipio aseguran que al no poder usar este mecanismo constitucional, se vulneran los derechos de los pobladores de Cajamarca, lo que califican como **una violación a la democracia.**

La decisión la tomaron 10 de los 11 concejales, luego de la realización de varias audiencias públicas, donde habitantes y ambientalistas, y por otro lado, quienes estaban en contra de la consulta, expresaran sus argumentos respecto a la idea de convocar este mecanismo, por medio de cual se conocería la aprobación de las personas, sobre la posibilidad de que la empresa minera ingrese a su territorio a extraer oro.

Los afectados señalaron que empezarán a estudiar la posibilidad de **instaurar tutelas, acciones populares, demandas al Estado y no descartan la búsqueda de ayuda internacional. **
