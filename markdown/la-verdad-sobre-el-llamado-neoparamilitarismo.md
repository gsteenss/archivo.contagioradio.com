Title: La verdad sobre el llamado ‘neoparamilitarismo’
Date: 2016-04-11 09:01
Author: AdminContagio
Category: Cindy, Opinion
Tags: Fiscalía, Neoparamilitarismo, Paramilitarismo, proceso de paz
Slug: la-verdad-sobre-el-llamado-neoparamilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/paramilitarismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: N24 

#### **[Cindy Espitia Murcia](https://archivo.contagioradio.com/cindy-espitia/) - [@CindyEspitiaM](https://twitter.com/CindyEspitiaM)**

###### 11 Abr 2016

El reciente paro armado organizado por el Clan Úsuga, que paralizó a 36 municipios del país, puso una vez más en el foco de la opinión pública el fenómeno del paramilitarismo en Colombia.

[Los medios de comunicación y fuentes oficiales han descrito la situación como un ‘resurgimiento’ de estos grupos ilegales; forma imprecisa de abordar la problemática pues, como lo han alertado constantemente ONG’s nacionales y organizaciones internacionales como HRW, “]**los paramilitares nunca dejaron de existir, simplemente se transformaron”.**

[Ahora bien,] [un asunto muy diferente es que se haya optado desde el Gobierno Nacional por dar la espalda, de manera rotunda, a esta realidad. En el año 2007, el entonces Presidente Álvaro Uribe rechazó y descalificó un informe de HRW que ponía en evidencia la continuidad del paramilitarismo, supuestamente, por su carácter sesgado y vacío de fundamento. Si bien Santos no ha negado la permanencia de estos grupos ilegales, es notable su pasividad y la falta de una estrategia clara de contención y desmonte de estas estructuras criminales.]

En todo caso, más allá de mirar quién tiene la culpa, es claro que desde la formulación de la desmovilización hasta, al menos, la semana pasada, no existía una política integral que pudiera abordar la problemática en toda su complejidad. Esto, por supuesto, trajo consigo un fortalecimiento operativo y económico que ha permitido su presencia en al menos 8 departamentos del país y que ha generado que hoy en día ellos busquen ser reconocidos como actores del conflicto armado.

Esta situación resulta especialmente grave; no sólo por la alteración al orden público y la seguridad de los colombianos, sino también por el riesgo que implica para la estabilidad de la paz, ante un eventual escenario de posconflicto con las Farc y el ELN, ya que si el Gobierno no ofrece verdaderas garantías de protección a los guerrilleros desmovilizados, desafortunadamente, Colombia revivirá o los horrores de lo ocurrido con la UP o el rearme de sus miembros.

Por lo tanto, se requiere desde ya una respuesta integral por parte del Estado para, además de atacar esta problemática, enviar un claro mensaje a las Farc y al ELN sobre las consecuencias de la reincidencia.

Así, por un lado, debe realizarse un esfuerzo desde la Fiscalía General por desentrañar estos grupos criminales pero, especialmente, determinar las estructuras de poder que permiten su consolidación. Pues, no nos digamos mentiras, ha sido documentado ampliamente que la formación y mantenimiento de un grupo de esta proporción requiere de un apoyo tanto del sector público como privado.

La investigación y posterior sanción de las estructuras que están detrás de estos grupos dotarán de legitimidad y fortaleza a un sistema que actualmente tiene ‘rabo de paja’ y carece de la autoridad moral para hacer frente a sus manifestaciones.

Por otro lado, para mitigar los ataques contra líderes sociales y defensores de derechos humanos, se requiere de la consolidación de una comisión interinstitucional, compuesta por el Ministerio de Defensa, el Ministerio de Justicia,  la Unidad de Protección, las Fuerzas Armadas, el Ministerio del Interior y representante de tales gremios, que propenda, por la formulación de propuestas e iniciativas que garanticen la seguridad de esta población y el libre desarrollo de sus actividades.

Este modelo, junto con los acuerdos a los que se llegó con las Farc en materia de seguridad y a los que se lleguen con el ELN, deberá ser replicado para garantizar plenamente la seguridad de sus miembros.

Lo que queda claro, hasta el momento, es que no se podrá cantar victoria, ni siquiera con la firma de un acuerdo de paz con las Farc y el ELN, hasta tanto no se logre desmontar el paramilitarismo en país. La obligación del Estado será atacar desde la raíz estas estructuras criminales que han tenido la oportunidad de fortalecerse por cerca de 11 años y que hoy busca entorpecer los esfuerzos y anhelos de todo un país de alcanzar la paz, tras más de 50 años de padecer el flagelo de la guerra.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
