Title: 20 años de impunidad, 20 años de lucha para "que no muera La Esperanza"
Date: 2016-08-23 17:39
Category: DDHH, Nacional, Sin Olvido
Tags: crímenes de estado, Desaparición forzada, impunidad, Paramilitarismo
Slug: 20-anos-de-impunidad-20-anos-de-lucha-para-que-no-muera-la-esperanza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/La-Esperanza-12.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Corporación Jurídica Libertad 

###### [23 Ago 2016] 

Este fin de semana se conmemoraron 20 años de la desaparición forzada de 17 campesinos de la vereda la Esperanza en el Carmen de Viboral, al oriente antioqueño. Una fecha en la que las más de **120 familias de las víctimas** junto a las nuevas generaciones de niños y niñas le siguen exigiendo al Estado y a los grupos paramilitares verdad, justicia y reparación.

**“Tenía 1 año y 4 meses cuando se llevaron a mi papá”**, dice Celeni Castaño, hija de Hernando de Jesús Castaño, y sobrina de Juan Carlos y Octavio Gallego Hernández, algunos de los campesinos desaparecidos por una acción conjunta entre paramilitares y el Ejército Nacional.

Celeni, relata las actividades en el marco de la conmemoración. “Fue un momento muy bello, dimos homenaje a cada uno de los familiares. Iniciamos con una marcha de resistencia donde los familiares de las víctimas y otras personas llegaron al río Cocorná, donde soñamos a partir de la memoria y se realizó una eucaristía”, además resalta la participación de niños y niñas en varias actividades culturales, a través de las cuales se rendía recordaron a sus padres y abuelos desaparecidos.

**“Llegamos a la conclusión de que la lucha también debe ser alegre y con la memoria viva**, a través de la cual seguimos esperando justicia, verdad y reparación”, expresa Castaño, haciendo énfasis en que su reto y el de los demás familiares es que las siguientes generaciones “continúen la lucha de búsqueda de verdad, si nosotros no la encontramos”.

Así mismo, **durante la conmemoración se resaltó la importancia del proceso de paz que se lleva a cabo entre las FARC y el gobierno,** visto como un momento oportuno y esperanzador para el  desarrollo de proyectos comunitarios en las regiones, para el cese de estigmatizaciones a la población campesina como sucedió hace 20 años, y como aún sigue sucediendo en diferentes territorios del país.

Frente al caso, cabe recordar que el pasado 21 de junio se realizó una audiencia ante la Corte Interamericana de Derechos Humanos sobre la desaparición de los campesinos de esta vereda. Una ocasión para que el tribunal examine los beneficios jurídicos y los procesos de esclarecimiento de la verdad enmarcados en la Ley de Justicia y Paz. “Pudimos llegar a la CIDH y presentar nuestro caso. Ahora, esperamos el siguiente año una respuesta de la Corte a nuestro favor, **para que el Estado reconozca su responsabilidad en los hechos”, señala Castaño.**

El caso está en completa impunidad, pese a que en el año 2001 la justicia colombiana inició un proceso contra el mayor del Ejército Carlos Alberto Guzmán Lombana, y en el 2007 vinculó en el mismo al exjefe paramilitar Ramón Isaza, excomandante de las Autodefensas Campesinas del Magdalena Medio, este último se encuentra, como dice Celeni, “campante en la calle, lo que evidencia la impunidad que ha reinado en este crimen”.

**“Que no muera La Esperanza”,** fue el título de la campaña que acompañó la conmemoración de este año apoyada por la Corporación Jurídica Libertad, Hijos e Hijas por la Memoria y contra la Impunidad y el Centro Nacional de Memoria Histórica, donde se pretendía visibilizar lo sucedido, reivindicar las luchas de los familiares y enviar un mensaje de paz para que estos hechos jamás vuelvan a repetirse.

<iframe src="http://co.ivoox.com/es/player_ej_12641979_2_1.html?data=kpejlpade5qhhpywj5acaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncaTZzcrby5CnpdTowoqwlYqmddChhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
