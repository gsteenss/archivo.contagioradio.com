Title: Peñalosa: ¿107 millones de razones para apoyar el Proyecto Proscenio?
Date: 2019-02-18 18:17
Author: AdminContagio
Category: Entrevistas, Política
Tags: Campaña, Construcción, Construcción Cultural, Peñalosa
Slug: penalosa-proyecto-proscenio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/penalosa_7-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Silla Vacia] 

###### [18 Feb 2018] 

En su más reciente columna de opinión, el periodista  Daniel Coronell denunció que Enrique Peñalosa estaría beneficiando a aportantes  de su campaña para la Alcaldía de Bogotá, promoviendo la construcción del Proyecto Inmoviliario Proscenio, que tendría irregularidades en su gestación.

En la denuncia, Coronell explica que el Proyecto afectará la movilidad de las vías cercanas al barrio La Cabrera, al norte de Bogotá, y debido a que ha recibido apoyos y modificaciones por parte del Alcalde, dichas afectaciones solo se verán en toda su magnitud hasta que Proscenio entre en operación. Lo que llama la atención del Proyecto, es que quienes lo construyen y se han visto beneficiados por decisiones de Peñalosa, fueron aportantes a su campaña para la Alcaldía.

### **¿Qué es Proscenio y cómo lo apoyo Peñalosa?** 

Proscenio es un proyecto urbanístico que incluye la construcción de viviendas, oficinas, comercio y centros culturales al norte de la Capital; esta construcción está a cargo de los Grupos A (antes Grupo Cahid Neme Hermanos) y Amarilo, a través de su filial Cimento. Tal como lo recuerda el concejal por el Polo de Bogotá, Manuel Sarmiento, el grupo Chaid Neme Hermanos fue durante décadas el único representante comercial de Volvo para Colombia, incluso durante la primer alcaldía de Peñalosa.

Sarmiento recuerda que el plan parcial Proscenio había sido aprobado ya en 2010, y cuando llego el actual mandatario de la Capital a la Alcaldía modificó el Proyecto, permitiendo que la construcción fuera más alta, para "mejorarles el negocio que se había montado hace unos años". (Le puede interesar: ["La fijación de Peñalosa con los árboles de Bogotá"](https://archivo.contagioradio.com/penalosa-arboles-de-bogota/))

Sobre esta modificación se pronunció la Defensoría del Pueblo, que alertó sobre la ilegalidad de la misma y sobre la violación al Plan de Ordenamiento Territorial (POT), específicamente en la circulación vehicular de la zona,  en tanto el estudio de movilidad presentado por Proscenio no tiene la rigurosidad que se requiere para este tipo de construcción. (Le puede interesar: ["99 mil millones de pesos gastó Peñalosa en publicidad entre 2016 y 2017"](https://archivo.contagioradio.com/99-mil-millones-penalosa-publicidad/))

Sin embargo, lo que más llama la atención del Concejal sobre el Decreto 674 de 2018, con el que se modificó el Plan Parcial Proscenio, son los aportantes a la campaña de Peñalosa que se ven beneficiados: Arpro Arquitectos, compañía que hace parte del Grupo a, encargada de estructurar el proyecto y que donó 20 millones a Recuperemos a Bogotá; Amarilo, actual encargada del Proyecto que aportó 40 millones a la campaña; y el Grupo Económico Neme, beneficiado por el Fideicomiso que administra varios de los predios en los que está el Proyecto, y que aportó 47 millones de pesos.

### **Desde el Concejo seguirán haciendo oposición a Peñalosa** 

El Concejal asegura que este es solo uno de los proyectos en los cuales harán veeduría a la administración Peñalosa, pues la Procuraduría ya ha pedido más información sobre la tala indiscriminada de árboles en Bogotá, pero aún quedan por resolver la adquisición de la nueva flota de transmilenio y las medidas para resolver la crisis ambiental, producida por la contaminación atmosférica en la ciudad. (Le puede interesar: ["Ordenan a Peñalosa detener tala de árboles en Parque Japón"](https://archivo.contagioradio.com/penalosa-detener-arboricidio-parque-japon/))

> Ninguna sugerencia señor alcalde [@EnriquePenalosa](https://twitter.com/EnriquePenalosa?ref_src=twsrc%5Etfw). Lo que digo es que usted recibió donaciones de los promotores de Proscenio y por ética debió declararse impedido para tomar decisiones sobre ese megaproyecto. <https://t.co/ntILMtBnPT>
>
> — Daniel Coronell (@DCoronell) [18 de febrero de 2019](https://twitter.com/DCoronell/status/1097325827396636673?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
<iframe id="audio_32654844" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32654844_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
