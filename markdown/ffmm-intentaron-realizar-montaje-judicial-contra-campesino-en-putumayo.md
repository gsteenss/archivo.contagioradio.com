Title: FFMM intentaron realizar montaje judicial contra campesino en Putumayo
Date: 2015-07-08 16:52
Category: DDHH, Nacional
Tags: Batallón Domingo Rico, Brigada XVII de Selva, Constituyente Nacional de cultivadores de Coca, Falsos Positivos Judiciales, Luis Angulo, Putumayo
Slug: ffmm-intentaron-realizar-montaje-judicial-contra-campesino-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/falso-positivo-judicial-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: notimundo.in 

<iframe src="http://www.ivoox.com/player_ek_4740137_2_1.html?data=lZyhkpaXe46ZmKiak5eJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdSZpJiSo6mPqdSf1tOYz9TSuMLexpDX18nNp8rVzZDRx5DQpdSfp6u6r5DJsozE1tniz8bds46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Angulo] 

###### [8 Jul 2015] 

El campesino y defensor de **Derechos Humanos del Putumayo, Luis Angulo**, denunció que en la noche del pasado 7 de julio fue víctima de un intento de montaje judicial por parte del **Batallón Domingo Rico perteneciente a la Brigada XVII de Selva**. Según el relato del campesino los militares irrumpieron, encapuchados, en un lugar público de Puerto Asis.

“*Cuando llegaron me hacen colocar boca abajo en el piso… cuando me quitan la mochila me percato de que estaban haciendo algo, y dos minutos después... cuando me dicen que la abra me doy cuenta de que hay algo que no era mio*” relata Angulo, quien afirma que el mismo **militar de apellido Mejía**, que lo estaba requisando le dice “¿entonces esos explosivos de quién son?”.

Luego de este operativo llega la policía, lo esposan y lo conducen a una camioneta gris del ejército rumbo al batallón Domingo Rico, lo encerraron en una habitación y se inició un interrogatorio en que lo acusaban de ser parte de un plan para colocar explosivos. **¿qué te mandaron a hacer?, ¿contra quién es el operativo?** eran algunas de las preguntas, relata Angulo.

Después  de ser interrogado por el ejército, llegó la policía judicial quienes también lo interrogaron sobre su actividad en la ciudad. El campesino relató que se **encontraba haciendo parte de la Constituyente de Coca, Marihuana y Amapola**, como encargado de funciones logísticas. Luego de ello y de que se firmara un acta de “buen trato” el campesino fue liberado.

Según el propio campesino horas de la mañana desapareció un Coronel del ejército y por ello los efectivos militares estaban en la busca de capturas para **“mostrar resultados”**. Este tipo de acciones, según organizaciones de DDHH se ha repetido con otras personas que están siendo judicializadas o en libertad una vez se logra demostrar su inocencia.
