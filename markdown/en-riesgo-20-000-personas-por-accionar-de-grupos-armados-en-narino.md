Title: En riesgo  20.000 personas por accionar de grupos armados en Nariño
Date: 2020-08-13 17:20
Author: AdminContagio
Category: Actualidad, Nacional
Tags: AGC, Alerta temprana, Disidencias de las FARC, grupos armados, nariño
Slug: en-riesgo-20-000-personas-por-accionar-de-grupos-armados-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Grupos-armados-en-Nariño.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Comisión Intereclesial de Jusiticia y Paz

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Defensoría del Pueblo advirtió a través de la Alerta Temprana 036 de 2020 que **la población de los municipios de El Rosario y Leiva en el departamento de Nariño, está en riesgo** debido a la presencia de disidencias de las FARC (Columna Móvil Jaime Martínez & Frente Carlos Patiño) y del grupo paramilitar  autodenominado Autodefensas Gaitanistas de Colombia -AGC-.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, **desde el 8 de agosto se presentan enfrentamientos entre las disidencias y las AGC para el control del territorio.** (Lea también: [Atentan contra la vida del líder Awá, Javier Cortés Guanga en Nariño](https://archivo.contagioradio.com/atentan-contra-la-vida-del-lider-awa-javier-cortes-guanga-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el reporte de la Defensoría **la confrontación entre estos grupos armados ilegales podría incrementar el riesgo de homicidios, confinamiento, desplazamiento forzado,  reclutamiento e incluso violencia sexual en contra de más de 20.000 personas en la zona.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A propósito de esta alerta, el pasado 9 de agosto se perpetró en Leiva, Nariño el **asesinato de los estudiantes de bachillerato Cristián Caicedo y Maicol Ibarra de 12 y 17 años** que según versiones de la comunidad iban a llevar una tarea que les habían delegado en su colegio. (Lea también: [Cristian y Maicol, menores víctimas de la barbarie en Cauca y Nariño](https://archivo.contagioradio.com/cristian-y-maicol-menores-victimas-de-la-barbarie-en-cauca-y-narino/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/marthaperaltae/status/1292866620986187778","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/marthaperaltae/status/1292866620986187778

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CIDH/status/1293891892309045248","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CIDH/status/1293891892309045248

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### El control territorial de los grupos armados en Nariño

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la Defensoría los grupos armados se disputan este territorio porque «*La ubicación geográfica de estos municipios permite la consolidación de tres importantes corredores de movilidad: hacia el norte, uno conducente al departamento de Cauca; hacia el suroccidente, otro proyectado en dirección hacia el río Patía con desembocadura hacia el Pacífico; y hacia el sur, un último corredor conducente hacia el cordón fronterizo colombo ecuatoriano*» **lo que los convierte en un punto estratégico para el narcotráfico y la minería ilegal.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De hecho, hace dos años la Defensoría del Pueblo había emitido una alerta similar en dicha zona, a través de la [Alerta Temprana 082 de 2018](http://www.indepaz.org.co/wp-content/uploads/2020/02/AT-N%C2%B0-082-18-NAR-Cumbitara-El-Rosario-Leiva-y-Policarpa.pdf), donde advertía que los municipios de Cumbitara, El Rosario, Leiva y Policarpa con predominio de comunidades indígenas y afrodescendientes estaban en riesgo por la presencia de las mismas AGC y de la disidencia de FARC denominada Estiven Gónzalez. (Le puede interesar: [Otra Mirada: Nariño resiste ante el olvido estatal](https://archivo.contagioradio.com/otra-mirada-narino-resiste-ante-el-olvido-estatal/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
