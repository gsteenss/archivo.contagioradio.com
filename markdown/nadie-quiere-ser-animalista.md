Title: Nadie quiere ser animalista
Date: 2017-01-17 09:57
Category: Opinion
Tags: Ambientalismo, Animales, Animalistas
Slug: nadie-quiere-ser-animalista
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Nadie-quiere-ser-animalista.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 17 Ene 2017 

#### Por **[Mateo Córdoba]** 

En la historia abundan los personajes que preliminarmente fueron juzgados como necios o ultras, y solo el transcurrir de los años les permitió ser comprendidos como hijos de su época, como hijos de una necesidad histórica. Ser señalada como una demente o una radical parece ser esa gran primera hostilidad con que han de lidiar quienes a pesar de su época, se atreven a tener una visión más amplia a lo dado por la contingencia socio-cultural, es decir, aquellas que son capaces de extender los criterios de justicia y empatía más allá de lo que su contexto les hubiese permitido.

Diré algo que bien podría ser simple pero –como afirmación– engendra un sinfín de fundamentos históricos, políticos y culturales: NADIE QUIERE SER ANIMALISTA. Esto me llevaría a ser supremamente juicioso con la definición de animalista. Sin embargo, comprendo como ‘animalista’ a todas aquellas que de una u otra manera hacen de la lucha por la protección, el trato digno y el respeto a los animales no-humanos uno de los pilares en su vida.  Que ello sea un pilar implica, entre otras, salir a las calles por la dignidad de nuestros compañeros de planeta y, en últimas, enfrentarse día a día con ese anacronismo al que aún parece estar condenado el animalismo. Lucha anacrónica en cuanto sus apuestas éticas y políticas retan a la sociedad a reflexionar trascendiendo de su supuesto sentido común, cuestionando sus más estables valores.  Anacronismo definido en este caso no por el anclaje a paradigmas improcedentes, sino por un llamado a la superación de esquemas y dar pasos radicales hacia el futuro.

Elegir ser animalista está muy lejos de ser un deseo de quienes asumimos dicha lucha. La historia y los más importantes triunfos para los movimientos sociales estuvieron siempre marcados por la gallardía de quienes, apostando por nuevas maneras de relacionarse entre géneros, entre grupos étnicos y entre especies, salieron deliberadamente del letargo y el ensimismamiento para confrontar al poder, para confrontar al sentido común que cuando pierde lo común merece ser encarado. El carácter deliberado de la lucha animalista no significa que sea anhelo de las activistas que la nutren. El animalismo, como todos los movimientos, es resultado de un proceso de proscripción de los fundamentos sobre los cuales se mueve el mundo de lo social y ello es necesariamente forzar la historia.

La dignidad de cualquier animal –humano y no-humano– no debería estar condicionada a la capacidad de un movimiento social para defenderle. La dignidad ha de ser respetada como imperativo de la relación humano-naturaleza (si es que insistimos en asumirnos como especie fuera de ella). Así pues, el animalismo es hijo de su época, de su trágica época. El desbocamiento de la industria alimentaria, el advenimiento del desastre ambiental y el entendimiento del maltrato animal como eje del bienestar y el entretenimiento humano es lo que nos tiene aquí en las calles y los parlamentos diciendo cuantas veces sea necesario “¡No más!”.

[A las animalistas se nos han impuesto esta lucha que libramos a sabiendas de que somos incapaces de voltear la mirada para evitar la escena de un animal violentado y humillado. A las animalistas nos impusieron una lucha que por justa no debiera ser perpetua pues, a fin de cuentas, la defensa de la dignidad y la vida tendrían que ser fundamento del sentido común sobre el cual la humanidad cimente su propio acontecer, si es que la paz y las sostenibilidad en verdad son esos derroteros que se ha fijado. Nadie quiere ser animalista pero lo seremos cuantas vidas sean necesarias hasta que a fuerza de movilización e ímpetu logremos, de una vez por todas, poder dedicarnos a la autoconstrucción despreocupada sin que ello implique restarle una voz a dicho coro que demanda la]**liberación animal**[, pues solo ella nos desvinculará –a las animalistas– de esta lacerante lucha.]

[Artículo dedicado a]*[Tilikum]*[, orca que toda su vida fue condenado al cautiverio y actualmente se encuentra ad portas de la muerte en Sea World.]

¡NI UNA MÁS!

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.
