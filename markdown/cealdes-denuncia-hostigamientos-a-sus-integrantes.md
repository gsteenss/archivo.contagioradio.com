Title: CEALDES denuncia hostigamientos a sus integrantes
Date: 2020-07-30 08:00
Author: AdminContagio
Category: Actualidad, DDHH
Slug: cealdes-denuncia-hostigamientos-a-sus-integrantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Sebastian-Gómez-CEALDES.mp3" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/36912489_1072425656241231_8282102118253330432_o.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*FOTO: @Cealdes*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las organizaciones integrantes de la Misión Humanitaria por la Vida y la Paz en los Llanos Orientales y la Orinoquia compuesta por 13 organizaciones de Derechos Humanos denunciaron este 27 de julio **el hurto de equipos con información recolectada en la misión humanitaria**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho se presentó el lunes en horas de la noche al norte de Bogotá, a la altura de la calle 200; allí hombres armados interceptaron a **Sebastián Gómez representante legal del Centro de Alternativas al Desarrollo social -CEALDES- y Catalina Oviedo participante de la misión** y responsable de la región Amazonas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las organizaciones los sujetos se movilizaban en diferentes vehículos y **hurtaron a los defensores sus celulares, computadores y equipos con información recolectada en el marco de la misión humanitaria,** incluyendo bases de datos de organizaciones sociales y comunitarias, líderes y lideresas territoriales; así como información sensible del trabajo que realiza CEALDES y datos personales de sus miembros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El robo además, se presentó algunas horas después de que los defensores entregaran junto a las otras organizaciones el primer avance de la Misión, al mismo tiempo **Sebastián Gómez**, representante de CEALDES, agregó que dentro de los elementos hurtados se encuentran también dos cámaras fotográficas con **material audiovisual inédito de las comunidades visitadas.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Esto va mas allá de posibles hostigamientos*. *La situaci*ón se *agrava mucho más teniendo en cuenta que se prepara **un informe sobre violaciones a los Derechos Humanos por parte de la Fuerza Pública** y la presencia de grupos armados ilegales en la región"*

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### "No son solo los hostigamientos, es la alerta ante el uso que se le pueda dar a la información"

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Sebastián Gómez**, señaló que este no son los únicos de hostigamientos en contra de las personas de la organización, *"los y las integrantes han recibido amenazas, también dos colaboradores expertos locales de algunas de las iniciativas"*, además señaló que desde **diferentes cuentas vinculadas a servicios militares y de seguridad privada visitan con regularidad las redes sociales de la organización.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma extendió la alerta a las comunidades de las diferentes regiones, así como a las organizaciones defensoras de derechos humanos de la situación vivida, **y exigieron a las autoridades que se realicen investigaciones que permitan identificar los responsables**, así como el paradero de la información hurtada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto según el defensor, teniendo en cuenta la situación actual del país y el incremento incontrolable de la persecución y asesinato de líderes, lideresas sociales y ambientales*, "**creemos que es necesario llamar la atención sobre el uso que pueden hacer manos criminales de la información tanto de las comunidades visitadas como el los deseables en su conjunto**"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Que denuncia la Misión Humanitaria ?

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la primera entrega de estas 13 organizaciones de la [Misión Humanitaria](https://www.justiciaypazcolombia.com/mision-humanitaria-por-la-vida-y-la-paz-en-los-llanos-orientales-y-la-orinoquia-2/)que se llevó a cabo durante 10 días (del 18 al 27 de julio), los y las investigadoras señalaron que, encontraron ***"territorios desolados y destrozados"*, así como también artefactos utilizados por el ESMAD, y residuos de armas largas utilizadas por el Ejército en el mismo lugar.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *" Vimos el miedo de los rostros de la población local por la amenaza de militares que durante los distintos operativos gritaban -No se preocupe por nosotros preocúpense por los que vienen detrás-* "

<!-- /wp:quote -->

<!-- wp:paragraph -->

También agregan que vieron cultivos de pan coger destrozados y arrancados, casas quemadas, depósitos destruidos, mangueras machetadas, mercados regados en el suelo, **sumado a los testimonios de dolor e indignación de mujeres y niñas agredidas por la [Fuerza Pública](https://archivo.contagioradio.com/otra-mirada-el-catatumbo-entre-fuego-cruzado/).**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Escuchamos testimonios indignados y dolidos de mujeres que señalaban ser víctimas de seguimiento de inteligencia militar que les describieron incluso y la ropa íntima que llevaban un día determinado"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Acciones que recuerdan el pasado más violento del país. De nuevo víctimas de minas antipersona, judicialización y extinción de dominio como estrategia despojo, estigmatizaciónes, hostigamientos y desarticulación del tejido social, esto acompañado de testimonios de incertidumbre e indignación en torno a los incumplimientos del Gobierno en la implementación del Acuerdo de Paz, especialmente al Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Sentimos la impotencia el miedo y la incertidumbre de las comunidades por todo lo que han vivido … sentimos dolor de patria al ver cómo los militares pisotearon su honor con el tratamiento de guerra a comunidades campesinas"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Una lista de acciones que se suma a un llamado urgente a la comunidad nacional e internacional para **activar acciones que eviten la tragedia humana y ambiental con consecuencias irreparables**, así como también la incidencia al cumplimiento de los acuerdos bilaterales y además de presionar a las empresas a respetar las exigencias éticas en Derechos Humanos y ambientales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:audio {"id":87563} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Sebastian-Gómez-CEALDES.mp3">
</audio>
  

<figcaption>
Escuche la entrevista completa de **Sebastián Gómez representante legal del Centro de Alternativas al Desarrollo social -CEALDES**

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:block {"ref":78955} /-->
