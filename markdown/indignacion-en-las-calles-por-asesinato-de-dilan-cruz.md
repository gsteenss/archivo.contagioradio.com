Title: Indignación en las calles por asesinato de Dilan Cruz
Date: 2019-11-26 12:40
Author: CtgAdm
Category: Estudiantes, Paro Nacional
Tags: Agresiones del ESMAD, Desmonte del ESMAD, Dilan Cruz, Paro Nacional
Slug: indignacion-en-las-calles-por-asesinato-de-dilan-cruz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Dilan1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Dilan.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/DSC_0310.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/DSC_03132-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Andres Zea/Contagio Radio] 

Las muestras de rechazo contra **el asesinato del joven manifestante Dilan Cruz**, como consecuencia del uso excesivo de fuerza del **Escuadrón Movil Antidisturbios** (ESMAD) continúan en las calles de todo el país donde los manifestantes exigen que tanto el Gobierno como la Fuerza Pública acepten su responsabilidad en lo ocurrido y se proceda al el desmonte de esta unidad de la Policía.

Los sucesos tuvieron lugar el sábado 23 de noviembre a las 4:00 p. m en Bogotá cuando el cacerolazo que se efectuaba en la cra 4ta con calle 19 empieza a ser dispersados por las fuerzas antidisturbios, en medio de los hechos,  uno de los integrantes del ESMAD accionó un arma en contra de Dilan, presente en la manifestación,  impactándolo en la cabeza, acto seguido, el joven fue llevado  al Hospital San Ignacio donde ingresó con trauma craneoencefálico tras haber sufrido dos paros cariorespiratorios.

Desde ese sábado, pese a que su estado era grave pero estable, su condición cerebral comenzó a deteriorarse rápidamente hasta entrar el pasado lunes en un estado crítico irreversible del que los médicos esperaban una "evolución de curso final", sin embargo horas más tarde su anunció su fallecimiento, un suceso que al conocerse, generó un rechazo de la población  e incentivó a una nueva ola de indignación y reprobación que ha sido llevada a las calles en una nueva jornada de paro nacional en rechazo del ESMAD.

Según avances en la investigación realizada, el arma que se habría accionado contra el joven, fue una escopeta calibre 12, la que se carga con cartuchos con pequeños balines envueltos en una micro fibra conocida como kevlar que aunque está diseñada para neutralizar, puede llegar a ser letal, acorde a expertos de seguridad.  [(Lea también: Salud de Dilan Cruz entra en estado “crítico irreversible”)](https://archivo.contagioradio.com/salud-de-dilan-cruz-entra-en-estado-critico-irreversible/)

### Dilan no murió, a Dilan lo mataron

El movimiento estudiantil exhortó [a continuar en la movilización, "se debe seguir trabajando por la construcción de este paro y esperamos la respuesta de Duque. Si no existe respuesta a estas demandas, lo más sensato, cómo expresamos en nuestro comunicado, es la renuncia inmediata del presidente", manifestó Cristian Reyes, integrante de la Unión Nacional de Estudiantes de Educación Superior (UNEES).]

![Dilan Cruz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Dilan1-1024x768.jpg){.size-large .wp-image-77157 width="1024" height="768"}

Foto: @ALEJOMICHELLS

Desde horas de la noche, manifestantes se desplazaron hasta el centro de la ciudad, lugar en el que ocurrieron los hechos para rendir un homenaje al joven, mientras  otra multitud se congregó frente al Hospital Universitario San Ignacio, en la carrera Séptima con calle 40, para despedir al joven mientras al ritmo de arengas, cacerolas y flores blancas.  [ (Le puede interesar: El prontuario del ESMAD)](https://archivo.contagioradio.com/salud-de-dilan-cruz-entra-en-estado-critico-irreversible/)

En Barranquilla las movilizaciones en su honor comenzaron cerca de las 9:28 a. m, los manifestantes, en su mayoría estudiantes de la Universidad Autónoma y el Sena, bloquean la calle 30, al sur de la arenosa, casi llegando a Soledad, Atlántico. Mientras en la ciudad de Cali, la marcha comenzó a las 9:00 am cuando decenas de manifestantes se concentraron a en la calle 13 con carrera 100, cerca a la universidad del Valle donde personas marcharon para exigir que se haga justicia.

\[caption id="attachment\_77164" align="aligncenter" width="745"\]![26 de Nov frente a la Universidad Nacional](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/DSC_03132-1.jpg){.wp-image-77164 .size-full width="745" height="420"} 26 de Nov frente a la Universidad Nacional\[/caption\]

En Bogotá, estudiantes de la Universidad Nacional salieron a la calle 26 en protesta por la muerte del estudiante y se espera que hacia las 4:00 pm la ciudadanía se congregue en el lugar que sucedieron los hechos, para realizar un plantón en su memoria.

> ¡Dilan no murió, ustedes lo mataron! A esta hora cientos de personas se pronuncian en rechazo del asesinato del joven. [pic.twitter.com/aB67deQP81](https://t.co/aB67deQP81)
>
> — Contagio Radio (@Contagioradio1) [November 26, 2019](https://twitter.com/Contagioradio1/status/1199372459784445963?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
