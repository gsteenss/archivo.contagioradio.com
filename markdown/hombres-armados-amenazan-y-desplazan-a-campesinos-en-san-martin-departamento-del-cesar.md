Title: Hombres armados amenazan y desplazan a campesinos en San Martín, Departamento del Cesar
Date: 2015-04-28 16:53
Author: CtgAdm
Category: Comunidad, DDHH, Nacional
Tags: abilio diaz, amenaza, campesino, cesar, cienaga, la torcoroma, Palma, Paramilitar, pescador, san martin
Slug: hombres-armados-amenazan-y-desplazan-a-campesinos-en-san-martin-departamento-del-cesar
Status: published

###### Foto: Agustin Codazzi 

 

Campesinos del sur del departamento del Cesar denuncian que el mes de junio del 2014, hombres armados acompañados de un individuo que se identificó como Alirio Diaz, los amenazaron y desplazaron forzosamente de los playones comunales Ciénaga La Torcoroma, del municipio de San Martín, para introducir maquinaria pesada con el objetivo de sembrar palma aceitera en el territorio.

Los campesinos aseguran que realizaron las denuncias pertinentes, pero más de 10 meses después, al no encontrar respuesta por parte de las autoridades, decidieron realizar actos pacíficos de posesión en Ciénaga, a partir del pasado sábado 24 de abril.

Dos días después, trabajadores de Alirio Diaz se presentaron armados en el playon para agredirlos verbal y físicamente, acusándolos de ser guerrilleros, e intentando arrollar a varios de ellos con uno de los tractores de la palmicultora.

Posteriormente, dos presuntos policias de San Martín, que no portaban placas, procedieron empadronar a los campesinos que permanecían en la Ciénaga La Torcoroma. Los habitantes de San Martín aseguran que este tipo de "listas negras" son posteriormente entregadas a paramilitares que asesinan a campesinos y pescadores en resistencia.

Campesinos y campesinas cesarenses hacen un llamado a las organizaciones defensoras de Derechos Humanos, a organismos internacionales y al gobierno colombiano para que garantice su derecho a la vida en sus territorios.
