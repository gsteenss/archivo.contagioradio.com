Title: Herencia de Timbiquí, sabor pacífico que recorre Latinoamérica
Date: 2015-10-02 13:07
Category: Cultura, eventos
Tags: Cultura, gira latinoamericana Herencia de TImbiquí, Herencia de Timbiquí, Música, Teatro Jorge Eliécer Gaitán, Viña del Mar
Slug: herencia-de-timbiqui-sabor-pacifico-que-recorre-latinoamerica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/herencia_de_timbiqui.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: youtube.com 

###### [2 oct 2015] 

La agrupación afrocolombiana Herencia de Timbiquí inicia **hoy a las 8:00 de la noche** en el **Teatro Jorge Eliecer Gaitán**, en** **Bogotá, una serie de conciertos, que harán parte de su **gira por Latinoamérica** en la que visitarán países como México, Argentina y Uruguay.

El viaje por Latinoamérica de este grupo conformado en el año 2000, comenzará **el 9 de octubre** con una presentación en el Festival internacional Cervantino de Guanajuato, en **México**.

El **11, 12 y 15 de octubre viajarán a Argentina** y estarán en el Centro Cultural Néstor Kirchner en Buenos, al día siguiente en Ciudad de La Plata, se presentarán en el Teatro Coliseo Podestá y el el 15 de octubre terminarán sus presentaciones nuevamente en Buenos Aires, en el Teatro Sha.

En Uruguay tendrán concierto el 17 de octubre en el Festival Contrapedal de Montevideo, y luego tocarán en la afamada Sala Zitarroza, de la capital uruguaya.

Canciones como “Amanece”, “Te invito”, “Coca por coco”, “La sargento Matacho”, “Quiero cantarte”, serán temas que estos exponentes del folklor colombiano tocarán en tarima.

Herencia de TImbiquí fusiona ritmos urbanos con la alegría característica de los ritmos tradicionales del pacífico como el Currulao, el Bunde, la Juga, los Arrullos, Abozaos, Bereju, Bambazu y el Porro, además ésta agrupación ha sido premiada en los premios Shock de la música colombiana; en el Festival del Petronia Álvarez y el Festival internacional de **Viña Del Mar, en Chile**, 2013, donde obtuvieron **Gaviota de Plata a la Mejor Interpretación Folclórica con su canción “Amanece”.**
