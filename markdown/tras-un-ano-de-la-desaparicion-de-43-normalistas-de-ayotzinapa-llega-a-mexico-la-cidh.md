Title: Tras un año de la desaparición de 43 normalistas de Ayotzinapa llega a México la CIDH
Date: 2015-09-28 12:53
Category: DDHH, El mundo
Tags: 43 estudiantes, 43 normalistas desaparecidos, Ayotzinapa, Jornada global por ayotzinapa, Normalistas de Guerrero
Slug: tras-un-ano-de-la-desaparicion-de-43-normalistas-de-ayotzinapa-llega-a-mexico-la-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/aYOTZINAPA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: subversiones.org 

<iframe src="http://www.ivoox.com/player_ek_8666266_2_1.html?data=mZujmJeaeo6ZmKiakp2Jd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNPV1JDi0JDFaaSnhqee0ZDIqYzgwpDRx9jFtMLmysjWh6iXaaOnz5DRx5CYd4zi0Nfaw9HNt9XV1JDRx5ClcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alina Duarte, Revolución 3.0.] 

###### [28 sep 2015] 

Doce meses después de la desaparición de los **43 normalistas** en Iguala (México) más de **35.000 personas** se movilizaron exigiendo al gobierno de Peña Nieto que devuelvan a los jóvenes con vida y que se cuente la verdad sobre lo que sucedió el 26 de septiembre del 2015.

Esta jornada a diferencia de las anteriores en las que el gobierno de Peña Nieto, prometía informaciones con las cuales se cerraría el caso, tuvo la particularidad que las personas esta vez sin ninguna promesa “*no solo exigieron la aparición con vida de los normalistas, sino también que se esclarezcan los hechos de Ayotzinapa*”, indicó Alina Duarte, reportera de Revolución 3.0.

Semanas atrás la Comisión Interamericana de Derechos Humanos (CIDH) **había derrumbado “la verdad histórica”**, indicando la imposibilidad científica y las irregularidades que presentaba este informe del gobierno de Peña Nieto, sobre lo que sucedió con los estudiantes de la normal de Ayotzinapa.

Hoy arribará a México una comisión del CIDH encabezada por su presidenta RoseMarie Belle Antoine, quienes mediante una “*observación de terreno"* revisarán los casos de garantías en el país, enfocándose en los casos de desapariciones extrajudiciales, desaparición forzada, tortura y el acceso de víctimas a la justicia. Esta comisión se reunirá con el [GIEI](https://archivo.contagioradio.com/giei-desmiente-version-oficial-sobre-la-desaparicion-de-43-estudiantes-de-ayotzinapa/) con el fin de estudiar el informe sobre los 43.
