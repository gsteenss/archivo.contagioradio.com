Title: Los tres puntos de organizaciones y procesos ambientales en el paro nacional
Date: 2019-11-27 18:15
Author: CtgAdm
Category: Ambiente, Paro Nacional
Tags: Ambiente, ESMAD, Movilización, Paro Nacional, paz
Slug: los-tres-puntos-de-organizaciones-y-procesos-ambientales-en-el-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/ambientalistas-en-el-paro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@PizarroMariaJo] 

El **Comité Nacional del Paro** comunicó las razones por las que decidió levantarse del dialogo con el Gobierno, una de las principales razones para abandonar la mesa es la falta de respuestas y acciones concretas por parte del Presidente Duque. De otra parte destacaron las acciones que están desarrollando en el continuidad de un paro pacífico y con diálogo social.

**Susana Muhamad,** ambientalista y ex- secretaria de ambiente, explicó que la razón del retiro, se dio luego de un diálogo sin acuerdo de términos ni acciones concretas, "este es un gobierno de oídos sordos que responde solo a sus intereses y no señala agenda, cambios o acciones ante el paro", y añadió diciendo que luego de algunas intervenciones el Presidente hizo un largo pronunciamiento  sobre sus acciones y logros en sus meses de gobierno. (Le puede interesar:[Gobierno Duque cumple un año obstaculizando la paz y la garantía de DDHH](https://archivo.contagioradio.com/gobierno-duque-cumple-un-ano-obstaculizando-la-paz-y-la-garantia-de-ddhh/))

De igual forma Muhamad agregó, "nosotros no elegimos un gobierno para que se abran diálogos eternos de promesas incumplidas, sino elegimos un gobierno que escuche y tome decisiones pensando en los ciudadanos".

### **Los puntos del movimiento ambientalista en el paro nacional**

La ambientalista destacó tres puntos por los que este movimiento se ha unido al paro: el primero, se refiere a la participación incidente, haciendo referencia a las consultas populares y la protección de los líderes sociales, "al igual que como pasa con la protesta social, a los defensores del ambiente los ataca el Esmad, a ellos también los están asesinando" dijo la ambientalista.

En segundo lugar presentaron temas como el uso del glifosato, la minería y el fracking; y por último hablaron de la emergencia climática, "según el reporte de alerta de las Naciones Unidad, hay que hacer grandes y significativos cambios en las políticas ambientales, acción en la que el Presidente hace todo lo contrario y comete los mayores actos que afecten el ambiente". (Le puede interesar: [Permitir minería en Santurbán es arriesgar el 85% del agua de los colombianos](https://archivo.contagioradio.com/permitir-mineria-en-santurban-es-arriesgar-el-85-del-agua-de-los-colombianos/))

### "¡ El Paro ya está generando resultados!"

Muhamad afirmó que "el paro ya está generando resultados y que es erróneo pensar que la movilización masiva de la gente para hacerse escuchar sea en vano", y destacó que los primeros cambios se han visto esta semana empezando con  el retiro que hizo el  senador Uribe  Vélez, del proyecto de ley de la Reforma Laboral con respecto al salario de los jóvenes, al igual que los cambios de a la reforma tributaria. (Le puede interesar:[El tal paquetazo sí existe: Oposición](https://archivo.contagioradio.com/el-tal-paquetazo-si-existe-oposicion/))

Y destacó los proyectos que ya están en marcha en defensa del ambiente, "en el Congreso se está trabajando en un proyecto de ley para la prohibición del fracking desde hace dos años; al igual que un proceso de consultas populares, iniciativas democráticas, reales y participativas", y afirmó que el trabajo de las próximas semanas fundamentará el dialogo ciudadano, que ya desarrolla en algunas localidades de la ciudad. Y concluyó diciendo, "nuestra invitación es a que más personas se sumen a este paro y así mismo a las conversaciones sociales".

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44903538" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44903538_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
