Title: Tras 16 años de la masacre, Bojayá no ha logrado cerrar el capítulo del dolor
Date: 2018-10-04 18:10
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: Bojaya, Chocó, Cristo, masacre, Reparación
Slug: bojaya-cerrar-dolor
Status: published

###### [Foto: @Bunkergio] 

###### [4 Oct 2018] 

**El Cristo mutilado de Bojayá,** símbolo de resistencia de esa comunidad, llegó por segunda ocasión a Bogotá para acompañarlos en su exigencia de que, tras 16 años de la masacre, lleguen a su municipio las medidas de reparación prometidas por el Gobierno, y la paz que les permita vivir sin las angustias de la guerra.

Entre las acciones que reclaman, está la búsqueda de personas desaparecidas, la construcción de **un centro de salud que atienda física y psicológicamente a las 149 personas afectadas por el cilindro bomba (eran 159 pero 10 han muerto de cáncer)** y la reparación colectiva de los habitantes de Buenavista y los cabildos indígenas Emberá que habitan en el territorio.

### **"El Cristo de Bojayá ha querido acompañarnos para denunciar la grave situación que viven las comunidades"** 

La primera vez que la imagen religiosa estuvo en la capital del país fue durante la visita del papa Francisco a Colombia, en esa ocasión, la presencia del sumo pontífice significó un apoyo a la implementación del acuerdo de paz firmado en el teatro Colón. Ahora, los habitantes de Bojayá esperan que la figura religiosa los acompañe nuevamente para denunciar la violencia, y para pedir que llegue la paz al Chocó.

Según **Leyner Palacios, sobreviviente de la masacre de Bojayá, el municipio no ha podido cerrar el capítulo del dolor porque la guerra en el departamento continúa**, nuevos grupos se están formando, líderes sociales siguen amenazados y en este momento "10 comunidades indígenas están sufriendo por confinamiento". En otras palabras, desde hace varios meses están viviendo un recrudecimiento del conflicto armado.

Palacios denunció que **en la zona hacen presencia grupos paramilitares y el ELN**, además, se presentan "situaciones complejas sobre el comportamiento de algunos miembros de las fuerzas militares, que se ubican en puntos donde las comunidades deben pasar obligatoriamente", hecho que pone en riesgo a las personas, porque no hay distinción entre civiles y actores armados, lo que es una violación al Derecho Internacional Humanitario.

Sin embargo, **la situación crítica que se vive en Bojayá es común a la región del Bajo Atrato chocoano, "en Riosucio, San Juan, Juradó y Los Baudós** hay una crisis humanitaria hace 9 años y que no ha podido superarse". Otras organizaciones humanitarias han denunciado que la crisis se extiende por todo el Departamento, y los problemas más graves incluyen violencia, desnutrición infantil y degradación ambiental. (Le puede interesar: ["Se agudiza crisis humanitaria en el Chocó"](https://archivo.contagioradio.com/se-agudiza-crisis-humanitaria-en-choco/))

### **"Los chocoanos la hemos sacado barata porque no hemos vivido otra tragedia como la de Bojayá"** 

El líder afro sostuvo que aunque ellos han sido constructores de paz, hay hechos que los revictimizan porque actualmente no encuentran un ambiente que brinde garantías de no repetición, e incluso, **"yo soy de los que creo que la hemos sacado barata los y las chocoanas, en la medida en que no hemos sufrido otra tragedia igual** o peor que la de Bojayá". (Le puede interesar: ["Cinco casos por los que la JEP pone la lupa a crímenes en Urabá y Bajo Atrato chocoano"](https://archivo.contagioradio.com/cinco-casos-los-la-jep-pone-la-lupa-crimenes-uraba-atrato-chocoano/))

Palacios aseveró que con la continuación en la búsqueda de una salida política al conflicto armado es posible prevenir estos hechos de violencia. En consecuencia, el Gobierno debería continuar las conversaciones de paz con el ELN y superar las diferencias de la mesa, porque **los habitantes de Bojayá están "convecidísimos de que la salida al conflicto por la vía militar no es posible en Colombia".**

Otra acción para apoyar la paz de parte del Gobierno Nacional debería ser la implementación del acuerdo de paz, hecho que requiere la asignación de **recursos para garantizar la operación de los mecanismos del Sistema de Verdad, Justicia, Reparación y No Repetición y los Programas de Desarrollo con Enfoque Territorial.** Adicionalmente, deberían aprobarse las Circunscripciones de paz, que permitirían que las comunidades tengan voz en el Congreso.

### **"Nosotros confiaremos una vez más en la institucionalidad"** 

El líder recordó que 16 años después de la masacre, aún no se ha avanzado en los procesos de reparación colectiva porque, muchos sobrevivientes no han recibido el acompañamiento psicosocial adecuado. De hecho, Palacios sostuvo que de las 159 personas que resultaron lesionadas en Bojayá, 10 fallecieron de cáncer; y otros tantos han llegado agonizantes al hospital de Quibdó, que queda a 4 horas de la población.

Por estas razones, y cansados del incumplimiento por parte del Gobierno, Palacios y otros bojayeseños vivieron a Bogotá, para que el Ministerio de Salud se comprometa a tomar acciones que mejoren el servicio en el Municipio, y que se retomen los compromisos de reparación hechos con las comunidades. (Le puede interesar: ["Los saldos pendientes con las víctimas de la masacre"](https://archivo.contagioradio.com/los-saldos-pendientes-con-las-victimas-de-bojaya/))

En consecuencia, el líder afro confirmó que el **Comisionado de Paz Miguel Ceballos,** en frente del Cristo de Bojayá, **se comprometió a retomar la agenda de acuerdos para mejorar las condiciones de las comunidades**; razón por la que ellos, nuevamente confiarán en la institucionalidad y esperarán que otra vez "no nos dejen con los crespos hechos".

<iframe id="audio_29120806" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_29120806_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
