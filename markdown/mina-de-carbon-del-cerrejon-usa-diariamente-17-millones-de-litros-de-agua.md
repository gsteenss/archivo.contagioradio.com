Title: Mina de carbón del Cerrejón usa diariamente 17 millones de litros de agua
Date: 2016-04-06 21:52
Category: DDHH, Nacional
Tags: El Cerrejón, Guajira, Mineria
Slug: mina-de-carbon-del-cerrejon-usa-diariamente-17-millones-de-litros-de-agua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/cerrejon-960x623.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Espectador 

###### [6 Abr 2016] 

En total, **17 fuentes hídricas en La Guajira se han secado debido la actividad minera de la empresa El Cerrejón,** según denuncia Angélica Ortíz, vocera de Fuerza Mujeres Wayúu. La combinación de las formas de privatización de las aguas, por apropiación y contaminación producto del modelo extractivo, son las principales causas por las cuales actualmente el pueblo indígena de la Guajira está muriendo de sed y hambre.

El Río Ranchería, es, o tal vez era, una de las principales fuentes de agua de todo el departamento, se extiende 223 kilómetros desde la Sierra Nevada de Santa Marta, para terminar su recorrido en el mar Caribe en Riohacha, y es uno de los afluentes más   impactados por la extracción de carbón.

De acuerdo con el artículo 'Conflictos socio - ambientales por el agua en La Guajira', escrito por Danilo Urrea e Inés Calvo , el proceso de extracción que tiene lugar en la región, se ha realizado sin tener **en cuenta los mecanismos de control ambiental sobre el Río Ranchería pues la mina del Cerrejón** se encuentra asentada en la parte media de la cuenca del Río. Grasas, aceites, combustibles, carbón mineral y nitrato de amonio son vertidos por la empresa según denuncian las comunidades que antes vivían del río.

Pero además, en el año 2010 Conalvias Construcciones entregó la represa El Cercado, que fue inaugurada con bombos y platillos por el gobierno nacional, anunciando que esta abastecería los acueductos comunitarios de **9 municipios,** pero la realidad es otra, **la actividad minera ya absorbió toda el agua de la represa,** y en cambio,  la comunidad se quedó sin el río, uno de los  más importantes de este departamento que recorre los municipios de **Albania, Barrancas, Distracción, Fonseca, Hatonuevo, Maicao, Manaure, Riohacha y San Juan del Cesar.**

Aunque el gobierno prometió además gran generación de energía, la más beneficiada fue la empresa de carbón. El plan de manejo ambiental se incumplió y una vez se llenó el embalse con la biomasa de fondo, la **contaminación fue evidente, y empezó a propagarse enfermedades como el dengue y la leishmaniasis,** según evidencian Urrea y Calvo en su artículo.

El agua de la represa llega hasta cierto punto. A los cultivos de arroz, fincas ganaderas y a la mina de carbón, pero muy poco le llega a las comunidades, debido a que las tuberías no fueron complementadas y el flujo del río solo alcanza hasta ese punto, señalado por las familias afectadas.

En el 2014 la Procuraduría General de la Nación advirtió que la represa El Cercado no se estaba usando para el consumo humano como era en principio el objetivo de dicho proyecto, sin embargo, las comunidades indígenas y específicamente los niños wayúu siguen muriendo de sed.

Pero además, debido a que el agua del río es contaminada diariamente con el polvillo de carbón, los pozos subterráneos de los cuales se abastecen otras comunidades también se contaminan. **Fuentes de agua como Aguas Blancas y Tabaco son ahora usadas para la actividad minera y fuentes arroyos como Bartolico y Araña e' Gato han desaparecido.**

Según datos del PNUD, **el consumo de agua por persona al día en La Guajira es de 0,7 litros, mientras que, de acuerdo con la comunidad, la mina El Cerrejón asegura necesitar diariamente 17 millones de litros extraídos del Río Ranchería** para disminuir el polvo en las vías de transporte.

Aunque es evidente el panorama, la empresa minera sigue convencida de desviar 26 kilómetros el Río Ranchería, y además, ya cuenta con el permiso de la Agencia Nacional de Licencias Ambientales y Corpoguajira para desviar el arroyo Bruno, del cual se abastecen decenas de familias que siguen preguntándose “¿por qué el gobierno sigue dando permisos para desbaratarnos la casa?”.

##### [Entrevista Angélica Ortiz, Fuerza de mujeres Wayúu] 

<iframe src="http://co.ivoox.com/es/player_ej_11082167_2_1.html?data=kpadmpeVepihhpywj5aYaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5ynca7dz8aYxsqPp8Lmw4qwlYqmd8%2BfxcrZjajJttPZy4qwlYqmd8%2Bf1tjOjcnNpdPdwtLS0NnJb5KrjNLWztHTso6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
