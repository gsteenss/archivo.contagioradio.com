Title: El BCE "chantajea" al nuevo gobierno griego
Date: 2015-02-05 21:52
Author: CtgAdm
Category: Economía, El mundo
Tags: BCE congela crédito a bancos de Grecia, Grecia, Syriza, Troika
Slug: el-bce-chantajea-al-nuevo-gobierno-griego
Status: published

###### Foto: Makis Sinodinos 

Al grito de **"Nos somos una colonia de Alemania"**, miles de manifestantes griegos han salido a la calle, para oponerse a la medida del Banco Central Europeo, por la que se suspende el crédito a los bancos griegos.

El **BCE ha dejado de aceptar los bonos griegos como garantía de financiación** para las entidades helenas, dejando así todo el peso financiero sobre el Banco Central de Atenas, con el objetivo de forzar otro rescate. La medida ha provocado la caída en la bolsa de la deuda griega y un total rechazo por parte de sus inversores.

Así la única forma de financiación para las entidades griegas sería el propio Estado, dejando al país en una situación de aislamiento. Con estas acciones queda claro que no va a ser fácil la negociación de la deuda, ya que Alemania es totalmente reacia a dicha solución, igual que gran parte de los banqueros europeos.

Distintas organizaciones sociales han calificado la medida **de "chantaje" de carácter político** y de escollo colonialista para que Syriza pueda cumplir sus promesas electorales.

A todo este panorama político hay que añadir las declaraciones de los analistas del banco Barclays "...el auge de partidos radicales en Europa, como Podemos en España, no deja mucho margen a los políticos europeos para ser indulgentes con las exigencias griegas..."
