Title: Convocan la creación de un Pacto Ciudadano por la paz de Colombia
Date: 2017-05-25 05:07
Category: Nacional, Paz
Tags: FARC, proceso de paz
Slug: pacto_ciudadano_por_la_paz_colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_202-e1495706645670.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [May 24 de 2017] 

En el marco de la incertidumbre que vive Colombia luego de que la Corte Constitucional, tumbara dos de los artículos claves del Acto Legislativo para la paz, lo que generó que las FARC entraran en asamblea permanente en cada una de las Zonas Veredales, la movilización social busca **mecanismos para impedir cualquier paso hacia atrás en el proceso construcción de la paz del país.**

En ese sentido, una de las iniciativas las lidera el Observatorio de Seguimiento a la Implementación del Acuerdo de Paz. Esta [convoca a las personas a firmar una petición a través del](https://www.change.org/p/pacto-ciudadano-para-la-implementaci%C3%B3n-del-acuerdo-de-paz)**[Change.org](https://www.change.org/p/pacto-ciudadano-para-la-implementaci%C3%B3n-del-acuerdo-de-paz), para que se cree un Pacto Ciudadano por la paz del pueblo colombiano.**

La petición habla sobre la obligación que tiene el Estado de adecuar las normas legales y constitucionales al espíritu del Acuerdo. En ese sentido, pide darle seguridad jurídica a los excombatientes de las FARC en transición a la vida civil, "proteger sus vidas, crear una institucionalidad para construir paz y, lo más importante, asegurar la realización de derechos políticos, económicos, sociales y culturales de las poblaciones más vulnerables, especialmente de las comunidades rurales en zonas de la guerra".

### Los incumplimientos 

A su vez, llama la atención sobre los incumplimientos del gobierno colombiano. Aunque el próximo 1 de junio se cumple el día D,  desde **el Observatorio se asegura el cumplimiento en la implementación de los acuerdos difícilmente llega a un 12%**. "Si bien hay avances legislativos y mínimos avances institucionales y administrativos, el panorama no es el mejor".

No obstante, se reconoce el cumplimiento por parte de la guerrilla, cuyos más de siete mil integrantes se concentraron en 26 Zonas Veredales y Puntos de Transición y Normalización e iniciaron, el proceso de desarme y reincorporación en medio de los incumplimientos estatales y el paramilitarismo.

De acuerdo a esa situación, se exige al **presidente, los poderes públicos, el Congreso de la República, Corte Constitucional y demás tribunales del poder judicial, toda la institucionalidad, incluidos gobiernos locales,** a asumir el compromiso de contribuir con el cumplimiento de los acuerdos; asimismo se pide a la comunidad internacional para que reafirme y fortalezca su apoyo a este Acuerdo y su implementación.

Finalmente, se convoca a la sociedad colombiana para que apoye la iniciativa de convocar **un Pacto Ciudadano "que garantice la implementación del Acuerdo Final y asegure un camino de confianzas** para transitar a la terminación del conflicto, con cambios que integren la justicia social, la justicia climática, la democracia profunda y la paz". Y que también acompañe y respalde el proceso de paz entre el Gobierno y el ELN.

###### Reciba toda la información de Contagio Radio en [[su correo]
