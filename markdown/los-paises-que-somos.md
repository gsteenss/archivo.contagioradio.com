Title: Los países que somos
Date: 2016-05-07 12:34
Category: invitado, Opinion, superandianda
Tags: Alvaro Uribe, colombia, gina parody, Peñalosa
Slug: los-paises-que-somos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/bandera-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Agudelo41 

#### **Por [Superandianda](https://archivo.contagioradio.com/superandianda/)- [@superandianda](https://twitter.com/Superandianda)** 

######  7 May 2016 

Entre tantas visiones de país, podríamos decir, que Colombia es un país con muchos países. Somos muchos en uno solo: somos el país de Juan Manuel Santos, el  país de Alejandro Ordoñez, el país de Álvaro Uribe Vélez, el país de los partidos políticos: derecha, centro derecha e izquierda dividida; pero además, somos el país que tiene por país a la  pobreza y a la  injusticia, y aunque llevamos un solo nombre, Colombia, y vivamos todos en el mismo territorio, en los países que somos, cada quien crea su propia patria: la patria que se acomoda al bien individual, la patria que cada uno maneja a su antojo, la patria donde  todos combaten a la corrupción pero todos son corruptos.

[En la variedad de países que somos, personalmente, quisiera vivir en el país de Gina Parody; sin dudarlo. El país donde todos los niños desayunan con leche, crema, quesos, y la educación pública es la excelencia. Definitivamente, Gina Parody,  es la Alicia moderna, y Colombia,  el país de las maravillas, visto desde sus lentes. El país del sombrero loco y el conejo blanco, donde los niños de la Guajira tienen una mesa a su disposición llena de  dulces, galletas y helados de sabores; es imposible morir de hambre. ¡Momento! los cuentos son cuentos, Gina no es Alicia, es la ministra de educación y Colombia no va a ser la maravilla que promete en sus intervenciones. No Gina, eso no pasa en el capitalismo, en el capitalismo la educación pública es mediocre, es pobre y para pobres, y el sentido social que se nombra en los micrófonos, es el mismo que ha dejado morir de hambre a los niños wayuu y que ha robado los pocos recursos de los comedores escolares.]

[Al contrario del país de Gina Parody, no quisiera vivir en el país del procurador, donde la constitución es una camándula, los animales no son personas y las bestias son políticos. El país inspirado en la edad media, donde las brujas son perseguidas, pero no las brujas del centro democrático, no. En la pequeña y limitada patria de Alejandro Ordoñez, todo aquel que no profese obediencia a la aristocracia –digamos que los corruptos son aristócratas- es un traidor y un pecador. El servilismo es ley, se azota a todo aquel que ponga en duda su palabra, que es como la palabra de Dios. Lo más importante, del país de Alejandro Ordoñez, es que se garantiza la vida, sin calidad, pero al fin y al cabo, miserable o no, la vida es vida y es un derecho, tenga o no EPS.]

[No podemos dejar a un lado el país del futuro, el de Enrique Peñalosa, donde cualquier brote de naturaleza es atraso y la inversión social viene en forma de ladrillo y cemento. Llenos de murallas de concreto, los ciudadanos se sienten en Europa, esos feos indigentes y pobres que solo ensombrecen a la ciudad, se eliminan, se esconden, se pavimentan -bien  pueden servir de bulto como bolardos-. Debajo del metro elevado hay cultura, pero no la cultura hip hop, ni la cultura de la señora de los tintos y cigarros, no, la cultura con etiqueta, la cultura con policía no con poesía; sin rastro urbano que ensucie a las paredes con colores. El país de Enrique Peñalosa, algo así como la seguridad democrática para dummies, es una gran empresa privada, lejos de cualquier razón pública y social.  ]

[Pero no debemos quitar a cada quien lo que le pertenece, puede que existan diferentes visiones y propuestas de país, pero el mejor de todos,  lo comparten Juan Manuel Santos y Álvaro Uribe Vélez, aunque no quieran. El país de la guerra y la paz, el cielo y el infierno, la cura y la enfermedad; pero a fin de cuentas el mismo país, mientras uno mata el otro no puede sanar.  Existe una gran diferencia en números de muertos, es cierto, no vamos a pretender igualar el talento que tenía la patria uribista, con sus resultados de botas y cabezas, a la patria que busca la paz, y que quiere dar resultados con desmovilizados, pero si podemos, en cambio, resaltar el número de víctimas que nos sigue dejando la desigualdad, el hambre y la ignorancia; el número considerable, de víctimas del neoliberalismo, que ni los discursos de la guerra y la paz han salvado. Este país,] [diseñado por Álvaro Uribe Vélez, Juan Manuel Santos y todos los partidos políticos y dinastías familiares que tienen a su espalda, señala la misma política: la patria que no es patria, la que se vende, se regala y se arrodilla ante cualquier multinacional. Ya es diferente el estilo de cada uno, donde sus participantes son mafiosos o grandes empresarios.]

[Entre tantas visiones de país, queda pendiente el país de los periodistas y los medios, el país que nos recrea.  ]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
