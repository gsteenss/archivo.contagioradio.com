Title: Las cooperativas agrícolas Italianas del postconflicto colombiano
Date: 2015-06-24 07:42
Category: Abilio, Opinion
Tags: Comisión de Justicia y Paz, Expo Milán, familia Aljure, mapiripan, Palma aceitera, Poligrow, Santos
Slug: las-cooperativas-agricolas-italianas-del-postconflicto-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Captura-de-pantalla-2015-06-24-a-las-7.40.11.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cancilleria 

#### [**Por [Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/) - @abiliopena**] 

###### [24 Jun 2015] 

En la reciente visita a Europa del presidente Santos a Italia, además de reunirse con en papa Francisco quien le recordó que la paz es fruto de la justicia, visitó el pabellón de Colombia en Expo Milán y participó en la VII Conferencia Italia-América Latina y el Caribe, donde se encontró con un grupo de empresarios a quienes les pidió replicar en Colombia el modelo de las cooperativas agrícolas de ese país.

En ese escenario el Presidente Santos recordó la reciente visita de cerca de 80 empresarios que ven en Colombia una oportunidad para la inversión y señaló que “sectores como la agroindustria –con el café, el banano, el aceite de palma y el sector de diseño y confecciones– representan grandes oportunidades” para los dos países.

El gobierno, a través del viceministro de agricultura, promocionó a Colombia como una nación con las puertas abiertas para la inversión agropecuaria y forestal. Ofertó los territorios que aparecieron como casi inexplorados en espera de quién les saque provecho: el país cuenta -dijo- con 22 millones de hectáreas aptas para la agricultura, con 15 millones de hectáreas aptas para la ganadería y con 48 millones de hectáreas aptas para plantaciones forestales. La feria Expo Milán se convirtió así en la vitrina de exhibición de los territorios colombianos para su mercantilización en Italia.

Dentro del grupo de empresarios que presenció las exposiciones, se encontraba el italiano Carlo Vigna, director de Poligrow Colombia, quien figura en la web del pabellón nacional de la feria  como parte de la delegación Colombiana. El empresario opinó que “*Hoy en Colombia hay una subutilización de los suelos que se pueden aprovechar para agricultura, lo cual lo convierte en un escenario propicio para producir el alimento necesario para alimentar el planeta”* *( [http://www.colombiaexpomilan.<wbr></wbr>co/sala-de-prensa/noticias/<wbr></wbr>seminario-de-oportunidades-de-<wbr></wbr>inversion-en-el-agro-<wbr></wbr>colombiano](http://www.colombiaexpomilan.co/sala-de-prensa/noticias/seminario-de-oportunidades-de-inversion-en-el-agro-colombiano)).*

Carlo Vigna y sus socios de Poligrow, sí han sabido aprovechar los suelos de Colombia. Se apropió de manera irregular, saltándose la ley de baldíos, grandes extensiones de tierra en el municipio de Mapiripán, Meta, luego de que, por la masacre de 1997, muchos de los propietarios se hubiesen visto obligados a desocuparlas. De esa apropiación irregular dan cuenta parlamentarios que debatieron sobre el acaparamiento de tierras en la Altillanura, diversas investigaciones periodísticas y dos informes de la Contraloría General de la Nación.

Aparte de la apropiación de baldíos documentada abundantemente, la Comisión de Justicia y Paz, hizo pública la ocupación de hecho adelantada por Poligrow de partes de la finca Santa Ana, de donde fueron despojados miembros de la familia Aljure. Luego de la destrucción de la casa, se instaló a pocos metros un hombre vinculado con las estructuras paramilitares, mientras esas áreas son paseadas por los administradores de la finca a nombre de la compañía, que avanza en la siembra de palma aceitera. Para presionar que desistan de su reclamación de tierras, paramilitares, junto con un abogado que ha prestado sus servicios jurídicos a Poligrow, obligaron a los Aljure a reunirse con ellos para dejarles claro que si seguían reclamando, su vida correría peligro ([http://justiciaypazcolombia.<wbr></wbr>com/El-despojo-palmero-<wbr></wbr>continua](http://justiciaypazcolombia.com/El-despojo-palmero-continua)).

Es difícil suponer que el gobierno desconozca la abundante información que circula sobre los procedimientos contra derecho de Poligrow en Mapiripán, máxime cuando el mismo Incoder, tras las denuncias de los parlamentarios, demandó los procedimientos de la compañía para la adquisición de los predios. Por eso sorprende de un lado que Carlo Vigna aparezca en la página de Expo Milán como parte de la delegación colombiana y que el presidente Santos ante el grupo de empresarios italianos entre los que se encontraba Vigna, propusiera como modelo para el posconflicto las cooperativas agrarias de ese país. Esta posición dista años luz de los acuerdos para el desarrollo agrario integral, alcanzados en al Habana.
