Title: Promover y  respetar el derecho humano al agua, tarea pendiente de Colombia #YoReporto
Date: 2015-03-20 18:17
Author: CtgAdm
Category: yoreporto
Tags: Ambiente y Sociedad, cambio climatico, deforestación, Derechos Humanos, Ginebra, megaproyectos, Naciones Unidas, ONU
Slug: promover-y-respetar-el-derecho-humano-al-agua-tarea-pendiente-de-colombia-yoreporto
Status: published

##### Foto: [stelrenewable.net]

\*Escrito por Milena Bernal ([Asociación Ambiente y Sociedad](http://www.ambienteysociedad.org.co/es/inicio/))  Marzo 19 de 2015

**The Geneva Pledge**

En el Día de los Derechos Humanos, 10 de diciembre, todos los relatores especiales de la ONU y expertos independientes emitieron una declaración conjunta instando a los Estados miembros de la Convención Marco de Naciones Unidas sobre el Cambio Climático para integrar las normas de derechos humanos y los principios en las negociaciones sobre el cambio climático durante la 20ª Conferencia de la Partes, que luego se llevó a cabo en Lima, Perú. Posteriormente al cierre de la plenaria del ADP 2,8 que tuvo lugar del 8 al 13 de febrero en Ginebra, Costa Rica anunció “*El compromiso de Ginebra sobre Derechos humanos en la  acción climática*”. Esta es una iniciativa voluntaria no vinculante destinada a facilitar el intercambio de buenas prácticas y de conocimiento entre los expertos de derechos humanos y de cambio climático a nivel Nacional, según informó la Fundación Mary Robinson en su página de internet.

Dado que “Colombia es un país altamente vulnerable al cambio climático por la ubicación de su población en zonas inundables de la costas y en suelos inestables de las partes altas de las cordilleras, y por presentar una alta recurrencia y magnitud de desastres asociados al clima”[\[1\]](http://www.ambienteysociedad.org.co/es/promover-y-respetar-el-derecho-humano-al-agua-tarea-pendiente-de-colombia/#_ftn1){#_ftnref1} es importante que todas las decisiones, planes y programas que se pretenden desarrollar, apunten no solo a la adaptación y mitigación del cambio climático sino también a la protección de los derechos fundamentales que pueden verse implícitos en el desarrollo de todos los objetivos de gobierno.

**Sobre la protección del Derecho al agua.**

La declaración Universal de los Derechos Humanos[\[2\]](http://www.ambienteysociedad.org.co/es/promover-y-respetar-el-derecho-humano-al-agua-tarea-pendiente-de-colombia/#_ftn2){#_ftnref2}, reconoce en su artículo 25 el derecho que tiene todo ser humano a tener un nivel de vida adecuado. Este derecho desarrolla su reconocimiento, y aplicación en los llamados Derechos Económicos, sociales y Culturales (DESC) como “aquellos derechos humanos relacionados con el lugar de trabajo, la seguridad social, la vida en familia, la participación en la vida cultural y el acceso a la vivienda, la alimentación, el agua, la atención de la salud y la educación”.[\[3\]](http://www.ambienteysociedad.org.co/es/promover-y-respetar-el-derecho-humano-al-agua-tarea-pendiente-de-colombia/#_ftn3){#_ftnref3}

Aun cuando el derecho al agua ha tenido un desarrollo relativamente corto, la discusión internacional sobre si este debe ser reconocido o no como derecho fundamental ha generado varios pronunciamientos por parte de los órganos supranacionales en materia de Derechos Humanos. Así por ejemplo en varias de las observaciones generales del Comité de derechos Económicos Sociales y culturales de la ONU, el derecho al agua se reconoce como un  “derecho humano amparado por el párrafo 1 del artículo 11 del Protocolo de San salvador”[\[4\]](http://www.ambienteysociedad.org.co/es/promover-y-respetar-el-derecho-humano-al-agua-tarea-pendiente-de-colombia/#_ftn4){#_ftnref4} en la “categoría de las garantías indispensables para asegurar un nivel de vida adecuado, en particular porque es una de las condiciones fundamentales para la supervivencia”[\[5\]](http://www.ambienteysociedad.org.co/es/promover-y-respetar-el-derecho-humano-al-agua-tarea-pendiente-de-colombia/#_ftn5){#_ftnref5}

La situación actual del país en torno al incremento en el desarrollo de megaproyectos de generación energética, infraestructura, la explotación minera y la deforestación por cambio en los usos del suelo ya está impactando negativamente la cantidad, calidad y suministro de los recursos hídricos. Así mismo, la falta de regulación clara sobre esta materia ha sido en muchas ocasiones el detonante de fuertes conflictos socio-ambientales ocurridos en el país. Además,  La no inclusión del derecho al agua dentro del rango de derechos fundamentales de la Constitución Nacional es uno de los principales argumentos para que la administración generalmente otorgue prevalencia al crecimiento económico por encima de la protección del derecho al agua.

La necesidad de disminuir las causas y efectos del cambio climático así como la forma en que el patrimonio ecológico y ecosistémico debe ser administrado, es un proceso que requiere de vastos conocimientos, complejos análisis y  decisiones en materia científica que requieren para su implementación un enfoque amplio de derechos humanos, es por esto que el intercambio de experiencias y conocimientos entre los representantes nacionales de la CMNUCC así como con académicos y expertos en derechos humanos a nivel nacional e internacional , podría fortalecer y aumentar el rango de inclusión y protección de derechos en el país.

[\[1\]](http://www.ambienteysociedad.org.co/es/promover-y-respetar-el-derecho-humano-al-agua-tarea-pendiente-de-colombia/#_ftnref1){#_ftn1} **PNUD, EL CAMBIO CLIMÁTICO EN COLOMBIA Y EN EL SISTEMA DE LAS NACIONES UNIDAS.**  Revisión de riesgos y oportunidades asociados al cambio climático.

[\[2\]](http://www.ambienteysociedad.org.co/es/promover-y-respetar-el-derecho-humano-al-agua-tarea-pendiente-de-colombia/#_ftnref2){#_ftn2} [**Declaración Universal de los Derechos Humanos (ONU)**](http://www.un.org/es/documents/udhr/), http://www.un.org/es/documents/udhr/law.shtml

 [\[3\]](http://www.ambienteysociedad.org.co/es/promover-y-respetar-el-derecho-humano-al-agua-tarea-pendiente-de-colombia/#_ftnref3){#_ftn3} **OFICINA DEL ALTO COMISIONADO DE LAS NACIONES UNIDAS PARA LOS DERECHOS HUMANOS**. P*reguntas Frecuentes Sobre Los Derechos Económicos, Sociales Y Culturales*. Folleto informativo 33.

[****\[4\]****](http://www.ambienteysociedad.org.co/es/promover-y-respetar-el-derecho-humano-al-agua-tarea-pendiente-de-colombia/#_ftnref4){#_ftn4} **Véanse los párrafos 5 y 32 de la Observación general Nº 6 (1995) del Comité**, relativa a los derechos económicos, sociales y culturales de las personas mayores.

[\[5\]](http://www.ambienteysociedad.org.co/es/promover-y-respetar-el-derecho-humano-al-agua-tarea-pendiente-de-colombia/#_ftnref5){#_ftn5} **Véase la Observación general Nº 15, El derecho al agua (artículos 11 y 12 del Pacto Internacional de Derechos Económicos, Sociales y Culturales)** 29º período de sesiones (2002) , Disponible en http://www.solidaritat.ub.edu/observatori/general/docugral/ONU\_comentariogeneralagua.pdf
