Title: Ante intentos de frenar la revocatoria de Peñalosa los bogotanos debemos salir a la calle: Carrillo
Date: 2017-09-14 16:40
Category: Movilización, Nacional
Tags: Enrique Peñalosa, revocatoria
Slug: ante-intentos-de-frenar-la-revocatoria-de-penalosa-los-bogotanos-debemos-salir-a-la-calle-carillo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/revoquemos-penalosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [14 Sept 2017] 

Los ciudadanos de Bogotá se encontraran este 14 de septiembre a las 4 de la tarde en la Plaza de Toros la Santa María para movilizarse **exigiéndole al Consejo Nacional Electoral que cumpla con los tiempos de la revocatoria del Alcalde Enrique Peñalosa** y de vía libre a este proceso.

La ponencia del magistrado Emiliano Rivera señala irregularidades en la presentación de las finanzas de los comités de revocatoria, sin embargo, de acuerdo con Carlos Carrillo, este magistrado **“le ha tendido la mano a Andres Villamizar**” director de la Fundación Azul, encargada de la defensa del alcalde de Bogotá. (Le puede interesar: ["En proceso de Revocatoria "no contamos con la mala fe del CNE" Carlos Carrillo"](https://archivo.contagioradio.com/en-proceso-de-revocatoria-no-contamos-con-la-mala-fe-del-cne-carrillo/))

¨Para Carrillo esas relaciones median en una posible decisión del CNE, además, la ponencia presentada por Rivera carece de argumentos **“ellos no pueden tumbar la revocatoria, porque hallan faltado recibos de fotocopias**, realmente las razones para tumbar la revocatoria es que se violen los topes generales o individuales” y eso no sucedió.

El vocero explica que el CNE iguala los préstamos con las donaciones “si Sintratélefonos hubiera querido dar 300 millones de pesos legalmente lo habría podido realizar porque lo que dice la ley es que a **quienes se les aplica el tope personal es a las personas jurídicas y Sintrateléfonos es una agremiación**”.

### **¿Qué pasaría si el CNE tumba la revocatoria?** 

Carlos Carrillo señaló que pese a que el CNE es una institución “desprestigiada” podría afectar la revocatoria y en dado caso existirían dos alternativas. La primera es la movilización social y la segunda son acciones penales, **denunciado un prevaricato por parte de los magistrados del CNE**. (Le puede interesar: ["Revocatoria a Enrique Peñalosa podría realizarse en Noviembre"](https://archivo.contagioradio.com/revocatoria-a-enrique-penalosa-podria-realizarse-en-noviembre/))

También agregó que **“simplemente si la investigación contra los títulos falsos del alcalde se hubieran movido, el alcalde ya no sería alcalde**”, es decir, si se acogieran todos los términos de la ley ya se han dado otras razones para revocar el mandato de Peñalosa en la alcaldía de Bogotá.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
