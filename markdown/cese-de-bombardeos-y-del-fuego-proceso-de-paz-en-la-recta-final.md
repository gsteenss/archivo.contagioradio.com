Title: Cese de bombardeos y del fuego: proceso de paz en la recta final
Date: 2015-07-27 16:11
Category: Nacional, Paz
Tags: bombardeos, Cese al fuego, desescalamiento, paz
Slug: cese-de-bombardeos-y-del-fuego-proceso-de-paz-en-la-recta-final
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Colombia_drogas_WEB.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Colombiano 

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Luis-Eduardo-Celis.mp3"\]\[/audio\]

##### [Luis Eduardo Celis, analista] 

###### [27 jul 2015] 

La declaración conjunta por parte del gobierno y las FARC el pasado 12 de julio, en que anunciaron que buscarían desescalar el conflicto y acelerar las conversaciones de paz en La Habana, se ha hecho efectiva en menos de una semana con los gestos de cese al fuego unilateral por parte de la insurgencia el 20 de julio, y cese a los bombardeos por parte del Gobierno el 25 del mismo mes.

Para el analista e investigador de la Corporación Arcoiris, Luis Eduardo Celis, esto demuestra que Colombia está viviendo la maduración de un proceso de paz que está en su recta final.

<div>

"La medida del presidente Santos del sábado, es una medida importante, igualmente lo que ha dicho el Ministro de Defensa, Luis Carlos Villegas, sobre evaluar nuevas medidas para disminuir la intensidad del conflicto", afirmó Celis.

Estas medidas podrían tener que ver, según el analista, con el cese de operaciones ofensivas terrestres por parte del Ejercito, teniendo en cuenta que las FARC ya se encuentran en esa tónica; o con la ubicación de nuevas zonas para el desminado.

Estos pasos que se dan en el desescalamiento del conflicto, sin embargo, no significan aún el cese bilateral del fuego. Celis insiste en la necesidad de avanzar en la concreción de una mesa de diálogos con el ELN, que continúa sus acciones ofensivas en el país, para que pueda concretarse prontamente un cese bilateral que pueda ser verificado por la comunidad nacional e internacional.

</div>
