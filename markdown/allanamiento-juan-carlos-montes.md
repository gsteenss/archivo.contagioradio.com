Title: Denuncian allanamiento a residencia de Juan Carlos Montes
Date: 2018-12-26 12:50
Author: AdminContagio
Category: Judicial
Tags: Fiscalía, juan carlos montes, Petro
Slug: allanamiento-juan-carlos-montes
Status: published

###### [Foto: Noticias Uno] 

###### [26 Dic 2018] 

Este miércoles trascendió que la residencia de Juan Carlos Montes, quien aparece en el video en que Gustavo Petro recibía dinero, fue objeto de allanamiento por parte del CTI de la Fiscalía. Montes quien fue interrogado  en los primeros días del mes de diciembre, ha denunciado que su familia ha sido objeto de amenazas vía telefónica y personales, así como interceptación de sus comunicaciones.

En desarrollo...

 
