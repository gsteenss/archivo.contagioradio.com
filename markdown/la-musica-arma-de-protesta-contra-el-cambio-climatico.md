Title: La música, arma de protesta contra el cambio climático
Date: 2015-09-21 11:21
Category: Cultura, eventos
Tags: Alerta Kamarada, Aterciopelados, Bunny Wailer, Concierto 22 de septiembre, Concierto contra el cambio climático, Doctor Krápula, Encuentro de las Américas Frente al Cambio Climático, Jorge Drexler, Malalma, Mitú
Slug: la-musica-arma-de-protesta-contra-el-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/atercio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nuenter.com 

###### 21 sep 2015 

En el marco del “Encuentro de las Américas frente al cambio climático”, a desarrollarse este martes 22 de septiembre, cientos de ciudadanos y ciudadanas alzaran su voz al ritmo de la música, con un concierto preparado especialmente para la ocasión.

Desde las 12 del medio día, subirán al escenario dispuesto en la centrica Plaza de Bolívar de Bogotá, las agrupaciones: Aterciopelados, Alerta Kamarada, Doctor Krápula, Bunny Wailer y la presentación del cantautor uruguayo Jorge Drexler.

Los grupos y solistas invitados, han demostrado en multiples oportunidades su compromiso con el medio ambiente, utilizando su música como herramienta para dejar un mensaje de conciencia ambiental en diferentes escenarios nacionales e internacionales.

De manera paralela, las agrupaciones Malalma y Mitú se presentarán en el Centro de Memoria Paz y Reconciliación  y en el Parque Nacional respectivamente.

**Los Aterciopelados:**

La agrupación de rock bogotana, conformada a finales de los años 90, ha apoyado a Greenpeace en las campañas realizadas en Colombia. Se presentaron en el COP20 de Lima en el 2014, además de sus acciones individuales por el planeta.

Canciones como Canción protesta, Río, Tréboles, baracunatana, son parte de su repertorio en temas medio ambientales.

<iframe src="https://www.youtube.com/embed/Ax9KWBeUmPs" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Doctor Krápula:**

El compromiso de este grupo de ska-rock bogotano ha sido inmenso con el planeta, tanto así que son parte del “Colectivo Jaguar”, desde el que se ha abanderado de la defensa del Amazonas, acercando a las personas la problemática del pulmón del mundo.

Han dirigido su música hacia la conciencia, movilización y acción contra el cambio climático.

<iframe src="https://www.youtube.com/embed/lVCiiNLclGY" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###  **Alerta Kamarada:** 

Desde 1996 ha logrado posicionarse en la escena del reggae colombiano, mezclando distintos sonidos que han logrado enriquecer su música. Alerta Kamarada siempre ha buscado generar conciencia frente al calentamiento global, colaborando en campañas del Greenpeace a nivel nacional.

Canciones como Libera tu alma, Sueño real, Tiempos positivos, Legal, De donde viene la cumbia y Somos uno, son algunas con las cuales nos podrían invitar a reflexionar sobre el calentamiento global.

<iframe src="https://www.youtube.com/embed/4sPc-yMAE-0" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Jorge Drexler:**

El cantautor y músico uruguayo ha estado también comprometido con el clima, durante 2008 participó en un concierto organizado en España con motivo de la celebración de “La hora del planeta”,  que tiene lugar todos los 27 de marzo.

Algunas de sus canciones son Disneylandia, Todo se transforma, Madre tierra, La edad del cielo y Mi guitarra y voz.

<iframe src="https://www.youtube.com/embed/8HHZSq07tt4" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Bunny Wailer:**

Este artista jamaiquino es uno de los tres miembros fundadores de The Wailers (1963), agrupación de la que también hizo parte el mítico Bob Marley. En 1967 abandonó el grupo e inició su carrera como solista, la cual hasta el día de hoy guarda la esencia del reggae roots.

Este año Wailer viene de presentarse del Rototm Sunplash (España), el cual tuvo como eje principal el cambio climático, con canciones como Armageddon, Fig tree, Here in Jamaica, Educated fools.

<iframe src="https://www.youtube.com/embed/z3cd-De3q6c" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
