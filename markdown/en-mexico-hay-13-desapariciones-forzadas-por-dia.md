Title: En México hay 13 desapariciones forzadas por día
Date: 2015-02-10 19:24
Author: CtgAdm
Category: DDHH, El mundo
Tags: Ayotzinapa, cerezo, DDHH, Derechos Humanos, desaparecidos, desaparición, enrique, mexico, nieto, peña
Slug: en-mexico-hay-13-desapariciones-forzadas-por-dia
Status: published

###### Foto: El Toque 

<iframe src="http://www.ivoox.com/player_ek_4064654_2_1.html?data=lZWjlpuZeI6ZmKiakpuJd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BfroqwlYqlfdndxNSYysbdb5KnjMnS1cbUpdPdxM7c0MrXb8fj09%2FOxsbXb9Hj05DRh6iXaaK4wpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejandro Cerezo, Defensor de DDHH] 

Así lo determinó la investigación adelantada por la **Maestría en Periodismo y Asuntos Públicos del CIDE**. Según el informe presentado, **entre el año 2007 y el 2014 se presentaron 23.272** casos de desaparición forzada, de los cuales el **40% (9.384) corresponden a los 2 años que Enrique Peña Nieto** ha presidido el gobierno del país centroamericano.

Para el defensor de Derechos Humanos, Alejandro Cerezo, la desaparición forzada es una estrategia dirigida por parte de estructuras paramilitares en complicidad con el Estado. El 40% del total de personas desaparecidas son **jóvenes entre los 15 y los 29 años de edad**, y se presenta por un parte, a manera de reclutamiento forzado para obligar a los y las jóvenes a realizar trabajos en formas de esclavitud moderna, y en los últimos años, por motivos políticos, contra personas que hacen parte de organizaciones sociales y que luchan en contra de la implementación de megaproyectos en el país.

Entre **junio del 2014 y febrero del 2015 se han registrado en México cerca de 80 personas desaparecidas por motivos políticos.**

Las organizaciones de Derechos Humanos buscan que en México exista un la ley general que tipifique la desaparición forzada como un delito y una violación a los Derechos Humanos, para dar un tratamiento efectivo a esta situación.
