Title: Asesinan a Julían Areiza, sobrino de líder del Movimiento Ríos Vivos
Date: 2018-09-24 14:50
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: HodroItuango, lideres sociales, Rios Vivos
Slug: asesinan-julian-areiza-rios-vivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/unnamed.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [24 Sep 2018] 

Según la denuncia del Movimiento Ríos Vivos en Antioquia, **el 23 de septiembre fue asesinado Julián de Jesús Areiza Moreno**, sobrino de **Ruben Areiza**, integrante del movimiento y líder comunitario del municipio de Briceño. Los hechos ocurrieron en la **vereda Altos de Chirí, **por un grupo de hombres armados que acabaron con la vida de Jesús y dejaron a un joven más herido.

Una de las principales preocupaciones es la cercanía de la **policía que se encuentra a tan solo 10 minutos del sitio en que se presentó el asesinato y que se habría negado a atender la denuncia** que de inmediato realizaron las víctimas, con el argumento de que podría tratarse de una emboscada, aseguró el líder social que realizó la llamada de auxilio.

Adicionalmente, Isabel Zuleta, una de las voceras del movimiento, aseguró que **también se intentó pedir la intervención del ejército, sin embargo, la ayuda se demoró en llegar varias horas al lugar**. Así mismo se comunicó del hecho a las demás autoridades civiles del municipio, sin que la atención haya sido oportuna.

### **Hay un patrón para presionar la renuncia de líderes de Rios Vivos** 

Sin embargo la situación es más preocupante, dado que los ataques a las familias de los líderes del movimiento se han ido incrementando **“son mucho más aterradores los ataques a las familias que incluso los ataque directos a los líderes”** asegura Zuleta, añadiendo que la semana pasada fueron asesinados dos familiares de la señora Cecilia, lideresa del municipio de Valdivia y el hijo de Juan de Dios Ramírez quien ya había denunciado amenazas en su contra.

Estas situaciones estarían reflejando un patrón en que se presiona a los líderes para que renuncien el movimiento de defensa de la tierra y que está aterrorizando a los integrantes, algunos de los cuales ya se vieron obligados a renunciar a su labor social.

A esta situación se suman las amenazas e intimidaciones por parte de la empresa EPM, funcionarios de gobierno y otras entidades en contra de las personas víctimas de las irregularidades de HidroItuango. Lea también: D[enuncian intimidaciones por parte de EPM](https://archivo.contagioradio.com/rios-vivos-denuncia-ituango/)

### **Es necesario investigar y castigar pero debe cesar la estigmatización** 

Según Zuleta, **la fiscalía debe investigar a fondo los ataques a los familiares de los y las integrantes del movimiento**. Así mismo piden la retractación de la policía y del gobernador de Antioquia quienes han afirmado que las víctimas del Mega proyecto están extorsionando y que los líderes asesinados no son parte del movimiento.

En las últimas semanas **ya serian 8 las víctimas de la violencia contra Ríos Vivos**, aunque no se trate directamente de líderes del movimiento sino sus familiares cercanos.

<iframe id="audio_28822786" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28822786_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
