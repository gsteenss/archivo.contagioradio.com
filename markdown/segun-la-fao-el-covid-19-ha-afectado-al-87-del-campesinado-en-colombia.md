Title: Según la FAO el COVID 19 ha afectado al 87% del campesinado en Colombia
Date: 2020-06-20 14:59
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: agricultura campesina, campesinos, colombia, FAO, ods
Slug: segun-la-fao-el-covid-19-ha-afectado-al-87-del-campesinado-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Campesinado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Foto: Campesinado/ FAO

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Académicos del sector de la agronomía consideran que desde la política pública del Gobierno siempre ha existido en Colombia un desconocimiento y aislamiento hacia el campesino debido al modelo de desarrollo rural que ha existido en el país desde mediados del siglo XX y cuyo único interés ha sido el de aumentar la producción del campo usando como excusa la reducción de la hambruna y el aumento de la productividad sin tener en cuenta los requerimientos de los agricultores y zonas rurales, revelando una deuda histórica con el campesinado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Fabio Pachón, profesor de la facultad de las ciencias agrarias de la Universidad Nacional concluye que las instituciones responsables del desarrollo del agro en el país no están interesadas en solucionar el problema, ejemplo de ello son organizaciones como el Instituto Colombiano de Desarrollo Rural (INCODER), creado en 2003 y liquidado en 2016 por corrupción y que precedió a las actuales agencias para el desarrollo rural y que por el contrario, se abordan políticas que permitan una disponibilidad alimentaria, sin importar de dónde venga la comida y si existe un desarrollo real en el campo .

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese al argumento que usan las instituciones de potenciar el campo para reducir el hambre en el mundo y cumplir con los denominados Objetivos de Desarrollo Sostenible, ni se ha cumplido con esta meta, ni se ha invertido en el campo. El Centro ODS para América Latina y el Caribe de la Universidad de los Andes, concluyó que en Latinoamérica solo ha avanzado en un 1% desde 2015 y que de seguir esta tendencia, no se alcanzaría la meta ni siquiera en los próximos 50 años, ocupando Colombia la novena posición en este ranking.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Se cierran las plazas se abren los almacenes de cadena

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito del dia sin IVA decretado por el Gobierno en el que miles de personas salieron pese a las medidas de confinamiento del Covid-19, el profesor se refirió a la paradoja que existe con relación a cómo las autoridades prohíben la movilización en las calles y cómo a su vez se han cerrado lugares como Corabastos o plazas de mercado por prevenir las aglomeraciones, pero sí se tolera las compras en almacenes de cadena, **"porque la comida no es importante y quien la produce tampoco, se desconoce la importancia del campesinado en términos nutricionales y alimentarios para la sociedad".** [(Le puede interesar: En Colombia una comunidad campesina ha luchado más de 40 años por el derecho a la tierra)](https://archivo.contagioradio.com/el-garzal-una-comunidad-campesina-ha-luchado-mas-de-40-anos-por-el-derecho-a-la-tierra/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En lugares como la Centras de Abastos, Corabastos, lugar en el que a diario llegan entre 10.000 y 12.000 toneladas de alimento y donde se conocieron casos de Covid-19 y se tuvo que reducir su actividad a un 35% de su capacidad estableciendo horarios de "pico y puesto", durante los primeros días de junio llegaron a perderse al menos 300 bultos de diversos productos como cebolla, arveja, zanahoria y papa que permanecían represados en 350 camiones que no habían podido descargar los alimentos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras esta situación se presenta en las ciudades, en el campo tampoco existen garantías, según la [Organización de las Naciones Unidas para la Alimentación y la Agricultura en Colombia (FAO por sus siglas en inglés)](https://twitter.com/FAO_Colombia) el 87% de los productores consultados manifiestan que han tenido dificultades en las ventas de sus productos agrícolas en 20 departamentos del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma, el pasado 2 de junio la Procuraduría manifestó que el 89% de la población rural del país se encuentra en condiciones de pobreza y mientras en el territorio nacional existen 22 millones de hectáreas aptas para la agricultura, sólo se trabajan 6 millones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante este panorama, el profesor señala que dificilmente se va a priorizar a los campesinos en una "sociedad inmediatista que no ve a largo plazo, en términos de política publica", tomando como ejemplo el Acuerdo de Paz, señalado que después de su implementación se configuró un escenario similar al de la mitad del siglo XX cuando en Colombia se crearon instituciones que buscaban potenciar el campo y cuyos resultados no se ven en la actualidad. [(Lea también: ¿Plan de Desarrollo Nacional de Duque se olvidó de la paz?)](https://archivo.contagioradio.com/plan-de-desarrollo-olvido-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Volvimos a hacer lo mismo que se hizo hace 60 años, se crearon 8 agencias que son las mismas de hace 60 años y que van a hacer exactamente lo mismo de forma poco exitosa, la situación no ha cambiado, seguimos en un circulo vicioso en el que no se valora al campesinado",** afirma con relación a instituciones como la Agencia Nacional de Tierras o la Agencia para el Desarrollo Rural que a propósito fueron desfinanciadas en el último año. [(Lea también: Recortes presupuestales frenan la posibilidad de una Reforma Rural Integral)](https://archivo.contagioradio.com/recortes-presupuestales-frenan-la-posibilidad-de-una-reforma-rural-integral/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La propuesta del campesinado, la Soberanía alimentaria

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a lo desolador del panorama, el profesor señala que hay que darle importancia a propuestas que han surgido desde el campo como la soberanía alimentaria que busca que se distribuyan los alimentos de una forma en que se fortalezcan los mercados locales y las condiciones sociales de los campesinos que buscan que sus productos se distribuyan con precios justos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El profesor Pachón advierte que no se debe confundir la soberanía alimentaria, una idea que ha surgido del campo con la seguridad alimentaria, una propuesta que proviene de las instituciones, **"la soberanía plantea que revaloricemos al campesinado y le demos el valor que debe tener, no necesariamente económico, pone en el centro, a la mujer y el conocimiento ancestral".**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
