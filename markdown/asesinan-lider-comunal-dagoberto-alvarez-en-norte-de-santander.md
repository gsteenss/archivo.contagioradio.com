Title: Asesinan líder comunal Dagoberto Álvarez en Norte de Santander
Date: 2019-06-01 18:34
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: juntas de acción comunal, lideres sociales, Norte de Santander
Slug: asesinan-lider-comunal-dagoberto-alvarez-en-norte-de-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/eurodiputados-lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

En la madrugada de ese sábado fue reportado el asesinato de **líder comunal Dagoberto Álvarez**, quien se desempeñaba como tesorero de la Junta de Acción Comunal de la **vereda los Cacaos, en el municipio de Playa de Belén**, Norte de Santander.

Según el reporte, el hombre de 36 años fue sorprendido a tempranas horas de este sábado en su vivienda, por **hombres armados que lo asesinaron**.

El reporte de la policía fue de un **homicidio por arma de fuego sin determinar los responsables**, mientras que la comunidad espera obtener información que permita establecer si sobre Álvarez existían amenazas en contra de su vida.

###### Reciba toda la información de Contagio Radio en [[su correo]

 
