Title: Empresario ganadero intenta despojar violentamente a familia en Curvaradó, Chocó
Date: 2018-09-11 16:31
Author: AdminContagio
Category: DDHH, Nacional
Tags: Chocó, Curvarado, tierras
Slug: empresario-ganadero-intenta-despojar-violentamente-a-familia-en-curvarado-choco
Status: published

###### [Foto: Archivo Contagio Radio] 

###### [11 Sept 2018] 

La Comisión de Justicia y Paz denunció las amenazas y hostigamientos por parte del empresario ganadero Darío Montoya, sobre la familia Martínez, víctima del despojo de tierras, para intentar una ocupación ilegal del territorio, ubicado en la Cuenca del Curvaradó, Choco.

La familia Martínez es la propietaria legitima del predio, que gracias a la sentencia T 025 de 2004, le fueron devueltas el pasado mes de agosto. **Ellos habían salido desplazados forzadamente de este territorio desde hace 22 años**, producto de la operación paramilitares en esta zona.Posteriormente, la Comisión de Justicia y Paz señaló que las tierras fueron ocupadas por el empresario Darío Montoya que ejerce la ganadería extensiva. (Le puede interesar: ["Lucha entre cifras y hechos, los resultados de la restitución de tierras"](https://archivo.contagioradio.com/asi-trabaja-unidad-restitucion-tierras/))

### **Las reiteradas amenazas a la familia Martínez** 

De acuerdo con las denuncias de la Comisión, desde los primeros días del mes de septiembre se han venido presentando hecho de hostigamiento y amenazas en contra de la familia Martínez y **provenientes de trabajadores de Darío Montoya, con la intención de que los mismos salgan del territorio**.

La primera de ellas ocurrió el pasado 5 de septiembre, sobre las 12:45 pm, cuando el empresario Darío  Montoya junto con sus trabajadores llegaron al lugar en donde se hospedaba la familia y los amenazaron con expulsarlos del territorio si no les presentaban los títulos de propiedad.

Posteriormente, el 6 de septiembre, sobre las 11pm, trabajadores de Montoya, ocupantes de mala fe, irrumpieron en el predio quemando el techo de la vivienda y una lona que se encontraba cerca de allí. Finalmente el 9 de septiembre, sobre las 10:00pm **se escucharon disparos y sonidos de moto sierras en predios colindantes a los de la familia Martínez**.

Según la Comisión "la ausencia de medidas eficaces de protección para que los legales y legítimos dueños disfruten y usen el territorio es evidente", hecho al que se le suma que a pesar de que se activen las alarmas tempranas, el gobierno continúe sin tomar medidas frente a estos hechos.

<iframe id="audio_28499865" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28499865_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
