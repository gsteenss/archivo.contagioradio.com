Title: Premio nobel es un "espaldarazo" al proceso de Paz
Date: 2016-10-07 18:46
Category: Nacional, Paz
Tags: Acuerdos de paz de la Habana, FARC, INDEPAZ, Nobel de Paz
Slug: premio-nobel-es-un-espaldarazo-al-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Camilo-Gonzalez-Posso-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: cyberspaceandtime] 

###### [07 Oct 2016]

El premio nobel de paz representa un espaldarazo al presidente Santos luego de los resultados del plebiscito, una espaldarazo al proceso de paz y también es una alerta de que no puede parar, que debe seguir avanzando afirma Camilo González, y agrega que el **[comunicado conjunto](https://archivo.contagioradio.com/farc-y-gobierno-discutiran-propuestas-de-promotores-del-no/) del equipo de paz del gobierno y de las FARC ratifica el respaldo** que ello representa, además de cerrar el espacio para que el uribismo se convierta en interlocutor en ese espacio de conversaciones.

El director de INDEPAZ explica que seguramente la metodología será que resulte un texto ajustado sin que se pierdan los grandes avances, para someterlos a una segunda refrendación que puede ser plebiscito o refrendación del congreso. Sin embargo resalta que el comunicado es claro porque reafirma que la mesa es entre el gobierno y las FARC y **no da la oportunidad de que los promotores del “NO” [tengan potestad de plenipotenciarios](https://archivo.contagioradio.com/uribismo-pretende-imponer-y-no-negociar-en-el-llamado-pacto-nacional/)**[.](https://archivo.contagioradio.com/uribismo-pretende-imponer-y-no-negociar-en-el-llamado-pacto-nacional/)

Esta posibilidad sería “una caja de pandora” puesto que los promotores del NO nunca inscribieron una serie de puntos y dejaron abierta cualquier posibilidad de oposición. Además la estrategia de campaña revelada por Juan Carlos Vélez Uribe demuestra que el **Centro Democrático tiene una apuesta de liquidación del [acuerdo](https://archivo.contagioradio.com/acuerdo-final-de-paz-entre-gobierno-y-farc/) en todos sus aspectos**. En últimas, un mecanismo que podría servir sería la organización de un Foro parecido a los que ha organizado la Universidad Nacional en el marco de las conversaciones de paz.

Según Camilo González la estrategia de Uribe es clara para dilatar, en pocas palabras que las FARC se encierren en campos de concentración “y ahí vamos hablando”, pero **no se puede repetir la experiencia que se dio con el ELN en unos primeros acercamientos con esa guerrilla en la Habana**. “El gobierno tiene que saber que no hay posibilidad de acuerdo con Uribe” porque este dijo que está en contra de todo el acuerdo y quiere “destruir toda[posibilidad de acción negociada](https://archivo.contagioradio.com/implementacion-de-acuerdos-de-paz-del-terreno-juridico-al-politico/)”.

Por último analiza la posibilidad de que se repita el plebiscito, no con el mismo acuerdo pero si con los ajustes que se puedan hacer sin modificar el grueso del acuerdo en que las víctimas son el centro, “de todas maneras **tiene que haber una forma de refrendación popular que puede ser un plebiscito o a través del congreso**”, sobre los cabildos Posso afirma que un mecanismo de deliberación válidos pero dispendiosos.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
