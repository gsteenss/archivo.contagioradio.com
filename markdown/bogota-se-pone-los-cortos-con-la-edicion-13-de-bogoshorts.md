Title: Bogotá se pone los "Cortos" con la edición 13 de "Bogoshorts"
Date: 2015-12-10 19:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Bogoshorts 2015, Cine Tonalá, Cinemateca Distrital, Felipe Montoya Bogoshorts, Invitro visual
Slug: bogota-se-pone-los-cortos-con-la-edicion-13-de-bogoshorts
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/festivaldecortosbogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_9661030_2_1.html?data=mpujk5WXdI6ZmKiak5WJd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDb0NmSpZiJhZKf1MqY0tTSqYzg0NiYh5eWh9Dm1dTgh5eWb8bijNHOjcrIrcTdhqigh6eXsoyllJDRx5Cms8jj1JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Felipe Montoya, Coordinador de programación] 

##### [10 Dic 2015]

Con la bendición de "Santa Lucía" patrona y galardón del evento y con Alicia en el país de las maravillas como temática central, vuelve a la capital Bogoshorts, el Festival de cortometrajes que en su 13 edición trae una selección de más de 300 producciones nacionales e internacionales en 15 espacios de exhibición de la ciudad.

Desde su primer año (2003) el evento se ha configurado como una fiesta cinematográfica con actividades que se desarrollan durante todo el año y que encuentran su máxima expresión durante la semana del Festival que incialmente se conocia como "Invitro visual" que servía además como vitrina para los nuevos realizadores de Colombia y el mundo.

Felipe Montoya, coordinador de progamación de Bogoshorts, destaca la inclusión para esta edición de Bugs Bunny, el clásico personaje de Warner Brothers en su aniversario 75 , con la proyección del primer cortometraje en que apareció en el cine; así como una muestra especial por la conmemoración de los 100 años de la publicación de "La Metamorfosis de Franz Kafka" y cortos y actividades relacionados a la Teoría de la relatividad de Einstein.

<iframe src="https://www.youtube.com/embed/N9S4XZN9gCg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[[Serán 34 secciones entre competitivas y fuera de competencia, 47 programas, 350 cortometrajes provenientes de 52 países, en 67 horas de programación, las que podrán disfrutar las y los bogotanos, la mayoría de estas de carácter gratuito y otras a muy bajo costo desde el 9 hasta el 21 de Diciembre.]]

http://issuu.com/movimientobogoshorts/docs/programa13bsff?e=0/31845177
