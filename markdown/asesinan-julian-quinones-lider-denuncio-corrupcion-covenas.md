Title: Asesinan a Julián Quiñones, líder que denunció hechos de corrupción en Coveñas
Date: 2019-06-08 23:58
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Coveñas, Julían Quiñones, Lider social
Slug: asesinan-julian-quinones-lider-denuncio-corrupcion-covenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Fotos-editadas-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

El pasado jueves 6 de junio fue asesinado **Julián Quiñones Uñate, líder comunal de Coveñas, Sucre,** que había denunciado corrupción en la construcción del polideportivo en su municipio. Quiñones era presidente de la Junta de Acción Comunal (JAC) del barrio Guayabetal Etapa I, pero se desconocía si había amenazas en su contra por sus denuncias sobre corrupción. (Le puede interesar: ["Asesinan a excombatiente de FARC, Jorge Enrique Sepúlveda en Córdoba"](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-farc-jorge-enrique-sepulveda-en-cordoba/))

El líder fue asesinado el jueves en horas de la mañana por hombres en moto que le propinaron varios disparos que le causaron la muerte, en momentos en que se dirigía para la casa de un familiar en el barrio Guayabetal. Con Julián Quiñones, la **cifra de líderes asesinados en lo corrido de 2019 asciende a 67**, siendo el primero en Sucre; mientras que en todo el **Gobierno Duque se han reportado un total de 174 defensores y defensoras de derechos humanos asesinados.**

Aunque la Policía del municipio realizó un consejo de seguridad extraordinario y se dijo que se haría lo posible para dar con los autores de los hechos, urge tomar medidas de fondo para brindar garantías a la labor de los liderazgos sociales que denuncian hechos como la corrupción, y hacen parte de procesos organizativos de base como las JAC. (Le puede interesar: ["Reportan asesinato del campesino Alneiro Guarín en Puerto Asís, Putumayo"](https://archivo.contagioradio.com/reportan-asesinato-del-campesino-alneiro-guarin-en-puerto-asis-putumayo/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
