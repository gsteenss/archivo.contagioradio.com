Title: Se conmemora el dia nacional de las víctimas del genocidio contra la UP
Date: 2016-10-11 16:38
Category: DDHH, Nacional
Tags: Corporación Reiniciar, Genocidio UP, Jaime Pardo Leal, Unión Patriótica
Slug: se-conmemora-el-dia-nacional-de-las-victimas-del-genocidio-contra-la-up
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/union-patriotica-dia-de-las-victimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 11 Oct 2016 

Desde hace 11 años, cada 11 de octubre se conmemora el **Día Nacional por la Dignidad de las Víctimas del Genocidio Contra la Unión Patriótica**, una iniciativa de memoria liderada por la Corporación REINICIAR y la Coordinación Nacional de Víctimas y Familiares de los más de **6000 militantes del movimiento, asesinados y desaparecidos de manera sistemática**.

La fecha recuerda los **30 años que se cumplen del primer Congreso Nacional de la UP**, en el que se proponía una alternativa política y democrática, distanciada de los poderes tradicionales, mismos que estuvieron vínculados en su exterminio, como lo fue el caso de **Jaime Pardo Leal, asesinado en la vereda "Patio bonito" del municipio de Tena, Cundinamarca el 11 de Octubre de 1987**, tan solo un año después de instalado en Congreso.

La necesidad de honrar la memoria y reivindicar el legado de todos y cada uno de los caídos en el país por cuenta del genocidio, reune desde hace once años a los familiares provenientes de diferentes regiones del país, en un **Encuentro Conmemorativo**, que para este año tendrá como marco los procesos de paz que actualmente se desarrollan con las FARC y el que iniciará su fase pública en Quito el 27 de Octubre con el ELN.

Para este año, edición XI del Encuentro Conmemorativo, se realizarán  varias actividades, que inician el próximo **jueves 20 de octubre** con una marcha que partirá a las 11:00 a.m. desde la Torre Colpatria por toda la carrera séptima hasta la Plaza de Bolívar de Bogotá. **El viernes 21**, continua la programación con la instalación oficial del encuentro, que tendrá lugar en el Centro de Convenciones Gonzalo Jiménez de Quesada a partir de las 9:00 a.m, actividad que se extenderá hasta el sábado 22 en horas de la tarde.

######  Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
