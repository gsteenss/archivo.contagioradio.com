Title: Las últimas tareas de Obama según Michael Moore
Date: 2017-01-16 12:33
Category: El mundo
Tags: Barack Obama, Irak, Michael Moore
Slug: las-ultimas-tareas-obama-segun-michael-moore
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/michael-moore.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Digital Trends] 

###### [16 Enero 2017] 

El documentalista Michael Moore envió una carta a Barack Obama, donde agradece sus 8 años de gobierno y hace 5 peticiones, que se resuelva el problema de aguas contaminadas en Flint, Michigan, que se liberen a algunos presos como Chelsea Manning y el líder nativo americano Leonard Peltier, que ponga fin a la criminalización de consumidores de marihuana y por último, que pida perdón a todas las familias del mundo que se han visto afectadas por las acciones de Estados Unidos.

“Con tan poco tiempo restante, espero no molestar si le pido un favor o dos. Bueno, en realidad cinco. Puedo garantizarle que no estoy solo en estas peticiones; de hecho, **adivino que millones de nuestros compatriotas estadounidenses comparten estas sinceras solicitudes hacia usted”**, es el mensaje con el que Moore inicia la misiva.

En primer lugar, Moore solicita que Obama envíe al Cuerpo de Ingenieros del Ejército a Flint, Michigan, pues el agua contaminada en esta zona está “envenenando lentamente” a la población, **“cada niño en Flint tiene ahora alguna forma de daño a su desarrollo cerebral** (…) por favor, no deje el cargo sin enviar efectivos a salvar esas 100 mil vidas”.

Luego, pide que se libere a la ex elemento del ejército Chelsea Manning, **quien dio a conocer al público estadunidense la verdad detrás de la guerra de Irak**, por lo que fue condenada a 35 años de prisión, y al líder nativo americano Leonard Peltier, **quien lleva 40 años en prisión luego de ser condenado por participar en el plantón de Wounded Knee, en 1977.**

Insta a Obama a **declarar el final de la "guerra a las drogas"**, “decenas de miles de estadounidenses han visto arruinadas sus vidas al haber sido arrojados a la cárcel por usar, poseer o vender mariguana”. El documentalista señala que es fundamental “dar fin a la prohibición federal contra la mariguana y retirarla de su actual designación como droga ilegal de nivel uno” e **indultar a los usuarios no violentos que actualmente se encuentran recluidos.**

Además, invita a Obama a **ofrecer “algo de humildad, redención y perdón” mediante un acto simbólico** a “las familias de civiles muertos por nuestros drones; a nuestros nativos americanos por el genocidio cometido contra ellos por nuestros antepasados; al pueblo de Irak por invadir su país y destruir su sociedad; a las mujeres estadounidenses porque de manera permanente y hasta hoy reciben un pago inferior por sus servicios y a la comunidad afroestadounidense en general”.

Moore denuncia en la misiva que en Estados Unidos las mujeres viven en constante peligro de violencia, **tienen poco poder en la democracia y la población afro continúa “sufriendo el legado del esclavismo y el racismo**, ocupa aún el peldaño más bajo de la escala económica, siguen siendo rehenes de nuestro sistema de justicia” y violentados por agentes de la ley.

Por último Michael Moore cierra la carta pidiendo a Obama que “use el púlpito –y el poder– que aún tiene para **ofrecer alguna cura, alguna libertad, algún perdón y un poco de agua limpia para una ciudad que ha esperado tres años** para beber siquiera un vaso de ella”.

[Mis Peticiones Finales a Obama-](https://www.scribd.com/document/336704066/Mis-Peticiones-Finales-a-Obama-Michael-Moore#from_embed "View Mis Peticiones Finales a Obama-Michael Moore on Scribd")Michael Moore by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_65592" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/336704066/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-kJboXqTYxEkRY5iZEFid&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
