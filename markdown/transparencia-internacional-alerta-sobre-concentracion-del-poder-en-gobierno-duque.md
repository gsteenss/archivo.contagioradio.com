Title: Transparencia Internacional alerta sobre concentración del poder en gobierno Duque
Date: 2020-09-03 19:27
Author: AdminContagio
Category: Actualidad, Política
Tags: Congreso de la República, Defensoría del Pueblo, Fiscalía, Gobierno Duque, Procuraduría
Slug: transparencia-internacional-alerta-sobre-concentracion-del-poder-en-gobierno-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Diseno-sin-titulo-4.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Twitter Fiscalía/ Iván Duque/ Procuraduría

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La organización Transparencia Internacional y su capítulo en Colombia alertó sobre una concentración del poder de la rama ejecutiva en el país. Cabe señalar que a lo largo del 2020 han sido elegidas personas cercanas al Gobierno de Iván Duque para encabezar diferentes organismos de control como la Fiscalía, la Procuraduría y la Defensoría del Pueblo, lo que podría poner en riesgo la independencia que deben tener dichos órganos externos de control frente al poder ejecutivo. [(Le puede interesar: En dos años del Gobierno Duque se ha derrumbado institucionalidad)](https://archivo.contagioradio.com/en-dos-anos-del-gobierno-duque-se-ha-derrumbado-institucionalidad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La organización, advierte sobre la capacidad que adquirió el Gobierno en medio de la emergencia de la pandemia por Covid-19 y que le dio facultades como legislador transitorio. Tan solo en el primer estado de emergencia para el mes de mayo, Duque decreto 72 actos legislativos, de los que al menos 6 fueron declarados inconstitucionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“Durante la pandemia se ha identificado una tendencia de aumento de la discrecionalidad del poder ejecutivo en muchos países, sin embargo, nos genera una profunda preocupación que el sistema de pesos y contrapesos en Colombia se vea afectado" expresó **Delia Ferreira, Presidenta de Transparencia Internacional.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Cercanía de Duque a organismos de control

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Otra de las razones ha sido la creciente cercanía del Gobierno a las mayorías en el Congreso además de la pérdida de autonomía de los organismos que deben ejercer un control sobre la rama ejecutiva con los nombramientos de **Francisco Barbosa en la Fiscalía, la llegada de la exministra de Justicia, Margarita Cabello a la Procuraduría y de Carlos Camargo a la Defensoría del Pueblo**. [(Le recomendamos leer: Las críticas tras la elección de Margarita Cabello en Procuraduría)](https://archivo.contagioradio.com/las-criticas-tras-la-eleccion-de-margarita-cabello-en-procuraduria/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Transparencia Internacional también se refirió a cómo ante la creciente concentración del poder del Gobierno en los últimos meses "su poder ha aumentado en forma preocupante" **"en detrimento de las otras ramas del poder y de las libertades ciudadanas"**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, **Andrés Hernández, director ejecutivo de [Transparencia por Colombia](https://transparenciacolombia.org.co/2020/09/03/alarmante-concentracion-del-poder-en-el-ejecutivo-en-colombia/)** señaló que pese a que el Gobierno destinó 25 billones de pesos para enfrentar la pandemia, "ya se han evidenciado irregularidades en el uso de recursos públicos" además de tomars "decisiones cuestionables sobre la ayuda directa a grandes empresas" que podrían favorecer intereses particulares. [(Lea también: Carlos Camargo será la voz del gobierno en la Defensoría del Pueblo)](https://archivo.contagioradio.com/cercania-del-nuevo-defensor-carlos-camargo-al-gobierno-es-un-dano-a-la-democracia/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
