Title: “Escritura y activismo” con el escritor ecuatoriano Mauro Javier Cárdenas
Date: 2018-02-11 10:07
Category: Cultura, Entrevistas, Nacional
Tags: Cultura, escritores, hay festival, literatura
Slug: escribir-bien-es-hacer-un-compromiso-con-la-claridad-del-lenguaje
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Fotografía-Mauro-Javier-Cardenas-AMR.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ana María Rodríguez 

###### Feb 2018 

#### [**Por: Carolina Garzón Díaz para Contagio Radio**] 

[El escritor ecuatoriano Mauro Javier Cárdenas, integrante del grupo Bogotá39-2017, que reúne a los mejores autores latinoamericanos menores de 40 años, habló con Contagio Radio desde el **Hay Festival Cartagena** sobre su novela “Los revolucionarios lo intentan de nuevo”, sus nuevos proyectos y la relación entre la literatura y el activismo: como escritor uno tiene un compromiso con una claridad que no existe en el diálogo diario, para que la gente pueda ver que las cosas no son como las están diciendo.]

[Compartimos algunos fragmentos de esta entrevista que puedes escuchar completa en audio.]

**Contagio Radio:** [En el marco del Hay Festival Cartagena, el 27 de enero se realizó el panel “Escritura y activismo”, allí participaron Mauro Javier Cárdenas, Juan Esteban Constaín, Emiliano Monge y Mónica Ojeda con Marta Orrantia, y la pregunta central fue ¿Tiene el escritor un compromiso que lo debería llevar a la acción?, en su criterio, ¿Lo tiene?]

**Mauro Javier Cárdenas:** [Realmente creo que no. Creo que pueden ocurrir dos cosas al mismo tiempo: hay escritores que son personas muy activas en la política y la sociedad, y hay escritores que escriben muy bien, que no tienen nada que ver con nada. Si piensas en Borges, él no estaba muy activo y escribió una obra buenísima. Entonces, a veces se habla mucho que realmente el compromiso del escritor es escribir bien. Ahora, escribir bien no significa que vas a escribir unas oraciones floridas y que suenen bonito, también es hacer un compromiso con la claridad del lenguaje.]

[Por ejemplo, en Estados Unidos con el desastre que es el Presidente Trump, hay un lenguaje tan virado, tan transformado, que como escritor uno tiene responsabilidad de hablar sobre estas cosas con una claridad que no existe en el diálogo diario, para que la gente pueda ver que las cosas no son como las están diciendo.]

**C.R:** [¿Cuáles son los espacios en los que actualmente está trabajando desde la escritura, haciendo vínculos entre su país de origen, Ecuador, y Estados Unidos, que es el lugar donde vive?]

**M.J.C:**[ En mi primera novela “Los revolucionarios lo intentan de nuevo” me pasé muchos años escribiendo sobre mi vida en Guayaquiil, porque vivía en Estados Unidos y tenía mucha nostalgia. ¡Y es como un chiste! porque no regresaba a Ecuador pero me pasaba todo el tiempo en Estados Unidos escribiendo sobre Ecuador: es como tener dos vidas en San Francisco. En la mañana me levantaba tempranito a escribir y pensar en Ecuador, en Guayaquil, en las memorias. Pasé 12 años escribiéndola, entonces el tema de Ecuador, por ahora, ya está. Ahora mi segunda novela y mi tercera novela son en San Francisco.]

**C.R:**[ Hablando sobre “Los revolucionarios lo intentan de nuevo”, allí se relata el deseo de varios jóvenes de luchar contra la corrupción de la clase política ecuatoriana mediante la candidatura electoral de uno de ellos. ¿Cree que este tipo de novelas pueden tener un impacto que inspire algún cambio en los lectores?]

**M.J.C:**[ Me gustaría decir que sí, me gustaría decir que la literatura cambia las cosas, pero me parece que no. La literatura es un espacio muy privado, que quizás pueda cambiar a una persona. Tengo una imagen en mi mente: un revolucionario en la selva leyendo una novela, haciéndolo para pasar el tiempo, quizás eso sea.]

[Se habla de la literatura como un mecanismo de cambio mucho más lento que el periodismo, que actividades más revolucionarias, pero es difícil realmente cuantificar el efecto de la literatura. Quizá podríamos pensarlo desde otro punto de vista: ¿qué pasaría si no hubiera literatura? ¿Qué nos pasaría a nosotros los seres humanos si no hubiera literatura? Quizás ahí encontremos la respuesta correcta.]

**C.R:** [Siendo “Los revolucionarios lo intentan de nuevo” una novela sensible a los problemas políticos de Ecuador y retratando ciertas condiciones políticas que nos unen como latinoamericanos, ¿por qué decide escribirla en inglés?]

**M.J.C:**[ Mi interés en los desafíos de la literatura es buscar diferentes maneras de expresarnos. Mi español era muy juvenil, era del colegio, era el de “joderle la vida” al compañero, y ese no era un lenguaje para hacer la literatura que yo quiero hacer, en la que yo quiero ver cómo escribo algo que no se ha escrito antes, una oración de una forma diferente. En inglés, como era nuevo para mí, cada palabra tenía una novedad y no tenía ninguna asociación con un pasado, entonces podría crear un sistema de comunicación muy particular a mí que, por supuesto, tiene muchísimo del español. Inclusive, la primera vez que una escritora mexicana, Carmen Boullosa, leyó esa novela, me mandó un mensaje diciendo “Esta novela fue escrita para mí” porque es una novela latinoamericana en inglés.]

**C.R:** [Hablando de lo que viene ¿Por qué ahora se centra en San Francisco y cuáles son los temas que lo llevan hoy a escribir?]

**M.J.C:**[ Mis temas son temas que me remueven por dentro, del subconsciente, son los temas que me hacen no dormir, que me producen insomnio, escalofríos. La segunda novela es sobre Antonio, que es el personaje de la primera novela, el cual no quiere lidiar con su hermana que ha perdido la razón. La tercera novela, que ya la comencé, también es sobre Antonio, el mismo personaje, que es deportado. Se trata de explorar que significa para él y sus hijas esa deportación.]

**C.R:** [¿Para cuándo se espera esta tercera novela?]

**M.J.C:**[ ¡Difícil! La segunda ya sale en 2019 en Estados Unidos. De la tercera ya llevo cuatro capítulos, pero quien sabe. Esperemos que no más de cinco años.]

**C.R:** [Es uno de los escritores más jóvenes de América Latina y también es integrante del grupo Bogotá39-2017, que reúne a los mejores autores latinoamericanos menores de 40 años, ¿cree que los escritores jóvenes deberían tener algún compromiso político con lo que sucede en sus países?]

**M.J.C:**[ La pregunta tiene que partir no desde la escritura, sino desde el ser humano. Es decir, yo, como ser humano ¿qué creo que debo hacer por los demás? Si la respuesta es “nada”, bueno ya está, se acabó la conversación. Sí la respuesta es “Algo. Creo que debería hacer algo por los otros seres humanos, especialmente en este mundo donde hay tanta injusticia y gente en la peores condiciones”, la segunda pregunta será “¿para qué eres bueno? Y si una de esas respuestas es “escritura” lo siguiente es ver cómo tú, con tu escritura, quizás puedas aportar algo. Tal vez no sea algo desde la literatura, podría ser a través del periodismo, de narrar lo que sucede. Al mismo tiempo, eso no significa que es lo único que puedes hacer, si tu carácter dice que debes salir a las calles a protestar, hazlo. Sí crees que puedes crear una Fundación para ayudar a la gente de tu barrio, hazlo. Pero no necesariamente tiene que partir de la literatura.]

**C.R:** [Cuando se hizo estas preguntas a usted mismo, ¿qué respondió?]

**M.J.C:** [Mi respuesta me la he preguntado mucho, porque ya no soy católico ni creo en Dios y siempre me he preguntado, ¿y si no crees en Dios, qué te lleva a decir que sí debes ayudar al prójimo? Bueno, creo que un católico que ya no es católico nunca puede decir, realmente, que ya no cree en nada porque tiene un pasado de creencia. Entonces mi respuesta es sí. Debo ayudar al prójimo.]

[Hay mucha gente que nace y tenemos gobiernos que no se enfocan en crear una igualdad para que todos cuando nazcamos tengamos las mismas oportunidades. Un filósofo polaco, Kołakowski, que está en mi primera novela, dice que el ser humano después de los cinco años no puede ser realmente feliz porque la infelicidad de prójimo le recluye de ser completamente feliz.]

**Entrevista completa: **

<iframe id="audio_23682643" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23682643_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
