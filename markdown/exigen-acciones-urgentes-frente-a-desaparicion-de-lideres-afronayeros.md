Title: Exigen acciones urgentes frente a desaparición de líderes afronayeros
Date: 2018-05-21 13:27
Category: DDHH, Movilización
Tags: afronayeros, comunidades del naya, Naya
Slug: exigen-acciones-urgentes-frente-a-desaparicion-de-lideres-afronayeros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/rio-naya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Nidiria Ruiz] 

###### [21 May 2018] 

Las comunidades que habitan en la cuenca del río Naya, Valle del Cauca, exigieron **respeto por la vida de los tres afronayeros** que se encuentran desaparecidos desde el pasado 5 de mayo. Denunciaron que la violencia en sus territorios continúa mientras que el Gobierno Nacional sigue incumpliendo las medidas de protección otorgadas por la CIDH y la implementación del Acuerdo de Paz.

Luego de haber participado durante dos días en la Asamblea Regiona por la Vida y la Permanencia en el Territorio, las comunidades pidieron que se libere a **Obdulio Angulo Zamora, Hermes Angulo Zamra, Simeon Olave Angudo e Iver Angulo Zamora**, afronayeros que fueron retenidos por estructuras armadas.

Además, exigieron que se respete la jurisdicción étnico territorial teniendo en cuenta la situación de **violación a los derechos humanos** que están viviendo las comunidades del Naya. Afirmaron la guerra “se ha agudizado” poniendo en riesgo la integridad física de las personas.

### **Afronayeros exigen medidas de protección** 

Ante esto, le han exigido al Gobierno Nacional que ponga en marcha las medidas de protección “acordadas con el Consejo Comunitario del Río Naya en el marco de las medidas cautelares de la CIDH” y que ha una implementación efectiva “del acuerdo de paz del Teatro Colón en toda la región del pacífico”.

Recordaron que “la paz no es posible sin el reconocimiento al territorio, sin el respecto y apoyo integral a nuestros planes de vida étnicos y colectivos”. Por esto, además de implementar el acuerdo de paz, exigieron que **se avance en el diálogo con la guerrilla del ELN** y que haya un avance en lo relacionado con “el sometimiento a la justicia de las estructuras neoparamilitares”. (Le puede interesar:["Crítica situación de derechos humanos en el Naya"](https://archivo.contagioradio.com/critica-situacion-de-derechos-humanos-en-el-naya/))

Adicionalmente, recalcaron en la necesidad de respetar “la consulta previa libre e informada y nuestra decisión de **no aceptar la mega minería**”. Pidieron que se otorguen garantías reales para la protección de los líderes sociales a la vez que le recordaron a la Fuerza Pública que se debe cumplir el principio de distinción del Derecho Internacional Humanitario.

Mientras que el Estado y el Gobierno Nacional responda por los afronayeros desaparecidos, **las 64 comunidades del Consejo Comunitario del Río Naya** continuarán en asamblea permanente.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
