Title: Niños indígenas y campesinos dibujan los efectos de la minería en sus cuerpos
Date: 2016-04-12 18:14
Category: Cultura, Otra Mirada
Tags: Guajira, indígenas, Mineria
Slug: ninos-indigenas-dibujan-los-efectos-de-la-mineria-en-sus-cuerpos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/IMG_2640-e1460428283260.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Maria Andrea Gómez] 

###### [12 Abr 2016] 

"No me gusta el polvillo que deja la mina de carbón. Las basuras que se acumulan y se queman. [No quiero que desvíen el Arroyo Bruno](https://archivo.contagioradio.com/empresa-cerrejon-desviara-una-de-las-principales-fuentes-de-agua-de-la-guajira/)", son algunos de los fragmentos que escribe María Andrea Gómez, comunicadora social y periodista con maestría en Estética y Creación, luego de un trabajo de "cartografía de los cuerpos" con niñas y niños indígenas y campesinos que viven en territorios afectados social y ambientalmente por la minería en departamentos como **[La Guajira](https://archivo.contagioradio.com/empresas-y-estado-responsables-de-la-situacion-en-la-guajira/), Cauca y Antioquia.**

"A pesar de que están en medio de un problema de sequía y hambre, todos sonríen, hacen corazones, es decir, no son víctimas, en eso es en lo que quiero hacer hincapié", afirma Maria Andrea, cuyo trabajo nace de su investigación de maestría. Un trabajo que lleva 4 años, y según dice, es una labor que apenas está empezando.

Maria explica que se trata de un trabajo realizado por Censat Agua Viva - Amigos de la Tierra Colomnbia, en el que se vincula la corporalidad con el territorio, a través de metodologías educativas y estéticas desde las cuales se levantan mapas que develan reflexiones frente a las percepciones que tienen las personas sobre la relación que hay entre su cuerpo y el espacio que habitan.

En este caso los niños y niñas hacen una reconstrucción de la **imagen corporal junto con símbolos, sobre los imaginarios que tienen de su familia, la naturaleza, la comunidad, la escuela y su hogar** que se ven fragmentados por la explotación de la naturaleza.

**[![exposicion-guajira ninos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/exposicion-guajira-ninos.jpg){.size-full .wp-image-22602 .alignright width="373" height="800"}](https://archivo.contagioradio.com/ninos-indigenas-dibujan-los-efectos-de-la-mineria-en-sus-cuerpos/exposicion-guajira-ninos/)"Son niños y niñas sobrevivientes",** como los describe Gómez. Para ella, el ejercicio decide hacerse con los más pequeños pues "hablan con conocimiento de causa. Nacen en medio de la problemática, nacieron y ya estaba la mina, el polvillo, la sequía y la comunidad ya estaba pobre", pero con este ejercicio se dan cuenta que esa situación, ni ellos ni sus comunidades la deberían  estar viviendo, **"Saben que necesitan amor, agua, alimento y unidad, ellos tienen claro eso", expresa la periodista.**

A partir del agua, el aire, el fuego y la tierra, dibujan su realidad, pero también lo que no quieren seguir viviendo y los sueños que tienen. Desde el agua, reconocen las riquezas hídricas que tienen y que han perdido a causa de la minería, dan cuenta de los ríos, lagunas, arroyos y mares; con la tierra señalan las montañas, los cerros, colinas, pero también las semillas, frutos y animales; con el aire se percatan de la contaminación por el polvillo que suelta el proceso de la minería, y con ello, enfermedades respiratorias.

La exposición además de los dibujos cartográficos de los niños y los escritos de Maria Andrea, también cuenta con la evidencia fotográfica de los impactos de la minería; un cementerio tradicional en La Guajira arrasado; una porción del arroyo Bruno usado como botadero de desechos de la mina; la sequía, y las increíbles proporciones de [la mina de El Cerrejón](https://archivo.contagioradio.com/mina-de-carbon-del-cerrejon-usa-diariamente-17-millones-de-litros-de-agua/).

La exposición se presentó del **6 al 8 de abril en la Universidad Nacional** en el marco de los 130 años de la Facultad de Artes, también será presentada en las regiones en las que se realizó el ejercicio, para obtener una retroalimentación, y luego enseñarlas en las calles de diferentes ciudades del país.

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
