Title: ESMAD sabotea Encuentro Internacional de Liberadores de la Madre Tierra
Date: 2017-08-03 17:40
Category: Ambiente, Nacional
Tags: Cauca, Corinto, ESMAD, indígenas, liberación de la madre tierra
Slug: esmad-sabotea-encuentro-internacional-de-liberadores-de-la-madre-tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Liberación_Madre_Tierra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

###### [03 Ago. 2017] 

Ad portas de comenzar el Encuentro Internacional de Liberadores de la Madre Tierra, en Corinto departamento del Cauca, según información entregada por *Alternativa Popular*, **el ESMAD arremetió contra la población, en el terreno que se encuentran para realizar el evento**. Aseguran que el cuerpo policial ya se retiró del lugar pero que en su momento llegó con motosierras y quemando la logística que se tenía lista.

**“Estamos preocupados porque no sabemos qué más puedan hacer por parte de las fuerzas represivas del Estado** contra un evento internacional que se estaba preparando hace mucho tiempo y que el Estado quiere opacar” manifiesta uno de los asistentes al evento y testigo de la situación. Le puede interesra: [La resistencia del pueblo Nasa en "A sangre y Tierra"](https://archivo.contagioradio.com/la-resistencia-del-pueblo-nasa-en-a-sangre-y-tierra/)

Manifiestan en la comunicación que “**esto es una clara muestra del sabotaje por parte del Estado y los terratenientes de la zona**, sumado a los ataques de las últimas semanas donde dos compañeros quedaron heridos por esquirlas de recalzadas”

Finalmente han llamado a la solidaridad de todos los luchadores del país para **seguir y aumentar el apoyo con el proceso de liberación y democratización de la tierra** que se está llevando a cabo en el Norte del Cauca.  Le puede interesar: [Continúa proceso de liberación de la madre tierra en Aguas Tibias, Cauca](https://archivo.contagioradio.com/continua-proceso-de-liberacion-de-la-madre-tierra-en-aguas-tibias-cauca/)

Cabe recordar que, en los últimos cinco meses, a propósito del proceso de liberación de la madre tierra que llevan a cabo los indígenas en el Cauca **han fallecido ocho comuneros y un sin número aún determinado han resultado heridos** por la intervención del ESMAD. Le puede interesar: [8 comuneros han sido asesinados en proceso de liberación de la madre tierra en Cauca](https://archivo.contagioradio.com/8-comuneros-han-sido-asesinados-en-proceso-de-liberacion-de-la-madre-tierra-en-cauca/)

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FAlternativaPopularCol%2Fvideos%2F1781563725417605%2F&amp;show_text=0&amp;width=560" width="560" height="420" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="osd-sms-wrapper">

</div>
