Title: Estos fueron los acuerdos pactados entre la Cumbre Agraria y MinAgricultura
Date: 2015-09-02 13:34
Category: Economía, Movilización, Nacional
Tags: Aurelio Iragorri, Cumbre Agraria Campesina Étnica y Popular, Luis Fernando Arias, Ministerio de Agricultura, ONIC, Paro agrario 2013, toma del ministerio de Agricultura
Slug: estos-fueron-los-acuerdos-pactados-entre-la-cumbre-agraria-y-el-minagricultura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/11950948_10153430505900020_111843201_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_7760069_2_1.html?data=mJyjkpWafY6ZmKiak5WJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTo0NiYyNrJttDijNHc1ZDFp9bZ08nc1ZDUpcTowsnc1ZDJsozgwpCw19LGtsafoszfw9fNpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Luis Fernando Arias, Consejero Mayor de la ONIC] 

###### [2 Sept 2015] 

**“La jornada de ayer era de carácter pacífico como hemos sido históricamente los pueblos indígenas,** en ningún momento se retuvo o agredió a algún funcionario del ministerio”, aseguró Luis Fernando Arias, Consejero Mayor de la ONIC, quien señala de **"maquiavélica" la forma como los medios empresariales mostraron el acto de resistencia que realizaron indígenas** y campesinos buscando hacer que el Ministro de Agricultura, Aurelio Iragorri, se sentara con ellos para dar respuesta sobre los incumplimientos de gobierno colombiano frente a los acuerdos pactados en el paro agrario de 2013.

Según cuenta Arias, **el Ministro de Agricultura, había “dejado plantados” a los voceros de la Cumbre Agraria** ya que se había acordado una reunión la semana pasada,  fue por eso, que ante los constantes incumplimientos del gobierno, indígenas y campesinos decidieron tomarse el Ministerio de manera pacífica, "suponemos que los funcionarios salieron de las instalaciones por orden del propio ministro", indica el consejero mayor de la ONIC.

**Los acuerdos**

Con la reunión la Cumbre Agraria logró que se estructurara **una agenda de trabajo y cinco mesas de diálogo con el Ministerio de Agricultura** para tratar temas que tiene que ver con el campo colombiano, también se acordó el establecimiento de 3 mesas para hablar con el Incoder sobre adjudicación de tierras.

Por otro lado, se planteó una reunión con el Departamento de Planeación Nacional, el Ministerio de Justicia y otras entidades estatales para hablar sobre la situación de los derechos humanos de los pueblos indígenas, teniendo en cuenta que **en lo corrido del 2015 los asesinatos de los nativos han aumentado un 58%**, según cifras dadas por la ONIC.

Hace 2 meses el presidente Juan Manuel Santos se había comprometido a cumplir los acuerdos y había afirmado que los recursos ya estaban dispuestos, eso mismo reiteró el Ministro Iragorri el día de ayer, por lo que el líder de la ONIC asegura que **el problema se basa en la falta de voluntad política del gobierno y no en la falta de recursos.**

Por el momento la Cumbre Agraria Campesina, Étnica y Popular **reafirma su posición de no irse de Bogotá hasta que no se haya concretado todos los puntos** acordados para que el gobierno le cumpla al campo.

Mañana se llevará a cabo una marcha pacífica hasta la Plaza de Bolívar, porque se llevará a cabo una audiencia en el Congreso de la República frente al tema “Los pueblos indígenas conflicto armado y paz” y el viernes se evaluará si se ha adelantado o no los puntos acordados.
