Title: Para 2030, 950 millones de niñas se habrán casado: UNICEF
Date: 2016-07-01 14:10
Category: DDHH, El mundo
Tags: Derechos niños y niñas, Infancia en el mundo, Informe UNICEF 2016, UNICEF 2016
Slug: para-2030-950-millones-de-ninas-se-habran-casado-unicef
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/UNICEF.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UNICEF ] 

###### [1 Julio 2016] 

["Millones de niños en todo el mundo están atrapados en un ciclo intergeneracional de desventaja que pone en riesgo sus futuros", asegura UNICEF en su más reciente informe, en el que estima que para 2030, más 165 millones de niñas y niños vivirán con US\$1,90 al día, **69 millones morirán por causas prevenibles a sus 5 años**, 60 millones de infantes de entre 6 y 11 años estarán desescolarizados y **950 millones de mujeres habrán contraído matrimonio siendo niñas**.]

[El informe titulado "Estado mundial de la infancia 2016; una oportunidad justa para cada niño", estudió la cotidianidad de varias familias en India, Nigeria, Serbia, Uganda, Sudan, Haití y otros 30 países, en los que UNICEF analizó las condiciones de salud, vivienda, educación y alimentación en las que viven los niños y las niñas alrededor del mundo, concluyendo que "l]a inequidad pone en peligro a millones de niños y amenaza el futuro del mundo", pues como lo asegura el organismo "en comparación con los niños más ricos, **los niños más pobres tienen 1,9 veces más probabilidades de morir antes de los cinco años**".

UNICEF alerta a gobiernos y activistas en todo el mundo para que establezcan acciones concretas y efectivas que garanticen los derechos de los niños y las niñas y sea posible cambiar el pronóstico que determinan para el 2030.

<iframe id="doc_18902" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/317229523/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-kPT01S6NU8MlARpmHd4k&amp;show_recommendations=true&amp;show_upsell=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

   
[  ]  
 
