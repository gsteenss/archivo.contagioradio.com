Title: Desde firma del acuerdo de paz reportan 40 indígenas asesinados: OPIAC
Date: 2017-10-31 12:58
Category: DDHH, Nacional
Tags: Asesinatos de indígenas, Derechos Humanos, derechos humanos de indígenas, indígenas, indígenas de Colombia
Slug: indigenas-asesinados-acuerdo-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/indigenas-asesinados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vanguardia Liberal] 

###### [31 Oct 2017] 

En el marco de la movilización indígena a nivel nacional las comunidades indígenas exigen que se respete los derechos humanos pues, **más del 62.7% de los pueblos están en peligro de desaparición.** Además se registran 40  indígenas asesinados desde que se pactó el acuerdo de paz entre el Gobierno y las FARC y más de 1000 han sido amenazados de muerte.

De acuerdo con Robinson López, encargado de derechos humanos de la Minga Indígena e integrante de la Organización OPIAC, “los pueblos indígenas están pasando **por una grave situación**, los asesinatos y la violencia es sistemática”. Además, dijo que “el Gobierno Nacional está tratando de intimidar a los pueblos para que no salgan a la Minga”.

### **En investigaciones contra asesinatos de indígenas hay impunidad total** 

López manifestó que, desde la Fiscalía, **la constante es que no existan avances en materia de investigación** por los asesinatos y la violencia a integrantes de las comunidades indígenas. Por esto, desde las diferentes organizaciones han denunciado y exigido que se realicen las investigaciones necesarias, “pero eso no ha sido posible, el Gobierno confunde a los medios con información errada”. (Le puede interesar: ["Gobierno ha incumplido 1300 acuerdo a la Minga Indígena"](https://archivo.contagioradio.com/inicia-minga-indigena-en-colombia/))

Además, manifestó que existe una **sistematicidad en los homicidios** en la medida que “las personas que asesinan son líderes y defensores de derechos humanos”. Dijo que, en el Gobierno Nacional, no han querido reconocer el fenómeno de la reparamilitarización y “no hay garantías a los más de mil líderes indígenas que hay amenazados en el país”.

### **Indígenas proponen política de atención diferencial** 

Como propuesta para superar esta situación, las organizaciones indígenas han manifestado que se debe **crear una política pública de atención diferencial** dirigida especialmente a los indígenas, en la medida que “se atienda un enfoque diferencial pues la Unidad Nacional de Protección no tiene el enfoque étnico para la protección de los líderes”.

López aseguró que “no es lo mismo proteger funcionarios del Gobierno **que proteger a líderes indígenas** que tienen usos y costumbres diferentes”. Como las autoridades no han tenido en cuenta esta diferencia, “lo único que hacen es facilitar esquemas de seguridad con escoltas y chalecos anti balas y eso para los pueblos indígenas no es protección”. (Le puede interesar: ["Ya no estamos para nuevos acuerdos, queremos hechos: indígenas de Kokonuco"](https://archivo.contagioradio.com/comuneros-exigen-al-gobierno-devolucion-de-territorios-ancestrales-en-kokonuko-cauca/))

### **Amenazas y asesinatos a indígenas ocurren por causas estructurales** 

El aumento del número de amenazas y asesinatos está relacionado, en principio, con **las actividades de defensa del territorio** que realizan los indígenas. López indicó que, “en los territorios hay intereses de carácter económico por parte de multinacionales petroleras y mineras que quieren realizar proyectos de extracción”.

Ante esto, cuando las comunidades intentan defender sus territorios, **son asesinadas o amenazadas**. Además, de manera reiterada, a los pueblos indígenas se les ha violado el derecho fundamental a la consulta previa, “que es el único mecanismo que tienen para hacer respetar su territorio”.

### **Falta de implementación del acuerdo también afecta a los pueblos indígenas** 

Además de esto,  no fueron tenidos en cuenta en el desarrollo del capítulo étnico del acuerdo de paz en la Habana y **los grupos armados han hecho presencia en los territorios** donde antes estaba la guerrilla de las Farc. López dijo que “el Gobierno no ha sido capaz de mantener la seguridad en los territorios indígenas”.

Finalmente, recordó que han sido olvidados y estigmatizados y “el mismo presidente ha dicho que **los pueblos indígenas son un obstáculo para el desarrollo**”. Adicionalmente, indicó que, en el marco de la movilización indígena, “la fuerza Pública ha agredido a 4 indígenas que están heridos”.

<iframe id="audio_21798274" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21798274_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
