Title: Pinturas rupestres de Guaviare en riesgo por incendios forestales
Date: 2018-02-28 14:25
Category: Ambiente, Nacional
Tags: arte rupestre, Guaviare, incendios forestales
Slug: pinturas-rupestres-de-guaviare-en-riesgo-por-incendios-forestales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Pinturas-rupestres-Guaviare.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ecoturismo Putumayo] 

###### [28 Feb 2018] 

Debido a los incendios que se han registrado en zonas del departamento de Guaviare, están en riesgo de desaparecer las pinturas rupestres que se encuentran plasmadas en rocas. Antropólogos y habitantes de Cerro Azul, donde se encuentran las pinturas, le han hecho un llamado al Gobierno Nacional para que intervenga de manera **urgente** en el control del incendio.

De acuerdo con Diego Pedraza, antropólogo de la Universidad Nacional, Cerro Azul es un sitio arqueológico muy importante para el país y para la humanidad “porque es uno de los lugares con **mayor densidad de arte rupestre en el mundo**”. Este lienzo tiene 12 mil años y contiene más de un kilómetro cuadrado de arte rupestre y yacimientos arqueológicos.

### **Fuego y humo ponen en riesgo las pinturas rupestres** 

Hoy, el fuego y el humo que se ha salido de control y que en su gran mayoría ha sido provocado por personas que han realizado quemas, **“ya ha tenido un impacto sobre las pinturas rupestres”** y se desconoce el impacto que tenga sobre éstas si no se atiende de manera urgente la emergencia. Por esto, el antropólogo le hizo un llamado a las autoridades para que le den prioridad a la atención de este incendio.

Pedraza enfatizó en que las llamas, de llegar al cerro **“se convertirían en un incendio incontrolable”**. Esto teniendo en cuenta la geografía, la formación geológica y el difícil acceso por lo que “sería imposible controlar un incendio allí”. Además, manifestó que en esta época ha habido cerca de 800 incendios en Guaviare “de los cuales la mayoría están activos”. (Le puede interesar: ["Casanare y 32 departamentos en alerta roja por incendios forestales"](https://archivo.contagioradio.com/casanare-y-otros-32-departamentos-estan-en-alerta-roja-por-incendios/))

### **Pinturas rupestres cuentan las historias de nuestros antepasados** 

Desde el punto de vista legal, este tipo de patrimonio arqueológico “debe ser protegido y hay leyes patrimoniales para conservarlo”. El antropólogo enfatizó en que “este sitio está en camino a convertirse en un parque nacional natural que ya es reserva y que podría ser **un parque arqueológico nacional**”. Esto fortalece el argumento de que se trata de un lugar con una riqueza inigualable “que podría estar protegido por la UNESCO”.

Las pinturas rupestres que están allí impregnadas en las rocas del Guaviare “cuentan historias de vida de nuestros antepasados, **la conformación del hombre**, las dinámicas de poblamiento, la mitología y el entorno en el que ellos vivieron”. Todo esto “que se compara con la gran biblioteca de Alejandría” está bajo la amenaza de desaparecer para siempre.

<iframe id="audio_24134644" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24134644_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
