Title: Amnístia Internacional invita a ser "Vigilantes de Trump"
Date: 2017-01-20 13:07
Category: DDHH, El mundo
Tags: Amnistía Internacional, Derechos Humanos, Donald Trump, migración
Slug: amnistia-internacional-trump
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/dt.common.streams.StreamServer.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:AFP 

##### 20 Ene 2016 

En el marco de la posesión de Donald Trump como nuevo presidente de los Estados Unidos, Amnistía Internacional envió una invitación abierta para que los **ciudadanos le envien un mensaje pidiendole que renuncie a las propuestas de su campaña que atentan contra los Derechos Humanos** y a que estas no se conviertan en sus propuestas presidenciales.

El llamado de la organización es a que las personas le exijan a Trump "**defender las protecciones de los defensores de los derechos humanos** y proteger los derechos de las personas que huyen de los conflictos y las crisis al **no darle la espalda a los refugiados**", uno de los temas controversiales durante su campaña electoral.

"Dígale que **detenga la venta de armas a los gobiernos que cometen abusos contra los derechos humanos** y que cierre Guantánamo" expresa la organización en su invitación instando a que se le pida al Republicano "que **renuncie inequívocamente a la tortura**". le puede interesar: [El muro no va a ser en la frontera sino en México entero](https://archivo.contagioradio.com/muro-no-va-a-ser-en-la-frontera-34924/).

A través de la plataforma [Take action now](https://netdonor.net/ea-action/action?ea.client.id=1839&ea.campaign.id=62058), Amnistía Internacional da la posibilidad a los ciudadanos de **unirse con un mensaje** que pueda sumarse al enviado por la organización en el que dicen al Presidente: "**Cada día en el cargo, cada elección que usted haga definirá su legado**".

En su mensaje, la ONG asegura estar **encontra de un registro musulmán y del cierre de programas dirigidos a los refugiados**, en su lugar resalta su trabajo en la defensa de los Derechos Humanos de mujeres, negros, personas en condición de discapacidad, miembros de la comunidad LGTBI y los activistas del mundo.

"Usted puede elegir dejar el mundo un lugar mejor o uno en el cual el odio, el miedo y la discriminación crezcan más fuerte. **Le instamos a renunciar al odio ya elegir los derechos humanos**" concluye la invitación.
