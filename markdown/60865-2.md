Title: 200 niños Wayúu se quedaron sin educación en la Guajira
Date: 2019-01-31 11:26
Author: AdminContagio
Category: Comunidad, Educación
Tags: comunidades de la Guajira, Comunidades Wayúu, Consejo de Estado, educacion
Slug: 60865-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/ninos-wayuu-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ONG Nación Wayúu 

###### 31 ene 2019 

Las comunidades Wayúu denunciaron el cierre de una escuela con enfoque étnico en el territorio indígena de Tokomana, en la Guajira, que dejó al menos de 200 niños y niñas de este pueblo sin educación.

Desde 2010, profesores, padres de familia y niños y niñas Wayúu pusieron en marcha un colegio en el que esperaban poder enseñar sus tradiciones, sus costumbres y en su lengua. Se trata de la sede educativa Tokomana que se quedó sin reconocimiento y sin recursos luego de una decisión de  Grisela Monroy, administradora temporal para la Educación en el Destino Turístico de Riohacha, según lo denunció la ONG Nación Wayúu.

José Silva, integrante de la organización, aseguró que no se surtió el trámite de consulta previa ordenado por el Consejo de Estado para que fueran las comunidades étnicas las que tomaran una decisión sobre la continuidad de este establecimiento educativo con un fuero especial.

Según la información publicada referente a la decisión del Consejo de Estado en mayo de 2018, la Administradora "deshabilitó la sede satélite Tokomana omitiendo el deber de consulta a la comunidad afectada y propiciando el traslado abrupto de los estudiantes hacia otra sede y la posible deserción escolar de dicha población."

Tras la decisión de la Administradora de cerrar el colegio por segunda vez, Silva afirmó que es posible que exista un interés adicional, dado que por cada cupo la asignación es de cercana al millón de pesos mensual, lo que sería un “buen negocio” si se ubican esos cupos en la ciudad.

Al respeto, Monroy manifestó que los estudiantes de la sede escolar Tokomana podrían trasladarse a las escuelas urbanas de Riohacha, el cual lo han rechazado las comunidades dado que los niños y niñas no estarían una educación con énfasis étnico. Silva aseguró que someter estos estudiantes Wayúu a una educación occidental consiste en una violación de derechos humanos de población indígena.

Además los niños y niñas deberían desplazarse de sus comunidades a los centros urbanos sin ningún tipo de garantía de transporte, alimentación un otros, además asumiendo el riesgo de perder su tradición y su cultura. Por ello los líderes sociales están exigiendo que se surta el trámite de la Consulta Previa y se garantice el funcionamiento de Tokomana con la asignación de los recursos necesarios y suficientes.

###### Reciba toda la información de Contagio Radio en [[su correo]
