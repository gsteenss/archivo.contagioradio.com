Title: Crece represión del gobierno hacia el paro camionero
Date: 2016-07-13 16:19
Category: Movilización, Nacional
Tags: Duitama, ESMAD, Gobierno, Paro camionero
Slug: crece-represion-del-gobierno-hacia-el-paro-camionero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/paro-camionero-e1468443085238.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Transporte 

###### [13 Jul 2016] 

Un joven de 27 años de edad fue asesinado por el ESMAD en medio de protestas del paro camionero en Duitama, Boyacá, según denuncian los manifestantes que fueron testigos del hecho. Además se militarizó la ciudad y se decretó un toque de queda desde la noche de ayer.

Se trataba del joven Luis Orlando Saiz, quien se encontraba en la carrera 42 con avenida de las Américas en la ciudad de Duitama, en medio de la manifestación fue herido en el cráneo por un artefacto disparado por un efectivo del ESMAD. Cuando el joven fue trasladado al hospital ya se había muerto. Además se denuncia que pese a las reiteradas llamadas a los entes de control, **solo hora y media después el CTI hizo presencia en el lugar de los hechos para la respectiva valoración del hecho.**

Asimismo, desde el Comité Permanente por la Defensa de los Derechos Humanos, seccional Boyacá, se informa que **hay dos jóvenes más heridos, entre ellos una menor de edad que presenta lesiones en una mano producto de un impacto con un artefacto de los que dispara el ESMAD** y otro joven que fue lastimado con una granada de aturdimiento lanzada a su cabeza.

Aunque según versiones de la policía, el hombre habría muerto porque había manejado un artefacto explosivo, los testigos afirman que “un agente del ESMAD le disparó a la cara a una distancia de 5 metros”.

### Continúa la represión por parte gobierno nacional 

**“El paro fue convocado de manera pacífica, queremos una protesta ejemplar, pero el ESMAD nos ha atacado**”, asegura Jorge García, quien indica que la fuerza pública ha incitado a la violencia dejando decenas de heridos, incluso, Contagio Radio conoció que **uno de los transportadores que vive en el barrio 20 de Julio, denuncia que está siendo vigilado por cinco patrullas de la policía.**

Desde el gremio se asegura que el gobierno ha mostrar un paro violento como una estrategia para desviar la movilización pacífica, sin embargo, el presidente de la Federación dice que las únicas armas con las que cuentan son sus camiones, que hoy se encuentran parqueados.

De acuerdo con el gremio, por eso es que el presidente había culpado a los transportadores de haber ocasionado el accidente que sufrió la caravana en la que se movilizaba el gobernador de Boyacá, Carlos Amaya. Desde la Federación Nacional de Camioneros se indica que en ese punto de la carretera no había concentración de los manifestantes y mucho menos se arrojó piedras a las camionetas, como también lo relató la periodista** Deisy Rodríguez, quien ha dicho que no vio piedras ni manifestantes en la vía.**

Por su parte, el gobierno nacional respondió a las protestas de los camioneros con amenazas de **multas que podrían ser de 480 millones de pesos para las empresas que se nieguen a prestar el servicio de transporte,** sin embargo la posición de los camioneros es firme porque están trabajando a pérdidas y no continuarán en esas condiciones. Adicionalmente aseveran que “ninguna empresa se puede sancionar por ese motivo, solo las que manejan pasajeros, en carga no se puede hacer. **Ni la empresa ni el camionero pueden ser sancionados”**.

**Con el gobierno se han hecho 23 reuniones** y durante la mañana del miércoles las directivas del gremio mantuvieron una reunión con la Conferencia Episcopal, en la que pidieron a ** Monseñor Luis Augusto Castro, ser intermediario en la negociación con el gobierno.**

Mientras tanto, **las protestas continúan pacíficamente en más de 100 puntos del país.** Desde el martes, los camioneros de Barranquilla que trabajan para la Sociedad Portuaria de Barranquilla suspendieron de forma indefinida las operaciones para unirse al llamado de paro nacional que ya cumple 37 días.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en [Otra Mirada](http://bit.ly/1ICYhVU) por Contagio Radio 
