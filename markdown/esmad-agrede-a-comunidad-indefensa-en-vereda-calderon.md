Title: ESMAD agrede a comunidad en Vereda Calderón
Date: 2019-10-30 12:55
Author: CtgAdm
Category: DDHH
Tags: Puerto Boyacá
Slug: esmad-agrede-a-comunidad-indefensa-en-vereda-calderon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-29-at-4.07.08-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-29-at-4.07.08-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-27-at-9.45.32-AM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-27-at-9.45.39-AM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-27-at-9.45.40-AM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

Desde el 16 de octubre habitantes de la Vereda Calderón  en Puerto Boyacá , han realizado una protesta pacifica en contra de la empresa Mansarovar Energy y la falta de cumplimiento ante los acuerdos que garantizaban mejora de las vías y abastecimiento de agua potable; manifestación que fue detenida por la Fuerza Pública el día sábado, con la intervención de 100 disponibles del ESMAD, quienes atacaron con gases lagrimógenos  a la comunidad, dejando a 6 campesinos con graves lesiones en su cuerpo.

La denuncia que hacen los habitantes de esta región hace alusión al arreglo de vías principales donde pasan rutas escolares y que se encuentran con grandes deformaciones por el paso de los vehículos pesados de la petrolera. Según Clara Díaz, lideresa e impulsadora del turismo comunitario en Puerto Boyacá, "ha sido imposible por parte de la comunidad generar un espacio donde haya un cumplimiento por parte de la  empresa quienes lleva dilatando estos procesos desde 2016". (Le puede interesar:[ESMAD agrede a campesinos que se oponen a multinacional petrolera en Caquetá](https://archivo.contagioradio.com/esmad-agrede-a-campesinos-que-se-oponen-a-multinacional-petrolera-en-caqueta/))

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-27-at-9.45.32-AM-1024x498.jpeg){.wp-image-75770 .alignleft width="459" height="223"}

En el acta de acuerdos Mansarova Energy se comprometió a generar agua potable, esto con respecto a las daños que a causado con la explotación a los puntos hídricos que abastecen al territorio,  "la comunidad lleva consumiendo agua industrial desde hace meses, nuestra agua no se puede usar para nada y debemos comprar una gran cantidad que supla todas las necesidades básicas en nuestros hogares", agregó Díaz.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-27-at-9.45.40-AM-1024x498.jpeg){.wp-image-75772 .aligncenter width="460" height="224"}

###  

### La petrolera no cumple, ni es consciente de los riesgos que causa 

La llegada de los agentes del ESMAD no solo se dio para disipar a los manifestantes también tenia como objetivo el ingreso de 10 tractomulas cargadas con sodio, uno de los  más importante metales alcalinos y altamente inflamable, "el escuadrón lanzó bombas lagrimógenas, perdigones muy cerca a estos vehiculos cargados, no eran conscientes que en cualquier momento podrían generar una explosión masiva y acabar con la vida de todos", señaló Díaz. (Le puede interesar:[ESMAD usó material no convencional y violó protocolos en movilizaciones estudiantiles](https://archivo.contagioradio.com/esmad-uso-material-no-convencional-y-violo-protocolos-en-movilizaciones-estudiantiles/))

Hasta el momento y según las declaraciones de la lideresa no hay judicializaciónes de los manifestantes, "no pudieron capturar  a nadie porque en ningún momento la comunidad les respondió su agresión, simplemente se quedó esperando a que los hirieran"; así mismo agregó que se registran 6 personas en el hospital  con heridas y  2 con lesiones graves en sus extremidades por causa de los ataques del escuadrón, "ahora estamos en el  proceso de poner denuncias en la Fiscalía contra el ESMAD, el comandante del Magdalena", resaltó Díaz.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
