Title: Crisis humanitaria en la cárcel "la Tramacua" afecta a 1500 internos
Date: 2015-02-03 21:20
Author: CtgAdm
Category: DDHH, Movilización
Tags: carcel, desobediencia, presos, prisioneros, tramacua, valledupar
Slug: crisis-humanitaria-en-la-carcel-la-tramacua-afecta-a-1500-internos
Status: published

###### Foto: Justice For Colombia 

##### <iframe src="http://www.ivoox.com/player_ek_4034437_2_1.html?data=lZWglpmXe46ZmKiakpyJd6Knl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkYa3lIquk9iPqMafkpOikpWPtNPd1M7c0MrWs9SfxcqYrsaPmNPVzsbQ18aPqc%2Bfxcrg0cfJqMrZz8iah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Ingrid Saavedra, Abogada] 

La Tramacua **no cuenta con un sistema de acueducto adecuado para garantizar condiciones de vida digna para los prisioneros**, **hay problemas de alimentación**, y los reclusos no cuentan con garantías políticas y de interlocución para sus representantes. A ello se suma la **tortura y malos tratos por parte de la guardia penitenciaria.**

Por ello, más de 1.500 prisioneros del establecimiento penitenciario y carcelario de alta y mediana seguridad de Valledupar, La Tramacua, anunciaron que iniciarán un **proceso de desobediencia civil** para protestar por la situación humanitaria que se sufre al interior del penal.

<div>

El 2 de febrero la Torre 7 del establecimiento empezó la manifestación, exigiendo que se de cumplimiento a la Sentencia T-282 del 2014 en que la Corte Constitucional reconoce que hay una **grave y sistemática violación a los Derechos Humanos de los prisioneros de La Tramacua,** otorgando un plazo de 1 año para resolver los problemas de agua potable. De no solucionarse estos inconvenientes, el centro deberá ser clausurado.

Se espera que en las próximas horas los demás patios del plantel penitenciario informen sobre el inicio de la acción de protesta.

Actualmente el sistema penitenciario en Colombia tiene una capacidad para 256 reclusos y cuenta con más de 1500 personas, lo que significa un índice de **hacinamiento superior al 300%** en promedio y la "operación reglamento" aplicada por diversos sindicatos del INPEC tiene viviendo en condiciones extremas a las personas recluidas en diversas estaciones de policía de todo el país.

A pesar de los diversos pronunciamientos de diversos organismos como la propia Defensoría del Pueblo y la Corte Constitucional, así como de los familiares de los presos y organizaciones de DDHH, ni el INPEC ni el gobierno nacional han tomado medidas para resolver esta crisis que tiende a empeorarse con el paso del tiempo.

</div>
