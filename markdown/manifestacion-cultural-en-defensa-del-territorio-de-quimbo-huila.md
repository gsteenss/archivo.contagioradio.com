Title: Manifestación cultural en defensa del territorio de Quimbo, Huila
Date: 2015-06-16 14:47
Category: Movilización, Nacional
Tags: Fiestas San Pedrinas, Folclor, Hidroelectrica, Huila, Miller Dusan, Plantón Cultural, Quimbo
Slug: manifestacion-cultural-en-defensa-del-territorio-de-quimbo-huila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/SanPedro1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto:artesaniaskyra.com 

##### <iframe src="http://www.ivoox.com/player_ek_4649023_2_1.html?data=lZuhm5WWd46ZmKiakpqJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcrgzcrfjanZt8LijoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Miller Dusan] 

##### [16 Jun 2015 ]

El día de hoy se han desarrollado varios actos culturales en **Quimbo**, **Huila**, actos que se desarrollan en defensa del territorio y en contra de la hidroeléctrica que allí se construye. El plantón cultural se lleva a cabo desde las 9 a.m. y a lo largo del día frente a la gobernación del **Huila**, buscando incorporarse a las actividades san pedrinas y al folclor regional sin dejar de expresar su descontento con el proyecto hidroeléctrico del **Quimbo**.

**Miller Dusan** vocero de la asociación víctimas del Quimbo, habla acerca de las manifestaciones culturales que se realizan durante la jornada, en la que harán presencia grupos de rajaleñas, cuenteros, la reina del folclor, pescadores y afectados de las zonas del **Quimbo**, que según asegura son aproximadamente **30.000** personas.

La invitación se abrió también para el gobernador del **Huila** para que hablara públicamente acerca de la problemática de la hidroeléctrica y sobre los compromisos de la licencia ambiental, de igual manera a todas las autoridades competentes para que hicieran parte de esta manifestación pacífica, Lo más importante es la parte cultural y artística, la idea es poder contribuir a la

“Es importante que el país y sobretodo los huilenses se enteren de la problemática que se vive actualmente y  de nuestra forma de contribuir a través del folclor a la defensa del territorio y de los afectados y víctimas del Quimbo”. Enfatiza **Miller Dusan**.
