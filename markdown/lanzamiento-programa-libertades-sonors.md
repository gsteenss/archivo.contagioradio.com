Title: Nace Libertades Sonoras, un espacio para deconstruir estereotipos
Date: 2017-03-27 15:35
Category: Libertades Sonoras, Mujer
Tags: feminismo, genero, Libertades Sonoras, mujeres
Slug: lanzamiento-programa-libertades-sonors
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/LibertadesFinal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [27 Mar. 2017]

Durante 8 años,  a través del trabajo que desarrollamos desde Contagio Radio, hemos apostado por **la comunicación con enfoque de género**,  una labor que comprendemos esencial para avanzar en la **construcción de una sociedad no sexista e incluyente. **Le puede interesar: [Las Mujeres tejen paz desde los territorios](https://archivo.contagioradio.com/las-mujeres-tejen-paz-desde-los-territorios/)

En este camino y gracias a las voces de mujeres y hombres hemos querido dar un paso más allá y apostar por un nuevo programa. **Libertades Sonoras, un espacio para deconstruir estereotipos socioculturales,** para aportar a la construcción de la paz,  para bailar, para cantar, para romper las brechas de nuestra sociedad. Le puede interesar: [Los retos del 2017 para enfrentar la violencia contra las mujeres en Colombia](https://archivo.contagioradio.com/los-retos-del-2017-enfrentar-la-violencia-las-mujeres-colombia/)

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F10154267556690812%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
