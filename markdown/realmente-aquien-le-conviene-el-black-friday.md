Title: ¿A quién le conviene el “Black Friday”?
Date: 2015-11-27 17:49
Category: Economía, El mundo
Tags: Black Friday, capitalismo, Ciberlunes, Compras, consumo, dinero, economia, Eduardo Rincón, Ética, Finanzas, Finanzas personales, Viernes Negro
Slug: realmente-aquien-le-conviene-el-black-friday
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/black-friday-line.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: mediatrends.es 

<iframe src="http://www.ivoox.com/player_ek_9531575_2_1.html?data=mpqgk5qbeY6ZmKiak5WJd6Kol5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaSmhqeztMrFsM7Zz9nSjcbVucqZpJiSo57Sb83ZjMjc0NvNqc%2FZjMrZjYqpdoaskYqmpafQpcTfjKvfy8nFvYa5k4qlkoqdiIanp5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Eduardo Rincón] 

###### [27 Nov 2015] 

Desde hace un par de años, Colombia se ha sumado a la larga lista de países que alrededor del mundo participan del “Viernes Negro”, fecha en que inaugura de forma oficial la temporada de compras navideñas, donde las empresas ofrecen a los consumidores bajos precios en todos sus productos. Sin embargo, ¿qué consecuencias morales y económicas puede tener la celebración de esta fecha en la sociedad?

El profesor e investigador Eduardo Rincón manifiesta que fechas como el “Black Friday” o el “Cyber Lunes”, son estrategias de mercado que sirven para expandir los brazos del capital y generar más pobreza en medio de la sensación de riqueza, puesto que “hemos creado unas necesidades que no necesitamos consumir”.

Además, Rincón afirma que antes de comprar artículos compulsivamente, los consumidores deben plantearse dos preguntas: **“¿Qué es lo que realmente necesitamos y qué es lo que se esconde detrás de la mercancía qué compramos?**”, ya que según dice el profesor, debemos ser conscientes acerca de las implicaciones morales y económicas que tiene caer en la lógica del consumo y la sobreacumulación.

En ese orden de ideas, el investigador contó que el ejercicio de compras en jornadas como estas “es ilusorio” y les da a las personas una sensación de empoderamiento sobre aquello que puede adquirir, pero que finalmente quedan vacías y más endeudadas. Asimismo, resalta que en estas fechas se incentiva el uso del crédito, lo que para Rincón “no solo genera beneficios para los productores de las mercancías, sino para las entidades bancarias”.

Frente al Black Friday, organizaciones como Ecologistas en Acción, han planteado realizar un día sin compras durante esa fecha. Esta protesta se organiza desde hace 23 años y plantea manejar políticas y comportamientos que correspondan a la economía responsable.
