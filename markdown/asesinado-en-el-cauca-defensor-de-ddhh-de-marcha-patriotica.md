Title: Asesinado en el Cauca Defensor de DDHH de Marcha Patriótica
Date: 2016-11-02 15:54
Category: DDHH, Nacional
Tags: Amenazas, Asesinado Líder de Marcha Patriotica, Caloto, Cauca, Derechos Humanos, marcha patriotica, víctimas
Slug: asesinado-en-el-cauca-defensor-de-ddhh-de-marcha-patriotica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Foto-Lider-asesinado-marcha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [2 Nov. de 2016]

El pasado 1 de Noviembre, **en la Vereda Santa Rita** sobre la vía intermunicipal que comunica el Resguardo Indígena de Toez y el centro poblado de El Corregimiento El Palo, Cerca de las 6:40 de la tarde,  **fue encontrado asesinado el campesino Defensor de DDHH de Marcha Patriótica, Jhon Jairo Rodríguez Torres.**

Según informaciones de organizaciones sociales, **el cuerpo presentaba tres impactos de arma de fuego** y a un costado del cuerpo se encontró una motocicleta DT en la que Jhon Jairo transitaba antes de ser asesinado.

Cristian Delgado, de la red de Derechos Humanos Francisco Isaías Fuentes aseguró que **Jhon Jairo, era un campesino de 34 años, padre de 4 hijos y se desempeñaba como integrante de la Junta de Acción Comunal del Corregimiento del Palo y del Movimiento Social y Político Marcha Patriótica.**

**El líder campesino no tenía amenazas, por lo que las asociaciones y movimientos de los que era parte recibieron con asombro la noticia de su asesinato** “Los paramilitares han realizado varias amenazas en contra de las asociaciones campesinas, de las asociaciones indígenas de la ACIN, del CRIC, de dirigentes de defensores de derechos humanos” aseveró Delgado. Le puede interesar: [En menos de un mes han sido asesinados 11 líderes sociales en Colombia](https://archivo.contagioradio.com/en-menos-de-un-mes-han-sido-asesinados-11-lideres-sociales-en-colombia/)

Según las primeras declaraciones de la red de Derechos Humanos el tratamiento que las autoridades le han dado a este caso ha sido “nefasto”.

“El comandante de Policía de Cauca ha puesto en duda que Jhon Jairo hacia parte del movimiento Marcha Patriótica, además de las preocupaciones que tenemos nos toca acreditarle al señor comandante la pertenencia al movimiento. Adicionalmente el CTI ha descalificado al compañero, diciendo que tenía antecedentes y que la moto encontrada al lado del cuerpo era robada, lo cual no es cierto” y añade que “**se está orquestando un nuevo acto de impunidad y se está revictimizando a las víctimas de este hecho”.**

Familiares, amigos, allegados y organizaciones sociales han manifestado que se encuentran preocupados ante este nuevo caso en el que pierde la vida un líder campesino, **“vamos a seguir insistiendo en que este hecho no puede quedar en la impunidad” concluyó Delgado.**

Según cifras del movimiento Marcha Patriótica, **120 integrantes de este movimiento han sido asesinados en cuatro años, de los cuales cuatro han sido en Cauca. **Le puede interesar: [Frustran atentado contra Piedad Córdoba, lideresa de Marcha Patriótica](https://archivo.contagioradio.com/frustrado-atentado-contra-piedad-cordoba-lideresa-de-marcha-patriotica/)  
<iframe id="audio_13586736" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13586736_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
