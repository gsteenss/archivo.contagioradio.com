Title: Usme y Tunjuelito también podrían ser afectadas con suspensión de SITP
Date: 2017-10-11 16:42
Category: DDHH, Nacional
Tags: ciudad bolivar, SITP
Slug: ciento-sesenta-mil-personas-afectadas-por-suspension-de-servicio-de-sitp-en-ciudad-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/diego-pinto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Diego Pinto] 

###### [11 Oct 2017] 

Son más de 160 mil personas las que se ven afectadas por la suspensión del servicio del Sistema Integrado de Transporte Público (SITP) en Bogotá, y podrían ser más según el concejal Manuel Sarmiento, **que aseguró que localidades como Usme y Tunjuelito también podrían ser afectadas.** Además la actitud del distrito ha sido reprochable puesto que no tenía previsto un plan de contingencia eficaz que garantizara la movilidad de los habitantes de la localidad.

De acuerdo con el concejal Manuel Sarmiento, es urgente que se estructure un plan de contingencia o localidades como Usme y Tujuelo serían las próximas en verse afectadas. Sarmiento denunció que otros concejales en diferentes sesiones del Consejo de Bogotá ya **habían advertido de lo que pasaría con el sistema de Transporte público de la capital y su colapso, debido a los problemas financieros**. (Le puede interesar: ["Transporte público de Bogotá puede ser suspendido por crisis de 4 empresas"](https://archivo.contagioradio.com/transporte-publico-de-bogota-puede-ser-suspendido-por-crisis-de-4-empresas/))

Para Sarmiento, la actitud que ha tenido el alcalde Peñalosa frente a esta situación es displicente y menosprecia el sufrimiento de los ciudadanos, “es inaudito que sabiendo que esto se iba a venir, porque al operador se le suspende el servicio al no tener renovación de su póliza, no se establece algún plan de contingencia, **sino que el gobierno distrital lo que le dice a la comunidad es miren haber como resuelven**” afirmó el concejal del Polo Democrático.

### **Ciudad Bolívar y el olvido distrital** 

De acuerdo con Lindon Arévalo, integrante de la Mesa Local de Paz, de Ciudad Bolívar, esta problemática de la suspensión del SITP se suma al resto de situaciones que deben afrontar los habitantes de este territorio, **como los malos olores del Relleno Sanitario de Doña Juana, la contaminación del Río Tunjuelo y la falta de vías al interior de los barrios**.

“La gente tiene que asumir sus propios medios de transporte, sobretodo en la zona alta, hay transporte que nos colabora de forma particular, son el único medio de transporte que ha existido” aseguro Arévalo, además agregó que, por día los habitantes de esta local gastan entre **\$.6.000 y \$8.000 pesos en transporte público para ir a sus trabajos y regresar a sus viviendas**.

Esto **debido a que las personas deben pagar otro transporte ilegal, que los acerque a sus viviendas**, debido a que las rutas alimentadoras o de buses no cubren por completo los barrios de las periferias. (Le puede interesar: ["Conductores de Transmilenio trabajan más de 16 horas: Ugetrans"](https://archivo.contagioradio.com/conductores-de-transmilenio-trabajan-mas-de-16-horas-ugetrans/))

De igual forma afirmó que la gran mayoría de los habitantes de esta localidad, trabajan en el norte lo que les obliga a tener que salir de sus casas hacia las **4:00 am para poder llegar a sus trabajos a las 7:00 o 8:00 am y salir a las 6 pm** para llegar sobre las 9 o 10 de la noche. Es decir que un habitante de Ciudad Bolívar invierte cerca de 5 horas diarias en el transporte público y en condiciones indignas.

### **La crisis anunciada del SITP** 

El concejal Sarmiento aseguró que el paso siguiente es realizar una intervención al operador SUMA y comenzar a mirar otros operadores que puedan prestar el servicio en Ciudad Bolívar, sin embargo, señaló que **la misma situación se podría presentar en otras localidades como Usme**, debido al estado de quiebra en el que también se encuentran otros operadores.

“Tomar soluciones a corto plazo es muy difícil, las comunidades se verán afectadas en el servicio de transporte y aunque una intervención **puede tardar bastantes meses, es lo único que se puede hacer para tener una solución de fondo**” afirmó Sarmiento.

De igual forma, alerto sobre la capacidad de Transmilenio ya que ahora deberá suplir la oferta para los usuarios del SITP, **lo que podría aumentar el caos debido al colapso de este transporte público en la capital**. (Le puede interesar: ["Sin estudios completos aprobarían cupo de endeudamiento para Transmilenio por la Séptima"](https://archivo.contagioradio.com/sin-estudios-completos-aprobaria-cupo-de-endeudamiento-para-transmilenio-por-la-carrera-septima/))

A su vez, el representante a la Cámara Alirio Uribe, también solicitó a la Procuraduría que se investigue las razones de las suspensiones del SITP en Ciudad Bolívar y a los funcionarios que aún sabiendo de las dificultades comunicadas por el operador, no advirtieron con tiempo a la comunidad, **ni pusieron en marcha el plan de contingencia que evitará la crisis que ahora viven miles de habitantes**.

<iframe id="audio_21407195" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21407195_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_21408858" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21408858_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
