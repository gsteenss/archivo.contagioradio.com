Title: Fiscal Martínez debe declararse impedido para investigar caso Odebrecht
Date: 2017-02-15 15:26
Category: Judicial, Nacional
Tags: Fiscal Martínez, Jorge Robledo, Odebrecht y Navelena
Slug: fiscal-martinez-declararse-impedido-investigar-caso-odebrecht
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Néstor-Humberto-Martínez-y-Jorge-Robledo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Feb 2017] 

Durante una rueda de prensa, el Senador Jorge Robledo presentó pruebas que involucran al fiscal Néstor Humberto Martínez en el caso Odebrecht, las razones van desde los vínculos con una firma de Luis Carlos Sarmiento Angulo, en los **contratos de la Ruta del Sol y el préstamo irregular de \$120.000 millones** que el Banco Agrario hizo a Navelena y Odebrecht, el senador **exigió que el fiscal se declare impedido para investigar el caso.**

Las pruebas que Robledo reunió junto a José Roberto Acosta de Justicia Tributaria, destaparon el caso Navelena y ello llevó a que se encontrara nueva información sobre los contratos de la **Ruta del Sol y el otrosí de la vía Ocaña - Gamarra, financiado en parte por la firma Episol, de Sarmiento Angulo** y que figura como socia de Odebrecht en el contrato, ello “representa un impedimento para su accionar (del fiscal Martínez) como ente investigador en este caso”, puntualizó Robledo.

### Fiscal estaría violando la Ley 

Robledo señaló que Martínez incurre en una falta grave “al no declararse impedido para investigar a Odebrecht”, y sostiene que “el fiscal asesoró a Sarmiento en la compra del diario El Tiempo y participó en su junta directiva (...) así como en la compra de Megabanco, BAC Credomatic y Promigas, entre otras cosas” **lo que probaría la existencia de una amistad entre el fiscal y el conglomerado empresarial.** ([Le puede interesar: Odebrecht la punta del iceberg de la corrupción en Colombia](https://archivo.contagioradio.com/odebrecht-la-punta-del-iceberg-de-la-corrupcion-el-colombia/))

El senador, resalta que de acuerdo con la ley 1497 de 2011 “la amistad entrañable es causal de conflicto de interés (...) y que el fiscal viola la ley cobijado por el entorno que lo rodea”.

Robledo aseguró que el fiscal debió retirarse de la investigación a Odebrecht **“desde que se supo que tiene que ver con el contrato de navegabilidad del Río Magdalena”**, el cual está en cabeza de la empresa Navelena, cuyo mayor accionista es la multinacional brasilera.

“Su firma asesoró a Navelena, tal y **como consta en el extracto de una presentación sobre el proyecto de recuperación del Río, enviado el 23 de julio de 2015** por Corficolombiana al Banco Agrario”, dijo Robledo en la rueda de prensa.

\[caption id="attachment\_36345" align="alignnone" width="438"\]![Pruebas presentadas por Jorge Robledo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/WhatsApp-Image-2017-02-15-at-12.15.29-PM.jpeg){.size-full .wp-image-36345 width="438" height="646"} Pruebas presentadas por Jorge Robledo\[/caption\]

![WhatsApp Image 2017-02-15 at 12.15.28 PM (2)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/WhatsApp-Image-2017-02-15-at-12.15.28-PM-2.jpeg){.alignnone .size-full .wp-image-36346 width="1018" height="544"} ![WhatsApp Image 2017-02-15 at 12.15.28 PM (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/WhatsApp-Image-2017-02-15-at-12.15.28-PM-1.jpeg){.alignnone .size-full .wp-image-36347 width="923" height="214"} ![WhatsApp Image 2017-02-15 at 12.15.28 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/WhatsApp-Image-2017-02-15-at-12.15.28-PM.jpeg){.alignnone .size-full .wp-image-36348 width="1094" height="800"}

Además, sostuvo que “en la presentación del proyecto a cargo del Consorcio Navelena S.A.S. **“se indica que la asesoría legal e institucional del mismo se encuentra a cargo de la firma Martínez Neira Abogados”**, firma del fiscal Néstor Humberto Martínez que hoy dirige su hijo Camilo Martínez Beltrán. ([Le puede interesar: Fiscal no prevarica si investiga el](https://archivo.contagioradio.com/fiscal-investiga-odebrecht/)caso Odebrecht)

### Los "elefantes" que no vio el fiscal 

Por otra parte, Robledo revelo que hubo varios “elefantes” que entraron en el caso de Navelena y el Banco Agrario, “no vio que en el caso de Navelena también aparece el nombre de Otto Bula” y tampoco se percató “ que Corficolombiana, había sido quien había tramitado en el Banco Agrario el crédito de los \$120.000 millones”.

Indicó que es urgente la declaración de impedimento por parte del fiscal, pues **“los colombianos estamos mamados de que nunca pase nada”**, comentó que Nestor Martínez fue elegido por el Gobierno Santos como Fiscal y que “siempre ha sido respaldado por el Centro Democrático”, lo que puede traducirse en “una inevitable impunidad”.

Por último, la Procuraduría señaló que hay suficiente evidencia para tomar determinaciones e investigar a otras personas como:

-   Francisco Solano Mendoza, presidente del Banco Agrario, de la época.
-   Andrés Escobar Arango, delegado del ministro de Hacienda.
-   Arturo Adolfo Dajud Durán, delegado del ministro de Agricultura.
-   Juan Luis Hernández Celis, representante del Gobierno Nacional
-   Luis Fernando Mejía Alzate, representante accionistas mayoritarios
-   Francisco Estupiñan Heredia, miembro independiente
-   César Pardo Villalba, miembro Independiente
-   Luis Eduardo Gómez Álvarez, miembro Independiente
-   Hernando Enrique Gómez Vargas, vicepresidente Jurídico del Banco Agrario.
-   Marcela Ferrán Muñoz, vicepresidente de Crédito y Cartera.
-   Mónica Santamaría Salamanca, vicepresidente de Banca Comercial
-   Moisés Mahecha, vicepresidente de Riesgos
-   Luis Francisco Ogliastri, vicepresidente de Operaciones
-   Omar Arango Páez, gerente Banca Comercial

### La Carta de Otto Bula

[Otto Bula desmintió al Fiscal Martínez, envió un manuscrito al Consejo Nacional Electoral en el que dice **“no es cierto, ni me consta, ni he dicho que el dinero que le entregué a Andrés Giraldo** fuera un aporte a la campaña Santos presidente o al señor Juan Manuel Santos”.]

El pasado 7 de febrero el fiscal Néstor Humberto Martínez, en rueda de prensa indicó que el testimonio de Otto Bula apuntaba a que **"había entregado dineros de sobornos de Odebrecht a Andrés Giraldo"** y que tenían como destinatario al gerente de la campaña presidencial de reelección “Santos Presidente”, Roberto Prieto.

En la carta, Bula también asegura que está dispuesto a rendir declaración, "**en el momento que el CNE lo requiera, bajo la gravedad de juramento"** tal como lo hizo ante la Fiscalía General.

###### Reciba toda la información de Contagio Radio en [[su correo]
