Title: Películas animadas sobre la importancia del ambiente
Date: 2016-11-15 09:28
Author: AdminContagio
Category: 24 Cuadros
Tags: Ambiente, animadas, Películas
Slug: peliculas-animacion-mensaje-ambiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/minusculos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Studio Ghibli 

##### 10 Nov 2016 

A través de sus películas, algunos directores han buscado enviar un mensaje en favor de la protección del ambiente. En ese camino, han encontrado en la animación una gran herramienta por medio de la cual pueden construir **historias fantásticas donde animales, plantas e incluso minerales cobran vida para alertar a los seres humanos**.

Además de ser un recurso técnicamente poderoso, **el cine animado tiene la gran cualidad de atraer a las audiencias más jóvenes**, siendo ellos en quienes el mensaje debe calar con mayor fuerza, si el propósito es producir cambios reales en el destino del planeta.

Seleccionamos **8 producciones** que por su contenido, relacionado con la defensa del ambiente, y por su calidad son ideales para compartir con toda la familia. Le puede interesar: [Cinco documentales que relatan el desastre de la gran minería](https://archivo.contagioradio.com/cinco-documentales-que-relatan-el-desastre-de-la-gran-mineria/).

-   M**inúsculos el valle de las hormigas perdidas (2013)**

![Minusculos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/minusculos.jpg){.alignnone .size-full .wp-image-32238 width="715" height="402"}

Co-producción entre Francia y Bélgica, en un pacífico bosque dos grupos de hormigas encuentran los restos de un picnic, lo que provocará una batalla entre ambos, particularmente por una caja de azúcar, una clara muestra de lo que ocurre cuando los desechos humanos alteran la vida de los ecosistemas.

-   **Pompoko (1994)**

![Pompoko](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/pompoko01.jpg){.alignnone .size-full .wp-image-32239 width="1200" height="686"}

Película japonesa del reconocido Studio Ghibli, dirigida por Isao Takahata, en ella un grupo de mapaches que habitan en un bosque frondoso que colinda con la ciudad, intentarán sabotear mágicamente el trabajo de los hombres que pretenden urbanizar su hogar.

-   **Lorax en búsqueda de la trufula perdida (2012)**

![Lorax](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/the-lorax-960x623.jpg){.alignnone .size-full .wp-image-32240 width="960" height="540"}

Basada en el libro escrito por el Dr Seuss en 1971, narra la historia de Lorax, un personaje encargado de vigilar el Valle de Trúfula, un espacio rico en flora y fauna, que deberá defender de las intenciones de "Una vez", de talar el bosque para obtener beneficios económicos.

-   **Vecinos Invasores (2006)**

![vecinos\_invasores](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Vecinos_Invasores.jpg){.alignnone .size-full .wp-image-32242 width="700" height="400"}

Del estudio Dreamworks, la película habla de cómo los animales del bosque ven alterada su cotidianidad cuando es construido un conjunto residencial junto a su hábitat y cómo estar tan cerca de los seres humanos y sus productos, transforma sus comportamientos naturales.

-   **Erase una vez un bosque (1993)**

![ONCE UPON A FOREST, Abigail, Russell, Edgar, 1993, TM and Copyright (c)20th Century Fox Film Corp. All rights reserved.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/541df6296234a255eee6760c28d19bb3-e1479216674636.jpg){.alignnone .wp-image-32243 .size-full width="1000" height="610"}

Con un fuerte mensaje sobre la contaminación ambiental, la cinta acompaña el viaje de una ratona, un erizo, un topo y un tejón, para buscar algunas plantas medicinales que puedan salvar a sus familiares y amigos afectados por la propagación de gas tóxico en el bosque donde viven.

-   **Rango (2011)**

![Rango](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/0eb0a-rango5.jpg){.alignnone .size-full .wp-image-32244 width="1024" height="576"}

Rango, es un camaleón doméstico que accidentalmente cae del vehículo en el que viajaba mientras cruzaba el desierto, en su trasegar llega al pueblo de Dirt, donde es nombrado sheriff y acogido como la última esperanza para enfrentarse a las oscuras intenciones de monopolizar el agua por parte del alcalde y sus feroces secuaces.

-   **Wall-e (2008)**

![wall e](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/walle-ground-laserdot-700x348.jpg){.alignnone .size-full .wp-image-32245 width="700" height="348"}

Es el año 2800 y Wall e, un pequeño robot que tiene la misión de limpiar el planeta tierra, devastado por la cantidad de basura producida durante cientos de años. Su cotidianidad cambiará cuando se encuentra con EVE, otra robot exploradora con la que vivirá una gran aventura por la galaxia.

-   **El bosque animado (2001)**

![el bosque animado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/05.El-bosque-animado_ampliacion.jpg){.alignnone .size-full .wp-image-32246 width="800" height="523"}

Basada en la novela homónima de 1943, escrita por el español Wenceslao Florez, la historia se centra en las especies. animales y vegetales, que habitan el bosque y en su relación con los humanos o, en el caso de la comunidad de árboles, con un nuevo vecino, un poste eléctrico, para enseñarnos la importancia de respetar el mundo que nos rodea.
