Title: Consejo de Estado frena desvío del Arroyo Bruno
Date: 2016-12-18 11:44
Category: Ambiente, Voces de la Tierra
Tags: arroyo Bruno, Consejo de Estado, Consulta Previa, indígenas, Wayuu
Slug: consejo-de-estado-frena-desvio-arroyo-bruno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/wayuu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 Dic. 2016]

En un fallo de segunda instancia el Consejo de Estado decidió ratificar la sentencia del Tribunal Administrativo de la Guajira que le **ordena a la multinacional Cerrejón suspender la intervención del Arroyo Bruno en este departamento de Colombia.**

Con esta decisión **son cerca de 27 comunidades quienes se verán beneficiadas** y a quienes Cerrejón, el Ministerio del Interior y la Autoridad Nacional de Licencias Ambientales -ANLA- debe realizar una consulta previa en un tiempo máximo de 1 mes.

Con este fallo **el Consejo de Estado ampara derechos fundamentales como la salud, el respeto por el territorio y el acceso al agua potable de las comunidades Wayúu.**

Esta decisión se da luego de una acción interpuesta por Lorenza Marcela Gil Pushaina, quien pertenece a una de las comunidades indígenas afectada por el desvío del Arroyo Bruno. Le puede interesar: [El Cerrejón desalojaría 80 indígenas para desvíar Arroyo Bruno](https://archivo.contagioradio.com/cerrejon-desalojaria-80-indigenas-desviar-arroyo-bruno/)

En la sustentación de tutela que fue presentada, se argumentó que **debido a la desviación del Arroyo Bruno los indígenas están viéndose afectados y sufriendo daños que podrán ser irreparables**, de modo que se están vulnerando derechos fundamentales de las comunidades y desconociendo su ancestralidad y su importancia como cultura. Le puede interesar: [¿Quién gana y quién pierde con la desviación del Arroyo Bruno?](https://archivo.contagioradio.com/quien-gana-y-quien-pierde-con-la-desviacion-del-arroyo-bruno/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
