Title: Águilas Negras amenazan a líderes sociales en Ciudad Bolívar
Date: 2018-08-14 17:35
Category: DDHH, Nacional
Tags: amenaza, Defensoría del Pueblo, lideres sociales, Panfletos
Slug: aguilas-negras-amenaza-ciudad-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/ciudad-bolivar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Galo Naranjo] 

###### [14 Ago 2018] 

Los líderes **Orlando García y Christian Robayo en Ciudad Bolívar,** sur de Bogotá, denunciaron una serie de amenazas de muerte que recibieron a través de mensajes de texto por parte de las **Águilas Negras.** Sin embargo, la situación vivida por ambos no es excepcional, puesto que la Defensoría del Pueblo ya había alertado sobre la delicada situación que se vive en esa localidad, en su [Alerta Temprana Nº30](http://www.defensoria.gov.co/es/nube/noticias/7257/Alerta-Temprana-de-Inminencia-030-18-para-la-localidad-de-Ciudad-Bol%C3%ADvar-en-Bogot%C3%A1.htm).

Las amenazas contra García comenzaron con panfletos que llegaron el 16 de junio, un día antes de la primera vuelta de los comicios presidenciales; misma intimidación que se repetiría el sábado 14 de julio. El último hecho que denuncian, tuvo lugar **el miércoles 8 de agosto, cuando tanto García como Robayo recibieron un mensaje de texto de las Águilas Negras en el que los advertían de su muerte.**

Christian Robayo es edil de la localidad por el Polo Democrático y trabaja con Orlando García para lograr el reconocimiento de barrios periféricos desde el Colectivo Vamos por los Derechos. Adicionalmente, trabajan en el tema de movilidad, Derechos Humanos y apoyando el Sistema de Alertas Tempranas (SAT) de la Defensoría del Pueblo.

### **Durante 2017 se registraron 841 solicitudes de protección en Bogotá** 

En la alerta temprana Nº 26, la Defensoría del Pueblo registró y verificó **28 amenazas contra líderes sociales en la Capital durante 2017**, adicionalmente, rastreó 841 solicitudes de protección en la capital del país. La misma entidad emitió una alerta (Nº30) en abril de este año en la que pidió que se tomaran medidas urgentes para proteger a los habitantes de Arborizadora, Lucero, Perdomo, El Tesoro y Jerusalén.

En esta alerta, se advierte de la presencia de las autodenominadas Autodefensas Gaitanistas de Colombia; Los Rastrojos; Las Aguilas Negras; Los Paisas; el ELN; y posibles integrantes de estructuras declaradas en disidencia de las FARC-EP; en Ciudad Bolívar, hecho que constituye un factor de riesgo para "jóvenes en condición de alta vulnerabilidad socioeconómica", poblaciones socialmente estigmatizadas y líderes sociales.

Aunque los panfletos y comunicaciones amenazantes ya son materia de investigación por parte de la Fiscalía y demás organismos de justicia, Robayo afirmó que es necesario establecer medidas de protección inmediatas e **inversión social en la localidad, que combata la pobreza y brinde oportunidades para los jóvenes.** (Le puede interesar: ["Así se estarían reclutando jóvenes en Bogotá"](https://archivo.contagioradio.com/asi-se-estarian-reclutando-jovenes-en-bogota/))

<div>

</div>

<iframe id="audio_27823099" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27823099_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<div>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

</div>

<div class="osd-sms-wrapper">

</div>

<div class="permalink-in-reply-tos" data-component-term="in_reply_to">

</div>

<div class="permalink-inner permalink-tweet-container ThreadedConversation ThreadedConversation--permalinkTweetWithAncestors">

</div>
