Title: "Los proyectos" de Charles Baudelaire
Date: 2015-08-31 11:07
Category: Viaje Literario
Tags: 148 años fallecimiento Baudelaire, Charles Baudelaire, Los proyectos (microcuento), Poetas malditos
Slug: los-proyectos-de-charles-baudelaire
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/baudelaire.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_7710805_2_1.html?data=mJyekp2UeY6ZmKiakpqJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRdZWsjMaSpZiJhpLj1JDRx9GPqsLgzcrQy9LNqc%2Fo0JDRx5CnrMLmzcrgjafFucXZzcbW1MqRb63j1JCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Charles Baudelaire- Los proyectos] 

###### [31 Ago 2015] 

Se cumplen 148 años del fallecimiento del escritor francés Charles Pierre Baudelaire,  fue también crítico de arte y traductor. Paul Verlaine lo incluyó entre los *poetas malditos*, debido a su vida bohemia y de excesos, y a la visión del mal que impregna su obra.  Fue el poeta de mayor impacto en el simbolismo francés. Las influencias más importantes sobre él fueron Théophile Gautier, Joseph de Maistre (de quien dijo que le había enseñado a pensar) y, en particular, Edgar Allan Poe, a quien tradujo extensamente.

A menudo se le acredita de haber acuñado el término "modernidad" (*modernité*) para designar la experiencia fluctuante y efímera de la vida en la metrópolis urbana y la responsabilidad que tiene el arte de capturar esa experiencia.
