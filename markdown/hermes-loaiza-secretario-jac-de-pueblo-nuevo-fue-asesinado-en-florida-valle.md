Title: Hermes Loaiza, secretario  JAC de Pueblo Nuevo, fue asesinado en Florida, Valle
Date: 2020-06-01 11:52
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Asesinato de líderes comunales, juntas de acción comunal, Valle del Cauca
Slug: hermes-loaiza-secretario-jac-de-pueblo-nuevo-fue-asesinado-en-florida-valle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Hermes-Loaiza.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Hermes Loaiza / Super Noticias

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En horas de la noche del pasado 31 de mayo fue asesinado Hermes Loaiza Montoya, secretario de la Junta de Acción Comunal de Pueblo Nuevo Florida, Valle del Cauca. Según organizaciones como la [Fundación Paz y Reconciliación](https://twitter.com/parescolombia) de los 360 asesinatos contra líderes y lideresas sociales, ocurridos entre noviembre de 2016 y febrero de 2020, **el 44% corresponde a hombres y mujeres pertenecientes a organizaciones comunales.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según versiones preliminares, los hechos ocurrieron en su vivienda cuando dos hombres jóvenes habrían golpeado a su puerta y preguntado por él, luego de que su esposa abriera la puerta, cuando el líder comunal salió a atenderlos se habría iniciado una discusión con las dos personas que finalizó cuando dispararon con arma de fuego en dos ocasiones contra Hermes. [(Le recomendamos leer: Asesinan a Tailor Gil, líder comunal de Cáceres)](https://archivo.contagioradio.com/asesinan-a-tailor-gil-lider-comunal-de-caceres/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque afirman que Hermes Loaiza no había recibido amenazas en su contra, aún se investiga si los hechos habrían sido consecuencia de su labor como secretario de la JAC, una tarea que ha sido visiblemente atacada en departamentos como Antioquia, Nariño, Norte de Santander , Putumayo, Córdoba, Cauca, Caquetá, Valle del Cauca y Chocó.[(Lea también: Asesinan a líder Aramis Arenas Bayona en Becerril, Cesar)](https://archivo.contagioradio.com/asesinan-a-lider-aramis-arenas-bayona-en-becerril-cesar/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Durante el mes de mayo fueron asesinados al menos cinco integrantes de Juntas de Acción Comunal,** así lo advirtió Guillermo Cardona desde la Confederación Nacional de Acción Comunal, quien señaló que a partir del octubre de 2017 se dio un incremento en la violencia contra las Juntas de Acción Comunal por promover la sustitución voluntaria. [(Lea también: En lo corrido de mayo han sido asesinados cinco integrantes de juntas de acción comunal)](https://archivo.contagioradio.com/en-lo-corrido-de-mayo-han-sido-asesinados-cinco-integrantes-de-juntas-de-accion-comunal/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo...

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
