Title: Restos de Magistrado Urán serán entregados dignamente a su familia
Date: 2018-08-10 15:15
Category: Nacional, Sin Olvido
Tags: entrega de restos, Palacio de Justicia
Slug: entrega-restos-carlos-uran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/magistrado-uran-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 10 Ago 2018 

Después de 33 años, parte de los restos del jurista y politólogo **Carlos Horacio Urán Rojas**,  magistrado auxiliar durante la toma y retoma del Palacio de Justicia,ocurrida el 5 y 6 de noviembre de 1985, **serán entregados a sus familiares**.

La identificación, resultante del trabajo realizado por la Fiscalía General de la Nación junto al Instituto de Medicina Legal y Ciencias Forenses, fue posible **tras realizar una segunda exhumación de los restos y un cotejo del dedo índice junto con el ADN de sus hijas**.

El caso del magistrado Urán, como el de varios de los trabajadores de la cafetería y otros funcionarios, confirma una vez más **el manejo irregular con el que se realizó el levantamiento de los restos**, confirmada por el grupo de investigación que en 2014 cuando aseguró que la mezcla de cuerpos y su genética, produjo la mala entrega a los familiares.

Carlos Horacio Urán tenía 42 años y preparaba su examen doctoral en ciencia política en la Universidad de Paris. mientras se desempeñaba como magistrado auxiliar del Consejo de Estado, participando en la composición de sentencias condenatorias a las Fuerzas Militares. En 1985, se concluyó que su muerte fue causada por laceración cerebral por herida en el cráneo con un arma de fuego, sin embargo, **su cuerpo presentaba lesiones en brazos y piernas que no correspondían con lo descrito en el informe inicial**.

En 2005, la Fiscalía reabrió el caso y dos años después se encontró la billetera de Urán perforada por una bala en una bóveda secreta del Cantón Norte del Ejército en Bogotá. Adicionalmente, un video revelado por Noticias Uno, registraba la salida del magistrado con vida del edificio, a pesar de que inexplicablemente fue encontrado entre los cadáveres abatidos en la toma.

Según declaraciones y pruebas recopiladas se indicó que el magistrado se encontraba en el baño del tercer y cuarto piso, sin embargo, r**egistros del levantamiento demuestran que su cuerpo fue hallado en el primer piso**, por lo que las circunstancias reales de su muerte siguen siendo investigadas.

Los resultados de la identificación serán expuestos el 10 de agosto de 2018 por medio de un informe médico legal que determina su identificación plena. **El sábado 11 de agosto, sus despojos mortales serán entregados dignamente a sus familiares **en un acto que tendrá lugar en la capilla Nuestra Señora de Fátima del Colegio San Bartolomé La Merced.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
