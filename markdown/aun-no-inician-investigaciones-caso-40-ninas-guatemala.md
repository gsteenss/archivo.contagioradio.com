Title: Aún no inician investigaciones en caso de 40 niñas de Guatemala
Date: 2017-03-13 15:27
Category: DDHH, El mundo, Mujer
Tags: 40 niñas de Guatemala, Feminicidio de Estado
Slug: aun-no-inician-investigaciones-caso-40-ninas-guatemala
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/niñas_guatemala.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MVS] 

###### [13 Mar 2017] 

La Red de Sanadoras Ancestrales del Feminismo Comunitario de Guatemala, denunció a través de un comunicado que el nùmero de niñas víctimas del incendio en el Hogar Virgen de la Asunción, subió a 40, este lunes 4 de ellas fueron trasladadas a un hospital en Galveston Texas para recibir atención. El pasado fin de semana, las y los guatemaltecos salieron a las calles para exigir que se inicien las investigaciones de **"las causas que llevaron a las niñas a protestar quemando los colchones"** y pidieron la renuncia del presidente de Guatemala, Jimmy Morales.

Lolita  Chávez, una de las integrantes de la Red, manifestó, que ni el actual, ni anteriores gobiernos se han preocupado por combatir las causas que llevan a estas niñas, niños y jóvenes a llegar a los Hogares Refugio, **“estas menores, provenientes de familias empobrecidas, algunas indígenas, con historias de violencia sexual, abortos forzados, embarazos por violencia sexual, trata,** reclutamiento forzado de las maras”, en  muchas ocasiones optan por internarse de manera voluntaria en estos centros.

### El Pueblo de Guatemala exige justicia en las Calles 

Chávez comentó que durante el fin de semana, en distintos puntos de la Ciudad de Guatemala, se reunieron para velar a las 40 niñas, **"con velas blancas, flores y rezos, quisimos iluminar el camino de estas niñas que murieron exigiendo respeto".** Además, cientos de personas entre ellas familiares, habitantes solidarizados con dicha causa, organizaciones defensoras de derechos humanos, de mujeres y feministas, se movilizaron por las principales calles exigiendo que el Estado de Guatemala asuma su responsabilidad, que se de inicio a las investigaciones de las causas de la protesta de las niñas y la renuncia del presidente Jimmy Morales.

La Red, confirmó que las niñas llevaban poco más de una semana "encerradas bajo llave, sin alimentación, no las dejaban bañarse, eso las llevo a exigir sus derechos". En la movilización, los participantes llevaban pancartas pidiendo también **castigo a los implicados en los abusos sexuales y torturas que sufrían las niñas en el Hogar, y que entre otras, las llevaron a realizar la protesta en la que perdieron la vida.**

La Red hizo un llamado para que se conformen Comisiones Internacionales que presionen al Gobierno de Jimmy Morales y se de celeridad a las investigaciones,  señalan que el caso de las 40 niñas, no puede quedarse en la impunidad como ha pasado con otros casos de crímenes de estado en ese país, "**vemos que no es un problema sólo de Guatemala, sino un fenómeno de América Latina, por eso pedimos la solidaridad** de los países hermanos” puntualizó Lorena Cabral, otra de las integrantes de la Red de Sanadoras. ([Le puede interesar: Muerte de niñas en Guatemala “fue un crimen de Estado”](https://archivo.contagioradio.com/muerte-de-ninas-en-gutemala-fue-un-crimen-de-estado/))

### Fue un Crimen de Estado 

Revelaron que los funcionarios de La Secretaria de Bienestar Social, encargadas de éste tipo de centros de protección a menores, **“ganan salarios impresionantes, por compadrazgos y pagos de favores políticos con el gobierno de Jimmy Morales”**, indican que hay pruebas de contratos realizados a personal “sin la formación la experiencia y capacidad para el abordaje de las graves problemáticas de niñez y juventud en éste país”.

Por último, invitaron a que organizaciones y sociedad civil en otros lugares del mundo apoyen estas exigencias y se movilicen pidiendo justicia en el caso de estas 40 menores. También invitan a que quienes deseen **“enciendan veladoras blancas para acompañar a las niñas en el camino de su dimensión espiritual y acompañar el duelo de las familias** y del pueblo de Guatemala que demanda Justicia”.

### Estos son los nombre de las menores: 

1.  Kimberly Odalis castillo Rodríguez, 15 años
2.  Anabela Maribel Leal Rax, 17 años
3.  Yesenia Paola Barrios, 14 años
4.  Jazmín Noemí Vásquez, 14 años
5.  Emily del Cid, 16 años
6.  Yamilet Cu Luan, 15 años
7.  Rosmery López, 16 años
8.  Brenda Carrizal, 16 años
9.  Noemí Fuentes Minas, 16 años
10. Eva Rosa Antun Leal, 16 años
11. JeniferJulisaMacarteKorea, 14años
12. María Vanesa Torres Suchité, 15 años
13. Indira Jarisa Pelicó
14. Daria Dalila López Meda
15. SoniaHernándezGarcía
16. MayraAidéChutánUrías
17. SkarlethYajairaPérezJiménez
18. Yohana Deciré Cuy Urizar
19. Sarvia Isel Barrios Bonilla
20. Ana Nohemí Morales Galindo
21. Wendy Anai Vividor Ramirez, 16 años
22. Velveth Judith Aguirre Elias, 13 años
23. Erika Elizabeth Aguirre Elias, 15 años
24. Erick Eliseo Aguirre Elias, 17 años
25. LorenaSanchezPereira, 14años
26. Yusbeli Jubitza Merari Makin Gomez, 14años
27. Osbin Daniel Gomez Jimenez, 11 años.
28. Rosa Julia Espino Tobar
29. Indira Jarisa Pelicó
30. AshelyGabrielaMéndezRamírez
31. MayraAidéChutánUrías
32. Rosalinda Victoria Ramírez Pérez
33. Madelin Patricia Hernández Hernández
34. Pendiente Inacif (Instituto Nacional de Ciencias Forenses) informe su identidad
35. Pendiente Inacif, (Instituto Nacional de Ciencias Forenses) informe su identidad
36. Pendiente Inacif, (Instituto Nacional de Ciencias Forenses) informe su identidad
37. Pendiente Inacif, (Instituto Nacional de Ciencias Forenses) informe su identidad
38. Pendiente Inacif, (Instituto Nacional de Ciencias Forenses) informe su identidad
39. Pendiente Inacif, (Instituto Nacional de Ciencias Forenses) informe su identidad

###### Reciba toda la información de Contagio Radio en [[su correo]
