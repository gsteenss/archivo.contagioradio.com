Title: 120 defensores de DDHH asesinados en 14 meses en Colombia: Defensoría
Date: 2017-03-03 18:48
Category: DDHH, Nacional
Slug: 120-defensores-asesinados-defensoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Paramilitarismo1-e1459970325314.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: archivo] 

###### [03 Mar 2017]

La Defensoría del Pueblo afirma que en los últimos 14 meses han sido asesinados 120 defensores de DDHH, además hay 433 amenazas y situaciones de riesgo, 33 atentados y 27 agresiones en 20 departamentos del país. Todas estas situaciones se presentan en **territorios que antes eran controlados por las FARC y se han cometido contra defensores de DDHH y líderes sociales.**

Una de las situaciones que resalta comunicado de la Defensoría es que las víctimas son de organizaciones de DDHH y líderes sociales que realizan su trabajo en regiones en que antes operaban las FARC, y que están siendo **ocupadas por organizaciones criminales que se disputan el dominio territorial y económico**. ([Lea también: Asesinada Ruth Alicia López en Medellín](https://archivo.contagioradio.com/asesinada-lideresa-asokinchas-medellin/))

Según el comunicado es necesario “tomar acciones urgentes para blindar moralmente y proteger a las posibles víctimas, organizaciones y movimientos sociales” puesto que hay una situación de riesgo **generalizada por las circunstancias de modo, tiempo y lugar en las que se han cometidos los crímenes**. ([Le puede interesar: Sigue en aumento el asesinato de defensores de DDHH en Colombia](https://archivo.contagioradio.com/?s=ase))

En ese sentido, el Defensor del Pueblo, Carlos Negret hizo un llamado urgente a la **fiscalía para que, en el marco de la unidad especial presente en el Acuerdo de Paz, realice las investigaciones acerca de estas violaciones de los DDHH** y se logren desmantelar las organizaciones criminales a partir de los trabajos de inteligencia que deberían realizarse. ([Lea tambien: Lider Eder Cuetia asesinado en Cauca](https://archivo.contagioradio.com/asesinado-eider-cuetia-lider-de-derechos-humanos-36907/))

El comunicado también indica que en los próximos días se entregará un **“informe especial de riesgo”** en el que precisarán las condiciones de “modo tiempo y lugar” en que han venido ocurriendo las agresiones con la idea de que se apliquen las medidas necesarias para evitar más daños irreparables contra los colombianos.

\[caption id="attachment\_37212" align="aligncenter" width="408"\]![defensoría](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/C6BtCEgWcAA9nDa.jpg){.wp-image-37212 width="408" height="517"} Comunicado Defensoría del Pueblo\[/caption\]

###### Reciba toda la información de Contagio Radio en [[su correo]
