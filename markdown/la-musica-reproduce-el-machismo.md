Title: ¿La música reproduce el machismo?
Date: 2016-12-14 17:39
Category: Mujer, Nacional
Tags: misoginia, música y machismo, sexismo, violencias de género
Slug: la-musica-reproduce-el-machismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/la-musica-y-el-machismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Dic 2016 ] 

En los últimos días la canción “Cuatro Babys” de Maluma generó gran conmoción debido a que contenido de su letra ha sido calificado como violento, sexista y que **legitima desde el machismo, entre otras, las violencias sexuales contra las mujeres.**

Distintas lideresas de organizaciones de mujeres y feministas iniciaron una campaña recolectando firmas para que el exponente de reggaetón retire su canción de todos los medios, y por otra, el cantante señaló en una entrevista que **"si la gente pide la canción es porque les gusta y no dejaré de cantarla".**

Otras, manifiestan que la discusión no ha de centrarse sólo en dicho género musical, es necesario despojarse de dobles morales y comprender que si la verdadera incomodidad con el reggaetón es la misoginia, habría que **revisar y criticar con igual vehemencia muchas piezas musicales de géneros como bolero, vallenato, salsa, merengue, rock, pop o tango, que no son menos violentos** por no inducir a los movimientos tan criticados por algunas élites.

Los sectores más radicales han planteado la total prohibición del género de raíces puertorriqueñas, e incluso en países como México se han gestado **grupos juveniles que han perseguido y asesinado a otros jóvenes seguidores del género.** La pregunta inevitable es si ¿la solución es prohibirlo?, empero, puede que eliminar el síntoma no cure, sino que oculte.

Algunas feministas han llegado a la conclusión que las personas no se convierten en misóginos por el mero hecho de escuchar una canción, las letras ofensivas contra las mujeres son un reflejo de la sociedad en que vivimos, no son causa sino síntoma. **Frente a ello la educación tiene un papel preponderante, para que los contenidos sexistas y violentos cambien,** habría que hacer un trabajo integral, no sólo con jóvenes, sino con la población en general para que existiera un resultado efectivo.

Muchos y muchas a lo largo de su vida, habrán escuchado distintas canciones con contenido machista, sin embargo, **cuestiones culturales y morales han hecho que las violencias sean naturalizadas y leídas como cotidianas.**

Canciones como **el bolero de Agustín Lara** donde declara que “tu párvula boca \[…\] siendo tan niña me enseñó a besar”, **el romántico pop anglosajón de Sting** que habla de un amor posesivo: “Every breath you take I’ll be watching you” (a cada respiro que des te estaré observando) o la canción de los Beatles llamada “Run for your life”, “Corre por tu vida” en la que un hombre persigue a una mujer amenazándola con feminicidio porque le fue infiel (“Well I’d rather / see you dead, little girl / Than to be with another man / You better keep your head, little girl / Or you won’t know where I am”) **son sólo algunos ejemplos** de la posibilidad de que a través de la música se reproducen y legitiman violencias hacia las mujeres.

###### Reciba toda la información de Contagio Radio en [[su correo]
