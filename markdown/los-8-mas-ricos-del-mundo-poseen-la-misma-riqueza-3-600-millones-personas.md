Title: Los 8 más ricos del mundo poseen la misma riqueza que 3.600 millones de personas
Date: 2017-01-17 10:33
Category: Uncategorized
Tags: Brexit, desigualdad, Oxfam, PIB, suiza
Slug: los-8-mas-ricos-del-mundo-poseen-la-misma-riqueza-3-600-millones-personas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/desigualdad.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [Plataforma Arquitectura] 

##### [16 Ene 2016]

En el marco del Foro Económico Mundial que se realiza en Davos, Suiza, la Organización Internacional OXFAM , presentó un informe en el que da cuenta de la grave situación de desigualdad que existe en el planeta, donde **8 hombres poseen la misma riqueza que la mitad de la población mundial.**

Bill Gates, fundador de Microsoft, Amancio Ortega, fundador de Inditex y propietario de la cadena de tiendas de moda española Zara, Warren Buffett, director ejecutivo y principal accionista de la empresa estadounidense Berkshire Hathaway; Carlos Slim Helu, propietario del mexicano Grupo Carso; Jeff Bezos, fundador y director ejecutivo de la empresa estadounidense Amazon; Mark Zuckerberg: presidente, director ejecutivo y cofundador de la empresa estadounidense Facebook, Larry Ellison, Cofundador y director ejecutivo de la empresa estadounidense Oracle; y Michael Bloomberg, fundador propietario y director ejecutivo de la empresa estadounidense Bloomberg LP, poseen la misma riqueza que la mitad más pobre de la población mundial, es decir, 3.600 millones de personas.

**En los últimos 25 años, el 1% de los multimillonarios ha acumulado más ingresos que el 50% más pobre de la población mundial**, y desde 2015, el 1% de los habitantes del planeta poseen más riqueza que el resto del mundo. Eso quiere decir, que entre 1988 y 2011 los ingresos del 10% más pobre de la población han aumentado menos de 3 dólares al año, mientras que el 1% más rico se han incrementado 182 veces más.

La publicación expone que las diez mayores corporaciones del mundo, según una lista que incluye a WalMart, Shell y Apple, tienen una facturación superior a los ingresos públicos de 180 países en conjunto, entre los que se encuentran Irlanda, Indonesia, Israel, Colombia, Grecia, Sudáfrica, Irak y Vietnam.

### ¿Por qué incrementa la desigualdad? 

El documento titulado **"Una economía para el 99% de la población",** evidencia cómo las prácticas de grandes empresas y los más ricos están acentuando la desigualdad, con lo que señala que es innegable que el modelo de economía globalizada ha beneficiado principalmente a las personas más ricas.

Las red de paraísos fiscales para eludir el pago de los impuestos, y la contratación de asesores financieros para garantizar altos rendimientos en las inversiones, son algunas de las prácticas más usuales de los más ricos para continuar acumulando riquezas. También,  Oxfam afirma que "más de la mitad de los mil millonarios del mundo ha heredado su fortuna o la ha acumulado gracias a su participación en industrias en las que la corrupción y el nepotismo son prevalentes".

El informe también revela cómo los **multimillonarios utilizan su dinero e influencia para que leyes y políticas se vuelvan a su favor**. En esa medida, se refieren a ejemplos como, Brasil, donde los milmillonarios han tratado de influir en las elecciones y han ejercido presión con éxito para lograr una drástica reducción de su contribución fiscal, mientras en Kenia las empresas petrolíferas han conseguido generosos privilegios fiscales.

### Las consecuencias de la extrema desigualdad 

Con ese panorama, Oxfam advierte que se incrementa la delincuencia y la inseguridad, obstaculizando la lucha contra la pobreza y generando que cada vez más personas vivan con más miedo y menos esperanza. Ejemplo de ello, es lo que **sucedió con el Brexit y el éxito de la campaña presidencial de Donald Trump,** hechos que se alimentan con el incremento del racismo y la desafección generalizada que genera la política convencional provocan que cada vez más ciudadanos de los países ricos den muestras de que no están dispuestos a seguir aguantando la situación actual.

Esta situación, **afecta particularmente a las mujeres,** pues son ellas las que además reciben los salarios más bajos, sufren mayores niveles de discriminación en el sector laboral y asumen la mayor parte del trabajo de cuidados no remunerado. [(Le puede interesar: igualdad de género en el mundo podría retrasarse 170 años)](https://archivo.contagioradio.com/igualdad-salarial-hombres-mujeres-se-retrasa-170-anos/)

### ¿Qué hacer? 

Teniendo en cuenta que las multinacionales, representan un elemento vital de la economía que podrían constituir un factor esencial para construir sociedades prósperas y justas. Oxfam propone que se plantee un modelo positivo frente a aparentes soluciones que generan mayores diferencias. Para ello, se necesitan gobiernos que apuesten por una visión de futuro y respondan ante la ciudadanía, y que las las empresas antepongan los intereses de trabajadores y productores.

De esta manera se podría propiciar un crecimiento dentro de los límites del planeta, el respeto de los derechos de las mujeres, y que el sistema fiscal sea justo y progresivo. Es posible avanzar hacia una economía más humana.

Mientras tanto, la ONG manifiesta que los recursos existentes permitirían **eliminar tres cuartas partes de la pobreza extrema si se incrementa la recaudación fiscal y se recorta el gasto militar y otros gastos igualmente regresivos.**

<iframe id="doc_81171" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/336793340/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-XlNFZZd30HvAscD4Lpth&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

######  

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
