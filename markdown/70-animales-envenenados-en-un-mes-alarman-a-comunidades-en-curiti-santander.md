Title: 70 animales envenenados en un mes alarman a comunidades en Curití, Santander
Date: 2016-06-30 13:12
Category: Animales
Tags: curití, derechos de los animales, envenenamiento animales, Santander
Slug: 70-animales-envenenados-en-un-mes-alarman-a-comunidades-en-curiti-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Curití.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [30 Junio 2016 ] 

De acuerdo con un poblador, en el último mes **70 animales, entre perros y gatos, han muerto por envenenamiento** en Curití, Santander. Las comunidades sospechan que se trate de un veneno agrícola que están usando en estado puro, porque los animales no alcanzan a llegar con vida a la veterinaria o llegan en tan mal estado que no responden a los antídotos suministrados. Así mismo, se denuncia que en las calles del municipio han regado salchichas con alfileres.

Un episodio similar ocurrió hace dos años en una de las veredas de este municipio, y se determinó que el responsable fue un comisario. Pese a la renuencia en este tipo de hechos, **la alcaldía no ha atendido integralmente la problemática**, sólo ha establecido recompensas para quienes denuncien a los responsables, y por su parte, la Policía asegura estar investigando la situación.

Organizaciones defensoras de los derechos de los animales y **pobladores rechazan este tipo de hechos porque han cercenado la vida de animales** tanto callejeros como los que viven en casas, por lo que adelantan iniciativas comunicativas para denunciar el hecho y llamar la atención de la ciudadanía frente al cuidado de animales.

<iframe src="http://co.ivoox.com/es/player_ej_12080259_2_1.html?data=kpedmpWWeZqhhpywj5WdaZS1kZeah5yncZOhhpywj5WRaZi3jpWah5yncaXZz9rbxc7Fb8bi18rbx9PFscrZz9ncjcnJb9HZ09fc1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
