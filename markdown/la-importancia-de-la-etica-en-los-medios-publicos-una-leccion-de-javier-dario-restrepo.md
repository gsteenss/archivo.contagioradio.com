Title: La importancia de la ética en medios públicos, una lección de Javier Darío Restrepo
Date: 2019-01-24 18:36
Author: AdminContagio
Category: Nacional
Tags: FLIP, Libertad de Prensa, RTVC
Slug: la-importancia-de-la-etica-en-los-medios-publicos-una-leccion-de-javier-dario-restrepo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/javier-dario-restrepo-libertad-prensa-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 23 Ene 2019 

[El experto en ética periodística, Javier Darío Restrepo hace un análisis sobre la importancia de la libertad de expresión y el correcto uso que se le debe dar a los medios públicos en el país a propósito de las grabaciones que dio a conocer la **Fundación para la Libertad de Prensa (FLIP)**, ratificando que el gerente de RTVC, Juan Pablo Bieri buscaba imponer una censura sobre el programa de Señal Colombia, 'Los Puros Criollos' y su presentador, Santiago Rivas.]

### **La obtención de la grabación** 

[El catedrático propone de fondo inicialmente una problemática relacionada a la  forma en la que fue obtenida la grabación en la que Bieri habla de cambiar el horario de ‘Los Puros Criollos’, ¿fue obtenida de una manera honesta, de modo que la persona que hablaba supiera que era grabado? se pregunta el maestro quien recuerda que la forma en que el material se obtiene debe tener en cuenta el derecho a la intimidad. ]

[“Unas de las fuentes de desconfianza de las fuentes es cuando se hace de forma clandestina  o no autorizada, sin el prejuicio de que hay alguien grabando” afirma el maestro quien se pregunta en qué momento se autorizó que el audio se convirtiese en un documento público.[(Le puede interesar: Sí hubo censura a Santiago Rivas y los Puros Criollos ) ](https://archivo.contagioradio.com/si-hubo-censura-a-santiago-rivas-puros-criollos/)  
]

### **Libertad de expresión** 

[En contraste, Javier Darío Restrepo explica que si ‘Los Puros Criollos’ fuese un programa noticioso, Rivas estaría en la obligación de conservar los hechos y dejar de lado sus opiniones, sin embargo en este caso en particular un programa en el que el presentador apela a la crítica sin acudir a la calumnia “presentando unas realidades en la forma en que estimaba eran más convenientes y comunicables”, es legítimo y hace uso del derecho a la libertad de prensa y el respeto a la opinión.]

[Ante el contenido de la grabación y la forma en que el gerente de RTVC sugiere acabar con el programa cultural, Restrepo señala que si el programa fue contratado, “**se debe respetar la palabra dada y a los contratos firmados, un respeto que no solo se le debe a la institución y al país”**.]

###  **El rol de lo público** 

[El experto hace énfasis en que los medios públicos son precisamente para el público y que el Gobierno “**no tiene derecho a tomar los asuntos públicos como si fueran asuntos personales o políticos”**  pues como servicio público está al servicio de la población y esta es la única que puede juzgar su contenido.  
  
El maestro indica que un gobernante que toma los medios “como si fueran su forma de expresión publicitaria y los convierte en su voz, asume una actitud dictatorial”, haciendo un paralelo con los medios de comunicación, Restrepo alude a la figura del ágora griega, un instrumento de democracia a través del cual los gobernantes entraban en contacto con la población, **“ahí está el medio para criticar, aplaudir o criticar, no es para elogiar al Gobierno ni hacer propaganda, es para permitir a la población expresarse”**, concluye.]

<iframe id="audio_31759412" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31759412_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
