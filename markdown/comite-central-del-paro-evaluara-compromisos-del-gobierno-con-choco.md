Title: Comité central del paro evalua preacuerdos del Gobierno con Chocó
Date: 2016-08-22 13:48
Category: Movilización, Nacional
Tags: Chocó, Movilizaciones, paro
Slug: comite-central-del-paro-evaluara-compromisos-del-gobierno-con-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Paro-en-Quibdó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Caracol ] 

###### [22 Ago 2016 ] 

Durante estos 6 días de paro en Quibdó, organizaciones sociales y ciudadanos convocantes han logrado que se instalen **diez mesas de concertación en las que acuerdan con el Gobierno compromisos** para el cumplimiento de sus exigencias en materia de servicios públicos, salud, educación, infraestructura, seguridad y corrupción. De acuerdo con el líder Emilio Pertúz, este lunes se reunirán con dos ministros para evaluar con el comité central los acuerdos a los que se han llegado.

El Gobierno se comprometió con el pago de todos los pasivos del Hospital San Francisco de Asís, calculados en \$37  mil millones, para que el Hospital pueda continuar prestando sus servicios; mientras que la Gobernación se comprometió a avanzar en la **creación del Nuevo Hospital San Francisco de Asís** y para mediados de 2017 deberá funcionar el nuevo modelo de atención diferencial de salud en el departamento.

Frente a la exigencia de solucionar la disputa entre Chocó y Antioquia por el municipio de Belén de Bajirá, el Gobierno se comprometió a respaldar el concepto técnico que el IGAC entregó a las Comisiones de Ordenamiento Territorial de Cámara y Senado con el que **se resuelve el conflicto a favor de Chocó**.

En materia de servicios públicos, el Gobierno se comprometió a inaugurar en los próximos dos meses un acueducto que **beneficiara al 95% de la población quibdoseña de forma continúa**, así como a culminar las obras de otros 10 acueductos municipales y cofinanciar los proyectos de acueductos que estén en fase III. Para la prestación de servicio de energía el Gobierno avanzará en la **conexión de 5 de los 11 municipios que no cuentan con el suministro** para garantizar la interconexión en todo el departamento.

Los compromisos en materia de seguridad tienen que ver con el aumento del pie de fuerza en las zonas de más alta criminalidad, con la activación de Gaula Militar y de Élite, la entrega de las cuatro estaciones de policía que están en obra, así como la **creación de un observatorio de derechos humanos y criminalidad**, y un centro de atención especializada para el menor infractor.

En relación a la lucha contra la corrupción, el Gobierno aseguró que se compromete a fortalecer la Comisión de Moralización en cabeza de la Secretaria de Transparencia y la Fiscalía General de la Nación, y frente a la adecuación de vías **se comprometió a financiar los 70 km necesarios para la terminación de la pavimentación** de las vías Quibdó - Medellín y Quibdó - Pereira.

Pertúz asegura que desde las 2 de la tarde de este lunes se reunirá el comité central de la movilización para evaluar si estos compromisos responden a las [[demandas de atención estructural por las que la población se ha movilizado](https://archivo.contagioradio.com/es-indignante-que-el-gobierno-no-tome-enserio-el-paro/)], por lo que exigen que hayan organismos internacionales como las Naciones Unidas, y nacionales como la Procuraduría y la Defensoría para garantizar que el Estado colombiano cumpla con sus compromisos.

<iframe src="http://co.ivoox.com/es/player_ej_12626917_2_1.html?data=kpejlJuddZihhpywj5aUaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncabhytHW0ZC0qdPohqigh6elvoampJC5h6iXaaK4xcrfjcjMs8TjwtPcj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
