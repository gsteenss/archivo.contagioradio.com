Title: Asesinado en Cauca, Hector William Mina, líder e integrante de Marcha Patriótica
Date: 2017-07-14 16:59
Category: DDHH, Nacional
Tags: asesinato de líderes sociales, Fiscalía General de la Nación, marcha patriotica, Paramilitarismo
Slug: asesinado-hector-william-mina-marcha-patriotica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/HECTOR-WILLLIAM-MINA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: REDFIC-ContagioRadio] 

###### [14 Jul 2017]

A través de un comunicado difundido por la Red de Derechos Humanos Francisco Isaías Cifuentes, se denunció que este 14 de Julio fue asesinado el líder afrodescendiente Hector William Mina, integrante de Marcha Patriótica y líder de la región. **El hecho sucedió en el Barrio Jorge Eliecer Gaitán de esa localidad del departamento del Cauca**.

Según la Red de Derechos Humanos, hacia la 11:30 de la mañana, Hector William Mina fue alcanzado por disparos de arma de fuego que le propinaron 4 sujetos que llegaron hasta la puerta de su lugar de residencia en Guachené. Aunque fue trasladado de inmediato a la Clínica Valle de Lili, Mina falleció por la gravedad de las heridas generadas por los disparos. [Le puede interesar: Asesinado integrante de sindicato en Valle del Cauca](https://archivo.contagioradio.com/presidente-de-sintrainagro-fue-asesinado-en-el-valle-del-cauca/).

Con Mina la cifra de personas asesinadas, entre líderes sociales y defensores de Derechos Humanos asciende a 182 desde el 2016, y hasta el momento no se ha implementado **ningún mecanismo que impida que sigan presentándose hechos tan lamentables** para el contexto de construcción de paz en las regiones y en el país, aseguran diversos líderes sociales.

De acuerdo con la documentación que ha hech**o Marcha Patriótica se registraron 116 asesinatos durante el año 2016 y 65 en lo que va corrido de este año.** Razón por la cual rechazaron el informe de la Fiscalía en que se asegura que los defensores de DDHH asesinados son menos y que ya se ha avanzado en el esclarecimiento de cerca del 50% de los casos. Le puede interesar: [Informe de Fiscalía sobre asesinatos de líeres sociales es "otro falso positivo".](https://archivo.contagioradio.com/informe-de-fiscalia-sobre-asesinatos-a-defensores-de-dd-hh-es-otro-falso-positivo/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
