Title: Armas de las FARC se van convirtiendo en homenaje y memoria: Carlos Lozada
Date: 2017-03-02 16:50
Category: Otra Mirada, Paz
Tags: Dejación de armas, FARC, ONU, paz
Slug: armas-de-las-farc-se-van-convirtiendo-en-homenaje-y-memoria-carlos-lozada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Carlos-Antonio-Lozada-Constituyente-FARC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RemapValle ] 

###### [02 Mar. 2017] 

**Julián Gallo Cubillos, alias “Carlos Antonio Lozada”**, tiene 55 años, un voz tranquila y que evoca esperanza. Lleva 38 años en la guerrilla de las FARC y estuvo presente en las fallidas conversaciones de 1999 en Colombia. En esta nueva etapa de diálogos encabezó la subcomisión técnica para el fin del conflicto en donde se encontró cara a cara con integrantes de las fuerzas militares, con quienes se enfrentó en la guerra.

**Lozada ahora es el encargado del proceso de dejación de armas y en entrevista para Contagio Radio, relató lo que esto significa para un integrante de las FARC**, los procedimientos y los alcances de este momento histórico que arranca y que es irreversible.

**Contagio Radio (CR):** Desde que estuvimos en la X conferencia en el Yarí nos dimos cuenta que tenían casi como su compañero permanente a su fusil ¿Qué está significando para las FARC empezar a dejar las armas?

**Carlos Lozada (CL):** Nosotros consideramos que **este proceso de dejación de armas va mucho más allá** del simple hecho de que los combatientes de la insurgencia vamos a dejar de usar las armas en el ejercicio de la política. Nosotros consideramos que eso también se hace extensivo en el sentido de no involucrar más armas en la política a todos los sectores, que han estado participando de una u otra forma en el complejo conflicto social y político colombiano. Es decir, **no hace referencia a la dejación de armas única y exclusivamente a la insurgencia, creemos que el Estado debe retirar las armas de la política** interna y dedicar las armas para la defensa de la soberanía, de la integridad del territorio, de los recursos naturales de nuestro país. Es decir, no es solamente la visión unilateral, que es solo la insurgencia la que va a hacer dejación de las armas. **Lo que representa este momento es el compromiso que asumimos, y que lógicamente vamos a cumplir** plenamente cuando firmamos el Acuerdo de Paz, que busca garantizar que no se repita nunca más está tragedia que hemos tenido que vivir los colombianos durante las últimas décadas.

**Contagio Radio:** ¿Cuál es procedimiento a seguir para la dejación de las armas?

**Carlos Lozano:** El procedimiento es el siguiente, en el Acuerdo se contempla que el proceso de dejación de las armas tiene distintos momentos, el primero se denomina el registro, y es digamos tomar nota de las armas que van a ser depositadas bajo custodia de las Naciones Unidas. Luego viene una parte que se denomina control de armamento, que sería ya el registro de las armas que durante el tiempo de permanencia en las Zonas Veredales van a tener los combatientes, es decir el arma individual con la cual permanecerán hasta que se termine definitivamente ese proceso y otra etapa consiste en trasladar las armas de artillería como las ametralladoras, los morteros, los lanzagranadas, que por lo general no están con las unidades. Esas armas serán llevadas a las Zonas Veredales y **en la medida en que se van cumpliendo los plazos acordados pues irán pasando poco a poco a control del componente internacional** del Mecanismo de Monitoreo y Verificación. Le puede interesar: [Inició proceso de dejación de armas y “es irreversible”: FARC](https://archivo.contagioradio.com/ya-comenzo-el-proceso-de-dejacion-de-armas-y-es-irreversible/)

**CR:** Iván Márquez ha hablado de artillería de las FARC y hacía referencia a unos misiles de doble tiempo. Usted puede decirnos ¿cómo ha sido ese proceso de elaboración, de tenencia y de uso de las armas?

**CL:** A lo largo de la confrontación nosotros logramos desarrollar varios aspectos del armamento popular, y nos hemos comprometido que el proceso de dejación de armas involucra absolutamente todo el armamento, de manera que **en la medida que se vayan agotando los plazos y cumpliendo los compromisos pactados mutuamente, pues así mismo nosotros iremos dando a conocer y colocando bajo control del componente internacional el armamento.** Precisando que este es un acuerdo que contempla que esta parte de la dejación de armas involucra exclusivamente a las FARC y al componente internacional. Porque las fuerzas militares y el Estado no van a tener nada que ver en esta parte. En contexto: [Gobierno y FARC acordaron puntos para discutir cese bilateral y dejación de armas](https://archivo.contagioradio.com/gobierno-y-farc-acordaron-puntos-para-discutir-cese-bilateral-y-dejacion-de-armas/)

**CR:** Dentro de este proceso y en medio de las evidentes dificultades ¿Qué es lo que tendría que hacerse de inmediato para que este proceso no vaya a tener algún tipo de tropiezo?

**CL:** Hay una serie de compromisos acordados y nosotros podemos **resumir las preocupaciones que tenemos en este momento** y que ronda la cabeza de las FARC de esta manera:

**La primera pues obviamente es la continuación de los asesinatos sistemáticos contra los dirigentes populares y los defensores de DD.HH**. Ese desangre contra los colombianos debe parar. Hace falta un mayor compromiso del Estado colombiano para dar garantías a la movilización y a las organizaciones sociales.

**La segunda es el incremento de los paramilitares** que cada día copan más áreas, ante la mirada a veces complaciente de algunas unidades de las fuerzas armadas. No podemos decir que sean todas las fuerzas armadas, pero sí tenemos y conocemos información precisa de la connivencia de algunas unidades militares con estos grupos.

**La tercera preocupación es lentitud en la implementación de lo que tiene que ver con la Ley de Amnistía e Indulto,** a esta hora ya después de dos meses de aprobadas esas leyes deberían estar en la calle los más de 4 mil combatientes de las FARC y milicianos que se encuentran en prisión, y los más de 5 mil colombianos que deben ser beneficiados que están prisioneros por participar de la lucha y la protesta social. Le puede interesar: [Duro reclamo de las FARC al gobierno por incumplimientos al acuerdo](https://archivo.contagioradio.com/duro-reclamo-de-las-farc-al-gobierno-por-incumplimientos-al-acuerdo/)

**CR:** A propósito del tema de la connivencia de la fuerza militar con paramilitares ¿ustedes ya hicieron una denuncia a la comisión de implementación?

**CL:** Ese es un tema que de manera recurrente se ha venido tratando en diversos espacios, donde nos encontramos con funcionarios y representantes del Gobierno y del Estado colombiano. **Hemos manifestado nuestra preocupación y aportado pruebas de ese incremento de la presencia paramilitar en algunas zonas**, con información que nos llega de las comunidades.

**CR:** Las armas se entregan, se dejan en poder de las Naciones Unidas y se van a hacer 3 monumentos ¿ya tienen la persona que los hará?

**CL:** Este tema de la construcción de los monumentos para nosotros tiene una significación y una trascendencia muy grande, en la medida en que queremos significar con estos monumentos la recuperación de la memoria de los miles de colombianos que fallecieron en el conflicto armado. **Esperamos que ese monumento sea un referente para las futuras generaciones**. En cuanto a la elaboración tenemos propuestas de colombianos muy destacados que nos han mostrado sus propuestas, pero ahora no puedo decir sus nombres, lo que si es que reflejan y sintetizan la idea de lo que nosotros consideramos debe ser ese testimonio histórico.

**CR:** ¿Qué puede pasar después con los integrantes de las FARC cuando se complete la dejación de armas, hay algún tipo de organización comunitaria, cooperativas?¿Cómo será la vida?

**CL: El acuerdo contempla a partir de la llegada a las Zonas se va a iniciar todo el proceso de reincorporación de las FARC a la sociedad**, tanto en lo económico, político y social de acuerdo con nuestros intereses. En lo político, apenas completemos la dejación de las armas presentaremos al país nuestra propuesta política. En lo económico, se dará la creación de una gran cooperativa que hemos denominado “*Economías del común”,* que generará proyectos productivos y además pretendemos que se convierta en lo que puede ser el desarrollo de una economía comunitaria alternativa. Además con esta iniciativa queremos que se beneficien los excombatientes, sus familias y las comunidades.

**CR:** Para Carlos Lozada ¿Qué ha sido lo más bonito o esperanzador de este proceso de paz?

**CL:** Hay muchas cosas que uno pudiera narrar, pero voy a referirme rápidamente a 3 cosas que realmente para mí han significado hechos trascendentales de esta etapa del proceso. La primera es la posibilidad de interlocutar    con oficiales de las fuerzas armadas (…), sin ellos este proceso no hubiera podido llegar a buen término. El segundo, **ver a los combatientes con sus familias, hemos podido ver cómo después de 10, 15 y hasta más de 30 años los combatientes se pueden reencontrar con sus seres, eso conmueve y nos hace entender el anhelo de paz**. El último es ver cómo las Zonas Veredales se están convirtiendo en un punto de peregrinación, a donde llegan para felicitarnos por la decisión tomada y nos ofrecen su solidaridad. Le puede interesar:[ Nueva ola de solidaridad con las Zonas Veredales Transitorias](https://archivo.contagioradio.com/nueva-ola-de-solidaridad-con-las-zonas-veredales-transitorias/)

<iframe id="audio_17324489" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17324489_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
