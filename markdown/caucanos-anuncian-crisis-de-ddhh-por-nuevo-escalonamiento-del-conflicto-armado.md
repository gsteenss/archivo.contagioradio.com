Title: Escalamiento del conflicto causaría crisis de DDHH en el Cauca: Habitantes
Date: 2015-04-16 17:18
Author: CtgAdm
Category: Nacional, Paz
Tags: Bombardeo, Cauca, Cese al fuego, Derechos Humanos, FARC, francisco isaias cifuentes, la esperanza, lizeth montero, paz, red, Santos
Slug: caucanos-anuncian-crisis-de-ddhh-por-nuevo-escalonamiento-del-conflicto-armado
Status: published

###### Foto: Primicia Diario 

<iframe src="http://www.ivoox.com/player_ek_4365563_2_1.html?data=lZijl5qad46ZmKiakpyJd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8LpxMbb0diPpc%2Fpz8jWw9OPp9Pd1M7gjcnJb6W4qa2Y0tTWb8%2FpxtvcjcrXp8Lg0NPOz87Jso6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Lizeth Montero, Red de Derechos Humanos Francisco Isaias Cifuentes] 

Para las y los habitantes de Departamento del Cauca, los anuncios del presidente Santos la tarde del miércoles 16 de abril, en que ordenó reactivar los bombardeos contra campamentos de la guerrilla de las FARC, son "sumamente preocupantes y desesperanzadores".

La abogada Lizeth Montero afirma que "nosotros como organizaciones sociales y populares, que hemos padecido a lo largo de la historia los rigores del conflicto social y armado, sabemos que es un retroceso muy significativo, y anunciamos y denunciamos lo que va a suceder: la agudización de la crisis en términos de Derechos Humanos por cuenta de estos anuncios, y la materialización de el copamiento militar de nuestros territorios".

Y es que los habitante del Cauca saben que con la reactivación de los bombardeos, (que no solo afectan a los grupos guerrilleros sino al territorio en el que se producen), se vivirá un nuevo escalonamiento del conflicto, se agudizarán los ametrallamientos indiscriminados que afectan a la población civil, los empadronamientos, la retenciones ilegales, los montajes judiciales y las ejecuciones extrajudiciales.

Afirma la abogada que "había sido un alivio para los caucanos la declaración de cese unilateral, en la medida en que había logrado una reducción muy significativa de las acciones de confrontación". Ante los anuncios realizados por el alto gobierno, declara Montero, retorna a la población la sensación generalizada de "sosobra, malestar, incomodidad, inseguridad, y constante de terror, en que han vivido muchos años sumidas las comunidades del Cauca".
