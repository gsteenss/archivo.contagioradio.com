Title: Comunidades inician 9° 'Pedaleada por la Memoria, la Justicia Restaurativa y la Construcción de Paz'
Date: 2016-07-01 14:48
Category: Paz, Sin Olvido
Tags: Huila, La Palestina, Novena Pedaleada por la paz, Red CONPAZ
Slug: comunidades-inician-9-pedaleada-por-la-memoria-la-justicia-restaurativa-y-la-construccion-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/La-Palestina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Palestina ] 

###### [1 Julio 2016 ] 

Desde Valle, Meta, Caquetá y otras regiones, llegan este viernes a La Palestina, Huila, comunidades, organizaciones y ciudadanos a hacer parte de la **novena versión de la 'Pedaleada por la Memoria, la Justicia Restaurativa y la Construcción de Paz'**, liderada por Familiares de Víctimas de la UP, la Red CONPAZ y la Comisión Intereclesial de Justicia y Paz.

De acuerdo con Ana María Chimonja, de la Red CONPAZ, la Pedaleada inicia en Pitalito, se traslada hasta la orilla del río Guarapa y llega a La Palestina a la Zona de Biodiversidad La Esperanza, dónde **se instalará un monumento y se realizará un acto de dignificación para las víctimas** de la Unión Patriótica Huila.

"Cada vez son más los que vienen a acompañarnos en estas bicicletadas, **a aprender cómo cuidar nuestros ríos y el medio ambiente**, cómo enamorarse de la vida y la biodiversidad y también ha hacer memoria de nuestras víctimas para recordar sus iniciativas", asegura Ana María.

<iframe src="http://co.ivoox.com/es/player_ej_12094094_2_1.html?data=kpedm5mUfZWhhpywj5WYaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncaLiwpC6w9eJh5SZoqnOjajMrc7jz8_OjZKPh7DCsabHj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]

 
