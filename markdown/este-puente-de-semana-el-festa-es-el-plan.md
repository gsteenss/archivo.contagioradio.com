Title: ¡Este fin de semana el FESTA es el plan!
Date: 2016-03-18 19:04
Category: Cultura, Nacional
Tags: arte, Corporación Colombiana de Teatro, FESTA, teatro alternativo
Slug: este-puente-de-semana-el-festa-es-el-plan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/arton223.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FESTA] 

###### [18 Mar 2016] 

Veinte  grupos diferentes y 35 funciones que se llevarán a cabo en 6 escenarios distintos, harán parte de la programación de este puente en el Festival de Teatro Alternativo de Bogotá.

El sábado 19 de marzo, el FESTA tendrá programación para todo tipo de público, desde el más chico hasta el más grande podrá disfrutar de la programación gratuita que se presentará en las Bibliotecas El Tintal, Virgilio Barco y del Deporte.

Además, grupos como La Mosca Negra con la obra “La memoria de las ollas o la Caligrafía de la Orfandad”, Teatro Quimera con “Manscupias y otros juegos”,  y el Presagio con “Acéfalos”, harán su participación nacional y distrital durante el fin de semana.

Desde los grupos internacionales, llega Lendias D\`Encantar, desde Portugal con su obra “No Limite Da Dor” sobre el proceso de resistencia de las personas en la dictadura que vivió ese país; se presentará el 19 de marzo con función de 5 y 8 de la noche. A su vez, desde Brasil, llega CIA, Poetícas Contemporáneas con la obra “El Equilibrista”, una puesta en escena que se atreve a interpelar la masculinidad y que se sumerge en el mundo de la identidad, se presentará el 21 de marzo a las 5 y 7 pm.

La programación del Festival de Teatro Alternativo se puede encontrar en su página web: [www.corporacioncolombianadeteatro](http://www.corporacioncolombianadeteatro/), también pueden consultarla en sus redes sociales. La boletería general para todas las obras tiene un costo de \$12.000 y estudiantes con carné, solo deben pagar \$8.000.
