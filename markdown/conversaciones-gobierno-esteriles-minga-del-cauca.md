Title: Conversaciones con el Gobierno han sido estériles: Minga del Cauca
Date: 2019-03-27 14:47
Category: Comunidad, Movilización
Tags: Minga en cauca
Slug: conversaciones-gobierno-esteriles-minga-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/D2YQ0jiWoAAE8Nq.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CRIC/ : @NancyPatricia\_G 

###### 26 Mar 2019 

Después de 17 días de Minga, más de 20.000 indígenas continúan a la expectativa  de la conversaciones que se han establecido con el Gobierno, pese a que se completan seis días de diálogo y se ha decidido trasladar las mesas de diálogo a Mandivá, Cauca para mejorar las condiciones de la negociación,  las partes no han llegado a un acuerdo alrededor de temas fundamentales, mientras tanto el presidente Duque se niega a atender el llamado de las comunidades.

**Organizaciones sociales exigen se respete la Minga**

**Giovanni Yule dinamizador político del CRIC**, manifiesta  que las comunidades se sienten preocupadas frente a la ausencia de poder decisión que tiene la ministra de Interior, **Nancy Patricia Gutiérrez**, "las conversaciones con el Gobierno han sido estériles" señala Yule quien añade que las propuestas planteadas no han tenido resultados en cuanto a los temas reivindicativos de las comunidades.

Yule agrega que existe un gran descontento de grandes sectores de la sociedad por la actitud que ha asumido el presidente Duque frente a la Minga, un clamor que ha sido respaldado por más de 308 organizaciones sociales, 73 internacionales, 10 congresistas y más de 85 figuras académicas y defensoras de DD.HH. quienes han expresado su preocupación por los pocos avances en la Mesa de Negociación y la alta militarización en los territorios donde está concentrada la Minga.

Tales declaraciones se suman al anuncio hecho por  la ONIC en rueda de prensa invitando a todos los sectores indígenas del país a sumarse a las movilizaciones que tendrán lugar el próximo fin de semana, llamado al que, como afirme el dinamizador se han unido los departamentos del **Valle del Cauca, Chocó, Nariño, Antioquia y más reciente las comunidades de La Guajira.**

Tal como lo adelantó la organización indígena, esperan que antes de finalizar marzo, todo el movimiento indígena a nivel nacional haga parte de la movilizando, comenzando por la vía Cali-Buenaventura, donde ya se empiezan a  congregar las comunidades. [(Le puede interesar: Indígenas y Gobierno establecen mesa técnica pero la Minga continúa)](https://archivo.contagioradio.com/indigenas-del-cauca-y-gobierno-establecen-mesa-tecnica-sin-embargo-la-minga-continua/)

 

\[caption id="attachment\_63703" align="alignnone" width="800"\]!["Que Colombia sepa que esta Minga Nacional es por la defensa del territorio, ahí está la vida y la pervivencia de todos y todas" afirma el Consejero Mayor Luis Kankui](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-27-at-1.07.31-PM-800x450.jpeg){.size-medium .wp-image-63703 width="800" height="450"} "Que Colombia sepa que esta Minga Nacional es por la defensa del territorio, ahí está la vida y la pervivencia de todos y todas" afirma el Consejero Mayor Luis Kankui\[/caption\]

**  
Duque se niega a atender el llamado**

El dinamizador Yule atribuye la ausencia del presidente a una orientación del Centro Democrático y en particular del senador Álvaro Uribe y la petición de los gremios del Cauca quienes en reunión con el mandatario solicitaron al presidente no presentarse a las negociaciones,  "entre el señor Duque tarde más, más se va a crecer la minga, no entendemos por qué siendo un servidor público no atiende al llamado de un sector de la sociedad" agrega.

<iframe id="audio_33780583" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33780583_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
