Title: España niega asilo político a activista saharaui condenado a cadena perpetua por desobediencia civil
Date: 2015-01-29 22:41
Author: CtgAdm
Category: El mundo, Movilización
Tags: asilo político, Hassana Aalia, Marruecos, Sahara
Slug: convocan-huelga-de-hambre-en-el-aeropuerto-de-barajas-madrid
Status: published

###### **Foto:Kaosenlared.net** 

[**Entrevista Roberto Mesa:**]  
<iframe src="http://www.ivoox.com/player_ek_4020132_2_1.html?data=lZWfkpaXdo6ZmKiakpmJd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTkwoqwlYqmdcKfxcrby8rLpYzZzZDO1Yqnd4a1pdHcjdXTsIa3lIquptnNp9CfycaY1MrKucjdwsncj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El gobierno español ha denegado la solicitud de asilo político solicitada por el activista **Hassana Aalia, condenado por el gobierno marroquí a cadena perpetua por participar en una acción de desobediencia civil en favor de los derechos del pueblo Saharaui**.

En 2010 fue detenido **por su participación en el campamento de ‘Gdeim Izik’**, realizado para exigir a Marruecos el **derecho de autodeterminación y el respeto de los derechos humanos**. El campamento es considerado por muchos analistas del mundo árabe como el embrión o el comienzo de lo que más tarde se conocería como la **"Primavera Árabe"**.

Más tarde fue puesto en libertad y pudo acceder a una beca de estudios en Bilbao (España), donde ha pasado los últimos años.

En 2013 el gobierno marroquí lo condenó por su participación en la acción, a cadena perpetua, por lo que solicito el asilo político en España, siendo este denegado el pasado 19 de enero.

Según los analistas, el gobierno español nunca ha querido entrometerse, a pesar de saber que es responsable de la situación del pueblo Saharaui, una antigua colonia española, debido principalmente a intereses comerciales, y políticos con Marruecos. El gobierno marroquí es considerado por organizaciones de derechos humanos como una de las **perores dictaduras del mundo.**

La asociación **"Sahara libre" ha convocado una huelga de hambre colectiva en el aeropuerto de Madrid (Barajas) ** para evitar que el próximo 4 de Febrero Hassana sea deportado,  y,  ya en Marruecos encarcelado de por vida.

Entrevistamos a Roberto Mesa, activista solidario con el pueblo saharaui, que actualmente está coordinando la huelga de hambre en el propio aeropuerto de Madrid.Él nos describe como están llegando solidarios de todas las regiones del estado, también nos explica que al ser una zona pública no ha habido ningún tipo de altercado con las fuerzas de seguridad.

Última hora: Después de 7 días la huelga ha finalizado pero continua la solidaridad y se están preparando futuras movilizaciones, por ahora el activista no ha sido deportado.
