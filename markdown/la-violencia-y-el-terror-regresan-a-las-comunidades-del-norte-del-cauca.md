Title: La violencia y el terror regresan a las comunidades del Norte del Cauca
Date: 2015-04-17 14:50
Author: CtgAdm
Category: DDHH, Nacional
Tags: ACIN, Asociación de Cabildos Indígenas del Norte del Cauca, Buenos Aires Cauca, Caloto, Cauca, Defensoría del Pueblo, Ejército Nacional, ESMAD, FARC, guerrilla, Guillermo Paví, Indígenas del Cauca, liberación de la madre tierra, Naciones Unidas, Norte del Cauca
Slug: la-violencia-y-el-terror-regresan-a-las-comunidades-del-norte-del-cauca
Status: published

##### Foto: diarioadn.co 

“**La guerra, la muerte, la violencia y el terror regresa a las comunidades y territorios del Norte del Cauca”**, es la denuncia que hace la Asociación de Cabildos Indígenas del Norte del Cauca -ACIN- CXHAB WALA KIWE, que en su comunicado a la opinión pública evidencian los últimos hechos violentos a los que se ha enfrentado la comunidad.

Uno de ellos, se presentó el pasado 14 de abril, en el reguardo Cerro Tijeras, corregimiento de los Robles municipio de Suarez –Cauca-, donde **desaparecieron  dos comuneros miembros de una misma familia,** conocidos como Berney y Wilson Trochez, de quienes aun no se sabe nada sobre su paradero.

Ante esta situación la comunidad  acompañada por la guardia indígena y la autoridad tradicional, adelantan labores de búsqueda en todo el territorio y población aledaña.

Otro de los hecho presentados durante esta semana, se registró el 15 de abril, cuando personas desconocidas vestidas de civil y portando armas de largo alcance, “sacaron de manera violenta de su casa de habitación, ubicada en la vereda Agua Bonita, a los comuneros **Mario Germán Valencia Vallejo, Belisario Trochez Ordoñez Y Cristian David Trochez.** Luego fueron  subidos a dos camionetas que partieron con rumbo desconocido. Siendo aproximadamente las 3:30 pm, **vecinos del lugar informaron la presencia de tres cuerpos abandonados a orillas de la carretera en la vereda Guadualito.** Al momento de ser encontrados los comuneros presentabas impactos de bala en la cabeza”, afirma el comunicado de la ACIN.

Estos dos crímenes, se suman al **asesinato del joven de 19 años, Guillermo Pavi Ramos,** quien falleció el pasado 10 de abril en la hacienda la Emperatriz municipio de Caloto Cauca, por un impacto de bala en su abdomen, cuando la comunidad  adelantaba la Minga de Liberación de la Madre Tierra, en la que llevan desde el mes de febrero de este año. [(Ver nota relacionada)](https://archivo.contagioradio.com/asi-fue-el-asesinato-de-guillermo-pavi-por-parte-de-las-ffmm/)

También se han registrado otros hechos de violencia en la región, desde que se empezó el proceso de Liberación de la Madre Tierra, que desde sus inicios se ha visto presionado por agentes del **ejército y el ESMAD.**

Es por eso que la ACIN, exige atención integral a las comunidades desde el espacio humanitario y psicosocial por el recrudecimiento del conflicto armado, **causado por la reactivación de los bombardeos contra las FARC-EP,** razón por la cual, la población civil vuelve a estar en medio de los enfrentamientos, siendo estigmatizada e involucrada con la guerrilla, por parte del gobierno Nacional y los medios de comunicación.

Así mismo, la comunidad indígena **reitera su apoyo al proceso de paz,** y piden la presencia de los  Organismos de Derechos Humanos, la Defensoría del Pueblo, la Procuraduría y la Comisión Interamericana de Derechos Humanos, con el fin de minimizar los impactos del conflicto dentro de los territorios indígenas, campesinos y afros.

Finalmente, en su comunicado, los indígenas del Norte del Cauca convocan a las Autoridades Indígenas, Gubernamentales, Organismos de Derechos Humanos, Sistema de Naciones Unidas, Sectores Sociales, a la **Audiencia Pública que se realizará el próximo 22 de Abril a las 9 de la mañana, en la Hacienda La Emperatriz** en Caloto-Cauca, para que se verifique la verdadera situación de la población indígena.
