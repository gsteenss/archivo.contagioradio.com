Title: En Cumbre de Paz proponen mesa de conversaciones con la sociedad civil
Date: 2016-02-23 13:05
Category: Nacional, Paz
Tags: Conversaciones de paz de la habana, Cumbres de paz, ELN, FARC, Juan Manuel Santos, Justicia Sociambiental, Movimiento Ríos Vivos
Slug: en-cumbre-de-paz-proponen-mesa-de-conversaciones-con-la-sociedad-civil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/cumbres_paz_medellin_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alianza de Medios Alternativos] 

<iframe src="http://co.ivoox.com/es/player_ek_10544688_2_1.html?data=kpWilpmafJmhhpywj5WcaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncabijKjiz8fWqYzYxpC9w9%2BPtNPj0dTbx9OPscbnwpDRx5DHs8%2Fqxtfgw8jNs8%2FZ1JDQ0dOPsMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Isabel Cristina Zuleta, Ríos Vivos] 

###### [23 Feb 2016] 

Uno de los puntos de discusión muy fuertes en la [Cumbre de Paz con justicia social y ambiental](https://archivo.contagioradio.com/inician-en-ibague-las-cumbres-de-paz-desde-los-territorios/) que se desarrolla en Medellín tiene que ver con los conflictos que genera la destrucción de la naturaleza y por consiguiente de la sociedad, por ello los más de 350 campesinos que se dan cita en esta cumbre proponen **una tercera mesa de conversaciones de paz para superar los conflictos sociambientales** que no se están debatiendo en las conversaciones con las FARC o con el ELN.

Isabel Cristina Zuleta, integrante del [Movimiento Ríos Vivos](https://archivo.contagioradio.com/?s=movimiento+rios+vivos), explica que aunque la gente está de acuerdo con las mesas de conversaciones con las FARC y exige el inicio de la fase pública con el ELN, **lo que están intentando vender es la idea de “una paz completa” y por ello insisten en que van a seguir en oposición al gobierno de Juan Manuel Santos** porque muchos de los conflictos sociales siguen vigentes.

Según Zuleta, esa tercera mesa de paz debería incluir a la Cumbre Agraria Ampliada, como un escenario que recoja todas las voces que desde diferentes partes del país **se están manifestando contra la destrucción de los territorios por parte de las empresas multinacionales y en general los grandes megaproyectos** como la hidroeléctrica de [Ituango](https://archivo.contagioradio.com/?s=hidro+ituango) y la del [Quimbo](https://archivo.contagioradio.com/?s=el+quimbo) u otros como la construcción de micro hidroeléctricas que han secado 19 fuentes de agua en Caldas.

Esta cumbre de paz terminará con una movilización por la ciudad de Medellín, se reforzará el próximo **14 de Marzo con diversas manifestaciones y plantones en la Jornada Mundial contra las Represas** y continuará con marchas el **[17 de Marzo en el marco del paro nacional convocado por el Comando Unitario de Paro.](https://archivo.contagioradio.com/?s=17+de+marzo)**
