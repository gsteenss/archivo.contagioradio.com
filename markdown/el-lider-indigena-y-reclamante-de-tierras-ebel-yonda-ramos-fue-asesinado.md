Title: Líder indígena y reclamante de tierras Ebel Yonda, asesinado en Caquetá
Date: 2019-03-28 13:11
Category: Comunidad, DDHH
Tags: Asesinato de líderes indígenas, Caquetá
Slug: el-lider-indigena-y-reclamante-de-tierras-ebel-yonda-ramos-fue-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/304b80b8-a44c-4585-a450-97145c8c783f-960x1000.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Recsur] 

###### [28 Mar 2019] 

El pasado 27 de marzo en zona rural de **Puerto Rico, Caquetá** fue encontrado con 10 impactos de bala el cuerpo sin vida del líder indígena **Ebel Yonda Ramos**, antiguo gobernador del Cabildo La Gaitana quien denunció en numerosas ocasiones junto con la comunidad de la Vereda Villa Nueva el desplazamiento del que fueron víctimas en 2010 en el marco del conflicto armado.

**Marino Ijají, representante legal de la Asociación de Cabildos Indígenas T'Wala** relata que las comunidades de La Gaitana, de la cual Yonda continuaba siendo un líder visible, fueron víctimas de desplazamiento forzado por las antiguas Farc en 2010 y que solo hasta la actualidad habían regresado a la Vereda Villa Nueva pues anteriormente habían sido desplazados hasta el municipio de Puerto Rico y Florencia, capital del departamento.

[Ebel Yonda Ramos, gobernador indígena] en el momento en que fue desplazada la comunidad en 2010, fue reelegido como gobernador en el 2016 durante  del Proceso de Paz,  situación que poco a poco permitió el regreso de los habitantes a la región pues **"los ánimos ya se habían calmado, lo que facilitaba la entrada del territorio"**, lo que permitió que el líder y agricultor regresara a trabajar a su territorio tan solo un mes atrás.

El representante legal, señala que una de las hipótesis que existen por el momento acerca de su muerte podría tener que ver con su posición como reclamante de tierras, y con las denuncias y demandas que realizó frente al tema durante los años en que él y su comunidad fueron desplazados, convirtiéndolo en uno de los rostros visibles del cabildo.

Según Marino Ijají no existe un interés de extractivismo del territorio por parte de los grupos armados sino que es un tema relacionado al dominio de una zona que funciona como corredor para sus actividades ilegales.

A pesar que las antiguas FARC, ya entregaron sus armas y se convirtieron en un partido político, Ijají señala que aún persisten en el territorio disidencias y diversas bandas criminales que podrían ser las responsables del asesinato del líder indígena, pero que hasta el momento todo es materia de investigación.

A lo largo de este tiempo, l**a comunidad de La Gaitana, que permanece en Florencia y en la vereda  Venecia, ha preferido mantenerse al margen del retorno a sus tierras siendo Ebel el único que estaba yendo gradualmente a la zona.** Ahora tras su muerte el temor a regresar a sus tierras ha incrementado, asímismo la actual gobernadora del cabildo y esposa del fallecido líder se encuentra en estado de conmoción debido a los sucesos, sin embargo tampoco se conoce que haya recibido algún tipo de amenaza.

<iframe id="audio_33817557" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33817557_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
