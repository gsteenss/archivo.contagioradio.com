Title: Continúan las amenazas y presiones a comunidades del Bajo Atrato
Date: 2018-03-26 13:20
Category: Comunidad, DDHH
Tags: amenazas a líderes sociales, Bajo Atrato, ELN, grupos armados, lideres sociales
Slug: continuan-las-amenazas-y-presiones-a-comunidades-del-bajo-atrato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/atrato.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  EFE] 

###### [26 Mar 2018] 

[Organizaciones sociales denunciaron que las amenazas contra los líderes del Bajo Atrato no se detienen **a pesar de las múltiples denuncias que han interpuesto.** Las comunidades denunciaron la presencia de hombres armados que han presionado a las personas a entregar sus tierras y no volver a ellas.]

[De acuerdo con la denuncia, el día 19 de marzo de 2018, “a eso de las 7:00 pm, dos hombres armados y encapuchados llegaron a la residencia del señor Miguel Mercado en la localidad de Apartadocito, cuenca del Curvaradó, municipio de Carmen del Darién”. Allí, hombres armados se identifican como **miembros del ELN**, “]sin embargo no llevan insignias, uno va de civil y otro con un camuflado”.

Adicional a esto, le indicaron al señor Miguel que “tienen información de que  él había cortado con machete 8 reses de propiedad del señor Demetrio Durango”. Ante esto, les comenta que **él no tiene problemas de linderos** con esa familia sino con el señor William Ramírez, “socio de Darío Montoya quien está quitandole las fincas a 34 familias”.

[Ante esto, los hombres armados le indican que “al señor William lo deje tranquilo, que con él nada tiene que ver en **asuntos de linderos**”. En ese sentido, les indica que él ha tratado de solucionar las diferencias a través del concejo municipal pero no lo ha logrado. A estos hechos se suman las denuncias que indican que, "entre la base militar de Llano Rico y la Comunidad de la Nevera hay presencia de hombres armados".]

### **Representante del Consejo Comunitario continúa realizando actividades en territorios usurpados** 

Además de estos hechos, la comunidad de Pedeguita y Mancilla ha manifestado que el representante del consejo comunitario mayor, “el señor Baldobino” ha realizado actividades en territorios **que fueron usurpados** a las comunidades para fortalecer las plantaciones de plátano. Indican que el señor “está pagando a quien hace los trabajos de retro excavación con parcelas  que no le corresponden”. (Le puede interesar:["Líderes del Bajo Atrato denuncian plan para matarlos"](https://archivo.contagioradio.com/lideres-del-bajo-atrato-denuncian-plan-para-asesinarlos/))

Afirman que estas acciones llevan **14 meses** en hectáreas que le quitó a uno de los habitantes de la comunidad y que además, ha continuado con trabajos de deforestación en la zona para ampliar su proyecto platanero. También denuncian que le está entregando tierras a su sobrino y  “los predios usurpados han sido vendidos a terceros, en este caso a un empresario retirado de Uniban”.

Finalmente, reiteran que desde el 5 de marzo han sido **arrasadas 29 hectáreas** de selva poniendo en riesgo los ecosistemas. Los líderes sociales del Bajo Atrato han venido denunciando desde el año pasado las situaciones en las que se han visto envueltos tras los asesinatos de los líderes sociales Mario Castaño y Hernán Bedoya.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
