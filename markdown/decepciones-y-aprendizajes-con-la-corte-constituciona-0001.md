Title: Decepciones y aprendizajes con la Corte Constitucional
Date: 2015-03-10 17:34
Author: CtgAdm
Category: Carolina, Opinion
Tags: Alvaro Uribe, Corte Constitucional de Colombia, Jorge Pretelt Chajub, Víctor Pacheco
Slug: decepciones-y-aprendizajes-con-la-corte-constituciona-0001
Status: published

Por [[**Carolina Garzón Díaz**]](https://archivo.contagioradio.com/carolina-garzon-diaz/)([@E\_vinna](https://twitter.com/E_Vinna))

A quienes, en un acto de fe y esperanza, depositamos nuestra confianza en la **Corte Constitucional de Colombia**, nos ha resultado doloroso el escándalo que se conoció hace algunos días por cuenta del Presidente de la institución, **Jorge Pretelt Chajub**. Este magistrado, según la denuncia, habría cobrado de \$500 millones al representante de Fidupetrol, abogado Víctor Pacheco, para tramitar favorablemente una tutela ante la Corte Constitucional. Pero aunque esta denuncia ha sido dolorosa a causa de la decepción por los trámites de la Corte, no resulta sorprendente.

La Corte Constitucional ha sido la guardiana de los derechos de miles de colombianas y colombianos que han encontrado en sus fallos, autos y sentencias alguna luz de justicia y las herramientas jurídicas para fortalecer la exigencia de sus derechos. No se pueden desconocer sentencias importantes en torno a los territorios colectivos de los afrodescendientes, el tratamiento integral en salud a mujeres víctimas de violencia sexual, la consulta previa, la constitución de familia por parte de parejas del mismo sexo y el derecho al aborto en tres casos específicos. Estos logros en la obtención de derechos no serían ni imaginables si se hubieran quedado en manos del Congreso, es decir, la Corte Constitucional ha sabido dar pasos en la garantía de los derechos de los colombianos.

Sin embargo, esta historia ha sido otra en los últimos meses. La Corte Constitucional ha tenido un notable retroceso y sus sentencias se alejan cada vez más del sentido garantista de los derechos humanos. A esto se suma el escándalo sobre la obtención de las millonarias pensiones de sus integrantes y ahora el Presidente de la Corte es quien se encuentra en el ojo del huracán.

En cuanto al magistrado Pretelt Chajub, ya se veían venir los problemas: para comenzar es herencia de Álvaro Uribe, quien lo postuló para magistrado;[ ]{.Apple-converted-space}desde su posesión como magistrado las tutelas de su escogencia han sido, curiosamente, aquellas relacionadas con ganaderos, terratenientes y conflictos que tienen en el centro millonarias sumas de dinero y cientos de hectáreas de tierras. Al parecer los derechos de los pobres o los más vulnerables, exigidos en miles de tutelas, no han sido del interés del Magistrado Pretelt; quien, a propósito, tiene un sueldo de 23 millones de pesos mensuales y su patrimonio líquido asciende a los 3.077 millones de pesos.

Lo que demuestran estas desafortunadas situaciones es que la Corte Constitucional no es un Olimpo en el que viven los dioses del derecho, sino que es una institución pública en la que trabajan simples mortales.

Sea que se pruebe o no que el Magistrado Pretelt cometió un delito, en este o en otro caso; esta situación ya nos puede dejar tres aprendizajes iniciales: primero, los magistrados también deben ser objeto de la veeduría ciudadana y deben responder ante todas y todos los colombianos por sus actuaciones y los procedimientos de la Corte. Segundo, el fuero especial de los miembros de las Altas Cortes se debe eliminar porque que no es sano para una democracia que sus jueces y magistrados se sientan por encima de la Ley y qué, además, quienes los juzgan sean los congresistas de la Comisión de Acusaciones, quienes tienen bien merecido su sobrenombre de "Comisión de absoluciones". Y en tercer lugar, lo qué ocurre con la Corte Constitucional nos debe servir para recordar que en un Estado de derecho el objetivo de las instituciones debe permanecer más allá de las personas que pasan por ellas y qué lo más importante es garantizar los derechos de quienes no compran la justicia.
