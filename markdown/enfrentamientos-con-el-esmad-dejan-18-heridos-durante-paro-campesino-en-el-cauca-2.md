Title: Enfrentamientos con el ESMAD dejan 18 campesinos heridos durante paro en Cauca
Date: 2019-03-01 15:54
Category: Comunidad, Movilización
Tags: Abuso de fuerza ESMAD, Paro en Cauca
Slug: enfrentamientos-con-el-esmad-dejan-18-heridos-durante-paro-campesino-en-el-cauca-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Esmad-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @camilovelasco99 

###### 1 Mar 2019

Durante la jornada de paro del pasado 28 de febrero promovida por las **comunidades campesinas y docentes del Cauca,**  los enfrentamientos con el** ESMAD** dejaron un saldo de 18 heridos de los cuales 6 son de gravedad, uno de los cuales estaría a punto de perder uno ojo, además de otros 6 docentes detenidos.

**Según Fernando Vargas presidente de la Asociación de Institutores y Trabajadores de la Educación del Cauca,  ASOINCA,** la represión por parte del Estado en cabeza del ESMAD ha golpeado con fuerza a los campesinos y maestros quienes marchan para que se cumplan los acuerdos a los que llegaron previamente con el Gobierno. [(Lea también Inicia paro de docentes en el departamento del Cauca) ](https://archivo.contagioradio.com/inicia-paro-de-docentes-en-el-departamento-del-cauca/)

> Rechazo el uso desmedido del [\#ESMAD](https://twitter.com/hashtag/ESMAD?src=hash&ref_src=twsrc%5Etfw) contra docentes y campesinos que se movilizan estos días en la Ciudad de Popayán exigiendo al gobierno cumplan los acuerdos suscritos por el Estado colombiano. [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) inicie una mesa de diálogo con los sectores movilizados. [@MinInterior](https://twitter.com/MinInterior?ref_src=twsrc%5Etfw) [pic.twitter.com/icdPU7TLwJ](https://t.co/icdPU7TLwJ)
>
> — Feliciano Valencia (@FelicianoValen) [28 de febrero de 2019](https://twitter.com/FelicianoValen/status/1101268377400721408?ref_src=twsrc%5Etfw)

A su vez la **Asociación  Nacional de Usuarios Campesinos (ANUC**) señaló a través de un comunicado, la desaparición de **Franklin Trochez y Pedro Nel Patiño** durante las manifestaciones, al respecto el dirigente de ASOINCA indicó que dos de las personas que se habían reportado desaparecidas ya fueron halladas, pero aguardan a que la información sea confirmada pues había un docente del que aún no se sabría su paradero, sin embargo no especificó que fuera uno de los campesinos mencionados.

Vargas espera que por la vía del diálogo puedan llegarse a soluciones concretas y que el Gobierno pueda resolver las peticiones del gremio, **"no es posible que utilicen este tipo de represión contra la sociedad civil que solo aspira a solucionar unos mínimos derechos"**, agrega el presidente de la organización.

###### Reciba toda la información de Contagio Radio en [[su correo]
