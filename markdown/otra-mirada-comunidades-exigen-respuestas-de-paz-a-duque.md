Title: Otra Mirada: Comunidades exigen respuestas de paz a Duque
Date: 2020-09-02 12:29
Author: PracticasCR
Category: Otra Mirada, Programas
Tags: Cese al fuego, comunidades, Tribunal Administrativo de Cundinamarca
Slug: otra-mirada-comunidades-exigen-respuestas-de-paz-a-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/fcmpaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: fcmpaz

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Tribunal Administrativo de Cundinamarca ordenó al presidente Iván Duque que en un plazo de cinco diera respuestas de fondo a las comunidades que han exigido repetidas veces un cese al fuego en sus territorios y la implementación del acuerdo de paz. (Le puede interesar: [Tribunal ordena a Duque responder a comunidades que exigen cese al fuego en sus territorios](https://archivo.contagioradio.com/tribunal-ordena-a-duque-responder-a-comunidades-que-exigen-cese-al-fuego-en-sus-territorios/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa de Otra Mirada, sobre lo que exigen las comunidades al presidente Duque, participaron Germán Rodriguez, abogado defensores de DD.HH. Comisión Intereclesial de Justicia Y paz, Jani Silva, lideresa ZRC de la Perla Amazónica-Putumayo, Luz Marina Cuchumbe, lideresa campesina de Inzá (Cauca). 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el conversatorio, los invitados explicaron cuáles eran esas exigencias que llevan pidiendo mediante cartas, desde marzo tanto al Gobierno Nacional como a los grupos armados que actúan en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, señalan cuáles son las dificultades que viven las regiones en medio de la pandemia en términos de atención médica; sin embargo también comparten cómo se las han arreglado en los 5 meses como comunidad haciendo frente a la pandemia y generando nuevas iniciativas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El presidente Duque tiene plazo para responder a las regiones hasta el 2 de septiembre, por lo que los panelistas comparten qué camino se tomará si el presidente no responde a las exigencias del Tribunal. 

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Deseamos que se llegue a un acuerdo para el cese del fuego, pero inmediatamente porque lo que está pasando es preocupante".
>
> <cite>Jani Silva, Lideresa ZRC de la Perla Amazónica- Putumayo.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Finalmente, llaman al presidente a no delegar las funciones que le corresponden como mandatario pues de él depende que las cosas cambien en los territorios. (Si desea escuchar el programa del 28 de agosto: [Otra Mirada: En peligro la autonomía de la comisión Interamericana de DD.HH.](https://www.facebook.com/contagioradio/videos/312181006668672))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el programa completo [Haga click aquí](https://www.facebook.com/contagioradio/videos/677584122844142)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
