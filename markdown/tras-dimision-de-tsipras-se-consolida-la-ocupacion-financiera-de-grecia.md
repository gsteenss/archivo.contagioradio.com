Title: Tras dimisión de Tsipras se consolida la "ocupación financiera" de Grecia
Date: 2015-08-21 17:45
Category: El mundo, Política
Tags: Austeridad, Banco Central Europeo, Banco Mundial, Elecciones en Grecia, Fondo Monetario Internacional, Grecia, podemos, Syriza, Tsipiras, Unión europea
Slug: tras-dimision-de-tsipras-se-consolida-la-ocupacion-financiera-de-grecia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/150814210753_sp_alexis_tsipras_624x351_ap.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: BBCMundo 

<iframe src="http://www.ivoox.com/player_ek_7293808_2_1.html?data=mJemlZ2UfI6ZmKiak5uJd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcriytjh1NSPq9PdxszcjabQqdnZ2pDB1c7UtsLnjMnWz87YrYa3lIqvlZDIqYzn1pDQw9fLs4ztjMaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Cesar Torres del Río, académico] 

###### [21 ago 2015] 

La dimisión de Alexis Tsipras para Grecia **puede significar una crisis política al interior del Syriza, partido de gobierno, y la reajuste** de las fuerzas al interior de los partidos de izquierda que votaron por el actual gobierno y en el referendo dijeron no a las condiciones iniciales que la Troika pretendía imponer. Para Alemania el anuncio de Tsipras significa un saldo de 100.000 millones de Euros de ganancias netas.

### **La estrategia política de Tsipras** 

Según Cesar Torres del Río, la estrategia política de Tsipras tiene que ver con la necesidad de fortalecer la posición de derecha al interior de Syriza de cara a un nuevo gobierno, por ello dimite y convoca a elecciones generales anticipadas. Y aunque no se pueda presentar el mismo Tsipras si habría continuidad en el gobierno y por lo tanto en las exigencias para la aplicación de las medidas de ajuste acordadas con la Unión Europea.

En resumidas cuentas, según Torres del Río, la continuidad de las políticas acordadas por Syriza con el BCE, el BM y el FMI significa una “ocupación financiera” en donde Alemania es el “pirata mayor”

### **Las posibilidades a futuro de lo ganado en Grecia** 

Del Río explica que las posiciones en la izquierda de Grecia son, por una parte, salir de la unión Europea, pero para ello no hay condiciones, por otra parte, la calificación de la deuda como “ilegítima” que puede ser la postura que más respaldo popular reciba, y así las cosas arrancaría una carrera por el respaldo político tanto al interior de Grecia como a nivel internacional.

En cuanto a la posibilidad de que la izquierda más radical llegue al poder en las elecciones, capitalizando lo que puede ser la desilusión frente a los acuerdos del gobierno de Tsipras, es una posibilidad lejana dado que esa facción de es una minoría con apenas 25 escaños en el parlamento de ese país.

### **Las lecciones aprendidas y por aprender** 

Syriza, en Enero de este año, logró colocar la lucha contra la austeridad capitalista, contra los memorandos de ocupación financiera, contra el capitalismo y animó a los movimientos sociales en toda Europa y en el mundo a defender sus reivindicaciones. Además promovió la agitación social y política con el fortalecimiento de movimientos sociales que ya tienen algún tiempo de formación.

Según Del Río el gran aprendizaje radica en reconocer que cualquier agrupación que surja debe ser apoyada por todos los movimientos y sectores con los que se puedan identificar. Sin emabrgo, también habría que estar adentro para, desde el interior, poder generar cambios favorables a los intereses de la mayoría, es decir, una lucha contra las direcciones que han demostrado capacidades de cambios hacia la derecha.
