Title: Llegaremos al Congreso a pedirle respuestas al presidente Duque: Ángela María Robledo
Date: 2020-03-12 17:08
Author: CtgAdm
Category: Actualidad, Política
Tags: Angela Maria Robledo, Cámara de Representantes, Colombia Humana
Slug: llegaremos-al-congreso-a-pedirle-respuestas-al-presidente-duque-angela-maria-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Angela-Robledo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/angela-maria-robledo-llegaremos-al-congreso-a-pedirle_md_48874570_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Angela María Robledo, Representante a la cámara

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Angela María Robledo/ Twitter

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Sala de lo Contencioso Administrativo del Consejo de Estado revocó el fallo que anulaba la curul de Ángela María Robledo en la Cámara de Representantes, como parte del estatuto de oposición. Dicho dictamen además aclara que el escaño se otorga a quienes obtienen la segunda posición en la contienda electoral por la presidencia, y que no existió doble militancia, cargo por el cual se presentaron las denuncias.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que [la líder política](https://twitter.com/angelamrobledo) expresó sus dudas respecto a la forma en que se abordó su caso, en particular al precedente que existía respecto a Marta Lucía Ramírez, que tenía una demanda similar por el cargo de doble militancia y cuyo caso se resolvió de forma favorable para la actual vicepresidenta, Robledo señala que los argumentos presentados en defensa de su curul hoy "reafirman el derecho del estatuto de oposición". [(Le recomendamos leer: Con estos argumentos Ángela Robledo defiende su curul)](https://archivo.contagioradio.com/angela-robledo-defendera-curul/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Con curul de Ángela María Robledo, mujeres recuperan una voz en el Congreso

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Con la recuperación de su curul en la Cámara de Representantes, Ángela María Robledo resalta que llega a un "Congreso mucho más antagónico", donde se sumará a las voces que exigen al presidente Iván Duque claridad respecto a las acusaciones en su contra sobre el presunto ingreso de recursos del narcotráfico a su campaña. [(Lea también: Pérdida de curul de Ángela María Robledo es un golpe a la oposición)](https://archivo.contagioradio.com/perdida-de-curul-de-angela-maria-robledo-es-un-golpe-a-la-oposicion/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Con Gustavo Petro, semanas antes de la segunda vuelta teníamos mucha preocupación, nos decían que había mucha plata en la región Caribe para la compra de votos"** - recuerda - "No puede ser el fiscal Barbosa quien se encargue de esta nvestigación, pues estuvo en el círculo más cercano de Iván Duque lo que sería un impedimento para proceder, pero aquí no puede pasar otro proceso 8.000". [(Le puede interesar: La llegada de un "camaleón rabioso" a la Fiscalía)](https://archivo.contagioradio.com/la-llegada-de-un-camaleon-rabioso-a-la-fiscalia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Seguiré para algunos dando lata, para otros y para otras levantaré la voz, porque las mujeres feministas recuperan una vocería en el Congreso de la República" afirma la representante, que asegura que continuará con su trabajo en la Comisión Primera, la Comisión de Paz y la Comisión de Mujeres. [(Lea también: Necesitamos que el poder sea femenino, transformador e incluyente: Ángela Robledo)](https://archivo.contagioradio.com/necesitamos-que-el-poder-sea-femenino-transformador-e-incluyente-angela-robledo/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
