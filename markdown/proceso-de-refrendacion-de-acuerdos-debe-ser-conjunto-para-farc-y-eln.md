Title: Proceso de refrendación de acuerdos debe ser conjunto para FARC y ELN
Date: 2015-01-26 22:42
Author: CtgAdm
Category: Otra Mirada, Paz
Slug: proceso-de-refrendacion-de-acuerdos-debe-ser-conjunto-para-farc-y-eln
Status: published

###### Foto: elpais.com 

El **proceso de conversaciones no se termina solamente con la firma de un cese bilateral** de fuego, hay temas gruesos que se quedaron en el congelador y que son cruciales para la firma de un acuerdo y para el proceso de aplicación de esos mismos acuerdos. Parece que el **gobierno estuviera supeditando todo el acuerdo a la firma de un cese bilateral** afirma el analista político Carlos Lozano.

Adicionalmente, Luis Eduardo Celis, afirma que el proceso de conversaciones que se está realizando con el **ELN deberá confluir necesariamente con el proceso con las FARC** y que para llegar a la firma de un acuerdo conjunto y un momento de refrendación único para las dos guerrillas, se tienen que agilizar las conversaciones con el ELN y se deberá esperar un tiempo prudencial. Eso sería en 2016 según Celis.

Cabe recordar que dentro de los puntos que se han quedado en el “congelador” están varios que tienen que ver con la política antidroga y otros, muy gruesos, que están alrededor del punto de tierras, como las **Zonas de Reserva Campesina** y muchas de las exigencias que hacen parte del pliego único de la Cumbre Agraria, además del tema de las víctimas.

En ese sentido se espera que la próxima semana, el grupo de personas que hacen parte de la **comisión de Memoria Histórica del Conflicto y sus Víctimas** presenten sus conclusiones, así mismo se espera que la comisión técnica de cese bilateral y dejación de armas pueda definir un cronograma que ponga sobre la mesa los tiempos reales que condicionan el proceso de conversaciones y la posibilidad de llegar a acuerdos.
