Title: “No hay garantías para concertar la sustitución de cultivos de uso ilícito en el Putumayo”
Date: 2015-03-28 15:00
Author: CtgAdm
Category: Economía, Nacional, yoreporto
Tags: Cultivos de uso ilícito, Mesa de Organizaciones Sociales del Putumayo, piedad cordoba, Plantaciones de Coca, Putumayo, Sustitución de cultivos de uso ilícito, Zona de reserva Campesina del Putumayo
Slug: no-hay-garantias-para-concertar-la-sustitucion-de-cultivos-de-uso-ilicito-en-el-putumayo
Status: published

###### Foto: latincorrespondent.com 

Voceros de la Mesa Regional de Organizaciones Sociales en Putumayo reunidos con ministro del interior manifestaron su preocupación por la falta de voluntad para avanzar en el plan de sustitución de cultivos de uso ilícito concertado con las comunidades.

Luego en el evento de instalación de la comisión de inversión social, los campesinos manifestaron al gobierno y asistentes su desacuerdo en que se instale la mesa debido a la falta de garantías y cumplimiento en los demás acuerdos como la instalación de sifones; sin embargo se había dado la palabra a otras manifestaciones organizativas quienes plantearon la necesidad de instalar la comisión argumentando ciertas necesidades.

Inmediatamente la propuesta fue acogida por el gobierno, sin embargo, los voceros de la mesa regional de organizaciones sociales se retiraron del espacio, exigiendo garantías y pidieron reunión con presidente santos para desempantanar el proceso.

Así mismo se reiteró y exigieron garantías para que Piedad Córdoba pueda seguir acompañando el proceso como garante, ya que la Unidad Nacional de Protección no ofreció los medios para que pudiera trasladarse a Villagarzon, municipio previsto para el diálogo con Gobierno.
