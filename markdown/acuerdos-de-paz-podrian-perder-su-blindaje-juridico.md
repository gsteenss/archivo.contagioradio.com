Title: Acuerdos de paz podrían perder el "blindaje jurídico"
Date: 2017-09-07 16:31
Category: Judicial, Nacional, Política
Tags: acuerdos de paz, corte suprema
Slug: acuerdos-de-paz-podrian-perder-su-blindaje-juridico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/corte-770x400.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [07 Sept 2017] 

La Corte Constitucional suspendió el debate sobre el blindaje jurídico de los acuerdos de paz, debido a que fueron recusados los magistrados Antonio José Lizarazo y Cristina Pardo, por la candidata presidencial Marta Lucía Ramírez, al señalar que los mismos trabajaron antes en el Gobierno del presidente Juan Manuel Santos, poniendo en riesgo los acuerdos de paz.

Ambos magistrados habían manifestado su interés en los debates para ser nombrados por continuar los esfuerzos de sus trabajos hacia una construcción de paz. Lo que podría significar que esta **recusación sería vital para avanzar en los esfuerzos por desaparecer el blindaje jurídico a los acuerdos de paz**.

### **¿Quién es Antonio José Lizarazo?** 

Lizarazo fue elegido con 55 votos a favor para ser magistrado de la Corte Suprema, abogado de la Universidad libre y con una maestría en derecho administrativo, se ha desempeñado como magistrado del Consejo Nacional Electoral, viceministro de Educación, magistrado del Consejo de Estado y catedrático de la Universidad Libre, además asesoró a la Oficina del Alto Comisionado para la Paz en el proceso de paz de La Habana.

Durante su elección, los representantes del Centro Democrático votaron en blanco alegando que al haber participado de los diálogos de paz podría presentar futuras inhabilidades, sin embargo, **Lizarazo afirmó que solo dio recomendaciones en el punto dos del acuerdo, sobre participación política de ex combatientes**, y que cuando esos puntos lleguen a la Corte se apartará de las discusiones.

### **¿Quién es Cristina Pardo?** 

Jurista egresada de la Universidad del Rosario, se ha desempeñado como secretaria Jurídica de la Presidencia y académica al interior de la Universidad del Rosario, en donde fue directora del área de Derecho Constitucional del a facultad de Derecho.

Tras la elección de Pardo, se había manifestado que con su llegada a la Corte se generaría un equilibrio de poderes **e incluso referente a las ponencias que se tendrían que votar en adelante sobre los acuerdos de paz**.

### **¿Qué viene para los Acuerdos de Paz?** 

Una vez se revisen las recusaciones sobre ambos magistrados **y se decida su participación se procederá a debatir sobre la ponencia del magistrado Luis Guillermo Guerrero** que pide quitar dos puntos del acuerdo:

El primero "Las instituciones y autoridades del Estado tienen la obligación de cumplir de buena fe con lo establecido en el Acuerdo Final. En consecuencia, las actuaciones de todos los órganos y autoridades del Estado, los desarrollos normativos del Acuerdo Final y su interpretación y aplicación deberán guardar coherencia e integridad con lo acordado, preservando los contenidos, los compromisos, el espíritu y los principios del Acuerdo Final".

Y el segundo  "El presente Acto Legislativo deroga el artículo 4 del Acto Legislativo número 01 de 2016 y rige a partir de su promulgación hasta la finalización de los tres periodos presidenciales completos posteriores a la firma del Acuerdo Final".

De ser así los acuerdos de paz perderían su blindaje constitucional y quedarían a la deriva de los gobiernos de turno y las modificaciones que pretendan hacer.(Le podría interesar: ["Circunscripciones de paz se quedaría por fuera del Fast Track por falta de voluntad política"](https://archivo.contagioradio.com/circunscripciones-de-paz-se-quedarian-por-fuera-de-fast-track-si-no-hay-voluntda-politica/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
