Title: ANLA y Ecopetrol alistan lo que sería el segundo proyecto de fracking en Colombia
Date: 2018-04-05 13:47
Category: Ambiente, Voces de la Tierra
Tags: ANLA, Ecopetrol, fracking, fracking en santander, Santander
Slug: anla-y-ecopetrol-alistan-lo-que-seria-el-segundo-proyecto-de-fracking-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/impactos-del-fracking-colombia1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: http://blog.gesternova.com] 

###### [05 Abr 2018] 

Desde La Alianza Colombia Libre de Fracking denunciaron que la Autoridad Nacional de Licencias Ambientales se emitió el Auto número 01243 del 23 de marzo de 2018, para iniciar la evaluación de la licencia ambiental del proyecto **piloto de fracking** **de la empresa Ecopetrol entre Barrancabermeja y Puerto Wilches en Santander**. Esto, a 29 kilómetros de donde ocurrió el derrame de petróleo del pozo de la misma empresa, la Lizama 158.

De acuerdo con Carlos Andrés Santiago, integrante de la Alianza Colombia Libre de Francking, luego de haber revisado la página de la ANLA constataron que “mientras que Ecopetrol **está recogiendo el petróleo derramado** del pozo la Lizama 158, se está iniciando lo que sería el segundo proyecto de fracking en Colombia”.  El primero es en San Martín, Cesar, donde ya se tiene adelantada la fase uno de exploración, que de hecho, se realizó ilegalmente, pues no tenía licencia ambiental.

### **Nuevo proyecto de fracking es en la misma zona afectada por el derrame de crudo** 

El ambientalista indicó que el territorio donde la empresa tiene pensado realizar actividades de exploración de **yacimientos no convencionales**, corresponde a la misma zona donde se presentó el derrame. Se trata de "la zona del Río Sogamoso y la desembocadura del Río Magdalena que fue la zona afectada”.

Para Santiago, esto demuestra una **irresponsabilidad por parte de la ANLA** pues se está iniciando un nuevo proceso de fracking, y más por Ecopetrol que “no contento con los daños que ya está causando con la explotación convencional ahora quiere implementar en los territorios una práctica contaminante”. (Le puede interesar:["En San Martín se contaminó el agua luego del inicio de fracking"](https://archivo.contagioradio.com/en-san-martin-se-contamino-el-agua-luego-del-inicio-del-fracking/))

Finalmente, el ambientalista recordó que éstas actividades ponen en riesgo la salud de las comunidades de los ecosistemas, **al igual que los proyectos económicos** de las mismas. Afirmó que “pese a que no se han otorgado licencias de fracking, ya se desarrolló la fase uno de exploración de un contrato de fracking en San Martín, cesar por parte de la Conoco Phillips de manera ilegal”.

<iframe id="audio_25134080" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25134080_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
