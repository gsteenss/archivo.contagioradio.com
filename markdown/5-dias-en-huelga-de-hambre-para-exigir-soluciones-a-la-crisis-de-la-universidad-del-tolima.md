Title: 5 días en huelga de hambre para exigir soluciones a la crisis de la Universidad del Tolima
Date: 2016-07-11 16:31
Category: Educación, Nacional
Tags: crisis universidad del tolima, crisis universidad pública Colombia, universidad del tolima
Slug: 5-dias-en-huelga-de-hambre-para-exigir-soluciones-a-la-crisis-de-la-universidad-del-tolima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/UTolima1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas ] 

###### [11 Julio 2016]

Nueve personas, entre trabajadores, profesores y estudiantes de la Universidad del Tolima, completan cinco días en huelga de hambre para exigir soluciones efectivas a la crisis que enfrenta la institución con un déficit de más de \$19 mil millones. De acuerdo con el docente Jorge Gantiva, **no se han pagado primas, ni sueldos y algunos de los maestros de planta han tenido que dejar sus puestos vacantes** ante la falta de garantías laborales.

Este fin de semana se instaló una comisión mediadora integrada por el alcalde de Ibagué, monseñor Fabio Calle, un exrector de la Universidad y una delegada del Ministerio de Educación, que sesionará este lunes por tercera vez para concertar el pliego de peticiones que ha elaborado la asamblea triestamentaria, con el fin de **encontrar una solución integral y coherente a la crisis estructural que afronta la institución. **

Desde abril hasta este mes, los estudiantes, profesores y trabajadores han liderado por lo menos 50 movilizaciones, buscando llamar la atención de autoridades y de la sociedad en general sobre la [[precaria situación de la Universidad](https://archivo.contagioradio.com/universidad-del-tolima-a-punto-de-un-nuevo-paro/)] y la **necesidad de que se retire de su cargo al actual rector** por ser el principal responsable del descalabro financiero que tiene a la institución a punto de desaparecer.

<iframe src="http://co.ivoox.com/es/player_ej_12223443_2_1.html?data=kpeflJiYeJShhpywj5aWaZS1lZaah5yncZOhhpywj5WRaZi3jpWah5yncavj08zSjazFstXd18aSlKiPiNDXxtPhx5C5cozYxtGYttTQrc7VjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en [[Otra Mirada](http://bit.ly/1ICYhVU)] por Contagio Radio] 
