Title: Las razones para que campesinos le digan al Congreso: #NoJodanElCampo
Date: 2019-06-05 17:49
Author: CtgAdm
Category: Comunidad, Política
Tags: campo, Congreso, Proyecto Legislativo, tierras
Slug: campesinos-congreso-nojodanelcampo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/NoJodanAlCampo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @asociaciónminga  
] 

Este miércoles más de 180 organizaciones llamaron la atención al Gobierno y al Congreso sobre el **Proyecto de Ley 003 de 2018, que busca modificar la ley 160 de 1994, o ley de tierras**. Con la tendencia en twitter \#NoJodanElCampo, las organizaciones pidieron que el Proyecto sea retirado del trámite legislativo, por considerar que no responde a la necesidad de garantizar el acceso a la tierra y ser contrario al punto 1 del Acuerdo de Paz.

### **"La intención del Proyecto es dejar el campo colombiano como está, en manos de quien está"** 

**Lucia Aldana, abogada de la Corporación Jurídica Yira Castro**, explicó que el proyecto es lesivo para el campo colombiano y los campesinos en dos sentidos: no garantiza el acceso a tierras para quienes no son poseedores, y profundiza una visión empresarial sobre el territorio. Sobre el primer punto, la Abogada señaló que se busca reconocer las expectativas legítimas sobre predios baldíos por parte de ocupantes no regulares.

De acuerdo a esta propuesta, los baldíos podrían ser adquiridos mediante sentencias judiciales bajo la consideración de que es una 'expectativa legítima', "pero hay que recordar que **en Colombia, los bienes baldíos solo pueden salir del patrimonio de la nación solo mediante una notificación"** a cargo de la Agencia Nacional de Tierras, argumentó Aldana. (Le puede interesar: ["Después de 20 años de lucha, campesinos regresan a sus tierras en Guacamayas, Uraba"](https://archivo.contagioradio.com/despues-de-20-anos-de-lucha-campesinos-regresan-a-sus-tierras-en-guacamayas/))

Adicionalmente, el proyecto busca que personas que ya tienen tierras o que no son sujetos de reforma agraria puedan acceder a baldíos; consideración que para la Abogada es nocivo, en tanto el Acuerdo de Paz planteaba la creación de un fondo de tierras a partir de los baldíos, que permita una verdadera Reforma Rural Integral garantizando el acceso a la tierra. Pero la entrega de estos terrenos a personas con capital adquisitivo restaría la cantidad de hectáreas que podrían ingresar al fondo.

En segundo lugar, Aldana señaló que el Proyecto quiere permitir el arriendo de tierras a empresas y la titulación a persona jurídicas, "que **profundiza la idea de que el campo debe ser para personas que ya tienen capital adquisitivo"**. Por estas razones, la Abogada concluyó que "la intención del Proyecto es dejar al campo colombiano como está, en manos de quien está". (Le puede interesar: ["Europarlamentarios piden retirar Proyecto de Maria Fda Cabal contra restitución de tierras"](https://archivo.contagioradio.com/eurodiputados-restitucion-cabal/))

> [\#NoJodanElCampo](https://twitter.com/hashtag/NoJodanElCampo?src=hash&ref_src=twsrc%5Etfw) proyecto de contra reforma agraria 003 de 2018 favorece modelo agroindustrial, desconoce sujeto para adjudicar baldíos y favorece concentración de tierras contrario a lo acordado en la Habana [@Dejusticia](https://twitter.com/Dejusticia?ref_src=twsrc%5Etfw) [@EFEnoticias](https://twitter.com/EFEnoticias?ref_src=twsrc%5Etfw) [@PoloDemocratico](https://twitter.com/PoloDemocratico?ref_src=twsrc%5Etfw) [pic.twitter.com/gCWYtbSFko](https://t.co/gCWYtbSFko)
>
> — Alberto Castilla Salazar (@CastillaSenador) [5 de junio de 2019](https://twitter.com/CastillaSenador/status/1136249891099947009?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Que \#NoJodanElCampo e impulsen una verdadera Reforma Rural Integral** 

Otra denuncia de las organizaciones que rechazaron el Proyecto es el incumplimiento de todos los lineamientos de la Consulta Previa, un paso que asegura que las leyes emanadas del Congreso respondan a las necesidades de la población; por estas razones, esperan que el trámite legislativo en este caso no continúe y se inicie un Proyecto que sea concertado con las comunidades, y responda a las necesidades reales planteadas en el Acuerdo de Paz.

<iframe id="audio_36744438" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36744438_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
