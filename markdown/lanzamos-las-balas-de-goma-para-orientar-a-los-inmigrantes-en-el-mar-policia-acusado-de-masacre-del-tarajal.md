Title: Califican de absurdas las declaraciones de policías acusados de la Masacre del Tarajal
Date: 2015-03-12 19:39
Author: CtgAdm
Category: DDHH, Otra Mirada
Tags: Frontera Marruecos-España, inmigracion España, Valla de la vergüenza, Violación DDHH España Melilla y Ceuta
Slug: lanzamos-las-balas-de-goma-para-orientar-a-los-inmigrantes-en-el-mar-policia-acusado-de-masacre-del-tarajal
Status: published

###### Foto: multimediadiarioinformación.com 

###### **Entrevista con [Ana Fornés]:** 

<iframe src="http://www.ivoox.com/player_ek_4205520_2_1.html?data=lZedl5qWdI6ZmKiak5WJd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8ro1sbQy4qnd4a2lNOYx9OPsMKfx9fc0NnJtsKfxtPh1MqPkcLm09rSxdTXb9qfptjdw4qnd4a2ksaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En el marco del juicio contra policías por la Masacre de Tarajal, en que los uniformados dispararon contra un grupo de inmigrantes que venían tratando de acceder por agua a suelo europeo, sucedida en febrero de 2014, continúan las agresiones contra personas que intentan llegar a Europa.

Este martes 11 de Marzo cerca de** 50 inmigrantes intentaron saltar la valla** que separa Marruecos de España en la ciudad de Melilla, quedando **12 de ellos atrapados en medio de la frontera**. Solo 5 consiguieron acceder a España. Después de 9 horas y de la llegada de organizaciones sociales de DDHH, los imigrantes han bajado y han sido detenidos por efectivos de las fuerzas de seguridad españolas.

Aprovechando la niebla y la escasa visibilidad un grupo de inmigrantes de origen subsahariano ha asaltado la valla cerca del  Centro de Estancia Temporal de Inmigrantes (CETI), donde muchos de los residentes esperaban a sus compañeros.

Los **12 inmigrantes** suspendidos en la valla de paso de Farhana **se niegaban a bajar** ante el fuerte **despliegue de fuerzas de la Guardia Civil española**, y solicitaban la asistencia de organizaciones de derechos humanos. También ha sido **detenida una fotoperiodista** cuando transportaba a varios inmigrantes en su propio vehículo.

Todo esto sucede en una jornada en la que están **siendo juzgados miembros de la Guardia Civil** de Melilla y de Ceuta, ciudades coloniales españolas en territorio marroquí, por su **actuación en la masacre del Tarajal**, también en la frontera, donde **murieron 15 inmigrantes tras** disparos de las fuerzas de seguridad españolas cuando llegaban por mar a territorio europeo.

En el proceso de juicio las **declaraciones** los guardias civiles se limitan a afirmar que **dispararon porque vieron a su teniente hacerlo** y por eso pensaron que ellos también tenían que hacerlo o que utilizaron las balas de goma para orientar a los inmigrantes en la orilla.

Pero según las organizaciones sociales de DDHH, como CIES NO (Centros de Internamiento de extranjeros NO), **existen pruebas contundentes de que los inmigrantes fueron asesinados** y además de que hay más de **80 personas desaparecidas** por la masacre.

Para **Ana Fornés**, miembro de CIES NO, actualmente existe una situación de emergencia humanitaria y de violación de DDHH por parte de la policía española y de la marroquí. Cuando la **policía disparó era totalmente consciente y de ello hay pruebas videográficas**.

La situación en la que llegan los migrantes es de **máxima precariedad**, ya que la mayoría **provienen del África subsahariana de guerras como Congo o Somalia**, ellos son los que se ven obligados a permanecer en campamentos y a tener que asaltar la Valla.

La gran mayoría de **inmigrantes ilegales** no entran por la valla sino a **través de los pasos con pasaportes falsos, en coche**, o pagando a las **mafias** para poder entrar.

Desde la organización **CIES** NO denuncian las políticas xenófobas de la UE y los Centros de Internamiento de Extranjeros como política con un único objetivo , **crear miedo entre las personas migrantes**. La UE solo quiere mano de obra barata y cuando la requiera, en ningún caso se plantea la libre movilidad de las personas.

**Estos hechos contrastan con la decisión de los partidos** de la **oposición** (excepto el conservador PP, actualmente en el gobierno con mayoría absoluta, y el ultraderechista Ciudadanos) han suscrito un **documento comprometiéndose a devolver la sanidad universal**, retornando los **derechos a los inmigrantes irregulares**.

En 2014 el ejecutivo conservador emitió un decreto ley por el que retiraba la universalidad de la salud, afectando principalmente al colectivo de inmigrantes "sin papeles". Con dicho documento se comprometen electoralmente todos los partidos de la oposición excepto el partido Ciudadanos.
