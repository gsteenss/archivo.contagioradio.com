Title: La política es un espacio patriarcal: Ángela Robledo
Date: 2015-11-25 17:51
Category: Entrevistas, Mujer
Tags: 25 de noviembre día de la no violencia contra la mujer, Activismo feminista, Angela Maria Robledo, Congreso de la República, se le exige al presidente Juan Manuel Santos que declare crisis humanitaria de las mujeres
Slug: la-politica-es-un-espacio-patriarcal-angela-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/angela-robledo1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nelson Cárdenas. 

<iframe src="http://www.ivoox.com/player_ek_9509864_2_1.html?data=mpqdm52aeI6ZmKiakp2Jd6KompKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKf0dTZh6iXaaK41c7Qw5DJt4zpz5DS1dXFp8rjjNXO1tfNpdPXwtGSlaaPaaSnhp2e0MzJsMKfs9TPzsrIs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [ Ángela María Robledo, Alianza Verde] 

###### [25 nov 2015]

En el marco del día internacional de la no violencia contra la mujer, la Representante a la Cámara Ángela María Robledo manifiesta su participación frente la defensa de los derechos de las mujeres desde su labor en el Congreso de la República.

Ángela Robledo asegura que la violencia en el ámbito político es un espacio de patriarcado donde existen muchas dificultades y restricciones, sin embargo la función actual de la bancada de mujeres  es continuar  luchando con la comisión legal  de la mujer.

Por medio de la defensa de los derechos de la mujer y el activismo feminista  se quiere que este escenario se convierta en un espacio político otorgando la garantía de los derechos  de las mujeres en las diferentes violencias como la sexual, intrafamiliar, laboral y  política. **“Todo esto como un país que se ve la guerra  tenemos que continuar tomadas de la mano con las tantas plataformas y organizaciones, se debe defender  los derechos y la dignidad y que las mujeres de Colombia vivan libres de violencia generando igualdad y  libertad”**.  Afirma Robledo.

A su vez  las mujeres en El Congreso  de la República cuentan con la caja de herramientas, esta consta un conjunto de leyes  que **“servirá como herramienta para construir la vida libre de violencias y una vida plena con dignidad “**menciona Ángela. Como la ley 1257 por la cual se dictan normas de sensibilización, prevención y sanción de formas de violencia y discriminación contra las mujeres , la ley 1719  busca que no exista impunidad contra la  violencia sexual  contra las mujeres en especial en territorio de guerra.

Adicionalmente se le exige al presidente Juan Manuel Santos que declare crisis humanitaria de las mujeres **“Cada día mueren  dos mujeres están matando a las mujeres en sus casas calles están meriendo muchas mas mujeres en su casa que en el conflicto armado”**.
