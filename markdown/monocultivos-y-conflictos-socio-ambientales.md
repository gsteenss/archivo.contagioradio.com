Title: Monocultivos y conflictos socio-ambientales
Date: 2016-09-21 12:09
Category: Ambiente
Tags: Desplazamiento en Colombia, Monocultivos, monocultivos en colombia, Palma aceitera
Slug: monocultivos-y-conflictos-socio-ambientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Captura-de-pantalla-2015-08-11-a-las-16.51.09.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ES ACADEMIC ] 

###### [21 Sept 2016] 

El 21 de septiembre del año 2006 se decretó como el Día Internacional Contra el Monocultivo de Árboles. La iniciativa tuvo lugar en Brasil cuando el movimiento "Red contra el desierto" manifestó su preocupación por los **impactos socio-ambientales generados por la plantación a gran escala**, de monocultivos conocidos como "desiertos verdes".

Las nefastas consecuencias de esta práctica en el marco del llamado "capitalismo verde", afectan de manera directa tanto a la naturaleza como a las diferentes poblaciones y comunidades. Según el Instituto Alexander Von Humboldt, **los monocultivos causan efectos nocivos como la alteración del ciclo hidrológico en los territorios**, la transformación de la abundancia y composición de especies de fauna y flora, la erosión de la tierra, y la modificación de la estructura y composición de los suelos.

Frente a las negativas consecuencias generadas en la naturaleza a raíz de esta práctica, es imprescindible señalar los **impactos también generados a las poblaciones se relacionan directamente con el territorio**. Desplazamientos forzados, [[conflictos sobre la tenencia de la tierra](https://archivo.contagioradio.com/documental-evidencia-despojo-paramilitarismo-y-accion-empresarial-en-mapiripan/)] entre actores empresariales forestales y los pobladores, la pérdida de las culturas indígenas y tradicionales, y la pérdida del sustento de la población nativa son algunos de los efectos más relevantes.

Según el movimiento campesino internacional, [[Vía Campesina](https://archivo.contagioradio.com/mision-internacional-verificara-situacion-del-campesinado-colombiano/)], el área con este tipo de plantaciones en el mundo se cuadriplicó desde 1980, particularmente en lo que refiere al crecimiento de monocultivos de eucalipto y palma.

De acuerdo al informe presentado en el 2014 por la Confederación Internacional OXFAM, Colombia, Paraguay y Guatemala, presentan casos similares de inversiones significativas para la producción de materias primas de gran demanda internacional, es el caso por ejemplo de los monocultivos a gran escala de maíz, soya y aceite de palma, coincidiendo además con la [[ocupación de territorios](https://archivo.contagioradio.com/suspenden-operaciones-de-poligrow-colombia-por-infracciones-ambiantales/)] antes ocupados, en su mayoría por **comunidades indígenas y campesinas a las que se le priva el acceso a la tierra**.

Uno de los antecedentes más relevantes en materia de monocultivos en Colombia, se remonta al año 2001 con el proyecto de sustitución de cultivos ilícitos del Plan Colombia, con el que se dio prioridad a **proyectos productivos de monocultivos como el cacao, la palma de aceite, el caucho y las especies forestales**. Departamentos como Cundinamarca, Santander, Caquetá, La Guajira, Casanare, Bolívar, Nariño, fueron los territorios en los que se implementaron dichos proyectos.

<div>

[Con el desarrollo del proceso de paz en Colombia, puntualmente con lo negociado en el primer punto sobre la Reforma Agraria Integral, el Gobierno Nacional y las FARC-EP han acordado como alternativa al desarrollo agrario la creación de un **mayor número de incentivos para la productividad familiar**, en contraste con la productividad de tipo agro-industrial a la que pertenecen las plantaciones a gran escala de monocultivos.]{lang="ES-CO"}

</div>

<div>

[Así mismo, se acordó la creación del Plan Nacional Integral de Sustitución de Cultivos de Uso Ilícito, en el que se estableció como principio clave, **trabajar de la mano de las comunidades en la creación y definición de **]{lang="ES-CO"}**[alternativas]{lang="ES-CO"}**[** para la sustitución**, bajo los criterios de sostenibilidad socio-ambiental y económica. Algunos de los fines principales de este punto consisten en la preservación del ambiente y en garantizar a las poblaciones y comunidades condiciones de vida digna. ]{lang="ES-CO"}

</div>

Vea también: [[Definición de bosques de la FAO amenaza al ambiente y legítima los monocultivos](https://archivo.contagioradio.com/definicion-de-bosques-de-la-fao-amenaza-al-ambiente-y-legitima-las-plantaciones/)]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

   
 
