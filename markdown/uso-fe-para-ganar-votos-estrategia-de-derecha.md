Title: El uso de la fe para ganar votos, una estrategia de la derecha
Date: 2019-09-11 13:56
Author: CtgAdm
Category: Nacional, Política
Tags: Cristianos, Fe, Pastor, politica
Slug: uso-fe-para-ganar-votos-estrategia-de-derecha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Publimetro-Colombia-e1568217572909.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Publimetro Colombia  
] 

En Brasil las iglesias cristianas fueron trascendentales para que llegara Bolsonaro a la presidencia, asimismo, en Colombia, la campaña de Iván Duque se vio impulsada por el apoyo de este sector. Una vez más, en estas elecciones locales estos sectores están siendo partes activas en las campañas de alcaldías y gobernaciones**, como en el caso del candidato del uribismo para Bogotá, Miguel Uribe Turbay.**

### **¿Todas las iglesias cristianas están apoyando los sectores de derecha?** 

El **pastor presbiteriano de la Costa Milton Mejía** explicó que las iglesias que suelen apoyar los candidatos con propuestas más conservadoras son carismáticas, de origen pentecostal y pertenecen a un sector cristiano conocido como evangélico. Su organización no depende de una denominación, es decir, son líderes que han logrado algún reconocimiento y buscan incidir en la política electoral con algunos fines determinados. (Le puedeinteresar: ["Cuidar y respetar la naturaleza es cuidar la vida, la obra de Dios"](https://archivo.contagioradio.com/cuidar-y-respetar-la-naturaleza-es-cuidar-la-vida-la-obra-de-dios/))

La fórmula, según el pastor, estaría copiada de otros países en américa latina: Atacar la ‘ideología de género’, la dosis personal de sustancias y a la comunidad LGBT+. Mediante estos argumentos, las iglesias se estarían articulando con sectores políticos, y ganando espacio en el plano electoral, una preocupación que, según Mejía, debería ser de todos los ciudadanos porque está tomando fuerza una sociedad “que se opone al desarrollo de los derechos humanos en toda su expresión”.

### **Caudillismos de fe  
** 

En consecuencia, cuestionó esta relación al declarar que “no es sano para la fe mezclarla con la política electoral”, porque si bien los pastores sí tienen una responsabilidad para orientar el voto de las personas de forma consciente e informada, **esta responsabilidad no significa imponer un candidato por fuerza de las creencias, lo que sería “una tergiversación de la fe cristiana”**. (Le puede interesar:["¿Qué es lo cristiano en los candidatos presidenciales Petro y Duque?"](https://archivo.contagioradio.com/que-es-lo-cristiano-en-los-candidatos-presidenciales-petro-y-duque/))

**Síguenos en Facebook:**  
<iframe id="audio_41569690" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_41569690_4_1.html?c1=ff6600"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
