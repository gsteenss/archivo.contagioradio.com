Title: Regresar al Uribismo es un error muy grande: León Valencia
Date: 2019-04-29 17:48
Author: CtgAdm
Category: Paz, Política
Tags: Iván Duque, León Valencia, uribismo
Slug: regresar-al-uribismo-es-un-error-muy-grande-leon-valencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Diseño-sin-título-7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo/@LeonValenciaA] 

A propósito de la percepción de retroceso que han manifestado algunos sectores del país con la llegada de Iván Duque al  Gobierno,  León Valencia autor del libro 'El Regreso del Uribismo' explica cómo se dio este fenómeno que hoy evidencia la grave crisis que vive Colombia con el retorno de las amenazas contra líderes sociales, el incremento de la violencia e incluso un posible regreso a las interceptaciones telefónicas.

**León Valencia, director de Fundación Paz y Reconciliación**, señala que su libro, próximo a ser lanzado en la Feria del libro, recopila la historia de Colombia desde el rompimientos de relaciones entre los expresidentes Álvaro Uribe y Juan Manuel Santos hasta la llegada de Duque al poder, destacando la crisis que se ha dado en su Gobierno

**"Los gobiernos siempre deben administrar el país con una aceptación en las encuestas o con una coalación de gobierno, pero si no tienen alguna de las dos cosas es una crisis total**" señala Valencia quien considera que ante esta imagen de desfavorabilidad, Duque decidió dar marcha hacia atrás, "hacia el uribismo más puro y duro" lo que significó un retroceso para el país. "pensó que eso le iba a dar otra representación, rompió negociaciones con el ELN, narcotizó la agenda nacional, cosas que eran del pasado, estamos ante un retroceso enorme", agrega.

### **Colombia ya no es el país del 2002** 

"Uno no se inventa el liderazgo, el liderazgo es nato, ahí radica la dificultad" señala Valencia quien cree que no es tan factible que Duque dé un giro en su forma de gobernar  al no contar con una coalición de Gobierno  pues decidió "ignorar  a los partidos que lo apoyaron durante su candidatura presidencial", lo que ha dejado como consecuencia una gran crisis en el país.

El analista señala que a pesar de evidenciarse este retroceso en Colombia, las condiciones del país son diferentes, con una oposición mucho más grande que "aprecia los acuerdos de paz" y que ha probado los beneficios de la paz en muchas en regiones del territorio nacional.

<iframe id="audio_35104676" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35104676_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
