Title: "Medios de información colombianos normalizan la violencia hacia las mujeres"
Date: 2015-09-12 10:12
Category: Movilización, Mujer, Nacional
Tags: Fabiola Calvo Ocampo, medios colombianos, Red colombiana de periodistas con visión de género, Violencia contra las mujeres
Slug: medios-de-informacion-colombianos-normalizan-la-violencia-hacia-las-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/maltrato-mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.infobae.com]

<iframe src="http://www.ivoox.com/player_ek_8301355_2_1.html?data=mZidk5iZeY6ZmKiakp2Jd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcbYytTgjcnJb8rix9Tfz8bHrYa3lIqvldOPp9Dg0NLPy8bSs9Sfz9Tfz8bQrdvVz5DZw5DardDgxtOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [ Fabiola Calvo Ocampo, Red Colombiana de Periodistas con Visión de Género] 

“La nota es muy baja”, dice Fabiola Calvo Ocampo, directora de la Red Colombiana de periodistas con visión de género, frente a la forma como los medios de información colombianos trabajan los hechos alrededor de la violencia contra las mujeres.

De acuerdo a la periodista, aunque se han generado ciertos cambios, lo cierto es que existe un abismo en las facultades de periodismo y en las salas de redacción, respecto al enfoque de género y derechos humanos de las mujeres.

Lo anterior, explica el por qué se ha  tolerado y normalizado en la sociedad colombiana la violencia contra las mujeres, ya que generalmente no se sabe cómo informar, sensibilizar y trasformar la mentalidad de las y los colombianos frente a este fenómeno, y en cambio, desde el periodismo se sigue repitiendo y fortaleciendo estereotipos que excusan el maltrato hacia las mujeres.

“La mató por celos o por amor”, son frases que repetidamente se evidencian en las noticias que hablan sobre violencia contra las mujeres, así mismo, el hecho de describir la forma como estaba vestida la víctima, son detalles que muchas veces se dan en las noticias y que lo que hacen es justificar al agresor, explica la periodista.

Por otra parte, Fabiola Calvo, asegura que otro de los errores más comunes tiene que ver con las pocas fuentes a las que se recurre, ya que muchas veces los periodistas se dirigen a las mismas personas que generalmente son funcionarios del gobierno que no conocen la forma como se debe tratar los casos de violencia contra las mujeres, sobre todo en una sociedad patriarcal y machista como la colombiana.

De esta manera, la escritora hace un llamado a que los y las periodistas tengan en cuenta la perspectiva de género y además se cuenten noticias donde no solo se informe sobre el hecho, sino donde también se generen medidas de prevención y solución frente a la violencia contra las mujeres.
