Title: Incineran casa de reuniones Wayúu en Katsaliamana, Guajira
Date: 2019-02-12 16:22
Author: AdminContagio
Category: Comunidad, DDHH
Tags: Aguilas Negras, Atentado, La Guajira, Wayuu
Slug: incineran-casa-reuniones-wayuu-katsaliamana-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/WhatsApp-Image-2019-02-12-at-11.42.38-AM-770x400.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Nacion Wayúu] 

###### [12 Feb 2019] 

La ONG de **derechos humanos Nación Wayúu** denunció que el pasado 11 de febrero, cerca de las 1:00 am, extraños violaron su territorio y atentaron contra un espacio de reunión de sus autoridades tradicionales en el territorio ancestral conocido como Katsaliamana. Allí, los hombres **quemaron una enramada en la que líderes indígenas y autoridades tradicionales adelantaban sus encuentros**.

De acuerdo a la denuncia de **José Silva, presidente de la ONG Nación Wayúu**, estas acciones criminales forman parte de una persecución sistemática que se presenta desde que se creo la Organización, y que ha incluido panfletos amenazantes contra sus integrantes, firmados por las **Águilas Negras.** (Le puede interesar: ["Panfleto de Águilas Negras amenaza a maestro de la sabiduría Wayuu"](https://archivo.contagioradio.com/panfleto-de-aguilas-negras-amenaza-maestro-de-la-sabiduria-wayuu/))

Aunque los integrantes de la ONG no tienen certeza sobre los posibles autores materiales e intelectuales del hecho, Silva indica que en la zona hay reductos de bandas delincuenciales que usan el nombre de Águilas Negras para amenazar a líderes en el territorio. Por estas razones, l**os indígenas solicitaron al Gobierno que se proteja sus comunidades y se garantice la vida de autoridades y miembros de la comunidad en su territorio**. (Le puede interesar: ["200 niños Wayuu se quedaron sin educación en La Guajira"](https://archivo.contagioradio.com/60865/))

\[caption id="attachment\_61563" align="alignnone" width="800"\][![Incendio Casa de reunión Wayúu](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/WhatsApp-Image-2019-02-12-at-11.42.37-AM-800x480.jpeg){.size-medium .wp-image-61563 width="800" height="480"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/WhatsApp-Image-2019-02-12-at-11.42.37-AM.jpeg) Incendio Casa de reunión Wayúu\[/caption\]

###### <iframe id="audio_32480756" src="https://co.ivoox.com/es/player_ej_32480756_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
