Title: Mujeres del Putumayo, caminan por la Paz y la Memoria
Date: 2016-04-07 15:51
Category: yoreporto
Tags: mujeres, Putumayo, Violencia de género
Slug: mujeres-del-putumayo-caminan-por-la-paz-y-la-memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/mujeres-putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Alianza Tejedoras de Vida 

###### 7 Abr 2016 

*Mujeres del Putumayo, caminan por la Paz y la Memoria*  
*“Porque tus ojos son cielo de la tarde”*  
*“Porque tus manos son pájaros ansiosos”*  
*Porque tu pelo es río*  
*y tus pasos…huellas de luna sobre el césped*”  
**Guisella López**.

Por **Claudia Lancheros Fajardo - Yo Reporto**

El domingo 3 de abril, entre cantos, oraciones y poemas, las mujeres del Putumayo, acompañadas por la alcaldía de Villagarzón, recorrieron un camino de catorce  estaciones, en un acto simbólico muy sentido en este municipio, protagonizado por las voces de indígenas, afrodescendientes y campesinas, que se hermanaron en torno a la dignificación de la memoria de las víctimas del conflicto armado y feminicidios.

El camino inició en el puente del río Mocoa, lugar en el que se entregó el propósito de la jornada y se pidió la asistencia y protección espiritual del fuego del amor divino y de las fuerzas que habitan el piedemonte andinoamazónico. Sobre ese puente de madera las mujeres recordaron y honraron la memoria de las víctimas que iniciaron un viaje  de no retorno, con dirección al Sur de las aguas del río. Un cementerio caudaloso, rodeado de la espesa manigua amazónica, por donde viajaron cuerpos mutilados, algunos no recuperados de la oleada paramilitar entre el 2001 – 2003. En ese puente, un lugar de la memoria colectiva del municipio y del departamento signado por recuerdos dolorosos y heridas en el espíritu de las víctimas que aún están abiertas,  por  delitos que lesionaron la conciencia de la humanidad, se realizó la primera estación de la Jornada.

En cada estación mujeres vestidas de negro, con inciensos y velas, se arrodillaron ante el nombre de cada una de las víctimas, pidiendo en nombre de su memoria, justicia, que pare la violencia y el Putumayo sea un territorio de Paz, libre de violencias contra las mujeres, que ama, respeta y protege la Vida y los Derechos Humanos. A pesar de las intimidaciones y el ambiente hostil que está viviendo el País y la región, estas valientes mujeres se vistieron de fortaleza y salieron una vez más a abrazar solidariamente, a las madres, hijas, hermanos y familiares de las víctimas.

Este tejido por la vida fue iniciado por las mujeres del departamento hace más de diez años, cuando decidieron realizar marchas y plantones en contra de la violencia del conflicto armado ante la oleada de asesinatos, que en algunos municipios saturó el cementerio local. Con la fuerza de las mujeres conectada con la energía de  vida de este territorio, se dignificó la memoria de Luz Marina Benavides, una líder comunitaria de Villagarzón, que en el más crítico de los momentos del ingreso paramilitar, defendió la vida de campesinos del municipio en la plaza de mercado. Con esa fuerza, enfrentó a quienes invadieron el territorio de miedo, sacando de los baúles de taxis a decenas de campesinos y campesinas, afirmando que ella los conocía, que eran gente del campo, trabajadora y honesta, ella gritaba a los paramilitares, rompiendo el silencio y el miedo que asolaba a la población. Esta valiente mujer fue asesinada el 25 de noviembre de 2003, una coincidencia dolorosa con el asesinato de las hermanas Mirabal en República Dominicana, cuarenta y tres años atrás. Dejó una hija de un año, hoy es una hermosa joven que creció sin la sonrisa ni consejos de su madre. La memoria de Luz Marina, es semilla bendecida por las mujeres del departamento, ha dado frutos en el tiempo, es la fuerza de las mujeres que construyen la Paz, las que aman y protegen la Vida.

\[caption id="attachment\_22395" align="aligncenter" width="800"\][![mujeres-putumayo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/mujeres-putumayo1.jpg){.wp-image-22395 .size-full width="800" height="475"}](https://archivo.contagioradio.com/mujeres-del-putumayo-caminan-por-la-paz-y-la-memoria/mujeres-putumayo-2/) Alianza Departamental de Mujeres – Tejedoras de vida\[/caption\]

La dignidad presente en la mirada de las madres, hermanas, hijas, amigas y familiares de las víctimas, protagonizaron la jornada, el abrazo solidario, sonoro, entre mujeres que bajo el sol insistieron a los habitantes del municipio y el departamento: “Basta ya de feminicidios, queremos un territorio libre de violencias, que prevalezca la Paz”. El recorrido terminó en el muro de la verdad ubicado en el parque central del municipio, lugar donde están inscritos los nombres de las mujeres asesinadas y desaparecidas, en placas de madera chonta.

Para la Asociación Alianza de organizaciones de mujeres “tejedoras de vida” del Putumayo, estos actos simbólicos han sido un mecanismo para promover y fortalecer la memoria de las víctimas del conflicto armado, con el propósito de construir un territorio de Paz. Estos son los nombres de las mujeres a las que se rindió homenaje:

1.  Esther Cadena, 10 de junio de 2001
2.  Joaquina Hernández,  16 de octubre de 2011
3.  Melba Alicia Erazo García, 10 de junio de 2001
4.  Marisol Martínez Vallejo, 6 de septiembre de 2002
5.  Sixta Rivera, 28 de abril de 2002
6.  Norby Pantoja, 7 de septiembre de 2012
7.  Bertha Araujo, 27 de junio de 1998
8.  Mónica León, 2015
9.  María Fernis Imbacuan, 16 de septiembre de 2001
10. María Elvia Pantoja Portilla, 27 de marzo de 2013
11. Marcionila Paredes, 30 de enero de 2016
12. Argenis del Socorro Solís, 24 de abril de 2012
13. Nerieth Marcela Josa Díaz, 23 de octubre de 2010
14. Luz Marina Benavides, 25 de noviembre de 2003
15. Kelly Viviana Mena Paredes, 19 de noviembre de 2003
16. Lady Soto Barragán,

Al día siguiente de este acto simbólico por la memoria y por la vida, registramos un nuevo feminicidio en el municipio del Valle del Guamuez. La víctima es Diana Milena Lucano Timana, de 26 años, una mujer joven que deja dos niños huérfanos.
