Title: Así deben proceder en la protección del Valle de Cocora
Date: 2020-11-21 10:27
Author: AdminContagio
Category: Actualidad, Ambiente
Tags: Quindío, Valle de Cocora
Slug: asi-deben-proceder-en-la-proteccion-del-valle-de-cocora
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Valle-de-cocora-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El Valle del Cocora, ubicado en el departamento de Quindío, **uno de los lugares en Colombia donde se encuentran la mayor reserva de Palma de cera, fue declarado como sujeto de derecho.** (Le puede interesar: [Incendios y glifosato amenazan bosque de Palma de Cera](https://archivo.contagioradio.com/bosque-de-palma-de-cera-simbolo-nacional-nuevamente-en-riesgo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Jaime Hernán Arias, presidente del Concejo Municipal de Salento y miembro de la Fundación Bahareque, interpuso la acción de tutela que solicitaba la protección cultural y ambiental del lugar ante el Tribunal Superior del Distrito de Armenia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con este fallo, el Tribunal ordena a la Alcaldía de Salento, la Gobernación del Quindío, la Corporación Autónoma Regional del Quindío y a Parques Nacionales Naturales de Colombia realizar un estudio de capacidad de carga para determinar cuántas personas pueden ingresar al Valle de Cocora.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las entidades deberán reunirse en máximo 15 días para determinar cuáles serán las estrategias y coordinar las acciones para la protección del valle, cuyo ecosistema se estaba viendo afectado por **el “turismo desbordado”**, de acuerdo con Arias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este estudio, deberán participar la Procuraduría Ambiental y al Cámara de Comercio de Armenia y el Quindío, así como los y las habitantes del municipio, con el objetivo de permitir participación en las decisiones locales, dado que **“en el 50% de las áreas protegidas no existe la participación de las comunidades tradicionales o locales, las cuales son fundamentales para participar en el aprovechamiento de los recursos naturales de los parques”**, según indica el documento.

<!-- /wp:paragraph -->

<!-- wp:heading -->

No solo el turismo era una amenaza para el Valle del Cocora
-----------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sin embargo, además de verse afectado por el turismo y los más de 10.000 visitantes que atraía cada año, el Valle del Cocora, se ha visto en riesgo en los últimos años por la posibilidad de que se adelantaran proyectos de megaminería.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto ponía en riesgo tanto a la palma de cera, árbol nacional de Colombia, y a las especies animales como el Loro orejiamarillo, que actualmente es una de las especies de loros más amenazadas del mundo debido a la tala de la palma de cera. (Le puede interesar: [Salento, cuna de la palma de cera, estaría en peligro tras fallo judicial](https://archivo.contagioradio.com/salento-la-cuna-de-palma-de-cera-estaria-en-peligro-tras-fallo-judicial/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
