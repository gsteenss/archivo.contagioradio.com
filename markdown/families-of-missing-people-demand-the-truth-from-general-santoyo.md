Title: Families of  missing people demand the truth from General Santoyo
Date: 2019-05-02 13:05
Author: CtgAdm
Category: English
Tags: General Mauricio Santoyo
Slug: families-of-missing-people-demand-the-truth-from-general-santoyo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Santoyo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

The morning of Monday April 29^th ^after 6 years sentence and 13 years condemnation for drug traffic helping self-defense groups too, the Police’s General Mauricio Santoyo, was captured. The prosecution stands also for participating in the alleged disappearance of the rights’ defenders: **Angel Quintero Mesa and Claudia Monsalve**.

Adriana Quintero, Angel’s daughter said that after 19 years it is time for justice and know abouts the destiny of his father.

**“We are flattered about the news concerning his capture in Colombia”**

The daughter of Angel Quintero Mesa who is part of the **Association for the Family’s Laws Detention due to Diseases (ASFADDES)** demanded immediate action with the compliance of the sentence. The night of October 6^th^2000 in Medellín in which her father disappeared , Qintero filed a report on missing families to the Gaula in Medellín and instead of being protecting by Santoyo, his father disappeared too.

> [\#ATENCIÓN](https://twitter.com/hashtag/ATENCI%C3%93N?src=hash&ref_src=twsrc%5Etfw) Tras su llegada a [\#Bogotá](https://twitter.com/hashtag/Bogot%C3%A1?src=hash&ref_src=twsrc%5Etfw), luego de cumplir condena en Estados Unidos, [\#Fiscalía](https://twitter.com/hashtag/Fiscal%C3%ADa?src=hash&ref_src=twsrc%5Etfw) capturó al General (R) Mauricio Santoyo por su presunta participación en la desaparición de dos defensores de derechos humanos en [\#Medellín](https://twitter.com/hashtag/Medell%C3%ADn?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/uXsvd0LlcL](https://t.co/uXsvd0LlcL)
>
> — Fiscalía Colombia (@FiscaliaCol) [29 de abril de 2019](https://twitter.com/FiscaliaCol/status/1122929118792884224?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
 

Adriana Quintero refers to the general asking him to "**face and look us in the face and became aware of this pain and many years of uncertainty. We have the right to know the truth, this is an act of humanity before the ransom our country needs from him**".
