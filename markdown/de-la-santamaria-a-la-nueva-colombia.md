Title: De la Santamaría a la nueva Colombia
Date: 2017-02-01 09:17
Category: Opinion
Tags: Animalistas, Plaza La Santamaría, Toros
Slug: de-la-santamaria-a-la-nueva-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/no-a-la-corrida-de-toros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Alona Malakhaeva 

#### [**Por Mateo Córdoba Cárdenas**] 

###### 1 Feb 2017 

**Alguna vez hablaba Eduardo Galeano sobre la sistemática negación de los gobiernos latinoamericanos frente a la realidad social y las agendas populares.** Decía Galeano que ante dicha insinuación de que ni la realidad ni la gente existen, le correspondía al pueblo la consigna “la democracia somos nosotros y estamos hartos”.

**Con los toros, a Bogotá volvió un bastión de la vieja Colombia, volvió la más rancia expresión de lo antidemocrático** que puede llegar a ser nuestro país. Aquí seguimos bajo el régimen de la inversión funcional de la voluntad popular, es decir, que cada demanda ciudadana adquiere para los poderosos –a duras penas– el estatus de ‘sugerencia’. Además, los mismos poderosos esperan que dicha minimización del clamor ciudadano sea respondida con concordia, que el pueblo sea dócil y se siente en el despacho de cualquier funcionario con ínfulas de mandamás para seguir ‘sugiriendo’ cercanías entre su querer y el hacer de los tomadores de decisiones. Ante la vehemencia de una nueva mayoría popular, que aborrece las corridas de toros y todo lo que representan, la élite ha querido construir una matriz mediática en la cual no existe un pueblo que demanda democracia, sólo ‘violentos ignorantes’ que no son capaces de hallar en la fiesta brava la elocuencia artística que se le adjudica. Ese poder que se niega a asumir que el pueblo estuvo y seguirá estando fuera de la Santamaría anunciando su hartazgo ante un selecto grupo acostumbrado a convertir su  agenda de clase en política pública.

Los aficionados a la tauromaquia insisten en que a la fiesta brava se le deje morir por su propios medios, haciendo alusión a una supuesta inercia social y generacional que sustenta dicho espectáculo. Lo anterior, lejos de ser un argumento, es un espejismo. Los medios de la fiesta brava para mantenerse en el tiempo y el territorio colombiano no han sido precisamente los del consenso cultural. Al contrario, ante la emergencia de una nueva moralidad, la tauromaquia se ha valido de un ‘lobby’  en el Congreso, en las altas cortes y demás baluartes de la república. En Colombia se ha forjado una coincidencia mortífera para la democracia entre aficionados a la tauromaquia y tomadores de decisiones. El debate deja de ser entre taurinos y anti-taurinos justo en el momento en que los primeros logran ser juez y parte es lo que respecta a la legislación y penalización del maltrato animal. La democracia se vuelve de papel cuando las instituciones le dan la espalda a la gente.

**Las corridas de toros han precipitado en Bogotá lo que se viene para el país**: la diversidad de un pueblo decidido a recuperar la democracia para sí mismo pues, en resumidas cuentas, de eso se trata. El coro del “¡Olé!” ya no sale de la Santamaría. Allí adentro siguen suponiendo que son intocables y que la democracia está a su merced. No se enteran de que la democracia es más robusta cuando las calles están llenas. Sorpresas se llevaron cuando salían de la corrida y se encontraron con que la voluntad popular es que allí adentro no muera torturado ni un animal más. Mayor será su sorpresa cuando vean que el regreso de las corridas de toros a Bogotá desembocó en la unidad de la gente dispuesta a cambiar nuestro país. Hombres y mujeres de todas las edades, de todos los estratos sociales y de todas orientaciones políticas, han constituido un frente popular dispuesto a impugnar en las calles –recinto de la democracia– todo el machismo, el clasismo y la antidemocracia que rodean el mantenimiento de la ‘fiesta brava’ en Bogotá. Dicho frente popular tiene las mismas características de aquel que, a gran escala, recuperará las instituciones para la gente y forje la nueva Colombia sin tauromaquia, sin corrupción, sin feminicidios y sin paramilitarismo.

Cuando recordamos que la democracia somos nosotros forzamos la historia y sale a la calle la voluntad popular que se edifica mientras los poderosos, obnubilados por su condición, suponen que unos miles de policías pueden detener a un pueblo. El miedo cambió de bando, la élite sale de la plaza con la bota escondida y el sombrero entre el bolso. Le temen al pueblo que dignamente sale a increpar su cultura de barbarie. El poder sólo atina a hablar de violencia cuando se trata de un pueblo decidido a cambiar las cosas, por eso militarizan la ciudad. Sin embargo, la unidad popular se está forjando afuera de la plaza entre arengas anti-taurinas y consignas de paz. El pueblo comprende que una democracia como la que trajo de vuelta los toros a Bogotá es más violenta que cualquier manifestación.

Tras alcanzar el fin de la guerra, hay que disputar el país con una élite que nunca estuvo dispuesta a negociar sus intereses y aficiones, un poder que lleva décadas sin cambiar de manos, sólo de sonrisa. Lo sucesos en la inmediaciones de la plaza de toros son supremamente ilustrativos: de la Santamaría a la nueva Colombia hay un paso, el pueblo está decidido.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
