Title: Los intereses tras las Águilas Negras en Colombia
Date: 2020-09-23 21:28
Author: AdminContagio
Category: Expreso Libertad
Tags: #AguilasNegras, #Paramilitares, #Suacha, #Usme
Slug: los-intereses-tras-las-aguilas-negras-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Caminantes-de-Soacha-Amenazados-e1552428556308.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En el último mes, distintas amenazas a **líderes sociales en localidades de Bogotá y el municipio de Suacha,** han generado temor en las comunidades. Los amedrantamientos, que se encuentran firmados por un grupo que se autodenomina como las **Águilas Negras**, anuncian una limpieza social y hacen señalamientos de muerte a integrantes de partidos de izquierda, movimientos sociales y pensamiento crítico.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa del Expreso Libertad el edil del municipio de Suacha José Alcides Alba, y Moisés Cubillos, líder juvenil e integrante de la Plataforma Social Usme y de la Escuela Popular de Arte Público la Quinta Porra, fueron dos de las víctimas de estas amenazas y relataron los intereses tras las águilas Negras en Colombia.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_57006993" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_57006993_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Asimismo, denunciaron la poca protección que brindan las distintas autoridades y las instituciones gubernamentales. En el caso de Suacha, el edil Alba, **asegura que el alcalde Saldarriaga**, no solo niega los hechos, aumentando el riesgo de vulneración, sino que tampoco ha tomado medidas investigativas frente a la situación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cubillos, por su parte manifiesta, que estas estructuras no son nuevas, sin embargo, si ha venido creciendo su presencia en el territorio. Sumado a ellos, la falta de medidas contundentes por parte del gobierno de **Iván Duque en torno a las estructuras paramilitares**, genera una ausencia de garantías para el pensamiento crítico en Colombia.

<!-- /wp:paragraph -->
