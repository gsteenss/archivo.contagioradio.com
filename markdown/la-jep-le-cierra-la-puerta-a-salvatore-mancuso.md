Title: La JEP le cierra la puerta a Salvatore Mancuso
Date: 2020-06-06 17:53
Author: CtgAdm
Category: Actualidad, Judicial
Tags: Paramilitarismo, Salvatore Mancuso
Slug: la-jep-le-cierra-la-puerta-a-salvatore-mancuso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Acogimiento-de-Mancuso-a-la-JEP.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: JEP

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Jurisdicción Especial para la Paz (JEP) anunció el **rechazo, en primera instancia, a la solicitud del exjefe paramilitar Salvatore Mancuso para su sometimiento y juzgamiento por parte de dicha jurisdicción**, **Mancuso había solicitado ser integrado** bajo el rotulo de “tercero civil colaborador o financiador del paramilitarismo entre 1989 y 1997” sin embargo se determinó que su rol fue el de un “miembro orgánico de la estructura criminal, desarrollando una función continua de combate”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para llegar a esta conclusión la Sala de Reconocimiento de la JEP manifestó haberse apoyado en la solicitud de sometimiento del propio Mancuso, en los procesos que contra este se adelantan en la jurisdicción ordinaria y en la jurisdicción especial de la ley 975; y en investigaciones extrajudiciales adelantadas por el Centro Nacional de Memoria Histórica (CNMH).

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La JEP también argumentó la **imposibilidad de acoger a Mancuso, dado que tuvo una “participación directa en las hostilidades” como paramilitar y habiendo una presunción de exclusión de los combatientes paramilitares de la competencia de la JEP**, los exmiembros de estos grupos que se presentan como terceros civiles deben demostrarlo suficientemente con hechos y pruebas, lo cual, no ocurrió según su Sala de Reconocimiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una decisión dividida con 4 votos a favor, 3 en contra y 2 aclaraciones de voto, **la JEP determinó que al no tratarse de conductas realizadas por un tercero civil sino por un combatiente paramilitar, ese tribunal “no es competente respecto de los delitos cometidos por Mancuso Gómez”.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La verdad de Mancuso aún está pendiente

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

De confirmarse esta decisión en segunda instancia, **Mancuso no podría declarar ante la JEP la ampliación de sus denuncias frente a los presuntos nexos del expresidente Álvaro Uribe Vélez y las Autodefensas Unidas de Colombia (AUC) o el complot que según él se adelantó en la Fiscalía para buscar su exclusión del proceso de Justicia y Paz**. Mancuso ha señalado, además,  que la Ley de Justicia y Paz no había reparado víctimas, ni había servido para condenar a los máximos responsables de violaciones a los DD.HH.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Incluso, varios analistas y defensores de DDHH han afirmado que el jefe paramilitar podría aportar muchos elementos de prueba en investigaciones que están estancadas o en curso en contra del senador Alvaro Uribe, otros políticos e incluso contra empresas que auparon el paramilitarismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por ello, el exjefe paramilitar ha sido recurrente en manifestar su disposición de acogerse a la [Jurisdicción Especial de Paz (JE](https://twitter.com/JEP_Colombia)P) para lo cual ha dicho **“Voy a decir la verdad convéngale a quien convenga y perjudique a quien perjudique”.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**De ser ratificada esta decisión impediría el sometimiento, no solo de Mancuso, sino de otros ex paramilitares**, por ejemplo Edwar Cobos, conocido en la guerra como «Diego Vecino» quien estuvo a cargo del Bloque Montes de María y ha manifestado que en la JEP habría una gran contrariedad al no permitir la entrada de Mancuso: «estamos en un país que habla de paz todos los días, pero estamos siendo excluidos en esa construcción de paz. La justicia puede ser diferenciadora, pero no excluyente. Es que ese ha sido el problema del país, la exclusión».

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**También el caso de Carlos Mario Jiménez, alias «Macaco» quien podría declarar sobre el accionar del paramilitarismo y sus relaciones con terceros y la Fuerza Pública** en el marco del conflicto armado, ya que, este ha declarado que en algunas de las acciones violentas cometidas en contra de la población civil, el Bloque Central Bolívar (BCB), el cual dirigía, habría recibido ayuda de altos militares, como el general Santoyo, quien justamente se encuentra compareciendo en la JEP. [(Le puede interesar: "Macaco" podría aportar a la verdad sobre relaciones de terceros y militares en el conflicto armado)](https://archivo.contagioradio.com/macaco-podria-aportar-a-la-verdad-relaciones-de-terceros-y-militares-en-el-conflicto-armado/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Principio de favorabilidad para las víctimas**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El experto jurídico Gustavo Gallón, director de la Comisión Colombiana de Juristas afirma que en caso de presentarse un conflicto de competencias entre laJEP y la Ley de Justicia y Paz, el **principio de favorabilidad para las víctimas es el que se debería aplicar**, para el esclarecimiento de los hechos justamente porque está presente en la misma norma y en uno de los principios fundantes de la Jurisdicción de Paz: La verdad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Otros expertos en justicia transicional están de acuerdo con esta postura. En especial si se tiene en cuenta que el Acuerdo de La Habana tenía fuero de atracción total para los hechos relacionados con el conflicto de manera directa o indirecta y eso contemplaba a todos los actores comprometidos en el mismo, incluidos los paramilitares. Sin embargo, después de que ganara el “No” en el plebiscito se modificaron algunas reglas de juego, entre ellas, limitar el alcance de la JEP.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
