Title: Defensores de DDHH "En la cuerda floja"
Date: 2015-04-02 19:30
Author: CtgAdm
Category: DDHH, El mundo
Tags: Derechos Humanos, Front Line Defenders, ley mordaza, Sistema Interamericano de Derechos Humanos
Slug: defensores-de-ddhh-en-la-cuerda-floja-informe-anual-de-front-line-defenders
Status: published

###### Foto: [www.lighthonestyhrd.org]

Según el informe anual de Front Line Defenders, organización que vigila la situación de defensores y defensoras, en todo el mundo más de **130 defensores fueron asesinados o murieron estando detenidos durante los primeros diez meses del 2014.**

En lo que corresponde a Colombia, el informe resalta la continuidad de practicas sistemáticas a la violación de los derechos de los defensores, en lo corrido del año se denuncia el homicidio de 47 defensores/as  de DD.HH.

El informe también resalta que **101 del total de** **130 asesinatos de DDHH asesinados en el 2014 tuvieron lugar en América**. En cuanto a las medidas represivas para las acciones de defensa de los DDHH, Front Line señala que globalmente, la privación de la libertad y los procesos judiciales fueron las estrategias más utilizadas para silenciar e intimidar a los defensores y defensoras.

Además la legislación restrictiva continua expandiéndose de manera viral en todo el mundo dado que los gobiernos la replican, muy similar en casi todos los Estados pero algunos con situaciones más graves que otros. Aunque el informe no señala la conocida “Ley Mordaza” recién aprobada por el legislativo del estado Español.

Según la organización “*En los últimos dos años, Front Line Defenders ha documentado un incremento en las represalias contra defensores y defensoras de derechos humanos (DDH), que ha alcanzado un punto crítico. En este contexto, las instituciones internacionales de derechos humanos al igual que los gobiernos que tradicionalmente apoyan a defensores y defensoras parecen no ser capaces de oponerse con firmeza y eficacia al cierra del espacio de la sociedad civil*”

[Informe Anual Front Line Defenders Contagio Radio](https://es.scribd.com/doc/260639394/Informe-Anual-Front-Line-Defenders-Contagio-Radio "View Informe Anual Front Line Defenders Contagio Radio on Scribd")

<iframe id="doc_48406" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/260639394/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="80%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
