Title: Denuncian montaje judicial en contra de líderes sindicalistas y defensores de DD.HH
Date: 2020-10-26 22:20
Author: AdminContagio
Category: Expreso Libertad
Slug: denuncian-montaje-judicial-en-contra-de-lideres-sindicalistas-y-defensores-de-dd-hh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/WhatsApp-Image-2020-10-19-at-3.06.15-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: Contagio Radio {#foto-por-contagio-radio .has-text-align-right .has-cyan-bluish-gray-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 20 de septiembre, el gobierno nacional anunció la captura de un conjunto de personas que señala como los responsables de generar los desmanes de las movilizaciones del pasado 21 de noviembre de 2019. Sin embargo, organizaciones sindicalistas denuncian que tras estos hechos habría una **criminalización de la protesta social**.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Las personas que fueron capturadas son Miguel Parga, Érika Flores, Greicy Perilla y un adulto mayor. No obstante, las cuatro personas ejercían actividades de sindicalismo, defensa de derechos humanos o pertenecían a movimientos sociales.

<!-- /wp:heading -->

<!-- wp:heading {"level":6,"textColor":"vivid-cyan-blue"} -->

###### [Le puede interesar: Organizaciones de DD.HH denuncian ola de montajes judiciales en Colombia](https://archivo.contagioradio.com/organizaciones-de-dd-hh-denuncian-ola-de-montajes-judiciales-en-colombia/) {#le-puede-interesar-organizaciones-de-dd.hh-denuncian-ola-de-montajes-judiciales-en-colombia .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

En este programa del **Expreso Libertad** conozca los casos puntuales de Greicy Perilla, defensora de derechos humanos y Miguel Parga, sindicalista, quienes actualmente se encuentran en prisión y que según sus familias estaría siendo víctimas de un montaje judicial, que tendría la intención de generar un precedente en contra de la movilización social en Colombia.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_58640405" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_58640405_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

[Otros videos de Contagio Radio](https://www.facebook.com/contagioradio/videos/?ref=page_internal)

<!-- /wp:paragraph -->
