Title: Naciones Unidas pide libertad para Milagro Sala
Date: 2016-10-28 15:04
Category: El mundo, Otra Mirada
Tags: Argentina, Milagro Sala, Naciones Unidas, Tupac Amaru
Slug: naciones-unidas-pide-libertad-para-milagro-sala
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/milagro-sala.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Nexofin 

###### [28 Oct 2016]

Como una "irreparable afectación del derecho a la libertad de expresión y a la protesta social" fue calificada por el **Grupo de Trabajo sobre Detención Arbitraria de la ONU (GTDA)**, el arresto de la líder indígena y barrial argentina **Milagro Sala**, privada de su libertad desde el 16 de enero del año en curso, por orden del gobernador jujeño Gerardo Morales. Le puede interesar: [Procuraduría declara 'ilegal' detención de Milagro Sala.](https://archivo.contagioradio.com/procuraduria-determina-que-detencion-de-milagro-sala-es-ilegal/)

La determinación consignada en un documento de 25 páginas, atiende a la denuncia presentada por el Centro de Estudios Legales y Sociales (CELS), Amnistía Internacional y Abogados y Abogadas del Noroeste Argentino en Derechos Humanos y Estudios Sociales, donde se exige al gobierno de Mauricio Macri **se ordene su liberación inmediata**, demanda que se ha reclamado en repetidas oportunidades por diferentes organizaciones, nacionales e internacionales, partidos políticos y legisladores, y que ha sido ignorada tanto por el gobierno nacional como por el provincial. Le puede interesar: [Acampan en plaza de mayo exigiendo la libertad de Milagro Sala](https://archivo.contagioradio.com/milagro-sala-esta-presa-por-reclamar-ctep/) .

El “llamamiento urgente” insta al gobierno Macri a tomar posición sobre el caso: **liberar a la detenida o justificar su oposición por escrito**. La segunda eventualidad motivaría a estos expertos a definirse sobre el caso, una decisión similar a la adoptada recientemente por el mismo grupo en beneficio de Julián Assange, refugiado en la embajada de Ecuador en Londres,  que no es vinculante.

Al mismo tiempo, el escrito de la ONU entregado a la cancillería argentina prevé que haya una "**reparación adecuada**" y una **compensación monetaria a Sala por los días que lleva en prisión**. Desde el gobierno de Jujuy, la respuesta se ha limitado a decir que "es por ahora una recomendación de un grupo de trabajo, pero no es definitivo". La comisión de Derechos Humanos de la ONU, será la encargada de definir la postura del organismo sobre la actual situación de Sala.
