Title: CSIVI de las FARC pide dirimir diferencias en la mesa y no en los medios
Date: 2017-09-29 16:32
Category: Paz, Política
Tags: CSIVI, FARC
Slug: csivi-de-las-farc-pide-dirimir-diferencias-en-la-mesa-y-no-en-los-medios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/acuerdo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Sept 2017] 

A través de un comunicado de prensa la Comisión de Seguimiento Impulso y Verificación de las FARC, CSIVI, manifestó su rechazo frente a las formas en las que el comisionado para la Paz, Rodrigo Rivera ha manejado la información sobre los “colados” que estaría en las listas de amnistías e indultos, debido a que solo **"dinamitan" la confianza construida entre las partes.**

De acuerdo con la CSIVI, desde que Rivera llegó a ocupar el cargo de comisionado de paz, se han generado muchas preocupaciones al interior de la organización, por su habitual costumbre a **“comunicar sus preocupaciones o críticas a la prensa antes que a sus interlocuciones**”, además afirmaron que Rivera ha relentizado tanto su trabajo que lo ha llevado a la parálisis y que tampoco ha accedido a tener reuniones bilaterales con las FARC.

Según el comunicado, los señalamientos hechos por Rivera, parece que estuvieran destinadas a **“judicializar a los integrantes de las FARC o convertirlos en blanco de alto valor estratégico”**, confundiendo sus funciones actuales como comisionado de paz, con las que ejercía antes como Ministro de Defensa. (Le puede interesar: ["La JEP necesita a la ciudadanía en movilización para defenderla"](https://archivo.contagioradio.com/la-jep-necesita-a-la-ciudadania-en-movilizacion-para-defenderla/))

Sobre las listas en donde supuestamente hay personas “coladas”, la CSIVI señaló que eso no es posible debido a que las listas no están concluidas ni cerradas, sino inmersas en un proceso de elaboración, **que no concluirá hasta que se haya terminado de examinar por la Sala de Amnistía e Indulto de la JEP**, la situación civil de todos los integrantes de las FARC.

Hasta el momento en las listas habría 14 mil integrantes de las FARC y la CSIVI ratificó que estas fueron hechas en conjunto por los equipos de gobierno nacional y de la organización política, de igual forma, **en el texto la CSIVI informó que previamente se han descartado a 1.100 personas de estas listas y que en el 80%** de los casos se hizo sin que previamente el Gobierno Nacional hubiese hecho alguna objeción.

Finalmente, la CSIVI expresó que las afirmaciones que ha realizado Rivera solo **“dinamitan”** la confianza que se había establecido entre ambas partes y envían un mensaje a la sociedad en donde se comprometen a que, “a diferencia del proceso con las AUC”, las FARC despejará la más mínima duda sobre quienes entre en las listas de Amnistías. (Le puede interesar:["Circunscripciones de paz deben respetar liderazgos de mujeres, jóvenes y afros"](https://archivo.contagioradio.com/circusncripciones-de-paz-deben-respetar-liderazgos-de-jovenes-mujeres-y-afros/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
