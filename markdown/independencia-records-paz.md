Title: Nace Independencia Récords una productora para la paz
Date: 2017-04-25 12:57
Category: Cultura, eventos
Tags: arte, colombia, Cultura, Independencia Records, paz
Slug: independencia-records-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Independencia-Records.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Independencia Récords 

##### 25 Abr 2017 

En el marco de la 30 edición de la Feria Internacional del Libro de Bogotá se realizará el próximo 28 de abril el **lanzamiento de Independencia Récords**, una propuesta de gestión artística y cultural con la que se busca aportar a la **construcción de la paz con justicia social en Colombia**.

En conversación con Contagio Radio, el **artista plástico y realizador audiovisual Diego Carreño**, cuenta  que el proyecto nace a partir de otras iniciativas de arte para la paz que se venían adelantando por parte de sus integrantes, algunas de ellas integradas en la Coordinadora Cultural Popular Nuestra América.

"Creemos en este momento que la transformación para la paz, se debe dar desde el arte y la cultura, desde la expresión, por su capacidad de recoger experiencias y sentimientos", asegura el artista poniendo como ejemplo a **Esteban Pérez**, **combatiente de las FARC EP**, cuyo nombre real es Hernán Darío, quien asume ahora el nombre de **"Black Esteban"** como artista hip hop. "Son subjetividades que permiten trasegar un camino hacia la paz y la reconciliación" asegura Carreño.

Otros ejemplos del trabajo de reconciliación a través del arte son los de **Martin Batalla y Julián Conrado**, que desde el colectivo aseguran "vamos a brindar una posibilidad a los **artistas que ayer tenían un fusil en la mano hoy tienen una guitarra** y le están cantando al amor, la vida y la paz y están construyendo una nueva Colombia desde el arte y la cultura hacia la paz con justicia social".

De Independencia Récords hacen **Pablo Araoz**, productor de Alerta Kamarada, **La Severa Matacera**, y están en acercamientos con agrupaciones como **Dr Krapula**. También hacen parte el realizador audiovisual argentino **Matías Mera**, responsable de varios videos de la agrupación Calle 13 y de "Reconciliación" primer trabajo de Black Esteban, la artista plástica **Inti Maleywa** y el percusionista **Gilbert Martínez**.

El lanzamiento oficial de la productora tendrá lugar en el **Stand 226**, primer piso del Pabellón 18 o Pabellón Internacional de Corferias, el **viernes 28 de abril de 4 a 10 pm**. En ese lapso de tiempo se realizarán dos charlas, la primera sobre arte y cultura, paz y reconciliación y la segunda sobre medios de comunicación para la paz.

Los conversatorios contarán con la presencia de **Carlos Antonio Lozada** y **Jesús Santrich** de las FARC EP, el director de noticias Caracol **Juan Roberto Vargas**, el Senador **Jorge Enrique Robledo**, el caricaturista **Vladdo**, el director de cine **Sergio Cabrera**, el actor **Alvaro Rodríguez** y la artista plástica **Inti Maleywa**, quien tendrá una exposición permanente en el gran salón de Corferias.

Otras actividades a desarrollar serán la firma de autográfos de libros disponibles en el Stand, el **lanzamiento de la revista independencia y del disco Reconciliación**, con el tema homónimo de Black Esteban, la canción Renace la Esperanza de Martín Batalla y **[Unamos nuestras voces](https://archivo.contagioradio.com/lanzan-unamos-nuestras-voces-la-cancion-par-la-reconciliacion/), composición colectiva de Reincidentes Bta** con algunos excombatientes  de las FARC EP.

<iframe src="https://www.youtube.com/embed/D8hTesp6AnY" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Para concluir las actividades, el domingo 30 de abril se estrenará en Corferias la película **"En eso se fue Fidel"** del realizador Matías Mera, sobre el sentimiento del pueblo cubano al despedir a Fidel Castro y en la noche a partir de las 9, se realizará la F**iesta de la Paz** en Latino Power, con presencia de **Julián Conrado y The Rebels All Stars, Salsan Groove, Martín Batalla, Los Hijos de los días, Black Esteban, DJ one2 y Reincidentes Bta.**

<iframe id="audio_18336739" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18336739_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
