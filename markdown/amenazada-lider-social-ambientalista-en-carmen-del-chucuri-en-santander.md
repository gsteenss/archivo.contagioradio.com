Title: Amenazada líder social ambientalista en Carmen del Chucurí, en Santander
Date: 2017-12-14 14:33
Category: Ambiente, Nacional
Tags: Consulta popular Carmen de Chucuri, Nini Johanna Cárdenas
Slug: amenazada-lider-social-ambientalista-en-carmen-del-chucuri-en-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/WhatsApp-Image-2017-10-31-at-7.15.59-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Wikipedia] 

###### [13 Dic 2017] 

La líder social y defensora del ambiente, Nini Johanna Cárdenas, vocera del comité organizador de la Consulta popular contra la explotación minera y de hidrocarburos en el Carmen del Chucurí, en Santander, **denunció que fue víctima de seguimientos y hostigamientos el pasado 10 de diciembre**, luego de promover un plantón en contra de la suspensión de la consulta.

De acuerdo con la denuncia, un hombre hizo presencia durante una hora en el parque en donde se desarrollaba el plantón y vigilo los movimientos de la líder social, e **incluso llegó a intimidarla, acercándose a ella con una mano dentro de una chaqueta que llevaba puesta**. Momentos después el sujeto observó que cerca del lugar había policías realizando una requisa a un muchacho, situación que lo inquieto y que hizo que se alejará para montarse en una motocicleta.

El pasado 28 de febrero la Johanna Cárdenas fue víctima de un **intento de feminicidio por parte de Milton Chacon Mateus**, quien ingreso a su vivienda atento contra la vida de la líder social, se apodero de las agendas de Cárdenas en donde había documentos como actas de la conformación del comité, planes de trabajo, un celular y una memoria con USB que contenía estudios de impactos ambientales sobre el territorio. (Le puede interesar: ["Comunidades del Carmen del Chucurí en alerta por posibilidad de fracking en su territorio"](https://archivo.contagioradio.com/carmen-del-chucuri/))

Estos hechos de violencia fueron denunciados ante la Fiscalía de Barrancabermeja, la Defensoría del Pueblo, sin embargo, aún no hay avances significativos en la investigación. Actualmente Nini Johanna Cárdenas además de estar en el comité promotor de la consulta popular, hace parte de la Fundación para el Desarrollo Integral de la Mujer Carmeleña “FUINMUCAR”, de la Mesa Nacional Ambiental, es integrante del grupo contra la Resistencia a la Minería en Colombia y del grupo Colombia Libre de Fracking.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
