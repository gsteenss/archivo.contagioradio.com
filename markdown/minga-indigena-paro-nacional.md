Title: Minga Indígena y Paro Nacional logran acuerdos regionales y siguen avanzando
Date: 2017-11-06 00:18
Category: Movilización, Nacional
Tags: Cauca, COCCAM, CRIC, marcha patriotica, Minga Indígena, ONIC, Paro Nacional, Sustitución de cultivos de uso ilícito
Slug: minga-indigena-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/Minga-Indigena-Paro-Nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: vice] 

###### [06 Nov 2017]

Las movilizaciones de la Minga Indígena y Paro Nacional lograron victorias regionales. Los campesinos el Catatumbo lograron que el gobierno pusiera agenda a  incumplimientos de los acuerdos del 2013. Por otra parte los indígenas del Consejo Regional Indígena del Norte del Cauca, avanzaron al sentar al Ministro del Interior y discutir los puntos críticos del pliego de exigencias, tierra y garantías de seguridad.

Por parte de los campesinos del catatumbo se logró que el gobierno nacional se comprometiera con una nueva agenda que incluye fechas concretas y cercanas, licitación y construcción de acueductos y vías, así como poner en marcha planes de sustitución de cultivos de uso ilícito y formalización de la tierra. Es decir, **dos puntos del acuerdo del fin del conflicto con las FARC**, lo que es calificado como un gran avance en medio de las dificultades de implementación. [Lea también: Las victorias de los campesinos del Catatumbo](https://archivo.contagioradio.com/victorias-campesinos-catatumbo/)

Por su parte los indígenas del Consejo Regional Indígena del Cauca llegaron al acuerdo de desbloquear la vía panamericana a cambio de la firma, el próximo 8 de Noviembre, del decreto 982 de 1999 que tenía como objetivo “*Diseñar e implementar las políticas que permitan superar la situación por la cual están atravesando las comunidades indígenas del Cauca, en cuanto a territorialidad, medio ambiente, derechos humanos, economía y seguridad alimentaria*”

Todo ello en el marco de una crisis humanitaria que se denunciaba desde ese entonces, **10 de Junio de 1999 y a la que el gobierno de Andrés Pastrana pretendía responder con una serie de decretos** que hacen parte de los más de 1300 que se han incumplido hasta el momento. [Lea también: Las razones de la Minga Indígena en Cifras](https://archivo.contagioradio.com/las-razones-de-la-minga-indigena-en-cifras/)

### **¿Y qué pasa en lo nacional?** 

En cuanto a la Minga Nacional en la tarde de este domingo se instalaría la mesa nacional luego el acuerdo con el CRIC en el que se desbloquearía la vía a partir de este 6 de noviembre y también se retirará el ESMAD. El encuentro se realizaría en Monterilla, sobre la vía Panamericana y se espera lograr una ruta de cumplimiento de los 6 principales puntos del pliego de exigencias que abarca las necesidades de las comunidades indígenas de todo el país.

Uno de los puntos del acta de acuerdo de este 6 de Noviembre señala que "Se reinstala la mesa con la minga regional el lunes 6 de noviembre e inmediatamente se agoten los temas relativos al Departamento del Cauca, se instalará la mesa nacional"

Por su parte las organizaciones que hacen parte de Marcha Patriótica y que incluyen a varias organizaciones campesinas y de cultivadores de Coca en todo el país ven como un avance el acuerdo en el catatumbo y avanzan hacia un acuerdo de reactivación de la Mesa de Interlocución y Acuerdos a nivel nacional para incluir puntos clave como la sustitución de cultivos de uso ilícito y el desarrollo rural integral a nivel macro.

###### Reciba toda la información de Contagio Radio en [[su correo]
