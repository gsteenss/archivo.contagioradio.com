Title: "En Colombia la historia se debe contar con las voces de la comunidad negra"
Date: 2020-06-25 05:40
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Racismo estructural
Slug: en-colombia-la-historia-hay-que-contarla-como-es-con-las-voces-de-la-comunidad-negra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/agresiones-sexuales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La situación en Estados Unidos luego del asesinato del afroamericano [George Floyd](https://archivo.contagioradio.com/george-floyd-antirracismo-mundial/) a manos de un agente de la Policía, **desató un sin numero de acciones contra el racismo, algunas de ellas la eliminación de símbolos que desconocían la historia** de la comunidad negra e indígena en todo el mundo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunos de estos actos reivindicativos se vieron en Bélgica, en donde ciudadanos derribaron **el monumento a Leopoldo II, responsable de la muerte de decenas de miles de personas en las colonias africanas en este país**, acto que se repitió en Inglaterra con el monumento del esclavista Edward Colston.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Estados Unidos como foco de las principales protestas, el **grito Black lives matter**!, se hizo cada vez mas fuerte en rechazo a las actuaciones racistas, desatando múltiples hechos de rechazo, entre ellos **pintar, mutilar y derribar monumentos de esclavistas, colonos y segregacionistas**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunas de estas estatuas fueron las de Jefferson Davis, presidente de los Estados Confederados en la guerra civil, Edward W. Carmack, periodista y quien en el siglo XIX editaba un periódico con un completo tono racista; **entre las figuras caídas más conocidas también estuvo la de Cristóbal Colón.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Andalalucha/status/1271156138311024640","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Andalalucha/status/1271156138311024640

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Esta actuación ciudadana se regló también en Latinoamericana, con gran impacto en países como México y Argentina**, en donde monumentos a conquistadores, esclavistas y opresores, puestos hace más de un siglo en las partes más representativas de las capitales fueron también intervenidas por la ciudadanía en contra unos símbolos que politizan los espacios públicos.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/LUISEROLAVE/status/1271482184688427009","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/LUISEROLAVE/status/1271482184688427009

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por su parte en Colombia la lucha para la resignificación y/o eliminación de estos símbolos empieza a despertar en medio de las cientos de estatuas, avenidas, colegios, universidades, centros culturales y demás espacios, **que al parecer han rendido con orgullo, honor a la esclavitud y genocidio; y aún luego de 200 de años a la corono española.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El 18 de junio el morro en el centro de Popayán despertaba con la estatua de Sebastián de Belalcázar cubierta con una sabana negra y acompañada de un mensaje de un grupo autodonimiado Movimiento 13D; *"**Tapamos a un Belalcázar qué representa el clasismo el racismo y el dominio de unos sobre otros**, lo tapamos de negro porque negra es nuestra sangre negro es el luto (...) "*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asímismo Terry Hurtadom, concejal de Cali propuso reubicar la estatua del conquitador español y fundador de ciudad, ubicada en un mirador de la capital vallecaucana, a la casa de las memorias en donde las personas puedas conocer el contexto y la historia que precede a Belalcázar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado **[Luis Ernesto Olave ](https://twitter.com/LUISEROLAVE), activista, defensor de DDHH y gestor de la Ley Antidiscriminación, 1482 de 2011**, presentó una petición en la que pretendía derogar y eliminar la ley 58 de 1890, la cual decreta la creación de una estatua en bronce de Cristóbal Colón y de la reina Isabel de Castilla, ubicada en en la avenida El dorado con calle 99.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Teniendo en cuenta que esta se encuentra a la entrada aérea de la capital del país, Olave argumentó que hace alegoría a la colonización y esclavización de los pueblos afrodescendientes e indígenas, además de *"un insulto a las víctimas de la esclavitud, haciendo **tributo no al descubrimiento de América, sino al mayor genocidio y exterminio de pueblos originarios en la época de la colonia**".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otro de los monumentos señalados por Olave es el histórico cuadro **"Apoteosis a Popayán", del artista Efraín Martínez, en el cual se puede ver como comunidades negras e indígenas se arrodillan ante una imagen casi divina de la realeza española;** a esto se suma el histórico monumento en Cartagena, de Pedro de Heredia, uno de los principales esclavistas en la época de la colonia.

<!-- /wp:paragraph -->

<!-- wp:image -->

<figure class="wp-block-image">
![Ensayos históricos y arqueológicos: Apoteosis de Popayán: Imágenes ...](https://3.bp.blogspot.com/-aNRM55KxAFM/U2uvn4GNwGI/AAAAAAAAARE/lOEbr7sfyO0/s1600/cuadro+paraninfo+-+blog.jpg)  

<figcaption>
*Apoteosis de Popayán*, Efraín Martínez, 1939,  óleo sobre tela, 900 x 600 c m.

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

En contraposición el defensor propone monumentos que reconozcan las luchas afrocolombianas en el país, una de ellas es el **primer y único presidente negro que ha tenido Colombia , Juan José Nieto Gil**, en el período de 1861, y el cual no fue hasta el 2018 reconocido como Presidente, en la **galería presidencial de la sala del Palacio de Nariño, en donde su rostro estuvo ausente por más de 157 años.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **"La gente cree que una estatua o un cuadro, no hacen daño y que la historia no se puede esconder; el tema, es que la historia la contaron mal"**
>
> <cite>Luis Ernesto Olave | Coordinador Pueblos Afro Red Latinoamericana y del Caribe</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "En 1851 acabó el esclavismo, pero no el racismo"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Dos días después de conocerse el asesinato de George Floyd, **en Colombia se denunció el homicidio del joven Anderson Arboleda, asesinado también a manos de la [fuerza pública](https://www.justiciaypazcolombia.com/colombia-eeuu-racismo-estructural/)**, situación de abuso de la fuerza pública nada desconocida en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y que según Olave se ha venido agudizando luego de la utilización de diferentes políticas, entre ellas la ley **70 de 1993, la cual tenía inicialmente como objetivo reconocer a las comunidades negras y los territorios que han ocupado históricamente**, contrario a ello, lo que ha hecho es generar un olvidó intencional por parte del Gobierno.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**En Colombia no hay ningún presidente que quiera efectivamente cumplir lo que dice la Ley 70,** sino al revés buscar argumentos para culpar al mismo pueblo negro de las situaciones de extrema pobreza y hambre que atraviesan"*
>
> <cite>Luis Ernesto Olave | Coordinador Pueblos Afro Red Latinoamericana y del Caribe</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Ante el actual del Estado, señaló que en el 2008 durante el Gobierno de **Álvaro Uribe**, cuando él le preguntó que qué opinaba el racismo en Colombia este le respondió que en ***"Colombia no había racismo, lo que pasaba era que a las personas no les gustaba la gente pobre".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Respuesta que para defensor de DDHH, sigue sino el reflejo de un actuar que se sigue repitiendo en un Gobierno ***"que permite acciones violentas en contra del pueblo que reclama sus derechos".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y en donde **nuevamente se retoman [políticas de guerra](https://archivo.contagioradio.com/brigadas-de-ee-uu-que-llegaron-a-colombia-tienen-potestad-para-asesorar-grupos-irregulares/) en las que se pretende intervenir con violencia los territorios,** *"territorios en donde está nuestra gente, en donde la gente quiere vivir en paz, pero contrario a esto hay comunidades que mueren de hambre"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y finalizó enfatizando que cuando la historia está mal contada, es necesario exigir reparación, gran parte de ella simbólica; acto relacionado con la re significación de las estructuras alusivas al racismo, ***"qué recuerdan esa esclavización, haciendo homenaje a quienes se beneficiaron del daño que nos hicieron y de una élite que domina aún el país " .***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray"} -->

Le puede interesar también ver el programa completo: Otra Mirada:Voces contra el racismo estructural

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F602068547092053%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
