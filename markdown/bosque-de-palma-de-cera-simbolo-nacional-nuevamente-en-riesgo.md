Title: Incendios y glifosato amenazan bosque de Palma de Cera
Date: 2020-02-05 18:26
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Loro, palma de cera, Quindío, riesgo ambiental, Tolima
Slug: bosque-de-palma-de-cera-simbolo-nacional-nuevamente-en-riesgo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/WhatsApp-Image-2020-02-05-at-6.03.44-PM-1.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/WhatsApp-Image-2020-02-05-at-6.03.44-PM-1-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/fundacion-pro-aves-sobre-incendios-provocados-fumigacion-con_md_47431336_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Luis Ortega - Alex Cortes | Fundación Pro-Aves

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Habitantes del cañón del río Toche, en zona rural de Ibagué, denunciaron incendios provocados y fumigación con glifosato en el **bosque de Palma de Cera**, ubicado en el límite entre los departamentos de Tolima y Quindío, lugar donde **pretenden cultivar arracacha**, producto que no es convencional de esta región y **atenta contra el bosque hábitat del loro Orejiamarillo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La denuncia fue visibilizada por la Fundación ProAves entidad que vela por la protección de especies en el territorio de Cajamarca, *"Con inmensa tristeza informamos que el 31 de enero de 2020, cerca de las 10:00 de la mañana, en el cañón del Río Toche **están abriendo nuevos cultivos de arracacha, fumigan con glifosato en bosques de Palma, para despejar el terreno. Luego, prenden fuego a la vegetación seca**"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Actualmente el bosque ubicado en la vereda Tochecito pertenece a 40 propietarios privados, y alberga cerca del 70% de estos bosques a nivel nacional. Reconociendo esto, en 2012 el Instituto de Investigación de Recursos Biológicos Alexander Von Humboldt, solicitó al Gobierno **incluir esta área dentro de los parques nacionales, acción que hasta el momento no se ha desarrollado.**  (Le puede interesar: <https://archivo.contagioradio.com/salento-la-cuna-de-palma-de-cera-estaria-en-peligro-tras-fallo-judicial/>)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Con la Palma de Cera también está en riesgo el loro orejiamarillo

<!-- /wp:heading -->

<!-- wp:paragraph -->

El director de la Fundación ProAves, Luis Ortega, afirmó que esta acción de fumigación y quema , se da especialmente por ahorrar mano de obra ignorando el impacto ambiental que esto produce, ***"utilizan químicos como el glifosasto para secar y acabar lo que ellos llaman maleza, esto mata rotundamente hierbas y enevena todo lo que toque"**.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Afirmación que coincide con los daños no solo en la flora de este bosque, sino en la fauna endémica como el loro orejiamarillo, que actualmente solo se encuentra en Colombia pues las poblaciones ecuatorianas se presumen extintas *"en la fundación llevamos 20 años trabajando junto con los campesinos por la preservación de esta **especie de loros única en el mundo y que se encuentra solo en Colombia** **y Ecuador**"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Alex Cortés integrante también de ProAves y encargado de la dirección de conservación agregó, *"Colombia es el único país que en la actualidad que cuenta una amplia población de loro orejiamarillo, **especie que habita al interior de las Palmas y cuya población está en riesgo cada vez que las Palmas lo están"**.*

<!-- /wp:paragraph -->

<!-- wp:image -->

<figure class="wp-block-image">
![loro orejiamarillo en nido](https://hablemosdeaves.com/wp-content/uploads/2017/04/loro-orejiamarillo12.jpg)  

<figcaption>
Foto: hablemosdeaves.com

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Un símbolo nacional en el olvido

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Palma de Cera fue designada como el **árbol nacional en 1985, a través de la ley 61 de 1985, a pensar de ello según Pro-Aves el reconocimiento se queda corto en temas de acción,** *"Colombia solo tiene el 1% de las palmas protegidas ignorando que en el mundo solo existen dos lugares donde se encuentran en cantidad, uno Valle del Cocora ubicado en el Quindío y afectado por la ganadería , y el otro que es el bosque de Palma Tochecito amenazado por la producción agrícola"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente la Fundación ProAves junto con otras organizaciones ambientalistas de la zona hicieron un llamado a que, *"**los organismos de control hagan presencia en este territorio único en su biodiversidad, y exijan a los propietarios privados responder por éste delito ambiental".*** (Le puede interesar: <https://www.justiciaypazcolombia.com/en-patia-cauca-protegen-el-bosque-seco-tropical-con-memoria-y-cultura-afrocolombiana/> )

<!-- /wp:paragraph -->
