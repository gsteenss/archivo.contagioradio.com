Title: COCCAM denuncia masacre de líder social y su familia en Bolívar, Cauca
Date: 2018-10-08 11:43
Author: AdminContagio
Category: DDHH, Nacional
Tags: bolivar, Cauca, COCCAM, Lider social
Slug: coccam-denuncia-masacre-de-lider-social-y-su-familia-en-bolivar-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-06-at-3.26.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [08 Octu 2018] 

La Coordinadora Nacional de cultivadores de hoja de coca, amapola y marihuana, COCCAM, denunció la masacre perpetrada el pasado 6 de octubre, en el municipio de Bolívar, Cauca, contra la familia Rivera, cuando hombres armados **llegaron a la casa de Jaime Rivera y luego de torturarlo lo asesinaron, junto con sus dos hijos, Reinel y Jeisson.**

De acuerdo con la denuncia los hechos se presentaron hacia las 5 de la mañana  y tanto Jaime como Reinel habían desempeñado un reconocido liderazgo social en defensa e implementación de los acuerdos de paz, y últimamente se encontraban acompañando **acciones de resistencia a la erradicación forzada, desarrollada por el Ejército Nacional, en el municipio de Bolívar. **

La COCCAM también denunció que en lo corrido del año han asesinado a **23 líderes sociales integrantes de esta organización**, que impulsaban proyectos de sustitución de cultivos de uso ilícito en el territorio y recordaron que este hecho es lamentable y se presenta un día después de la audiencia pública, hecha en Popayan, para denunciar el asesinato a líderes sociales. (Le puede interesar:["Miguel Morales, sexto docente asesinado en Cauca este año"](https://archivo.contagioradio.com/miguel-morales-profesor-asesinado/))

Asimismo, la organización reiteró su llamado para que el Estado brinde garantías y seguridad a los líderes sociales y los procesos que acompañan, e insistió en la necesidad de que se convoque el Consejo  Asesor Territorial, para abordar la agencia departamental de los planes nacionales integrales de sustitución.

[![WhatsApp Image 2018-10-06 at 10.53.20 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-06-at-10.53.20-PM-643x800.jpeg){.alignnone .size-medium .wp-image-57386 width="643" height="800"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-06-at-10.53.20-PM.jpeg)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
