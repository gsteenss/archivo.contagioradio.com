Title: Medicina Legal confirma que Víctima de la retoma del Palacio fue asesinada
Date: 2016-08-08 14:15
Category: DDHH, Nacional
Tags: Cristina del Pilar Guarín, Retoma del Palacio de Justicia, Victimas palacio de justicia
Slug: medicina-legal-confirma-que-victima-de-la-retoma-del-palacio-fue-asesinada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Guarín.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [8 Ago 2016] 

Según el informe pericial del Instituto de Medicina Legal Cristina del Pilar Guarín, víctima de la retoma del Palacio de Justicia, **murió tras recibir cuatro disparos con arma de fuego**, uno de los cuales impactó en su tercera vertebra dorsal, causándole la muerte. Este dictamen confirma lo que durante 30 años su familia ha venido asegurando, y es que Cristina **no habría muerto calcinada en alguno de los incendios** que se dieron en el Palacio, como lo han asegurado las defensas de los militares y del Estado colombiano ante la Corte Interamericana de Derechos Humanos.

De acuerdo con Rene Guarín, hermano de la víctima, pese a que el informe no cuenta con la veeduría internacional que siempre exigió, es un pequeño avance en la búsqueda de verdad. Ahora, según Guarín, le compete entonces a la Fiscalía General reunir las pruebas suficientes para determinar las **condiciones de modo, tiempo y lugar en las que se cometió el homicidio**, pues ya está confirmado que Cristina recibió cuatro disparos, uno de los que impactó en su tercera vertebra dorsal y le ocasionó la muerte al no haber recibido atención inmediata.

Este informe concluye entonces que Cristina del Pilar fue víctima de un homicidio y que **la exposición de su cuerpo a altas temperaturas fue posterior al momento del deceso**. Para su hermano este dictamen es producto de la persistencia que ha tenido durante un poco más de treinta años, en la que ha comprendido que "claudicar es peor que la muerte" y que la desconfianza hacia el Estado colombiano no es gratuita porque fue capaz de asesinar a su hermana, mantener desaparecidos sus restos y [[emplear más de un año para devolverlos](https://archivo.contagioradio.com/fiscalia-y-medicina-legal-siguen-sin-entregar-restos-de-victimas-del-palacio-de-justicia/)].

<iframe src="http://co.ivoox.com/es/player_ej_12479857_2_1.html?data=kpehmZ6ceZihhpywj5WdaZS1lp6ah5yncZOhhpywj5WRaZi3jpWah5yncbPZz8qYqdrFtoa3lIquptOJdqSfqdLb0ZCoqdTV0cbfx8jNqMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
