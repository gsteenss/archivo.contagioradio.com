Title: Hay una acción concertada para sacar figuras de la oposición de la vida política: Iván Cepeda
Date: 2019-04-14 18:32
Author: CtgAdm
Category: Nacional, Política
Tags: Angela Maria Robledo, Mockus, Objeciones a la JEP, Senado
Slug: accion-concertada-para-sacar-oposicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Diseño-sin-título-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El senador del Polo Democrático Iván Cepeda, concluyó que se está atacando a figuras de la oposición de diferentes formas, pero todas con el mismo fin: sacarlos del escenario político; afirmación que obedece a la más reciente decisión del Consejo de Estado de anular la investidura del senador por la Alianza Verde Antanas Mockus, y mientras se espera un fallo por parte de ese Tribunal sobre la curul de Ángela María Robledo, integrante de Colombia Humana.

El fallo del Consejo se produjo en contra Mockus al considerar que estaría inhabilitado porque no finalizó su vínculo con Corpovisionarios en el tiempo previsto antes de lanzarse al Senado, teniendo en cuenta que esta organización era contratista con el Estado. Sobre esta decisión, Cepeda repitió el mensaje del mismo Senador implicado: se debe acatar y respetar el fallo; pero resaltó que la determinación encerraba una estrategia de abogados que litigan en favor de un partido muy cuestionado como es Opción Ciudadana.

Los abogados a cargo de esta estrategia son José Manuel Abushaibe, Nesly Edilma Rey y Víctor Velásquez Reyes. Según el Senador, con sus acciones buscarán acabar con la bancada alternativa que incluye a congresistas de FARC, Polo, Colombia Humana, Mais, UP y Alianza Verde; muestra de ello es el segundo golpe que podría recibir la oposición, con una nulidad en la curul de Angela María Robledo.

No obstante, Cepeda indicó que la oposición se está preparando para respaldar las acciones que emprenda el profesor Mockus, así como para responder a los procesos de distinta índole que se inicien contra los diferentes integrantes de la bancada porque "no es un secreto que hay una acción concertada para desprestigiar y descalificar figuras de la oposición" buscando sacarlos de la vida política. (Le puede interesar:["Bancada alternativa y Gobierno miden fuerzas por Plan Nacional de Desarrollo y JEP"](https://archivo.contagioradio.com/congreso-por-plan-nacional-de-desarrollo-y-jep/))

> La doble militancia es estar inscrit@ simultáneamente en dos partidos.
>
> Yo renuncio el 16 de marzo de 2018 al Partido Verde y me presento como fórmula de [@petrogustavo](https://twitter.com/petrogustavo?ref_src=twsrc%5Etfw) a título personal, sin inscribirme a ningún partido o agrupapacion; nunca estuve simultáneamente en 2 partidos. [pic.twitter.com/b8FPYTKUWO](https://t.co/b8FPYTKUWO)
>
> — Ángela María Robledo (@angelamrobledo) [12 de abril de 2019](https://twitter.com/angelamrobledo/status/1116796453383561217?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Objeciones a la JEP: La otra batalla en el congreso**

El pasado jueves se llevó a cabo la primera audiencia en la comisión accidental del Senado creada para evaluar las objeciones presidenciales a la Jurisdicción Especial para la Paz, durante la cual el fiscal Néstor Humberto Martínez señaló que en la Ley Estatutaria se había introducido un artículo que afectaba la justicia, en tanto prioriza que los comparecientes respondan a sus víctimas sobre los pedidos de extradición, lo que a Cepeda le pareció una intimidación y pidió la palabra. Sin embargo, la senadora Paloma decidió no brindarle la opción de hablar.

En ese sentido, el Cepeda enfatizó en que no permitirán presiones por parte del Fiscal para lograr la aprobación de las objeciones en el Senado; al tiempo que insistirán en que se debata cuanto antes las objeciones por encima de otros temas que también están en la agenda legislativa, pero que no tienen la misma trascendencia. (Le puede interesar:["Estamos más unidos que nunca para defender la paz: Bancada Alternativa"](https://archivo.contagioradio.com/estamos-mas-unidos-nunca-defender-la-paz-oposicion/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
