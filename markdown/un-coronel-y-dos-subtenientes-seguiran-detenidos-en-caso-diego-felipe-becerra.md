Title: Un Coronel y dos subtenientes seguirán detenidos en caso Diego Felipe Becerra
Date: 2015-01-30 21:32
Author: CtgAdm
Category: Judicial, yoreporto
Tags: #YoReporto, Diego Felipe Becerra, Gustavo Trejos, impunidad
Slug: un-coronel-y-dos-subtenientes-seguiran-detenidos-en-caso-diego-felipe-becerra
Status: published

###### Foto: ElPais 

Este viernes 30 de enero fue confirmada la medida de aseguramiento contra el Coronel John Harvey Peña, y los Sub-Tenientes Leandro Zaravanda Payán y Nelson Giovanny Pineda, en el caso del asesinato del joven grafitero Diego Felipe Becerra.

A los uniformados se los procesa por favorecimiento al homicidio, tráfico y porte ilegal de armas, destrucción y alteración del material probatorio, falso testimonio y fraude procesal. **En este proceso ya son diez uniformados y tres civiles cobijados con medida de aseguramiento.**

El juez 73 de control de garantías de Bogotá les dictó a los 3 acusados medida de aseguramiento en agosto del año 2014, medida que fue apelada por la defensa de los uniformados, siendo ratificada en segunda instancia este viernes 30 de enero por el Juez 37.

**El Coronel y los dos Sub-Tenientes enfrentan una pena de hasta 15 años de prisión**.

\*Gustavo Trejos en \#YoReporto
