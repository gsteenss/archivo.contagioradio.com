Title: Congreso de Paz y Desarrollo por "Un sueño llamado Colombia"
Date: 2015-05-08 13:59
Author: CtgAdm
Category: Nacional, Paz
Tags: Alejandro Toro, Bogotá, Congreso Internacional de Propuestas para la Paz y el Desarrollo, Congreso paz y desarrollo, desarrollo, FARC, Funivida, Gobierno Nacional, paz, proceso de paz
Slug: congreso-de-paz-y-desarrollo-por-un-sueno-llamado-colombia
Status: published

##### Foto: Congreso Paz y Desarrollo 

<iframe src="http://www.ivoox.com/player_ek_4462855_2_1.html?data=lZmjlJ2ZeY6ZmKiakpuJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9DiyNfS1dSPjc%2Foxtfbw8jNs8%2FVzZDRx5C0ttDk1srg1sbXb9HV08aYzsaPlMLujN6Yx9GPiI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejandro Toro, director de Funivida] 

El pasado 6 de mayo en Bogotá, se desarrolló el **Primer Congreso Internacional Propuestas para la Paz y el Desarrollo de Colombia,** cuyo eslogan era "Un sueño llamado Colombia" y el objetivo principal era acompañar las conversaciones de paz en la Habana entre las FARC-EP y el gobierno nacional.

De acuerdo a Alejandro Toro, director de Funivida y uno de los organizadores del evento, el propósito fue proponer  estrategias para la paz en el país, desde visiones nacionales e internacionales, ya que según Toro, “la paz no está en la Habana, debemos construirla todos los colombianos”, y agrega que se deben generar propuestas para el posconflicto y por el país soñado.

"Enviamos un mensaje contundente de que **no pueden parase de la mesa de negociación antes de llegar a unos acuerdos claros que posibilite una paz verdadera”**, afirma el director de Funivida.

El Congreso contó con la participación de diversos ponentes nacionales e internacionales, entre ellos, el cantautor argentino Piero, Esteban Silva del Movimiento Allendista de Chile, Aida Avella, líder política de la Unión Patriótica, León Valencia, entre otros, ponentes internacionales de países como México, Perú,  Ucrania y Argentina.

Según Toro, “las conclusiones fueron muy heterogéneas”, aunque la conclusión final fue que **“el proceso de paz tiene que darse más desde el pueblo** y tiene que haber más organizaciones sociales proponiendo en la Habana”.

Así mismo, se propuso la **creación de una Federación Internacional de Prensa Popular**, que se consolidaría y lanzaría en octubre de este año, cuyo objetivo es agrupar la prensa rural, popular y alternativa de toda América.

**La idea es que el segundo Congreso se realice a inicios del 2016 en México,** sin embargo, se espera que el siguiente encuentro se lleve a cabo en el panorama del posconflcito, cuando a se haya finalizado con éxito el proceso de refrendación.
