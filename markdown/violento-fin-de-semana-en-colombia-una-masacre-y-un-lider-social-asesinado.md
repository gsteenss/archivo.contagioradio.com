Title: Violento fin de semana en Colombia, una masacre y un líder social asesinado
Date: 2020-12-27 20:40
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Colombia, #Conflicto, #LiderSocial, #Masacre, #Meta
Slug: violento-fin-de-semana-en-colombia-una-masacre-y-un-lider-social-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Asesinatos-en-Cauca-y-Narino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este fin de semana se presentaron dos hechos de violencia que se suman al difícil contexto del país. Uno de ellos es la **masacre perpetrada por un grupo armado, en el municipio de Montecristo**, en donde 5 personas fueron asesinadas; el segundo es el **asesinato de Roberto Eduardo Parra,** líder social en el departamento del Meta.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Masacre en Montecristo
----------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con la organización AHERAMIGUA, la masacre se presentó en la Quebrada Honda, en el municipio de Montecristo, departamento de Bolívar. En los hechos, un grupo armado desconocido **asesinó a 5 personas, todas integrantes de la misma familia**. Entre las víctimas se encontraba la ex combatiente Rosa Mendoza y una menor de edad.

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/AHERAMIGUA/status/1343302939096592384?s=08","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AHERAMIGUA/status/1343302939096592384?s=08

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

En un comunicado de prensa, el partido político FARC señaló que Rosa de 25 años, fue la fundadora de ASOVISBOL; Asociación de Vivienda Agraria y Ambiental en el Sur de Bolívar. (Le puede interesar:["Las razones que explican el regreso de las masacres a Colombia"](https://archivo.contagioradio.com/las-razones-que-explican-el-regreso-de-las-masacres-a-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con esta masacre se completan 90 hechos de violencia de este tipo perpetrados en el 2020. El pasado 16 de diciembre, la Organización de las Naciones Unidas, manifestó su alarma producto el aumento de la violencia en el país.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Asesinato del líder social Roberto Parra en el Meta
---------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este viernes festivo 25 de diciembre, fue asesinado R**oberto Eduardo Parra, en la vía Mesetas-Uribe,** en la vereda El Gobernador, departamento del Meta. El líder social venía adelantando un proceso de denuncia en contra de la deforestación y el despojo de tierras que estarían adelantando empresas madereras en el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según familiares, **Parra había solicitado protección a su vida** al gobernador del departamento, Juan Guillermo Zuluaga. A noviembre de este año, en Colombia han asesinado a 251 líderes sociales, sobre los que según organizaciones de derechos humanos hay un 100% de impunidad en el esclarecimiento de los hechos.

<!-- /wp:paragraph -->
