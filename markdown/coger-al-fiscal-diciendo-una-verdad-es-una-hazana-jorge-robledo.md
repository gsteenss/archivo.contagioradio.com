Title: Coger al Fiscal diciendo una verdad es una hazaña: Jorge Robledo
Date: 2018-11-19 18:13
Author: AdminContagio
Category: Nacional, Política
Tags: Jorge Enrique Robledo, Nestor Humberto Martínez, Odebrecht
Slug: coger-al-fiscal-diciendo-una-verdad-es-una-hazana-jorge-robledo
Status: published

###### Foto: Archivo 

###### 19 Nov 2018 

Después de conocerse nuevas pruebas y audios aportados por el fallecido ingeniero Jorge Pizano que confirman el conocimiento del fiscal **Néstor Humberto Martínez** sobre el pago de coimas en La Ruta del Sol II, desde el Congreso algunas voces insisten en que el Fiscal debe renunciar, entre ellos el senador del Polo Democrático, Jorge Robledo.

El congresista, férreo opositor del Fiscal, señala que desde un principio debió declararse impedido pues “la plata de Odebrecht y de Luis Carlos Sarmiento Angulo resonaba en sus bolsillos” algo que le impediría ser imparcial en una investigación que involucrará al dueño de Corficolombiana, (socia de Odebrecht en la contratación de la Ruta del Sol II) y de la cual Martínez Neira fue asesor jurídico.

Asimismo para Robledo está claro que para cuando fue nombrado Fiscal, **este ya sabía todo lo que acontecía al interior del consorcio de La Ruta del Sol y que su nombramiento fue un enroque político** para “proteger al grupo Aval, a Luis Carlos Sarmiento y de paso a Juan Manuel Santos y Germán Vargas Lleras” y que a pesar de las pruebas que le suministró Pizano en el 2015, Martínez fue elegido sin alertar sobre la corrupción que se gestaba.

“¿Alguien en Colombia cree que se va a saber la verdad sobre esa historia dramática y dolorosa de la familia Pizano en una Fiscalía manipulada y controlada por el señor Martínez Neira?, ese señor no es garantía de nada” cuestiona el senador, quien señala que** el Fiscal no solo impide que la investigación se haga a fondo sino que también que se investigue a él mismo por el cargo que ostenta**, al que solo puede hacerlo la Comisión de Acusaciones de la Cámara, que según el congresista “son compadres políticos del fiscal”.

El senador también resaltó que la única razón por la que Néstor Humberto Martínez se sostiene en la Fiscalía es porque “lo defienden personas como Iván Duque, Álvaro Uribe, Juan Manuel Santos, Germán Vargas Lleras, César Gaviria o Andrés Pastrana” y que en “en el Congreso les tienen miedo a todos los altos funcionarios del Estado” razón por la cual ninguna bancada se ha pronunciado.

Robledo extendió una vez más una invitación al fiscal para que se haga presente en el tercer debate de la plenaria y “dé la cara al país”, en el escenario donde también estará presente la ministra del Interior, Nancy Patricia Gutierrez en representación del Gobierno.(Le puede interesar [Por primera vez veo a Néstor Humberto Martínez acorralado: Ramiro Bejarano](https://archivo.contagioradio.com/por-primera-vez-veo-a-nestor-humberto-martinez-acorralado-ramiro-bejarano/))

<iframe id="audio_30189688" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30189688_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
