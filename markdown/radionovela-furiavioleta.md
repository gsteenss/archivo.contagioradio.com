Title: Radionovela "Furia Violeta"
Date: 2019-10-29 13:30
Author: CtgAdm
Category: Expreso Libertad
Tags: Cárcel La Picota, Comunidad LGBTI, derechos LGBTI, Expreso Libertad, LGBTI, LGBTI Colombia, radionovela
Slug: radionovela-furiavioleta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/fv.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/fvvv.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/FURIA-VIOLETA.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div id=":n9" class="a3s aXjCH">

<div dir="ltr">

</div>

<div dir="ltr" style="text-align: justify;">

**Furia Violeta** es una historia basada en hechos reales  que cuenta la historia de un colectivo teatral Trans. Fue la herramienta que encontraron tres mujeres al interior de la Cárcel La Picota para defender sus derechos e iniciar un camino de unión y fraternidad hacia las personas **LGTBI** que se encontraban en ese centro penitenciario.

</div>

<div dir="ltr">

<div>

Stefania y sus compañeras no solo tendrán que vivir la violencia por parte de los demás reclusos, sino también los abusos de la Fuerza Pública en ese lugar. Sin embargo, será su compañerismo el que las haga surgir.

<iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/700687486&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true&amp;visual=true" width="100%" height="300" frameborder="no" scrolling="no"></iframe>

</div>

</div>

</div>

Le puede interesar|[Radionovela "Sobrevivimos"](https://archivo.contagioradio.com/radionovela-sobrevivimos/)
