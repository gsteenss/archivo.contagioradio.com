Title: Minería en Colombia ocupa 5 millones de hectáreas y la agricultura solo 4 millones
Date: 2016-07-22 17:10
Category: Ambiente
Tags: Anglo Gold Ashanti, Gran Minería en Colombia, Jornada contra la gran miería, La Gran Minería Envenena
Slug: mineria-en-colombia-ocupa-5-millones-de-hectareas-y-la-agricultura-solo-4-millones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/mineria-colombia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: confederacionminera.blogspot 

###### [22 Jul 2016] 

En el marco de la Jornada Mundial contra la Gran Mirería, diversas organizaciones sociales que denuncian los efectos nocivos de la realización de la minería a gran escala afirman que la situación en Colombia es grave, dado que en este momento los terrenos entregados en concesión **suman cerca de 5 millones de hectáreas, mientras que las extensiones de tierra dedicadas a la agricultura no superan las 4 millones de hectáreas**.

Una de las situaciones más preocupantes en torno a esa situación es la que se vive en el departamento de la Guajira, con la desnutrición de los indígenas de la comunidad Wayuu y el evidente abandono estatal en el que se encuentran y que contrasta con el alto nivel de explotación de carbón.

Según explica Alejandro Pulido, uno de los integrantes de la **campaña "la gran minería envenena"**, ya se han entregado en concesión 5 millones de hectáreas en 12 mil títulos, pero las solicitudes de concesión abarcan cerca de 25 millones de hectáreas, lo que abarcaría un cuarto del total de la extensión geográfica del país. **Las hectáreas concesionadas se encuentra en las montañas de los andes donde está el 75% de la población del país.**

En cuanto a la cantidad de minerales extraídos por las empresas las cifras que entrega la campaña contra la gran minería son inciertas dado que el propio Estado colombiano no tiene un mecanismo de registro efectivo, así las cosas la información acerca de las cantidades de material corresponden a las reportadas por las empresas. **80 millones de toneladas de Carbón, 165 mil  toneladas de Niquel y 60 toneladas de oro.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
