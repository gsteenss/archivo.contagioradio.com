Title: Nuevas amenazas contra líderes de restitución de tierras
Date: 2015-03-31 18:41
Author: CtgAdm
Category: Comunidad, Nacional
Tags: buenaventura, Consejo Comunitario "La Esperanza", desplazado, Desplazamiento, Estrategia de Despojo, Paramilitar, paramilitares, Paramilitarismo, restitución, tierra
Slug: nuevas-amenazas-contra-lideres-de-restitucion-de-tierras
Status: published

###### Foto: ADN 

<iframe src="http://www.ivoox.com/player_ek_4290489_2_1.html?data=lZemkpmcfY6ZmKiak5qJd6KplZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRktbZ18bgjcbRqc%2FV28bgjcjTstXmwpDZh6iXaaK4xcrfx9iPqMaf08rg1s7YucTdhqigh6eXsozYxpDhy8rWcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Manuel Becerra, Consejo Comunitario La Esperanza] 

Tras 12 años de desplazamiento forzado, las familias del **Consejo Comunitario de la Esperanza**, en cercanías a **Buenaventura**, continúan recibiendo amenazas por parte de grupos paramilitares. Según denuncia Manuel Becerra, representante legal del Consejo Comunitario, en lo que va del 2015 ha recibido **2 amenazas directas** por parte de personas que aseguran  "**Gacha tiene a 200 hombres detrás de él"**, que "si sigue jodiendo con sus tierras, se va a morir", y que "hay mucha tierra para enterrarlos a todos".

Becerra, de **78 años de edad**, asegura que el 17 de marzo fue a la Fiscalía "y me dijeron que ya yo había puesto muchas denuncias, y que cada denuncia que colocaba le costaba mas de 3 millones a la nación. Entonces que esperara los resultados de lo que se estaba haciendo, y no me recibieron la denuncia".

El desplazamiento de las 28 familias que integran el Consejo de La Esperanza se produjo el 17 de marzo del 2003 por el ingreso de grupos armados paramilitares. **8 familias del Consejo Comunitario retornaron voluntariamente en el año 2008**, para generar confianza y que las familias restantes decidieran también hacerlo. Sin embargo, el retorno ha sido intermitente: las familias van a la vereda y regresan a Buenaventura, porque "no sienten confianza". Muchas de las personas del Consejo han presentado problemas de salud por la presión y las amenazas que sufren.

Mientras tanto, el desarrollo de proyectos en la vereda avanza con la constitución de un consejo comunitario en manos de personas que, según becerra, usurparon la representación legal, con el beneplácito de la estrategia de despojo **paramilitar**.

Según la denuncia Manuel Becerra, están ofreciendo a los afordecendientes propietarios de la región 1, 3, 5 y 10 millones de pesos por comprar sus territorios, con la intensión, según Becerra, de dividir a la comunidad; y en caso de no lograrlo, **desplazan forzadamente a los legítimos propietarios** y presentan papeles de compra de terrenos y realización de consultas previas a personas que no han vivido nunca en ese territorio.

El Consejo Comunitario han solicitado a la **Secretaría de Gobierno** una visita humanitaria a la vereda para que se compruebe que los llamados "colonos" no han vivido nunca allí, no tienen casas ni cultivos que así lo demuestren.

Finalmente, señala Manuel Becerra que "si algún miembro de la vereda la esperanza desaparece o muere, es responsabilidad de estos señores promotores, Javier Gamboa, Heriberto Riascos, Hernando de Jesus Velázques, Cristobal Salazar, Alberto y Jhon Jairo Peti.
