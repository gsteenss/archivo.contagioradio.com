Title: Del tres al seis de octubre se realizará el tercer Festival de las Memorias en Caño Manso, Curbaradó
Date: 2019-10-01 17:19
Author: CtgAdm
Category: DDHH, Memoria
Tags: Comisión de Justicia y Paz Colombia, Curbaradó, Festival de las Memorias, masacre de Brisas, septiembre negro
Slug: octubre-realizara-festival-de-las-memorias-en-cano-curvarado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/DayiraCricha-Festival-de-las-Memorias.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ContagioRadio] 

A partir del tres de octubre y hasta el próximo seis del mismo mes, se llevará a cabo en la Zona Humanitaria de Caño Manso, el tercer Festival por las Memorias, una iniciativa que busca a partir del encuentro de afectados y responsables en el marco del conflicto armado, tejer una memoria restaurativa.

Este Festival se desarrollará en medio de la conmemoración a las víctimas de la Operación Septiembre Negro, realizada en 1996 por estructuras paramilitares en connivencia con el Ejército Nacional, en la que también se perpetró la masacre de Brisas. Razón por la cual se visitarán lugares emblemáticos para la memoria. (Le puede interesar:[Víctimas conmemoran los 20 años de la operación &\#171;Septiembre Negro&\#187;](https://archivo.contagioradio.com/spetiembre_negro_operacion_victimas_memoria/))

De igual forma, entre los temas que se abordarán durante este Festival se encuentran el desplazamiento y el despojo, dos de las grandes problemáticas que ha tenido que afrontar la región del Bajo Atrato y sus comunidades. Por este  motivo, también se realizarán recorridos por zonas humanitarias y zonas de biodiversidad que se han constituido como apuestas de construcción de paz y protección al ambiente en este territorio.

Entre las comunidades que invitan a este Festival se encuentran Consejos Comunitarios de Curbaradó, Jiguamiandó Pedeguita Mancilla, La Larga Tumaradó, Cacarica y las comunidades indígenas de CAMERUJ y Juin Phubuur, la Comunidad de La Balsita de Dabeiba, la organización Nahiqui y La Comisión Intereclesial de Justicia y Paz. (Le puede interesar: <https://bit.ly/2LnT4hb>)

### **Ediciones anteriores del Festival por las Memorias** 

Este es el tercer Festival por las memorias que se realiza, bajo la coordinación de la Comisión Intereclesial de Justicia y Paz, los otros Festivales se desarrollaron en los territorios de Cacarica y Alto Guayabal, y al igual que esta ocasión contaron con la participación de diversos delegados de comunidades víctimas del conflicto armados, líderes sociales y organizaciones defensoras de derechos humanos tanto en Colombia, como en el resto del mundo.

Asimismo, se espera que próximamente se puedan desarrollar más Festivales de las Memorias en otras zonas del país como el departamento del Valle del Cauca. (Para más información ingrese al siguiente link  <https://bit.ly/2mhOjcX> )

 

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
