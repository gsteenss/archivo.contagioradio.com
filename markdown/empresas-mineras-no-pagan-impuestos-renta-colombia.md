Title: Empresas mineras no pagan impuestos de renta en Colombia
Date: 2016-11-18 19:30
Category: Economía, Nacional
Tags: Impuestos, La Guajira, Mineria, Reforma tributaria
Slug: empresas-mineras-no-pagan-impuestos-renta-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/foto_mineria.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Víctor Correa 

###### [18 Nov 206] 

**“Paga más impuestos una tienda de barrio que las grandes mineras**”, han denunciado integrantes del Comité Ambiental y Campesino de Cajamarca, quienes buscan impedir la actividad extractiva del proyecto minero La Colosa. Y aunque suena exagerado, así es. En Colombia, las empresas mineras pagan cero pesos de impuesto de renta.

Así lo evidencian la declaraciones de renta de multinacionales como **Carbones El Cerrejón en La Guajira; Cerro Matoso en Córdoba, y Drummond en Cesar**. Compañías cuyos impuestos a pagar aparecen en cero pesos, mientras, el gobierno nacional promueve una reforma tributaria en la que los colombianos más pobres serán quienes deban pagar mayores impuestos.

Y es que un informe de La Contraloría, demuestra que **por cada \$100 que paga en Colombia una multinacional, los colombianos deben devolverle \$200. **Además en comparación a lo que deben pagar el común de los ciudadanos por la gasolina, para las mineras extranjeras un galón de combustible no vale de más de \$3.500.

Esto quiere decir que las multinacionales no le están generando renta a la nación, y "**se viola el principio constitucional de progresividad,** según el cual deberían pagar más quienes más tienen”, afirma el representante a la Cámara Víctor Correa.

Rubén Darío Gómez Cano, secretario general de la Confederación Nacional de Mineros de Colombia,  ha denunciado que de acuerdo con cifras de Planeación en las zonas donde hay grandes explotaciones mineras, las necesidades básicas insatisfechas están por encima del 80%, mientras en zonas donde se desarrolla minería ancestral están por debajo del 30%.

Una prueba de ello, es la empresa Carbones El Cerrejón. Según el documento, **esta multinacional tiene un saldo a favor 11 mil millones de pesos,** mientras su declaración de renta es de cero pesos. Sin embargo, según han denunciado líderes índigenas y afro de La Guajira, El Cerrejón ha profundizando la situación de sequía de la región, lo que ha aumentado los índices de pobreza y además ha generado la muerte por desnutrición y enfermedades respiratorias de miles de integrantes de las comunidades, especialmente de niños y niñas.

Frente a esta situación el congresista Víctor Correa, **hace un llamado a la ciudadanía a movilizarse contra la Reforma Tributaria**, que pretende lesionar el bolsillo de los colombianos más desfavorecidos, mientras continúa dándose beneficios a este tipo de empresas extractivas que están causando irreparables impactos ambientales y sociales en el país.

![carbones-el-cerrejon](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Carbones-El-Cerrejon.jpg){.wp-image-32569 .aligncenter width="540" height="720"}

![cerrejon-zona-norte](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Cerrejon-zona-norte.jpg){.wp-image-32570 .aligncenter width="543" height="724"}

![drummond](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Drummond.jpg){.wp-image-32572 .aligncenter width="545" height="726"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
