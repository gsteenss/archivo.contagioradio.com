Title: Cancionero para señoritas, cuestionando el machismo a través de la danza
Date: 2018-08-27 13:31
Category: Cultura
Tags: Bogotá, Cultura, danza, teatro
Slug: cancionero-para-senoritas-danza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/prensa_3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa CCT 

###### 27 Ago 2018

La danza y el teatro llegan en temporada de estreno llega a la Sala Seki Sano de Bogotá con **"Cancionero para señoritas"**, una propuesta artística a cargo de la compañía **La Bestia en co-producción con el grupo Jóvenes Zapateadores de México**, que tendrá funciones del 27 de agosto al 1 de Septiembre.

El montaje dirigido por el bailarín y coreógrafo **Eduardo Oramas y Sara Rengina Fonseca** bailarina e investigadora, cuenta la historia de **seis mujeres colombianas y mexicanas que viven de manera conflictiva los ideales femeninos** que determinan sus sueños, sus deseos y sobre todo, sus cuerpos.

Entre danzas y cantos tradicionales de los Andes colombianos y la región Veracruciana de México, las mujeres **hablan sobre sus maneras de vivir el amor y la devoción religiosa**. Las formas tradicionales de la guabina, el torbellino y el son jarocho son apropiadas para **cuestionar la cultura machista**, que en esta obra es evidenciada por las mismas protagonistas.

La obra, ganadora de los apoyos a coproducción de espectáculos otorgado por **IBERESCENA**, se presentará a las 8 p.m, con **boletería para público general de \$20.000 y estudiantes o tercera edad \$15.000** disponibles [Aquí](http://corporacioncolombianadeteatro.com/producto/cancionero_para_senoritas/) y en la sala Seki Sano, calle 12 No 2 – 65 de Bogotá.

**Sobre la compañía**

La Bestia es una compañía de creación escénica dirigida por el bailarín y coreógrafo Eduardo Oramas, cuyo fundamento es la **investigación de movimiento, y la idea de que el cuerpo es el lugar en el que construimos nuestra manera de ser y estar en el mundo**.

Desde sus inicios en el 2012, La Bestia ha creado obras que abordan la relación cuerpo-ciudad, explorando el espacio urbano de diferentes maneras. La nueva producción **"Cancionero para señoritas" surge de nuestra pregunta por la presencia de la tradición en las corporalidades contemporáneas,** y es abordada desde la perspectiva de los cuerpos femeninos.

En dos ocasiones la compañía **ha sido ganadora de la beca de Investigación/Creación que otorga el IDARTES:** en el año 2012 con la obra Patafuerte y en el año 2015 con la obra Del otro lado.
