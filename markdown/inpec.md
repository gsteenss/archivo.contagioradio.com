Title: La injerencia del Uribismo en las cárceles según trabajadores del INPEC
Date: 2019-09-09 18:24
Author: CtgAdm
Category: Entrevistas, Política
Tags: Corrupción en Colombia, INPEC, uribismo
Slug: inpec
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Inpec.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/inpec.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Procuraduría] 

Desde el **Sindicato de Empleados Unidos Penitenciarios (SEUP)**, los trabajadores del Instituto Nacional Penitenciario y Carcelario (INPEC), advierten que al interior de las cárceles en Colombia, el abandono estatal sumado a un sesgo político en su interior vinculado al uribismo, ha permitido que se den irregularidades en la contratación de servicios como la seguridad, la salud y la alimentación, además de beneficiar a algunas figuras políticas que pagan su condena entre ellas, a Andrés Felipe Árias.

### Existe un sesgo político en el INPEC

Nélson Barrera, presidente del SEUP, advierte que desde la directriz del INPEC si existe un sesgo político, evidenciado en sucesos como la llegada del ex ministro Árias a la Escuela de Caballería en lugar de una cárcel común. [(Le puede interesar: Andrés Felipe Arias debe ir a un centro carcelario si es extraditado a Colombia)](https://archivo.contagioradio.com/andres-felipe-arias-como-cualquier-colombiano-condenado-por-corrupcion/)

Aunque hace la salvedad que en el Ejército si se han capacitado en el manejo de cárceles, incluso desde el INPEC se desconoce con qué cuentan o no los reclusos que se encuentran en instalaciones como la Escuela de Caballería, pero resalta que **"si fuera tan nefasta la situación, los políticos no pedirían ir allá"**, resaltando que a este lugar solo llegan poderosas figuras del país.

Esta situación, sumada a lo denunciado por la columna de Daniel Coronell quien revela la forma en que el actual director del INPEC favorece o no el  acceso a entrevistas que son claves para el caso de Álvaro Uribe en la compra de falsos testigos,  son elementos que al **SEUP **les permite señalar cómo en la actualidad es imposible desconocer **"que el uribismo tiene una gran ingerencia en los militares de nuestro país"**.

Además de la afinidad que existe entre la Fuerza Pública y partidos como el Centro Democrático, el director señala que **"el INPEC siempre ha sido una cuota política, hemos sido liberales, conservadores, de Cambio Radical y hoy somos del Uribismo"** afirma Barrera quien atribuye a este constante cambio la ausencia de una mejora en las políticas penitenciarias de Colombia.

### El negocio de la salud y la alimentación

Barrera explica que el dinero destinado al sistema de salud penitenciario hace parte de una fideicomiso con un valor aproximado a los 6.000 millones de pesos destinados a 134 establecimientos carcelarios, sin embargo en la mayoría no existe un nivel de atención ni de segundo ni de tercer nivel.

Por el contrario este capital es destinado al traslado de los reos de un centro hospitalario a otro, lo que genera que los recursos se desvíen, según relata Barrera, **"hay cárceles que no tienen los medicamos mínimos, no tienen doctores, ni enfermeras las 24 horas , trasladan médicos y por ahí se va fugando la platica de la salud"**.

En cuanto a la alimentación, Barrera indica que siempre han existido contratos  por cada zona del país con las mismas empresas pero "con nombre diferente". La mala calidad de la comida ha llevado a que varios de los 'ranchos' que reparten los alimentos al interior de las cárceles, hayan sido cerrados por cuestiones de sanidad.

Barrera indica que cada plato de comida para recluso equivale a cerca de \$2.800, que son cobrados por los distribuidores de comida, incluso si no todos se presentan para comer. [(Lea también: La Corrupción en las Cárceles de Colombia)](https://archivo.contagioradio.com/la-corrupcion-en-las-carceles-de-colombia/)

Con respecto a estas problemáticas, señala a la dirección de la Unidad de Servicios Penitenciarios y Carcelarios (USPEC) como culpable de estas deficiencias pues es desde allí que se manejan los recursos del sistema penitenciario.

**Síguenos en Facebook:**  
<iframe id="audio_41569066" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_41569066_4_1.html?c1=ff6600"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
