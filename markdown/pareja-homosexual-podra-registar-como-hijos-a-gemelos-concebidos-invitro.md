Title: Pareja homosexual podrá registrar como hijos a gemelos concebidos invitro
Date: 2015-11-20 14:01
Category: DDHH, LGBTI
Tags: Activistas LGBT, Comunidad LGBTI, Dos hombres homosexuales registran legalmente la paternidad de sus gemelos, familia homoparental, familias homoparentales casadas o en unión libre dentro o fuera del país deben ser reconocidos legalmente como hijos de ambas personas, LGBTI
Slug: pareja-homosexual-podra-registar-como-hijos-a-gemelos-concebidos-invitro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/homo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:ovejarosa 

###### [19 nov 2015]

Una orden de la Corte Constitucional permitirá por primera vez en Colombia que dos  hombres homosexuales puedan registrar legalmente la paternidad  de  sus  gemelos, tras  ser aprobada la ponencia de la Magistrada Gloria Estela Ortiz, defensora de los intereses de la familia homoparental.

En el  fallo se establece que los niños y niñas concebidos en familias homoparentales, casadas o en unión libre dentro o fuera del país, deben ser reconocidos legalmente como hijos de ambas personas y presentar los mismos derechos que los niños de parejas heterosexuales. Desde el 12 de noviembre del 2015, en el  registro civil deben aparecer como padres o madres.

La pareja responsable de interponer la tutela, lleva más de diez años de relación y fue una de las beneficiarias de la sentencia C-577 del 2011, por medio de la cual la Corte Constitucional les otorgó la aprobación legal de ser  una  familia  constituida. Posteriormente  registraron su  vínculo civil en el 2013 en una notaría de Medellín y viajaron a California donde se unieron en matrimonio, y un año  después por  medio de  inseminación in vitro y un vientre de alquiler procrearon gemelos.

La Magistrada Ortiz asegura que el no permitir el registro de los gemelos sería una "vulneración de los  derechos fundamentales y se  evidenciaría  la  discriminación hacia este  núcleo familiar". En la actualidad los menores de un año y medio de edad y cuentan con la nacionalidad norteamericana y la colombiana.

El fallo es celebrado tanto por la  familia homoparental como por la comunidad LGBTI, entre quienes se cuenta a la representante Angélica Lozano, quien  a través de su cuenta de Facebook ratificó el hecho y agregó que **“La Corte Constitucional de Colombia reconoció nuevamente que en nuestro país las familias  son diversas pero tienen los mismos derechos”.**
