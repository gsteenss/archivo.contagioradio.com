Title: Búsqueda de desaparecidos en HidroItuango debe incluir a las víctimas: Ríos Vivos
Date: 2018-05-09 10:47
Category: DDHH, Movilización
Tags: audiencias CIDH, búsqued de personas desparecidas, CIDH, Hidroituango, Movimiento Ríos Vivos
Slug: busqueda-de-desaparecidos-en-hidroituango-debe-incluir-a-las-victimas-rios-vivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/62-masacres-en-los-12-municipios-donde-se-desarrolla-el-proyecto-Hidroituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [09 May 2018] 

En el marco del periodo 168 de audiencias de la Comisión Interamericana de Derechos Humanos, que se realiza en Santo Domingo, República Dominicana, la sociedad civil afectada por el proyecto hidroeléctrico Hidroituango, manifestó su preocupación por la labor realizada por el Estado en **proceso de búsqueda de personas desparecidas** en el área de influencia del proyecto e indicó que las víctimas no han sido tenidas en cuenta.

### **Labores de búsqueda de desaparecidos no son claras** 

En su intervención, el Movimiento Ríos Vivos, cuestionó la labor de entidades como la Fiscalía, quien argumentó que las labores de búsqueda están planteadas para desarrollarse entre mayo y junio en las zonas de influencia del proyecto. De acuerdo con Ríos Vivos, el Estado permitió el desarrollo del proyecto en un lugar donde se sabe que hay fosas comunes donde podría haber por lo menos 2 mil cuerpos de personas desaparecidas.

Para la organización de la sociedad no está clara la forma cómo se va a realizar esta labor teniendo en cuenta que la inundación de la presa **“ya empezó de manera ilegal”.** Afirmó que en dos meses no es posible realizar el proceso de búsqueda toda vez que el proyecto abarca la extensión de 12 municipios en el bajo Cauca antioqueño. (Le puede interesar:["Memoria y Resistencia en el Cañón del Río Cauca"](https://archivo.contagioradio.com/memoria-y-resistencia-en-el-canon-del-rio-cauca/))

Como parte de su intervención, el Estado colombiano manifestó que no es posible hablar de un universo de personas desaparecidas pero que **desde 2014 han sido encontrados 159** cuerpos. Esta afirmación fue cuestionada por uno de los comisionados argumentando la poca claridad de la información aportada en tanto que no se sabe si la búsqueda de personas desaparecidas hace parte de un plan constituido y no es claro el rol de la sociedad civil en este proceso.

### **Fiscalía General de la Nación no garantiza la existencia de la información del número de desaparecidos** 

En esta vía, la sociedad civil insistió en que su participación en los procesos de búsqueda ha sido nula, toda vez que “la Fiscalía General de la Nación ha dicho que **no hay garantía de que haya más cuerpos** en la zona del proyecto Hidroituango”. Además, afirmó que el Estado ha presionado a las víctimas a que de la información a la empresa EPM que desarrolla el proyecto y Ríos Vivos “nunca ha sido tenido en cuenta”. (Le puede interesar:["Comunidades afectadas por Hidroituango temen una posible avalancha"](https://archivo.contagioradio.com/comunidades-afectadas-por-hidroituango-temen-una-posible-avalancha/))

Finalmente, Ríos Vivos le pidió al Estado colombiano que explique cómo se va a realizar la labor de búsqueda teniendo en cuenta que el tiempo **está en contra** por el inicio del llenado de la presa de Hidroituango. Lamentaron que el Estado, basado en una presunción de que no hay cuerpos, no realice las labores de búsqueda por lo que se deben proteger los lugares de inhumación para garantizar los derechos de las víctimas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
