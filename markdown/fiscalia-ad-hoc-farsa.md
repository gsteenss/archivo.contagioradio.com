Title: Fiscalía ad hoc es una farsa: Jorge Enrique Robledo
Date: 2018-12-05 17:35
Author: AdminContagio
Category: Nacional, Política
Tags: Fiscal, Fiscal Ad Hoc, Iván Cepeda, Jorge Robledo, Odebrecht
Slug: fiscalia-ad-hoc-farsa
Status: published

###### [Foto: Contagio Radio] 

###### [12 Dic 2018] 

Diferentes criticas ha suscitado la terna elegida por el presidente Iván Duque para elegir el Fiscal 'ad hoc', algunas de ellas tienen que ver con la idoneidad de los elegidos para ocupar el cargo, otras con el conflicto de intereses y por último, aquellas dirigidas a la figura misma de Fiscalía 'ad hoc', que podría no ser la solución para resolver el caso más grave de corrupción que ha tenido Colombia.

Para el senador por el Polo **Iván Cepeda,** la terna compuesta por Margarita Leonor Cabello Blanco, Clara María González Zabala y Leonardo Espinosa Quintero, tiene errores en su elección, en tanto **el Presidente debió declararse impedido para presentarla**. Su impedimento consistiría en la posible relación que tuvo con Duda Mendoza, cuando Duque era senador de la república y asesor de la campaña de Oscar Iván Zuluaga en 2014.

Adicionalmente, Cepeda señaló que **los 3 nombres que se plantean tienen afinidad con el Gobierno**, en razón de que Cabello es Magistrada de la Corte Suprema de Justicia (ternada por Uribe), González es secretaria jurídica de la presidencia, y Espinosa está ligado a la Universidad Sergio Arboleda, institución de la cual es egresado Duque, y que tiene una línea política afín al uribismo. (Le puede interesar:["Fiscal especial para Odebrecht sería una maniobra de Martínez para manipular el proceso: Robledo"](https://archivo.contagioradio.com/fiscal-ad-hoc-es-otra-maniobra-de-martinez-para-manipular-el-proceso-robledo/))

> Le piden al Presidente Duque una terna independiente para Fiscal ad hoc y propone a una magistrada ternada por Uribe para Fiscal, al decano de su facultad y a su secretaria jurídica! De vainas no ternó a su cantante favorito.  
> No le pega a una Duque que va en 68% desfavorabilidad
>
> — Claudia López (@ClaudiaLopez) [4 de diciembre de 2018](https://twitter.com/ClaudiaLopez/status/1069969861546254336?ref_src=twsrc%5Etfw)

El Presidente defendió su terna argumentando que la Fiscalía 'ad hoc' no es un cargo público sino excepcional, por lo tanto, no aplican las inhabilidades, pero otra crítica que ha surgido es que ninguno de los tres es penalista. Aquellos temas que tendría que investigar el elegido de la terna serían infracciones a la ley penal; y **si por conflicto de intereses se cayeran las designaciones de Cabello y González, sería Espinosa el elegido, un abogado que no es penalista, pero sí experto en derecho comercial, igual que Néstor Humberto Martínez.**

### **“Elección de Fiscal ‘ad hoc’ es una farsa”** 

El también senador por el Polo, Jorge Enrique Robledo, afirmó que el nombramiento de un fiscal ‘ad hoc’ es una farsa de Néstor Humberto Martínez y quienes lo acompañan, porque **quien ocupe ese cargo sólo podría investigar 3 de las 18 líneas de investigación que tiene el caso Odebrecht**.  Posición que también defendió Cepeda, al señalar que el designado podría generar impunidad, al evitar que se sepa a fondo todo sobre este caso de corrupción.

> Un fiscal ad hoc y terna de [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) es otra reafirmación de impunidad para Odebrecht-Grupo Aval. En la terna, Cabello participó en elección de NHM, González no es penalista y si uribistas; se investigarán solo 3 de 18 líneas de investigación. Se necesita nuevo fiscal General
>
> — Ángela María Robledo (@angelamrobledo) [4 de diciembre de 2018](https://twitter.com/angelamrobledo/status/1069989239675412481?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
Por estas razones, Cepeda anunció que **la oposición impugnará ante la Corte Suprema de Justicia el procedimiento empleado para postular la terna**. De igual forma, y ante la negativa de Martínez para retirarse de la Fiscalía, el senador sostuvo que “cabría pensar en una comisión de lucha anticorrupción de carácter internacional como la que está en marcha en Guatemala o Honduras”. (Le puede interesar: [Coger al Fiscal diciendo una verdad es una hazaña: Jorge Robledo"](https://archivo.contagioradio.com/coger-al-fiscal-diciendo-una-verdad-es-una-hazana-jorge-robledo/))

</p>
### **¿Pánico económico?** 

Por último, sobre el “boicot” que se ha promovido en redes sociales contra el Grupo Aval, dada la relación entre Odebrecht y el conglomerado económico, propiedad del Luis Carlos Sarmiento Angulo, Cepeda sostuvo que era una reacción entendible, porque **la situación que se ha generado en torno al Grupo Aval, Corficolombiana y Sarmiento Angulo ha generado mucha indignación.** Esta protesta de tipo financiera invitara a los ciudadanos a dejar de usar productos relacionados con el Grupo Aval como forma de manifestar el inconformismo con la empresa. Dicha manifestación ha sido tachada por algunos como un delito (de pánico económico), a lo que analistas han respondido señalando que se trata del ejercicio de un derecho fundamental: el de la libre expresión.

> Discrepo Jorge: Una cosa es el delito de pánico económico, que debe ser combatido. Pero otra distinta es la invitación a retirar, por razones morales, los dineros de una entidad financiera, que es un ejercicio legítimo de la libertad de expresión. <https://t.co/99CB6DdtY3> — Rodrigo Uprimny (@RodrigoUprimny) [3 de diciembre de 2018](https://twitter.com/RodrigoUprimny/status/1069437168509231104?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
<iframe id="audio_30609047" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30609047_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
