Title: Autodefensas Gaitanistas amenazan a líderes en Barrancabermeja
Date: 2016-07-28 13:06
Category: DDHH, Nacional
Tags: ANZORC, ASCAMCAT, Autodefensas Gaitanistas de Colombia, Catatumbo
Slug: autodefensas-gaitanistas-amenazan-a-lideres-en-barrancabermeja
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/PARAMILITARES-cordoba.gif" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio ] 

###### [28 Julio 2016] 

El pasado lunes el grupo paramilitar de las Autodefensas Gaitanistas circuló en el área urbana de Barrancabermeja un comunicado en el que **amenazan de muerte a todo aquél sindicalista, defensor de derechos humanos y funcionario público** que apoye el proceso de paz con la guerrilla de las FARC-EP y el eventual con el ELN; intimidación que también dirigieron contra quienes participen de marchas, paros, bloqueos y cualquier forma de protesta social.

De acuerdo con Cesar Jerez, vocero de la Cumbre Agraria Étnica, Campesina y Popular, los **59 asesinatos registrados durante los últimos meses en Tibú**, Norte de Santander, prueban el auge de la presencia de grupos paramilitares en la región del Catatumbo, situación que preocupa, sí se tiene en cuenta que allí está planeada la construcción de una de las zonas de concentración para miembros de las FARC-EP.

Otra de las situaciones que preocupa a las comunidades de esta zona, es la presencia de frentes de las guerrillas del ELN y el EPL, que han sostenido enfrentamientos con brigadas del Ejército Nacional, como el ocurrido en la noche de este miércoles; por lo que insisten en que el Gobierno debe atender el llamado de las organizaciones y **garantizar que la presencia de grupos armados no dificulte la implementación de los acuerdos de paz**.

En este sentido insisten en la necesidad de lograr el desmonte del paramilitarismo, según lo pactado en La Habana, e instalar mesas de diálogo con las guerrillas que tienen presencia en la región, porque como asegura Jerez, **no se puede permitir el auge del paramiltarismo bajo la comnivencia de instituciones estatales** y de la fuerza pública, pues con el fenómeno activo será muy díficil la implementación de los acuerdos de paz.

Para encontrar solución a estas y otras problemáticas se ha establecido una mesa de interlocución entre la Asociación Campesina del Catatumbo y el Gobierno que este jueves evaluará los avances en materia de construcción de Zonas de Reserva Campesina, sustitución de cultivos ilícitos, implementación de proyectos mineros y garantías de participación política. Temas en los que los que de acuerdo con Jerez **no han habido avances significativos**, por lo que se espera que las autoridades del orden nacional que asistan al encuentro asuman compromisos puntuales.

[![Comunicado AUG](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Comunicado-AUG.jpg){.aligncenter .size-full .wp-image-26934 width="1011" height="499"}](https://archivo.contagioradio.com/autodefensas-gaitanistas-amenazan-a-lideres-en-barrancabermeja/comunicado-aug/)

<iframe src="http://co.ivoox.com/es/player_ej_12368235_2_1.html?data=kpegmJ2Wd5ahhpywj5aUaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncaSZpJiSo57XpdOfq8rfx9-Sb6LCu7S_pZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
