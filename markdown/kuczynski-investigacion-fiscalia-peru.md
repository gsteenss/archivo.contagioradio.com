Title: Fiscalía anticorrupción en Perú investigará a Kuczynski
Date: 2016-12-14 18:11
Category: El mundo, Política
Tags: corrupción, Pedro Pablo Kuczinsky, Perú
Slug: kuczynski-investigacion-fiscalia-peru
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Pedro-Pablo-Kuczynski-e1465511453189.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

##### 14 Dic 2016 

La unidad anticorrupción de la Fiscalía general del Perú, ordenó re abrir **una investigación por la presunta colaboración de Pedro Pablo Kuczynski**, actual presidente de esa nación, en la **concesión de contratos a la empresa brasilera Odebrecht**, cuando se desempeñaba como primer ministro del entonces mandatario Alejandro Toledo.

Los documentos legales presentados el pasado martes, **motivaron la ampliación de la investigación preliminar tanto a Kuczynski como a Toledo** y a la compañía de construcción, tras concluir que el caso, abierto a inicios del presente año, fue cerrado prematuramente el pasado mes de septiembre.

El proceso inició por solicitud de un abogado que argumenta que para el año 2006 fue aprobada una ley por la cual se cambiaron las reglas de licitación, que s**e ajustó para que la empresa Odebrecht pudiera competir en proyectos de infraestructura por más de 500 millones de dólares**, acusación que Kuczynski negó en declaraciones dadas en ese momento.

Anque el fiscal que había cerrado inicialmente el caso aseguró que la acusación estaba fundamentada en especulación política, **la Fiscalía anticorrupción afirmó que no se había indagado lo suficiente**, ordenando en este nuevo proceso obtener los testimonios de Toledo y de ejecutivos de Odebrecht. Le puede interesar: [Quien es Pedro Pablo Kuczynski](https://archivo.contagioradio.com/quien-es-pedro-pablo-kuczynski-el-nuevo-presidente-de-peru/).

Por el momento no se ha presentado un pronunciamiento oficial desde la oficina presidencial, que ahora enfrenta una situación política complicada por la censura que prepara la oposición en el congreso a su ministro de Educación, y que empaña su publicitada campaña para acabar con la corrupción desde su programa de gobierno.

##### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio
