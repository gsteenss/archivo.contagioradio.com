Title: Las colombianas también paran este 8M
Date: 2018-03-07 15:29
Category: Mujer, Nacional
Tags: 8 de marzo, 8M, acoso sexual, mujeres, violencia sexual
Slug: las-colombianas-tambien-paran-este-8m
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Mujeres-Buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [07 Mar 2018] 

Este 8 de marzo organizaciones feministas y en defensa de los derechos de la mujer se unirán al paro internacional 8M, en el que participarán más de 178 países del mundo, exigiendo garantías laborales, que finalice cualquier tipo de violencia contra las mujeres y en el caso concreto de Colombia, **para que se acabe la guerra que ha usado a las mujeres como botín de guerra.**

El paro del 8M ha sido una apuesta por retomar las reivindicaciones y luchas de las mujeres que fueron desplazadas por la celebración de estereotipos que han sido impuestos socialmente, “con tantas situaciones que perviven y **permanecen contra los derechos de las mujeres, nos hemos dado a preparar esta tarea de 8M que ya lleva dos años”** afirmó Marina Gallego,  directora de la Ruta Pacífica de la Mujeres.

Debido a las actividades electorales del fin de semana, no se podrán llevar a cabo todas las movilizaciones que se esperaba en las ciudades del país, sin embargo, **se harán diferentes actividades como plantones o acciones simbólicas en los territorios**. (Le puede interesar:["La seguridad desde una visión de mujer"](https://archivo.contagioradio.com/mujeres_seguridad_foro_internacional/))

En Bogotá la cita será a las 4 de la tarde en el parque de los Hippies, en la calle 60 con séptima, en donde las mujeres se encontrarán para exigir garantías a la vida y que se tomen medidas y acciones concretas en términos de política que favorezcan los derechos de las mujeres.

### **¿Por qué paran las mujeres colombianas?** 

De acuerdo con Marina Gallego, si bien es cierto que en el país se ha avanzado en lo legislativo en los derechos de las mujeres, en la práctica el panorama es muy distinto.  **“El problema que tenemos es en la materialización de los derechos, es decir que las leyes se cumplan**, un asunto nodal es la violencia contra las mujeres que no cede y que cada vez es más degradado”.

Así mismo afirmó que en Colombia, los más recientes estudios sobre paridad y retribución salarial, las mujeres están muy por debajo de los hombres, a pesar de tener más estudios que ellos. Gallego expresó que el **60% del trabajo informal en el país es desarrollado por mujeres,** que no cuentan con seguridad social, y además semanalmente las mujeres trabajan 33 horas en los hogares sin ser reconocida esa

Finalmente, cabe recordar que los niveles de violencia contra las mujeres en el país parecen no disminuir. Según el último informe Forense 2016 de Medicina Legal, cada 33 minutos una niña es abusada sexualmente; cada 30 horas una mujer es asesinada por razones de género; y  cada día 140 mujeres son agredidas por su pareja o expareja. (Le puede interesar: "

<iframe id="audio_24272646" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24272646_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
