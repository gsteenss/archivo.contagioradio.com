Title: "Soldados prófugos en casos de “falsos positivos” podrán acceder a JEP" Corte Suprema
Date: 2017-07-29 18:05
Category: DDHH, Paz
Tags: acuerdo de paz, Ejecuciones Extrajudiciales, falsos positivos, JEP, proceso de paz
Slug: 44422-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/falsos-positivos-e1459276020392.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Europapress] 

###### [29 Jul. 2017] 

La Corte Suprema de Justicia aseguró que los integrantes de la Fuerza Pública vinculados a procesos o condenados por Ejecuciones Extrajudiciales o "Falsos Positivos", podrán acceder a la Jurisdicción Especial de Paz (JEP), **incluyendo a quienes se encuentren prófugos de la justicia, para quienes se les suspenderían las órdenes de captura.** Decisión que ha causado rechazo por parte de organizaciones sociales, defensores de derechos humanos y víctimas de ejecuciones extrajudiciales.

Así mismo, la Corte Suprema manifiesta que esta es una medida temporal que **no quiere decir que el Estado  no pueda seguir impartiendo justicia** y añade que para estos casos de ejecuciones extrajudiciales no existirá el beneficio de indulto ni amnistía. Le puede interesar:[ "Falsos Positivos" son crímenes de Lesa Humanidad](https://archivo.contagioradio.com/falsos-positivos-son-crimenes-de-lesa-humanidad/)

### **Los 3 primeros beneficiados con esta decisión** 

Para la Corte, las ejecuciones extrajudiciales cometidas por 3 militares beneficiados con esta decisión fueron cometidas en el marco del conflicto armado, razón por la cual si manifiestan su interés de ser parte de la JEP dentro de los próximos 10 días, sus órdenes de captura serán suspendidas.

El primer caso es el **del ex soldado Henry Díaz Fabra quien en 2007 fue condenado por homicidio** en persona protegida y quien pese a haber sido absuelto en primera instancia, fue condenado a 31 años de cárcel en segunda instancia.  Le puede interesar: ["Juez noveno de garantías "se burla" de las víctimas de ejecuciones extrajudiciales"](https://archivo.contagioradio.com/juez-noveno-de-garantias-se-burla-de-las-victimas-de-ejecuciones-extrajudiciales/)

Los otros dos militares son **Rodrigo Galindo Herrera y Mauricio Cubillos Luna,** quienes en Gigante, departamento del Huila, participaron en marzo de 2006 en una operación que dejó un campesino muerto que se encontraba recogiendo café junto a su hijo.

Los integrantes de la Fuerza Pública que se encuentren en la clandestinidad y quieran acceder a los beneficios de la JEP tendrán que demostrar que son militares** o que lo eran en el momento de los hechos,** mostrar que los delitos por los que se les acusa se dieron en el marco del conflicto armado y que las víctimas fueron ejecutadas antes del Acuerdo de Paz.

### **Víctimas y organizaciones emprenderán acciones legales** 

Según el Colectivo de Abogados José Alvear Restrepo es necesario recordar que la ley es clara en exigir que para acogerse a la JEP es necesario que **los militares firmen su intención de contribuir con la verdad, a la justicia restaurativa** y a la garantía de no repetición. Le puede interesar: [Derogan suspensión de audiencias contra militares responsables de Falsos Positivos de Soacha](https://archivo.contagioradio.com/juzgado-44-de-penal-de-conocimiento-da-la-razon-a-las-victimas-de-falsos-positivos/)

En entrevistas anteriores dadas por las madres de Soacha a Contagio Radio y de diversas víctimas y organizaciones han asegurado que están trabajando en varios mecanismos jurídicos para **evitar que los militares implicados en los asesinatos de sus hijos recobren la libertad.**

Cabe recordar que el acuerdo de paz prevé que **será la propia JEP —y no los jueces ordinarios de Colombia— la encargada** de determinar cuáles son los casos de su competencia. Le puede interesar: ["Falsos positivos" no deben pasar a la JEP](https://archivo.contagioradio.com/38886/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="osd-sms-wrapper">

</div>
