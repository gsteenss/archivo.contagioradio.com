Title: ¿Quién defiende a las víctimas?
Date: 2020-10-11 11:47
Author: AdminContagio
Category: Nacional
Tags: Álvaro Uribe, Gabriel Jaimes, Iván Cepeda, Juez 30, Juez 30 de Control de Garantías
Slug: quien-defiende-a-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Quien-defiende-a-las-victimas-en-el-caso-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En la audiencia del pasado jueves, en la que se decide el futuro de la medida de aseguramiento que pesa en contra del expresidente Álvaro Uribe en el proceso que se le adelanta por los delitos de fraude procesal y soborno, todos los intervinientes argumentaron su posición frente a si el expresidente debía seguir detenido en su residencia o por el contrario ser liberado. Sin embargo, **la posición esgrimida por el Fiscal delegado para el caso, Gabriel Jaimes, en el sentido de solicitar la libertad de Uribe, causó malestar y preocupación en varios sectores que han seguido de cerca el proceso y particularmente en las víctimas del caso.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La posición de la Fiscalía en la audiencia, ha causado extrañeza si se tiene en cuenta que la lógica del sistema penal acusatorio de la Ley 906 de 2004 es «adversarial», **es decir la Fiscalía y la defensa del procesado obran como contrapartes en los procesos; de ahí que sea bastante particular que la Fiscalía se muestre afín con los pedidos de su contendor en el caso, especialmente si este se trata de la libertad del indiciado.** (Le puede interesar: [Caso Álvaro Gómez plantea un debate en el que no debe entrar el Gobierno](https://archivo.contagioradio.com/caso-alvaro-gomez-plantea-un-debate-en-el-que-no-debe-entrar-el-gobierno/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador Iván Cepeda, acreditado como víctima en el caso, fue uno de los mayores críticos frente a esta postura de la Fiscalía, argumentando que la tesis del Fiscal Jaimes era «*absurda y peligrosa*» y que la intervención del funcionario le había dado la razón en tener dudas de la actuación que tendría la Fiscalía en el caso pues tenían «*sobrados argumentos para haberlo recusado por su parcialidad*». **Cepeda agregó que el Fiscal Jaimes, «*no pudo disimular que su objetivo es anular lo actuado \[por la Corte Suprema\] y garantizar a como dé lugar la impunidad de Uribe*»**(Le puede interesar: [Iván Cepeda recusará a fiscal y llevará caso Uribe a instancias internacionales](https://archivo.contagioradio.com/ivan-cepeda-recusara-a-fiscal-y-llevara-caso-uribe-a-instancias-internacionales/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1314565920975663105","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1314565920975663105

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La Constitución y la Ley son claras en los deberes que imponen a la Fiscalía en torno a la salvaguarda y garantía de los derechos de las víctimas en los procesos penales. El artículo [250](http://www.secretariasenado.gov.co/senado/basedoc/constitucion_politica_1991_pr008.html#250) de la Constitución en sus numerales 1, 6 y 7, así como el artículo [114](http://www.secretariasenado.gov.co/senado/basedoc/ley_0906_2004_pr002.html#114) del Código de Procedimiento Penal en sus numerales 6, 8 y 12 establecen la obligación de la Fiscalía de velar y proteger los intereses de las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, hay quienes piensan que la Fiscalía no solo no está actuando en ejercicio de esta obligación, sino que por el contrario se está situando del lado del procesado Álvaro Uribe que es a quien debe investigar y eventualmente acusar.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/HELIODOPTERO/status/1314258659837435904","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/HELIODOPTERO/status/1314258659837435904

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/marthaperaltae/status/1314608053573103617","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/marthaperaltae/status/1314608053573103617

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DanielSamperO/status/1314271970582765570","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DanielSamperO/status/1314271970582765570

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Juez 30 de Control de Garantías debe considerar los intereses de las víctimas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Frente a las críticas que ha despertado el actuar del Fiscal Jaimes en el caso Uribe, Contagio Radio indagó a un abogado para preguntarle quién protege a las víctimas ante una aparente posición de la Fiscalía de decantarse por los intereses del procesado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado señaló que la Fiscalía es la dueña del ejercicio de la acción penal y en esa medida defiende los intereses del Estado para investigar y castigar las conductas que lesionan los derechos de las personas. Adicionalmente, aseguró que también dentro de las funciones de la Fiscalía están las de adelantar una investigación integral, imparcial y efectiva que permita determinar la realidad de unos hechos denunciados y la de velar por los derechos de las víctimas en los procesos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El jurista, expresó que si bien la Fiscalía no está obligada a solicitar que la medida de aseguramiento contra Uribe se mantenga, **el ambiente político y social del país, lleva a que se despierte un temor en la ciudadanía sobre la eventual falta de imparcialidad de este órgano** **especialmente en un escenario de «*aparentes relaciones o inclinaciones a beneficiar a una parte*»,** lo cual lleva a una deslegitimación de la propia institución y de la función de la misma.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El legista, aseguró también, que aún mayor que el miedo que produce la solicitud del Fiscal para solicitar la libertad de Uribe, es **la falta de certeza que se tiene sobre si este funcionario sí va a adelantar una investigación que permita probar los hechos que se investigan o simplemente adelantará una «*investigación pobre que se puede caer ante un juez*».** El abogado agregó que es indispensable que la Fiscalía realice una investigación rigurosa y que formule los cargos de manera adecuada porque eso es lo único que garantiza que el juez obtenga los elementos de juicio necesarios para decretar la culpabilidad del procesado y así poder condenarlo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, agregó que si bien el Fiscal puede elevar solicitudes y pedidos en torno al caso, como el que realizó Gabriel Jaimes sobre la libertad de Uribe, **es en últimas la Juez de Garantías quien tiene la potestad de decidir sobre la eventual liberación, o no del expresidente.** En ese orden de ideas, la Juez 30 de Garantías de Bogotá es quien, ponderando los argumentos de los intervinientes en la audiencia, los derechos del procesado y también los de las víctimas, tendrá que determinar si la privación de la libertad de Uribe debe mantenerse o se suspenderse. (Le puede interesar: [Juez 30 mantiene la detención de Álvaro Uribe](https://archivo.contagioradio.com/caso-uribe-ley-600-o-ley-906-justicia-o-impunidad/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
