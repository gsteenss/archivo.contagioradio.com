Title: Madres de la Plaza de Mayo vuelven a ser víctimas del Sistema Judicial de Argentina
Date: 2016-08-04 16:02
Category: El mundo, Mujer
Tags: Argentina, Desaparecidos Argentina, Hebe de Bonafini, Madres Plaza de mayo
Slug: ordenan-captura-sobre-hebe-de-bonafini-titular-de-madres-de-plaza-de-mayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/2221188.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La nación 

###### 4 Agos 2016

Cientos de manifestantes salieron a las calles a protestar ante la orden de detención emitida contra la titular de la Asociación de Madres de la Plaza de Mayo, Hebe de Bonafini, por parte del juez federal Marcelo Martinez de Giorgi, luego de negarse por segunda ocasión a presentarse a declarar en el marco de la causa por presuntas irregularidades en la [ejecución del programa Sueños Compartidos.]{.text_exposed_show}

El magistrado determinó que la titular de las Madres se encuentra en situación de "rebeldía" y que estaba entorpeciendo la investigación por lo que dispuso que sea detenida para ser llevada a declarar, aunque fuentes judiciales informaron a Télam que "cumplida la indagatoria no necesariamente quede libre".

<div class="text_exposed_show">

"Se ordenó un allanamiento a la sede de las Madres de Plaza de Mayo para lograr su aprehensión. La orden de captura ya está librada por lo que si no la detienen ahí podrían hacerlo en otro lado", indicaron fuentes judiciales que remarcaron que el procedimiento será llevado a cabo por mujeres enroladas en la Policía Federal.

El juez dispuso que a lo largo de todo el procedimiento la policía cuente con el acompañamiento de una ambulancia y una vez que se produzca la detención decidirá si procede hoy mismo a indagarla o si deberá "pasar la noche en un destacamento policial" para tomarle indagatoria mañana a primera hora, indicaron las fuentes.

La titular de las Madres de Plaza de Mayo está acusada de haber recibido, con la asociación, financiamiento del Ministerio de Trabajo entre 2008 y 2011 que, según la investigación, no habría sido trasladada a los trabajadores que prestaron sus servicios en el programa de construcción de viviendas Misión Sueños Compartidos.

Bonafini y otras Madres de Plaza de Mayo salieron esta tarde de la sede de la entidad, en la zona de Congreso, para dirigirse a la Plaza de Mayo a cumplir con la tradicional ronda de los jueves.

##### Con información de [Aler Satelital](https://www.facebook.com/aler.satelital/?fref=ts) 

</div>
