Title: Sin vida fueron encontrados los cuerpos de dos integrantes de FARC
Date: 2018-11-22 14:04
Author: AdminContagio
Category: DDHH
Tags: asesinato, Cuerpo, FARC, Iván Duque, lideres sociales
Slug: sin-vida-integrantes-farc
Status: published

###### [Foto: Contagio Radio] 

###### [22 Nov 2018] 

El partido político **Fuerza Alternativa Revolucionaria del Común (FARC)**, denunció en un comunicado que los cuerpos de dos de sus integrantes en Meta y Nariño fueron encontrados sin vida. La organización política alertó que **con estos ataques, son ya 84 los asesinatos de ex-combatientes en proceso de reincorporación**, todo ante la mirada pasiva del Gobierno.

El primer cuerpo fue identificado como **Sebastián Coy Rincón**, integrante del Espacio Territorial de Capacitación y Reincorporación (ETCR) Mariana Páez, ubicado en el departamento del Meta. Coy había sido reportado como desaparecido el pasado jueves 15 de noviembre. Según información de pobladores de la zona, **el hecho ocurrió próximo a donde acampaban integrantes del Ejército Nacional que actúan en el Departamento**.

Por su parte, el cuerpo de **Ángel Aleyser Melenedez fue encontrado en la vereda El Tablón, del municipio de Leiva, en Nariño**; departamento que, junto con Cauca y Antioquia, presenta mayor número de asesinatos contra líderes y movimientos sociales. (Le puede interesar: ["Decreto de Duque no es novedoso en la protección a líderes sociales: Somos Defensores"](https://archivo.contagioradio.com/decreto-de-duque-no-es-novedoso-en-la-proteccion-a-lideres-sociales-somos-defensores/))

\[caption id="attachment\_58561" align="aligncenter" width="462"\][![líderes asesinados](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/Captura-de-pantalla-2018-11-21-a-las-9.11.10-a.m.-462x612.png){.wp-image-58561 .size-medium width="462" height="612"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/Captura-de-pantalla-2018-11-21-a-las-9.11.10-a.m..png) Foto: Indepaz\[/caption\]

### **¿Está el presidente Duque comprometido con los excombatientes?** 

Tras la misiva del Procurador General de la Nación al presidente Duque, pidiendo garantías para el proceso de reincorporación, el mandatario respondió públicamente que “**estamos comprometidos con ayudarle a esa base guerrillera que ha iniciado ese proceso de desmovilización, desarme y reinserción para que eso salga bien**. Nosotros recibimos muchos problemas en los ETCR, pero yo no estoy llorando sobre el pasado, sino tratando de construir sobre el futuro”.

Sin embargo, la FARC recordó que el pasado 6 de noviembre solicitaron una reunión al Presidente para hablar sobre soluciones concretas que brinden garantías de seguridad y protección efectivas para todos los excombatientes que están en proceso de reincorporación; así como  a "los miles de líderes y lideresas sociales que realizan su actividad en los territorios", **pero hasta el momento no han obtenido respuestas**.

Por estas razones, el partido solicitó intervención de la comunidad internacional, "especialmente a los países garantes", con el fin de **demandar al Estado Colombiano su obligación de cumplir lo pactado en el Acuerdo de Paz**. (Le puede interesar: ["Duque se raja en los primeros 90 días de su mandato en defensa de Derechos Humanos"](https://archivo.contagioradio.com/duque-se-raja-en-los-primeros-90-dias-de-su-mandato-en-defensa-de-derechos-humanos/))

[Comunicado FARC](https://www.scribd.com/document/393891747/Comunicado-FARC#from_embed "View Comunicado FARC on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_80939" class="scribd_iframe_embed" title="Comunicado FARC" src="https://www.scribd.com/embeds/393891747/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-WJ9UhxiEnlUEUhudDSS3&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
