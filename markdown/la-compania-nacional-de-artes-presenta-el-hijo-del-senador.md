Title: La Compañía Nacional de Artes presenta: El hijo del Senador
Date: 2019-05-28 10:51
Author: CtgAdm
Category: Cultura
Tags: Bogotá, teatro
Slug: la-compania-nacional-de-artes-presenta-el-hijo-del-senador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/image001.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CNA 

Como parte del lanzamiento de su nuevo espacio cultural y teatral en Bogotá,  **la Compañía Nacional de Artes presenta la obra El hijo del Senador**, una pieza escrita y dirigida por César Morales y protagonizada por Alejandro Gómez, Andrés Torres y Laura Osorio,  **en temporada del 23 de mayo al 1ro de junio**.

La pieza da una nueva y contemporánea mirada al texto de **Strindberg "La señorita Julia"**, donde se muestra lo que puede ocurrir cuando las clases altas pierden los valores morales y los menos favorecido se obsesionan con ascender socialmente. Morales, aterriza el texto en el contexto social- político colombiano, en donde **nuestros destinos, cada vez más, están manipulados por la incómoda y peligrosa omnipresencia de la política**.

El Hijo del Senador, **es el reflejo de la fragmentación que sufre la sociedad gracias a las diversas corrientes políticas, donde lo público trasciende a lo privado.** Un joven en conflicto desea huir de un sistema impuesto por su padre, pero lo que parecía un refugio se transformará en otra decepción. En la cocina de la casa, el hijo del senador, el guardaespaldas y la sirvienta, se enfrentan en una lucha de poder donde se ponen en juego las barreras sociales, los sueños, el amor, los anhelos y las frustraciones.

#### **Sobre el director y la Compañía** 

**César Morales es maestro en arte dramático, actor y director del Teatro Libre** por más de diez años, docente y miembro fundador de la CNA en cuya sede del barrio La Soledad se presenta este montaje y funciona, además, la Compañía Nacional de las Artes, la cual busca, por medio de diversas manifestaciones artísticas, crear propuestas novedosas y dinámicas que satisfagan las necesidades sociales y culturales de la ciudad.

Del 23 de mayo al primero de junio **jueves a sábado a las 8:00 p.m**. Bono de apoyo: **30.000** General Lugar: SALA CNA – Transversal 26b No 41-40
