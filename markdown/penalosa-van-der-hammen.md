Title: Peñalosa obligado a responder por urbanización en Reserva Van der Hammen
Date: 2016-11-10 15:11
Category: Ambiente, Nacional
Tags: Alcaldía Enrique Peñalosa, Reserva Forestal Thomas Van der Hammen, Urbanización en reserva forestal
Slug: penalosa-van-der-hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Reserva-Tomas-Van-der-Hammen-e1457630338748.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Reserva-Tomas-Van-der-Hammen-e1457630338748.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [10 Nov de 2016] 

Desde hace ocho meses Enrique Peñalosa se ha negado a las invitaciones de representantes a la Cámara para que socialice su Plan de Ordenamiento Territorial y el **proyecto de construcción de 80.000 viviendas en la Reserva Van der Hammen**. Su argumento ha sido que “la reserva Van der Hammen y el Plan de Desarrollo **son temas locales que no ameritan debates de control político en el Congreso”.**

El pasado 9 de Noviembre, con 7 votos a favor y 2 en contra, la **Corte Constitucional ordenó que el alcalde Enrique Peñalosa debe asistir a un debate de control político** en el Congreso sobre el futuro de las **1.395 hectáreas que conforman la reserva natural**, y en la cual pretende cumplir su plan de expansión urbana.

### **¿Por qué es importante la Reserva Van der Hammen?** 

Este pulmón verde en medio de la ciudad, cuenta con la presencia de **187 especies de aves, de las cuales 42 son migratorias**, vienen de Norteamérica y de algunos países latinoamericanos. Algunas de estas aves son endémicas, es decir, son exclusivas de esta la región y están **en peligro de extinción, como la Tingua bogotana y el Chamicero Cundiboyacense.**

**La Tingua Pico Verde y el Pato Turrio **también se han reportado en la zona del humedal "El Conejito" que esta dentro de la Reserva. Además es el hogar de **23 especies de mariposas, 11 especies de murciélagos, de mamíferos pequeños y roedores como curíes y musarañas.**

Inti Asprilla uno de los representantes a la Cámara, y quien hizo la solicitud del debate de control manifestó que **“La reserva es de interés nacional estratégico**. Se creó como reserva con base a una orden del Ministerio de Ambiente. **Es una reserva de orden regional. No es un tema solamente local y tenemos que verificar cuál es el interés del alcalde Peñalosa de urbanizar”.**

### **Estas son las preguntas del Debate de Control Político:** 

1.  ¿Qué implicaciones ambientales y legales tiene que en el Acuerdo 011 de 2011 de la CAR, se establezca que la reserva Van Der Hammen forma parte de la estructura ecológica principal del Distrito?
2.  ¿Cuál es la posición de esta Administración sobre la reserva? ¿Qué estudios soportan la posición?
3.  ¿Cuáles son los principales usos del suelo de la reserva?
4.  Especificar recursos invertidos por las Administraciones locales en la reserva, desde el 2011
5.  ¿Cuáles son los principales responsables en el cumplimiento de las obligaciones y financiación para que la reserva cumpla objetivos de su creación?
6.  ¿Las administraciones locales han destinado recursos para comprar predios en la reserva? ¿Cómo se han ejecutado?
7.  ¿Existen programas de adaptación y mitigación del cambio climático y preservación del riesgo para la reserva? Y, ¿existen planes de contingencia para inundaciones?
8.  ¿Las administraciones de Bogotá han generado programas para incentivar la conservación de la reserva, como sustitución de usos no permitidos?
9.  ¿Las administraciones han adelantado campañas de sensibilización ciudadana y educación para proteger la reserva?
10. Como funcionario público, ¿qué tipo de acciones activas o judiciales ha emprendido en relación con la reserva?
11. ¿En qué consiste el proyecto urbanístico que pretende adelantar la actual administración en la reserva?
12. ¿Cuáles serían las compañías constructoras interesadas en adelantar ese proyecto?

Por otra parte, respecto a la citación, en las ultimas horas Enrique Peñalosa publicó en su cuenta de Twitter **“Me da mucho gusto ir al Congreso a hablar sobre Reserva Van Der Hammen. Porque se dicen muchas cosas que no son ciertas”.**

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
