Title: Maltrato animal al código penal
Date: 2015-12-04 10:59
Author: ContagioRadio
Category: Dragonfly, invitado, Opinion
Tags: Alexander Mondragon, Animales, Animalista, Ley 172, Ley Animalista, Maltrato animal
Slug: maltrato-animal-al-codigo-penal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/MA-e1449244583177.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Por ](https://archivo.contagioradio.com/alexander-mondragon/) **[Dragonfly](http://bit.ly/1QkHTjT) **- **[@[MeVle5]

###### 4 Dic 2015 

Desde siempre el ser humano ha tenido como consigna una declaración explícita de guerra sin tregua contra los animales aun cuando ellos no lo estén contra nadie, las leyes así lo han servido para proteger los intereses humanos y su displicencia en el trato a quienes no tienen voz y pueden ser dañados cuando por el contrario por su condición de vulnerabilidad debieran ser protegidos.

Ayer finalmente Colombia se sumó al justo reconocimiento de los animales como seres sintientes, como ha debido ser hace mucho y en línea con la legislatura en otros países, una larga odisea de una sociedad movilizada en vocería de valientes grupos animalistas apoyados en el legislativo por el Representante a la Cámara Juan Carlos Losada y en el Senado por Juan Manuel Galán, un trabajo meritorio para derribar ese muro que los mantenía al nivel de simples cosas en la visión utilitarista del ser humano.

La vara ha quedado muy alta como le he escuchado decir a Natalia Parra, directora de Plataforma ALTO, gracias a todos ellos, reconocimiento justo cuando lejos de triunfalismos han dejado ver que los ganadores no son sino los animales no humanos, que tras haber logrado conseguir la aprobación de la Ley 172 de 2015, no reclaman los réditos que pudieran merecer siendo solo los intermediarios de aquellos que no tienen voz.

Sin embargo queda mucho por hacer, aún quedan protegidas las excepciones que tratan en la Ley 84 de 1989, Artículo 7 Capitulo III, y sin entrar a nombrar los senadores que dejaron ver en su discurso de oposición a los articulados de la ley, por cuanto en voz de ellos mismos defendieran intereses particulares arguyendo simplemente, porque siempre ha sido simplista, el eufemismo recurrido al mencionar la tradición y cultura de algunas actividades en algunas regiones, como el coleo y las corridas de toros, que más que cultural es en realidad el carácter económico de negocios que se lucran del maltrato animal, que además en la mayoría de los casos se sirven de recursos y bienes públicos para usufructo de particulares.

El sector taurino particularmente hoy traduce la ley aprobada con las excepciones tratadas como un blindaje de su barbárica actividad, pero no, muy por el contrario debiera intranquilizarlos porque es un paso más que los acorrala y los acerca más a su abolición, cuánto se quisiera que las excepciones de la ley en mención fueran eliminadas de tajo, derogar ese artículo 7 sería la solución definitiva, sin embargo aún quedan varios recursos para conseguir ese objetivo, el animalismo ha demostrado solventar vicisitudes, guerra sucia y manipulación mediática de los intereses obscuros que se lucran de esos negocios, gremios poderosos que se van viendo limitados en la forma que los defienden, tan solo 4 o 5 senadores de más de 100 presentaron el rechazo y objeciones a la ley, y aunque entre ellos los senadores del Centro Democrático presentaron sus reparos al proyecto original, la mayoría se quedó a la votación, cosa que no hizo el partido Conservador ni el senador Robledo que luego he haber comido (parecía un restaurante toda la sala cerca de las 8pm), abandonaron la sala para sabotear el quorum pero no lo lograron y la Ley fue aprobada por unanimidad.

En Veracruz, México, esta semana con satisfacción ya reportan reducción del 70% en casos de maltrato animal luego de que se promulgara una ley que castiga y condena este delito en forma similar, sin duda podemos esperar que en Colombia los maltratadores se verán disuadidos a seguir haciendo de las suyas y ahora lo piensen dos veces antes de agredir a los animales porque finalmente ya le dieron dientes a la policía para actuar, confiemos que sea la policía entonces garante en sus procedimientos, que acuda al llamado de los denunciantes con celeridad y que no sea solamente la sanción moral y el repudio en redes sociales las que evidencien la crueldad con los animales que no dejaban más que un sentimiento de frustración e impotencia por falta de castigos ejemplarizantes.
