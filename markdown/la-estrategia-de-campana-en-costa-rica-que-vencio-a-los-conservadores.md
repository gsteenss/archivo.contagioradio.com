Title: La estrategia de campaña en Costa Rica que venció a los conservadores
Date: 2018-04-04 20:29
Category: El mundo
Tags: Carlos Alvarado, Costa Rica, elecciones Costa Rica
Slug: la-estrategia-de-campana-en-costa-rica-que-vencio-a-los-conservadores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Carlos-Alvarado-1920-2-e1522879540802.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Reuters] 

###### [4 Abr 2018] 

Costa Rica nuevamente le da una lección a toda América Latina. El pasado domingo, Carlos Alvarado, quien era candidato por el Partido de Acción Ciudadana, derrotó con más del 60% de  los votos a Fabricio Alvarado, el predicador evangélico que entró en la política con el partido confesional Restauración Nacional (PRN) y que prometía defender los "valores tradicionales".

"Se impuso la agenda de la promoción de los DDHH sobre la mezcla de religión y política alrededor de un discurso monotemático", expresa Víctor Morales jefe campaña de Alvarado, quien agrega que " el logro no es de un partido, son los logros de la sociedad costarricense que se ha construido alrededor de una vocación del diálogo, la paz, la equidad y el respeto".

Bajo el eslogan "Elijo el futuro", quien fue vocalista del grupo de rock Dramatika, periodista y escritor, es el presidente electo del país y quien deberá enfrentar los restos de este nuevo periodo gubernamental, de la mano de los movimientos sociales en el país, con quienes **construyó una agenda de gobierno conjunta que pasó por las propuestas de los ambientalistas, las mujeres y los sindicatos, entre otros.**

De acuerdo con Morales  la ciudadanía pudo darse cuenta de **"la inconveniencia de mezclar política y religión,** articulado el eje de la familia y teniendo oposición al tema del matrimonio igualitario. Ese fue el elemento detonador que terminó siendo derrotado en estas elecciones".

### **Participación ciudadana** 

La estrategia del Partido de Acción Ciudadana, se basó justamente en promover la participación de las personas, logrando pactar una agenda de gobierno en la que se tuvo en cuenta las propuestas del movimiento social. **"Logramos pasar a la segunda vuelta con una agenda conjunta y alianzas de sectores de centro y de la sociedad civil" r**eunidas en la Coalición por Costa Rica, a partir de los resultados de la primer vuelta.

Para Morales, dicho resultado del pasado domingo "solo es posible a partir de una determinada cultura política, un elemento específico de la realidad costarricense. Por eso decimos que fue una especie de plebiscito, en términos de que si ver esa cultura política se mantenía o cambiaba".

Es así como la estrategia de la campaña de Alvarado, parte de ver que **"la solución pasa por la activación de la ciudadanía, y no de las élites y cúpulas de poder",** sino a través de una acción consciente y responsable", concluye Víctor Morales.

<iframe id="audio_25113550" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25113550_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
