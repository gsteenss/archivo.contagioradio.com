Title: Líder campesino Orangel Galvis fui víctima de intento de homicidio en Catatumbo
Date: 2018-04-27 12:08
Category: DDHH, Política
Tags: ASCAMCAT, Catatumbo, lideres sociales, violencia contra líderes sociales
Slug: lider-campesino-orangel-galvis-fui-victima-de-intento-de-homicidio-en-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/atentado-e1518713869698.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: T13] 

###### [27 Abr 2018] 

La Asociación Campesina del Catatumbo (ASCAMCAT), denunció que el pasado 26 de abril, en horas de la tarde, Orangel Galvis, integrante de esta organización y líder campesino del municipio de Hacarí, en Norte de Santander fue víctima de un atentado, cuando hombres ingresaron a su vivienda y le dispararon con un fusil. **Galvis pudo escapar y esconderse, mientras que una caravana de campesinos, acompañada por la Defensoría del Pueblo, lo buscaba**.

De acuerdo con Olga Quintero, vocera de ASCAMCAT, Galvis había sido amenazado por personas que se auto identificaron como paramilitares que hacen presencia en la región y esta información ya es de conocimiento por parte del Estado. **Tras el intento de asesinato, el líder campesino logró escapar y adentrarse a las montañas**, desde donde se contactó con su organización. (Le puede interesar:["Asesinan a James Jiménez, líder de proceso de restitución de tierras en Turbo"](https://archivo.contagioradio.com/asesinan-a-james-jimenez-lider-de-proceso-de-restitucion-de-tierras-en-turbo/))

La comitiva que salió en busca de Galvis, afirmó esta mañana que se encuentra bien de salud y que ya está siendo trasladado a un lugar seguro y aseguró que tampoco reconoció a sus agresores. Sin embargo, **para Quintero este hecho se suma a la oleada de violencia contra los líderes sociales en Colombia**.

"Estamos preocupados por la situación en el Catatumbo y contra los líderes sociales” afirmó Quintero, que además, recordó que hace dos semanas fue asesinado el líder campesino Álvaro Pérez. Su cuerpo fue hallado por la Policía el pasado dos de abril, en el tramo de la vía de Astilleros, con un impacto de bala.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
