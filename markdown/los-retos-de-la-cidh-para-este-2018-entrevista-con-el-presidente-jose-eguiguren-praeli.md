Title: Los retos de la CIDH para 2018, entrevista con el presidente José Eguiguren Praeli
Date: 2018-01-22 15:19
Category: DDHH, Entrevistas
Tags: acuerdos de paz, CIDH, colombia, Francisco Eguigures, impunidad, víctimas
Slug: los-retos-de-la-cidh-para-este-2018-entrevista-con-el-presidente-jose-eguiguren-praeli
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/entrevista-presidente-de-la-cidh-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CIDH] 

###### [22 Ene 2017] 

**Francisco José Eguiguren Praeli, presidente de la Comisión Interamericana de Derecho Humanos (CIDH)**, desde marzo del 2017, habló en Contagio Radio sobre el análisis y balance sobre la situación de derechos humanos en el continente y específicamente en el caso de Colombia, en donde a partir del próximo periodo de sesiones será el quien asuma la relatoría de los casos de este país.

### **Contagio Radio: ¿Cuáles son las novedades en la Comisión Interamericana este 2018?** 

**Francisco Eguiguren**: La Comisión se encuentra en un escenario donde, si bien en América Latina y en el continente, hemos avanzado en el establecimiento de regímenes democráticos y la superación de muchas de las viejas prácticas severas y crueles de vulneración a los derechos humanos,** vemos con preocupación una oleada de problemas para los derechos humanos**, debido a formas de violencia de represión de la protesta social, establecidas como una política de Estado.

Además, la Comisión viene afrontando nuevos temas como las problemáticas de los pueblos indígenas, las mujeres, la comunidad LGTBI, **hemos tenido que abrir nuevas unidades que se encarguen del tema de la memoria, de las personas mayores, la discapacidad y el tema de los derechos económicos, sociales y culturales** que tienen serios inconvenientes en el continente.

### **CR: ¿Se pudo resolver la crisis financiera de 2017 en la CIDH?** 

FE: El año pasado, fue particularmente difícil, hacia mediados de año se afrontó un desfinanciamiento que pudo llevar a poner fin a los contratos del 40% del personal, afortunadamente tras un diálogo muy intenso con los Estados se creó conciencia de que la obligación de defender los derechos humanos es un compromiso asumido internacionalmente que presupone cumplir con el pago de las cuotas a la organización de Estados Americanos, **que nos da el 50% del presupuesto, y el resto lo buscamos con Cortes y colaboraciones especiales**.

También se logró, en la asamblea general de la OEA, que se aprobara el acuerdo de duplicar el presupuesto de la Comisión en un periodo de 3 años. Tenemos mucha esperanza en que esto se cumpla y así asegurar la continuidad de algunas líneas permanentes de la Comisión que hoy dependen de recursos adicionales.

En este momento, estamos en una situación delicada, entendemos que transitoria, porque **hemos empezado ya el año y recursos que tenían que disponerse a finales del año pasado no han llegado aún**, pero esperemos que sea una situación coyuntural y que en una semana estemos en una situación más regular.

### **C:¿Cuáles han sido esas estrategias para continuar pese a la falta de financiación?** 

FE: Una de las ideas impulsadas, que es importante, es que la Comisión se acerque más a los pueblos y este en los distintos estados del continente, **por eso una forma de colaboración que conseguimos de algunos estados, fue que financiaran un periodo de sesiones ordinario o extra ordinario en su país**. Así sesionamos en Argentina, Perú, México y Uruguay.

**Empezando este año vamos a sesionar en Colombia**, tenemos otro ofrecimiento de República Dominicana y nos faltan dos más por concretar. Además, hacemos contacto directo con las autoridades, con la población, con las organizaciones de la sociedad civil, con la presan que ven funcionar por más de una semana a la Comisión.

### **C: ¿Esto podría ser un lavado de imagen para algunos países no tan comprometidos con la garantía de Derechos Humanos?** 

FE: No creo que ninguno de los Estados que nos hayan invitado sean Estados que se les pueda señalar como principales responsables de violaciones actuales a los derechos humanos. Estamos en una relación compleja con los Estados, que no puede afectar la independencia de cada uno, pero creo que** citar las sesiones en cada país, es la mejor forma de permitir que se vea la realidad, que nos vean y sí hay algo que ocultar no se va a poder**.

### **C: ¿Cómo ha afectado a la CIDH la incidencia de las empresas sobre los gobiernos, que crean relaciones corruptas?** 

FE: Respecto a los derechos de los pueblos indígenas, a la protección del medio ambiente a la consulta previa que tiene que realizarce para el desarrollo y la aprobación de este tipo de actividades, la Comisión ha sido muy enfática, hemos trabajado muy intensamente.

Este año pondrá más énfasis al tema de la relación derechos humanos y empresa, es un tema de gran actualidad que requiere un compromiso de las empresas y los Estados. Los casos de corrupción hacen parte de esta nube negra que enfrentan los derechos humanos en el continente, porque la corrupción, además de la apropiación de recursos públicos o de sobornos, **hace que el dinero que debería llegar a las obras públicas sea sustraído, generando inestabilidad política**.

En varios países del contiene hemos tenido situaciones de denuncias que comprometen a altas autoridades, a ex presidentes a presidentes, a funcionarios parlamentarios o judiciales, han habido caídas de presidentes.

La Comisión el año pasado emitió su primera resolución sobre el tema de corrupción, derechos humanos y democracia en el continente y estamos brindando atención y tocaremos el tema en las próximas sesiones en Colombia, porque debemos enfrentarlo de manera clara, la próxima Cumbre de las Américas que se desarrollará en Perú tendrá como tema claro la corrupción porque **las principales responsabilidades de corrupción a gran escala, comprometen a altas autoridades gubernamentales y a importantes empresas nacionales y transnacionales**.

### **C: ¿Cómo se tratará el tema de la corrupción en la CIDH?** 

FE: El tema de industrias estractivas, actividades de este tipo, derechos de la consulta previa en pueblos indígenas lo venimos manejando desde la relatoría de los pueblos indígenas desde ya, con la relatoría de derechos económicos, sociales y culturales también vamos a entrar a estos temas de corrupción.

En los hechos vamos a entrar con una unidad especial, pero para crear una relatoría se necesitan financiamiento y recursos, entonces estamos en ese camino pero sin contar aún con los recursos. Porque también hay una dimensión de los derechos humanos en la corrupción, **la denuncia de la misma genera riesgos, dificultades, represión a los defensores de los derechos humanos que denuncian la corrupción política y económica**.

Hay otro tema del debido proceso y la independencia judicial, nosotros tenemosuna relatoría  de defensores de derechos humanos y también una relatoria de independencia judicial, porque son los sistemas judiciales, las fiscalías o los ministerios públicos, los jueces, los tribunales quienes tienen la responsabilidad de investigar y sancionar a la corrupción y eso no siempre ocurre, porque esta también ingresa a los aparatos del sistema judicial.

### **C:¿Qué significa lo que está sucediendo con el presidente actual de Perú, Pedro Pablo Kuczynski y el indulto a Fujimori, para la búsqueda de justicia de las víctimas en este país?** 

FE: En el próximo periodo de sesiones de la CIDH, en Costa Rica, la Corte va a tener la ocasión de realizar la supervisión de dos sentencia que dictó en su momento respecto al Perú, en el caso Barrios Altos y e la Cantuta,  y en el Frontón, para ver si en algún tipo de actos del Estado, como en el indulto al Fujimori o un proceso de juicio político iniciado contra un magistrado del Tribunal Constitucional, resulta compatibles con la responsabilidad y **obligaciones asumidas por el Estado Peruano a partir de estas dos sentencias de la Corte**.

### **C: En medio de esas situaciones y otras vulneraciones de DDHH ¿la incidencia de la CIDH sigue siendo efectiva?** 

FE: La labor de la CIDH ha conseguido en muchos casos resultados muy importantes respecto a la protección de la vida, de la integridad de algunas personas que pueden ser víctimas de detenciones por motivos políticos o de riesgo para su vida. Los Estados en la gran mayoría de casos del continente, a veces con reticencia o molestia, toman muy en cuenta lo que dice la Comisión, y las medidas cautelares que se dictan en general se cumplen.

Además, **hay un impacto interno que ayuda a la protección de los derechos y hace ver a las autoridades que esos casos están siendo observados** y tienen seguimiento internacional. Hay casos más complejos, en donde estamos en permanente contacto con los Estados y con las organizaciones civiles para que la medida dictada se cumpla o se avance en ella.

Ahora para el caso de Colombia,** en enero asumiré ser su relator, es un reto muy grande, la Comisión se encuentra muy interesada en el seguimiento y apoyo al proceso de paz**. Hace algunas semanas, participamos en Washington, en el lanzamiento de unas directrices en el tema de la investigación sobre la vulneración a los derechos de defensores de derechos humanos, en compañía de la Fiscalía General y la Procuraduría.

### **C: ¿cuáles serán esas directrices que asuma la CIDH?** 

FE: El tema de la Justicia Especial para la Paz es prioritario y aprovechando nuestro periodo de sesiones en Colombia, trabajaremos más en detalle con las autoridades y la sociedad civil. Sobre las directrices **buscamos crear mecanismos para un efectivo juzgamiento, investigación y reparación a la vulneración de derechos a defensores**.

También está el tema de la Justicia Transicional, sabemos que hay sectores que no son favorables a ella, seguiremos trabajando con perseverancia en crear consensos y avanzar en la búsqueda de paz, desarrollo, derechos humanos, reconciliación a un país que vivió décadas de violencia, no solo por el conflicto armado sino también por el narcotráfico.

### **C: ¿Cómo abordaría la CIDH la problemática de asesinatos de lideres y defensores de DDHH?** 

FE: Los Acuerdos de paz son un hito, pero es utópico creer que es la solución a todos los problemas, efectivamente preocupa que, a pesar de ello, se produzcan cifras alarmantes de asesinatos o perdidas de vida, nosotros tenemos casos en muchos países en donde se han solicitado medidas cautelares de protección a personas que temían por su seguridad por el accionar de órganos estatales, pero muchas veces, **más por el accionar de órganos privados vinculados a actividades estractivas o del crimen organizado**.

Lo que quiere decir que está la obligación del Estado de brindar protección a las personas en riesgo, que además requiere acciones integrales de los cuerpos del Estado, de seguridad para tener una acción preventiva y de vigilancia contra el crimen organizado que viene, incluso, desde organizaciones de la sociedad civil, que tienen intereses económicos o políticos.

### **C:El año pasado asesinaron en Colombia  líderes sociales que tenían medidas cautelares ¿Qué acciones debe tomar el Estado Colombiano para que esta situación no se le salga de las manos y qué no se pierda credibilidad sobre las medidas otorgadas por la CIDH?** 

FE: Temas sobre tierras e industrias extractivas los tenemos en este momento en varios países del continente, donde la situación de riesgo para los defensores de las comunidades, del ambiente, de la propiedad comunal es muy seria. De parte del** Estado esto requiere un accionar firme porque son acciones criminales contrarias a la ley** y los aparatos judiciales internos tienen y deben asumir el compromiso para investigar y sancionar este tipo de conductas de manejar ejemplar.

Pero también el Estado tiene responsabilidades como, por ejemplo, de allí dónde hay problemas históricos de tierras ancestrales, reconocer el territorio y tener una labor de fiscalización y supervisión al accionar de las empresas. El derecho a la consulta previa busca justamente ser un mecanismo preventivo para lograr el compromiso y la participación de la población directamente involucrada y afectada por este tipo de proyectos en el desarrollo de los mismos. Esto requiere un Estado firme, que haga cumplir los estados internacionales como la consulta previa o la legislación interna, en materia ambiental y de respeto a los derechos.

Cuando el Estado no actúa es complaciente, en ese sentido, lo único que hace es permitir que esto continúe y prolifere. Es muy importante, por ejemplo, la prensa, es un fuente muy importante de información para nosotros, la labor que hacen al enunciar este tipo de sucesos ayuda a ponerla al descubierto y de alguna manera** presiona a las autoridades estatales**.

**Hay intereses intereses poderosos que muchas veces se han acostumbrado a trabajar con la corrupción y con la impunidad**, imponiéndose por encima de los derechos de las personas y de la norma.

### **C: ¿Qué expectativa tienen ustedes con la JEP en materia del esclarecimiento de la verdad, de justicia y reparación a las víctimas?**

FE: Expectativas grandes, permite avocarse de una manera especializada, preferente, con una atención especial a estos temas, nosotros hemos planteado a diversos estados y a Colombia, tener un trabajo más directo para tratar de analizar y enfrentar, los expedientes y buscar en muchos casos soluciones amistosas, que pongan fin a procedimientos en la CIDH que a veces tienen dos décadas. **Esto requiere una actitud activa, una disposición a buscar soluciones y requiere un acercamiento entre Estado, víctimas y organizaciones de la sociedad civil**.

La Comisión espera poder acompañar este proceso, se necesita una voluntad política, en primer lugar, por parte del Estado y las autoridades de llevar adelante este esfuerzo y una disposición de las organizaciones de la sociedad y de las víctimas de tratar de arribar a algún tipo de solución, esperamos que la JEP traiga eso, una dedicación, una justicia, una **lucha contra la impunidad, pero también una reparación y un resarcimiento a las víctimas que han esperado tantos años**.

### **C: ¿Qué mensaje entregar en este momento a los colombianos en materia de la paz y del compromiso de la CIDH hacia la salvaguarda de los derechos humanos en el país?** 

FE: Ratificar nuestro compromiso de apoyar y participar en los que nos corresponde en este tipo de proceso, en donde la dirección corresponde al pueblo colombiano y a sus autoridades. Nosotros coadyuvamos, pero** no podemos sustituir el protagonismo y la responsabilidad que compete a los propios colombianos**.

Estas tareas no son sencillas, trabajar en el cumplimiento de acuerdos, pero sobretodo **reconstruir una sociedad en este terreno de los derechos humanos, de la reconciliación y la paz**, son procesos sociales, décadas no se superan en pocos años.

Tener que aceptar todos estos procesos ha tenido y tiene un altísimo costo humanos, su superación también lo va a tener, supone una visión positiva y realista hacia el futuro, tratando de entender que nunca las posiciones que uno propugna van a ser 100% atendidas porque hay que encontrar puntos de encuentro y conciliación, congruentes con los principios de los derechos humanos, sin tranzar con lo que puede significar la impunidad o la corrupción.

Nosotros** vamos a tratar de acompañar estos procesos de blindar la asistencia técnica que pueda requerirse**, al lado de otras varias organizaciones internacionales, de las Naciones Unidas y de la sociedad que están avocadas a esta tarea con las autoridades gubernamentales de Colombia, con las que tenemos un muy contacto.

Tenemos que poner hombro y fuerza todos, trabajar, sabemos que esto no es fácil, que hay todavía resistencias, que hay muchas heridas abiertas y que esto requiere tiempo y buena voluntad para sacarlo adelante.

<iframe id="audio_23213316" src="https://co.ivoox.com/es/player_ej_23213316_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
