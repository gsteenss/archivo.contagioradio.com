Title: Viaje Literario - Faraón Angola
Date: 2015-01-24 18:04
Author: CtgAdm
Category: Viaje Literario
Tags: Faraón Angola, literatura, poesia, Rodrigo Parra Sandoval
Slug: viaje-literario-faraon-angola
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Captura-de-pantalla-2015-01-24-a-las-13.00.39.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[Viaje Literario - Faraón Angola:]

<iframe src="http://www.ivoox.com/player_ek_3994089_2_1.html?data=lJ6mlpWcfY6ZmKiak5eJd6Kol5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmsrVy8qYrs7YqdPV087cjZKPisLmwoqwlYqmd8%2BfotPU0dHFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

"Si hay un escritor que pueda clasificarse de experimental en Colombia es Rodrigo Parra Sandoval. Con una larga y significativa carrera como investigador en Sociología de la educación, su labor literaria, que cuenta con cerca de veinte títulos publicados, está marcada por la sospecha de que la narrativa tradicional no abarca todos los registros de la realidad, lo que desemboca en una constante apuesta por métodos no tradicionales de representación"
