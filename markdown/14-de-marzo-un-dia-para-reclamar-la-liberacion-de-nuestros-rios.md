Title: 14 de marzo, un día para reclamar la liberación de nuestros ríos
Date: 2018-03-15 14:32
Category: CENSAT, Opinion
Tags: Agua, ambientalistas, Ambientalistas colombia, Ambiente, ríos
Slug: 14-de-marzo-un-dia-para-reclamar-la-liberacion-de-nuestros-rios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/rios-vivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ríos Vivos 

#### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

###### 14 Mar 2018 

En Colombia nuestros grandes ríos han sido la vía utilizada desde la época de la colonia para el saqueo de nuestra naturaleza, actualmente el saqueo se configura a través del modelo minero energético extractivista que privilegia los intereses privados y corporativos estrangulando, trasvasando, fracturando y desapareciendo nuestros ríos en favor de una falacia energética.

En la generación de electricidad para Colombia las represas representan un 70% de la capacidad instalada (a diciembre de 2014) y entre el 70 % y el 80 % de la generación, según las variaciones en la hidrología anual (ANH, 2016). No obstante, teniendo en cuenta el consumo energético nacional por sectores, podemos afirmar que la creciente producción de energía no es para privilegiar a las comunidades sino a las empresas; pues el 80% es para el sector productivo (industrial, minero, transporte, agropecuario) y sólo el 20% para los hogares.  Esto significa, que la hidroenergía que se produce en Colombia tiene una destinación diferenciada, relacionada con el extractivismo, actividades industriales e infraestructura.

Pese a que el depredador modelo minero energético colombiano ha insistido en el carácter sustentable y limpio de la hidroenergía, los efectos negativos han sido comprobados. La emisión de inmensas cantidades de metano que es 34 veces más nocivo en cuanto a efecto invernadero, así como el desplazamiento de comunidades, la militarización de los territorios, la fractura territorial y cultural y el arrasamiento de prácticas y saberes del agua muestran la dimensión de este espejismo “verde”.

Los  impactos de las represas no sólo se reducen a los generados en el proceso previo de ocupación territorial, construcción y llenado, existen numerosos encadenamientos económicos que han significado que las comunidades sobrevivan en condiciones desiguales y cambios continuos que los han empobrecido y revictimizado. Proyectos  agroindustriales, mineros y ahora el turismo son otras formas de explotación y de despojo en los territorios que entran en conflictos con las prácticas y formas de vida de las comunidades.

Sumado a ello el modelo hidroeléctrico quiere crecer, actualmente se proyectan 128 proyectos hidráulicos con capacidad de 4.227 megavatios. Además la generación de energía hidráulica a partir de Pequeñas Centrales Hidroeléctricas (menor a 20 megavatios) en el país ha llegado a un tope de 12.5 gigavatios este año y su potencial es de alrededor de 56 gigavatios, lo que plantea la posibilidad de aumentar en más de cuatro veces la generación de energía a partir de este tipo de proyectos. Esto significa una mayor e intensiva privatización de más ríos y quebradas en el país y con ello la ampliación del copamiento militar y corporativo de nuestras aguas.

Hoy 14 de Marzo conmemoramos el día Mundial Contra las Represas  y en acción por los ríos, el agua y la vida. Las y los afectados con sus comunidades en resistencia  se niegan a dejar de existir, a través de sus apuestas de vida nos invitan a reflexionar sobre nuestra relación con el agua y la necesidad de construir condiciones para una transición energética justa y popular. Ejercicios de memoria ambiental territorial, defensa de las prácticas de barequeo y pesca, preservación de la identidad territorial e implementación de energías autónomas y alternativas, se cuentan dentro del repertorio que las comunidades afectadas por las represas hoy realizan para permanecer y defender su territorio.

Para enfrentar este modelo miles de personas se movilizan hoy, un día para reclamar la vida de los ríos y celebrar los procesos de las comunidades que luchan por protegerlos. Hoy 14 de marzo es también un día para actuar juntas en la búsqueda de constituir una sociedad alternativa basada en los planes de vida, planes de permanencia y soberanías energéticas populares.

¡Ríos para la vida No para la muerte!

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
