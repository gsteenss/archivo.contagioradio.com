Title: La movilización volvió a las calles para rechazar la  Reforma Laboral
Date: 2020-09-08 11:45
Author: AdminContagio
Category: Actualidad, Movilización
Tags: colombia, Movilización social, Paro Nacional, Reforma laboral
Slug: asi-se-activa-la-movilizacion-social-en-el-pais-en-rechazo-a-la-reforma-laboral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-1.41.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 7 de septiembre, en el marco de la Semana por la Paz se dio inicio a las jornadas de **movilización social** **con la Caravana Nacional, contra la reforma laboral y pensional de Duque y la derogación del [Decreto 1174.](https://archivo.contagioradio.com/reforma-laboral-por-decreto-denuncian-gremios-y-sectores-politicos/)**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/maytemisast/status/1303045915398242306","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/maytemisast/status/1303045915398242306

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En en una acción de protesta ciudadanos salieron exigir acciones inmediatas en favor de la vida al presidente Iván Duque. Algunos de esos hechos que suman indignación, según la Carvana, son la impunidad ante las [masacres](https://archivo.contagioradio.com/instalan-campamento-humanitario-por-la-vida-ante-la-crisis-humanitaria-en-colombia/) en Colombia, la designación de US370 millones de dólares para la empresa extranjera [Avianca,](https://archivo.contagioradio.com/si-hay-recursos-para-avianca-pero-no-para-sectores-mas-vulnerables/) mientras el índice de desempleo, pobreza y crisis en el sistema de salud se agudizan.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otro hecho que también generó incoformismo en la ciudadanía, fue el paso de la investigación que se adelantaba en la Corte Suprema de Justicia contra el expresidente Álvaro Uribe Vélez y que ahora será revisada por la Fiscalía.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Colombia se moviliza
--------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Movimientos sindicales, sociales, populares, organizaciones de mujeres, fuerzas sociales y políticas alternativas; entre otros grupos**, se tomaron las calles de Bogotá, Medellín, Cali, Barranquilla, Ibagué, Yopal, Bucaramanca y Popayán. ([«Si no salimos nos van a masacrar a todos… volveremos a las calles»](https://archivo.contagioradio.com/si-no-salimos-nos-van-a-masacrar-a-todos-volveremos-a-las-calles/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Bogotá, las manifestaciones salieron desde el Parque Nacional, por la carrera séptima, rumbo hacia el Ministerio del Trabajo, mientras que **en el resto de ciudades en una acción denominada Caravana Nacional,** recorrieron en carros, motos y bicicletas las diferentes calles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A las exigencias de esta movilización social, se sumaron desde Bucaramanga diferentes colectivos que reclamaron la defensa del Páramo de Santurbán y el rechazo a la intervención de la empresa Arabe Minesa. ([Ni consultas previas ni licencias ambientales deben ser virtuales](https://archivo.contagioradio.com/el-gozar-de-un-ambiente-sano-no-frena-el-desarrollo-del-pais/)[«Por medio de mentiras Minesa pretende desacreditar el movimiento social»](https://archivo.contagioradio.com/por-medio-de-mentiras-minesa-pretende-desacreditar-el-movimiento-social/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**el apoyo a las demandas del movimiento estudiantil universitario en torno a la [matrícula cero](https://archivo.contagioradio.com/retos-para-una-educacion-digna-en-tiempos-de-pandemia/),** la lucha de las comunidades afros, campesinas e indígenas por la defensa de la vida y la madre tierra, así como las garantías del ejercicio de su autonomía política económica y cultural, también hicieron parte de la agenda de movilización.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Igualmente la defensa al derecho a la salud y la bioprotección de las comunidades indígenas , así como también en contra de la fumigación con glifosato**, la defensa de lo público con la venta de Cenit y la activación del fracking en Colombia. ([Senado le dice No al fracking pero todavía no se puede cantar victoria](https://archivo.contagioradio.com/senado-le-dice-no-al-fracking-pero-todavia-no-se-puede-cantar-victoria/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre otros argumentos que se hicieron aún más fuerte en las **exigencias para que se implementen los Acuerdos de Paz**, un trabajo que se da respetando la vida de los territorios y el derecho a la verdad, la justicia, la reparación integral y las garantías de no repetición.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente hicieron un llamado a toda la población a sumarse el próximo 21 de septiembre en la jornada nacional en un Gran Paro Nacional por la vida, y **por el derecho de todas las generaciones a vivir libre de violencia en el país.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y señalaron que en el mes de octubre, el país nuevamente realizará una movilización social en el marco de la [Semana de la Indignación](https://www.justiciaypazcolombia.com/por-la-vida-digna-justicia-social-y-paz-movilizacion-y-resistencia-popular-contra-el-regimen-de-duque/), para seguir avanzando en la preparación ***"desde lo popular, los sectores sociales y políticos en campos y ciudades para el gran paro nacional".***

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
