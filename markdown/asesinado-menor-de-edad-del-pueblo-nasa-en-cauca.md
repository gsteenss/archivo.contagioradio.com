Title: Asesinado menor de edad del pueblo Nasa en Cauca
Date: 2020-04-16 20:46
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Asesinato de indígenas, Cauca, tacueyo
Slug: asesinado-menor-de-edad-del-pueblo-nasa-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/ACIN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Asociación de Cabildos Indígenas del Norte del Cauca

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 15 de abril, el menor Yilber Andrés Yatacué Méndez, de 14 años, integrante de a comunidad La Laguna del resguardo Nasa de Tacueyó fue herido con impacto de bala y posteriormente falleció producto del fuego cruzado entre la Fuerza Pública y la Columna Móvil Dagoberto Ramos según reportes preliminares.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según **Eduin Capaz, coordinador de DD.HH. de la** **Asociación de Cabildos Indígenas del Norte del Cauca [(ACIN)](https://twitter.com/ACIN_Cauca)** los combates ocurridos en horas de la mañana del miércoles 15 se habrían extendido a lo largo de cuatro veredas del resguardo de Tacueyó con una duración promedio de cuatro horas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Fue en medio de este combate que resultó herido el joven Yilber Andrés Yatacué quien se encontraba en su casa junta a su madre y habría sido víctima de una bala perdida que impactó en su frente. [(Lea también: Guerra y Covid-19, comunidades indígenas y afro bajo doble confinamiento en Valle del Cauca)](https://archivo.contagioradio.com/guerra-y-covid-19-comunidades-indigenas-y-afro-bajo-doble-confinamiento-en-valle-del-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las autoridades indígenas señalan que intentaron trasladar al joven al casco urbano de Toribio, sin embargo por la gravedad de su herida, fue remitido a un centro de alto nivel en Cali. **En el trayecto tuvo que ser internado en el hospital Francisco de Paula Santander en Santander de Quilichao, donde finalmente falleció.** [(Le recomendamos leer: Tras asesinato de indígena, Pueblo Awá exige garantías en medio de las balas y el Covid-19)](https://archivo.contagioradio.com/tras-asesinato-de-indigena-pueblo-awa-exige-garantias-en-medio-de-las-balas-y-el-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Urge recalcar el llamado que han hecho las autoridades indígenas a un cese unilateral y bilateral al menos durante la época de confinamiento" expresó Eduin Capaz. El defensor de DD.HH. recalca que l**os pueblos ancestrales han tenido que enfrentar diversos retos en medio de la emergencia sanitaria y el conflicto armado.** [(Lea también: Preservemos la vida ante la pandemia: Espacio humanitario a actores armados en Buenaventura)](https://archivo.contagioradio.com/preservemos-la-vida-ante-la-pandemia-espacio-humanitario-a-actores-armados-en-buenaventura/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre los hechos, el Jefe de la Mision de Verificación de la ONU en Colombia, Carlos Ruiz Massieu instó una vez más al cese de la violencia que afecta a comunidades. [(Le puede interesar: Alimentación, salud, agua y acuerdo humanitario, exigen 110 comunidades a Duque)](https://archivo.contagioradio.com/alimentacion-salud-agua-y-acuerdo-humanitario-exigen-110-comunidades-a-duque/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Ocho resguardos del Norte del Cauca afrontan la guerra en sus territorios

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A un mes de cumplirse el aislamiento preventivo, Capaz señala que **se han registrado combates o acciones armadas en al menos ocho resguardos del norte del Cauca** mientras los señalamientos y amenazas tampoco se han detenido, esto a raíz de los controles que han establecido las comunidades para impedir que el Covid-19 llegue hasta sus territorios. [(Lea también Gobierno debe dialogar con Cuba para enfrentar la pandemia)](https://archivo.contagioradio.com/gobierno-debe-dialogar-con-cuba-para-enfrentar-la-pandemia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito de estas medidas, **se han desplegado 92 puntos de control con un promedio de 15 guardias indígenas por punto a dos turnos lo que lleva a cerca de 2.400 guardias activos para controlar la movilidad y el ingreso a territorios indígenas.** Pese a las medidas, las comunidades han expresado su preocupación pues departamentos como Valle y Cauca continúan escalando en casos positivos de Covid-19.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Menores de edad siguen muriendo en medio de la guerra

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En hechos relacionados, ocurridos en el Valle del Cauca, el pasado 10 de abril en la comunidad de **Unión Agua Clara, bajo San Juan, Buenaventura** f**alleció Abraham Chiripúa Chamarra**, de dos años de edad, convirtiéndose en el tercer niño indígena que muere por confinamiento forzado producto de enfrentamientos. organismo internacional. [(Lea también: Muere tercer niño indígena por confinamiento forzado producto de enfrentamientos)](https://archivo.contagioradio.com/muere-tercer-nino-indigena-por-confinamiento-forzado-producto-de-enfrentamientos/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
