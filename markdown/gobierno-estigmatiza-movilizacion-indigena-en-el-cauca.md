Title: Gobierno estigmatiza movilización indígena en el Cauca
Date: 2019-03-20 17:51
Category: DDHH, Nacional
Tags: Cauca, Guillermo Botero, Minga, Ministro de defensa, paro
Slug: gobierno-estigmatiza-movilizacion-indigena-en-el-cauca
Status: published

###### [Foto: CIRC] 

###### [20 Mar 2019] 

Funcionarios del Gobierno del presidente Iván Duque, como su Ministro de Defensa Guillermo Botero, han aseverado públicamente que la movilización y el paro Indígena, en el departamento del Cauca, están infiltrados por disidencias, hecho que de acuerdo a la defensora de derechos humanos Diana Sánchez, **es una estigmatización al movimiento social y recae en lo "absurdo**".

Sánchez explicó que **en el Cauca las relaciones históricas entre las comunidades Nasa y la entonces guerrilla de las FARC siempre fueron complejas**, debido a que los indígenas se oponen al control territorial de cualquier grupo armado, como lo hacen actualmente con la aparición de las disidencias.

"Decir que detrás de esas mingas, también hay una infiltración de las disidencias es desconocer el país, desconocer un contexto o simplemente **utilizar la estigmatización como una herramienta desde el Estado, para desestimar la protesta social**" afirmó la defensora de derechos humanos.

### **Ministro de defensa enemigo de las movilizaciones** 

Sánchez recordó que en el pasado, el Ministro de Defensa ya había arremetido en contra del derecho a la protesta social, **afirmando que la movilizaciones son financiadas con dineros provenientes de grupos criminales**. (Le pude interesar:["¡Que se retracte el Ministro de Defensa!"](https://archivo.contagioradio.com/retracte-ministro-defensa/))

En ese momento diversas organizaciones defensoras de derechos humanos le exigieron a Botero que se retractara, debido a que ese tipo de afirmaciones no solo vulnera el derecho a la protesta, **sino que deslegitima la movilización, al generar en la ciudadanía un imaginario de que todo aquel que se moviliza pertenece a un grupo armado**, en este caso las disidencias.

Sánchez también aseguró que en el Plan de Acción Oportuna, el gobierno de Duque afirmó que realizaría campañas en contra de la estigmatización hacia los liderazgos sociales en el país. Sin embargo, del mismo modo ordenó realizar campañas en contra a la estigmatización que hay al Estado.

Este hecho para la defensora de derechos humanos, **"no tiene sentido" porque hay una desproporción, ya que el que tiene el uso legítimo de las armas** y todo el aparato represor es el Estado a través de la Fuerza Pública.

Finalmente Sánchez aseguró que el Estado tiene el poder para evitar muertes durante el desarrollo de este paro si atendiera el llamado que están haciendo las poblaciones indígenas, a quienes les han incumplido más de 138 acuerdos que buscaban garantizar sus derechos humanos.

<iframe id="audio_33554943" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33554943_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
