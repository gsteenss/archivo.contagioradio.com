Title: Familiares recibirán restos de Bernardo Beltrán pero seguirán exigiendo verdad
Date: 2017-09-02 17:53
Category: Entrevistas, Nacional
Tags: Bernardo Beltrán, Palacio Justicia, víctimas
Slug: bernardo-beltran-palacio-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/bernardo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Justicia y Paz 

##### 2 Sep 2017 

Al recibir confirmación de la aparición de lo restos de **Bernardo Beltrán**, empleado de la cafetería del Palacio de Justicia durante la toma y retoma ocurrida en noviembre de 1985, su familia no comprende como **32 años después aparece el cuerpo incinerado en el cuarto piso**, cuando múltiples evidencias demuestran que **salió con vida del lugar**.

Sandra Beltrán, hermana de Bernardo, manifestó que la familia recibió la noticia presentada por Medicina legal y la Fiscalía con "**desánimo y con bastante tristeza**, ratificando que los desaparecidos del palacio de justicia **salieron vivos y fueron asesinados y devueltos al Palacio**" Le puede interesar: [Luego de 30 años familiares del Palacio de Justicia siguen sin reparación integral](https://archivo.contagioradio.com/luego-de-30-anos-familiares-del-palacio-de-justicia-siguen-sin-reparacion-integral/)

"Tenemos una reclamación directa con el Estado colombiano, **una explicación  judicial de por qué al salir ellos vivos ahora aparecen**, en el caso específico de Bernardo muerto e incinerado en el cuarto piso, 32 años después de lo ocurrido y sepultado en Manizales en la tumba del magistrado auxiliar **Jorge Alberto Echeverry Correa"**.

A pesar de no poder seguir el orden planteado para dar a conocer la noticia, la familia asegura que tienen "clara la forma en que queremos recibir a nuestro hermano pero tenemos que esperar el protocolo del gobierno reunirnos con cancillería y demás para ponernos de acuerdo para la entrega digna de Bernardo" como ya se hiciera en los casos de Cristina Guarin, Lucy Amparo Oviedo y Luz Mary Portella.

Sin embargo, la familia espera además de recuperar la estructura ósea, que **sigan adelante las investigaciones y la parte jurídica y judicial** "continuamos en búsqueda de la verdad y que nos aclaren las dudas y las grandes brechas que se abren al encontrar a Bernardo incinerado luego de salir vivo".

### **Familia rechaza el manejo dado por los medios ** 

Además, dice Beltrán, que pocos minutos después de haber salido de la reunión con la Fiscalía y Medicina Legal y de conocer la noticia, los medios de comunicación comenzaron a buscarlos para entrevistarlos sin respetar el momento de duelo.

"Salimos y comenzaron a sonar nuestros teléfonos y ya los medios de comunicación sabían que habían aparecido los restos de Bernardo. Ellos se enteran cuando salimos de la Fiscalía. **Nos parece bastante irrespetuoso porque ni siquiera habíamos acabado de digerir la información cuando los medios nos hacían preguntas** a las que nosotros no teníamos respuesta. Fue maluco, molesto que no respeten el espacio tan íntimo los medios".

### **“Esta entrega no se da por voluntad del Estado” Sandra Beltrán**

La hermana de Bernardo asevera que el hallazgo de los restos de Beltrán no se da propiamente por el trabajo que hace el Gobierno, ni por la voluntad política de los últimos 8 Gobiernos, sino que se debe al trabajo de la familia, las organizaciones sociales acompañantes y la sentencia de la CIDH.

**“Esto se debe al grupo de abogados, nuestros representantes y de entidades que nos han acompañado en este proceso,** más la persistencia y resistencia de nosotros como familiares. Lo que hace el Gobierno es cumplir la orden de la condena proferida por la Corte Interamericana de Derechos Humanos – CIDH-. No es voluntad del Estado, solo obedecen una orden”.

### **Familia del Magistrado Echeverry se encuentra impactada por la noticia** 

Relata Beltrán que el impacto ha sido muy fuerte para la familia Echeverry porque no se esperaban una noticia de este tipo 30 años después de creer que habían enterrado al magistrado.

“**Yo pude hablar con uno de sus hermanos, está absolutamente descompuesto, triste.** Sentí yo que él estaba defraudado, engañado, aturdido y bastante ofendido por todos estos hechos (…) Queremos agradecer y expresar toda la solidaridad y el respeto, nuestro agradecimiento a la familia del Magistrado Auxiliar Echeverry por haber cuidado, protegido, respetado y amado a Bernardo en esa tumba”

Dice que van a esperar que la Cancillería llame a la familia y los cite para hablar y poder gestionar la entrega de los restos, sin embargo, a la fecha no tienen una citación exacta para darle sepultura y llevarlo a la tumba donde se encuentra la mamá de Bernardo Beltrán.

<iframe id="audio_20682231" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20682231_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
