Title: El drama de la salud de las comunidades de La Guajira por la minería de El Cerrejón
Date: 2017-11-20 15:10
Category: DDHH, Nacional
Tags: El Cerrejón, La Guajira
Slug: comunidades-de-la-guajira-exige
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/La-Guajira-e1480359645641.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Defensoría del Pueblo] 

###### [20 Nov 2017] 

A propósito del Día Universal de los Derechos de las niñas y los niños, este lunes en el Congreso de la República se desarrolló una audiencia pública sobre la situación de las y los menores de edad integrantes de las comunidades de La Guajira, afectados por la **actividad minera de la empresa El Cerrejón.**

Así lo denuncian las comunidades y la abogada Dora Lucy Arias, integrante del Colectivo de Abogados José Alvear Restrepo, quien señala que esta audiencia se convoca para llamar la atención nuevamente "no a voces, sino a gritos sobre los impactos en la salud de la minería de carbón en las comunidades indígenas y afrodescendientes".

En ese sentido, han traído a la capital casos puntuales de niñas y niños gravemente afectados por la extracción de carbón a manos de El Cerrejón, como es el caso de Moisés Guette, niño indígena Wayuu de 2 años quien padece graves problemas respiratorios y de hemoglobina, por cuenta de las **fuertes explosiones producidas para la extracción carbonífera, según denuncian.** (Le puede interesar[: Cerrejón incumple tutela a menor de dos años)](https://archivo.contagioradio.com/el-cerrejon-incumple-tutela-fallada-a-favor-de-un-menor-de-2-anos/)

Los niños están sufriendo mayores enfermedades pulmonares, de la piel y del estómago, y aunque usualmente la explicación que se da es la desnutrición, se trata de un problema cuyas raíces nacen en la actividad minera que acaba con la comida de las comunidades y con la calidad del aire, debido a la contaminación que del agua, la tierra, y el aire.

### **Las exigencias de las comunidades** 

**Ante dicha situación y 35 años después de que iniciara la extracción de carbón, el Tribunal de La Guajira le dijo al gobierno que debe realizar los estudios** pertinentes para determinar científicamente este impacto en la salud y en el ambiente. A su vez, las comunidades indígenas exigen que se aplique el principio de precaución y prevención, para que se impida que se mantenga esa contaminación hasta tanto, no se verifique que esa actividad es inocua.

**No obstante, "ha habido una respuesta muy cosmética por parte de la empresa y el gobierno",** señala la abogada, quien agrega que pareciera que se busca tapar esta situación para impedir que no se debata de manera seria para que el problema sea atendido de la manera adecuada.

### **Otras actividades de visibilización** 

Además de la audiencia, hay otra serie de actividades que se realizarán en Bogotá para que se conozca la realidad de este territorio. Una de ellas es una exposición fotográfica en la Plaza de Bolívar el lunes en la mañana, y en la tarde de 2 a 5 pm, se presentará en la Facultad de medicina en la Universidad Nacional.

El martes se realizará la II Conferencia Temática:  ​“Los derechos de los Niños, Niñas, Adolescentes y Jóvenes, uno de los Desafíos de la PAZ en Colombia”.  de 09:00 a 5:30 p.m. en las instalaciones del Centro Cultural Gabriel García Márquez, -Auditorio Rogelio Salmona. (Le puede interesar: [Resguardo provincial de la Guajira denuncia restricción de acceso al agua por parte del Cerrejón)](https://archivo.contagioradio.com/?p=46387)

<iframe id="audio_22181672" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22181672_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
