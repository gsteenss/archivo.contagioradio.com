Title: Más de 300 mil hectáreas se han entregado de forma irregular en Colombia
Date: 2018-09-28 16:37
Author: AdminContagio
Category: DDHH, Nacional
Tags: ANT, baldios, restitución, tierras, UAF
Slug: 300-hectareas-irregular-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/informe-platano083116_22.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Amnistía Internacional] 

###### [28 Sep 2018] 

**Entre 1991 y 2015 se habrían registrado 12.005 casos de baldíos entregados en forma irregular** usando la figura de prescripción adquisitiva del dominio. En total, **habrían sido cerca de 300.000 hectáreas en Córdoba, Boyacá, Tolima, Casanare y Nariño** que fueron asignados a personas que no podrían ser sujetos de reforma agraria.

El estudio del grupo de investigación de Derecho Constitucional y Derechos Humanos, de la Facultad de Derecho, Ciencias Políticas y Sociales de la Universidad Nacional fue entregado a la Corte Constitucional esta semana, y resalta que esta situación se podría estar presentando en todos los departamentos con mayor presencia institucional, dado que este procedimiento irregular es judicialmente "sofisticado".

La investigación se produjo en seguimiento de la sentencia[T-488](http://www.corteconstitucional.gov.co/RELATORIA/2014/T-488-14.htm)de 2014 emitida por la Corte Constitucional, en la que se detectó que los jueces civiles estaban entregando presuntos baldíos a particulares a través de un proceso judicial llamado **prescripción adquisitiva de dominio**; que es una figura legal en la que el propietario de unos predios los abandona y otra persona los habita y explota.

Pero la abogada e integrante del grupo de académicos que presentó la investigación, **Diana Isabel Güiza**, recordó que la norma establece que **"los particulares pueden acceder a los baldíos siempre y cuando sean sujetos de reforma agraria"**, es decir, que no tengan otra propiedad rural y su patrimonio sea inferior a los mil salarios mínimos. Adicionalmente, exige que estén usando los baldíos según los topes de extensión, cuya medida establece para cada región la **Unidad Agricola Familiar (UAF).**

Los requisitos para entregar los baldíos deben ser corroborados por la **Agencia Nacional de Tierras (ANT)**, antes INCODER, mediante un trámite llamado adjudicación administrativa. Pero en la sentencia de la Corte, ya **se había detectado que jueces estaban entregando presuntos baldíos por una ruta distinta que no verifica estos requisitos,** haciendo que personas que no son sujetos de reforma agraria se queden con esos baldíos, incluso en extensiones que superan la UAF.

### **Se podrían haber entregado más de 300 mil hectáreas de tierras ** 

En el estudio, los investigadores detectaron que entre 1991 y 2015 se habrían dado por lo menos 12.005 procesos de este tipo. Y Güiza aseguró que, aunque la información sobre la extensión no es precisa porque se trabajó basados en datos arrojados por la Super Superintendencia de Notariado y Registro, cuyas observaciones no tienen información sobre la extensión de los predios en un 18% de los casos, **más de 300 mil hectáreas habrían sido entregadas por esa ruta**.

Si se compara esta cifra **(300.000 ha) con el número de hectáreas con sentencia restitutiva (313.129 ha) por parte de la Unidad de Restitución de Tierras hasta julio de 2018**, se podría decir que usando el sistema judicial, se ha entregado a personas no sujetos de reforma agraria la misma cantidad de tierras que a las víctimas del conflicto. (Le puede interesar: ["Lucha entre cifras y hechos, los resultados de la restitución de tierras"](https://archivo.contagioradio.com/asi-trabaja-unidad-restitucion-tierras/))

### **Es una vía sofisticada con la que se están fugando baldíos** 

El equipo hizo un análisis estadístico del comportamiento de este fenómeno en distintos territorios; encontrando que **Córdoba (3.484 casos) y Boyacá (2.699 casos)** ocupan los primeros lugares por el número de casos; y añadió que en la lista también aparecen departamentos como Casanare , Tolima y Nariño. Sin embargo, Caquetá y Putumayo registran un bajo número de casos, porque requieren cierta presencia consolidada del Estado para que haya disponibilidad de jueces que no ocurre en esas zonas.

Los académicos detectaron que hay unos **juzgados específicos en Paipa y Pesca que han entregado 30.900 ha y 19.000 ha respectivamente**; y gracias al seguimiento de algunos de estos casos, los investigadores se dieron cuenta que el fenómeno se concentro entre 2008 y 2013,  en los que habría ocurrido el 50% de los casos detectados.

### **¿Qué tan problemático es esto?** 

Una de las alertas que sirvió para hacer la investigación fue la comparación del promedio de tierra entregada por departamento con el promedio de UAF, hecho que evidenció comportamientos problemáticos. Según la abogada**, en Casanare el promedio de extensión de predios entregados vía irregular es 11 veces mayor que las UAF** del Departamento. (Le puede interesar:["Octubre, fecha crucial para la restitución de tierras"](https://archivo.contagioradio.com/octubre-limite-restitucion-de-tierras/))

Esa dinámica es problemática especialmente en el contexto de posacuerdo que vivimos, pues como lo explicó Güiza, el Fondo de Tierras busca formalizar y redistribuir la tierra; pero entre los diagnósticos hechos para implementar este Fondo, se ha encontrado que faltan baldíos disponibles para ser entregados, y **la investigación demuestra que no faltan baldíos sino que se están entregando de forma irregular.** (Le puede interesar: ["Ley de tierras de Gobierno acabaría con banco de tierras para la paz"](https://archivo.contagioradio.com/proyecto-de-ley-de-gobierno-acabaria-con-banco-de-tierras-para-la-paz/))

Adicionalmente, hay iniciativas en el Congreso que pretenden legislar sobre la entrega de tierras, se refirió al Proyecto de Ley 03 de 2018 del gobierno santos presentado hace 2 meses, con el que se busca modificar la Ley 160 de 1994 sobre la Reforma Agraria. También, hace su curso legislativo un Proyecto que busca modificar la Ley 1448 sobre reparación integral de víctimas, que incluye medidas sobre la tenencia de tierras. (Le puede interesar: ["María Fernanda Cabal busca proteger a despojadores de tierras")](https://archivo.contagioradio.com/maria-fernanda-cabal-busca-proteger-a-despojadores-de-tierras/)

### **Hay acciones que el Estado puede implementar para solucionar este problema** 

Güiza manifestó que **hay personas que se han apropiado de 10 a 12 predios gracias a la prescripción adquisitiva del dominio**, pero indicó que hay diferentes acciones que el Estado puede tomar para remediar la situación: En primer lugar, la procuraduría delegada de tierras y la ANT deberían focalizar los casos más problemáticos para recuperar las propiedades; en segundo lugar, que se abran investigaciones penales para quienes han acumulado tierras, e **investigaciones disciplinarias para los jueces que lo han permitido.**

Por último, la abogada afirmó que es decisivo que el Estado tenga una acción coordinada  para clarificar cuales predios son efectivamente  baldíos y recuperarlos, porque **si no se resolviera esta problemática, se dejaría en una grave situación a campesinos que sí son sujetos de reforma agraria.** (Le puede interesar: ["En más del 90% de los casos de restitución de tierras existen intereses mineroenergéticos"](https://archivo.contagioradio.com/concesiones-minero-energeticas-en-restitucion-de-tierras-no-han-permitido-retorno-de-victimas-movice/))

<iframe id="audio_28958863" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28958863_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
