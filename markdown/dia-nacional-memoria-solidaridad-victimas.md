Title: Así se conmemora el Día Nacional de la Memoria y Solidaridad con las Víctimas
Date: 2019-04-09 15:47
Author: CtgAdm
Category: Movilización, Nacional
Tags: 9 de abril, memoria, víctimas
Slug: dia-nacional-memoria-solidaridad-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-09-at-10.09.29-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

[Este martes 9 de abril se realizarán diferentes actividades para conmemorar el día nacional de la memoria y la solidaridad con las víctimas del conflicto armado; movimientos y organizaciones sociales de todo el país participaran en caminatas, plantones y recorridos de memoria para recordar los hechos ocurridos en más de 60 años de conflicto y evitar que se repitan. Para Erick Arellana, vocero del Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE), este día también es una oportunidad de visibilizar a las personas que son víctimas, y sus justos reclamos.]

Arellana indicó que algunas de las actividades estarán enmarcadas en el contexto de la Minga, razón por la que a las 9 de la mañana se realizará un ritual de armonización en la plazoleta del Concejo de Bogotá, en la calle 26 con Carrera 28. A las 2 de la tarde, en la Cámara de Representantes (salón Elíptico) se realizará una audiencia pública para escuchar a las víctimas, por su parte, en la Plaza de Bolívar se realizará una audiencia en paralelo como protesta por el poco cumplimiento que se da a las necesidades de las víctimas en este recinto.

Sobre las 3 de la tarde se realizará una caminata desde el Centro de Memoria, Paz y Reconciliación (CMPR) hasta el Parque Nacional, en el que se hará un encuentro. El Vocero de MOVICE señaló que concluirán la jornada de eventos con una velaton frente al Búnker de la Fiscalía General de la Nación, en protesta por la poca efectividad en als investigaciones sobre hechos ocurridos en el conflicto. (Le puede interesar:["Hablan por las sobrevivientes del conflicto pero cuando van al Congreso no las escuchan"](https://archivo.contagioradio.com/victimas-congreso-escuchan/))

Por su parte, desde el CMPR se realizará una Bici Movilización \#SinOlvido, que saldrá desde el Centro, se dirigirá en Caravana hacía la Confederación de Trabajadores de Colombia en la Calle 39 con Carrera 24, posteriormente visitará el Monumento de Jaime Garzon, la oficina de Eduardo Umaña y el Club el Nogal; y finalizará en la Universidad Pedagógica Nacional.

### **Eventos que se realizarán en otras ciudades**

En Ibague se realizará un acto cultural con varias organizaciones de víctimas a las 5 de la tarde en la plaza central de la ciudad; en Tunja se desarrollará un acto simbólico en el centro de la ciudad y en Sucre se realizará un conversatorio sobre la Unidad de Víctimas. De igual forma, en Barranquilla se realizará una jornada de movilización desde las 6 de la tarde.

> Mañana 9 de abril, día nacional de las Víctimas.  
> Este será el recorrido que inicia a las 9am en la Plazoleta Concejo de Bogotá.[\#MingaParaSanar](https://twitter.com/hashtag/MingaParaSanar?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/u0q2cr8DMR](https://t.co/u0q2cr8DMR)
>
> — Corporación Reiniciar (@Reiniciarco) [9 de abril de 2019](https://twitter.com/Reiniciarco/status/1115446246951591937?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo] [[Contagio Radio] 
