Title: Hay un déficit de 300 controladores aéreos en Colombia
Date: 2017-10-08 08:25
Category: DDHH, Entrevistas
Tags: Avianca, Pilotos ACDAC, SIndicatos
Slug: controladores-aereos-inician-operacion-reglamento-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/operación-reglamento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: webinfomil 

En un comunicado la **Asociación Colombiana de Controladores de Tránsito Aéreo** invita a los controladores a seguir cumpliendo las funciones de control aéreo dentro del “estricto sentido de la reglamentación aeronáutica” en respaldo a las exigencias de los pilotos de ACDAC, en defensa del derecho a la protesta y por los incumplimientos de la Aeronáutica Civil a los acuerdos alcanzados con el gremio.

Según la Asociación la “operación reglamento” está motivada por varias razones, entre ellas los ataques al sindicalismo por parte de multinacionales. Por otra parte califican como “irregulares” las decisiones de la Aeronáutica civil que “ponen en riesgo la seguridad aérea y que, según ellos, incrementan la carga laboral de los trabajadores en operaciones de control aéreo. [Ver: Estas son las exigencias de los pilotos de Avianca](https://archivo.contagioradio.com/estas-son-las-exigencias-de-los-pilotos-de-avianca/)

Además, en el comunicado público, señalan su inconformidad con la administración militar “aun habiendo conflicto de intereses” y “la falta de competencia” por parte de las directivas y a las afectaciones que han sufrido en el marco de los traslados a las nuevas instalaciones del aeropuerto. Por último aseguran que se oponen a las posibles “indebidas permisiones” de la Aeronáutica civil a las compañías aéreas.

Esta movilización también es un llamado a los controladores a “aunarse en la defensa de los preceptos constitucionales que “garantizan el derecho a la protesta y al ejercicio sindical de negociación y de participación colectiva como garantía de las mejoras y justas condiciones laborales.

###### Reciba toda la información de Contagio Radio en [[su correo]
