Title: Arnoldo Palacios una estrella más en el cielo
Date: 2015-11-16 18:40
Category: Cultura, Viaje Literario
Tags: 9 de abril 1948, Arnoldo Palacios, Chocó, Escritores Chocoanos, Las estrellas son negras, Muere escritor Arnoldo Palacio
Slug: arnoldo-palacios-una-estrella-mas-en-el-cielo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Revistaarcadia.com_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:revistaarcadia.com 

###### [13 Nov 2015]

El escritor chocoano **Arnoldo Palacios autor de “Las estrellas son negras” falleció a los 91 años en Bogotá** dejando una obra con la que dialogó sobre el hombre, sus problemas, sus sueños, su vida íntima, su fuerza, su vigor, su esperanza y sus luchas por alcanzar mejor niveles de desarrollo y satisfacción colectiva y personal en todo los sentidos, además de una voz de protesta contra las injusticias propiciadas por el Estado, una obra que definió su voz como la de una comunidad.

**Las mil y una Noches fueron las historias que abrieron su perspectiva hacia las letras**, además la poliomelitis de la que fue víctima “**a mí me llevó a escribir, probablemente, el hecho que en mi infancia sufrí un ataque de poliomielitis** que me atacó sobre todo las piernas, los músculos motores, yo tenía dos y ya caminaba, me fui a bañar y en el río me atacó el virus, pero después ya no pude caminar, no pude correr por el pueblo como lo había hecho siempre, ni ir al río a bañarme con mis amigos”.

El **original de su obra** “Las estrellas son negras”, **se perdió bajó las llamas el 9 de abril de 1948**, sin embargo esto no fue impedimento para publicarla y **recibir la Cruz de Boyacá, en 1998,** pero Palacios sostuvo que esa distinción no dio dinero, además recibió numerosos reconocimientos por su obra, el Ministerio de Cultura reeditó “Las estrellas son negras” en ese mismo año y tuvo la oportunidad de fundar una asociación de estudios sobre el Chocó.

Arnoldo Palacios, un hombre inquieto que viajó y vivió en Francia, un hombre que se sostuvo en muletas, pero estan cimentadas de letras e historias, **un chocoano que no dejará de representar muchas voces y toda una raza**.
