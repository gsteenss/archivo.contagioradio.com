Title: Randy Berry, encargado por EE.UU para defender los derechos LGBTI en el mundo
Date: 2015-03-16 21:57
Author: CtgAdm
Category: El mundo, LGBTI
Tags: comunidad gay, LGBTI, Randy Berry
Slug: randy-berry-encargado-por-ee-uu-para-defender-los-derechos-lgbti-en-el-mundo
Status: published

El secretario de Estados Unidos John Kerry, nombro al diplomático **Randy Berry** actual cónsul general en los Países Bajos como enviado global para combatir y defender los derechos de la comunidad LGBTI en el mundo.

En el discurso de nombramiento Kerry señalo: ''*La defensa y promoción de los derechos humanos de las personas LGBT es el núcleo de nuestro compromiso con la promoción de los derechos humanos en todo el mundo - el corazón y la conciencia de nuestra diplomacia*" el anuncio se da luego de varios pronunciamientos que desde la casa blanca se han emitido en **defensa de la comunidad LGBTI en el mundo.**

El papel que defenderá Beery en el mundo tendrá como objetivo **impulsar esfuerzos para que se respeten los derechos humanos de la comunidad LGBTI** y que  se garantice la igualdad y dignidad de las personas sin importar su orientación sexual o identidad de género, resalta el comunicado del secretario de los Estados Unidos.

Cabe recordar que en 75 países se penaliza la homosexualidad y las relaciones entre personas del mismo sexo y en 7 de estos países se aplica la pena de muerte.
