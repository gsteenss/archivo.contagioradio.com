Title: Exigen a empresa francesa terminar complicidad con apartheid Israelí
Date: 2017-08-12 11:00
Category: Onda Palestina
Tags: Apartheid, BDS, Israel, Palestina
Slug: francesa-empresa-axa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/francesa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:palestinalibre.org 

###### 10 Ago 2017 

Más de Setenta **organizaciones de la sociedad civil francesa**, exigen al conglomerado de seguros **AXA** a que ponga fin a su apoyo a la ocupación, el colonialismo y el apartheid israelí.

Las organizaciones le exigen a esta empresa de seguros que se desprenda de tres bancos israelíes, **Hapoalim, Leumi y Mizrahi Tefahot**, que están involucrados en la construcción de asentamientos ilegales israelíes en tierras palestinas ocupadas. Los grupos también señalan que a través de su filial AB, **AXA tiene acciones en el fabricante israelí de armas Elbit**.

En el comunicado publicado el viernes de la semana pasada los grupos de la sociedad civil, entre los que se encuentran sindicatos, asociaciones estudiantiles, agrupaciones religiosas y culturales, señalan que **las campañas en solidaridad con Palestina han obligado a varias grandes empresas, como Veolia y Orange, a poner fin a su participación en la colonización israelí**.

**La empresa de seguros no se ha manifestado aún**, por lo que se espera próximas manifestaciones exigiendo que cese la complicidad con Israel.

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
