Title: “Queremos que nos crean que si estamos por la paz” Timoleón Jiménez
Date: 2015-11-23 17:29
Category: Nacional, Paz
Tags: Diálogos de La Habana, dialogos de paz, Diálogos Gobierno y FARC
Slug: queremos-que-nos-crean-que-si-estamos-por-la-paz-timoleon-jimenez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Entrevista-Timoléon.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: YouTube 

<iframe src="http://www.ivoox.com/player_ek_9484092_2_1.html?data=mpmllpWddo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56nldbZ08ra0diPtdbZjNPc1ZDHtsbVz5De18qPt8qfxtjhw9LTt4zk0NeYzsaPtMLuhqqfh52UaZq4jJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Timoleón Jiménez, FARC EP] 

###### [23 Nov 2015 ]

[En entrevista concedida al 'Informativo Insurgente', Timoleón Jiménez extendió un saludo fraterno a los guerrilleros recluidos en las cárceles que en sus palabras “libran una lucha por sus derechos”, así mismo aseguró que las FARC no se cerraron a la banda, se sentaron a releer y discutir con el Gobierno el **acuerdo de justicia transicional** y de lo pactado hasta el momento **hay acuerdo en 74 de los 75 puntos discutidos**.]

[El comandante máximo de las **FARC**, agregó que esa guerrilla siente una **carga de responsabilidad muy grande con Colombia y ante el mundo**, por el compromiso que con el Gobierno nacional hicieron ante todos los medios de comunicación “ahí confluyen toda una serie de factores que de los que queremos nosotros que el presidente se haga consciente de que meten ruido y no dejan avanzar esto y está en sus manos el que la mesa se dinamice, el que la mesa coja una dinámica que nos permita llegar lo más pronto posible al acuerdo final”.]

[“Queremos que nos crean que si estamos por la paz” afirmo Timoleón Jiménez, pues **uno de los obstáculos para el proceso es el actual estado de sospecha de la contraparte** en aspectos como la liberación de guerrilleros recluidos tras la firma del acuerdo final, “están creyendo que esos presos van a salir a conformar cuadros urbanos para irse a tomar el Palacio de Nariño y eso es absurdo… **sí se cambia la dinámica y se acaba el estado de sospecha, de seguro se van a llegar a acuerdos en temas complejos** como el paramilitarismo”.]
