Title: Es hora de que el Gobierno deje esa actitud “arrogante” frente al asesinato de líderes
Date: 2019-06-10 14:21
Author: CtgAdm
Category: Líderes sociales
Tags: Arzobispo de Cali, Cali, Gobierno Nacional, lideres sociales
Slug: gobierno-deje-actitud-arrogante-frente-asesinato-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-10-at-1.20.39-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

**Monseñor Darío Monsalve, arzobispo de Cali (Valle del Cauca),** aseguró que en medio de los asesinatos de líderes sociales en Colombia se ha notado una actitud y un compromiso con la vida muy bueno por parte de las autoridades locales, regionales e internacionales sin embargo**, por parte del Gobierno Nacional se constata una actitud “arrogante”, al no asumir el problema y las propuestas de solución de una manera eficaz**. (Le puede interesar: ["Académicos del mundo pide a Duque políticas serias de DD.HH."](https://archivo.contagioradio.com/academicos-mundiales-critican-a-duque-por-ola-de-violencia-contra-lideres/))

La afirmación de Monseñor, tuvo lugar en medio de un evento en el que se invitó a la sociedad colombiana a contribuir en la protección de los y las líderes sociales. **“Sus vidas por todos, todos por sus vidas”**, es el slogan que busca generar una reacción positiva a favor del trabajo y la vida de líderes y lideresas en Colombia. (Le puede interesar: ["Reportan asesinato del campesino Alneiro Guarín en Puerto Asís, Putumayo"](https://archivo.contagioradio.com/reportan-asesinato-del-campesino-alneiro-guarin-en-puerto-asis-putumayo/))

En el **evento, realizado este 7 de Junio en la Universidad Católica de Cali**, también participaron, entre otras, la **Asociación Nomadesc** quienes hicieron referencia a que la protección debe ser colectiva e integral. Según lo explicó **Berenice Celeita**, el Estado y el Gobierno tienen que entender que no se trata de implementar medidas individuales de protección, sino que debe darse una protección integral y territorial para resolver los problemas estructurales.

Por otra parte, **Francia Márquez, del Proceso de Comunidades Negras (PCN),** reafirmó que son las comunidades las que deben asumir la tarea de la protección de sus líderes dado que el gobierno no ha cumplido con su labor constitucional de salvaguardar los derechos de las personas y de las comunidades. Por ello lanzó la idea, que ya está en construcción, de alcanzar acuerdos humanitarios regionales con los actores armados presentes en los territorios. (Le puede interesar: ["Comunidades buscan acuerdos humanitarios para acabar con asesinatos de líderes sociales"](https://archivo.contagioradio.com/comunidades-acuerdos-humanitarios-acabar-asesinatos-lideres/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
