Title: Paramilitares violaron a niña indígena de 14 años de edad en Mapiripan
Date: 2015-09-01 14:54
Author: AdminContagio
Category: Comunidad, DDHH, Nacional, Resistencias
Tags: Comisión de Justicia y Paz, comunidades indígenas, Corte Interamericana de Derechos Humanos, extinción de comunidades indígenas, Jiw, mapiripan, Paramilitarismo, Poligrow, Sikuane
Slug: paramilitares-violaron-a-nina-indigena-de-14-anos-de-edad-en-mapiripan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/mapiripan_1_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: minagricultura 

###### [1 Sept 2015] 

El pasado mes de Agosto, indígenas de la comunidad Jiw que habitan la zona rural del municipio de Mapiripan en Meta, se percataron del **abuso sexual y las torturas en contra de una niña indígena de tan solo 14 años**. Según el relato que la menor entregó a los líderes comunitarios, paramilitares la secuestraron y la violaron mientras le preguntaban por los nombres de los líderes de la comunidad.

La denuncia de la Comisión de Justicia y Paz relata que **ese mismo días, hacia las 4 Pm, la comunidad indígena encontró a la niña en grave estado de salud, sin embargo relató los hechos de los que había sido víctima**. La menor de 14 años, que al momento de ser encontrada presentaba signos claros de tortura y abuso sexual, se encuentra recluida en una institución hospitalaria de la región.

En días anteriores indígenas **Sikuane, habitantes del mismo municipio denunciaron que las mismas estructuras paramilitares que controlan en el casco urbano de Mapiripan y la zona rural de ese municipio denunciaron que los “urabeños” les exigieron que entregaran a 4 niñas de la comunidad para abusar sexualmente de ellas.** Los paramilitares amenazaron a los indígenas luego que se negaron a entregar a las menores de edad.

Tanto la comunidad indígena **Jiw como la Sikuane denunciaron a través de un video documental las operaciones paramilitares en la región como las actividades irregulares de la empresa palmera Poligrow**, que ha venido apropiándose de manera irregular de las tierras y restringiendo el acceso a los territorios sagrados de las comunidades indígenas causando graves afectaciones a los ecosistemas.

Desde la masacre de Mapiripan, perpetrada por paramilitares en operación conjunta con las fuerzas militares, hecho por el que la **Corte IDH condenó al Estado colombiano**, los grupos paramilitares controlan la región sin que hasta el momento se haya logrado un desmonte efectivo de esas estructuras que operan a la vista de todos y con la complicidad de las FFMM y de policía.
