Title: Pensamientos - Julian Moreno
Date: 2015-01-09 15:24
Author: CtgAdm
Category: Viaje Literario
Tags: literatura, poesia
Slug: pensamientos-julian-moreno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/descarga-10.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Internet 

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsnndKwV.mp3)  
"Julían Moreno, licenciado en Ciencias Sociales, amante del rock en español, dibujante y poeta bogotano. Hoy nos comparte un retazo de alma plasmado en colores, figuras y palabras, evocadas de silencios y caminatas entre filosofías, nostalgias y risas"
