Title: Dos discusiones internas en las FARC-EP de cara a la clausura de la X Conferencia
Date: 2016-09-23 12:38
Category: Nacional, Paz
Tags: Acuerdos de paz en Colombia, Delegación de paz de FARC, Diálogos de La Habana
Slug: dos-discusiones-internas-en-las-farc-ep-de-cara-a-la-clausura-de-la-x-conferencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/X-Conferencia-FARC-EP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana  ] 

###### [23 Sept 2016 ] 

A punto de finalizar de la X Conferencia de las FARC-EP en los llanos del Yarí, las discusiones entre los delegados se centran en dos temas, por una parte la **conformación del nuevo secretariado y por otra, el equilibrio en las tendencias políticas**. Lo cierto es que todo estará resuelto cuando asomen a tarima Timoleón Jimenez y las nuevas caras de la política de las FARC-EP.

En torno a la discusión sobre el nuevo secretariado, que ha sido el organismo de dirección de las FARC-EP desde su fundación, las tensiones normales en todo partido político comienzan a tener agenda. Hasta ahora el Secretariado ha estado integrado por 31 personas, más los 9 comandantes del Estado Mayor, ahora, **el número podría rondar los 50 integrantes**.

Uno de los puntos álgidos es el debate frente a las caras nuevas que serán parte del organismo de dirección, pues si se quiere mantener el número, **algunos de los cuadros tradicionales tendrían que ceder sus posiciones**, lo que para algunos militantes, podría implicar que se perdiera parte de la experiencia y los aprendizajes históricos que serían de gran aporte en el paso a movimiento político.

Sin embargo, hay quienes dicen que es necesario que el órgano se renueve de cara al nuevo papel que deberá asumir, en este sentido el Secretariado tendrá que fortalecerse con la presencia de mujeres, indígenas y afros, una medida que ubicará a las FARC-EP como **un movimiento campesino con alta representatividad en los sectores sociales**. Así las cosas lo que se espera es una nutrida representación que sumaría 10 personas a los actuales representantes de las FARC-EP.

Como todos los partidos, las FARC-EP también tiene diversas tendencias al interior, sin embargo, **sus alas militar y política saben que deberán llegar al equilibrio en el nuevo movimiento** político y en el propio Secretariado. Aunque el liderazgo de Timochenko es claro para todos los delegados, es notoria y normal la pelea por la representación.

Así las cosas a partir de las 5 de la tarde se inciará el acto de clausura y los gritos de paz invadirán los llanos del Yarí, y quedaremos a la espera de la decisión que tomen quienes [[acuden a las urnas el próximo 2 de Octubre](https://archivo.contagioradio.com/el-respaldo-a-los-acuerdos-en-el-plebiscito-es-el-primer-paso-hacia-la-paz/)] a refrendar los acuerdos de paz.

Vea también: [["En San Vicente del Caguán estamos comprometidos con la paz"](https://archivo.contagioradio.com/en-san-vicente-del-caguan-estamos-comprometidos-con-la-paz/)]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
