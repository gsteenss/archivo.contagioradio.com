Title: CIDH alerta sobre grave situación de las y los defensores de DDHH en Honduras
Date: 2017-07-26 11:21
Category: DDHH, El mundo
Tags: Berta Cáceres, CIDH, honduras
Slug: cidh_defensores_derechos_humanos_honduras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/COMISION-CIDH-e1501085604514.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CIDH 

###### [26 Jul 2017] 

Mediante un comunicado, la Comisión Interamericana de Derechos Humanos (CIDH), anuncia que condena los crímenes contra **Berta Zúñiga Cáceres, coordinadora general del COPINH e hija de Berta Cáceres, Sotero Chavarría y Asunción Martínez,** defensoras y defensores de derechos humanos en Honduras.

Las declaraciones se da luego de que el pasado 30 de junio los y las defensoras fueran objeto de un ataque, con el que buscaron intimidarlos con machetes cerrándoles el paso al vehículo en el que viajaban. [(Le puede interesar: Atentan contra Bertica Zuñiga)](https://archivo.contagioradio.com/atentan_hija_berta_caceres/)

Un ataque que se dio en el marco de **una campaña que llevan a cabo las y los lideres sociales por el derecho a la información y consulta previa sobre megaproyectos,** que han amenazado la permanencia de las comunidades, especialmente la del pueblo Lenca.

"La Comisión Interamericana reitera su alarma por la persistencia de ataques en contra de personas defensoras de derechos humanos en Honduras. En agosto de 2016 el Relator sobre Defensoras y Defensores de Derechos Humanos, José de Jesús Orozco Henríquez, y el Relator Especial de las Naciones Unidas sobre la situación de las y los defensores de derechos humanos, Michel Forst, advirtieron que Honduras se ha convertido en uno de los países más peligrosos para las personas defensoras e instaron al Gobierno de Honduras a adoptar y aplicar de manera inmediata medidas efectivas para protegerlas", señala la CIDH.

Además, llama la atención sobre el seguimiento que ha hecho a ese país en el último año, con lo que se ha podido evidenciar la grave situación de riesgo en la que se encuentran los defensores y defensoras de derechos humanos de Honduras, **a  pesar de que muchos de ellos son beneficiarios de  medidas cautelares de ese mismo órgano,** como es el caso de la hija de Berta Cáceres.

En ese sentido le advierte al Estado hondureño, que es su deber garantizar la vida de estas personas.“**El Estado de Honduras debe adoptar de inmediato medidas de protección para combatir los factores de riesgo que originan la violencia** contra personas defensoras de derechos humanos”, dijo el Relator José de Jesús Orozco.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
