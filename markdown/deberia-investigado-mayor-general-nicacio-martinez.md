Title: Los casos por los que debería ser investigado el Mayor General Nicacio Martínez
Date: 2019-05-28 15:51
Author: CtgAdm
Category: DDHH, Judicial
Tags: Ascenso, Ejecuciones Extrajudiciales, falsos positivos, general, Nicacio Martínez
Slug: deberia-investigado-mayor-general-nicacio-martinez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Mayor-General-Nicacio-Martínez.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @COMANDANTE\_EJC  
] 

La Procuraduría General de la Nación recientemente anunció la apertura de un proceso disciplinario contra el comandante del Ejército, mayor general Nicacio Martínez, quien **tendría la posibilidad de ser ascendido la próxima semana en el Congreso**. El ministerio público inició las indagaciones contra Martínez por la presunta promoción de directrices que podrían desencadenar nuevos casos de ejecuciones extrajudiciales; razón por la que organizaciones sociales piden que sea detenido su ascenso, y se lo aparte de las Fuerzas Militares.

**Sebastián Escobar, integrante del Colectivo de Abogados José Alvear Restrepo**, explicó que la Procuraduría abrió el proceso disciplinario como consecuencia de la publicación del New York Times sobre órdenes que habría dado el Mayor general a sus subalternos, que podrían resumirse en  "duplicar resultados, reducir los controles respecto a los objetivos militares y aliarse con grupos paramilitares para mejorar los indicadores de resultados". (Le puede interesar: ["Duplicar resultados a toda costa, la orden a militares en el gobierno Duque"](https://archivo.contagioradio.com/duplicar-resultados-a-toda-costa-la-orden-a-militares-en-el-gobierno-duque/))

> Procuraduría ([@PGN\_COL](https://twitter.com/PGN_COL?ref_src=twsrc%5Etfw) ) abrió indagación al comandante del Ejército, general Nicacio Martínez Espinel
>
> ?<https://t.co/okROLTHXtj> [pic.twitter.com/Bqk0lB7x5G](https://t.co/Bqk0lB7x5G)
>
> — Procuraduría Colombia (@PGN\_COL) [27 de mayo de 2019](https://twitter.com/PGN_COL/status/1133138770491002881?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
La próxima semana el Congreso de la República deberá aprobar los ascensos en medio de un trámite que inicia en la comisión segunda del Senado y entre los nombres que están para ser ascendidos a General está el de Nicacio Martínez. Antes de la públicación del diario nortemaricano, que involucra al uniformado, organizaciones defensoras de derechos humanos ya habían enviado una carta a esa Comisión, señalando que existían serios cuestionamientos sobre Martínez que deberían frenar su ascenso.

Escobar afirmó que en la carta pedían detener el proceso porque mientras Nicacio Martínez ofició como segundo comandante de la Décima Brigada Blindada **se cometieron "por lo menos, 31 casos de ejecuciones extrajudiciales", con un total de víctimas que supera el medio centenar**. Allí, también existirían documentos en los que el Mayor firmaba pagos a supuestos informantes que habían entregado información que, posteriormente permitió la realización de operaciones, pero que con el tiempo, y gracias a sentencias judiciales, se evidenció que se trataron de 'falsos positivos'.

Sobre este tema, la agencia de prensa Associated Press (AP) publicó unas actas de pago firmadas por Martínez, en las que se vería una omisión, pues en la evaluación que se hace de las operaciones se podría constatar que en realidad se trató de ejecuciones extrajudiciales. "Hay elementos para que se dieran cuenta que así era", puntualizó el Escobar. (Le puede interesar: ["General acusado de falsos positivos no debería ocupar cargo públicos"](https://archivo.contagioradio.com/general-cargos-publicos/))

Pese a que estos casos actualmente no tienen un proceso judicial abierto, el Abogado dijo que **era necesario un estudio sobre la idoneidad ética y moral del Mayor por parte del Congreso,** puesto que en palabras más simples: "si un gerente de una empresa presenta indicadores inflados, ¿debería ser promovido?; la respuesta es que no". (Le puede interesar: ["Nueve comandantes del Ejército estarían implicados en falsos positivos : Human Rights Watch"](https://archivo.contagioradio.com/nueve-comandantes-del-ejercito-estarian-implicados-en-falsos-positivos-human-rights-watch/))

#### **Mientras la JEP investiga, la Fiscalía demora casos con uniformados involucrados  
**

Escobar sostuvo que el Colectivo  ha presentado distintos informes a la Jurisdicción Especial para la Paz (JEP) sobre algunos integrantes de la Fuerza Pública que podrían ser ascendidos como Nicacio Martínez. Uno de los mencionados es **Adolfo León Hernández, quien fue comandante del batallón La Popa, y podría estar relacionado con  26 ejecuciones extrajudiciales**.

Con base en esos informes, pidieron a la JEP medidas cautelares para remover del cargo a los mencionados, "evitando que su posición de mando afecte la protección de evidencia que los involucra en casos, como la propia seguridad de víctimas y testigos". (Le puede interesar: ["Presentan a la JEP informe que vincula a comandante de la cúpula militar con falsos positivos"](https://archivo.contagioradio.com/jep-adolfo-leon-hernandez-martinez/))

Otra fue la situación judicial respecto a la Fiscalía, pues como lo anotó Escobar, "Néstor Humberto Martínez le hizo mucho daño a la investigación de altos mandos vinculados con estos casos"; pues detuvo procesos como el que se llevaba contra el general (r) Mario Montoya, que desde 2016 tenía lista una imputación de cargos, pero no se llevó a cabo en estos años. Sin embargo, con la salida de Néstor Martínez del ente acusador, el activista espera que se comiencen a investigar a fondo los hechos, y se establezcan responsabildiades en la cadena de mando y no solo para los autores materiales como ha ocurrido hasta ahora.

### **Circula una petición para apartar a Nicacio Martínez de la comandancia del Ejército**

Las organizaciones que enviaron la carta a la Comisión del Senado formularon una [petición en la plataforma Change.org](http://chng.it/dL94CKJm), con la que están recogiendo firmas para pedir al presidente Iván Duque que releve a Nicacio Martínez de la comandancia del Ejército. Aunque no es posible medir la influencia que está petición tenga en el Presidente, Escobar dijo que se trataba de generar un rechazo en la ciudadanía de "unas prácticas que le hicieron mucho daño a Colombia, a las personas y la institucionalidad".

<iframe id="audio_36426355" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36426355_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
