Title: Objetivo de oposición venezolana no es resolver la crisis sino “llegar a Miraflores”
Date: 2016-01-06 11:07
Category: El mundo, Política
Tags: Hector Rodríguez, MUD, Nicolas Maduro, PSUV, Venezuela
Slug: objetivo-de-oposicion-venezolana-no-es-resolver-la-crisis-a-sino-llegar-a-miraflores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/trabajadores_venezuela_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: trabajadores.ve 

###### [06 Ene 2016]

En las intervenciones del PSUV en la instalación de la nueva Asamblea Nacional en Venezuela, se reafirmó que muchos de los congresistas posesionados por la Mesa de Unidad Democrática, MUD, no representarían ningún cambio puesto que sus más significativos representantes como el autonombrado presidente de la AN, **Henry Ramos Allup, ha ocupado un puesto similar desde inicios de la década de los 80’s.**

\[embed\]https://www.youtube.com/watch?v=qSzjAvzKTeU\[/embed\]

Hector Rodríguez, diputado del PSUV afirmó que la bancada de esa coalición negaba la propuesta de que se posesione Henry Ramos Allup como nuevo presidente, y también recordó que los partidos representados en la MUD han sido los mismos que gobernaron antes de la llegada a la presidencia de Hugo Chávez. **Según Rodríguez, la MUD engañó al pueblo con el “supuesto cambio” y lo que representan es un retroceso y una “traición”**.

Rodríguez recordó que **Ramos Allub de 72 años, fue diputado durante 5 periodos consecutivos en los cuales se aprobaron y se refrendaron políticas como la del “Punto Fijo”** una especie de Frente Partidista para mantener el poder en las mismas élites después de la dictadura de Perez Jiménez. Allup también aprobó el paquete de medidas [neoliberales que condujeron a la crisis de los 80 y 90 que provocó protestas sociales como el Caracazo en 1989 o las políticas del Estado Mínimo Necesario, entre otras.](https://archivo.contagioradio.com/elecciones-en-venezuela-no-son-una-guerra-perdida/)

Por su parte Ramos Allup afirmó que uno de los primeros objetivos de la bancada mayoritaria de la MUD en la Asamblea Nacional será lograr **que Nicolás Maduro esté por fuera de la presidencia en 6 meses**, ante lo cual la bancada del [PSUV sentó su voz de protesta](https://archivo.contagioradio.com/en-riesgo-logros-de-misiones-sociales-de-venezuela/) y afirmó que la MUD se quitaba la máscara porque su objetivo es llegar al Palacio de Miraflores y no aportar en la solución de la crisis económica en Venezuela.

Además la bancada de la MUD propuso una ley de Amnistía criticada por muchos porque pretende imponerse la ley a la justicia, ya aplicada en algunos casos, y además implica el reconocimiento del crimen, pero lo califica como un crimen político, cuando los cargos que pesan sobre algunos de los condenados como Leopoldo López son por asesinatos, instigación al crimen y otros delitos comunes.
