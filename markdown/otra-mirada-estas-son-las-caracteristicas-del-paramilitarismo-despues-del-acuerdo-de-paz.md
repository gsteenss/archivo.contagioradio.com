Title: Otra Mirada: Estas son las características del paramilitarismo después del acuerdo de paz
Date: 2020-08-26 15:50
Author: AdminContagio
Category: Otra Mirada, Programas
Tags: estructuras paramilitares, Paramilitarismo, Violencia en los territorios
Slug: otra-mirada-estas-son-las-caracteristicas-del-paramilitarismo-despues-del-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/paramilitarismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Na-24.net

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con el resurgir de la violencia este 2020 en los territorios, ha regresado el accionar de los grupos paramilitares; grupos que en distintas partes del país, están actuando y  atacando a las comunidades. Sin embargo, a pesar de la difícil situación humanitaria, no se ven acciones por parte del Gobierno Nacional ni de los gobiernos locales. (Le puede interesar: [Cuatro masacres en 48 horas, 3 jóvenes son asesinados en Venecia, Antioquia](https://archivo.contagioradio.com/cuatro-masacres-en-48-horas-3-jovenes-son-asesinados-en-venecia-antioquia/)) 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa de Otra Mirada Camilo González, presidente de Indepaz Astrid Torres, investigadora Corporación Jurídica Libertad y María Paula Feliciano, coordinadora del programa de Garantías para la Paz del CSPP, analizaron este nuevo accionar paramilitar en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los invitados explicaron que el paramilitarismo actual no es igual al de hace unos años, y es que ahora se manejan pequeños grupos locales que parecen separados del fenómeno, pero responden a la misma estructura paramilitar, facilitando su accionar y haciendo más difícil para el gobierno luchar contra esto. (Le puede interesar: [Las razones que explican el regreso de las masacres a Colombia](https://archivo.contagioradio.com/las-razones-que-explican-el-regreso-de-las-masacres-a-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, comentan cómo se ha desarrollado esa relación entre los grupos paramilitares y la fuerza pública, con la particularidad de que los territorios más golpeados por estos actores armados, son los mismos con mayor presencia del Ejército.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, teniendo en cuenta que al parecer el gobierno no quiere reactivar la Comisión Nacional de Garantías de Seguridad, comparten que es realmente necesario poner en marcha tanto la Comisión como la Mesa Nacional de Garantías e implementar los puntos del Acuerdos de Paz sobre el desmonte de los grupos sucesores de paramilitarismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el programa del 20 de agosto: [Otra Mirada: Masacres, ¿de vuelta al terror?  ](https://www.facebook.com/contagioradio/videos/324202188725648)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el análisis completo [Haga click aquí](https://www.facebook.com/contagioradio/videos/3838335699517144)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
