Title: Cerca de 1000 organizaciones civiles e internacionales envían carta por el Proceso de paz
Date: 2015-11-19 07:55
Category: Entrevistas, Paz
Tags: carta de las organizaciones, ELN, FARC, Justicia y Paz, proceso de paz
Slug: cerca-de-1000-organizaciones-civiles-e-internacionales-envian-carta-por-el-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/rueda-de-prensa-paz-organizaciones.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

Alrededor de 1000** organizaciones de la sociedad civil colombiana y la comunidad internacional**, realizaron un comunicado a las delegaciones del Gobierno y FARC en La Habana, además de la comandancia del ELN, reiterando su apoyo al proceso de paz, pero aclarando que existen preocupaciones puntuales.

Entre ellas, se encuentran las acciones militares terrestres contra la guerrilla de las FARC que de acuerdo a las organizaciones, pueden poner en riesgo el acuerdo de cese al fuego unilateral impidiendo además el avance hacia un cese bilateral, por cuenta de la presencia de grupos paramilitares en zonas en donde existe actividad operacional de la Fuerza Pública en numerosas regiones del país.

Asimismo, se expone que deben acelerarse los procesos técnicos y políticos que faciliten el avance del proceso y para ello, proponen que se continúe desescalando el conflicto en Colombia para **“avanzar rápidamente en la finalización y difusión del acuerdo sobre el Sistema Integral de Verdad, Justicia y Reparación”.**

También, se propone que antes de que finalice el año 2015, se inicie formalmente los diálogos de paz entre el Gobierno Nacional con la guerrilla del ELN.

En el documento, se expresa que las organizaciones están dispuestas a contribuir en lo que se requiera para que las conversaciones y acuerdos lleguen a buen término.

\[embed\]https://www.youtube.com/watch?v=pW1GR6a\_6gE \[/embed\]

Consulte aquí la carta completa:

[Carta SOS Proceso de Paz INTERNACIONALES Final Noviembre 18](https://es.scribd.com/doc/290390534/Carta-SOS-Proceso-de-Paz-INTERNACIONALES-Final-Noviembre-18 "View Carta SOS Proceso de Paz INTERNACIONALES Final Noviembre 18 on Scribd")

<iframe id="doc_22018" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/290390534/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

[Carta SOS Proceso de Paz NAC. 18 Nov. (1)](https://es.scribd.com/doc/290390614/Carta-SOS-Proceso-de-Paz-NAC-18-Nov-1 "View Carta SOS Proceso de Paz NAC. 18 Nov. (1) on Scribd")

<iframe id="doc_5801" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/290390614/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>  
   
   
   
 
