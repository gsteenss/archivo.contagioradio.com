Title: Petardos que explotaron en Segovia irían dirigidos a integrantes de mesa minera
Date: 2017-08-25 13:12
Category: DDHH, Nacional
Tags: ESMAD, paro minero, Remedios, Segovia
Slug: petardos-que-explotaron-en-segovia-irian-dirigidos-a-integrantes-de-mesa-minera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Planton-por-Mineros-Segovia-14.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Instituto Popular de Capacitación] 

###### [25 Ago. 2015] 

Luego de los dos artefactos que explotaron esta madrugada en el municipio de Segovia, la Mesa Minera de Segovia y Remedios aseguraron a través de un comunicado que **estos hechos dan cuenta de la forma sistemática como su paro cívico ha estado infiltrado** por actores ajenos a su manifestación y hace un llamado a las instituciones a garantizar la vida, toda vez que estos hechos son una amenaza directa contra su ejercicio de protesta.

“Cuando nos encontrábamos reunidos con diferentes líderes campesinos y comunidades agro-mineras de los municipios de Remedios y Segovia en el polideportivo “Abel Rivera Villa” **fuimos objeto de un atentado con un elemento explosivo”** denuncian en el comunicado. Le puede interesar: [Policía usa francotiradores para reprimir mineros en Remedios y Segovia](https://archivo.contagioradio.com/mineros-de-remedios-y-segovia-denuncian-presencia-de-francotiradores-en-el-paro/)

Por este hecho no hay víctimas o heridos de las personas que se encontraban en la reunión. Sin embargo, del segundo atentado perpetrado en la calle Bolívar del municipio de Segovia contra un establecimiento comercial, si hay numerosas pérdidas materiales.

“Cabe la pena anotar que **casi sobre esa misma hora del artefacto explosivo se trasladaba por dicha zona uno de nuestros asesores de la mesa minera”** recalcan. Le puede interesar: [Informe ratifica grave situación de DDHH en Segovia y Remedios, Antioquia](https://archivo.contagioradio.com/situacion-de-ddhh-en-segovia-y-remedios/)

Debido a estos hechos la Mesa Minera ha exigido a las autoridades adelanten todas las acciones pertinentes para establecer los autores materiales e intelectuales, además **instan a que se brinden las garantías a los miembros de esta organización,** así como a toda la población de Remedios y Segovia, quienes han denunciado que en la actualidad sufre desabastecimiento de alimentos. Le puede interesar: [En 32 días de paro hospital de segovia ha atendido 50 heridos](https://archivo.contagioradio.com/en-32-dias-de-paro-hospital-de-segovia-ha-atendido-50-heridos/)

<div class="gmail-content gmail-clearfix">

<div class="gmail-field gmail-field-name-body gmail-field-type-text-with-summary gmail-field-label-hidden">

<div class="gmail-field-items">

<div class="gmail-field-item even">

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

</div>

</div>

</div>

</div>
