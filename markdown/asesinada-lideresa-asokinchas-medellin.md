Title: Asesinada lideresa de Asokinchas en Medellín
Date: 2017-03-02 13:11
Category: DDHH, Nacional
Tags: Asesinatos a líderes comunitarios, ASOKINCHAS, Cumbre Agraria
Slug: asesinada-lideresa-asokinchas-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/alicia-lopez1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [2 Mar 2017 ] 

**La lideresa** **Alicia López Guisao**, quien acompañaba la ejecución del proyecto Cumbre Agraria y Gobierno Nacional con la Asociación Agroecológica Interétnica e Intercultural ASOKINCHASA, en el departamento del Chocó, **fue asesinada este jueves en el barrio Olaya Herrera de la ciudad de Medellín, cuando se transportaba en  un taxi. **Alicia habría recibido varios impactos de bala por parte de sujetos desconocidos que movilizaban en moto.

“**Hoy es un día muy triste** como viene sucediendo últimamente en el país, **acompañado de una mirada indolente del Gobierno nacional** que sigue insistiendo que son hechos aislados” dijo Adriana Arboleda de la Corporación Jurídica Libertad, quien conoció a Alicia. Le puede interesar: [Asesinatos de líderes sociales son práctica sistemática: Somos Defensores](https://archivo.contagioradio.com/asesinatos-de-lideres-son-practica-sistematica-33507/)

**Alicia, era un joven de 32 años de edad,** quien fue desplazada de la comuna 13 en el marco de la Operación Orión y es hija de Rosa Guisao, reconocida lideresa del barrio Olaya Herrera. Le puede interesar: [279 agresiones y 35 asesinatos contra defensores de DDHH en Colombia durante 2016](https://archivo.contagioradio.com/aumentan-los-riesgos-para-defensores-de-ddhh-en-colombia/)

**“Alicia estaba comprometida desde chiquita con el trabajo comunitario, estaba vinculada a procesos sociales** y todo porque hace parte de una familia que históricamente le ha apostado a la construcción de un país distinto (…)  tenían un trabajo comunitario de salud, de vivienda, de educación que fue señalado y estigmatizado por paramilitares y por la Fuerza Pública” cuenta Arboleda.

En el último tiempo **Alicia venía vinculada en el departamento del Chocó a un proceso de soberanía alimentaria con comunidades indígenas y afro descendientes en la asociación  Asokinchas**, articulada al Coordinador Nacional Agrario y con todo el proceso de Congreso de los Pueblos. Trabajo por el que ya había recibido amenazas.

También, denunciaron que ella y su familia **"en reiteradas ocasiones han sufrido amenazas, persecución y desplazamiento por parte de estructuras paramilitares"**, explican que inicialmente habían sido desplazados de la región de Urabá, por lo que llegaron a la Comuna Trece de Medellín, de donde fueron desplazados nuevamente en el marco de la operación Orión.

“Ella ya no vivía en Medellín, fue a visitar a un familiar y las estructuras paramilitares que siguen operando desde hace 3 décadas pues la vieron y la asesinaron. Al parecer fueron ellos los que la asesinaron" dice Arboleda.

Además, revelaron que la hermana de Alicia recibió el mismo día de los hechos en horas de la tarde una llamada de una habitante del barrio Olaya, quien le comunicó que **"están esperando a que las hermanas y la mamá aparezcan en el entierro para también atentar** contra la vida de ellas, que lo mejor es que no se aparezcan en el barrio".

**“En este país tener esperanza y pensar que se puede construir algo distinto es sinónimos de criminalidad.** El único delito que cometió Alicia fue creer que podía hacer un trabajo comunitario y que podía construir algo distinto. Lo que tenemos es una joven que todos los días se levantaba creyendo que a partir de los proyectos comunitarios podía fortalecer la organización de las comunidades” dijo Arboleda.

Ante las recientes declaraciones de Claudia Jaramillo, directora seccional de Fiscalías de Medellín, en las que aseguró que el asesinato se había dado porque Alicia había sido miliciana, la abogada Arboleda aseveró "eso fue un acto de total irresponsabilidad e indolencia con el dolor de la familia y en vez de comprometerse, sale a hacer señalamientos (...) hay que decirle a la doctora Jaramillo que la memoria de este país no pasa por los computadores de la Fiscalía, se le acusó de miliciana pero todo fue un montaje"

Según la integrante de al Corporación Jurídica Libertad, le exigirán a la directora seccional de Fiscalías de Medellín que rectifique la información que fue entregada a la opinión pública y que evidencie que eso hace parte de la persecución. Le puede interesar: [Colombia reportó 85 asesinatos contra Defensores de DDHH en el 2016](https://archivo.contagioradio.com/colombia-reporto-85-asesinatos-contra-defensores-de-ddhh-en-el-2016/)

Las organizaciones, denunciaron los hechos y hacen un llamado al Gobierno Nacional y a la Fiscalía, para que investiguen el caso, den con los responsables y se construyan medidas efectivas para garantizar la seguridad de los y las lideresas comunitarias, resaltaron que en lo corrido del año **ya son 25 los líderes y lideresas asesinados, presuntamente por estructuras paramilitares y las autoridades no han brindado atención oportuna** a la problemática.

"No basta lo que el Gobierno está haciendo de reuniones, de decir aquí hay una Unidad Nacional de Protección, tiene que haber un compromiso real con el desmantelamiento del paramilitarismo, sino va a a ser imposible cualquier nivel de protección para los defensores" concluyó Arboleda.

Con Alicia, el departamento de Antioquia completa 7 líderes y lideresas asesinados. Le puede interesar: [CIDH registra 14 asesinatos de defensores de DDHH en Latinoamérica en 2017](https://archivo.contagioradio.com/cidh-registra-14-asesinatos-defensores-ddhh-latinoamerica/)

<iframe id="audio_17343940" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17343940_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
