Title: Asesinan a Robert Hurtado, firmante de la paz en Chocó
Date: 2020-05-22 09:46
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Chocó, excombatiente, FARC, Robert Hurtado
Slug: asesinan-a-robert-hurtado-firmante-de-la-paz-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Excombatiente-Robert-Hurtado.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @PartidoFARC {#foto-partidofarc .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este jueves el partido Fuerza Alternativa Revolucionaria del Común (FARC) denunció el asesinato de Robert Hurtado, firmante de la paz que vivía en la comunidad Munguido del litoral del San Juan, Chocó. Según el partido, los hechos ocurrieron en la noche del pasado miércoles 20 de mayo, en su comunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo a la información brindada por el partido, Robert Hurtado tenía 27 años de edad, era aserrador y vivía con su esposa y 3 hijos menores de edad. Su crimen se suma al de 196 personas que dejaron las armas y le apostaron a la paz, por lo que FARC nuevamente reclamó "que la paz no nos cueste la vida".

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1263579628565749760","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1263579628565749760

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Este año han sido asesinados 26 firmantes de la paz

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el Instituto de Estudios para el Desarrollo y la Paz ([INDEPAZ](http://www.indepaz.org.co/paz-al-liderazgo-social/)) este año inició con el asesinato del excombatiente Benjamí Banguera González el 1 de enero en Guapí (Cauca), y hasta el 7 de mayo, con el asesinato de Wilder Daniel Marín en Bello (Antioquia), contaban 25 firmantes de la paz que han perdido la vida en medio de hechos violentos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo invitamos a consultar: [Un cacerolazo para amplificar la voz de casi 200 excombatientes asesinados](https://archivo.contagioradio.com/cacerolazo-mplificar-voz-casi-200-excombatientes-asesinados/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
