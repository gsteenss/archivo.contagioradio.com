Title: Lo bueno y lo malo del remezón en la cúpula militar
Date: 2015-07-07 16:02
Category: Nacional, Política
Tags: alirio uribe, Carlos Bueno, Centro Democrático, Cese al fuego bilateral, código de policía, Cuba, cúpula militar, FARC, Jaime Lasprilla, Juan Pablo rodríguez, Leonardo Santamaría, Leonardo Santamaría es la cabeza de la Armada Carlos Bueno, Noruega, países garantes del proceso de paz, Polo Democrático Alternativo, Presidente Juan Manuel Santos, proceso de paz, Rodolfo Palomino, uribismo
Slug: lo-bueno-y-lo-malo-del-cambio-de-la-cupula-militar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/foto12_g_5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Señal Radio Colombia 

<iframe src="http://www.ivoox.com/player_ek_4733113_2_1.html?data=lZyglZaVd46ZmKiakpuJd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc3d087cjbrWrcPZhpewjdfJtNPZ1Mrb1sqPpYzgwpCwh6iXaaKlzsbfw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alirio Uribe, represente a la Cámara Polo Democrático Alternativo] 

El presidente Juan Manuel Santos, anunció este lunes varios cambios respecto a su cúpula militar. Para Alirio Uribe representante a la Cámara y defensor de derechos humanos, **el cambio de Jaime Lasprilla,**  por Alberto José Mejía, ahora comandante del Ejército Nacional, **es positivo para el proceso de paz.** Sin embargo, **mantener a Juan Pablo Rodríguez como comandante de las Fuerzas Militares, es negativo para las víctimas** en la medida en que él está vinculado a casos de ejecuciones extrajudiciales.

Ahora, el almirante Leonardo Santamaría es la cabeza de la Armada y Carlos Bueno, pasó a ser el comandante de la Fuerza Aérea, lo que para el representante del Polo Democrático Alternativo, podría ser un **buen síntoma en relación a que los bombardeos contra la guerrilla podrían disminuirse,**  lo que eventualmente podría generar un nuevo cese de hostilidades por parte de las FARC – EP.

Por otra parte, con la ratificación del general Rodolfo Palomino, Alirio Uribe, esperaría que el general de la Policía, se **retracte sobre la reforma al Código de Policía**, que violaría los derechos de los ciudadanos, como ya lo han explicado algunos analistas.

**“En general son cambios positivos, ojalá esta cúpula militar contribuya a la terminación de la guerra y no a su prolongación**”, expresa Uribe Muñoz, quien añade que el Uribismo y el Centro Democrático han intentado colocar a los militares en contra del Proceso de Paz y de las posiciones del presidente Santos.

 
