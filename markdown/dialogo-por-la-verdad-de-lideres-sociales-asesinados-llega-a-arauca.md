Title: Diálogo por la verdad de líderes sociales asesinados llega a Arauca
Date: 2019-09-11 18:40
Author: CtgAdm
Category: Nacional, Paz
Tags: comision de la verdad, Diálogos para la No Repetición
Slug: dialogo-por-la-verdad-de-lideres-sociales-asesinados-llega-a-arauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Líderes.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Comisión.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de la Verdad] 

Este 12 de septiembre en Arauca tendrá lugar el Segundo Diálogo para la No Repetición, un espacio de participación y discusión social impulsado por la Comisión de la Verdad para reflexionar sobre las causas del conflicto armado, enfocado esta vez en el asesinato de líderes sociales y **profundizar en quiénes son los autores de estos homicidios y quiénes se ven beneficiados.**

Sofía Cabarcas, integrante del equipo de No Repetición de la Comisión de la Verdad señala que el primer diálogo del 11 de junio de Bogotá además de  visibilizar la problemática, buscó responder por qué se continúa asesinando a los líderes y cuál es el rol de la institucionalidad, ahora se centrará en resolver quién o quiénes están detrás de estos crímenes, principalmente en la zona nororiental del país y quiénes se benefician con su asesinato. [(Le puede interesar: Tres conclusiones del Diálogo para la No Repetición sobre asesinato de líderes sociales)](https://archivo.contagioradio.com/tres-conclusiones-del-1er-dialogo-para-la-no-repeticion-sobre-el-asesinato-de-lideres-sociales/)

### La verdad llega a Arauca 

Arauca, departamento que reunirá a diferentes actores sociales, autoridades departamentales, periodistas, academia, empresarios y Fuerza Pública para dialogar de esta problemática, fue seleccionada La importancia estratégica del departamento, por su riqueza en recursos y personas, pero a su vez, por la intensidad del conflicto [(Lea también: Con Diálogos para la No Repetición, Comisión de la Verdad promoverá defensa de líderes sociales)](https://archivo.contagioradio.com/con-dialogos-para-la-no-repeticion-comision-de-la-verdad-promovera-defensa-de-lideres-sociales/)

Acerca de esta segunda edición del Diálogo para la No Repetición, el comisionado Saul Franco expresa que **"si seguimos asesinando a los líderes sociales, estamos decapitando a la sociedad, perdiendo la capacidad de organización y de transmisión de fuerza",**  agregando que, solo al frenar esta ola de violencia, la sociedad será capaz de comprender las razones por las que continúa el conflicto.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
