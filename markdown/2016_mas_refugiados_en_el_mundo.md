Title: En 2016 se registraron 10 millones más de personas refugiadas en el mundo
Date: 2017-06-20 10:50
Category: DDHH, Nacional
Tags: ACNUR, Desplazamiento forzado, ONU, Refugiados
Slug: 2016_mas_refugiados_en_el_mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/refugiados_3-e1497973741723.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: EFE 

###### [20 Jun 2017] 

El más reciente informe de la Agencia de la ONU para los Refugiados, ACNUR revela un panorama a nivel mundial preocupante. Según cifras de ese organismo, la cantidad de personas en situación de desplazamiento forzado continúa aumentando. **Son 65,6 millones las mujeres, hombres, niños, niñas, adultos mayores** en el mundo que padecen esta situación.

De ese total de refugiados, 17,2 millones se registran bajo el mandato de la ANCUR y 5,3 millones de palestinos son contabilizados por el Comité UNRWA España. Además hay 40,3 millones de desplazados internos en el mundo, y 2,8 millones son solicitantes de asilo.

Las cifras son alarmantes, pues hace 20 años se registraba casi la mitad de ese número, es decir, 33,9 millones. La cantidad de personas refugiadas recopiladas por la ACNUR, evidencian que el aumento se registró entre 2012 y 2015, de acuerdo con las cifras dadas por los gobiernos, las organizaciones no gubernamentales y el organismo internacional.

La población **siria continúa siendo la mayor víctima de este flagelo con 12 millones de personas, y Colombia ocupa el segundo lugar con 7,7 millones de desplazados.** Enseguida siguen Afganistán con 4,7 millones; Irak (4,2 millones), Sudán del Sur (3,3 millones), Sudán (2,9 millones), la República Democrática del Congo (2,9 millones), Somalia (2,6 millones), Nigeria (2,5 millones), Ucrania (2,1 millones) y Yemen (2,1 millones).

Durante **en 2016 hubo 10,3 millones de nuevos desplazados, por conflictos, violaciones a los derechos humanos o persecució**n. Lo que significa que por minuto fueron 20 las personas que debieron salir forzadamente de sus países. De ese número, 6,9 millones son desplazados dentro de sus propias naciones, y 3,4 millones son nuevos solicitantes de asilo.

### Incapacidad internacional 

Si bien António Guterres, secretario general de la ONU, ha llamado la atención frente a que “La protección de los refugiados no es solo responsabilidad de los Estados vecinos de una crisis: es una responsabilidad colectiva de la comunidad internacional”, las cifras que demuestran el nivel de respuesta a esta situación no evidencian que haya tal solidaridad internacional.

De acuerdo con la Comisión Española de Ayuda al Refuagiado, CEAR, la Unión Europea sigue incumpliendo sus compromisos de reubicación y reasentamiento. **De las 182.000 personas que debía trasladar desde Grecia, Italia y terceros países, solo ha acogido a algo más del 20%**. Un compromiso que debería estar cumplido el próximo 26 de septiembre.

Pese a los pactos internacionales, más de 72.000 personas permanecen atrapadas en la ruta de los Balcanes, principalmente en Grecia, debido al cierre de fronteras y la lentitud en el proceso de reubicación. **Además cerca de 1.700 personas han perdido la vida tratando de llegar a Europa.**

Estas cifras concluyen que hasta el momento **la UE ha sido incapaz de cumplir sus compromisos de mínimos para acoger a los refugiados,** como asegura el CEAR. “El número de personas que se han visto obligadas a huir de sus hogares se eleva a 65,6 millones y el número de muertes en el Mediterráneo no deja de aumentar, pero **la respuesta de las autoridades españolas y europeas no corresponde con este drama humano**”, expresa Estrella Galán, secretaria general de la entidad.

![Día Mundial del Refugiado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Día-Mundial-del-Refugiado.png){.alignnone .size-full .wp-image-42428 width="1500" height="800"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
