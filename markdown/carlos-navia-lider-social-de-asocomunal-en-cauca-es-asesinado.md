Title: Carlos Navia líder social de Asocomunal en Cauca, es asesinado
Date: 2020-10-26 17:54
Author: CtgAdm
Category: Actualidad, Líderes sociales
Slug: carlos-navia-lider-social-de-asocomunal-en-cauca-es-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Captura-de-Pantalla-2020-10-26-a-las-5.28.25-p.-m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Cortesia Indepaz*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 26 de octubre fue reportado el **asesinato de Carlos Navia, reconocido líder social en el departamento del Cauca**, quién conformaba varios procesos sociales en defensa de la vida y el territorio, uno de ellos era el Congreso de los Pueblos en Argelia.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1320795820195762176","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1320795820195762176

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Carlos Navia, era un reconocido líder social, y **Fundador de la Asociación de Juntas de Acción Comunal de Argelia , e integrante del Coordinador Nacional Agrario** y el [Congreso de los Pueblos](https://www.congresodelospueblos.org/), desde donde impulsaba el comité procarreteras de este mismo municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El homicidio fue registrado por organizaciones de Derechos Humanos en la vereda, La Hacienda, ubicada en el corregimiento de El Plateado entre los municipios de Argelia y El Tambo** en el departamento del Cauca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Zona en la que según el Instituto de Estudios para el Desarrollo y la Paz, (Indepaz), **se registra la presencia de grupos armados como el ELN, el frente Carlos Patiño y el grupo delincuencial denominado Los Pocillos**, relacionados con la segunda Marquetalia y grupos narcotraficantes de la región.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ademas Indepaz indica que el asesinato de **Carlos Navia se suma a los 243 líderes, lideresas y defensores de Derechos Humanos asesinados durante el 2020,** de estos 83 corresponden al departamento del Cauca. ([«Violencia contra líderes sociales se concentró en 29 de los 32 departamentos»](https://archivo.contagioradio.com/violencia-contra-lideres-sociales-se-concentro-en-29-de-los-32-departamentos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo, organizaciones a las que pertenecía Navia señalan que durante este 29 de octubre hasta el domingo primero de noviembre, **se llevará a cabo la Caravana Humanitaria al Cañón del Micay, una acción de movilización que pretende rechazar y pedir un cese a la violencia que enfrenta este territorio.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las organizaciones sociales señalan que esta misión humanitaria pretendía convocar a funcionarios públicos, invitados con varios días de antelación, así como a integrantes de la comunidad internacional; **pero pese a las reiteradas acciones de atención que solicita este territorio, a la fecha ningún integrante de las organizaciones gubernamentales ha confirmado asistencia**, bajo el argumento de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
