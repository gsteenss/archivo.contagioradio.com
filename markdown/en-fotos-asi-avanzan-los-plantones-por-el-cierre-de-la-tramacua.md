Title: En Fotos: Así avanzan los plantones por el cierre de la Tramacua
Date: 2016-02-23 12:09
Category: Galerias, Nacional
Tags: cárcel la tramacua, crisis carcelaria, presos politicos
Slug: en-fotos-asi-avanzan-los-plantones-por-el-cierre-de-la-tramacua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @TramacuaNuncaMa 

###### [23 Feb 2016] 

Desde las 10 de la mañana en ciudades como Bogotá, Bucaramanga, Valledupar, Medellín, Cúcuta, Popayán, Ibagué y en países como Canadá y Estados Unidos se realizarán acciones en solidaridad con los presos de la cárcel La Tramacua.

A inicios del mes de febrero, cuando se esperaba que la cárcel “Tramacua” fuera cerrada por las evidentes fallas que presenta, se demuestra que se trata de un centro de atentados contra los derechos Humanos de las personas recluidas, el Tribunal Administrativo del Cesar decidió creer en la intensión manifiesta del INPEC de solucionar los problemas que se presentan, sin embargo, nada se ha hecho hasta el momento y la situación no tiende a cambiar, por el contrario a empeorar.

\
