Title: Ola de homicidios no se detiene en Argelia, Cauca
Date: 2019-02-17 12:44
Author: AdminContagio
Category: Comunidad, DDHH
Tags: Argelia Cauca, reincorporación farc, Sustitución de cultivos
Slug: ola-homicidios-argelia-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/muerte-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 17 Feb 2019 

Termina la semana con una saldo de **ocho personas asesinadas de manera violenta en el municipio de Argelia, sur del Cauca,** el más reciente caso registrado fue el de **Uriol de Jesús López**, encontrado sin vida en el sector conocido como La Emboscada el pasado viernes.

Tan solo algunas horas antes había sido reportado el hallazgo del cuerpo de **Jefferson Daza**, conocido como "el flaco" en el corregimiento de El Mango; allí mismo en la vereda La Playa fue encontrado el lunes 11 el cadáver de un hombre identificado como **Edinson Andrés Torres Gutiérrez** de 27 años de edad.

El 8 de febrero fue reportado por el partido Fuerza Alternativa Revolucionaria del Común el asesinato de **Jhon Cleiner López Castillo**, quien pertenecía a las Nuevas Áreas de Reincorporación de Santa Clara, en Argelia. Ese mismo día en el corregimiento de El Plateado, identificaron a **Alex Andrés Londoño Burbano** y **Fernando Iles**, este último al parecer también hizo parte de las FARC y se encontraba en proceso de desmovilización.

Otro de los casos registrados fue el de **Jhon Jairo Hoyos Córdoba** y **Jhon Cleiner López Castillo**, quienes se desplazaban en una motocicleta en la vereda Botafogo, desde el Mango hacia Argelia y fueron baleados por no atender una señal de pare. Hoyos era excombatiente de las FARC en proceso de reincorporación (Le puede interesar: [Corinto bajo el fuego cruzado de EPL y disidentes de las FARC](https://archivo.contagioradio.com/conrito-cauca-bajo-el-fuego-cruzado-por-enfrentamientos-entre-disi/))

Según habitantes consultados por el Diario del Cauca en la mayoría de los casos se trata de “jóvenes que han venido de localidades como **El Bordo, Balboa** y a los pocos días aparecen muertos. Otros son personas que se dedican al comercio, pero sin saber los motivos, llegan a sus fincas o casas y los matan”

La respuesta de las autoridades municipales es que se trata de **homicidios relacionados con la alta presencia de cultivos de uso ilícito en la región**, y la lucha por el control de los mismos por parte de grupos armados, así como **por los incumplimientos del gobierno en los programas de sustitución** y la **falta de planes de inversión social**.

El gobierno nacional anunció la llegada de un cuerpo élite de policía para afrontar la situación para prevenir y acompañar las amenazas sobre líderes sociales y excombatientes, mientras que desde el partido FARC pidieron **garantías para la vida, la integridad y el buen vivir de quienes se encuentran en proceso de reincorporación** así como para líderes y defensores de derechos humanos.

###### Reciba toda la información de Contagio Radio en [[su correo]
