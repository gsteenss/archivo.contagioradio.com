Title: FARC denuncia hostigamientos y amenazas contra sus militantes en Bogotá
Date: 2018-05-24 10:33
Category: Paz, Política
Tags: Arley Estupiñan, FARC, Francisco Toloza, Hostigamientos, paramilitares, Usme
Slug: farc-denuncia-hostigamientos-y-amenazas-contra-sus-militantes-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/Dd5CjrfUwAAnpJx.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FARC] 

###### [24 May 2018] 

En una rueda de prensa, el partido político FARC, denunció que en Bogotá continúan las amenazas y hostigamientos a sus integrantes, que, de acuerdo a la organización, viene dándose por parte de grupos criminales sucesores del paramilitarismo. **Los casos más preocupantes se han registrado en las localidades de Usme, Ciudad Bolívar y San Cristóbal**.

A finales del año pasado la FARC denunció las agresiones y hostigamientos cometidos contra Leidy Poblador en Ciudad Bolívar y Brayan Segura en la localidad de Suba. A estos hechos, ahora se suman la persecución contra el militante de este partido Arley Estupiñan y otros líderes juveniles, de otras organizaciones de la localidad de Usme, **por el trabajo que vienen acompañando en la defensa de la vida de las personas** que se encuentran en los asentamientos informales del barrio San Germán.

Otro de los hechos que vienen denunciado, es que desde el asesinato del líder social Wilfredy Gómez, ocurrido el pasado 27 de febrero de 2017 en el barrio Compostela en Usme, ha venido aumentado la presencia de grupos criminales **“sucesores del paramilitarismo**”, los expendios de drogas controlados por estas bandas, patrullajes de personas encapuchadas en el barrio San Germán, en la misma localidad y hostigamientos hacia integrantes del partido FARC.

En el caso particular de Estupiñan, el integrante de FARC, afirmó que **hay un plan para atentar contra su vida y la de su familia**, producto del acompañamiento que viene realizando en la localidad de Usme y a las denuncias que se han realizado sobre alianzas entre bandas criminales y grupos paramilitares. (Le puede interesar: ["Atentan contra vida de defensora de derechos humanos María Ruth Sanabria"](https://archivo.contagioradio.com/atentan-contra-vida-de-defensora-de-derechos-humanos-maria-ruth-sanabria/))

En la localidad de Ciudad Bolívar, la situación no es muy diferente, de acuerdo con Sergio Marín, vocero del partido FARC, la alerta temprana activada por la Defensoría del Pueblo confirma el accionar de varios grupos armados en la localidad. Además, en días recientes, en la localidad circuló un panfleto en donde se amenaza a la Mesa de Víctimas del territorio, líderes ambientales y defensores de derechos humanos.

De igual forma señalan como preocupante, los diferentes seguimientos que se le han hecho a Francisco Toloza, integrante del Comité Político de Bogotá FARC, **en diferentes lugares como lo han sido su vivienda, el Bunquer de la Fiscalía y el centro comercial Gran Estación.**

En ese sentido, le exigieron al Estado que brinde todas las garantías, desde las instituciones administrativas, investigativas y de la Fuerza Pública, para la protección de los integrantes del partido FARC, así como el desmantelamiento y persecución penal a todos los grupos criminales sucesores del paramilitarismo.

###### Reciba toda la información de Contagio Radio en [[su correo]
