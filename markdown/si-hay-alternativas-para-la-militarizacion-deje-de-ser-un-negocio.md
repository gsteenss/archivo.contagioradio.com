Title: Con alternativas a la militarización la guerra dejará de ser un negocio
Date: 2019-08-01 18:49
Author: CtgAdm
Category: Ambiente, DDHH
Tags: Ambiente, Antimilitarismo, Antimilitarización, No violencia, paz
Slug: si-hay-alternativas-para-la-militarizacion-deje-de-ser-un-negocio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/EA6QScBXUAAHGum.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:][@warresistersint] 

En marco de la Conferencia Internacional de Antimilitarismos en Movimiento, que se llevó cabo del 30 de julio al 01 de agosto en Bogotá, y que contó con la participación de 150 experiencias internacionales de diferentes organizaciones, dejó claras las implicaciones de la militarización a nivel mundial y los efectos de la misma en el ambiente.

Igor Seke integrante War Resisters' International expresó su preocupación por la militarización a nivel mundial, ya que se han olvidado y dejando en un segundo plano las vías diplomáticas en organismos internacionales u otras instancias para priorizar las salidas unilaterales y la guerra. (Le puede interesar: [Más de 150 activistas se reúnen en Bogotá para hacer resistencia pacífica a la militarización](https://archivo.contagioradio.com/mas-de-150-activistas-se-reunen-en-bogota-para-hacer-resistencia-pacifica-a-la-militarizacion/)).

Para el caso se resaltó, por ejemplo, la invasión militar a Palestina por parte de Israel, el conflicto en Siria, los ataques contra de Venezuela y la “actitud guerrerista” de Estados Unidos en varias regiones del mundo, llegando hasta anunciar su salida del Consejo de Seguridad de la ONU por lo reiterados llamados a condenar las acciones de Israel contra Palestina.

“Están militarizados los territorios, el medio oriente y están militarizando las fronteras”, por ejemplo, “En América Latina imponen la militarización… Trump le dobló la mano a México, quienes bajo unas sanciones económicas aceptaron la militarización de sus fronteras, contrario a sus propuestas electorales” aseguró Zec.  
“El ejército estadounidense es el contaminante número uno del planeta”. (Le puede interesar: [La Objeción de Conciencia, un derecho ignorado en Colombia](https://archivo.contagioradio.com/objecion-conciencia-derecho-ignorado/))

**"El ejército estadounidense es el contaminante número uno del planeta"**

Otro de los hechos que se resaltó en esta Conferencia son los efectos y graves costos ambientales que producen los ejércitos a nivel mundial. “El ejército estadounidense es el contaminante número uno del planeta”. Los aviones, tanques y diferentes armamentos militares tienen una implicación directa en el ambiente.

Según el integrante de War Resisters’ la gasolina que usa un buque de guerra en un día contamina lo que 50 mil automóviles, “hablamos de la reducción del Co2 y el uso de plástico” pero no de las consecuencias militarismo en este aspecto. Por otro lado, Seke agregó que el negocio ‘redondo’ de los grandes empresarios estadounidense en cuanto a la destrucción de los países al Norte de África y en el Medio Orientes, pues, destrozar la infraestructura y entregarla a estas empresas cuyos dueños son generales retirados, se convirtió en un negocio lucrativo.

**En cuanto a Colombia**  
Dentro de los temas trabajados no podía faltar el caso colombiano que atraviesa una situación especial con el acuerdo de paz, que se planteó como la posibilidad de una salida negociada al conflicto armado, es decir una apuesta contra el militarismo. Para Seke “La paz no es ausencia de guerra, por eso es necesario que la paz tome cuerpo y se le dé vida en los territorios”.

**Las alternativas a la guerra y la militarización**  
Sin embargo, y pese a la expansión militar en el mundo, los ejércitos están presentando una crisis al menos en cuanto a los niveles de reclutamiento, “en países europeos a los jóvenes ya no les interesa la guerra”, por eso para suplir sus necesidades han iniciado campañas de nacionalización de migrantes que deseen unirse a esas fuerzas.  
Uno de los aspectos más importantes mencionados por el experto es la importancia de fortalecer no solo las políticas para la no militarización, sino también prácticas económicas alternativas “Los pueblos indígenas lo han entendido muy bien, México está retornando a autónomas económicas según sus costumbres”, esto para evitar que el ‘reclutamiento’ sea una salida económica.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_39460440" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_39460440_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
