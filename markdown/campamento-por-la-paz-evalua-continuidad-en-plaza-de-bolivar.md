Title: Campamento por la Paz evalúa continuidad en Plaza de Bolívar
Date: 2016-11-15 12:19
Category: Entrevistas, Paz
Tags: #AcuerdosYA, Campamento por la Paz
Slug: campamento-por-la-paz-evalua-continuidad-en-plaza-de-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/campamentopaz1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Nov 2016] 

El Campamento por la Paz es una de las iniciativas cívicas que más ha generado presión en torno a los acuerdos de paz de La Habana y ahora tras el anunció un segundo acuerdo, las personas que hacen parte de este campamento **están reevaluando la posibilidad de continuar hasta que se dé la implementación de los acuerdos en el país.**

Una de las propuestas que había salido del Campamento por la Paz era la posibilidad de **gestar unas veedurías ciudadanas  en los puntos  de dejación de armas y participación política**, en donde miembros del Campamento por la paz pudieran hacer un acompañamiento.

De acuerdo con Arie Capella, “**tener el acuerdo no significa la aprobación del mismo**, y es necesaria la garantía de la implementación de los acuerdos, pensamos que necesitamos garantías y podríamos seguir hasta que se de paso a esta implementación”.

**Esta implementación podría durar aproximadamente un año y medio**, y al igual que el Campamento por la paz, otras organizaciones y plataformas como la Mesa Ecuménica por la paz, han expresado su voluntad de esbozar y participar en estas veedurías ciudadanas. Por ahora hay que esperar a que se decida cuál será el mecanismo de refrendación de los acuerdos de paz, para dar paso a la tan anhelada implementación de los mismos. Le puede interesar:["Se cumple un mes de Campamento por la Paz"](https://archivo.contagioradio.com/se-cumple-un-mes-de-la-instalacion-del-campamento-por-la-paz/)

“Hay que evolucionar y nosotros establecimos el campamento, se ha hecho pedagogía con el mismo, pero d**ebemos pasar a un nuevo nivel y durante todo el fin de semana hemos pensado en esa conveniencia**” afirmo Capella.

Hoy se llevará a cabo la **“Gran marcha en respaldo a los Acuerdos de la Esperanza”** como se le ha denominado a este segundo texto, la cita es en el Planetario a las 4:30 pm y la movilización llegará hasta la Plaza de Bolívar, exigiendo que se de una pronta implementación de los acuerdos y pidiendo que se resuelva prontamente la instalación de la mesa de conversaciones con el ELN

Ataques contra el Campamento por la Paz
---------------------------------------

Pese a que esta iniciativa surge como escenario pacífico que no busca la confrontación política con ningún sector, las personas que han participado de este escenario han sido víctimas de amenazas y más recientemente fueron tachadas de **“guerrillero y marihuaneros” por Coronel Plazas Vega. **Le puede interesar: ["Campamento por la Paz en Montería se levanta por amenazas"](https://archivo.contagioradio.com/campamento-por-la-paz-en-monteria-se-levanta-por-amenazas/)

El hecho se dio en el lanzamiento del libro “Defender la Democracia Maestro” en donde Plazas Vega acuso a las y los ciudadanos que hacen parte del Campamento por la paz de pertenecer a guerrillas: **“en la Plaza de Bolívar ondean las banderas del M-19 y en las carpas instaladas por las juventudes de las FARC, exhala el olor a marihuana”.**

<iframe id="audio_13768261" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13768261_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
