Title: Esta Semana Santa, Bogotá se viste de Teatro
Date: 2016-03-25 08:00
Category: Cultura
Tags: Corporación Colombiana de Teatro, Festival Alternativo de Teatro
Slug: esta-semana-santa-bogota-se-viste-de-teatro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Teatro-en-Bogotá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ABC del Bebé 

###### [25 Mar 2016]

Los capitalinos que decidieron quedarse en Bogota podrán disfrutar de la programación que el Festival de Teatro Alternativo tiene preparada para esta Semana Santa. Grupos internacionales de España, Ecuador, México, Panamá y grupos nacionales de Medellín, Pereira y Bogotá, hacen parte de la agenda cultural del festival.

El próximo Jueves 24 se presentará el maestro Nicolas Buenaventura con su obra “Dar a Luz” en el teatro Seki Sano, mientras que México hará su presentación con el grupo Kalipatos y su obra “En los Dientes del León”, en el teatro  La Candelaria. El grupo La Hora 25, de Medellín se presentará en el teatro Varasanta.

El 25 de marzo Maite Guevara, una de la especialista en clown se presentará en el teatro Seki Sano con la obra “Ahora” y el grupo El Galeón de Medellín traerá su espectáculo “Noctambulo”, una obra que se realiza desde los sentidos de los espectadores.

El sábado 26 de marzo, la programación gratuita estará en la Biblioteca Virgilio Barco con la obra “Movimientos” y en la Biblioteca Fransisco José de Caldas en Suba, con la obra “De Gallinas, Huevos y Gallos”.

El gran cierre del FESTA estará a cargo del grupo Malayerba de Ecuador con su obra “La República Análoga” y  los grupos distritales Orca Produce, con la obra “Mientras el Mundo Gira”; Teatro Estudio Alcaravan con “La Soledad de los Nadies” y Teatro Barraca con “Ay días Chiqui”.

Recuerde que la boletería tiene un costo de \$8.000 para estudiantes con carné y \$12.000 boletería general.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
