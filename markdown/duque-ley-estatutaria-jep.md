Title: Presidente Duque sancione la ley estatutaria de la JEP: Organizaciones
Date: 2019-02-18 13:20
Author: AdminContagio
Category: Paz, Política
Tags: #YoDefiendoLaJEP, Duque, JEP, Ley estatutaria
Slug: duque-ley-estatutaria-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Diseño-sin-título2-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 F eb 2019] 

Una semana ha pasado desde que Ernesto Macias, presidente del Senado, envió el documento del Proyecto de Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP) a presidente Iván Duque para su sanción, pero nada ha ocurrido; por esta razón, más de 200 organizaciones **suscribieron una carta en la que piden al Presidente sancionar el Proyecto, y así permitir el pleno funcionamiento de la Jurisdicción**.

**Soraya Gutiérrez, abogada integrante del Colectivo de Abogados José Alvear Restrepo (una de las organizaciones que firmaron la misiva),** asegura que la firma de la carta podría tardar cerca de dos semanas más, porque el mismo Presidente ha señalado que se tomará los **20 días hábiles que tiene para estudiar el Proyecto de Ley**. (Le puede interesar: ["¿Firmará Duque la Ley Estatutaria de la JEP?"](https://archivo.contagioradio.com/ley-estatutaria-jep/))

Sin embargo, Gutiérrez sostiene que dicho plazo es un obstáculo para que esta Institución opere con plenas garantías, **afectando directamente los derechos de las víctimas a acceder a justicia, verdad y reparación**. Demora que estaría injustificada, porque tanto el Congreso de la República como la Corte Constitucional dieron su aprobación política y jurídica al texto normativo.

### **Sancionar la Ley para avanzar en el compromiso con las víctimas** 

Pasados los 20 días hábiles, el presidente Duque tendría que decidir entre objetar el proyecto de Ley, o sancionarlo y permitir que se convierta en Ley. Si decidiera objetarlo, el **Proyecto tendría que regresar al Congreso para ser nuevamente discutido,** y pasaría nuevamente a revisión por parte de la Corte Constitucional; implicando que tomaría, meses su nueva aprobación.

Este nuevo plazo representaría una "moratoria" en tiempo para las víctimas de los más de 13 mil comparecientes a la JEP, entre miembros de las extintas FARC y agentes del Estado, que esperan saber la verdad de los hechos ocurridos en el conflicto. Por el contrario, la demora significaría que quienes han recibido libertad condicionada estén en medio de vacíos legales, que les permitan disfrutar de este beneficio penal sin nada a cambio.

El otro camino que podría tomar Duque sería la sanción presidencial, en cuyo caso la JEP contaría con su "columna vertebral" para actuar, lo que implicaría garantías jurídicas para quienes se someten ante este mecanismo de justicia transicional, como para las víctimas que esperan ver satisfechos sus derechos. (Le puede interesar: ["¿Plan Nacional de Desarrollo de Duque se olvidó de la paz?"](https://archivo.contagioradio.com/plan-de-desarrollo-olvido-paz/))

Sin embargo, la Abogada resaltó que si el Proyecto de Ley no fuera sancionado podrían tomar medidas jurídicas internas, o podrían acudir a instancias internacionales como el Sistema Interamericano de Derechos Humanos, o la **Corte Penal Internacional, que está vigilante de la investigación que hace el Estado de Colombia sobre crímenes graves cometidos en el marco del conflicto**, y que pueden ser juzgados por la JEP.

[Organizaciones sociales pid...](https://www.scribd.com/document/399921133/Organizaciones-sociales-piden-a-Duque-sancionar-Ley-Estatutaria-de-la-JEP#from_embed "View Organizaciones sociales piden a Duque sancionar Ley Estatutaria de la JEP on Scribd") by on Scribd

<iframe class="scribd_iframe_embed" title="Organizaciones sociales piden a Duque sancionar Ley Estatutaria de la JEP" src="https://www.scribd.com/embeds/399921133/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true&amp;access_key=key-knGZAfJBRzlqAZzZbPSo" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="true" data-aspect-ratio="null"></iframe>

> Estamos ahora en [@ComisiondePaz](https://twitter.com/ComisiondePaz?ref_src=twsrc%5Etfw) de [@SenadoGovCo](https://twitter.com/SenadoGovCo?ref_src=twsrc%5Etfw) y [@CamaraColombia](https://twitter.com/CamaraColombia?ref_src=twsrc%5Etfw) donde Jenny Lincoln del [@CarterCenter](https://twitter.com/CarterCenter?ref_src=twsrc%5Etfw) pide que se apruebe la Ley Estatutaria de la [@JEP\_Colombia](https://twitter.com/JEP_Colombia?ref_src=twsrc%5Etfw) [@CSIVIFARC](https://twitter.com/CSIVIFARC?ref_src=twsrc%5Etfw) [@onucolombia](https://twitter.com/onucolombia?ref_src=twsrc%5Etfw) [@Ccajar](https://twitter.com/Ccajar?ref_src=twsrc%5Etfw) [pic.twitter.com/kb3ADuTErS](https://t.co/kb3ADuTErS)
>
> — Alberto Castilla Salazar (@CastillaSenador) [13 de febrero de 2019](https://twitter.com/CastillaSenador/status/1095695217158688769?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
<iframe id="audio_32651169" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32651169_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
