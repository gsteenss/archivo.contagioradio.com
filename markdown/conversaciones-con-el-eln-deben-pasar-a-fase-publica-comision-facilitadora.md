Title: "Conversaciones con el ELN deben pasar a fase pública" Comisión Facilitadora
Date: 2015-01-30 20:27
Author: CtgAdm
Category: Otra Mirada, Paz
Tags: ELN, FARC, paz, proceso de paz
Slug: conversaciones-con-el-eln-deben-pasar-a-fase-publica-comision-facilitadora
Status: published

##### <iframe src="http://www.ivoox.com/player_ek_4020401_2_1.html?data=lZWfkpmUdY6ZmKiakpmJd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkNbd1JCyxtrFtsXjjKjSzs7XcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Luis Eduardo Celis ] 

Mediante un comunicado, la comisión facilitadora de los diálogos con el ELN, de la que hacen parte, el exprocurador Jaime Bernal Cuéllar, el senador Horacio Serpa y el profesor Alejo Vargas; le pidieron al gobierno y al Ejército de Liberación Nacional, **terminar con la fase preliminar de las conversaciones de paz, para pasar a la mesa pública.**

Así mismo, Luis Eduardo Celis, investigador en temas de paz y conflicto, y asesor en la Corporación Nuevo Arcoiris, indicó que “por información del jefe de la delegación del ELN, han tenido **doce ciclos y tres rondas de trabajo durante el 2014,** y aun no han logrado concluir (…) estamos ante la necesidad de que esa fase exploratoria discreta se culmine y se pase a una fase publica abierta en los próximos días o semanas”.

Además, Celis, aseguró que es importante que exista una construcción de un marco de justicia transicional único, para las FARC y el ELN. En el comunicado, la comisión dijo que es importante aprovechar el momento, debido a que **hoy existe una cercanía entre amabas guerrillas,** lo que se ve como positivo para lograr un **acuerdo de paz completo**.

En el comunicado se afirma que ambas partes tienen la voluntad de dialogar y acabar con medio siglo de conflicto. Así lo informaron: “**Las delegaciones guerrilleras están haciendo el esfuerzo de llegar lo más pronto posible al cierre de las conversaciones**”, por otro lado, respecto al gobierno, señalaron que, “no dudamos de la voluntad del presidente de apostar el capital político para llegar a un acuerdo”.

Finalmente,  de la voz del exprocurador Bernal, la comisión reafirmó su apoyo en las conversaciones y “las tareas que nos designen”, informaron por medio del documento.
