Title: Servicios de seguridad en Francia podrían actuar sin orden judicial
Date: 2015-04-14 15:20
Author: CtgAdm
Category: DDHH, El mundo
Tags: francia, Manuel valls ministro interior Francia, yihadismo islámico
Slug: servicios-de-seguridaden-francia-podria-actuar-sin-orden-judicial
Status: published

###### Foto:Taringa.net 

A **tres meses** del atentado contra la revista satírica **Charlie Hebdo**, en la que murieron 17 personas, incluidos dibujantes de la propia revista, el **gobierno francés** está preparando un **proyecto de ley para permitir a los servicios de inteligencia actuar sin orden judicial**.

El ministro del interior **Manuel Valls** presentó el polémico proyecto de ley este lunes, según el propio ministro para poder **combatir el yihadismo islámico** y defender a Francia del terrorismo.

El proyecto de ley dotará de más recursos a los servicios de **inteligencia** pero sobre todo cambiará su control judicial a un mero control administrativo y político del mismo. He aquí la polémica al poder ser usado partidariamente y al servicio del partido gobernante o de las fuerzas de seguridad públicas.

**Los servicios secretos de inteligencia podrán espiar redes sociales, solicitar datos privados a compañías telefónicas o empresas de información que se verán obligadas a facilitar los datos**.

También se **pretende legalizar prácticas que hasta ahora entraban en la ilegalidad** o no estaban reguladas como la instalación de micrófonos en domicilios particulares. Entre las prácticas más polémicas está la de entrar en los domicilios sin la autorización de un juez.

Los **agentes podrían actuar en una situación de emergencia sin autorización previa**, exceptuando a jueces, abogados y parlamentarios.

Con la propuesta del ministro del interior para **la actuación de los servicios de inteligencia será necesaria sola la autorización del ministro del interior** que podrá contar con la opinión de una Comisión Nacional de Control de Técnicas de Información compuesta por 4 parlamentarios, dos magistrados, dos miembros del consejo de estado y un experto en telecomunicaciones.

El anuncio coincide con la **publicación en el periódico “Le Monde”** sobre un artículo que habla de que los servicios de inteligencia actualmente tienen una **base de datos ilegal donde acumulan todo tipo de información privada** sobre cientos de ciudadanos franceses.

La futura ley va enmarcada en las **políticas contra el yihadismo de la UE**, que justificándose en las amenazas terroristas, están poniendo en funcionamiento **mecanismos legales que atentan contra la libertad de los ciudadanos y que según la ONU y numerosas organizaciones sociales de DDHH constituyen una clara violación de los DDHH.**
