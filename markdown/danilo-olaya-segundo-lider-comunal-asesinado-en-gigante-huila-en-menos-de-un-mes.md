Title: Danilo Olaya, segundo líder comunal asesinado en Gigante, Huila en menos de un mes
Date: 2019-08-26 14:03
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: asesinato de líderes sociales, Gigante, Huila
Slug: danilo-olaya-segundo-lider-comunal-asesinado-en-gigante-huila-en-menos-de-un-mes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Danilo-Olaya.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

En la madrugada del pasado 25 de agosto fue asesinado el campesino **Danilo Olaya Perdomo de 60 años,** quien se desempeñaba como presidente de la Junta de Acción Comunal de la vereda Alto Cachaya de Gigante, Huila. Con su asesinato son dos los líderes comunales asesinados en este municipio en menos de un mes.

Según Óscar Prieto, asesor técnico del Observatorio Surcolombiano de Derechos Humanos, hombres armados llegaron hasta su finca, redujeron al mayordomo y posteriormente asesinaron al líder comunal con dos disparos de arma de fuego. Aunque se desconocen los móviles del crimen, información preliminar descarta que el líder comunal hubiera sido amenazado con anterioridad.

**Danilo era un líder dinámico quien fue postulado y trabajó varias veces como presidente de la junta**, lo que representaba el nivel de confianza que los habitantes de Alto Cachaya depositaban en él, era a su vez uno de los mayores productores de panela de la localidad.

**"En el asesinato de un líder social no solo se vulneran los derechos de la víctima y su familia, sino de toda una comunidad que cuenta con él"**, declaró Prieto destacando que Danilo constantemente hacía requerimientos para solventar las necesidades de su comunidad.

El asesinato de Danilo se suma al de **Humberto Díaz Tierradentro, presidente de la Junta de Acción Comunal de la vereda La Guadalupe** quien también fue víctima de homicidio el pasado 20 de julio. [(Le puede interesar: Mientras Duque daba un discurso en el Congreso, 3 líderes fueron asesinados en Colombia)](https://archivo.contagioradio.com/duque-lideres-asesinados-colombia/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_40607698" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_40607698_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
