Title: Violencia contra la mujer durante la pandemia
Date: 2020-08-07 08:29
Author: AdminContagio
Category: El mundo
Tags: Violencia contra las mujeres, violencia doméstica
Slug: violencia-contra-la-mujer-durante-la-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/violencia_mujer_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: yolandadelrio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Polonia, el ministro de justicia Zbigniew Ziobro anunció en el mes de julio que se retiraría de la Convención de Estambul, tratado cuyo objetivo es combatir la violencia de género e intrafamiliar y proteger a la .  El ministro argumentó que el tratado contenía disposiciones “ideológicas” y que iría en contra de los derechos de los padres a criar a sus hijos de acuerdo con sus creencias religiosas”. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A si mismo, en Turquía el gobernante del Partido de la Justicia y el Desarrollo (AKP) informó que la próxima semana se decidiría si el país dejará el tratado para proteger a las mujeres, utilizando un argumento muy similar al de Polonia; el pacto destruye a las familias turcas y los valores tradiciones que ya están protegiendo a las mujeres de la violencia”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“La retirada del Convenio tendría consecuencias desastrosas para millones de mujeres y niñas en el país \[..\] El debate sobre esta posibilidad está teniendo enormes repercusiones negativas en la seguridad de las mujeres y las niñas”, manifestó Anna Błús investigadora de Amnistía Internacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas medidas están causando protestas en ambos países, sobre todo por este contexto de pandemia en que, sin un tratado que es fundamental para la protección, las mujeres y niñas podrían estar corriendo más riesgo que nunca, habitando diariamente con su maltratador. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Magdalena Lempart, quien ayudó a organizar una de las protestas en Varsovia, capital de Polonia, dijo en una entrevista a CNN, que el objetivo de la medida era realmente "legalizar la violencia doméstica".  (Le puede interesar: [Más de 186 mujeres padecen Covid 19 en la Cárcel El Buen Pastor](https://archivo.contagioradio.com/mas-de-186-mujeres-padecen-covid-19-en-la-carcel-el-buen-pastor/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

**La violencia contra la mujer ha aumentado en todo el mundo**
--------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Y es que desde que inició la pandemia y con ella, el confinamiento, organizaciones internacionales advirtieron que con esta medida, se evitaría elevar el número de contagios, pero la violencia doméstica especialmente contra la mujer aumentaría a un punto preocupante.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dubravka Šimonović, relatora especial de la ONU, destacó el 27 de marzo, que las acciones para enfrentar la actual crisis de salud podrían provocar una agudización de la violencia doméstica contra las mujeres. Igualmente lo hizo Human Rights Watch afirmando que “los confinamientos asociados con estas, pueden desencadenar una mayor incidencia de la violencia doméstica por motivos como el aumento de estrés, el deterioro en las condiciones de vida, el hacinamiento y el quiebre de los sistemas de apoyo comunitarios.”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Efectivamente esto no demoró en sentirse en los hogares de todo el mundo. Según los reportes de la ONU, la violencia doméstica se incrementó en un 20% donde la población está sometida a confinamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Londres las denuncias telefónicas por casos de violencia doméstica registraron un 11,4% de aumento con respecto al mismo período del año pasado, según registros de investigadores del Centro de Desempeño Económico (CEP) de la London School of Economics.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, en todo America Latina, el [Mapa Latinoamericano de Feminicidios](https://mundosur.org/index.php/feminicidios/)(MLF), herramienta de monitoreo diseñada por MundoSur muestra que 373 feminicidios se han registrado en 10 países de América Latina durante las cuarentenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Colombia el ministro de Salud, Fernando Ruiz Gómez señaló que la violencia contra la mujer ha aumentado, basándose en el registro de índice de llamadas recibidas, ya que cerca del 2,5 % a la línea 123, se refieren a casos de violencia doméstica. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, en México, que es uno de los casos preocupantes, la violencia contra la mujer aumentó en un 60% durante los meses de pandemia.

<!-- /wp:paragraph -->

<!-- wp:heading -->

La ausencia del colegio también afectaría a las niños
-----------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Al mismo tiempo, la pandemia ha impedido que niños, niñas, adolescentes y jóvenes asistan a las escuelas. Así lo presenta el informe de la Organización de las Naciones Unidas para la Educación (UNESCO), que explicaría que 24 millones de estudiantes al rededor del mundo, desde el nivel de preescolar hasta el ciclo superior, podrían no volver a la escuela en 2020 y gran parte de estos casos serán por falta de recursos monetarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo que significaría que aún abriendo las escuelas, para muchos estudiantes representa permanecer en sus hogares, en el mismo lugar que puede estar su agresor.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
