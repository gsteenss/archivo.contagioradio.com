Title: Alex Vitonás Casamachín, indígena asesinado en Toribío, Cauca
Date: 2019-11-03 16:20
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cauca, indigena, Nasa, Toribío
Slug: alex-vitonas-casamachin-indigena-asesinado-en-toribio-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Este sábado el Consejo Regional Indígena del Cauca (CRIC) denunció el asesinato de Alex Vitonás Casamachín, integrante del pueblo Nasa en la vereda Loma Linda del municipio de Toribío en Cauca. Según las primeras informaciones, Vitonás tenía 18 años, no ejercía labores de licerazgo y fue atacado por hombres armados que le quitaron su vida. (Le puede interesar: ["No se detiene el exterminio hacia los pueblos indígenas"](https://archivo.contagioradio.com/no-se-detiene-el-exterminio-hacia-los-pueblos-indigenas/))

El CRIC advirtió el mismo día que fue asesinado Alex Vitonás, **desde tempranas horas de la mañana, que se estaban presentando enfrentamientos en la zona alta de Corinto** entre el Ejército y grupos armados, dejando a las veredas de los Andes, el Alto y Esther en medio del fuego cruzado. Los combates se dan tras la llegada de carros blindados, y el anunció de envío de más pie de fuerza al territorio para combatir al grupo que asesinó a cinco integrantes de los pueblos indígenas en Tacueyó.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcric.colombia%2Fvideos%2F547301582499139%2F&amp;show_text=0&amp;width=261" width="261" height="476" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **La militarización del territorio**

En entrevista con Contagio Radio, un integrante de la Asociación de Cabildos Indígenas del Norte de Cauca (ACIN) señaló que la propuesta de militarizar más el territorio no serviría para acabar la violencia, y al contrario, podría intensificar el conflicto. Por el contrario, pidió que se implementara la propuesta de 'Carpa Blanca', como una forma de fomentar la interacción entre instituciones y comunidades para proteger la vida en la zona. (Le puede interesar: ["Militarizar más al Cauca es una propuesta «desatinada y arrogante»: ACIN"](https://archivo.contagioradio.com/militarizar-mas-al-cauca-es-una-propuesta-desatinada-y-arrogante-acin/))  
**Síguenos en Facebook**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
