Title: Estos son los finalistas al Premio Nacional a la Defensa de los Derechos Humanos
Date: 2017-08-29 12:04
Category: DDHH, Nacional
Tags: defensores de derechos humanos, Diakonia, Premio Nacional a la Defensa de los Derechos HUmanos
Slug: estos-son-los-finalistas-al-premio-nacional-a-la-defensa-de-los-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/premio-defensores-e1504026219312.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Consejería DDHH] 

###### [29 Ago 2017] 

Luego de la recepción de 71 historias de 18 departamentos de Colombia, se conocen los **16 finalistas de la sexta versión del Premio Nacional a la Defensa de los Derechos Humanos.** El premio otorga un reconocimiento público en 4 categorías que buscan visibilizar el aporte de los y las defensoras a la construcción de democracia y paz en Colombia.

El Programa Colombia de Diakonia, y este año en conjunto con la Iglesia Sueca, ha otorgado este premio desde el año 2012 **como un mecanismo de respaldo al trabajo que realizan los defensores de derechos humanos y del territorio en el país**. Como en años pasados, el reconocimiento a los hombres, mujeres y organizaciones que contribuyen a la democracia y la paz se hará como parte de un mecanismo de protección ante la alarmante situación de riesgo en la que viven estas personas en Colombia. (Le puede interesar: ["Denuncia 335 agresiones a defensores de DDHH en 2017 "Agúzate"](https://archivo.contagioradio.com/somos-defensores-agresiones-en-2017-aguzate/))

### **Categorías y finalistas** 

### **Categoría 1: "Defensor o defensora del año"** 

**Samuel Arregoces**- Defensor del territorio, el agua y la vida de varias comunidades afrocolombianas y del pueblo indígena Wayúu en el sur de la Guajira.

**Enrique Chimonja Coy**- Integrante de la Comisión Intereclesial de Justicia y Paz, defensor de los derechos humanos en Buenaventura y el Valle del Cauca. Él realiza un acompañamiento a las víctimas de violaciones de los derechos humanos y violaciones al Derecho Internacional Humanitario.

**Milena Quiroz Jimenez**- Defensora de la vida y la permanencia de territorios que respetan las prácticas productivas tradicionales. Ella hace parte de la Comisión de Interlocución del Sur de Bolívar, Centro y Sur del Cesar.

**Leyner Palacios Asprilla**- Líder afrocolombiano que defiende los derechos humanos en especial de las víctimas de la masacre de Bojayá. Lidera la reivindicación por los derechos a la verdad, justicia, reparación y no repetición.

### **Categoría 2A: "Experiencia o proceso colectivo del año (proceso social comunitario)"** 

**Comunidad de Autodeterminación Vida y Dignidad (CAVIDA)-** este proceso colectivo reúne a 1.400 familias campesinas que realizan inicativas de construcción de paz en medio de la guerra. Estas familias fueron víctimas de asesinatos y despariciones forzadas a causa de la “Operación Génesis”.

**Asociación Sutsuin Jiyeyu Wayuu, Fuerza de Mujeres Wayuu, SJW-FMW-** Esta organización visibiliza las violaciones a los derechos humanos y los derechos étnicos de la Guajira. Han denunciado los mega proyectos mineros, el desplazamiento forzado y la situación de vulneración de derechos de las mujeres indígenas.

**Comisión de Interlocución del Sur de Bolívar, Centro y Sur del Cesar (CISBCSC)-** Es un proceso de diálogo que ha buscado incidir en la construcción de una salida política al conflicto armado con el propósito de construir la paz.

**Resguardo Indígena Buenavista-Pueblo Indígena Ziobain-** El pueblo ancestral Siona de Buena Vista en el Putumayo se ha organizado para defender su territorio y sus derechos promoviendo y exigiendo la cultura de los derechos humanos de los pueblos con enfoque diferenciado.

### **Categoría 2B: "Experiencia o proceso colectivo del año (modalidad de ONG)"** 

**Corporación Jurídica Libertad-** Es una ONG dedicvada a la defensa y la promoción de los derechos humanos, los derechos de los pueblos y la representaciñon de las víctimas de crímenes de Estado en Antioquia y Chocó.

**Corporación Regional para la Defensa de los Derechos Humanos (CREDHOS)-** Promueve la defensa de los derechos humanos, la democracia y el DIH. Busca además favorecer a los sectores vulneraBles y victimizados de Barrancabermeja y el Magdalena Medio.

**Fundación por la Defensa de los Derechos Humanos y el DIH del Oriente y Centro de Colombia (DHOC)-** Esta organización la integra la población campesina y popular del oriente y centro de Colombia. Trabajan por defender lo público y la permanencia en el territorio.

[**Asociación**]** de Personeros de Catatumbo-** Ellos y ellas realizan acciones para visibilizar, prevenir y proteger a las víctimas de violaciones de derechos humanos en 11 municipios del Catatumbo.

### **Categoría 3: "Reconocimiento a “Toda una vida”** 

**Magdalena Calle**- Lideresa antioqueña que defiende la vida, promueve la gobernabilidad para la paz y la justicia y ha realizado labores educativas e investigativas  para formetar la labor de defensa de los derechos humanos.

**Presbítero Gersaín Buendía-** Dirige la Fundación para el Desarrollo Social y la investigación agrícola FUNDESIA en Caldono, Cauca y trabaja para promover la educación rural y social y el emprendimiento comunitario para defender los derechos humanos.

**Socorro Aceros Bautista-** Lideresa que ha trabajado por la promoción y la defensa de los derechos humanos acompañando a víctimas del paramilitarismo en el municipio de Tame en Arauca.

**Gustavo Adolfo Escamilla Parra-** Ha defendido los derechos humanos, el derecho a la tierra, a la salud, la cultura y el medio ambiente desde 1980. Ha trabajado también por realizar acciones para la restitución de tierras en el Quindío, Cundinamarca y Caquetá.

El director de Diakonia en Colombia, Cesar Grajales, indicó que la labor de las y los defensores de derechos humanos **“aporta a la construcción de paz y solidifica la democracia en el país”** por lo que es importante hacer un reconocimiento que visibilice el trabajo que ellos realizan. Dijo que la situación de agresiones a los defensores es muy preocupante por lo que el premio “contribuye a la legitimidad política y social que les ayuda a fortalecer su trabajo”.

Finalmente, invitó a la ciudadanía a hacer parte de la ceremonia de entrega del Premio Nacional a la Defensa de los Derechos Humanos el día **19 de septiembre en el salón Luis Carlos Galán de la Universidad Javeriana a partir de las 8:00 am**.

<iframe id="audio_20583264" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20583264_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
