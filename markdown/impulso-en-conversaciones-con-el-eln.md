Title: Encuentro de iglesia y plenipotenciarios podría ser impulso en conversaciones con el ELN
Date: 2016-11-16 13:17
Category: Paz
Slug: impulso-en-conversaciones-con-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [16 Nov 2016]

Hoy se realiza una reunión entre representantes de la iglesia católica que han contribuido con el proceso de conversaciones con el ELN, y los integrantes de la comisión de paz del gobierno, para buscar **impulsar el incio de la fase pública de conversaciones con esa guerrilla**, que está aplazada desde el pasado 27 de Octubre y que sigue a la espera de una nueva fecha de partida en Quito, Ecuador que sería la primera sede.

En el encuentro se espera que participen Monseñor Darío Monsalve, Arzobispo de Cali, el obispo de Tibú y el de Chocó entre otros integrantes de la iglesia Católica, así como Juan Camilo Restrepo, jefe de la delegación del gobierno.

Uno de los temas sería Odín Sánchez y el operativo de liberación, puesto que este tema fue una de las condiciones del gobierno para el inicio, incluso por encima del **acuerdo previo que se violó, según lo ha denunciado el propio ELN.** (Lea también [ELN y gobierno deben cumplir su palabra](https://archivo.contagioradio.com/eln-y-gobierno-deben-cumplir-su-palabra-carlos-velandia/))

Para Victor de Currea, profesor universitario e integrante del comité de impulso de la Mesa Social para la Paz, es muy importante que se aclaren los puntos de los acuerdos y que se manifiesten a la sociedad en general, esto con el fin de **entender los pulsos que se estarían tomando en el marco de las conversaciones con el ELN.** (Le puede interesar [Gobierno le incumplió al ELN según Gabino](https://archivo.contagioradio.com/gobierno-le-incumple-al-eln-comandante-nicolas-rodriguez/))

Currea afirma que todos los integrantes de la delegación de paz del ELN están desde del 27 de Octubre en Quito a la espera de que el gobierno cumpla su palabra y de inicio a las conversaciones de paz. Sin embargo, a**segura que es necesario que se inicie la discusión de algunos de los puntos presentes en el nuevo acuerdo de paz con las FARC**,

En concreto, la crítica se centró en algunos aspectos de la vida social y económica del país como el tema de la sustitución de cultivos de uso ilícito, puesto que para este tema se habría reformulado el mecanismo de los subsidios y por otra parte la reforma tributaria.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
