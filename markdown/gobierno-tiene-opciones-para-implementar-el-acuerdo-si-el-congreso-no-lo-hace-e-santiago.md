Title: Gobierno tiene opciones para implementar el acuerdo si el congreso no lo hace: E Santiago
Date: 2017-10-26 16:13
Category: Nacional, Paz
Tags: acuerdo de paz, enrique santiago, Fast Track
Slug: gobierno-tiene-opciones-para-implementar-el-acuerdo-si-el-congreso-no-lo-hace-e-santiago
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Oct 2017] 

El abogado asesor del proceso de paz entre el gobierno y las FARC, Enrique Santiago, hizo un balance del avance de la implementación del Acuerdo de Paz, de los incumplimientos y modificaciones por parte del gobierno al texto final pactado en La Habana, **afirmando que el gobierno tiene las herramientas para salvaguardar el Acuerdo incluso de los intereses electorales**.

El abogado Enrique Santiago afirmó que “**hay una relentización de las leyes básicas para la implementación del acuerdo como la Jurisdicción Especial para la Paz**, la Reforma Política y las normas para poner en marcha las instituciones para el desmantelamiento del paramilitarismo” que probablemente no alcancen a ser tramitadas en el Fast Track. (Le puede interesar: ["Rodrigo Lara actúa contra la ética: Alirio Uribe"](https://archivo.contagioradio.com/rodrigo-lara-actua-contra-la-etica-alirio-uribe/))

### **Las modificaciones al Acuerdo de Paz** 

Según el abogado las modificaciones que más afectan el Acuerdo de Paz son las que se hacen en medio del debate parlamentario o las reincorporaciones que ha hecho el gobierno, porque solo se conocen una vez pasen tanto por Cámara como Senado, un ejemplo es la “absurda” petición que hacen legisladores sobre incorporar leyes que impidan el desarrollo y ejercicio de los derechos políticos de integrantes de las FARC hasta que no pasen por la JEP, **y al mismo tiempo evitan que el proyecto de la JEP sea aprobado**.

Sumado a esa “relentización” se encuentra la carrera electoral que ya empezó y que, de acuerdo con Enrique Santiago, ha dificultado el avance, con estrategias como la falta de quorum. **Razón por la cual considera que será necesario que desde la presidencia se tomen acciones urgentes que blinden los Acuerdos de Paz.**

“La presidencia y las instituciones del Estado deberán tomar algunas de las herramientas de la Constitución para intentar al menos poner **en marcha las normas más importantes y básicas, antes de que el pase se suma en elecciones**” afirmó el abogado y agregó que de no hacerlo el Acuerdo podría terminar en un gran fiasco por la inoperancia del Estado. (Le puede interesar: ["Cambio Radical y Centro Democrático se unen para hacer trizas el acuerdo: FARC"](https://archivo.contagioradio.com/cambio-radical-y-centro-democratico-se-unen-para-hacer-trizas-los-acuerdos-farc/))

### **Los derechos políticos en el Acuerdo de Paz** 

Frente a las múltiples trabas que se han puesto a la participación de los integrantes de las FARCA en el Congreso, Enrique Santiago manifestó que **“no puede ser que en una negociación se le exija a una parte que renuncie a sus derechos fundamentales**” y agregó que en ningún proceso de paz ha pasado algo como esto.

De igual forma señaló que quienes están haciendo este tipo de propuesta solo tienen un fin populista por recolectar votos en lugar de busca la consolidación de la paz, “las personas que las utilizan a largo plazo **quedarán en entredicho y desprestigiadas por lo que significa la práctica de dificultar e impedir la marcha del acuerdo de paz**”.

### **La Reforma Agraria se quedó fuera del Fast Track** 

Uno de los puntos que aún no ha pasado a debate tiene que ver con la reforma Agrícola que se proponía dentro de los Acuerdos de Paz, frente a este tema, Enrique Santiago señaló que es un gran problema que el paquete legislativo que abarca la principal causa del conflicto, **no se haya implementado, aprobado o debatido desde un inicio y demuestra una irresponsabilidad y falta de generosidad de la clase política.**

“Esto pone de manifiesto la poca voluntad de la oligarquía del establecimiento colombiano para renunciar a unos pocos privilegios” afirmó Enrique Santiago y añadió que la posible solución para que se debata e implemente este punto, **podría ser pasar este paquete a debates ordinarios de carácter urgente**, para que antes de que finalice este año puedan ser implementados. (Le puede interesar: ["Corte Cosntitucional obliga a funcionarios públicos a cumplir el acuerdo de paz"](https://archivo.contagioradio.com/corte-constitucional-obliga-a-funcionarios-publicos-a-cumplir-el-acuerdo-de-paz/))

### **Simón Trinidad y la JEP**

Para el abogado la acogida de Simón Trinidad debe ser analizada teniendo en cuenta diferentes situaciones, la primera de ellas es que se acoge a la **JEP pero al mismo tiempo ya se había acogido a la justicia colombiana**, y eso no significa que las leyes de Estados Unidos queden sin vigencia.

De igual forma en la OEA existen convenios jurídicos de cooperación que existen antes del Acuerdo de Paz que permiten que personas condenadas en el extranjero, puedan ser trasladados a su país. **Lo que significa que un traslado de Simón Trinidad a Colombia era posible incluso antes del Acuerdo de Paz**.

### **La esperanza de los Acuerdos de Paz** 

Frente a este panorama que afrontan los Acuerdos de Paz, el abogado expresó que aún queda una buena noticia para el país, y es la voluntad irreductible de las FARC por cumplir los acuerdos de Paz, consolidar la paz y participar en política.

Además, señaló que las **Naciones Unidas y otras organizaciones internacionales tendrían que pronunciarse sobre los diferentes incumplimientos** al acuerdo de paz, el asesinato a ex guerrilleros y sus familias y la falta de aplicación de las amnistías.

<iframe id="audio_21714663" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21714663_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
