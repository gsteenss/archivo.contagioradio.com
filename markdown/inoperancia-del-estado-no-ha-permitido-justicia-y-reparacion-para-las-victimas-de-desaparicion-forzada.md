Title: Inoperancia del Estado no ha permitido justicia y reparación para las víctimas de desaparición forzada
Date: 2015-08-29 19:57
Category: DDHH, Otra Mirada
Tags: 30 de agosto, Bogotá, Bucaramanga, buenaventura, Cali, Detenidos desaparecidos, Día de las Víctimas de Desaparición Forzada, La escombrera, Medellin, Palacio de Justicia
Slug: inoperancia-del-estado-no-ha-permitido-justicia-y-reparacion-para-las-victimas-de-desaparicion-forzada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/DEsaparecidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [desaparicionyejecucionescolombiaimb.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_7664651_2_1.html?data=mJujlpuZdY6ZmKiakpyJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BfpNTZ0dLGrcKfxt3W1dnJsozVxNniw9HRqc%2FoxpChl5DRrc2f0crf1dTSpdSfxcrgw9XFto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Karen Quintero, antropóloga forense] 

###### [29 Ago 2015]

En el marco del Día Internacional de las Víctimas de Desapariciones Forzada, la Coordinación  Colombia Europa, convocó una reunión con familiares de desaparecidos y la comunidad internacional, donde se revelaron graves denuncias frente a este flagelo que consolida una cifra de 45 mil personas desaparecidas.

Durante la reunión se escuchó a los familiares de personas desaparecidas quienes hablaron sobre las principales dificultades que se les ha presentando para acceder a la verdad y la reparación integral.

La Fiscalía General de la Nación tiene datos de 69 mil personas reportadas como desaparecidas, pero de acuerdo con el registro único de consolidación se estima que existen más de 45mil personas en esta condición al año 2015.

En la reunión se denunció sobre la inoperancia de las autoridades estatales para impulsar acciones de búsqueda, por lo cual los familiares aseguran no haber recibido ningún tipo de respuesta, como lo relataron personas que llegaron desde Antoquia por el caso de “La Escombrera”, también los familiares del Palacio de Justicia y personas de Buenaventura que afirmaron que en esa zona del país continúan las casas de pique y se mantiene la desaparición forzada.

Tras conocerse  las declaraciones de las víctimas, la comunidad internacional hizo un llamado al Estado colombiano para que se asuman compromisos reales que permitan coordinar estrategias y medidas donde se incluya todos los sectores de la sociedad para buscar a los desaparecidos. Es por eso que se acordó la realización de un debate de control político para el mes de noviembre.

En el Día de las Víctimas de Desaparición Forzada, se realizarán diversas actividades culturales y movilizaciones en todo el país para recordar a los colombianos desaparecidos. En Medellín, Cali, Bucaramanga y Bogotá, habrá marchas, galerías de la memoria y conciertos.
