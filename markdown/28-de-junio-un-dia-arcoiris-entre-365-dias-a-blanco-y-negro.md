Title: 28 de Junio: Un día arcoiris entre 365 días a blanco y negro
Date: 2015-06-30 07:44
Category: Escritora, Opinion
Tags: Anthony Venn-Brown, Día del Orgullo Gay, LGBTI, Marcha Gay
Slug: 28-de-junio-un-dia-arcoiris-entre-365-dias-a-blanco-y-negro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/MG_9364-e1475069278488.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### [Por: [Escritora Cubierta ](https://archivo.contagioradio.com/escritora-encubierta/) - [~~@~~OneMoreHippi](https://twitter.com/OneMoreHippie) 

###### [29 Jun 2015]

Cada 28 de Junio (Día Internacional del Orgullo LGBT) está marcado por alegría y mucho color; en las calles se respira un aire con sabor a libertad, las personas se reúnen para marchar ondeando banderas y pancartas arcoíris, las redes sociales se inundan de mensajes de apoyo e incluso, por unos segundos, los noticieros pasan de ser amarillistas a ser multicolor. *Casi* se tiene la sensación de que vivimos en una utopía donde la discriminación no tiene cabida, sin embargo siempre hay una piedra en el zapato: el homofóbico dispuesto a arruinarlo todo.

Algunos andan en manada, otros andan solos. Algunos se posicionan en esquinas y murmuran entre sí palabras de desprecio, otros simplemente lanzan miradas fulminantes a los que marchan. Algunos se persignan manifestando que ''estamos en los últimos tiempos'', otros gritan a lo troglodita[ ]{.Apple-converted-space}''¡maricón!'' en un inconsciente pero desesperado intento por realzar su hombría, o más bien la poca que les queda. Es curioso, parece que hay más diversidad entre los homofóbicos que en la misma comunidad LGBT. Me explico, no importa si eres gay o lesbiana, hetero o bisexual, negro o blanco, todos amamos de la misma forma, sin embargo los homofóbicos odian de mil y un maneras.

Otra cosa que tampoco falta es el comentario del heterosexual queriendo actuar de *troll* pero que en realidad queda como un idiota. Sí, ese mismo, junto con el muy escuchado ''¿Y por qué no hay día del orgullo heterosexual?'' Querido amigo, permítame responderle con una cita traducida del escritor australiano Anthony Venn-Brown: ''Cuando usted oiga hablar del Día del Orgullo Gay, recuerde, no nació por la necesidad de celebrar ser gay. Se desarrolló a partir de nuestra necesidad como seres humanos de liberarnos de la opresión y de existir sin ser penalizados, patologizados o perseguidos. Dependiendo de varios factores, especialmente la religión, de liberarnos de la vergüenza gay y aproximarnos al amor propio y la aceptación, no sólo puede ser un viaje angustioso, puede tardar años. Trágicamente algunos no lo hacen. En lugar de preguntarse por qué no hay día del orgullo heterosexual, esté agradecido de que nunca ha necesitado uno''.

Algo más que se hace lucir de alguna forma cada 28 de Junio es la discriminación entre la comunidad LGBT. Existe una tendencia a priorizar al que ha salido del closet sobre el que aún no lo ha hecho y ésta se ve más reforzada en el Día del Orgullo Gay hasta tal punto en que los closeteros pueden llegar a sentirse culpables, excluidos o desvalorados por sus mismos hermanos de la comunidad. ¿Qué pasa, señores? Tenemos que unirnos, no fragmentarnos. Este día no se trata de declarados tirándole piedras a los closeteros, se trata de luchar por nuestros derechos, hágalo por aquellos que no pueden hacerlo.

Si usted es uno de esos closeteros que mencioné anteriormente y me está leyendo, déjeme decirle algo que siempre le digo a las personas que acuden a mí por Twitter, Ask o Tumblr pidiéndome desesperadamente consejos sobre salir del closet con sus padres: *Ser gay, lesbiana o bisexual no es firmar un contrato cuya primera clausula es gritarle a los cuatro vientos tu orientación sexual*. No hay por qué sentirse presionado, la sexualidad es algo personal, sólo usted puede decidir cuándo y a quién contarle al respecto, hacerlo antes o después no te hace menos o más que otra persona. Piense racionalmente, analice los factores y si se siente seguro de salir del closet, hágalo. Siempre y cuando se acepte usted mismo, lo demás no importa.

Para terminar, quiero invitar a todo el que me esté leyendo, sin importar su orientación sexual, a que asista a la marcha en su ciudad que conmemora los disturbios que ocurrieron en Stonewall en 1969. Si no puede asistir, aporte colocando un mensaje positivo en sus redes sociales. Recuerde, la igualdad es para todos.

¡**Feliz Día del Orgullo LGBT**!
