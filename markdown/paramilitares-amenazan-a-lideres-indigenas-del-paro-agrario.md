Title: Previo al paro agrario paramilitares amenazan a líderes indígenas
Date: 2016-05-27 15:27
Category: Nacional, Paro Nacional
Tags: indígenas, ONIC, paramilitares, Paro Nacional
Slug: paramilitares-amenazan-a-lideres-indigenas-del-paro-agrario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Indígenas-del-Norte-del-Cauca-e1464380469118.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Los mundos de Hachero 

###### 27 May 2016

En las últimas semanas, dirigentes indígenas han sido blanco de amenazas y atentados por parte del grupo paramilitar conocido como **“Águilas Negras”.** Luego del atentado fallido contra Albeiro Camayo, coordinador de la Guardia indígena del Norte del Cauca, ahora circula un panfleto donde esta estructura **ofrece hasta 20 millones de pesos por cada indígena asesinado.**

Camayo fue perseguido por **un grupo de personas que se movilizaban en motos y que luego le dispararon con un arma de fuego cuando** se dirigía hacia su lugar casa en la vía Panamericana entre Santander de Quilichao y Popayán. El líder pudo evadir a los individuos, sin embargo algunas balas impactaron en su maletín, en el chaleco antibalas concedido por la Unidad Nacional de Protección y el cojín de la motocicleta en la que se transportaba.

Luis Fernando Arias, consejero mayor de la ONIC, asegura que **los paramilitares están en busca de impedir el desarrollo del Paro Nacional que inicia este 30 de mayo**. Asimismo, denunció que el gobernador del Norte del Cauca, está estigmatizando la movilización del paro agrario y lo  relacionó con un paro armado, por lo además dio instrucciones a la fuerza pública de arremeter contra la protesta.

Frente a esta situación, autoridades indígenas adelantan reuniones con el Ministerio de Interior, para que se inste a los entes territoriales de manera que se garanticen las condiciones para el libre tránsito de las comunidades que inician su movilización este lunes.

<iframe src="http://co.ivoox.com/es/player_ej_11691491_2_1.html?data=kpajm5aYfZKhhpywj5WaaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5ynca3pytiYqMrWssLixdSYo9fNpdSfjpC8sK6ncYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
