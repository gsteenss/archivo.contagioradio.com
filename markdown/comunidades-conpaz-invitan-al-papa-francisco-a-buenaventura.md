Title: Comunidades CONPAZ invitan al Papa Francisco a Buenaventura
Date: 2015-04-08 12:04
Author: CtgAdm
Category: Nacional, Paz
Tags: carta al papa, colombia, comunidades, Conpaz
Slug: comunidades-conpaz-invitan-al-papa-francisco-a-buenaventura
Status: published

###### foto tomada de internet 

En una carta, **CONPAZ, red que reúne  más de 170 comunidades de diferentes lugares de Colombia  invitan a Papa Francisco a visitarlos en sus regiones**, esto luego de conocerse la confirmación de la próxima visita del sumo pontífice al país.

En el comunicado CONPAZ resalta la importancia de la visita del Papa en este momento en que Colombia vive un escenario de construcción de paz, que por una parte realiza diálogos entre el gobierno y las guerrillas de las FARC y el ELN y por otra hay una amplia movilización exigiendo paz con justicia social. Es en ese escenario en que las comunidades consideran que el aporte del Papa Francisco es de especial relevancia.

La invitación concretamente es para que el Papa Francisco visite una de las comunidades que conforma CONPAZ, en especial el **Espacio Humanitario en Buenaventura**  y desde allí escuche de la voz de las víctimas los testimonios que han enfrentado durante su vida, e igualmente que conozca las propuestas de resistencia y paz que han construido en más de 15 años.

*"Estamos seguras y seguros  que si usted nos oye, nuestros dirigentes y mandatarios, los empresarios nacionales e internacionales, escucharan; su escucha hará posible que nuestro  mensaje que sale  como un grito y un clamor hasta el cielo será tenido en cuenta para cimentar otra Colombia en paz, la Colombia que nace de la justicia social y ambiental*." Señala el comunicado de la  red de comunidades colombianas CONPAZ

[Leer todo el contenido de la invitación ](http://bit.ly/1Fhbar3)
