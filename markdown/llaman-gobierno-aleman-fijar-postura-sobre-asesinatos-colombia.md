Title: Llaman al gobierno alemán a fijar postura sobre asesinatos de líderes sociales en Colombia
Date: 2019-05-13 17:51
Author: CtgAdm
Category: Líderes sociales, Política
Tags: Alemania, defensores de derechos humanos, lideres sociales, paz
Slug: llaman-gobierno-aleman-fijar-postura-sobre-asesinatos-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Alemanía-pide-apostar-a-la-paz-en-Colombia.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CarlosHainsfurt] 

Este lunes el movimiento Unidos por la Paz, envió una comunicación al gobierno alemán pidiendo que se "adopte una línea clara en la cooperación económica y de desarrollo con el Estado colombiano sobre la exigencia básica" de que se garantice la vida de los líderes sociales, y se trabaje en cosolidar una paz estable y duradera. El movimiento decidió enviar esta solicitud en rechazo al atentado sufrido por líderes afro en el norte de Cauca.

Como lo recordó Carlos Ceballos, integrante de Unidos por la Paz en Berlin (Alemanía), el asesinato de defensoras y defensores de derechos humanos viene en aumento desde 2016 (año en que se firmó el Acuerdo de Paz), "y se han intensificado cuando asumió el poder el presidente Iván Duque"; situación que preocupa a la comunidad internacional. A ello se suma el poco interés que parece generar los asesinatos en altas esferas del Estado, pues aunque el Presidente dice estar preocupado por la situación, no se presenta a los sitios en los que lo citan las comunidades para tratar estos temas. (Le puede interesar: ["Señor Presidente, lo esperamos en Santander de Quilichao: ACONC"](https://archivo.contagioradio.com/senor-presidente-lo-estamos-esperando-en-santander-de-quilichao-aconc/))

En la comunicación el Movimiento cita a la Defensoría del Pueblo al señalar que desde 2016 hasta abril de 2019 han sido asesinados "681 defensore(as) de derechos humanos y 128 miembros en proceso de reincorporación de las FARC"; entre ellos resaltan el asesinato de [Dimar Torres](https://archivo.contagioradio.com/militar-implicado-senala-que-ejercito-ordeno-seguir-a-dimar-torres/), y el atentado sufrido por diferentes líderes en norte de Cauca el pasado 4 de mayo. (Le puede interesar:["Muchos nos ven como una piedra en el zapato por defender el territorio"](https://archivo.contagioradio.com/muchos-nos-ven-como-una-piedra-en-el-zapato-aconc/))

### **Las exigencias del Gobierno alemán a Colombia**

Ceballos señaló que la trascendencia de este tipo de violaciones contra las personas que defienden los derechos humanos en Europa es relativa, porque "por un lado la Unión Europea lanzó una campaña de protección en favor de las y los líderes sociales; pero por otro lado, el actual Gobierno pareciese que no se interesa mucho por estas acciones". En ese sentido, la intención de la carta es llamar la atención al Gobierno alemán sobre estos hechos.

Por eso, las 17 organizaciones firmantes de la Comunicación pidieron al Estado alemán que la política económica y de cooperación "advierta que la situación de Colombia no puede seguir así" y el Gobierno debe tomar cartas para detener los asesinatos, así como implementar lo acordado en 2016 y continuar en la búsqueda de una salida negociada al conflicto con el ELN. (Le puede interesar: ["Según CINEP en 2018 fueron victimizados 98 líderes cívicos y comunales"](https://archivo.contagioradio.com/en-2018-fueron-victimizados-98-lideres-civicos-y-comunales-cinep/))

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fnotes%2Funidos-por-la-paz-alemania%2Fla-paz-no-llega-a-colombia-681-l%25C3%25ADderes-sociales-asesinadosas-desde-2016-rechazo-%2F622834018183417%2F&amp;width=500" width="500" height="620" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_35764156" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35764156_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
