Title: Corte Interamericana de DDHH condenó al Estado colombiano por desaparecidos del Palacio de Justicia
Date: 2014-12-11 15:09
Author: CtgAdm
Category: DDHH
Slug: corte-interamericana-de-ddhh-condeno-al-estado-colombiano-por-desaparecidos-del-palacio-de-justicia
Status: published

Luego de 29 años de impunidad la Corte Interamericana de Derechos Humanos, emitió sentencia condenatoria contra el Estado colombiano por las desapariciones forzadas de siete empleados de la cafetería del Palacio de Justicia (Carlos Augusto Rodríguez Vera, Cristina del Pilar Guarín Cortés, David Suspes Celis, Bernardo Beltrán Hernández, Héctor Jaime Beltrán Fuentes, Gloria Stella Lizarazo Figueroa, Luz Mary Portela León), de dos visitantes del Palacio de Justicia (Lucy Amparo Oviedo Bonilla y Gloria Anzola de Lanao) y de una guerrillera del M-19 (Irma Franco Pineda); Por La desaparición forzada y posterior ejecución extrajudicial del Magistrado Auxiliar Carlos Horacio Urán Rojas; Las detenciones arbitrarias e ilegales y torturaso tratos crueles y degradantes sufridos, respectivamente, por Yolanda Santodomingo Albericci, Eduardo Matson Ospino, Orlando Quijano y José Vicente Rubiano Galvis, quienes fueron considerados sospechosos de colaborar con el M-19 en el marco de estos hechos; La falta de esclarecimiento judicial de los hechos y la violación del derecho a la integridad personal delos familiares de las víctimas.
