Title: Otra Mirada: El Catatumbo entre fuego cruzado
Date: 2020-07-29 19:55
Author: AdminContagio
Category: Otra Mirada, Otra Mirada, Programas
Tags: ASCAMCAT, Catatumbo, Crisis humanitaria
Slug: otra-mirada-el-catatumbo-entre-fuego-cruzado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Catatumbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @AscamcatOficia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Recientemente, AsSCAMCAT, denunció el asesinato de Jhony Alexander Ortiz, a 1 km del albergue del Ambato, en el municipio de Tibú, Norte de Santander; este se suma a los otros cinco asesinatos de líderes sociales registrados durante este 2020 en la región del Catatumbo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, los asesinatos no son lo único que enfrenta esta región sino también ejecuciones extrajudiciales, masacres y fuego cruzado entre diferentes grupos armados que dejan en medio a la comunidad, generando una crisis humanitaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lina Amaya, directora de la corporación Construyendo Poder, Democracia y Paz (PODERPAZ), Hassan Dodwell, director de Justice for Colombia y Olga Quintero, lideresa de la Asociación Campesina del Catatumbo (ASCAMCAT) fueron los invitados de este conversatorio y dieron un análisis de la situación actual en la región. (Si desea saber más: [Catatumbo sufre seis tipos de pandemias](https://archivo.contagioradio.com/catatumbo-sufre-seis-tipos-de-pandemias/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ya que este es un problema sistemático que se viene generando desde hace mucho tiempo, los panelistas explicaron cuál consideran que es la situación de fondo en la región y qué atributos tiene la misma para que se presenten tantas violaciones a los derechos humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, comentan cuál ha sido el papel del Ejército, teniendo en cuenta que es una de las regiones con más presencia de la fuerza pública, y expresan que el Gobierno Duque ha tenido muy baja presencia fallando en el cumplimiento del acuerdo de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También explican la situación en relación al fuego cruzado entre grupos armados, los monocultivos, las rutas estratégicas de narcotráfico y la influencia e intereses de terceros en el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez, Hassan Dodwell comenta la percepción que existe desde la comunidad internacional y lo que vienen articulando para ser un punto de apoyo a las comunidades, además explica qué tanta credibilidad tiene el presidente en la comunidad internacional frente a su accionar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, los invitados describen cuál podría ser el futuro de la región si no se frenan los actos de violencia y hacen un llamado al Gobierno para hacer acompañamiento y que se implemente el acuerdo de paz. (Si desea escuchar el programa del 27 de julio: [Otra Mirada: vía crucis de la justicia en Colombia](https://www.facebook.com/contagioradio/videos/300195014433539))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Si desea escuchar el análisis completo

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/363792208114463","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/363792208114463

</div>

</figure>
<!-- /wp:core-embed/facebook -->
