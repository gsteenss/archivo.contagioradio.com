Title: Estos son los acuerdos entre Gobierno y Chocoanos
Date: 2017-05-28 09:31
Category: Movilización, Nacional
Tags: Paro Chocó, Quibdo
Slug: se-levanta-paro-en-el-choco-tras-establecer-pre-acuerdos-entre-comunidad-y-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/dignidad-choco-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pulzo] 

###### [28 May 2017]

Luego de 17 días de protestas pacíficas, los habitantes del Chocó lograron establecer pre acuerdos con el gobierno Nacional y levantar el paro que mantenían en el departamento. La **inversión en infraestructura, vías, salud y educación deberán tener resultados en los próximos meses** y serán prioritarias, al igual que definir los límites entre Antioquia y Chocó.

Estos pre acuerdos fueron producto de una intensa jornada de trabajo en la mesa de conversaciones que posteriormente fueron presentados por el comité impulsor del paro, a la ciudadanía y aprobados en un cabildo abierto teniendo como base los 10 puntos pactados en el año 2016.

### **Presupuesto para vías e infraestructura** 

El gobierno se comprometió a **adicionar 440 mil millones de pesos al presupuesto del departamento en infraestructura**, que se les adhieren a los 280 mil millones con los que actualmente cuenta el Chocó.

Según Emilio Pertúz integrante del paro en Chocó el dinero aprobado por el gobierno se ejecutará en 5 años y los restantes serán incluidos en el marco macroeconómico o fiscal del 2018 “es decir antes que el señor presidente Juan Manuel Santos termine su mandato”.

Todo lo anterior para que las vías de comunicación puedan tener la financiación y la terminación adecuada, en donde se incluyen las vías de Medellín – Quibdó y la de Pereira – Quibdó. Le puede interesar: [Comunidades responsabilizan a Santos por compromisos incumplidos en Chocó](https://archivo.contagioradio.com/el-presidente-santos-es-el-responsable-de-los-compromisos-asumidos-e-incumplidos-en-el-choco-emilio-pertuz/)

### **¿Cuáles fueron los acuerdos en el tema salud?** 

Frente a la falta de garantías del derecho a la salud, el Secretario General Alfonso Prada, expresó que se invertirán 84 mil millones de pesos, en el sistema de salud del departamento y se darán otros 2 mil millones que se entregarán a la gobernación.

Por su parte, Pertúz manifestó que se realizará la construcción y adecuación del Hospital de tercer nivel y los tres hospitales regionales **“va a haber recursos del gobierno nacional e igualmente del gobierno departamental enfocado en tomar recursos de las regalías** (…) el Estado colombiano debe hacer unas políticas que realmente respondan a nuestras características de tipo regional, étnico y social” Le puede interesar: ["En Chocó, indígenas y afrodescendientes marchan contra el paramilitarismo"](https://archivo.contagioradio.com/en-choco-indigenas-y-afrodescendientes-marchan-contra-el-paramilitarismo/)

### **¿Qué va a pasar si el gobierno no cumple?** 

Luego de muchos años de diálogos con el gobierno nacional y de varios acuerdos, las comunidades de Chocó manifestaron que seguirán atentos para ver el avance que tengan estos, sin embargo, **si hay un nuevo incumplimiento los chocoanos propondrán que “Colombia siga por una ruta y el departamento del Chocó siga como república independiente por otra ruta”** cuenta Pertúz.

Dice él, que es muy desgastante ver cómo unas regiones del país puedan acceder a 2 o 3 billones de pesos para sus necesidades y mientras tanto el departamento del Chocó tenga que someterse a un paro para obtener lo que requieren.

“Cada vez sentimos más que el gobierno central no siente al Chocó como Colombia y como chocoanos tenemos que pensar seriamente en que tenemos que ver en una vía que administrativamente no sigamos dependiendo de Colombia” dijo Pertúz.

### **¿Qué viene ahora?** 

Para el líder si no se comienzan a tener políticas públicas bien pensadas, vías, energía y el tema productivo resuelto se van a volver a enfrentar a un grave problema, razón por la cual, continuarán cada 2 meses haciendo seguimiento y reporte de los que se ha avanzado en ese departamento en cuanto a los acuerdos y continuarán con la movilización.

“Quisiéramos que se hubiera hecho el giro de los recursos inmediatamente, pero sabemos que esto no se va a logra de la noche a la mañana, **porque el Estado nunca ha pensado en serio ni en el Chocó ni en el Pacífico** (…) Para el Estado colombiano pensar en 1 billón para Chocó es imposible, el Estado sigue siendo racista y excluyente” añade Pertúz.

<iframe id="audio_18984153" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18984153_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
