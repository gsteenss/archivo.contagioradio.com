Title: Proyecto de Ley reduciría aporte de pensionados a la salud
Date: 2016-04-06 13:41
Category: Economía, Nacional
Tags: alirio uribe, pensionados colombia, Proyecto de ley 062
Slug: proyecto-de-ley-reduciria-aporte-de-pensionados-a-la-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Proyecto-de-ley-062.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alirio Uribe ] 

###### [6 Abril 2016 ]

Actualmente cursa en el Congreso el Proyecto de Ley 062 radicado en septiembre de 2015, que busca que las personas pensionadas dejen de aportar el 12% de su pensión al régimen contributivo de salud para contribuir con el 4%; iniciativa que ya había sido Ley pero que de acuerdo con el Representante Alirio Uribe Muñoz, **al ser promulgada "mal y de mala fe", fue declarada inconstitucional**.

Conforme con lo establecido por la Ley 100, el 12.5% de la nomina salarial debe aportarse al régimen contributivo salud, un 8.5% a cargo del empleador y el 4% por el trabajador; sin embargo, la Ley 1607 de 2012 hizo que los empleadores aportaran a través del impuesto CREE, manteniéndose intacto el aporte de los trabajadores y **relevando la responsabilidad que debía asumir el empleador**.

Según el Representante, mientras que los trabajadores activos contribuyen con el 4% de su salario, **los pensionados deben aportar al régimen contributivo el 12.5%** de su pensión, que en el 90% de los casos es menor a un salario mínimo, "a todas luces una injusticia" sí se tiene en cuenta que los pensionados reciben el 75% del promedio salarial, con aumentos inferiores al IPC, que los han llevado a **perder el 23% de su capacidad adquisitiva**.

Situación por la que Alirio Uribe afirma que debe presionarse al presidente Juan Manuel Santos para que cumpla con la promesa hecha en campaña y permita reformar el aporte de los pensionados a la salud, acción que había intentado a través del **Proyecto de Ley 183 de 2014 que fue "hundido y saboteado" por el Ministerio Hacienda**, bajo el argumento de que la medida afectaba la sostenibilidad fiscal.

"Se legisla en función de los poderes económicos pero no del pueblo (...) pareciera que tener pensión es un privilegio y que no se justifican los aumentos anuales" pero si las **altas cargas de los pensionados en sus aportes al régimen de salud**, asegura el Representante, quien invita a las personas pensionadas a "rodear el Congreso" para que sea aprobado el Proyecto de Ley en el último debate que le queda en Cámara y los dos restantes en el Senado.

<iframe src="http://co.ivoox.com/es/player_ej_11068368_2_1.html?data=kpadmJ2Xepmhhpywj5aWaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncbHm0N7SxdnTb8XZjLHS25DWqcXpxM7fh6iXaaK4wpDO0tTWuMafxcqY0srSt8rjz8bR0diPpYzgwpDgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]

 
