Title: Matilda González nueva secretaria de la Mujer en Manizales
Date: 2019-12-27 12:32
Author: CtgAdm
Category: LGBTI, Nacional
Tags: Matilda Marín LGBTI Manizales Feminismo
Slug: matilda-gonzalez-nueva-secretaria-de-la-mujer-en-manizales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Diseño-sin-título-3-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:]@MatiGonzálezGIl 

Matilda González Gil fue nombrada este jueves 26 de diciembre como la primera mujer líder del movimiento trans, en encabezar la Secretaría de la Mujer y Asuntos de Género en Manizales; el nombramiento lo hizo el nuevo alcalde Carlos Marín. (Le puede interesar:[Necesitamos que el poder sea femenino, transformador e incluyente: Ángela Robledo](https://archivo.contagioradio.com/necesitamos-que-el-poder-sea-femenino-transformador-e-incluyente-angela-robledo/))

> Muy agradecida, honrada y emocionada de hacer parte del equipo del [@Alcalde\_Verde](https://twitter.com/Alcalde_Verde?ref_src=twsrc%5Etfw)  
> Con toda la concentración, mente, y sobretodo, el corazón en este nuevo reto. Aún no me las creo (¡omaigad amigas!) [pic.twitter.com/9aUFBJimZC](https://t.co/9aUFBJimZC)
>
> — MATILDA GONZALEZ GIL (@MATIGONZALEZGIL) [December 27, 2019](https://twitter.com/MATIGONZALEZGIL/status/1210580972351246338?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
De acuerdo con el Alcalde Marín , *“Es hora de que Manizales se convierta en una ciudad que este a la vanguardia del momento histórico que está atravesando el país, es el momento de que a esta ciudad lleguen pensamientos globales con un equipo humano consciente, donde podamos proyectarnos como una ciudad moderna en el* futuro", palabras que abrieron el discurso del alacalde Marín antes de mencionar su nueva Secretaria de la Mujer.

### ¿Quién es Matilda Gil?

Gil es profesional en derecho, de la Universidad de los Andes, además tiene un master en Derecho Internacional del American University en Washington. Ha trabajado como becaria en la Comisión Interamericana de Derechos Humanos en la sección de derechos de personas LGBTI, al igual que hizo parte del movimiento Colombia Diversa.

<iframe src="https://www.youtube.com/embed/F_0_LjXlFTw" width="806" height="453" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Asimismo Matilda Gil creo junto con el apoyo de El Espectador, La Prohibida, un canal con un enfoque de opinión y satira política. Es coautora del Informe Mapeo Legal Trans, del International Lesbian, Gay, Bisexual, Trans and Intersex Association (ILGA); y trabajó como consultora de ILGA y de la International, National Civil Liberties Organization (INCLO).

Matilda se suma a los integrantes del movimiento trans que se han vuelto representantes de su comunidad , en categorías como teatro, danza , periodismo y ciencia donde destaca la directora del Instituto Van Humbold, Brigitte Baptiste; a pesar de esto la brecha de desigualdad en ámbitos como el trabajo, la educación y la salud, para las personas trans en Colombia sigue siendo muy alta, teniendo en cuenta que solo el 7,9% puede acceder a la educación superior.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo](http://bit.ly/1nvAO4u)][[Contagio Radio](http://bit.ly/1ICYhVU).]
