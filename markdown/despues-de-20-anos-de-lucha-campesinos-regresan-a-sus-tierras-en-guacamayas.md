Title: Después de 20 años de lucha, campesinos regresan a sus tierras en Guacamayas, Urabá
Date: 2019-05-15 16:26
Author: CtgAdm
Category: Nacional, Paz
Tags: Restitución de tierras, Urabá Antioqueño
Slug: despues-de-20-anos-de-lucha-campesinos-regresan-a-sus-tierras-en-guacamayas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Campesinos-Restitución.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Tierras-4.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Tierras-5.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Tierras-6.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Tierras-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Tierras-2.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Tierras-3.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

Más de 20 años después de ser desplazados, y sus tierras vendidas bajo presión de comisionistas y frentes de las AUC, y 10 de afrontar procesos judiciales para poder recuperarlas, la Corte Suprema de Justicia falló a favor de ocho familias de la **vereda Guacamayas en el Urabá antioqueño**, que hoy regresan a sus fincas como parte del plan de Restitución de Tierras.

Juan Durán Guerra, uno de los beneficiarios de la devolución de predios, indica que transcurrieron entre 10 y 11 años para que este proceso concluyera satisfactoriamente, y con él dos décadas de desplazamiento pues desde 1996 fueron despojados de sus tierras, "todo por causa de lo que nosotros no tenemos la culpa, desafortunadamente nos llevaron por delante", recuerda.

<iframe src="https://co.ivoox.com/es/player_ek_35898639_2_1.html?data=lJqlm52ad5qhhpywj5WZaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncavpwtOYptrWaaSnhqae0JCrucbm08aSlKiPuc_jjMnSjdHTt4zWxtPSyM7HrcLmytTgjcnJb83VjMnS2NSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según el campesino, las propiedades de estas familias abarcan fincas de 40, 50, 60 y hasta 75 hectáreas como es su caso, respecto a la protección efectiva del territorio, con relación a la presencia de actores armados que aún hacen presencia en el lugar, manifestó que a pesar de ser incierto el futuro, **"las cosas se están haciendo a la luz, para que todos lo vean y se den cuenta que tenemos derecho a lo nuestro"**.

### Regresar a sus tierras, una bendición 

Juan Durán afirma que a pesar que en el pasado las hectáreas fueron aprovechadas por otras personas, hoy tienen la oportunidad de tener en sus manos un documento que los hace acreedores de sus tierras, "vamos a trabajar desde hoy en nuestras fincas, hay potrero, hay ganado y vamos a darle hacia adelante", declaró. [(Vea también: Campesinos de Urabá demandan acompañamiento policial en proceso de restitución)](https://archivo.contagioradio.com/campesinos-de-uraba-demandan-acompanamiento-policial-en-proceso-de-restitucion/)

La beneficiaria María Candelaria, también expresó su alegría frente al hecho,  manifestando que veían poco probable el regreso a sus tierras debido a todas las dificultades y el tiempo que tomó el proceso de restitución, **"no tengo claro nada todavía pero lo único que sí sé es que pienso dejarlo como un patrimonio familiar para mis hijos, que fue lo que su papá luchó para dejarles en su despedida del mundo"**.

<iframe src="https://co.ivoox.com/es/player_ek_35898686_2_1.html?data=lJqlm52afJehhpywj5WXaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5ynca7V04qwlYqliMKfpMbbxsrQpdPdwoqfpZDGqc_Zx87Qy8bWrcKfxcqY1MrXuMro1sjWh6iXaaOnz5DS0JCrucLXjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

\

 

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
