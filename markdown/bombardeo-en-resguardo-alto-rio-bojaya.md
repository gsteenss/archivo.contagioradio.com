Title: Bombardeos afectan resguardo Alto Río Bojayá
Date: 2020-09-03 19:03
Author: AdminContagio
Category: DDHH, Nacional
Tags: Autoridades indí, Enfrentamientos grupos armados, Resguardo Alto Río Bojayá
Slug: bombardeo-en-resguardo-alto-rio-bojaya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Boletin-de-la-ONIC-de-pandemia-en-pueblos-indigenas.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Comunicado-de-autoridades-indigenas-de-Bojaya.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En un comunicado compartido por la [Organización Nacional Indígena de Colombia (ONIC) a través de su twitter](https://twitter.com/ONIC_Colombia/status/1300971439940608006), las autoridades indígenas del resguardo Alto Río Bojayá en Bojayá, Chocó alertaron sobre los graves enfrentamientos que se presentaron el 25 de agosto, el 31 de agosto y el 01 de septiembre entre grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las autoridades indígenas informan que hubo enfrentamientos cerca de la comunidad de chano, afectando las fincas de algunos miembros de la comunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Igualmente, aseguran que indiscriminadamente se produjo un bombardeo y se atacó a la comunidad disparándoles.** En esos momentos, la población se encontraba en ese momento realizando sus actividades de recolección de los cultivos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el comunicado, las autoridades **“denunciamos y rechazamos a la fuerza pública del Estado por el enfrentamiento que se viene presentado en el resguardo”. **(Le puede interesar: [Franklin Velásquez, líder del Putumayo es asesinado](https://archivo.contagioradio.com/franklin-velasquez-lider-del-putumayo-es-asesinado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las autoridades del resguardo afirman que los continuos enfrentamientos entre los grupos armados ponen a toda la comunidad del Alto Río Bojayá  en una situación crítica en que deben afrontar el confinamiento, el asesinato de líderes sociales y el desabastecimiento de las fuentes de alimentación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para que estos sucesos no sigan ocurriendo en el territorio, la comunidad le hace un llamado urgente a las instituciones tanto locales como nacionales e internacionales para que intervengan, con el objetivo de “garantizar los derechos fundamentales como la paz, salud" y la seguridad de los pobladores. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, la comunidad espera que se deje de poner en riesgo la existencia y permanencia de los distintos grupos indígenas que han sido víctimas de los conflictos armados en el territorios.

<!-- /wp:paragraph -->

<!-- wp:image {"id":89256,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Comunicado-de-autoridades-indigenas-de-Bojaya.jpg){.wp-image-89256}  

<figcaption>
ONIC

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->
