Title: Más de 300.000 personas son víctimas de trata de personas en Colombia
Date: 2017-07-29 10:00
Category: DDHH, Hablemos alguito
Tags: explotación sexual, Paramilitarismo, Trata de personas
Slug: mas-300-000-personas-victimas-trata-personas-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/trata-de-personas-e1469852928477.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: infobaires 

###### [26 Jul 2017] 

Este **30 de julio se conmemora el Día Mundial Contra la Trata de Personas**, un flagelo que ocupa el segundo lugar de negocios ilegales más lucrativos en el mundo y del cual, en Colombia cerca 300 mil personas son víctimas de acuerdo con datos de Walk Free Foundation.

Se trata de una problemática que se presenta en prácticamente todas las regiones del país, debido a causas estructurales como la pobreza y la desigualdad, como lo afirma como lo asegura Claudia Quintero, vocera de la Corporación Anne Frank, quien explica que este tipo de delito ocurre bajo la tolerancia de las instituciones.

El tipo de delito más recurrente de la trata suele ser la explotación sexual, en cuyo caso las principales víctimas son niñas, niños, adolescentes y mujeres adultas. Principalmente porque **"somos un país que naturaliza hasta la explotación sexual infantil**", expresa la vocera de la Corporación Anne Frank. De acuerdo con ella los niños, niñas, y adolescentes son el especial punto de atención para estas mafias, que ahora usan internet como herramienta para captar víctimas, en cerca de dos mil portales donde se expone este tipo de material que atenta contra la dignidad de las personas.

### Migración y paramilitarismo 

La masiva migración de venezolanos hacia Colombia y la vigente presencia del paramilitarismo han profundizado esta forma de vulneración de los derechos humanos de una persona. En el primer caso, la situación actual que vive el país vecino está impulsado que las mafias tengan nuevas víctimas que solo buscan una oportunidad laboral, y que **luego temen denunciar por temor a ser deportadas o deportados.**

Por otro lado, la trata de personas se ha vuelto un negocio más rentable que el narcotráfico, lo que generó que tras la implementación de la Ley de Justicia y Paz, con la que se esperaba la desmovilización de las estructuras paramilitares, ahora los **grupos emergentes de estas bandas se estén financiando de la trata de migrantes y víctimas del conflicto armado.**

### ¿Qué hacer? 

Medellín, Bogotá, Valle, Ipiales, incluso en la región fronteriza del Amazonas se evidencian casos de trata de personas, usualmente con fines de explotación sexual, "**No hay sitio de Colombia en el que no haya casos de trata de personas**", afirma Quintero, y agrega que  las alcaldías y gobernaciones deben implementar la normativa existente en contra de la trata de personas, pues actualmente son pocas las capturas y muchos menos son las condenas.

Aunque desde la Corporación Anne Frank no se niegan algunos esfuerzos importantes por parte del gobierno para frenar este flagelo, esto no ha sido suficiente y debe ponerse mayor atención a este delito cuyas víctimas han sido invisibilizadas e incluso revitimizadas por el mismo estado, la los medios de comunicación y la sociedad en general.

“Estamos hablando del grupo humano más asesinado en el mundo. **Lo que se debe hacer es perseguir al que compra sexo y al que se lucra, no  las víctimas”**, manifiesta la activista quien agrega, “Colombia ha retrocedido, pues se revictimiza por un modelo reglamentarista que vulnera especialmente a las mujeres y además les cobra impuestos”, indica Quintero, a quien en carne propia han amenazado tildándola de prostituta y guerrillera.

<iframe id="audio_20018232" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20018232_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
