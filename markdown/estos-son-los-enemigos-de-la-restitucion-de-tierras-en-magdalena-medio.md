Title: Los enemigos de la Restitución de Tierras
Date: 2016-04-11 21:26
Category: Nacional
Tags: conflicto armado, Paramilitarismo, restitución de tierra
Slug: estos-son-los-enemigos-de-la-restitucion-de-tierras-en-magdalena-medio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Restitución-de-Tierras-e1510251256744.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  [Colectivo Agrario Abya Yala]

###### [11 Abr 2016]

Según el último informe publicado por la Fundación Forjando Futuros y la Universidad de Antioquia, sobre la tenencia y restitución de tierras en Colombia, existen cinco personas y once empresas que se oponen a que se restituyan las tierras a las víctimas del despojo y el conflicto armado.

Este grupo de personas y empresas se han hecho acreedoras de grandes extensiones de tierras en diferentes zonas del país durante los años y su mayor argumento para oponerse a la restitución de tierras, es acudir al principio de tenencia de buena fe, sin embargo, ninguno de los tenedores de tierras ha probado la buena fe en los procesos judiciales.

Las cinco personas poseen un 46% del total de las oposiciones en los procesos judiciales y representa 334 predios, las personas son:

**Gabriela Inés Henao Montoya:** Adquirió 94 parcelas que suman 500 hectáreas de la Hacienda Santa Paula, ubicada en el corregimiento de Leticia en Montería. Henao fue capturada en el año 2014 por el presunto delito de desplazamiento forzado, además se le relaciona con el asesinato de Yolanda Izquierdo, líder de restitución de tierras asesinada en el año 2013 quién le vendió sus tierras a Henao.

Esta mujer está actualmente investigada por testaferro, y las tierras pertenecientes a la Hacienda Santa Paula están en proceso de restitución con 117 solicitudes.

**Hever Walter Alfonso Vicuña**: Es propietario de la Hacienda el Paraíso, conocida también como la Hacienda Santa Paula, ubicada en la vereda de Leticia en Montería, esta propiedad hizo parte de los terrenos de los hermanos Castaño hacía el año 1990 que posteriormente se entregaron a FUNPAZCOR, para que fuesen repartidos a las víctimas de violencia en la zona. Entre los años 1996 y 2006, los campesinos que estaban en estas tierras fueron nuevamente hostigados y desplazados por el paramilitarismo. A su vez la Fundación FUNPAZCOR bajo la dirección de Sor Teresa Castaño ofreció a los campesinos la suma de un millón de pesos para adquirir de nuevo las parcelas, oferta a la que muchos accedieron bajo amenazas.

Hever Walter dice que adquirió las tierras después de una compraventa entre el señor Ángel Horacio Cardona y Leónidas Quirino Berrocal y que desconocía la procedencia de los terrenos, sin embargo, no se ha comprobado el principio de buena fe. Estas tierras deben ser restituidas a Leónidas Quirino quien fue desplazado y obligado con amenazas a vender su parcela a FUNPAZCOR.

**Gabriel Jaime Vasquez Quintero:** Dueño de la Hacienda Cedro Cocido ubicada en la vereda El Tronco, corregimiento de Leticia, en Montería. Esta propiedad hizo parte de la familia Castaño hasta el año 1990, año en donde se donan estas tierras a FUNPAZCOR, para ser entregada a las víctimas de la violencia en Córdoba. Posteriormente entre los años 1996 al 2006, los campesinos beneficiarios de estas tierras, son desplazados y amenazados por el paramilitarismo para que salgan de las tierras. FUNPAZCOR les ofrece la suma de un millón de pesos por sus parcelas.

Gabriel Jaime Vasquez Quintero dice que es un comprador tercero de la Hacienda Cedro Cocido en el año 2006 y que desconocía la procedencia de venta de la tierra. No se ha comprobado el principio de buena fe en este proceso, de igual forma estas tierras hacen parte del proceso de restitución a más de 10 familias campesinas.

**Gerardo Escobar Correa**: Posee tierras del predio Jaraguay y 300 hectáreas del predio Santa Rosa. El predio Jaraguay pertenecía a la Milagrosa S.A.C, esta asociación vendió los previos a diferentes personas como Gloria Amalia Grisales Álvarez, Raúl José Muentes Ballesteros, Diego Ferney Ortíz Grajales y Jenny María Padilla Ramos. Estos a su vez le vendieron a Gerardo Escobar Correa quien hoy aún posee estas tierras.

Los predios 10 y 7 de Jaraguay están actualmente en proceso de restitución.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
