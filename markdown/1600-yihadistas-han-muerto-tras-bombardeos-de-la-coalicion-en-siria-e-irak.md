Title: 1600 yihadistas han muerto tras bombardeos de la coalición en Siria e Irak
Date: 2015-02-23 19:15
Author: CtgAdm
Category: El mundo, Política
Tags: Coalición EEUU Siria, Observatorio sirio para los derechos humanos, Siria, yihadistas
Slug: 1600-yihadistas-han-muerto-tras-bombardeos-de-la-coalicion-en-siria-e-irak
Status: published

###### Foto:Eldinamo.cl 

Según el Observatorio de Derechos Humanos de Siria, en cinco meses de bombardeos der la **coalición liderada por EEUU** han muerto** 1.600 personas, la mayoría yihadistas** pertenecientes al EI, y a otras milicias de carácter similar.

En total han sido **1.465 los muertos yihadistas, principalmente extranjeros**, el resto provienen  de las milicias de  Abu Bakr al Bagdadi, y otros 73 son yihadistas del Frente Al Nusra, brazo de Al Qaeda en Siria.

Los bombardeos de la coalición entre EEUU y otros países árabes comenzaron el **23 de Septiembre de 2014**, apuntando hacia objetivos del EI y del ISIS en Irak, la frontera de Siria con Turquía, así como otras regiones de Siria, que además coinciden con ser las zonas donde comenzó el levantamiento contra el régimen de Bashar Asad.

Los **objetivos principales** han sido frenar el financiamiento de los grupos islámicos radicales, destruyendo infraestructuras y **campos de petróleo y gas en sus manos**. También han conseguido **frenar su avance en la ciudad de Kobane**, fronteriza con Turquía.

Aunque los ataques han tenido un saldo total de **62 civiles muertos, un saldo en vidas humanas muy alto que ha sido fuertemente criticado por organizaciones sociales de derechos humanos,** en los bombardeos también han sido afectados milicianos de otros grupos no islamistas que se alzaron contra el régimen sirio y que también han combatido a los yihadistas.

Además del descontento en las regiones bombardeadas hacia Occidente y hacia la coalición, la situación de violación de los derechos humanos hace pensar a distintos analistas políticos que ahora se renueve el reclutamiento de los grupos ortodoxos.

Por último **EEUU ha realizado un acuerdo con Turquía** para armar y entrenar **5.000 milicianos con el objetivo de defender la frontera con Siria** e impedir un rearme del EI.
