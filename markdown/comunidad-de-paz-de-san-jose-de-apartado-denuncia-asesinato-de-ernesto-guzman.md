Title: Comunidad de Paz de San José de Apartadó denuncia asesinato de Ernesto Guzman
Date: 2015-09-25 15:43
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Asesinato de campesino en San José de Apartadó, Comunidad de Paz de San José de Apartadó, Paramilitarismo en San José de Apartadó, Polícias desconocen sentencias constitucionales, Radio de derechos humanos
Slug: comunidad-de-paz-de-san-jose-de-apartado-denuncia-asesinato-de-ernesto-guzman
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/San-José.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: www.hrw.org 

###### [25 Sept 2015]

El **lunes 21 de septiembre**, fue **asesinado por paramilitares** el campesino **Ernesto Guzmán**, padre de 5 hijos y habitante de la vereda Playa Larga del Corregimiento de San José. Según campesinos de la zona los paramilitares “Le propinaron varios impactos de bala” denuncia la Comunidad de Paz de San José de Apartadó.

Este mismo día se registró la presencia de **2 hombres vestidos con traje oscuro que intentaron ingresar a la vivienda de un miembro del Consejo de San Josecito**, tras no lograr abrir la puerta y escuchar ruido “Los sujetos emprendieron la huida”.

[Una de las cosas que preocupa a la comunidad es que **el Comandante de la Brigada XVII del Ejército Nacional**, Coronel German Rojas Días, luego de un evento público a las afueras de la Alcaldía Municipal de Apartadó, **manifestó el interés absoluto porque sean derogadas las medidas cautelares y provisionales** de la CIDH a favor de la Comunidad de Paz.]

[Los pobladores manifiestan que no es la primera vez que atienden comentarios de este talante  por parte de funcionarios del gobierno colombiano, pues han sido objeto de “la persecución enconada y malévola” y de “los niveles de **estigmatización, chantaje y persecución que ha tejido dicho coronel contra nuestro proyecto de vida**”.]

A continuación el comunicado completo

##### Una ves mas nuestra Comunidad de Paz, acude a la humanidad y a la historia, para dejar constancia de nuevos hechos de muerte, persecución y dolor al que es sometida nuestra comunidad y la población de nuestro entorno geográfico y social.

-   ##### El **<u>miércoles 5 agosto de 2015,</u>**en horas de la mañana el Comandante de la Brigada XVII del Ejercito Nacional, Coronel German Rojas Días, a través de un evento público en las afueras de la Alcaldía Municipal de Apartadó, manifestó el interés absoluto de que se quiten las medidas cautelares y provisionales de la Corte Interamericana de Derechos Humanos a favor de la Comunidad de Paz. No es la primera ves que un funcionario del Gobierno Colombiano expresa dicho anhelo, ni es extraño escuchar de dicho Coronel semejantes atrevimientos ambiciosos cargados de inclemencia contra nuestro proceso de vida de la Comunidad de Paz. No basta la persecución enconada y malévola, ni los niveles de estigmatización, chantaje y persecución que ha tejido dicho coronel contra nuestra proyecto de vida. Entendemos claramente esas, las razones y propósitos que recaen sobre dicho funcionario para mantenerlo en esa guarnición militar, con el objeto de destruirnos a como de lugar, incluyendo el que se levanten las medidas de protección de la Corte Interamericana de Derechos Humanos en relación a la Comunidad de Paz.

-   ##### El **<u>miércoles 26 de agosto de 2015</u>**, mientras tres miembros de nuestra Comunidad de Paz se trasladaban desde el casco urbano de Apartadó, hacia el asentamiento de la Comunidad en San Josecito, fueron abordados por agentes de la Policía Nacional que se movilizaban en motocicleta, quienes sometieron a los miembros de nuestra comunidad a chantaje, luego procedieron a anotar en una libreta las placas del vehículo en el que se trasladaban. En forma contundente se evidencia el método de pillaje y persecución que se vuelve a implementar contra los miembros de nuestra Comunidad de Paz.

-   ##### El **<u>jueves 10 de septiembre de 2015</u>**, hacia las 11:00 horas, dos miembros de nuestra Comunidad de Paz que se trasladaban desde San Josecito hacia el casco urbano de Apartadó, fueron abordados por agentes de la Policía Nacional, a la altura del barrio Alfonso López, en Apartadó. Allí, fueron empadronados y fotografiados por agentes de la Policía,  argumentando que ellos hacían lo que se les daba la gana, que ellos eran la autoridad y que estaban por encima de cualquier sentencia de la Corte Constitucional, en total desacato a la sentencia C-1024 del 2002 la cual prohíbe a la fuerza pública los empadronamientos, y dice; **´´*<u>esta Corte declaró contrario a la Constitución el registro de la población que en ella se autorizaba adelantar en los llamados “Teatros de operaciones militares”, pues esa clase de empadronamientos no se encuentra autorizada ni en estados de normalidad ni en estados de excepción</u>***. \`\`

-   ##### El **<u>viernes 11 de septiembre de 2015</u>**, varios campesinos procedentes de la vereda Mulatos del Corregimiento de San José, los cuales se encontraban en la vereda Playa Larga del mismo Corregimiento, fueron abordados por varios paramilitares vestidos con prendas militares y portando armas largas, los cuales afirmaron que los habitantes de la vereda Mulatos y Resbalosa eran guerrilleros y milicianos de las FARC, que no se les permitía volver a pasar por allí, según ellos, estaban haciendo inteligencia para la guerrilla.

-   ##### El **<u>lunes 21 de septiembre de 2015</u>**, hacia las 12:00 horas fue asesinado el campesino ERNESTO GUZMAN, padre de 5 hijos y habitante de la vereda Playa Larga del Corregimiento de San José, según campesinos de la zona, la muerte de da a manos de paramilitares quienes le propinaron varios impactos de bala, ante un Estado cómplice y asesino una nueva muerte, se atenta contra una vida.

-   ##### Este mismo **<u>lunes 21 de septiembre de 2015</u>**, hacia las 3:00 horas, en el asentamiento de San Josecito, dos hombres vestidos de traje oscuro, intentaron ingresar a la vivienda de un miembro de Consejo Interno, al no lograr abrir la puerta y al escuchar el ruido de los ocupantes de la vivienda, los sujetos emprendieron la huida.

##### Rechazamos terminantemente toda acción de muerte y destrucción de la vida, e imploramos a que se respete lo mas digno de un humano, la VIDA, y a que cese el derramamiento de sangre.

##### El dolor no nos hará retroceder jamás, nos dará fuerza y moral para seguir construyendo día a día un mundo incluyente, justo y digno.

##### 

##### Comunidad de Paz de San José de Apartadó

##### Septiembre 22 de 2015
