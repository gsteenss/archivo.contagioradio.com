Title: Frustran atentado contra Piedad Córdoba, lideresa de Marcha Patriótica
Date: 2016-04-01 17:09
Category: Nacional, Paz
Tags: marcha patriotica, piedad cordoba
Slug: frustrado-atentado-contra-piedad-cordoba-lideresa-de-marcha-patriotica
Status: published

###### Foto: Salvatore Di Nolfi 

###### [1 Abr 2016] 

Piedad Córdoba, líder del movimiento social y político Marcha Patriótica, denunció que un hombre habría intentado dispararle  mientras se encontraba en el municipio de Quibdó. Según información de sus familiares, la defensora de derechos humanos se encontraba a la salida de un hotel de Quibdó, cuando vio el  movimiento de los sicarios en medio de población, la exsenadora alcanzó a reaccionar junto a su esquema de seguridad.

“Infortunadamente trataron de dispararme aunque los escoltas y yo reaccionamos y logramos salirnos de esto. La ciudad está sitiada. Afortunadamente no pasó nada y fue un momento muy difícil. Él salió detrás de un poste, hace una seña y yo lo vi. Se vinieron los de unas motos, que me imagino que eran apoyo de él. Se formó un despelote y salimos corriendo hasta que llegamos a la esquina, hasta cuando llegó la Policía. Realmente no sé por qué no pasó nada. Creo que a él lo capturaron”, relató Piedad Córdoba a la emisora *Q Radio* de Chocó.

La líder había tenido una reunión con el Ministro del Interior, Juan Fernando Cristo, donde denunció que se está configurando un genocidio contra el movimiento Marcha Patriótica, señalando que de 2013 a 2015 han asesinado 115 integrantes del movimiento.

"Podemos decir que se está configurando un genocidio contra la Marcha Patriótica y nosotros no queremos que se repita lo de la Unión Patriótica ni que sigan ocurriendo estas cosas”, sostuvo Córdoba.

Cabe recodar, que el pasado 18 de marzo con un acto simbólico y de manera pacífica, 150 mujeres integrantes del movimiento social y político Marcha Patriótica, se tomaron la iglesia de San Francisco en Bogotá para denunciar la actual situación en materia de derechos humanos del país, la persecución y estigmatización contra las organizaciones sociales y populares, y particularmente el genocidio contra este movimiento, que cuenta con 115 de sus dirigentes asesinados.

Según versiones del ministro de Defensa, Luis Carlos Villegas, “la versión que hasta el momento tiene la Policía es que al salir de esa charla, ella y su escolta cruzaron para ir al hotel. En ese momento se produjo una alarma de un artefacto explosivo en la zona, alarma que resultó falsa, y que hizo que ciudadanos corrieran en dirección de la exsenadora”.

Por otra parte, el **mayor Joaquín Guillermo Domínguez**, comandante de Policía de Quibdó, desmintió que se tratara de un atentado en contra de la defensora de derechos humanos, "**Piedad Córdoba está bien; no fue ningún atentado contra ella.** Ella está haciendo unas conferencias en el auditorio. Nada ocurrió”. Mientras que el coronel Luis León aseguró que “es desinformación; ella está aquí en el comando de nosotros en una reunión y no ha habido ningún inconveniente. El hotel donde ella se está quedando queda al frente del comando y al frente queda la Sijín y ellos son personas de civil, **vieron a 10 con armas cortas y se creó un mal entendido,** pero son hombres de la Policía Nacional”.

https://soundcloud.com/oderiudadano/mensaje-de-piedad-cordoba
