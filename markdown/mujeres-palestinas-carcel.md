Title: La dureza de la cárcel para las mujeres palestinas.
Date: 2017-11-17 12:18
Category: Onda Palestina
Tags: Apartheid Israel, BDS, Palestina
Slug: mujeres-palestinas-carcel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/pales-ta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: MEE/Anne Paq and Ahmad Al-Bazz 

###### 14 Nov 2017 

Nuevamente el Comité de Presos y Prisioneros Liberados de Palestina denunció las difíciles condiciones que deben pasar las personas detenidas ilegalmente en las cárceles israelíes. Según su portavoz Hanan Al-Khatib las mujeres palestinas son sometidas a situaciones de aislamiento total, privando toda posibilidad de ser visitadas por sus familiares o amigas, siendo humilladas y provocadas desde que son trasladadas por primera vez a la prisión, estando expuestas a insultos, maltratos psicológicos y condiciones dolorosas que afectan su salud.

Mujeres como Nisreen Hassan Abdullah de Gaza han tenido que pasar por este tipo de abusos. En su caso, desde el momento de su detención en el 2015 no ha podido ver a ninguno de sus siete hijos o miembro de su familia. También ha estado expuesta a humillaciones durante los tratamientos médicos y hacinamiento. Los servicios penitenciarios israelíes expresaron de manera indolente ante denuncias como estas que “las afectadas están sufriendo intencionalmente”.

En esta emisión en Onda Palestina trataremos temáticas variadas, donde hablaremos en detalle sobre una alternativa de conciliación desde el deporte, la fuerte lucha de Israel contra Boicot, y nuestra entrevista de la semana estará acompañada de la documentalista catalana Laura Arau quien nos estará hablando de la iniciativa de la Flotilla de la Libertad, el drama de la doble diáspora palestina en el Líbano y los vínculos entre Israel y Cataluña armamento y entrenamiento militar. Y para finalizar en nuestra sección cultural estaremos hablando del rico y delicioso Knafeh Bakery.

<iframe id="audio_22136693" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22136693_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
