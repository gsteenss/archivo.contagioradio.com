Title: Habrá paro general en Guatemala para exigir cárcel para presidente Pérez Molina
Date: 2015-08-24 12:27
Category: El mundo, Entrevistas, Movilización
Tags: Andrea Ixchiu, Centro América, CICIG, corrupción, Guatemala, Iván Velásquez, La Línea, Movilizaciones, Otto Pérez Molina, Prensa Comunitaria
Slug: otto-molina-no-renuncia-pese-a-relaciones-con-mafias-de-guatemala
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/PROTESTA-GUATE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: monitoreodemedios.gt 

<iframe src="http://www.ivoox.com/player_ek_7519327_2_1.html?data=mJqem5iWe46ZmKiakpyJd6KmmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjMLW04qwlYqldYzkwtfcjczJssbmwtGYx9OPi9bV1craw9HFb9HV08aYx93Nq8rmjMiSpZiJhZLmxMrZjdXFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Andrea Ixchiu de la organización Prensa comunitaria en Guatemala] 

###### [24 Ago 2015] 

Tras 14 semanas de movilización y luego de la detención de 21 funcionarios del gobierno de Otto Pérez,  ciudad de Guatemala y las principales ciudades del país son testigos de nuevas marchas y movilizaciones por parte de miles de personas que exigen la renuncia del presidente y su enjuiciamiento por las acusaciones por corrupción. A partir de mañana 25 de Agosto está citado el paro general en el país centroamericano.

Andrea Ixchiu de la organización Prensa comunitaria en Guatemala, dijo que en la noche de este domingo el **Presidente Otto Pérez indicó que no va a renunciar a pesar de las exigencias sociales, declarando que hay “una estrategia de invasión y control de la elite de Norteamérica en su contra”,** además negó sus vínculos con las estructuras mafiosas del país lo que provocó la indignación ciudadana.

Ixchiu señala que existe una preocupación por el discurso de Otto Pérez porque busca la confrontación entre distintos sectores, **buscando evadir su responsabilidad** de los señalamientos que se le han hecho a él y a su gobierno, además que es un discurso cercano al uno de los candidatos que aparece mejor posicionado para las elecciones de este 6 de septiembre, brindando un escenario de impunidad frente a la corrupción.

Estos anuncios de Otto Pérez hacen que las movilizaciones continúen en Guatemala, Andrea Itxu comenta que **“para hoy se han convocado movilizaciones en las principales ciudades de Guatemala y desde mañana se ha convocado a una huelga general y paro nacional, por parte diversos sectores, principalmente de las organizaciones indígenas, campesinas y estudiantiles”**.

La indignación de la sociedad Guatemalteca ha hecho que distintos sectores se estén organizando para generar una plataforma con aproximadamente 130 organizaciones civiles, en donde también está la participación de algunos grupos económicos “en donde se tratan de hacer concertaciones y convergencias respecto de las acciones a tomar en toda esta coyuntura y crisis políticas” indica Itxu.

### [**El proceso judicial**] 

Hasta ahora el Congreso y Corte Constitucional de Guatemala han protegido el gobierno de Otto Pérez, en este caso ante la negativa este se pone a disposición de estos entes. Otra posibilidad es negociar no ir a la cárcel, sin embargo esto implica que salga del país y esto solo lo posibilita un autogolpe o golpe consensuado de Estado. La Corte Suprema levantó la inmunidad a Pérez Molina luego del avance de las investigaciones denunciadas por CICIG.

El 19 de agosto se realizó el coloquio "El costo social de la corrupción" en donde el Director Ejecutivo del Instituto Centroamericano de Estudios Fiscales (ICEFI), Jonathan Menkos Zeissig presentó el informe de investigación de Oxfam "La Corrupción: sus caminos, su impacto en la sociedad y una agenda para su eliminación".

Este informe r**eveló nexos entre distintos miembros del gabinete presidencial y del mismo Pérez Molina con distintas organizaciones mafiosas como La Línea**, del país centroamericano.

El pasado viernes 21 de agosto detuvieron a la ex Presidenta Rosanna Baldetti, quien dimitió del cargo en el mes de mayo, también por vínculos con esta mafia, por el cual también hay 21 personas detenidas. Ver también: [Detenida ex vice presidenta de Guatemala Rosanna Baldetti](https://archivo.contagioradio.com/por-escandalos-de-corrupcion-detienen-a-ex-vice-presidenta-de-guatemala-roxanna-baldetti/).

 
