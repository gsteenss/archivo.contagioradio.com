Title: Financiación para el desarrollo sostenible: ¿esta vez será posible?
Date: 2015-07-06 15:24
Category: Ambiente y Sociedad, Opinion
Tags: Ambiente y Sociedad, cambio climatico, Consenso de Monterrey, COP 21, Margarita Flórez, Tercera Conferencia Internacional sobre Financiamiento para el Desarrollo
Slug: financiacion-para-el-desarrollo-sostenible-esta-vez-sera-posible
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/shutterstock_125889539.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: shutterstock 

#### [Por] [[Margarita Flórez](https://archivo.contagioradio.com/margarita-florez/) - Asociación Ambiente y Sociedad] 

###### [6 Jun 2015] 

En la próxima semana se instala la Tercera Conferencia Internacional sobre Financiamiento para el Desarrollo, la cual fue precedida por la Conferencia Internacional de Monterrey, México en 2002, y la Conferencia Internacional de Seguimiento sobre la Financiación para el Desarrollo encargada de Examinar la Aplicación del Consenso de Monterrey celebrada en Doha en el 2008.

 La decisión fue adoptada mediante la Resolución A/RES/68/204 del 21 de enero de 2014, Sexagésimo octavo período de sesiones de Naciones Unidas (NNUU) por la Asamblea General de Naciones Unidas (NNUU). Después de hacer un recuento de los antecedentes, y la necesidad de enfrentar el desafío de avanzar en lograr el desembolso de la ayuda para el desarrollo que constituyen buena parte la financiación de los Objetivos de Desarrollo Sostenibles (ODS), la Asamblea General de NNUU en esta resolución 68, en el Tema 18 determinó la realización de la Tercera Conferencia para la Financiación del Desarrollo que se celebrará entre el 13, y el16 de julio de 2015 en Addis Abeba, Etiopía. La  Tercera Conferencia debe establecer el avance de la aplicación del Consenso de Monterrey (2002), y de la Declaración de Doha (2008). La crisis económica puso de relieve que las obligaciones de los países desarrollado hasta ahora incumplidas, se hacen menos viables en el nuevo escenario podrían, e incluso disminuirse, lo cual entrabará sin duda la aplicabilidad de otro de los esfuerzos de NNU cuál es la adopción de los Objetivos de Desarrollo Sostenible (ODS.

Pero más allá de este hito institucional del sistema de Naciones Unidas lo que se trata es de definir la interacción entre los diversos roles que juega una cooperación al desarrollo que cuenta actualmente con múltiples fuentes, que está agenciada por numerosos actores, que de manera directa a través de los gobiernos dispone de fondos para la cooperación cada vez más ligados a la entrada de las empresas nacionales del país donante; de la cooperación de los bancos de desarrollo, de las instituciones regionales, de la filantropía empresarial, de ONG así como un concepto ampliado de de desarrollo que aborda los aspectos de inequidad creciente; el medio ambiente, el cambio climático, la propuesta de acciones frente a la evasión fiscal, en fin un agenda y un conjunto de actores que reflejan la complejidad del tema.

Desde el año pasado ONG de todo el mundo, entre ellas Ambiente y Sociedad, han dirigido documentos de posición a NNU acerca de lo elementos que deben ser tenidos en cuenta si es que de veras lo que se pretende es avanzar en una coordinación que haga efectiva en cantidad y calidad la cooperación para el desarrollo\[1\]. Primero que todo, se aboga por que haya transparencia y rendición de cuentas lo cual implica un adecuado acceso a la información y lograr que haya mecanismos independientes de rendición de cuentas con la inclusión de todos los actores sociales incluidos en las acciones propuestas. Esta rendición de cuentas deberá incluir actores gubernamentales, empresariales, no gubernamentales que gestionen fondos. Y se estrecha la ligazón entre desarrollo, y el respeto por el medio ambiente mediante el fortalecimiento de salvaguardias sociales y ambientales. El principal mecanismo diseñado para el cambio climático, el Fondo Verde está lejos de recibir los fondos prometidos.

¿Qué tanto se puede lograr? ¿Cuáles son las agendas no visibles? ¿Qué es lo que se quiere? ¿Y qué es lo que se logra? Trataremos de darle seguimiento al tema que consideramos da inicio a un movido segundo semestre del 2015, que continúa con la Conferencia sobre Objetivo de Desarrollo Sostenible en septiembre y con la COP 21 en diciembre evento que decidirá si es que somos capaces de avanzar en objetivos comunes con mecanismos comunes.

###### [\[1\]Las negociaciones sobre financiamiento para el desarrollo de Naciones Unidas: ¿sobre qué resultados se debería acordar en Adís Abeba en 2015? Este documento fue iniciado por Afrodad, Eurodad, Jubilee South Asia Pacific Movement on Debt and Development1, Latindadd y Third World Network, y está respaldado por 137 Organizaciones de la Sociedad Civil. <http://www.un.org/esa/ffd/wp-content/uploads/2014/12/gc-outcomes-Addis-CSO-Sp.pdf>​] 
