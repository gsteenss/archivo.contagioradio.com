Title: Ex ministros de Alvaro Uribe condenados por escándalo de la "Yidispolítica"
Date: 2015-04-15 16:57
Author: CtgAdm
Category: Judicial, Nacional
Tags: Alberto Velásquez Echeverry, Alvaro Uribe, Cohecho, Corte Suprema de Justicia, Diego Palacio, Sabas Pretelt, Yidispolítica
Slug: ex-ministros-de-alvaro-uribe-condenados-por-escandalo-de-la-yidispolitica
Status: published

###### Foto: semana.com 

La decisión de la **Corte Suprema de Justicia** cobija a los ex ministros **Sabas Pretelt y Diego Palacio**, quienes fueron condenados a 80 meses de prisión, y una multa de 167 salarios mínimos legales mensuales vigentes así como 112 meses de inhabilidad para el ejercicio de funciones públicas. El exsecretario de la Presidencia, **Alberto Velásquez Echeverry**, fue sentenciado a 60 meses de prisión, multa de 83 salarios mínimos y a una inhabilidad de 84 meses.

Según la decisión que se publicó la tarde de este miércoles, los tres ex funcionarios del gobierno del ex presidente **Álvaro Uribe** actuaron conjuntamente para garantizar la aprobación del trámite legislativo en el Congreso con el que se logró la reelección inmediata en 2004.

De acuerdo al fallo los ex ministros hicieron ofrecimientos de cargos a los entonces parlamentarios **Yidis Medina y Teodolindo Avendaño**, con lo cual incurrieron en el delito de cohecho por dar u ofrecer.

Esta decisión se da luego de 7 años en que la congresista **Yidis Medina** fuera condenada por el delito de cohecho para votar favorablemente en el proyecto de acto legislativo que permitió la reelección inmediata de Álvaro Uribe.

Cabe señalar que la sentencia de la Corte Suprema afirma que sin esas actuaciones por parte de los funcionarios del gobierno condenados, la situación del país cambió drásticamente y si esos delitos no hubiesen sido cometidos serían otras las circunstancias en las que las instituciones tanto de la justicia como de los otros estamentos del Estado se encuentran en crisis y con una considerable pérdida de credibilidad y respeto por parte de la ciudadanía.
