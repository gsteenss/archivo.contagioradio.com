Title: Un sentimiento musical llamado Bolero
Date: 2016-03-24 12:16
Category: Cultura, En clave de son
Tags: Bolero género musical, historia del bolero, máximos representantes del bolero
Slug: un-sentimiento-musical-llamado-bolero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/un-bolero-para-ti.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Ilustración: Miguel Ángel Arzuaga. Foto Abel Rojas 

##### [11 Mar 2016] 

Con mucha facilidad, un bolero pasa del oido al corazón, un ritmo capaz de despertar los sentimientos de quien lo escucha. Si bien muchos de sus temas le cantan al amor y por supuesto al desamor, también lo hacen a la nostalgia de un tiempo mejor, de una tierra natal lejana y hasta de un amor filial, acompañados por una estructura musical diseñada para armonizar y complementar la lírica.

Durante la segunda mitad de la decada de los 60, se redujo notablemente la popularidad del bolero como género, en parte por el nacimiento y crecimiento de uno  nuevo y arrasador: La salsa; una baja que siendo sensible, no representaría el fin del bolero y su importancia al interior de la musica latinoamericana, por el contrario, la llama que tuvo durante los años 40, 50 y principios de los 60, volvería a encenderse.

Parte de la responsabilidad en el resurgimiento del bolero, llegaría de la mano de artistas salseros, cuyas interpretaciones significarían un reencuentro con este género dándole una segunda vida a muchas buenas canciones que se encontraban el olvido, en sus versiones originales o con adaptaciones a diferentes estilos musicales y formaciones como la balada y los trios.

La importancia del bolero desde sus inicios y la influencia prolongada hasta nuestros tiempos, motivan la realización de este programa especial, bajando en este episodio sus revoluciones salseras, para darle paso a una selección boleros inmortales que nos llegan al corazón.

<iframe src="http://www.ivoox.com/player_ek_9671188_2_1.html?data=mpukk5acfI6ZmKiakpeJd6KlkYqgo5accYarpJKfj4qbh46kjoqkpZKUcYarpJLC0JDXqc%2FoytLWx9PYs4zh1tjWxcbQb83gwtLOxtSPhtDgxtfcj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [En clave de son, Bolero] 
