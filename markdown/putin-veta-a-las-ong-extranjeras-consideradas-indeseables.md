Title: Putin veta a las ONG extranjeras consideradas  "indeseables"
Date: 2015-05-25 17:22
Category: El mundo, Otra Mirada
Tags: Putin saca ley para vetar ONG's "indeseables", Putin veta ONG's, Violación DDHH en Rusia
Slug: putin-veta-a-las-ong-extranjeras-consideradas-indeseables
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/imrs.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Europauniversal.blogspot.com 

**Rusia** emite una **ley que permite vetar a ONG's consideradas por el gobierno como "indeseables"**. La medida ha recibido fuertes críticas de las oposición y de la comunidad internacionales, así como de ONG's y organizaciones sociales de DDHH.

 La ley permitiría **vetarlas tipificandolas por su trabajo como “...amenaza para la capacidad de defensa o de seguridad de Rusia o que afecten al orden público o la salud pública...**". La ley permitiría a las fiscalías del país realizar una lectura subjetiva sobre su trabajo permitiendo en un futuro vetar todo lo que no convenga al propio gobierno.

Las **sanciones alcanzarían los 10.000 euros y podrían conllevar hasta seis años de prisión** para quién colabore con ellas, a parte de la expulsión de los miembros extranjeros que pertenezcan a la organización.

La responsabilidad de decidir si las actuaciones de las Organizaciones No Gubernamentales atenta contra el propio estado o alteran el orden público, recae sobre la **fiscalía y la vicefiscalía, las cuales, denuncian las organizaciones sociales de DDHH, están en manos del gobierno y son usadas para su conveniencia**.

**Personajes de la vida** pública Rusa como **Gary Kasparov**, líder de la oposición y conocido ajedrecista, ha criticado la medida como parte de la represión ejercida por **Putin contra ONG's que trabajan en temática referida a los DDHH,** denunciando todas las injusticias que vive el país.

Además añadió: “**...Durante mucho tiempo, la maquinaria de la propaganda rusa ha estado persiguiendo a las organizaciones no gubernamentales** como enemigos de Rusia, como espías que trabajan para los intereses de países extranjeros...”.

En las declaraciones del ex-ajedrecista queda clara la **persecución desplegada** por el régimen de **Putin hacía todo pensamiento u organización social contraría a la política desplegada por el presidente** y hacia la sistemática violación de DDHH en Rusia.
