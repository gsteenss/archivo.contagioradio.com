Title: "Ganges" un espejo de la crisis ambiental global
Date: 2019-06-06 12:28
Author: CtgAdm
Category: 24 Cuadros
Tags: Cine Colombiano, Documental, ganges
Slug: ganges-documental-estreno-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Ganges-un-viaje-por-los-sentidos-del-agua.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Prensa Ganges 

En esta semana que se conmemora el Día Mundial del Medio Ambiente, el estreno este jueves 6 de junio de **Ganges un viaje por los sentidos del agua** en salas de cine del país resulta ser una cita pertinente y necesaria por tratarse de una amalgama audiovisual entre crudas realidades contrastadas con la belleza extrema, espiritualidad, tradiciones milenarias y propuestas para enfrentar el  cambio climático a nivel global.

Entre los varios objetivos que se plantea esta producción colombiana, destaca el ánimo de **generar conciencia sobre lo que viene ocurriendo en India, como un espejo y ejemplo de los efectos de la crisis ambiental global** que estamos viviendo y que se agudizará si no tomamos medidas urgentes, teniendo en cuenta que uno de los principales problemas que enfrenta el río Ganges es el retroceso de los glaciares del Himalaya que lo alimentan, fenómeno producido por las emisiones de gases efecto invernadero.

Por otro lado invita a reflexionar a partir de algunos valores de la tradición india, como la concepción de la naturaleza como un todo del que somos partícipes. El lirismo de la película no solo hace alusión a la belleza de los paisajes, sino **al carácter contemplativo de la tradición espiritual,** lo que explica la intencionalidad de utilizar un montaje lento y fluido, como la corriente de un río calmo, compuesto por planos largos de situaciones culturales donde se desarrollan acciones sin una finalidad dramática, tratando de hacer que el espectador se sienta incluido en el espacio en el que se desarrolla la película.

<iframe src="https://www.youtube.com/embed/05Y1KawzYi0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Ganges, un viaje por los sentido del agua,  es una producción colombiana, a cargo de **Roberto Restrepo**, la ecóloga e investigadora **Ana Milena Piñeros**, la codirectora **Talía Osorio**, los fotógrafos **Helkin René Díaz y Felipe Aguilar**; y el script  de **Camila Martínez**, que cuenta con  la participación de **Veer Bhadra Mishra (QEPD)**, creador de la Fundación Sankat Mochan, reconocido por la revista Forbes como uno de los 10 héroes ambientales del planeta, el **Dr. Anamitra Anurag Danda**, vocero del Programa de Adaptación Cambio Climático WWF India y la **Dra. Vandana Shiva**, activista ambiental, filósofa, escritora y ganadora del Premio al Sustento Bien Ganado o Premio Nobel Alternativo en 1993.

El documental se proyectará en las **salas de Cine Colombia desde este 6 de junio**, tiene una duración de 75 minutos, está repleto de datos actuales, comprobados y supremamente preocupantes: de convertirse el río Ganges en un arroyo estacional se verían afectados alrededor de 1.000 millones de personas que se suplen de su caudal.

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
