Title: Así fue la movilización en defensa de la Educación
Date: 2017-10-04 09:44
Category: Educación, Nacional
Tags: Movimiento estudiantil, ser pilo paga
Slug: estar-seran-las-rutas-de-movilizacion-de-la-marcha-por-la-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/informe-ocde.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [04 Oct 2017] 

Miles de estudiantes salieron a la movilización en defensa de la educación este miércoles, en diferentes ciudades del país, para exigir que cesen los recortes al presupuesto para la educación, el apoyo a la política pública “Ser Pilo Paga” y que frenen las medidas que provocan la privatización de la educación pública en Colombia.

En Bogotá las rutas de movilización salieron desde 3 puntos. La Universidad Pedagógica salieron por la carrera 13, posteriormente bajaron hacia la calle 45, tomaron la carrera 30 para llegar a la Avenida **26 y llegar al Ministerio de Educación**. (Le puede interesar:["Colombia le cierra las puertas a la investigación y a la ciencia"](https://archivo.contagioradio.com/recorte-a-ciencia/))

Las universidades ubicadas en la zona del centro de la capital, se encontraron en el Planetario tomaron la Avenida 26 y de igual forma llegaron al Ministerio de Educación. Otra de las movilizaciones salió desde la **Plaza “Che” de la Universidad Nacional** en donde se encontraron con el resto de la movilización. (Le puede interesar:["Universidad Nacional necesita presupuesto para seguir siendo pública"](https://archivo.contagioradio.com/presupuesto-un-regalo-urgente-para-la-u-nacional-en-sus-150-anos/))

**Además hubo protestas en Cali**, donde los estudiantes salieron desde la Universidad del Valle, sede Meléndez y llegaron hasta la biblioteca Departamental, para realizar un plantón. **En Santa Marta** los estudiantes se encontraron a partir de las dos de la tarde en la UniMagdalena para llegar al Parque Bolívar.

**En Manizales**, la comunidad estudiantil de la Universidad de Caldas realizó asambleas por facultades para llevar a cabo un balance del estado de la financiación del centro educativo. Universidades como la Sur Colombiana, en Nevia - Huila y la Universidad de Pamplona, en Norte de Santander también tuvieron actividades durante la jornada.

\

###### Reciba toda la información de Contagio Radio en [[su correo]
