Title: Comunidad de Zona Humanitaria Nueva Vida amenazados por presencia Paramilitar
Date: 2016-11-23 12:02
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, cacarica, cavida
Slug: comunidades-de-nueva-vida-amenazados-por-presencia-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/paramilitares-Caucasia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [23 Nov 2016] 

Pobladores de la Zona Humanitaria de  Nueva Vida **denunciaron que un grupo aproximado de 120 paramilitares** se encuentra asentado en el canal Zapata, sobre el río Cirilo, lugar que se encuentra a una hora en bote de la Zona Humanitaria, y pese a que entes gubernamentales y estatales conocen la situación que pone en riesgo la vida de los pobladores, **no se ha tomado ninguna medida que los proteja.**

Hasta el momento, la única acción por parte de las Instituciones fue el envió de unidades **militares que ingresaron a la Zona Humanitaria  y que desde hace un mes, violando el espacio humanitario**, han estado irrumpiendo en el lugar con el pretexto de “que se deben comunicar y comprar víveres” y que luego de haber estado en el lugar tomaron un camino contrario al lugar en donde se encuentran los paramilitares. Le puede interesar:["Mujeres de Cacarica: 20 años espantando la guerra"](https://archivo.contagioradio.com/mujeres-de-cacarica-20-anos-espantando-la-guerra/)

No obstante los paramilitares que se autodenominan como **“Autodefensas Gaitanistas de Colombia” continúan amedrentando a los habitantes de la Zona Humanitaria**, pese a que estos les hayan exigido que se retirarán de su territorio. El pasado 20 de noviembre pobladores informaron que paramilitares detuvieron a un muchacho al que amenazaron de muerte y le robaron todas sus pertenencias.

Además, se conoció que los militares grabaron su ingreso a la Zona Humanitaria de Nueva Vida y lo están mostrando en sus recorridos a los pobladores de Cacarica que se encuentran en el camino, señalando que los habitantes de la Zona Humanitaria “son los que joden y los tienen en la mira”Le puede interesar:["120 paramilitares se toman comunidad en la cuenca del Río Cacarica"](https://archivo.contagioradio.com/120-paramilitares-se-toman-comunidad-en-la-cuenca-del-rio-cacarica/)

Las zonas humanitarias son lugares que proponen una aplicación concreta del derecho Internacional de los derechos humanos y del derecho humanitario que garantice los derechos de la población civil a una vida digna y un ambiente sano, **por lo tanto ningún actor del conflicto armado, como militares, guerrillas o paramilitares pueden hacer presencia en este lugar**. Principal razón por la que se pone en riesgo la vida de las comunidades, frente las acciones tanto de militares como de paramilitares.

###### Reciba toda la información de Contagio Radio en [[su correo]
