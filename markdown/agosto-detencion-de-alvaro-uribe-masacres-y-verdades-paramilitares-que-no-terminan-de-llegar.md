Title: Agosto: Detención de Álvaro Uribe, masacres y verdades paramilitares que no terminan de llegar
Date: 2020-12-31 11:19
Author: CtgAdm
Category: Especiales Contagio Radio, Nacional, Resumen del año
Tags: agosto, Alvaro Uribe, Masacres, Paramilitarismo
Slug: agosto-detencion-de-alvaro-uribe-masacres-y-verdades-paramilitares-que-no-terminan-de-llegar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-5.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Sin lugar a dudas la orden de arresto contra Álvaro Uribe Vélez por parte de la Corte Suprema de Justicia fue una de las noticias más relevantes del mes de Agosto. Luego de la detención el gobierno y sus funcionarios enfilaron baterías contra el máximo órgano de justicia en Colombia lo que fue calificado como una intromisión y una afrenta contra la democracia y la independencia del poder judicial frente al ejecutivo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Álvaro Uribe, para eludir la acción de la CSJ renunció a su curul en el senado obligando a que el proceso pasara a manos de la Fiscalía, en cabeza de Francisco Barbosa, amigo del presidente Duque. Han pasado ya cuatro meses y dicho ente de investigación no ha proferido escrito de acusación, lo que confirma que para quienes detentan el poder es posible eludir la acción de la justicia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo la violencia se recrudeció. Siguió en aumento el número de líderes y excombatientes asesinados mientras que las masacres marcharon con paso firme a la vista de miles de efectivos militares. En agosto la organización INDEPAZ había documentado 45 masacres en lo corrido del año. La masacre de Llano Verde en la que fueron asesinados 5 niños y la de Samaniego en Nariño en donde fueron asesinados 9 jóvenes estremecieron al país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En medio de esta altísima conflictividad política, social y armada, sobrevino la esperanza de la verdad para las comunidades. Salvatore Mancuso tendría que venir a Colombia luego de su promesa de aportar verdad para las víctimas. Sin embargo la acción del gobierno a través de la cancillería y la negativa parcial de la JEP para recibirlo y proteger el derecho de las víctimas a saber la verdad retrasan, y al parecer siguen retrasando esta posibilidad. Más ex jefes paramilitares está volviendo al país una vez cumplidas sus condenas en EEUU y sus verdes parecen difuminarse con el tiempo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Corte Suprema de Justicia ordena medida de aseguramiento contra Álvaro Uribe Vélez

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Corte Suprema de Justicia ordena medida de aseguramiento contra Álvaro Uribe Vélez](https://archivo.contagioradio.com/corte-suprema-de-justicia-ordena-medida-de-aseguramiento-contra-alvaro-uribe-velez/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Corte Suprema de Justicia ordenó medida de aseguramiento al expresidente y senador **Álvaro Uribe Vélez** quien es investigado por presunta manipulación de testigos. Esta se convierte en una decisión histórica al ser primera vez que un expresidente de la República tiene una medida de este tipo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Sala de Instrucción de la Corte Suprema de Justicia se reunió este lunes de manera extraordinaria sin embargo no llegó a ningún acuerdo por lo que el debate continúo este martes y tras dos días de sesión virtual. se tomó la decisión de dictar detención domiciliaria. Cabe señalar que en el proceso participaron cinco de los seis integrantes de la Sala de Instrucción, tras la separación del caso de la también magistrada Cristina Lombana.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Salvatore Mancuso reafirma su compromiso con las víctimas

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Salvatore Mancuso reafirma su compromiso con las víctimas](https://archivo.contagioradio.com/salvatore-mancuso-reafirma-su-compromiso-con-las-victimas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por medio de un audio conocido este 13 de agosto el líder paramilitar Salavatore Mancuso, afirmó que las [víctimas ](https://archivo.contagioradio.com/jep-nego-en-segunda-instancia-tutela-presentada-por-el-coronel-r-plazas-acevedo/)pueden estar seguras del aporte que hará para la reconstrucción de la verdad en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde la cárcel de Irwin, en la que esta recluido el exjefe paramilitar de las Autodefensas Unidas de Colombia, envió un audio en el que** expresa a las autoridades judiciales de Colombia y a las víctimas, que reafirma su compromiso con la verdad , la justicia y las garantías de no repetición **en relación con el *«conflicto colombiano en el cual tuve participación».*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### 5 niños afro fueron masacrados en el barrio Llano Verde en Cali

<!-- /wp:heading -->

<!-- wp:paragraph -->

[5 niños afro fueron masacrados en el barrio Llano Verde en Cali](https://archivo.contagioradio.com/5-ninos-afro-fueron-masacrados-en-el-barrio-llano-verde-en-cali/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La noche de este martes **se registró una masacre en la que cinco niños entre los 13 y 16 años fueron asesinados en un cañaduzal del barrio Llano Verde de la ciudad de Cali, Valle del Cauca.** Sobre las 11 de la noche el alcalde de la ciudad, [Jorge Iván Ospina](https://twitter.com/JorgeIvanOspina), reportó el lamentable hecho.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según medios locales **los menores asesinados fueron** **Jair Andrés Cortés (14 años), Jean Paul Cruz (16 años), Luis Hernando Preciado (14 años), Arturo Montenegro (13 años) y Leider Hurtado (14 años); todos pertenecientes a la comunidad afro.** Informes preliminares de Policía señalan que cuatro de los menores  fueron asesinados con tiros de gracia y el restante fue degollado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[45 masacres han sido perpetradas en lo que va del 2020: Indepaz](https://archivo.contagioradio.com/45-masacres-han-sido-perpetradas-en-lo-que-va-del-2020-indepaz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En medio del cruento escenario de violencia que se vive actualmente en el país; en el que **solo durante este fin de semana se perpetraran cuatro masacres, en los departamentos de Arauca, Cauca, Nariño y Antioquia, en las cuales fueron segadas las vidas de al menos 20 personas;** el Instituto de Estudios para el Desarrollo y la Paz –[Indepaz](http://www.indepaz.org.co/)– publicó un [informe](http://www.indepaz.org.co/wp-content/uploads/2020/08/Masacres-en-Colombia-2020-INDEPAZ.pdf) que documenta las masacres que se han registrado durante el año 2020 en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Considerando el más reciente hecho violento, en el que fueron asesinadas tres personas en el municipio de Venecia, Antioquia este domingo; **el informe documenta la ocurrencia de 45 masacres desde el 1° de enero hasta el 23 de agosto de 2020, en las que han sido asesinadas 182 personas. **

<!-- /wp:paragraph -->
