Title: Reforma tributaria, un golpe a los contribuyentes más débiles
Date: 2016-10-20 17:20
Category: Economía, Nacional
Tags: Canasta familiar, colombia, economia, fisco, gasesosas, Gasto público, hacienda, iva, Ministro de Hacienda, reforma, Reforma tributaria, tributos
Slug: reforma-tributaria-un-golpe-a-los-contribuyentes-mas-debiles-hector-moncayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Mauricio-Cardenas-2-e1477001217144.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Inteligencia Petrolera] 

###### [20 de Oct 2016] 

Luego de varias semanas de estar a la espera de la **presentación del texto de la reforma tributaria al Congreso por parte del gobierno**, **finalmente ocurrió. Éste, no fue recibido de manera positiva por varios sectores**. La reforma contempla entre otros temas, un aumento del IVA del 16 al 19% y ampliará la base para la declaración de renta a personas con ingresos superiores a \$2.700.000. Además, se gravarán las actividades de pequeños comerciantes con el 3% o el 1% si se acogen a un paquete de impuestos.

Otro de los argumentos dados por el gobierno para impulsar esta reforma tributaria, es que el país tiene un hueco fiscal que debe resolver, pese a que esto es cierto, **analistas como Héctor Moncayo aseguran que una reforma como la presentada no dará solución completa al grave problema por el que atraviesa la nación** “es claro que se tenga que hacer un reforma tributaria, pero no se puede con mentiras, siempre se da a entender que esta reforma es necesaria por los gastos del posconflicto y eso es una mentira. Si antes había gastos en guerra sencillamente habría que trasladarlos de la guerra a la paz” aseveró.  Le puede interesar: [Reforma tributaría afectará a trabajadores y pensionados](https://archivo.contagioradio.com/reforma-tributaria-afectara-a-trabajadores-y-pensionados/)

Frente a las implicaciones que podría tener que la reforma tributaria se esté ajustando a  las exigencias de la OCDE y de otros organismos financieros internacionales, Moncayo  aseguró que “**reñirse a las recomendaciones de estas instituciones es muestra del arribismo de la oligarquía, pero en realidad dentro de varias recomendaciones solo se está teniendo en cuenta una que tendría que ver con los paraísos fiscales**”.

En el tema del monotributo que ha causado junto con el impuesto a las bebidas azucaradas, el aumento a la gasolina y la canasta familiar un alto impacto en la población de Colombia, Moncayo aseguró que **la experiencia mundial ha mostrado que medidas como éstas no han sido tan efectivas como se dice, ni para la salud ni para el fisco, además aseveró que hacen que se distraiga la atención. **Puede leer: [Sindicatos rechazan reforma tributaria y convocan a paro general](https://archivo.contagioradio.com/sindicatos-rechazan-reforma-tributaria-y-convocan-a-paro-general/)

**Por último, manifestó que los impuestos a los honorarios es un tema que habría que analizar mejor**, puesto que por ejemplo pensar que una persona que gana 2.700.000 es rica “ya eso es pura demagogia” y concluyó diciendo que **hay que hacer el ejercicio de leer la reforma para poder saber qué se avecina** “estas reformas siempre le pegan a ese sector de honorarios, de ingresos de comercio, de servicio  y como son los más débiles en la cadena tributaria se les pasa la mano. Hay que hacer el ejercicio de leer la reforma”. Le recomendamos: [Reforma tributaria desaparecerá el 94% de las organizaciones sociales del país](https://archivo.contagioradio.com/reforma-tributaria-desaparecera-el-94-de-las-organizaciones-sociales-del-pais/)

<iframe id="audio_13406991" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13406991_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
