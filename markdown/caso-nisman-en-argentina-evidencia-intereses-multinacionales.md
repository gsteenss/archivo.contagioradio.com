Title: Hay intereses multinacionales tras el caso "Nisman" en Argentina
Date: 2015-01-27 20:40
Author: CtgAdm
Category: Entrevistas, Política
Tags: Argentina, impunidad, Nisman
Slug: caso-nisman-en-argentina-evidencia-intereses-multinacionales
Status: published

###### Foto: taringa.net 

##### [Julio Moureo] 

<iframe src="http://www.ivoox.com/player_ek_4006750_2_1.html?data=lZWdmJyZdI6ZmKiak56Jd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidPixtjh0ZCuuc3d0JC60drWqdChhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
Cada vez toma más fuerza la hipótesis de que **Alberto Nisman se habría suicidado**. Varias razones rodean esta versión puesto que el 20 de Enero, un día después de su muerte, tendría que presentar resultados en una audiencia que escucharía todo el país y no tenía información suficiente.

Por otra parte, fuentes indicaron que a Nisman le estarían **ofreciendo un pago y por ello habría dejado “plantada” a su hija**, justo el día de su cumpleaños, para viajar de manera intempestiva a Argentina. Según las fuentes **el pago no se realizó** y esto habría agudizado la depresión de Nisman, hecho que lo habría empujado al suicidio.

Ernesto Julio Moureo, integrante del Comité Ejecutivo de la Liga Internacional de Derechos Humanos, afirma que también **hay interese políticos y económicos detrás de la información que se ha publicado en los medios hegemónicos** como el grupo Clarin, que en principio lograron que el tema se moviera a nivel internacional para llegar al nacional con una **presión política muy fuerte sobre el gobierno de Cristina Fernandez.**

Algunos de los antecedentes del Fiscal Nisman, han estado relacionados con causas por crímenes de Estado cometidas durante gobiernos civiles anteriores a la dictadura, uno de ellos es conocido como “La Tablada” que ocultó la responsabilidad de los organismos de inteligencia de Argentina y otros en los que se ha comprobado la vinculación de la Mossad de Israel, en causas relacionadas con Irán.

Un elemento positivo y que podrá aportar en la investigación es que la propia familia del fiscal es parte querellante de la investigación y eso garantizaría la imparcialidad y salvaría la responsabilidad del gobierno, puesto que una de las dificultades es la **politización de muchos de los organismos de investigación** como algunos casos al interior de la propia fiscalía.

En ese sentido, el anuncio de la presidenta Cristina Fernandez, de **desmontar uno de los organismos de inteligencia** que venían funcionando desde la época de la dictadura.
