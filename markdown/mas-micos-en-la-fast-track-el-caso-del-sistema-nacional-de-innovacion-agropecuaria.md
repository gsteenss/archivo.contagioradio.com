Title: Más micos en la Fast Track: el caso del Sistema Nacional de Innovación Agropecuaria
Date: 2017-04-21 11:41
Category: Opinion, Paola
Tags: Fast Track, Proyecto de Ley, Sistema Nacional de Innovación Agropecuaria
Slug: mas-micos-en-la-fast-track-el-caso-del-sistema-nacional-de-innovacion-agropecuaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/campo-colombiano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo 

#### **Por: [Paola Galindo](https://archivo.contagioradio.com/paola-galindo/)** 

###### 21 Abr 2017 

En el 2014 el gobierno nacional le encargó a José Antonio Ocampo, ex ministro de Hacienda y Crédito Público del Gobierno de Samper,  la dirección de la ‘Misión Rural para la transformación del Campo’. En ese documento el profesor Ocampo critica tres fallos de la descentralización en lo que a política rural se refiere: el primero de ellos es el atraso en la construcción y mantenimiento de las vías terciarias; el segundo, pone sobre la mesa la baja prestación de asistencia técnica y  la incapacidad de los gobiernos de constituir un sistema institucional estable en lo que a ello se refiere y, por último, el déficit cuantitativo y cualitativo de la vivienda de interés social rural.

Frente al segundo fallo de la política rural, el tercer Censo Nacional Agropecuario realizado en el 2014 lanza algunos datos bastante alarmantes, entre ellos que sólo el 16,5% de las Unidades de Producción Agropecuaria han recibido asistencia técnica y la concentración de la prestación de este servicio en los departamentos de Antioquia, Huila, Cauca y Tolima. Vemos entonces que la asistencia técnica llega a muy pocos productores y productoras y que, además, es un servicio concentrado en algunos pocos departamentos, en 4 de 32.

Con la intención de superar esta situación el Gobierno Nacional en el marco del proceso de implementación presentó al Congreso el Proyecto de Ley 04 de 2017 para desarrollar el ‘Sistema Nacional de Innovación Agropecuaria’ (SNIA), el cual “crea” el servicio público de «extensión agropecuaria» cuyo propósito es el impulso de procesos de innovación para el mejoramiento de la productividad, la competitividad y la sostenibilidad del sector.  Pero, ojo, la inocencia es bonita hasta cierta edad y Santos está usando la Fast Track para meter uno que otro gorila.

En este proyecto de ley se incluyen como principios la «orientación al mercado e incorporación a cadenas de valor»,  la «gradualidad y temporalidad» y la «sujeción a la normatividad de propiedad intelectual». Es decir, se privilegia un enfoque en el que la articulación entre pequeños y medianos productores con el agronegocio se convierte en única finalidad de la prestación del servicio de «extensión agropecuaria», sin considerar el fortalecimiento de las pequeñas y medianas unidades de producción campesinas como uno de los fines en sí mismos de la prestación del servicio en cuestión.

[Adicionalmente, con el principio de «gradualidad y temporalidad» puede advertirse de manera preliminar una de las modificaciones más importantes y regresivas existentes en este proyecto de ley, la existencia de un cobro o tarifa del servicio público de «extensión agropecuaria», cuyo subsidio será “diferencial, decreciente y finito”. Así, además de empezar a cobrar un servicio que hoy es]*[gratuito]*[ y que es condición de justicia económica en el campo, el subsidio prestado a quienes no tienen la capacidad adquisitiva para adquirirlo, menguará con el tiempo.]

Resulta curioso que se incluya a los productores en “espacios para la retroalimentación de la política pública sectorial, además del empoderamiento para gestionar la solución de sus necesidades” en lo que a asistencia técnica y extensión agropecuaria se refiere, pero que el acceso a los contenidos y beneficios de esa política pública se vea condicionado a la capacidad de comprar el servicio.

Así mismo, sujetar el conjunto de procesos de investigación y creación de ciencia y tecnología a la normatividad de propiedad intelectual, restringirá o anulará, en el peor de los casos, la labor de protección y reproducción del material genético de las semillas propias.

[Estamos entonces ante un escenario de incumplimiento de lo consignado en el Nuevo Acuerdo, pues el «Plan Nacional de Asistencia Integral, Técnica, Tecnológica y de Impulso a la Investigación» allí consignado,  está orientado por tres criterios que pueden resumirse en i) la garantía de la prestación del servicio público]*[gratuito]*[ de asistencia técnica y tecnológica para los beneficiarios y beneficiarias del Fondo de Tierras, para los productores y productoras, priorizando a las mujeres cabeza de familia; ii) “La vinculación de la asistencia técnica y tecnológica con los resultados de procesos de investigación e innovación agropecuaria, incluyendo el uso de las tecnologías de la comunicación y la información” y, iii) la promoción y protección de las semillas nativas y los bancos de semillas.]

Así como está redactado este proyecto, ni gratuidad, ni participación, ni protección de las semillas nativas o propias.

#### **[Leer más columnas de Paola Galindo](https://archivo.contagioradio.com/paola-galindo/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de  Contagio Radio 
