Title: Contra viento y marea la minga sigue exigiendo diálogo con Duque en Bogotá
Date: 2020-10-16 12:53
Author: AdminContagio
Category: Actualidad, DDHH
Tags: CRIC, Gobierno Duque
Slug: 7-000-indigenas-de-la-minga-buscan-un-dialogo-con-duque-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/CRIC.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/121471521_2812872178815149_5229210965982667468_n.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: Minga / CRIC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cerca de 7.000 indígenas agrupados en la minga continuarán su recorrido hacia Bogotá mientras otros 3.000 regresarán a sus regiones de origen. En medio del recorrido que comenzó este 15 de octubre con destino a Bogotá, realizarán paradas en Ar**menia, Ibagué, Fusagasuga y Soacha**, donde realizarán diferentes actos reivindicativos. Posteriormente, al llegar a Bogotá, en principio se instalarían en la Universidad Nacional para después dirigirse hacia la plaza de Bolívar y acordar un encuentro con el presidente Duque.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las comunidades se trasladan a Bogotá, días después de que a la mesa de diálogo propuesta por la Minga indígena y social no llegara el presidente Duque y no se lograra un acuerdo con los representantes del gabinete del Gobierno al que la minga le exige un cambio de rumbo en la dirigencia del país".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A raíz de ello, la comunidad indígena avanza con decisión hacia la capital en medio de los señalamientos por parte del Gobierno como los hechos por la ministra del Interior, Alicia Arango u organismos de inteligencia quienes sostienen que la minga es política o que está se encuentra infiltrada por grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto voceros de la minga como la lideresa Aida Quilcué han reiterado que la minga no está infiltrada y que estos señalamientos han sido históricos, expresando que "aquí no hay infiltración, hay indígenas, campesinos, estudiantes que caminan en el marco de la vida y la dignidad”. Otros líderes indígenas como Hermes Pete, consejero del Consejo Regional Indígena del Cauca (CRIC).han expresado que lo que se ve sobre estas declaraciones es "una estigmatización y señalamientos para tratar de deslegitimar la minga”. [(Lea también: Avanza la Minga en defensa de la vida)](https://archivo.contagioradio.com/la-minga-sera-en-defensa-de-la-vida-el-territorio-la-democracia-y-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Son múltiples las razones por las cuales sectores indígenas, campesinos y organizaciones sociales, han tomado la decisión de reactivar la Minga, en particular para exigir garantías para la vida, el ejercicio político, y defender el territorio del extractivismo y la implementación del Acuerdo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El Congreso abre sus puertas a la minga pero ¿escuchará el Gobierno?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras el Comisionado de Paz Miguel Ceballos, asegura que si se trata de un escenario político, el debate debe ser llevado al Congreso, las autoridades indígenas responden que su propósito es ser escuchados por el mandatario pues no ven otro tipo de acciones que puedan frenar las violaciones de DD.HH. que se vive en sus territorios, y en particular buscar avances para la sustitución de cultivos de uso ilícito. [(Le puede interesar: El racismo permanece en las instituciones del Estado: CRIC)](https://archivo.contagioradio.com/el-racismo-permanece-en-las-instituciones-del-estado-cric/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito de esta situación, el Senado aprobó una proposición para realizar una audiencia pública y que la Minga sea escuchada por el Gobierno en el Congreso y a su vez crear una mesa de seguimiento a los acuerdos a los que lleguen las partes.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Senadores como Armando Benedetti, quien propuso el espacio, señalan que se debe crear el escenario ideal para escucharles y en lo posible "llevarle un mensaje al presidente o si se puede que ellos vayan y asistan a una reunión con el presidente”, una opción que consideró nula al señalar que al acceder sería como aceptar un juicio de parte de la población indígena.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"El presidente no va ir, no me lo ha dicho él ni me lo ha dicho nadie, no necesito tenerlo a más de dos centímetros de frente para decirme que un presidente no acudirá a un mecanismo distinto al que él tiene como jefe de Estado". [(Le puede interesar: Tres comuneros indígenas fueron asesinados en Santander de Quilichao, Cauca)](https://archivo.contagioradio.com/tres-comuneros-indigenas-fueron-asesinados-en-santander-de-quilichao-cauca/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Defensoría del Pueblo aboga por un encuentro entre Minga y Gobierno

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras la caravana de chivas se dirige a la capital del país, la [Defensoría del Puebl](https://www.defensoria.gov.co/es/nube/comunicados/9661/Comunicado-de-prensa-15-de-octubre-minga-social-CRIC-Defensor%C3%ADa-del-Pueblo-protesta-covid.htm)o expresó a través de un comunicado, su voluntad de acompañar **“espacios para el encuentro”** entre el presidente Duque y representantes de la minga.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque aún no se ha confirmado ni existe una propuesta de diálogo por parte del presidente Duque, la Defensoría, consciente "de la importancia de la protesta y la movilización social como instrumento de reivindicación de los derechos de los pueblos indígenas" exaltó la necesidad de abrir un espacio de diálogo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agregan, que si así lo considera el CRIC, tendrían a su disposición el acompañamiento y asistencia de una comisión de alto nivel de la Defensoría que acompañará el trayecto de los pueblos ancestrales, reiterando el compromiso que la comunidad indígena debe tener con los protocolos de seguridad en medio de la pandemia. [(Le puede interesar: Comunidades indígenas generan espacios de protección contra el COVID - 19)](https://archivo.contagioradio.com/comunidades-indigenas-generan-espacios-de-proteccion-contra-el-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Alerta por minguero desaparecido

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, desde el CRIC denunciaron que el **líder indígena Félix Antonio Hernández, alcalde del Pueblo Embera Chamí de Caldas**, había desaparecido desde el pasado 14 de octubre en Cali, en el marco de la minga. [(Lea también: "En medio del miedo la comunidad indígena en vez de retroceder se ha reforzado", Milton Conda)](https://archivo.contagioradio.com/milton-conda-lider-indigena-nasa-denuncia-amenazas-en-su-contra/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Señalan que en su carpa fue hallada su billetera con documentos de identidad, celular y demás objetos personales por lo que hacen un llamada para activar los protocolos de búsqueda y dar con su paradero.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este viernes, las autoridades confirmaron que el líder indígena, **Félix Antonio Hernández**, apareció en buenas condiciones de salud en Pereira, Risaralda. Aunque desconocen cómo logró llegar hasta la ciudad, las autoridades señalan que llegó en un estado de confusión por lo que se busca esclarecer los hechos de su desaparición.

<!-- /wp:paragraph -->

<!-- wp:image {"id":91588,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/121471521_2812872178815149_5229210965982667468_n.jpg){.wp-image-91588}  

<figcaption>
Félix Antonio Hernández, alcalde del Pueblo Embera Chamí

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
