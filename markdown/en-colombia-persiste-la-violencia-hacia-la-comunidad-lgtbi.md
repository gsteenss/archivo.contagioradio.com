Title: En Colombia persiste la violencia hacia la comunidad LGTBI
Date: 2017-06-29 13:45
Category: Entrevistas, LGBTI
Tags: Caribe Afirmativo, Colombia Informa, Comunidad LGTBI
Slug: en-colombia-persiste-la-violencia-hacia-la-comunidad-lgtbi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/colombia-diversas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Diversa] 

###### [29 Jun 2017] 

Durante el año 2016 en Colombia se mantuvieron las violencias sistemáticas contra integrantes de la comunidad LGTBI, **las agresiones por parte de la Fuerza Pública y las amenazas a líderes y defensores de derechos humanos de esta comunidad, cobraron la vida de 108 personas**. Así lo evidencia el más reciente informe publicado por las organizaciones Caribe Afirmativo y Colombia Diversa.

### **Agresiones a la Comunidad LGTBI** 

De acuerdo con Wilson Castañeda, director de la organización Caribe Afirmativo los departamentos del Valle del Cauca, Antioquia y Atlántico son los que reportan la gran mayoría de casos de violencia hacia la comunidad LGTBI, c**on un 60% de los homicidios registrados. **Le puede interesar: ["Mafe Rojas un transfeminicidio que enluta a la comunidad LGTBI"](https://archivo.contagioradio.com/40077/)

Otra de las problemáticas que han venido señalando las organizaciones defensoras de derechos de las comunidades LGTBI, es la falta de un sistema de información, por parte del Estado, **que permita contratar las cifras de la sociedad civil con las oficiales**, impidiendo tener una cifra total de la cantidad de homicidios.

Frente a las agresiones de la Policía Nacional, Castañeda afirmó que hay un trabajo en doble vía, debido a que, si bien por un lado la autoridad ha emitido comunicados internos y manifestado que respeta los derechos de las personas LGTBI, por el otro **en las calles permanece el irrespeto y la falta de reconocimiento a las personas de esta comunidad por parte de miembros de La Policía.**

La impunidad en los casos de personas LGTBI, tampoco ha disminuido, Castañeda señaló que los casos que se llevan contra miembros de la Policía no han sido fructíferos, por la falta de acciones de la Procuraduría, “**muchas de las agresiones terminan siendo investigadas por la Policía, diluyéndose y sin sanciones significativas**” afirmo el director de Caribe Afirmativo. Le puede interesar: ["En vídeo quedó registrada la agresión contra comunidad LGTBI"](https://archivo.contagioradio.com/video-agresion-comunidad-lgbti/)

Solamente el **6% de los casos de violencia a personas LGTBI avanzan con investigaciones**, mientras que los demás han sido archivados. A su vez, las amenazas a defensores de derechos humanos de la comunidad LGTBI y líderes y lideresas han aumentado, sin que se activen los mecanismos de alertas tempranas por falta de las autoridades.

### **Los avances en los derechos LGTBI** 

De otro lado Castañeda manifestó los avances en cuanto a la atención rápida por parte de la Fiscalía General de la Nación en casos de violencia contra la comunidad LGTBI. Le puede interesar:["Hay que erradicar el lenguaje discriminatorio contra comunidad LGTBI"](https://archivo.contagioradio.com/hay-que-erradicar-el-lenguaje-discriminatorio-contra-comunidad-lgtbi/)

Una de las recomendaciones que realiza el informe es la necesidad de que el Estado re afirme los derechos de la comunidad LGTBI, q**ue aumente las políticas públicas sobre el reconocimiento de los derechos**  y que realice campañas más profundas con la ciudadanía que permitan aceptar la diversidad.

<iframe id="audio_19544810" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19544810_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
