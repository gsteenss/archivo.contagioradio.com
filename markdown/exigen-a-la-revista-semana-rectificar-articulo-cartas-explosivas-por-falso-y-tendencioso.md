Title: Exigen a la Revista Semana rectificar artículo "cartas explosivas" por falso y tendencioso
Date: 2017-11-17 18:03
Category: Judicial, Nacional
Tags: Andino, Atentado andino, Boris Rojas, Juan Camilo Pulido, Mateo Gutierrez, Mauricio Bohorquez, Revista Semana
Slug: exigen-a-la-revista-semana-rectificar-articulo-cartas-explosivas-por-falso-y-tendencioso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/jovenes-piden-rectificacion-revista-semana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Carta y portada Revista Semana] 

###### [17 Nov 2017] 

Ante los señalamientos de la **Revista Semana, Boris Rojas, Juan Camilo Pulido, Andrés Mauricio Bohórquez**, detenidos luego de la explosión de un artefacto en el Centro Comercial Andino, y Mateo Gutiérrez, detenido por la explosión de un artefacto panfletario, exigieron una rectificación a este medio de información, manifestado que las afirmaciones que hacen en su artículo **“Cartas explosivas en el caso Andino”, son falsas, tergiversadas, carecen de fundamento y atentan contra su presunción de inocencia**.

En el documento que enviaron al medio, las 4 personas detenidas expresaron que en el artículo se les** “responsabiliza anticipadamente de los lamentables hechos ocurridos en el C.C Andino”** y se afirma que desde su reclusión han continuado “urdiendo… siniestros planes”. Además, se asevera la pertenencia de los jóvenes a la “célula terrorista” referida al grupo autodenominado MRP, sin que la Justicia así lo haya determinado, manifestado que esta organización “intentaba nuevas acciones terroristas desde la captura de 10 de sus integrantes”.

De igual forma, quienes escriben la misiva, señalaron que la Revista Semana utiliza un lenguaje “injurioso y tendencioso” al equiparar los nombres de cada uno de ellos como un alias, es decir a Boris Rojas se le indetifica como alias “Boris, a Juan Camilo Pulido como alias “Juan Camilo, a Andrés Mauricio Bohorquez como alias “Andrés” y a Mateo Gutiérrez como alias “Mateo”. (Le puede interesar:[ "Cárcel para implicados en el caso Andino esta motivada por "Posturas idelógicas""](https://archivo.contagioradio.com/carcel-para-implicados-en-caso-andino-esta-motivada-por-posturas-ideologicas/))

### **Las afirmaciones contra Boris Rojas** 

En el caso particular de Boris Rojas, se asegura en el texto que alias “Boris” es el jefe de la célula terrorista, es decir del MRP, **que de acuerdo con el detenido no tienen ningún tipo de fundamento o comprobación**.

Igualmente en el artículo se menciona que, la información incautada a José Antonio Núñez alias “Diego”, presunto integrante del ELN, es “idéntica a la encontrada en la USB y computadores de Boris y alias “Japo”, afirmación que **es imposible de corroborar debido a que ni siquiera hay descubrimiento probatorio en el marco del proceso**. Sin embargo, Semana asegura en el artículo que “los documentos encontrados no dejan duda de la relación entre el MRP y el ELN”.

Finalmente, en la misiva señaló que,** la Revista Semana saca conclusiones sin fundamento al indicar que una carta que supuestamente tenía Boris** en su celda, en donde le manifestaban que “los pelados continúan con la actitud” es una prueba suficiente para señalar que, afuera de la cárcel habría otros integrantes del MRP “dispuestos a continuar con las acciones terroristas”.

### **Las afirmaciones contra Mateo Gutiérrez** 

En el artículo publicado el pasado 12 de noviembre, se presenta información sobre cartas de Mateo Gutiérrez, que de acuerdo con él es tendenciosa, en primera medida porque los textos “fueron extraídos irregularmente”, además Mateo afirma que la carta, que fue enviada a un líder indígena, se refiere a Feliciano Valencia y fue escrita con motivo de enviar un “saludo fraterno, mostrando alegría” por su salida; **mientras que Semana la presenta en el marco de una “gran cantidad de cartas” que supuestamente tenía escondidas y que generan sospecha**.

Sobre la carta dirigida a Tony, que en la Revista Semana es presentado como un hombre vigilado por la inteligencia colombiana e internacional, porque supuestamente pertenecía al G2, Mateo afirmó que la carta presentada como **“llamativa y enigmática” y entre líenas reproduce una acepción de Cuba como un país comunista** que amenaza la seguridad colombiana. (Le puede interesar: ["José Antonio "Tony" responde responde a publicación de Revista Semana sobre Mateo Gutiérrez"](https://archivo.contagioradio.com/jose-antonio-lopez-tony-responde-a-publicacion-de-semana-sobre-mateo-gutierrez/))

De igual forma Aracely León, mamá de Mateo, aseguró que **"la Fiscalía está haciendo un caso paralelo en los medios de información"** con el caso de su hijo y reiteró que Mateo no esta siendo investigado por el caso del Centro Comercial Andino, como también ha intentado señalarse en medios de información.

Finalmente los jóvenes expresaron en la carta que “lejos de realizar un ejercicio periodístico y objetivo, la Revista Semana hace apreciaciones totalmente subjetivas e incluso, sin tener pruebas reales de la comisión de conductas delictivas de nosotros como reclusos, conmina a las autoridades penitenciarias a que se nos traslade a patios diferentes”.

[Carta en la que solicitan la rectificación a la revista Semana](https://www.scribd.com/document/364738236/Carta-en-la-que-solicitan-la-rectificacion-a-la-revista-Semana#from_embed "View Carta en la que solicitan la rectificación a la revista Semana on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_67815" class="scribd_iframe_embed" title="Carta en la que solicitan la rectificación a la revista Semana " src="https://www.scribd.com/embeds/364738236/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-peekyLTMiyCM6kXT8ErX&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
