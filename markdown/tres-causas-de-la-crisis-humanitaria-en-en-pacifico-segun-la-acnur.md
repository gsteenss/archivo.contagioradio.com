Title: Tres causas de la crisis humanitaria en el Pacífico según la ACNUR
Date: 2017-08-30 16:52
Category: DDHH, Entrevistas
Tags: Desplazamiento, pacífico
Slug: tres-causas-de-la-crisis-humanitaria-en-en-pacifico-segun-la-acnur
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/desplazamiento-en-el-pacifico-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 Ago 2017] 

De acuerdo con el último informe de la ACNUR, en el último año en Colombia 9.000 personas han sido víctimas de desplazamientos forzados, de las cuales la gran mayoría lo hicieron en el Pacífico como producto de 3 factores que se están presentando en el territorio: el primero son los escenarios proclives a la instauración de actores del conflicto armado, **el segundo es la violencia política y el último es el desconocimiento sistemático de los derechos fundamentales** por parte de los pobladores.

### **El drama del Desplazamiento en el Pacífico** 

La ACNUR estableció que las causas que están provocando estos desplazamientos forzados, de acuerdo con los reportes de las comunidades, se deben a enfrentamientos entre actores ilegales, Rocío Castañeda, oficial de información de la ACNUR manifestó que otros de los elementos son las **“disputa por el control del territorio”**, la falta de presencia estatal y además, la posición geográfica del territorio que permite establecer rutas de narcotráfico.

Otro de los elementos que ha provocado desplazamiento forzados en el territorio es la riqueza mineral que hay allí y que se ha visto expuesta históricamente a la minería ilegal, todas esas situaciones, según la ACNUR, **dejan a las comunidades inmersas en conflictos y con la única alternativa de salir de su lugar de origen**. (Le puede interesar: ["Paramilitares asesinan a dos integrantes de la comunidad Wounnan"](https://archivo.contagioradio.com/paramilitares-asesinan-a-dos-intengrantes-de-la-comunidad-wounnan/))

### **El desplazamiento forzado otra forma de victimización** 

De acuerdo con Castañeda, el fenómeno del desplazamiento forzado provoca múltiples afectaciones, “**en los territorios donde se están produciendo todos estos hechos hay contaminación por minas antipersonales**, se presentan homicidios selectivos, amenazas, intimidaciones y limitaciones a la movilidad”.

En ese sentido, la ACNUR señaló que el confinamiento de las comunidades a causa de la violencia, impide que las poblaciones puedan incluso realizar sus acciones básicas para sobrevivir como lo es devengar el sustento. (Le puede interesar: ["Denuncian operaciones de paramilitares en casco urbanos de Río Sucio"](https://archivo.contagioradio.com/denuncian-operaciones-paramilitares-en-casco-urbano-de-riosucio-choco/))

Otro tipo de afectaciones, según Castañeda, se da después del desplazamiento, cuando las comunidades llegan a ciudades o **cascos urbanos y no hay una respuesta estatal suficiente para atender las necesidades de las personas**.

### **El llamado de la ACNUR a las organizaciones institucionales** 

Rocío Castañeda aseveró que el llamado que la ACNUR le está haciendo al Estado y a las autoridades locales es que se atienda de manera oportuna a las comunidades, “muchas veces los desplazamientos, **por el número de personas que ha implicado, desbordan las capacidades de los municipios** y requiere que sea apoyado por entidades del orden nacional”.

Y afirmó que debe generarse una mayor coordinación entre las diferentes instituciones, locales – nacionales, para dar respuestas oportunas a estas situaciones.

<iframe id="audio_20611512" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20611512_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
