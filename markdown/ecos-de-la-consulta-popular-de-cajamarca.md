Title: Ecos de la consulta popular de Cajamarca
Date: 2017-05-26 11:03
Category: CENSAT, Opinion
Tags: Cajamarca, consulta popular, Tolima
Slug: ecos-de-la-consulta-popular-de-cajamarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/consulta-popular.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Censat 

###### 26 May 2017 

#### **[Por: CENSAT Agua Viva /Amigos de la Tierra Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

**Menos autonomías territoriales y democracia directa**

La decisión de la compañía minera AGA de suspender la exploración y extracción de millones onzas de oro en el municipio de Cajamarca, hasta no tener seguridad jurídica de la inversión extranjera en el país, pone en alerta a empresas extractivistas y gobierno para decidir reformar los mecanismos de participación ciudadana, limitando el alcance de los mismos respecto de los usos del suelo, subsuelo, las autonomías territoriales, y la construcción autónoma de los territorios (CP 1991, Ley 388/97, Ley 134/94, Ley 152/94 y Ley 99/93).

Los últimos resquicios democráticos que aun presenta el sistema político colombiano (CP91, Estado social de Derecho, Corte Constitucional y participación ciudadana) han sido herramientas dinámicas para defender la vocación y usos del suelo territorial, las autonomías territoriales y la participación ciudadana directa. Es el caso de las consultas populares de Tauramena, Piedras, Cajamarca y Cabrera, y la Iniciativa Popular Normativa de Cerrito –Santander, que han decidido modelos propios y sustentables de vida y economía municipal –regional, contrarios al avasallante modelo económico extractivista petrolero, minero, energético y agroindustrial generador de enormes plusvalías financieras para el mercado global pero con cuantiosos pasivos socio-ambientales no cubiertos, constituidos en deuda ecológica territorial.

Restringir aun más estos mecanismos participativos por parte del régimen político en anuencia con el mercado global (proyectos de ley para limitar las consultas populares, consultas previas), implica fortalecer la seguridad jurídica que busca atraer la inversión transnacional, defender la utilidad pública y el interés general (1) (del mercado). A su vez implica vaciar las ciudadanías políticas nacionales para asegurar la extracción máxima de la riqueza nacional sin las cortapisas de las “comunidades emotivas” (votantes en las consultas populares) que no conocen lo técnico de la minería responsable, sostenible y eco-amigable.

Este escenario, ubica más elementos de contradicción, tensiones, y conflictos en los espacios del poder territorial. Por una parte, los municipios y regiones en disputa con el poder del estado central; dos, las apuestas por la diversidad biocultural territorial respecto de la visión homogenizante nacional y global; y tres, los conflictos de soberanías populares territoriales respecto de las democracias representativas nacionales.

Pero no serán solo las vías garantistas constitucionales las que posibiliten la defensa de diversidades y economías para la vida ante las economías contra la vida (minero-energético), sino la condición de sujetos históricos alcanzada por las comunidades campesinas y las organizaciones populares ambientalistas, como el gran eco político dejado por la consulta popular de Cajamarca en el Tolima.

------------------------------------------------------------------------

###### [ (1) El concepto jurídico de utilidad pública y el interés general (nacional) entra en abierta contradicción con el significado y alcance de la utilidad pública e interés general divergente de las entidades territoriales (municipios, regiones y localidades) y de las comunidades territoriales y locales.] 
