Title: Congreso de los Pueblos finaliza su seminario político anual
Date: 2015-02-02 22:22
Author: CtgAdm
Category: Movilización
Tags: congreso de los pueblos, dialogos de paz, participación
Slug: congreso-de-los-pueblos-finaliza-su-seminario-politico-anual
Status: published

###### **Foto:Congresodelospueblos** 

##### [Entrevista a Ricardo Herrera:] <iframe src="http://www.ivoox.com/player_ek_4028863_2_1.html?data=lZWfmp2ad46ZmKiakpqJd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8bhytPO1M7Tb9HjzYqwlYqliNXdxNSYpdTSq9PZ1NSYxsqPsNDnjNXix8fQs9Shhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

Más de 350 personas pertenecientes a las distintas organizaciones sociales de las regiones han concluido el seminario político anual del **Congreso de los Pueblos** para analizar el trabajo político del año anterior y crear las directrices del 2015.

Entre los ejes de trabajo ha estado el análisis de la caracterización del Congreso de los pueblos, es decir, de su propia naturaleza y del **trabajo avanzado ya por las distintas organizaciones sociales** en las regiones donde tienen presencia.

Las conclusiones van alrededor de las dificultades para conformarse debido a la represión y a la falta de participación del pueblo colombiano en la política. Desde la organización se valora los procesos de paz y se apoyan, aunque remarcando que la paz tiene que ser con justicia social y participación.

Por último nos explica que en 2015, **la unidad y la convergencia van a ser el lema principal del Congreso de los pueblos** de cara a las elecciones ya la construcción de país.
