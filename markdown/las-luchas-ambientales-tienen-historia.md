Title: Las luchas ambientales tienen historia
Date: 2016-05-19 14:06
Category: CENSAT, Opinion
Tags: Ambiente, Kimy Pernía Domicó, luchas sociales
Slug: las-luchas-ambientales-tienen-historia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Historia-Ambiental.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Diana Aya 

#### **Por [CENSAT Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva)

###### 19 May 2016 

[La primera gran lucha antiminera en Colombia tuvo lugar en la Provincia García Rovira - Santander, en la década de los 90 del siglo XX. Allí el movimiento campesino, ligado a la Asociación Nacional de Usuarios Campesinos – Unidad y Reconstrucción  (ANUC – UR) y una importante articulación de sectores sociales del municipio de Cerrito, logró frenar un proyecto carbonífero en el páramo del Almorzadero que después de tres décadas y gracias a la organización social no se ha realizado. No hay que olvidar la IV Conferencia Nacional de Páramos que en 1999 logró movilizar más de 600 personas de todo el país, dando un respaldo a esta histórica lucha que ha inspirado la defensa de los diversos páramos en Colombia, entre ellos el Santurbán.]

En esa misma época, los U'wa, un pueblo indígena que habita la cordillera oriental en la frontera con Venezuela, logró movilizar el afecto de amplios sectores de Colombia, e incluso de otros países que se solidarizaron con su lucha. “Ruiría es petróleo, y el “petróleo es la sangre de la tierra” fueron algunas de sus consignas. Su lucha contra la petrolera estadounidense Occidental, logró destacar la importancia de lo sagrado para defender el territorio, y su ejemplo fue seguido por numerosos pueblos en el mundo. Occidental abandonó el proyecto que posteriormente retomó Ecopetrol. Esta lucha aún persiste.

A finales del siglo XX y principios del XXI, emerge un importante proceso de defensa territorial en contraposición a la construcción del proyecto  hidroeléctrico Urrá en territorio embera – katío, tanto indígenas como campesinos y pescadores del bajo Sinú (Córdoba) organizados en la Asociación de Productores para el Desarrollo Comunitario de la Ciénaga Grande del Bajo Sinú (Asprocig) lograron sensibilizar a estudiantes, activistas y defensores de derechos humanos, tanto en Colombia, como en Suecia,  EEUU, y otros países para oponerse férreamente a la represa Urrá.

Estas luchas tuvieron un costo enorme en vidas humanas, entre ellas Kimy Pernía Domicó fue desaparecido y posteriormente asesinado por las Autodefensas Unidad de Colombia  - AUC- lideradas por el paramilitar Carlos Castaño. U'was y Emberas mantuvieron presente el debate del petróleo y las represas durante varios años. Cientos de movilizaciones, plantones, mítines simultáneos en diversos países, foros y debates públicos por todo el país, y numerosas acciones internacionales se llevaron a cabo.

Estas y otras importantes luchas que no logramos mencionar acá por cuestión de espacio, son sin duda los más cercanos antecedentes de lo que es hoy la movilización ambiental en Colombia, y que ha requerido fortalecerse en la medida que el modelo extractivo se profundiza.

Sin embargo, no fueron las primeras luchas ambientales en el país, en los años setenta, organizaciones del eje cafetero como los Grupos Ecológicos de Risaralda y la Fundación Cosmos se opusieron a los monocultivos forestales, también fueron importantes las movilizaciones nacionales en defensa del Parque Tayrona y la lucha contra las fumigaciones.

Aunque estos antecedentes son desconocidos para muchos y algunos incluso consideran que sólo hasta ahora el país se moviliza por conflictos ambientales, hay que tener presente que “mucha agua ha corrido bajo el puente”, y es importante reconocerlo porque hay mucho que aprender de cada uno de dichos procesos.

De hecho, esta movilización ambiental ha significado en términos políticos una clara confrontación a los regímenes económicos e ideológicos, posicionando nuevas racionalidades y prácticas, y cuestionando las relaciones con la naturaleza que desafían la subordinación de ésta en los discursos desarrollistas. Así mismo, las luchas ambientales ha disputado el lugar de los discursos tecnicistas que habían acaparado los debates ambientales, poniendo en el centro otros valores y conceptos como el de territorialidad, ancestralidad, cosmovisión, producción y consumo sustentable. En este sentido, hacer memoria de la lucha ambiental significa también pensar en lo que estos procesos han aportado para lo que hoy sucede en un contexto más complicado pero con características específicas.

La particularidad de las luchas de hoy es que ya no son sólo de poblaciones étnicas o grupos autodenominados ambientalistas, sino que han logrado articular amplios sectores de la población, considerando que hoy esto se da principalmente por tres elementos: 1) la crisis ambiental que no es sólo nacional sino planetaria, 2) una mayor sensibilidad frente a lo ambiental y 3) el fracaso de la promesa del desarrollo.

Es claro que está en ciernes un movimiento social por el agua en Colombia, fortalecido por iniciativas  como el del referendo por el agua y por los procesos aquí mencionados. La de hoy no es una lucha espontánea, es la expresión de un acumulado de procesos que históricamente han defendido el  territorio y que han tenido en el agua su mayor fortaleza. Sin embargo, la complejidad de los procesos, la atomización de las luchas, la amplitud de los sectores que se organizan, se movilizan y se articulan hacen también difícil la construcción de un gran movimiento nacional. Y aunque hay importantes esfuerzos de articulación de orden nacional, como el caso del Movimiento Ríos Vivos, que congrega a los y las afectadas por represas en el país, la Red Nacional de Acueductos Comunitarios, la Red de Semillas Libres de Colombia y la Confluencia Nacional por el Agua, entre otras, constituir un entramado más amplio del ambientalismo que supere complejidades y diferencias, representa un gran reto. De un lado, habrá que lidiar con protagonismos individuales, pero sobre todo, tendremos que entender las motivaciones que juntan a las personas para defender su territorio, y que unos y otros rompamos las limitaciones para comprender la lucha de los demás.

Examinar la historia del ambientalismo, comprender los procesos actuales y actuar con prudencia y autocrítica es determinante para lograr converger en un proceso nacional que nos permita enfrentar la perversidad del modelo de desarrollo, al tiempo que logramos concebir y labrar rutas hacia la sustentabilidad y la vida digna.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
