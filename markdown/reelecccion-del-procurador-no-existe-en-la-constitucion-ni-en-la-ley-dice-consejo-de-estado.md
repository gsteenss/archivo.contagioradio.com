Title: Reeleccción de Procurador no existe en la Constitución ni en la ley dice Consejo de Estado
Date: 2016-09-07 17:53
Category: Judicial, Nacional
Tags: paz, Proceso de conversaciones de paz, Procurador general, procuraduria
Slug: reelecccion-del-procurador-no-existe-en-la-constitucion-ni-en-la-ley-dice-consejo-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/procurador-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: la fm] 

###### [07 Seo 2016]

La reelección del **procurador Alejandro Ordoñez** fue declarada ilegal por el Consejo de Estado que con 14 votos a favor avaló la ponencia de la Magistrada Araujo que sostenía que la figura de la reelección de una persona en el cargo de la procuraduría no existe ni en la constitución ni en la ley. Además el alto tribunal consideró que existieron serias irregularidades en la postulación porque no se aceptaron los impedimentos de 3 magistrados de la Corte Suprema de Justicia.

La decisión se tomó luego de seis meses de estudio por parte del Consejo de Estado, y aunque se acusó en varias ocasiones a la defensa de Ordoñez de **aplicar maniobras dilatorias para impedir que el proceso avanzara** y se pudiera completar el periodo al que le faltaban solamente 6 meses.

Otro de los argumentos planteados en la demanda fue que cuando se votó en el congreso para la elección** **en 2012, se presentaron 28 recusaciones y 39 impedimentos por parte de congresistas que tenían [familiares trabajando en el Ministerio Público](https://archivo.contagioradio.com/el-procurador-se-nos-metio-en-la-cama-en-la-casa-y-hasta-en-los-calzones/). Pero ninguna prosperó. Además, la tutela expresó que 59 de los congresistas que votaron por la reelección tenían conflicto de intereses

Para algunos analistas consultados por contagio radio, la decisión del Consejo de Estado favorece al propio Ordoñez porque lo convierte en víctima y eso podría significar un re lanzamiento de su imagen de cara a las próximas elecciones presidenciales, para las que ya suena su posible postulación por los sectores de derecha en Colombia.

Por otra parte surgen las incógnitas en torno a las decisiones que tomó Ordóñez al ejercer el cargo de [manera ilegal e inconstitucional](https://archivo.contagioradio.com/el-procurador-viola-abiertamente-la-constitucion-ivan-cepeda/), así como los procesos que se han abierto durante su segunda gestión al frente del ente estatal. Queda por establecerse si los actos quedarían también invalidados o siguen su curso cuando asuma el cargo, de manera interina, Marta Isabel Castañeda, actual vice procuradora.
