Title: Soledad y Crítica
Date: 2020-03-05 12:03
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: colombia, critica, pensamiento crítico, soledad
Slug: soledad-y-critica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/johan.mp3" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Que dolor tan grande sufre Colombia con ese pesado ruido de la infamia, con ese agobiante relativismo ético y su sentencia del “todo vale”; sufrimiento inculcado por tanto y tanto descarado, por tanta y tanta amargura obligada a sonreír a la fuerza o voluntariamente.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Qué dolor tan grande el de una patria que padece necesidades tan profundas

<!-- /wp:heading -->

<!-- wp:paragraph -->

¿Es el hambre la que mata? ¿es la sensación de que nadie puede hacer nada la que nos constriñe a aceptar como el más obediente de los esclavos que incluso hasta el amor es una transacción? 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Se trata de aceptar la vida de un mundo ya resuelto como si no hubiera nada por hacer?  ¿se trata de aceptar los estilos de vida según los ofrece el mercado, bien sea el estilo del individuo consumista y despilfarrador que dedica su obra a su propio ego mientras la realidad lo supera toda vez que la conciencia de su miseria lo condena a querer más y más y a importarle cada vez menos los demás?  ¿o de aceptar el estilo de vida de un ambientaveganista encerrado en las maneras de enseñar cómo sufre el animal (o a afirmar su estilo de vida) mientras disminuye las razones para advertir el sufrimiento humano o vegetal que, como formas de vida, adoptan grados defendiblemente más bajos que la defensa integral de un sufrimiento integral palpitante, hermosamente dialéctico y perpetuo? 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una patria donde los paramilitares han escrito la historia y próximamente la memoria, donde la verdad es esa mezcla de barro con aceite usado, donde la academia se pudre por dentro procurando mejorar sus edificios y mal logrando todos sus iniciales propósitos; la mentira es más cómoda, es más atractiva, más brillante, más rápida, la mentira en pocas palabras, pocas palabras para la sociedad mediocre, pocas palabras ante una imagen que vale mil de ellas… qué gran mentira los que aún no dan cuenta de cómo nos engañan las imágenes, eso *Joan Fontcuberta* lo había advertido. Podría seguir jurando en el cadalso, que mil palabras valen más que una imagen, pero qué dolor tan grande el que sufre un país acomodado incluso en la negación política y cotidiana de su propia lengua. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### El sentido crítico no emerge tan fácil.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Se necesita un sujeto dispuesto a asesinarse de manera figurada. Porque en esa muerte figurada muere todo el peso de la barbarie que quiere carcomerlo a punta de caricias y besos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sujeto vacilante que cree que entre más racional más bello. Sujeto vacilante que cree que entre más bello más aceptado o viceversa. Sujeto vacilante que cavila sobre su vacilación esperando el próximo momento para saltar a la escena de una opinión que no importará a tantos por más citada o limitada sea. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sujetos aglomerados y empujados a aumentar la densidad. Ya ni en las aceras se soportan, pero alimentan con gusto un secreto a vox populi, consideran su vida privada como privada mientras que aislados, el reflejo de sus soledades forma un gran espectro de coordinación y réplica hasta en el más íntimo acto. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El sujeto aislado constituye subjetivamente parte de una masa compuesta por sujetos aislados que pierden simultáneamente, pero a la vez aisladamente, la capacidad crítica ante la estimulación excitante de las series, las imágenes, los micro videos que los dopan durante horas de soledad. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La soledad ya no tiene su belleza marchita.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La soledad es una compañía nefasta de soledades compartidas que claman consumir lo que le muelan, incluso miedo. Siendo así, los aislados, esos sujetos, andan aislados, pero compartiendo exactamente el mismo estilo solitario, lo que dificulta el surgimiento del sujeto crítico que tanto clama Colombia. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Y qué del contenido que consumen los sujetos aislados en su soledad compartida? Multiplica lo mismo por 30 millones y obtendrás un potaje hegemónico. Desgarradora la violencia que vive Colombia, pero desgarradora también la soledad que consume el colombiano, una soledad sin cuerpo ni alma, una soledad guiada que no le permite ser, que no le permite pensar, que decapita una a una todas sus musas. Una contradicción del efecto de la soledad, que niega la reflexión y no le impide sacar una conclusión de todo lo bello que están asesinando en Colombia.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Rueda y rueda la información con contenido hegemónico a través de las pantallitas de teléfonos más o menos costosos, esto desarrolla en ese sujeto excitado noche a noche, la naturalización de ese contenido, la naturalización de las formas hegemónicas de ver el mundo, es decir negándose a ver la posibilidad, la alternativa, la revolución

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ¿Acaso eso no produce como consecuencia una subjetividad planificada, unas personalidades diferenciadas solo sobre la base de lo que se consume por muy obediente o desobediente que sea el producto? 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los caminos de respuesta a esa pregunta permitirán reconocer si existen o no las bases de una dominación política pasiva pero eficaz. Esa que nos asustó cuando mataron a uno y se calmaron millones. Esa que nos puso a discutir sobre las tipologías de la protesta, pero nos ocultó todas sus causas. Esa que nos bendice con el martirio de vivir la vida así porque así toca vivirla. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Por eso hoy el sujeto crítico es tan necesario en Colombia, para que comprenda tres cosas: 

<!-- /wp:heading -->

<!-- wp:list -->

-   la primera, que los campos de lucha tradicionales, contemporáneos y determinantes serán siempre las calles, serán siempre las plazas públicas.

<!-- /wp:list -->

<!-- wp:paragraph -->

No importa lo que diga Sergio Fajardo, la política es de plaza, no importa lo que los onmicontentos quieran imponer, la política es de masas pasionales y violentas; el concepto “masa crítica” es una traición a toda nuestra historia, porque aunque las masas son intempestivas y violentas, tienen la gran capacidad de tener el destino de la sociedad en sus manos durante esas pocas horas en las que todo cambia. Cosa compleja es que hoy las masas, también están compuestas por sujetos aislados. Callar la boca y atormentar los pensamientos de cualquier intelectual es una de las funciones históricas de la violencia y las pasiones políticas, por eso el intelectual siempre será mejor que esa masa histriónica y violenta, pero esa masa será por siempre más pragmática que todos y cada uno de sus tratados.  

<!-- /wp:paragraph -->

<!-- wp:list -->

-   La segunda: que un pueblo en las calles puede arrinconar a cualquier gobierno.

<!-- /wp:list -->

<!-- wp:paragraph -->

Pero ese pueblo no saldrá a la calle sin que exista un puñado considerable de sujetos críticos que conduzcan la salida. Un sujeto solitario pero preso de las alucinaciones colectivas percibidas de manera aislada no es más que un número más. Hace falta esa capacidad de discernimiento, hace falta ese gran rechazo a la hegemonía política que viaja en empaque placentero y llega todas las noches a los ojos de cada individuo. Hace falta replantearnos la vivencia misma de nuestra cotidianidad. 

<!-- /wp:paragraph -->

<!-- wp:list -->

-   La tercera: el sujeto crítico debe plantearse nuevos mundos posibles, de placer, de violencia, de paz, de liberación política, de liberación del tiempo libre.

<!-- /wp:list -->

<!-- wp:paragraph -->

¡El tiempo libre parece que ya no es nuestro! ¡y voluntariamente se está aceptando esa derrota! … la lucha del sujeto crítico no es dolorosa, pero sí genera resistencias entre quienes gozan del cómodo engaño, pues el poder de esa enorme masa de individuos aislados no es racional, es decir el sujeto crítico en Colombia podría sufrir pena moral o desasosiego al ser apartado de forma cruel y traumática por su protesta impresa en lo cotidiano. Es su constricción, es el duelo que debe superar, es la primera lucha que debe dar. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Patria existente o inexistente.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los racionalistas dicen que la patria es un mito, sin darse cuenta de que la razón también lo es… si… lo es… cuando le diga que el burro es negro es porque tengo los pelos en la mano.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Patria existente como sueño lejos de la tiranía de esta democracia totalitaria. Patria existente como Matria, ¿Matria o Patria? Matria de una nueva vida embarazada del sueño eterno de una mejor sociedad. Patria de una nueva vida que fecunde sueños que nos ayuden a caminar un poco más allá de nuestra distraída soledad y permita construir sujetos críticos, aquí, ahora y siempre por los siglos de los siglos abracadabra.   

<!-- /wp:paragraph -->

<!-- wp:audio {"id":81691,"align":"center"} -->

<figure class="wp-block-audio aligncenter">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/johan.mp3">
</audio>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

Ver mas: [Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
