Title: La deforestación uno de los mayores riesgos para los animales en Colombia
Date: 2018-10-04 17:28
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Animales, colombia, corridas de toros, deforestación
Slug: la-deforestacion-uno-de-los-mayores-riesgos-para-los-animales-en-colombia
Status: published

###### [Foto:WWF] 

###### [04 Octu 2018] 

En el día mundial de los animales, Natalia Parra, vocera de la plataforma animalista ALTO, afirmó que si bien Colombia es uno de los países con más biodiversidad en el mundo, necesita avanzar en la protección de los animales y sus ecosistemas, con **medidas que eviten la deforestación o la puesta en marcha de proyectos que pongan en riesgo la fauna del país. **

Según Parra, si bien es cierto que hemos tenido avances en aspectos culturales que han cambiado, políticas públicas que se han implementado y pasos legislativos hacia la protección de los animales, aún falta mucho, "seguimos reproduciendo prácticas en donde los animales son vulnerados, no hemos podido superar la situación de los animales en condición de calle y **el tráfico sigue siendo uno de los yugos que cae sobre la fauna" afirmó. **

Otro de los grandes riesgos para los animales, que el movimiento animalista ya había advertido al Ministerio de Ambiente, **tiene que ver con el fin del conflicto y las miles de hectáreas que ahora están siendo deforestadas**, sin que hayan medidas que protejan los territorios.

"Cuando se van las FARC, multinacionales, ganaderos, todo el mundo, aprovecha para tomar estos espacios y correr cercas que generan conflictos con las especies que están acostumbradas a circular por esas partes donde antes había un ecosistema y ahora son potreros para vacas" afirmó Parra.

Además, la animalista aseguró que, a pesar de tener legislación en favor de la protección de los animales, no se aplica  y agregando que "el problema es que aveces quienes ejecutan las políticas no son los más idóneos". (Le puede interesar:["Animales los más afectados con el derrame de petroleo en Barrancabermeja"](https://archivo.contagioradio.com/animales-los-mas-afectados-con-el-derrame-de-petroleo-en-barrancabermeja/))

### **Las corridas de toros un retroceso en la protección a los animales** 

Para Parra, uno de los retrocesos más grandes que ha dado Colombia este año en la protección a animales, tiene que ver con el fallo de la Corte Constitucional sobre las corridas de toros, debido a que reverso una sentencia en la que prohibía actividades en donde se maltratará **animales como las novilladas, las peleas de gallos y las corridas de toros. **

Frente a este hecho el movimiento animalista señaló que continuará realizando incidencia para garantizar la protección de cualquier animal y lograr la prohibición de las corridas de toros, debido al maltrato del que es víctima esta especie. (Le puede interesar: ["Las corridas de Toros: muy costosas y no dejan beneficios para Bogotá](https://archivo.contagioradio.com/corridas-toros-costosas-bogota/)")

<iframe id="audio_29120772" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_29120772_4_1.html?c1=ff6600"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
