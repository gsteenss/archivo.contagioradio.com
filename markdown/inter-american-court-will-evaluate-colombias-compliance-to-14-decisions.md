Title: Inter-American Court will evaluate Colombia's compliance to 14 decisions
Date: 2019-09-05 11:41
Author: CtgAdm
Category: English
Tags: Inter-American Court on Human Rights, international law, Lawyers' Collective José Alvear Restrepo
Slug: inter-american-court-will-evaluate-colombias-compliance-to-14-decisions
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/33604591058_2b32d1473b_k-e1567630386414.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Photo: Corte IDH] 

The Inter-American Court of Human Rights will conduct hearings on Sept. 6 and 7 to evaluate Colombia's compliance to 14 of the 22 verdicts the international tribunal has emitted against the Andean country. According to the Lawyers’ Collective José Alvear Restrepo (CAJAR), the Court will revise cases that occurred between 1985 and 1998, which affects 96 direct victims of forced disappearances, 77 extrajudicial killings, 2 homicide attempts and one case of torture.

### **The Court goes to Colombia**

As part of the process, the Court took the unusual decision of conducting public hearings in Colombia. Jomary Ortegón[, a lawyer for CAJAR, explained that these type of hearings typically take place in Costa Rica or other invited countries. Her best guess as to why the Court may have decided otherwise is that the tribunal may need additional evidence to determine whether Colombia is complying with its verdicts.]

[She added that another reason may be that the Court is responding to Colombia's request to verify that the country is following some of the tribunal's orders.]In any case, the location of the public hearing will allow victims to testify — usually an inaccessible option for many when the hearings are conducted outside the country where the case is taking place.

[In another rare decision, the Court will also group various cases together. This Thursday, Sept. 5, the Court will examine the cases of forced disappearances in Las Palmeras, Pueblo Bello, La Esperanza, Isaza Uribe, 19 comerciantes and Caballero Delgado and Santana. According to the lawyer, the Court will decide how Colombia has advanced in the search, identification and the return of bodies to family members. ]

[The Court will also look at cases in which the tribunal ordered the Colombian state to provide victims with free medical and psychosocial treatment close to their places of residence. Other cases such as the Palace of Justice, the Massacre of Mapiripán and of Palmeras will each have individual hearings.]

After the hearings, the Court will make a decision about the compliance of the state to its decisions. Ortegón added that one of the cases that will especially be under the observance of the Court will be the Palace of Justice. The Court ordered the creation of a search plan, but the state has failed to comply.

### **The state has advanced in basic issues, but not structurally**

The lawyer argued that the state has followed certain orders that demand the indemnification of victims and public apologies, but in terms of administering justice and searching for disappeared persons, the state has largely failed. These judicial cases advance slowly, Ortegón said, meanwhile the state has remained absent from the search to find the disappeared.

This conflict is expected to play in the second group hearing that will take place this Thursday on medical and psychosocial support. Ortegón said that the State insists that medical and psychosocial attention should be given through the general medical system while the victims argue that if they pay to take part in this system, then they are paying for their own reparations.

Considering this support should be free, Ortegón said she expects the Court to understand that the compliance of the state has been insufficient. "It hasn't allowed for the re-establishment of rights and it has not repaired the victims."

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
