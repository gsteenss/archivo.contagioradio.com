Title: Brasil alcanza cifra récord de desempleo con Temer
Date: 2016-12-29 16:40
Category: El mundo, Otra Mirada
Tags: Brasil, crisis, desempleo, Michel Temer
Slug: desempleo-brasil-temer
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/desempeleo-brasil.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Am La Salada 

##### 29 Dic 2016 

El ascenso en las cifras de desempleo en Brasil vienen alcanzando topes históricos. Según reporta el Instituto Brasileño de Geografía y Estadística ([IBGE](http://www.ibge.gov.br/espanhol/)) **12.1 millones de personas en la actualidad se encuentran sin trabajo en el pais más grande de Suramérica**, lo que corresponde a un incremento de 11.9% entre los meses de septiembre y noviembre.

Son un total de cien mil nuevos desempleados en Brasil, un recórd histórico que ocurre en el gobierno del presidente no electo Michel Temer, que se suman a las 3 **millones de personas que no consiguieron trabajo durante 2016**, lo que equivale a un aumento del  33,1%; crecimiento que se equipara al rechazo a la figura de Temer que, de acuerdo a una medición de Ipsos, ya alcanza el 77%.

De acuerdo con el análisis de la entidad de estadística, las cifras **dan cuenta del deterioro del mercado laboral en este año**, frente al mismo período del año pasado, al haber aumentado el desempleo tres puntos porcentuales más que en 2015 y **se convierte en la tasa más alta desde 2012**, cuando Brasil cambió la forma en que mide la desocupación.

Analistas aseguran que **la situación laboral es** r**eflejo de la crisis económica y la recesión que atraviesa el país**, y que están relacionadas con la desestabilización política ocurrida tras la destitución de la presidenta Dilma Rousseff, que se refleja en la poca aprobación que la población tiene del mandatario interino y sus políticas de gobierno.

Temer ha insistido que Brasil vencerá la crisis en 2017, a partir de **medidas regresivas como el congelamiento del gasto público por 20 años**, y la ampliamente rechazada [reforma al sistema de pensiones](https://archivo.contagioradio.com/jubilacion-trabajadores-brasil/), y aseguró que trabajara por la aprobación de las reformas tributaria y laboral e "incentivará" cambios en el sistema político.

Mientras tanto, los **escándalos políticos siguen salpicando a funcionarios de su gobierno por casos de corrupción** que podrían causar su retiro del cargo  para el cual no fue elegido por el pueblo Brasileño. Le puede interesar: [Movimientos populares piden impeachment contra Temer](https://archivo.contagioradio.com/movimientos-populares-impeachment-temer/).
