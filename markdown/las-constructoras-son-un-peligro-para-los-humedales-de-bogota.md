Title: Las constructoras son un peligro para los humedales de Bogotá
Date: 2015-02-02 19:40
Author: CtgAdm
Category: Ambiente
Tags: Ambiente, Bogotá, Humedales, La Conejera
Slug: las-constructoras-son-un-peligro-para-los-humedales-de-bogota
Status: published

##### Foto: humedalesbogota.com 

##### [Jorge Escoba]r<iframe src="http://www.ivoox.com/player_ek_4029378_2_1.html?data=lZWfm5ibfI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRjtDmyMqYp9jHs8PV05KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [ ] 

**Este 2 de febrero se celebra el día mundial de los humedales** en conmemoración a que ese día en 1977, se firmó el Convenio sobre los Humedales en Ramsar, al cual hace parte Colombia, que se caracteriza por tener enormes extensiones de humedales que suministran varios bienes y servicios para el desarrollo de las actividades económicas, así como a las comunidades locales.

Aunque estos ecosistemas representan diversidad biológica, regulan el ciclo del agua y del clima y además son generadores de recursos hídricos; **en Bogotá la situación de algunos humedales es preocupante y tiene varios "puntos negativos**", como lo afirma Jorge Escobar, director de la fundación humedales Bogotá.

**“La Conejera” y “Tibabuyes”, por dar algunos ejemplos, son humedales que se ven afectados por la presión  de las constructoras**, debido a que en la ciudad cada vez hay menos suelo urbanizable. Así mismo, “existen casos puntuales donde se sigue presentado pastoreo y especies invasoras” señala Escobar, sin embargo **las constructoras siguen representando el mayor impacto para estos ecosistemas.**

Bogotá cuenta con **16 humedales,** y aunque hay varios  que están en peligro, se ha logrado recuperar  algunos. Para que se siga fortaleciendo esta tarea, la **Fundación Humedales Bogotá realizará este 7 de febrero, la tercera versión de la Bicicaravana**, donde los asistentes tendrán la oportunidad de recorrer varios ecosistemas de la ciudad,  para conocer sus riquezas y amenazas con el objetivo de que se apropien de ellos.

El tema de la Bicicaravana de este año es **“Humedales para nuestro futuro”** por ésta razón la Fundación invita a todos los ciclistas a hacer parte de este evento donde también habrá varias actividades culturales.

Para mayor información sobre este evento,

[www.humedalesbogota.com/bicicaravana/](http://humedalesbogota.com/bicicaravana/)

311 500 91 92

<info@humedalesbogota.com>
