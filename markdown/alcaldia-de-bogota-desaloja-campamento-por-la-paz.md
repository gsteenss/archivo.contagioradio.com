Title: Alcaldía de Bogotá desaloja Campamento por la paz
Date: 2016-11-18 13:42
Category: Nacional, Paz
Tags: Alcaldía de Bogotá, Campamento por la Paz, Desalojo, Salsa al Parque
Slug: alcaldia-de-bogota-desaloja-campamento-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/campamento-por-la-paz-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 Nov. 2016] 

El Campamento por la Paz que lleva más de 45 días en la Plaza de Bolívar, **tendrá que retirarse de este lugar luego de que la alcaldía de Bogotá escogiera este sitio como epicentro del festival Salsa al Parque.**

Una de las primeras medidas que ha tomado la administración de Enrique Peñalosa, ha sido **quitar el vallado que protegía a las personas que acampan en el lugar. **Sin embargo los campistas han decidido reemplazar esa valla por telas blancas que les permitan unos mínimos de seguridad.

Según Arie Campella vocero del Campamento por la Paz, se sostuvo una reunión con personas de la Alcaldía quienes les manifestaron debían salir de la Plaza de Bolívar para permitir desarrollar este evento, que en años anteriores se había llevado a cabo en la Parque Simón Bolívar. Motivo por el cual, los campistas realizaron una asamblea general en donde mayoritariamente decidieron mantenerse en la Plaza hasta que se implementen los Acuerdos de Paz.

De otro lado un grupo de campistas, en cabeza de Katherine Duque, esposa del concejal Jorge Torrres,  se retiraron del lugar, ya que "consideran que se ha cumplido el objetivo principal del Campamento por la paz y que ahora existen otros retos y lenguajes que asumir", pasando por encima de la decisión asamblearia y ordenando que se quitaran las carpas en donde estaban los alimentos y los baños.

Arie manifestó, quien se encuentran aún acampando, van a hacer todo lo posible por quedarse en la Plaza de Bolívar. Le puede interesar: [Presidente atiende llamado de Campamento por la Paz](https://archivo.contagioradio.com/santos-campamento-por-la-paz/).

De otro lado, se había mencionado que el jefe del equipo negociador del gobierno Humberto de la Calle haría presencia en el Campamento por la Paz para hacer entrega oficial a los campistas de los Acuerdos, sin embargo el encuentro aún no se ha dado.

> Marta Gomez miembro de [@CampamentoPaz](https://twitter.com/CampamentoPaz) afirma que continuarán [pic.twitter.com/NzWHZZ2H72](https://t.co/NzWHZZ2H72)
>
> — Contagio Radio (@Contagioradio1) [18 de noviembre de 2016](https://twitter.com/Contagioradio1/status/799684279756320768)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
