Title: "Los acuerdos son para cumplirlos" Comunidades negras del Cauca al Gobierno
Date: 2019-06-17 18:49
Author: CtgAdm
Category: Comunidad, Nacional
Tags: ACONC, Cauca, Minga Nacional, Proceso de Comunidades Negras
Slug: los-acuerdos-son-para-cumplirlos-comunidades-negras-del-cauca-al-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Comunidades-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: PCN ] 

Tras la reunión del pasado 16 de junio en **Santander de Quilichao** entre representantes del Gobierno, organizaciones garantes y representantes de organizaciones como la Asociación De **Consejos Comunitarios del Norte Del Cauca (ACONC) y el Proceso de Comunidades Negras (PCN)**, los líderes y lideresas sociales denunciaron que el acta final suscrita entre ambas partes había sufrido modificaciones sin su consentimiento.

**Clemencia Carabalí, parte de la Asociación de Mujeres Afrodescendientes del Cauca**, explica que cuando se retomó la reunión el pasado 16 de junio, al llegar la directora de Asuntos Étnicos con parte del equipo del **Ministerio del Interior**,  el acta con lo inicialmente pactado no correspondía en algunos puntos a lo que se se había concertado originalmente, lo que causó malestar entre las comunidades pues según la lideresa, resultaba un "desconocimiento a los acuerdos".

> Un día de desgaste y mala fe por parte del Gobierno, la Ministra del Interior [@NancyPatricia\_G](https://twitter.com/NancyPatricia_G?ref_src=twsrc%5Etfw) incumple su palabra, firmamos un acta el 6 de junio y la cambiaron, los acuerdos son para cumplirlos Presidente [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw). Les esperamos mañana con el acta firmada. [@fcarrilloflorez](https://twitter.com/fcarrilloflorez?ref_src=twsrc%5Etfw) [pic.twitter.com/RhiZfIT4dB](https://t.co/RhiZfIT4dB)
>
> — Francia Márquez Mina (@FranciaMarquezM) [16 de junio de 2019](https://twitter.com/FranciaMarquezM/status/1140062241372409857?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Representantes y líderes de las organizaciones acordaron no continuar con la agenda hasta que no fueron corregidos los cambios en el acta, **"es una muestra de la poca seriedad y de no querer asumir  lo acordado",** expresó Carabalí, señalando que no puede existir confianza si incluso estando los garantes del acuerdo, una de las partes hacía cambios en lo acordado. [(Le puede interesar: "Señor Presidente, lo estamos esperando en Santander de Quilichao": ACONC)](https://archivo.contagioradio.com/senor-presidente-lo-estamos-esperando-en-santander-de-quilichao-aconc/)

### Cambios evidenciados por las comunidades negras 

Al comparar ambas actas, son evidentes algunos cambios que fueron realizados en la versión final del Ministerio, específicamente en secciones donde se excluyeron apartados que aclaran que, "una vez adelantado el proceso de la sincronización de las actas, no se excluirá ningún acuerdo de los que se encuentren establecidos en las mismas".

También se excluye un apartado en el se especifica que de los días 16 a 19 de julio se convocará una reunión en Bogotá para decidir sobre el fortalecimiento de la Guardia Cimarrona. [(Lea también: Comunidades buscan acuerdos humanitarios para acabar con asesinatos de líderes)](https://archivo.contagioradio.com/comunidades-acuerdos-humanitarios-acabar-asesinatos-lideres/)

**La lideresa Francia Márquez** cuestionó las declaraciones de la ministra del Interior quien afirmó que solo asumirán los compromisos suscritos con el Gobierno actual, "me pregunto si tampoco van a cumplir los acuerdos y tratados internacionales, por que estos también fueron pactados con Gobiernos anteriores".

> Para nosotros como gobierno [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) la palabra es sagrada. No nos comprometemos a lo que no podemos cumplir. Por eso, no podemos refrendar compromisos del pasado, sin saber de qué se trata. Este es un gobierno serio. <https://t.co/14JS4D8GPl>
>
> — Nancy Patricia G (@NancyPatricia\_G) [16 de junio de 2019](https://twitter.com/NancyPatricia_G/status/1140073128850329600?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Aunque accedió a firmar el acto como se acordó, la ministra del Interior realizó unas salvedades de las que notificó a las organizaciones sociales,  ahora, según Clemencia Carabalí se debe continuar construyendo la matriz del acuerdo conforme al compromiso adquirido "en aras al respeto mutuo". [(Le puede interesar: "Muchos nos ven como una piedra en el zapato por defender el territorio" ACONC)](https://archivo.contagioradio.com/muchos-nos-ven-como-una-piedra-en-el-zapato-aconc/)

> Presidente [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw), [@MinInterior](https://twitter.com/MinInterior?ref_src=twsrc%5Etfw) firmó acta. El pueblo negro del norte del Cauca [@renacientes](https://twitter.com/renacientes?ref_src=twsrc%5Etfw), [@aconcolombia](https://twitter.com/aconcolombia?ref_src=twsrc%5Etfw), [@ASOMBUENOSAIRES](https://twitter.com/ASOMBUENOSAIRES?ref_src=twsrc%5Etfw), dispuesto a seguir trabajando con el Estado colombiano, señores [@PGN\_COL](https://twitter.com/PGN_COL?ref_src=twsrc%5Etfw), [@DefensoriaCol](https://twitter.com/DefensoriaCol?ref_src=twsrc%5Etfw) estamos construyendo confianza. [pic.twitter.com/LyBRRlWdwC](https://t.co/LyBRRlWdwC)
>
> — Victor Hugo Moreno Mina (@VictorMorenoMin) [16 de junio de 2019](https://twitter.com/VictorMorenoMin/status/1140293672082313217?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
