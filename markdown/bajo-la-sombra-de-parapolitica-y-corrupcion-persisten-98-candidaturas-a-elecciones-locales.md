Title: En 27 departamentos persisten candidatos vinculados a parapolítica y corrupción
Date: 2019-08-09 18:26
Author: CtgAdm
Category: Nacional, Política
Tags: corrupción, Elecciones 2019, parapolítica
Slug: bajo-la-sombra-de-parapolitica-y-corrupcion-persisten-98-candidaturas-a-elecciones-locales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/parapolitica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Punto Rojo Agency] 

Previo a las elecciones locales de 2019,  la Fundación Paz y Reconciliación dio a conocer el informe **"Candidatos Cuestionados 2019"**, que revela las inconsistencias de un total de 98 candidatos que han sido vinculados a parapolítica, corrupción o relaciones con fuerzas ilegales y que pese a ellos hoy están postulados a alcaldías o gobernaciones en 27 departamentos del país.

Según **[el informe](https://pares.com.co/wp-content/uploads/2019/08/Informe-Candidatos-Cuestionados-2019-1.pdf)**, son 53 los candidatos ligados a corrupción, mientras son 38 los herederos de la parapolítica y otros 7, candidatos con vínculos a diversos actores armados. [(Lea también: Procuraduría suspende a Fabio Otero, alcalde de Tierralta, Córdoba)](https://archivo.contagioradio.com/procuraduria-suspende-a-fabio-otero-alcalde-de-tierralta-cordoba/)

Se evidenció que muchos de los candidatos cuestionados han acudido a las firmas y a coaliciones para participar en estas contiendas, lo que según el informe les da un "aire ciudadano a las candidaturas" y les permite iniciar la campaña con antelación, mientras los clanes y partidos que los apoyan se mueven tras bambalinas sin exhibir sus nombres.

Para León Valencia, director de Paz y Reconciliación, la muestra de estos 98 candidatos con cuestionable trayectoria es un reflejo de la impunidad judicial, política y social que se vive en Colombia, **"la impunidad empieza a por los directores de partidos que dan avales a estos actores, hay una responsabilidad de los aparatos judiciales"**.

El informe también hace referencia a un nuevo fenómeno que ha surgido como es el de uso de partidos de origen étnico como fachadas, detrás de las que se esconden viejos parapolíticos que reparten numerosos avales como es el caso de  la Alianza Democrática Afrocolombiana, que con sólo tres meses de existencia avaló a 3.500 candidatos.

"Es un burladero de la democracia (...), allí hay una cosa que investigar a fondo y cuál va a ser el avance de ese partido durante estas elecciones" concluye Valencia.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_39776365" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_39776365_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
