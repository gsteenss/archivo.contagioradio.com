Title: Indígenas de Chocó confinados por presencia paramilitar
Date: 2018-07-23 12:54
Category: DDHH, Nacional
Tags: AGC, Chocó, Curvarado, indígenas, Jiguamiandó
Slug: indigenas-confinados-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/jiguamiando.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<header class="post-header">
###### [Foto: Archivo Contagio Radio] 

###### [23 Jul 2018] 

Líderes del Resguardo Urada Jiguamiandó, en el Bajo Atrato chocoano, denunciaron que desde el sábado 21 de julio, **cerca de 40 neoparamilitares de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC), ingresarón al resguardo** vestidos de camuflado y con armas largas, manteniendo a los indigenas confinados en el territorio.

Según el comunicado de la Comisión Intereclesial de Justicia y Paz, **los integrantes de las AGC continuaron en la zona hasta las 7 de la noche del sábado,** pero debido a las malas condiciones de la comunicación,  no se ha podido establecer hasta el momento que ha ocurrido con la comunidad indígena.(Le puede interesar:["121 familias se encuentran confinadas por paramilitares: Pueblos Indígenas"](https://archivo.contagioradio.com/121-familias-se-encuentran-confinadas-por-paramilitares-pueblos-indigenas/))

Una vez conocida la denuncia, la organización de derechos humanos puso en conocimiento la información al Gobierno Nacional, aclarando que, hasta el momento **"no hay acciones concretas y específicas que identifiquen la situación y actúen"** para solucionarla. (Le puede interesar:["66% de los pueblos indígenas está a punto de desaparecer: ONIC"](https://archivo.contagioradio.com/pueblos-indigenas-a-punto-de-desaparecer-onic/))

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]

</header>
<div class="gdlr-blog-content">

</div>
