Title: Manuel Cepeda y la memoria en sus poemas
Date: 2016-08-09 14:54
Category: Sin Olvido
Tags: literatura, memoria, Sin Olvido
Slug: manuel-cepeda-y-la-memoria-en-sus-poemas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/manuel-cepeda-vargas-poemas.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sin Olvido 

###### 9 Ago 2016

Se cumplen 22 años del asesinato del, poeta, pintor y político **Manuel Cepeda Vargas**, senador e integrante de la Unión Patriótica. Para muchos de sus amigos Manuel fue más que eso, fue un humanista que hizo de la indignación por las múltiples injusticias una llama que reavivó la esperanza de construir un país mejor. Desde el Congreso impulsó  el estatuto de la oposición que sigue en discusión 22 años después. Y desde la vida y con la poesía sembró también la memoria.

Aquí cinco de sus múltiples poemas para continuar regando la memoria con la esperanza.

### **Ave Fénix** 

¿Y por qué  
no escapaste  
cuando cayeron Jaime,  
Leonardo,  
Miller?  
¿Cuando Teófilo partió en un viaje sin retorno  
y Antequera entró en el aeropuerto  
y Bernardo lanzó su última sonrisa?  
Y cuando aquel desconocido (labriego, obrero, estudiante)  
dijo: -Aquí luché, aquí muero.-  
Y murió.

No hablamos de la cacería del tigre  
ni de la flecha contra el águila  
sino de un genocidio a la luz del día,  
del racimo entregado al sacrificio,  
de abuelos, nietos, hijos, madres  
que en vano anhelaron tu regreso.

… Los árboles  
transmiten sus semillas  
en el aire.  
Las lanzas de Bolívar,  
las luciérnagas  
de Policarpa  
y el aullido  
de los libertadores  
resucitan con niebla  
en la hondonada.

Tras la pared de fusilados  
invencibles retoñan  
los geranios  
y deidades anónimas  
bautizan callejuelas  
del mercado.

¿Quizás el exilio  
nos habría salvado?  
¿Tal vez el asilo  
preservaría a Jaime y a Leonardo  
cuando el palacio  
armó la mano del esbirro?

Dejemos al examen del futuro  
el error de la lucha, si era bueno  
acudir a la cita, si era mejor agazaparse, si el tejido  
del tapete gigante debería  
llevar un hilo de oro o de topacio  
en lugar del cabello aniquilado.

Pero  
no teorices  
sin medir el esfuerzo, la titánica  
labor que hace la ola en el océano  
ni consigas el titulo con tesis  
bien (mal) pagadas  
sobre la tumba de los mártires.  
Sube un rayo  
de luz anunciadora,  
una espuma veraz  
del fondo oceánico  
desde la sencillez  
de Jaime  
Pardo.

------------------------------------------------------------------------

### **Los caballitos de la semana** 

Siete caballitos tienen la semana  
Y no se sabe cual va más cargado de gente  
Cuál va más tísico  
más cercano a la tumba.

Cada hora es un día,  
cada día un año,  
cada año es un siglo.

Buen día, mal día caballito,  
Espero que los hombres te envidien la hombría  
Que la muerte tenga piedad de ti.

Eres obrero sin derecho de huelga,  
Prohibido espantarte,  
Prohibidos pactos colectivos  
Y mucho menos pliegos petitorios.

Estamos en la pre- esclavitud.

Y una locomotora arrastra todos los vagones  
Y el humo oscurece el rostro del caballo de fuerza,  
Tomo mi café  
Como mi pan  
Y oigo cómo relinchas en cada miligramo de alimento.

Siete días tiene el padecimiento,  
Siete caballitos  
Arrastran el carro de la muerte.

------------------------------------------------------------------------

### **AMOR SALVAME DE LA PREHISTORIA** 

Hallo tus fotos  
caídas entre los libros,  
clavadas en la espalda de mi casa,  
atravesándome la frente.  
Te veo, reportera,  
tapándote de la ventana cruel,  
huyéndole al viento verdugo.  
Y allí vas  
saltando charcos,  
retratando a los niños,  
descifrando la esfinge suburbana.  
Mariposa árabe:  
sácame de la prehistoria,  
rescátame del diluvio universal.  
Y preguntas. Preguntas mucho.  
Indagas existencias  
hasta hacerlas cantar en tus crónicas  
y vuelves a salir  
(no esperes, llego tarde)  
a la calle apoteósica.  
Afuera Bogotá nocturno  
emprende comunidades estelares  
en su tapíz asiático.  
Asciendes  
barriadas ventisqueros  
y luego:  
“Hoy visite Lucero Alto,  
pasé por las Colinas,  
cómo llovía en el Meissen”.  
Entrégame la flor de la suerte  
trébol  del nomeolvides  
fluvial Ofelia en pos de Hamlet.  
Cierra la ventana por Dios:  
el sacrílego viento no me deja dormir.

------------------------------------------------------------------------

### **NEOVIEJO IDIOMA** 

Cuando aparece (fenece) un mundo  
el idioma se cambia, los sentidos se alteran,  
dicen los reyes: tiranía es esa diosa libertad,  
la justicia es injusta y lo injusto es justicia  
y tú rebelde cuenta los escalones del patíbulo.  
Y la hoguera devora a los infieles.  
Algo debe estar gritando abajo  
remontándose en sombras hasta la luna,  
porque llaman vetusto a lo reciente,  
modernidad a vamos al pasado,  
reaccionario a lo nuevo  
y derecha a la izquierda.  
Nuevo hipócrita idioma  
cambiaron los sentidos, los colores, los días,  
lo liquido detúvose, se evaporó lo inmóvil  
y una esperanza surge entre el desastre:  
que el pueblo vuelva  
y ruede la baraja  
y salga el astro rey  
de entre los naipes.

------------------------------------------------------------------------

### **ESTO VA A SER PARA LARGO** 

Esto va a ser para largo.  
Que así lo entienda la gente.  
No van a desensillarnos  
de repente.  
Codos con codos con codos,  
frentes con frentes con frentes,  
despierten si están dormidos,  
indiferentes,  
Que aquí va a haber, para todos  
postes y pestes,  
y largo va a ser, muy largo.  
Noches y noches, y noches.  
Inmensamente.

Si por mártires inermes  
se hicieron las patrias libres,  
¡libertad como la yerba  
tuvieses, triste!  
Y si con un muerto hallases  
Lo que en el muerto perdiste,  
¡300.000 muertos tuyos  
están diciendo que están  
muertos en el fondo, triste!

¡Armas con armas con armas  
para ti, triste!  
¡Armas con armas con armas  
Que así naciste!

Tiembla al fondo de la noche  
una cantina en penumbras.  
Allá voy.  
Cobres sonidos inmóviles  
bajo una campana hondísima  
piden amor.  
Los suelos, yertos, verdecen.  
Las mesas, en un rincón,  
mientras que por la cantina  
vuela, errante golondrina,  
esta canción:  
-A las farolas azules  
quiero volar, lentamente.  
Esto va a ser para largo  
y el ultimo tren paso  
ya, tuertamente.

Iba el coche comedor  
separando los dos coches.  
El de primera dejándose,  
el de tercera al ataque,  
hollín, y toses.  
Y por donde resbalaba  
un humo negro emitía  
que le tapaba la cara  
al ultimo tren sonámbulo.  
Y maldecía.

No sé quien me trajo aquí  
que así me duele la vida.  
Me duele, y sé que ya quiere  
brindarme su despedida.  
Sólo le pido me lleve  
la nave de su partida  
pues tengo que irme muy lejos,  
muy lejos,  
para saber qué es la vida.

Hay un ave por la tierra  
hostia en rojo, Libertad.  
Cuando los hombres la pierden  
no son ya hombres de verdad.  
No sé cuándo la he perdido  
y ella me viene a llamar  
en un rostro que es el rostro  
sol de una nube solar.  
Hay un ave, fuego al rojo  
por la tierra, Libertad.

Quiero preguntarle cosas,  
A todos los presidentes:  
-no se cansan de comer  
señores dientes?  
No se cansan de beber  
sangre caliente?

Un año nuevo llega,  
banco y banquete:  
gentes con gentes con gentes  
con gentes, gentes, con gentes.

Sabemos que en el Tolima  
los hombres están muriendo.  
Sabemos que en Tierradentro  
Están los hombres muriendo.  
Pero de qué enfermedad  
se están los hombres muriendo?

Sabemos que en las brigadas  
dan sentencia de colonias  
pero no se abren las alas  
con tal historia.  
Por qué no se abren las alas,  
si hay más campo y más gloria?

¡Estudiantes, estudiantes!  
Masacres!  
Masacres!  
Masacres!

¡Campesinos, campesinos!  
Sangre!  
Sangre!  
Sangre!

¡Obreros, obreros, obreros!  
Cárceles!  
Cárceles!  
Cárceles!

¡Soldados, soldados, soldados!  
Cárceles!  
Sangres!  
Masacres!

Aquí me duele la vida:  
aquí, en mitad de mi pecho.  
No soy más que un hombre más,  
pero me duele mi pecho  
de ver como nos confunden  
con basura y con afrecho.  
No soy más que un hombre más,  
pero me duele mi pecho!

Antes que el ave despunte  
cuánta sangre por los puentes.  
Tendrán que armarse con armas  
los inocentes  
codos con codos con codos,  
frentes con frentes,  
despierten si están dormidos,  
indiferentes,  
que aquí va a haber para todos  
postes, y pestes,  
y ésto va a ser para largo:  
noches, con noches, con noches.  
Inmensamente.
