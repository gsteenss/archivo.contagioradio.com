Title: La fuerza pública es el principal perpetrador de los casos de tortura en Colombia
Date: 2017-06-26 00:20
Category: DDHH, Nacional
Tags: Derechos Humanos, tortura
Slug: en-los-ultimos-5-anos-se-han-reportado-casos-de-tortura-en-141-paises
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Tortura-stop-e1466918178683.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ctg Chile 

###### [26 Jun 2017] 

Este lunes se conmemora el Día Internacional de Apoyo a las Víctimas de la Tortura. Una fecha en la que se sigue alertando sobre el aumento de casos de tortura, y se llama la atención sobre los altos índices de impunidad, siendo los propios estados quienes amparan este tipo de prácticas, como lo señala la ONU.

Amnistía Internacional indica que en los últimos cinco años ha habido **reportes de tortura en 141 países; es decir, en tres cuartas partes del mundo**, lo que significa que no han sido suficientes las medidas tomadas por los gobiernos para enfrentar esta situación. Según esta organización, “**el aumento de la tortura se debe no tanto al irrespeto de las leyes sino a que muchos gobiernos la utilizan activamente,  o peor aún, en muchos casos la amparan”.**

Desde la ONU se afirma que "**esta práctica deshumanizante sigue siendo generalizada y, de manera aún más preocupante, está incluso ganando aceptación”.** Es por eso que desde la ONU se ha instado a los gobiernos a "velar por que las víctimas de la tortura bajo su jurisdicción obtengan reparación", según establece la Convención Contra la Tortura, de la que hacen parte 159 estados.

Este tipo de violación a los derechos humanos, implica acciones que rompen el tejido social, como la discriminación y otro tipo de ataques por motivos de género, raza, creencias religiosas, ideologías políticas, condición física y económica. Incluye también la detención, desplazamiento y desaparición forzado, maltrato, ejecución extrajudicial, violencia sexual y amenaza. (Ver: [“Gobierno debe investigar abusos cometidos por el ESMAD” Congresistas de EEUU](https://archivo.contagioradio.com/gobierno-debe-investigar-los-abusos-cometidos-por-la-policia-de-esmad-congresistas-de-eeuu/))

### En Colombia 

La situación en el país sigue siendo alarmante. La Coalición Colombiana Contra la Tortura, en su [informe sobre las cifras del 2009 al 2014,](https://issuu.com/cajar/docs/informe_sobre_tortura_24-04-15) donde **se manifiesta que en ese lapso de tiempo se registraron 349 casos de tortura física,** pese ello, esta problemática sigue invisibilizándose por el gobierno.

"En Colombia la tortura y los tratos o penas crueles e inhumanas o degradantes se practican de manera sistemática y generalizada", dice la CCCT. Según la Coalición,  la Fuerza Pública es registrada como **el principal perpetrador de estos actos, seguido de los grupos paramilitares posdesmovilización.**

De acuerdo con este informe, en el país, la tortura sigue siendo un mecanismo de persecución política, en el marco de detenciones, con el propósito de obtener una confesión o información, o como método de sometimiento de la población carcelaria. Igualmente, como mecanismo de discriminación, como instrumento de represión de la protesta social, o simplemente para sembrar terror en las comunidades.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
