Title: FFMM ocupan desde hace 20 meses las instalaciones de Politécnico en Ituango
Date: 2015-10-07 15:48
Category: Nacional, Paz
Tags: Brigada móvil 18, Casco urbano ituango, Derechos Humanos, Ejército Nacional de Colombia, Ituango, presencia militar en institución educativa, Radio derechos Humanos
Slug: ffmm-ocupan-desde-hace-20-meses-las-instalaciones-de-politecnico-en-ituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Ituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: radiomacondo 

<iframe src="http://www.ivoox.com/player_ek_8842887_2_1.html?data=mZ2hlJ2ce46ZmKiakpyJd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRdpGfzsrgx9iPqMafytPjw9jNaaSnhqeg0JDRrc3d1cbfjcaPrc%2Fn1c7h18jNaaSnhqeg0JDJqNbXwtnW2MaPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ananías Vega, Asociación de campesinos de Ituango] 

###### [7 oct 2015] 

Los habitantes del casco urbano de Ituango, **denuncian que la brigada móvil 18 del ejército** se estableció al interior de la institución educativa "Politécnico Isaza Cadavid", desde hace más de 20 meses, impidiendo el derecho a la educación a aproximadamente **300 jóvenes** .

Los soldados que inicialmente llegaron pidiendo 15 días para instalarse, han permanecido mas de 1 año en la sede de la institución de educación media superior y lo han usado como ** puesto de defensa** para protegerse de ataques que hasta el momento no se han presentado. Según la denuncia de los habitantes de Ituango, las instalaciones quedan en medio del casco urbano.

“La exigencia es que ellos desocupen ese espacio porque **los estudiantes se quedan andando las calles,** haciendo cosas que no deben hacer, porque no hay otra opción para que ellos sigan estudiando”, indicó Ananías Vega, presidente de la asociación de campesinos de Ituango, frente a la invasión, presentada en este centro educativo.

La comunidad no busca que se retiren de la zona sino que “desocupen ese espacio” y se ubiquen “alrededor del municipio”, ya que el **temor de la comunidad por un ataque es latente** y el derecho a la educación de estos 300 jóvenes se ve afectado.

Según Vega, esta información ha sido puesta a disposición del Ministerio de Defensa, el Ministro del Interior, la Brigada Móvil 18, la Séptima división, la Gobernación, el Alcalde e incluso la ONU, que ha servido como garante de esta situación.
