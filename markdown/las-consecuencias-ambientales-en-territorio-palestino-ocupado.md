Title: Las consecuencias ambientales de la ocupación israelí sobre Palestina
Date: 2017-02-22 17:01
Category: Ambiente, Voces de la Tierra
Tags: acceso al agua, Israel, Palestina
Slug: las-consecuencias-ambientales-en-territorio-palestino-ocupado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/palestina-e1487788435663.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [Noticias Shiítas]

###### [22 Feb 2017]

[Como si fuera poco la violencia y la represión a la que se ve sometido el pueblo palestino por parte de Israel, ahora es evidente el deterioro ambiental de los territorios ocupados. El acceso al agua y la contaminación son algunas de las principales problemáticas.]

[Eso es lo que asegura la Coordinadora de Acción Humanitaria de la ONG Alianza por la Solidaridad, Cristina Muñoz, en una entrevista que le realizó la agencia EFE Verde. Según Muñoz se trata de una **“situación irreversible”**, ya que cada día es peor, debido al control que tiene Israel.]

[**“El acceso al agua, a los medios de vida, al control de los recursos naturales, está denegado para la población Palestina”.** La política de cierres vulnera la libertad de movimiento de las personas,dice Muñoz, y agrega que se trata de una situación de carácter ilegal desde el Derecho Internacional Humanitario, a pesar de que la ocupación esté permitida, ya que se deben garantizar las condiciones de dignidad de vida para toda la población.]

[El problema, es que, poco o nada le interesa a Israel brindar condiciones básicas de vida a los palestinos. Más bien, **su accionar está basado en una “perspectiva militar”** que a su vez, busca dominar los recursos naturales, desplazar forzadamente a las familias palestinas y posicionar cada vez más colonos israelíes.]

### [Acceso al agua  ] 

[Mientras el promedio de consumo de agua al día de un israelí es de 300 litros, el de un palestino es de 70 litros. Además, la población colona cuenta con agua a bajo costo y de libre acceso, en cambio los palestinos se encuentran a largas distancias de los afluentes de agua, **están sometidos a cortes de este servicio y además pagan hasta 9 veces más por el líquido vital.  **]

### [Acuíferos inservibles] 

[De esa manera denomina la ONU en el ‘World Food Programme’ el acuífero de Gaza. Para el 2020 el agua estará tan contaminada con químicos y pesticidas que no se podrá usar para consumo humano ni para la agricultura, denuncia Cristina Muñoz. Adicionalmente Israel controla el Mar Muerto, fuente importante de recursos para la explotación industrial y turística.]

[La coordinadora afirma también que el **acuífero de Cisjordania está completamente deteriorado. El “80 % del mismo está explotado por Israel”,** sumado a eso, los habitantes de los asentamientos judíos ilegales en Cisjordania suelen arrojar sus residuos y descargar sus aguas servidas en ríos y otras fuentes de agua. ]

### [Agricultura extintiva] 

[Con el limitado acceso al agua, a la población palestina no le queda más que practicar una agricultura de “carácter extintivo”, que no permite que la tierra se recupere, debido al uso constante de pesticidas y químicos perjudiciales. ]

[“No hay acceso a las tierras”, muchas han quedado divididas por la construcción del muro y por los usos militares, “el paisaje es complejo”, dice la coordinadora de la ONG. Todo ello, tiene un impacto sobre el paisaje y los ecosistemas.]

### [Contaminación] 

[Las bombas de uranio empobrecido y el uso de sustancias químicas en los ataques del régimen de Israel contra la Franja de Gaza han ocasionado el aumento de casos de cáncer en los palestinos, debido a la contaminación en el aire por estos elementos tóxicos. Así lo afirma el director del programa de ayuda y esperanza para el tratamiento del cáncer, Iman Shannon, quien explica que **cerca de 11 mil palestinos en la Franja de Gaza padecen de cáncer** y un número desconocido está afectado pero todavía no ha sido incluido en la lista de enfermos del Ministerio de Sanidad del gobierno.]

### [Tratamiento de residuos] 

[La semana pasada, las **fuerzas militares de Israel** **cerraron un vertedero de basura en Rojib al estar cerca del asentamiento de Itamar.** El consejo del pueblo de Rojib tuvo que trasladar el vertedero a otro lugar lejos de la zona, lo que dificulta el procesamiento de las basuras.]

[Muchas ciudades y pueblos palestinos de la Ribera Occidental se quejan de la fuga de aguas residuales de los asentamientos israelíes y los campamentos militares que contaminan el ambiente y provocando la propagación de enfermedades.]

[Todo esto ejemplifica lo difícil que significa el tratamiento de las basuras. La gestión de los residuos sólidos es responsabilidad de la Autoridad Nacional Palestina, pero esta solo tiene el control del 20% de Cisjordania, mientras que allí viven más de 2 millones de personas entre colonos y palestinos. La prioridad es dar cabida a la población palestina que vive hacinada y que no pueden construir vertederos porque tienen muy poco espacio, pero ello, es imposible debido al poco control que tiene Palestina.]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
