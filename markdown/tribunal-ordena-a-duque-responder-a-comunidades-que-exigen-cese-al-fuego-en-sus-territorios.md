Title: Tribunal ordena a Duque  responder a comunidades que exigen cese al fuego en sus territorios
Date: 2020-08-28 14:09
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: comunidades
Slug: tribunal-ordena-a-duque-responder-a-comunidades-que-exigen-cese-al-fuego-en-sus-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/festival-de-las-mermorias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: COmunidades / Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante las reiteradas peticiones de más de 120 comunidades al Gobierno, exigiendo garantías en los territorios y en relación con la petición de cese al fuego y el amparo de sus derechos fundamentales a la salud, la vida y la paz, e**l Tribunal Administrativo de Cundinamarca** resolvió que en un plazo de cinco días el presidente Iván Duque dé respuestas de fondo a dichas comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Orlando Castillo, coordinador del Espacio Humanitario Puente Nayero en Buenaventura** explica que esta petición se venía haciendo desde el mes de marzo a través once cartas y peticiones radicadas exigiendo al Gobierno la garantía del derecho a la "paz, vida y salud de las comunidades".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicha petición remitida por la [Comisión de Justicia y Paz,](https://www.justiciaypazcolombia.com/tribunal-ordena-en-cinco-dias-al-presidente-duque-dar-respuesta-a-la-solicitud-de-comunidades-etnico-territoriales-de-hacer-un-cese-de-fuegos/) incluía una petición a las Fuerzas Militares, que han apoyado la erradicación forzada en los territorios el suspender sus operaciones y decretar un cese al fuego por 90 días junto al ELN y así acojan el pronunciamiento del **Secretario General de Naciones Unidas, Antonio Guterres de un alto al fuego mundial de todas las partes que intervienen en los conflictos armados** y que en medio de la pandemia resultan más lesivas. [(Lea también: Piden cese de operaciones militares para frenar la propagación del COVID 19)](https://archivo.contagioradio.com/piden-cese-de-operaciones-militares-para-frenar-la-propagacion-del-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque desde el 9 de abril se hizo un segundo derecho de petición al presidente **ante la agudización del control territorial por parte de grupos armados en medio del aislamiento preventivo**, para el 6 de julio de 2020, las comunidades no habían recibido una respuesta de fondo a sus solicitudes. [(Le recomendamos leer: El Gobierno está del lado contrario de la ciudadanía: Arzobispo de Cali)](https://archivo.contagioradio.com/el-gobierno-esta-del-lado-contrario-de-la-ciudadania-arzobispo-de-cali/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Respuesta del Gobierno no es satisfactoria para las comunidades

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Ante ello, el Tribunal, advirtió que aunque el Gobierno respondió a la solicitud, la respuesta con relación al cese al fuego en los territorios **"no cumple con el requisito de ser suficiente, efectiva y congruente" pues no soluciona el caso planteado, al no hacer un pronunciamiento al respecto sino limitarse a remitir las peticiones a otras instituciones como Fiscalía, Ministerio de Defensa y de Interior.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Sala del Tribunal agrega que el presidente Duque tiene la competencia para responder sobre la petición de un cese al fuego presentada por las comunidades que acudieron al mandatario ante la crisis humanitaria que ubica a las comunidades en medio del fuego cruzado, incrementado el riesgo de contagio del Covid-19. **Señalan que de no responder a esta decisión en un término no mayor a cinco días a partir de la notificación, el caso será remitido a la Corte Constitucional para que sea revisado.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Es un pequeño paso de que unidos como red y proceso podemos exigir garantías de que dé garantía para que se haga el ejercicio comunitario en Colombia y no se siga masacrando a los líderes y lideresas por su pensamiento y la defensa del territorio y la vida
>
> <cite>Orlando Castillo </cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Las comunidades han manifestado la esperanza de que el presidente Duque responda a su solicitud para que esta no tenga que llegar hasta la Corte Constitucional, donde los trámites son más largos mientras "lo que están viviendo las comunidades está enmarcado en hechos y acciones que se deben solucionar lo más pronto posible, estamos en una situación de crisis humanitaria". [(Lea también: Unión Europea reafirma su apoyo a los liderazgos en Colombia)](https://archivo.contagioradio.com/union-europea-reafirma-su-apoyo-a-los-liderazgos-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Nosotros aspiramos que sea una respuesta positiva que permita resolver muchas de las trabas que hoy tienen los procesos en materia de violación de DD.HH, reactivar el proceso de paz con el ELN y sobre todo los hechos que acontecen con líderes y liderezas en los territorios"**, expresa el líder Orlando Castillo destacando la existencias de garantías para su ejercicio como defensores de DD.HH.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
