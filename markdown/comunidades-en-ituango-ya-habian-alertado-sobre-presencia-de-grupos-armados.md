Title: Comunidades en Ituango ya habían alertado sobre presencia de grupos armados
Date: 2018-09-28 14:55
Author: AdminContagio
Category: DDHH, Nacional
Tags: Antioquia, Autodefensas Gaitanistas de Colombia, Ituango
Slug: comunidades-en-ituango-ya-habian-alertado-sobre-presencia-de-grupos-armados
Status: published

###### [Foto: Archivo Particular] 

###### [28 Sep 2018] 

Luego del desplazamiento de más de 300 personas de las veredas[ La Miranda, El Cedral y Bajo Inglés, en Ituango Atioquia, debido enfrentamientos entre las Autodefensas Gaitanistas de Colombia y disidencias de FARC, Oscar Zapata integrante de la Corporación Colombia Europa Estados Unidos, **señaló que ya habían alertado a las autoridades de este posible hecho, sin que se tomara alguna medida.**]

Según los habitantes, los mismos grupos armados les informaron que debían salir de sus territorios por el aumento que habría en los combates, y señalaron que, aunque la gobernación departamental y la Fuerza Pública fueron avisadas desde anoche de estos acontecimientos, las autoridades solo hicieron presencia hasta esta mañana.

Zapata afirmó que en lo corrido del año se han presentado por lo menos **13 desplazamientos forzados masivos en la zonas que rodean al municipio de Ituango, que han provocado que más de 4.500 personas estén fuera de sus hogares** y lleguen a lugares como Medellín.  (Le puede interesar: ["Aumenta el control paramilitar en el departamento de Antioquia"](https://archivo.contagioradio.com/aumenta-el-control-paramilitar-en-antioquia/))

Además, aseguró que después de la firma de los Acuerdos de Paz, diversas organizaciones le habían expresado a las instituciones gubernamentales y a la Fuerza Pública, que con la salida de FARC del territorio **"era muy probable el interés de otros grupos armados en esas zonas del departamento"** sin que se tomaran acciones frente a esta alerta.

### **Volver al pasado de una Colombia en guerra** 

Las 103 familias, que fueron recibidos en viviendas de campesinos en la vereda El Cedral, están pensando en la posibilidad de retornar a sus territorios, debido a la presencia de Fuerza Pública que hay en las veredas de la "aparente calma" que hay desde las últimas 24 horas, sin embargo Zapata señaló que muchas de estas familias ya habían sido víctimas del conflicto armado en el país y de otros éxodos de desplazamiento.

"**Es un volver al pasado y con una situación ya advertida, porque muchas organizaciones han hablado de qué es lo que se mueve en el municipio de Ituango** sin que las autoridades realmente le garanticen la seguridad y la vida a la gente" afirmó Zapata.

<iframe id="audio_28960995" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28960995_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
