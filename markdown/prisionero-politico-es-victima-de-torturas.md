Title: Prisionero político es víctima de torturas
Date: 2020-03-09 23:43
Author: AdminContagio
Category: Expreso Libertad
Tags: carceles de colombia, Expreso Libertad, INPEC, prisioneros políticos, Prisioneros políticos en Colombia, Torturas
Slug: prisionero-politico-es-victima-de-torturas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/presos-politicos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En este programa del **Expreso Libertad**, el abogado Uldarico Flórez, de la Brigada Jurídica Eduardo Umaña Mendoza denunció que Aimer Serrano, prisionero político fue víctima de torturas en cárcel La Picota, por un grupo de guardias del **INPEC,** cuando este iba a ser trasladado a la Cárcel de Valledupar, conocida como La Tramacúa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según  
la denuncia, un grupo de guardias ingresaron al patio 9 para sacar al  
prisionero que iba a ser trasladado a la Cárcel de la Tramacua, en ese  
momento Serrano fue víctima de torturas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado Uldarico Flórez, defensor de derechos humanos e integrante de la Brigada, señaló que «hubo gravísimas violaciones a derechos humanos, por parte de guardia que no tenían ninguna distinción» y que este hecho podría haberse gestado como retaliación en contra de la labor de defensa de derechos humanos que realiza Serrano desde el movimiento cárcelario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Vea mas de[Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F673657700041984%2F&amp;width=600&amp;show_text=false&amp;appId=1237871699583688&amp;height=338" width="600" height="338" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<!-- wp:paragraph -->

[Otros Programas de Contagio Radio en facebook](https://www.facebook.com/contagioradio/videos)

<!-- /wp:paragraph -->
