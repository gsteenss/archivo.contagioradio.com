Title: Indígenas Muinane reconstruyen memoria tras holocausto cauchero
Date: 2017-04-26 13:17
Category: Otra Mirada
Tags: amazonas, caucherias, indígenas, memoria
Slug: muinane-amazonas-libro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/indigenas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comunidad Muinane 

###### 26 Abr 2016 

Este miércoles 26 de abril se presenta el libro: "Fééne fíívo játyɨme iyáachimɨhai jíínɨje: Territorio primordial de vida de la descendencia del Centro. Memorias del territorio del Pueblo Féénemɨnaa", una publicación que recoge la memoria del retorno de los pueblos Muinane a sus territorios ancestrales, quienes buscan reconstruir sus vidas luego de haber sido torturados, asesinados y desplazados durante el auge de las caucherías en el Amazonas.

Con presencia de las autoridades tradicionales del Pueblo Féénemɨnaa (Muinane), Brigitte Baptiste directora del Instituto Alexander von Humboldt, Jorge Ortíz y Eduardo Paki en representación de los autores, se presentará en detalle el proceso de investigación y elaboración del libro, que inicio en 1991 con los recorridos realizados por los cinco clanes Muinane: Chuumójo (Gente de Gusano), Kɨɨmɨjo (Gente de Manguaré), Killéyɨmɨjo (Gente de Piña), Néjégaimɨjo (Gente de Coco de Cumare) y Gaigómɨjo (Gente de Mujer).

En 130 paginas la publicación recoge las narraciones orales que los ancianos compartieron durante esos recorridos, las notas y dibujos que los jóvenes realizaron a partir de esos relatos. Posteriormente se realizó el registro de los talleres y mapeo, bailes y rituales donde se mezclaban el tabaco con el llamado a los espíritus de sus antepasados con quienes buscaban restablecer su conexión ancestral.

El libro fue editado por el profesor Juan Alvaro Echeverri, quien asegura que en  la publicación aflora " la noción de territorio que tienen los indígenas, que es mucho más que un espacio geográfico o un resguardo titulado. El territorio es una relación con la vida, es donde están sus lugares de nacimiento, es donde está el sudor, la sangre, los muertos, los caminos, los rastrojos de sus cultivos”.

En cuanto a la parte gráfica el texto esta acompañado por las ilustraciones de los indígenas Eliseo Ortiz, José Daniel Suárez y Luis Alfredo Suárez y los mapas de Andrés Platarrueda, con el apoyo en los recorridos e investigación  de la Fundación Gaia-Amazonas. Le puede interesar: [Artesanas indígenas en Ciudad Bolívar salvaguardan la cultura ancestral](https://archivo.contagioradio.com/mujeres-indigenas-tejen/).

![muinane](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Portada-del-libro-Fééne-fíívo-játyɨme-iyáachimɨhai-jíínɨje.jpg){.wp-image-39716 .aligncenter width="333" height="431"}

Para la presentación del libro, las comunidades Muinane contaron con el apoyo del Consejo Regional Indígena del Medio Amazonas CRIMA y la organización Forest Peoples Programme, misma que financió la publicación. El evento que incluye una muestra de comidas y bebidas tradicionales, tendrá lugar en Auditorio Porfirio Barba Jacob-Centro Gabriel García Márquez (Cl. 11 \#5- 60) a partir de las 6p.m.

<iframe id="audio_18360539" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18360539_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
