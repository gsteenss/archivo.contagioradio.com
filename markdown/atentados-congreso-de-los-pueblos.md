Title: Congreso de los pueblos denuncia dos atentados ocurridos en 48 horas
Date: 2017-02-12 14:46
Category: DDHH, Nacional
Tags: Amenazas, atentados, colombia, congreso de los pueblos, lideres sociales
Slug: atentados-congreso-de-los-pueblos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Congreso de los Pueblos 

###### 12 Feb 2017 

El Congreso de los Pueblos emitió durante la mañana de este domingo, un comunicado en el que denuncian ante la comunidad nacional e internacional **una serie de amenazas y atentados ocurridos contra integrantes de esta organización en varias regiones del país**, hechos que, desde la organización, reafirman la persecución al movimiento social en Colombia.

De acuerdo con la información publicada, el día 9 de febrero ocurrió un atentado en contra del dirigente sindical [Alfonso Barón](https://archivo.contagioradio.com/atentan-contra-la-vida-de-alfredo-baron-sindicalista-en-valledupar/), en la ciudad de Valledupar, Cesar. En esa misma fecha, **circularon en el departamento del Cauca dos panfletos que incluían amenazas en contra de varias organizaciones** sociales incluyendo en el listado al Congreso de los Pueblos, firmados por el grupo autodenominado Águilas Negras.

La organización denuncia además que las amenazas, se extendieron al **municipio del Tambo**, puntualmente contra la vida de **Daniel Ulcue, líder campesino de la Asociación Campesina y Ambiental de Playa Rica y Líder campesino del Coordinador Nacional Agrario-CNA**. Le puede interesar: [Continúan las amenazas a líderes en el Cauca](https://archivo.contagioradio.com/continuan-las-amenazas-a-lideres-en-el-cauca/).

En el comunicado, se  registra el **atentado con arma de fuego ocurrido el sábado 11 de febrero en contra Carlos Jair Trujillo**, hechos ocurridos en le municipio de Agustin Codazzi, Cesár. De acuerdo con la información, el integrante de la Organización se encontraba en casa de su señora madre cuando un desconocido disparó contra la vivienda, hiriendo a su esposa en una mano.

El Congreso de los Pueblos, solicitó al gobierno a**doptar las medidas necesarias para proteger la vida de sus integrantes**, incluyendo la depuración de las fuerza pública, el desmantelamiento de los grupos paramilitares, y el cumplimiento de los acuerdos suscritos con el movimiento social, en lo que respecta a garantías de no repetición, protección y participación.
