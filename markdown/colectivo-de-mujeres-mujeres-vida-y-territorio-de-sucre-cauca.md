Title: Colectivo de mujeres "Mujeres, Vida y Territorio" de Sucre Cauca
Date: 2015-05-21 14:14
Author: CtgAdm
Category: DDHH, Mujer
Tags: Cauca, Clip sobre violencia contra de las mujeres en Cauca, Colectivo de mujeres del Cauca Sucre, mujeres, Vida y Territorio", Violencia sexual e intrafamiliar contra mujeres en Sucre
Slug: colectivo-de-mujeres-mujeres-vida-y-territorio-de-sucre-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/colombia-858-mujeres-indc3adgenas-paeces5-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Foto-12.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Elpueblo.com.co 

###### **Entrevista con [Leidy Correa], representante del Colectivo de Sucre, Cauca, "Mujeres, Vida y Territorio":** 

<iframe src="http://www.ivoox.com/player_ek_4528868_2_1.html?data=lZqfmp2afI6ZmKiak5WJd6Kll5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dgxsjhy9vTb8XZjNLizMrWqdSfxcrZjajFucTVjLjixdfJb4amk7LizMrWqdSZk6jDy8nFb9qftcqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Continuando con la **emisión especial de Contagioradio desde el Cauca, Sucre,** y con el objetivo de dar voz a los distintos procesos de lucha y expresiones culturales de la región, presentamos el colectivo de mujeres** "Mujeres, Vida y Territorio"**.

Por otro lado, dos representantes del colectivo también han participado en los talleres que se están impartiendo desde Contagioradio en las comunidades de Sucre sobre **comunicación social y formación de comunicadores.**

**Leidy Correa,** representante del grupo de mujeres, nos cuenta que el colectivo surge de la **"Escuela política, cultura y paz "de Sucre en 2012**, como expresión de las distintas colectividades de mujeres del municipio.

El grupo tiene como objetivo **luchar por la defensa y el respeto de los DDHH de las mujeres en Sucre,** brindando asistencia, acompañamiento y visibilización de las distintas problemáticas que afectan al colectivo.

"Mujeres, Vida y Territorio" se compone de **17 mujeres jóvenes del municipio  y de las veredas colindantes,** que se reúnen una vez al mes para analizar la situación y conformar las tareas del trabajo diario.

Actualmente, trabajan en distintos sectores como el de **programar talleres para promover los DDHH de las mujeres, la prevención de la violencia sexual e intrafamiliar** o proyectos laborales autogestionados.

Uno de esos proyectos conforman la **recuperación de huertos caseros y urbanos por mujeres con el objetivo de dar una salida laboral** y crear espacios de economía autogestionada. En este caso, se trata de **recuperar la sabiduría ancestral sobre el uso de medicina natural** a través de las distintas plantas de la biodiversidad del Cauca, así como su **cultivo y comercialización**.

**Correa** nos explica que de cara a un futuro los principales retos van encaminados ha paliar las **problemáticas referidas a la violencia sexual e intrafamiliar** que padecen las mujeres, y como programa de inclusión está la **participación de las mujeres en la creación y consolidación de la paz en la región.**

Dentro de estos retos está la **comunicación como herramienta** y difusión de las problemáticas y afectaciones de las mujeres en el conflicto armado en la región, así como su cultura y tradiciones. Este proyecto ya tendría un **antecedente en el videoclip que realizó el colectivo sobre la violencia contra las mujeres en los 80' en el Cauca.**

 
