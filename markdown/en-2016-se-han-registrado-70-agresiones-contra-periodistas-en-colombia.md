Title: 70 agresiones contra periodistas en Colombia en lo corrido de 2016
Date: 2016-05-03 12:52
Category: Entrevistas
Tags: colombia, dia mundial de la libertad de prensa, FECOLER, FLIP, Libertad de Prensa
Slug: en-2016-se-han-registrado-70-agresiones-contra-periodistas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Libertad-de-expresion1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La nueva crónica 

###### [3 Mayo 2016]

En el marco del Día Mundial de la Libertad de Prensa en Colombia, en el más reciente informe de la Fundación Colombiana de Periodistas, FECOLPER, sobre afectaciones a la libertad de prensa en el país, se evidencia que las agresiones contra periodistas continúan en aumento. **En lo que va del 2016, FECOLPER ha registrado un total de 70 hechos de violencia contra este oficio, mientras que para el mismo periodo del 2015 se contabilizaban 5 casos.**

Esta situación, para Ignacio Gómez, periodista y activista por la libertad de expresión, “es el resultado de una larga historia del ejercicio del periodismo dentro de un país con un conflicto armado activo donde se resalta el tema de la impunidad con **152 periodistas (como lo ha señalado la Fundación para la Libertad de Prensa) asesinados a la fecha”**, y añade que es también el reflejo de “las debilidades de la democracia colombiana.

Según este informe de FECOLPER, **Bogotá es la ciudad con mayor número de afectaciones a la prensa con un 24%,** seguida por Antioquia con el 9%, La Guajira 9%, Tolima 7%, y Casanare 6%. A su vez, se evidencia que los medios comerciales pequeños y los medios comunitarios alternativos, son las principales víctimas de estas agresiones protagonizadas en su mayoría por funcionarios o servidores públicos, grupos paramilitares y otros actores desconocidos.

Para Gómez, los actuales procesos de negociación de paz con las guerrillas de las FARC y el ELN, deben significar **la posibilidad de que se  democratice la información y se ponga límites al monopolio mediático** que existe en Colombia, teniendo en cuenta que en el país existen muy pocos medios en comparación  al resto de América Latina.

“El estado debe reparar una regiones que no tienen medios de comunicación o que los tuvieron antes del conflicto pero que desaparecieron, a causa del conflicto armado”, en ese sentido, según el periodista, **“el proceso de paz está obligado a estimular y financiar la construcción de ese tejido, pero sin una tendencia política específica”.**

De acuerdo con algunas conclusiones del informe de FECOLPER, **el fortalecimiento de las grupos paramilitares, pone en mayor riesgo a los periodistas de diferentes regiones** que cubren temas relacionados con los proceso de paz “lo que va en contravía del ambiente de pluralidad informativa y de contenidos que debería primar en este momento de ensanchamiento de la democracia”, indica el informe.

A partir de eso, el Estado colombiano debe trabajar para que haya un ambiente propicio para el ejercicio periodístico coherente con la realidad política del país, de manera que todas las voces tengan espacio en los medios de comunicación y a su vez, otros medios alternativos también tengas nuevas posibilidades.

[Informe Fecolper Enero 1 Abril 30 de 2016](https://es.scribd.com/doc/311359720/Informe-Fecolper-Enero-1-Abril-30-de-2016 "View Informe Fecolper Enero 1 Abril 30 de 2016 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_56896" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/311377317/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-7yelI63Qunb0dp1gBiX4&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

######  

<iframe id="audio_11396381" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11396381_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
