Title: Comunidades instalarán campamento humanitario frente a hostigamiento paramilitar
Date: 2016-06-20 15:21
Category: DDHH, Nacional
Tags: campamento humanitario en Antioquia, El Bagre Antioquia, paramilitarismo en Colombia
Slug: comunidades-instalaran-campamento-humanitario-frente-a-hostigamiento-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/El-Bagre-paramilitarismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [20 Junio 2016 ]

En este primer semestre del año, **seis campesinos han sido asesinados, desmembrados y enterrados en fosas comunes** en los corregimientos de Puerto Claver y Puerto López, en El Bagre, Antioquía, según las comunidades, a manos de los grupos paramilitares que tienen presencia en la región y que podrían estar al servicio de empresas mineras.

"En Puerto López puede ocurrir una masacre", asegura Mauricio Sánchez, de la Asociación de Hermandades Agroecológicas y Mineras de Guamocó', e insiste en que las comunidades están muy preocupadas porque en el municipio **está circulando una lista con los nombres de 70 personas que podrían ser asesinadas** y por temor 500 estudiantes han dejado de ir a clases.

En las dos últimas semanas en el municipio de Zaragoza fueron asesinadas dos personas, y pese a las denuncias de las comunidades, las autoridades departamentales, municipales y locales no han prestado atención, por lo que los campesinos insisten en la **necesidad de que haya una presencia institucional de carácter integral**, más allá de que envíen soldados a la zona.

Por esta situación de constante peligro y la inoperatividad de las instituciones del Estado, las comunidades instalarán el próximo martes **un campamento humanitario en el que se prohibirá el uso de armas de fuego**, así como el ingreso de cualquier actor armado, para salvaguardar la vida de las familias campesinas, este refugio sería transitorio y se levantaría cuando las comunidades evidencien [[garantías en términos de seguridad](https://archivo.contagioradio.com/?s=el+bagre+)].

<iframe id="audio_11967560" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11967560_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
 

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 

   
   
 
