Title: Contratos entregados "a dedo" por la Fiscalía suman $44.300 millones
Date: 2015-09-14 16:54
Category: Economía, Judicial, Nacional
Tags: Carrusel de contrataciones, corrupción, Detrimento patrimonial, Eduardo Montealegre, Fiscalía General de la Nación, Natalia lizarazo, Natalia Springer
Slug: contratos-entregados-a-dedo-por-la-fiscalia-suman-44-300-millones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Montealegre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:www.elheraldo.com ] 

###### [14 Sept 2015] 

[El actual Fiscal General de la Nación, Eduardo Montealegre, ha otorgado directamente cerca de **782 contratos cuya cifra supera los 44.000 millones de pesos, a  personas que fueron **decisivas en la elección de su cargo, aunque el ****Fiscal argumenta que se trata de una necesidad con el fin de recibir asesorías de profesionales externos para adelantar procesos judiciales e investigaciones.]

[Son **contradictorias las cifras asignadas para el curso de las investigaciones en relación con la calidad de los profesionales que estuvieron a cargo y los resultados obtenidos**, como ocurrió con el contrato adjudicado a **Natalia Springer** y su firma Springer Von Schwarzenberg Consulting Services S.A.S, para la investigación “El secuestro en Colombia, análisis cualitativo y cuantitativo sobre la Amplitud y la Sistematicidad de la Práctica Criminal de responsabilidad de las FARC-EP” y que junto a los otros dos contratos con la fiscalía, entre 2013 y 2015, suman **4.276 millones de pesos**.]

[La Unidad Nacional de Análisis y Contextos de la Fiscalía UNAC, en diciembre de 2013 emitió un documento formal sobre la interventoría a la firma y al primer informe de la investigación citada, anotando **graves inconsistencias**. Entre ellas, que no se presentaron como anexos los documentos desde los que se establecieron los datos del informe, no se explicó la metodología ni los métodos utilizados para realizar el restudio y por tanto **no se conocen las fuentes que validan su veracidad**.  ]

[La idoneidad de la firma y su apoderada es otro de los elementos que el informe de la UNAC pone en tela de juicio. Llama la atención que **Natalia cuando era columnista del Tiempo se apellidaba Lizarazo, luego argumentando que estaba en riesgo su seguridad cambió de apellido por el de su esposo**. Según la hoja de vida presentada a la fiscalía, **Natalia Springer tiene un PHD en politología de la Universidad de Viena, sin embargo, la institución niega ofertar este programa**. Las firmas para contrataciones públicas deben tener como mínimo 10 años de experiencia y **la creada por Springer contaba con poco menos de 2 años al firmar contrato con la fiscalía**.]

[A la lista de contratos asignados directamente por el fiscal se suman el **exmagistrado del Consejo de Estado Pedro Munar por \$345 millones anuales**, la Fundación Baltasar Garzón por \$1.380 millones, el **exmagistrado del Consejo de Estado Enrique Gil Botero por \$751 millones**, la firma Vargas Rincón & Asociados por \$517 millones, Luis Gustavo Moreno Rivera por \$139’200.000, Nathalia Bautista Pizarro con \$174’000.000, Aida Patricia Hernández Silva y Abogados S.A.S. con \$136.358.510, Beltrán & Castellanos Asociados Ltda. por \$551’000.000, Manuel José Cepeda Espinosa con \$551.000.000, Miguel Samper Strouss con \$261’000.000, Carlos Eduardo Medellín Becerra con \$240’245.280, Alejandro David Aponte Cardona con \$700’888.704, Pedro Eugenio Medellín Torres con \$241.164.000, Luis Eduardo Hoyos Jaramillo con \$385.955.200, Andrea Forer con \$113.100.000, Rafael Enrique Ostau de Lafont por \$121.929.038, Néstor Iván Osuna. con \$174.000.000, **Susana Buitrago con 174’000.000, Vargas Rincón y Asociados S.A.S. con \$517.266.666**, José Hernán Muriel Ciceri con \$101’713.040,  Pedro Alfonso Hernández Abogados Consultores S.A.S. con \$250’560.000, la Corporación Vivamos Humanos con \$143’687.500, Mikel Iñaki Ibarra Fernández con \$229’680.000, y la firma Germán Villegas Asociados S.A.S. con \$117’224.000.]

[Los números de contratos y montos totales desde marzo de 2012 hasta agosto de 2015, asignados por Montealegre se discriman así: en **2012 \$5.200 millones por 142 contratos**; de 2**013 \$6.490 millones por 235 contratos**; en **2014 \$16.657 millones por 240 contratos** y **en lo que va corrido del año \$15.964 millones en 165 contratos**.]
