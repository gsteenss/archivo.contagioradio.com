Title: Famila Aljure regresa a sus tierras en Mapiripán Meta
Date: 2017-04-03 16:19
Category: DDHH, Entrevistas
Tags: colombia, Desplazamiento, Dumar Aljure, mapiripan, Meta, paramilitares
Slug: aljure-mapiripan-meta-retorno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-02-at-8.52.57-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo- Contagio Radio 

###### 03 Abr 2017 

Luego de haber sido desplazados en 2012 por las estructuras paramilitares, los nietos de Dumar Aljure, legendario guerrillero liberal en el departamento del Meta regresan a las tierras que fueron adjudicadas en los años 50 a su abuelo luego de un proceso de paz entre esa guerrilla y el gobierno.

Según William Aljure, ellos hubieran querido regresar antes pero **la presencia paramilitar en la zona y la acción de la empresa multinacional Poligrow se los ha impedido**. Frente a ese tema Aljure aseguró que ya esta semana la empresa estuvo en la finca, acompañada por la policía y, al parecer, la unidad de restitución de tierras, lo cual plantea como **una “pelea entre David y Goliat”** los que lo tienen todo contra las víctimas, aseguró.

Y aunque Willliam manifiesta que siente miedo, también recuerda que el acompañamiento de las organizaciones internacionales que están llegando lo llenan de valor porque se siente “bien acompañado”, sin embargo hace **un llamado al Estado colombiano para que garantice la permanencia en el territorio** y que evite más agresiones por parte de estructuras paramilitares.

Ya **son 10 víctimas mortales en la familia desde la firma del acuerdo de paz con las guerrillas liberales**, sin embargo y a pesar de las dificultades propias de la presencia paramilitar, los hermanos Dumar y William, **están resueltos a regresar a sus tierras y a realizar el sueño del abuelo**, el guerrillero liberal, que repartiría la tierra para las personas que la necesiten la trabajen.

En ese sentido, ya hace una semana se hizo pública la [propuesta de poner un porcentaje de la hacienda a disposición de las víctimas y de las personas de las FARC](https://archivo.contagioradio.com/familia-aljure-dona-tierras-para-proyectos-productivos-de-las-farc/) que están en proceso de dejación de armas para que allí se realicen proyectos productivos que materialicen la reconciliación.
