Title: “Xenofobia no debe tener un lugar en la política estadounidense”: WOLA
Date: 2016-11-11 13:25
Category: El mundo, Nacional
Tags: América Latina, Donald Trump
Slug: xenofobia-no-debe-tener-un-lugar-en-la-politica-estadounidense-wola
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/inmigrantes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Univision] 

###### [11 Nov 2016] 

La Oficina en Washington para Asuntos Latinoaméricanos (WOLA) emitió un comunicado de prensa en donde afirma que después de la elección de Donald Trump en la presidencia de los Estados Unidos, **“está más comprometida que nunca para defender los derechos humanos y la justicia social”** y añade que la **“xenofobia no debe tener un lugar en la política estadounidense”.**

Y es que luego de que se dieran las elecciones, muchos latinoamericanos en este país han señalado su **temor frente a los actos de odio o racismo en contra de comunidades hispanas, latinas, afros y árabes**, fundamentadas en los discursos de Donald Trump con actos y propuestas contra los inmigrantes como la construcción del muro en México o las restricciones para el ingreso de refugiados de países como Libia o Siria. Le puede interesar: [""En Estados Unidos ganó el miedo y el racismo" Lilia Ramírez" ](https://archivo.contagioradio.com/en-estados-unidos-gano-el-miedo-y-el-racismo-lilia-ramirez/)

WOLA señaló que “**estas elecciones han alarmado a las personas y comunidades del hemisferio que trabajan por el progreso de los derechos humanos en las Américas y que temen que estos progresos desaparezcan**” Sin embargo, afirmaron su voluntad de continuar trabajando junto a sus aliados frente a las políticas que protejas los derechos humanos y reconozcan la dignidad humana”

Esta organización lleva aproximadamente 40 años  apoyando diferentes iniciativas en América que velen por los derechos de los ciudadanos, razón por la cual también indicaron que velarán para que procesos que Estados Unidos se encuentra alentando como el d**e Cuba con el restablecimiento de las relaciones políticas y económicas o el de Colombia con los acuerdos de paz de la Habana. **Le puede interesar.["Donlad Trump no es nuestro presidente: latinos en USA"](https://archivo.contagioradio.com/trump-no-es-nuestro-presidente-latinos-en-usa/)

######  Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
