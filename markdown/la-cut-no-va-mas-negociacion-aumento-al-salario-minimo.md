Title: La CUT no va más en negociación por aumento al salario minimo
Date: 2016-12-26 13:07
Category: Economía, Nacional
Tags: CUT, Salario Digno, Salario Mínimo en Colombia
Slug: la-cut-no-va-mas-negociacion-aumento-al-salario-minimo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/salario-mínimo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Portafolio  
] 

###### [26 Dic 2016] 

Luego de sustentar ante la Comisión Nacional de Concertación las razones para solicitar un aumento al salario mínimo del 14% y no obtener respuesta alguna por parte del Gobierno, la Central Unitaria de Trabajadores de Colombia –CUT–, **dio a conocer a través de un comunicado su decisión de concluir su participación en la negociación .**

“Durante meses hemos venido realizando estudios serios para sustentar nuestra posición de un aumento en 14%, pero no hemos obtenido aval ni contrapropuesta por parte del Gobierno o empresarios, **no han fijado posición y las reuniones realizadas sólo han dilatado la concertación**, por eso decidimos retirarnos”, manifestó Luis Alejandro Pedraza, presidente de la CUT.

Si fuese aprobada la propuesta de la organización sindical, el salario mínimo de los trabajadores y trabajadoras colombianas sería de **\$785.977, lo que significaría un incremento de \$ 96.523 al ingreso mínimo actual de \$689.454**.

Frente a la decisión de retiro por parte de la organización sindical, algunos empresarios propusieron un reajuste del **6,7% que implicaría sólo un alza de \$ 46.193, lo que quiere decir que el nuevo salario mínimo legal del 2017 aumentaría a \$ 735.647.**

Adicionalmente, la CUT denuncia que la falta de compromiso del Gobierno con los trabajadores colombianos se evidencio en **“la aprobación de la reforma tributaria que incluye la ampliación del IVA al 19% y la autorización del aumento al valor de la gasolina en 140 pesos** por galón, lo que deja sin posibilidad real acuerdo alguno”.

Por último, Pedraza señala que a pesar de los esfuerzos, fue imposible lograr un acuerdo “que en verdad dignifique el salario mínimo y por lo menos recupere en parte su poder adquisitivo para la canasta familiar” y agrega que "será el gobierno nacional quien **asuma la responsabilidad de seguir en la tendencia de mayor inequidad social de los colombianos y colombianas con su política económica”. **

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)
