Title: Colombia necesita todo menos un fiscal de bolsillo: Comisión Colombiana de Juristas
Date: 2019-07-10 17:45
Author: CtgAdm
Category: Judicial, Política
Tags: Decreto, Fiscal, Fiscalía, Iván Duque
Slug: colombia-fiscal-de-bolsillo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Fotos-editadas2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Contagio Radio  
] 

Recientemente **el presidente Iván Duque fue criticado por derogar el decreto que permitía escoger al Fiscal, tras un proceso de evaluación pública**. Aunque la medida no brindaba garantía absoluta sobre la idoneidad de los candidatos ternados para ocupar el cargo de Fiscal General de la Nación, era una forma de ampliar las posibilidades para elegir la persona indicada; por eso, dada la decisión del Presidente, analistas han pedido que se elija a la persona que llegue a dirigir el ente investigador con serenidad e independencia.

**Gustavo Gallón, director de la Comisión Colombiana de Juristas (CCJ)**, se pronunció sobre el tema, afirmando que no era posible entender la derogación del mencionado decreto; no obstante, expresó la sospecha sobre el caso: "esta derogación implica quién sabe qué simio dentro del Gobierno para la elección del fiscal". El Abogado recordó que durante el gobierno Santos, cuando se designó a Néstor Humberto Martínez sí hubo una postulación pública, lo que no significó garantía sobre el escogido para ser el jefe máximo de la Fiscalía.

Sin embargo resaltó que era una forma de estar alerta sobre los candidatos, y hacer un mayor seguimiento al proceso, razón por la que la ausencia del decreto, hace dudar sobre el proceso de elección del próximo fiscal. En ese sentido, Gallón afirmó que es de esperar que el Gobierno rectifique sobre la derogación, "o por lo menos se cuide para escoger una persona de las mejores calidades".

### **"Necesitamos todo menos un fiscal de bolsillo"**

El director de la CCJ declaró que la independencia de la rama judicial es uno de los pilares de la democracia, argumentando que "cuando un gobierno se mete con los jueces, la democracia se desestabiliza". Razón por la que aseveró que **Colombia necesita "todo menos un fiscal de bolsillo";** no alguien como Néstor Humberto Martínez, que venía cuestionado por su trabajo en relación a grandes intereses económicos y que tenía a la Fiscalía "de tumbo en tumbo", comportándose "como un artista del show dedicado a la pantalla".

En consecuencia, el Abogado resaltó el trabajo que está haciendo el Fiscal encargado del ente investigador; y dijo que para asumir este cargo se requiere de juristas que brinden seguridad a los ciudadanos, en conclusión, "necesitamos un fiscal sereno, que inspire confianza y que no sea partidista". (Le puede interesar: ["Las razones para pedir nulidad en elección del fiscal Néstor Humberto Martínez"](https://archivo.contagioradio.com/piden-nulidad-en-eleccion-del-fiscal-nestor-martinez-por-ocultar-informacion/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38274143" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38274143_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
