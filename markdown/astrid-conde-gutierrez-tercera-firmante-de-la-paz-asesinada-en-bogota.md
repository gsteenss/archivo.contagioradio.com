Title: Astrid Conde Gutiérrez, tercera firmante de la paz asesinada en Bogotá
Date: 2020-03-06 17:39
Author: AdminContagio
Category: Actualidad, Paz
Tags: Astrid Conde, Bogotá, Ex-Combatiente, FARC
Slug: astrid-conde-gutierrez-tercera-firmante-de-la-paz-asesinada-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Astrid-Conde-Gutiérrez.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Organizaciones sociales denunciaron el pasado jueves 5 de marzo el asesinato de Astrid Conde Gutiérrez, ex prisionera política de las FARC, que obtuvo su libertad gracias al Acuerdo de Paz y se encontraba adelantando su proceso de reincorporación en Bogotá. Con Astrid son 3 firmantes de la paz asesinados en la capital desde que se firmó el Acuerdo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la[Corporación Solidaridad Jurídica](https://twitter.com/CSJURIDICA/status/1235776898397024257/photo/1), Astrid Conde fue asesinada en el barrio Tintal de Kennedy, y actualmente se encontraba estudiando y desarrollando su proyecto de vida en Bogotá. Ella hacía parte del grupo de mujeres de la Corporación Defensa&Derechos. (Le puede interesar: ["Un partido no se desmorona por las críticas: FARC"](https://archivo.contagioradio.com/un-partido-no-se-desmorona-por-las-criticas-farc/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo a fuentes oficiales, el presunto autor material de los hechos fue capturado con lo que sería el arma homicida, y la Alcaldía de Bogotá se comprometió a esclarecer totalmente este nuevo hecho que enluta al partido FARC y los colombianos. (Le puede interesar:["Asesinan a Daniel Jimenez, excombatiente de las FARC en Puerto Guzmán"](https://archivo.contagioradio.com/asesinan-a-daniel-jimenez-excombatiente-de-las-farc-en-puerto-guzman/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Es al tercera firmante asesinada en Bogotá**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En febrero de 2018, el partido FARC denunció el asesinato de Jhon Mariano Ávila Matiz en Bosa. Mariano Ávila fue también prisionero político que obtuvo su libertad gracias a la amnistía del Acuerdo de Paz, y era el exprisionero 12 que habían asesinado hasta ese momento. Además de Mariano, FARC recordó el homicidio de Audias Cárdenas, ocurrido en Sumapaz el 10 de septiembre de 2019.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Vale la pena recordar también que empezando el mes de febrero apareció un panfleto firmado por las Águilas Negras, en las que aparecían mencionados dos integrantes del movimiento político. Ante estas situaciones, el partido pidió respeto y garantías para el ejercicio político así como para la vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar: [La paz no la detiene nadie, afirman excombatientes de FARC](https://archivo.contagioradio.com/la-paz-no-la-detiene-nadie-afirman-excombatientes-de-farc/).

<!-- /wp:paragraph -->

<!-- wp:html -->

> Denuncia Pública [pic.twitter.com/DPfCFi3gq7](https://t.co/DPfCFi3gq7)
>
> — FARC Bogotá (@farc\_bogota) [February 6, 2020](https://twitter.com/farc_bogota/status/1225210151759732738?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:block {"ref":78980} /-->
