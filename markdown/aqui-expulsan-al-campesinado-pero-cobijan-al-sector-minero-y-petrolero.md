Title: "Aquí expulsan al campesinado pero cobijan al sector minero y petrolero"
Date: 2020-03-10 17:01
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: campesinado, fracking, Parques Naturales, Puerto Boyacá
Slug: aqui-expulsan-al-campesinado-pero-cobijan-al-sector-minero-y-petrolero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/IMG-20200310-WA0042.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/estefany-grajales-aqui-expulsan-al-campesinado-pero-cobijan_md_48807643_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Estefany Grajales, representante legal de la Fundación Comunidades Unidas de Colombia

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Estefany Grajales, representante legal de la Fundación Comunidades Unidas de Colombia, e integrante de la Alianza Colombia Libre de Fracking, que *"les delimitan el uso y aprovechamiento de los recursos, ignorando que son comunidades que llevan mas de 40 años asentadas en estos territorios; y nunca se les ha explicado sobre la riqueza ambiental presente".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó, que durante más de 13 años el Gobierno se ha impedido en la aplicación de un [Plan de Manejo Ambiental](http://www.minambiente.gov.co/images/normativa/decretos/2010/dec_2820_2010.pdf)que genere alternativas económicas y ambientales encaminadas a la protección y conservación de este territorio estratégico.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Hoy se vive una situación compleja, puesto que debido a la falta de alternativas económicas, la comunidad sigue haciendo lo mismo, tala o usa los bosques para poder sobrevivir".*
>
> <cite>*Estefany Grajales, Fundación Comunidades Unidas de Colombia* </cite>

<!-- /wp:quote -->

<!-- wp:heading -->

¿La culpa es del campesinado?
-----------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Grajales por años los campesinos han afrontado delimitaciones, multas e incluso, judicializaciones, *"el Gobierno ha olvidado que también en el territorio hay una fuerte presencia de bloques petroleros, pasan gaseoductos y oleoductos por encima del Parque Natural; pero ante ello nadie dice nada*".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además afirmó que la ley en este caso presenta contrariedad con respecto a la habitabilidad, *"no sabemos si piden el desplazamiento de la comunidad porque esta es zona protegida o porque hay un titulo minero o bloque petrolero que tiene una proyección que lo pide"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Estigmantización y agresión a la comunidad para la conservación ambiental

<!-- /wp:heading -->

<!-- wp:paragraph -->

Así como se evidenció con las comunidades campesinas de Chiribiquete y Picachos, donde la Fuerza Publica intervino agrediendo y desplazando a miles de campesinos y campesinas en medio de una estrategia ambiental sin ofrecer alternativas económicas, algo similar temen que ocurra en la Serranía de las Quinchas. (Le puede interesar: [Situación de la comunidad de Chiribiquete y Picachos](https://archivo.contagioradio.com/persecucion-a-pobladores-de-la-macarena-en-desarrollo-de-operacion-artemisa/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La representante afirmó que por redes se difundió que habían más de 2.000 hectáreas de coca sembradas en este territorio, algo que según Grajales es completamente falso, pero que incentivo una campaña de desprestigio y señalamiento a la comunidad.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Hoy nos reunimos con Corpoboyacá y la Fuerza Pública para pedir que nos digan cuales son las hectáreas y las fincas que según ellos siembra, y exigir además que se cumpla lo pactado en el Plan de Sustitución donde señalaban a boca llena que Boyacá iba a ser el primer departamento sin coca, y que hasta la fecha nada de eso se ha cumplido"*.
>
> <cite>*Estefany Grajales - Fundación Comunidades Unidas de Colombia* </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Adicional denunció que en el territorio ya han experimentado las consecuencias del uso de glifosato para la erradicación, *"no queremos que se vuelva a usar este mecanismo que pone en riesgo la salud de las comunidades y la integridad de los cultivos de alimento de los campesinos".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Los campesinos han vivido más de medio siglo en la región, lastimosamente el único oficio que aprendieron fue el aprovechamiento forestal, el campesino hoy quiere quedarse en el territorio, quiere aprender a conservar, hoy quiere aprender otros modelos económicos y productivos que vayan acorde con la conservación del ecosistema "
>
> <cite>*Estefany Grajales - Fundación Comunidades Unidas de Colombia*</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y finalmente destacó su preocupación ante las promesas incumplidas, *"solicitamos a la alcaldia Otanche y Puerto Boyacá con sus respectivos concejos, y a la Corporación Autónoma Regional de Boyacá que invierta en el plan que garantice la seguridad ambiental así como la social"* .

<!-- /wp:paragraph -->
