Title: Catatumbo necesita un acuerdo humanitario, no erradicación forzada
Date: 2020-05-05 21:54
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Catatumbo, Erradicación Forzada, Norte de Santander
Slug: catatumbo-sigue-clamando-por-un-acuerdo-humanitario-que-ponga-fin-a-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Erradicación-de-Cultivos-de-Coca-en-Catatumbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @AscamcatOficial {#foto-ascamcatoficial .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

En medio de la pandemia, campesinos en Catatumbo y el área rural de Cúcuta, en Norte de Santander, han pedido al Gobierno que cese las erradicaciones forzadas de cultivos de uso ilícito porque con este tipo de acciones se pone en riesgo a la comunidad, en tanto se desconoce si los erradicadores son portadores del Covid-19. Adicionalmente, desconoce que algunas familias se acogieron al Plan Nacional Integral de Sustitución (PNIS).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ramón Torrado, párroco de El Cármen (Norte de Santander), afirma que las comunidades no han dejado que se haga la erradicación forzada, cuyos operativos iniciaron a mediados de marzo. El religioso explica que a los campesinos no les gusta sembrar Coca ni quieren vivir de eso, pero dado que los cultivos de pancoger no les ofrecen el sustento necesario para vivir, se ven obligados a recurrir a los cultivos de uso ilícito.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Organizaciones sociales han denunciado que la Autoridad Nacional de Licencias Ambientales (ANLA) está intentado que se realicen audiencias públicas de forma virtual para entregar el aval a la fumigación aérea con Glifosato en departamentos como Norte de Santander, Chocó, Nariño, Cauca, Antioquia, Bolívar, Córdoba, Guaviare, Meta, Putumayo, Valle del Cauca o Vichada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A propósito de la fumigación aérea, Torrado declara que si bien es cierto que la Coca contamina, también es cierto que el Estado no puede hacer lo mismo y afectar fuentes hídricas, zonas boscosas y cultivos de pancoger. Además, califica de ilógico e irregular que se pretendan adelantar audiencias virtuales, "porque sabemos que nuestros campesinos no tienen conectividad: luz eléctrica o internet en los territorios".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, reafirma que se deben detener los operativos de erradicación forzada y cumplir con el PNIS, para que la erradicación sea de la mano de los campesinos, gradual y que esté acompaña de proyectos productivos que les aseguren un ingreso económico para sostener a sus familias. (Le puede interesar:["Alejandro Carvajal, joven de 20 años asesinado por el Ejército en Catatumbo: ASCAMCAT"](https://archivo.contagioradio.com/alejando-carvajal-joven-de-20-anos-asesinado-por-el-ejercito-en-catatumbo-ascamcat/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los acuerdos humanitarios y la situación de confrontación en Catatumbo

<!-- /wp:heading -->

<!-- wp:paragraph -->

El párroco afirma que con el cese al fuego unilateral declarado por el Ejército de Liberación Nacional (ELN) durante abril se alivió la confrontación en los territorios, pero luego del levantamiento volvieron a presentarse combates en el territorio. Por esa razón llamó al Gobierno para se dialogue con los campesinos, y se encuentren caminos para construir la paz en la región.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ejemplo, una de las propuestas que se ha trabajado en esta, y otras zonas del país, tiene que ver con la creación de acuerdos humanitarios entre todos los actores armados que permita desescalar el conflicto, y proteger a la población civil. En ese sentido, [Torrado](https://twitter.com/AscamcatOficia/status/1257505989370224641) recuerda que la Comisión por la Vida y la Reconciliación del Catatumbo ha jugado un papel para impulsar este tipo de actos, y concluye que construir la paz en el territorio es posible.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
