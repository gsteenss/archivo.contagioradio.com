Title: El "sí" y el "no" de los ministerios por el glifosato
Date: 2015-04-28 14:02
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Alejandro Gaviria, Glifosato, Huila, ministerio de defensa, Ministerio de Salud, Monsanto, OMS, Organización Mundial de la Salud, semillas transgénicas
Slug: el-si-y-el-no-de-los-ministerios-por-el-glifosato
Status: published

##### Foto: [elobservadormendocino.com]

<iframe src="http://www.ivoox.com/player_ek_4419945_2_1.html?data=lZmem56YeY6ZmKiak5mJd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcritMbZ18mPtsbX0NLWx9PIpYzYxs%2FO1JDIqYzp1MbfjczQrcfj1Mbh0YqWh4zhysrb1tfFt4zBjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Esaín Calderón, médico y abogado de Semillas de Montealegre, Huila] 

Hace unas semanas, la Organización Mundial de la Salud, concluyó que el glifosato si podría causar cáncer, como ya se pensaba, sin embargo, Estados Unidos continua defendiendo el uso del herbicida. En Colombia,  el Ministerio de Salud decidió acoger el reporte de la OMS, y recomendó la suspensión del uso de glifosato para hacerle frente a los cultivos ilícitos, aun así el **Ministerio de Defensa afirmó que las fumigaciones con glifosato continuarán hasta “nueva orden”.**

**En Colombia han sido fumigadas 1,5 millones de hectáreas de cultivos ilícitos desde el año 2000,** lo que ha afectado varios cultivos campesinos, su salud y el ambiente. En el país, se argumentaba que la dosis no era toxica, pero de acuerdo a un estudio desarrollado en 2009, por las investigadoras, Marcela Varela, Gloria Lucia Henao y Sonia Díaz, se encontró una **asociación significativa entre los niveles de glifosato sanguíneo y los niveles de esa sustancia en varios pacientes con cáncer,** explica Esaín Calderón, médico y abogado de Semillas de Montealegre, Huila.

De acuerdo a la OMS, el glifosato, es una sustancia toxica que **se clasifica como una sustancia tipo 2A, es decir, que puede causar cáncer** a los  seres humanos, especialmente, patologías que tengan que ver con el sistema linfático.

**“De entrada se sabía que el glifosato es tóxico”,** expresa el médico Calderón, quien agrega que en la población colombiana se ha encontrado presencia de patologías sanguíneas de cánceres que se han incrementado debido al uso de pesticidas.

Monsanto, ha argumentado, que por medio de la transferencia genética se puede crear semillas más resistentes que no se verían afectadas por el glifosato, pero ante esta posibilidad, el abogado de Semillas de Montealegre, asegura que **la transferencia genética crea implicaciones en la salud humana y el ambiente.**

Los afectos del glifosato son casi inmediatos, debido a que los herbicidas caen directamente en las fuentes de agua, “por ejemplo, **en Huila han desaparecido varias especies de peces, que eran propios de aguas limpias**. Eso también ha generado graves consecuencias para los pobladores cercanos al río, ya que su economía y su sustento alimenticio se soportaban en los peces del río, de los que ya no queda ni un rastro.

Además, según el médico, **“la presencia de cualquier toxico en el alimento va generando efectos acumulativos,** lo que hace que las dosis vallan aumentando en el cuerpo hasta que llegue a los topes tóxicos y se generen alteraciones en la salud”.

Pese a que restablecer los campos con presencia de glifosato puede tardar décadas, la petición de Alejandro Gaviria, Ministro de Salud, no ha sido acogida por el Ministerio de Defensa, y mientras se mantenga esta política para frenar cultivos ilícitos, los más afectados seguirán siendo los campesinos, los cultivos y el ambiente.

 
