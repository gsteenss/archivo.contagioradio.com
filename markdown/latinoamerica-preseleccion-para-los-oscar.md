Title: Los rostros de Latinoamérica en la preselección para los Oscar
Date: 2018-12-21 16:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, pajaros de verano, Premios Oscar, roma
Slug: latinoamerica-preseleccion-para-los-oscar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Diseño-sin-título-770x400-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 21 Dic 2018 

Esta semana que termina, la Academia de Artes y Ciencias Cinematográficas de Hollywood anunció la **lista de 9 cintas preseleccionadas para participar en la categoría película en lengua no inglesa**, en la próxima edición de los premios Oscar en 2019, en la que destacan dos cintas latinoamericanas.

Se trata de las películas **Roma** del director mexicano **Alfonso Cuarón** y **Pájaros de Verano** de los colombianos **Cristina Gallego y Ciro Guerra**, pareja que compitió en esta categoría con El abrazo de la serpiente, en 2016.

<iframe src="https://www.youtube.com/embed/6BS27ngZtxg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Roma es una cinta autobiográfica, sobre una empleada domestica de origen indígena en el México de los años 70, quien cuida a los hijos de una familia de clase media en el Distrito Federal. Entre los premios internacionales está el **León de Oro de Venecia 2018** y obtuvo el tercer lugar en el Premio del Público en el **Festival Internacional de Cine de Toronto**.

<iframe src="https://www.youtube.com/embed/htDO3BpyM-I" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Por su parte la película colombiana, se alzó con el premio **Coral de ficción en el Festival del Nuevo Cine Latinoamericano de La Habana**. El filme hablado en Wayúu (una lengua amerindia originaria de La Guajira) y castellano, narra la historia de una familia de ese pueblo en la época del gran apogeo de la exportación de marihuana a Estados Unidos a finales de la década de los 70 que ve peligrar sus tradiciones ancestrales tras haber ingresado a este negocio.

Ambos largometrajes latinoamericanos,  junto con **The Guilty (Dinamarca), Obra sin autor (Alemania), Un asunto de familia (Japón), Ayka (Kazajstán), Cafarnaúm (Líbano), Cold War (Polonia) y Burning (Corea del Sur)** son las nueve preseleccionadas para competir por el Oscar.

Seis entraron en la lista por las votaciones de los académicos y tres por decisión del comité específico de la Academia. La polaca Cold War consiguió el premio a la mejor dirección en Cannes y arraso en los Premios del Cine Europeo, y la japonesa Un  asunto de familia ganó la Palma de Oro del pasado Cannes, son los dos rivales más potentes de esta lista de películas escogidas por la academia.

De resultar seleccionada, esta **sería la segunda participación de Colombia en los Oscar** y de ser junto con Roma las dos únicas películas precandidatas que representaran a Latinoamérica. **Los Premios Óscar 2019 se entregarán el 24 de febrero y las nominaciones definitivas se anunciaran el próximo 22 de enero**.

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
