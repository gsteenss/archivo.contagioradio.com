Title: Wilber Grueso, reincoporado de FARC asesinado en Cali.
Date: 2020-09-16 14:30
Author: AdminContagio
Category: Actualidad, Nacional
Tags: asesinato, Cali, Excombatientes de FARC, FARC, Firmante de paz
Slug: wilber-grueso-reincoporado-de-farc-asesinado-en-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/mapa-masacres-indepaz.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/mapa-masacres-indepaz-departamentos.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Teleantioquia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El partido FARC denunció que en la mañana del domingo 13 de septiembre fue asesinado el excombatiente y firmante de paz, Wilber Grueso (Pedro Grueso) en la ciudad de Cali, en el departamento del Valle del Cauca. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El excombatiente de 43 años, que había adelantado su proceso de reincorporación en el municipio de Policarpa, departamento de Nariño,  fue asesinado con arma de fuego en el barrio Metropolitano de la comuna 5 de Cali. ([Le puede interesar:](https://archivo.contagioradio.com/farc-pidio-perdon-a-las-victimas-en-un-gesto-de-paz/)[FARC pidió perdón a las víctimas en un gesto de paz](https://archivo.contagioradio.com/farc-pidio-perdon-a-las-victimas-en-un-gesto-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1306248040437690369","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1306248040437690369

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

De acuerdo con el coordinador de las FARC, Gustavo Arbeláez, **Grueso se movilizaba en carro junto a su compañera sentimental cuando fueron abordados por un sicario que se movilizaba en moto. Su esposa lo llevó al Centro Médico Imbanaco, donde finalmente falleció.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según lo indican integrantes del partido, Wilber Grueso había recibido amenazas en contra de su vida. La Policía Metropolitana y la Fiscalía investigan las causas del crimen. Asimismo, se trabaja en establecer el paradero de  los responsables del crimen.

<!-- /wp:paragraph -->

<!-- wp:heading -->

El gobierno sigue negando la crisis que afronta el país
-------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ante este hecho, el movimiento político exige al Gobierno una respuesta sobre lo que está pasando con los reincorporados en el país y sobre la sistematicidad de los hechos, pues ya son 227 los firmantes de paz asesinados en el país y el Gobierno de Iván Duque sigue negando la crisis en la que se encuentran los excombatientes, firmantes de paz y líderes sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo sucede con las masacres que se han cometido en lo que va del 2020; el presidente Duque ha insistido en que las masacres han disminuido en su gobierno y sin dar respuestas efectivas se ha limitado a presentar estadísticas y cifras; cifras muy inferiores a las presentadas por otros organismos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el[Instituto de estudios para el desarrollo y la paz -Indepaz-](http://www.indepaz.org.co/informe-de-masacres-en-colombia-durante-el-2020/) **durante este año 2020, hasta el 15 de septiembre, se han perpetrado en 57 masacres, en las que 230 personas han perdido la vida.** ([Le puede interesar:](https://archivo.contagioradio.com/las-razones-que-explican-el-regreso-de-las-masacres-a-colombia/) [Las razones que explican el regreso de las masacres a Colombia](https://archivo.contagioradio.com/las-razones-que-explican-el-regreso-de-las-masacres-a-colombia/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":89974,"width":580,"height":326,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/mapa-masacres-indepaz.jpg){.wp-image-89974 width="580" height="326"}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

En este nuevo reporte, **los departamentos en los que se han presentado más masacres son Antioquia con doce (13), seguido por Cauca y Nariño con ocho (8) cada uno y Norte de Santander con cinco (5).**

<!-- /wp:paragraph -->

<!-- wp:image {"id":89975,"width":563,"height":317,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/mapa-masacres-indepaz-departamentos.jpg){.wp-image-89975 width="563" height="317"}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

El pasado 25 de agosto Indepaz había publicado un [informe](https://archivo.contagioradio.com/45-masacres-han-sido-perpetradas-en-lo-que-va-del-2020-indepaz/) en el que se documentaban 45 masacres en las que habían sido asesinadas 182 personas, es decir, **en poco menos de un mes, se perpetraron 12 nuevas masacres.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
