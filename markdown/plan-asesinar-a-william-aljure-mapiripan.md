Title: Denuncian plan para asesinar a William Aljure lider de Mapiripan en Meta
Date: 2017-12-22 17:32
Category: DDHH, Nacional
Tags: mapiripan, Meta, William Aljure
Slug: plan-asesinar-a-william-aljure-mapiripan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/retorno-familia-aljure-7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

------------------------------------------------------------------------

###### [Foto: Contagio Radio] 

###### [22 Dic 2017]

William Aljure, reconocido defensor de DDHH, reclamante de tierras y líder de las víctimas de Mapiripán, fue amenazado de muerte por estructuras paramilitares que operan en la región. Según la denuncia, **paramilitares afirmaron que Aljure, ha “jodido mucho” y de este año “no pasa”** porque ya estaría lista la orden para asesinarlo.

La denuncia dada a conocer por la Comisión de Justicia y Paz, **relata que algunos testigos escucharon a los paramilitares conversando en un establecimiento público.** En la conversación afirmaron que el ambientalista y víctima de desplazamiento forzado, William Al juré, sería asesinado porque ya estaba la orden para hacerlo, además la acción se realizaría antes de finalizar este año 2017.

El líder también es vocero de la red de Comunidades Construyendo Paz en los Territorios, CONPAZ, que ha sido golpeada por el asesinato de 5 de sus líderes este año, **dos de ellos Mario Castaño y Hernán Bedoya, ambientalistas y reclamantes de tierras de la región del Bajo Atrato Chocoano**, asesinados durante las dos primeras semanas de este mes de diciembre. [Lea también: Asesinado Hernan Bedoya, lider de Bajo Atrato Chocoano](https://archivo.contagioradio.com/asesinado-reclamante-de-tierras-hernan-bedoya/)

### **Familia de William Aljure le ha apostado a la paz desde hace casi 70 años** 

Aljure, fue desplazado de sus tierras en el año 2012 por operaciones de tipo paramilitar que se desarrollan en la región y ha luchado por el derecho a sus tierras, fruto del proceso de paz de 1957 entre el gobierno y la guerrilla de Dumar Al juré. Desde hace 3 años ha liderado procesos de reparación colectiva y ha puesto sus tierras al servicio de la reintegración social. [Lea también: Familia Aljure ofrece sus tierras para proyectos de reincorporación en el Meta](https://archivo.contagioradio.com/familia-aljure-dona-tierras-para-proyectos-productivos-de-las-farc/)

Además el líder ha denunciado múltiples acciones contra el ambiente en el desarrollo de las **operaciones empresariales de Poligrow**, que según documentos públicos de distintas instituciones ha producido un detrimento en aguas y bosques de la región. El líder también ha sido víctima de ataques físicos en su contra durante los últimos meses.

###### Reciba toda la información de Contagio Radio en [[su correo]
