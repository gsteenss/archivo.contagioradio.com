Title: Estas son algunas de las multas que establece la Ley Mordaza en España
Date: 2015-07-02 17:07
Category: El mundo, infografia, Otra Mirada
Tags: código penal España, Comisión Legal del Sol, españa, ley mordaza, Ley orgánica, leyes dictatoriales, Libertad de expresión, protesta social en España
Slug: ley-mordaza-ataca-la-movilizacion-social-la-solidaridad-y-la-pobreza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Manifestación_contra_la_Ley_Mordaza_en_Madrid_20-12-2014_-_22.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [commons.wikimedia.org]

<iframe src="http://www.ivoox.com/player_ek_4715872_2_1.html?data=lZyel52bdo6ZmKiak5iJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMbtjLLc1MnFvsKfwtnOxcaPsMKfztTjy9HNvsLXyoqwlYqmd8%2Bf1NTQy8bQaZO3jNHOjdjTsMrYwtfWxpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Arantza Legal, Comisión Legal del Sol.] 

Ley Orgánica de seguridad ciudadana, más conocida como la **“Ley Mordaza” entró en vigor el pasado 1 de Julio e**n España y contempla penalizaciones económicas que oscilan entre los **600 y los 600.000 euros**, algunas de las conductas punibles a partir de ahora van desde tomar bebidas alcohólicas en la calle hasta hacer un video de un operativo policial.

Esta ley contempla tres tipos de faltas:

1.       **Faltas muy graves:** con multas desde 30.000 a 600.000 euros, y contemplan la fabricación, almacenamiento o uso de armas o explosivos, celebrar espectáculos públicos no autorizados y proyectar luces sobre los pilotos o conductores de medios de transporte.

2.       **Faltas graves:** con multas desde 600 hasta 30.000 euros y que contemplan la celebración de manifestaciones públicas frente al senado, impedir desahucios e incluso no contribuir con las autoridades o tolerar actos en contra de decisiones administrativas.

3.       **Faltas leves:** con multas desde 100 hasta 600 euros y que penalizan prácticas como la ocupación de bienes abandonados, o la presentación de la documentación exigida por la policía.

Para varias organizaciones sociales, **el fondo de este paquete de leyes,** (Ley mordaza y reforma al código penal que implica penas de prisión) está destinado a atacar la protesta y las expresiones sociales como la **movilización social** en contra de reformas estatales, la **solidaridad con las personas inmigrantes, y de igual manera afecta a los más pobres.**

Lo más grave de este paquete de leyes, es ver que un repertorio de acciones que se han venido haciendo libremente, se han transformado desde la legitimidad a infracciones administrativas, esa es una clara criminalización de la protesta y de las formas de solidaridad, señala la abogada **Arantza de la Comisión Legal del Sol.**

Respecto a la solidaridad con los inmigrantes la situación no es menos grave, el paquete de Leyes Mordaza contempla penas al brindar ayuda y atención a personas indocumentadas, el transporte de personas que no cuenten con la documentación completa, incluso el prestar ayuda para legalización, medidas que se convierten en la **“criminalización de la solidaridad”** señala la abogada.

Sin embargo, diversas organizaciones que desde la enumeración de la ley encontraron las amenazas, siguen buscando las grietas a la ley, puesto que aunque antes, las prácticas sociales como el impedir desahucios provocaron detenciones y judicializaciones la Ley Mordaza es bastante específica, y criminaliza con nombre propio esas opciones ciudadanas. En resumidas cuentas, con Mordaza o sin mordaza, **mucha gente sigue estando dispuesta a defender sus derechos incluso por encima de la criminalización.**

[![leymordaza](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/leymordaza.png){.size-full .wp-image-10822 .aligncenter width="812" height="1000"}](https://archivo.contagioradio.com/ley-mordaza-ataca-la-movilizacion-social-la-solidaridad-y-la-pobreza/leymordaza/)
