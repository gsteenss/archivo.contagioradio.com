Title: Arranca la Mesa Social para la Paz
Date: 2016-11-03 12:51
Category: Nacional, Paz
Tags: Alberto Castilla, conversaciones de paz con el ELN, ELN, Mesa Social para la paz.
Slug: arranca-la-mesa-social-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Mesa-Social-Para-La-Paz-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alberto Castilla] 

###### [03 Nov 2016]

La **Mesa Social para la Paz** se instalará esta tarde en el edificio Tequendama del centro de Bogotá. A las 3 de la tarde se darán cita cientos de personas pertenecientes a diversas organizaciones sociales que esperan ser la contraparte de la sociedad civil en el marco de las conversaciones de paz del gobierno nacional con la guerrilla del ELN.

Alberto Castilla, senador del Polo Democrático e integrante del comité de impulso de la Mesa Social para la Paz, afirma que este escenario representa un punto de encuentro de organizaciones civiles** que han venido trabajando históricamente en la construcción de lo que han llamado la “Paz Completa”** es decir, para que las conversaciones de paz incluyan la voz de sectores diferentes a las guerrillas. [Le puede interesar El papel de la sociedad civil en las conversaciones de paz con el ELN)](https://archivo.contagioradio.com/esta-lista-la-mesa-social-para-la-paz/)

Frente al incumplimiento sistemático del gobierno a espacios de conversaciones como la Cumbre Agraria, recientemente suspendida, Castilla reitera la necesidad de oír las voces de otros sectores sociales y abogar para que el **gobierno cumpla y asuma una postura coherente frente a las diversas exigencias en salud, educación y otras** que se han evidenciado en los últimos días y en diferentes regiones del país.

### **Así funcionará la Mesa Social para la Paz** 

Aunque se han planteado varias formas de organización de la Mesa Social para la Paz, aún no se ha definido el mecanismo para su puesta en marcha, uno de ellos es el de [Cabildos Abiertos](https://archivo.contagioradio.com/cabildos-abiertos-propuesta-mesa-social/) que facilitan la participación a través de los Consejos Municipales, o la apertura de otros escenarios similares también a nivel municipal.

Según Castilla se deben abrir t**odos los macanismos necesarios y que satisfagan las expectativas de los sectores sociales.**

[[Frente a la demora en el inicio de la fase pública de conversaciones con el ELN,] Lea también] ([Mesa de conversaciones condicionada a libertad de Odin Sánchez](https://archivo.contagioradio.com/mesa-social-para-la-paz-se-dispone-para-dialogos-eln/)), el senador afirma que deben conocerse los acuerdos previos, “parece que el gobierno tiene un documento de acuerdo diferente al del ELN” puesto que esa guerrilla afirma que la **liberación de Odín Sánchez sería después de la instalación,** mientras el gobierno afirma que es una condición previa.

Así las cosas, la instalación de la **Mesa Social para la Paz será un impulso para e inicio de la fase pública de conversaciones de paz**, y una señal de apoyo al comité tripartita que se conformó para sacar adelante la liberación del político chocoano acusado de corrupción y que se encuentra retenido desde hace 6 meses. De esa comisión hacen parte los países garantes, el CICR y un delegado de la iglesia católica cercano al proceso.

<iframe id="audio_13600646" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13600646_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU)
