Title: Así se adelanta proceso de desminado humanitario en El Orejón, Antioquia
Date: 2015-07-08 17:37
Category: Otra Mirada, Paz
Tags: Antioquia, Briceño, desminado humanitario, El Orejón, FARC, Frente 36 FARC, Meta, países garantes, proceso de paz
Slug: asi-se-adelanta-proceso-de-desminado-humanitario-en-el-orejon-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/desminado-humanitario.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Agencia Prensa Rural 

<iframe src="http://www.ivoox.com/player_ek_4739982_2_1.html?data=lZygm56cdo6ZmKiak5eJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdSZpJiSo6mPt8afwsnSzsbSuMKf0dfcxcrXs4zYxpDRx9jRrc%2FVxdSYytrRpc%2Fd1cbfy9SPqc%2BfsNeah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Habitante de El Orejón, Antioquia] 

###### [8 de Julio 2015]

En el municipio de Briceño en el sector denominado El Orejón, al norte de Antioquia, **desde el viernes pasado se adelantan las labores para realizar el proyecto piloto de desminado humanitario.**

**Tres delegados de las FARC del frente 36,  **llegaron a esa parte del país para mostrar la ubicación de las minas antipersonales a los 48 agentes del Ejército Nacional que desminarán la zona. Así mismo, hay acompañamiento por parte de los países garantes y de la Cruz Roja Internacional.

Una de las condiciones para que se realice esta actividad, era que no hubiera ningún actor armado, sin embargo,  **hubo presencia del Ejército Nacional a menos de 500 metros** de la zona , lo que generó que **se estancara el proceso de desminado  humanitario durante dos días,** hasta que se retiró el ejercito.

Los habitantes de la zona aplauden la labor de desminado, pero piden al gobierno nacional **mayor inversión social y la finalización del proceso de paz** con acuerdos en favor de las comunidades.

Se espera que el proceso de desminado dure dos semanas más, para que luego la misma comisión se traslade hacia **el Meta donde se iniciará la segunda etapa del plan piloto** sobre desminado acordado en el proceso de paz.

 
