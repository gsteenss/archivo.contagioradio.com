Title: Retos para una educación digna en tiempos de pandemia
Date: 2020-08-25 14:13
Author: CtgAdm
Category: Actualidad, Ana Erazo Ruiz, Opinion
Tags: colombia, dignidad, dolor, Inmarcesibles, Lagrimas
Slug: retos-para-una-educacion-digna-en-tiempos-de-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/educacion-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ICBF 

#### **Por: Ana Erazo Ruiz** 

[[[La situación mundial nos invita a reflexionar sobre la metodología de la enseñanza en línea y la importancia de la tecnología. Si bien este no es un tema nuevo, la realidad es que este contexto llegó sin planificación, sobre todo dentro de las IEO, hogares, familias y docentes que no cuentan con las condiciones, herramientas, tiempos y tecnologías. La escuela y el hogar se convierten en un mismo lugar; las(os) estudiantes junto con sus familias se enfrentan al reto de generar nuevos hábitos dentro de la casa ya que este será nuestro nuevo espacio de permanencia casi que 24/7.]]

[[[Hablar de **ciudades para la vida digna** es también hablar de adecuación de espacios y viviendas que nos garanticen el acceso al internet para el ejercicio de nuestro aprendizaje, disfrute y conocimiento, tanto individual como colectivo]]

[[[Según un estudio de la U. Javeriana, se revela que el 96% de los municipios del país no podrían implementar lecciones virtuales debido a que **menos de la mitad de los 10millones de estudiantes de colegios públicos (cerca del 37%) tiene computador e internet en su casa**. Univalle y otros colegios públicos como El Liceo Departamental han generado convocatorias, y donatones para estudiantes que no cuentan con herramientas tecnológicas para el ingreso de sus clases, sin embargo, llega el problema del internet ¿contamos con una cobertura nacional de internet?]]

[[[Para nadie es un secreto que en nuestra Cali muchos hogares y familias no pueden obtener este servicio y la lucha será siempre que dentro de este siglo comunicacional y tecnológico todas(os) podamos acceder a él. **Es importante reconocer que la tecnología es un aliado que renueva y fortalece, pero lastimosamente la posibilidad de adquisición se regula según el mercado y la competencia.**]]

[[[Estamos atravesando un enorme desafío de **equidad educativa** que puede tener consecuencias que alteren la vida de las(os) estudiantes más vulnerables y hogares en condiciones de pobreza, extrema pobreza, madres cabeza de hogar, jóvenes que no cuentan con un apoyo financiero y un soporte mínimo tanto económico como emocional.]]

[[[**La vuelta a clases debería tener un componente extra que nos puede regalar esta coyuntura y es la de evaluar los niveles educativos según los privilegios y comodidades**; se deberá garantizar no sólo la seguridad en cuanto a salud y la Covid19, sino también todo lo que atraviesa al modelo educativo ya que ahora con el aislamiento y la pandemia viene aflorar las grandes desigualdades que siempre hemos tenido, y que esta crisis de salud pública las profundiza.]]

[[[**Es menester seguir fortaleciendo iniciativas, campañas, para recoger recursos y herramientas tecnológicas para estudiantes de colegios y universidades**. Campañas que se han generado desde la solidaridad, porque como hemos dicho “*solo el pueblo salva al pueblo”*. Un ejemplo de ello es la que actualmente está organizando “Soñadores Siloé” con la]]

[[D[entro del actual PDM “Cali unida por la vida 2020-2023” saludamos la apuesta en cobertura para el acceso a la educación superior, donde el proyecto movilizador *Universidad Distrital* se cataloga como uno de los proyectos más importantes por su componente social, generando un gran hito, una transformación también en temas urbanos, y cualitativos frente a la percepción de la educación. No obstante, **la desfinanciación de la educación superior pública en Colombia es una de las problemáticas que ha estado en el panorama de lucha del movimiento estudiantil** (más de 30 años), quienes se han enfrentado a las políticas de privatización progresiva de la educación superior pública, son ellas/os los grandes afectados por las condiciones económicas para su sostenimiento durante la cuarentena y, es por ello que dentro del marco de esta crisis varias universidades públicas del país están hoy en resistencia para lograr la **matrícula cero**. Nuestra Univalle logró ello con algunas condiciones; lo mismo la Universidad Distrital, La UIS, la U. Antioquia y otras más que en estos momentos se encuentran en huelga para seguir exigiendo este derecho.]]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
