Title: Jurisdicción de Paz será una herramienta para las víctimas
Date: 2017-03-14 13:42
Category: Nacional, Paz
Tags: Fast Track, implementación acuerdos de paz
Slug: jurisdiccion-de-paz-sera-una-herramienta-para-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/marcha-de-las-flores101316_264.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Mar 2017] 

Luego de tres jornadas de debate y con 61 votos fue aprobada la Jurisdicción Especial para la Paz, una herramienta que, pese a las modificaciones que vivió con respecto a la responsabilidad de terceros en la financiación del conflicto y la cadena de mando de las Fuerzas Militares, **mantiene en firme el Sistema Integral de justicia, verdad, reparación y no repetición para las víctimas del conflicto armado del país.**

Jairo Estrada, integrante de Voces de Paz, señala que tras la aprobación de la Jurisdicción Especial para la Paz se consolida la Comisión de Esclarecimiento de la Verdad, la Unidad de Búsqueda de Desaparecidos, el desarrollo de medidas de reparación integral, colectivas, de poblaciones y organizaciones sociales y la incorporación de medidas de no repetición con la finalidad de **“superar la impunidad que ha existido en el país”. **

En este mismo sentido el senador Iván Cepeda afirmó que si bien es cierto que hay algunas modificaciones sustanciales, **“ha surgido un sistema que tiene unas atribuciones extensas, que podrá contar con una autonomía durante 15 años en Colombia** y con la participación de las víctimas”.

### **Las modificaciones a la JEP** 

Referente a la limitación de la responsabilidad de civiles y de acuerdo con lo propuesto por el senador Germán Varón del partido Cambio Radical, esta estará en el marco de la Jurisdicción   Especial para la Paz cuando se demuestre que “hay una participación activa o determinante en la comisión de los delitos de genocidio, delitos de lesa humanidad, graves crímenes de guerra”, **dejando de lado la responsabilidad de civiles por la financiación de grupos armados.**

Estrada señaló que frente a la responsabilidad en la cadena de mando de las Fuerzas Militares hay una conceptualización de responsabilidad que se aleja de los entendimientos predominantes del Derecho Internacional y que avanza hacia una interpretación problemática este concepto que no tiene otro propósito que “**generar condiciones de exculpación para altos mandos militares que han estado comprometidos con crímenes de guerra y de lesa humanidad**”. Le puede interesar: ["tres generales serían los primeros en acogerse a Jurisdicción de Paz"](https://archivo.contagioradio.com/generales-acogidos-jurisdiccion-de-paz/)

Para el integrante de "Voces de paz" esta situación, más que favorecer a los militares o beneficiarlos los pondrá en riesgo porque la **Corte Penal Internacional ya lanzó serias advertencias sobre las posibles violaciones al Estatuto de Roma** y en concreto en su artículo 28 que clarifica y penaliza la cadena de mando en la comisión de graves violaciones de DDHH.

Otra de las modificaciones a la JEP consistió en endurecer las condiciones de participación política para integrantes de las FARC, la ponencia de Carlos Galán, también del Partido Cambio Radical, propuso que “el Tribunal de Paz podrá, de acuerdo a los parámetros que establezca la ley de JEP, **determinar la inhabilitación política y para el ejercicio de derechos de participación política**”. Le puede interesar: ["Armas de las FARC se van convirtiendo en homenaje y memoria: Carlos Lozada" ](https://archivo.contagioradio.com/armas-de-las-farc-se-van-convirtiendo-en-homenaje-y-memoria-carlos-lozada/)

### **Víctimas y veeduría Ciudadana presionaron por la JEP** 

Durante la votación a plenaria, tanto personas que hacían parte de la **veeduría ciudadana Ojo a la Paz como víctimas del conflicto armado, estuvieron presenten en el recinto para verificar la participación de los congresistas**, hecho que permitió general un mayor control hacia los senadores y una mejor participación de la ciudadanía en el ejercicio de la democracia.

Sin embargo, el resultado de la veeduría es la **inasistencia de 21 senadores y una presencia intermitente de otros cuantos**, durante la votación de la Jurisdicción Especial para la paz. Le puede interesar: ["Implementación de los Acuerdos requiere de Movilización social: Iván Cepeda"](https://archivo.contagioradio.com/implementacion-de-los-acuerdos-requiere-movilizacion-social-ivan-cepeda/)

<iframe id="audio_17542248" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17542248_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio.
