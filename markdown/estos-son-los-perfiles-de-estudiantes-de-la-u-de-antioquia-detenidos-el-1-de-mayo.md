Title: Estos son los perfiles de estudiantes de la U de Antioquia detenidos el 1 de Mayo
Date: 2016-05-04 17:52
Category: DDHH, Nacional
Tags: aceu, Marchas en Colombia, Primero de Mayo, Universidad de Antipoquia
Slug: estos-son-los-perfiles-de-estudiantes-de-la-u-de-antioquia-detenidos-el-1-de-mayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/1-Mayo-Medellín-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: caracol] 

###### [4 May 2016] 

Desde la **Asociación Colombiana de Estudiantes Universitarios ACEU**, comparten los perfiles de los [4 jóvenes detenidos en Medellín](https://archivo.contagioradio.com/detencion-de-4-estudiantes-de-la-aceu-podria-ser-un-montaje-judicial/) tras la movilización del 1 de Mayo.

Según la organización estudiantil, el hoja de vida de los estudiantes demostraría que están siendo víctimas de una falsa judicialización. Además recordaron que las pruebas presentadas por la fiscalía no son coherentes con los delitos que se les imputan.

### **ALEXIS CASAS VALENCIA**: 

28 años; estudiante de Microbiología y Bioanálisis, séptimo semestre, adelanta su Trabajo de Grado titulado “Determinantes Sociales de la Anemia, Parasitismo y la Desnutrición”; es un reconocido Dirigente Estudiantil en su Escuela tanto por sus compañeros y amigos, como por sus profesores y directivos. Es miembro del Consejo Estudiantil de la Escuela de Microbiología, donde dirige el Comité de Deporte y Cultura. Por su gran amor al pueblo y la paz de Colombia decidió vincularse hace más de dos años a la Juventud Comunista Colombiana JUCO y a la Asociación Colombiana de Estudiantes Universitarios (ACEU). Su pasión por el fútbol se tradujo en la creación del Torneo Universitario Deporte y Cultura para la Paz que organiza cada semestre en la Universidad. Como buen hijo, vela por  la salud de su madre y la lleva a todas las citas médicas y consigue los medicamentos para su ella quien padece el Síndrome de Guillain-Barré.

### **CRISTIAN CAMILO PEÑA**: 

24 años; estudiante de Biología, sexto semestre, apasionado por la botánica. La pérdida de parte de su antebrazo derecho no ha sido impedimento para practicar Taekwondo,  buceo, montar bicicleta y tocar el Bajo, su instrumento preferido en la banda de Rock Ley Sucia HC cuyas letras tienen una fuerte denuncia y contenido social. Desde hace más de un año se vinculó a la ACEU en donde ayudó activamente a la construcción del 6º Foro Nacional de los Universitarios Frente al Conflicto, participa en el Colectivo EcoLector en donde llevan a cabo un acompañamiento ambiental en la comuna 9 de Medellín y proyectan trabajo de sensibilización ambiental en la comunidad de la Quebrada La Clara del municipio de Caldas – Antioquia. Por su personalidad un tanto introvertida se hizo acreedor del apodo de “El Mudo”, pero cuando entra en confianza puede ser uno de los más burleteros con sus amigos y compañeros.

### **JUAN CAMILO ÁNGEL**: 

25 años; estudiante de Química Farmacéutica, quinto semestre, apasionado de la Farmacología y destacado estudiante de su Facultad, también deportista en formación de Taekwondo de la Universidad; junto con el Colectivo EcoLector de la ACEU estuvo en la preparación y desarrollo del Festival “Seamos Útiles” en la comuna 9 de Medellín el pasado 13 de febrero de 2016, en donde se recogieron cientos de kits escolares para los niños menos favorecidos de la comuna; su trabajo en la ACEU desde hace más de un año se ha centrado en la defensa del ambiente y la lucha contra las políticas del Rector Mauricio Alviar. Amante de los perros y la jardinería.

### **SANTIAGO ÁNGEL**: 

24 años; estudió Biología en la Universidad hasta el año 2013 y continuó su formación en la Universidad de la vida, decidió dedicarse a la agricultura en el municipio de Granada - Antioquia donde siembra café, hortalizas, aguacates, plantas aromáticas y árboles frutales; asiduo lector de botánica y la ornitología. Junto con Juan Camilo, su hermano, desarrollan una huerta en su casa de Caldas – Antioquia. Su trabajo en la ACEU se centra en el proceso de sensibilización ambiental en la Quebrada La Clara de este municipio, buscando conciencia en la comunidad del impacto que trae arrojar basuras y lavar vehículos en este afluente. Aficionado al ciclismo, deporte que practica todos los días.

Con información de la ACEU.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
