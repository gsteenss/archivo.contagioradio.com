Title: Apoyo unánime de la CELAC para la paz de Colombia
Date: 2016-01-28 13:04
Category: Nacional, Paz
Tags: CELAC, ONU, proceso de paz, Zika
Slug: celac-apoya-proceso-de-paz-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/CELAC-e1454003512407.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Presidencia de Colombia 

###### [28 Ene 2016] 

Con apoyo unánime la Comunidad de Estados Latinoamericanos y Caribeños, **CELAC, aceptó la propuesta que se realizó desde la mesa de negociaciones en la Habana**, para que junto a la ONU, participe en la verificación del fin del conflicto y la dejación de armas.

“Apoyamos con todo nuestro corazón al presidente Juan Manuel Santos en el búsqueda de la paz definitiva para Colombia y entregamos una CELAC con toda la capacidad de apoyar la verificación de cese al fuego y dejación de armas en Colombia, pero **la paz no es solo ausencia de guerra, debe ser sobre todo presencia de justicia, dignidad  y de oportunidades** para todos. Las respuestas a la inequidad deben ser contundentes”, aseveró el presidente de Ecuador, Rafael Correa.

Es así como se dispondrá una Misión Política Especial, de la cual varios de los 33 países que hicieron parte de la Cumbre, participen de ese proceso de verificación una vez se firmen los acuerdos pactados.

En respuesta a esa decisión, el presidente Juan Manuel Santos, mostró su agradecimiento a los países por su aporte a las conversaciones en Cuba indicando que **“el apoyo por unanimidad de la misión especial hace irreversible este [proceso de paz](https://archivo.contagioradio.com/?s=proceso+de+paz+)”**. De manera que, de acuerdo con el mandatario,  falta esperar que las Naciones Unidas escojan el personal que hará parte de la verificación.

Por su parte, Carlos Medina Gallego, miembro del Centro de Pensamiento y Seguimiento al Diálogo de Paz, afirmó en diálogo con Contagio Radio que se trata de "un espaldarazo muy importante que se le da al proceso de paz… las declaraciones de las ONU y de la CELAC constituyen un importante reconocimiento de la comunidad internacional a los esfuerzos que el país viene haciendo a través de la mesa de negociaciones en la Habana”, y concluye que “lo acontencido en la CELAC, es una muestra de que el mundo encuentra en la razones a través de las cuales se llegó esa mesa de conversaciones de paz, los fundamentos esenciales para construir no solo la paz de Colombia, sino la de toda la región”.

### **Otros temas tratados en la CELAC** 

Juan Manuel Santos y Nicolás Maduro aprovecharon la cumbre, para reunirse y dialogar frente a la **situación de la frontera colombo-venezolana** que se encuentra cerrada desde el pasado 19 de agosto de 2015 por orden del jefe de Estado venezolano.  Durante la reunión los presidentes acordaron desarrollar siete puntos que abordan temas relacionados para enfrentar las bandas criminales, el contrabando y el narcotráfico.

**El virus del Zika** fue otro de los temas tratados durante el desarrollo de la Cumbre. Frente a esta situación que afectará a toda América, según informó la Organización Mundial de la Salud (OMS), se acordó programar rápidamente una reunión en la que participen todos los ministros de salud, para intercambiar estrategias, información y experiencias para combatir el virus.

**El fin del bloqueo económico para Cuba,** fue otro de los temas de la CELAC. La declaración final de la IV Cumbre solicitó fin del bloqueo económico que mantiene Estados Unidos contra la isla desde hace más de 50 años.

Así mismo, se habló de otros temas como: Resguardar y recuperar bienes culturales de la región, acciones en defensa de la recuperación de las Islas Malvinas, debates sobre Paraguay como Estado sin litoral, apoyar políticas en defensa de la protección de migrantes en América Latina, la transparencia en la lucha contra la corrupción, apoyar políticas en resguardo de océanos y mares, destinar recursos en el desarrollo energético, seguir apoyando el proceso de paz de Colombia, no abandonar el tema de  países de renta media y altamente endeudados y se firmó el tratado de prohibición de ensayos nucleares.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU)
