Title: Notas sobre el nuevo acuerdo FARC-Estado
Date: 2016-11-18 07:00
Category: Cesar, Opinion
Tags: FARC, nuevo acuerdo, Nuevo Acuerdo de Paz
Slug: el-nuevo-acuerdo-farc-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/acuerdo-de-paz-gobierno-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### Foto: Farc 

#### Por  **[Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)** 

###### 18 Nov 2016 

Y bueno, una de las incertidumbres de hoy es qué postura asumir frente al nuevo acuerdo. El anterior texto nos era conocido y junto con millones de colombianos llamamos a votar por el SÍ en el plebiscito que se convocó para el 2 de octubre de 2016; un SÍ sincero, con convicción; un SÍ político, activo; un SÍ para que el conflicto armado terminara y las Víctimas (todas) fueran reconocidas como tales; un SÍ de lucha y de resistencia; un SÍ por la verdad histórica y judicial.

[Hoy estamos frente a una realidad diferente. Independientemente de que el triunfo del NO haya sido producto del fraude y de la proyección del miedo y el oscurantismo religioso, se ha convalidado en los hechos. Por eso hay un nuevo texto, un nuevo acuerdo.]

[Cuando llamamos a votar por el SÍ dijimos al mismo tiempo que las FARC no nos representaba; no en un sentido individual sino colectivo, por supuesto. Hoy reafirmamos ese criterio. Nunca hemos confiado en el stalinismo ni en la socialdemocracia; y las FARC tienen tanto de uno como de la otra.]

[Escuchar y leer el discurso de Iván Márquez luego del nuevo acuerdo produce náuseas. Iguales, o peores mejor, a las que sufrimos cuando Tsipras, el griego, luego del referendo popular que votó que NO, dijo que sí, que él aceptaría el paquete neoliberal de la troika; iguales, o peores, mejor, a las que sufrimos cuando el PSOE, el español, decidió abstenerse para que el franquista Rajoy siguiera gobernando.]

[Tiempos difíciles vivimos y tiempos difíciles se avecinan. Los mayores de Occidente hablaban de “malestar en la cultura”, de “crisis de la civilización”; por su lado los mayores-otro (y sus hijos-otro)  nos advertían (advierten) de la crisis del colonialismo (Césaire), de la crítica de la razón negra (Mbembe) y de los condenados de la tierra (Fanon). Efectivamente, asistimos a la crisis globalizada del capitalismo, de la modernidad (Marx); sufrimos la guerra permanente y su corolario termidoriano: el Estado de Excepción mundial y su dispositivo biopolítico y cosificador.]

[Por eso hoy un discurso como el de Iván Márquez, de aceptación, de conformismo, de adaptación frente a lo real-moderno genera animadversión. Está hecho a la medida de la ideología del “progreso”, de la razón ilustrada, del tiempo histórico cerrado e infinito, del “desarrollo” y de su teodicea justificatoria. Es tramposo incluso cuando apela al Dios universal  para que bendiga el nuevo acuerdo, porque los creyentes  vencidos saben  que son vanas palabras colocadas para impresionar y captar incautos.]

[Con todo, el nuevo acuerdo aún contiene aspectos - debilitados eso sí -  que hay que sostener: la figura del delito político, la justicia transicional y el derecho a la Verdad con epicentro en las Víctimas.]

[El Nuevo Acuerdo es, claro, uno entre dos partes: FARC y Estado. Con total independencia frente a ellas lo haremos valer, en el entendido, primero, de que el  partido-frente que reemplace a la guerrilla ni copa el espectro]*[democrático-antidemocrático]*[ ni será el  “representante” social del mismo y, segundo, de que es un]*[comienzo que continúa]*[ la aspiración y la lucha por la democracia, la Verdad y la justicia eco-ambiental. Es por ello por lo que, tal y como está la situación, la mejor forma de refrendar el nuevo texto es por la vía del Cabildo Abierto.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.] 
