Title: "Régimen político es discriminatorio y favorece a los partidos tradicionales" Aida Avella
Date: 2015-10-27 15:49
Category: Nacional, Política
Tags: Aida Avella, Alcaldía de Bogotá, Alejandro Ordoñez, Clara López, elecciones regionales Colombia, Enrique Peñalosa, Gustavo Petro, Polo Democrático Alternativo, Unión Patriótica
Slug: regimen-politico-es-discriminatorio-y-favorece-a-los-partidos-tradicionales-aida-avella
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/623053-1076929.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.lapatria.com]

<iframe src="http://www.ivoox.com/player_ek_9183378_2_1.html?data=mpallZibfI6ZmKialJuJd6Kmk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOms4qwlYqlfcjdzsrbjdXTsIa3lIquptnNp9CfxtiYxs7Xp9Pdzs7bw9nTtsrjjN6YyMbas9PZxMqYw5DQs9Shhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [ Aida Avella, Unión Patriótica] 

###### [27 Oct 2015] 

Aida Avella, presidenta de la Unión Patriótica, quien no alcanzó a obtener una curul en el Concejo pese a tener mayor cantidad de votos individualmente que otros candidatos que si lo lograron, asegura que esa situación se debe **“al régimen político discriminatorio que favorece a los partidos tradicionales  que siempre logran ganancias dada la normatividad jurídica”,** lo que a su vez convierte la **“política en una mercancía”.**

**“Seguiremos defendiendo la paz con justicia social”,** expresa la presidenta de la Unión Patriótica, quien añade que ahora, con la gobernabilidad de Bogotá en manos de Enrique Peñalosa, le preocupa  cuestiones como la minería en el Páramo de Sumapaz, que provocarán que el 40% del agua para los bogotanos esté en riesgo.

Frente al abstencionismo, Aida Avella, señala que la situación de pobreza y hambre en la que viven las personas de los estratos más bajos de la ciudad y en todo el país imposibilita que la población colombiana tenga conciencia sobre el valor del voto. Así mismo, debido a los múltiples escándalos de corrupción y compra de votos se genera “**el desencanto de la gente frente a la política”.**

Para la dirigente de la UP, los medios de comunicación hegemónicos siempre le han servido a la derecha colombiana, lo que se evidencia con “el matoneo” en las entrevistas que le realizaban a Clara López, y los ataques constantes a la administración del alcalde Gustavo Petro **durante sus 4 años de gobierno, lo que sumado a la persecución del Procurador, Alejandro Ordoñez impidió que Petro pudiera gerenciar tranquilamente la ciudad.**

La preocupación de Aida Avella ahora gira en torno a la paz  ya que según ella, “los poderes mafiosos y paramilitares regionales ganaron las elecciones”, lo que para ella, no es un buen síntoma para los diálogos en la Habana, teniendo en cuenta que **Cambio Radical y los liberales “no son muy amigos de la paz”.**
