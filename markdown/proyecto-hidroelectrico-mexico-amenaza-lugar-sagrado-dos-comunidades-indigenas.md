Title: Proyecto hidroeléctrico en México amenaza lugar sagrado para dos comunidades indígenas
Date: 2017-06-29 07:41
Category: Ambiente, El mundo
Tags: Hidroeléctricas, mexico
Slug: proyecto-hidroelectrico-mexico-amenaza-lugar-sagrado-dos-comunidades-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/IMG_7015-960x500.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  [Animal Político]

###### [29 Jun 2017] 

Dos pueblos **indígenas mexicanos se unen para enfrentar un proyecto hidroeléctrico que destruiría el Keiyatsita**. Un lugar sagrado para ambas poblaciones y que además afectará las tierras de 33 comunidades por cuenta de la represa que quiere llevar a cabo la Comisión Federal de Electricidad, CFE.

Keiyatsita, es un centro ceremonial ubicado en Nayarit, donde ancestralmente se reúnen los pueblos indígenas Náyeri y Wixárika para la realización de distintos ritos espirituales y comunitarios, sin embargo es allí donde se tiene proyectada la represa Las Cruces. **Dicha construcción dejaría sumergida la comunidad de Rosarito, donde se ubica Keiyatsita, debido a una cortina de 188 metros de altura.**

### Denuncias de los indígenas 

Según el medio de comunicación mexicano, ‘Animal Político’, las comunidades denuncian que las autoridades gubernamentales **no han cumplido con el Convenio 169 de la Organización Internacional del Trabajo**, que entre otros aspectos obliga a los Estados adheridos a respetar la importancia especial que para las culturas y valores espirituales de los pueblos indígenas.

Aunque la empresa afirma que se realizó una consulta a las comunidades indígenas con la cual los pueblos avalaron la realización de la represa, se reconoce que **sólo 31% de los pobladores participó en esta consulta.** De hecho se habría tratado de una consulta que no fue organizada y realizada por las autoridades indígenas sino por la CFE, y que solo se tomó en cuenta a los que aceptaron acudir a sus reuniones.

Frente a ese reclamo el pasado 13 de junio lideres y lideresas indígenas fueron a Ciudad de México para advertir a las autoridades estatales que: **“No nos vamos a dejar. Ésa es nuestra iglesia, nuestro futuro y eso tiene que reconocerse”.**

Y es que el propio documento de la CFE señala que el Proyecto Hidroeléctrico Las Cruces, contempla una superficie de 5 mil 493 hectáreas, que afectarán las tierras de 33 comunidades, **Rosarito, de 54 habitantes quedaría completamente inundada.**

Además se asegura que el gobierno no contempló la importancia cultural del centro ceremonial Keiyatsita, ni de otros centros rituales, que también se verán afectados por el proyecto.

Uno de los líderes indígenas entrevistados por ‘Animal Político’ afirma que “no es un centro nada más religioso-espiritual, sino social también”. Y agrega que pese a que la represa no inundaría el centro ceremonial de La Muxatena, sí afectará su accesibilidad a distintos pueblos, que viven en la cuenca del Río San Pedro Mezquital, del cual se alimentará la presa Las Cruces.

### Los argumentos de la CFE 

Las autoridades aseguran que este lugar fue pensado para causar el menor “impacto cultural”, y también porque “cumple con las mejores condiciones hidrológicas, geológicas, sociales y culturales”.  Asimismo según la CFE, el proyecto sólo se afectará 3% del territorio que legalmente pertenece a estos pueblos.

De acuerdo con la CFE, **la represa atenderá parte de las necesidades de energía en el occidente del país, las cuales crecerán 3.7% al año hasta 2026.** Esto, teniendo en cuenta el agotamiento de fuentes petroleras y el aumento de los daños al ambiente, buscando la generación de energías limpias con el fin de que para el 2050, al menos la mitad de la electricidad provenga de fuentes que no generen emisiones al ambiente.

Pese a lo anterior, organizaciones ambientalistas como Greenpeace, indican que el aprovechamiento de la energía hidráulica con fines eléctricos implica el control artificial de ríos y cuencas fluviales,  lo que a su vez **promueve y contribuye al aumento de la temperatura de la tierra, ya que los embalses de las grandes hidroeléctricas que existen en el mundo emiten al menos 4% del total de gases de efecto invernadero**, debido principalmente a la emisión de metano generado por la materia orgánica en descomposición sumergida en las grandes reservas de agua, sin contar con la emisión de gases de dióxido de carbono y otros gases que se liberan durante la cadena de producción de hidroelectricidad.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
