Title: CONPAZ conmemora 18 años de la Operación Génesis
Date: 2015-02-23 22:15
Author: CtgAdm
Category: Paz, Resistencias
Tags: Conpaz, habana, proceso de paz
Slug: conpaz-conmemora-18-anos-de-la-operacion-genesis
Status: published

##### Foto: Justicia y Paz Colombia 

<iframe src="http://www.ivoox.com/player_ek_4124411_2_1.html?data=lZaflpmVdY6ZmKiakpqJd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh7DCsabHjcjTss7ZztTfw5CVfIzVhqigh6eVs9SfxcqYzsaPk9HZ08bQy4qnd4a2lNOYqYqnd4a1mtPS1c7XcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan Martínez, CONPAZ ] 

Este martes y miércoles, en el Centro de Memoria Histórica en Bogotá, se realizará una de las asambleas nacionales de la red CONPAZ, a propósito de los **18 años que se cumplen de la operación génesis.**

A la ciudad han llegado **400 delegados y delegadas de 11 departamentos**, que harán parte de este foro, donde se generarán y discutirán propuestas para el proceso de paz que se lleva a cabo en la Habana.

**“El desescalamiento del conflicto, la justicia, la comisión de la verdad y género, serán los temas puntuales sobre los que la red construirá sus propuestas**”, afirmó, Juan Martínez, integrante de CONPAZ de la comunidad de Tamarindo en el departamento de Atlántico.

Conpaz, está conformada por una red de comunidades que han sido víctimas de la violencia del conflicto armado, su objetivo es construir paz desde los territorios.
