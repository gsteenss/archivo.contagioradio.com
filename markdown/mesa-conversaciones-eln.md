Title: Se acaba el tiempo para reanudar la mesa de conversaciones con el ELN
Date: 2018-11-30 17:19
Author: AdminContagio
Category: Nacional, Paz
Tags: ELN, Iván Duque, Secuestro, Víctor de Currea
Slug: mesa-conversaciones-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/ivan-duque-y-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 Nov 2018] 

Aunque hay algunos intentos por parte de la sociedad civil, políticos y miembros de la comunidad internacional para empujar la reanudación de la mesa de conversaciones entre el Gobierno y el ELN, diferentes analistas han señalado que **las condiciones que ha puesto Duque a la mesa, solo generan más obstáculos para continuar con los diálogos**.

En opinión del analista y profesor universitario **Víctor de Currea-Lugo**, el anterior Gobierno dejó una mesa de negociación establecida y una agenda de conversaciones con puntos acordados; sin embargo, el presidente Duque ha sido reiterativo al señalar que hasta que el ELN no suspenda todas sus actividades de confrontación con el Estado, no se reiniciarán las conversaciones.

Para el profesor **el actual Gobierno no está entendiendo el momento histórico y su “soberbia” no le permite ver la importancia del mismo**. Como lo señala Currea, muestra de ello es que el Gobierno se volvió monotemático sobre el secuestro; señalando que el ELN debe dejar en libertad a todas las personas que tiene retenidas, pero sin señalar cuantas son, ni reconocer las liberaciones que se realizaron de forma unilateral en Arauca y Chocó.

Adicionalmente, según el analista, hay buenas iniciativas y “esfuerzos loables” para empujar la reanudación de las conversaciones, pero tienen un poder medido para presionar al Estado; a ello se suma que **embajadas como la de Suecia, cuidando sus intereses económicos, han decidido apoyar la política de paz del gobierno Duque**.(Le puede interesar: ["ELN afirma que Duque está haciendo trizas el proceso de paz"](https://archivo.contagioradio.com/eln-trizas-proceso-paz/))

El analista concluyó que, pese a las buenas intenciones de muchos actores, incluidas las embajadas de Noruega o Cuba, **el Gobierno Duque tiene una agenda de acabar el proceso**, “pero como no lo puede romper, lo que hacen es dejar la mesa en una especie de Congelador”. (Le puede interesar: ["Aún no sabemos para dónde va el Gobierno en materia de paz: Víctor de Currea-Lugo"](https://archivo.contagioradio.com/donde-va-gobierno-en-materia-paz/))

<iframe id="audio_30509010" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30509010_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
