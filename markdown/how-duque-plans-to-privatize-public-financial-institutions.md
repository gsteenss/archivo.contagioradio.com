Title: How Duque plans to privatize public financial institutions
Date: 2019-11-28 19:23
Author: CtgAdm
Category: English
Tags: Bicentennial Group, Iván Duque, privatization
Slug: how-duque-plans-to-privatize-public-financial-institutions
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Holding-financiero-la-jugadita-de-Duque-a-favor-de-los-inversionistas-privados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Contagio Radio] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[Amidst anti-government protests that have paralyzed the country, President Iván Duque has moved forward with a controversial financial policy. The presidential decree “Bicentennial Group,” with which Duque pretends to create a state-run financial holding, has raised alarm among workers’ organizations that consider the policy to be a “labor massacre.”]

[For organizations such as the Confederation of Workers of Colombia (CTC) and the Worker’s United Center (CUT), this new policy — if implemented — would provoke terrible consequences for workers and for citizens in general because it “eliminates the direct control the state has over money that flows within state-run financial companies.”]

### **What does financial holding mean?**

[There are 13 financial conglomerates in Colombia, according to the political analyst Daniel Libreros. The most important of these is the Sarmiento and Bancolombia conglomerates. These two groups hold almost 75% of pension funds in Colombia and are the ones most interested in pension reforms.]

[The holding consists of “coordinating all the companies under one same owner” to enter a capital market where actions, titles and financial investments are negotiated. According to Libreros, Duque pretends to unite the public financial institutions — such as the Icetex, Agrarian Bank and the National Fund of Savings — to produce a specific amount of money each month that will be sent to the capital market.]

### Where does this presidential decree come from?

[For the analyst, the Duque administration pretends to act along with the interests of the Organization for Economic Co-operation and Development (OCDE), the World Bank and the International Monetary Fund (IMF), which seek guarantees for investment in capital, such as pension funds, insurers and big banks.]

[This policy was approved as part of the National Development Plan, which was expedited as a decree. The quickness with which this was executed was justified by the president when he said that the deadline for congressional approval was Nov. 25.]

[What is most worrisome, according to the detractors of the decree, is that “the sense of public service is lost, and the state-run financial institutions become intermediaries in the business market to strengthen the private sector’s buy of shares and of titles.” This is what Libreros calls the “financialization” of the public entities. The administration becomes the intermediaries of businesses, takes funds from the state and converts this money to part of the businesses of private capital, expanded by the financial investors. This permits money to land in the businesses of those that act as intermediaries in shares and titles. ]

[The internal relationship of these businesses is of cost-benefit, seeking to produce a certain amount of money monthly. If the National Savings Fund produces more costs than benefits, the subsequent action to address the deficit would be to fire workers, which is the most worrisome part of the text of the decree that authorizes such actions.]

[Monthly production trumps any other consideration. If the Icetex has an interest rate that fails to produce profits, the next action would be to increase the interest rate so that it would be adjusted to the interest rate of the market. This could be considered an expropriation of the public patrimony to benefit private investors. ]

### Will the state become a third conglomerate?

[The financial holding — if it is created — would become a third financial conglomerate. The direct investment of the state would be close to 14. 5 billion of Colombian pesos, but the total of the patrimonies would be close to 84.5 billion. This would transform “the state into the subject of capital market businesses,” which would allow state money to enter the stock exchange. This would privatize public money.]

[Much has been said of the “paquetazo de Duque,” a series of reforms that would directly affect the citizenry and attack specifically the poorest classes. The financial holding would be part of these reforms since it would privatize the public patrimony that circulates money. For example, the Icetex, the number of people that are in debt and the family crises that it provokes. This would create higher interest rates,” said Libreros.]

[One of the strongest rejections felt in the demonstrations is against the way financial capital is eroding the money of Colombian families. Libreros said that various sectors of society are asking for the executive branch to take into consideration their fundamental rights when taking big decisions.]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
