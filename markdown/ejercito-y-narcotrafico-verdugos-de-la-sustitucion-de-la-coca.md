Title: Ejercito y narcotráfico: verdugos de la sustitución de la Coca
Date: 2020-06-03 18:36
Author: CtgAdm
Category: Actualidad, DDHH
Slug: ejercito-y-narcotrafico-verdugos-de-la-sustitucion-de-la-coca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Erradicacipon.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: COCCAM*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a las solicitudes y tutelas interpuestas para detener las incursiones del Ejército en los territorios para ejecutar labores de [erradicación forzada](https://archivo.contagioradio.com/persecucion-y-desplazamiento-campesino-el-resultado-de-incumplimiento-del-gobierno-en-guaviare/), **el Gobierno sigue, en medio de la pandemia, realizando acciones que vulneran múltiples derechos de las comunidades.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto organizaciones nacionales e internacionales defensoras de Derechos Humanos y ambientales, hicieron un llamado para que se detuvieran los procesos de erradicación forzada y a que se cumpla lo pactado en el [Plan de Sustitución](https://archivo.contagioradio.com/suspension-de-audiencia-virtual-el-primer-paso-en-la-lucha-contra-el-glifosato/) y se detenga la militarización de los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo a pesar de ello cada vez son más frecuentes los enfrentamientos entre [grupos armados](https://archivo.contagioradio.com/a-pocos-kilometros-de-base-militar-binacional-funcionaria-una-pista-del-narcotrafico-en-choco/) por la disputa de las llamadas "rutas del narcotráfico"; y las movilizaciones de las comunidades para que pare la violencia en su territorio, acción que responden con más violencia las tropas del Ejército.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Otra Mirada, hablamos con **Deivin Hurtado, vocero de la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana** (COCCAM); quien explicó el estado de la sustitución voluntaria en Colombia a la luz de tres factores de riesgo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Accionar del Ejército en contra de la sustitución voluntaria

<!-- /wp:heading -->

<!-- wp:paragraph -->

El 16 de marzo unidades de la Fuerza de Tarea Vulcano, realizaban acciones de erradicación en Sardinata, Santander. La comunidad se organizó para defender de manera pacifica los cultivos**, a pesar del ello el Ejercito realizó un disparo que acabó con la vida de Alejandro Carvajal.** (Le recomendamos leer:[Alejandro Carvajal, joven de 20 años asesinado por el Ejército en Catatumbo: ASCAMCAT](https://archivo.contagioradio.com/alejando-carvajal-joven-de-20-anos-asesinado-por-el-ejercito-en-catatumbo-ascamcat/)).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El 22 de abril en Tumaco, Nariño, campesinos e indígenas que se oponían a la intervención de la Fuerza Pública en nuevas acciones de erradicación, fueron amenazados con armas de fuego, hecho que causó el **asesinato del Indígena Awá, Ángel Artemio Nastacuaz.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunos días después el 18 de mayo en zona rural de Cúcuta, **fue herido de gravedad por un disparo y posteriormente falleció, **el campesino[Digno Buendia](https://archivo.contagioradio.com/en-medio-de-operacion-militar-fue-asesinado-el-indigena-joel-villamizar/)****, a causa de un disparo de un integrante del Ejército, este mismo día resultaron heridos 3 campesinos más.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El caso más reciente fue el 26 de mayo en **[Vista Hermosa Meta](https://archivo.contagioradio.com/ilario-mecha-guardia-indigena-wounaan-asesinado-en-bogota/), en donde se denunció nuevamente agresiones por parte de integrantes del Ejército** hacia la comunidad campesina que protestaba en contra de la erradicación; allí se registraron tres campesinos heridos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante estos y otros hechos que muy probablemente se desconocen, Hurtado señaló que *"siempre en los procesos de erradicación hay enfrentamientos entre la Fuerza Pública y las comunidades, y lastimosamente siempre hay un muerto de por medio"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al accionar del Estado se suma la reciente llegada de tropas [militares estadounidenses](https://archivo.contagioradio.com/operaciones-militares-de-eeuu-en-colombia-deben-ser-autorizadas-por-el-congreso/)al país con el supuesto objetivo de apoyar la lucha contra el narcotrático.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto Hurtado afirmó, *"estos extranjeros vienen con unas garantías jurídicas lo que no permite al Estado colombiano por las acciones violentas que realicen el país, **dándoles carta blanca para que hagan más de lo que ya han hecho**"*. (Le puede interesar leer:[Ejército quería que Ariolfo Sánchez fuera un "falso positivo": Comunidad de Anori, Antioquia](https://archivo.contagioradio.com/ejercito-queria-que-ariolfo-sanchez-fuera-un-falso-positivo-comunidad-de-anori-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Grupos armados ilegales

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por otro lado el vocero de la Coccam, destacó que **las comunidades que cultivas son solo víctimas del olvido Estatal y de grupos narcotraficantes que usan la necesidad** de las personas como alivio a sus acciones delictivas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Las comunidades cultivan por necesidad, también porque a muchos los amenazan con que o cultivan o los matas, **jamás lo hacen por buscar la riqueza**"*, y resaltó que ante los incumplimientos a los Acuerdo, la comunidad se ha escudado en estos grupos ilegales que les ofrecen protección.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Este Gobierno de alguna manera entonces permite que los grupos ilegales acaparen muchas zonas, **dando la espalda a lo que sucede y dejando en manos de los líderes la responsabilidad de la paz**"*, ejemplo de ellos los hechos de desaparición, amenazas y hostigamiento a defensores de la sustitución voluntaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tan solo en el departamento del Cauca en los últimos meses se han registrado 15 asesinatos a miembros de la Coccam, y que según Hurtado *"lo único que han hecho es apostarle a la sustitución de cultivos".* (Le puede interesar:[Persecución y desplazamiento campesino: el resultado de incumplimiento del Gobierno en Guaviare](https://archivo.contagioradio.com/persecucion-y-desplazamiento-campesino-el-resultado-de-incumplimiento-del-gobierno-en-guaviare/)).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Menos sustitución, más erradicación forzada

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hurtado de igual forma destacó que en los territorios existe una clara intención de cumplir lo pactado, sin embargo señaló que **es imposible basar una economía rentable en otros cultivos, cuando la realizar no permite que los producto salga al menos del territorio**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Hoy por lo menos se puede cambiar y **sustituir su cultivo por tomate pero si no hay vías para sacar ese tomate, si no hay quien lo compre, pues es difícil** y la historia de Colombia a mostrado eso, que no hay garantías después de tantos años para la sustitución voluntaria"*, agregó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y afirmó que este es solo un ejemplo básico antes las acciones que ha hecho la comunidad para cumplir los Acuerdo; destacando, que pese a los esfuerzos *"este es un **Gobierno que le apuesta a cumplir nada, porqué no tienen interés en cambiar la violencia, ellos viven de esa violencia** y del discurso guerrerista".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Aún hay esperanza en materia de sustitución voluntaria en Colombia?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las comunidades a pesar de tantos incumplimientos al Programa Nacional Integral de Sustitución de Cultivos Ilícitos (Pnis) y vulneraciones a sus derechos, según Hurtado, se siguen organizando y generando procesos para que sea posible una real sustitución voluntaria.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Se sigue resaltando la necesidad de organizarse en pro de **defender los Acuerdo de Paz, no solo el punto de los cultivos de Uso Ilicito**, porqué el acuerdo en su totalidad da garantías para una mejor vida para campesinos y campesinas"* .

<!-- /wp:quote -->

<!-- wp:paragraph -->

A esta lucha se suman pronunciamientos como los de la Defensoría del Pueblo, quien señala, *“debe de contarse con una ruta para garantizar la estabilización socioeconómica urgente de las familias que hayan resultado o resulten afectadas por los operativos de[erradicación forzosa](http://www2.justiciaypazcolombia.com/continuan-labores-de-erradicacion-forzada-en-la-zrcpa-sin-medidas-de-proteccion-para-covid19/)”* .

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DefensoriaCol/status/1267409470780510208?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1267409470780510208\u0026ref_url=https%3A%2F%2Ftwitter.com%2FDefensoriaCol%2Fstatus%2F1267409470780510208","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DefensoriaCol/status/1267409470780510208?ref\_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1267409470780510208&ref\_url=https%3A%2F%2Ftwitter.com%2FDefensoriaCol%2Fstatus%2F1267409470780510208

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

O como las de cerca de 100 organizaciones que se han opuesto a las aspersiones aéreas, y las audiencias virtuales para el regreso del glifosato al país. (Lea también:[Suspensión de audiencia virtual un paso en la lucha contra el glifosato](https://archivo.contagioradio.com/suspension-de-audiencia-virtual-el-primer-paso-en-la-lucha-contra-el-glifosato/)).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y que han dado como resultado la orden interpuesta por el Juzgado II de Nariño, a organizaciones como la ANLA y la Policía Nacional para que se suspender toda acción relacionada al glifosato en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"El uso del glifosato para quienes están de acuerdo es fácil decir que es como tomar un vaso de agua, pero nosotros sabemos del daño, la muerte y las perdidas que trae consigo este veneno"*; señaló Hurtado, y agregó que el único interés de esta práctica *"es el beneficio económico que trae para el Gobierno, porque es la forma más barata y rápida para erradicar".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y finalizó enfatizando que los campesinos van a seguir en la resistencia impidiendo que se sigan dando acciones violentas en tu territorios, " *los campesinos defenderán muy seguramente con su cuerpo y celulares lo que ocurra en el territorio, **pero lógicamente no pueden solos, y es allí donde entra la movilización social que les permita saber que no están solos"***.

<!-- /wp:paragraph -->
