Title:  Le escribo ante la imposibilidad de hablar : La gota que rebosó la copa
Date: 2019-07-04 11:36
Author: A quien corresponde
Category: Opinion
Tags: Conferencia de los Obispos, Crisitianos, politica, Religión
Slug: le-escribo-ante-la-imposibilidad-de-hablar-cordialmente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Examínenlo todo y quédense con lo bueno

(1Tesalonisenses 5,21)

Bogotá, 29 de junio del 2019.

Diferenciar entre información y opinión

es necesario para tomar una buena decisión.

Estimado

**Hermano en la fe**

**Cristianas, cristianos, personas interesadas**

Cordial saludo,

Le escribo ante la imposibilidad de hablar cordialmente. Lo hago públicamente porque hay muchas personas en situaciones parecidas.

Tomé la decisión de escribirte porque se ha vuelto imposible hablar, crítica y argumentadamente, de política, de religión,  del “proceso de paz", de la corrupción y sus causas, de la ética civil, de la moral pública, entre otros temas.

"**La gota que rebosó la copa**"

Fue cuando me descalificaste, groseramente, por mis opiniones gritándome: “*estás ideologizado y politizado, mi religión no tiene nada que ver con política o ideología*". Con tu afirmación y tus gritos me dejaste sin derecho a hablar y opinar; asumiste una actitud autoritaria, como poseedor de la verdad completa, sin necesidad de argumentar tus opiniones y sin dar razón de tus afirmaciones, la mayoría de ellas reproducidas por medios de información o redes sociales afines a tu manera de ver y pensar las cosas.

Es evidente que con las mismas palabras hablamos de realidades distintas, por ejemplo, ideología. El Diccionario de la Real Academia de Lengua Española (RAE), dice que **ideología** es un "*conjunto de ideas fundamentales que caracteriza el pensamiento de una persona, colectividad o época, de un movimiento cultural, religioso o político*". Las ideologías describen modos de ver y actuar sobre la realidad colectiva, ya sea sobre el sistema general de la sociedad sobre sistemas específicos, como el económico, social, científico-tecnológico, político, cultural, religioso, ambiental.

Toda persona, comunidad, grupo humano, religioso o político tiene una ideología, que, consciente o inconscientemente, determina, guía, orienta, condiciona su visión de la realidad, sus opiniones, sus valoraciones, sus actuaciones y sus criterios. En este sentido, tu y yo somos personas con ideología, aunque no nos demos cuenta de ello y los mejor que podemos hacer es reconocerlo, sin descalificarnos.

 

La  RAE:  define la **política** como:

el "*Arte, doctrina u opinión referente al gobierno de los Estados. Actividad de quienes rigen o aspiran a regir los asuntos públicos. Actividad del ciudadano cuando interviene en los asuntos públicos con su opinión, con su voto, o de cualquier otro modo*".

 

La III Conferencia de los Obispos latinoamericanos, reunidos en la ciudad de Puebla en el año 1979, dice que: "La dimensión política, constitutiva del hombre, representa un aspecto relevante de la convivencia humana. Posee un aspecto englobante, porque tiene como fin el bien común de la sociedad. La fe cristiana no desprecia la actividad política; por el contrario, la valoriza y la tiene en alta estima" (Puebla, conclusiones 513 y 514).

Popularmente, política es igual a política partidista, es decir, las prácticas electorales, la manera como los partidos políticos se organizan y proyectan para alcanzar, manejar y conservar el poder, según sus intereses, visiones y comprensiones políticas.

También se puede hablar de orientación política, del conjunto de ideas y creencias, ligadas a la ideología, que todos tenemos y con las cuales, consciente o inconscientemente, tomamos decisiones y hacemos valoraciones sobre lo público, el manejo del estado, sobre las relaciones sociales, culturales, humanas y familiares.

Las opiniones tuyas y las mías, están determinadas por nuestra orientación o visión política, ya sea que hablemos de religión, de economía, del ambiente, de la paz, de la cultura, de la educación, la salud" y, ordinariamente, lo hacemos sin darnos cuenta.

Me parece que tus opiniones y afirmaciones obedecen a una ideología y a una visión política del mundo que se considera la buena, la mejor, la cristiana y por eso descalificas a quienes no piensan como los tuyos. Por tu orientación política e ideológica no reconoces o minimizas la corrupción, los delitos, la doble moral y la responsabilidad en la crisis humana y social del país de los políticos y grupos económicos de tu gusto. Sin darte cuenta asumes una doble medida para los hechos negativos: consideras muy grandes los de los "los otros" y muy pequeños "los tuyos". Además, reproduces mentiras o verdades a medias en favor de quienes están de tu lado y los consideras buenos.

Olvidas que un mismo hecho, una misma situación tiene distintas miradas y comprensiones de acuerdo a los intereses y perspectivas políticas e ideológicas, es decir, que no hay una versión totalmente neutra e imparcial de los hechos, reconocerlo nos lleva a una mejor comprensión de la realidad y evita el irrespeto y la descalificación en las discusiones.

Como cristianos, deberíamos tener en cuenta la pregunta de Jesús: “*¿Por qué te fijas en la pelusa que está en el ojo de tu hermano y no miras la viga que hay en el tuyo?*" (Mateo 7,3; Lucas 6,41).

Para hablar decentemente debemos reconocer que hay distintas versiones de la realidad de acuerdo a los intereses políticos y económicos, según la ideología; debemos escuchar los argumentos de las distintas partes antes de hacer afirmaciones contundentes y descalificadoras.

Al final de la primera carta a los Tesalonicenses, San Pablo, hace una recomendación a los cristianos de su tiempo, que nos vienen bien a los cristianos de hoy: “*Cuidado, que nadie devuelva mal por mal; busquen siempre el bien entre ustedes y con todo el mundo" **examínenlo todo y quédense con lo bueno,** eviten toda forma de mal*" (1Tesalonicenses 5,15. 21-22).

Fraternalmente, su hermano en la fe,

P. Alberto Franco, CSsR, J&P

<francoalberto9@gmail.com>
