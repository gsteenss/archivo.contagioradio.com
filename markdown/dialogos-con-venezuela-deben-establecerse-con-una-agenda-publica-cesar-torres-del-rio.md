Title: Acercamientos entre Colombia y Venezuela deben tener una agenda pública
Date: 2015-09-17 13:18
Category: El mundo, Nacional, Política
Tags: César Torres Del Río, cierre de frontera colombo venezolana, crisis fronteriza, Deportados, Diálogo Bilateral, Dialogos Santos y Maduro
Slug: dialogos-con-venezuela-deben-establecerse-con-una-agenda-publica-cesar-torres-del-rio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/santos_y_maduro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: radio970.com 

<iframe src="http://www.ivoox.com/player_ek_8439109_2_1.html?data=mZmgm5aUfY6ZmKiak5WJd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMqZpJiSo5bQs8jj1JDQ0dOPmsbixt%2Fix9HFb8XZw8rbjcrXuMLWzcrQx9fXqYzX0NOY19PFb8LbxtOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [César Torres del Río, Analista político] 

###### [17 sep 2015]

Frente a la reunión que se llevará el próximo 21 de septiembre entre los mandatarios de Colombia y Venezuela, se espera que se den las condiciones para **establecer una agenda pública** en la que se discutan las problemáticas que hoy afectan las zonas fronterizas de los dos países, afirma el analista político e internacional, César Torres del Río.

De acuerdo con el analista, el contrabando, información sobre actividades mafiosas a lado y lado de la frontera, y la lucha contra el paramilitarismo “**se deben discutir en términos de planes de Estado**” a partir de una agenda pública, de manera que "la diplomacia deje de ser secreta y distintos sectores puedan participar”, indica César Torres del Río.

Torres también asegura que es importante que esa agenda sea "discutida en el seno mismo de organismos colombianos”, con el fin de **tomar medidas de control desde adentro y adquirir fortaleza en las discusiones** que se den entre las cancillerías.

Pese a que estas discusiones no son garantía de soluciones, el analista dice que “esta situación se ha dado debido a una obligación impuesta por las circunstancias mismas y las solicitudes de distintos sectores”.

### **Un pésimo papel de los medios Hegemónicos:** 

Según César Torres del Río, “h**ay una corriente de derecha que le ha apuntado desde el comienzo a la caída de Maduro**”, mostrando una tendencia intervencionista en lo que ha representado esta crisis en la frontera.

el analista, también hizo un llamado a que los **medios alternativos tengan manifestaciones de solidaridad**, con análisis críticos sobre la relación de hermandad de los dos países, “para mantener la solidaridad y que se atienda inmediatamente la crisis con los deportados de Venezuela”.

 
