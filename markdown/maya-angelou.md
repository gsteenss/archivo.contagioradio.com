Title: La singular vida y luchas de Maya Angelou
Date: 2018-04-06 11:45
Category: Viaje Literario
Tags: afro, Derechos, mujer, poesia
Slug: maya-angelou
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/o-MAYA-ANGELOU-facebook.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:BBC 

###### 4 Abr 2018 

Una mujer extraordinaria que siempre se mantuvo en pie de lucha, poetisa, periodista, actriz, escritora, bailarina, productora de cine y televisión, incluso se convirtió en la primera mujer negra en conducir un tranvía, simplemente fue Maya Angelou.

Nacida el 4 de abril de 1928 en Saint Louis, Misuri, Estados Unidos en una familia con problemas económicos y marginados por el trabajo de su madre quien fue prostituta por unos años. Maya creció en un ambiente negativo por el que tuvo que vivir varias experiencias negativas, a los 8 años fue violada por el novio de su madre. Hecho que le provocó mutismo patológico por 5 años.

En medio de la adversidad encontró un refugio en las letras, lo que le ayudó a superar los traumas de una infancia tormentosa, adquiriendo tal fortaleza que con el pasar de los años le sirvió para superar la pobreza, la violencia y la segregación racial. (Le puede interesar: [48 frases en las que vive Martin Luther King](https://archivo.contagioradio.com/48-frases-en-las-que-vive-martin-luther-king/))

Fue la primera escritora afroestadounidense en escribir un best seller: *Sé por qué canta el pájaro enjaulado*, libro autobiográfico que narra como sobrellevo los hechos dramáticos que enfrentó en su niñez y la vida de la mayor parte de la población negra en el Sur de los Estados Unidos durante la primera mitad del siglo XX.

Amiga de Martin Luther King y Malcom X con los que trabajó por varios años por el movimiento de los derechos civiles. Maya Angelou viajó por África como periodista y ayudante en varios movimientos independentistas africanos. Escribió 36 libros, que en su mayoría denuncian el racismo y la exaltación del valor de la perseverancia y la autoestima.

Esta mujer icónica que fue galardonada con más de 50 premios al destacarse como activista de derechos humanos, falleció a la edad de 86 años el 28 de mayo 2014, hoy la recordamos con uno de sus mas bellos poemas.

***Still Rise***

“Tú puedes escribirme en la historia  
con tus amargas, torcidas mentiras,  
puedes aventarme al fango  
y aún así, como el polvo… me levanto.  
¿Mi descaro te molesta?  
¿Porqué estás ahí quieto, apesadumbrado?  
Porque camino  
como si fuera dueña de pozos petroleros  
bombeando en la sala de mi casa…  
Como lunas y como soles,  
con la certeza de las mareas,  
como las esperanzas brincando alto,  
así… yo me levanto.  
¿Me quieres ver destrozada?  
cabeza agachada y ojos bajos,  
hombros caídos como lágrimas,  
debilitados por mi llanto desconsolado.  
¿Mi arrogancia te ofende?  
No lo tomes tan a pecho,  
Porque yo río como si tuviera minas de oro  
excavándose en el mismo patio de mi casa.  
Puedes dispararme con tus palabras,  
puedes herirme con tus ojos,  
puedes matarme con tu odio,  
y aún así, como el aire, me levanto.  
¿Mi sensualidad te molesta?  
¿Surge como una sorpresa  
que yo baile como si tuviera diamantes  
ahí, donde se encuentran mis muslos?  
De las barracas de vergüenza de la historia  
yo me levanto  
desde el pasado enraizado en dolor  
yo me levanto  
soy un negro océano, amplio e inquieto,  
manando  
me extiendo, sobre la marea,  
dejando atrás noches de temor, de terror,  
me levanto,  
a un amanecer maravillosamente claro,  
me levanto,  
brindado los regalos legados por mis ancestros.  
Yo soy el sueño y la esperanza del esclavo.  
Me levanto.  
Me levanto.  
Me levanto.”
