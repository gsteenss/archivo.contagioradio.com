Title: Cuatro acciones con las que se quiere atacar a la Colombia Humana
Date: 2018-12-10 12:47
Author: AdminContagio
Category: Entrevistas, Política
Tags: Alvaro Uribe, Colombia Humana, Gustavo Petro, Nestor Humberto Martínez
Slug: atacar-colombia-humana
Status: published

###### [Foto: @angelamrobledo] 

###### [10 Dic 2018] 

Líderes de la Colombia Humana **denunciaron ser víctimas de persecución política, jurídica y económica, aludiendo como causa la investigación adelantada por la bancada de oposición**, sobre el fiscal Néstor Humberto Martínez en el caso Odebrecht; y por la intención de eliminar al movimiento político de la contienda presidencial para 2022.

Jorge Rojas, precandidato de esa colectividad para la Alcaldía de Bogotá, afirmó que el “Debate del Siglo” del pasado martes 27 de noviembre, puso el dedo en la llaga y evidenció los más altos niveles de corrupción a nivel político y económico del país, añadiendo quela discusión no fue sobre corrupción como la de “los ñoños y las gatas”, sino que “involucra al expresidente Uribe, al expresidente Santos, a Luis Carlos Sarmiento Angulo, y Germán Vargas Lleras”, de allí que se estén tocando fibras sensibles del poder en Colombia.

El líder del movimiento político, aseguró que tocar esas fibras genera una reacción, y dicha respuesta incluiría evitar que se llegue a fondo en la investigación del caso Odebrecht, y que Gustavo Petro pueda ser candidato presidencial en 2022. De allí se explica en su criterio que quieran acabar con toda alternativa de poder eliminando a Petro “por la vía jurídica, económica, política e incluso, podrían intentarlo por la vía física”.

### **Las 4 acciones de persecución política que denuncia Colombia Humana** 

A través de su cuenta de Twitter, el senador Gustavo Bolivar informó que Alix Lesmes, una funcionaria que trabajó en la campaña de la Colombia Humana s**ufrió un robo en el que se llevaron únicamente su computador, dispositivo que contenía una base de datos de la militancia del movimiento político** y “otras informaciones de campaña”. (Le puede interesar: ["Negar personería jurídica a Colombia H. es un mal mensaje para la oposición"](https://archivo.contagioradio.com/negacion-de-personeria-juridica-por-parte-del-cne-atenta-contra-la-oposicion-en-colombia/))

> Campañas q recibieron dinero d Odebrecht impunes mientras:  
> -Sacan del CNE libros contables de Col Humana.  
> -Abren indagación penal a Gerente d la campaña.  
> -Roban anoche computador con base de datos de la C.H  
> Mensaje:no hagan debates, no denuncien, no se metan con los amos del país [pic.twitter.com/y4lTWMgZiG](https://t.co/y4lTWMgZiG)
>
> — Gustavo Bolívar (@GustavoBolivar) [6 de diciembre de 2018](https://twitter.com/GustavoBolivar/status/1070785208406953984?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Sobre esta acción, Rojas recordó que era un procedimiento usado durante el gobierno de Álvaro Uribe Vélez, en el que se realizaban allanamientos ilegales, o se robaba información de las oficinas de organizaciones defensoras de derechos humanos para “establecer procesos o eliminar críticos y opositores al Gobierno”. (Le puede interesar:["Colombia H. denuncia amenazas de Aguilas Negras"](https://archivo.contagioradio.com/colombia-humana-denuncia-amenazas-de-aguilas-negras/))

La segunda acción ocurrió el 26 de noviembre, un día antes del “debate del siglo”, cuando la **Fiscalía acudió al Consejo Nacional Electoral, para indagar sobre las cuentas de la campaña para la presidencia de Gustavo Petro**; situación que Rojas afirmó que no se explicaba por qué el ente abría una investigación contra Petro, sabiendo que en su condición de senador solo puede ser juzgado por la Corte Suprema de Justicia. (Le puede interesar: ["Se quiere desviar la atención sobre las acusaciones a Néstor Humberto Martínez: Cepeda"](https://archivo.contagioradio.com/el-debate-contra-nestor-humberto-martinez-no-ha-acabado/))

A esa indagación se sumó la de Blanca Durán, gerente de la campaña de Colombia Humana, quien tendría abierta una investigación penal por una violación al código electoral, por el presunto ingreso de recursos provenientes del exterior. El precandidato a la alcaldía de Bogotá manifestó que **se trataba de una falsa financiación proveniente de Venezuela, que se correspondería con el ‘miedo al castrochavismo’** utilizado durante la campaña.

La más reciente de las acciones sería el video en el que Petro aparece contando unos fajos de billetes, material que Rojas calificó como “desafortunado”, pero que manifestó, **será la Corte Suprema de Justicia quien investigue y juzgue, hasta determinar si se cometió o no delito**. No obstante, el exsecretario de Integración Social de Bogotá, aseveró que conoce a Gustavo Petro hace mucho tiempo, y puede dar testimonio de su honestidad y pulcritud”.

<iframe id="audio_30703788" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30703788_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
