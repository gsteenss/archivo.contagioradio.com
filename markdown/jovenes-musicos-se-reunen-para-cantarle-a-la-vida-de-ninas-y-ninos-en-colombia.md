Title: Jóvenes músicos se reúnen para cantarle a la vida de niñas y niños en Colombia
Date: 2019-04-17 17:13
Author: CtgAdm
Category: Nacional
Tags: comunidades, jovenes, Música, reclutamiento forzado
Slug: jovenes-musicos-se-reunen-para-cantarle-a-la-vida-de-ninas-y-ninos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-17-at-4.16.49-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión de Justicia y Paz 

###### 17 Abr

Desde el pasado 13 al 15 de Abril del presente año, veintidós jóvenes provenientes de diversas regiones del país se reunieron en Popayán, en la Escuela de Formación Musical Artistas de la Vida, para continuar creando escenarios de construcción de paz, a través de la música, como parte de una estrategia para rechazar el reclutamiento forzado de menores al conflicto armado y **garantizar una infancia plena a los niños y niñas de las comunidades, víctimas de la guerra en Colombia.**

La Escuela es parte de creación de un escenario para afrontar las nuevas dinámicas del reclutamiento forzado de niñas, niños y jóvenes al conflicto armado y las nuevas formas de criminalidad. (Le puede interesar: ["Jóvenes de Colombia se movilizan en contra del cambio climático"](https://archivo.contagioradio.com/jovenes-colombia-se-movilizan-del-cambio-climatico/))

En este tercer encuentro, las y los jóvenes que llegaron de lugares como el Valle del Cauca, el Bajo Atrato Chocoano, Putumayo, Cauca y Meta, utilizaron los cantos, los ritmos y el baile, para crear mensajes de esperanza, que junto con instrumentos como la marimba, el cunuco, el clarinete, la tambora, la guitarra, los guasas, la quena, entre otros, dieron paso a la composición de canciones, que además de reflejar la convicción de los territorios por impulsar la paz, evidencia la fuerza de las generaciones más nuevas por **construir un bello existir donde los niños, niñas y jóvenes crezcan entre juegos y carcajadas sin sumarse a la guerra.**

Asimismo, la música permitió que las y los pequeños artistas conocieran más a fondo las dinámicas de la violencia, producto del conflicto armado, en contra de la infancia y las apuestas desde las comunidades para proteger a los niños y niñas.

“Qué nadie apague el sueño de jugar y sonreír”, “qué no existan más muertes de niños y líderes” y “qué los jóvenes no tengan que abandonar los territorios por temor a ser reclutados y se les quite la libertad” fueron algunas de las composiciones que realizaron durante este encuentro, en memoria a los 8 niños fallecidos en Alto Guayabal, en la región del Bajo Atrato, entre enero y marzo de 2019 y en homenaje a la vida de los niños, niñas y jóvenes de Colombia que re-crean la paz.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](https://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](https://bit.ly/1ICYhVU)[.]{.s1}
