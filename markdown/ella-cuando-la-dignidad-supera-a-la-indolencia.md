Title: "Ella", cuando la dignidad supera a la indolencia.
Date: 2015-06-03 14:31
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine Ciudad Bolívar, Dos de Dos producciones, Ella gana en Festival de Cine de Tigre, Ella película colombiana, Humberto Arango, Julio Contreras productor, Libia Stella Goméz
Slug: ella-cuando-la-dignidad-supera-a-la-indolencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/1004817_322522941215853_1090327803_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_4585713_2_1.html?data=lZqll5yVd46ZmKiakpuJd6KpmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMrWysaYtdnJsM3VjKySpZiJhpThxt%2BSlKiPqMrmxsjh0dfFb8XZjNHOjdXJsIa3lIqupsjZsMKfhpefp9HQpYamk5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Libia Stella Gómez directora y Julio Contreras Productor de "Ella" 

*Ella* es el segundo largometraje argumental de la directora Libia Stella Goméz, que llegará a las salas colombianas el próximo 11 de junio, casi 10 años después del estreno de su ópera prima "La historia del baúl rosado". Una película sincera y humana que refleja la madurez de la cineasta nacida en Santander.

En respuesta al interrogante sobre ¿Quien es El*la*?, la directora afirma que **"***Ella* es todas ellas, aunque en la película el hilo conductor y protagonista sea masculino, *Ella* es una historia donde vemos diferentes tipos de mujeres con diferentes tipos de historia**"**, **"**En general es un homenaje a las mujeres y su lucha diaria y a sus valores en cuanto a la solidaridad y a mantener la dignidad en ese espacio que es el barrio que ellos habitan**".**

[![11061249\_603446906456787\_8498047299527256430\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11061249_603446906456787_8498047299527256430_n.jpg){.aligncenter .size-full .wp-image-9658 width="960" height="720"}](https://archivo.contagioradio.com/ella-cuando-la-dignidad-supera-a-la-indolencia/11061249_603446906456787_8498047299527256430_n/)

*Ella *está inspirada en principio por la obra teatral "La historia de un anciano que quedó viudo", una historia ambientada en los días en que la peste bubónica en Europa cobraba tantas víctimas que no habían suficientes cajones para enterrarlas de manera digna; un contexto que para la directora **"**refleja un poco la realidad de nuestro país, donde hay tanto cadáver incepulto y tanta actitud de indolencia frente a la muerte y lo que le pasa al otro**"**.

Son esas realidades que se viven en lugares como Ciudad Bolívar, localidad en la que se llevó a cabo el rodaje de la cinta en junio de 2013, y en otras regiones y poblaciones de Colombia donde, de acuerdo con Libia Stella "nos hemos acostumbrado a que la muerte esta en el orden del día y nos hemos olvidado de la sensibilidad, del dolor del otro".

[![59399\_324378051030342\_1765123103\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/59399_324378051030342_1765123103_n-e1433359196578.jpg){.aligncenter .wp-image-9659 width="625" height="381"}](https://archivo.contagioradio.com/ella-cuando-la-dignidad-supera-a-la-indolencia/59399_324378051030342_1765123103_n/)

La producción resultó ganadora durante las convocatorias del Fondo para el Desarrollo Cinematográfico colombiano, asegurando los recursos para su financiación desde las primeras etapas de pre-producción y para su realización, una propuesta que para fortuna de sus realizadores desde el principio a contado con una **"**muy buena estrella**"**.

Una vez terminada la etapa de montaje, la cinta estuvo presente en varios festivales alrededor del mundo entre los que se cuentan el Festival de Cine de Montreal, el Festival de Mar del Plata, el Festival de cine Femenino en Dormunt, entre otros, y recientemente en el Festival de Cine Latinoamericano de Tigre, Argentina, donde obtuvo los galardones a mejor película Latinoamericana y mejor actor para Humberto Arango.

De las experiencias vividas durante la proyección de su película, la directora recuerda de manera emotiva lo ocurrido en Canadá, donde el público al finalizar la exhibición la aplaudió de pie, **"**eso es el mayor premio, más que la taquilla, más que otra cosa, es que el público logre sensibilidad y alcance a emocionarse con los personajes. Eso para mi es el mejor premio de todos**"**.

[![1071281\_326827004118780\_639849350\_o](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/1071281_326827004118780_639849350_o.jpg){.aligncenter .size-full .wp-image-9661 width="1204" height="714"}](https://archivo.contagioradio.com/ella-cuando-la-dignidad-supera-a-la-indolencia/1071281_326827004118780_639849350_o/)

*Ella* es una producción de "Dos de Dos", una empresa **"**basada en el trabajo y en la capacidad de conseguir nosotros mismos los recursos**",** en la que Gómez une sus fuerzas con el director y guionista Julio Contreras para quien la cinta representa su estreno como productor, en lo que al cine se refiere. *Ella* es solo uno de varios proyectos concebidos a cuatro manos,  que próximamente llegarán a las salas de cine.

#### <iframe src="https://www.youtube.com/embed/yrgcLQgP1IA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

#### Sinopsis:  
Alcides y Georgina son dos ancianos olvidados del mundo que viven en un inquilinato de Ciudad Bolívar, en los suburbios de Bogotá. La mujer vive su día a día en compañía de su vecina Guiselle, una hermosa niña de 12 años a quien su padre Facundo maltrata a diario. Tras una violenta discusión con Facundo, Georgina muere y como Alcides quiere procurarle un funeral digno, emprende una peregrinación por la ciudad, con su cadáver en una carreta, en busca de los recursos para poder enterrarla. Durante su recorrido, el anciano se encuentra con muchos obstáculos, especialmente la indiferencia de la mayoría de la gente. A través de su travesía, Alcides aprende a vivir sin Georgina y cambia su forma indolente de ver el mundo. Finalmente, cumple con el último deseo de su amada. 
