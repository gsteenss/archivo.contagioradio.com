Title: Residentes de barrio Normandia en Bogotá rechazan canchas sintéticas
Date: 2019-06-26 18:53
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: canchas sintéticas, Enrique Peñalosa, Parque de Normandia
Slug: residentes-de-barrio-normandia-en-bogota-rechazan-canchas-sinteticas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Captura-de-pantalla-133.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: [[@**bogotaampm**] 

Aproximadamente 40 residentes del barrio Normandia, de la ciudad de Bogotá, protestaron de manera pacífica la installación de canchas sintéticas en el parque central de esta vecindario, que inició este miércoles con el encerramiento de la zona de construcción.

Según Juan Carlos Figueroa, líder comunitario del barrio Normandia, contratistas acompañados por 100 policías y dos buses del Escuadrón Móvil Antidisturbios (ESMAD) llegaron al barrio a las 5 de la mañana para iniciar la primera fase de la construcción.

Sin embargo, el líder comunitario afirmó que los residentes del barrio rechazan a la installación de céspedes artificiales pues prefieren las canchas naturales que ya existen y advierten que el uso de fibras sintéticas para la construcción de los escenarios deportivos puede ocasionar daños ambientales y a la salud. (Le puede interesar: "[A pesar de protestas, Alcaldía adelanta obras en Humedal Tibabuyes](https://archivo.contagioradio.com/a-pesar-de-protestas-alcaldia-de-bogota-adelanta-obras-en-humedal-tibabuyes/)")

En particular, residentes se han manifestado en contra de los gránulos de caucho reciclado, hecho de componentes tóxicos, que se usan para los tapetes sintéticos. Estos gránulos han sido el centro del debate sobre lo seguro que resultan ser estos predios artificiales dado que se ha cuestionado el efecto en la salud que puede ocasionar la inhalación, el contacto o la ingestión de este material.

Finalmente, Figueroa aclaró que los vecinos no se oponen a que el Distrito de Bogotá arregle la zona."Queremos que nos arreglen los andenes, que nos arreglen los juegos de los niños y dos canchas de basket, pero no queremos que nos hagan una cancha sintética", afirmó.

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
