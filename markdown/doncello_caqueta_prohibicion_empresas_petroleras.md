Title: Doncello, Caquetá se prepara para prohibir actividad petrolera en su territorio
Date: 2017-06-30 15:55
Category: Ambiente, Voces de la Tierra
Tags: Caquetá, Petroleras
Slug: doncello_caqueta_prohibicion_empresas_petroleras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Manifestación-contra-las-petroleras-en-Doncello-Caquetá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [Selva.com.co]

###### [30 Jun 2017] 

**La alcaldía de Doncello, Caquetá, ya sancionó la decisión del concejo municipal para que en ese territorio sea prohibida las actividades mineras y petroleras**. Una propuesta que espera impulsar la economía del municipio a través de otros sectores como el ecoturismo, tal y como lo quisieran sus habitantes.

“A nivel normativo hay una jerarquía con base en acuerdos municipales. No se puede prohibir actividad petrolera porque ya hay unas licencias, se quiere regular el uso del suelo, para que los consejos den las normas correspondientes en defensa de los recursos naturales”, explica el concejal Carlos Mora.

Le siguiente paso será la aprobación por parte de la gobernación, y que el Tribunal Administrativo de Caquetá revise la decisión ya sancionada por la alcaldesa. “Se busca espacios de discusión que antes el gobierno no le daba a las comunidades”, dice Mora.

**Actualmente la comunidad también trabaja en el desarrollo de una consulta popular**. La Registraduría ya avaló las firmas, y se está a la espera del aval constitucional referente a la pregunta que se haría a los pobladores, esperando que antes de diciembre se realice la consulta. [(Le puede interesar: En Paujil habrá consulta popular)](https://archivo.contagioradio.com/en-paujil-caqueta-habra-consulta-popular/)

### Las petroleras 

Cabe recordar que desde inicios del 2016 las comunidades vienen denunciando que al departamento de Caquetá ya estaba llegando maquinaria para iniciar la perforación de pozos petroleros en los municipios de Paujil y Doncello, lo que **causaría graves efectos ambientales en la Amazonía colombiana**, **zona productora de agua.**

Para ese entonces, Mercedes Mejía, coordinadora de la Mesa Departamental por la Defensa del Agua y el Territorio, aseguraba que ya se había iniciado el ingreso de maquinaria de la **empresa Monterico, hacia  los municipios de El Doncello y El Paujil**. Además según reportes de habitantes del municipio de Puerto Rico, allí  también estaban llegando las máquinas. [(Le puede interesar: 43 bloques petroleros en Caquetá amenazan la Amazonía colombiana)](https://archivo.contagioradio.com/43-bloques-petroleros-en-caqueta-amenazan-la-amazonia-colombiana/)

El concejal Mora manifiesta que las comunidades no tienen ningún tipo de diálogo con las petroleras, sin embargo señala que estas han querido persuadir y comprar a la comunidad. Asimismo afirma que las empresas no cumplieron con la socialización sobre los proyectos petroleros. **De las 68 veredas del municipio, solo 10 estuvieron en reuniones donde se explicaba el tipo de actividad que se iba a realizar en el territorio.**

<iframe id="audio_19566308" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19566308_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
