Title: Los impuestos afectan más a las mujeres que a los hombres
Date: 2017-06-14 11:48
Category: Economía, Nacional
Tags: Impuestos, mujeres, sistema tributario
Slug: los-impuestos-afectan-mas-a-las-mujeres-que-a-los-hombres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/mujer-bolsillos-vacios-sin-dinero_0-e1497458576595.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Telemundo] 

###### [14 Jun 2017]

En el marco de la Cumbre Mundial sobre los Derechos de las Mujeres y Justicia Tributaria. se presentará una **demanda ante la Corte Constitucional para reducir del 5% al 0% el gravamen en los impuestos a las toallas higiénicas**. Según las organizadoras del evento "la carga impositiva en Colombia afecta más a las mujeres que a los hombre por la diferencia de salarios y los hábitos de consumo".

Para María Fernanda Valdés, coordinadora de la Friedrich-Ebert-Stiftung en Colombia FESCOL, “**los impuestos en el país, que en su mayoría son regresivos, afectan en mayor medida a las mujeres**, 1 de cada 3 no tienen ingresos propios”. Ante esta inequidad tributaria, las mujeres han establecido que existe una necesidad de hacer que los impuestos sean cada vez más progresivos para que las mujeres no se vean afectadas.

Según explicaron un impuesto es regresivo en la medida que capta un porcentaje menor del ingreso a medida que este aumenta. Por ejemplo, **el iva en Colombia es regresivo** y afecta a la población más pobre quienes pagan, a través del consumo el 60% de los impuestos fijados. (Le puede interesar: "[Demandan reforma tributaria por una menstruación con 0% impuestos](https://archivo.contagioradio.com/demandan-reforma-tributaria-por-una-menstruacion-con-0-impuestos/)")

Por el contrario, un impuesto sería calificado como progresivo cuando la tarifa de recaudo aumenta mientras los ingresos también aumentan. Por ejemplo, **el impuesto a la renta y a la propiedad son progresivos**.

Si bien en América Latina el 33% de las mujeres alcanzan a estar en el rango de quienes tienen un salario alto, **la brecha salarial con respecto de los hombres es del 58%**. Es por esto que las mujeres tienen más dificultades para acceder a sistemas de salud o educación de calidad. Según Valdés, “en la región latinoamericana se necesita que exista un recaudo por impuestos progresivos mayor y así se logra fomentar programas laborales, educativos y de salud para las mujeres”.

En temas tributarios, las mujeres de América Latina no están muy presentes por lo que no existen investigaciones de las brechas tributarias y su afectación en la población femenina. Sin embrago, según Valdés “**las mujeres dedicamos más tiempo en las labores del hogar** por lo que es posible que se haga un tratamiento especial tributarios a los productos del cuidado del hogar”. (Le puede interesar: ["Por una "menstruación libre de impuestos", no a la reforma tributaria"](https://archivo.contagioradio.com/por-una-menstruacion-libre-de-impuestos-no-a-la-reforma-tributaria/))

En la cumbre las mujeres de la región han podido establecer que las mujeres tienen problemas al momento de hacer una reforma tributaria progresiva. Ellas manifestaron que “los hombres son quienes deciden que impuestos se ponen y cuales se quitan, lo que pone en desventaja a las mujeres”. Según Valdés, “**las mujeres queremos hacer un movimiento global donde las mujeres puedan influir en el sistema tributario**”.

<iframe id="audio_19269587" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19269587_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
