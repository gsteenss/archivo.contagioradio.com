Title: Uribe a investigación por presunta manipulación de testigos contra Iván Cepeda
Date: 2018-02-18 14:50
Category: Nacional
Tags: falsos testigos, Iván Cepeda, paramilitares, uribe
Slug: uribe-testigos-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/uribe-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:El Heraldo 

###### 18 Feb 2018 

En decisión de la Corte Suprema de Justicia, con ponencia del magistrado José Luis Barceló, **se archivó la denuncia** presentada por el expresidente y senador Alvaro Uribe Vélez en la que acusa al senador Iván Cepeda de haber comprado en 2012 falsos testigos que lo relacionaran con grupos paramilitares, y **ordenó abrir investigación en contra del ex mandatario "por su presunta participación en la manipulación de testigos**"

La Corte asegura que las visitas realizadas por el Senador del Polo Democrático a la prisión a los ex integrantes de las Autodefensas Unidas de Colombia Pablo Hernán Sierra y Juan Guillermo Monsalve, no fueron ilegales porque se dieron en el marco de sus competencias como congresista y por que no medio ofrecimiento alguno por los testimonios recogidos en los que sindican a Álvaro y Santiago Uribe Vélez de patrocinar grupos paramilitares en Antioquia.

Adicionalmente, la Corte concluye que la indicación de investigar a Uribe por la manipulación de testigos, obedece a largas horas de grabación en las que aparece su voz relacionada con el tema. Conclusión que el ex mandatario ha calificado como una persecución por parte de la máxima instancia judicial frente a la cuál su abogado Jaime Granados interpondrá un recurso de reposición.

"Las llamadas mencionadas en procedencia, más las que se citarán enseguida, dejan al descubierto que Juan Guillermo Villegas Uribe, y otros, han intervenido testigos para involucrar al doctor Iván Cepeda en la conformación de un supuesto cartel de falsos testigos, cuando lo que parece ocurrir es lo contrario, un complot, como lo señaló el defensor del congresista, para desprestigiar su labor legislativa y, de paso, las entrevistas lícitamente recogidas en ejercicio de sus funciones”, señaló la Corte.

**Las evidencias**.

La decisión parte de la consideración de la Corte en el caso de Cepeda, parte del argumento que indica que las declaraciones de Sierra "no hacen parte de un plan criminal gestado entre él y el doctor Cepeda" descartando que los testimonios sean parte de "manipulaciones basadas en ofrecimientos hechos por el Congresista", mientras que en relación con Monsalve aseguró que no hubo manipulación por parte del Senador.

En cuanto a las grabaciones donde aparece Uribe Vélez, la Corte asegura que las interceptaciones en que se obtuvieron no fueron a números del ex presidente sino de sus colaboradores, citando el caso de Juan Guillermo Vargas Uribe, hombre de confianza señalado como supuesto fundador del Bloque metro de las AUC, organización que presuntamente se gestó en reuniones realizadas en la hacienda Guacharacas, de los Uribe, según testimonio aportado por Monsalve, quien fue empleado del lugar

En  conversación registrada el 22 de diciembre de 2015, Uribe advertía a su subalterno que conocía de la interceptación de llamadas de las que habían sido objeto el  21 de octubre de ese mismo año (un día después que el Procurador Ordoñez abriera la investigación en contra de Cepada), cuando se reunieron junto a Humberto Gómez Garro en una frutería de la calle 70 de Medellín, que la fiscalía los venía siguiendo y que incluso en ese momento la conversación la estaban "escuchando esos hijueputas"

En la misma charla Uribe advierte a Vargas que al día siguiente armaría  un "escándalo en twitter" sobre el tema, que efectivamente realizó denunciando que estaba siendo "interceptaciones y seguimientos de la justicia". El conocimiento que sobre la intervención de los números de sus interlocutores tenía el ex mandatario, es otra de las variables que la Corte ordenó investigar, por lo que sería una posible filtración de información reservada desde el CTI de la Fiscalía.

La Corte cuestiona además que en el proceso algunas llamadas que eran claves en la investigación, no fueron grabadas por supuestos problemas técnicos, como algunas llamadas del Ganadero Villegas Uribe al padre de Monsalve después de terminar una declaración en la Corte Suprema. Según la corte Villegas controlaba los movimientos de la familia Monsalve y "los insta a declarar en el momento y los términos" establecidos por el ganadero.

**Paramilitares dispuestos a colaborarle a Uribe contra Cepeda.**

A partir de las grabaciones de las llamadas sostenidas por Villegas y Gómez Garro el 21 y 22 de octubre, labores de verificación y otras donde se registraban ingresos a la cárcel de Itagu, lugar donde el 12 de septiembre y el 14 de noviembre de 2015 Gómez habría conversado con Gabriel Muñoz Ramírez, alias Castañeda, uno de los 'paras' quien contactó a su vea a Ramiro de Jesús Henao, alias Simón quien también estaba dispuesto a colaborar en incriminar "falsamente al doctor Iván Cepeda de hacerles ofrecimientos a cambio de declaraciones".

En sus testimonios los dos paramilitares aseguraron que Cepeda los visitó desde 2012 proponiendoles que declararan que los Uribe Vélez estaban relacionados con un asesinato cometidos por su organización, declaraciones que tras su evaluación  la Corte consideró  que  "se pretendió distorsionar la verdad de lo ocurrido" y llamó la atención porque sus versiones aparecieron dos semanas antes del debate de control político que Cepeda le hizo a Uribe en septiembre del 2014.

Adicionalmente el Tribunal apunta a que ambos testimonios son "sustancialmente idénticos" y  que "recogen todas las circunstancias que recogen las entrevistas de Pablo Hernán García Sierra y Juan Monsalve Pineda", ordenando adicionalmente que los testigos sean protegidos por el Inpec ante posibles intentos por asesinarlos en prisión.

**El alcalde de Amagá y el caso de El aro**

Wilser Darío Molina, quien fue abogado de Luis Arnulfo Tuberquia "alias Memin" un jefe paramilitar, aparece en las llamadas que realizó Alvaro Uribe, según su declaración para solicitarle que, aprovechando su labor en la cárcel, documentara las denuncias de los detenidos en el supuesto montaje que Cepeda estaba realizando en su contra, para lo cual contrató un investigador privado.

En 2016 Molina llegó a ser alcalde de Amagá Antioquia por el Centro Democrático, y apesar que manifestó que se desentendió del tema de los testigos en 2014, la Corte asegura que "inexplicablemente siguió sosteniendo conversaciones con Álvaro Uribe, Santiago Uribe y Juan Guillermo Villegas" y que las grabaciones existentes, entre los hoy senador y alcalde, fueron para tratar el tema de los testigos que señalaban al senador y "la urgencia de buscar mecanismos para neutralizarlos, lo cual evidencia un grado de cercanía y confianza superior a lo normal".

En los registros de las conversaciones Uribe menciona estar preocupado por que la Agencia Ancol, lo mencionaba como autor de un montaje contra Cepeda y que el ex presidente le pagaba a Molina con el aval del partido advirtiéndole que "de todas maneras tenga mucho cuidado, doctor Wilser, porque esta izquierda va a estar pendiente de perseguirlo a usted". El abogado y alcalde buscaba información sobre testigos que señalaron a Uribe en otros procesos, como la masacre de El Aro, según evidencian las grabaciones. (Le puede interesar: [Compulsan copias contra Álvaro Uribe por dos masacres en Antioquia](https://archivo.contagioradio.com/alvaro_uribe_investigacion_masacres_antioquia_paramilitarismo/))

En noviembre de 2015 Uribe llamó a Medina a preguntarle por un nuevo testigo en su contra y le sugiere enviar investigadores que hablen con el nuevo testigo. Al día siguiente, el abogado llama a Santiago Uribe y asegura que coordinó con un periodista de Medellín para que publicara una noticia que desmintiera las versiones del testigo con la de otros paramilitares involucrados en los hechos.

Dentro de las decisiones proferidas por la Corte, esta la solicitud de un informe por parte de uno de sus magistrados que responda a por qué una investigación contra Álvaro Uribe sobre sus supuestos nexos con las masacres 'paramilitares' en su periodo como gobernador no han avanzado, caso que, según fuentes de la Corte, lo tenía el cuestionado magistrado Gustavo Malo Fernández.

[Jose l. Barcelo c. Sp245-2018](https://www.scribd.com/document/371797566/Jose-l-Barcelo-c-Sp245-2018#from_embed "View Jose l. Barcelo c. Sp245-2018 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_42308" class="scribd_iframe_embed" title="Jose l. Barcelo c. Sp245-2018" src="https://www.scribd.com/embeds/371797566/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-CHnjj8Q4wzDJ7VIghwwB&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="1.645985401459854"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
