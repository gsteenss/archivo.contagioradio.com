Title: La muerte se apodera de Puerto Guzmán, 8 asesinatos en dos semanas
Date: 2020-01-26 11:34
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Asesinatos, lideres sociales, Puerto Guzmán
Slug: la-muerte-se-apodera-de-puerto-guzman-8-asesinatos-en-dos-semanas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Asesinato-lider-indigena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Archivo

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la Red de Derechos Humanos del municipio, al rededor de las 7:50 pm de este 25 de enero, fueron asesinadas dos personas en la cabecera inspeccional de José María, municipio Puerto Guzmán, Putumayo. (Le puede interesar:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según los defensores una de las víctimas fue identificada como Modesto Vega de 45 años, quien hacía parte de la Junta de Acción Comunal y era propietario de un predio ubicado en la Vereda Bajo Caño Avena, ubicado en el mismo municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hasta el momento la Red no ha podido identificar el nombre de la otra persona asesinada, ni la razón o identidad de los responsables de este hecho. (Le puede interesar: <https://archivo.contagioradio.com/asesinato-lider-yordan-tovar-puerto-guzman-putumayo/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con estos dos nuevo casos, ya son 8 los asesinatos que se registran en lo corrido del año en el municipio de Puerto Guzmán, muertes de las cuales 4 corresponden s líderes sociales. (Le puede interesar: <https://www.justiciaypazcolombia.com/asesinatos-y-desplazamientos-forzados-en-santa-lucia-puerto-guzman/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo…

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
