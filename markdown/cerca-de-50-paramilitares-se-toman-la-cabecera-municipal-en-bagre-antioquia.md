Title: Cerca de 50 paramilitares se toman la cabecera municipal en Bagre, Antioquia
Date: 2017-07-01 16:09
Category: DDHH, Nacional
Tags: Antioquia, Derechos Humanos, paramilitares
Slug: cerca-de-50-paramilitares-se-toman-la-cabecera-municipal-en-bagre-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Notimundo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Notimundo] 

###### [01 Jul. 2017] 

Según un comunicado de la Asociación de Hermandades Agroecológicas y Mineras de Guamocó – AHERAMIGUA éste sábado cerca de 50 paramilitares autodenominados Autodefensas Gaitanistas de Colombia se tomaron la cabecera municipal del corregimiento Puerto López en el municipio del Bagre, departamento de Antioquia “**los paramilitares portan armas largas, de combate, algunos de ellos están uniformados y con brazaletes de identificación, y otros se encuentran de civil”.**

Jairo Rodríguez y Gloria Granados quienes recientemente han recibido amenazas de muerte consideraron **esta toma paramilitar como un riesgo para sus vidas,** por lo cual decidieron huir con rumbo hacia las montañas.

“Huyeron a través del monte y se encuentran desplazados en éste momento, escondidos de los criminales y salvaguardando sus vidas” asevera el comunicado. Le puede interesar: [Con cruces negras en las casas amenazan a líderes en Puerto López](https://archivo.contagioradio.com/pintan-cruces-en-las-casas-de-lideres/)

Manifiesta AREMIGUA que esta situación no es nueva pues **desde el 9 de enero del 2016 vienen denunciando hechos graves que acontecen en el municipio** por cuenta de la presencia de paramilitares como desapariciones forzadas, amenazas vía mensaje de texto y asesinatos de líderes de la región.

Pero este no es el único hecho el más reciente se presentó a finales del mes de marzo del presente año, cuando **“todo el municipio del Bagre amaneció con panfletos que amenazaban con hacer “limpieza social” y “toque de queda”. **Le puede interesar: [Comunidades denuncian 17 asesinatos a manos de paramilitares en El Bagre, Antioquia](https://archivo.contagioradio.com/comunidades-denuncian-17-asesinatos-a-manos-de-paramilitares-en-el-bagre-antioquia/)

Dada la grave situación las comunidades están exigiendo a las instituciones, al gobierno departamental y nacional **garantice la seguridad de la comunidad y de los líderes sociales desmantelando a los grupos paramilitares** que actúan en la región y la implementación de las medidas de protección individuales y colectivas que en reiteradas ocasiones han sido solicitadas por AHERAMIGUA y pese a ser aprobadas no han sido aplicadas.

E hicieron un llamado a los organismos internacionales, a la comunidad Nacional e Internacional y a las organizaciones defensoras de Derechos, intervenir con carácter urgente, exigir soluciones inmediatas al Estado colombiano y acompañar a estas regiones. Le puede interesar: [Paramilitares amenazan a víctimas colombianas exiliadas en España](https://archivo.contagioradio.com/amenazas-de-paramilitares-llegan-a-las-victimas-colombianas-exiliadas-en-espana/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
