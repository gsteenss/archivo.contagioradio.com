Title: Entre la teocracia y el simulado liberalismo
Date: 2017-05-14 18:10
Category: Camilo, Opinion
Slug: entre-la-teocracia-y-el-simulado-liberalismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/no1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: actualidad radio 

###### Por[Camilo de Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)

###### [14 May 2017]

La apatía y la indiferencia urbana por la transición de 7000 hombres y mujeres de las FARC EP a la vida civil es una demostración de la distancia del conflicto armado y social de los centros urbanos de decisión. Esto, muy a pesar del crecimiento de una imagen favorable de las FARC, según encuestas de opinión. Mucho más lejano, con incertidumbre y escepticismos ronda la mesa de conversaciones de Quito con el ELN, una guerrilla que sigue desconociendo el mundo de la video política, las nuevas gramáticas juveniles de expresión, entre otros asuntos comunicativos

El abismo de la guerra armada rural como alzamiento armado reivindicando los derechos de los excluidos rurales es muy distante del país nacional, del que habla Gaitán. Nadie se asombra, ni alega, ni pelea o no logra impactar a ese país nacional por los dos guerrilleros asesinados de las FARC EP o por la extensión del odio con el asesinato de ocho familiares de integrantes de la guerrilla en transición.

Importa la vida de unos, los derechos de unos, y los modelos de sociedad y de Estado de unos y para ellos mismos es nuestra cultura, y nuestra cultura política. Los mensajes repetitivos e infantilizados en amplios sectores de los medios de información  que reproducen las versiones oficiales, civiles y militares, sobre sus enemigos internos, desde hace 70 años han construido una cultura política acrìtica, maniquea, intolerante, excluyente, que banaliza la vida.

Basta escuchar los discursos de Guillermo León Valencia sobre la operación armada en RioChiquito y el tratamiento de los medios en su época frente a los guerrilleros. O la cruzada contra un intento de reforma Agraria convertido por obra y gracia de Álvaro Gómez en la llamadas “Repúblicas Independientes”, impidiendo resolver la causa de un conflicto que derivó en más de 220 mil muertos hasta hoy. Años después la misma expresión es parte de la urdimbre retórica de quien también se llama Álvaro, para referirse a las Zonas de Reserva Campesina.  Hoy ya en el mundo de la multimedia política se profundiza esa cultura de impacto, sin profundidad, de imagen y de breves palabras para hacer creíble la mentira, así lo vimos hace menos de ocho días, y en el plebiscito.

Si algo queda de ese proceso histórico ha sido una sociedad con una cultura política veterotestamentaria. La  expresión del rancio sector empresarial de la tierra, ganadero, palmero, ganadero, extractivista, de cristianos fundamentalistas, entre ellos además de sectores de iglesias carismáticas, de los católicos, que se niegan a aceptar la importancia del pluralismo y la inclusión, y el respeto mutuo que permitiría coexistir, se expresó en la convención del Centro Democrático. Saben hacerlo de tal manera, que se convierten en información de todos los medios, que operan en resonancia para cuestionar la concesión básica de una Reforma Agraria Básica para los excluidos en el campo o la modernización del Estado y el reconocimiento de los derechos de muchos ciudadanos como un Estado laico.

El machismo religioso cuenta con un gran mentor, un expresidente de ocho años, que cautivó, que engañó, que mintió, y  tantos horrores más, y hoy es elevado en motivación de las virtudes cristinas, de tres prohombres católicos Paloma Valencia, Fernando Londoño Hoyos, y de Monseñor Ordóñez, como una nueva divinidad. Hipocresía, maniqueísmo, integracionismo, absolutismo para hacer trizas no solo la firma de un Acuerdo, si no para imponer un modelo de familia y de sociedad, y guardar silencio de la criminalidad de Estado

Ese almizcle cultural de la catolicidad española y visiones cristianas maniqueas,  de políticas excluyentes e intolerantes, que legitimó el sectarismo político, que condenó la ideas liberales, en el que se vive de un modo en el día y de otro en las noches; en  que reprimen sus deseos para ajustarlos a unos designios divinos basados en el terror deshumanizante, explica las voces en el senado en cabeza de Vivian Morales en la promoción del referendo para negar el derecho a la adopción más amplia posible.

La oposición a la adopción de niños por solteras y solteros, por parejas del mismo sexo expresa esas mentalidades y prácticas ambientales que imposibilitan la sociedad moderna que habilite un camino hacia la paz en una nueva democracia, de respeto a la diferencia y no de imposición e intolerancia, los problemas de los guerrilleros son de ellos, no son nuestros, asì seamos de este mismo país.

El debate en la cámara de representantes con argumentos religiosos, respetables, según la creencia, y la instalación de la deidad del expresidente en el Centro Democrático, se alientan en el fondo en un orden teocrático frente a una constitución en 1991, protectora de los los derechos de los humanos y sus almas.

Sin embargo, unos y otros de ese establecimiento, y de aquellos que ya son parte de él, que se asumen como 16 millones de cristianos en Colombia coinciden en los propósitos estratégicos de la Pax Neoliberal: la pacificación para la liberalización de la tierra con pequeñas economías rurales, haciendo aún más famélicas e insustanciales las consultas previas y tratando de desmoronar las consultas populares. Salvemos las almas, que del cuerpo se encargan otros

Las diferencias no son contradicciones, se expresan en el modo de hacer la Pax Neoliberal , unos dentro de un tipo de teocracia y otros en una democracia simuladamente liberal. Algunos de ellos, son incapaces de seguir sosteniendo que existe  un único modo de familia, y que la guerra militar es el camino,  en lo demás todos son muy parecidos. Esa es la cultura política mayoritaria en el país nacional, el que el movimiento social y lo llamado alternativo, está muy distante de asumir y reconocer para transformar.
