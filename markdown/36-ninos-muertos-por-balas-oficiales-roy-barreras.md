Title: “36 niños muertos por balas oficiales”: Roy Barreras
Date: 2020-10-28 08:07
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Bombardeo a menores, Carlos Holmes Trujillo, Debate de Control Político, Moción de Censura, Roy Barreras
Slug: 36-ninos-muertos-por-balas-oficiales-roy-barreras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Ninos-muertos-en-operaciones-militares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En medio del debate de control político en contra del Ministro de Defensa, Carlos Holmes Trujillo, el senador citante Roy Barreras, denunció que **no serían 8, sino 36, los niños y niñas entre 12 y 18 años, que resultaron muertos en operativos militares por cuenta de “*balas oficiales*”.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador Barreras cuestionó fuertemente la gestión de Carlos Holmes al frente de la cartera de Defensa calificándola como un “*fracaso*”; al tiempo que criticó el hecho de que después de tanta espera, el debate se hubiese tenido que realizar de manera virtual, por cuenta de la decisión de la mesa directiva del Senado de decretar cuarentena, luego que se conociera que la senadora del Centro Democrático, María del Rosario Guerra, contrajo Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otra de las graves denuncias realizadas por el citante consistió en señalar que **54 niñas habían sido violadas con implicación de miembros de la Fuerza Pública,** aparte en el cual recordó, el caso de la menor Embera abusada sexualmente por varios soldados; a la vez que reprochó al ministro que no hubiese informado sobre esos casos al país. (Le puede interesar: [Agresión sexual del Ejército contra menor embera es un ataque a los 115 pueblos indígenas del país: ONIC](https://archivo.contagioradio.com/agresion-sexual-del-ejercito-contra-menor-embera-es-un-ataque-a-los-115-pueblos-indigenas-del-pais-onic/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, Barreras criticó la política del Gobierno que instrumentalizaba a los miembros de las Fuerzas Militares y señaló que las tropas estaban desmoralizadas pues para ascender al interior de estas instituciones, había que “*politiquear*”, por encima de tener un rendimiento militar destacado. Frente a esto denunció **la indebida injerencia de Andrés Pastrana, Martha Lucia Ramírez, Luigi Echeverry y hasta del exministro de Defensa Guillermo Botero en nombramientos y ascensos militares.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el senador citante, esto ha generado el fortalecimiento de los grupos armados al margen de la ley, que según denunció han sumado más personas fortaleciendo sus filas, como el caso del ELN, que según Barreras cuando Duque llegó al poder contaba con 1.500 combatientes y al día de hoy tenía cerca de 4.500.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, agregó que **el Gobierno Duque, era responsable de la tasa de producción más grande de cocaína de la historia (1,5%)** porque sus cifras de erradicación eran “*falsos positivos*”. Barreras denunció que la producción actual de cocaína en el país implicaba que cerca de 50 mil toneladas de insumos y 129 millones de galones de gasolina transitaban por las vías colombianas para llegar a los laboratorios de fabricación del alcaloide “*en las narices del Ejercito*”, bajo la dirección de Holmes, sin que estos fuesen decomisados o neutralizados, por lo que habló de omisión y connivencia de la Fuerza Pública. (Lea también: [Hay políticas que propician la criminalidad en las Fuerzas Militares: CCEEU](https://archivo.contagioradio.com/hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RoyBarreras/status/1319605627686146049","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RoyBarreras/status/1319605627686146049

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Barreras cerró señalando que iba a documentar a través de 14 videos cada una de sus denuncias**, publicando uno por cada día de la cuarentena impuesta por la mesa directiva del Congreso que supuso el regreso a la virtualidad de las sesiones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No serían 8 sino 36, los niños muertos en operaciones militares

<!-- /wp:heading -->

<!-- wp:paragraph -->

El ministro de Defensa también fue cuestionado por haber ascendido como comandante de las Fuerzas Militares, al general Eduardo Enrique Zapateiro, quien lideró la “Operación Atai” en la que se bombardeó de manera deliberada a niños reclutados forzosamente, para efectuar un golpe militar en contra de alias “Gildardo Cucho”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe recordar que una investigación realizada por Cuestión Pública y [Dejusticia](https://www.dejusticia.org/), reveló que el bombardeo efectuado en San Vicente del Caguán, Caquetá, por el Ejército Nacional el pasado 29 de agosto, el cual acabó con la vida de al menos 8 menores de edad, **fue ejecutado, pese a que, los altos mandos militares, el entonces Ministro de Defensa, Guillermo Botero, y el presidente Iván Duque, tenían el pleno conocimiento de que en el lugar había presencia de niños.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe de inteligencia No. 937 del 24 de agosto de 2019, emitido cinco días antes de la denominada Operación Atai, reveló que **el Ejército tenía conocimiento de que el esquema de seguridad de alias “Gildardo Cucho” **—**objetivo principal del operativo**—** estaba conformado por menores de edad, los cuales habían sido reclutados de manera forzada. **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Roy Barreras sostiene que en este caso hay dos menores más que se encuentran desaparecidos, por lo cual los muertos podrían ser 10 y no 8; y en todo caso con su última denuncia, anticipó que la cifra fatal asciende a 36 niños muertos en medio de operaciones militares. (Lea también: [Presidente Duque y altos mandos militares sabían de los menores y autorizaron bombardeo](https://archivo.contagioradio.com/presidente-duque-y-altos-mandos-militares-sabian-de-los-menores-y-autorizaron-bombardeo/)**)**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
