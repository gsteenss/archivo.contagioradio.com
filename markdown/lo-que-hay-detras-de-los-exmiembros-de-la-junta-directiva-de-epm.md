Title: Lo que hay detrás de los exmiembros de la Junta Directiva de EPM
Date: 2020-08-13 18:26
Author: AdminContagio
Category: yoreporto
Tags: Alcalde Daniel Quintero, EPM, Hidroituango, Junta Directiva de EPM
Slug: lo-que-hay-detras-de-los-exmiembros-de-la-junta-directiva-de-epm
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Junta-Directiva-de-EPM.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Junta Directiva de EPM

<!-- /wp:paragraph -->

<!-- wp:block {"ref":88544} /-->

<!-- wp:paragraph -->

Durante muchos años, Empresas Públicas de Medellín -[EPM](https://www.epm.com.co/site/)- ha sido la empresa insignia de Antioquia, pero en el último tiempo, también **ha sido foco de polémica en especial por su proyecto Hidroituango, acelerado de manera irregular y apenas superando una crisis técnica que puso en riesgo la continuidad y el éxito del proyecto**, todo esto, generó que el alcalde de Medellín Daniel Quintero tomara acciones jurídicas lo cual según el mandatario era un mandato de ley. (Le puede interesar: [Ríos Vivos denuncia intimidaciones por parte de EPM y autoridades de Ituango](https://archivo.contagioradio.com/rios-vivos-denuncia-ituango/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el marco de estas acciones se decidió demandar al consorcio que construye Hidroituango, y con este a contratistas, aseguradores y proveedores, por el daño que la crisis dejó para la región, la empresa y el proyecto, lo cual, desembocó en la renuncia de la junta directiva de EPM, cuyos miembros afirmaron en su carta de renuncia que no fueron consultados y que la decisión de la demanda no fue debatida en las reuniones del máximo órgano de dirección de la empresa. **También que no fueron tenidos en cuenta en la decisión de solicitar el cambio de objeto social ante el Concejo de la ciudad pese a que ambas decisiones están en el espectro de funciones y la autonomía del gerente.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según versiones, más allá de una discusión jurídica o de políticas de buen gobierno lo que se hace evidente es la alta influencia y la diversidad de intereses económicos que tienen los exmiembros de la junta que los ha tenido por años al frente de EPM. Algunos van más allá, señalando que ahora, lejos del «*silencio cómplice de anteriores gobernantes de la ciudad*», es mucho más claro que el interés del grupo empresarial más importante de Medellín y uno de los más poderosos de Colombia **—el Grupo Empresarial Antioqueño -GEA-** **—** **  tenía «*secuestrada*» a la empresa insignia de los antioqueños.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, los perfiles de los exmiembros de la junta directiva de EPM dan cuenta de intereses que irían más allá de la empresa y de salvaguardar los recursos públicos; sino que estarían destinados a proteger los fines del Grupo Empresarial Antioqueño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Manuel Santiago Mejía Correa:** Ingresó a la junta el 30 de abril de 2012, es considerado en el mundo de los negocios el patrón del GEA; hijo del fundador del entonces sindicato Antioqueño Santiago Mejía Olarte, quien se desempeñó como secretario de hacienda de Medellín en 1980 y 1982, cuando los alcaldes se escogían por decreto, en ese periodo hizo una amistad con Álvaro Uribe Vélez que lo llevó a ser aportante en las campañas de Andrés Pastrana, Marta Lucia Ramírez, Oscar Iván Zuluaga y también de Uribe Vélez. En las últimas elecciones regionales donó 15 millones de pesos a la campaña del candidato del Centro Democrático Alfredo Ramos Maya.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Andrés Bernal Correa:** Fue miembro de la junta directiva desde el 6 de febrero de 2012, hace parte de múltiples juntas directivas de las que se dice ha sacado provecho. Es vicepresidente financiero de inversiones Suramericana y además también hace parte de la junta directiva de Argos, ambas empresas del Grupo Empresarial Antioqueño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Javier Genaro Gutiérrez:** Ingresó a la junta el 9 de marzo de 2016, se ha ganado una fama de «privatizador». Hace parte de la junta de directiva de ProAntioquia; fue gerente de ISA desde 1992 hasta el año 2007 y durante su gestión se dio la privatización de gran parte de esta empresa. Después siendo gerente de Ecopetrol desde el año 2007, sacó a la venta acciones de la empresa. Ya siendo miembro de la junta de EPM ideó lo que denominó “desinversión” que es considerada como una venta «disimulada» del 10% de participación que la empresa de servicios públicos tenía en ISA.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Elena Rico Villegas:** Ingresó a la junta de EPM el 21 de enero de 2016, fue gerente de HMV y directora de licenciamiento en Colombia Móvil, donde se gestó  la polémica venta de UNE a Millicom. Fue hasta 2016 la gerente de la Feria Internacional del Sector Eléctrico FISE que se realiza en Medellín; ha generado suspicacia que revisando la contratación de EPM el Centro de Innovación y Desarrollo Tecnológico del Sector Eléctrico CIDET ha tenido contrataciones por encima de los 6.000 millones de pesos en los años en que Rico Villegas ha sido miembro de la junta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Gabriel Ricardo Maya:** Fue miembro de la junta desde el 17 de Agosto de 2006, reconocido como un defensor a ultranza de la explotación petrolera, por lo que es directivo de la Federación Nacional de Combustibles creada para representar los intereses de los empresarios del sector petrolero; su ingreso a EPM se dio por decisión de Sergio Fajardo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La decisión del alcalde Quintero propició la salida de estos exfuncionarios cuyos intereses económicos se hacían evidentes, pero plantea ahora el reto para que los nombramientos de los nuevos miembros de la junta contribuyan a recuperar las finanzas de EPM para que represente los intereses de los ciudadanos que son quienes la sostienen con el pago de los servicios públicos.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
