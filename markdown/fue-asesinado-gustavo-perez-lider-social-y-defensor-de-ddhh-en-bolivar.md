Title: Fue asesinado Gustavo Pérez, líder social y defensor de DDHH en Bolívar
Date: 2019-09-15 11:36
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: asesinato de líderes sociales, bolivar
Slug: fue-asesinado-gustavo-perez-lider-social-y-defensor-de-ddhh-en-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Gustavo-Pérez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Equipo Jurídico Pueblos] 

Según la información brindada por el Equipo Jurídico Pueblos, el campesino, defensor de derechos humanos y líder social, Gustavo Pérez Arévalo de 58 años fue asesinado el pasado viernes 13 de septiembre en horas de la mañana mientras se encontraba  en la vereda Palma Chica del corregimiento Los Canelos en Santa Rosa del Sur,  Bolívar.

El dirigente social hacía parte de la asociación Santa Rita y la Corporación Sembrando Futuro de Paz, **además  fue delegado de la mesa local de víctimas de La Candelaria. ** [(](https://archivo.contagioradio.com/lideresa-social-mayerlis-angarita-sobrevive-a-atentado-en-barranquilla/)Lea también: Lideresa social Mayerlis Angarita sobrevive a atentado en Barranquilla)

Su cuerpo fue trasladado a la ciudad de Bucaramanga, Santander para la realización de la necropsia y así esclarecer las causas de su muerte. La familia exige claridad de los hechos.

Con anterioridad, Gustavo Pérez ya había solicitado protección por parte del Estado colombiano debido a amenazas por parte de grupos paramilitares, sin embargo su petición no fue atendida.[(Le puede interesar: Fiscalía se niega a recibir denuncias por violaciones de DDHH en Sur de Bolívar)](https://archivo.contagioradio.com/fiscalia-se-niega-a-recibir-denuncias-de-habitantes-de-casa-de-barro-en-el-sur-de-bolivar/)

Noticia en desarrollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
