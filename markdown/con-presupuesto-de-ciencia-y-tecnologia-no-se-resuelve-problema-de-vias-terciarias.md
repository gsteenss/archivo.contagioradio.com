Title: Con presupuesto de Ciencia y Tecnología no se resuelve problema de vías terciarias
Date: 2017-02-23 23:49
Category: Educación, Nacional
Slug: con-presupuesto-de-ciencia-y-tecnologia-no-se-resuelve-problema-de-vias-terciarias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ciencia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Universia] 

###### [23 Feb 2017] 

La comunidad académica y el Consejo Académico de la Universidad Nacional de Colombia, rechazaron el anuncio del presidente Juan Manuel Santos, quien durante la Cumbre de Gobernadores en Calí, **propuso descongelar recursos de las regalías de ciencia y tecnología, por cerca de billón y medio de pesos** para destinarlas a la construcción de vías terciarias.

Amparados en los artículos 360 y 361 de la Constitución Política, manifestaron que el 10% del Sistema Nacional de Regalías ha sido otorgado para incrementar la capacidad científica, tecnológica y de innovación en las regiones, por lo cual no debería disponerse para otros fines, que podrían ser financiados desde otras carteras, **para no afectar los “proyectos para producción, uso, integración y apropiación del conocimiento”** para distintos ámbitos de las comunidades, en lo económico, educativo, político, social y cultural.

Catalina Ramírez, secretaria del Consejo Académico, señaló que no es apropiado ni deseable **tratar de solucionar los problemas de infraestructura vial del país “con los recursos destinados a resolver necesidades apremiantes en el contexto de paz**, equidad y educación”.

### ¿Realmente se resolvería el problema? 

Algunos expertos como Moisés Wasserman, quien fue rector de la Universidad Nacional, reveló que el presupuesto “equivaldría al triple del presupuesto actual de Colciencias” y ello sería invertido en 2.500 km de vías tercias, es decir, unos 50 km por municipio priorizado. También resaltan que el Gobierno Nacional a la hora de implementar los acuerdos de La Habana, se ha mostrado negligente, y nuevamente en su obligación de diseñar y ejecutar un **Plan Nacional de Vías Terciarias, que no debería obstaculizar ni estar en contravía del Plan Nacional de Educación Rural**, promueve propuestas que “tanto en su fondo como en su forma, son una vergüenza para el Gobierno”, puntualizó Wasserman.

En los Acuerdos de La Habana, el punto 1 sobre la **Reforma Rural Integral, incluye el numeral 1.3.2.2, el cual habla del Plan Especial de Educación Rural, el cual el Gobierno Nacional se comprometió a desarrollar**, otorgando entre tanto, 1.6 billones de pesos durante cinco meses, así lo anunció el Viceministro de Educación Víctor Saavedra, en octubre del año pasado.

Po otra parte, de acuerdo con el CONPES 3857 de 2016, existen en el país 142.284 kilómetros de redes viales terciarias en áreas rurales, en contraste, el “ajuste que propone el gobierno” y que quitaría un monto importante al presupuesto para la investigación en Colombia, sólo subsanaría el 1,75% del total de km que se requieren para la red vial terciaria, porcentaje que algunos advierten **“en nada resuelve la precaria situación de este tipo de infraestructura, fundamental para el desarrollo de la Reforma Rural Integral”**, para comunicar áreas rurales productoras de alimentos, materias primas, manufacturas y que ayudaría a la implementación de los planes de sustitución de cultivos y fomentaría las economías campesinas.

En esa misma línea, Enrique Forero, presidente de la Academia Colombiana de Ciencias Exactas, Físicas y Naturales, aseguró que se trata de una **“medida improvisada del Gobierno” que afectará gravemente el desarrollo investigativo del país**, resaltó además que “antes de llevar a cabo esta reasignación de presupuestos”, el Gobierno debe realizar un inventario de vías terciarias y del estado de las mismas, puesto que hasta el momento lo dispuesto en la ley 1228 de 2008, sobre la materia, no es una realidad.

Por último, académicos y analistas, instaron al presidente Juan Manuel Santos, a reconsiderar su anunció y a que contemple la importancia del fortalecimiento y promoción de la **investigación para el sector agropecuario además de la ampliación de la oferta en educación superior en modalidades técnicas, tecnológicas y universitarias,** para habitantes de las zonas rurales del país, y de cumplimiento y efectividad a lo que se pactó en La Habana.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
