Title: Asesinan a José Oliver Maya, niño indígena Awá en operativos de erradicación forzada
Date: 2020-07-21 15:20
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Asesinato de indígenas, indígenas Awá, José Oliver Maya
Slug: asesinan-a-jose-oliver-maya-nino-indigena-awa-en-operativos-de-erradicacion-forzada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/José-Oliver-Maya-asesinado-por-la-Fuerza-Pública.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: José Oliver Maya

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este lunes 20 de julio **fue asesinado el menor indígena José Oliver Maya Goyes de 15 años y herido el campesino Silvio López en medio de operativos de erradicación forzada adelantados por la Policía Nacional** en el corregimiento La Castellana, municipio de Villagarzón, Putumayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

José, **era un menor perteneciente al pueblo indígena Awá y según denuncias de la comunidad habría sido impactado por un disparo de arma de fuego accionada por la Policía Nacional Antinarcóticos**, impactándolo a la altura del pecho. (Le puede interesar: [Hay políticas que propician la criminalidad en las Fuerzas Militares: CCEEU](https://archivo.contagioradio.com/hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho fue repudiado por organizaciones sociales e indígenas y por sectores políticos presentes en el Congreso de la República como por ejemplo la Representante a la Cámara [Ángela María Robledo](https://twitter.com/angelamrobledo).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/angelamrobledo/status/1285635660037025792","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/angelamrobledo/status/1285635660037025792

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Este es el segundo **asesinato en el mes de julio que se produce a manos de la Fuerza Pública** **en operativos de erradicación forzada de cultivos de coca.** El pasado viernes 3 de julio también fue asesinado el campesino Educardo Alemeza en el corredor Puerto Vega Teteyé, municipio de Puerto Asís, Putumayo en el marco de estos operativos. (Le puede interesar: [En operativos de erradicación en Putumayo deja dos campesinos muertos y tres heridos](https://archivo.contagioradio.com/en-operativos-de-erradicacion-en-putumayo-deja-dos-campesinos-muertos-y-tres-heridos/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### José Oliver Maya una víctima más de la violencia por parte de la Fuerza Pública

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Las comunidades han denunciado el uso excesivo de la fuerza con el que opera la Unidad Antinarcóticos** ante los reclamos por el incumplimiento de los acuerdos del Plan Nacional de Sustitución de Cultivos de Uso Ilícito (PNIS). (Lea también: [Ejercito y narcotráfico: verdugos de la sustitución de la Coca](https://archivo.contagioradio.com/ejercito-y-narcotrafico-verdugos-de-la-sustitucion-de-la-coca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, las denuncias de las comunidades también se centran en que **los operativos de erradicación forzada adelantados por la Fuerza Pública no cuentan con los protocolos de bioseguridad que se requieren en medio de la pandemia de la Covid-19, poniendo en riesgo a quienes habitan los territorios. **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este actuar de la Fuerza Pública, **también fue uno de los hechos que motivó movilizaciones sociales como la Marcha por la Dignidad**, que arribó a Bogotá el pasado 20 de julio, donde **varios marchantes se movilizaron, entre otras cosas, en contra de los «*abusos y arbitrariedades*» que llevan a cabo las Fuerza Militares en los territorios.** Así lo expresó Erika Prieto, integrante de la «Ruta Comunera» de la Marcha por la Dignidad.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
