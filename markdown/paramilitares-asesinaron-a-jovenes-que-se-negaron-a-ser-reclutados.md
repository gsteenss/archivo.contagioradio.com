Title: Paramilitares asesinaron a jóvenes que se negaron a ser reclutados
Date: 2015-07-21 14:30
Category: DDHH, Uncategorized
Tags: buenaventura, comunidades, Desplazamiento, Espacio Humanitario, paz, puente nayero, reclutamiento
Slug: paramilitares-asesinaron-a-jovenes-que-se-negaron-a-ser-reclutados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/niños.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ElPueblo 

<iframe src="http://www.ivoox.com/player_ek_5062481_2_1.html?data=lpWjlJmcdY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRlMLmwtLWzs7YpdPZ1JDO1crXrc%2FV09TbjcaPssqZpJiSpJbTt4zl1sqY1cqPssbbwtfc0JDFb9TZ05Cah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Maria Nieves Torres, Espacio Humanitario de Buenaventura] 

##### [21 jul 2015] 

El domingo 19 de julio, Cristian David Aragón, de 17 años de edad, caminaba por Buenaventura cuando hombres conocidos como "Mongo", "Alipio" y "Mono Concho", intentaron subirlo a un camión para reclutarlo forzadamente en las filas del paramilitarismo de este puerto del pacífico colombiano. Su amigo Sol Ángel Mina, también de 17 años de edad, intentó defenderlo. Ambos recibieron puñaladas y disparos en la espalda cuando intentaban escapar. Cristian murió pasadas las 8 de la noche, Sol Angel le sobrevivió sólo 6 horas más.

Los hechos ocurrieron, según denuncian los familiares de los jóvenes, a escasos 150 metros de un puesto de control de los Infantes de Marina. "No entendemos como en una zona que esta militarizada la policía no puede hacer nada." señala Maria Nieves Torres, lideresa del Espacio Humanitario de Puente Nayero, en Buenaventura, y a quien hace un año los paramilitares también le asesinaron a su hijo menor de edad.

Y es que la indignación crece en Buenaventura, pues el caso de Cristian y de Sol Ángel se repite a diario en el puerto. Mientras los jóvenes son reclutados forzadamente, las niñas y jovencitas son obligadas a prostituirse. "Los niños terminan el bachillerato y no pueden ingresar a la universidad", agrega Maria Nieves, quien recuerda que Cristian David había regresado a Buenaventura hace menos de dos semanas después de intentar buscar oportunidades de vida fuera de la ciudad.

"Hoy estamos encerrando a nuestros hijos", señala Maria Nieves. "Nosotros no queremos coger las armas para defender a nuestros hijos de los grupos paramilitares, pero estamos enterrando a los jóvenes, cuando deberíamos enterrar es a los adultos mayores. Será que es porque somos negros? ¿Será que creen que no somos humanos, que están permitiendo que nos maten? ¿Qué mas tenemos que esperar? ¿Qué joven más tenemos que enterrar?" finaliza.

En marzo del 2015, la CIDH instó al gobierno colombiano a adoptar medidas cautelares para el Espacio Humanitario, sin que a la fecha se hayan efectivas. Cabe recordar que en su informe anual sobre violación a los Derechos Humanos, Human Rights Watch alertó sobre la crítica situación del Puerto de Buenaventura, que actualmente cuenta con la tasa más alta de desplazamiento forzado en Colombia, con 22.383 personas desplazadas entre enero y noviembre del 2014.
