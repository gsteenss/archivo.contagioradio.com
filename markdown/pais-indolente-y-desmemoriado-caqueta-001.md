Title: País indolente y desmemoriado
Date: 2015-02-12 12:10
Author: CtgAdm
Category: Fernando Q, Opinion
Tags: contagio radio, Florencia, sepelio de niños en Caqueta, violencia colombia
Slug: pais-indolente-y-desmemoriado-caqueta-001
Status: published

###### Foto: HispanTv 

#### **[Por] [[Luis Fernando Quijano]](https://archivo.contagioradio.com/fernando-quijano/)**

Cientos de personas gritaban casi al unísono **Justicia**, durante el sepelio de los niños asesinados en  zona rural de Florencia, Caquetá. A este coro de indignados se han unido miles de colombianos que exigen duras penas para los responsables de tan abominable hecho.

Por fin, aunque desafortunadamente se de en estas circunstancias, el vil asesinato de los cuatro niños ha logrado que buena parte de la sociedad despierte de la insensibilidad y la indiferencia, en la cual ha estado sumida debido a las décadas de violencia y de guerra que ha soportado.

Esa violencia, que aletarga y vuelve fría a la sociedad, ha sido la causante de más de 40.000 desapariciones, millones de desplazamientos y cientos de miles de muertos; buena parte de esas víctimas son niños, niñas y adolescentes.

El Presidente Juan Manuel Santos dio la orden a la cúpula de la Policía Nacional para que viajaran al Caquetá y no regresaran hasta no encontrar y llevar a la justicia a lo responsables del horrendo crimen.

Cabe, ahora, preguntar a la sociedad y al Estado, ¿por cuánto tiempo se movilizarán? La pregunta parece capciosa pero no lo es,  ya que esta sociedad y este Estado ha contemplado silenciosamente, y muchas veces con mirada cómplice, cómo se  tortura, se pica y se asesina el futuro del país.

A la sociedad colombiana desmemoriada e indolente, y al Estado mediático y paquidérmico, se les debe recordar que no son los primeros ni los últimos niños que sucumbirán bajo la violencia y la degradación de la guerra.

Seguramente algún reinado de belleza, un partido de fútbol, los trinos de la extrema derecha y la respuesta que no da espera de la derecha y la izquierda, entre otras pasiones, distraen y logran que la sociedad olvide lo que a diario constata: desigualdad, pobreza y miseria extrema, generada por un modelo de acumulación capitalista, y que causa muertes de infantes ya sea por escasa atención médica, hambre, desnutrición; o exceso de balas.

¿Por qué se olvida que los depredadores sexuales, algunos de ellos cobijados en sotanas, causan cientos de víctimas debido al abuso sexual entre la población infantil?

¿Por qué es tan difícil recordar que la violencia intrafamiliar, el accionar de funcionarios y servidores públicos, la guerra y la violencia generalizada, dejan decenas de víctimas inocentes en la Colombia de la prosperidad democrática y su antecesora la de la seguridad democrática?

Las cifras no mienten, según el diario el País de Cali, en lo corrido del año 2015, han sido asesinados ocho niños en Colombia,  y en el año 2014 se registraron 872 homicidios de menores de edad, en los que Valle del Cauca, Antioquia y Bogotá se llevaron los primeros lugares: el primero con un registro de 299 homicidios, el segundo con 106 casos, y el tercero con 101.

En una entrevista, el padre del chico de siete años asesinado en la vereda Nagui Alto en La Vega (Cundinamarca) hecho ocurrido dos días después del asesinato de los cuatro menores en la zona rural de Florencia, Caquetá, afirmaba: "Qué le puedo decir. Hoy me entregan a mi hijo en pedazos. Ese dolor me está matando. No se lo deseo a nadie. Quiero que el culpable se pudra en la cárcel. Una persona así no merece vivir en este mundo”. Y aún así, este dolor no hace que nos solidaricemos y nos movilicemos exigiendo el fin de tanta masacre.

¿Cuándo se materializarán las voces que exigen **justicia,** las movilizaciones ciudadanas frente a la masacre de población infantil, la ordenes presidenciales que ordenan la captura de los responsables?

Es hora de recobrar la memoria, es hora de dejar la indolencia, es hora de tomar partido en la elaboración de una verdadera Paz. Es claro que la violencia y la guerra están acabando con el futuro del país, así que marchemos y exijamos el fin del conflicto ya.

   
 
