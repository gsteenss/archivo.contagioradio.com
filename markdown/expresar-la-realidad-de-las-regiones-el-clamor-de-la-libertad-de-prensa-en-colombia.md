Title: Expresar la realidad de las regiones: el clamor por la libertad de prensa en Colombia
Date: 2019-05-03 13:34
Author: CtgAdm
Category: DDHH, Nacional
Tags: dia mundial de la libertad de prensa, periodismo, Periodismo en las regiones
Slug: expresar-la-realidad-de-las-regiones-el-clamor-de-la-libertad-de-prensa-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/la-prensa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Nuevo Diario] 

A propósito del **Día Internacional de la Libertad de Prensa,**  periodistas que narran la realidad del país desde las regiones hacen un balance del oficio en diferentes lugares del Colombia, [un país en el que según la  Fundación para la Libertad de Prensa (FLIP) en 2018  se registraron 477 casos de ataques a la prensa, 167 más que el año anterior.](https://flip.org.co/micrositios/informe-2018/descargas/informe-anual-2018.pdf)

### ¿Qué significa hacer periodismo desde las regiones? 

"En las provincias, es muy meritorio hacer prensa, porque tienen el valor de seguir denunciando, porque cuando callan a un periodista, callan nuestro derecho a saber" indica el **periodista William Vianney de Buga**, Valle del Cauca,  quien considera necesario que la sociedad misma rodee y proteja a los periodistas regionales.

 <iframe src="https://co.ivoox.com/es/player_ek_35334485_2_1.html?data=lJqglZmYfJahhpywj5aUaZS1kZ2ah5yncZOhhpywj5WRaZi3jpWah5yncbjdzdHWw9KPmsrVz9PS25DIqYy21szOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Esta petición cobra sentido después de conocer las cifras de más reciente informe de la FLIP, registrando el número más alto de violaciones a la libertad de prensa en el país y que **en lo corrido del 2019, se han documentando 124 violaciones a la libertad de prensa afectando a 135 personas.** [(Lea también: Javier Darío Restrepo: El periodista y los medios de información en tiempos de paz)](https://archivo.contagioradio.com/javier-dario-restrepo-el-periodista-y-los-medios-en-tiempos-de-paz/)

**Gonzalo Portilla, periodista del Putumayo** afirma que a pesar de ser amenazado en el pasado por informar lo que sucede en su región, se requiere "escudriñar de forma veraz la realidad de los territorios, y dar la posibilidad de que la gente pueda conocer qué están pasando", incluso si eso significa causar malestar en ciertos sectores que no están interesados en que los hechos se den a conocer.

 <iframe src="https://co.ivoox.com/es/player_ek_35334447_2_1.html?data=lJqglZmYeJihhpywj5WZaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5yncajjz9_OztSPlNDm1c7ZzsaJdqSf0crfy9TIrdTowpDRx9GPlNbo1tLO29SPcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Por su parte, **Martha Isabel Martínez integrante  de la Asociación de Radios Amigas Comunitarias de Norte de Santander, (RADAR)** destaca que en la región " no solo hay violencia, también hay muchas cosas positivas" agregando que, es a partir de las cosas buenas que se puede aportar y construir desde la cotidianidad.

 <iframe src="https://co.ivoox.com/es/player_ek_35334472_2_1.html?data=lJqglZmYe5Ohhpywj5WYaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5ynca7V09nVw5Ctt8LWxtGYr8bWuIa3lIquptPJvozdz9nSydfFstXZhqifh6aUb8XZjNHOjabXs8TdwsjWh6iXaaOnz5Cah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### Un día para reivindicar el oficio y la libertad de prensa

Tanto Gonzalo como Martha coinciden en que es un día en el que el periodismo debe ser conmemorado, en particular por las personas que han dado su vida por este oficio o por quienes han sido amenazados o detenidos por cumplir con su labor, "es importante exigirle al Gobierno que dé las garantías, e investigue los casos de amenazas y asesinatos contra periodistas que quedan impunes", declara el periodista del  Putumayo.

"Debemos ser solidarios con quienes incluso han dado la vida en este oficio por la comunidad y la mejor forma es capacitarnos" afirma la integrante de RADAR quien señala la importancia de trabajar en red para fortalecer el periodismo regional, incentivando el conocimiento y el derecho a informar. Según datos complementarios de la FLIP, es precisamente la amenaza, la agresión más frecuente en lo corrido del año revelando que **se han denunciado 35 casos de amenazas, de los que 4 han llevado a que los periodistas se desplacen forzadamente.**

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
