Title: Más de 100 detenidos y 20 heridos deja la violencia policial contra movilización del 15 de Junio
Date: 2020-06-15 20:54
Author: CtgAdm
Category: Actualidad, DDHH
Slug: violencia-policial-15-junio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/violencia-policial-ESMAD.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-15-at-21.10.46.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-15-at-21.05.55.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-15-at-20.47.23.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-15-at-20.47.25.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-15-at-20.47.24.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-15-at-20.57.50.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: Contagio Radio/Carlos Zea

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La movilización de este 15 de Junio que se realizó en varias ciudades del país, entre ellas Cali, Bogotá y Medellín estuvo atravesada por una fuerte violencia policial del ESMAD y la Fuerza Disponible, a pesar que las movilizaciones no superaban las 500 personas y se estaban desarrollando en tranquilidad. La intervención de los uniformados dejó por lo menos a 90 personas detenidas y cerca de una decena de heridos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En Bogotá la violencia policial fue contra la mayoría de marchantes

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una de las ciudades más golpeadas por la acción del ESMAD fue la capital del país en la que, hacia las 7 de la noche, se reportaron por lo menos 50 personas detenidas. Según lo relata Daniela Buriticá integrante de la campaña Defender la Libertad un asunto de todos, la acción de los integrantes de esa fuerza policial fue desmedida, pues se impusieron comparendos, golpearon a las personas detenidas, un defensor de DDHH fue detenido lo mismo que un periodista.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Buriticá señaló que miembros de la Policía en Bogotá, detuvieron un total de 73 personas, amparados en la figura del “Traslado por Protección”; no obstante, manifestó que este mecanismo se viene utilizando de manera «arbitraria», ya que, a todas las personas retenidas les fueron impuestos comparendos. Por otra, parte destacó que aún se encuentran 4 manifestantes privados de la libertad con fines de judicialización, sin que aún haya versión oficial sobre los cargos que se les imputarían.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/watch/?v=185664759526380","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/watch/?v=185664759526380

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

Según la campaña, los detenidos fueron trasladados en varios buses dentro de los cuales las personas fueron víctimas de golpes y de decomiso de sus aparatos de comunicación. Adicionalmente se impusieron comparendos por la supuesta violación de las medidas de aislamiento, sin embargo los manifestantes aseguran que estaban cumpliendo los protocolos de bioseguridad.

<!-- /wp:paragraph -->

<!-- wp:html -->

> [\#EnVivo](https://twitter.com/hashtag/EnVivo?src=hash&ref_src=twsrc%5Etfw) Este lunes festivo [\#15Junio](https://twitter.com/hashtag/15Junio?src=hash&ref_src=twsrc%5Etfw) se registran 23 hombres y 3 mujeres detenidas por el GAEPVD en medio de movilizaciones en [\#Bogota](https://twitter.com/hashtag/Bogota?src=hash&ref_src=twsrc%5Etfw) .  
> ¡Conozca lo que ocurre! [pic.twitter.com/LHqsEPbnUH](https://t.co/LHqsEPbnUH)
>
> — Contagio Radio (@Contagioradio1) [June 15, 2020](https://twitter.com/Contagioradio1/status/1272644908713684993?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:paragraph -->

Dentro del grupo de periodistas agredidos, que según la [FLIP,](https://www.flip.org.co/index.php/es/) asciende a 13, se encuentra Carlos Zea, integrante del equipo de Contagio Radio quien fue agredido en medio de la movilización en el centro de Bogotá.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Por lo menos 45 personas entre heridos y detenidos en la ciudad de Medellín

<!-- /wp:heading -->

<!-- wp:paragraph -->

Otras de las ciudades en las que se registró movilización fue en Medellín en los que se reportan entre 45 y 48 personas detenidas y cerca de tres personas heridas entre ellas el periodista conocido como Aquí Noticias, quien desde tempranas horas se encontraba cubriendo la movilización.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según lo informado por el Proceso Social de Garantías, PSG, quienes acompañan la movilización en la capital antioqueña la mayoría de las personas detenidas presentan diferentes tipos de heridas por lo que varias de ellas tuvieron que ser trasladadas a centros asistenciales con múltilpes fracturas y heridas en diversas partes del cuerpo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Verónica López integrante del Comité de Solidaridad con los Presos Políticos (CSPP) en Antioquia, manifestó que dos personas resultaron con fracturas graves tras ser golpeados por miembros de la Policía y se centró en **el hacinamiento que presentan los centros de detención que según ella desbordan el 300% de su capacidad**, lo cual iría en contravía de las medidas de bioseguridad y distanciamiento social que se tienen que tener en cuenta en el marco de la pandemia actual.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La información que se tiene hasta el momento sobre los posibles cargos que se les imputan, abogados que acompañan los procedimientos han afirmado que se vincularían al delito de asonada.

<!-- /wp:paragraph -->

<!-- wp:html -->

> Denuncian fuerte agresión contra periodistas y personas en movilización pacífica en Medellín <https://t.co/0EcFX9hRjF>
>
> — Contagio Radio (@Contagioradio1) [June 15, 2020](https://twitter.com/Contagioradio1/status/1272621552178212865?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:heading {"level":3} -->

### Violencia policial es más fuerte en tiempos de pandemia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cabe resaltar que muchas de las personas detenidas, cerca del 90% según el PSG, hacen parte de organizaciones de DDHH, organizaciones estudiantiles o de la prensa. Adicionalmente las marchas no alcanzaron grupos mayores a trescientas personas, es decir que las detenciones y las heridas podrían aproximarse al 25% del total de los manifestantes, lo que podría interpretarse como un claro mensaje a quienes deciden protestar en medio del aislamiento y lo que algunos han llamado el autoritarismo de gobiernos locales y el gobierno nacional.

<!-- /wp:paragraph -->

<!-- wp:gallery {"ids":[85394,85403,85411,85404,85407,85410,85408]} -->

<figure class="wp-block-gallery columns-3 is-cropped">
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/violencia-policial-ESMAD-1024x682.jpeg){.wp-image-85394}  
   <figcaption class="blocks-gallery-item__caption">
    Movilización 15 de junio Bogotá
    </figcaption>
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-15-at-21.10.46-1024x576.jpeg){.wp-image-85403}  
   <figcaption class="blocks-gallery-item__caption">
    Movilización 15 de junio Bogotá
    </figcaption>
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-15-at-20.57.50-1024x682.jpeg){.wp-image-85411}  
   <figcaption class="blocks-gallery-item__caption">
    Movilización 15 de junio Bogotá
    </figcaption>
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-15-at-21.05.55-1024x682.jpeg){.wp-image-85404}  
   <figcaption class="blocks-gallery-item__caption">
    Movilización 15 de junio Bogotá
    </figcaption>
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-15-at-20.47.23-1024x682.jpeg){.wp-image-85407}  
   <figcaption class="blocks-gallery-item__caption">
    Movilización 15 de junio Bogotá
    </figcaption>
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-15-at-20.47.24-1024x682.jpeg){.wp-image-85410}  
   <figcaption class="blocks-gallery-item__caption">
    Movilización 15 de junio Bogotá
    </figcaption>
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-15-at-20.47.25-682x1024.jpeg){.wp-image-85408}  
   <figcaption class="blocks-gallery-item__caption">
    Movilización 15 de junio Bogotá
    </figcaption>
    </figure>

<figcaption class="blocks-gallery-caption">
Movilización 15 de junio Bogotá - Andrés Zea - Contagio Radio

</figcaption>
</figure>
<!-- /wp:gallery -->
