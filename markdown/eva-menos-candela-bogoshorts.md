Title: Eva menos Candela abre una nueva noche especial de BOGOSHORTS sessions
Date: 2019-05-07 15:07
Author: CtgAdm
Category: 24 Cuadros
Tags: bogoshorts, Cine Colombiano, Cine Tonalá
Slug: eva-menos-candela-bogoshorts
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/EC3.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

###### Foto: Fotograma Eva menos Candela 

Este martes 7 de mayo, como parte de **BOGOSHORTS sessions** se presentará el cortometraje colombiano **Eva menos Candela, de la directora Ruth Caudeli,** proyección que contará con la presencia de la misma directora de la cinta ¿Cómo te llamas?' Eva y Candela.

La producción de Ovella Blava Films, sirve a manera de secuela de la cinta estrenada en 2018, a partir del encuentro de **Eva y Candela quienes se reúnen dos años después de termina su relación porque Eva necesita algo de Candela. Aunque parece que han rehecho sus vidas, todavía tienen temas pendientes entre ellas.**

<iframe src="https://www.youtube.com/embed/YUoDceh6KmA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Los espectadores tendrán la oportunidad de saber que ocurrió con la gran pareja explosiva tras lo ocurrido en la primera cinta. **A partir de las 7 de la noche en la Carrera 6 \# 35 - 37 (Barrio La Merced), Bogotá.** Boletería \$2.000 (Público general) \$1.000 (Estudiantes presentando carnet vigente y mayores de 65 presentando la cédula).

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
