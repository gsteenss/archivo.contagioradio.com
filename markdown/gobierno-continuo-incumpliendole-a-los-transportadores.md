Title: Gobierno continuó incumpliéndole a los transportadores
Date: 2016-09-26 12:17
Category: Movilización, Nacional
Tags: Camioneros, ESMAD, Gobierno Nacional, Movilizaciones, Paro camionero
Slug: gobierno-continuo-incumpliendole-a-los-transportadores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/camioneros-contagioradio-e1474909758381.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: noticiascolombianas 

###### [26 Sep 2016]

Debido al incumplimiento por parte del gobierno en cabeza del Ministerio de Transporte, los **transportadores nuevamente piensan convocar un segundo paro nacional.** Así lo manifestó el presidente de la Asociación Colombiana de Camioneros de Caldas, Alex López quien asegura que de los puntos acordados en la mesa de interlocución entre el Gobierno Nacional y el gremio de camioneros, **ni un solo punto ha sido cumplido por parte del gobierno.**

El pasado mes de julio se finalizó uno de los paros de mayor duración por parte del gremio nacional de transportadores, con la firma del pliego de peticiones en el que se acordaba trabajar en puntos referentes a los precios de los combustibles acordes a los demás indicadores del transporte de carga, que el valor de los fletes fuera coherente con las exigencias de las carreteras del país y que no se favoreciera el monopolio a través de estos cobros, entre otros.

Al día de hoy, ninguno de los puntos ha sido cumplido. De acuerdo a lo señalado por Alex López, algunas de las falencias e incumplimientos se deben a **la no reglamentación sancionatoria de los decretos** estipulados que permiten a las empresas transportadoras sacar provecho de la situación en detrimento de los camioneros, el aumento de **un 160% en el costo de los peajes** y el aumento en el precio de los combustibles.

Cabe recordar, que en el marco del pasado paro de transportadores se registraron alrededor de 40 denuncias por abuso de fuerza y autoridad por parte del ESMAD a civiles, el asesinato en Duitama de Luis Orlando Saiz Villamil y 21 detenciones arbitrarias según el informe de la Comisión de derechos Humanos que hicieron presencia en los territorios.

<iframe id="audio_13066199" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13066199_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
