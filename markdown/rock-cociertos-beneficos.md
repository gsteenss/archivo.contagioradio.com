Title: 5 de los conciertos benéficos más memorables
Date: 2017-07-13 17:19
Category: Cultura, Otra Mirada
Tags: ayuda, Música, solidaridad
Slug: rock-cociertos-beneficos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/MAIN-Live-Aid-30th-Anniversary.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Live Aid 

###### 13 Jul 2017

A propósito del Día mundial del rock que se celebra cada 13 de julio en conmemoración del "Live Aid", evento que reunió en 1985 a grandes figuras del rock para apoyar la situación de pobreza extrema de países africanos como Etiopía y Somalia, repasamos algunos de los conciertos benéficos más recordados. (Le puede interesar: [Los versos de Neruda convertidos en canción](https://archivo.contagioradio.com/neruda-musica-poesia/))

**The concert for Bangladesh (Agosto 1 de 1971)**

<iframe src="https://www.youtube.com/embed/A8CivPhu0fw?list=PLCj-HMIMU6s6BI-CRKaOlfhBTCT6rfXU-" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

En el Madison Square Garden de Nueva York, se reunieron por invitación de George Harrison y Ravi Shankar, grandes artistas como Ringo Starr, Eric Clapton y Bob Dylan entre otros. 40.000 personas disfrutaron con las presentaciones destinadas a solidarizarse con el pueblo de Bangladesh, que vivía por entonces una catástrofe humanitaria y una terrible hambruna por cuenta de su separación violenta de Pakistán y el Ciclón Boha desastre natural que por poco lo borra del mapa.

**Live Aid (13 de Julio de 1985)**

<iframe src="https://www.youtube.com/embed/kR9CiWyzcKw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Es considerado por muchos el Concierto más grande de todos los tiempos. Dos escenarios, uno dispuesto en el mítico Estadio de Wembley  de Londres y el otro en John F. Kennedy Estadium de Filadelfia, acogieron a reconocidos artistas de la década de los 60, 70 y 80, en Beneficio de los países de África Oriental, puntualmente Etiopía y Somalia. El activista, músico y actor Bob Geldof fue el encargado de convocar artistas como Queen, U2, David Bowie, Elton John, Black Sabbath, Led Zeppelin, The Who, BB King y muchos más, logrando una masiva asistencia de 85.ooo personas en Inglaterra y 99000 en EEUU.

**Live 8 (2 de julio de 2005)**

<iframe src="https://www.youtube.com/embed/xVQTKSWULu8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El mismo Bob Geldof volvería 20 años después a convocar grandes estrellas de la música para cantar en diferentes escenarios del mundo. Ciudades como Londres, Filadelfia, Paris, Berlín, Roma, Johannesburgo y Moscú entre otras, se unieron para presionar a los líderes del G8 a comprometerse a combatir la pobreza en lugares como África. U2, Paul McCartney, Deep Purple, Audioslave, Cold Play, Madonna, Bon Jovi y The killers entre otros atendieron al llamado que tuvo como acontecimiento histórico el reencuentro de Pink Floyd después de 24 años de división en lo que sería su última aparición juntos en un escenario.

**Live Earth (7 de Julio de 2007)**

<iframe src="https://www.youtube.com/embed/1AeY1xE5u2Q?list=PL-jwYtxv4AZpalXlreGLaYIpNBWQWo9IW" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Inspirado en el Live Aid y con el propósito de llamar la atención del mundo sobre el impacto del calentamiento global en el planeta, el ex presidente de los Estados Unidos Al Gore logró reunir más de 150 artistas del mundo como Foo Fighters, Genesis, Bon Jovi, Roger Waters, The Smashing Pumpkins y Linkin Park entre otros, con tarimas ubicadas en ciudades como Sidney, Nueva Jersey, Rio de Janerio, Tokio, Londres, Washington y Roma.

**12 12 12 The Concert for Sandy Relief (12 Diciembre 2012)**

<iframe src="https://www.youtube.com/embed/TagbEY3SK28" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El Madison Square Garden de Nueva York, sirvió una vez más como tarima para los conciertos beneficos. En Diciembre de 2012 un grupo de artistas como The Rolling Stones, Bruce Springsteen, Billy Joel, Roger Waters, The Who, Alicia Keys, Paul McCartney y Eddie Vedder entre otros, ofrecieron un magno concierto en solidaridad con las víctimas del Huracán Sandy en el Noroeste de los Estados Unidos.

 
