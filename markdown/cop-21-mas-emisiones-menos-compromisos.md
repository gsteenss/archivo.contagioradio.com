Title: COP 21: más emisiones, menos compromisos
Date: 2015-12-10 09:57
Category: CENSAT, Opinion
Tags: Acuerdos COP 21, Calentamiento global, CENSAT, COP 21
Slug: cop-21-mas-emisiones-menos-compromisos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/coalition-climat21.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: coalitionclimat21] 

#### **Por [Censat Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [@CensatAguaViva](https://twitter.com/CensatAguaViva)** 

###### [10 Dic 2015] 

En medio de un aire enrarecido por la represión y los anuncios de guerra, avanza en París la 21 Conferencia de la Partes (COP) sobre cambio climático. Lo que surja de esta COP 21 más que desalentador es preocupante, se alistan más retrocesos que aumentan el riesgo de desaparición de la vida humana y los responsables directos son los gobiernos y las corporaciones que intentan por todos los medios influir en las decisiones, al tiempo que se limita la participación de la sociedad.

Los retrocesos que se prevén son claros en el texto de la negociación de París, en él se promueve el término de ´todas las partes´ que desdibuja la consideración de las responsabilidades comunes pero diferenciadas´ contenida en el Protocolo de Kyoto. Además, el texto invita, no obliga, a los países a ponerse metas “más ambiciosas” para el período posterior a 2020, determinando que cada país haga en adelante lo que le plazca.

Las metas voluntarias de reducción de emisiones están contenidas en las Contribuciones Previstas Determinadas a Nivel Nacional (INDC por sus siglas en inglés), la suma de estas contribuciones llega a casi el doble de los niveles de emisiones permitidas para evitar la debacle ambiental. Esto tendrá consecuencias agravadas si tenemos en cuenta que las negociaciones no ponen tope a la extracción de combustibles fósiles, aún cuando científicos han sugerido dejar el 80% de las reservas bajo el suelo.

De otro lado Estados Unidos y otros han venido haciendo incidencia con países insulares para retirar del acuerdo lo relacionado con “pérdidas y daños”, esto implicaría dejar de lado los impactos ya ocasionados y que no han sido atendidos.

Otro retroceso de las negociaciones es el “avance” de la estructuración de los mercados de carbono. La economía verde ha acaparado el centro de las negociaciones, de hecho la mayoría de las contribuciones adoptan las falsas soluciones como son los Mecanismos de Desarrollo Limpio (MDL), Comercio de emisiones, REDD+ y carbon pricing (poner precio al carbono). Propuestas que permiten a los países industrializados evadir sus responsabilidades históricas mientras propician la mercantilización de la atmósfera para el beneficio de las empresas, corporaciones conservacionistas y financieras.

La regresión en materia ambiental y las contradicciones de la COP 21 tienen paralelo con la política colombiana, donde se incentiva la actividad extractiva mientras se elevan discursos de crecimiento verde. Por un lado se acelera el ritmo de extracción de combustibles fósiles para la exportación, mientras se subsidia la producción de agrocombustibles argumentando la necesidad de mitigar el efecto invernadero; a su vez se promueven proyectos hidroeléctricos bajo el argumento de ser energías limpias, no obstante la emisión de metano y la violación sistemática de derechos humanos y ambientales; por otra parte, mientras se promete el aumento de áreas protegidas en 2.5 millones de hectáreas, se plantea la sustracción de inmensas áreas de reserva forestal de ley segunda para abrir espacio a las ZIDRES; además, el gobierno asume la meta de deforestación cero para 2020 al tiempo que mantiene la titulación minera y oferta 24 millones de hectáreas para monocultivos comerciales.

En medio de este vago escenario de la COP21 que pone en riesgo la humanidad y favorece los intereses de las corporaciones, los pueblos siguen recreando propuestas novedosas, apostándole a la vida, no a los negocios. La Red Internacional Oilwatch, de la cual Censat Agua Viva es parte, propone la creación del Anexo Cero donde estarían los pueblos, nacionalidades y comunidades, quienes tienen capacidad e interés por acelerar las transformaciones requeridas para la protección del clima global, pueblos que dan pasos firmes para no extraer hidrocarburos y quienes tienen iniciativas basadas en la solidaridad, el intercambio tecnológico sin ataduras y la existencia de una deuda ecológica asociada al clima.

Así mismo la Vía Campesina ha propuesto la agricultura campesina como opción para enfriar el planeta y cambiar la agricultura basada en petróleo. Adicionalmente el Tratado de los Pueblos impulsado por múltiples organizaciones sociales, busca ser vinculante para juzgar a las trasnacionales por la violación de derechos humanos y ambientales.

Estas propuestas están a tono con las voces del Movimiento Internacional por la Justicia Climática que exigen soluciones efectivas y proponen cambios profundos para enfrentar la crisis climática. Indígenas, campesinos, mujeres, pescadores, jóvenes, trabajadores se han congregado en París; y aunque el gobierno francés ha prohibido y reprimido las movilizaciones sociales bajo el argumento de la amenaza terrorista, “restringiendo libertades con el fin de defender la libertad”, el pasado 29 de noviembre, más de un millón de personas en el mundo -10 mil en parís- salieron a la calle a demandar justicia climática.

Es la hora de actuar de los pueblos, superar el miedo y la intimidación de los que pregonan la guerra. Es tiempo de volcarse a exaltar el valor de la vida, afianzar el discernimiento informado, defender las diversidades naturales – culturales y esforzarnos por la creatividad para generar el necesario cambio del sistema.

¡El sistema debe cambiar, no el clima!
