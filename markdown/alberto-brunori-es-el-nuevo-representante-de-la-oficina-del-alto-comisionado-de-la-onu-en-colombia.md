Title: Alberto Brunori nuevo representante de la Oficina del Alto Comisionado de la ONU en Colombia
Date: 2018-03-21 14:21
Category: DDHH, Nacional
Tags: Estado Colombiano, la Oficina del Alto Comisionado de Derechos Humanos de la ONU, ONU
Slug: alberto-brunori-es-el-nuevo-representante-de-la-oficina-del-alto-comisionado-de-la-onu-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/PAG-08-F01-AC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Periódico] 

###### [21 Mar 2018] 

Tras la presión ejercida por más de 416 organizaciones, de carácter nacional e internacional que expresaron su preocupación por la falta de un representante de la Oficina del Alto Comisionado de las Naciones Unidas para Colombia, **fue nombrado Alberto Brunori**. De acuerdo con Diana Sánchez, defensora de derechos humanos, esta fue otra acción por parte del Estado Colombiano para obstruir la construcción de paz y la defensa de los derechos humanos en el país.

Esta situación se da luego de que el Estado colombiano retiró la credencial al Todd Howland, anterior representante, sin antes haberla otorgado al nuevo representante, Alberto Brunori, evitando la presentación del informe sobre el balance de los derechos humanos que hace esta organización en el país.

Para Diana Sánchez, quien es integrante de la organización Minga, también se tiene información sobre que “**el Estado, tampoco tendría la intención de acreditarlo prontamente”**, lo que significaría que esta oficina no puede tener una vocería oficial para continuar con el monitorio, la veeduría de la defensa de los derechos humanos, ni podría pronunciarse sobre lo mismo.

### **¿Cuál es la importancia de este nombramiento en Colombia?** 

Para Sánchez, Colombia está viviendo un momento bastante particular con el aumento de asesinatos, amenazas y agresiones tanto a líderes sociales como a defensores de derechos humanos, la coyuntura electoral y la implementación de los Acuerdos de Paz, todos temas en los que la Oficina del Alto Comisionado de las Naciones Unidas para los derechos humanos, tenía una alta incidencia.

Esta situación, para la defensora de derechos humanos, se suma a las acciones que ha tenido el Estado **Colombiano por obstaculizar el trabajo y avance en la construcción de paz al interior del país**. (Le puede interesar: ["Corte Penal Internacional abrirá investigación formal contra generales colombianos"](https://archivo.contagioradio.com/cpi-abriria-investigacion-formal-contra-generales-colombianos/))

 “En este momento el gobierno está jugando con una doble faz, es decir, no cierra la oficina, pero si aprovecha la oportunidad para bajarle el perfil, dejarla acéfala de representante que no se pueda pronuncia en temas  eventuales y por supuesto que no le haga un **contrapeso en la imagen que quiere manejar el gobierno en el nivel internacional, frente a la superación de derechos humanos**” afirmó Sánchez.

<iframe id="audio_24725691" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24725691_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
