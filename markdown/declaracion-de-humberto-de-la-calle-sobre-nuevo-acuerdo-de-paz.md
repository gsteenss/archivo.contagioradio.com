Title: Declaración de Humberto de la Calle sobre nuevo acuerdo de paz
Date: 2016-11-12 19:11
Category: Nacional, Paz
Slug: declaracion-de-humberto-de-la-calle-sobre-nuevo-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/humberto-de-la-calle-nuevo-acuerdo-de-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Eluniversal] 

###### [12 Nov 2016]

DECLARACIÓN JEFE DE LA DELEGACIÓN  
Humberto de la Calle  
La Habana, 12 de noviembre de 2016

Buenas tardes,  
Después del resultado obtenido el pasado 2 de octubre en el que más de seis millones de colombianos expresaron mediante su voto los reparos al Acuerdo firmado entre el Gobierno y las FARC, comprendimos que era necesario trabajar con celeridad y responsabilidad en la búsqueda de un nuevo Acuerdo. Un Acuerdo incluyente y respetuoso.

A partir del mismo 3 de octubre, los colombianos hemos sido testigos de un ejercicio profundo de diálogo.

Además de las múltiples reuniones que sostuvo el Jefe de Estado con los diferentes líderes del No, muy rápidamente designó una Comisión conformada por el Ministro de Defensa, la Canciller y por mí, en calidad de Jefe Negociador; en la que también participaron el Equipo Negociador, el Ministro del Interior y el Alto Consejero para el Posconflicto y el Alto Comisionado para la Paz, Sergio Jaramillo; con el propósito de sostener diálogos abiertos y recoger inquietudes, preocupaciones y sugerencias al Acuerdo para la Terminación del Conflicto.

Fue un esfuerzo genuino, leal, que nos permitió recopilar la totalidad de las iniciativas, ordenarlas, estudiarlas a fondo, luego de cerca de cien horas de intercambio directo de opiniones en un ejercicio constructivo, franco y democrático.

Después de terminar las reuniones con los voceros del No, viajamos a La Habana para discutir con las FARC las preocupaciones expresadas en Bogotá, insistir en la importancia de incorporar el mayor número de propuestas al texto y trabajar en la construcción de un nuevo Acuerdo que cumpliera en lo posible con las expectativas de muchos colombianos.

Las reuniones con la delegación de las FARC fueron igualmente intensas. Más de 15 días y noches de trabajo reuniones con discusiones arduas dada la complejidad de los temas. Debo destacar el ambiente constructivo y la buena voluntad de las delegaciones para trabajar con disciplina y seriedad.

Este nuevo Acuerdo fue además una oportunidad para aclarar dudas, resolver inquietudes pero sobretodo para unirnos los colombianos.

Una vez más nos probamos que, a pesar de las diferencias y visiones distintas, a través del diálogo es posible llegar a puntos de encuentro.

Esta tarea la asumimos con compromiso, respeto y responsabilidad. Pensamos en los colombianos, en sus expectativas y sus válidas inquietudes, pero también trabajamos con la certeza de que no hay más tiempo que perder. Estamos convencidos de que este documento señala caminos viables y posibles para acabar con tantas décadas de conflicto en Colombia.

Ahora deseo hacer una confesión personal: Dije que el Acuerdo del 26 de septiembre era el mejor Acuerdo posible, aunque era también susceptible de críticas. Y señalé que esa afirmación no se hacía desde la arrogancia. Era el mejor por cuanto permitía dar por terminado el conflicto, no para desestimar las insatisfacciones que podía generar.

Hoy con humildad reconozco que este Acuerdo es mejor en cuanto resuelve muchas de esas críticas e insatisfacciones. Su aceptación no va a ser unánime. Como tampoco lo fue en el primer acuerdo. Pero esperamos que la base de apoyo que recoja lo haga más sólido.

Pero más que exaltar las virtudes de éste frente al anterior, en lo que gana con creces es que mediante un ejercicio democrático se ha logrado construir una base de sustento más amplia. La cuestión no es sólo la ampliación del apoyo, sino que este ha sido fabricado en un ejercicio leal de democracia y diálogo sincero.

A los otros seis millones de votos que nos acompañaron en la refrendación del Acuerdo, quiero decirles que los ajustes y precisiones que hemos realizado, no sacrifican las convicciones que le dieron forma al primer Acuerdo.

No cancelamos la ilusión. No dimos marcha atrás en la posibilidad de un país nuevo. No desistimos del propósito de reivindicar la familia campesina. Limpiar la política. Combatir las bandas criminales y la corrupción. Contribuir a superar el problema mundial de la droga. Impartir justicia pensando más en las víctimas que en los barrotes. Integrar nuestros territorios. Superar desigualdades ancestrales. Crear una sociedad más equitativa. Crearla a través del diálogo, del uso razonable de la autoridad del Estado, buscando darle cimientos fuertes a la seguridad humana. Escuchando, sobre todo escuchando a los más débiles.

Quiero destacar algunas de las innovaciones de este Acuerdo. :

Durante el término de la dejación de armas, las FARC presentarán un inventario de bienes y activos para destinarlos a la reparación material de las víctimas.  
En cuanto a la Jurisdicción Especial para la Paz se atendió la mayoría de las propuestas formuladas.

Se precisaron de manera concreta las características y mecanismos de la restricción efectiva de la libertad. En efecto, se fijaron los espacios territoriales específicos para el cumplimiento de las sanciones con un tamaño máximo a las zonas veredales, los periodos de ejecución de las acciones reparadoras, la precisión del lugar de residencia, los mecanismos de monitoreo y la regulación del sistema de autorización para los desplazamientos por fuera de las zonas, requisito necesario en todos los casos. Por iniciativa de algunos opositores se aceptó que mientras entra en funcionamiento la JEP, las acciones reparadoras debidamente verificadas pueden ser descontadas de la sanción que se imponga.

Sobre el discutido tema de la conexidad del narcotráfico con el delito político, el acuerdo es que los Magistrados tendrán en cuenta caso a caso la jurisprudencia de las cortes colombianas.

Se eliminan los Magistrados extranjeros pero se acepta la presencia de amicus curiae, -expertos extranjeros- para rendir conceptos sobre los casos que se tramiten.

Queda claro que entre la normatividad aplicable se incluye el Código Penal Colombiano y que las normas procedimentales deberán ser incorporadas al ordenamiento legal.

Serán de competencia de la JEP las conductas de financiación o colaboración con actores del conflicto en que hayan incurrido los terceros no combatientes que tuvieron una participación activa o determinante en los crímenes más graves. Se eliminó la participación habitual. Y se reafirmó que los demás, si contribuyen a las medidas de verdad y reparación, pueden beneficiarse de la renuncia de la acción penal u otro tipo de terminación anticipada del proceso.

Se estableció el término concreto de duración de la Jurisdicción.  
Se eliminó la idea de incorporar el Acuerdo a la Constitución Política y al llamado bloque de constitucionalidad. El principio general de garantía de cumplimiento es el compromiso de que ambas partes cumplirán de buena fe lo pactado, y en lo que tiene que ver con el Estado, los principios que informan el Acuerdo serán parámetro de interpretación y guía de la aplicación normativa y práctica.

Se estableció también que la revisión de la tutela contra decisiones de la Jurisdicción Especial en cabeza de la Corte Constitucional.

En el nuevo Acuerdo se define qué se entiende por enfoque de género. Significa el reconocimiento de la igualdad de derechos entre hombres y mujeres y de las circunstancias especiales de cada uno. Supone reconocer que el conflicto ha impactado de manera diferenciada a la mujer y que, en consecuencia, se requieren acciones distintas y específicas para restablecer sus derechos.

Incluye también un principio de respeto a la igualdad y no discriminación, que implica que toda persona, independientemente de su sexo, edad, creencias religiosas, opiniones, identidad étnica, pertenencia a la población LGBTI, o cualquier otra razón, tiene derecho a disfrutar de todos los derechos. Ningún contenido del Acuerdo Final se entenderá o interpretará como la negación, restricción o menoscabo de los derechos de las personas.

En atención al llamado que nos hicieron de diversos sectores religiosos, se estipula que en la implementación de lo acordado se deberá respetar la libertad de cultos, lo que significa el reconocimiento y respeto a la práctica de cualquier manifestación de religiosidad.  
Mantenemos vivo el compromiso con el campo a través de una política de recuperación de la familia campesina, que impulse el acceso equitativo a la tierra y que cree condiciones de vida digna.

En la Reforma Rural Integral hemos afirmado que “nada de lo establecido en el acuerdo debe afectar el derecho constitucional a la propiedad privada”.  
También quedó explícito que los programas cuyo destinatario es el campesino no impiden la puesta en práctica de diversas formas de producción, tales como la agroindustria o el turismo.

Para lograr darle la mayor solidez a la reforma rural, se ajustaron los tiempos de implementación a las nuevas realidades fiscales. El acuerdo además por sí mismo no crea Zonas de Reserva Campesina.

Desde el Acuerdo de Cartagena se había dado un paso inmenso en la lucha contra el problema mundial de la droga. Las FARC se comprometieron a romper todo vínculo con él y a cooperar en la superación de ese fenómeno. Ahora, en el nuevo Acuerdo se ha logrado precisar de manera concreta las características de esa cooperación y además quienes acudan a la JEP –todos, no solo las FARC- deberán informar de manera exhaustiva y detallada sobre las informaciones de las que dispongan para atribuir responsabilidades.

De igual modo se aclara que los programas de sustitución buscan tener territorios libres de cultivos de uso ilícitos de modo que no se establezca un marco de coexistencia entre el programa de sustitución y la continuación de tales cultivos.

En cuanto a la aplicación de las políticas sobre el consumo, se robustece el papel de la familia y de los grupos religiosos.  
Se estableció el compromiso de respetar el principio de sostenibilidad de las finanzas públicas. En tal sentido, el Plan Marco acordado para las inversiones, deberá contener las fuentes de financiación.

A partir de hoy los colombianos podrán consultar los cambios y los nuevos elementos del nuevo Acuerdo en las páginas de internet de La Presidencia de la República, de la Oficina del Alto Comisionado para la Paz, de Equipo Paz Gobierno y de La Mesa de Conversaciones. En el transcurso de la próxima semana estará disponible la versión integrada.

Estimados señores y señoras, No podemos pedir que cese la discusión. Pero sí podemos desear que la gran decisión nacional sea poner en marcha la ejecución de los acuerdos, superar el conflicto armado, abrir caminos a la reconciliación y profundizar los esfuerzos para lograr una sociedad equitativa.

Ese es el reto ahora. El reto de nosotros, por nuestros hijos y nuestros nietos.
