Title: Comisión asesora de paz no puede relegar al Consejo Nacional de Paz
Date: 2015-03-11 17:18
Author: CtgAdm
Category: Entrevistas, Paz
Tags: alirio uribe, Angela Maria Robledo, Clara López, Comisión Asesora de Paz, Consejo Nacional de Paz, Frente Amp
Slug: comision-asesora-de-paz-no-puede-relegar-al-consejo-nacional-de-paz
Status: published

###### Foto: ContagioRadio 

Luego del anuncio del presidente Juan Manuel Santos de instalar una **Comisión Asesora de Paz**, las reacciones por parte de vario sectores no se han hecho esperar. Unas de ellas tienen que ver con la preocupación que surge de que el presidente no hay hecho alusión al **Consejo Nacional de Paz**, Alirio Uribe, representante a la cámara afirma que se crea esa comisión por un discurso y se relega al  CNP que es Ley.

**Clara López, presidenta del Polo Democrático** y candidata a la alcaldía de Bogotá fue una de las figuras políticas nombradas para esta Comisión Asesora y afirma que es muy positivo que la conformación de esta Comisión sea diversa y plural, en esa pluralidad radica la riqueza de la comisión, afirma López, quien también señala que se informará primero de lo que no se sabe para luego emitir opiniones o recomendaciones respecto de los temas que le sean consultados.

López afirma que tanto la Comisión Asesora como el Consejo Nacional de Paz tienen que ser escenarios complementarios y en ningún momento se podrá relegar el papel del CNP.

De esta comisión también hacen parte Antanas Mockus, el expresidente de la CEC Rubén Salazar, Carlos Yepez presidente de Bancolombia, Julio Roberto Gómez de la CGT, el G ® Rafael Samudio, Vera Grabe ex integrante del M-19, Paula Moreno, Ati Quigua, Marta Lucía Ramírez, Oscar Iván Zuluaga y el Ex presidente Andrés Pastrana.

Para algunos analistas la conformación de esta comisión asesora deja por fuera amplios sectores populares que hacen parte del Consejo Nacional de Paz que no ha sido convocado aún, luego de varios meses de su instalación. Por ejemplo, Ángela María Robledo, representante a la Cámara afirma que es el CNP el escenario en que se aterrizarán los acuerdos y se debe comenzar a hacer **pedagogía para la paz.**
