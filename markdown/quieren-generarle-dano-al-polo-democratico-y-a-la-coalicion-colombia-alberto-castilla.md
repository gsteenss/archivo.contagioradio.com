Title: "Quieren generarle daño al Polo Democrático y a la coalición Colombia" Alberto Castilla
Date: 2018-03-20 13:30
Category: Entrevistas, Política
Tags: Alberto Castilla, Polo Democrático Alternativo
Slug: quieren-generarle-dano-al-polo-democratico-y-a-la-coalicion-colombia-alberto-castilla
Status: published

###### [Foto: Archivo Partícular] 

###### [20 Mar 2018] 

El senador Alberto Castilla afirmó que las acusaciones hechas por la Fiscalía General de la Nación, en donde lo señalan de tener vínculos con la guerrilla del ELN, hacen parte de una estrategia por **“generarle daño” no solo al partido Polo Democrático, sino también a la colación Colombia** y a las posibilidades de avanzar en una coalición más grande que busque la presidencia de Colombia.

“Desde el Polo Democrático Alternativo, hemos dicho, y así lo han expresado los directivos, que esta ha sido una práctica recurrente, desde Gerardo Molina que ha sido vinculado a investigaciones, el maestro Carlos Gaviria, Jorge Enrique Robledo, Alexander López, Ivan Cépeda, hay una secuencia de actuaciones que buscan generarle daños a un partido tan importante en la política colombiana” afirmó Castilla.

### **La persecución política** 

Para el senador Castilla, el hecho de que los medios de información sean los que notifican primero, en vez del conducto regular que pasa por la Corte Suprema de Justicia, **evidencia un propósito distinto a que se quiera adelantar la investigación.**

“Está dirigida a obstaculizar, a generar una opinión en la gente de temor y miedo y oponerse a una gran confluencia para buscar ser gobierno” expresó Castilla y aseguró que “tratar de **enlodar el nombre de un parlamentario que fue reconocido en las urnas el 11 de marzo** y que mantiene una curul producto de su trabajo, tiene el propósito de afecta la imagen del Polo Democrático”. (Le puede interesar: ["Las víctimas y la cultura lograron curul en el Congreso"](https://archivo.contagioradio.com/las-victimas-y-la-cultura-lograron-curul-en-el-congreso-de-la-republica/))

De igual forma manifestó que no tiene conocimiento aún de la compulsa de copias que ordena la investigación, ni de las pruebas que se han mostrado en los medios de comunicación, que lo relacionan con el ELN, y frente a la financiación de su primera campaña electoral al senado el periodo pasado, el senador afirmó que presentó sus cuentas al CNE y a la Registraduría en donde se evidencia todos los aportes que se le realizaron.

### **La elección de Castilla al Congreso** 

El Senador del Polo Democrático explicó que durante el 2014, primer periodo en el que es elegido, su campaña se da en un momento de movilización tras la victoria del movimiento social y del paro campesino y logra **21.850 votos que lo eligen como el primer campesino en la historia política de Colombia que llega al Congreso**.

Mientras que las elecciones de este son producto del trabajo que de acuerdo con el senador, realizó en defensa del campesinado, que explica el crecimiento de sus electores.

<iframe id="audio_24652433" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24652433_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
