Title: El documental de Ciudad Bolívar que ganó un 'India Catalina'
Date: 2016-03-10 15:12
Author: AdminContagio
Category: 24 Cuadros
Tags: Nar-Heb, Ojo al sancocho, Premios India Catalina
Slug: el-documental-de-ciudad-bolivar-que-gano-un-india-catalina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/NAR-HER.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Ojo al sancocho 

<iframe src="http://co.ivoox.com/es/player_ek_10753516_2_1.html?data=kpWkl5iZdZehhpywj5aXaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5yncabgjMncxdrRqc%2FowtGYxsqPh8rpxcbRjafTsIa3lIquptvFtozl1sqYycbSaaSnhqegjdrSb4ammK7bxs7Fb6TVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Yaneth Gallego, Productora] 

##### [10 Mar 2016]

“Nar-Heb, Nuestro territorio", es el título del documental con el que la productora Sueños Films, consiguió el primer Premio India Catalina para Ciudad Bolívar, localidad que ha hecho del cine y el video comunitario, la mejor herramienta para contar las realidades de miles de colombianos y colombianas que llegan a la capital buscando un mejor vivir.

El audiovisual es fruto del acompañamiento a un grupo de indígenas desplazados, pertenecientes al pueblo Wounan del Chocó, y los conflictos territoriales que representa para ellos su llegada y estadía en una ciudad de condiciones sociales tan complejas como Bogotá y la marginalidad con la que históricamente se ha estigmatizado y relegado a Ciudad Bolívar.

La distinción como 'Mejor producción de Canal comunitario' de la edición número 32 de los Premios, reconoce el duro trabajo realizado por jóvenes como Yaneth Gallego, productora del documental, quienes desde el cine comunitario y las escuelas populares para jóvenes y adultos, fomentan el desarrollo audiovisual con sentido social, y en los últimos 5 años particularmente con la población indígena desplazada.

[![india catalina](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Cc1El8gWIAA9P32-e1457639002421.jpg){.aligncenter .wp-image-21318 width="498" height="350"}](https://archivo.contagioradio.com/el-documental-de-ciudad-bolivar-que-gano-un-india-catalina/cc1el8gwiaa9p32/)

"No es suficiente grabar a las personas sin algún trasfondo pedagógico, esto requiere un replanteamiento constante que va de mano con el acompañamiento social, con el fin de buscar una buena calidad de vida de las poblaciones vulnerables de este país, este resultado no es para nosotros, sino con quien arduamente trabajamos, los indígenas" asegura la productora del documental, quien además hace parte del equipo creador del Festival de cine y video comunitario 'Ojo al Sancocho'.

El proceso de producción de “Nar-Heb, Nuestro territorio" inicio en el año 2010, a partir de la colaboración y acompañamiento de integrantes de Ojo al Sancocho y su apuesta pedagógica audiovisual, con el objetivo de "exponer la visión de ellos, no una mirada subjetiva que parte de un grupo externo a este espacio", buscando con su construcción y divulgación aportar en "la búsqueda de una vida digna y todo lo que esta acoge, lo cual va de la mano con el arte" afirma Gallego.

<iframe src="https://www.youtube.com/embed/keChsWrQWrU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Y es precisamente el arte una herramienta fundamental para la apuesta del grupo de jovenes entusiastas, quienes valoran su capacidad de "incidir, cuestionar… sobreponiendo realidades que se desconocen" materializadas en el cine comunitario con la reconstrucción del contexto en el que viven sus protagonistas, que permita visibilizar esas realidades marginalizadas, fomentar el pensamiento crítico y producir transformaciones  a partir de estas.

Desde el equipo de Sueños Films, lamentaron profundamente el asesinato de Klaus Zapata, quien hizo parte de uno de los procesos pedagógicos de las escuelas populares, asegurando que "es la clara muestra de que la violencia que acoge a los jóvenes citadinos, no gira en torno a la falta de dinero, sino de una intervención social que haga reconocer la paupérrima calidad de vida y el incremento de la brecha social, pues muchos conflictos sociales parten de la estigmatización y la marginalización" que esta produce.

El próximo sábado 12 de marzo, desde las 5 de la tarde se realizará el conversatorio "1er India Catalina para Ciudad Bolívar", en el que se presentará el documental con presencia de algunos de sus protagonistas, en la Casa de la Cultura de Ciudad Bolívar, ubicada en la carrera 38 N 53B 43 Sur, Candelaria la Nueva.[  
](https://archivo.contagioradio.com/el-documental-de-ciudad-bolivar-que-gano-un-india-catalina/cc1el8gwiaa9p32/)
