Title: Continúa la violencia en Jerusalén y Cisjordania
Date: 2015-10-05 16:21
Category: DDHH, El mundo
Tags: ataques contra palestinos, Cisjordania, Explanada de las Mezquitas, Franja de Gaza, Hamás, Isarel, Israel Katz, Mahmud Abbas, Oslo, Palestina, Ramala
Slug: continuan-la-violencia-en-cisjordania-y-franja-de-gaza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/gaza.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:20minutos.com 

###### [5 oct 2015]

Este este lunes en horas de la mañana, aviones militares de Israel nuevamente atacaron la Franja de Gaza con cuatro explosiones que estarían dirigidas al Movimiento de Resistencia Islámico, Hamás. Así mismo, en Cisjordania durante el fin de semana se registraron **enfrentamientos entre manifestantes palestinos y miembros de las fuerzas israelíes, dejando dos jóvenes palestinos muertos, uno de 13 años y otro de 18.**

Huthaifa Othman Suleiman, de 18 años, falleció en la madrugada, cuando un grupo de jóvenes atacó un puesto de control del Ejército israelí a las afueras de la ciudad de Tulkarem, en el noroeste de Cisjordania. Por otra parte, Abed Al Rahman Shadi Obeidalah, **el menor de 13 años de edad murió luego de recibir un disparo en el pecho de un soldado israelí.**

Según la Sociedad de la Media Luna Roja en Ramala, "**hay 41 heridos de bala con munición real, 143 con balas de acero recubiertas de goma, 297 intoxicados por inhalación de gas y 18 que han sufrido trauma**". Además, hay 27 personas de los equipos de emergencia de la Media Luna Roja que resultaron heridos y cinco de sus ambulancias fueron dañados por el Ejército de Israel.

A estos hechos se suman los **enfrentamientos que se han presentado en las últimas semanas por el acceso a la zona de la Explanada de las Mezquitas o Monte del Templo** en Jerusalén, que impiden las fuerzas israelíes a estos lugar sagrado tanto para los musulmanes como para los judíos.

Estos sucesos se presentan después de que Mahmud Abbas, presidente de Palestina, pidiera la solidaridad del mundo frente a las continuas provocaciones de Israel, además que anunciaran su desvinculación de los acuerdos de paz firmados en Oslo en 1993.

Por su parte el ministro de Inteligencia** israelí, Israel Katz, amenazó con ejecutar nuevas operaciones militares contra los palestinos** tras estos últimos ataques.
