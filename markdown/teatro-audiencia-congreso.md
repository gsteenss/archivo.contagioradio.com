Title: Audiencia Pública por la defensa del patrimonio Cultural
Date: 2017-03-27 09:32
Category: Nacional
Tags: Congreso, Cultura, teatro
Slug: teatro-audiencia-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/743b79b9-d4cf-4517-88e9-4c24c42be092.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div id="js_1pl" class="_5pbx userContent" data-ft="{&quot;tn&quot;:&quot;K&quot;}">

###### Foto: Contagio Radio 

###### 27 Mar 2017 

En el [[[\#]{._58cl ._5afz}[DíaMundialDelTeatro]{._58cm}]{._5afx}](https://www.facebook.com/hashtag/d%C3%ADamundialdelteatro?source=feed_text){._58cn} , artistas agrupados en el Movimiento 27 de Marzo se encuentran en el Congreso de la República, en Audiencia Pública por la defensa del patrimonio Cultural

</div>

<div class="_3x-2">

</div>

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F10154305215945812%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
