Title: Votar por los Acuerdos, no por Santos
Date: 2016-07-21 08:43
Category: Camilo, Opinion
Tags: Plebiscito, proceso de paz, Santos
Slug: votar-por-los-acuerdos-no-por-santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/santos-y-el-si.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: SIG 

#### **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [@CamilodCasas](https://twitter.com/CamilodCasas)** 

###### 20 Jul 2016 

“Al perro no lo capan dos veces”, dice el conocimiento popular. La segunda vuelta fue implícitamente la legitimación a Santos para que ejerciera un gobierno para pactar un Acuerdo con la guerrilla de las FARC EP y con el ELN.

Ahora se tratara de asegurar garantías políticas con el voto por el sí a lo que se acuerde en La Habana. La suerte está echada y hay que motivar para dimensionar lo que se va a decidir con el voto, mucho más cuando las FARC EP asumió en el último acuerdo que asumiría el mecanismo que adoptara la Corte Constitucional.

El mecanismo de refrendación aprobado por la Corte Constitucional es el plebiscito; y esto a pesar de las objeciones presentas por escrito y en las audiencias públicas por diversas personas y organizaciones. Todas ellas coincidían en indicar que el referendo era el mecanismo más adecuado para la consulta a una decisión social sobre los acuerdos, otros más moderados plantearon,  la modulación. Al final se impuso el criterio del gobierno con o sin cabildeo y o mermelada.

Uno de los lunares que debe asumir desde ya la Corte Constitucional para sumar a la deslegitimación del aparato de justicia y jurisdiccional en Colombia es la reducción del umbral al 13 %, que se traduce en 4.286.770 votos, del  25% que se contempla en la ley.  Indudablemente, es este otro logro de Santos en medio de su baja popularidad, pasarse por encima de determinados preceptos que se concebían inmodificables.

La aprobación del plebiscito, de 7 votos contra 2 en la Corte Constitucional,  le podría servir a Santos para relegitimarse ante la pésima gestión de gobierno y ante una fina maquinaria que ha dejado en el partido Liberal y el de la U, los asuntos estratégicos de la implementación coincidiendo todos en sus intereses burocráticos y la consolidación de la paz territorial, que es control social territorial, seguridad para los inversionistas. Un país en el que hace metástasis la corrupción heredada de gobiernos anteriores y su corte plutocrática; la destrucción ambiental por el modelo extractivo asumido y las concesiones carreteables 4G, entre otros;  la crisis agraria y de salud;  la represión a través del monstruoso ESMAD y las nuevas formas de paramilitarismo; así como, los montajes judiciales en los que indudablemente no hay inocencia de sectores del ejecutivo, convierte al plebiscito en el mecanismo legitimador de la llamada Prosperidad Democrática. Santos, el impopular, vuelve a salirse con el vestido a su medida. Efectivamente, el presidente, amante del póker, es un jugador de primera, y lo sigue demostrando.

Pero desde otro lugar, de lo que se trata, en medio de las tensiones uribistas santistas, es de la aprobación de los Acuerdos a los que se lleguen en La Habana, no de la gestión de Santos. En este sentido, hay aprender del pasado reciente. Votar por Santos en la segunda vuelta, no era asegurar la paz, si la continuidad de unas conversaciones. Ahora se trata de asegurar que lo acordado, que aún está sin terminar,  sea compromiso del gobierno. Se trata del primer paso para asegurar lo que el gobierno debería implementar de los acuerdos. De ahí que el apoyo al Sí es para empezar a construir el proyecto de país que nos merecemos, para que la paz sea de verdad y no un remedo de papel.

Así las cosas una segunda precisión clave. El plebiscito es una oportunidad para la sensibilización conciente y la construcción del poder conciente, de los contenidos de los acuerdos y  de sus límites,  y de su interacción con los planes y proyectos de vida de las comunidades, y de todos los actores sociales que creen que es necesaria otra democracia. Es lo más importante, mucho más, cuando es menor el peso de voto rural al urbano, de los que han padecido la guerra a los que habitan en ciudades como Bogotá, Cali, Medellín y Barranquilla, quiénes son los que definitivamente eligen. El plebiscito rural es importante solo en perspectiva de sensibilidad conciente de las apuestas comunitarias, locales y regionales. El plebiscito rural los votos que eligen son el pulso Santos y Uribe, y para la otra perspectiva la articulación rural urbano

Y la tercera clave, como lo ha demostrado la experiencia. Tenemos grandes decisiones constitucionales que no se cumplen. ¿Santos incumplirá? Esa es la tradición. Si se trata de refrendar lo acordado y que los gobiernos cumplan, ya sabemos qué es lo que sucede. Lograr que Santos  cumpla con los asuntos del punto de víctimas como la Comisión de Esclarecimiento de la Verdad, la Jurisdicción Especial de Paz, los familiares de los desaparecidos, y algunos otros sobre la Unidad de Investigación sobre paramilitarismo, y algunos  asuntos sociales y de participación, será el logro y el reto del movimiento social. El conjunto de la paz, la paz de verdad,  seguirán siendo en su conjunto parte de la lucha social.

De ahí que este es un momento histórico, más que por Santos e incluso más allá de  las propias FARC EP, pero abierto por su voluntad política,  para crear otro bloque de poder, distinto, cualitativamente novedoso para otra democracia. Es la oportunidad del ELN, para aportar a este proceso, rompiendo el negacionismo santista con creatividad.

El plebiscito es un tiempo de rupturas, las propias, las del movimiento social tradicional, la de la izquierda armada y en transición a la izquierda sin armas, para el primer momento por excelencia de la sensibilización y la formación consciente, en que se cimienta la paz con justicia socio económica y ambiental. Allí se empieza a visualizar esa democracia, una propuesta metodológica de ejercicio de poder consciente distinta y del programa de gobierno de la auténtica paz, que no es Santos, ni su sucesor Vargas Lleras. “Al perro no lo capan dos veces”, estamos en una oportunidad de más para avanzar en la democracia profunda.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
