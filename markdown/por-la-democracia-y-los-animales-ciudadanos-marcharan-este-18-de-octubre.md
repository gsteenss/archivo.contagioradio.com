Title: Por la democracia y los animales ciudadanos marcharán este 18 de octubre
Date: 2015-10-16 15:17
Category: Movilización, Voces de la Tierra
Tags: Consejo de Estado, Consulta Antitaurina, crueldad animal, defensa a animales, Estocada a la democracia, Maltrato animal, marcha 18 de octubre, Plataforma Alto
Slug: por-la-democracia-y-los-animales-ciudadanos-marcharan-este-18-de-octubre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/12006164_10153474606275020_7499046589258384535_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Plataforma ALTO 

<iframe src="http://www.ivoox.com/player_ek_9026851_2_1.html?data=mpWfmJ2ZdY6ZmKiak5aJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNDmjNHOjcnJsdDX08bQy8aPvYzVjNHc1ZDFssrhwtHS1ZDHrdbYwsnO0NTXb87V08jVw9eJh5SZopaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Catalina Reyes, Plataforma ALTO] 

###### [16 oct 2015] 

Por la **"estocada a la democracia"** defensores y defensoras de los animales convocan este 18 de octubre, a una gran marcha en rechazo a la suspensión de la consulta antitaurina por parte del Consejo de Estado.

En el marco de la **marcha mundial por los derechos de los animales** que se realiza el primer fin de semana de octubre cada año, la Plataforma ALTO ha postergado la misma al día 18 de octubre para manifestar su indignación frente a la violencia hacia animales como los toros, pero también para **"decirle al Estado que nos respete como ciudadanos"**, señala Catalina Reyes, integrante de la Plataforma ALTO. (Lea también: [Decisión del Consejo de Estado es una "Estocada a la democracia"](https://archivo.contagioradio.com/decision-del-consejo-de-estado-fue-una-estocada-a-la-democracia/))

Los animalistas realizarán diversos actos simbólicos  a partir de las 10 de la mañana con una marcha desde la Plaza La Santa María hasta la Plaza de Bolívar, para demostrar que la consulta popular vive. **“Estamos invitando a la ciudadanía a que marche por el toro, por indignación y por otros animales violentados"**, asegura Catalina Reyes.

Así mismo, agrega que llevarán símbolos patrios durante el recorrido hacia la Plaza de Bolivar que representarán el duelo en que está el país por la democracia violentada.  **"Tal vez esa democracia que tanto suena en este país no existe para el verdadero Estado que somos los ciudadanos. Habrá una representación de cómo el Estado ha sepultado la democracia”**, afirma Reyes.

Cabe recordar, que la consulta antitaurina era el escenario que daba la posibilidad de que la ciudadanía demostrara si hay o no arraigo cultural mayoritario frente a las corridas de toros en Bogotá. La defensora animalista asegura que la lucha continúa y que la consulta no está muerta, pues desde ya se trabaja desde frentes políticos y jurídicos para que próximamente la ciudadanía pueda decidir sobre este tipo de espectáculos violentos con animales.

[![12087977\_10153767131483267\_1085888188683796668\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/12087977_10153767131483267_1085888188683796668_n.jpg){.aligncenter .size-full .wp-image-15839 width="480" height="480"}](https://archivo.contagioradio.com/por-la-democracia-y-los-animales-ciudadanos-marcharan-este-18-de-octubre/12087977_10153767131483267_1085888188683796668_n/)
