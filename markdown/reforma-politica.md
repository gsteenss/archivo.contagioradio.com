Title: Los 5 puntos claves de la reforma política
Date: 2017-02-16 12:49
Category: Nacional, Política
Tags: acuerdo de paz, ELN, FARC, partidos politicos, Reforma Política
Slug: reforma-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/reforma-política-2017.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: convergencia por la paz] 

###### [16 Feb 2016]

El gobierno propone al congreso una reforma política que contiene 5 puntos claves. Cada uno de ellos con unos puntos a favor y otros en contra. La ampliación del censo electoral para ejercer el derecho al voto a partir de los 16 años, el voto obligatorio por dos periodos electorales, la ampliación del periodo de 4 a 5 años a presidente, gobernadores, alcaldes, procurador y fiscal; y la eliminación de la figura del vicepresidente.

A este respecto consultamos a Ramiro Bejarano, abogado y catedrático y a Alejandra Barrios directora de la Misión de Observación Electoral, MOE, para ver los alances de la propuesta de reforma política y el juego que tendría el gobierno para intentar aprobarla antes de que termine este periodo presidencial y para aprovechar la coyuntura en torno a la implementación del acuerdo de paz entre el gobierno y las FARC.

### **El derecho al voto desde los 16 años de edad** 

Esta medida representaría un aumento del censo electoral en una cifra cercana a los 2 millones de votos. Para Alejandra Barrios de la MOE esta sería una medida que debería estar acompañada de un debate amplio tanto en el congreso como en otros espacios como los colegios y las universidades.

Según Barrios sería muy bueno que el Ministerio de Educación se diera a la tarea de plantear el tema como un eje estructural de las líneas educativas que se están planteando.

Esta medida debería estar acompañada de una serie de discusiones en torno a las nuevas prácticas electorales y la calidad de la democracia, para que el ejercicio electoral realmente robustezca el sistema y no se termine cayendo en una situación que en lugar de mejorar la democracia tengamos una ciudadanía acudiendo a las urnas desinformada o con intereses lejanos a las necesidades del país. ([Lea también: Hay concensos en partidos para un cambio en las reglas electorales](https://archivo.contagioradio.com/cuales-seran-los-nuevos-lineamientos-para-la-participacion-politica-en-el-pos-acuerdo/))

### **Voto obligatorio por dos periodos** 

Ramiro Bejarano manifestó no estar de acuerdo con que a la ciudadanía se le exija ejercer el voto, sin embargo, Barrios manifestó que sería un ejercicio interesante en la medida en que reduce los costos para la venta o compra de votos y establece una práctica que podría contribuir en la reducción del abstencionismo.

Por otra parte estaría pendiente el tema de las sanciones. Barrios señala que para que el voto sea un ejercicio obligatorio tendría que haber un sistema de sanciones robusto, y una capacidad técnica en el Estado que permita aplicar las sanciones.

En ese sentido, si votar es obligatorio, las sanciones deben ser lo suficientemente efectivas para que las personas sientan que realmente su voto va a contar para el país y en caso de no votar significa una afectación directa de sus ejercicios ciudadanos.

### **Eliminar la figura del vicepresidente** 

La MOE no ha establecido una posición al respecto, es un tema que debe ser estudiado con mucho detenimiento, sin embargo si es necesario que la reforma política estén en medio de esta discusión.

Para Bejarano la figura del vicepresidente debería responder más a compatibilidad política que a alianzas electorales. Por eso propone que no eliminar la figura, sino que se establezca como norma que la formula sea conformada por dos personas del mismo partido político y así garantizar que no se repitan situaciones de evidente confrontación como con Angelino Garzón y Germán Vargas en el gobierno actual.

### **Ampliación del periodo de gobierno a 5 años y prohibición de reelección para algunos cargos** 

Bejarano señala no estar de acuerdo puesto que se ha comprobado que 4 años son suficientes tanto para el presidente como para el Fiscal o el Procurador. No sería muy sano para la democracia tener periodos de gobierno o de ejercicio de facultades en el sector público por más de 4 años. ([Le puede interesar Listo el Estatuto de la Oposición](https://archivo.contagioradio.com/estatuto-de-oposicion-saldaria-una-deuda-de-26-anos-con-la-constitucion-de-1991/))

### **Hay tiempo para aprobar la reforma política** 

Aunque el Fast Track para la implementación de los acuerdos de paz debería ocupar la mayor parte de la agenda legislativa en los próximos 6 meses, el músculo con el que cuenta el gobierno no es muy favorable a 1 año de terminar un periodo de 8 en la presidencia y ad portas de una campaña electoral, a esto se le agrega el escándalo de Odebrecht que debilita al gobierno.

Aunque hay tiempo en el calendario legislativo es muy probable que las cosas no avancen al ritmo debido dado que cada congresista y partido estará en función de la campaña y en algunos casos en función de la implementación de los acuerdos de paz. ([Lea tamibíen: el riesgo de para los acuerdos de paz en las elecciones presidenciales](https://archivo.contagioradio.com/el-riesgo-de-los-acuerdos-de-paz-en-elecciones-presidenciales-2018/)).

Otras posturas

El representante a la Cámara Alirio Uribe, coincidió con otros analistas en que señalan que esta propuesta lo que hace es tender una cortina de humo para tapar el escándalo de Odebrecht, puesto que las propuestas y la embergadura de una reforma como esta necesita mucho más respaldo y debería hacerse al inicio del periodo de gobierno y no al finalizarlo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
