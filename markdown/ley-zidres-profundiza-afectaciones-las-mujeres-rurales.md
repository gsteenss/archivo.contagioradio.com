Title: Ley Zidres profundiza afectaciones a mujeres rurales
Date: 2017-05-09 12:52
Category: Mujer, Nacional
Tags: Acceso a la tierra, derecho a la tierra, Ley Zidres, mujeres
Slug: ley-zidres-profundiza-afectaciones-las-mujeres-rurales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Zidres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas] 

###### [09 May. 2017] 

En el marco de la campaña **“Queremos Campo Sin Ley Zidres”, este 9 de mayo, mujeres líderes y defensoras de derechos humanos de la región del Caribe, radicaron mil cartas** en la Corte Constitucional para respaldar una de las demandas presentadas contra la Ley 1776 de 2016 que crea dichas zonas. Le puede interesar:["Radican demanda contra Ley ZIDRES"](https://archivo.contagioradio.com/polo-partido-verde-y-cumbre-agraria-radican-demanda-contra-zidres/)

Esta es tan solo una de las demandas que ha sido radicada en la Corte Constitucional y que se caracteriza por que logró **identificar las afectaciones de género que causarían dichas zonas. **Le puede interesar: [Exequibilidad de Ley ZIDRES atenta contra Banco de Tierras por la Paz](https://archivo.contagioradio.com/exequibilidad-de-ley-zidres-atenta-contra-banco-de-tierras-por-la-paz/)

**Lorena Morales, integrante de la Asociación Colectivo Mujeres Al Derecho,** manifestó que al desarrollarse estas zonas va a haber una mayor cantidad de tierras dedicadas a grandes productos de agroindustria que “le van a seguir restando espacio a la economía campesina y a **la posibilidad que miles de familias y mujeres campesinas puedan acceder a la tierra** que en este momento no tienen”.

Asegura Morales que lo que debería verse ahora, es que cada día sea más el número de población rural accediendo a la cantidad de tierra a la que por ley tiene derecho, sin embargo “lo que vamos a encontrar cuando se implemente la ley, es que en los territorios donde se implementen estas zonas **las hectáreas van a estar disponibles para los proyectos y no para campesinos y campesinas”**.

En ese orden de ideas, las mujeres se verían doblemente afectadas pues en la actualidad su acceso a la tierra es casi nulo “en el contexto rural las mujeres tienen mayores afectaciones, porque tradicionalmente **esto es un país que ha discriminado que las mujeres accedan a la propiedad y en particular a la propiedad rural.** Allá se ven múltiples discriminaciones que van desde lo legal, familiar, social y cultural”. Le puede interesar: ["Ley ZIDRES para acaparadores de tierra afecta territorios colectivos"](https://archivo.contagioradio.com/ley-zidres-para-acaparadores-de-tierras-afecta-territorio-colectivos/)

Así las cosas, en el contexto de la concentración de tierras en **Colombia las mujeres son las mayores afectadas porque son las que menos tierra tienen** “de hecho no son claras las cifras en relación de cuánta cantidad de tierras de manera formal e informal están en manos de las mujeres en Colombia, y con la implementación de las Ley Zidres esa realidad se perpetuará” manifiesta la integrante de la Asociación.

Añade Morales, que lo que han podido analizar es que el Estado está obligado a promover acciones afirmativas que velen por que las situaciones de discriminación o brecha que existe con las mujeres rurales se transforme o mejore.

**“Lo que vemos con esta Ley de Zidres es que no plantea ninguna acción afirmativa para las mujeres,** por el contrario, deja en mano de los empresarios la posibilidad de delimitar la cantidad de tierra que quedaría siendo propiedad de las mujeres” puntualiza Morales. Le puede interesar: [Solo el 24% de las mujeres rurales pueden tomar decisiones sobre sus tierras](https://archivo.contagioradio.com/solo-el-24-de-las-mujeres-rurales-pueden-tomar-decisiones-sobre-sus-tierras/)

Las organizaciones sociales y de mujeres del país y en este caso de la región Caribe, han planteado que esta es una ley regresiva, dado que en ella **el Estado no plantea ninguna solución para que la realidad de exclusión de las mujeres a la propiedad se transforme**, y además, porque las escasas acciones planteadas se dejan en manos de particulares.

Además de la entrega de las cartas, la Asociación Colectivo Mujeres Al Derecho continuará realizando actividades en pro de la negativa a la Ley Zidres “las mil cartas están firmadas por organizaciones sociales y diversos sectores de la región Caribe que están respaldando las pretensiones de **la demanda en la que decimos que es inconstitucional” relata Morales.**

En redes sociales están impulsando el apoyo de la sociedad civil y de las organizaciones, a estas iniciativas de rechazo a la Ley Zidres con los hashtags \#QueremosCampoSinLeyZidres, \#CorteNoLeFalleALasMujeresCampesinas \#CorteNoLeFalleALasRegiones. Le puede interesar: [Mujeres rurales trabajan más que los hombres y ganan menos](https://archivo.contagioradio.com/mujeres-rurales-trabajan-mas-que-los-hombres-y-ganan-menos/)

<iframe id="audio_18587109" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18587109_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
