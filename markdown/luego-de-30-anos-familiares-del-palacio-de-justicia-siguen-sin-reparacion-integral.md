Title: Luego de 30 años familiares del Palacio de Justicia siguen sin reparación integral
Date: 2017-07-05 15:39
Category: DDHH, Nacional
Tags: CIDH, Palacio de Justicia, pilar navarrete
Slug: luego-de-30-anos-familiares-del-palacio-de-justicia-siguen-sin-reparacion-integral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/palacio-de-justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [05 Jul 2017] 

Las víctimas del Palacio de Justicia denunciaron que el **Estado Colombiano continúa incumpliendo las medidas de reparación, salud e indemnización a las que estas tienen derecho**, e incluso singue sin hacer efectivas las disposiciones de la Corte Interamericana de Derechos Humanos.

Pilar Navarrete, esposa de Héctor Jaime Beltrán, desaparecido en la retoma del Palacio de Justicia, manifestó que, si bien es cierto que el Estado ha cumplido con las peticiones y actos de reconocimiento de responsabilidad, **sus acciones han sido mínimas frente a la garantía de los derechos humanos de los familiares de las víctimas del Palacio**, obligando a muchos a interponer tutelas para acceder al derecho a la salud.

“Lo principal que es la salud, que es inmediato por el estado en el que se encuentran muchos de los familiares, tanto mental como física, se ordenó inmediatamente y hasta este momento ni el Ministerio de Salud, **ni el Estado han creado un protocolo para que las personas sean atendidas**” afirmó Navarrete. Además, agregó que, en lo corrido de estos más de 20 años, los familiares de las víctimas no han recibido acompañamiento psicosocial por parte del Estado

### **La indemnización económica a las víctimas** 

Navarrete expresó que “ha sido sumamente revictimizante y doloroso” esto debido a que el fallo de la Corte Interamericana de Derecho Humanos había estipulado que cada persona familiar de un desaparecido tenía derecho a recibir un monto de 100 mil dólares, sin embargo, **el Estado interpreto que los 100 mil dólares era el monto para reparar en conjunto a todas las víctimas**.

Finalmente, **el Estado otorgó a cada persona 2 millones de pesos**, lo cual no alcanzaría a cubrir ni las deudas adquiridas. Frente a este hecho las familias decidieron continuar con el proceso jurídico, razón por la cual a esa deuda que previamente tenía el Estado deben sumársele los intereses por la falta de pago. [(Le puede interesar: "Gobierno incumple reparación en caso del Palacio de Justicia")](https://archivo.contagioradio.com/gobierno-incumple-reparacion-en-el-caso-del-palacio-de-justicia/)

### **Los restos de Jaime Beltrán** 

**El pasado 7 de Junio fueron encontrados los restos de Jaime Beltrán**, quien trabajaba como mesero y fue desaparecido en el Palacio de Justicia y habían sido enterrado como si fueran los del Magistrado auxiliar Julio César Andrade. Este descubrimiento se logró tras la exhumación que pidió la hija de Andrade.

Pilar Navarrete, esposa de Beltrán **expresó que el proceso de entrega de los restos se realizará el próximo 18 de septiembre**, fecha en la que Jaime cumpliría 60 años, se realizará un acto protocolario y la misa en el Colegio San Bartolomé. ([Le puede interesar: "31 años después del Holocausto del Palacio de Justicia entregan restos de Luz Mary Portela"](https://archivo.contagioradio.com/entregaran-restos-de-luz-mary-portela/))

“Cuando yo me enteró que fue la familia Andrade, que también fue engañada por el Estado, a la que le entregan un cadáver, unos huesos calcinados con una cédula y un pedazo de tela y **le dicen es su papá yo sentí alegría porque es una familia que cuido y protegió el cuerpo de mi esposo**” afirmó Navarrete.

Este 5 de Julio frente a la Cancillería los familiares de las víctimas del Palacio de Justicia harán un plantón en donde expondrán estas denuncias y la falta de cumplimiento por parte del Estado. ([Le puede interesar: "No entregan restos de víctimas del Placio de Justicia hallados en 2015"](https://archivo.contagioradio.com/9-meses-despues-no-entregan-restos-de-victimas-del-palacio-de-justicia-hallados-en-2015/))

<iframe id="audio_19647255" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19647255_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
