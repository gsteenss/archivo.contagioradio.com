Title: Red de comunidades insta a la verdad en 2017
Date: 2017-01-02 18:21
Category: Nacional, Paz
Tags: Carta Abierta, Conpaz, FARC, Juan Manuel Santos, paz
Slug: red-de-comunidades-insta-a-la-verdad-34234
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/La-Esperanza-CONPAZ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [2 Enero 2017] 

Como "una afrenta" calificaron** **las comunidades que son parte de CONPAZ (Comunidades construyendo paz en los territorios),  **la sustracción, en el punto de víctimas, en lo que respecta a la responsabilidad de mando como lo expresa el artículo 28, del Acuerdo Final con las FARC EP.**

En una carta abierta las comunidades de CONPAZ, **como víctimas de la guerra manifestaron además que esperan poder "restaurarse como seres humanos" y por ello instan al Gobierno Nacional en cabeza del actual presidente no espere ordenes judiciales nacionales o internacionales para pedir perdón.**

Y añaden que "el Gobierno Nacional asuma su responsabilidad en diversos casos" así como lo ha hecho la guerrilla de las FARC-EP. Le puede interesar: [Líderes de CONPAZ son víctimas de amenazas y seguimientos](https://archivo.contagioradio.com/lideres-de-conpaz-son-victimas-de-amenazas-y-seguimientos/)

En la misiva las comunidades aseguran que el proceso de paz firmado con las FARC-EP,les ha permitido **"intentar celebrar con menos zozobra, con menos angustia"** en sus territorios.** **Le puede interesar: [CONPAZ exige voluntad política del gobierno para implementación de Acuerdo](https://archivo.contagioradio.com/conpaz-exige-voluntad-politica-para-implementacion-de-acuerdos/)

De igual manera, dicen que se encuentran en espera de que **el próximo 10 de enero pueda iniciarse la mesa de conversaciones con la guerrilla del ELN y "se adopten definiciones con el EPL".**

Por último, hacen una invitación a todas y todos los colombianos, a la solidaridad nacional e internacional, a las empresas, a las iglesias, a militares libres y encarcelados, a la clase dirigente, a **"dimensionar la importancia de la verdad en el derecho restaurador para lograr plenas libertades y expresiones hacia un camino de justicia socio ambiental, tributaria, económica y de género".**

**Dicha carta abierta estuvo dirigida **al presidente de Colombia Juan Manuel Santos, a las FARC-EP, al ELN, al EPL y a los actores privados dada a conocer en días pasados.

Leer carta abierta aquí: <http://bit.ly/2hLaPYq>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
