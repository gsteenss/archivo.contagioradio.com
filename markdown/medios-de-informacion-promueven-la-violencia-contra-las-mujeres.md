Title: Medios de información promueven la violencia contra las mujeres
Date: 2015-09-08 17:13
Category: El mundo, Mujer
Tags: Bolivia, derechos de las mujeres, Machismo, medios de comunicación, patriarcado, periodismo con visión de género, Violencia contra las mujeres
Slug: medios-de-informacion-promueven-la-violencia-contra-las-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/violencia-contra-la-mujer.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [elchacoinforma.com]

De acuerdo a una investigación en Bolivia, la manera cómo los medios informan sobre la violencia contra las mujeres, “**constituye uno de los elementos más limitantes para avanzar en los derechos e igualdad de las mujeres, puesto que perpetúa creencias y prácticas que generan distintos tipos de violencias”.**

Se trata del estudio realizado por Alianza por la Solidaridad, en Bolivia. Allí  se analizó la forma como ocho medios de comunicación de los municipios de Pucarani, El Alto, Quillacollo y Arque cubren los hechos alrededor de la violencia hacia las mujeres.

En el estudio, se asegura que en los últimos años las noticias aunque el número de noticias sobre este tema ha aumentado, lo cierto es que **sólo una quinta parte de las noticias habla sobre acciones de prevención.** Por ejemplo, el abuso sexual es la que más se visibiliza con un 33%, sin embargo, no se informa sobre otras formas de violencia como lo es el acoso, la violencia económica o psicológica.

“La celosa acosaba” y “Ebrio violento masacró a su mujer”, son algunos de los calificativos que usan los periodistas **para referirse a hechos violentos contra las mujeres, lo que genera una cultura de tolerancia y “excusas”**, para los hombres que ejercen violencia contra las niñas y las mujeres, lo que a su vez refuerza estereotipos machistas, como lo explica la investigación.

Finalmente, se concluye que la escasa profundización y análisis de los hechos de violencia cometidos contra mujeres; la invisibilización de las otras formas y manifestaciones de la violencia, la escasa voluntad para indagar en otras fuentes de información que no sean solo la policía o la fiscalía, la reproducción y estereotipos, contribuyen a perpetuar la subordinación de las mujeres al varón.
