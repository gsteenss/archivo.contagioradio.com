Title: Otra mirada del feminicidio y la violencia contra la mujer
Date: 2020-07-01 23:30
Author: AdminContagio
Category: Actualidad, Mujer
Tags: feminicidio, feminismo, Rosa Elvira Cely, Violencia contra la mujer, vivas nos queremos
Slug: otra-mirada-del-feminicidio-y-la-violencia-contra-la-mujer
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Feminicidio.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/GAB3316.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: MDZ

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las desoladoras y crecientes cifras de feminicidio y violencia contra la mujer, las cuales se han recrudecido en medio del aislamiento obligatorio, en el que han sido asesinadas al menos [113 mujeres desde el inicio de la cuarentena](http://observatoriofeminicidioscolombia.org/index.php/seguimiento/noticias/428-vivas-nos-queremos-dossier-de-feminicidios-en-cuarentena) según registros del Observatorio de Feminicidios en Colombia, han puesto el foco sobre esta problemática social.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La violencia de la que son víctimas las mujeres, se manifiesta tanto en zonas rurales, donde por ejemplo se tuvo noticia de los indignantes **casos de feminicidio en contra de Alba Lucia Caro y Sandra Patricia Martínez perpetrados por sus exparejas**; como en zonas urbanas, donde por ejemplo en Bogotá, según información de la Secretaria de Seguridad del Distrito, **los asesinatos contra mujeres aumentaron en un 8,6% en relación con el año anterior.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### **[La violencia contra la mujer en los territorios rurales]{.has-inline-color .has-very-dark-gray-color}** {#la-violencia-contra-la-mujer-en-los-territorios-rurales .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Jani Silva, lideresa social y representante de la Zona de Reserva Campesina Perla Amazónica  (ZRCPA) es un vivo ejemplo de las mujeres que se han visto sometidas a padecer la violencia en su contra. Jani, ha sido amenazada en varias ocasiones por el liderazgo que ejerce en el Putumayo, viéndose obligada a desplazarse de la zona rural, al área urbana de su territorio, por miedo a que grupos ilegales atenten contra su vida y materialicen las amenazas que han realizado contra ella. (Lea también: [Lideresa Jani Silva en riesgo tras descubrirse plan para atentar contra su vida](https://archivo.contagioradio.com/lideresa-jani-silva-en-riesgo-tras-descubrirse-plan-para-atentar-contra-su-vida/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> algo que se ha podido rescatar de la pandemia es la colaboración y la solidaridad que se ha visto entre mujeres para su surgimiento, desarrollo mutuo y para el posicionamiento del rol de la mujer en los territorios.
>
> <cite>Jani Silva. Lideresa social del Putumayo</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Jani afirma que **«la situación de persecución en contra de las lideresas es crítica y hay una falta de garantías para  poder desempeñar nuestro trabajo»**. No obstante, señala que existen esfuerzos desde las comunidades para integrar a las mujeres en actividades productivas y que algo que se ha podido rescatar de la pandemia es la colaboración y la solidaridad que se ha visto entre mujeres para su surgimiento, desarrollo mutuo y para el posicionamiento del rol de la mujer en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Jani resalta también la labor que se ha adelantado para involucrar a los hombres con miras a desarraigar un machismo que está muy instalado especialmente en algunas zonas rurales por cuenta de tradiciones e imaginarios equivocados de género. (Le puede interesar: [¿La justicia restaurativa puede contrarrestar la violencia de género?](https://archivo.contagioradio.com/justicia-restaurativa-puede-contrarrestar-violencia-genero/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Nosotras hemos obtenido resultados cuando invitamos a nuestros encuentros y reuniones a toda la familia incluidos los hombres. Allí se pueden dar cuenta que en estos encuentros no se habla en contra de ellos, sino del valor que tiene la mujer y el respeto que debe tener el uno con el otro dentro del hogar»**
>
> <cite>Jani Silva, lideresa social y representante de ZRCPA</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### **[La violencia contra la mujer en las ciudades]{.has-inline-color .has-very-dark-gray-color}** {#la-violencia-contra-la-mujer-en-las-ciudades .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la ciudad, dos miradas diversas de la misma violencia en contra de la mujer, dos mujeres que sufrieron de distintas formas este flagelo. Yaneth Suárez, sobreviviente de un intento de feminicidio y Adriana Arandia Cely, activista y hermana de Rosa Elvira Cely.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Yaneth Suarez tras ser víctima de un intento de feminicidio por parte de su ex yerno, quien también maltrataba física y psicológicamente a su hija, abanderó la causa de la defensa de los derechos de la mujer, primero abogando por su hija ante todas las instancias jurídicas e institucionales posibles y posteriormente, por todas las mujeres a quienes hoy asesora sobre las rutas de atención y las denuncias que pueden emprender para salvaguardar su vida, integridad y derechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adriana Arandia Cely por su parte, quien sufrió de primera mano el dolor y el duelo que conllevó el crimen infame de su hermana y que hoy por hoy también está dedicada a la lucha para evitar que casos como el de Rosa Elvira Cely se lleguen a repetir.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las dos coinciden en señalar que fueron los hechos desafortunados de las que fueron víctimas las que las motivaron a emprender esa lucha para proteger a las mujeres.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Jamás crees que una violencia te pueda llegar a afectar a ti. Tristemente hasta que no la vives, no entiendes que existe y que es real**»****
>
> <cite>Yaneth Suárez, sobreviviente de intento de feminicidio</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

También concuerdan en afirmar que no  se pueden «justificar las violencias» y menos aún revictimizar a las mujeres responsabilizándolas o culpándolas de las agresiones en contra de ellas mismas.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **A las mujeres nos culpan por el hecho de ser mujeres y siempre se quieren justificar las violencias en nuestra contra**
>
> <cite>Adriana Arandia Cely, activista y hna. de Rosa Elvira Cely</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Adriana Cely recalcó que lamentablemente la ley de feminicidio que se expidió luego del asesinato de su hermana «no está siendo efectiva y se quedó en el papel». Razón por la cual ella y otras mujeres han decidido «denunciar a los funcionarios que imponen barreras para el acceso a la justicia». (Le puede interesar: [En Colombia no hay avances para detener la violencia contra las mujeres](https://archivo.contagioradio.com/en-colombia-no-hay-avances-para-detener-la-violencia-contra-las-mujeres/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, ambas insisten en que es importante la labor de pedagogía y orientación que se hace desde algunas organizaciones y colectivos feministas para educar a las mujeres sobre las herramientas y mecanismos que tienen a su alcance para proteger sus derechos y el hecho de tejer «redes de apoyo» que propicien un acompañamiento y un soporte para las víctimas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/473187150189431/?v=473187150189431","type":"video","providerNameSlug":"facebook","align":"center","className":""} -->

<figure class="wp-block-embed-facebook aligncenter wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/473187150189431/?v=473187150189431

</div>

<figcaption>
[Otra mirada: [\#VivasNosQueremos](https://www.facebook.com/hashtag/vivasnosqueremos?source=feed_text&epa=HASHTAG)]{.has-inline-color .has-cyan-bluish-gray-color}

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
