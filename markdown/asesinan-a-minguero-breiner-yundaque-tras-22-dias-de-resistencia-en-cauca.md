Title: Asesinan a minguero Deiner Yunda tras 22 días de resistencia en Cauca
Date: 2019-04-02 14:50
Author: ambiente y sociedad
Category: Comunidad, Nacional
Tags: Asesinato de campesino en San José de Apartadó, Minga en cauca, Minga Nacional
Slug: asesinan-a-minguero-breiner-yundaque-tras-22-dias-de-resistencia-en-cauca
Status: published

###### [Foto: CRIC] 

###### [2 Abr 2019] 

Desde la Comisión de Derechos Humanos del Consejo Regional Indígena del Cauca (CRIC) se dio a conocer el fallecimiento del **comunero Deiner Yunda Camayo** como consecuencia de un impacto de bala en medio de la arremetida de la Fuerza Pública contra los mingueros concentrados en El Cairo, Cajibío, en el marco de la Minga del suroccidente del país.

Breiner Yunda fue herido con arma de fuego a la altura del torax al costado derecho de su cuerpo y aunque fue trasladado al hospital de Piendamó, la gravedad de su herida le ocasionó la muerte; en el mismo hospital se encuentra el comunero Walter Flores quien en medio de la misma confrontación resultó herido en un brazo.

Según información preliminar, el minguero, quien pertenecía al Resguardo Jebala del municipio de Totoró  se encontraba en una caseta comunal **a tan solo 300 metros de la vía Panamericana, donde desde hace 22 días se encuentran la Minga** de los pueblos indígenas, cuando el proyectil fue disparado desde un cafetal.

Breiner, de 20 años, era sobrino del actual gobernador del Resguardo de Jevalá, además de fungir como comunero al interior de su resguardo era conocido por sus aptitudes para el deporte.

A raíz de la muerte del minguero fue levantada la mesa de diálogo entre la Minga y el Gobierno, sin avances en las negociaciones  y las continuas arremetidas del ESMAD contra las comunidades que permanecen en paro desde hace tres semanas, las víctimas mortales ascienden a dos: el comunero Deiner Yunda Camayo y el patrullero Boris Benítez. [(Le puede interesar: Así habría sido la muerte de ocho indígenas según la Minga en Dagua, Valle del Cauca)](https://archivo.contagioradio.com/nuestras-autoridades-llegaran-al-fondo-de-la-verdad-afirman-indigenas-en-dagua/)

<iframe id="audio_34097389" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34097389_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
