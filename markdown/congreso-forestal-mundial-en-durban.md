Title: ¿Bosques y futuro sostenible?
Date: 2015-09-22 11:59
Category: CENSAT, Opinion
Tags: Ambientalistas colombia, Los bosques y la gente, monocultivos en colombia, Recursos Forestales Mundiales 2015, rganización de Naciones Unidas para la Alimentación y la Agricultura, XIV Congreso Forestal Mundial en Durbán
Slug: congreso-forestal-mundial-en-durban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Plantacionesmodificado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Por Censat - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva)** 

###### [22 de Sep 2015]

XIV Congreso Forestal Mundial en Durbán

[El pasado 11 de septiembre terminó el] [XIV] [**Congreso Forestal Mundial en Durban** – Sudáfrica,] [y tuvo como] [tema central]*[ Los bosques y la gente: invertir en un futuro sostenible.]* [A pesar de contar con ejes temáticos] [sobre] ['bosques' y mitigación del cambio climático,] [provisión energética mundial, y] [agricultura para la erradicación del hambre, los graves problemas e impactos negativos provocados por el “modelo forestal”, y que beneficia principalmente a un pequeño grupo de corporaciones forestales, pasaron desapercibidos en el Congreso.]

[Un] [principal] [asunto] [cuestionado por las organizaciones de la sociedad civil y movimientos presentes en Durban consistió en que para la Organización de Naciones Unidas para la Alimentación y la Agricultura,] [FAO] [por sus siglas en inglés,] [los monocultivos de árboles se incluyen en la categoría de 'bosques', incluidos los de árboles transgénicos. Es decir, la FAO tan sólo se limita a testificar la presencia de árboles, y permite que las inmensas extensiones de monocultivos de pino, eucalipto y teca, entre otras, sean integradas a la noción de bosque. De hecho, la FAO hizo público en Durban su reciente]*[ Evaluación de los Recursos Forestales Mundiales 2015,]*[ donde indica que las tasas netas de deforestación en el mundo se han reducido, esto gracias a que la superficie de “bosque plantado”, o en otras palabras monocultivos forestales ha aumentado en más de 110 millones de hectáreas desde 1990.]

[Los]*[ soldados plantados]*[ (así llaman los mapuches al monocultivo forestal porque son verdes, se encuentran en fila y avanzan contundentemente) o] [desiertos verdes (como han sido caracterizados por los ambientalistas) tienen un rápido crecimiento en países ecuatoriales y tropicales como Colombia, dadas las condiciones de alta radiación solar, suelos fértiles, y alta disponibilidad de agua, y son justamente la pérdida de fuentes hídricas y fertilidad unos de los principales impactos de las plantaciones a gran escala.]

[En Colombia como en el resto del continente, los monocultivos de pino, eucalipto y teca, entre otros se extienden en inmensas áreas, especialmente en el Cauca, Norte del Valle, Eje Cafetero, Antioquia,  Alto Magdalena y Orinoquia. Aunque los proyectos se han expandido considerablemente en los últimos años, debido a políticas públicas que han subsidiado el cultivo y el desarrollo de las plantaciones forestales, no se tienen datos actualizados del área que éstas ocupan y existen pocos estudios oficiales o académicos que den cuenta de las afectaciones sobre las fuentes hídricas y la biodiversidad. El país no cuenta con un inventario forestal que permita dimensionar qué empresas, dónde y cómo se están expandiendo. Precisamente, en el reciente Estudio Nacional del Agua brilla por su ausencia un análisis detallado sobre las afectaciones a las aguas de estas plantaciones forestales en el país. Asunto que un próximo estudio debería abordar.]

[Las cifras sobre el consumo de agua del monocultivo forestal son escandalosas, un sólo árbol de eucalipto consume alrededor de 20 litros de agua diarios, es decir, que si cada hectárea contiene en promedio 1000 árboles, esto implica un consumo diario de 20.000 litros por hectárea/día.]

[Ahora bien, advirtiendo] [una profunda crisis de agua en] [diversas regiones del país, y] [que según datos del IDEAM se] [extenderá por algunos meses más,] [las autoridades anuncian racionamientos radicales de agua en más de 300 municipios colombianos. En este contexto, nos preguntamos si la meta 20/20 de]*[ deforestación cero]*[ para Colombia se cumplirá con la implantación de más y más monocultivos aprovechando el aval conceptual de la FAO, o por el contrario sería necesario emprender el camino de recuperar y proteger las cuencas abastecedoras de agua de toda amenaza, entre ellas, la de las plantaciones.]

[En Durbán, mientras el elitista Congreso debatía sobre las formas de expansión forestal, cientos de activistas de todo el mundo se movilizaron para denunciar el fomento a la expansión de monocultivos de especies exóticas, especialmente en el sur global, y las implicaciones sobre las comunidades locales y los territorios. Por eso, las organizaciones sostienen como consigna que “las plantaciones no son bosques”, idea que cobró mayor fuerza] [el pasado 21 de septiembre, día mundial contra los monocultivos de árboles, donde organizaciones y comunidades en Colombia como en el resto del mundo se movilizaron para visibilizar los conflictos y luchas territoriales frente a estas plantaciones. Es hora de actuar ¡Apoyémosles!]
