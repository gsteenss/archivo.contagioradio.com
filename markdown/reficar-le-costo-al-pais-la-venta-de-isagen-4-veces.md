Title: Reficar le costó a Colombia la venta de ISAGEN 4 veces
Date: 2016-02-02 21:42
Category: Economía, Nacional
Tags: Ecopetrol, ISAGEN, reficar
Slug: reficar-le-costo-al-pais-la-venta-de-isagen-4-veces
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Reficar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Región Caribe 

###### 2 Feb 2016

De acuerdo con la Contraloría la Refinería de Cartagena (Reficar) es hasta el momento el proyecto que más dinero le ha costado a Colombia. **Entre 2006 y 2015 sus costos de ejecución incrementaron en 101%,** pasó de un presupuesto inicial de 3.993 millones de dólares a un gasto total de 8.016 millones de dólares. Sobrecostos que presuntamente no contaron con la debida justificación y que representan un detrimento patrimonial de 4 veces la cifra en la que fue vendida la generadora de energía ISAGEN y 1,15 veces lo que costaría el Metro para Bogotá.

Entre los factores que llevaron al sobrecosto de Reficar se destaca el aumento del 43% en la cantidad de materiales de construcción, 15% en el incremento de los precios de los mismos, 25% por demora en la entrada de operación que se tenía prevista para 2010, y 13% por el descenso en la productividad, estimada hace cinco años en 24% y actualmente fijada en 4,3%. **Un total de 3.593 millones de dólares en pérdidas, es decir, más de 1,69 billones de pesos.**

Estos sobrecostos no se presentaron únicamente durante la ejecución de las obras, pues fue la misma Contraloría la que alertó a Ecopetrol que Glencore no era el mejor socio estratégico para un proyecto de tal envergadura. Pese a las advertencias en 2007 la petrolera colombiana recibió de esta multinacional 630 millones de dólares como pago por la participación del 51% en la actividad de la refinería, que luego en 2009 debió volver a comprar a Glencore ante su negativa de continuar con el proyecto.

La salida de Glencore implicó una mayor inversión no sólo por la recompra de la parte que había adquirido sino porque la multinacional dejó amarrados los contratos de ejecución con **la compañía estadounidense CB&I, a la que Ecopetrol terminó pagando una cifra superior a los 6,5 billones de pesos.** Esta decisión de Glencore también llevó a que Ecopetrol modificara en cuatro oportunidades el valor de la obra que terminó costando 4.023 millones de dólares más del presupuesto fijado inicialmente.

En los 1.846 contratos suscritos en pesos, Ecopetrol terminó gastando 35 mil millones más de los 7,3 billones contemplados al inicio de las contrataciones, que tuvieron un total de 516 adiciones. De 60 contratos firmados en dólares, la petrolera colombiana gastó 142 millones más de los 112 millones fijados inicialmente, en razón de las 127 adiciones presentadas. Del total de estas contrataciones 17% demandaron adiciones de más del 100% y según denunció la Contraloría se concentraron en un reducido número de empresas entre las que se destacan CB&I y Foster Wheeler.

Por estos descomunales sobrecostos que no necesariamente representaron beneficios para las obras de modernización y ampliación de la Refinería de Cartagena, **desde el año pasado la Procuraduría investiga a 12 directivos y exdirectivos por su grado de responsabilidad en este detrimento patrimonial,** entre ellos Reyes Reinoso, presidente de Reficar; Javier Gutiérrez, expresidente de Ecopetrol; Álvaro Echeverry, exvicepresidente jurídico de Ecopetrol; Orlando Diaz, gerente de la refinería de Barrancabermeja; Federico Maya, exvicepresidente de refinación; Luis Sanabria, gerente de refinados; Pedro Rosales exrepresentante legal suplente y Adriana Echeverry, gerente de Ecopetrol América.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
