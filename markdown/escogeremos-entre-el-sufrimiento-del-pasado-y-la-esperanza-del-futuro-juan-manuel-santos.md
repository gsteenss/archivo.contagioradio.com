Title: "Escogeremos entre el sufrimiento del pasado y la esperanza del futuro": Juan Manuel Santos
Date: 2016-09-27 17:08
Category: Nacional, Paz
Tags: Conversaciones de paz con las FARC, Diálogos Gobierno - FARC, FARC
Slug: escogeremos-entre-el-sufrimiento-del-pasado-y-la-esperanza-del-futuro-juan-manuel-santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/CtXJMyMUsAA5FoK.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Delegación de Paz FARC-EP ] 

###### [27 Sept 2016 ] 

"Hemos vivido, hemos sufrido, por 52 años, **un conflicto armado entre hijos de una misma nación**" aseguró el presidente Juan Manuel Santos frente a los 2.500 asistentes a la firma del acuerdo de paz este lunes en Cartagena, cuando toda Colombia junto a la comunidad internacional saludó este hecho "como la mejor noticia en medio de [[un mundo convulsionado por la guerra](https://archivo.contagioradio.com/palestina-su-lucha-es-tambien-la-nuestra-kwara-kekana-lider-sudafricana/)], los conflictos, la intolerancia y el terrorismo".

El mandatario aseguró que lo que se pactó entre la guerrilla de las FARC-EP y el Gobierno colombiano es un no rotundo a la guerra que dejó cientos de [[miles de muertos, millones de víctimas y desplazados](https://archivo.contagioradio.com/1-641-dias-de-dialogos-para-poner-fin-a-52-anos-de-guerra-en-colombia/)]; a la violencia como medio para defender las ideas y a la intolerancia que ha exigido doblegar o excluir al otro por el solo hecho de pensar diferente. Un NO MÁS por los **jóvenes, soldados, policías, campesinos y guerrilleros muertos y mutilados en una guerra absurda**.

"Nadie como yo, desde el Ministerio de Defensa y la Presidencia de la República, los combatió y los golpeó tanto, cuando la dinámica de la guerra lo exigió. Yo, que fui su implacable adversario, **reconozco que fueron dignos negociadores en la mesa de conversaciones**, y que trabajaron con seriedad y voluntad, sin las cuales hubiera sido imposible llegar a este momento", continuó.

El presidente colombiano hizo referencia a las diferencias que tiene el Gobierno con la guerrilla de las FARC-EP frente al modelo político o económico que debe seguir el país y aseguró que no por ello dejará de "**defender con toda la determinación su derecho a expresar sus ideas** dentro del régimen democrático, porque esa es la esencia de la libertad dentro de un Estado de derecho".

Según lo expresado por el mandatario el acuerdo pactado en La Habana hará más efectiva la lucha del Estado contra el narcotráfico y ayudará a sustituir miles de hectáreas de coca por cultivos legales, de la mano de las comunidades, "un acuerdo que tendrá dividendos muy positivos en la lucha por la protección del medio ambiente y de los recursos naturales", y que por primera vez en la historia crea un completo [[sistema de justicia transicional](https://archivo.contagioradio.com/entrevista-especial-justicia-transicional-puede-estar-lista-pero-necesita-espacio-politico-ictj/)], en el que **los crímenes de lesa humanidad no son amnistiados, sino investigados, juzgados y sancionados**.

"La paz de Colombia **es la paz de la región y de todo el continente**", afirmó Santos y envío agradecimientos especiales a las Naciones Unidas, a su Secretario General Ban Ki-moon y al Consejo de Seguridad, también a Cuba y Noruega como países garantes y a Chile y Venezuela como acompañantes. El reconocimiento también fue dirigido al Comité Internacional de la Cruz Roja, a Estados Unidos, a la Unión Europea, a Alemania y a los países de América Latina, del Caribe y de todo el mundo.

El jefe de Estado concluyó su discurso invitando a los colombianos a que acudan a las urnas porque cada uno de los votos "tendrá [[UN PODER INMENSO](https://archivo.contagioradio.com/colombia-le-dice-al-mundo-que-enfrentar-el-miedo-y-movilizarse-si-paga/)]: el poder de salvar vidas; **el poder de dejarles a sus hijos un país tranquilo donde crezcan sin miedo**; el poder de ayudar a los campesinos despojados a que regresen al campo; el poder de atraer más inversión al país y, por consiguiente, más empleo".

<iframe id="audio_13084051" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13084051_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]

 
