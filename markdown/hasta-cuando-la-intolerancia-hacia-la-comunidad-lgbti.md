Title: ¿Hasta cuándo la intolerancia hacia la comunidad LGBTI?
Date: 2015-04-17 08:19
Author: CtgAdm
Category: Escritora, Opinion
Tags: adopcion gay, Centros Comerciales que discriminan, Comunidad LGBTI, Discriminación, Escritora encubierta
Slug: hasta-cuando-la-intolerancia-hacia-la-comunidad-lgbti
Status: published

###### Foto: larazon.net 

**Por [[Escritora Encuebierta ](https://archivo.contagioradio.com/escritora-encubierta/)]**

María y Ana (1), dos chicas lesbianas, se encontraban en un parque de atracciones muy reconocido y acreditado del país. La pareja se besaba, se abrazaba, andaba tomada de la mano: cosas que cualquier pareja haría. El problema es que no cualquier pareja es hostigada por demostrar su afecto en público.

Como era de esperarse, un empleado no tardó en acercarse a las chicas vociferando que tenían que *controlarse* porque *había niños presentes* y lo que hacían era una *falta de respeto*. La pareja por supuesto no se quedó callada, conocían sus derechos y estaban dispuestas a hacerlos cumplirse. Sin embargo, el empleado no estaba a dispuesto escuchar una sola palabra por parte ellas, cegado por su ignorancia solamente repetía que era una *falta de respeto*, acompañando su frase con gestos despectivos y burlescos. La pareja fue persistente, no abandonó el lugar porque sabía que era **ilegal** obligarlas a irse por eso.

La situación antes mencionada es el pan de cada día para los miembros de la comunidad LGBTI, de hecho, me atrevo a afirmar que el 80% de nosotros ha escuchado alguna vez estas mismas frases. Y es que, a pesar de que en el 2011 la Corte Constitucional falló a favor de una pareja homosexual masculina que impuso una tutela al haber sido forzada a abandonar un centro comercial por besarse y estableció que las parejas homosexuales pueden besarse en público (2), este acto discriminatorio sigue sucediendo una y otra vez.

La discriminación no viene sólo por parte de empleados, vigilantes e incluso policías, sino también por los mismos clientes que denuncian este *comportamiento inmoral*. Esto probablemente se deba a que para ellos es más fácil pretender vetar a dos personas del **derecho a la dignidad humana y el libre desarrollo de la personalidad** que tener una larga charla con sus hijos explicándoles la diversidad sexual.

Sin embargo, los homosexuales no tenemos por qué pagar por este tabú de las familias colombianas conservadoras a hablar sobre la diversidad con sus hijos. No tenemos por qué escondernos para demostrarle afecto a la persona que amamos. No tenemos por qué soportar este tipo de comentarios ofensivos. No tenemos por qué sobrellevar el escarnio público. La única *falta de respeto* que podemos estar cometiendo al ser homosexuales es permitir que nos pisoteen nuestros derechos.

Por esto, invito a todos los homosexuales a que no tengan miedo de tomar la mano de su pareja en la calle, a que no piensen dos veces antes de demostrarle cariño sólo porque están en un lugar público. Según la Corte Constitucional ''besarse de modo romántico con la pareja, sea o no homosexual, hace parte de los espacios de libertad individual que toda persona natural posee a la luz de su dignidad para vivir como se quiere, para su libre desarrollo personal y para el derecho a no ser molestado en esa elección específica que sólo a él o ella interesa''. Además, este derecho de libertad no puede ser restringido ''ni por un centro comercial en sus estatutos, ni una empresa de vigilancia''.

Y a todos los heterosexuales, los invito a que no tengan miedo de tocar esos temas tabúes con sus hijos. De todas formas, aunque lo eviten, en algún momento esos niños se verán enfrentados a este tipo de situaciones y es importante que haya una buena educación previa. Cabe destacar que hablar sobre la diversidad sexual no hará que sus hijos se *conviertan* en homosexuales, simplemente hará que se conviertan en personas más tolerantes y respetuosas.

Finalmente, para todos: ***''Los países libres son aquellos en los que son respetados los derechos del hombre y donde las leyes, por consiguiente, son justas''. Maximilien Robespierre.***

##### *(1)** **Los nombres han sido cambiados para proteger la identidad de las víctimas.* 

##### *(2) Sentencia T-909/11 http://www.corteconstitucional.gov.co/relatoria/2011/t-909-11.htm* 

[[@[OneMoreHippie]
