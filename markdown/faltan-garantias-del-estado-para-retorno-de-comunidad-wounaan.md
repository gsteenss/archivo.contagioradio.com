Title: Faltan garantías del Estado para retorno de Comunidad Wounaan
Date: 2019-08-26 18:08
Author: CtgAdm
Category: DDHH, Nacional
Tags: comunidad Wounaan, Pichimá, Quebrada
Slug: faltan-garantias-del-estado-para-retorno-de-comunidad-wounaan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-25-at-8.19.42-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

La comunidad indígena Wounaan del Resguardo de Pichimá Quebrada, que cumple 100 días de desplazamiento forzado en el municipio de Docordó, afirmó que ya tiene listo su plan de retorno para regresar a su territorio. Sin embargo, aún hace falta lo más importante, **las garantías por parte del Estado  y la Fuerza Pública frente a derechos como la seguridad, la salud y la vida.**

Durante este fin de semana, una misión de verificación hizo presencia en los 3 albergues en los que se encuentran las más de 95 familias Wounaan, en donde se logró comprobar que siguen empeorando las condiciones de habitabilidad en estos lugares. Desde la **ausencia de alimentos, hasta la falta de agua potable son algunas de las dificultades** que niños, mujeres, jóvenes  y ancianos deben afrontar.

Guillermo Peña, docente de la comunidad manifestó que a estos hechos se suma la falta de enfoque diferencial con el que las comunidades indígenas deben ser atendidas, razón por la cual, algunos alimentos han enfermado a las personas. [(Le puede interesar: "50 días de desplazamiento del Resguardo Pichimá, Quebrada")](https://archivo.contagioradio.com/50-dias-de-desplazamiento-del-resguardo-pichima-quebrada/)

Asimismo, los integrantes de esta comunidad siguen sin recibir atención médica oportuna, actualmente hay dos adultos  y un menor de edad que presentan afectaciones a su salud. Cabe recordad que **otro menor ya fue víctima de la falta de asistencia en salud y falleció el pasado 23 de junio.**

Las mujeres también mencionaron que es urgente que los habitantes del resguardo tengan atención sicosocial, ya que muchas personas presentan traumas luego de haber estado en medio de un enfrentamiento armado.

### **Hace falta la labor de las instituciones para retorno de comunidad** 

Uno de los objetivos de la misión de verificación, era constatar el estado del Resguardo indígena de Pichimá Quebrada. Por este motivo, las organizaciones defensoras de derechos humanos: Comisión Intereclesial de Justicia y Paz, Las Brigadas Internacionales de Paz, el Colectivo Acción Permanente por la paz y el Diálogo intereclesial por la Paz, junto con un delegado de cada familia Wounaan se dirigieron hasta este lugar, en donde se pudo establecer el impacto del conflicto armado y la falta de condiciones por parte de las instituciones responsables para realizar el retorno.

En el resguardo, aún quedan casquetes de balas, esqueletos de artefactos explosivos conocidos como pipetas y rastros de pólvora**. Elementos que le hacen recordar a los habitantes, que tras la firma del Acuerdo de paz, la guerra no se fue del todo.**

Sin embargo, su plan de retorno ya está listo, en el tienen planeado el regreso al Resguardo  y el gran trabajo que deben realizar, ya que la maleza se ha tragado algunos espacios como la cancha de fútbol, mientras que otras casas se han caído.  Para este hecho solo hace falta que las diferentes autoridades den las garantías de retorno  y por supuesto brinden el acompañamiento que estas familias necesitan.
