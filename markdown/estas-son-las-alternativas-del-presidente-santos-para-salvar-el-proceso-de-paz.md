Title: Estas son las alternativas del Presidente Santos para salvar el proceso de paz
Date: 2017-05-19 16:08
Category: Nacional, Paz
Tags: acuerdos de paz, Angela Maria Robledo, Corte Constitucional
Slug: estas-son-las-alternativas-del-presidente-santos-para-salvar-el-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/CtXJMyMUsAA5FoK.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [19 May 2017] 

Con la decisión de la Corte Constitucional, el Congreso de la República entrará a ser el órgano más importante para mantener o no el espíritu de los acuerdos de paz entre las FARC-EP y el Gobierno Nacional, de acuerdo con la representante a la Cámara Ángela María Robledo, es de **gran importancia que el presidente Santos re conduzca el proceso de Paz escuchando mucho más a las víctimas y ejerciendo su poder**.

Robledo hizo un llamado al gobierno y las instituciones que lo conforman a cumplir con lo acordado y no repetir historias como la de Guadalupe Salcedo o el M19, en donde tras incumplir lo acordado asesinan a los diferentes líderes de ambos movimientos y recalcó que es el Presidente el que “**tiene que tomar con fuerza este ejercicio y tomar el mando del país”**. Le puede interesar: ["Corte Constitucional somete al proceso de paz a más obstáculos"](https://archivo.contagioradio.com/corte-constitucional-somete-al-proceso-de-paz-a-mas-obstaculos/)

### **Las alternativas para el Proceso de Paz** 

Para Robledo las tareas que debe asumir el Congreso en estos momentos **debe tramitar lo que falte, como la ley estatutaria de la Jurisdicción Especial de Justicia y Paz**, la Reforma política y el punto uno de los acuerdos sobre tierras y de otro lado pensar en la construcción de políticas públicas para temas como educación rural, salud, entre otras.

Además expresó que ya se ha avanzado en el pacto político por la paz, desde el Congreso Nacional de Paz que permitirá **"propiciar el mayor consenso" al rededor de la  defensa de los Acuerdos de Paz**. Le puede interesar: ["Corte Constitucional congela Acuerdo de Paz con las FARC: Enrique Santiago"](https://archivo.contagioradio.com/corte-constitucional-congela-acuerdo-de-paz-con-las-farc-enrique-santiago/)

Otro de los sectores que para la representante tendrá que volver y jugar un papel fundamental es la **ciudadanía que tendrá que volver a presionar a las instituciones** para que cumpla lo pactado en el Teatro Colón. Le puede interesar: ["Con movilizaciones y pactos políticos haremos frente a decisión de la Corte: Cepeda"](https://archivo.contagioradio.com/con-movilizaciones-y-pactos-politicos-haremos-frente-a-decision-de-la-corte-ivan-cepeda/)

“Nada ha sido fácil en este proceso, esto ha sido como una carrera de obstáculos, pero en este momento yo **creo que los campamentos en las plazas de todas las ciudades, las movilizaciones y una reconducción del proceso**, se puede revisar que debe ir en leyes y que obedece a la capacidad del presidente y de su mandato de establecer la paz” afirmó la representante.

<iframe id="audio_18792808" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18792808_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
