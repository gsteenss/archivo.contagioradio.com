Title: Autodefensas Gaitanistas hostigan a familias de la vereda Guacamayas, Turbo
Date: 2016-07-19 15:10
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas, Guacamayas Turbo, Restitución de tierras
Slug: autodefensas-gaitanistas-hostigan-a-familias-de-la-vereda-guacamayas-turbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Autodefensas-Gaitanistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [19 Julio 2016] 

"Amanecemos muy preocupados por la situación (...) a la 1:30 de la madrugada entraron siete hombres armados con fusil a la finca Las Delicias de la vereda Las Guacamayas, identificándose como Autodefensas Gaitanistas, le dijeron a los dueños de la finca que les daban ocho días de plazo para desocuparla, **los intimidaron, los amenazaron y les dejaron una vela en la mesa diciéndoles que a los ocho días no respondían por ellos**", asegura Julio Correal, poblador de la zona, quién denuncia el hostigamiento paramilitar que vienen enfrentando, desde hace por lo menos un mes, las familias que retornaron luego de ser desplazadas entre 1996 y 1997.

Hasta hace una semana estuvieron en la vereda algunos miembros del Ejército Nacional, frente a quienes los pobladores denunciaron la identidad de los atacantes; sin embargo, los militares aseguraron que **"estaban maniatados" y que no podían detenerlos porque no habían ordenes de captura en su contra**, agregando además, que la seguridad de la zona le compete a la Policía y no a las Fuerzas Militares. A esta situación se suma el silencio por parte de la Fiscalía, la Gobernación de Antioquía y la Alcaldía de Turbo, quienes [[pese a las denuncias de las comunidades no han velado por la integridad de las familias](https://archivo.contagioradio.com/presuntos-paramilitares-desplazan-a-por-lo-menos-5-familias-en-turbo-antioquia/)] que permanecen en inminente riesgo.

"La situación es crítica porque como hay niños y también adultos mayores. Lo que dicen las familias es que nosotros no nos vamos porque las tierras son nuestras y para dónde nos vamos a ir sí no tenemos para dónde irnos. Lo que exigimos al Estado es que por favor nos ayuden con la protección colectiva, que es lo que le hemos pedido, que estén pendientes de las comunidades de este sector porque el Ejército hace muchos días se fue de la vereda y el teniente me decía que la vereda es muy extensa y que no podían estar. **Es preocupante porque al Ejército por aquí no lo hemos visto, pero los agresores están a 500 metros de dónde están las comunidades**", concluye el poblador.

<iframe src="http://co.ivoox.com/es/player_ej_12274311_2_1.html?data=kpefmZmXdZKhhpywj5WbaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5yncavpzc7cjbHJaaSnhqeg0JCns9PmxsbZh5enb7Hjw9HOxtTWcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
