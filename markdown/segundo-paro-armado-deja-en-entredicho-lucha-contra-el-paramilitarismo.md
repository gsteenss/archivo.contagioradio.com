Title: Segundo paro armado deja en entredicho lucha contra el paramilitarismo
Date: 2016-04-05 10:14
Category: Fernando Q, Opinion
Tags: Autodefensas Gaitanistas, Paramilitarismo, paro armado
Slug: segundo-paro-armado-deja-en-entredicho-lucha-contra-el-paramilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Ce4oH6qUsAAMfci.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Redes sociales 

#### **[Luis Fernando Quijano](https://archivo.contagioradio.com/fernando-quijano/) - [@FdoQuijano](https://twitter.com/FdoQuijano) ** 

###### 5 Abr 2016 

[Terminó el segundo paro armado ordenado por la estructura paramilitar y mafiosa que tiene su epicentro en Antioquia y su bastión principal ubicado en la región de Urabá.]

[De forma adrede hoy no las llamaré Autodefensas Gaitanistas de Colombia, solidarizándome con la posición de la familia de Jorge Eliecer Gaitán, quienes ven cómo los paramilitares mancillan la figura de tan importante luchador colombiano, asesinado por la misma alianza legal e ilegal que fortalece al paramilitarismo que hoy usurpa su apellido. No obstante, son ellos quienes firman así y quienes deberían dar respuesta.]

[Tampoco los llamaré Urabeños por respetar a los habitantes de una hermosa tierra que bastante sangre ha derramado por intereses particulares que poco o nada piensan en su gente, y sólo les importa extraer sus riquezas y explotar su estratégica posición, como la construcción del puerto en Turbo lo demuestra.]

[Menos aún los denominaré banda criminal del Clan Usuga: considero que las bacrim no existen y no creo que una familia mafiosa este controlando uno o varios negocios criminales con poderío suficiente para poner en jaque al país. Esta denominación dada por el gobierno del presidente Santos es errónea, simplista y algo cínica; llamarlos Bacrim del Clan Usuga no borrará la existencia de una estructura paramilitar y mafiosa que, incluso, le hace paros armados, le envía emisarios, y le mandan cartas a La Habana para ser tenida en cuenta en la justicia transicional.]

[El mote dado intenta enterrar de un plumazo un pasado paramilitar; pero los últimos hechos reafirman que ese paramilitarismo está vivo, fortalecido y, además, está presente en varias regiones de la geografía colombiana, tiene alianzas transnacionales con el crimen internacional -caso del Cartel de Sinaloa-, y viene asesinando a defensores de Derechos Humanos en Colombia y a miembros de la fuerza pública para ser reconocidos como organización político militar. Sin embargo, la discusión sobre su nombre la daré otro día.]

[Aclarado lo anterior, me referiré al segundo paro armado paramilitar y desde ya mencionaré la responsabilidad del gobierno de Santos por no tener una estrategia integral que desmonte las estructuras paramafiosas y por derrochar miles de millones de pesos en operaciones como Agamenón, operación que cuenta con oficiales ineptos –o corruptos- frente a la lucha contra los paramilitares y que sólo los desplaza hacía zonas con presencia de las FARC.]

[Los paramilitares -que a veces se presentan como Águilas Negras y es el nombre que podría acomodarse a esta estructura-, en cabeza de alias Otoniel, han promovido dos paros armados ilegales y ambos encontraron un Estado colombiano impróvido, pese a que su ocurrencia fue advertida de antemano.]

[El primero tuvo como justificación la muerte de Juan de Dios Usuga, alias “Geovanny Guerrillo”, dado de baja por la Policía Nacional el 1 de enero de 2012, pero que los paramilitares de Antioquia aseguran fue fusilado en estado de indefensión; este duró 48 horas y se llevó a cabo en varios departamentos donde se paralizó el comercio, la vías quedaron solitarias y la gente se confinó dentro de las casas; afectó a Sucre, Magdalena, Chocó, Córdoba, Antioquia y a su capital, Medellín, territorios que, irónicamente, volvieron a vivir un paro armado, aunque este último fue más violento y dejó como saldo ocho policías y un oficial asesinados, civiles muertos y varios heridos.   ]

[El segundo paro tuvo sus efectos más violentos en el departamento de Antioquia y resonó con potencia en algunas comunas de Medellín, donde los paramilitares mostraron su poder y dejaron en entredicho una estrategia de seguridad que tiene como prioridad negar la presencia paramilitar.]

[Ahora que los paramilitares han mostrado más que sus colmillos, como lo afirmó mediáticamente el ministro de Defensa, Luis Carlos Villegas, ¿qué se debe hacer para desmantelarlos? Los diálogos y la negociación son el camino; el sometimiento a la justicia debe ser la hoja de ruta.]

[Para lograr esto, es necesario implementar una estrategia integral que golpee eficaz y contundentemente su parte económica, militar, territorial y, sobre todo, su  protección oficial y a sus verdaderos jefes que no son la familia Usuga, ellos sólo representan una estructura que va más allá de lo simple delincuencial, son uno de los componentes militares y económicos de la clase emergente.]

------------------------------------------------------------------------

**Píldora urbana**[: Como van las cosas, en cualquier momento los paramilitares de Antioquia, que mantienen el pacto del fusil con la Oficina del Valle de Aburrá, apoyarán e impulsarán a esta última para que realice un paro armado urbano que exija diálogo y negociación, y que busque el reconocimiento político. Tremenda implicación que deja la falta de un Estado competente.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
