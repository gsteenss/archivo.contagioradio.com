Title: Durante gobierno Duque han sido asesinados 97 comuneros indígenas: ONIC
Date: 2019-08-13 10:21
Author: CtgAdm
Category: DDHH, Política
Tags: Amenazas contra indígenas, Cartel de Sinaloa, Cauca, Colombianos en el exterior, Exterminio Indígena
Slug: onic-gobierno-duque-97-indigenas-asesinados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/CRIC-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

El atentado del pasado 10 de agosto en Caloto contra integrantes de la Guardia Indígena, que dejó un saldo de tres comuneros muertos y siete heridos, se suma al total de 15 ataques cometidos contra esta comunidad según la ONIC. Este suceso y los 97 indígenas asesinados durante el gobierno Duque, dan cuenta de la realidad de los pueblos originarios en Colombia que los ha llevado a declarar un estado de emergencia.

Para Aída Quilcué, acompañante del proceso de derechos humanos del CRIC,  se trata de una temática que no solo incumbe a los pueblos indígenas sino a los distintos sectores sociales del país, quienes deben acompañar su ejercicio de gobernabilidad propia, investigar la sistematicidad de los ataques contra sus líderes y revisar problemáticas como el narcotráfico en el departamento.

Quilcué señala que el vacío que queda tras la muerte de los integrantes de la guardia no solo afecta a sus familias sino al movimiento indígena en general, **"nos están tocando en lo más profundo de nuestras cosmovisiones y nuestras raíces"**,  afirma la lideresa al resaltar que el asesinato de guardias y de los mayores espirituales busca desastibilizar a las comunidades. [(Lea también: Plan pistola en marcha contra líderes indígenas: ACIN)](https://archivo.contagioradio.com/plan-pistola-en-marcha-contra-lideres-indigenas/)

Frente a los atentados, advierte que se trata de un problema estructural que se deriva de la importancia estratégica que tiene el territorio para muchos actores, entre estos las multinacionales a las que no solo se les han concedido licencias mineras sino de las que también ha derivado la presencia de grupos residuales que buscan desplazar a las comunidades para que las empresas tengan acceso total al territorio.

### **La resistencia ha sido milenaria y hoy no puede ser la excepción** 

Como respuesta a estos ataques, la integrante del CRIC destaca además que es necesario ofrecer a las comunidades garantías de protección colectiva, propuestas que ya han sido compartidas con el Gobierno y que pese a ello no se han visto materializadas. [(Le puede interesar: Amenazan a líderes del CRIC y ACIN que participan en Minga indígena)](https://archivo.contagioradio.com/amenazan-a-lideres-del-cric-y-acin-que-participan-en-minga-indigena/)

Aunque desde el Gobierno atribuyen estos ataques a disidencias de las Farc, como la columna móvil Dagoberto Ramos, también ha circulado a través de redes sociales un panfleto del **Cartel de Sinaloa, atribuyéndose el atentado del pasado 10 de agosto**. Cabe resaltar que la Guardia indígena junto a la Defensoría del Pueblo viene desde hace un tiempo alertando sobre la presencia de este actor armado en el territorio.

**"Los pueblos originarios viene haciendo control territorial y eso no le gusta a quien tiene interés en nuestros territorios (...)  la resistencia ha sido milenaria y hoy no puede ser la excepción"** concluye Quilcué quien espera que el Estado no se escude en la autonomía de las autoridades indígenas sobre el territorio para no combatir el problema, pues se trata de un grupo que no solo actúa en esa región.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
