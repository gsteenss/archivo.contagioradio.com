Title: Lideresa barrial es víctima de hostigamientos por la Fuerza Pública en Medellín
Date: 2018-06-21 11:31
Category: DDHH, Movilización
Tags: COEUROPA, Fuerza Pública, Medellin, Moravia
Slug: lideresa-barrial-es-victima-de-hostigamientos-por-la-fuerza-publica-en-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/moravia3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Balndex Report] 

###### [20 Jun 2018]

El nodo Antioquia de la Coordinación Colombia Europa Estados Unidos y el Proceso Social de Garantías para la Labor de Líderes/as y Defensores/as de Derechos Humanos en Antioquia denunciaron que la líder comunitaria y defensora de derechos humanos **Vanesa Álvarez, junto con la mesa de concertación del Morro de Moravia, han sido víctimas de hostigamientos por parte de la Fuerza Pública. **

Los hechos se presentaron el pasado 12 de junio, hacia las 4:20 de la tarde, **cuando 2 hombres armados, vestidos de civil y que se identificaron como integrantes de la SIJIN**, llegaron al sector conocido como Morro de Moravia, preguntando por la defensora y la mesa de concertación.

Los habitantes preguntaron a los dos hombres las razones de la búsqueda, sin obtener mayor información. Al día siguiente el mismo hecho se presentó en la comunidad. El nodo Antioquia de la Coordinación Colombia Europa Estados Unidos y el Proceso Social de Garantías para la Labor de Líderes/as y Defensores/as de Derechos Humanos en Antioquia, recordaron que hechos similares **en contra de la defensora ya se habían presentado el pasado 5 de abril de 2017. **

En esa ocasión un hombre y una mujer vestidos de civil, que se movilizaban en una moto de placas RSU 58D exhibieron a diferentes personas de la comunidad identificaciones de la Policía Nacional, preguntaron en diferentes lugares del barrio por la Lideresa. (Le puede interesar:["Denuncian asesinato de líder indígena Arnulfo Catimay"](https://archivo.contagioradio.com/denuncian-asesinato-del-lider-indigena-arnulfo-catimay/))

Ambas organizaciones solicitan a  las autoridades correspondiente aclarar de manera expresa este comportamiento que genera un "escenario de intimidación, miedo, zozobra a la lideresa y a los miembros de la Mesa de Concertación del Morro de Moravia". Asimismo expresaron que es necesario **que se inicien investigaciones al interior tanto de la Policía como de los entes judiciales**.

[ACCIÓN URGENTE](https://www.scribd.com/document/382142882/ACCIO-N-URGENTE#from_embed "View ACCIÓN URGENTE on Scribd") by [Luis Fernando Quijano Moreno](https://es.scribd.com/user/171788311/Luis-Fernando-Quijano-Moreno#from_embed "View Luis Fernando Quijano Moreno's profile on Scribd") on Scribd

<iframe id="doc_10099" class="scribd_iframe_embed" title="ACCIÓN URGENTE" src="https://www.scribd.com/embeds/382142882/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-fRTM5yKoHLGIT5OuSRYA&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en su correo bit.ly/1nvAO4u o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio bit.ly/1ICYhVU
