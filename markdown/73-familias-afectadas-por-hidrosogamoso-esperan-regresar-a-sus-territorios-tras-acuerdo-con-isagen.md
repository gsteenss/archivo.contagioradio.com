Title: 73 familias afectadas por Hidrosogamoso esperan regresar a sus territorios tras acuerdo con ISAGEN
Date: 2015-09-09 13:32
Category: DDHH, Movilización, Nacional
Tags: Acuerdo con el gobierno, Gobernación de Santander, hidrosogamoso, ISAGEN, megaproyectos en colombia, Movilizaciones, Movimiento Ríos Vivos, represa, Santander
Slug: 73-familias-afectadas-por-hidrosogamoso-esperan-regresar-a-sus-territorios-tras-acuerdo-con-isagen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Hidrosogamoso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:veredasogamoso.blogspot.com 

<iframe src="http://www.ivoox.com/player_ek_8243093_2_1.html?data=mZehlZWdd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRe5Sfx8bay9HNpdSfwsvSxdnFqMLnjNXc1JCsrcXm0NjcycbRs9TjjMrg0srWpc%2Bf08rU1MrXpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Juan Pablo SoleR, Ríos Vivos / Ricardo Sánchez Mov. Defensa Río Sogamoso] 

###### [9 Sep 2015] 

Tras 5 meses de permanecer acampando frente a la Gobernación de Santander, **73 familias esperan regresar hoy a sus territorios**, tras la firma de un acta de acuerdos entre la comunidad, ISAGEN y las autoridades, donde se reparen algunos daños sociales que ha generado la realización del proyecto hidroeléctrico [Hidrosogamoso](https://archivo.contagioradio.com/hidrosogamoso-huele-a-podrido/).

Este convenio que tiene la firma de la comunidad, pero aún no cuenta con la de ISAGEN, sería el cuarto acuerdo al que se comprometería la empresa, teniendo en cuenta que los anteriores no los ha cumplido, como lo asegura Ricardo Sánchez, vocero de Movimiento en defensa del río Sogamoso.

A pesar de que el acta de compromisos cubre algunas necesidades de una parte de la población afectada por Hidrosogamoso, la comunidad ha rechazado que ISAGEN no se comprometa con todas las víctimas, es decir con más de **20.000 personas.**

**Así mismo, según** Juan Pablo Soler, integrante del Movimiento Ríos Vivos, otro de los acuerdos que no tiene muy contentos a los afectados es que sólo a 5 familias de las 73 se les entregarán tierras.

En el acuerdo se tiene planteado la realización de proyectos productivos, acceso a la tierra, reubicación, proyectos caprinos y bovinos. Por su parte, los afectados que permanecen frente a la gobernación aseguraron que se irán de allí, una vez el acta tenga la firma de ISAGEN, de no ser así **continuarían frente a la gobernación por tiempo** indefinido.
