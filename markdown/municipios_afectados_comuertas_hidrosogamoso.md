Title: Seis municipios inundados por apertura de las compuertas de Hidrosogamoso
Date: 2017-05-08 19:13
Category: Ambiente, Nacional
Tags: hidrosogamoso, Santander
Slug: municipios_afectados_comuertas_hidrosogamoso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/C_Ym2nCXgAA8KtJ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mocimiento Ríos Vivos 

###### [8 May 2017] 

Cerca de **mil familias han sido afectadas por la apertura de las compuertas de la hidroeléctrica Hidrosogamoso,** de ISAGEN, que el pasado 26 de abril decidió dejar pasar el agua por las precipitaciones, ocasionando graves inundaciones en las zonas aledañas al río.

Son 6 los municipios afectados: **Barrancabermeja, Sabana de Torres**, Betulia Puerto Wilches, Giron y San Vicente. Los habitantes de esos lugares, no han podido dormir debido a la angustia de que en cualquier momento se vuelva a inundar la zona, como lo explica Claudia Ortiz, integrante de  **Movimiento Social en Defensa de los Ríos Sogamoso y Chucurí.**

**“La gente no ha podido retomar sus actividades, como a la pesca artesanal.** La situación sigue angustiante, las personas no están durmiendo, están con temor. Los cultivos se inundaron y los animales se ven afectados”, cuenta Ortiz.

**Fueron más de 1500 metros cúbicos de agua**, los que ISAGEN permitió que pasaran a través de las compuertas. Y aunque las inundaciones han sido evidentes, ni las autoridades regionales, ni locales, y tampoco la empresa han dado algún tipo de asistencia o de respuesta ante la situación de las familias.

“Son **más de mil familias las afectadas, pero los comités de riesgos no se han presentado, y la comunidad se siente sola.** No es justo que jueguen así con la gente en nombre del desarrollo”, señala la defensora de los ríos.

La comunidad, **exige que la Agencia Nacional de Licencias Ambientales se pronuncie sobre este hecho.** Lo único que se conoce es que la empresa decidió abrir las compuertas por la gran cantidad de agua que tenía represada. No obstante, no existe ningún tipo de acompañamiento psicosocial a las comunidades. Además, debido a esta situación, Claudia Ortiz, **agrega que hay presencia de materia orgánica en el embalse que ha generado contaminación**.

Asimismo denuncian que ISAGEN solo ha pasado un parte asegurando que todo está bajo control, y el gobernador de Santander, único que ha dicho es que “la gente debe aprender a convivir con eso, así que no  hay soluciones reales”, afirma Ortiz.

Desde el movimiento ambientalista y defensor de las comunidades, en lo que se está trabajando es en avanzar en toda la documentación suficiente, y en la realización de un monitoreo para estar al pendiente del movimiento del agua.

> Por apertura de compuertas embalse Topocoro HIDROSOGAMOSO, primeras inundaciones en San Luis de Rionegro, Sabana de Torres, Santander [pic.twitter.com/1OMP49VEtg](https://t.co/1OMP49VEtg)
>
> — Wilson Lozano (@wilsonlzano) [3 de mayo de 2017](https://twitter.com/wilsonlzano/status/859850968745332736)

<h6>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio.

</h6>

