Title: El regreso de los Aljure Martínez
Date: 2017-05-04 17:48
Category: Abilio, Opinion
Tags: Aljure, mapiripan, Poligrow
Slug: el-regreso-de-los-aljure-martinez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/retorno-familia-aljure-6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo - Contagio Radio 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 3 May 2017 

[Luego de leer el articulo de [El Espectador sobre la transnacional Poligrow](http://www.elespectador.com/noticias/judicial/multinacional-poligrow-y-nietos-de-exguerrillero-liberal-en-pleito-por-tierras-en-meta-articulo-690329) y la familia Aljure Martinez, reclamantes de esas tierras, en el que se entre leen las operaciones “no santas” de la compañía para adquirir la finca Santa Ana, herencia del guerrillero liberal de los 50 Dumar Aljure, pareciera que se tratara de dos contrincantes en igualdad de condiciones, se habla del  "el clan Aljure" para nombrar  a la familia de víctimas. ]

[Quizá el gran valor que raya con la locura, "la bella locura" como repite tanto el propio William, deja la impresión  de que encarna un poder tal como para  enfrentar una compañía que en nuestras cuentas ha intentado  acaparar en Mapiripán más de 90 mil ha de tierras y que ha contado con respaldo de diversos estamentos de los dos últimos gobiernos. Con su poder ha  logrado dormir el proceso judicial más avanzado  en su contra  por más de un año, luego que la fiscal de conocimiento, en el caso Barandales,  fuera removida en extrañas circunstancias;   la Contraloría de la República documentó las  irregularidades en  adquisición de predios, los indígenas Sikuani probaron que no fueron consultados y que se les borró del mapa por parte del propio gobierno, en favor de la compañía, para hacer creer que no existían en el área de influencia del proyecto palmero; Cormacarena, luego de la incidencia de la comunidad internacional, debió sancionarla  por varios daños ambientales,  y es de público conocimiento el crimen de un trabajador en una de las sedes de la compañía. A pesar de esas y otras evidencias, sigue campante.]

[La imagen  bíblica de David contra Goliat ilustra mejor esta desigual reclamación de derechos que se expresa en la afirmación que un día el director de la compañía transnacional  le hizo a uno de los Aljure Martinez: "no sea bobito, yo tengo cómo dormir el pleito 100 años, y para cada año pongo un abogado".]

[De ésta  saga no conocemos el resultado, si ésta vez Goliat impone su poder o si David logra atravesarse a la voluntad acaparadora del gigante. Lo cierto es que basados en el derecho que les asiste, y confiados en la comunicación púbica en la que la empresa afirmaba que se había retractado de ese negocio, y que no había adelantado gestión alguna para adquirir el derecho real de propiedad, pero sobre todo acompañado de amigas y amigos de la comunidad internacional, las iglesias, periodistas y defensores de derechos humanos,  los Aljure Martinez dieron el paso de regresar a la tierra heredada el pasado 3 de abril.]

En el camino un motorizado con medio camuflado y arma corta en el cinto, se cruzó con la carabana del retorno. Se trataba de uno de los “puntos” que los paramilitares utilizan para el control de la zona. Ya en el lugar, en la Finca Santa Ana, conocida también como La Esmeralda, todo quedó claro. La credibilidad de la empresa se fue desvaneciendo como un castillo de naipes. No era cierto que habían desistido de sus pretensiones con el predio.  Quienes estaban en la casa de Santa Ana eran trabajadores suyos, entre ellos el propio jefe de maquinaria de la transnacional, y al día siguiente un abogado alegó en defensa de la compañía, y exhibió un poder del representante en Colombia de la transnacional. Nada de lo dicho a la comunidad internacional resultó cierto, pues la ocupación  de la transnacional palmera quedó en evidencia (ver  http://noticiasunolaredindependiente.com/2017/04/23/noticias/nietos-del-guerrillero-dumar-aljure-recibieron-sus-titulos-de-tierra/).

[No obstante el nuevo revés  a la buena fe de la empresa, los camping se  fueron armando, el mercado bajando, las reuniones con los ocupantes se fueron dando explicandoles las razones de legalidad y legitimidad que justificaban el retorno.  El lugar de la preparación de las comidas  los fueron conquistando los recién llegados en medio de la tensión por el uso de la leña, “tienen que traerla de lejos” gritó el “encargado”  puesto por Poligrow,  hasta que  hasta que alguien se animó a tomar un tronco cercano y darle el primer hachazo para preparar los alimentos en el patio de la casa.]

[La memoria de Dumar Aljure,  asesinado justo hace 49 años por el ejército, convocó a colonos, ocupantes, familia de presencia esporádica y a los retornantes en una ceremonia religiosa cuya mesa se vistió de ponchos llaneros, sombreros, alpargatas.  Hasta los trabajadores de la compañía se paseaban sorprendidos cerca al altar, a cuyo fondo se divisaban cientos de sacos de cal usados para el cultivo de palma aceitera.  Los colonos visitantes compartieron yuca y algo del chiguiro, los retornantes y sus acompañantes  lentejas y arroz en ese almuerzo compartido.]

[El encuentro religioso como un bálsamo disipó las tensiones de la forzada convivencia y generó las condiciones para planear el recorrido del día siguiente hasta La Esmeralda, casa construida por Dumar Aljure y Ana Felisa Peña, donde crecieron sus hijos, a casi dos horas de la casa a la que se retornó. La veloz puesta del rojizo sol del llano colmó el alma de belleza, en el mismo lugar en que la palma aceitera, traída de otros lados, crece al amparo de Poligrow.]

[Al otro día las vallas con el mensaje “Espacio de paz y Diversidad en Memoria de Dumar Aljure” fueron ubicados en diversos lugares  de la inmensa finca anunciando el ofrecimiento ya hecho por los Aljure Martinez de disponer la parte que les corresponde de esa heredad para compartirla con  víctimas y  ex combatientes de las Farc-Ep en transito a la vida civil, buscando que esa tierra sea repartida entre quienes no tienen dónde reconstruir sus esperanzas, respetando los derechos de los ocupante de buena fe.]

[Aún  confían en  que los compromisos asumidos por el gobierno les garanticen  condiciones para permanecer en esas tierras, como también se avance en los trámites para hacer real su ofrecimiento. Aún no se conocen respuestas. Ojalá  se reconozcan los derechos de la  familia, y con ellos los de quienes podrán disfrutar de esas tierras si el postconflicto pasa de ser consigna a realidad, más allá del desarme,  ante la pretensión acaparadora del poder transnacional.]

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
