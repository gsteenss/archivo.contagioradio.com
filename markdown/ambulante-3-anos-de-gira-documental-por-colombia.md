Title: 5 recomendados de la gira documental Ambulante
Date: 2016-08-24 11:20
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Documental, Gira ambulante Colombia
Slug: ambulante-3-anos-de-gira-documental-por-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/video-colombia-acoge-el-festival-de-cine-ambulante-508512.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  Foto: Ambulante Colombia 

###### 23  Agos 2016 

El festival mexicano de documentales **Ambulante** llega por tercera vez a nuestro país, en una gira que durará 35 días y podrá disfrutarse desde el 23 de agosto en **Bogotá, Medellín, Cali, Barranquilla y Cartagena**, donde culminará el 25 de septiembre.

Para esta edición la imagen y el enfoque del festival “**busca promover con su programación una reflexión acerca de las fronteras**” con producciones sobre la guerra en Siria y la crisis de refugiados en Europa, el conflicto por el mar entre Chile y Bolivia, entre otras.

La gira contará con 225 funciones, 40 al aire libre, 60 documentales y 40 estrenos nacionales, y la presencia de directores invitados como **Luis Ospina**, Pamela Yates, Michal Marczak, Fenton Bailey y Randy Barbato.

El festival fue creado por **Gael García Bernal**, **Diego Luna**, Pablo Cruz y Elena Fortes en México en 2005, con el objetivo de difundir el cine documental **como herramienta de transformación cultural y social**. Puede consultar la programación [Aquí](http://www.ambulante.com.co/2016/web/)

<iframe src="https://www.youtube.com/embed/JzmJHjtop74" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

24 cuadros, el espacio de cine de Contagio Radio, les recomienda **5 producciones imperdibles** de Ambulante Colombia.

**The land of the enlightened - Pieter-Jan De Pue (Bélgica - 2016)**

https://www.youtube.com/watch?v=pxmZum2bPgY

**La Buena Vida Trailer - Jens Schanze (Alemania- 2015)**

https://www.youtube.com/watch?v=\_LsdJEr\_fHs

**WATANI – MY HOMELAND - Marcel Mettelsiefen (Siria/Alemania - 2016)**

https://www.youtube.com/watch?v=NK2JLee2xHk

**Ovarian psycos - Joanna Sokolowski, Kate Trumbull-LaValle (USA - 2016)**

https://www.youtube.com/watch?v=yfysrPNiZ8g

**Ghostland: The View of the Ju'Hoansi - Simon Stadler (USA-2016)**

https://www.youtube.com/watch?v=N-sO\_8ejgcM
