Title: Las dos Colombias
Date: 2019-05-20 16:59
Author: Foro Opina
Category: Opinion
Tags: lideres sociales, paz
Slug: las-dos-colombias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/eurodiputados-lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### \*Por la Fundación Foro Nacional por Colombia 

El país no puede dejar de expresar su voz de rechazo al asesinato de líderes y lideresas y al velo de impunidad que hoy sigue protegiendo a sus autores materiales y, sobre todo, a quienes de manera intencional y sistemática han diseñado y promovido este halo de muerte en varias regiones del país. Según el informe más reciente publicado por Indepaz, al 30 de abril de este año 681 personas, entre líderes sociales, defensores y defensoras de Derechos humanos, han sido asesinados desde 2016, con una tendencia al aumento de víctimas año a año. En lo que va corrido de este, ya han sido asesinados 59, y 982 han recibido amenazas en los últimos doce meses, según lo señala la Defensoría del Pueblo.

¿Qué hay detrás de esta escalada de violencia? ¿Por qué matar a hombres y mujeres cuyo único “pecado” ha sido trabajar por sus comunidades y defender el territorio, la cultura, los recursos naturales y su derecho a una vida digna? No se trata de hechos fortuitos. Hay algo más que, probablemente, no aflora plenamente en el debate público.

Lo que, en efecto, revela este fenómeno es una disputa entre dos modelos, dos visiones opuestas acerca del presente y el futuro del país: de un lado, la Colombia que quiere superar la guerra, los odios, y que anhela un país en paz, más equitativo, basado en la cooperación y respetuoso de la diversidad social y cultural, con mejores oportunidades para todos y todas. Una Colombia en la que nadie pueda ser acallado por sus ideas, ni por su condición social, en la que el Estado garantice a todas las personas por igual el pleno ejercicio de sus derechos.

De otro, la Colombia de los privilegiados, de quienes desde hace dos siglos manejan a su antojo los hilos de la economía y del Estado. La Colombia excluyente, de quienes dicen querer la paz, mientras les conviene la guerra; que hablan de igualdad y de pactos, pero no quieren perder las ventajas que les otorga el monopolio de la riqueza y del poder político; la Colombia de quienes rechazan al diferente y lo declaran enfermo o terrorista; la Colombia de quienes no quieren la verdad, la justicia y la reparación.

Los líderes asesinados y amenazados no han hecho otra cosa que trabajar por una nueva Colombia más justa y en paz. Y eso no es del agrado de una minoría que solo está dispuesta a mantener sus prerrogativas y que ha decidido considerar como enemigos a quienes piensan diferente y hacer uso de la violencia para acabar con sus vidas y amedrentar a quienes siguen en esa lucha.

Que haya visiones diferentes, incluso opuestas, de país es concebible en una democracia, aún en una imperfecta como la nuestra. Lo que no podemos admitir es que esas diferencias tengan que dirimirse por la vía de la violencia. El Estado debe intensificar su tarea de protección a los líderes y lideresas, castigar a los responsables y evitar a toda costa más muertes en el país. Pero tan importante como la acción del Estado es la tarea, que concierne a todos y todas, de deponer los odios y sentarse a dialogar sobre cuál es el país que queremos y cómo lo podemos construir entre todos, sin exclusión.
