Title: Concejo municipal de Salento, Quindío aprueba consulta popular
Date: 2017-12-04 07:55
Category: Ambiente, Nacional
Tags: consulta popular, Salento, Valle del Cocora
Slug: consulta_popular_salento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Valle-de-Cocora.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Salento] 

###### [1 Dic 2017] 

Como lo venían exigiendo los habitantes del municipio de Salento en el departamento de Quindío, el consejo municipal le dio el visto bueno a la posibilidad de que a través de una consulta popular puedan decidir si quieren o no el desarrollo de actividades mineras en su territorio.

"Ayer en el Consejo se debatió la conveniencia de la pregunta y dieron el visto bueno, ahora queda que el Tribunal Administrativo del Quindío haga la revisión pertinente de la pregunta", explica la personera del municipio Yessica Herrera. De acuerdo con ella lo que se busca que es que su territorio sea protegido teniendo e**n cuenta que el 98% del territorio se encuentra sin ningún tipo de protección, mientras que cerca de un 30% ya está titulado para empresas mineras.**

Frente a esa situación la comunidad espera poder proteger riquezas ambientales como el Parque Nacional Natural Los Nevados y el Valle del Cocora, donde se encuentra la palma de cera y donde vive el loro orejiamarillo, que se encuentra en peligro de extinción debido a la tala de y el daño causado a su hábitat. (Le puede interesar: [Árbol nacional en peligro por actividades mineras)](https://archivo.contagioradio.com/mineria-en-salento-quindio-amenaza-el-arbol-nacional-de-colombia/)

Asimismo la personera señala que las actividades mineras podrían conllevar graves consecuencias para la comunidades, ya que **Salento provee de agua a Circasia en un 70%, también a Armenia, Calarcá, La Tebaida, y se surte así mismo**, es decir, es un productor hídrico del departamento del Quindío.

No obstante, Herrera manifiesta que no se trata de "una lucha contra las empresas, sino que hay una obligación del Estado de proteger los territorios teniendo en cuenta a las comunidades" y en esa línea, señala que blindarán su derecho a decidir sobre su territorio, teniendo en cuenta "los avances de la jurisprudencia que han sido claros y en los que se muestra que hay que respetar la voluntad de las comunidades".

<iframe id="audio_22404692" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22404692_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
