Title: Así fue la marcha del orgullo gay en Bogotá
Date: 2015-06-28 21:00
Category: Fotografia, LGBTI, Nacional
Tags: 2105, gay, lgtbi, marcha, Movilización, orgullo
Slug: galeria-de-fotos-el-orgullo-gay-marcha-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/MG_9364-e1475069278488.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [28 jun 2015] 

Así se vivió el 28 de junio en Bogotá, durante la XX marcha de Orgullo Gay. Ver [El domingo 28 de Junio se realizará la XX marcha de la ciudadanía plena en Bogotá](https://archivo.contagioradio.com/xx-marcha-de-la-ciudadania-plena-en-bogota/)

\

###### [Fotos: Carmela María] 
