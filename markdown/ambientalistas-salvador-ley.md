Title: Proponen ley para proteger ambientalistas en el Salvador
Date: 2017-02-01 13:11
Category: DDHH, Otra Mirada
Tags: DDHH, Defensores, Mineria, Salvador
Slug: ambientalistas-salvador-ley
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/miningMineria-600x350.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nodal 

###### 1 Feb 2017 

Ante las continuas **amenazas y el asesinato de cuatro líderes ambientalistas** opositores al proyecto minero El Dorado, ubicado en el Departamento Cabañas, de El Salvador, s**e presentará ante la Asamblea de ese país una propuesta de ley para garantizar su protección por parte del Estado**.

La iniciativa promovida por Tutela Legal, busca **crear una ley de defensores de Derechos Humanos, la realización de un protocolo verdadero para su protección y un manual de atención a las víctimas**, quienes padecen además los señalamientos por parte de algunos diputados que los acusan de defender delincuentes.

Ovidio Mauricio, integrante de Tutela, aseguró a la Radio Ysuca que **es necesario contar con el apoyo de los cabildantes** por que "los únicos que tienen iniciativa de ley son los diputados". Los homicidios registrados contra los activistas, ocurrieron **entre los años 2009 y 2011**. Le puede interesar: [Alarma en América latina por asesinatos de líderes ambientalistas](https://archivo.contagioradio.com/alarma-america-latina-asesinatos-lideres-ambientalistas/).

Tutela Legal, es una oficina para la protección y defensa de los derechos humanos en El Salvador, comprometida a **defender, denunciar e investigar cualquier violación a los derechos humanos en el país centroamericano**.
