Title: Los siete informes sobre crímenes del Estado que presentarán a la JEP
Date: 2019-04-11 23:18
Author: CtgAdm
Category: DDHH
Slug: los-siete-informes-sobre-crimenes-del-estado-que-presentaran-a-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-60.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

Organizaciones de derechos humanos presentarán entre mayo y junio de este año siete informes que señalan a las Fuerzas Militares en diferentes violaciones de derechos humanos. Estos informes agrupan **210 casos **de ejecuciones extrajudiciales, abusos de organismos de inteligencia del Ejército y crímenes de paramilitarismo, en que estarían comprometidos agentes del Estado.

Según las organizaciones, la entrega de estos informes al Sistema Integral de Verdad, Justicia, Reparación y Garantías de No Repetición (SIVJRNR) "busca demostrar la **sistematicidad y las prácticas generalizadas, basadas en órdenes desde los altos mandos el Ejército Nacional** y expresadas en acciones y formas de operar que no fueron aisladas, sino que obedecieron a una política de Estado y que en su gran mayoría **permanecen en la impunidad en la justicia ordinaria**".

La mayoría de los informes examinan casos de ejecuciones extrajudiciales, cometidos por distinctos batallones en Arauca, Antioquia, Sur de Bolívar, Cesar y la Guajira, que implicaría a generales retirados y altos mandos de la actual Estado Mayor del Ejército. Además, investiga presuntos crímenes del Ejército en los departamentos del Meta y Guaviare y casos de violaciones de derechos humanos llevadas a cabo por integrantes de estructuras de inteligencia por más de 20 años. (Le puede interesar: "[Presentan a la JEP informe que vincula a comandante de la cúpula militar con falsos positivos](https://archivo.contagioradio.com/jep-adolfo-leon-hernandez-martinez/)")

A diferencia de las investigaciones de la Fiscalia, estos informes independientes analizan los casos de manera conjunta que los permite tener en cuenta el contexto en que ocurrieron los hechos, encontrar las relaciones entre diferentes casos y identificar si existía un plan criminal  y como se desarrollaba dentro de la Fuerza Pública.

Según Liliana Uribe, coordinadora de la articulación de organizaciones, ellos esperan que con la entrega de estos informes la Jurisidicción Especial para la Paz (JEP) pueda abrir casos nuevos sobre algunos de los crímenes investigados que aún no ha sido priorizado por la justicial transicional, como hechos de inteligencia o aquellos ocurridos en el Meta, Arauca y Guaviare. También, instan a los señalados de comparecer ante la justicia transicional para rendir su versión voluntaria.

Las organizaciones que desde el 2017 investigan estos casos son la **Asociación Minga**,  el **Colectivo de Abogados "José Alvear Restrepo"**, el **Colectivo Sociojurídico Orlando Fals Borda**, el **Comité de Solidaridad con los Presos Políticos**, la **Comisión Colombiana de Juristas**, la **Comisión Intereclesial de Justicia y Paz**, la **Corporación Jurídica Libertad**, la **Corporación Jurídica Yira Castro y** **Humanidad Vigente**, que cuentan con el respaldo de la **Coordinación Colombia Europa Estados Unidos** y el **Movimiento de Víctimas de Crímenes de Estado**.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
