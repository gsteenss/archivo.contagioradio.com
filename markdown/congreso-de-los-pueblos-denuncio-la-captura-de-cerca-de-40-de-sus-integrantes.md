Title: Congreso de los Pueblos denunció la captura de cerca de 40 de sus integrantes
Date: 2018-06-15 18:15
Category: DDHH, Movilización
Tags: congreso de los pueblos, Fiscalía General de la Nación, Julian Gil
Slug: congreso-de-los-pueblos-denuncio-la-captura-de-cerca-de-40-de-sus-integrantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Foto-Congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Jun 2018] 

Continúan las denuncias de la organización Congreso de los Pueblos frente a la persecución política de la que han sido víctimas sus integrantes y en específico de las acciones emprendidas desde la Fiscalía contra Julian Gil, integrante de esta plataforma.

El secretario técnico y miembro de la Comisión Internacional de Congreso de los pueblos, Julián Andrés Gil es **sindicado de participar en un hecho ocurrido el 20 de Julio en el que dos hombres se enfrentaron a la policía** en el municipio de Vianí, Cundinamarca cuyo resultado fue un uniformado herido, la incautación de armas de fuego y material explosivo.

Según Fabián Laverde, miembro del área jurídica del Congreso de los Pueblos ¨los delitos por los que Andrés Gil está capturado son tentativa de homicidio agravado, porte ilegal de armas y receptación¨. (Le puede interesar:["Continúan las capturas contra integrantes de Congreso de los Pueblos"](https://archivo.contagioradio.com/continuan-las-capturas-contra-integrantes-de-congresos-de-los-pueblos/))

### **Los errores en el procedimiento judicial contra Julian Gil** 

El primer error que señala Laverde en el procedimiento de captura de Julián Gil, es la presentación de un único testigo quien afirma que Julián participó en los hechos ocurridos en el municipio de Viani. Por esa razón, para Laverde, **no hay pruebas suficientes que señalen la participación** del Secretario Técnico del Congreso de los Pueblos en los hechos mencionados.

Otra irregularidad en el caso fue el procedimiento de captura: pues se realizó por parte de personas que no estaban debidamente identificadas como miembros de la policía, y el traslado de Julián se realizó en un vehículo de uso particular. Pese a que la captura se efectuó en Bogotá, Julián fue trasladado a Viani y posteriormente a la estación de policía de Facatativá, Cundinamarca después de que el Juez de control de garantías decretó medida privativa de la libertad.

Además, el ente que está investigando el caso de Julián es la dirección de investigación de la policía (SIJIN), hecho que para Laverde **no brinda las garantías necesarias al proceso de Julián pues es la misma Policía la que está investigando hechos que la involucran**.

También señala que aunque Julián fue imputado por el delito de porte de armas y receptación: en el momento de su captura no le fue incautado ningún armamento y no se explica a que se refieren con el segundo delito.

### **Congreso de los Pueblos vivió en menos de un año la captura de cerca de 40 de sus integrantes** 

Para Fabian Laverde, la captura de Julián se enmarca en una persecución judicial al movimiento social pues Congreso de los Pueblos vivió en menos de un año la captura de cerca de 40 de sus integrantes: En marzo del año pasado fueron capturados 13 líderes del Congreso de los Pueblos en el Sur de Bolívar señalados de ser integrantes del ELN; y en Marzo del presente año fueron capturadas **33 personas en el suroccidente del país de las cuales 24 ya fueron dejadas en libertad por falta de pruebas**.

Para el caso de Julián Andrés Gil, sus abogados defensores tanto como los investigadores judiciales del Congreso de los Pueblos estudian las pruebas que lo señalan de tentativa de homicidio buscando demostrar que Julián no ha realizado nunca acciones criminales, así como señalar la falsedad del testimonio presentado por el ente acusador. (Le puede interesar:["Por persecución judicial se presentan líderes de Congreso de los Pueblos ante la Fiscalía"](https://archivo.contagioradio.com/persecucion-judicial-congreso-de-los-pueblos/))

<iframe id="audio_26561938" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26561938_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
