Title: Reclutamiento y desplazamiento forzado amenazan a pobladores del río Atrato
Date: 2019-04-15 22:35
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Defensoría del Pueblo, Ejercito de Liberación Nacional, Río Atrato
Slug: reclutamiento-y-desplazamiento-forzado-amenazan-a-pobladores-del-rio-atrato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/rio-atrato.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Organizaciones de derechos humanos emitieron una nueva alerta temprana por la crisis humanitaria y social que viven **las poblaciones afrodescendientes e indígenas de los ríos Opogadó, Napipí, Bojayá y Atrato, en el departamento del Chocó**, a raíz del recrudecimiento del conflicto armado y la falta de protección por parte de la Fuerza Pública.

Según un habitante de la región, la disputa territorial entre el Ejército de Liberación Nacional (ELN) y las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) ha significado un incremento en la siembre de minas antipersonales, el reclutamiento forzado de menores, desplazamiento forzado, el asesinato de líderes sociales, casos de violencia basada en género y la restricción de la movilidad, especialmente en los resguardos indígenas.

En consecuencia, numerosas familias de los municipios de Carillo, Unión Cuití, Unión Baquiaza, Mesopotamia y Pogue se han visto forzadas a desplazarse. Mientras que más de 7.000 personas de Bojayá se encuentran confinadas en ese municipio, donde se teme que las condiciones se están dando para que los hechos que conllevaron a  la masacre de Bojayá en 2002 se vuelvan a repetir. (Le puede interesar: "[Comunidades denuncias asesinato de jóvenes en Riosucio, Chocó](https://archivo.contagioradio.com/comunidades-denuncian-asesinato-jovenes-riosucio-choco/)")

Otras alertas tempranas similares se han emitido desde organizaciones de derechos humanos y la Defensoría del Pueblo durante los últimos dos años por cuenta de la llegada de nuevos actores armados a la región y la ineficacia de la Fuerza Pública. Como lo señala el poblador de la región, la presencia del Ejército no ha significado un mejoramiento en las condiciones de seguridad dado que "no son muchos para cubrir cada metro del Chocó".

Por lo tanto, el Consejo Comunitario Mayor de la Asociación Campesina Integral del Atrato, la Mesa de Diálogo y Concertación de los Pueblos Indígenas del Chocó, el Foro Interétnico Solidaridad Chocó, la Diócesis de Quibdó, Pacipaz, y la Coordinación Regional del Pacifico Colombiano instan al Estado colombiano a realizar **las acciones necesarias para evitar que la situación empeore** y a **cumplir con sus obligaciones constitucionales dentro de las normas del Estado Social de Derecho**.

A las autoridades locales y departamentales, estas organizaciones recomiendan hacer **más presencia administrativa en las comunidades**, apoyar **las iniciativas comunitarias de construcción de paz desde el territorio** y realizar integralmente, los **Comités de Justicia Transicional que les corresponde**.

A los actores armados, piden adicionalmente la **observación del Derecho Internacional Humanitario**, **no instalarse en medio de las comunidades,** **no utilizar a la población civil como escudo humano**, **respetar el principio de distinción** y **reconocer el rechazo absoluto de la sociedad civil**.

<iframe id="audio_34519160" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34519160_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
