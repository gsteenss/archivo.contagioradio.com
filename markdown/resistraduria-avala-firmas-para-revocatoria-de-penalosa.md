Title: "La revocatoria es en las calles": Comité Unidos Revocamos a Peñalosa
Date: 2017-10-27 15:14
Category: Movilización, Nacional
Tags: Bogotá, Enrique Peñalosa, revocatoria, Revocatoria Enrique Peñalosa, Unidos revocamos a peñalosa
Slug: resistraduria-avala-firmas-para-revocatoria-de-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/revocatoria-e1509130563662.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [27 Oct 2017] 

A pesar del aval de la Registraduría a 458.935 firmas recogidas para la revocatoria del alcalde de Bogotá, Enrique Peñalosa, el Consejo Nacional Electoral **deberá validar la contabilidad de los comités** promotores del mecanismo. Sin embargo el CNE se ha escudado en que no hay un plazo fijado por la ley y no ha emitido sentencia, lo que significa una dilación injustificada para los integrantes del Comité de revocatoria.

Según Carlos Carrillo, la excusa del CNE es que las cuentas no están claras, en ese caso lo que procede sería un fallo en contra que anule la revocatoria, sin embargo no hay una decisión porque no hay argumentos para fallar en contra de una revocatoria de las más baratas de la historia. Carrillo asegura que si el CNE emite un fallo adverso los magistrados saben que estarían prevaricando.

El vocero del comité agrega que no se pudieron violar los topes puesto que solamente se gastaron 110 millones de pesos y el mayor aportante fue Sintrateléfonos con una suma de 40 millones y el tope máximo es de 41 millones. Además señala que el comité AZUL, que defiende los intereses del alcalde se ha negado a mostrar sus cuentas que sobrepasarían cualquier suma pues se han contratado grafólogos para revisar las firmas y ese es un servicio costoso.

### **No habrá revocatoria hasta que el CNE se pronuncie** 

La siguiente etapa del proceso está en las manos del Consejo Nacional Electoral, quien deberá dar el aval para la revocatoria una vez revise la contabilidad de los comités. Sin embargo, Carrillo aseguró que **“el CNE es el órgano más politiquero** y corrupto des diseño institucional colombiano y siguen secuestrando la democracia”. (Le puede interesar: ["Alcalde Peñalosa es irresponsable y no respeta a la Corte Constitucional": Recicladores"](https://archivo.contagioradio.com/alcalde-penalosa-es-irresponsable-y-no-respeta-a-la-corte-constitucional-recicladores/))

Dijo que en el proceso ha habido un vacío jurídico en la medida que la Registraduría le ha impuesto al CNE unos tiempos para desarrollar las labores, “pero el consejo, con todo el descaro, **dice que a ellos la ley no les da tiempos**”. Esto es interpretado por el Comité como una “tomadura de pelo a la ciudadanía, escudándose en el tema de la revisión del costo de unas fotocopias y unas empanadas”.

Con esto en mente, Carrillo manifestó que “si las cuentas son incorrectas, **¿por qué no han fallado** y llevan tres meses revisando una contabilidad de apenas 110 millones de pesos?” A lo que respondió que, cuando haya un acto administrativo, “nosotros podemos tomar las medidas legales a las que haya lugar y los magistrados se meterían en problema”.

Además, Carrillo fue enfático en que **ya no hay ningún mecanismo que haga falta** para completar la revocatoria a parte de la decisión del CNE que, “es el único que se interpone entre los bogotanos y la democracia”. (Le puede interesar: ["Ante intentos de frenar la revocatoria de Peñalosa, los bogotanos debemos salir a la calle"](https://archivo.contagioradio.com/ante-intentos-de-frenar-la-revocatoria-de-penalosa-los-bogotanos-debemos-salir-a-la-calle-carillo/))

### **Es difícil saber cuándo será el proceso de revocatoria** 

Carrillo manifestó que aún es difícil determinar si la revocatoria se va a llevar a cabo este año. Dijo que **las medidas legales que faltan toman tiempo** y “el Consejo Nacional Electoral, hará todo lo posible por salvar a sus jefes”. Afirmó que si la revocatoria se lleva a cabo antes de la mitad del próximo año, tendría que haber elecciones, de lo contrario, el presidente de la República tendría que nombrar un alcalde encargado.

Finalmente, Carrillo hizo énfasis en que la revocatoria de Peñalosa, más allá de sacar al alcalde del palacio de Liévano, “le envía un mensaje contundente a una clase política **que ha creído que es capaz de hacer lo que quiere**”. Por esto, afirmó que es necesario que los ciudadanos presionen a las instituciones por la vía de la movilización pues, “el alcalde se revoca es en las calles”.

<iframe id="audio_21732351" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21732351_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
