Title: Destruyen Jaba Alduweiuman, Alma del conocimiento en la Sierra Nevada
Date: 2020-05-19 07:21
Author: CtgAdm
Category: Actualidad, Ambiente
Tags: Agua, Sierra Nevada, Turismo
Slug: indigenas-de-la-sierra-nevada-denuncian-destruccion-de-lugar-sagrado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/2ad27947-47ed-46a7-9c47-18f6322ffc3b.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/WhatsApp-Video-2020-05-17-at-1.29.23-PM.mp4" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Indígenas de la Sierra Nevada en Resguardo Kogi Malayo Arhuaco denunciaron **intervenciones a gran escala y con maquinaria pesada en los manglares y áreas aledañas a la desembocadura del Río Ancho en el departamento de la [Guajira](https://archivo.contagioradio.com/guajira-sin-agua-por-desvio-de-arroyo-bruno-y-sin-ayudas-para-enfrentar-al-covid/)** municipio de Dibulla.

<!-- /wp:paragraph -->

<!-- wp:video {"autoplay":false,"id":84403,"loop":false,"muted":false,"playsInline":false,"src":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/WhatsApp-Video-2020-05-17-at-1.29.23-PM.mp4"} -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/WhatsApp-Video-2020-05-17-at-1.29.23-PM.mp4">
</video>
  

<figcaption>
*Vídeo cortesía Resguardo Kowi -Malayo- Arhuaco*

</figcaption>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

Por medio de un [comunicado](file:///C:/Users/Usuario/Downloads/Denuncia%20destrucci%C3%B3n%20espacio%20sagrado%20may.%202020.pdf) las **autoridades de los pueblos indígenas de la Sierra Nevada de Santa Marta expresaron su preocupación por la magnitud de las obras y los daños que estás causan** a escala ambiental y ancestral. Acción que posteriormente fue evaluada con una visita al lugar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esos espacios sagrados ubicados en la desembocadura del Río Ancho, según la comunidad Kogi, nunca habían sido intervenidos de esta manera, *"dentro de nuestro conocimiento **esta actividad es ilegal** y no se ha expedido ningún tipo de permiso para realizar estas obras, o muchos menos ingresar al espacio"*, denuncia **Jose Sauna líder del resguardo Kogi**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo señalan que este espacio denominado *Jaba Alduweiuman*, está reconocido desde agosto del año 2018 como **alma del conocimiento, *"un lugar donde además se conectan las lagunas los nevados y todas las desembocaduras y cuencas de los ríos de la Sierra"*.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el mismo pronunciamiento señalan que este espacio esta reconocido en la **resolución 837 de 1995** y por tanto cualquier acción que involucre intervención en la [tierra](https://www.justiciaypazcolombia.com/continuan-labores-de-erradicacion-forzada-en-la-zrcpa-sin-medidas-de-proteccion-para-covid19/) debe ser consultado a las comunidades, antes de cualquier ingreso.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "*El río ancho es la desembocadura del saber sobre la forma de vivir desde acá se emite el conocimiento sobre cómo debemos llevar la vida y cumplir funciones tareas y responsabilidades respecto al hogar".*
>
> <cite>Apartado del comunicado</cite>

<!-- /wp:quote -->

<!-- wp:heading -->

¿Una intervención en la Sierra Nevada con fines turísticos ?
------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Maurico Blanco Director del resguardo Kogi Malayo Arhuaco**, señaló en [Otra Mirada](https://www.facebook.com/contagioradio/videos/260524181970688/), que han hecho seguimiento para poder esclarecer lo que ocurre en el territorio, ***"todavía estamos investigando, lo claro es que hay una ruptura de la madre vieja, afectaron el canal y la vida misma"***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Blanco existen dos hipótesis sobre la intervención, por un lado señala que se trata de una obra para fortalecer el humedal mediante la expansión y la segunda **el inicio de un proyecto turístico ejecutados por terceros.** E

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En cualquier caso para el Director, *"la intervención en este espacio genera incalculables consecuencias para la estabilidad ecológica y social de todo el territorio"*. (Le puede interesar: <https://archivo.contagioradio.com/el-oficio-de-la-pesca-artesanal-debe-ser-reconocido-por-el-estado/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las autoridades indígenas solicita que se tomen medidas urgentes para proteger los ecosistemas sagrados, *"se debe realizar una visita con las autoridades ambientales del Estado para determinar la gravedad de las afectaciones y realizar acciones que permitan determinar si las actividades allí realizadas se hacen con fines turísticos"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente y tras la activación de las actividades mineras en La Guajira, los indígenas wiwas de la Sierra Nevada de Santa Marta, asentados en territorio del municipio de Dibulla, **dieron un ultimátum de 48 horas a los grupos de mineros ilegales que se encuentran en su territorio ancestral** para que lo abandonen o de lo contrario intervendrán por la fuerza para recuperar el dominio territorial.

<!-- /wp:paragraph -->
