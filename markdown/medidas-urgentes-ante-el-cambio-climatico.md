Title: Medidas urgentes ante el cambio climático
Date: 2016-11-08 15:38
Category: Ambiente, Nacional
Tags: cambio climatico, COP 22, gases efecto invernadero
Slug: medidas-urgentes-ante-el-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/cop22.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Lexpress] 

###### [8 Nov de 2016] 

En el marco de la Cop22 en Marruecos y ante la crítica situación del cambio climático, en la que según algunos expertos, la posibilidad de que el planeta aumente su temperatura a niveles máximos en los próximos años si no se llevan a cabo acciones contundentes por parte de los gobiernos, el  objetivo consiste ahora en **orientar los esfuerzos de los países en la implementación  de medidas y acciones que permitan cambiar el grave panorama en materia del calentamiento global y sus impactos. **

De acuerdo al informe presentado por World Resources Institute en 2015, alrededor de **10 países en el mundo son los mayores emisores de CO2 provocando un total del 72%, mientras que 100 países sólo producen el 3%. **Le puede interesar: [Colombia tendrá un aumento de temperatura del 2,14 °C. ](https://archivo.contagioradio.com/colombia-tendra-un-aumento-de-temperatura-del-2-14-c/)

Dentro de los países que mayores gases emiten produciendo el efecto invernadero se encuentran: **China 25.36%; Estados Unidos 14.4%; la Unión Europea 10.16%; India 6.96%; Rusia 5.36%; Japón 3.11%; Brasil 2.4%; Indonesia 1.76%; México 1.67% e Irán 1.65%.**

El  análisis desarrollado en el informe evidenció que más del **75% de los gases efecto invernadero tienen que ver con el sector energético,** mientras que para los casos de Brasil y Australia guarda una estrecha relación con la agricultura. Le puede interesar: [COP22 deberá definir reglas para frenar el efecto invernadero.](https://archivo.contagioradio.com/cop22-debera-definir-reglas-frenar-efecto-invernadero/)

Según el quinto Informe de Evaluación llevado a cabo en 2014 por el Panel Intergubernamental Sobre Cambio Climático (IPCC), **se estimaba que la emisión de gases efecto invernadero aumentaría la probabilidad de impactos graves e irreversibles para los seres humanos y los ecosistemas.**

Algunos de los balances realizados hasta el momento sobre los impactos  generados debido al cambio climático tratan sobre el **aumento de sequías, inundaciones, incendios forestales y aquellos relacionados con la salud humana.**

De acuerdo con el informe presentado en septiembre de 2016 por la  Organización Mundial de la Salud (OMS), la contaminación del aire presenta un importante riesgo medioambiental para la salud.  **Accidentes cardiovasculares, cánceres de pulmón, neumopatías crónicas y agudas como el asma son algunas de las consecuencias.**

Así mismo, la OMS indicó que aproximadamente **1,3 millones de personas mueren al año a causa de la contaminación atmosférica**, mientras que en 2012 se estimó alrededor de un 72% de muertes prematuras relacionadas con la contaminación.

Para 2016 las ciudades que mayores niveles de contaminación presentan en el aire se encuentran en países como  **Irán, China, Chile, México, la India y Brasil.**

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
