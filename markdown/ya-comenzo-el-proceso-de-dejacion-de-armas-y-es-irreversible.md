Title: Inició proceso de dejación de armas y “es irreversible”: FARC
Date: 2017-02-28 15:39
Category: Nacional, Paz
Slug: ya-comenzo-el-proceso-de-dejacion-de-armas-y-es-irreversible
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/entrega-de-armas-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 Feb 2017] 

En rueda de prensa a la que asistieron Iván Márquez, Pastor Alape y Jesús Santrich, anunciaron que ya está listo el primer momento de dejación de armas que será este 1 de Marzo. En esta primera etapa se hará dejación de los explosivos y armas de grueso calibre o “artillería guerrillera”, así como las armas de los 60 integrantes de las FARC que harán tareas de pedagogía del acuerdo.

Según Iván Márquez lo importante es que, a pesar, de los evidentes retrasos por parte del gobierno en la adecuación de las Zonas Veredales, se está cumpliendo un compromiso que “atañe exclusivamente” a las FARC y a la ONU en el marco del Mecanismo de Monitoreo y Verificación. Márquez explicó que en este punto **se está hablando de la destrucción del material de guerra inestable y el registro de las armas en los campamentos**. ([Le puede interesar: ONU propone "recalendarizar" ](https://archivo.contagioradio.com/onu-propone-modificacion-de-cronograma-por-falta-en-adecuacion-de-zonas-veredales/)fechas para entrega de armas)

Por su parte Pastor Alape resaltó que es necesario resaltar que ya comenzó el proceso de dejación de armas y es irreversible “eso es lo que hay que mostrarle al país” y no “enredarse con fechas que pueden confundir” además recalcó que las FARC **se están despojando de la principal arma de la guerra de guerrillas** que es la artillería o explosivos que están en su poder.

Además Jesús Santrich señaló que una vez se terminó la movilización de los integrantes de las FARC EP hacia las zonas veredales, se está dando por terminada una etapa de guerra, puesto que ese traslado implicó el abandono de los puestos estratégicos de la guerra. Sin embargo recordó que uno de los principales problemas es **la persistencia del paramilitarismo y es un tema que debe solucionarse a la mayor brevedad posible** y para ello ya está funcionando una comisión.

### **El caso de Nader es un “mensaje muy negativo para las FARC EP”** 

Según Alape, este caso representa una violación flagrante a los acuerdos de paz, sobre todo por lo que ha representado para los guerrilleros de las FARC porque fue capturado y trasladado a Bogotá con fines de extradición, además el guerrillero contaba con el aval del Mecanismo de Monitoreo y Verificación, lo que implica la necesidad de una respuesta rápida a la que ya se comprometió el Estado. ([Lea también: Las 700 solicitudes de amnistía represadas](https://archivo.contagioradio.com/700-solicitudes-de-amnistia-sin-respuesta/))

Como otro de los puntos que resaltaron los integrantes de las FARC son los incumplimientos, sobre todo en lo que tiene que ver con la ley de **amnistía, 70 días después de aprobada y 11 días después del decreto se están tramitando las solicitudes de libertad condicionada y se conoce que ya son 4 las amnistías concedidas por la fiscalía. Lea también:** [(La amnistía y los derechos de las víctimas.)](https://archivo.contagioradio.com/ley-de-amnistia-la-manzana-de-la-discordia/)
