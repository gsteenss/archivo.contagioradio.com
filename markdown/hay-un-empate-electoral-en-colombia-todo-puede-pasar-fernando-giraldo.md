Title: "Hay un empate electoral en Colombia, todo puede pasar" Fernando Giraldo
Date: 2018-05-31 17:05
Category: Nacional, Política
Tags: Gustavo Petro, Iván Duque, Polo Democrático
Slug: hay-un-empate-electoral-en-colombia-todo-puede-pasar-fernando-giraldo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Votaciones.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [31 May 2018]

Tres momentos importantes sucedieron el día de hoy en Colombia que van configurando el panorama político electoral  para las elecciones 2018. La primera es el anuncio de César Gavira, presidente del Partido Liberal, de unirse a la candidatura de Iván Duque. La segunda es la decisión del Polo Democrático de apoyar a Gustavo Petro, menos el MOIR, que afirmó que invita al voto en blanco y la última, la rueda de prensa del Partido Verde, en donde se dejó en libertad sus integrantes de apoyar a Petro o al voto en blanco, prohibiendo a Duque como una opción.

### **¿El Partido liberal decepcionó a sus bases?** 

De acuerdo con el analista Fernando Giraldo, la decisión del Partido Liberal ya se había expresado en las votaciones a primera vuelta de Iván Duque, debido a que ahí estaba la maquinaria de este partido y no en el apoyo a Humberto De La Calle. "**Cundo el partido Liberal y César Gaviria corre para allá**, divide al partido, pero todos ya estaban allá" manifestó Giraldo.

Así mismo indicó que para la segunda vuelta Duque cuenta con los votos de Cambio Rádical, el Partido Conservador, un sector de los liberales, y los más de dos millones de electores del Centro Democrático. (Le puede interesar: "

### **El Polo Democrático y los Verdes con las viejas tradiciones de la derecha**

Frente a la afirmación de Jorge Robledo, vocero del MOIR, de ir contravía de la decisión de su partido el Polo Democrático, que apoyará a Petro, y votar en blanco, Giraldo señaló que **ese es el 10% de la votación de primera vuelta que ya se presentía reaccionaría de esa forma**.

Sin embargo expresó que la postura de Robledo es "incoherencia política, de la que la historia hablé en su momento". En esa misma medida aseguró que Robledo junto a fajardo o no entienden el panorama político, o están realizando otros cálculos políticos. "**Parte de los problemas de la izquierda en Colombia es que se mimetiza en la disputa de los vicios que tanto le crítica a la derecha, pero sin los méritos". **

Situación similar a la que afrontará el Partido Verde "en donde la mayoría votará por Petro, un porcentaje más reducido se abstendrá o votará en blanco y habrá un 5% que vote por Duque".

No obstante, apenas ha pasado una semana desde la primera vuelta, quedan dos más, razón por la cual Giraldo afirmó que en este instante hay un empate de fuerzas política en el país que se **definirá por el voto de opinión y el voto libre**, que ya en las primeras elecciones derrotaron a las maquinarias y que son las grandes apuestas.

<iframe id="audio_26299881" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26299881_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

 
