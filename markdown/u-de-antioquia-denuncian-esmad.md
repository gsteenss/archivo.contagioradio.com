Title: Estudiantes de la U. de Antioquia denuncian agresiones por parte del ESMAD
Date: 2018-10-18 18:17
Author: AdminContagio
Category: Educación, Movilización
Tags: Capturan estudiantes, ESMAD, U de Antioquia, UNEES
Slug: u-de-antioquia-denuncian-esmad
Status: published

###### [Foto: @farnayaramirez] 

###### [18 Oct 2018] 

Estudiantes del Universidad de Antioquia denuncian agresiones por parte del Escuadrón Móvil Anti Disturbios (ESMAD), y la **retención de 10 universitarios** que se encontraban en la institución; los hechos se presentaron luego que los uniformados iniciaran una acción ofensiva contra una de las estudiantes que se participaba en el **plantón pacífico** por la educación en la ciudad de Medellín, que se desarrolló el pasado 17 de octubre.

Según el **delegado de Derechos Humanos de la UNEES Santiago Valencia,** en Medellín se realizó un plantón cerca de la Universidad de Antioquia, en una calle próxima al MOVA (centro de innovación del maestro) que el presidente Duque estaba inaugurando. Aunque la manifestación se desarrolló de forma pacífica, durante la tarde, **un integrante del ESMAD empujó a una estudiante que formaba parte de la protesta.**

Ese hecho desencadenó una reacción por parte de los estudiantes, que fueron atacados por los uniformados hasta hacer que se refugiaran en las instalaciones de la Universidad. Allí, las acciones violentas escalaron hasta convertirse en una pedrea, en la que **el ESMAD intentaba ingresar al centro educativo, mientras los estudiantes lo evitaban usando piedras y botellas. **(Le puede interesar: ["Profesores de universidades públicas protestan con huelga de hambre"](https://archivo.contagioradio.com/profesores-huelga-de-hambre/))

Los estudiantes denunciaron que el escuadrón además de agredirlos con gases, bombas aturdidoras y pistolas marcadoras, **comenzó a capturar a quienes salían del campus universitario sin importar si la persona había participado o no en la pedrea.** Por esta situación, quienes se encontraban en la institución, por diferentes razones, tuvieron que abandonar en grupos las instalaciones de la Universidad para evitar ser capturados.

Por estos hechos según lo relato el delegado de DD.HH., **fueron capturados 3 jóvenes y se presentó un herido que fue dado de alta el mismo miércoles**, a altas horas de la noche, y agregó que los 3 capturados están en la Fiscalía de Medellín, y aún no saben si los dejarán en libertad o serán presentados ante un juez de control de garantías para realizar su judicialización. No obstante, señaló que esperaban su libertad, porque hasta donde tienen conocimiento, no hay material probatorio en contra de los estudiantes.

### **Estudiantes están viendo entorpecidas sus protestas**

En la **Universidad de Pamplona se registró el ingreso de efectivos de la Policía,** mientras otras instituciones, como la Escuela Superior de Administración Pública (ESAP) que se declaró en asamblea permanente, los estudiantes señalan que están en confrontación con la dirección de la universidad que **intenta sabotear la asamblea pidiendo a los profesores sacar notas de sus clases. **(Le puede interesar: ["Profesores se suman a la defensa de la educación pública"](https://archivo.contagioradio.com/profesores-defensa-educacion-publica/))

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FSocialUniPamplona%2Fposts%2F2325478987467762&amp;width=500" width="500" height="688" frameborder="0" scrolling="no"></iframe>

Sin embargo, Valencia sostuvo que **las marchas en las calles aumentarían, y los estudiantes seguirían protestando por mejoras en la educación superior pública**. Adicionalmente, indicó que ahora, con el Presupuesto General de la Nación aprobado en primer debate, la puja seguirá para que se hagan adiciones presupuestales, hasta cubrir los **4,5 billones** de pesos que hacen falta en lo inmediato para las Instituciones de Educación Superior del país.

<iframe id="audio_29428690" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29428690_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
