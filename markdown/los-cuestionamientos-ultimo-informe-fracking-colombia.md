Title: Ambientalistas cuestionan informe sobre viabilidad de fracking
Date: 2019-02-14 13:26
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Agencia de Protección Ambiental, fracking, Ministerio de Minas y Energía
Slug: los-cuestionamientos-ultimo-informe-fracking-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-2018-08-27-a-las-5.17.01-p.m.-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CarlosSantiagoL] 

###### [14 Feb 2019] 

Algunas organizaciones y comunidades defensoras del ambiente, prevén que las conclusiones del informe preliminar que se presentará este jueves en Bogotá, sobre la viabilidad de la fracturación hidráulica o fracking en Colombia, respaldará la postura del gobierno de avalar esa técnica extractiva en el territorio nacional.

Según información conocida por quienes se oponen al fracking, el comité de 13 académicos designados por el Ministerio de Minas y Energía, concluirá que **sí es una técnica viable para la explotación de hidrocarburos en Colombia,** siempre y cuando sea acompañado por garantías de seguimiento y control por las autoridades competentes. Además, que recomendarán realizar pruebas piloto iniciales.

Sin embargo, Rodrigo Negrete, abogado ambiental, sostiene que esta conclusión **desafiaría las realidades del país donde estas garantías se prometen pero no se cumplen,** lo que se evidencia en los últimos desastres ambientales que pasaron por alto de las autoridades en **Hidroituango, El Quimbo y el poso Lizama 158**. "Las instituciones no están preparadas en los elementos de juicio para poder hacer un control y seguimiento adecuado" sostuvo el abogado.

Según [un estudio](https://cfpub.epa.gov/ncea/hfstudy/recordisplay.cfm?deid=332990) de seis años de la Agencia de Protección Ambiental de Estados Unidos, está comprobado que esta técnica de explotación de yacimientos no convencionales, perjudica las fuentes de agua potable y en algunos casos ha estado vinculada con el incremento de la actividad sísmica en las zonas donde se ha implementado.

"Se dice que hay muchas incertidumbres frente a los impactos del fracking, pero lo cierto es que **hay un impacto demostrado y hay un riesgo que ni el país ni las comunidades locales tendrían porque asumir**, sobre todo en un país con la mayor biodiversidad por metro cuadrado del mundo," dijo Negrete.

### **Las limitaciones del estudio** 

Negrete resaltó que nueve de los 13 académicos designados para investigar y presentar este reporte respaldan públicamente el fracking y están asociados con intereses del sector privado pese a que se desempeñan en entidades públicas. Por tal razón, concluyó que las recomendaciones de este estudio podrían estar sesgadas a los intereses del Gobierno y de sectores pro-fracking.

Además, el abogado cuestionó el nivel de investigación que se pudo producir dado que el plazo del estudio fue de tan solo tres meses, en los que visitaron tres comunidades en las que se implementaría el fracking. "Un país serio no podría soportar una decisión tan delicada, tan compleja en un informe de esta naturaleza porque no hubo el tiempo suficiente porque el Gobierno de Duque está presionando para que esto se autoriza," dijo Negrete.

Frente a estos hechos, Negrete afirmó que es necesario formar y fortalecer las redes de solidaridad en contra del fracking para poder combatir efectivamente esta amenaza a las comunidades y los territorios. El informe se presentará este jueves a las 2:30 de la tarde en el Biblioteca Virgilio Barco de Bogotá.

<iframe id="audio_32557508" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32557508_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
