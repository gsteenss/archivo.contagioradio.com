Title: Día del maestro avanza con paro y movilización de FECODE
Date: 2017-05-15 13:18
Category: Educación, Nacional
Tags: Día del maestro, fecode, Paro de maestros
Slug: en-el-dia-del-maestro-avanza-paro-de-fecode
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/425026_173928_5-e1494866968585.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana] 

###### [15 mayo 2017]

El paro de docentes completa su día número cuatro de cese de actividades ante los incumplimientos del gobierno. **Fecode le pide al gobierno que cumpla lo pactado** para mejorar la calidad educativa de los estudiantes y las condiciones laborales de los docentes y aseguró que no han sido convocados a ningún espacio de diálogos por parte del gobierno.

**Los maestros han afirmado que no levantarán el paro para negociar.** Por el contrario, el gobierno ha reiterado que los maestros deben volver a clases para adelantar las negociaciones. Sin que se resuelva el déficit presupuestal de 6 mil millones de pesos, los maestros han dicho que no retomarán las actividades.

En estos cuatro días de paro los maestros han realizado campañas de socialización con padres de familia y estudiantes. Luz Patricia Zuluaga, miembro de ADIDA y Fecode en Antioquia, aseguró que el 70% de los maestros están en paro y hemos recibido el apoyo de los padres de familia y los estudiantes". Le puede interesar: ["300 mil maestros de Colombia inician paro nacional por incumplimientos del gobierno"](https://archivo.contagioradio.com/300-mil-maestros-de-colombia-inician-paro-nacional-por-incumplimientos-del-gobierno/)

Ante los descontentos que tuvieron los sindicatos con Fecode en el paro del 2015, Zuluaga asegura que "en este paro tenemos más madurez política y sabemos que acuerdos podemos lograr, no podemos pretender solucionar temas que no se logran con el paro".

Entre las actividades, que se tienen previstas para la celebración del día del maestro, **los educadores tiene previsto realizar labores culturales y plantones.** En Bogotá saldrá una movilización desde el centro Administrativo de la Asociación de Docentes y Educadores, hasta el Ministerio de Educación. Le puede interesar: ["Esta es la agenda del paro nacional de Fecode a partir de este 11 de mayo"](https://archivo.contagioradio.com/asi-sera-el-paro-nacional-de-fecode-a-partir-de-este-11-de-mayo/)

[**SALARIOS Y NÚMERO DE MAESTROS OFICIALES EN COLOMBIA**]

1\. **Los maestros del sector público en el país, para devengar el salario, están regidos por un sistema de escalafón** en donde los estudios y la aprobación de un concurso, determina el valor del salario. Hay maestros que se rigen por el estatuto 2277 de 1979 que cuenta con 14 puestos de escalafón. El estatuto 1278 de 2002, comprimió el escalafón en 3 grados con 3 niveles (A,B,C)

2\. El salario de los docentes de educación pública, según Fecode, **no supera el \$1´500.000,** contrario a los \$2, 500.00 que asegura el gobierno.

3\. El salario máximo de un educador del sector público es de **\$6´136.508 pesos únicamente si el maestro tiene doctorado y ha aprobado los concursos del Estado.**

4\. Según un estudio de Fecode, en 2016, había **320.043** docentes del sector oficial en Colombia.

5\. Por las condiciones mismas del sistema, son pocos los maestros que pueden aspirar a salarios más altos. Para 2016, Fecode aseguró que con título de pregrado hay 87.806 docentes; con especialización 29.618, con maestría 6.842 y **con doctorado 37 docentes** en todo el país.

6\. Según el Ministerio de Educación, **los maestros trabajan 1720 horas al año** y de esas 1290 son horas presenciales, que incluyen reuniones de preparación de clases, y 880 son destinadas a dar clases.

El gremio de educadores oficiales en Colombia, **aún no cuenta con las condiciones laborales suficientes** como si lo tienen otros trabajadores estatales. Los maestros le han pedido al gobierno que se haga una inversión mayor en el sector de la educación pues solo están pensando en cómo cumple la regla fiscal, le cumplen a la OCDE y a la Banca Internacional.

###### Reciba toda la información de Contagio Radio en [[su correo]
