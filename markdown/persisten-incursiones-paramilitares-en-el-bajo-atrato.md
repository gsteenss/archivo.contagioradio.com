Title: Persisten incursiones paramilitares en el Bajo Atrato
Date: 2020-01-07 16:51
Author: CtgAdm
Category: Comunidad, Nacional
Tags: AGC, Bojaya, Chocó, Jiuguamiandó, Paramilitarismo
Slug: persisten-incursiones-paramilitares-en-el-bajo-atrato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/AGC-Jiguamiando.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El pasado 3 de enero se conoció de una nueva incursión por parte de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) en el territorio de Jiguamiandó, Chocó, mientras que el 5 de enero en horas de la tarde un grupo de más de 30 paramilitares, hicieron presencia en la comunidad embera de Dearade. La situación, que ha se ha visto reflejada en diversas comunidades y municipios como Bojayá, donde 7.000 personas continúan confinadas a llevado a las comunidades a denunciar la inoperancia de la Fuerza Pública frente a este control territorial.

Según habitantes del territorio de Jiguamiandó, hombres con armas largas y vestidos de camuflado ordenaron reunir a toda la comunidad, preguntándoles por líderes y lideresas, y a su vez **manifestándo a la población que debían deforestar y sembrar coca afirmando tener total control territorial**. Como precedente, en julio de 2019, en Jiguamiandó fue desaparecida una persona que se negó a entregar las siembras de coca al grupo paramilitar. Al respecto, los pobladores embera han reiterado la ineficacia del Ejército y la Policía frente a este tipo de operaciones que ocurren en lugares con fuerte presencia de la Fuerza Pública.

En otras comunidades esta misma agrupación ha ordenado a los líderes guardar silencio sobre sus operaciones, forzándoles a recibir los denominados “Puntos”: hombres vestidos de civil, con armas cortas y radios de comunicación  y que hacen presencia en cada uno de los caseríos que se encuentran en estos sitios de tránsito terrestre desde Belén de Bajirá hasta el casco urbano del municipio de Riosucio.

Según pobladores afrocolombianos que han tenido que abandonar la región ante las amenazas de muerte, por denunciar las operaciones de las AGC, esta estructura armada controla el río Jiguamiandó y el río Tamboral, afluentes que permiten la libre movilidad de las comunidades hacia **Murindó, Curbaradó y a Mutatá.**  [(Lea también: Asedio paramilitar continúa en territorios colectivos de Curbaradó y Jiguamiandó)](https://archivo.contagioradio.com/asedio-paramilitar-continua-en-territorios-colectivos-de-curbarado-y-jiguamiando/)

### Ante presencia de paramilitares, Chocó pide implementar Acuerdo de Paz al Gobierno 

Tras las recientes denuncias hechas por el sobreviviente de la masacre de Bojayá, Leyner Palacios, coaccionado por grupos paramilitares para que abandone la región, desde el Gobierno se indicó que el presidente Iván Duque se reunirá con el líder social para abordar estrategias para hacer frente a esta situación. [(Le puede interesar: Control paramilitar de AGC se extiende desde frontera con Panamá, Bajo y Medio Atrato)](https://archivo.contagioradio.com/no-es-solo-bojaya-bajo-atrato-y-dabeiba-blanco-de-operaciones-paramilitares/)

Palacios, quien se rehusó a dejar Bojayá, indicó que son 7.000 personas las que continúan en condición de confinamiento en Bojayá quienes temen ser víctimas de alguna mina antipersonal al salir de la cabecera municipal o realizar sus actividades diarias de pesca o cultivo.

"La problemática de seguridad de Bojayá se ha denunciado por parte de la Defensoría del Pueblo desde el 9 de abril y no se han atendido” afirma el líder social quien ha sido explícito en que de no contrarrestar el control paramilitar en la región, es factible que en Bojayá suceda una tragedia como la ocurrida en 2002 y que terminó con la vida de más de cien personas de la población civil. [(Lea también: Tras 16 años de la masacre, Bojayá no ha logrado cerrar el capítulo del dolor)](https://archivo.contagioradio.com/bojaya-cerrar-dolor/)

Palacios cuestionó la incapacidad del Estado para hacer frente a esta situación, indicando que aunque se han realizado consejos de seguridad, nada ha cambiado, **"¿Cómo es posible que 600 paramilitares entren a Bojayá y que el Ejército no los vea?"**. Por su parte, el comisionado de Paz, Miguel Ceballos respondió que [el número de paramilitares en la zona sería cercano a 100 personas, desconociendo que el problema no es el número de integrantes de un grupo armado sino la convivencia del mismo con el Ejército, patrón que ha sido detallado]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}en informes aportados por organizaciones sociales a la Comisión de la Verdad  y a la Jurisdicción Especial Para la Paz.

Finalmente el líder social señaló que durante su reunión con el mandatario resaltará la importancia de implementar en su totalidad el Acuerdo de Paz que incluye un enfoque étnico y planes especiales de protección, además destacó la necesidad de restablecer la mesa de negociaciones con el ELN y de encontrar una salida dialogada al conflicto. [(Lea también: Académicos del mundo piden a la ONU exigir cumplimiento de protocolos con el ELN)](https://archivo.contagioradio.com/academicos-del-mundo-piden-a-la-onu-exigir-cumplimiento-de-protocolos-con-el-eln/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
