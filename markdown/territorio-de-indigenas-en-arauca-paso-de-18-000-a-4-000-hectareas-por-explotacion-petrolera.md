Title: 60% del territorio Araucano está destinado a explotación petrolera
Date: 2015-05-13 16:42
Author: CtgAdm
Category: DDHH, Nacional
Tags: Arauca, comunidades indígenas, Cumbre Agraria, Derechos Humanos, El Instituto Colombiano de Desarrollo Rural, Explotación petrolera, INCODER, INCORA, OXY, territorios indígenas
Slug: territorio-de-indigenas-en-arauca-paso-de-18-000-a-4-000-hectareas-por-explotacion-petrolera
Status: published

##### Foto: [notiagen.wordpress.com]

<iframe src="http://www.ivoox.com/player_ek_4491666_2_1.html?data=lZmmk5uaeo6ZmKiak5aJd6Kpk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIqdSfqtPRh6iXaaK4yMrbw9iPqc%2BfotfO18jFb9HdxtfRx9OPt9af1crf1M7Ys9Pd0JCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alvaro Hernández, vocero de las Organizaciones Cívicas y Populares de Arauca] 

Comunidades indígenas de Arauca, denuncian que les **han sido despojadas alrededor de 14.000 hectáreas de tierras**, además no hay agua potable, acceso a educación, ni salud.

Según denuncia, Alvaro Hernández, vocero de las Organizaciones Cívicas y Populares de Arauca, la situación se debe que **el 60% del territorio Araucano está destinado para la explotación petrolera,** ya que desde el 2010, a través de la Agencia Nacional de Hidrocarburos, el departamento ha sido **concesionado a 16 multinacionales.**

Pese a que los indígenas debían estar amparados por  el Auto 382 de 2010 de la Corte Constitucional que se refiere a la protección de los territorios indígenas, **la situación “es caótica”** como lo asegura Hernández. Aunque en 1964 el INCORA, hoy INCODER,  le otorgó un territorio de 18.000 hectáreas a los indígenas,  desde 1985 por la explotación petrolera, el territorio de los indígenas **se ha ido reduciendo a 4.000 hectáreas y lugares como la laguna del Lipa se han visto afectados** generando graves consecuencias en la fauna, la flora, la seguridad alimentaria de los indígenas para quienes este lugar tiene gran importancia espiritualmente.

**La explotación petrolera tiene a su disposición alrededor de 250.000 hectáreas de Arauca**, lo que no solo ha afectado a los nativos sino también a los campesinos, quienes también han perdido sus fuentes de agua, ya que el petróleo ha contaminado los ríos.

Otro de los problemas que aqueja a la comunidad, es la falta de acceso a la educación ya que existen dos asentamientos indígenas que no cuentan con profesores para la escuela primaria y además las condiciones en las que estudian muchos niños indígenas no son las adecuadas.

Por su parte, el vocero de las comunidades, asegura que no se ven programas claros por parte del Incoder para defender el territorio de los pobladores. Pese a que se estableció el Auto 382 de 2010 de la Corte Constitucional, de acuerdo a Hernández, “**en estos 4 o 5 años que se emite el Auto las soluciones han sido muy pocas,** en todos los temas, el territorio, agua potable y educación, aunque se anuncian medidas no se cumple nada”.

Así mismo, en el marco del paro agrario del 2013, se había concretado una mesa de conversaciones para que el gobierno solucionara la situación de la población indígena y campesina del Arauca, sin embargo hasta el momento no ha habido respuestas concretas de parte del gobierno, por lo que podría “**haber un nuevo paro reclamando los derechos de las comunidades”,** expresa el vocero de las comunidades.

Por el momento, se espera que la visita de la comisión humanitaria liderada por el Vicedefensor del Pueblo, Esiquio Manuel Sánchez Herrera,  haga que las autoridades regionales y departamentales le pongan atención a esta situación y se tomen medidas para garantizar los derechos de las familias indígenas y campesinas de este departamento.


