Title: Inversión en Chino
Date: 2015-05-20 09:31
Author: CtgAdm
Category: Ambiente y Sociedad, Opinion
Slug: inversion-en-chino
Status: published

#### [**[Margarita Flórez ](https://archivo.contagioradio.com/margarita-florez/)– Directora de [[Ambiente y Sociedad](http://www.ambienteysociedad.org.co/es/inicio/)]**] 

Desde el 18 de mayo y por una semana el Primer Ministro de la República Popular de China visita Brasil, Colombia, Perú, y Chile países con quienes el intercambio comercial conjunto constituye el 57% del total que China mantiene con América Latina. Se aspira a consolidar las inversiones de ese país en varios sectores, como el tecnológico, industrial, tratados de libre comercio, y créditos o convenios de cooperación no reembolsable. Sin embargo existen diferencias en el tratamiento hacia los países: respecto de Brasil Perú, y Chile el objetivo de los gobiernos es avanzar en Asociación Estratégica Integral mientras que para el caso colombiano es un Acuerdo de Cooperación Amistosa.

En el Brasil se quiere llegar a inversiones cercanas a los 50.000 millones de dólares (43.800 millones de euros) destinados principalmente a la construcción de infraestructura, y el proyecto estrella es el ferrocarril que uniría la costa brasileña con la peruana, y servirá como medio de transporte de la soya brasileña hacia el mercado chino.

Este tipo de acuerdos están siendo analizados como el caso de los firmados con Argentina, y han recibido comentarios que señalan carencias, como la falta de contenidos ambientales, la confidencialidad de los textos que impiden tener un acceso oportuno a ellos, y cláusulas de preferencia según las cuales las empresas chinas cuya financiación provenga de organismos de ese país tendrán un tipo de contratación fuera del sistema de licitación pública. Dado que China y Colombia ya firmaron cerca de una decena de acuerdos, cuyos textos no conocemos, y uno de los cuales dio como resultado el Plan Maestro del Rio Magdalena, PMA, nos parece que tenemos el derecho de conocer las condiciones en que se pactan dichos acuerdos, y el impacto social y ambiental que las inversiones chinas pudieran llegar a producir.

Esta cooperación no difiere de la que se está imponiendo, por parte de los diferentes gobiernos, que dirigen sus esfuerzos a lograr contrataciones para su empresariado dentro del esquema de las alianzas público – privadas financiadas con fondos públicos.

Ante esta proliferación de acuerdos de los cuales se obtiene mayor información fuera del país, lo que habría que solicitar es un mayor acceso a la información en tiempo real sobre los acuerdos para de esta manera poder realizar el seguimiento oportuno de los acuerdos, y examinar a nivel nacional si se cumple con la legislación sobre medio ambiente, la cual pareciera ser más fuerte cuando China invierte, pero cuyo alcance, y aplicación no puede seguirse sin que los ciudadanos tengamos acceso a los diferentes instrumentos que se firman.
