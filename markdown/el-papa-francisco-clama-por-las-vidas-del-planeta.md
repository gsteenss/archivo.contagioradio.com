Title: El Papa Francisco clama por las vidas del planeta
Date: 2015-07-08 16:16
Category: Abilio, Opinion
Tags: cambio climatico, crisis ecológica, encíclica Laudato Si, Papa, Papa Francisco
Slug: el-papa-francisco-clama-por-las-vidas-del-planeta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/papa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Por [Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)]** [**- [~~@~~Abiliopena](https://twitter.com/Abiliopena) 

###### [8 Jun 2015] 

La lectura de la Encíclica Laudato Si, del papa Francisco, en medio de las síntesis apretadas de los medios de información que no podían dejar de registrarla como noticia, produjo en muchos de nosotros una profunda satisfacción por la sensación de que al fin se ponían al día tantas luchas de mujeres y hombres creyentes y no creyentes en favor de la preservación de la vida humana y natural, con la voz oficial de la iglesia Católica universal, nada más ni nada menos que con el mismo papa.

Comprendimos que el nombre adoptado por el cardenal Bergoglio el día de su consagración como pontífice, no era demagógico como algunos creíamos. El nombre de Francisco, el de Asís, realmente lo comprometía con su causa en la defensa del pobre, con la vida sencilla, con contemplación de lo bello y sublime, con el amor por las criaturas, con la crítica a la institucionalidad y al poder que destruyen la creación, con la vuelta a las raíces evangélicas.

En la encíclica Francisco el Papa hace una interpelación profunda al modelo de mercado y a su racionalidad instrumental que convierte en medios, para el crecimiento económico los bosques, las aguas, el aire, el suelo, el subsuelo, la biodiversidad, las mujeres y hombres. Contradice el todopoderoso mito del progreso ilimitado y a la tecno - ciencia funcional a las empresas de los países ricos, principales responsables del cambio climático, que argumentan con inmensa irresponsabilidad suicida que la ciencia encontrará la salida a la crisis ambiental y prosiguen sin mas con la depredación del medio ambiente. Entre muchas denuncias, reclama por la homogenización de la cultura impuesta por el modelo consumista del mercado y por la expulsión a la que se ven sometidos pueblos enteros por empresas que imponen monocultivos con semillas genéticamente modificadas.

Revitaliza la opción preferencial por los pobres, tan entrañable en la iglesia latinoamericana de los documentos de Medellín y Puebla y llama a una comprensión de la integralidad de la casa común donde la crisis ecológica y la pobreza son expresiones de la amenaza a todas las vidas. No vacila en denunciar al modelo capitalista de mercado que considera que los pobres son desechables.

Llama a comprendernos como parte del mundo natural, no como superiores en relación con los demás seres, llama a comprender la naturaleza con mirada integral y no como especialistas de parcelas cada vez más diminutas de la realidad que ignoran las interconexiones. Llama, entre otras cosas, a la toma de conciencia de la interdependencia de todo lo existente y en consecuencia de la responsabilidad que recae sobre los sectores de poder de los países ricos, sobre los gobiernos por el menosprecio de lo creado. Convoca a emprender practicas, también en lo micro, que apunten a las transformaciones estructurales y que permitan el disfrute de lo bello, lo sublime de la creación para las generaciones futuras.

La riqueza de las propuestas de *Laudato Si* van desde el llamado a los sectores de poder a un cambio radical de su actitud irresponsable con el planeta, pasa por invitar a la movilización social para la exigencia de los derechos de la casa común, a la censura severa a la ficción de los estudios de impacto ambiental repletos de corrupción, y a la garantía del derecho a la consulta a las poblaciones afectadas por un determinado emprendimiento, de modo que este se aplace, modifique o cancele si sus impactos afectan la vida humana y natural.

En el fondo, está la fuerza de las motivaciones de la tradición cristiana, recuperada por Francisco en su integra dimensión. Aquella que comprende que la tierra es de Dios, que es para todas y todos, que se preocupa por que la tierra y los animales descansen, que ordena que garantice la vida digna de los pobres, que habla del cuidado que Dios depara a todas sus criaturas.

Con enorme capacidad autocrítica, fundada en la autoridad de mujeres y hombres de iglesia que como San Francisco de Asís entregaron hasta el último suspiro de su vida en fidelidad al Evangelio, reconoce gravísimos errores en la interpretación bíblica, como aquel que atribuyó a los hombres superioridad y dominio sobre la creación.

Así mismo, afirma que *“ si una mala comprensión de nuestros propios principios a veces nos ha llevado a justificar el maltrato a la naturaleza o el dominio despótico del ser humano sobre lo creado o las guerras, la injusticia y la violencia, los creyentes podemos reconocer que de esa manera hemos sido infieles al tesoro de sabiduría que debemos custodiar”* (L.S 200). Reconocimientos como estos antes que minar la credibilidad en instituciones como la iglesia, la aumenta y da autoridad a portes como el pronunciado por el papa Francisco en esta Encíclica.

Sin duda el recorrido que hace Francisco por la casa común, la deja ver seriamente averiada; sin embargo, muestra que muchos movimientos están trabajando por protegerla y restaurarla. La esperanza que deja ver no es barata, no es ilusoria. Los cambios no son fáciles y requieren de la fuerza de la voluntad, de la movilización, de la conversión, por eso concluye llamando a que *“nuestras luchas y nuestra preocupación por el planeta no nos quieten el gozo de la esperanza”* (L.S 244)

Y al final, Francisco, como su homónimo de Asís, hace también su *“oración por nuestra tierra”,* de la que transcribimos sólo estos apartes por la fuerza de contemplación y síntesis que contienen:

###### “*Dios de los pobres,* 

###### *ayúdanos a rescatar* 

###### *a los abandonados y olvidados de esta tierra* 

###### *que tanto valen a tus ojos* 

###### *...Toca los corazones* 

###### *de los que buscan sólo beneficios* 

###### *a costa de los pobres y de la tierra.* 

###### … *Aliéntanos, por favor, en nuestra lucha* 

###### *por la justicia, el amor y la paz”* (L.S 246). 
