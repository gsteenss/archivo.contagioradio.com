Title: Razones a favor y en contra de la curul de Ángela María Robledo
Date: 2019-04-22 16:28
Author: CtgAdm
Category: Movilización, Política
Tags: Angela Maria Robledo, Consejo de Estado, Curul, oposición
Slug: razones-curul-angela-maria-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Df7trgxX4AESEuS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @angelamrobledo] 

Esta semana se espera que el Consejo de Estado falle en el caso que lleva contra la representante a la Cámara Ángela María Robledo, quien a través de sus redes sociales, ha explicado los argumentos jurídicos por los que espera el fallo de su caso como ocurrió con el de la vicepresidenta, Marta Lucia Ramírez. Los argumentos de Robledo para defender su curul son que para ser presidente o vicepresidente (como fue su caso) no incurría en inhabilidades por doble militancia, y que llegó al Congreso de la república gracias al artículo 112 de la Constitución Política de Colombia así como el Estatuto de la oposición.

Como lo explica la misma Representante, cuando ella tomó la decisión de acompañar a Gustavo Petro como fórmula para la presidencia, lo hizo segura de esa propuesta política para Colombia, y que no incurriría en doble militancia al hacerlo. En su momento, Robledo fue Representante a la Cámara por el partido Alianza Verde, sin embargo, renunció a su cargo y su partido para hacer equipo con Petro en la Colombia Humana, un movimiento político que no tiene personería jurídica.

<iframe src="https://co.ivoox.com/es/player_ek_34779798_2_1.html?data=lJmkmZ6bfZmhhpywj5WcaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncaLiyMrZw5C2s8Pgxsnch5enb9Tjw9fSjdHFb8XZzsbbxsaPp9Di1dfOjdjZb8Tp09rZjcrSb83VjJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En ese sentido, Robledo recuerda que consultando a abogados y amigos, todos afirmaron lo dicho por el doctor Augusto Hernandez (quien ejerce su defensa jurídica), y es que "las inhabilidades deben ser absolutamente  restrictivas, e inhabilidades como la doble militancia para presidencia y vicepresidencia no existen". Las inhabilidades para presidente y videpresidente están señaladas en el[artículo 197](http://www.constitucioncolombia.com/titulo-7/capitulo-1/articulo-197) de la Constitución, citando los numerales 1, 4 y 7 del [articulo 179](http://www.constitucioncolombia.com/titulo-6/capitulo-6/articulo-179) de la Carta Magna.

A ello se suma el precedente que existe del caso con la vicepresidente Marta Lucía Ramírez, quien fue demandada por doble militancia, pues al igual que Robledo pertenecía al Partido Conservador y renunció al mismo para ser fórmula junto a Iván Duque. En su caso, el proceso recibió un fallo de archivo y fue exonerada de culpa. (Le puede interesar:["Ganaremos quienes rechazamos las objeciones de Duque a la JEP: Angela. M Robledo"](https://archivo.contagioradio.com/rechazamos-objeciones-angela-robledo/))

### **Se debería archivar el proceso, y mantener la curul de Robledo**

> Mi actual curul NO es resultado de una elección popular directa; me la da la Constitución (Art.112) y el Estatuto de la Oposición; demandar la curul es cuestionar la Constitución.
>
> No tenía que renunciar 1año antes al Verde, para la dignidad de vice no existe la doble militancia. [pic.twitter.com/YbrE2JdY3C](https://t.co/YbrE2JdY3C)
>
> — Ángela María Robledo (@angelamrobledo) [17 de abril de 2019](https://twitter.com/angelamrobledo/status/1118541593705889797?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
En caso de que se fallara contra Robledo por doble militancia, la bancada alternativa perdería otra importante figura en el Congreso, sin embargo, el argumento jurídico que emitió la defensa señala que ella no se presentó a elección popular para ser parte de la Cámara de Representantes, sino para ser vicepresidenta; y su escaño en el Legislativo es gracias al Estatuto de la Oposición, que otorga el cargo de Senador y Represente a la Cámara para la fórmula que obtenga la segunda mayor votación en los comicios presidenciales.

</p>
En ese sentido, Robledo y otras figuras políticas han expresado que esperan un fallo en derecho del Consejo de Estado que actúe archivando el caso como en el caso de la actual Vicepresidenta. (Le puede interesar:["Hay una acción concertada para sacar figuras de la oposición de la vida política"](https://archivo.contagioradio.com/accion-concertada-para-sacar-oposicion/))

### **La opinión del demandante sobre el caso de Ángela María Robledo**

<iframe src="https://co.ivoox.com/es/player_ek_34779827_2_1.html?data=lJmkmZ6cdpihhpywj5aZaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncavpwtOYpcbWsNDnjKjOzsnJtoa3lIqvldOJdqSf18rSxtTWb8Td1snOxsbSs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Por su parte, Juan Carlos Calderón España, presidente de la Veeduría Nacional Recursos Sagrados y quien demandó a Robledo por doble militancia, sostiene que en ello radica su petición: Amparado en el artículo segundo de la [Ley 1475 de 2011](http://www.secretariasenado.gov.co/senado/basedoc/ley_1475_2011.html), Calderon cree que la Representante incurrió en esta falla al no presentar su renuncia al partido Alianza Verde un año antes de ser fórmula con Gustavo Petro. (Le puede interesar: ["Tejeremos la bancada de oposición": Angela M. Robledo"](https://archivo.contagioradio.com/tejeremos-la-bancada-de-oposicion-angela-maria-robledo/))

Sin embargo, la Procuraduría General de la Nación envío un concepto al Consejo de Estado en el que afirma que no están dadas las condiciones para que Robledo incurriera en la prohibición de doble militancia, pues esta ocurre solamente cuando el candidato se presenta por otro partido o movimiento político para la misma corporación pública o una similar (Congreso, asamblea departamental, concejo municipal...); hecho que no ocurrió en este caso. Razón por la que el ente de control pidió que se mantenga la Curul de la congresista de la Bancada de Oposición.

> La Procuraduría ha solicitado al [@consejodeestado](https://twitter.com/consejodeestado?ref_src=twsrc%5Etfw) no anular mi actual curul en cuanto es resultado de la Constitución y no se configuró doble militancia.
>
> Debo resaltar que no me presenté  
> al Congreso, renuncié al Verde y no tenía que hacerlo 1año antes porque la CP no lo exige. [pic.twitter.com/127vWaf5zz](https://t.co/127vWaf5zz)
>
> — Ángela María Robledo (@angelamrobledo) [22 de abril de 2019](https://twitter.com/angelamrobledo/status/1120420542547464192?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
