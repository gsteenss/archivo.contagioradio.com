Title: Acceder al agua, un privilegio para 1 de cada 3 personas en el mundo
Date: 2019-06-25 12:03
Author: CtgAdm
Category: El mundo
Tags: acceso al agua, Informe, niños, Unicef
Slug: acceso-al-agua-privilegio-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/POBREZA-INFANTIL-AGUA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: La portada 

En 2019 el acceso a agua potable segura sigue siendo una utopía para **1 de cada 3 personas en el mundo, y la posibilidad de utilizar las instalaciones de saneamiento para al menos 3 mil millones de personas también está amenazada**. Es lo que revela un informe del Fondo Internacional de Emergencia de las Naciones Unidas para la Infancia (UNICEF) realizado en colaboración con la Organización Mundial de la Salud (OMS) **"Avances en el agua potable, el saneamiento y la higiene: 2000-2017: enfoque especial en las desigualdades"**, subrayando cuán concretas y serias son las diferencias en el acceso a los recursos necesarios para suplir las necesidades diarias de los seres humanos.

El número de niños menores de cinco años que mueren cada año debido a situaciones higiénicas precarias y la falta de agua potable es de **297.000**, una cifra que alerta sobre las afectaciones producidas puntualmente en la población infantil. **Más de la mitad de la población mundial, es decir 4,2 mil millones de personas, no disfrutan de condiciones sanitarias seguras** con respecto al consumo de agua, mientras que **2,2 mil millones no tienen acceso al servicio de agua potable**. Infortunadamente, no solo el acceso representa un problema, sino la calidad del recurso que en ocasiones por su consumo ocasiona más daño a las personas.

Kenny Ann Naylor,directora del Departamento de Agua, Salud e Higiene de UNICEF, asegura que de esta manera se socava la oportunidad de crecimiento en un ambiente saludable para las generaciones futuras. A pesar de que se han tomado medidas para aumentar la cobertura, sin embargo la brecha global en la calidad de los servicios prestados sigue aumentando, tanto así que **mientras 1.800 millones de personas más han tenido la oportunidad de usar agua potable desde el año 2000, 785 millones no pueden beneficiarse de los servicios básicos de agua.**

Las dos organizaciones señalan en el informe que una inversión mundial en agua e higiene es necesaria, especialmente para hacer frente a la propagación de algunas enfermedades. La Dra. Maria Neira, Directora del Departamento de Salud Pública y Medio Ambiente de la OMS pidió a **la comunidad internacional que redoble sus esfuerzos para lograr un acceso universal digno para 2020**.
