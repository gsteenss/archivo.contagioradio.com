Title: Se conforma red de solidaridad en defensa de Mateo Gutiérrez
Date: 2017-02-27 13:23
Category: DDHH, Nacional
Tags: falso positivo judicial, Mateo Gutierrez
Slug: red-solidaridad-defensa-mateo-gutierrez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/mateoG.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [27 Feb 2017] 

Pese a que no se conoce la fecha de la próxima audiencia de Mateo Gutiérrez, sin embargo una **cadena de solidaridad se ha gestado a su alrededor denunciando un “falso positivo jurídico” del que el estudiante sería víctima. **

Sobre las dos de la tarde se llevará a cabo en la Universidad Nacional de Colombia, una reunión entre los padres de Mateo Gutiérrez, estudiantes de Ciencias Humanas y docentes de esta universidad, para generar una **red de apoyo hacía el estudiante y obtener una respuesta de solidaridad por parte de las directivas de la Institución.** El encuentro se realizará en el auditorio 104 de la Facultad de Sociología.

El jueves, **2 de marzo se realizará un toque en la Universidad Nacional y se pintará un mural en la Facultad de Ingeniería de esta universidad**. El viernes 3 de marzo se llevará a cabo un **plantón en la URI de Puente Aranda**, en donde en este momento se encuentra retenido Mateo Gutiérrez.

Durante toda la semana se **recolectarán libros y cartas para ser entregadas el próximo miércoles 8 de marzo**.

De igual forma, el profesor de la Nacional,  Miguel Ángel Herrera expresó a través de una carta, la **indignación que le produce la captura de Mateo Gutiérrez** y la imputación de cargos sobre los hechos ocurridos el 18 de septiembre del año 2015 con la explosión de un petardo panfletario. Le puede interesar:["Mateo Gutiérrez León sería víctima de otro falso positivo judicial"](https://archivo.contagioradio.com/mateo-gutierrez-leon-seria-victima-de-falso-positivo-judicial/)

### **Carta de Miguel Ángel Herrera** 

*UNA SEMBLANZA DEL ESTUDIANTE*

*MATEO GUTIÉRREZ LEÓN.*

*Durante la semana que termina se viene haciendo mención a dos estudiantes de sociología de la Universidad Nacional. Yo conocí a uno de ellos, Mateo Gutiérrez León el semestre pasado, porque fue uno de los estudiantes que cursó conmigo la asignatura electiva dedicada al estudio del pensamiento de Antonio Gramsci.*

*A lo largo del semestre, el joven estudiante respondió a cabalidad con los compromisos académicos, y obtuvo por ello la calificación correspondiente. En ninguno de sus actos, Mateo mostró actuaciones desobligantes o un tratamiento que no fuera el normal y cordial de un estudiante con sus compañeros de clase y conmigo. Dispuesto siempre a contribuir con sus apreciaciones y comentarios a los asuntos que tratamos.*

*Antes de escribir estas líneas comenté con la monitora de mi curso, para tener certeza completa de lo que digo y pienso de este joven, su disposición para el estudio del curso, y el cuidado en atender a las obligaciones de clase.*

*Por lo dicho estoy sorprendido, y extrañado, de que lo llamen con el alias de Mateo, y lo señalen como responsable de actuaciones ilegales o criminales, y en lugar de estar asistiendo a sus clases, normalmente, tengan a Mateo Gutiérrez en una cárcel ahora.*

*Estoy, como tantos otros miembros de la comunidad universitaria de la Nacional, a la expectativa de que las autoridades precisen sus apreciaciones y sindicaciones que se han apresurado a hacer circular por los medios de comunicación.*

*Colombia, en ningún caso, y menos tratándose de un joven estudiante, quien de suyo piensa y debate los problemas del país en las clases regulares a las cuales asiste, puede ser objeto de la triste e insultante "doctrina Ñungo," donde lo que se presume es la culpa o el dolo, y no la inocencia de las personas, por parte de las autoridades.*

*No me cabe la menor duda, que las directivas de la Universidad Nacional, desde el Rector hasta los más inmediatos colaboradores, y las instancias Jurídica, de Bienestar, la facultad de Ciencias Humanas, y la carrera de Sociología, a la mayor bevedad, se pronunciarán como corresponde.*

*Tampoco dudo que asistirán a los dos estudiantes de sociología, quienes aparecen encartados de manera intempestiva, insólita, expuestos de manera irresponsable al amarillismo de los medios de comunicación.*

*La reunión de este lunes 27 de febrero, que vienen convocando los estudiantes de sociología, a través de las redes sociales, será oportunidad propicia para enterarnos a todos con el mayor detalle de la situación.*

*Para brindarle solidaridad a los jóvenes, y las correspondientes asistencias institucionales de parte de la Universidad Nacional de Colombia, mientras se determina la verdad de las imputaciones que se les endilgan, conforme con los procedimientos establecidos por la justicia penal y la guarda de los D.H. fundamentales.*

*Miguel Angel Herrera Profesor Universidad Nacional*

<iframe id="audio_17247891" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17247891_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
