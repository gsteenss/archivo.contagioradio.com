Title: "Buscamos la paz con todas sus complejidades, u optamos por la guerra con todo su dolor"
Date: 2019-01-21 13:18
Author: AdminContagio
Category: Paz, Política
Tags: ELN, guerra, paz, polarización
Slug: paz-o-guerra-con-dolor
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/UNIDOS-POR-LA-PAZ-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Ene 2019] 

Tras el reconocimiento por parte del Ejército de Liberación Nacional (ELN) de su autoría en el atentado con carro bomba del pasado jueves en la Escuela General Santander, el analista Víctor de Currea-Lugo afirmó que **el problema hoy no es si Gobierno y guerrilla se vuelven a sentar a negociar, sino hasta donde están dispuestos a ir cada uno para lograr la paz.** (Le puede interesar: ["Se desconoce el paradero de presos políticos del ELN en cárcel de Bellavista, en Medellín"](https://archivo.contagioradio.com/prisioneros-politicos-del-eln-se-encuentran-desaparecidos/))

El profesor y analista recordó que **entre el ELN y el Gobierno Duque no ha habido ni una sola reunión para plantearse directamente** sus posturas; entre tanto, el presidente ha tenido una posición intransigente, mientras que esta guerrilla ha tenido errores políticos al no leer la respuesta ciudadana frente a atentados como el ocurrido en 2017 en el barrio La Macarena de Bogotá, o contra un cuartel de la Policía en Barranquilla.

De Currea señaló que ambas partes en conflicto deben replantearse “a qué juegan”, para lograr sentarse en una mesa de negociación; situación que aclaró, “no es una cosa de días”; y aunque reconoció la necesidad de negociar para buscar la paz, sentenció que no era posible seguir “arando en el desierto”, por lo que los colombianos necesitan certezas: por parte del ELN, para que cese la violencia y del Estado Colombiano, para que avance en el sentido de la paz.

A la dificultad propia de un proceso de negociación entre partes que están en conflicto, el analista añadió la polarización en la ciudadanía, que se hizo presente durante la “marcha contra el terrorismo” que se realizó el domingo, en la que **manifestantes de distintas ciudades atacaron verbalmente y amenazaron a quienes también marchaban contra la muerte de líderes sociales y en favor de la paz**.

Esta situación también la analizó a través de su portal web, mediante un escrito en el que sentencia que dicha polarización tendrá que resolverse entre dos posturas que se evidencian en el país y resultan contrarias: buscar la paz, con los costos políticos que está tiene; o enfrentarse a la guerra, con el dolor y la violencia que dicha decisión supone. (Le puede interesar: ["Mujeres víctimas piden no cerrar la puerta del diálogo con el ELN"](https://archivo.contagioradio.com/queremos-la-paz-lideresas-comunitarias-piden-al-gobierno-no-cerrar-puerta-al-dialogo-con-eln/))

<iframe id="audio_31650342" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31650342_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
