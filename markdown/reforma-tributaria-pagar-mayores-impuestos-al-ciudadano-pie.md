Title: Reforma Tributaria hace pagar mayores impuestos al ciudadano de a pie
Date: 2016-10-27 16:57
Category: Economía, Nacional
Slug: reforma-tributaria-pagar-mayores-impuestos-al-ciudadano-pie
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Captura-de-pantalla-2016-03-17-a-las-11.10.22-a.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [27 Oct de 2016] 

En todo el país **organizaciones sindicales, sociales, docentes, entre otros, continúan exigiendo que la reforma tributaria no se apruebe**, pues ha sido considerada como regresiva y que perjudica a las personas que cuentan con menores ingresos. Le puede interesar: [Sindicatos rechazan reforma tributaria y convocan a paro general](https://archivo.contagioradio.com/sindicatos-rechazan-reforma-tributaria-y-convocan-a-paro-general/)

Frente a este tema, Fabio Arias presidente de la CUT  -Central Unitaria de trabajadores aseguró que **están movilizandose porque “esta reforma tributaria pone a pagar mayores tributos e impuestos al ciudadano de a pie,** al trabajador, a los que no tenemos mayor ingreso, ni mayores recursos económicos. Ahí está el tema del IVA, va a subir hasta el 19%”.

Contrario al tributo que deberán pagar las personas de menores ingresos económicos, Arias manifestó que “**los que más tienen, es decir las personas que tienen las grandes riquezas**, los grandes capitales, las multinacionales, el gran capital financiero **les van a reducir sus impuestos al pasarlos del 40 al 34% en el impuesto a la renta, por eso nos estamos movilizando**”.

Por último, **aseguró que están trabajando – el gremio sindical - en una serie de propuestas que van encaminadas a que no se afecte a los menos favorecidos de la sociedad colombiana** “una de nuestra propuesta va encaminada a exigir que la DIAN cumpla su papel de buscar a los evasores y que los órganos de control persigan a los ladrones del erario, con eso recuperaríamos extraordinarios recursos” y concluyó diciendo “no puede seguir exonerándose al gran capital, a las multinacionales que cada vez pagan menos impuestos, un ejemplo es el Cerrejón”.

Según lo han manifestado las centrales obreras, ya cuentan con un pliego de propuestas “serias” como las han denominado, y esperan que sean tenidas en cuenta por el Gobierno. Le puede interesar: [Sindicatos rechazan reforma tributaria y convocan a paro general](https://archivo.contagioradio.com/sindicatos-rechazan-reforma-tributaria-y-convocan-a-paro-general/)

<iframe id="audio_13507713" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13507713_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
