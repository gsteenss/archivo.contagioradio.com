Title: "Elección de rector de la Universidad Distrital es ilegítima" Estudiantes
Date: 2017-09-19 16:29
Category: Educación, Nacional
Tags: educacion, Universidad Distrital
Slug: eleccion-de-rector-de-la-universidad-distrital-es-ilegitima-estudiantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Universidad-Distrital-e1462569966410.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [19 Sept 2017] 

El próximo 5 de noviembre la Universidad Distrital conocerá a su nuevo rector, sin embargo, la polémica frente al sistema para escogerlo continúa, los estudiantes manifiestan que **debe generarse elecciones con candidatos que representen al estudiantado y que asuman la importancia de una educación pública, gratuita y de calidad**.

De acuerdo con Amaranta Delgadillo, representante de la Licenciatura en Ciencias Sociales, los estudiantes han acordado no apoyar a ningún candidato, debido a la ilegitimidad del proceso para escoger al rector, “el movimiento estudiantil se ha separado de la elección de rector, porque tenemos una postura clara y es **que no discutimos por la ilegitimidad de los candidatos, sino a la forma y el mecanismo de elegirlo**”.

Amaranta manifestó que actualmente los estudiantes esperan que la movilización estudiantil contra esta elección logre llamar la atención de la comunidad académica en su totalidad, para que no participe en esta elección y a través de escenarios más locales, como las representaciones estudiantiles, **se posiciones temas como la reforma a los estatutos de la universidad, que tanto a afectado a los alumnos.**

Frente al mecanismo que se está utilizando para realizar la elección del rector, los estudiantes alegan la falta de votación, debido a que ahora esta elección se realiza a través de postulaciones y análisis de aptitudes y capacidades que suman puntajes que finalmente deben superar 3 filtros, **una vez se llegue a la última instancia se realizará la votación en la comunidad universitaria, sin posibilidad de voto en blanco**.

Algunas de las personas que se han postulado para este cargo son Víctos Ándres Galves Ordóñez, que hace poco se presentó como candidato a la rectoria de la Universidad de Pamplona, en Norte de Santander y Ricardo García Duarte, quien ya  había sido rector de la Universidad Distrital y destituido en el 2003 por presunto peculado. (Le puede interesar:["Universidad Distrital dejaría de recibir más de 7 mil millones de pesos al año"](https://archivo.contagioradio.com/universidad-distrital-dejara-de-recibir-mas-de-7-mil-millones-de-pesos-al-ano/))

De igual forma existen dos condiciones que se han impuesto para acceder al cargo que ya fueron demandadas, la primera es que la persona que ocupe el cargo debe ser reconocido como investigador de COLCIENCIAS, y tan solo 2 personas han cumplido este requisito, y la segunda es que las personas que hagan parte de las directivas no podrán postularse, condición que ha sido demandada por el Sindicato de Trabajadores de la universidad, por **violar la constitución**.

<iframe id="audio_20970307" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20970307_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
