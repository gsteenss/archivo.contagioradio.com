Title: Debe haber un "constitucionalismo" para la paz: C Gonzáles
Date: 2017-06-01 15:18
Category: Entrevistas, Paz
Tags: Corte Consitucional, Fast Track
Slug: debe-haber-un-constitucionalismo-para-la-paz-c-gonzalez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/23_08_2015corte_constitucional-e1473979535347.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [01 Jun 2017] 

El Congreso de la República escogió Diana Fajardo como la nueva integrante de la Corte Constitucional, sin embargo, Camilo Gonzáles, analista político, señaló que los integrantes que conforman esta corte son cuotas políticas que **provienen de sectores tradicionales que han ejercido el poder en Colombia** y que en ese mismo sentido definirán los temas referentes al Acuerdo de Paz y su implementación.

Frente a la elección de Diana Fajardo, Gonzáles señaló que lo importante es que se comprenda que “s**e entienda que no estamos para la aplicación de un constitucionalismo tradicional, sino que se trata de garantizar el derecho a la paz**, y no quiere decir eso que se deba pasar por encima de cualquier consideración, pero sí que haya una proyección con sentido de la realidad política y el Acuerdo de Paz”.

“La conformación de la Corte, con los cupos que se están llenando, van a tener que establecer mucho de la constitucionalidad y de los proyectos de ley de los acuerdos, y hay mucha inquietud de que se pueda imponer como criterio la aplicación del parámetro de sustitución constitucional **que fue alegado para modificar las reglas del juego de votación para el Fast Track**” afirmó Gonzáles.

Además, el analista recalca que la máxima preocupación recae en que se vaya a hacer una modulación de los Acuerdos de La Habana que se les cambie el sentido y se re planteen desde su propio contenido, **a partir de las perspectivas políticas de cada uno de los integrantes de la Corte**. Le puede interesar: ["Elección de magistrado de la Corte Constitucional es clave para implementación del Acuerdo"](https://archivo.contagioradio.com/eleccion-de-magistrado-de-la-corte-constitucional-es-clave-para-implementacion-del-acuerdo/)

Sobre el avance de los Acuerdos de Paz, Gonzáles manifestó que una realidad es que lo que tendría que haberse formalizado constitucionalmente se culminó rápidamente con el paquete de decretos y las reformas que están en debate en curso en el Congreso. Sin embargo, **afirmó que queda una tarea grande para que se dé la institucionalización de los acuerdos y debe ser vigilada por la ciudadanía**.

“Las leyes son conquistas en el marco de los acuerdos de paz, dan posibilidades, pero el camino está por recorrerse, además no es toda la implementación, **la implementación se hace con medidas administrativas, con medidas concretas e iniciativas**” indicó Gonzáles. Le puede interesar: ["Corte Constitucional congela acuerdos con las FARC-EP: Enrique Santiago"](https://archivo.contagioradio.com/corte-constitucional-congela-acuerdo-de-paz-con-las-farc-enrique-santiago/)

<iframe id="audio_19029034" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19029034_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
