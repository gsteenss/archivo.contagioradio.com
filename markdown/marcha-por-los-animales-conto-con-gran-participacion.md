Title: ¡Marcha por los animales contó con gran participación!
Date: 2014-10-05 16:18
Author: CtgAdm
Category: Animales, Voces de la Tierra
Tags: Animales, Bogotá, derechos de los animales
Slug: marcha-por-los-animales-conto-con-gran-participacion
Status: published

El pasado 5 de octubre, se realizó la VII Marcha Mundial por los Derechos de los Animales. El evento se trató de una gran manifestación de los colombianos y colombianas alrededor de todo el país, demostrando que está en favor de la vida y los derechos de los animales como ser seres sintientes.

En Bogotá, Las personas se reunieron a las 9 de  la mañana en la Plaza de ToDos de la Santamaría, y desde las 10 de la mañana, empezaron el recorrido hacia la Plaza de Bolívar, donde gracias a labor de diferentes organizaciones animalistas como Plataforma Alto, Resistencia Natural, Defenzoores, Fundación Franz Weber, ADI, Agenda Bogotá, entre otras, de la mano del distrito, trabajaron en conjunto para la logística del evento, donde un escenario recibiría a los manifestantes con un gran concierto.

Latin Latas, Chucho Merchán, Contra la Pared, Doctor Krápula, entre otros artistas fueron los protagonistas del concierto para aproximadamente 20.000 hombres, mujeres, niños y niñas, que acompañados de sus mascotas, decidieron decirle NO a las corridas de toros, las peleas de perros, las peleas de gallos, las cabalgatas, caballos cocheros, tráfico de fauna silvestre, zoológicos, etc. Donde se vulnera los derechos de los animales, debido a que la legislación colombiana sigue viendo a los demás seres vivos como simples objetos.

Desde Onda Animalista, agradecemos a todos y todas por haber hecho parte de esta marcha y demostrarle al gobierno y a la Corte Constitucional, que somos más los que exigimos una legislación más justa con los animales, pues este podría ser un primer paso si realmente queremos empezar a hablar de paz, ya que las víctimas no son solo humanas, también son los animales y así mismo sus hábitats.
