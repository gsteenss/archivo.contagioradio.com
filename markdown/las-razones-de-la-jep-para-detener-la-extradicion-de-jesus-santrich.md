Title: Las razones de la JEP para detener la extradición de Jesús Santrich
Date: 2018-05-17 11:47
Category: Paz, Política
Tags: acuerdo de paz, extradición jesus santrich, JEP, Jesús Santrich
Slug: las-razones-de-la-jep-para-detener-la-extradicion-de-jesus-santrich
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Jesus-Santrich-en-entrevista-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [17 May 2018] 

En un comunicado, la Jurisdicción Especial para la Paz ordenó la **suspensión del trámite de extradición** en contra de Jesús Santrich. Afirmó que la justicia especial “asume competencia para conocer de la solicitud de aplicación de garantía de no extradición” teniendo en cuenta la protección del derecho a la verdad y la reparación de las víctimas. Santrich se encuentra detenido desde el 10 de abril de 2018 y realiza una huelga de hambre que completa 40 días.

Seuxis Paucias Hernández Solarte, es señalado de haber cometidos delitos de **narcotráfico** luego de haberse firmado el Acuerdo de Paz entre las FARC y el Gobierno Nacional por lo que fue detenido en Bogotá. Cabe resaltar que el 19 de abril de 2018, la Sección de Revisión de la JEP decidió dar inicio “a una fase previa que tenía como propósito establecer la existencia de un trámite de extradición” de Jesús Santrich.

### **JEP solicitó pruebas en el caso contra Santrich** 

La JEP indicó que tuvo en cuenta la petición presentada por el integrante del partido político Fuerza Revolucionaria Alternativa del Común, sobre la aplicación de la garantía de **no extradición** contenido en el artículo transitorio 19 del Acto Legislativo No. 001 de 2017. (Le puede interesar:["Piden a Jesús Santrich que abandone la huelga de hambre y siga trabajando por la paz"](https://archivo.contagioradio.com/ivan-cepeda-y-alvaro-leyva-le-pidieron-a-jesus-santrich-termine-su-huelga-de-hambre/))

Afirmó la institución que se corre el traslado por un periodo de 10 días “para que los intervinientes **soliciten las pruebas que consideren necesarias**” y así continuar con el desarrollo del caso en Colombia. Además, “ordenó al Ministerio de Relaciones Exteriores, que una vez reciba la solicitud formal de extradición, remita copia de la misma de manera inmediata ante la Sección para lo de su competencia”.

La decisión fue tomada teniendo presente el principio de **integralidad** que rige el Sistema Integral de Verdad, Justicia, Reparación y No Repetición y “para proteger y satisfacer los derechos de las víctimas”. Aclaró que no hay elemento de juicio suficientes para pronunciarse a la petición de nulidad de la medida de aseguramiento que solicitó Santrich por lo que continuará detenido.

Finalmente, la JEP precisó que la determinación estuvo ligada a la recolección de información y documentación que fue solicitada a los entes gubernamentales y judiciales que han tenido alguna relación con la extradición de Santrich.

[Comunicado 30 Sobre Extradición Caso Santrich](https://www.scribd.com/document/379499549/Comunicado-30-Sobre-Extradicio-n-Caso-Santrich#from_embed "View Comunicado 30 Sobre Extradición Caso Santrich on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_96453" class="scribd_iframe_embed" title="Comunicado 30 Sobre Extradición Caso Santrich" src="https://www.scribd.com/embeds/379499549/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-LaBy9PDOSA4HV13loWCZ&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7055599060297573"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
