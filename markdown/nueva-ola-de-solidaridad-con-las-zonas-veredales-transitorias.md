Title: Nueva ola de solidaridad con las Zonas Veredales Transitorias
Date: 2017-03-02 15:24
Category: Nacional, Paz
Tags: paz, Zonas Veredales Transitorias de Normalización
Slug: nueva-ola-de-solidaridad-con-las-zonas-veredales-transitorias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_2018.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [2 Mar 20217] 

Una nueva jornada de solidaridad se vivirá este fin de semana en la zona veredal de Vista Hermosa, en Meta. Diferentes organizaciones sociales irán en caravana para llevar elementos recolectados y donados por la ciudadanía con la campaña **¡Venga esa mano por la paz!, como útiles escolares, ropa, objetos para la salud y el cuidado de los bebés y para las y los guerrilleros.**

De acuerdo con Luis Emil Sanabria director de Redepaz, una de las organizaciones que se encuentra recolectando las donaciones, esta es una campaña que ha permitido que la sociedad Colombiana se avoque a la defensa de la implementación de los Acuerdos de Paz, **“todos tenemos algo que dar algo, una mano amiga que ofrecerle a los excombatientes que van a ser muy pronto reincorporados en la sociedad”.**

Sin embargo, pese a las donaciones que ya se han recibido, Sanabria comenta que aún se necesitan muchas colaboraciones en las diferentes zonas veredales para garantizar unas buenas condiciones en las que los y las guerrilleros puedan hacer su proceso de reincorporación. Entre la lista de cosas que más se necesita están: **elementos de aseo personal para bebes y adultos, ropa en buen estado, libros y útiles escolares, pinturas y materiales artísticos.**

En ese sentido, también recalca que no todas las zonas veredales están en las mismas condiciones, “**los puntos que se encuentran  en Antioquia, al ser las zonas más alejadas ha existido mayor dificultad para el traslado de materiales y elementos”,** motivo por el cual, indica que se están evaluando esos requerimientos para así mismo colaborar en la necesidades. Le puede interesar: ["Venga esa mano por la paz"](https://archivo.contagioradio.com/venga-esa-mano-por-la-paz/)

Referente a la organización de las caravanas para que las personas puedan colaborar en las zonas veredales, Sanabria afirma que **se están gestionando los permisos concernientes con el gobierno y las FARC-EP**, para lograr visitar más zonas veredales. Le puede interesar: ["Zonas veredales contarán con bibliotecas y ludotecas para la paz"](https://archivo.contagioradio.com/zonas-verdales-contaran-con-biblioteca-y-ludoteca-para-la-paz/)

En Bogotá las donaciones pueden hacerse en la carrera **10 \# 19-65, en el Edificio de Camalacol, o en la transversal 28 a \#37-75**. De igual modo, las organizaciones están evaluando la posibilidad de abrir puntos de recolección en otras ciudades, **Villavicencio tiene como punto de recolección la calle 35\#20e-40 Casa 1.**

<iframe id="audio_17322062" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17322062_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
