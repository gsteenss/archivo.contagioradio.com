Title: Universidad Distrital dejará de recibir más de 7 mil millones de pesos al año
Date: 2017-09-15 16:10
Category: Educación, Nacional
Tags: educacion, educación pública, Universidad Distrital, Universidad Nacional
Slug: universidad-distrital-dejara-de-recibir-mas-de-7-mil-millones-de-pesos-al-ano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Universidad-Distrital-e1462569966410.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [15 Sept. 2017] 

La Alianza Verde, en cabeza del concejal Antonio Sanguino, han denunciado que la Comisión de Hacienda del Concejo de Bogotá aprobó una estampilla con una tarifa del 1,1% sobre todos los contratos que celebre la ciudad, porcentaje que según el cabildante es un recaudo tacaño que **reduce el presupuesto para la Universidad Distrital en \$7.666 millones de pesos al año.**

La disposición de la Comisión de Hacienda es que del recaudo total **se destine el 70% para la Universidad Distrital y 30% para la Universidad Nacional,** lo que hace que de un 100% que recibía anteriormente, ahora solo recibirá el 70%. Le puede interesar: [Predios de la Universidad Nacional quedarían en manos de inmobiliaria privada](https://archivo.contagioradio.com/predios-de-la-universidad-nacional-quedarian-en-manos-de-inmobiliaria-privada/)

Manifiesta Antonio Sanguino que si bien es positivo invertir en la Universidad Nacional “porque todos sabemos que se está derrumbando pues no podemos reducir la inversión para la Distrital. **Lo que hicimos un sector de concejales fue insistir en que esa estampilla no se aplicara sobre el 1% sino que fuera hasta el 2%** y luego propusimos un 1.5% y la Secretaria de Educación se amangualó con la Comisión de Hacienda y solo permitieron el aumento de 1,1%”.

### **¿Qué implica que la Universidad Distrital dejé de recibir más 7 mil millones?** 

Manifiesta el Concejal que habría menos aulas, menos capacidad para atender la demanda de educación pública superior en Bogotá la cual no puede garantizar a cerca de 40 mil jóvenes al año el acceso a la educación universitaria “la Universidad Distrital es el principal instrumento para ofertar educación superior en Bogotá”.

**Un estudiante de la Universidad Distrital le puede estar costando \$10 millones de pesos** al año y con esta reducción de la inversión podría estar significando, por ejemplo, que la Universidad Distrital de la localidad de Bosa no pueda generar 5 mil nuevos cupos de educación superior en Bogotá.

### **¿A quiénes beneficia esta tasa del 1.1%?** 

Para Sanguino quienes se beneficiarán de esta decisión son los grandes contratistas de la ciudad **“la administración distrital ha dicho que el 1.5% genera un impacto fiscal sobre la ciudad,** pero más impacto fiscal sobre la ciudad produce un cupo de endeudamiento de 2.4 billones de pesos como el que les fue aprobado en el Consejo de la ciudad. Por eso me parece incoherente que se alegue el impacto fiscal”.

### **Hay que persuadir a los concejales para evitar la reducción** 

Frente a esta decisión lo que queda por hacer es una “pataleta” dice Sanguino, persuadiendo a los concejales y bancadas en la plenaria del Consejo para que corrijan esta decisión de la Comisión de Hacienda de modo que se pueda incrementar el valor de la estampilla. Le puede interesar: [En la Universidad Distrital ganó la inconformidad con el voto en blanco](https://archivo.contagioradio.com/en-vilo-la-rectoria-de-la-udistrital-tras-el-triunfo-del-voto-en-blanco/)

**“De tal suerte que por lo menos garanticemos que la Distrital reciba lo que ya está recibiendo** y no pierda recursos por la aplicación de la nueva estampilla. Pero ya sabemos cómo se comportan las mayorías en estas corporaciones públicas sobre todo cuando son mayorías que están entregadas a las decisiones de la administración distrital”.

<iframe id="audio_20905827" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20905827_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
