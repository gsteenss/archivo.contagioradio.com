Title: "Ataques contra las universidades tienen fines políticos" Miguel Angel Beltrán
Date: 2015-06-09 11:34
Category: Educación, Entrevistas
Tags: Acuerdo 2034, Derechos Humanos, educacion, estudiantes, ley 30, mane, miguel angel beltran, presos, prisioneros políticos, sociologo
Slug: ataques-contra-las-universidades-tienen-fines-politicos-miguel-angel-beltran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Captura-de-pantalla-2015-06-09-a-las-11.32.35.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

######  

###### [may 2015] 

Entrevista con el profesor, sociólogo e historiador Miguel Ángel Beltrán en que habla sobre su proceso, las políticas públicas de educación en Colombia, y la situación de los defensores de Derechos Humanos en Colombia.

\[embed\]https://www.youtube.com/watch?v=HVqNfA\_g-T8\[/embed\]
