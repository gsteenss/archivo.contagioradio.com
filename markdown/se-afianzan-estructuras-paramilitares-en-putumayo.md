Title: Se afianzan estructuras paramilitares en Putumayo
Date: 2016-02-05 18:18
Category: DDHH, Nacional
Tags: Paramilitares en Putumayo
Slug: se-afianzan-estructuras-paramilitares-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/paramilitarismo-e1454090118583.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Notimercia ] 

###### [5 Feb 2016 ] 

De acuerdo con la 'Comisión Intereclesial de Justicia y Paz' persisten las operaciones de tipo paramilitar en Puerto Asis, Putumayo. Esta semana han circulado **panfletos en los que se anuncian asesinatos selectivos de hombres y mujeres** a quienes acusan de ser “ladrones, expendedores de drogas y prepagos”, así mismo advierten a la población que no puede salir después de las 10:00 p.m y que no pueden movilizarse por ciertas zonas.

Esta situación ha sido denunciada por las comunidades ante las entidades del Estado, quienes **continúan negando esta realidad y no han procedido con acciones tendientes a neutralizar las acciones paramilitares** que se han venido presentando desde el año pasado cuando surgieron amenazas de tipo paramilitar en Orito y Puerto Asís, y se registró una incursión contra una comunidad del Pueblo Nasa que se oponía a la ampliación petrolera, sin que las unidades militares hubieran actuado pese a la cercanía al lugar de los hechos.

Ante la falta de resultados efectivos contra este tipo de estructuras, pese a la fuerte militarización del departamento y los millonarios convenios firmados entre las Fuerzas Militares y las petroleras, **estos hechos generan preocupación e incertidumbre entre la población ya que persisten acciones e intimidación contra los civiles**.

Conozca el panfleto:

[![12660252\_10209039657247719\_1005853891\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/12660252_10209039657247719_1005853891_n.jpg){.aligncenter .wp-image-20118 width="478" height="554"}](https://archivo.contagioradio.com/se-afianzan-estructuras-paramilitares-en-putumayo/12660252_10209039657247719_1005853891_n/)
