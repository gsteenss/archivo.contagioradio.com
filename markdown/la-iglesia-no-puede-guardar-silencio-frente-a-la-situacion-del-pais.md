Title: "La iglesia no puede guardar silencio frente a la situación del país"
Date: 2020-07-13 11:57
Author: CtgAdm
Category: Nacional
Slug: la-iglesia-no-puede-guardar-silencio-frente-a-la-situacion-del-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Dario-Monsalve.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Desde representantes de la iglesia, la primera semana del mes de junio estuvo rodeada de diferentes anuncios, entre ellos la afirmación del Arzobismo de Cali, sobre una *"venganza genocida contra el proceso de paz"* , la replica de la Nunciatura Apostólica a este y lo que algunos denominaron como una división del sector religioso en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunos días después Monseñor Monsalve, y a pesar de que el vocero en Colombia del Vaticano señaló que esta afirmación era mal usada y no representaba el parecer de la iglesia frente a las acciones del gobierno; Monsalve [mantiene su afirmación](https://www.justiciaypazcolombia.com/arzobispo-monsalve-persiste-en-criticas-a-gobierno-por-su-actitud-negativa-frente-a-acuerdo-de-paz/), entorno al asesinato de líderes sociales y excombatientes, y además asegura que ***"la iglesia no puede guardar silencio frente a la situación del país".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y pese a que las posturas de los dos representantes resultan ser divergentes, en algunas entrevistas **Monseñor Montemayor, nuncio apostólico nombrado** hace diecinueve meses por el papa Francisco, **coincidido en que** ***"resulta desalentador ver los pocos avances en materia de diálogo de paz con el ELN**".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto luego del llamado que hizo el[Consejo de Seguridad de la ONU](https://archivo.contagioradio.com/consejo-de-seguridad-de-la-onu-llama-a-un-cese-al-fuego-global/) a un cese al fuego global, durante 90 días, al cual el ELN propuso al Gobierno nacional unirse, con el fin de crear ***"un clima de distensión humanitaria, favorable para reiniciar los Diálogos de Paz"*, invitación a la que el Gobierno se negó.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La iglesia no puede desconocer el aumento de la violencia en Colombia"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Resaltando que entiende la frustración de Mons. Monsalve luego de 44 años de trabajo riguroso por la paz, ante un panorama de claro retraso en el Proceso de Paz; el Nuncio afirmó en una entrevista que este no debe ser motivo para culpar al Gobierno señalando que este tiene una mala intención.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Respeto lo que dice el nuncio y otros religiosos de otras comunidades, pero en este punto tenemos que ser nosotros inflexibles"*
>
> <cite>Arzobispo| Oscar Dario Monsalve</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

**Y agregó que esta es una tarea conjunta en donde "la sociedad tiene que poner su parte, no todo lo hace el Gobierno, esto es una democracia"**, extendiendo el llamado a los sectores privados, a las comunidades, los Gobernadores y Alcaldes, para que se movilicen, exijan, argumenten y participen en este proceso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último ante el llamado al cese bilateral el Nuncio afirmó que se debe esperar que puede pasar, pero que por su parte **la comunidad internacional *"desearía un cese del fuego bilateral de 90 días, por razones humanitarias***", y agregó que es probable que la guerra continué, pero esto, *"no tiene por qué golpear al pueblo y a las regiones más pobres".*

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/otramirada-en-colombia-se-habla-de-genocidio/751932185643838/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/otramirada-en-colombia-se-habla-de-genocidio/751932185643838/

</div>

</figure>
<!-- /wp:core-embed/facebook -->
