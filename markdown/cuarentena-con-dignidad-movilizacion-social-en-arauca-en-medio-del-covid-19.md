Title: 'Cuarentena con dignidad', movilización social en Arauca en medio del Covid-19
Date: 2020-06-04 21:46
Author: CtgAdm
Category: Nacional
Slug: cuarentena-con-dignidad-movilizacion-social-en-arauca-en-medio-del-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/b199a179-1421-4e54-86d8-3bcc4c2c7c1b.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/ba848f9e-6842-46d2-b405-3e35c7967116.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Arauca/ Movimiento Político de Masas Social y Popular del Centro Oriente de Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio del aislamiento preventivo en los territorios, organizaciones sociales y gremios en las zonas rurales han encontrado diferentes formas de hacer resistencia y contribuir a la movilización social a través de ayudas humanitarias, Arauca departamento en el que únicamente se ha presentado un caso de Covid-19 hasta la fecha, se ha convertido en una región que a pesar de las judicializaciones contra líderes sociales, ha decidido buscar el bienestar de las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para Sonia López, dirigente social y vocera del [Movimiento Político de Masas Social y Popular del Centro Oriente de Colombia](http://centroriente.org/), la pandemia ha puesto en evidencia la crisis social, humanitaria y ambiental que se padecía desde **antes del Covid-19 y la ausencia de garantías de un Gobierno que va "en contra vía de los territorios y la soberanía de los pueblos".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que el Estado ha hecho entrega de ayudas humanitarias durante el primer mes de la pandemia, Sonia resalta que esta ayuda ha sido insuficiente por lo que desde **la guardia interétnica y campesina** se han empezado a realizar acciones autónomas que incluyen los controles de bioseguridad y lecciones de pedagogía junto a las comunidades [(Le recomendamos leer: Ejército Nacional disparó contra Guardia Campesina en Arauca)](https://archivo.contagioradio.com/ejercito-nacional-agrede-a-guardia-campesina-en-arauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, las organizaciones sociales vienen impulsando las huertas urbanas y recolección de alimentos para ser entregados a las familias más vulnerables en los barrios y veredas, "**han sido los campesinos los que nos han salvado la patria, atentos a la solidaridad a través de la donación de los productos**, también hay que reconocer el trabajo que realizan los transportadores de carga para poder trasladar estas ayudas", relata la dirigente social.

<!-- /wp:paragraph -->

<!-- wp:image {"id":85059,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/b199a179-1421-4e54-86d8-3bcc4c2c7c1b-1024x491.jpg){.wp-image-85059}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Las acciones incluyen la interolocución con entes gubernamentales para cumplir con acuerdos pactados producto de la movilización, entre ellos el fortalecimiento de la red pública hospitalaria, pues aunque Arauca cuenta con hospitales de primer nivel, no cuentan con las condiciones técnicas necesarias para afrontar la pandemia, en caso de que los contagios aumenten en esta fase de aislamiento que permite la salida de miles de personas a las calles.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Otras acciones incluyen fortalecer la educación virtual, pues aunque esta se ha generalizado en muchas ocasiones **"no se tiene en cuenta que territorios como el nuestro son de zonas rurales y no cuentan con la tecnología necesaria para participar de forma eficiente".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sonia Lopez plantea que existe una doble moral del Gobierno que por un lado expresa preocupación por la salud de los colombianos y expide medidas para evitar la propagación del virus, sin embargo resalta que esas prohibiciones a la movilidad únicamente son aplicadas a "las clase menos favorecida porque se está obligando a la gente a quedarse en la casa, o me protejo o salgo a la calle porque sino me muero de hambre es la situación que se está viviendo" [(Le puede interesar: Denuncian nuevo falso positivo judicial contra líder José Vicente Murillo, en Arauca)](https://archivo.contagioradio.com/denuncian-nuevo-falso-positivo-judicial-contra-lider-jose-vicente-murillo-en-arauca/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Pese al confinamiento las judicializaciones en Arauca continúan

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Después de la judicialización en 2019 del líder social José Vicente Murillo, capturado por la Fuerza pública en el municipio de Saravena, Arauca y en febrero de este año contra otros cuatros dirigentes comunales, Sonia manifiesta que, incluso en cuarentena, se reiteran las agresiones, esta vez con la captura de más dirigentes el pasado 22 de mayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Se trata de las líderes comunales Primitiva Becerra presidenta de la Junta de Acción Comunal de la vereda Río Colorado y Marien Elena Villamizar Vera tambien líder comunal de la vereda Chucarima, municipio Chitagá, en Norte de Santander y los campesinos Humberto Villamizar Calderón también comunal, y Carlos Alberto Mogollón, todos "en el marco de una estigmatización de pertenecer a una organización rebelde de la que hacen presencia en los territorios", sin embargo, afirma López se trata de una intención de desarticular el tejido social [(Lea también: Si no nos matan nos judicializan: lideresa de Arauca)](https://archivo.contagioradio.com/si-no-nos-matan-nos-judicializan-lideresa-de-arauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente a las judicializaciones, Sonia resaltó que la comunidad carcelaria viene exigiendo garantías en medio de la situación que se vive al interior de los centros penitenciarios, a propósito de los casos de José Vicente Murillo y Jorge Enrique Niño, líderes que permanecen reclusos en La Picota, a kilómetros de distancia de su tierra natal. "Uno creería que por situaciones humanitarias el Estado debería estar concentrado en paliar la crisis pero está en función de los intereses de clase", expresó. [(Lea también: Covid-19 y crisis carcelaria colombiana)](https://archivo.contagioradio.com/covid-19-y-crisis-carcelaria-colombiana/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Señala además que en territorios como Arauca, mientras las personas permanecen en casa, empresas petroleras como la empresa Occidental (Oxy), continúan desarrollando sus actividades, transportando gente de otras regiones, despidiendo de manera injusta a sus trabajadores y exponiendo a las comunidades al Covid-19.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
