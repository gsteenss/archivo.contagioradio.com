Title: Es asesinado Jorge Solano, reconocido líder social de Ocaña
Date: 2020-11-03 14:38
Author: AdminContagio
Category: Nacional
Tags: Líder Social, Norte de Santander, ocaña
Slug: es-asesinado-jorge-solano-lider-social-de-ocana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/BuscarlasHastaEncontrarlas-4.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/BuscarlasHastaEncontrarlas-4-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: [@NotiOcana](https://twitter.com/NotiOcana)*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este martes 3 de noviembre sobre las 2:00pm el líder social, **Jorge Solano fue asesinado en la ciudad de Ocaña, Norte de Santander.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/catatumbol281/status/1323702831078121473","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/catatumbol281/status/1323702831078121473

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según medios de comunicación locales, **Jorge Solano Vega, fue abordado para hombres armados quienes le dispararon reiteradas ocasiones** dejándolo gravemente herido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho se registró mientras el líder Jorge Solano **iba saliendo de su vivienda en el sector de Miraflores, municipio de Ocaña**, Norte de Santander, lugar en donde minutos después falleció.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Solano **era un reconocido líder social, defensor de víctimas y veedor ciudadano** en esta ciudad, además de integrante de la organización de Derechos Humanos, Tierra y Vida. ([«Violencia contra líderes sociales se concentró en 29 de los 32 departamentos»](https://archivo.contagioradio.com/violencia-contra-lideres-sociales-se-concentro-en-29-de-los-32-departamentos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Instituto de Estudio para el Desarrollo y la Paz (Indepaz), con Jorge Solano son 250 líderes y defensores de DDHH asesinados en Colombia durante este 2020.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
