Title: Víctimas de masacre de Curuguaty condenados a 30 años de prisión
Date: 2016-07-12 12:41
Category: DDHH, El mundo
Tags: condenas contra campesinos paraguay, curuguaty, masacre de curuguaty, paraguay
Slug: victimas-de-masacre-de-curuguaty-condenados-a-30-anos-de-prision
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Curuguaty.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Izquierda Diario ] 

###### [12 Julio 2016] 

Este lunes se conocieron las sentencias en el marco de la investigación que cursa contra 11 campesinos víctimas de la masacre de Curuguaty, quienes fueron juzgados por homicidio doloso, asociación criminal e invasión de inmueble ajeno, con penas de entre 30 y 4 años de prisión. **Diversas organizaciones sociales se han pronunciado en rechazo a esta decisión porque consideran que los jueces no fueron imparciales**.

El Tribunal de Sentencia que juzga el caso declaró culpables a 11 campesinos, cuatro de los que fueron sentenciados por homicidio doloso agravado, asociación criminal e invasión de inmueble ajeno. El campesino **Rubén Villalba fue condenado por homicidio a 30 años de prisión**, mientras que Luis Olmedo fue sentenciado con 20 años y Néstor Castro junto con Arnaldo Quintana a 18 años, por coautoría de homicidio.

Tres campesinas fueron condenadas a 6 años de prisión por ser consideradas cómplices de homicidio y **los demás acusados fueron sentenciados a 4 años**, **por asociación criminal e invasión de inmueble ajeno**. Líderes campesinos rechazan la sentencia y aseguran que seguirán luchando y manifestándose para exigir los derechos de los más empobrecidos.

El 15 de junio de 2012 decenas de familias campesinas de Curuguaty intentaron ocupar las tierras que estaban siendo apropiadas por empresarios, cuando integrantes de la Fuerza de Operaciones de la Policía Especializada [[perpetraron una masacre en su contra](https://archivo.contagioradio.com/entre-5-y-40-anos-de-prision-pagaran-campesinos-victimas-de-la-masacre-de-curuguaty/)], que dejó como resultado **once campesinos asesinados, seis policías muertos** y la destitución parlamentaria de Fernando Lugo.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en [[Otra Mirada](http://bit.ly/1ICYhVU)] por Contagio Radio] 
