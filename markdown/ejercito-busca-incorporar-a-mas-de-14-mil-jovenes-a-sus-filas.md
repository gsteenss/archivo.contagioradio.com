Title: Ejército busca incorporar a más de 14 mil jóvenes a sus filas
Date: 2020-02-14 18:35
Author: CtgAdm
Category: DDHH, Nacional
Tags: Fuerza Pública, jovenes, reclutamiento
Slug: ejercito-busca-incorporar-a-mas-de-14-mil-jovenes-a-sus-filas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Ejército-busca-reclutar-a-14-mil-jóvenes-e1581723243225.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @RECLUTAMIENTOCO {#foto-reclutamientoco .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/alvaro-pena-sobre-incorporacion-jovenes-a-ejercito-audios-mp3_rf_47927534_1.html&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Alvaro Peña | abogado activista por la objeción de conciencia

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Hasta el próximo 17 de febrero, el Ejército busca incorporar a más de 14 mil jóvenes a sus filas, en el marco de su primera jornada de reclutamiento del año. Según la Defensoría del Pueblo, el 90% de los reclutados pertenecerán al estrato 1, 2 y 3, aunque la Ley no dispone que así sea. (Le puede interesar: ["Menos del 10% de quienes presentan el ICFES se gradúan de la Universidad"](https://archivo.contagioradio.com/menos-icfes-graduan-universidad/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ACOOCobjecion/status/1224790887412248577","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ACOOCobjecion/status/1224790887412248577

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **Los motivos para pensar que se cometen irregularidades en el reclutamiento por parte del Ejército**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Alvaro Peña, abogado activista por la objeción de conciencia declaró que es común que los gobiernos, a pesar del Acuerdo de Paz, sigan con una lógica militarista que busca robustecer el pie de Fuerza mediante jóvenes de sectores excluidos, que asumen las consecuencias de dicha política. En ese sentido, recordó que según la Defensoría, un 90% de los reclutados son de los estratos 1,2 y 3, aunque la Ley de reclutamiento no hace distinción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, señaló que también se han rastreado irregularidades en el proceso de incorporación de la Fuerza Pública, por ejemplo, en el trato que se da a los jóvenes que son padres de familia: Sin un joven es padre de familia y culminó el bachillerato, usualmente es exonerado; pero si es padre de familia y no culminó sus estudios de secundaria, suele ser llevado a prestar el servicio militar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta situación, la ya referida de preferencia por jóvenes con bajo o medio nivel económico y otras, Peña sostuvo que es posible pensar "que las Fuerzas Militares están cometiendo irregularidades en el reclutamiento de los jóvenes, que podrían asimilarse a las prácticas que hacen otros actores armados". (Le puede interesar: ["En Colombia los niños crecen hablando de muerte: Oxfam"](https://archivo.contagioradio.com/en-colombia-los-ninos-crecen-hablando-de-muerte-oxfam/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Prestar el servicio militar no es obligatorio, su deber es resolver su situación militar**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Abogado declaró que es importante que los jóvenes sepan que la Ley de reclutamiento contempla que se debe definir la situación militar, no que es obligatorio prestar el servicio militar. En consecuencia, si no se quiere prestar el servicio militar se puede acudir a las 12 causales establecidas en la Ley 1861 de 2017 para no ser incorporado a la Fuerza Pública, o declararse como [objetor de conciencia](https://archivo.contagioradio.com/cuatro-pasos-para-ser-un-objetor-de-conciencia-en-colombia/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, expresó que lo que se conoce como batidas es un procedimiento ilegal, que la [Corte Constitucional definió como "detención arbitraria con fines de reclutamiento"](https://www.colectivodeabogados.org/?Sentencia-de-la-Corte-Constitucional-sobre-Batidas-Ilegales-T-455-14), y exhortó al Ejército a no volver a realizar este tipo de prácticas. Por lo tanto, si una persona resulta conducido a una unidad militar en medio de una batida, puede acudir a organizaciones defensoras de DD.HH., para denunciar su caso ante un juez.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Peña concluyó que incluso, los jóvenes pueden acudir a la Procuraduría para interponer una queja sobre el uniformado que cometa esta conducta, así como pedir a la Personería y la Defensoría del Pueblo que les sean resarcidos sus derechos. (Le puede interesar: ["La Objeción de Conciencia, un derecho ignorado en Colombia"](https://archivo.contagioradio.com/objecion-conciencia-derecho-ignorado/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
