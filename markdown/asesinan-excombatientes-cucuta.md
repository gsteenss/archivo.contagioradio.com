Title: Asesinan a excombatientes Miltón Urrutía y José Peña en Cúcuta
Date: 2019-09-08 12:21
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato, Cucuta, excombatientes, Sicario
Slug: asesinan-excombatientes-cucuta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Excombatientes-e1567187945860.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Este viernes en horas de la mañana fueron asesinados en Cúcuta, Norte de Santander, Milton Urrutia Mora y José Milton Peña Pineda, excombatientes que llevaron adelante su proceso de reincorporación en el Espacio Territorial de Capacitación y Reincorporación (ETCR) de Caño Indio. Los hombres se encontraban en un hotel, junto al también excombatiente Arsenio Maldonado, cuando fueron víctimas de un ataque sicarial, del que sólo logró sobrevivir Maldonado.

Según las primeras informaciones, los tres hombres acostumbraban encontrarse cada 12 días en un hotel de la ciudad; fue mientras desarrollaban esa reunión que un hombre armado atentó en diferentes ocasiones contra ellos, huyendo en motocicleta. Los excombatientes no contaban con esquemas de protección ni medidas de seguridad; de la acción salió herido Arsenio Maldonado, quien tuvo que ser trasladado a un centro de atención hospitalario. (Le puede interesar:["La paz no la detiene nadie, afirman excombatientes de FARC"](https://archivo.contagioradio.com/la-paz-no-la-detiene-nadie-afirman-excombatientes-de-farc/))

### **Jackson Mena, excombatiente asesinado en Quibdó**

En horas de la noche del mismo viernes, fue asesinado en el barrio Las Margaritas de Quibdó, Chocó, Jackson Mena, excombatiente de 33 años que estaba llevando adelante su proceso de reincorporación y se había convertido en líder del barrio El Porvenir. Según [el delegado del partido FARC ante el Consejo Nacional de Reincorporación (CNR), Pator Alape Lascarro, Mena había solicitado medidas de protección ante la Unidad Nacional de Protección (UNP), pero estas no habían sido resueltas.]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

### **Más de 140 excombatientes asesinados** 

El pasado 24 de julio se había denunciado el asesinato de Carlos Alberto Montaño, en Cali (Valle del Cauca). Montaño había sido preso político de FARC, pero gracias al Acuerdo de Paz había recuperado su libertad y se encontraba trabajando en un taller de soldadura. Junto a Carlos, Milton, José y Jackson se cuentan más de 140 excombatientes asesinados luego de dejar las armas en diciembre de 2016. (Le puede interesar:["Asesinan a excombatiente de Farc, Carlos Alberto Montaño en Cali"](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-farc-carlos-alberto-montano-en-cali/))

Noticia en desarrollo.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
