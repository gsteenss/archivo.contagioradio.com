Title: En 2019 se registraron 25.303 nuevos desplazados en Colombia: CICR
Date: 2020-03-04 16:12
Author: AdminContagio
Category: Actualidad, DDHH
Tags: CICR, comunidades, Derechos Humanos, humanitario
Slug: en-2019-se-registraron-25-303-nuevos-desplazados-en-colombia-cicr
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/CICR-Fernando-Forero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Fernando Forero, CICR {#foto-fernando-forero-cicr .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este miércoles 4 de marzo el Comité Internacional de la Cruz Roja (CICR) presentó su balance en materia humanitaria del año 2019, señalando un deterioro en las condiciones de vida de los ciudadanos en departamentos como Chocó, Cauca, Antioquia, Arauca, Norte de Santander y Córdoba. En el balance se alertó ante el aumento de víctimas de artefactos explosivos, y la persistencia en las cifras de personas desaparecidas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Confinamientos, desplazamientos, violaciones al DIH y desapariciones**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En 2019, el Comité documentó 987 violaciones al Derecho Internacional Humanitario (DIH) y otras normas humanitarias, asimismo, lograron registrar 93 desapariciones durante el año. No obstante, Harnisch señaló que esta cifra no debía tomarse como un estimado nacional, dado que el alcance de la Institución no es de carácter nacional como sí el de otras entidades estatales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el mismo sentido, destacó el trabajo de la Unidad de Búsqueda de Perssonas dadas por Desaparecidas, señalando su importancia, así como la necesidad de que toda la sociedad se movilice para encontrar a las más de 80 mil personas que, según el Centro Nacional de Memoria Histórica, son víctimas de este flagelo. (Le puede interesar: ["Inicia búsqueda de los 120 mil desaparecidos en los territorios"](https://archivo.contagioradio.com/inicia-busqueda-120-mil-desaparecidos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, el [informe](https://www.icrc.org/es/document/colombia-preocupaciones-del-conflicto-armado-y-la-violencia) del Comité llamó la atención sobre la situación de confinamiento que afecto a 27.694 personas, principalmente en Chocó, Antioquia, Nariño y Putumayo. Aunque hubo un leve descenso en la cantidad de personas desplazadas masivamente, pasado de 27780 en [2018](https://archivo.contagioradio.com/desplazamiento-forzado-cicr-colombia/) a 25303 en 2019, la organización insistió en la necesidad de que los ciudadanos puedan habitar en sus territorios con garantías de derechos.

<!-- /wp:paragraph -->

<!-- wp:group -->

<div class="wp-block-group">

<div class="wp-block-group__inner-container">

<!-- wp:heading {"level":3} -->
</p>
### **Más de 350 personas víctimas de minas y otros artefactos explosivos**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Comité llamó la atención sobre el aumento en 131 casos de víctimas de artefactos explosivos y minas (352 en 2019), de las cuales 159 eran civiles. El mayor aumento de afectados por estos artefactos estuvo en Norte de Santander, seguido por Arauca y Antioquia, en estos departamentos se presentaron el 57% de los casos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el jefe de la misión del CICR en Colombia Christoph Harnisch, todos los actores hacen uso de estas armas. Organizaciones defensoras de derechos humanos en regiones como el Bajo Cauca Antioqueño y Chocó han señalado que las minas son una forma de ejercer control territorial sobre las poblaciones, restringiendo su derecho a la libre movilidad.

<p>
<!-- /wp:paragraph -->

</div>

</div>

<!-- /wp:group -->

<!-- wp:heading {"level":3} -->

### **Ataques a la misión médica y persistencia de los Conflictos Armados No Internacionales**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Otro tema relevante fueron los ataques a la misión médica, que aumentaron en más del doble respecto a 2018, pasando de 101 ataques a 218. En un 74% de los casos se trató de amenazas o lesiones al personal, y llama la atención que el 56% de los presuntos agresores fueron civiles, pero Harnisch señaló que igualmente actores armados ven la misión médica como elementos con valor estratégico.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, el Jefe de la Misión del Comité también declaró que el diálogo que suele sostener la entidad con grupos al margen de la Ley por temas humanitarios se ha hecho más difícil, y las comunidades en algunas zonas del país no identifican al actor armado que hace presencia. Esta situación dificulta la promoción del respeto al DIH y los derechos humanos en los territorios.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El mayor reto humanitario: Que las comunidades vivan sin miedo**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Otro hecho que Harnisch resaltó fue que las comunidades en los territorios sienten miedo de hablar con el CICR por temor a represalias por parte de los actores armados ilegales que actúan en la zona, lo que se traduce en el silencio de violaciones a los DD.HH. Para el experto, esta situación es delicada porque se ha presentado hace años, y significa que la población ni siquiera puede denunciar las situaciones de las que es víctima.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, el Comité señaló como uno de los principales retos el que las comunidades puedan vivir sin miedo en Colombia, especialmente en los departamentos en los que persiste el conflicto armado. (Le puede interesar: ["En Colombia hay cinco conflictos armados"](https://archivo.contagioradio.com/colombia-conflictos-armados-cicr/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CICR_co/status/1235196623652671488","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CICR\_co/status/1235196623652671488

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78980} /-->
