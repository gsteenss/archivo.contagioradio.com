Title: Bojayá despide sus víctimas y pide atención para quienes habitan allí
Date: 2019-11-13 16:21
Author: CtgAdm
Category: Paz
Tags: exhumación, Masacre de Bojayá, Medellin, ONU, Unidad de Víctimas
Slug: bojaya-despide-sus-victimas-y-pide-atencion-para-quienes-habitan-alli
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/EJG0HljWsAAKXPI.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Tras 16 años de los hechos ocurridos en Bellavista Bojayá, en el que un cilindro bomba explotó en medio de un combate dejando **79 víctimas fatales, se hizo entrega de 72 víctimas identificadas a sus familiares, quienes los despidieron en medio de cantos y oraciones. ** (Le puede interesar: [&\#8216;Bojayá entre fuegos cruzados&\#8217;, el documental que reconstruye la verdad de un pueblo](https://archivo.contagioradio.com/bojaya-entre-fuegos-cruzados-el-documental-que-reconstruye-la-verdad-de-un-pueblo/))

### **El derecho a un entierro**

El 2 de mayo del 2002 en medio de un enfrentamiento entre las  Autodefensas Unidas de Colombia (AUC) y la entonces guerrilla FARC-EP, 79 habitantes de Bellavista en Bojayá  perdieron la vida; **a partir de ese momento tanto familiares como sobrevivientes de estos hechos exigieron a las instituciones correspondientes la exhumación y clasificación los restos de las personas que murieron en estos hechos.**

De esas víctimas, 72 fueron entregadas el pasado 12 de noviembre, **7 casos restantes correspondes a restos que han sido difíciles de identificar** ya sea porque no hay ADN de familiares vivos para contrarrestar datos, o en su defecto por la complejidad en la clasificación de estos, teniendo en cuenta que muchas de las víctimas compartieron por un largo periodo el mismo espacio de sepultura. (Le puede interesar:[Víctimas de Bojayá piden respeto al duelo](https://archivo.contagioradio.com/victimas-de-bojaya-piden-respeto-de-su-intimidad-para-procesos-de-duelo/))

Según Yuber Palacios, representante del Comité de Víctimas de la Masacre de Bojayá la sensación para la comunidad es agridulce, **"aquí no hay  motivo de celebración, este es un momento de tristeza, silencio y dolor"**, y resaltó que este es un proceso para culminar el duelo para muchos familiares, "hemos recibido cuerpos plenamente identificados, otros que no lo están por completo y otros aún en condición de desaparecidos, por ahora no perdemos  la esperanza de poder culminar este ciclo".

Así mismo Leyner Palacios víctima y habitante de Bojayá agregó, "[**nosotros esperamos que este proceso permita iniciar un momento de cierre del duelo suspendido por 17 años**, esperamos que en estos 15 días podamos hacer los actos rituales mortuorios como son  el velorio y la novena y poder dar cristiana sepultura a estas personas que han significado para nosotros esperanza **en medio de un periodo de olvido estatal de las comunidades afro en Colombia.**".  (Le puede interesar:[&\#171;Yover&\#187;el cortometraje de Bojayá que se estrenará en la Berlinale](https://archivo.contagioradio.com/yover-cortometraje-bojaya/))]

 

> pos de la Masacre perpetrada el 2 de mayo de 2002 en el municipio de Bojayá, Chocó, a su territorio. [\#HonraALosSagradosEspíritus](https://twitter.com/hashtag/HonraALosSagradosEsp%C3%ADritus?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/5T8WbxnTMo](https://t.co/5T8WbxnTMo)
>
> — Unidad Víctimas (@UnidadVictimas) [November 11, 2019](https://twitter.com/UnidadVictimas/status/1193995713362960391?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **"Esperamos que cerrar este duelo en Bojayá implique una mirada distinta del gobierno a nuestras comunidades"**

Más de una década después de una de las masacres más lamentables en el país, **las comunidades afro de Colombia viven con temor, los hoy familiares de las victimas de este episodio, resaltan el  pánico ante la repetición de un hecho similar**, según Leyner Palacios, "h[oy lamentamos que en Bojayá estemos recibiendo los cuerpos de nuestros familiares y vecinos, pero también esperamos que esta sea **una lección para nuestro Estado, en el sentido que esto no debería seguir pasando en nuestro territorio**".]

[Para Palacios esta entrega no solo  significa el cierre de un periodo de duelo, sino también que sea el momento para que **el gobierno preste atención a Bojayá más allá de sus muertos,"necesitamos una mirada distinta del gobierno a nuestras comunidades en materia de atención en salud, educación y vivienda** que tanto necesita esta juventud que sobrevivió a esta masacre" resaltó Palacios.]

> **"Bojayá existe sobre todo, y cada vez más, como símbolo de paz y reconciliación" Leyner Palacios**

**Palacios perdió a 29 de sus familiares en la noche del 2 de mayo en la iglesia de Bellavista,  y es uno de los mayores creyente en el proceso de paz en Colombia**, y destaca que Bojayá se posicionó en la cabeza y el corazón de miles de colombiano por la tragedia de una "guerra sin límites"  como se denominó en un informe de memoria histórica , pero también quiso acotar que Bojayá existe sobre todo, y cada vez más, como símbolo de paz y reconciliación; "recuerdo el acto de  perdón de las Farc al  pueblo  bojayacenses en el  2015, para nosotros como víctimas fue de gran importancia, nos permite soltar y recordar que el perdón es posible", dijo Palacios.

Por último el bojayence añadió que **el proceso de reconciliación es  lento e incierto, y viene en un proceso de sanación de quienes perdieron a sus seres queridos, pero también de quienes están aún en este territorio y que buscan rescribir una nueva historia de su pueblo**, "la recuperación de Bojayá requiere apoyo y que el gobierno se siente a dialogar y ofrecer herramientas para el crecimiento de nuestra comunidad".

 
