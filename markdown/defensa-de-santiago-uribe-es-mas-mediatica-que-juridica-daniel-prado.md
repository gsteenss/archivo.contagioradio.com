Title: Defensa de Santiago Uribe es más mediática que jurídica: Daniel Prado
Date: 2018-09-03 10:22
Author: AdminContagio
Category: Entrevistas, Judicial
Slug: defensa-de-santiago-uribe-es-mas-mediatica-que-juridica-daniel-prado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Santiago-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [31 Ago 2018] 

El abogado, representante de las víctimas de los 12 Apóstoles en el proceso de que sigue contra Santiago Uribe, aseguró que “los medios de información están muy mal” porque, según él, a pesar de que es clara **la falta de coherencia en algunos testimonios** presentados por la defensa, lo que se difunde en los medios de información es la explicación, que al término de la audiencia, brinda el abogado Jaime Granados.

Pardo explica que aunque hay presencia permanete de los medios de información lo que se divulga de la audiencia **no es el material probatorio que se presentó sino la explicación de Granados**, siempre consultado al final de las audiencias por los periodistas. Ello sería evidencia de que se quiere mostrar la inocencia de Santiago Uribe ante los medios pero el manejo jurídico es muy pobre e “irrespetuoso”.

### **En proceso contra Santiago Uribe habría un nuevo caso de manipulación de testigos** 

Uno de los ejemplos de Prado, fue la intervención del abogado Diego Cadena en las declaraciones de unos de los testigos que “a todas luces es un testigo falso”. Se trata del exmilitar Rodríguez Agudelo, conocido como “Zeus” quien ofreció su declaración a favor de Santiago Uribe, quien “dice cosas que no son ciertas” y reconoce que fue contactado por el abogado Diego Cadena, también contratado por Alvaro Uribe. (Le puede interesar: [Piden al Estado prevenir manipulación de testigos tras la libertad de Santiago Uribe](https://archivo.contagioradio.com/manipulacion_testigos_santiago_uribe_12_apostoles/))

Según el abogado este caso no fue anexado por el juzgado y por ello presentarán las pruebas ante la fiscalía para que se inicie un nuevo proceso por un posible hecho de **manipulación de testigos** como en el caso que se sigue contra Alvaro Uribe y Ernesto Prada, y que en los próximos días podrá tener una de las primeras indagatorias que tiene que rendir el senador del Centro Democrático ante la Corte Suprema de Justicia.

### **Eunicio Pineda sigue siendo un testigo válido** 

Por otra parte, el caso de Eunicio Pineda fue nuevamente ratificado por el juzgado, dado que el concepto de Medicina Legal reitera que **este testigo siempre ha dicho la verdad.** Según Prado lo único que no es cierto para la defensa y los testigos de la defensa es que hay relación con grupos paramilitares, mientras que los lugares, las fincas, las personas de los que testifica corresponden a la verdad.

Además para el abogado de la parte civil es claro, según los testimonios y los habitantes de los municipios, que la presencia paramilitar era normal, dado que muchos de los propietarios de haciendas o empresarios de la región contaban con los paramilitares con el pretexto de ayudar en la protección de sus bienes del acecho de las guerrillas. (Le puede interesar:['12 Apostoles' al mando de Santiago Uribe cometieron cerca de 570 crímenes](https://archivo.contagioradio.com/santiago_uribe_homicidios_12_apostoles/))

### **Alegatos finales se realizarían en Diciembre** 

El juzgado primero penal especializado del circuito de Antioquia resolvió que la próxima audiencia se realizará el 10 de Octubre y los alegatos finales serían la primera semana del mes de Diciembre, esto en el caso contra Santiago Uribe por la conformación del grupo paramilitar los 12 Apóstoles. En este caso se espera que el Juez lea el expediente y tome una decisión conforme a los hechos presentados a lo largo del juicio.

<iframe id="audio_28242830" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28242830_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### **[ Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).]** 
