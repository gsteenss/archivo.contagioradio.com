Title: Comité Ambiental presentó ante la ONU informe sobre mecanismos de participación
Date: 2017-09-20 12:09
Category: Nacional, yoreporto
Tags: Comité DESC, ONU, PIDESC
Slug: comite-ambiental-presento-ante-la-onu-informe-sobre-mecanismos-de-participacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Informe-Onu-e1505927097648.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Yo Reporto** 

###### 20 Sep 2017

#### **Por: Valentina Camacho Montealegre - @valentinacmpm** 

[El Comité de Derechos Económicos, Sociales y Culturales de la ONU evaluará durante esta semana el cumplimiento de los derechos económicos, sociales y culturales en Colombia.]

[En este espacio, organizaciones sociales de todo el territorio colombiano presentaron informes paralelos en donde hicieron un llamado de atención sobre la desigualdad, la falta de acceso al agua potable, el uso de semillas transgénicas, la soberanía de los pueblos indígenas, la erradicación forzada de cultivos ilícitos, el extractivismo, entre otros.]

[Cabe recordar que el Comité de Derechos Económicos, Sociales y Culturales es el órgano de expertos independientes que supervisa la aplicación del Pacto Internacional de Derechos Económicos, Sociales y Culturales (PIDESC), del cual el Estado Colombiano hace parte desde el 16 de diciembre de 1966.]

[A través de la Red Internacional de Derechos Humanos – RIDH organizaciones que han impulsado consultas populares en contra de proyectos minero-energéticos en el país presentaron un informe sobre los mecanismos de participación ciudadana, que fue expuesto, en Ginebra – Suiza, ante los expertos por parte de los delegados del Comité Ambiental en Defensa de la Vida, Alejandro García Pedraza y Jaime Andrés Tocora.  ]

[“Expusimos ante la comunidad internacional que en Colombia no se está cumpliendo el convenio 169 de la OIT, el de la consulta previa, libre e informada, de igual forma, exigimos respeto por las consultas populares, los acuerdos municipales para definir el uso del suelo en temas de proyectos mineros, petroleros e hidroeléctricas y solicitamos que se mantenga el carácter vinculante de los mismos”, explicó Alejandro García Pedraza, vocero del Comité Ambiental.]

[“En el artículo 3 de la Constitución Política quedó estipulado que la soberanía reside exclusivamente en el pueblo y que el poder público emana de este,  el pueblo está ejerciendo su soberanía por medio de las consultas populares, mecanismo mediante el cual está manifestándose en oposición al modelo económico que el gobierno quiere imponer, esperamos que acate la decisión de las comunidades”, agregó García.]

[Los expertos del Comité DESC, escucharán este martes al Gobierno Colombiano quien rendirá, desde la institucionalidad, un informe con relación a los derechos económicos, sociales y culturales.]

[Es así como, luego del análisis de los expertos, en tres semanas aproximadamente, el Comité de Derechos Económicos, Sociales y Culturales de la ONU emitirá un informe con recomendaciones para el cumplimiento de este tipo de derechos, dirigido al Estado Colombiano.]
