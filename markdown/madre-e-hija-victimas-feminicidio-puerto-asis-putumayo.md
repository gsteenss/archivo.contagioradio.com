Title: Madre e hija, víctimas de feminicidio en Puerto Asís, Putumayo
Date: 2019-03-20 14:50
Category: DDHH, Mujer
Tags: feminicidios, junta de acción comunal, Putumayo
Slug: madre-e-hija-victimas-feminicidio-puerto-asis-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/feminicidios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Lunita Lu] 

###### [20 Mar 2019] 

El pasado 17 de marzo, **Yaneth Ordóñez** y su madre **María Maura Ortega**, integrantes de la Junta de Acción Comunal (JAC) de Puerto Asís, Putumayo, fueron asesinadas con armas de fuego por sujetos desconocidos mientras se encontraban en su hogar. En lo corrido de 2019 ya van al menos **12 feminicidios** registrados en ese departamento.

Según informó Yuri Quintero, coordinadora de la Red de Derechos Humanos de Putumayo, la madre e hija fueron cofundadoras de su comunidad en la vereda de Campoquema, del corredor de Puerto Vega - Teteyé y estaban afiliadas a la Junta de Acción Comunal de la zona. Las dos mujeres también trabajaban como cultivadoras de coca para su sustento, actividad que indica Quintero podría estar relacionada con su asesinato.

Cabe resaltar que **el esposo de Ordóñez, quien era líder de los obreros de las empresas petroleras de Puerto Asís, también fue asesinado** en su casa en ese mismo municipio hace cinco años. En ese tiempo, Ordóñez indicó que ella y su compañero habían sido amenazados y temían por su seguridad. Tras el asesinato de su pareja, la lideresa se había mudado a Puerto Vega - Teteyé y no se conocía de una amenaza dirigida en su contra.

Hasta la fecha, la Fiscalía no se ha comunicado con la Red de Derechos Humanos y por lo tanto **las comunidades no tienen conocimiento si las autoridades han iniciado una investigación sobre estos crímenes.** "Esperamos que las autoridades correspondientes estén haciendo las investigaciones y que dichas investigaciones realmente arrojen respuestas que le permiten a la comunidad tener tranquilidad", manifestó la coordinadora.

Con el asesinato de las dos mujeres, se registran al menos **12 feminicidios cometidos en Putumayo en lo que va de 2019**, situación que ha generado zozobra y silencio en las comunidades. Quintero señaló que se esperaba un reporte de la Fiscalía sobre las investigaciones de estos asesinatos para la última semana del mes de febrero, sin embargo, la autoridad no cumplió con su compromiso. (Le puede interesar: "[Feminicidios en Putumayo ascienden a más de 13 con asesinato de lideresa Zonia Rosero](https://archivo.contagioradio.com/zonia-rosero/)")

Por último, la defensora de Derechos Humanos indicó que las comunidades han buscado reunirse con el Gobierno para encontrar una solución al conflicto y violencia que se sigue viendo en el departamento, sin embargo **las autoridades han aplazado las fechas para el encuentro.** "No atienden la crisis que están viviendo las comunidades en temas de seguridad, de garantías y cumplir lo acordado".

<iframe id="audio_33553935" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://www.ivoox.com/player_ej_33553935_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
