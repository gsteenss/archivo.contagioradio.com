Title: Fiscal no prevarica si investiga el caso Odebrecht
Date: 2017-02-09 12:38
Category: Entrevistas
Tags: colombia, corrupción, Fiscalía, Odebrecht, Santos, zuluaga
Slug: fiscal-investiga-odebrecht
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/nestor.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las dos Orillas 

###### 09 Feb 2017 

El constitucionalista Rodolfo Arango, afirma que **el fiscal Nestor Humberto Martínez no caería en el delito de prevaricato si investiga el caso Odebrecht** y la financiación de las campañas presidenciales que llegaron a la segunda vuelta en 2014, es decir, Santos Presidente y Zuluaga Presidente. La investigación del Consejo Nacional Electoral (CNE) se pude realizar también de manera paralela.

### **La campaña presidencial se “anticipó”** 

Para Arango, lo que ha pasado con **la empresa brasilera es que ha ayudado a adelantar la campaña política de cara a las elecciones**, “la campaña presidencial del 2018 se anticipó” en buena parte debido a que el vicepresidente Germán Vargas Lleras, tiene a su cargo una serie de funciones que podrían estar ayudando a impulsar sus aspiraciones presidenciales y en el tema Odebrecht podría haber sectores interesados en impulsarla.

En ese sentido señala que “Si el fiscal no está jugando políticamente y no quiere favorecer a algunos de los candidatos **tiene que empezar a mostrarnos con hechos que en efecto se está investigando seriamente** y que es posible impedir que se politice este debate” puesto que la acusación contra la campaña de Santos podría parecer una cortina de humo para esconder el escándalo que significa la financiación de Odebrecht a la campaña del Centro Democrático.

### **No se puede condicionar la investigación penal a la investigación del CNE** 

El constitucionalista afirma que el Consejo Nacional Electoral no tiene la capacidad para adelantar una investigación tan grande como la que tiene que ver con este tema. Para Arango, aunque existe la función en el CNE, **no se puede supeditar la investigación de esa entidad a la investigación penal** porque aunque se puede llegar a una destitución muchas veces las sanciones se quedan en el pago de una multa por violación de los topes de la campaña.

Arango señala que no se puede “Condicionar lo penal que son la defraudación, el uso de documentos fraudulentos, el concierto para delinquir, el soborno o el lavado de activos”, a una investigación del CNE, eso “suena muy mal” y lastimosamente esa parece ser la postura del fiscal en la rueda de prensa, en la que también afirmó que la acusación contra la campaña Santos, únicamente se sostiene en el testimonio de Otto Bula, congresista altamente cuestionado.

El caso es que el Fiscal tiene que “soportar la crítica” que demostrar que está investigando y que es posible **impedir que el tema Odebrecht se politice y termine favoreciendo a uno u otro candidato**, “se puede esclarecer el tema judicialmente sin que por el contrario se enlode a la gente o se enturbie a investigación y finalmente nadie acabe respondiendo por nada” lo cual es común en Colombia cuando “un escándalo tapa a otro escándalo”

### **La corrupción es estructural y trasciende los intereses de los partidos políticos** 

El escándalo de **Odebrecht era inevitable para Colombia** y según Arango lo que se está demostrando es que todos nuestros países tienen una “debilidad institucional” en unos más que en otros. Por fortuna lo que muestra el escándalo es que **hay que hacer grandes reformas**. Lo que se ve es que se siguen eligiendo a los mismos independientemente de si gobernaron bien o mal.

Arango señala que hay una posibilidad de que en Colombia se asuma ese problema y por lo menos **se creen instancias o reformas que tipifiquen como delitos las hasta ahora “malas prácticas” de los partidos** y de las empresas, eso puede suceder en la reforma política y de partidos que está contemplada en el acuerdo de paz entre el gobierno y las FARC.

En conclusión, lo que debería pasar es que **en las próximas elecciones no reine la apatía** en que la corrupción en la política gane como siempre y termine eligiendo a los mismos de siempre, por ello Arango resalta que lo que se debe hacer es **salir a votar y con ello impulsar los cambios que necesita la política colombiana**.

<iframe id="audio_16922219" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16922219_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
