Title: En 30 puntos de Colombia camioneros se movilizan exigiendo respuestas del gobierno
Date: 2016-06-16 16:02
Category: Movilización, Nacional
Tags: Gobierno Nacional, Paro camionero
Slug: en-30-puntos-de-colombia-camioneros-se-movilizan-exigiendo-respuestas-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Paro-camiones-061-e1466110579707.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El debate boyacense 

###### [16 Jun 2016] 

En 30 puntos del país durante el día de hoy, se adelantan diversas movilizaciones de los transportadores de carga, pues aún no existen respuestas de parte del gobierno nacional a las reclamaciones de los camioneros, a quienes el fin de semana el Ministerio de Transporte los dejó esperando para iniciar conversaciones.

**En Ipiales, Pasto, Jumbo, Valle; Buga; el Puerto de Buenaventura; Manizales; Duitama, Tunja, Pitalito, diferentes puntos de Antioquia,** entre otros, se lleva a cabo la movilización exigiendo  soluciones al gobierno nacional.

Según con Juan Carlos Bobadilla, secretario general de la Asociación Colombiana de Camioneros, ACC, desde este miércoles se reiniciaron los diálogos con el gobierno y se acordó que a partir de la 1 de la tarde del jueves se reunirían nuevamente, pues el Ministerio de Transporte deberá presentar al gremio un borrador de la resolución para garantizar los acuerdos.

Debido a que el gobierno no ha querido responder a los camineros, quienes desde hace días adelantan un paro nacional pacífico, la economía nacional es la que se ha visto afectada, pues **al día se pierden cerca de 50 mil millones de pesos. Así mismo, se perjudica la producción industrial y a todos aquellos comerciantes que necesitan que sus mercancías sean transportadas,** explica Bobadilla, quien también señala que debido a esa situación desde el segundo día se reportó alzas en alimentos como la cebolla y la papa del 70 y el 100%.

Dentro de las exigencias específicas del gremio de los camioneros se exige que el precio de los combustibles acorde a los demás indicadores del transporte de carga, el valor de los **fletes coherente con las exigencias de las carreteras del país, y que no se favorezca el monopolio a través de esos cobros.**

Además, exigen  que no se presente un proyecto del Ministerio de Transporte que reemplaza la orden de la chatarrización por el pago de una póliza. Según los camioneros, **todos los acuerdos firmados con el gobierno del presidente Juan Manuel Santos han sido incumplidos,** y por ello se ven en la necesidad de movilizarse.

<iframe src="http://co.ivoox.com/es/player_ej_11928799_2_1.html?data=kpamlJ2bfZqhhpywj5WcaZS1k5aah5yncZOhhpywj5WRaZi3jpWah5yncavpwtOYpcbWsNDnjKfcxMbIrc3gwpCajabXs8TdwsjWh6iXaaOnz5Cw0dHTscPdwtPOjcnJb6TVzs7cj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
