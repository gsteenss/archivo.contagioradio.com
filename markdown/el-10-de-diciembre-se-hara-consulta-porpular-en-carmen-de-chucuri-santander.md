Title: El 10 de diciembre habrá consulta popular en Carmen de Chucuri, Santander
Date: 2017-10-31 15:07
Category: Ambiente, Nacional
Tags: Carmen del Chucuri, consulta popular
Slug: el-10-de-diciembre-se-hara-consulta-porpular-en-carmen-de-chucuri-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/WhatsApp-Image-2017-10-31-at-7.15.59-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Wikipedia] 

###### [31 Oct 2017] 

El próximo 10 de diciembre se llevará a cabo la consulta popular en el Carmen de Chucurí, en el departamento del Santander, la consulta busca evitar que se realicen actividades de exploración y extracción minera, en un territorio que de acuerdo con los habitantes es de vocación agrícola, **el umbral de votación que deberán superar es de 3.550 votos.**

Esta consulta, además se realizará a pesar de las afirmaciones de la Registraduría Nacional que informó que no hay dinero para implementar este mecanismo en ninguna región y la del Ministerio de Hacienda que continúa señalando que las consultas populares deben ser efectuadas con los presupuestos municipales.

Sin embargo, de acuerdo con Johanna Cárdenas, este no será un impedimento para dejar de usar este mecanismo y por el contrario se seguirá exigiendo la garantía de participación democrática de las comunidades en las decisiones que tienen que ver con sus territorios.

Pese a que aún la Registraduría no le ha informado al municipio si hay o no los recursos para hacerla, los habitantes de El Carmen de Chucurí iniciaron **una recolecta de fondos, en donde pueden aportar personas, organizaciones y plataformas**, para reunir el dinero suficiente que les permita realizar la campaña y la consulta en la fecha acordada.

“Es preocupante que el Estado y todo su gabinete quiera hacerle zancadilla a algo que como ciudadanos tenemos derecho” afirmó Cárdenas y agregó que considera que con el paso del tiempo saldrán más leyes que impedirán que las consultas populares sean válidas. (Le puede interesar: ["Concejo Municipal aprobó consulta popular en Carmen de Chucuri, Santande"](https://archivo.contagioradio.com/concejo-municipal-aprobo-consulta-popular-en-carmen-de-chucuri-santander/))

Actualmente, en este municipio se han otorgado títulos mineros de 20 mil hectáreas a multinacionales extrajeras como Centromin y Colco, además los habitantes señalaron que las empresas habían convocado a los trabajadores para ponerlos en contra de la consulta popular, sin embargo, se logró realizar el concejo en donde se informó la finalidad del mecanismo para lograr el apoyo de todas las personas.

<iframe id="audio_21802000" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21802000_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [[su correo]
