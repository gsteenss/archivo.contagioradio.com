Title: "Si Duque objeta la Ley Estatutaria vulneraría derechos de víctimas" organizaciones
Date: 2019-03-04 14:59
Author: ContagioRadio
Category: Judicial, Nacional, Política
Tags: jurisdicción especial para la paz
Slug: razones-para-aprobar-la-ley-estatutaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/jep-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [04 Mar 2019] 

Un grupo de organizaciones de la sociedad civil, académicos y centros de investigación, presentaron, mediante un comunicado de prensa, las razones claves por las cuales el presidente Iván Duque debería sancionar esta semana la Ley Estatutaria, que brindaría el marco normativo a la Jurisdicción Especial para la Paz (JEP).

El argumento principal, presentado por las organizaciones, señala que la decisión del Presidente debe respetar la revisión jurídica que realizó la Corte Constitucional a través de la sentencia C- 080 de 2018. "Este fallo judicial **determinó claramente qué artículos de la Ley Estatutaria de la JEP se encontraban o no ajustados a la constitución y fijó la interpretación de ese texto** normativo en su totalidad, con el fin de asegurar que dicha norma respetara la  Constitución Política", indicó el comunicado.

Las organizaciones sostienen que los argumentos del Fiscal General, Nestor Humberto Martínez, pretenden modificar **el proyecto de ley para incluir contenidos que ya fueron consideradas inconstitucionales** por la sentencia mencionada. Motivo por el cual, la acción del Presidente al objetar la ley violaría el artículo 243 de la Constitución Política. (Le puede interesar: "[¿Firmará Duque la Ley Estatutaria de la JEP?](https://archivo.contagioradio.com/ley-estatutaria-jep/)")

Además, esta decisión resultaría en **"el retraso innecesario de la sanción del proyecto de ley sin ningún efecto de fondo",** debido a que una vez un contenido es declarado inconstitucional no se puede revivir en un debate legislativo. De igual forma, si por alguna razón se aprobaran las modificaciones, **la Corte las declararía nuevamente inconstitucionales**. (Le puede interesar: "[Presidente Duque sancione la Ley Estatutaria de la JEP: Organizaciones](https://archivo.contagioradio.com/duque-ley-estatutaria-jep/)")

La única justificación por la cual el Presidente puede objetar el proyecto legislativo es la inconveniencia política. Sin embargo, afirman que esta posible decisión tendría efectos negativos para el avance del proceso de paz dado que **afectaría el derecho de las víctimas a la verdad y a la justicia.** Además, dejaría a los comparecientes de la JEP — los miembros de la Fuerza Pública, excombatientes y terceros — sin seguridad jurídica.

Entre los firmantes del comunicado se encuentran el Centro de Estudios de Derecho, Justicia y Sociedad (Dejusticia), la Comisión Colombiana de Juristas (CCJ), la Misión de Observación Electoral (MOE), el Instituto para las Transiciones Integrales (IFIT), la Fundación Ideas para la Paz (FIP), el Centro de Pensamiento y Seguimiento al Diálogo de Paz de la Universidad Nacional, así como varios docentes de la Universidad de los Andes, del Rosario y el Externado.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
