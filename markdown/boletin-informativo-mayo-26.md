Title: Boletin informativo Mayo 26
Date: 2015-05-26 16:52
Category: datos
Tags: Boletín Informativo, cascos azules, Cauca, Haiti, incumplimiento del Plan de Acción Laboral, Noticias del día, personas desplazadas Guapi, Refugiados en Europa
Slug: boletin-informativo-mayo-26
Status: published

[*Noticias del Día: *]

<iframe src="http://www.ivoox.com/player_ek_4553767_2_1.html?data=lZqilZyae46ZmKiakpiJd6Knk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfk5uah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Este miércoles 27 de mayo, en la Comisión Segunda de Cámara de Representantes, se llevará a cabo el **debate sobre el incumplimiento del Plan de Acción Laboral** suscrito con EEUU, **en el marco del Tratado de Libre Comercio con Colombia**. Habla **Alirio Uribe**, representate a la cámara por el **Polo Democrático Alternativo.**

-Todo tipo de **movimientos sociales, y sindicales de Brasil y Haití** se dieron cita el 22 y 23 de Mayo en Sao Paulo **para iniciar una Campaña en Solidaridad y para exigir el fin de la misión de Naciones Unidas**. En los encuentros se decidió luchar contra la militarización del país que está impidiendo la autodeterminación de los Haitianos y el libre desarrollo, además de servir a intereses de carácter imperialistas basados en la posición geoestratégica de la nación.

-La agudeza de los **operativos militares en Guapi, Cauca,** ha generado que **alrededor de 500 personas, se vean obligadas a desplazarse**, luego de que se desatara nuevamente la guerra tras el operativo de la fuerza pública que acabó con la vida de 26 guerrilleros de las FARC-EP el pasado 22 de mayo. **Orlando Pantoja**, de la **Asociación de Consejos Comunitarios del Cauca** habla sobre la situación de la comunidad.

-El lunes 25 de Mayo **Grecia, Bulgaria y Turquía firmaron un acuerdo para controlar la segunda entrada más importante de inmigrantes en Europa** y de esta forma poder **frenar a las mafias que trafican con los refugiados de Oriente Próximo**. En un principio el acuerdo determinaria la **creación de un Centro de Información** presumiblemente en territorio Búlgaro, que será quien lidere la misión en la frontera entre este y Turquía. **El Centro tendrá una duración de tres años con posibilidad de renovación del acuerdo.**
