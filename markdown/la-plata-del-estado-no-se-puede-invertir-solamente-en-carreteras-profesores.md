Title: La plata del Estado no se puede invertir solamente en carreteras: Profesores
Date: 2017-06-13 12:30
Category: Educación, Nacional
Tags: educacion, fecode, Paro de maestros
Slug: la-plata-del-estado-no-se-puede-invertir-solamente-en-carreteras-profesores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/marcha-de-profesores-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Contagio Radio ] 

###### [13 Jun 2017] 

Se cumple un mes del paro de los profesores y profesoras en el país. Ellos y ellas insisten en que **el gobierno si tiene plata para asumir las inversiones que necesita la educación**, que el paro no es ilegal y que los padres de familia están comprometidos con el apoyo a los maestros y maestras del país.

El gobierno nacional, en cabeza de la Ministra de Educación, Yaneth Giha, ha dicho en repetidas ocasiones que no hay plata para cubrir las demandas de FECODE. Sin embargo, William Agudelo, presidente de la Asociación Distrital de Educadores, ADE, afirmó que "**el gobierno si tiene plata y debe invertir en lo que es prioridad para la administración actual, la educación".**

Agudelo dijo que la plata que necesita la educación, por ejemplo para financiar el Sistema General de Participaciones, **“se puede sacar del iva y de la reforma tributaria”.** Además, FECODE ha dicho que "los 28 billones de pesos que el gobierno nacional se está ahorrando en la inversión para la guerra, puede invertirse en las escuelas que tienen una crisis estructural". (Le puede interesar: ["Dura crítica a RCN y Caracol por no cubrir agresión a paro de maestros"](https://archivo.contagioradio.com/42081/))

**Paro de maestros no es ilegal**

Adicionalmente, Agudelo estableció que **“los maestros y maestras no nos vamos a dejar amedrentar con las amenazas del gobierno”**. Esto pues la Ministra de Educación afirmó que el paro de los maestros es ilegal y que les van a descontar de los salarios los días no trabajados por estar en paro.

FECODE ha afirmado que los **descuentos a los salarios son una medida represiva y van en contra de las libertades sindicales propuestas por la OIT.** Adicionalmente, el Ministerio de Educación ha hecho énfasis en que el decreto 2790 del 2000, establece que no se les podrá pagar a los maestros los días no laborados porque violan el derecho a la educación de los niños y las niñas del país. (Le puede interesar: ["Esmad ataca el paro de maestros en Bogotá"](https://archivo.contagioradio.com/esmad-ataca-con-agua-y-gases-lacrimogenos-el-paro-de-maestros-en-bogota/))

Para Agudelo “este decreto es viejo y no se puede aplicar ahora. Además, el decreto fue derogado en 2001”. Esto resultaría en que el paro de los maestros no es ilegal y les deben pagar de cualquier forma.

Agudelo afirmó que el paro en vacaciones continua. **“Si tenemos que reponer las clases en diciembre lo haremos,** pero debemos seguir diciéndole al gobierno que la situación de la educación es crítica por lo que se debe llegar a acuerdos que den respuestas a los pliegos presentados en febrero". Igualmente, FECODE, le ha pedido al gobierno que aplace y reprograme las pruebas Saber 11.

Finalmente, los maestros y las maestras han sido enfáticos en que los padres han mantenido un apoyo constante. Los medios de información han dicho que los padres de familia y la ciudadanía están cansados del paro. Sin embargo, para Agudelo, **“no es verdad que los padres estén cansados del paro**. Son conscientes de que sus hijos están recibiendo refrigerios podridos y ellos apoyan el paro y si hemos de sacarlos a las calles para que se movilicen lo haremos”. (Le puede interesar: ["Gobierno está "repartiendo la pobreza" pero no aumenta presupuesto: Fecode"](https://archivo.contagioradio.com/gobierno-esta-repartiendo-la-pobreza-pero-no-aumenta-presupuesto-fecode/))

Ante la falta de acuerdos con el gobierno, FECODE manifestó que las movilizaciones seguirán. **Hoy los docentes saldrán a las calles en Bogotá para realizar una marcha de antorchas** y este miércoles 14 de Junio, a partir de las 10:00am, llevarán a cabo una “Gran Actividad Nacional”.

<iframe id="audio_19243047" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19243047_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
