Title: Minga continúa en Valdivia tras amenazas de grupos paramilitares
Date: 2016-06-20 15:41
Category: DDHH, Nacional
Tags: Cumbre Agraria, Minga, Paramilitarismo
Slug: minga-continua-en-valdivia-tras-amenazas-de-grupos-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/ClKTUAGWQAAWrYQ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Congreso de los Pueblos 

###### [20 Jun 2016]

Debido a la falta de soluciones por parte de las autoridades departamentales y regionales, el Movimiento Ríos Vivos en Antioquia decidió continuar en Minga, en el municipio de Valdivia, donde **siguen siendo hostigados y amenazados por la fuerza pública, pero también por estructuras paramilitares.**

Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos, asegura que se encuentran en asamblea permanente, pues no existen garantías de seguridad en los territorios, ya que se ha registrado un aumento en amenazas y crimenes en Valdivia. Según se denuncia, se contabilizan **4 asesinatos, además ha habido 6 amenazas individuales y 3 amenazas colectivas por parte de grupos paramilitares.**

**“Las amenazas hacen referencia a que no podemos protestar en contra de EPM y el proyecto hidroeléctrico Hidroituango.** Además dicen que nos van a judicializar y se nos prohíbe bloquear las obras de la represa”, explica Zuleta, quien agrega que por esa situación desde el pasado jueves continuaron  en protestas en Valdivia, exigiendo respuestas a sus reclamaciones por parte de las autoridades que aseguran que no pueden atender sus preocupaciones, pues dicen que se trata de un asunto que debe atender el gobierno nacional.

Por el momento el Movimiento elabora un informe detallado sobre lo que ha sucedido en materia de derechos humanos y **piden un espacio en la   Comisión Política de la Cumbre Agraria** para que desde ese lugar el gobierno se pueda dar respuestas sobre los conflictos y violaciones a los derechos humanos que ha generado el proyecto de Empresas Públicas de Medellín.

<iframe id="audio_11967576" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11967576_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
