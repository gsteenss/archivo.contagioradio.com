Title: 5 destinos que el turismo puede proteger
Date: 2018-12-28 17:33
Author: AdminContagio
Category: Ambiente
Tags: deforestación, derechos de los indígenas, ecoturismo, medio ambiente
Slug: 5-destinos-turismo-puede-proteger
Status: published

###### [Foto: Gavin Rough] 

###### [28 Dic. 2018] 

[Este fin de año, miles de ciudadanos saldrán de las ciudades a conocer los patrimonios culturales y las maravillas naturales de las regiones colombianas, muchas de ellas ubicadas en zonas de gran diversidad biológica.]

[Y en medio de estos merecidos descansos las personas que practican el turismo pueden aportar a la conservación del medio ambiente al seguir los consejos recomendados por guías, comunidades locales y asociaciones turísticas en varios destinos.]

[Por ello, es necesario tener en cuenta que varias de esas maravillas hoy se ven afectadas por actividad humana. El 46 % de ecosistemas en Colombia están en peligro y solo el 16% cuentan con protección, según un estudio del 2017 de la Universidad Javeriana, Conservación Internacional y el Instituto von Humboldt.]

### [Ciudad Perdida] 

[En el siglo VIII, comunidades del pueblo tayrona construyeron el asentamiento de piedra  en la selva de la Sierra Nevada. Después de ser abandonada por los tayrona en la era de conquista española, los restos de la Ciudad Perdida fueron desconocidas por el mundo hasta 1973, cuando guaqueros la descubrieron por accidente.]

[Hoy en día, el asentamiento de más de 30 hectáreas es considerado patrimonio cultural y administrado por el Instituto Colombiano de Antropología e Historia (ICANH). Notable por su biodiversidad única y por ser el territorio ancestral de cuatros pueblos indígenas, la Sierra Nevada fue declarada por el gobierno como zona de protección y desarrollo de los recursos naturales renovables y del medio ambiente este año.]

[La Ciudad Perdida, tal como el bosque húmedo subtropical de la Sierra, se ha visto afectada por el conflicto armado, el narcotráfico y recientemente, el cambio climático. En el 2016, comunidades indígenas manifestaron su preocupación sobre el interés de parte de Elemental SAS en construir una hidroeléctrica cerca del Parque Nacional Sierra Nevada de Santa Marta.]

[Consejos: Apoyar a compañías de turismo indígenas, denunciar la presencia de grupos armados ilegales, tener conciencia crítica frente a un proceso de privatización que pueden estar enfrentando los territorios de comunidades indígenas y por último, no arrojar basura.]

\[aesop\_image img="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/CAÑO\_CRISTALES\_EL\_RÍO\_DE\_COLORES.jpg" panorama="off" align="center" lightbox="on" captionposition="left" revealfx="off" overlay\_revealfx="off"\]

### [Caño Cristales] 

[Por solo unas semanas, entre septiembre y noviembre, el río Caño Cristales, normalmente un opaco verde-azul, se vuelve color de arcoíris: un paisaje de rojos, amarillos, negros, verdes y azules intensos. Por su apariencia singular, Caño Cristales ha sido denominada “ el río más hermoso del mundo” y atrae miles de turistas a esta zona aislada de la Macarena, Meta, antiguamente poblada por el grupo indígena los guayaberos. ]

[En los últimos nueve años, Comacarena, administrador del destino turístico, ha cerrado por temporadas el acceso público al río debido a la sequía, causada por el cambio climático y la tala ilegal, que amenaza el frágil ecosistema de plantas acuáticas que brinden al río su color caleidoscopico. Otra amenaza al río de cinco colores son los 14 proyectos de explotación de hidrocarburos que existen en la Macarena. A pesar de la voluntad de los habitantes del municipio, la Registraría no aprobó que la consulta popular de la Macarena siguiera debido a una carencia de recursos. ]

[Consejos: Apoyar el derecho de las comunidades de La Macarena a la consulta popular en contra de la extracción de hidrocarburos; pensar críticamente sobre las causas fundamentales de la sequia; y respetar los senderos, los cuales fueron completados este año para disminuir el impacto humano en los humedales. ]

\[aesop\_image img="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Paramo\_de\_Sumapaz\_-\_Yuri\_Romero\_Picon.jpg" panorama="off" align="center" lightbox="on" captionposition="left" revealfx="off" overlay\_revealfx="off"\]

### [Parque Nacional Natural Sumapaz] 

[Ubicado a 4.000 metros sobre el nivel del mar, en la Cordillera Oriental, el Parque Nacional Natural Sumapaz es único en el mundo, considerado el más grande a nivel mundial. Antes de la llegada de conquistadores en el siglo XVI, los Muisca prohibían el ingreso a esta zona de páramo, bosque altoandino y humedales al considerarlo un sitio sagrado.]

[Hoy en día, los más de 330.000 hectáreas de páramo son administrados por el Sistema Nacional de Áreas Protegidas (SINAP) y atraen hasta 1.500 de visitantes cada fin de semana, de acuerdo a]

[ El Tiempo. Este hecho ha indignado la población local, el cual asegura que el turismo hecho de manera irresponsable es un detrimento al medio ambiente.]

[Además, posibles proyectos de extracción de gas natural en los alrededores del Sumapaz y el Páramo de Chingaza han causado alerta a las poblaciones locales y activistas ambientales debido a que estas actividades podrían contaminar la fuente principal de agua para la ciudad de Bogotá. En un intento de proteger el páramo de los proyectos extractivos, los habitantes de Fusagasugá prohibieron el fracking en su municipio a traves de una consulta popular en octubre. Otras consultas populares en los alrededores del Sumapaz, como en los municipios de Pasca y San Bernardo, han quedado suspendidas.]

[Consejos: Apoyar el derecho de los habitantes en los alrededores del Sumapaz a la consulta popular, apoyar a agencias de turismo locales y rurales y denunciar agencias de turismos que usen los depósitos de agua para uso personal.]

###  

### [Capurganá] 

[ Las playas prístinas de Capurganá han atraído turistas locales y del extranjero a la costa atlántica del departamento del Chocó desde los años 70s, cuando primero se comenzaron a construir albergues para visitantes.  “La tierra del ají,” así denominado por los pobladores ancestrales, los Kuna, es un paraíso que sufre de la contaminación de ríos y la erosión costera, causado por actividad de grupos armados, economías ilícitas, el cambio climático y corrupción.]

[Los pueblos afrodescendientes que habitan el área han resistido a estas agresiones en contra del medio ambiente ante la indiferencia de autoridades como Codechocó, el Instituto de Investigaciones y Ministerio de Ambiente, dijo el empresario y economista Carlos Castillo al diario El AfroBogotano.]

[Consejos: Apoyar entidades locales como Cocomasur, que cuida al bosque chocoano de la deforestación; pensar críticamente al rol del Estado en proveer seguridad ante la presencia de economías ilegales y grupos armados; y no arrojar basura, lo cual se ha acumulado en playas alrededores y amenaza la vida marina.]

\[aesop\_image img="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Amazonian\_rainforest.jpg" panorama="off" align="center" lightbox="on" captionposition="left" revealfx="off" overlay\_revealfx="off"\]

### [Amazonas] 

[La Amazonía es unos de los ecosistemas más importantes que le pertenece a Colombia debido a que sus bosques húmedos absorbe un 10% de las emisiones de dióxido de carbono y produce el 20% del oxígeno del mundo. A pesar de su importancia a nivel internacional, "el pulmón del mundo" esta en riesgo debido a la industria maderera, la agricultura, la ganadería, la producción de cultivos de uso ilícito y la minería ilegal, algunos de las causas por la deforestación de 220.000 de hectáreas en el país en el 2017, de acuerdo a el Instituto de Hidrología, Meteorología y Estudios Ambientales (IDEAM). Sucesivamente, se esta perdiendo el hábitat natural de especies en vía de extinción, como los jaguares y los defines rosados.]

[El gobierno declaró 8.000.000 hectáreas del Amazonía como zona protegida en abril en un medida para preservar la selva ante la deforestación. La Amazonía también es protegida adentro de los 185 resguardos indígenas, que ocupan más de 26.000.000 hectáreas, según el Sistema de Información Ambiental Territorial de la Amazonía Colombiana.]

[Consejos: Crear consciencia sobre el derecho de los pobladores indígenas a las consultas previas, respetar a los ríos y bosques y no visitar sitios como Puerto Alegría, donde los animales silvestres de la Amazonía, como micos, osos perezosos y tortugas, son capturados y utilizados para atraer turistas.]

###### [Reciba toda la información de Contagio Radio en [[su correo] 
