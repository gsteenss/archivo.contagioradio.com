Title: Fecode, CUT y USO iniciarán paro nacional este 22 de abril
Date: 2015-04-21 12:12
Author: CtgAdm
Category: Educación, Nacional
Tags: Central Unitaria de Trabajadores de Colombia, CUT, eduación, fecode, Federación de Trabajadores de la Educación, gina parody, Gobierno Nacional, Juan Manuel Santos, la Unión Sindical Obrera, magisterio, Ministerio de Educación, Movilización, Protesta magisterio México, USO
Slug: fecode-cut-y-uso-iniciaran-paro-nacional-este-22-de-abril
Status: published

##### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4387055_2_1.html?data=lZilmZWZeY6ZmKiakp6Jd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRisbX0MnSh5enb6TJtZDmjbq3k4zdz87Qy8bWaaSnhqae0JDUpdPjjNPOxc7TssLgjMrZjZeWb8XZjMbP1JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Elias Fonseca, ejecutivo de la CUT] 

La Federación de Trabajadores de la Educación (Fecode), Central Unitaria de Trabajadores de Colombia (CUT) y la Unión Sindical Obrera (USO), anunciaron que **mañana 22 de abril empezarán una jornada de paro nacional**, debido a que el gobierno del presidente Juan Manuel Santos, “**no ha respondido satisfactoriamente a un solo punto del pliego de peticiones** presentado por el magisterio el pasado 26 de febrero”, asegura Elias Fonseca, docente y ejecutivo de la CUT.

**Incremento y nivelación salarial, asensos, salud y prestaciones** son los tres puntos sobre los cuales se centran las exigencias de los maestros y en general, los trabajadores estatales, que denuncian que el gobierno **únicamente aumentó un 3,66%** el salario, lo que “es humillante”, expresa Fonseca.

El magisterio asegura que **los profesores son los empleados estatales peor pagos,** responsabilidad que cae en el Presidente Santos, que a través de la ministra de educación Gina Parody, ha dicho que “no hay absolutamente ninguna posibilidad de dinero, para hacer mejorar las condiciones de los profesores”, afirma Elias Fonseca, quien agrega que “**el gobierno se desentiende de atender los problemas de la educación pública y el magisterio**”.

La jornada de paro empezará con **asambleas municipales y movilizaciones programadas por los sindicatos en todo el país,** “llamamos a que los maestros acudan a las manifestaciones”, expresa Elias Fonseca, quien asegura que el ambiente es favorable para desarrollar el paro, y por ello saben que “con fuerza, seguridad y convicción, la huelga es la única herramienta para cambiar las decisiones del gobierno”.

**Este 20 y 21 de abril se llevan a cabo reuniones con el Ministerio de Educación** para continuar con las negociaciones, sin embargo, hasta que el gobierno no responda a las solicitudes de los trabajadores, la CUT, Fecode y la USO siguen firmes con la decisión de realizar el paro nacional.
