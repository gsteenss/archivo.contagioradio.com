Title: Ni Ecopetrol, ni contratistas atienden derrame de petróleo en cuerpo hídrico de Barrancabermeja
Date: 2020-02-06 17:07
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Cenit, contaminación, petroleo
Slug: ni-ecopetrol-ni-contratistas-atienden-derrame-de-petroleo-en-cuerpo-hidrico-de-barrancabermeja
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/petroleo10.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ImagendeArchivo

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/heidy-sanchez-sobre-aplazamiento-debate-control_md_47430964_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Nubia Linares | Propietaria de predio afectado en Barrancabermeja

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

La Corporación Regional Yariguíes (CRY-GEAM) denunció este miércoles los daños generados en la comunidad de El Llanito en Barracabermeja a causa del derrame de petróleo en una de las fuentes hídricas principales donde pasa oleoducto de Ecopetrol y Cenit. (Le puede interesar: <https://archivo.contagioradio.com/plan-piloto-para-fracking/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo mayores afectados de este hecho son las comunidades rurales y campesinas, entre ellas la p**ropietaria del predio Nuevo Edén Nubia Linares**, quien afirmó que este derrame viene afectando su territorio desde el 23 de noviembre del 2019, *"**estoy enferma por culpa de esto, y la empresa no hace nada**, no da respuestas concretas ni genera acciones para descontaminar el agua y evitar que esto siga pasando"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La propietaria también agregó que desde hace años empresas como **Ecopetrol y Cenit ingresar sin permiso a su predio, causando daños ambientale**s no solo a esta finca sino a los 14 parcelas ubicadas en la vereda.(Le puede interesar: <https://archivo.contagioradio.com/esmad-agrede-a-campesinos-que-se-oponen-a-multinacional-petrolera-en-caqueta/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El **daño exactamente se produjo en un cuerpo hidrico denominado Jangüey**, superficie similar a una laguna usada especialmente para almacenar agua en los periodos de seguía, este espacio según los habitantes es cercano al Caño Jeringa, el cual comunica directamente con la Ciénaga Magdalena, *"el crudo va corriendo cada vez que llueve, si no se hace algo generará un daño mayor"* añadió a Linares.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro según **Oscar Sampayo** encargado de este caso e integrante de la **Corporación Podion** informó que **Ecopetro**l, empresa responsable de este oleoducto **afirma que los daños el cuerpo hídrico son producto de una válvula ilícita ubicada en este lugar en el 2019.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"A febrero de 2020 no tenemos evidencia de que esta situación se haya presentado y por el contrario **tenemos la versión del Ismocol contratista de  Ecopetrol y Cenit que manifiesta que el derrame se debe a una porosidad en la línea de conducción** que pasa por esa vereda"*, afirmó Sampayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y resaltó, que **Ecopetrol debe asumir la responsabilidad de este daño**, y "recuperar, contener y reparar las afectaciones que se dieron sobre este predio campesino", por su parte la comunidad del El Llanito junto con CRY- GEAN hacen un llamado a las autoridades ambientales y civiles a que se apropien  de esta emergencia y tomen acciones inmediatas. ( Le puede interesar: <https://www.justiciaypazcolombia.com/descontaminacion-por-petroleo-derramado-genera-nuevas-afectaciones-en-la-comunidad-de-bajo-lorenzo-zrcpa/>)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Nosotros somos pequeños fanicultores, yo soy madre cabeza de familia y con él sudor de la frente trabajamos para sostener nuestro terrenos y no hay excusa para que vengan y hagan esas empresas lo que quieran acá sin responder "*
>
> <cite> Nubia Linares - Propietaria del predio Nuevo Edén</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->
