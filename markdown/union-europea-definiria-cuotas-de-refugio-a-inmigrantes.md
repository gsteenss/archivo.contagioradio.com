Title: Unión Europea definiría cuotas de refugio a inmigrantes
Date: 2015-09-07 16:19
Category: Otra Mirada
Tags: Alemania, Ángela Merkel, francia, Inmigración, inmigrante, Plan de Cargas, Unión europea
Slug: union-europea-definiria-cuotas-de-refugio-a-inmigrantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/imigrantes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: posta 

###### [ Sep 5 2015] 

Francia y Alemania lanzaron una propuesta denominada “cargas” para distribuir a los países miembros de la Unión Europea una cantidad de inmigrantes por los cuales cada nación **brinde las condiciones de asilo necesarias a los 160.000 refugiados** que hay actualmente.

La propuesta que **se presentará formalmente ante los miembros de la UE el 14 de septiembre**, dice que esta distribución se realizaría basándose en criterios como población, desempleo, número de refugiados ya presentes en el país y el peso de su economía.

Según esos criterios a Alemania le correspondería el mayor porcentaje (**21,91%**), seguido de Francia (**16,88%**), España (**10,72%**), Polonia (**6,65%**) y Holanda (**5,12%**), sin embargo países como Polonia , la República Checa, Eslovaquia y Hungría rechazan esta propuesta y solicitan que la cuota sea voluntaria y puedan dar asilo a menos personas.

El llamado de Ángela Merkel para que los países miembros acojan esta propuesta ha estado dirigido para que s**ean coherentes con los elementos fundacional y fundamentales de estos estados los cuales son los Derechos Humanos**.

**Los repartos serían así:**

Polonia 7,7% (9.287), Holanda 6,0% (7.214), Rumanía 3,9% (4.646), Bélgica 3,8% (4.564), Suecia 3,7% (4.469), Austria 3,0% (3.640), Portugal 2,6% (3.074), República Checa 2,5% (2.978), Finlandia 2,0% (2.398), Bulgaria 1,3% (1.600), Eslovaquia 1,3% (1.502), Croacia 0,9% (1.064), Lituania 0,7% (780), Eslovenia 0,5% (631), Letonia 0,4% (526), Luxemburgo 0,4% (440), Estonia 0,3% (373), Chipre 0,2% (274), Malta 0,1% (133)
