Title: Mujeres en América inician un camino por abolir las cárceles
Date: 2019-07-22 16:02
Author: CtgAdm
Category: El mundo, Mujer
Tags: Cárceles colombianas, Derechos Humanos, LGBTTTIQ+, Mujeres y cárceles
Slug: mujeres-en-america-inician-un-camino-por-abolir-las-carceles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/mujeres-carceles-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Los días 17 y 18 de Julio **Bogotá fue la sede del primer Encuentro de Mujeres de las tres Américas,** **con participación de diversidades LGBTTTIQ+,** que estuvieron reclusas en las cárceles. Mujeres que, tras haber vivido esta experiencia, **están exigiendo la abolición del sistema punitivo carcelario**. Ya que en vez de apostar por una real resocialización de quienes están allí adentro, esos establecimientos  castigan, torturan y vulneran los derechos humanos de la población reclusa.

Durante el evento, las más de 58 asistentes, de países como Estados Unidos, Argentina, El Salvador, Brasil, Colombia, México, entre otros, **intercambiaron sus vivencias frente a las difíciles situaciones que tuvieron que pasar estando dentro de los centros penitenciarios** y las problemáticas que estos lugares generan para las mujeres.[(Le puede interesar, Expreso Libertad: "Maternidad en las cárceles"](https://www.youtube.com/watch?v=d7AJEjcwhXA))

### **¿Quienes son las mujeres que entran en las cárceles, y por qué abolir?** 

Una de las situaciones que evidenciaron las mujeres tuvo que ver con la población femenina y de **diversidades LGBTTTIQ+ que se encuentran reclusas, ya que la gran mayoría de ellas pertenecen a sectores marginales,** en donde existen altos índices de violencia, poca presencia del Estado, y por tal razón pocas garantías para el desarrollo pleno de sus vidas. [(Le puede interesar, Expreso Libertad: Enfoque de género en las cárceles - Expreso libertad)](https://www.youtube.com/watch?v=RFPuQGx3k8E)

Razón por la cual, las mujeres manifestaron que hay una urgencia porque los Estados de las tres Américas comprendan que una de las formas de disminuir la presencia de ellas en las cárceles, es poner freno al aumento de los círculos de violencia, pobreza y exclusión, que solo pueden superarse con **políticas integrales que cubran y protejan los derechos humanos como a la salud, educación, trabajo, entre otros.**

### **Las Cárceles son promotoras de la desigualdad** 

Otra de las realidades que deben vivir las mujeres, es la precarización o nulidad total de los derechos humanos al interior de las cárceles. Esto debido a que **los países no invierten lo necesario en los centros penitenciarios para garantizar que a las personas no se les vulneren derechos básicos** como a la salud, a la alimentación, el libre desarrollo de la personalidad, entre otros. [(Le puede interesar, Expreso Libertad: Crisis de salud en las cárceles de Colombia).](https://www.youtube.com/watch?v=LUnyqJ75XAk)

En casos puntuales de países como México o Colombia, **las mujeres señalaron que a estos hechos se suman los carteles de corrupción que se roban los recursos delegados para las prisiones,** sin que existan mayores medidas por parte de los gobiernos e instituciones para acabar o desarticular esas redes.

Estos actos de corrupción y la falta de atención estatal, provocan que las mujeres sufran afectaciones a su salud, al acceso a una buena alimentación o incluso evita que puedan redimir sus penas, ya que los talleres o cursos que permiten esa reducción de tiempo en sus condenas, tienen poco cupos y casi siempre son ofertas que profundizan los roles de género.

Finalmente, **cuando la mujer cumple su tiempo en la cárcel, no hay garantías para su retorno a la sociedad**, debido a que en la gran mayoría de los casos, no logra encontrar trabajos estables, resultado de la estigmatización, generando que tampoco pueda acceder a una educación, vivienda digna, entre otras cosas y aumentando la posibilidad de su retorno a los círculos de violencia.(Le puede interesar: [Mujeres se reúnen para denunciar discriminaciones tras salir de las cárceles)](https://archivo.contagioradio.com/mujeres-denunciar-discriminaciones-carceles/)
