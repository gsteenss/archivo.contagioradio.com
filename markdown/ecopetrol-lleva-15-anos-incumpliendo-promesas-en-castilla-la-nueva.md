Title: Ecopetrol lleva 15 años incumpliendo promesas en Castilla la Nueva
Date: 2018-02-21 08:30
Category: DDHH, Nacional
Tags: Castilla La Nueva, Ecopetrol, Meta, trabajadores ecopetrol
Slug: ecopetrol-lleva-15-anos-incumpliendo-promesas-en-castilla-la-nueva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/ecopetrol-e1518546206771.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Comunitaria de Acacías] 

###### [20 Feb 2018] 

Tras las protestas de algunos de los trabajadores de Ecopetrol en Castilla La Nueva, Meta, la empresa ha dicho que se han presentado actos vandálicos. Por su parte, quienes se encuentran desarrollando la protesta, indican que no es la comunidad quien está desarrollando estos actos y la empresa continúa **sin brindarle las garantías suficientes** para desarrollar su trabajo.

De acuerdo con Vitelio Álvares, líder de la comunidad de Castilla La Nueva, “aún no hay claridad sobre **quién está desarrollando estas actividades** pues se tiene conocimiento de que hay un derrame de crudo”. Manifestó que los daños a las infraestructuras de la empresa, no son hechos por la comunidad y esto aumenta la posibilidad de que no se llegue a un arreglo entre los trabajadores y la empresa.

### **Ecopetrol no ha cumplido con acuerdos desde hace más de 15 años** 

Los trabajadores de la empresa y la comunidad de Castilla La Nueva se encuentran desarrollando un cese de actividades teniendo como premisa una falta de cumplimiento en algunos acuerdos. En repetidas ocasiones han denunciado que la empresa y sus filiales, **llevan trabajadores de otras zonas del país** y no les dan la oportunidad laboral a los habitantes de ese municipio en el Meta.

Álvarez enfatizó que “la gente **está cansada de las mentiras de Ecopetrol** y la comunidad sólo pide trabajo”. Recordó que la empresa les había pedido a los jóvenes que se capacitaran para poder ingresar a trabajar. Sin embargo, una vez desarrollaron esta capacitación, no les han brindado las oportunidades laborales. (Le puede interesar:["Habitantes de Castilla La Nueva dicen no ser terroristas y protestas contra ECOPETROL continúan"](https://archivo.contagioradio.com/habitantes-de-castilla-la-nueva-dicen-no-ser-terroristas-y-protestas-contra-ecopetrol-continuan/))

Además, recalcó que hay incumplimientos relacionados con la retribución social que debe hacer Ecopetrol en el municipio y “ellos no están dejando nada, **solamente están dejando ruinas**”. Indicó que, “si todos los días están sacando petróleo, deben dejarle algo a Castilla, aunque sea trabajo para los habitantes de acá”.

### **Planes de manejo ambiental no han sido positivos** 

La comunidad también ha puesto diferentes quejas ante instituciones como la Autoridad de Licencias Ambientales argumentando que **no ha habido un plan de manejo ambiental** que proteja el derecho de los ciudadanos a un ambiente sano. Han explicado que “cuando están desarrollando perforaciones el agua llega a los afluentes y cuando llueve el agua recoge los químicos y son botadas en potreros”.

Finalmente Álvarez denunció que en los afluentes de agua **“sale petróleo** y nos han dicho que son algas como si no superamos que es petróleo”. Recordó que “la comunidad está cansada” de poner diferentes quejas sin que sean escuchados por lo que han tenido que tomar la decisión de continuar protestando.

###### Reciba toda la información de Contagio Radio en [[su correo]
