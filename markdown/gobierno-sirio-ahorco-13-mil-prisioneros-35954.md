Title: Gobierno Sirio ahorcó a 13 mil prisioneros en 4 años
Date: 2017-02-07 16:17
Category: El mundo
Tags: Aministía Internacional, carcel, Siria, Torturas
Slug: gobierno-sirio-ahorco-13-mil-prisioneros-35954
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/CARCEL3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semanario Polémica] 

###### [7 Feb. 2017] 

Según un informe de la organización Aministía Internacional titulado “***El matadero humano: ahorcamientos masivos y exterminio en la prisión de Saydnaya”* todas las semanas entre el 2011 y 2015 fueron ahorcadas hasta 50 personas,** en su mayoría civiles a los que se les consideró como contradictores del gobierno de Bashar al Assad.

El informe asegura que **en esta prisión también se imponen torturas y privación de acceso a agua, alimentos y servicios de salud para los presos**, lo que en algunos casos les ha costado la vida. Le puede interesar: [Los niños y niñas sirias](https://archivo.contagioradio.com/los-ninos-y-ninas-sirias/)

Para Aministía, al interior de esta cárcel **se hicieron durante ese tiempo ejecuciones extrajudiciales de manera continua “de madrugada y en secreto”**, además la organización dice que podrían existir indicios de que este tipo de prácticas continúan llevándose a cabo en el centro penitenciario.

Este informe fue logrado gracias a una serie de investigaciones realizadas por la organización, acompañadas de entrevistas a 84 personas dentro de las cuales se encuentran **detenidos, guardias, abogados, jueves y expertos de talla nacional e internacional. **Le puede interesar: [22 niños muertos por bombardeos en Siria](https://archivo.contagioradio.com/22-ninos-muertos-por-bombardeos-en-siria/)

Lo más preocupante del asunto, señala Aministía, es que de esos 13 mil prisioneros ninguno pudo tener acceso a un juicio o “algo similar”, dado que antes de someterlos al ahorcamiento les permitían presentar sus alegatos durante dos minutos, en lo que se conoce como Tribunal Militar de Campaña.

Ese “juicio” consiste en que un juez pregunta al acusado su nombre completo y su responsabilidad en algún delito; sin importar la respuesta que dé el implicado, la decisión final será llevarlos a la horca como culpable.  “Eso no es un tribunal y no tiene nada que ver con el Estado de derecho pues **los condenados conocen su pena minutos antes de ser ahorcados” precisó amnistía.**

\[embed\]https://www.youtube.com/watch?v=Yzgb2vDqcHs\[/embed\]

Adicionalmente, Amnistía asegura que los detenidos no pueden acceder a un abogado para defenderse y son engañados con que serán trasladados  a prisiones. Sin embargo, “con los ojos vendados, minutos después se les da una golpiza fuerte, se les lleva a otro edificio donde se les ahorca (…) **los cadáveres se sacan en camiones y son enterrados en fosas comunes sin informar  a sus familias”. **Le puede interesar: ["El mundo debe levantarse por los niños de Alepo"](https://archivo.contagioradio.com/el-mundo-debe-levantarse-para-los-ninos-de-alepo/)

Muchos de los testimonios recogidos por Aministía son desgarradores, **varios presos aseguraron haber sido violados e incluso en algunos casos obligados a violar a otras personas.** Adicionalmente fueron torturados y maltratados con fuertes palizas.

Esta organización internacional **completa 30 años denunciando la tortura realizada por autoridades sirias en cárceles** de ese país, práctica que ha considerado como sistemática desde 2011 para reprimir las manifestaciones sociales dadas luego del estallido de la guerra en ese país.

Amnistía hace un llamado urgente a la comunidad internacional y al Consejo de Seguridad de la ONU para que de manera pronta se puedan tomar medidas que pongan fin a este tipo de prácticas, "**los horrores descritos en este informe revelan una campaña secreta y monstruosa, autorizada al más alto nivel por el Gobierno sirio**, con el objetivo de aplastar cualquier forma de disenso dentro de la población siria" dijo la subdirectora de Investigación de la oficina regional de Amnistía Internacional en Beirut, Lynn Maalouf.

\[embed\]https://www.youtube.com/watch?v=RtoWWGpYUVw\[/embed\]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
