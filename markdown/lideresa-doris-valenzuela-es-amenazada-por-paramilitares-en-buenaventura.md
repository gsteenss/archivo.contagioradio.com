Title: Lideresa Doris Valenzuela es amenazada por paramilitares en Buenaventura
Date: 2016-07-12 18:05
Category: DDHH, Nacional
Tags: amenazas paramilitares, buenaventura
Slug: lideresa-doris-valenzuela-es-amenazada-por-paramilitares-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/BUENAVENTURA-03-10-2014-BN-51.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Lasillavacia] 

###### [12 Jul 2016]

De acuerdo con las declaraciones de Doris Valenzuela, en la mañana del 8 de julio recibió una llamada de Diego Fernando Caycedo, conocido bajo el alias de "Diego Macuña" líder paramilitar de Buenaventura, en donde **la amenazaba con asesinar a sus hijos y luego acabar con su vida.**

La amenaza de alias "Diego Macuña" estaría fundamentada en las diferentes denuncias que la lidereza ha hecho frente al asesinato de su hijo Cristian Dainer Aragón Valenzuela, que habría sido asesinado por **no querer trabajar con el paramilitar**. En la amenaza "Diego Macuña" afirma qu**e tiene contactos en la policía** y que esta iba a ser la forma de ubicar a la familia de la lidereza.

En la actualidad Doris Valenzuela no posee ningún tipo de seguridad por parte de las Autoridades e índica que las investigaciones que se adelantan por paramilitarismo en la zona de Buenaventura, no se han adelantado lo suficiente y que tampoco posee toda la información por miedo a que el **paramilitarismo siga actuando en contra de su familia**.

"Me estoy quedando sin esperanzas, me estoy quedando como sin destino, **se supone que si uno es víctima de un hecho de violencia ya no se debe repetir** y estoy asustada porque si me hablo de la policía es porque realmenete tiene los contactos y podría encontrarme" afirmo la lidereza.

<iframe src="http://co.ivoox.com/es/player_ej_12203455_2_1.html?data=kpefkpiYeZahhpywj5aVaZS1lJmah5yncZOhhpywj5WRaZi3jpWah5yncaXj087gjbvFsMbi29rSzsaJdqSfrc7Rx9fJvsKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
