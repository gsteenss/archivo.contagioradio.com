Title: Comisión parlamentaria monitorea DDHH en Colombia
Date: 2020-02-21 18:43
Author: AdminContagio
Category: DDHH, Nacional
Tags: acuerdos de paz, cooperación internacional, DDHH
Slug: comision-parlamentaria-monitorea-ddhh-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/comisión-parlamento.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/justice-for-colombia-comision-parlamentaria-monitorea-ddhh-en_md_48222862_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Justice for Colombia

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Una delegación de trece parlamentarios y sindicalistas de distintos países Europeos se encuentra visitando Colombia, como parte la estrategia de monitoréo de la **organización Justice for Colombia**, a los Acuerdos de Paz y las garantías para el ejercicio de los derechos humanos en el país.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Preocupa la situación de derechos humanos en el país

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las y los delegados visitaron la zona de reincorporación de **San José de León, en el municipio de Mutatá, Antioquia**; y la zona de biodiversidad de **La Madre Unión**, en el territorio colectivo de **La Larga Tumaradó**, en **Chocó**; y lograron constatar el muy bajo nivel de implementación de los Acuerdos de Paz y la **crisis humanitaria** que afrontan las comunidades del Bajo Atrato quienes están sometidos al accionar de grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con **Sergio Bassolli**, integrante de esta delegación, luego de estos encuentros hay "*preocupación por las condiciones de protección de los derechos humanos"*. (Le puede interesar: <https://archivo.contagioradio.com/es-declarada-crisis-humanitaria-en-tres-regiones-del-pais/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar de reconocer que las instituciones están haciendo su trabajo en la implementación de los Acuerdos de La Habana, aseguró que dicho proceso no se está desarrollando en el tiempo prudencial para evitar una ola de violencia y un ejercicio de control por parte de actores que están llegando a los territorios, tiempos que tampoco corresponden a las necesidades de la gente. (Le puede interesar: <https://archivo.contagioradio.com/78-de-los-casos-de-asesinatos-a-periodistas-se-encuentran-en-la-impunidad-flip/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"*El proceso está en marcha, pero las expectativas que tienen las personas que han tomado el campo de la paz como elección para sus vidas piden que este proceso vaya mucho más rápido y que las respuestas sean mucho más concretas*" afirmó Bassolli.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Preocupaciones de Parlamentarios y Sindicalistas se harán llegar a Europa

<!-- /wp:heading -->

<!-- wp:paragraph -->

La comisión se encuentra conformada por Clive Efford, miembro del Parlamento británico por el Partido Laborista; Adelina Escandell, senadora catalana por el partido ERC-Sobiranistes; Neil Findlay MSP, miembro del parlamento escoces por el Partido Laborita y Lloyd Russell-Moyle, integrante del parlamento británico por el Partido Laborita.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo esta delegación cuenta con la presencia de 8 sindicalistas europeos: Sergio Bassoli, oficial político en el departamento de Política Internacional de la Confederación General Italiana del Trabajo (CGIL); Josie Bird, presidenta de UNISON, el sindicato de servicios públicos más grande de Gran Bretaña; Douglas Chalmers, presidente del Sindicato UCU que agremia a las y los trabajadores y profesores de universidades y colegios de educación de adultos; Dave Kitchen , presidente del sindicato NASUWT que representa a más de 280.000 educadores y educadoras en Gran Bretaña e Irlanda del Norte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, también participaron Gerry Murphy presidente de la Central Sindical Irlandesa (ICTU); Susan Quinn, encargada de educación en el sindicato IES que representa educadores y educadoras escoceses; Hans A. *S*ørensen, presidente regional de la Federación Unida de Trabajadores Danéses, el sindicato más grande de Dinamarca y Hassan Dodwell, director de Justices for Colombia y coordinador de este proyecto de monitorio a la implementación de los Acuerdos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Vamos a mantener nuestro apoyo firme a la implementación de los Acuerdos de Paz en Colombia"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Hassan Dodwell, la situación del proceso de paz en Colombia no ha avanzado mucho "*hemos escuchado muchas preocupaciones, especialmente por el avance tan lento hacia la reincorporación, donde muchos no han recibido garantías, ni dinero para la ejecución de sus proyectos"*, y añadió que es preocupante la situación que viven las y los defensores y líderes sociales del país.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"muchos no han recibido garantías, ni dinero para la ejecución de sus proyectos"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Sin embargo destacó que este es un momento histórico para el desarrollo del país y que por tanto se debe seguir trabaja en la implementación de los Acuerdos, *"los Acuerdos se deben ejecutar de manera integral, y esto aún no se ha hecho, consecuencia de ello es todo lo que está pasando en los territorios"*. (Le puede interesar: <https://www.justiciaypazcolombia.com/destruccion-de-vallas-de-identificacion-de-la-comunidad-sikuani/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Reiteró también el apoyo que hacen como organización al país, *" el mensaje que llevaremos a Europa es que el Proceso de Paz hay que seguirlo apoyando, vamos a mantener nuestro apoyo muy firme y seguiremos haciendo seguimiento al cumplimiento a la garantía de la vida de los voceros de las comunidades, así como el alto a los lamentables ataques a los DDHH de miles de colombianos*".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente añadió que para que el proceso pueda avanzar se requiere un verdadero compromiso por parte del Gobierno, *"el Gobierno no ha participado tanto como la gente lo ha esperado, en la mesa de garantías por ejemplo, allí no vemos un interés estatal, y esto es visible en el malestar de muchas personas con las que nos hemos reunido, que afirman que no hay compromiso total de este Gobierno para apoyar la implementación"*.

<!-- /wp:paragraph -->
