Title: ¡No a la minería, sí a la vida! Comunidad de Mercaderes
Date: 2019-08-03 14:11
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Cauca, Mercaderes, Mineria, Minería ilegal
Slug: no-a-la-mineria-en-mercaderes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/EBDbFunWsAEAZmH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @asociacionminga·] 

Con 6.449 votos en contra **(99,46% del total), la comunidad de Mercaderes le dijo no a la extracción de metales e hidrocarburos en su territorio,** mediante la onsulta popular legítima. La Consulta debió ser auto- gestionada por la comunidad, ya que la registraduría negó el apoyo, aludiendo no tener los recursos suficientes para su realización. (Le puede interesar [En Mercaderes, Cauca, también quieren proteger la vida por encima de la Minería](https://archivo.contagioradio.com/en-mercaderes-cauca-tambien-quieren-proteger-la-vida-por-encima-de-la-mineria/))

### **¿Legal o legítima?**

Según explicó Uber Castillo, integrante de la Consulta Popular Legítima de Mercaderes, este mecanismo de participación es legítimo porque la Registraduría Nacional no permitió su realización de forma legal pese a que se presentaron todos los requerimientos para que así fuera. En su momento, el ente dijo que no era posible garantizar la Consulta porque el Ministerio de Hacienda no había apropiado los recursos económicos necesarios para su desarrollo.

No obstante, Castillo dijo que entendían que el tema se escacaba del plano económico a lo político, porque hay un trasfondo en que el Gobierno evita que los habitantes de los territorios decidan sobre lo que ocurre en el suelo o subsuelo del mismo. De allí que hayan decidido llamar a esta, como una consulta popular legítima, como una forma de promulgar el respeto de la misma. (Le puede interesar: ["Corte Constitucional deja sin piso jurídico futuras consultas populares"](https://archivo.contagioradio.com/corte-constitucional-deja-sin-piso-juridico-futuras-consultas-populares/))

### **¿Por qué evitar el extractivísmo en Mercaderes?**

Mercaderes ya tuvo experiencia de las graves consecuencias de las actividades mineras, en especial las ilegales, en cuanto a la afectación de sus ríos**, dada la extinción del río San Bingo,** uno de los primeros en desparecer a causa del **vertimiento de mercurio y cianuro.** (Le puede interesar: [Mercaderes Cauca le dice no a la minería](https://archivo.contagioradio.com/mercaderes-cauca-le-dice-no-a-la-mineria/)

Organizaciones sociales y ambientales apoyaron a la comunidad en la consulta popular, y rechazar cualquier tipo de desarrollo minero en el municipio. Uno de los lugares de manifestación fueron las redes sociales, donde se compartió el hashtag \#MercaderesdiceNO; igualmente en Bogotá, encabezados por el abogado y defensor de derechos humanos Alirio Uribe Muñoz, se realizó una movilización para la protección del agua en el Municipio.

> Desde Bogotá se apoya la consulta popular de Mercaderes Cauca! No a la Minería! Defensa del territorio y del agua! [@Ccajar](https://twitter.com/Ccajar?ref_src=twsrc%5Etfw) [pic.twitter.com/GkufH3JNof](https://t.co/GkufH3JNof)
>
> — Alirio Uribe Muñoz (@AlirioUribeMuoz) [August 2, 2019](https://twitter.com/AlirioUribeMuoz/status/1157391510188494849?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Ambientalistas buscarán blindar a Mercaderes de la extracción de hidrocarburos**

Castillo explicó que lo que vien**e en términos del proceso legítimo es mantener vigente la motivación de la gente,** que las personas se den cuenta que es posible participar y defender el agua. En términos de lo legal, declaró que contarían con el Concejo municipal y el actual Alcalde de Mercaderes como aliados para aprobar un Acuerdo local en el que se consigne los resultados de la Consulta.

Adicionalmente, el ambientalista afirmó que el próximo mandatario de Mercaderes tendría que actualizar el **Plan de Ordenamiento Territorial (POT)**, por lo tanto, se revisaría el uso de los suelos del Municipio, así que **buscarán también que el mandato quede "insertado allí"**, para regular el suelo y evitar que se pueda hacer exploración o explotación de Hidrocarburos. (Le puede interesar:["La ejemplar consulta popular de San Lorenzo"](https://archivo.contagioradio.com/consulta-popular-san-lorenzo/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
