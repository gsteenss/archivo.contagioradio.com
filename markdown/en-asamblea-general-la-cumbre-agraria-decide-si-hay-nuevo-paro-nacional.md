Title: En asamblea general la Cumbre Agraria decide si hay nuevo paro nacional
Date: 2015-09-03 11:28
Category: Uncategorized
Tags: congreso de los pueblos, Coordinador Nacioinal Agrario, Cumbre Agraria Campesina Étnica y Popular, Diálogos de paz en la Habana, Jornadas de indignación, Paro Nacional Agrario
Slug: en-asamblea-general-la-cumbre-agraria-decide-si-hay-nuevo-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Cumbre.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: trochando sin fronteras] 

<iframe src="http://www.ivoox.com/player_ek_7808479_2_1.html?data=mJ2dmpmbfY6ZmKiak5iJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9bhw9fSjabLtsLmysaYxc7YpYzVjNXfx9jNqMbi1cqY0sbWpYzmxtPRy8jNaaSnhqeg0JDIqYzX1sqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ricardo Herrera, vocero del CNA] 

###### [3 Sept 2015] 

En el marco de la jornada de indignación que adelanta la Cumbre Agraria en Bogotá, el próximo sábado se define si en 2016 hay de nuevo una jornada de Paro Nacional Agrario. Según Ricardo Herrera, vocero del CNA, "a este gobierno hay que hacerle un paro para que se comprometa, otro para que firme y otro para que cumpla".

[En mayo de 2014 se estableció la Mesa de interlocución entre la Cumbre y el Gobierno y en ese marco se pactaron acuerdos. Hoy, 16 meses después, **la instalación de la Mesa no se ha dado y el cumplimiento de los acuerdos no se ha efectuado**. Más de 5000 indígenas, campesinos y afro descendientes continúan movilizándose en Bogotá y exigiendo acciones concretas por parte del gobierno colombiano.  ]

[En los dos últimos días los voceros de la Cumbre se han reunido con funcionarios del Ministerio de Agricultura y del INCODER, para discutir los puntos del pliego de negociaciones y su implementación. La subcomisión de derechos humanos y garantías, como exigencia para la concertación con el Ministerio del Interior, no ha sido instalada.]

[Las mesas de trabajo entre delegados de la Cumbre Agraria y técnicos del Ministerio de agricultura, sesionaran hoy en horas de la tarde, para continuar estudiando los proyectos pactados entre el Gobierno y la Cumbre, tras la finalización de los paros agrarios de 2013 y 2014.]

[Los encuentros entre funcionarios del INCODER y voceros de la Cumbre, para tratar el tema de tierras, continúan. **Se discute la posibilidad de una reforma agraria para redistribuir equitativamente  la tierra entre campesinos, indígenas y afro descendientes**,  y constituir las figuras territoriales de zonas de reserva campesina, resguardos indígenas y consejos comunitarios.]

[El INCODER ha declarado no contar con los recursos necesarios para la adjudicación y compra de tierras, y llama la atención que la respuesta del Gobierno sea acabar con la institución, como lo han afirmado.  ]

[Ricardo Herrera, asegura que el objetivo es que estas figuras territoriales lleguen a constituirse como **territorios agroalimentarios, interétnicos e interculturales**, en los que se organice el territorio para la protección de recursos naturales y se garantice que el campesino acceda a la tierra.]

[Herrera insiste en que el **gobierno colombiano no tiene política clara para el fortalecimiento del campo, tampoco para la distribución equitativa de la tierra**, por el contrario el marco legal vigente favorece a las empresas transnacionales para la implementación de proyectos agroindustriales que expropian al campesinado de sus tierras, como las llamadas ZIDRES, agudizando el problema de distribución de la tierra y su producción.]

[La Cumbre Agraria denuncia que la **legislación actual en materia rural, va contravía de lo que el presidente está acordando en el proceso de paz**, por lo que citan el próximo sábado 5 de septiembre una asamblea general para que el Gobierno rinda cuentas e informe las acciones concretas a implementar tras estas jornadas de indignación.]
