Title: "El Fiscal buscará la forma para que Santrich permanezca en la cárcel": Gustavo Gallardo
Date: 2019-02-05 14:45
Author: AdminContagio
Category: Judicial, Paz
Tags: Corte Constitucional, Fiscal, Fiscalía, JEP, Satrich
Slug: fiscal-santrich-carcel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-2019-02-05-a-las-2.34.52-p.m.-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @rafaelperezb] 

###### [5 Feb 2019] 

La Jurisdicción Especial para la Paz **(JEP) emitió este lunes un comunicado en el que daba 20 días hábiles de plazo al Gobierno de Estados Unidos para presentar las pruebas contra Zeuxis** Pausias Hernández Solarte, alias Jesús Santrich; este nuevo plazo se otorgó por parte del Tribunal luego de que la carta en la que se pedían dichas pruebas se extraviara en su camino al norte del continente.

**Gustavo Gallardo, uno de los abogados de Santrich,** calificó como absurda la situación por la que pasa el caso, en la que se piden las pruebas al Gobierno norteamericano para decidir sobre la extradición del exguerrillero, y la comunicación se pierde en Panamá por no usar los canales oficiales para este tipo de documentos. (Le puede interesar: ["Se venció el plazo, EE.UU. no aportó pruebas en caso Jesús S."](https://archivo.contagioradio.com/se-vencio-el-plazo-y-eeuu-no-aporto-pruebas-en-caso-santrich/))

Para el Abogado, **la pérdida del documento puede obedecer a una presión que se está ejerciendo contra la JEP y contra la misma implementación del proceso de paz**, mediante el uso de argucias jurídicas para dilatar el proceso de su defendido.  Esto, porque el caso ya había concluido su fase probatoria y se encontraba en los alegatos de conclusión, previos al cierre de la acción judicial. (Le puede interesar: ["Jesús S. podría salir de la cárcel a mediados de febrero: Defensa"](https://archivo.contagioradio.com/jesus-santrich-podria-salir-de-la-carcel-a-mediados-de-febrero-defensa/))

La percepción de dilatación del caso se fundamenta en que la misma defensa le pidió al Fiscal, la DEA y el Gobierno norteamericano que envíe las pruebas contra Santrich; además, que el canal usado para enviar la comunicación no haya sido la vía diplomática sino un servicio de correos civil; y que tras el nuevo plazo de la JEP, **el Gobierno de Colombia sí advirtió a Estados Unidos la extensión del periodo para entregar las pruebas de forma rápida**.

### **"El Fiscal buscará la forma para que Santrich permanezca en la cárcel"** 

Gallardo recordó que el tramite que se está surtiendo en este momento es la decisión  por parte de la JEP, sobre mantener la garantía de no extradición para reincorporados de las FARC; puesto que el Acuerdo de Paz brindaba esa garantía, pero condicionada a que no se cometieran delitos después de 2016. Teniendo en cuenta que según el abogado, **contra Santrich no habría pruebas que dieran cuenta de delitos cometidos tras esa fecha, y que no tiene otros procesos abiertos que ameritan cárcel, alcanzaría la libertad en marzo**.

Sin embargo, el escenario para el líder del partido FARC sería más difícil, porque el año pasado la Corte Constitucional decidió que la JEP mantendría la competencia para mantener o quitar la garantía de extradición, pero lo concerniente a la privación de la libertad de ex guerrilleros quedaría en manos del Fiscal. Decisión que para Gallardo fue un error, "porque generó competencia en dos autoridades distintas con jurisdicciones diferentes".

Teniendo en cuenta que la Fiscalía se opuso incluso a que Santrich fuera trasladado al Consejo de Estado para participar en la audiencia sobre la perdida de su investidura como congresista, así como a la audiencia de acogimiento a la JEP; **la defensa jurídica del ex guerrillero prevé que Nestor Humberto Martínez se oponga también a su libertad, y fabrique razones jurídicas para mantenerlo privado de la libertad**.

<iframe id="audio_32292376" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32292376_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
