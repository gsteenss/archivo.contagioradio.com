Title: #5000TroopsToColombia: ¿Agresión mediática contra Venezuela?
Date: 2019-01-29 13:14
Author: AdminContagio
Category: El mundo, Política
Tags: Boltón, Estados Unidos, Tropas, Venezuela
Slug: 5000troopstocolombia-venezuela
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Diseño-sin-título-5-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Contagio Radio] 

###### [29 Ene 2019] 

Durante una rueda de prensa en la Casa Blanca, el consejero de Seguridad Nacional del presidente Trump, John Bolton dejó ver su agenda de anotaciones en la que había dos frases: "Afganistan, bienvenidas las negociaciones" y **"5000 tropas a Colombia".** La primera frase tiene que ver con la posibilidad de lograr un acuerdo de paz entre EE.UU. y los talibanes en medio oriente, mientras siembra dudas sobre la intención de la segunda.

Para el analista y profesor de la Universidad Nacional, **Victor de Currea-Lugo,** el manuscrito hace parte de una cuestión anecdótica, porque "nadie de la talla del señor Bolton deja ver de manera accidental esta nota". De hecho, diferentes medios han hecho públicos videos en los que se observa a Bolton quitando una hoja blanca que estaba sobre la libreta amarilla, y luego dando la vuelta a la libreta para que su mensaje fuera visible.

No obstante, de Currea señala que este movimiento solo es un hecho más que se enmarca en una **estrategia que contiene diferentes mensajes de agresión mediática contra Venezuela.** En dicha estrategia también se contaría la construcción de una base militar en Ocaña, Norte de Santander, supuestamente para atender las necesidades de orden público en el Catatumbo, pero que tiene artillería antiaérea, como si el ELN o el EPL tuvieran esa capacidad de combate.

Esa estrategia, según el analista, tendría la intención clara por parte de Estados Unidos de tumbar al gobierno de Maduro, y para ello, **"no ha descartado ninguna opción"**. (Le puede interesar: ["Aumento de tropas de EE.UU. en frontera con Panamá, ¿qué está pasando?"](https://archivo.contagioradio.com/eeuu-atacar-venezuela-darien/))

### **"Por menos recursos naturales que los que tiene Venezuela se han iniciado invasiones"** 

El profesor afirmó que se debe estudiar las últimas guerras emprendidas por Estados Unidos, por ejemplo, la guerra de 2003 en Irak obedecía exclusivamente a la apropiación del petróleo en la nación del medio oriente, y Venezuela tiene grandes reservas de crudo, así como de coltán y oro, razón por la que De Currea recuerda que **"por menos recursos naturales, muchísimos menos de los que tiene Venezuela, han empezado invasiones"**.

De hecho, en una reciente entrevista realizada a Bolton por parte de la cadena Fox, él asesor **asegura que la economía y habitantes de Venezuela estarían mucho mejor si empresas estadounidenses produjeran el petróleo** de esa nación en las proporciones de las que son capaces. (Le puede interesar:["57 países se solidarizan con Venezuela y rechazan intenciones de intervención"](https://archivo.contagioradio.com/57-paises-se-solidarizan-con-venezuela-y-rechazan-intervencion-de-paises/))

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Tomando en cuenta la experiencia de Afganistán, en la que Pakistán sirvió de plataforma aérea de abastecimiento y ataque, **Colombia sería geográficameante determinante en una guerra contra Venezuela**; el profesor señala que valdría  la pena cuestionarse si: ¿Es Colombia un país soberano y con autodeterminación?, y si ¿la sociedad colombiana apoyaría una invasión al vecino país sirviendo como plataforma de lanzamiento de dicho ataque?

Según el analista, **nuestro país sigue un libreto que es escrito desde Washington y su política exterior, en ese sentido, se limita a obedecer;** y buena parte de la población colombiana estaría dispuesta a apoyar una guerra contra Venezuela. Precisamente sobre este tema, De Currea señala que debería preverse que quienes más sufrirían los impactos de una confrontación bélica serían colombianos y venezolanos, así como los centros urbanos, por lo que vale la pena cuestionar si **hace falta que caiga una bomba en Bogotá, Cali o Medellín para que se entienda lo qué significa un conflicto armado**.

### **Rusia y China: Las razones que disuaden a EE.UU. de iniciar la invasión** 

De acuerdo a De Currea, **lo único que ha detenido la invasión a Venezuela por parte de Estados Unidos han sido los pronunciamientos de Rusia y China en apoyo al presidente Maduro**, no obstante, en un escenario de confrontación bélico habría que revisarse nuevamente el apoyo que estas naciones brindarían al vecino país.

Bajo esa óptica, los trascendente sería cuestionarse, qué tanto la sociedad colombiana y el mundo apoyaría esa guerra, porque se ha visto que "a EE.UU., no le ha importado incendiar el mundo con unas agendas absolutamente injustificadas como en Afganistan". Adicionalmente, la respuesta a esta pregunta, debería tomar en cuenta que Colombia comparte 2 mil km de frontera con Venezuela, y hay familias enteras que son binacionales.

<iframe id="audio_31935344" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31935344_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
