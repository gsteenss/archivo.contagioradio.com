Title: Enfrentamientos entre paramilitares y ELN afectan comunidades en Chocó
Date: 2017-09-11 12:42
Category: DDHH, Nacional
Tags: Chocó, ELN, Jiguamiandó, paramilitares
Slug: 46439-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/paramilitares-Caucasia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [11 Sept. 2017] 

Enfrentamientos entre paramilitares, autodenominados Autodefensas Gaitanistas de Colombia –AGC- y presuntamente la guerrilla del ELN en el territorio de Jiguamiandó, departamento del Chocó tienen **atemorizadas a las comunidades que habitan esta zona, quienes desde marzo han estado denunciando las operaciones** de las AGC.

Según el comunicado entregado por la Comisión Intereclesial de Justicia y Paz, los pobladores de las Zonas Humanitarias de Pueblo Nuevo, Nueva Esperanza y el Resguardo Humanitario de Urada, **escucharon el viernes 8 de septiembre una serie de detonaciones y ráfagas de fusil durante dos horas**. Le puede interesar: [Paramilitares imponen controles a transporte público en Chocó](https://archivo.contagioradio.com/44498/)

Cabe recordar que desde el mes de marzo la Comisión de Justicia y Paz ha estado denunciando que las AGC desarrollan operaciones de control social en 9 lugares del Consejo Comunitario “así mismo, mantienen control de operaciones económicas ilegales, a pesar de la objeción de los integrantes de los Consejos y las autoridades tradicionales del Resguardo”. Le puede interesar: [Control paramilitar se afianza en Bajo Atrato y Cauca](https://archivo.contagioradio.com/control-paramilitar-se-afianza-atrato-cauca/)

Por último, dice la organización que **además “de la ineficacia de la Fuerza Pública se suma la incapacidad del conjunto del Estado** para realizar presencia civil asumiendo sus responsabilidades que están siendo asumidas en materia social y regulación territorial por las AGC”. Le puede interesar: [Si AGC se acogen a la justicia Chocó, Antioquía y Urabá sería los más beneficiados](https://archivo.contagioradio.com/si-agc-se-acogen-a-la-justicia-choco-antioquia-y-uraba-seria-los-mas-beneficiados/)

###  

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
