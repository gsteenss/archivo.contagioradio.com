Title: Líderes del Bajo Atrato denuncian plan para asesinarlos
Date: 2018-01-30 13:48
Category: DDHH, Nacional
Tags: asesinatos de líderes sociales, Bajo Atrtato, Chocó, lideres sociales, líderes sociales amenazados
Slug: lideres-del-bajo-atrato-denuncian-plan-para-asesinarlos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/marcha-de-las-flores101316_160.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [30 Ene 2018] 

A pesar de las fuertes denuncias que realizaron a finales del años pasado los líderes sociales del Bajo Atrato y luego de los asesinatos de Mario Castaño y Hernán Bedoya, han denunciado que hay un plan para asesinar a 30 líderes. Los paramilitares siguen presentes en el territorio, **han instalado puestos de control a la salida Zonas Humanitarias** e incluso han amenazado a los integrantes de la Unidad Nacional de Protección que prestan seguridad a los líderes.

A finales del año pasado, cerca de 25 líderes sociales se desplazaron a Bogotá para denunciar la grave situación que viven en el Bajo Atrato teniendo en cuenta los asesinatos de **Mario Castaño y Hernán Bedoya**. Allí, con sus rostros cubiertos con máscaras denunciaron las actividades ilegales de varios empresarios y las amenazas constantes por parte de grupos paramilitares.

Frente a la situación, el Gobierno Nacional se reunió con ellos y ellas. Según el testimonio de una lideresa, que le aportó a Contagio Radio, “el Gobierno nos prometió que iban a poner más Fuerza Pública y que iban a aumentar los esquemas de protección”. Si bien algunos líderes sí han recibido carros y escoltas, **“no se ve dónde están los integrantes de las Fuerzas Armadas que iban a llegar”**.

Por el contrario, denuncian que las Fuerzas Armadas “no ven a los paramilitares porque entre ellos **están confabulados**".  Con el fortalecimiento de las estructuras paramilitares, han llegado a denunciar que en la Zona Humanitaria de Costa Azul “han puesto un paramilitar con el alias “cuervo” que está armado, tiene un radio de comunicación y está realizando acciones intimidatorias". (Le puede interesar: ["Una luz de memoria en el Bajo Atrato"](https://archivo.contagioradio.com/una-luz-de-memoria-en-el-bajo-atrato/))

Con este panorama, las comunidades de la cuenca del Curvaradó se encuentran en alerta constante pues “hasta los escoltas están amenazados”. A los escoltas de los hijos del líder asesinado Hernán Bedoya, “les han dicho que tuvieran mucho cuidado con los muchachos porque **los iban a matar**”. Esta situación se repite con diferentes familias que ya han tenido que salir desplazadas de sus territorios.

### **Organizaciones sociales continúan denunciando acciones violentas contra líderes del Bajo Atrato** 

A la situación retratada por los líderes sociales se suman las constantes denuncias de organizaciones que acompañan procesos colectivos en esa zona como la Comisión Intereclesial de Justicia y Paz. El 29 de enero se presentó la última denuncia por la existencia de un **plan para atentar contra la vida de 30 líderes** y lideresas de la cuenca de Curvaradó allí en el Bajo Atrato chocoano.

Según la denuncia y como lo confirman los líderes del Bajo Atrato, diferentes hombres armados han manifestado que **hay una lista con los nombres de los líderes** que serían asesinados. De acuerdo con la Comisión, “las amenazas de muertes provienen de los empresarios ocupantes de mala fe” quienes ya han obligado a líderes y lideresas a desplazarse de sus territorios. (Le puede interesar: ["Aumentan amenazas contra líderes sociales del Bajo Atrato y Urabá"](https://archivo.contagioradio.com/aumentan-las-amenazas-contra-lideres-sociales-del-bajo-atrato-y-uraba/))

### **Amnistía Internacional alertó sobre situación que viven los líderes sociales y las comunidades del Chocó.** 

Teniendo en cuenta la última alarma temprana de la Defensoría del Pueblo de Colombia, la ONG alertó sobre que aún **“persiste y se agudiza el riego de violaciones a los derechos humanos** e infracciones al derecho internacional humanitario” por la confrontación armada en los territorios de estas comunidades.

La organización reiteró que, a pesar de la firma del Acuerdo de Paz, **la violencia se ha recrudecido** y los casos de asesinatos y desplazamientos forzosos “son evidencia de la re victimización que vive esta zona”. Además, indicó que, con el anuncio de la suspensión de los diálogos de paz con el ELN, hay una preocupación de las comunidades que temen que se recrudezca el conflicto.

Adicional a esto, las comunidades han denunciado en repetidas ocasiones ante Amnistía Internacional que las estructuras paramilitares **“actúan en convivencia con el Estado** y esto ha permitido su re acomodación en la coyuntura de implementación territorial del Acuerdo de Paz”. (Le puede interesar: ["Los empresarios son los que están detrás de los asesinatos": Líderes del Bajo Atrato y Urabá"](https://archivo.contagioradio.com/hasta-cuando-y-cuantos-mas-continuan-la-violencia-contra-lideres-sociales-del-bajo-atrato-y-uraba/))

### **Bajo Atrato, una zona que continúa sufriendo la violencia armada** 

Si bien la situación en todo el departamento del Chocó tiene en alerta a las organizaciones sociales, se ha hecho especial énfasis en lo que **ocurre en el Bajo Atrato** que limita con el Urabá antioqueño. En el informe de Amnistía Internacional llamado “Continúan los años de soledad: Acuerdo de paz y garantías de no repetición en El Chocó” y que fue presentado el año pasado, se retrata la situación de una zona que es estratégica para los grupos armados.

Allí, ha habido una **reconfiguración de los grupos armados** que buscan ocupar el territorio y donde ha habido desplazamientos colectivos fruto de los combates entre diferentes grupos. La organización constató que, tras la salida de las FARC del territorio, se ha recrudecido la confrontación armada y los intereses de los diferentes grupos han marcado las distintas dinámicas del conflicto.

Adicional a esto, “el departamento se ha convertido en escenario de conflicto por los derechos territoriales para el desarrollo de **actividades económicas como el negocio de la palma, la madera, la minería, la ganadería**, entre otras”. En esta disputa por la tierra, “se ha logrado comprobar la acción conjunta entre fuerzas de seguridad del Estado y grupos paramilitares”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
