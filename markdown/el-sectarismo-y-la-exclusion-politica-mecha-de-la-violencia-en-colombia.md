Title: El sectarismo y la exclusión política:  mecha de la violencia en Colombia
Date: 2020-10-29 19:13
Author: AdminContagio
Category: Opinion
Slug: el-sectarismo-y-la-exclusion-politica-mecha-de-la-violencia-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/hablando-de-politica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

*Por:[Itayosara Rojas Herrera](https://www.instagram.com/itayosararojas/?hl=es-la)*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La exclusión política de sectores diferentes a los tradicionales es sin lugar a dudas una de las causas estructurales del conflicto armado colombiano. Así fue desde que el partido conservador lideró el proceso de regeneración en el Siglo XIX que sumió a la sociedad colombiana en el oscurantismo, bajo el manto de la iglesia y el latifundio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los conservadores desplegaron todas las tácticas posibles para mermar cualquier idea que se les opusiera, y como dirían los autores de la investigación sobre “La Violencia en Colombia”, Orlando Fals-Borda, German Guzmán y Eduardo Umaña Luna, desde sus oficinas en Bogotá fueron responsables de un derramamiento de sangre por el cual nunca se han responsabilizado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cuando la violencia estaba salida de madre, las cabezas liberales y conservadoras decidieron pactar, primero delegando el poder a los militares a través del general pacificador Gustavo Rojas Pinilla y luego mediante un pacto excluyente de alternancia del poder conocido como El Frente Nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La exclusión también se expresó en el asesinato de líderes que cuestionaran el orden político y además provinieran de sectores sociales distintos a quienes se han creído con  el derecho divino de gobernar, como pasó con Gaitán en el 48  y los asesinatos de Jaime Pardo Leal, Bernardo Jaramillo Ossa, Carlos Pizarro Leongómez y Luis Carlos Galán. Por todo esto, el punto de participación política del acuerdo de paz, de ser implementado, podría convertirse en  la mayor transformación democrática de la sociedad colombiana y sus instituciones desde el Siglo XIX.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero, siguiendo su tradición histórica, las clases gobernantes se negaron desde el principio a implementar el acuerdo de paz y ha reconocer la participación política de otros sectores. Tal y como lo expresaron al haber privado a las víctimas de la guerra a ocupar curules en el congreso, tal y como lo contemplaba el acuerdo; así como, en todas las artimañas que usan para despojar de sus puestos en el congreso  a quienes suscribieron y están cumpliendo el acuerdo de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin ningún tipo de vergüenza, ese sector político que hoy gobierna Colombia y que ha permanecido amarrado al poder durante las últimas dos décadas continúa restringiendo la democracia y determinando a dedo quien puede participar en ella y quien no. La exclusión de personas, proyectos políticos y sectores sociales en Colombia ha sido un detonante de la violencia,  uno de sus orígenes; insistir en esta práctica es mantener encendida la mecha de la violencia que tanto dolor ha causado y sigue causando.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Más allá del programa de gobierno que hoy propone Gustavo Petro uno de los antagonistas más visibles al actual gobierno y su ideología, la persecución a toda costa contra este  no es más que la reproducción de esa vieja práctica de exclusión y violencia a la que históricamente han apelado las clases gobernantes. Es por ello que resulta desastrosamente grave que sectores de centro, y algunas tendencias del partido verde nieguen de entrada la participación de Petro en una eventual coalición alternativa para la presidencia en el 2022. Flaco favor le hacen a la democracia quienes se autodenominan alternativos pero continúan profundizando en las prácticas de exclusión política que han caracterizado a las élites.     

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El sectarismo y la exclusión han alimentado la violencia en Colombia, en las luchas políticas se deben ganar las mayorías pero sin restringir la participación desde el principio, pues esta es quizás una de las razones sustanciales de todas las guerras civiles en la historia de vida republicana del país,  y  una de las razones que continúa alimentando la guerra y a quienes se lucran y sacan réditos políticos de ella. Ojalá los sectores alternativos comprendan lo nocivo del sectarismo y de restringir la participación de otros sectores. Ojalá dentro del progresismo de Petro se den cuenta también del terrible error que cometen al marginalizar a Angela María Robledo, así como la guerra sin sentido que desataron contra las feministas que no estaban de acuerdo con la candidatura de Holman Morris a la alcaldía de Bogotá. Las luchas políticas para obtener resultados en beneficio de las mayorías no solo responden a los juicios o valores propios o de ciertos individuos, por ello reconocer estos juicios de valor y actuar sobre los mismo es la primera necesidad que tenemos en Colombia para derrotar el proyecto sectario y mafioso que hoy gobierna, para poder finalmente sentar las bases de una sociedad democrática, con justicia social.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
