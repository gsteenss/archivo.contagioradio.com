Title: Yaneth Giha Tovar entra a la dirección de Ministerio de Educación
Date: 2016-11-13 18:32
Category: Educación, Nacional
Tags: Ministerio de Educación, Movimiento estudiantil Colombia
Slug: yaneth-giha-tovar-entra-a-direccion-de-ministerio-de-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/giha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana] 

###### [13 Nov 2016] 

Después de un mes de incertidumbre para el Ministerio de Educación, llega a su dirección Yaneth Giha Tovar, quién tendrá que seguir a la cabeza de programas como Ser Pilo Paga, la implementación de la política pública del Sistema Nacional de Educación Terciaria como parte de la política pública para la Educación 2034, **la polémica por el cambio de enfoque de la carrera Trabajo Social y la crisis de la desfinanciación a las Instituciones de Educación Pública.**

Economista de la Universidad de los Andes, con una especialización en resolución de conflictos y un máster en Estudios Políticos, se abre la pregunta de **¿por qué este ministerio sigue sin tener a la cabeza a un experto en educación o pedagogía?** Claro está que el último puesto que tuvo Giha fue la dirección de Colciencias, hasta el mes pasado.

No obstante, Giha tiene poco tiempo para empezar a rendir frutos, por un lado está el programa insignia del Ministerio de Educación **Ser Pilo Paga, que en lo que lleva estandarizado ha demostrado ser limitado y profundizar la crisis de la privatización de la Educación** debido a que el 85% de los estudiantes beneficiados con este programa eligen matrículas en universidades privadas, sin mencionar los resientes daños en los edificios de la Universidad Nacional de Colombia, que ha dificultado en algunas ocasiones la toma de clases en las construcciones. Le puede interesar: ["Por falta de presupuesto se está cayendo la Universidad Nacional"](https://archivo.contagioradio.com/por-falta-de-presupuesto-se-esta-cayendo-la-universidad-nacional/)

Otro de los temas que deben saldarse este año es la **implementación del Sistema Nacional de Educación Terciaria,**  que se pondría en marcha en diciembre de este año y al que muchos estudiantes han cuestionado desde su inició por el cambio de carreras profesionales a técnicas reduciendo el tiempo sin aumentar los recursos en cuanto a calidad. Le puede interesar:["Estudiantes se movilizan contra reforma tributaria"](https://archivo.contagioradio.com/estudiantes-del-sena-se-movilizan-en-contra-de-reforma-tributaria/)

De acuerdo con Luis Grubert, presidente de FECODE, otro de los retos que asumirá la nueva ministra son los incumplimientos al pliego de exigencias que surgió del último paro docente y del cual hasta el momento no se ha cumplido nada “debe haber gestiones pertinentes al interior del gobierno, porque **no hay manera de que Colombia sea la más educada si eso no es con recursos suficientes para que el derecho a la educación tenga los derechos y las garantías suficientes”.**

Habrá que esperar como se desenvolverá Yaneth Giha Tovar en este cargo y como logra mediar las diferentes problemáticas tanto con el movimiento estudiantil, como con los docentes, en el próximo 2017.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
