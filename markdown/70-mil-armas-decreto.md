Title: Más de 70 mil armas han sido incautadas desde firma de decreto que prohibe su porte
Date: 2018-12-26 15:48
Author: AdminContagio
Category: DDHH, Nacional
Tags: Decreto, Duque, Porte de armas, Restricción
Slug: 70-mil-armas-decreto
Status: published

###### [Foto: El sol de Hermosillo] 

###### [26 Dic 2018] 

El pasado 24 de diciembre, el presidente Duque firmó el **Decreto 2362**, con el que se extendió la prohibición para el porte de armas en el territorio nacional durante el próximo año; sin embargo, en el documento aparece un parágrafo nuevo que permite al Ministerio de Defensa, en cabeza de Guillermo Botero, otorgar **permisos de porte especiales para quienes por "razones de urgencia o seguridad", requieran un arma**.

> - Papá, si matamos a todos los malos, ¿solo quedamos los buenos?
>
> - No hijo, solo quedaríamos los asesinos.
>
> Una reflexión para quienes piden que en el país haya tenencia y porte libre de armas.
>
> — Piedad Córdoba (@piedadcordoba) [26 de diciembre de 2018](https://twitter.com/piedadcordoba/status/1077886468918525953?ref_src=twsrc%5Etfw)

El decreto que prohibe el porte de armas inició como una restricción que operó entre diciembre y enero de 2015; en su momento, **el ministro de defensa Luis Carlos Villegas manifestó que gracias a la medida, se había logrado una reducción del 13% en casos de homicidio,** razón que llevó al presidente Santos a prorrogar la medida con un Decreto que operó en 2017 y 2018.

En Bogotá, la medida para la restricción en el porte de armas fue implementada durante la alcaldía de Petro, y según cifras del Distrito, **la decisión significó un descenso en la criminalidad de 19,5 a 14,3 por cada 100 mil habitantes**. Adicionalmente, según cifras del Centro de Recursos para el Análisis de Conflictos (CERAC), la prohibición en el porte de armas podría reducir en un 20% la taza de homicidios que ocurren con armas de fuego en el país.

### **Decreto abre la posibilidad para que el Min. Defensa entregue permisos de porte ** 

El decreto, que fue anunciado primero en la cuenta del senador Álvaro Uribe que en la del propio presidente Duque, incluye un parágrafo en el que se aclara que el Ministerio de Defensa, determinará próximamente "lineamientos y/0 directrices para la expedición de las autorizaciones especiales que requieran los titulares por razones de urgencia o seguridad", lo que significa que **ciudadanos podrían portar armas pese a la prohibición**.

En el estudio de carácter mundial titulado "Civilian Firearms Holdigns" de 2017, se calcula que **en Colombia hay 4'971.000 armas de las cuales solo al rededor de 700 mil están registradas**; mientras las demás son ilícitas. Teniendo en cuenta que gracias al decreto la Policía puede confiscar las armas ilegales que portan los civiles, y que cerca del 84% de los homicidios ocurren con armas de fuego, **es posible señalar que la medida permite evitar muertes violentas causadas por estos elementos**.

De acuerdo a cifras de la Policía Nacional, entre 2015 y 2017 se incautaron un total de 78.744 armas, 70 mil más de las que entregaron las FARC a la ONU; a ello se suma la alerta emitida por diferentes voces, quienes recordaron que **el permiso para portar armas permitió la creación de las Convivir, cuyo armamento terminó en manos de paramilitares,** razones suficientes para desincentivar el porte de armas a cargo de civiles. (Le puede interesar: ["La relación entre Chiquita Brands, Álvaro Uribe y las convivir")](https://archivo.contagioradio.com/la-relacion-entre-chiquita-brands-alvaro-uribe-y-las-convivir/)

> Las Convivir nacieron y funcionaron con salvoconductos de porte de armas que se otorgaron en las Brigadas de ejército, como término esa historia lo conoce bien el país.
>
> — Félix de Bedout (@fdbedout) [26 de diciembre de 2018](https://twitter.com/fdbedout/status/1077896337272815617?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
