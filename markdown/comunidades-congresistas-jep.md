Title: Comunidades reconocieron el trabajo de congresistas que defendieron la JEP
Date: 2019-05-13 07:54
Author: CtgAdm
Category: Comunidad, Política
Tags: Congresistas, JEP
Slug: comunidades-congresistas-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/cata.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div style="text-align: justify;">

</div>

<div style="text-align: justify;">

</div>

<div class="gmail_quote" style="text-align: justify;">

Más de 107 comunidades rurales victimas de Estado y de la violencia política, y de nuevas dinámicas de criminalidad, enviaron una misiva al parlamento, reconociendo su gran decisión por haber obrado democráticamente. Esto, superadas las presiones, la llamada "mermelada"  en el trámite de las objeciones del presidente Duque a la ley estatutaria de la Jusrisdicción Especial de Paz, JEP, en que 110 representantes y 47 congresistas, ganaron el pulso con la bancada uribista.

</div>

<div>

</div>

<div class="gmail_quote" style="text-align: justify;">

</div>

<div dir="auto" style="text-align: justify;">

"Es para nosotros una fortuna él que ustedes hayan cumplido con su responsabilidad enfrentando las mentiras, el clientelismo (mermelada), las trampas dilatorias, el sucio cabildeo que el partido del Presidente Duque desarrolló", expresan las comunidades. Lea también: [Las trampas para evitar la votación de la JEP](https://archivo.contagioradio.com/centro-democratico-busca-dilatar-votacion-de-objeciones-a-jep-oposicion/)

</div>

<div dir="auto" style="text-align: justify;">

</div>

<div dir="auto">

</div>

<div style="text-align: justify;">

De acuerdo con su carta pública y entregada en la Cámara y el senado el pasado 9 de mayo, estas interpusieron una acción de tutela hace cuatro semanas, negada por un tribunal y en solicitud de estudio,  "en que argumentamos como las objeciones desconocen la última instancia en términos constitucionales, como lo es la Corte Constitucional,  indicando como las mismas negaban nuestro derecho a la paz, y limitaban aún nuestros derechos a la verdad y la justicia, las garantías de no repetición, así como los derechos de los excombatientes de las FARC, exmilitares, y los terceros que se encuentran acogidos en la Jurisdicción Especial de Paz".

</div>

<div style="text-align: justify;">

</div>

<div>

</div>

<div style="text-align: justify;">

En el texto las comunidades afirman que el hecho de que los congresistas hayan defendido la ley estatutaria y actuado con firmeza en contra de las objeciones interpuestas por el presidente es un ejercicio responsable con el poder que se les otorgó. [Conozca también las firmas de la carta ](https://www.justiciaypazcolombia.com/comunidades-victimas-reconocen-el-si-a-ley-estatutaria-de-la-jep-sin-objeciones-de-presidente-duque/)

</div>

<div style="text-align: justify;">

</div>

### El siguiente es el texto de la carta a congresistas:

<div style="text-align: justify;">

</div>

<div>

"*Las comunidades que seguimos afirmando el derecho a la paz en nuestros territorios, en medio de un pos conflicto, conflictos socio políticos sin resolver, nuevas operaciones de tipo paramilitar y criminalidades armadas, reconocemos en ustedes integrantes del Senado y Cámara de Representantes, su gran aporte a la democracia al haberse opuesto a las objeciones presentadas por el presidente Duque.Nosotros interpusimos una acción de tutela en la que argumentamos como las objeciones desconocen la última instancia en términos constitucionales, como lo es la Corte Constitucional, indicando como las mismas negaban nuestro derecho a la paz, y limitaban aun nuestros derechos a la verdad y la justicia, las garantías de no repetición, así como los derechos de los excombatientes de las FARC, exmilitares, y los terceros que se encuentran acogidos en la Jurisdicción Especial de Paz. El Tribunal nos negó ese amparo y hemos solicitado revisión de la mismas ante la Corte Constitucional.*

*Es para nosotros una fortuna el que ustedes hayan cumplido con su responsabilidad enfrentando las mentiras, el clientelismo (mermelada), las trampas dilatorias, el sucio cabildeo que el partido del Presidente Duque desarrolló.*

*Gracias por el ejercicio responsable de su poder. En medio de nuestras diferencias esperamos que este ejercicio de una democracia madura de ustedes pueda hacerse en los campos de la inclusión y la justicia socio ambiental frente al Plan Nacional de Desarrollo. Creemos que es posible conciliar respeto a derechos humanos, al ambiente y la inversión guiada hacia una democracia profunda en la que sea posible la protección de todas las vidas: las humanas, las comunitarias, las aguas y los bosques, y las del país.*

*Nos pueden contactar en el correo verdadyjusticia.0@gmail.com*

*Muchas gracias."*

</div>
