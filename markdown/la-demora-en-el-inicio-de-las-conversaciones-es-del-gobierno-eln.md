Title: En próximos días vendrán buenas noticias de las conversaciones de paz con ELN
Date: 2016-05-13 17:15
Category: Nacional, Paz
Tags: ELN, paz, proceso de paz, Santos
Slug: la-demora-en-el-inicio-de-las-conversaciones-es-del-gobierno-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Pablo-Beltrán.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [13 Mayo 2016]

De acuerdo con Pablo Beltrán, integrante de la delegación de paz del ELN, la demora en el inicio de las negociaciones se debe a que **el Gobierno ha hecho exigencias nuevas y de última hora**. Según el comandante, estas demandas están incluidas en la agenda y serán tratadas en su momento, teniendo en cuenta que se trata de una negociación en la que no deben haber imposiciones unilaterales.

El pasado martes, las delegaciones se reunieron para examinar las dificultades que rodean el proceso de paz, en el encuentro **se acordó con el Gobierno que el tema del secuestro ya no será una exigencia, sino que va a ser un punto de negociación**. "Ya eso son otras palabras, otros términos y en ese campo, claro nosotros estamos dispuestos a dialogar, a negociar lo que sea, porque por eso se trata de solución política del conflicto", aseguró Beltrán.

El comandante insiste en la [[disposición que tienen el ELN](https://archivo.contagioradio.com/la-postura-de-la-guerrilla-del-eln-frente-al-proceso-de-paz/)] para **solucionar políticamente el conflicto, "sin hechos de fuerza ni presiones"**; sin embargo, llama la atención por el asesinato de líderes y dirigentes sociales, así como las [[continúas violaciones a los derechos humanos](https://archivo.contagioradio.com/de-156-defensores-de-ddhh-asesinados-en-2015-en-el-mundo-51-eran-colombianos-segun-informe-de-ai/)] que ocurren día a día en Colombia "mucho más altas que las que le preocupan al gobierno" y que exigen a ambas partes garantizar que la población civil no sea involucrada en el conflicto.

"Hay que actuar de acuerdo a lo que pactamos, [[tenemos unas reglas del juego](https://archivo.contagioradio.com/conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln/)], trabajemos con eso, y todos esos asuntos que le preocupan más a una u otra parte, pues llevémoslos a la mesa, pero no tranquemos la mesa haciendo una exigencia previa", sostiene Beltrán, y agrega que **el ELN está listo para ir al Ecuador, la demora corre por cuenta del gobierno**.

<iframe src="http://co.ivoox.com/es/player_ej_11548857_2_1.html?data=kpailp2ceZihhpywj5WbaZS1lp2ah5yncZOhhpywj5WRaZi3jpWah5yncbfdxNnc1JCoqYy31tffx8aPkNbb0IqfpZClssLgytjhw5CRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
