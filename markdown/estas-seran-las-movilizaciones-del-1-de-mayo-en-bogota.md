Title: Estas serán las movilizaciones del 1 de mayo en Bogotá
Date: 2017-05-01 11:24
Category: Movilización
Tags: Primero de Mayo
Slug: estas-seran-las-movilizaciones-del-1-de-mayo-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/DSC1707.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

###### [01 May 2017] 

Miles de personas se congregarán hoy, primero de mayo, en diferentes plazas y calles del mundo para celebrar el día internacional de las y los trabajadores. En Colombia, las movilizaciones se harán en diferentes ciudades del **país como Medellín, Bogotá, Cali, Villavicencio, Bucaramanga, entre otras.**

En Bogotá, se realizarán movilizaciones en diferentes sectores de la ciudad, la tradicional marcha que se realiza desde el centro, **partirá del Parque Nacional y culminará en la Plaza de Bolívar**. En la localidad de **Bosa**, el punto de partida será el parque principal; en Usme, el punto de encuentro será el parque La Aura y en la localidad de **Ciudad Bolíva**r, el punto de encuentro será la sede de la Universidad Dístrital Tecnológica.

**Las marchas que proceden de las localidades de Bosa, Ciudad Bolívar, Kennedy, Usme y San Cristóbal se concentraran en el Parque de Villa Javier**, ubicado en la Carrera 6 con Calle 8 A sur. Le puede interesar: ["Salario mínimo, cada vez más mínimo en Colombia"](https://archivo.contagioradio.com/salario-minimo-cada-vez-mas-minimo-en-colombia/)

###### Reciba toda la información de Contagio Radio en [[su correo]
