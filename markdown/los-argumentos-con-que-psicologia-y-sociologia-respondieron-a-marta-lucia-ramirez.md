Title: Los argumentos con que Psicología y Sociología respondieron a Marta Lucía Ramirez
Date: 2020-02-17 19:05
Author: CtgAdm
Category: Entrevistas, Política
Tags: Marta Lucía Ramírez, sociologia, Vicepresidenta
Slug: los-argumentos-con-que-psicologia-y-sociologia-respondieron-a-marta-lucia-ramirez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Marta-Lucía-Ramírez-habla-de-psicología-y-sociología.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @mluciaramirez {#foto-mluciaramirez .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/fernando-gonzalez-profesor-psicologia-sobre-declaraciones-de_md_47975067_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Fernando González | Profesor de psicología de la Universidad Católica

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Diferentes asociaciones de psicología y sociología del país respondieron a la vicepresidenta Marta Lucía Ramírez, que en un evento público, el pasado viernes, invitó a mujeres a dejar de estudiar estas carreras y elegir oficios de 'ciencias duras' que les generen mayores ingresos. Las declaración, sin embargo, sirve para abrir la discusión sobre temas de fondo respecto a estas carreras.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Son necesarias la psicología y sociología?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para Fernando González, profesor de psicología de la Universidad Católica, es claro que el país necesita psicólogos: Somos un país que tiene un problema serio con la salud mental, mire las tasas de suicidio o de violencia intrafamiliar. De acuerdo a la Organización Mundial de la Salud (OMS), **mientras la depresión afecta a 4,4% de las personas a nivel mundial, el promedio en Colombia es de 4,7%.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, González sostiene que los y las psicólogas tendrían conocimiento para aportar y transformar dichas situaciones. Por otra parte, la Red Colombiana de Facultades y Departamentos de Sociología (RECFADES), recordó la trascendencia de **una ciencia cuyo conocimiento de las sociedades se aplica en el diagnóstico y solución de problemas como el conflicto armado**, el narcotráfico o los conflictos agrarios del país.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Sobre los salarios, un tema espinoso en la mesa**

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/isarroyo/status/1228776879949217794","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/isarroyo/status/1228776879949217794

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En su intervención, la Vicepresidenta invitó a las mujeres a estudiar profesiones 'más rentables', tomando en cuenta la realidad salarial de la sociología y psicología. Teniendo claro que ambas profesiones son necesarias, y además relevantes en el contexto actual colombiano, la discusión sobre el salario y las condiciones trasciende a los propios profesionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El profesor González señaló que la psicología en efecto no está bien remunerada, pero aclaró que es un problema del mercado y no de exceso de profesionales. De acuerdo a este planteamiento, la RECFADES planteó que hay brechas entre los profesionales de ciencias humanas respecto de las ciencias técnico-instrumentales, pero ello se debe a un error de mirada sesgada sobre lo que es 'útil' o no a ciertos sectores.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Siguiendo con esta idea, la Asociación Colombiana de Sociología (ACS) afirmó que **la utilidad de una disciplina científica y académica no está subordinada a una supuesta rentabilidad económica**; y que en todo caso, "los altos o bajos ingresos de un o una profesional no dependen tanto del tipo de profesión como de la estructura y la coyuntura de un modelo económico dado".

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FSociologiaIcesi%2Fposts%2F3217947038233929&amp;width=500" width="500" height="786" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>  
<!-- /wp:html -->

<!-- wp:heading {"level":3} -->

### **Poner la lupa sobre el tipo de profesionales que gradúan las universidades**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Con ánimo de superar las tensiones generadas por las declaraciones de la Vicepresidenta, el profesor González se concentró en algunos elementos. En primer lugar, **la concentración de los profesionales en las ciudades**, porque considera que sus conocimientos deberían estar a disposición de todo el país, incluidas las comunidades rurales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, hizo un llamado al Ministerio de Educación Nacional para que se revise los programas de las Universidades: Esperando **que todos los programas cuenten con acreditación,** y que aquellos centros de estudios que no la tengan, se abstengan de seguir graduando profesionales. (Le puede interesar: ["¿Cuáles son los retos de la psicología social en el postconflicto?"](https://archivo.contagioradio.com/cuales-son-los-restos-de-la-psicologia-social-en-el-postconflicto/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el mismo sentido, cuestionó que en la carrera de psicología haya universidades que permitan grados sin que se realice prácticas presenciales. Por último, el académico propuso una reflexión de fondo para todas las carreras: ¿Son pertinentes los saberes que están obteniendo para el mundo del siglo XXI?

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
