Title: Las verdades ocultas del Plan Colombia
Date: 2016-02-04 14:47
Category: DDHH, Nacional
Tags: fumigaciones, plan Colombia, Violaciones de DDHH
Slug: las-verdades-ocultas-del-plan-colombia
Status: published

###### Foto: derechosdelospueblos.blogspot. 

<iframe src="http://www.ivoox.com/player_ek_10318073_2_1.html?data=kpWgk52Ue5Shhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5ynca3V1JDjx9fIpcXZ1JDcxdrQuMLnjMnSzpC0sMLijKjcztTRpsrVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gimena Sánchez, WOLA] 

###### [04 Feb 2016.] 

Glorificar el Plan Colombia es “un error de Estados Unidos” afirma la organización WOLA, una de las organizaciones que ha estado al tanto de la aplicación y desarrollo del Plan, y de otras políticas de cooperación de Estados Unidos hacia Colombia como el Tratado de Libre Comercio y el Plan de Acción Laboral, condicionante de ese acuerdo comercial y que ha sido incumplido.

Según Wola, el Plan Colombia no puede ser glorificado puesto que muchos de los objetivos que se trazó no han sido cumplidos y por otra parte se han generado situaciones problemáticas a partir de las líneas estratégicas del Plan.

### **Plan de erradicación del narcotráfico** 

Uno de los principales objetivos del Plan Colombia era detener el flujo de cocaína hacia Estados Unidos o erradicarlo definitivamente, sin embargo los resultados dan cuenta de otra realidad. Para la organización Wola, lo que ha generado la aplicación de esa política de lucha contra el narcotráfico son los [efectos nocivos de las fumigaciones](https://archivo.contagioradio.com/?s=fumigaciones) como pérdida de soberanía alimentaria, la dispersión de los cultivos, creado problemas de salud en las comunidades y no se ha detenido el flujo o afectado las rutas de la cocaína.

### **Las violaciones a los DDHH “se dispararon de una manera atroz”** 

La asistencia técnica a las FFMM de Colombia es otro de los objetivos del Plan Colombia, sin embargo durante su aplicación en el gobierno de Álvaro Uribe, las **[violaciones de DDHH se dispararon de “una manera atroz”](https://archivo.contagioradio.com/577-agresiones-contra-defensores-de-ddhh-se-han-registrado-en-2015/)** señala Gimena Sánchez, a tal punto de que las ejecuciones extrajudiciales fueron más de 3000, se violó el principio de distinción y se dieron altos índices de desplazamiento forzado.

Además hubo colaboración con grupos paramilitares que están en la lista de terroristas que manejan las propias fuerzas militares de Estados Unidos. Sánchez señala que hay restricciones en las FFMM de ese país para realizar este tipo de colaboraciones y por ello tuvo que reformularse ese plan de cooperación.

Vale la pena recordar que dentro de los condicionamientos eran que **no haya ningún [indicio de colaboración con grupos paramilitares, que las FFMM no estén vinculadas con violaciones de DDHH](https://archivo.contagioradio.com/human-rights-watch-expone-su-ultimo-informe-sobre-falsos-positivos-en-colombia/)** y que se proteja a todas las personas que están en riesgo. El incumplimiento de esas condiciones trajo consigo varias demandas por parte del congreso de EEUU y el departamento de Estado.

Recientemente la organización Wola emitió una serie de recomendaciones basadas en la evaluación del Plan Colombia recomendaciones entre las que se incluyen varias propuestas para apoyar la consolidación de la paz con las FARC y con el ELN, el desmantelamiento de los grupos paramilitares y el respaldo en el mediano plazo a todo lo respectivo a la construcción de la paz en un plazo no menor a cinco años.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
