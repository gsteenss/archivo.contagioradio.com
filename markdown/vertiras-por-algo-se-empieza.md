Title: Vertiras, por algo se empieza
Date: 2015-03-04 12:14
Author: CtgAdm
Category: Abilio, Opinion
Tags: Conpaz, falsos positivos, general (r) Rafael Colón, Masascre El Salado
Slug: vertiras-por-algo-se-empieza
Status: published

###### Foto Contagio Radio 

#### [Por] [**[Abilio Peña]**](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

*Vertiras* es la composición de sílabas con las  palabras verdad y mentiras que hizo un niño de seis años para resumir las conversaciones en las que los adultos decimos verdades y mentiras a la vez. Para el niño  hay verdades, hay mentiras y hay vertiras.

Esta composición   me vino a la mente[ ]{.Apple-converted-space}luego de escuchar [ ]{.Apple-converted-space}al general (r)[ ]{.Apple-converted-space}Rafael Colón ante los 450 delegados  y delegadas a la Asamblea Nacional de las **Comunidades Construyendo Paz en los Territorios -Conpaz**- la semana pasada en Bogotá, casi todos víctimas de crímenes de Estado. Escucharlo me hizo temer en que puede darse en Colombia una comisión de vertiras y no una de verdad como la propuesta por Conpaz, luego de la eventual firma de los acuerdos para la paz.

El general (r) Colón[ ]{.Apple-converted-space}tuvo el valor de[ ]{.Apple-converted-space}hablar ante un auditorio  atiborrado de indígenas, afrodescendientes, mestizos de 120 procesos de casi todo el país, que han sido víctimas de actuaciones conjuntas de militares y paramilitares y muchos de ellos solo de militares que veían en él  a uno[ ]{.Apple-converted-space}de sus representantes.

En su intervención [ ]{.Apple-converted-space}recordó el reconocimiento que hizo años atrás[ ]{.Apple-converted-space}de[ ]{.Apple-converted-space}la responsabilidad [ ]{.Apple-converted-space}en la masacre de El Salado y en los llamados falsos positivos por parte de[ ]{.Apple-converted-space}los militares y del combate que emprendió contra estos en las unidades que comandó. [ ]{.Apple-converted-space}El auditorio recibió estos comentarios [ ]{.Apple-converted-space}con[ ]{.Apple-converted-space}positiva sorpresa,[ ]{.Apple-converted-space}como pasos ciertos de construcción de paz.

Pero esa verdad reconocida fue adornada con ciertas justificaciones que[ ]{.Apple-converted-space}no convencieron. Mencionó[ ]{.Apple-converted-space}que  en la **masacre de El Salado** no hubo órdenes de mandos superiores, que se trataba de manzanas podridas de la institución militar responsables por acción y omisión de tan horrendos crímenes. Dijo también que en los llamados falsos positivos se dio una mala interpretación  de las ordenes del presidente Uribe que fueron escuchadas por soldados que vieron la oportunidad  de hacerse a primas, ascensos y[ ]{.Apple-converted-space}descansos, asesinando civiles, haciéndolos pasar como guerrilleros muertos en combate.

En relación con la masacre de El[ ]{.Apple-converted-space}Salado, el general pasó por alto[ ]{.Apple-converted-space}la sentencia condenatoria contra el Capitán de Corveta[ ]{.Apple-converted-space}de la Infantería de Marina[ ]{.Apple-converted-space}Héctor Martín[ ]{.Apple-converted-space}Pita Vásquez, en calidad de cómplice por el delito de homicidio agravado. En[ ]{.Apple-converted-space}la sentencia condenatoria, a pesar de la impunidad presente en el caso, se [ ]{.Apple-converted-space}estableció claramente[ ]{.Apple-converted-space}que el oficial[ ]{.Apple-converted-space}no actuó[ ]{.Apple-converted-space}como manzana podrida, sino “*que las conductas punibles en que incurrió fueron en cumplimiento de órdenes de sus superiores*”, tal como lo resume[ ]{.Apple-converted-space}la CCJ en su balance a 15 años de la masacre, en el mes de febrero[ ]{.Apple-converted-space}de este año.

Frente a[ ]{.Apple-converted-space}los llamados falsos positivos[ ]{.Apple-converted-space}el general (r) [ ]{.Apple-converted-space}obvió la directiva del Ministerio de Defensa 029 de 2005 que incentivó el pago de recompensas y las acusaciones de subalternos según las cuales comandantes[ ]{.Apple-converted-space}de brigadas[ ]{.Apple-converted-space}enseñaban a sus tropas que[ ]{.Apple-converted-space}“l*a guerra se medía en litros de sangre*”. O como decía el general [ ]{.Apple-converted-space}Montoya, en el momento máximo comandante del ejército,[ ]{.Apple-converted-space}de acuerdo con testigos:[ ]{.Apple-converted-space}“*yo no quiero regueros de sangre sino ríos de sangre*”. Serios informes[ ]{.Apple-converted-space}de organismos nacionales e internacionales de derechos humanos hablan de una actuación sistemática.

Las  intervenciones de delegadas y delegados fueron claras,  contundentes y muy respetuosas, dejando ver su satisfacción[ ]{.Apple-converted-space}por las verdades y su inconformidad por las vertiras. Mostrando, además, su disposición a ese tipo de diálogos[ ]{.Apple-converted-space}buscando esclarecer las máximas responsabilidades de los crímenes de los que han sido víctimas.

El encuentro entre el militar (r) y los delegados de las comunidades,[ ]{.Apple-converted-space}es sin duda un paso importante en el largo camino hacia la construcción de la paz en Colombia, a pesar de las justificaciones encubridoras de la verdad. Es claro igualmente[ ]{.Apple-converted-space}que se debe contar con acceso a la[ ]{.Apple-converted-space}pluralidad de fuentes, como las enumeradas en la propuesta de Comisión de la Verdad hecha por Conpaz, si se quiere una aproximación a la verdad. En todo caso, por[ ]{.Apple-converted-space}algo se empieza.
