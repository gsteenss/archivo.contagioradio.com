Title: Esta semana se sabrá si avanza o no Revocatoria al alcalde Peñalosa
Date: 2017-12-11 12:17
Category: DDHH, Nacional
Tags: CNE, Enrique Peñalosa, revocatoria
Slug: revocatoria-a-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/marcha_de_las_antorchas_para_revocar_a_penalosa_foto_blu_radio_2.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Blu Radio] 

###### [06 Dic 2017] 

La revocatoria a Enrique Peñalosa recibió un espaldarazo por parte del Tribunal Administrativo de Cundinamarca, que ordenó acelerar el proceso de revisión que adelanta el Consejo Nacional Electoral. Para Carlos Carrillo, integrante de uno de los comités impulsores de la revocatoria, re afirma que se cumplió con la ley a la hora de poner en marcha el mecanismo y** se da un plazo de 10 días para saber que pasa con la revocatoria.**

“**Este fallo, lo que dice básicamente, es que el Consejo Nacional Electoral tiene 48 horas para resolver las recusaciones**” afirmó Carrillo y explicó que esta entidad se había escudado en que la ley no les exigía ningún tiempo para realizar las revisiones, como si lo tiene la Registraduría.

Ahora el CNE tendrá 10 días para fallar sobre el asunto de las cuentas y en ese lapso pasar la investigación a las autoridades correspondientes como la Fiscalía, sin embargo, Carrillo expresó que es probable que ahora este mismo órgano imponga una apelación y que el proceso tampoco llegue a la Fiscalía. (Le puede interesar: ["La revocatoria es en las calles: Comite Unidos Revocaremos a Peñalosa"](https://archivo.contagioradio.com/resistraduria-avala-firmas-para-revocatoria-de-penalosa/))

“El CNE tendrá que dejar de jugar a inventarse pequeñeces y simplemente lo correcto sería que le dieran trámite al proceso, el fallo es claro y para nosotros el **CNE continúa buscando salvar intereses políticos”** afirmó Carrillo.

<iframe id="audio_22511052" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22511052_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
