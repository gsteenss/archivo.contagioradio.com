Title: Un detenido y 7 heridos deja represión del ESMAD a estudiantes de la U. del Valle
Date: 2015-10-15 11:50
Category: DDHH, Nacional
Tags: Camila Urrea, Crisis Hospital Universitario del Valle, ESMAD en Cali, HUV, Movilización Cali octubre 15 de 2015, Paro en la Universidad del Valle, Radio de derechos humanos, Universidad del Valle
Slug: 1-detenido-y-7-heridos-por-represion-del-esmad-a-estudiantes-de-la-universidad-del-valle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/U-Valle1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Elpaís.com.co 

<iframe src="http://www.ivoox.com/player_ek_9006558_2_1.html?data=mpWdmJqZfI6ZmKiakpqJd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRdYzYxtnS0M7Is4ztjJyYysrWrcXj1JDd0dePtsbk08rgy4qnd4a2lNOYxsrQb6bHrqaxjcaPqdTo1smah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Marcela Urrea, representante al Consejo Académico de la Universidad del Valle] 

###### [15 Oct 2015]

[En el marco de la movilización pacífica adelantada por estudiantes de la Universidad del Valle con el objetivo de informar a la ciudadanía caleña de la grave **crisis por la que atraviesa el Hospital Universitario**, efectivos del ESMAD causaron fuertes **heridas en extremidades a 8 estudiantes, 1 de los cuales fue detenido**.]

[Marcela Urrea, representante al Consejo Académico de la Universidad del Valle, asegura que para **informar a la ciudadanía sobre la crisis financiera del hospital**, decidieron movilizarse pacíficamente por varias vías de Cali desde tempranas horas de esta mañana.]

[No obstante **unidades del ESMAD "No respetaron el esquema de derechos humanos"**, hirieron a 7 estudiantes en sus piernas y capturaron a 1 alumno de cuarto semestre de atención prehospitalaria, quienes hasta el momento no han sido remitidos a centros de atención.]

[Urrea afirma que debido a esta crisis y a la desatención por parte del Gobierno, desde hace 4 semanas la Facultad de Ciencias de la Salud decidió entrar en paro, días después el resto de facultades se unieron y al día de hoy **toda la universidad se encuentra en paro indefinido**.]

[A esta hora se está llevando a cabo un foro con los profesores Mario Hernández y Carolina Corcho, en las instalaciones de la Universidad del Valle, con la asistencia de sus trabajadores, estudiantes y profesores en el que se discute la **actual crisis de financiación de la universidad y del hospital**.]

[Debido a que no han habido respuestas concretas por parte del Gobierno, un grupo de estudiantes se dirigirá al Ministerio de Salud en Bogotá el próximo martes, para exigir que sean atendidas sus reclamaciones, puesto que la Mesa instalada tras el bloqueo a la Gobernación del Valle la semana pasada, contempla como solución a la crisis la **intervención del hospital**, lo que para diversos sectores **podría significar su cierre**.]
