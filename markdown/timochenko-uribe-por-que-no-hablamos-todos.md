Title: Timochenko, Uribe: ¿Por qué no hablamos todos?
Date: 2016-05-21 06:00
Category: Fernando Q, Opinion
Tags: Alvaro Uribe, FARC, paz, proceso de paz, Timochenko
Slug: timochenko-uribe-por-que-no-hablamos-todos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/uribe-timochenko.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: PGD 

#### **Por [Luis Fernando Quijano](https://archivo.contagioradio.com/fernando-quijano/) - [@FdoQuijano](https://twitter.com/FdoQuijano) ** 

###### 21 May 2016 

Han trascurrido 48 años de mi vida parte de ellos esperando los triunfos revolucionarios en América  Latina, desde muy joven estuve siguiendo paso a paso el desarrollo de la guerra revolucionaria en Nicaragua, vibraba cada vez que el Frente Sandinista Liberación  Nacional (FSLN) le ganaba alguna batalla a la dictadura de Anastasio Somoza, títere de los Estados Unidos, igualmente me emocione con razón con la lucha revolucionaria del Farabundo Martí Liberación Nacional (FSLN)  el primero triunfo, el segundo pacto, las emociones fueron contradictorias, triunfo y empate militar.

Durante décadas estuve atento al desarrollo del proceso revolucionario en Colombia, mis inclinaciones estuvieron cercanas a la guerrilla del M19, al ADO, posteriormente al EPL y al ELN, confieso que tuve reparos con el hegemonismo de las FARC, eso me inculcaron, ellos eran los revisionistas, incluso tuve esperanzas que el proyecto miliciano urbano fuera la panacea de la lucha insurgente urbana, eso se llama ingenuidad, lamentablemente fue un espejismo, los enemigos de diversa índole frustraron ese propósito loable de liberar al movimiento social y comunitario y por ende a las comunidades  de las imposiciones del crimen urbano servil al poder real en la ciudad, el debacle llegó para quedarse, el paramilitarismo se tomó la ciudad metropolitana.

Décadas después ya solo pienso en la importancia de que nuestro país llegue la paz, la paz completa, desafortunadamente el pragmatismo y la aplicación del concepto atribuido a  Nicolás Maquiavelo de que el fin justifica los medios alejaron los deseos del triunfo revolucionario a través de la lucha armada lo que no significa de ninguna manera que la rebelión no esté justificada ante el estado de cosas que nos rodea, el neoliberalismo rampante criminal, pensemos que esta vez haremos la revolución  desde la movilización social y la lucha popular pero siempre luchando contra el dios mercado o sea el capitalismo neoliberal.

Hablar de paz en estos tiempos donde se habla de resistencia civil a la paz y pregones de paz pero acompañados de la venta del país al gran capital me llegó la imagen del abrazo de Álvaro Uribe a Piedad Córdoba, pensé que la reconciliación en Colombia no estaba lejos, me reafirme en esto ahora que el comandante de las Farc invita a su acérrimo enemigo Álvaro Uribe Vélez a  que conversemos, la respuesta del expresidente y hoy senador ambigua pero racional  no me preocupa, él tiene una imagen que cuidar no puede mostrar debilidad ya que lo crucificarían sus seguidores que no son pocos.

Pero el abrazo de Uribe y Piedad Córdoba, la invitación de Timochenko a Uribe para que hablen y la respuesta de este me da pie para decirle a ellos dos y a los ciudadanos rurales y urbanos, a las organizaciones sociales, partidos políticos, empresariado, en fin a todos los que habitan nuestra patria, ¿Por qué no hablamos todos? Pongámonos de acuerdo en lo fundamental, sí, como decía Álvaro Gómez Hurtado.

¿Será muy difícil ponernos de acuerdo en la necesidad de acabar el conflicto armado que desangra campos ciudades? ¿Será muy difícil dar pasos firmes para alcanzar la paz completa: Paz urbana, paz rural, posconflicto para todos? ¿Será que alguno de  nosotros necesita la guerra como sustento para seguir vigente? Espero que no.

La paz es urgente y necesaria, ha llegado la hora de que todos demos el paso firme a ella, por eso, Timoleón Jiménez y Álvaro Uribe Vélez ustedes pueden llegar a hablar y hasta ponerse de acuerdo, eso no significa que la paz va a llegar, les propongo que hablemos todos y pongámonos de acuerdo en lo fundamental: la reconciliación como camino a la paz y a la reconciliación nacional, ¿Por qué no intentarlo? Si fallamos nos vamos a la guerra y después que el Dios del pueblo disponga.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
