Title: Inicia protesta pacífica de la población reclusa en Colombia
Date: 2020-04-08 14:23
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Crisiscarcelaria, #IvánDuque, carcel, covid19
Slug: inicia-protesta-pacifica-de-la-poblacion-reclusa-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/unnamed-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Una nueva protesta pacífica nacional inició desde este 7 de abril en las cárceles de Colombia, luego de que ninguna de las medidas manifestadas por el gobierno de Iván Duque para salvaguardar **la vida de la población carcelaria, se estén llevando a cabo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con la población reclusa esta protesta está encaminada a lograr que "el Estado colombiano visualice la magnitud del problema carcelario" y que tome medidas urgentes para evitar una catástrofe al interior de los centros penitenciarios.

<!-- /wp:paragraph -->

<!-- wp:heading -->

¡Queremos vivir! exigencia del movimiento carcelario
----------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La población reclusa, en su pliego de exigencias le está pidiendo al presidente Iván Duque que decrete de inmediato la **emergencia social, humanitaria y carcelaria** en todas las cárceles del país. (Le puede interesar: ["Movimiento Carcelario denuncia represalias por parte del INPEC"](https://archivo.contagioradio.com/movimiento-carcelario-denuncia-represalias-por-parte-del-inpec/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También están pidiendo que se decrete una reducción de penas del 50% a las penas de la población reclusa y que el INPEC cese los operativos al interior de los centros penitenciarios para evitar posibles contagios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los reclusos señalan que si se toman medidas urgentes se podrán evitar "masacres como la cometida el pasado 21 de marzo". Asimismo afirman que aunque sean considerados como "el último eslabón de la sociedad", **son seres humanos con derechos consagrados constitucionalmente.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"El derecho a la vida, es un derecho fundamental inherente al ser humano, sin distinción alguna. El Estado Colombiano es el responsable de hacer prevalecer este derecho y la salud" aseguran. (Le puede interesar:["Todos tenemos derechos, ellos también"](https://www.justiciaypazcolombia.com/todos-tenemos-derechos-ellos-tambien-2/))

<!-- /wp:paragraph -->

<!-- wp:video {"id":82968,"src":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/received_208807630558633.mp4"} -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/received_208807630558633.mp4">
</video>
  

<figcaption>
Video prisioneros Eron Picota

</figcaption>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

Hasta el momento se han registrado amotinamientos en la cárcel de Pasto, Nariño. En otros centros penitenciarios como La Picota y el Buen Pastor, en Bogotá, las manifestaciones se han hecho de forma pacífica. La población reclusa de la cárcel de Cúcuta también se unieron a estas peticiones.

<!-- /wp:paragraph -->

<!-- wp:html -->

> [\#DerechosDeLxsPrwsxs](https://twitter.com/hashtag/DerechosDeLxsPrwsxs?src=hash&ref_src=twsrc%5Etfw) | ESTABLECIMIENTO PENITENCIARIO DE ALTA Y MEDIANA SEGURIDAD CÓMBITA - BOYACÁ  
>   
> Más cárceles del país se unen a la jornada nacional de protesta pacífica en el interior de los centros penitenciarios. [pic.twitter.com/GxqH0mrepU](https://t.co/GxqH0mrepU)
>
> — DerechosdelosPueblos (@equipojuridico) [April 7, 2020](https://twitter.com/equipojuridico/status/1247588671664029698?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>

