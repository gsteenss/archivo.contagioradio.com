Title: Paramilitares amenazan a defensor de DH de Marcha Patriótica
Date: 2017-01-04 11:19
Category: DDHH, Nacional
Tags: Amenazas, Cauca, marcha patriotica, paramilitares
Slug: paramilitares-amenazan-a-defensor-de-dh-de-marcha-patriotica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [4 Enero 2017] 

A través de un comunicado la Red de Derechos Humanos del Suroccidente Colombiano “Francisco Isaías Cifuentes” dio a conocer la **amenaza que llegó al celular del defensor de DH Oscar Gerardo Salazar** en la que aseguran *“ustedes son declarados objetivo militar por su relación con las farc y por hacerse su falsa labor de dirigentes”.*

Dicha imagen fue enviada al celular del dirigente, quien es parte del Movimiento social Marcha Patriótica, Vocero de la Cumbre Agraria Campesina Étnica y Popular, vicepresidente de la subdirectiva CUT Cauca, entre otros cargos.

En la amenaza, que es firmada por paramilitares autodenominados “AUC”, afirman que estas estructuras han “llegado con más fuerza para terminar con sus propósitos de su tal paz” y añaden “en cualquier momento se darán cuenta defensores de derechos humanos de mierda de reservas campesinas somos nosotros quienes vamos a gobernar en poco tiempo (sic)”. Le puede interesar: [No cesan las agresiones contra integrantes de Marcha Patriótica](https://archivo.contagioradio.com/no-cesan-agresiones-contra-integrantes-marcha-patriotica/)

La intimidación fue recibida por el defensor de DH Oscar Gerardo, luego de haber participado en la VI Marcha por la Vida y por el Agua Macizo en el municipio de La Vega (el día 23 de diciembre de 2016), en la que comunidades campesinas e indígenas realizaron acciones de control y protección del Macizo contra la minería.

Frente a dichas amenazas, de los cuales continúan siendo víctimas líderes y defensores de DH en el departamento del Cauca, **las organizaciones han responsabilizado al Estado colombiano en cabeza del presidente Juan Manuel Santos**, a la Gobernación del Departamento del Cauca, al Ejército y a la Policía de la zona **por cualquier situación que pueda presentarse y que signifique el riesgo a la vida e integridad de los defensores de DH en la región.**

Así mismo, han exigido al Estado colombiano y a las instituciones competentes a que se garantice el derecho a la vida, a la honra y reputación y al trabajo, de Oscar Salazar y de los líderes y defensores de Derechos Humanos de la Coordinación Campesina y del Movimiento Social y Político Marcha Patriótica en el Departamento del Cauca. Le puede interesar: [5 propuestas de Marcha Patriótica para frenar oleada de violencia](https://archivo.contagioradio.com/5-propuestas-marcha-patriotica-frenar-oleada-violencia/)

[Denuncia Pública Amenazas a Líderes de Marcha Patriótica en Cauca](https://www.scribd.com/document/335685669/Denuncia-Publica-Amenazas-a-Lideres-de-Marcha-Patriotica-en-Cauca#from_embed "View Denuncia Pública Amenazas a Líderes de Marcha Patriótica en Cauca on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_15672" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/335685669/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-stY4s0NwIUFj0fRw6URk&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
