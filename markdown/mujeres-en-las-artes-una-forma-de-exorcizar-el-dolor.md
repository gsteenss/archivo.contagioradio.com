Title: "Mujeres en las Artes", una forma de exorcizar el dolor
Date: 2016-09-23 13:03
Category: Cultura, eventos
Tags: mujer, paz, Teatro Bogota, Teatro Quimera
Slug: mujeres-en-las-artes-una-forma-de-exorcizar-el-dolor
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/mujeres-en-las-artes-e1474653107495.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa Teatro Quimera 

###### 22 Sep 2016 

Por primera vez se realizará en Bogotá el festival “**Mujeres en las Artes**”, del 22 de septiembre hasta el 1 de octubre en el **Teatro Quimera**.

Este espacio busca presentar por medio del arte teatral, como varias **mujeres víctimas de violencia sexual**, **intrafamiliar, prostitución, pobreza** entre otros, han exorcizado sus dolores y transformado sus vidas a través del teatro.

**Nohora Cruz**, directora de la **Fundación Vida Nueva**, institución co-organizadora del Festival, asegura que "**el arte es una forma de sanar heridas profunda**s" en referencia al trabajo que realizan con las mujeres, descubriendo en ellas "verdaderos talentos que las hacen sentir que **no nacieron únicamente para ejercer un oficio como estos**".

![mujeres-en-las-artes](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/mujeres-en-las-artes1.jpg){.alignnone .size-full .wp-image-29729 width="681" height="925"}

Las obras seleccionadas dentro de la programación del Festival son “**Madre y coraje, los hijos de la Guerra**” del grupo Génesis Teatro, “**Cenizas a las Cenizas**” del Teatro Quimera, “**No se lo cuentes a nadie**” del Teatro Suspendido  y “ **Con tacto de Mujer**” de la Fundación Vida Nueva, en los horarios de la tarde y noche.

Así mismo en la sala del teatro **estarán expuestos varios poemas, relatos, memorias e imágenes** que buscan sembrar en el espectador las vivencias de estas mujeres que optaron escapar de su dolor en las tablas.

<iframe id="audio_13030809" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13030809_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
