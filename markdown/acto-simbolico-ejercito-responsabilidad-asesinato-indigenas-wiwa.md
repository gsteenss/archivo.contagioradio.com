Title: Con acto simbólico, Ejército reconoce responsabilidad en asesinato de indígenas Wiwa
Date: 2019-05-28 17:35
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Pueblo indígena, Wiwa
Slug: acto-simbolico-ejercito-responsabilidad-asesinato-indigenas-wiwa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-28-at-5.27.11-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Este jueves 28 de mayo se instaló una placa en conemoración a los hermanos **Ángel Milqueades Loperena Montero y Darío Loperena Montero**, líderes del pueblo **Wiwa en el batallón Rondón, de La Guajira.** El acto hace parte de una decisión judicial que ordena la instalación de una placa en lugares visibles de Comandos de polícia en los municipios de Rioacha, Santa Marta, los batallones Rondón y La Popa (de Valledupar).

> [\#Ahora](https://twitter.com/hashtag/Ahora?src=hash&ref_src=twsrc%5Etfw) Descubrimiento de placa en memoria de indígenas Wiwa Ángel y Rafael Loperena Montero, asesinados por paramilitares el 18 de enero de 2005 en Valledupar. Estado fue responsable bajo la comandancia de la X brigada del hoy Cdte del Ejército Nicasio Martínez [\#ParaQueNoSeRepita](https://twitter.com/hashtag/ParaQueNoSeRepita?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/sG1hPsyT9p](https://t.co/sG1hPsyT9p)
>
> — ColectivoDeAbogad@s (@Ccajar) [28 de mayo de 2019](https://twitter.com/Ccajar/status/1133416930180849666?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Ángel y Darío Loperena, hermanos y líderes indígenas del pueblo Wiwa** 

Ángel Loperena fue tesorero de la Organización Indígena Wiwa Yugumaiun Tayrona, su hermano, Darío Loperena se desempeñó como maestro de la comunidad Siminke del pueblo Wiwa, pero tuvo que dejar su labor debido a amenazas. Según el **Colectivo de Abogados José Alvear Restrepo (CAJAR),** en 2005 ambos hermanos se dedicaron a la siembra de yuca para conseguir el dinero de su subsistencia. (Le puede interesar:["Comunidades de La Guajira demandaron a Cerrejón y piden anular licencia ambiental"](https://archivo.contagioradio.com/comunidades-la-guajira-presentan-demanda-cerrejon-estado/))

Según relata el Colectivo, el 18 de enero de ese año los hermanos " se encontraban departiendo en un local comercial en San Juan del Cesar, fueron abordados y requisados por la Policía. Horas después, al dirigirse a otro establecimiento, hombres armados les dispararon segando sus vidas". Por este crimen, el Tribunal Contencioso Administrativo de La Guajira condenó a la Policía y el Ejército por omisión, puesto que no cuidó la vida de los líderes, aún teniendo en conocimiento la presencia paramilitar en la región y el riesgo contra sus vidas.

Por estos hechos, el tribunal ordenó fijar una placa en lugares visibles en "los Comandos de Policía de los municipios de Riohacha y Santa Marta, en el Batallón Rondón, en Cartagena y La Popa, Valledupar", como una forma de honrar la memoria de los hermanos Loperena y reconocer los errores del Estado. (Le puede interesar: ["Los casos por los que debería ser investigado el mayor general Nicacio Martínez"](https://archivo.contagioradio.com/deberia-investigado-mayor-general-nicacio-martinez/))

### **Hasta el momento, solo han sido instaladas dos placas de cinco** 

**Pedro Loperena, comisionado de derechos humanos del pueblo Wiwa**, expresó que 14 años después de ocurridos los hechos sigue siendo triste recordar los hechos, "se trata de encender una vela como recordando la vida y cuando se apaga la vela es como si se apagara la vida". A ello se suma la dificultad en el reconocimiento de la omisión por parte de agentes del Estado, puesto que "algunos funcionarios del Ministerio de Defensa no aceptan este tipo de hechos".

No obstante, el Comisionado de DD.HH., manifestó que el respeto del fallo emitido en este caso constribuye a que se construya un Estado Social de Derecho que sea "aún más social", que se protejan los derechos y no que se actúe contra los mismos. En consecuencia, reconoció el trabajo de las máximas autoridades que actualmente están en los batallones de La Popa y Rondón, dando cumplimiento al fallo pese a algunas fallas logísticas que se han presentado en la instalación de las placas.

### **Falta avanzar en verdad, reparación y garantías de no repetición** 

El Líder indígena dijo que las autoridades judiciales deberían aportar en el esclarecimiento de los hechos para evitar que situaciones como estas se repitan; sin embargo, sostuvo que el proceso judicial ha sido lento y poco se ha avanzado. Loperena recordó que "muchos casos del pueblo Wiwa están quedando en la impunidad, **hay más de 22 casos de ejecuciones extrajudiciales que no deben quedar impunes"**.

Según el Colectivo de Abogados, el pueblo Wiwa fue estigmatizado y acusado de ser colaborador de las FARC-EP; y entre 1998 y 2002, producto de acciones del **Bloque Norte de las Autodefensas Unidas de Colombia (AUC)** se documentaron 44 desapariciones forzadas, 166 ejecuciones extrajudiciales, 92 torturas, 52 secuestros, 2 masacres y el desplazamiento de, por lo menos, 1300 integrantes de pueblos indíegas de la Sierra Nevada de Santa Marta.

Por estos hechos, la Organización Nacional Indígena de Colombia (ONIC) y el CAJAR solicitaron ante la Comisión Interamericana de Derechos Humanos (CIDH) medidas cautelares que fueron otorgadas en 2005. Loperena sostiene que estas decisiones, así como la instalación de las placas contribuyen en la reparación de la comunidad, pero cree que "falta todavía para que este tipo de hechos tan lamentables no vuelvan a pasar", sobre todo en materia de investigación penal; y porque "no se han tomado las medidas necesarias para garantizar la vida de la dirigencia Wiwa".

<iframe id="audio_36426751" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36426751_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
