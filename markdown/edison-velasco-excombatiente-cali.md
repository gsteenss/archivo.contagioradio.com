Title: Asesinato del excombatiente Edison Velasco, el segundo en menos de un mes en Cali
Date: 2019-12-28 12:09
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato de excombatientes, Cali, Valle del Cauca
Slug: edison-velasco-excombatiente-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Edison-Velasco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

El pasado viernes 27 de diciembre en Cali, Valle del Cauca, fue asesinado **Edison Velasco**, de 43 años. El excombatiente había sido aceptado dentro del proceso de reincorporación desde febrero de 2018. Con el homicidio cometido contra Edison, ya son cinco los firmantes de la paz asesinados durante el mes de diciembre y al menos 69 en lo corrido del 2019.

Los hechos ocurrieron en la parte alta de la vereda Los Andes, corregimiento Pueblo Nuevo, zona rural de Cali, donde según información preliminar, hombres armados atacaron a Edison Velasco cuando se movilizaba en un vehículo a la entrada de la finca Las Mercedes. [(Lea también: Asesinato de Ender Ravelo evidencia la ausencia de garantías para excombatientes en Norte de Santander)](https://archivo.contagioradio.com/asesinato-ender-ravelo-evidencia-incremento-amenazas-excombatiente-norte-de-santander/)

El propietario de la finca Las Mercedes quien reportó el crimen, explicó a las autoridades que escuchó varias detonaciones frente a su residencia, sin embargo pensó que estaban quemando pólvora, pese a ello y debido a los ladridos de los perros de la propiedad , salió a ver qué estaba ocurriendo y observó el vehículo y la víctima.  [(Lea también: Con Wilson Parra, son 169 excombatientes de FARC asesinados desde la firma del Acuerdo de Paz](https://archivo.contagioradio.com/con-wilson-parra-son-169-excombatientes-de-farc-asesinados-desde-la-firma-del-acuerdo-de-paz/))

A este suceso se suma el también ocurrido en la capital del Valle del Cauca el pasado 12 de diciembre de 2019 en el Barrio Mojica de Cali, donde fue hallado sin vida **Arley Mejía Saldaña, reincorporado de las FARC** a quien se encontró varias heridas de arma blanca. Su familia, lo buscaba desde el pasado jueves e incluso lo había reportado como desaparecido. [(Le puede interesar: Asesinato de Ender Ravelo evidencia la ausencia de garantías para excombatientes en Norte de Santander](https://archivo.contagioradio.com/asesinato-ender-ravelo-evidencia-incremento-amenazas-excombatiente-norte-de-santander/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
