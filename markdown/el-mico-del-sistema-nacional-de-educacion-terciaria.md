Title: El mico del Sistema Nacional de Educación Terciaria
Date: 2017-03-10 10:14
Category: Opinion, Paola
Tags: educacion, Fast Track, Ministerio de Educación
Slug: el-mico-del-sistema-nacional-de-educacion-terciaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/acuerdo-superior.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Montaje 

#### Por: [Paola Galindo](https://archivo.contagioradio.com/paola-galindo/) 

###### 10 Mar 2017

En el mes de enero las alarmas de la Comunidad educativa y científica de las Instituciones de Educación Superior del país se encendieron a causa del anuncio, por parte de los Ministerios de Educación y del Trabajo, de implementar vía Fast Track el Sistema Nacional de Educación Terciaria (SNET) y el Marco Nacional de Cualificaciones (MNC), bajo el argumento de desarrollar el punto de educación rural de los acuerdos de paz.

En el marco de esta coyuntura, se adelantó una Audiencia Pública sobre el tema convocada por el Representante a la Cámara del PDA, Víctor Correa, en la que, en consideración de los argumentos expuestos por la comunidad académica, el Ministerio de Educación Nacional echó para atrás su intención de presentar esta iniciativa empleando la vía rápida de implementación. Lo que no moderó fue su postura frente a la necesidad de un sistema de Educación Terciaria, orientación consignada tanto en la política pública de educación superior 2034 como en el Plan Nacional de desarrollo.

La propuesta de implementar el Sistema Nacional de Educación Terciaria no es una novedad, es apenas la reglamentación de algo que viene sucediendo «de hecho» y que en palabras del profesor Andrés Felipe Mora bien puede resumirse de la siguiente manera: “La expansión de la educación superior en Colombia se ha fundamentado en el estímulo a niveles educativos distintos a los estudios universitarios y se ha correspondido con la estructuración de un sistema de educación terciaria segmentado y jerárquico (…)” (Mora, 2016: 47).

Así por ejemplo, y teniendo como fuente datos del CESU (2014) y las proyecciones del DANE (2005), mientras que en “(…) el año 2002 del total de la matrícula de Educación Superior la educación técnica y tecnológica representaba el 19,5%. Para el año 2014 ese porcentaje ascendió a un 37,1%; \[Por otro lado\] mientras que en el año 2002 la matrícula en educación profesional ascendía al 80,5%, en 2014 disminuyó a 62,9% (Mora, 2016:47).

Este comportamiento de la matrícula en Educación Superior no es azaroso, es producto de una tendencia histórica de reproducción de esquemas de desigualdad, en la que las familias con menores ingresos se conducen obligadamente a los segmentos educativos de educación y formación para el trabajo, mientras que, quienes tienen la capacidad adquisitiva o en su defecto, de endeudamiento, acceden a los segmentos más altos.

¿Cuáles son las razones de esta situación?: el lugar dónde se nace, la pertenencia étnica o cultural y los ingresos económicos determinan la modalidad de la educación superior a la cual se ingresa.

El SNET sin duda refuerza esta tendencia ya que todos los términos, procesos o niveles de enseñanza superior son subsumidos a la Categoría de

«Educación Terciaria», es decir se equipara «Educación Superior» con la «Educación terciaria» al definir esta última como “Toda aquella educación y formación, posterior a la educación media, \[comprendiendo\] los programas educativos y formativos de los niveles de pregrado y posgrado” (Proyecto de Decreto Ley SNET, 2017).

De este modo la educación para el trabajo es la categoría estructurante del sistema y el criterio de definición del conjunto de la oferta educativa y del proceso de evaluación de la calidad sin garantizar posibilidad de elección, esto es, que incluso en las áreas más alejadas del país –aquellas denominadas áreas rurales dispersas-, se cuente con el total de la oferta educativa y con la posibilidad de acceso, por igual, a cada una de ellas.

Que las universidades campesinas y los componentes de investigación sean posibles en estas áreas dónde el conflicto ha actuado con fuerza, permite que la creatividad sea la alternativa que haga florecer elecciones voluntarias y no imposiciones del mercado.

\*Mora Cortés, A.F. (2016). La seudorevolución educativa. Desigualdades, capitalismo y control en la educación superior en Colombia. Bogotá:

###### Foto: Consejo de Educación Superior —Cesu - Acuerdo por lo superior 2034 - propuesta de política pública para la excelencia de la educación superior en Colombia 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
