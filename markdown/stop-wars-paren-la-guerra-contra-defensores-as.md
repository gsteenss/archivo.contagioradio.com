Title: "Stop Wars, paren la guerra contra defensores (as)”
Date: 2017-09-12 12:27
Category: DDHH, Nacional
Tags: asesinatos de lideres, defensores de derechos humanos, Derechos Humanos, Somos defensores
Slug: stop-wars-paren-la-guerra-contra-defensores-as
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/stop-wars4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ORGS Colombia ] 

###### [12 Sept 2017 ] 

La organización Somos Defensores presenta hoy la trilogía “Stop Wars”; **tres informes sobre la grave situación que viven los defensores de derechos humanos en Colombia** y usando la analogía del símbolo del cine para llamar la atención de forma especial en tres aspectos, la impunidad, el uso del aparato de inteligencia y la ausencia de un modelo de protección eficaz.

Carlos Guevara, director de la organización Somos Defensores, indicó que los informes son un mensaje para **“detener la guerra contra los defensores** por lo que estamos tocando puntos críticos que enfrentan los defensores en el país”.

Ante esto, uno de los informes hace referencia a **la falta de resultados en las investigaciones que se llevan a cabo contra asesinatos o amenazas** que ocurren contra los defensores. El segundo informe detalla el uso histórico de la inteligencia del Estado como arma de guerra contra los líderes. El último, hace referencia a la forma como el gobierno protege a estas personas. (Le puede interesar: ["Somos Defensores documentó 51 asesinatos de líderes de DDHH en 2017"](https://archivo.contagioradio.com/somos-defensores-documento-51-asesinatos-de-defensores-de-derechos-humanos-en-2017/))

Para realizar estos informes, la organización **realizó investigaciones a lo largo de 2 años** en donde además de detallar la situación de riesgo que viven los defensores del país, buscan aportar a la construcción de una solución que involucre a las diferentes instancias de la sociedad.

### **Temporalidad de las investigaciones que detallan los informes** 

Guevara indicó que el informe sobre la impunidad en las investigaciones sobre homicidios y amenazas **relata las acciones que se han llevado a cabo desde 2009 hasta 2016** donde “se establece la Mesa Nacional de Garantías para los Defensores”.

Por su parte, el informe sobre la inteligencia del Estado como arma de guerra contra los defensores incluye una temporalidad más grande. “En Colombia no hay un libro o una investigación que revise las operaciones de inteligencia que ha hecho el Estado de manera histórica contra los defensores”. Por esto **realizaron una línea de tiempo de 40 años** “que comienza con el batallón Colombia y la teoría del enemigo común”.

Finalmente, el informe sobre los mecanismos de protección que ha utilizado el Estado para proteger de manera parcial a los defensores, **hace un recorrido de 22 años** “desde que existe el programa de protección a personas en riesgo. (Le puede interesar: ["Asesinatos de líderes sociales son práctica sistemática: Somos Defensores"](https://archivo.contagioradio.com/asesinatos-de-lideres-son-practica-sistematica-33507/))

Somos defensores invitó a la ciudadanía en general pero en especial a los defensores de derechos humanos a asistir a la presentación de **“Stop Wars: Paren la guerra contra los defensores”.** Guevara afirmó que “los informes son para los defensores para conocer la situación que viven cuando quieren defender los derechos humanos”.

La presentación será en el **Museo Nacional de Bogotá a las 6:30 pm.** Para asistir debe confirmar su asistencia en <info@somosdefensores.org> debido a que los cupos son limitados.

<iframe id="audio_20827965" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20827965_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
