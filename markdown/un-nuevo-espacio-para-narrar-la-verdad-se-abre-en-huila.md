Title: Un nuevo espacio para narrar la verdad se abre en Huila
Date: 2019-09-13 11:28
Author: CtgAdm
Category: Nacional, Paz
Tags: casa de la verdad, comision de la verdad, Neiva, víctimas del conflicto armado
Slug: un-nuevo-espacio-para-narrar-la-verdad-se-abre-en-huila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/EEWzqFKXkAElUdk.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-13-at-4.35.15-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@ComisionVerdadC] 

A partir de este viernes 13 de septiembre, como parte de la extensión territorial de la Comisión de la Verdad, se abre en Neiva, una nueva casa preparada para escuchar y acompañar a las víctimas, al igual que a los responsables y personas vinculadas al conflicto en Colombia que decidan aportar testimonios que apoyen el trabajo de esclarecimiento de la verdad del conflicto en esta zona.

Martha Patricia Obregón, coordinadora territorial de la nueva casa en el Huila, reconoce la importancia de estos espacios en todos los territorios afectados por el conflicto armado en el país, “las Casas de la Verdad son espacios creados para la ciudadanía, un lugar que les permite reconocer la importancia de la verdad para el reconocimiento, la convivencia y la no repetición”. (Le puede interesar:[Comisión de la Verdad llega a Buenaventura y fortalece su presencia en el Pacífico](https://archivo.contagioradio.com/comision-de-la-verdad-llega-a-buenaventura-y-fortalece-su-presencia-en-el-pacifico/))

![Verdad](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-13-at-4.35.15-PM-1024x682.jpeg){.aligncenter .size-large .wp-image-73631 width="1024" height="682"}

El territorio huilense ha dejado en un saldo de 192.000 hechos victimizantes, esto según el Registro Único de Víctimas (RUV), razón por la cual se convirtió en otro punto para la construcción de esta sede, “esta casa ha sido pedida por las víctimas y por los diferentes actores en el marco del conflicto, es un escenario donde las personas sentirán plena confianza para expresar sus experiencias en resiliencia”, manifestó Obregón.(Le puede interesar: [El derecho a la verdad es el de todas las verdades](https://archivo.contagioradio.com/el-derecho-a-la-verdad-es-el-de-todas-las-verdades/))

> [\#EnVivo](https://twitter.com/hashtag/EnVivo?src=hash&ref_src=twsrc%5Etfw) | En la apertura de la Casa de la Verdad de [\#Neiva](https://twitter.com/hashtag/Neiva?src=hash&ref_src=twsrc%5Etfw), se desarrolla el conversatorio: ‘Desde la verdad, hablemos de la no repetición’. Siga aquí la transmisión &gt;&gt; <https://t.co/k1RKQDMITZ> [pic.twitter.com/9YvEyKNmZR](https://t.co/9YvEyKNmZR)
>
> — Comisión de la Verdad (@ComisionVerdadC) [September 13, 2019](https://twitter.com/ComisionVerdadC/status/1172525093764964352?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
La apertura se dará en el recinto ferial ‘La Vorágine’ ubicado en la carrera 4 No. 22-11 de la ciudad de Neiva, y contará con la participación de víctimas, campesinos, organizaciones sociales, excombatientes, comunidad internacional, instituciones del gobierno departamental, Fuerzas Militares entre otros; Obregón señala que durante “la apertura tendremos un conversatorio con diferentes actores, el cual llevará por nombre “para la verdad, hablemos de la no repetición”.

Con esta nueva Casa en Neiva, ya son 20 las que se suman a este trabajo de la Comisión de la Verdad , por reconstruir y esclarecer qué ocurrió durante más de 50 años de conflicto armado en Colombia. (Le puede interesar: [Comisión de la Verdad lista para tejer nuevos relatos de memoria en Medellín](https://archivo.contagioradio.com/comision-de-la-verdad-lista-para-tejer-nuevos-relatos-de-memoria-en-medellin/))

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
