Title: Julián Gil ¿Un falso positivo Judicial?
Date: 2019-02-05 12:26
Author: AdminContagio
Category: Expreso Libertad
Tags: congreso de los pueblos, presos politicos
Slug: julian-gil-un-falso-positivo-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/DfCZAbUXUAALIEf-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colombia Informa 

###### 5 Feb 2018 

Desde hace más de 8 meses se produjo la captura de **Julian Gil, secretario de la plataforma Congreso de los Pueblos e integrante del proceso popular Quinua**. Lo último que ha sucedido en el caso, es que el testigo que lo inculpaba fue asesinado, mientras que la Fiscalía no ha revelado más pruebas en su contra. ¿Se tratará de otro caso de Falsos positivos judiciales en Colombia?

Escuche en esta emisión del Expreso Libertad las voces de las personas que conocen a Julian Gil y su relato frente a este hecho.

<iframe id="audio_32285702" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32285702_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
