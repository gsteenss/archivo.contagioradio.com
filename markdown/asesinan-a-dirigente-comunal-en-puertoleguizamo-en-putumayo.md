Title: Asesinan a dirigente Comunal en Puerto Leguízamo, Putumayo
Date: 2018-08-10 09:53
Category: DDHH, Nacional
Tags: Alejandro Jacanamijoy, dirigentes comunales, lideres sociales
Slug: asesinan-a-dirigente-comunal-en-puertoleguizamo-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [10 Ago 2018] 

El pasado 9 de agosto fue asesinado **Alejandro Jacanamejoy, presidente de la Junta de Acción comunal,** cuando se encontraba en la comunidad de Piñuña, en el municipio de Puerto Leguízamo, Putumayo. El líder comunal hacía parte del programa Nacional de Sustitución de cultivos de uso ilícito y promovía la implementación de los Acuerdos de Paz con la FARC.

Los hechos se presentaron hacia el medio día y la comunidad ha manifestado que se encuentra  consternada y atemorizada con lo ocurrido.  Además denunciaron que en la zona **hay presencia de grupos armados qué se están disputando el control social territorial.** Algunos argumentando que nacen por el incumplimiento de los Acuerdos, y otros vinculados con la criminalidad del tráfico de drogas desde Puerto Asís.

Asimismo, la Comisión Justicia y Paz denunció que estas operaciones de grupos armados se vienen dando en el territorio en medio de la presencia militar regular que evidencia una "ineficacia en las medidas de militarización". (Le puede interesar: ["Uriel Gómez, primer líder asesinado en el gobierno Duque"](https://archivo.contagioradio.com/primer-lider-asesinado-gobierno-duque/))

Noticia en desarrollo...
