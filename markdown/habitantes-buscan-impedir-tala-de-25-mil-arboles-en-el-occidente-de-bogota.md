Title: Habitantes buscan impedir tala de 25 mil árboles en el occidente de Bogotá
Date: 2017-07-12 17:02
Category: Ambiente, Nacional
Tags: deforestación, Kenedy, tala de árboles
Slug: habitantes-buscan-impedir-tala-de-25-mil-arboles-en-el-occidente-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/defensa-de-los-arboles-bavaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Habitantes de Kennedy] 

###### [12 Jul 2017] 

Los habitantes de la localidad de Kennedy realizaron una movilización en defensa del ambiente y contra la tala de, aproximadamente, **350 árboles que se encuentran ubicados en la antigua planta de Bavaria** y que actualmente sirve como uno de los pulmones que descontamina el aire de la capital.

Esta sería la primera parte de la tala de aproximadamente **unos 25 mil árboles con los que se acabaría para iniciar la construcción de un complejo de viviendas**. De acuerdo con Carlos Bustos, habitante de la localidad desde hace 35 años, este bosque se encuentra en una de las zonas más vulnerables de Bogotá, debido a la contaminación que en el pasado provocó la fábrica de Bavaria y que luego de su cierre ha permitido un cambió en el aire que se respira.

“**Desde que estuvo en operación la planta todas las mañanas salía una cantidad de humo por la chimenea, vapores, que contaminaban todo el barrio**, uno le pasaba la mano a un carro y quedaba negra” afirmó Bustos. (Le puede interesar: ["Más de 2700 especies de fauna y flora en peligro por deforestación en Colombia"](https://archivo.contagioradio.com/flora_fauna_en_peligro_deforestacion/))

Los habitantes del sector han manifestado que acabar con este pulmón de aire, significa afectar a una**s 800 mil personas que se encuentran en los alrededores de este bosque**, además Bustos señaló que esta es una de las áreas verdes más grandes que hay en la localidad razón por la cual, también afectaría el aire que respira la comunidad y los barrios de alrededor.

Lo que lo vecinos estamos diciendo es que, si Bavaria contaminó durante tantos años a nuestro barrio y a nuestra gente, es importante que **Bavaria nos deje ese pulmón como una contraprestación a toda esa contaminación que nos dejó durante muchos años**” manifestó Bustos.

<iframe id="audio_19770814" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19770814_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
