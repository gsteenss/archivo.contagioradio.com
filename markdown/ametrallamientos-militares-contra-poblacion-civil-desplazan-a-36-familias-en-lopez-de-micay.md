Title: Ametrallamientos militares contra población civil desplazan a 36 familias en López de Micay
Date: 2016-05-23 12:33
Category: DDHH, Nacional
Tags: Ejército Colombia, Familias desplazadas López de Micay, López de Micay
Slug: ametrallamientos-militares-contra-poblacion-civil-desplazan-a-36-familias-en-lopez-de-micay
Status: published

###### [Foto: Periódico Virtual ] 

###### [23 Mayo 2016 ]

10 familias afrocolombianas continúan desplazadas en el casco urbano de López de Micay, Cauca, por cuenta del [**ametrallamiento indiscriminado propinado por tropas del Ejército Nacional** el pasado viernes. Según la comunidad de San Isidro, tras sobrevuelos cuatro helicópteros arribaron a la zona, de ellos desembarcaron militares que dispararon contra el caserío. Finalizado el ataque, se escucharon dos explosiones.    ]

De acuerdo con el sacerdote Kilian Cuero, inicialmente 36 familias se desplazaron a la cabecera municipal de López de Micay, otras tuvieron que quedarse en zona rural porque no se contaban con las embarcaciones suficientes. Sin embargo, dadas las **condiciones inhumanas que vivieron en el casco urbano y aún sin un protocolo de retorno**, 26 de las familias decidieron regresar a sus territorios, pese a la presencia militar que en ellos persiste.

"Lo más preocupante para el pueblo, es que no entiende porqué los ametrallamientos, tantos hombres desembarcados en las montañas cercanas al pueblo sí en ese lugar no hay presencia guerrillera, esa es la gran preocupación de la gente, que **no entiende porque el Ejército en este cese al fuego ametralla", y además no se pronuncia**, asegura el sacerdote.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
