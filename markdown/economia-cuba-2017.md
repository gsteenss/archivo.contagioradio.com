Title: Salud, educación y asistencia social prioridades en Cuba
Date: 2016-12-28 14:35
Category: DDHH, El mundo
Tags: bloqueo, Cuba, economia, Raul Castro
Slug: economia-cuba-2017
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/RAÚL-CASTRO-RECONOCE-QUE-CRISIS-EN-VENEZUELA-AFECTÓ-A-CUBA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Poder y Crítica 

##### 28 Dic 2016 

A pesar de las dificultades financieras por las que ha atravesado la isla de Cuba durante el año que termina, **el gobierno de Raúl Castro ve con optimismo los nuevos retos que en materia económica vienen para 2017**, asegurando que se c**ontinuaran garantizando los servicios sociales que se brindan a la población**.

En sus palabras durante la clausura de la última sesión de la Asamblea nacional, el mandatario aseguró que **es necesario implementar algunas medidas para potencializar el crecimiento de la economía**, como son garantizar las exportaciones y su cobro oportuno, incrementar la producción nacional para sustituir importaciones, reducir los gastos no imprescindibles y usar racional y eficientemente los recursos disponibles.

Durante la jornada, el parlamento aprobó la Ley de presupuesto de Estado para el año que viene, estableciendo la destinación del **51% de los gastos corrientes a garantizar la salud, la educación y la asistencia social de los cubanos y cubanas**. Castro hizo un llamado a dinamizar la inversión extranjera en Cuba asegurando que "no vamos hacia el capitalismo, pero no hay que temer ni poner trabas a lo que podemos hacer en el marco de las leyes vigentes".

El 2016 no ha sido un año fácil para la economía cubana. Además del bloqueo económico de EEUU que impide  realizar operaciones internacionales con dólares estadounidenses y los daños ocasionados por cuenta del Huracán Matew; **la isla ha visto disminuido su PIB en 0.9%** por las limitaciones en el suministro de combustible y financieras. Le puede interesar: [Este es el impedimento para que se levante el bloqueo económico a Cuba](https://archivo.contagioradio.com/lobby-republicano-ha-impedido-que-se-levante-el-bloque-economico-a-cuba/).

Pese a esto, el mandatario destacó que **se han cumplido las obligaciones puntualmente con la deuda externa**, sin embargo el atraso de los pagos corrientes a los proveedores se mantiene por lo cual agradeció a los socios comerciales "su comprensión y su confianza en Cuba". Castro vaticinó un crecimiento en el PIB de al menos 2% para 2017, lo que mejoraría sustancialmente la economía local.
