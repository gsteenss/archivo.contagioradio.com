Title: Las aves también son víctimas del cambio climático
Date: 2015-10-04 15:53
Category: Ambiente, Voces de la Tierra
Tags: cambio climatico, Día de las aves, Ecologistas en Acción, Proaves
Slug: las-aves-tambien-son-victimas-del-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Torito-Cabecirrojo-Red-headed-Barbet-Eubucco-bourcierii.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### www.ubarroi.com 

<iframe src="http://www.ivoox.com/player_ek_8755711_2_1.html?data=mZyil5yVdY6ZmKiak5WJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiIa3lIqupsaPjc%2Foxtfbw8jNs8%2FVzZDRx5DQs9SfotPWz8bQqdShhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejandro Grajales, ProAves] 

###### [02 oct 2015] 

El 3 y 4 de octubre se celebrará **el Día de las Aves,** con el fin de dar a conocer la importancia de la biodiversidad referida a las especies de aves en Colombia[**, y así mismo para evidenciar la responsabilidad que tienen los países y las personas en la conservación y protección de las diferentes especies.**]

De acuerdo a la Fundación ProAves, al 2011, se han encontrado 1.889 especies de aves en Colombia, lo que significa que es la nación con la mayor biodiversidad en ese tipo de especies.

Las sequías y  las olas de calor ocasionan **“la  alteración de procesos naturales como la migración o la floración”,** según afirman la organización Ecologistas en Acción.

**La aves migratorias  son la más afectadas** ante el cambio climático, actualmente número de migraciones de estos animales ha disminuido debido a que las altas temperaturas generan que las aves anticipen su migración lo que "provoca que realicen la reproducción cuando la disponibilidad de alimento es escasa, lo que pone en riesgo el éxito de la época de cría”

Según el estudio **“efectos del cambio climático sobre la distribución de especies, estructura de la comunidad y la conservación de las aves en áreas protegidas de Colombia” **las aves amenazadas y de rango restringido en Colombia, podrían perder en promedio entre el 33 y el 43% de su área de distribución total en el futuro

Por otra parte, una de las especies en peligro de extinción en Colombia debido a los efectos del cambio climático es **el Arrierito Antioqueño (Lipaugus weberi, EN),** que se encuentra en las laderas septentrionales de la cordillera central con un núcleo de población superviviente ubicado en la Reserva Natural de las Aves Arrierito Antioqueño “indica Proaves”.
