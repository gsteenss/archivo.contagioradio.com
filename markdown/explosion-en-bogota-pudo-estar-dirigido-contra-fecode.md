Title: Explosión en Bogotá pudo estar dirigida contra FECODE
Date: 2017-05-19 13:29
Category: Educación, Nacional
Tags: explosión, fecode, Movilizaciones, Paro de maestros
Slug: explosion-en-bogota-pudo-estar-dirigido-contra-fecode
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/sede-de-fecode-e1495218461649.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La2orillas] 

###### [19 mayo 2017] 

Luego de que anoche estallara un petardo en una de las sedes de Fecode en la calle 35 con carrera 15 en el centro de Bogotá, los dirigentes de esta agremiación, manifestaron su **preocupación ante las faltas de garantías que tienen para continuar con la movilización.**

Over Dorado, integrante del Comité Ejecutivo de Fecode, manifestó que “**el país vive una situación muy complicada”.** Los dirigentes de la agremiación de educadores, los promotores del paro en Buenaventura, defensores de derechos Humanos y reclamantes de tierras han sido víctimas de constantes amenazas, que han perdurado durante las jornadas de movilización. Le puede interesar: ["Déficit en educación es de 600 mil millones de pesos: Fecode"](https://archivo.contagioradio.com/deficit-en-educacion-es-de-600-mil-millones-de-pesos-fecode/)

Para las directivas de Fecode, “no hay justificación para que se ponga un petardo en una casa vacía”. Sin embargo, concuerdan con que los hechos son una amenaza directa contra la agremiación que ha intentado por la vía de la **movilización pacífica llamar la atención del gobierno** para que cumpla las demandas del sector de la educación.

**NO HAY ACUERDO CON EL GOBIERNO**

Después de haber reanudado los diálogos con el gobierno, Dorado afirmó que **“no ha sido posible llegar a un acuerdo porque el gobierno continúa manifestando que no hay plata”.**Over Dorado aseguró también que, “el gobierno se ha centrado únicamente en decir que los maestros quieren un aumento salarial sin tener en cuenta las solicitudes de fondo que hacemos los educadores”.

Entre las solicitudes de los educadores está que se implementen medidas para lograr una cobertura del 80% en educación pública que actualmente se encuentra **en el 56% en todo el país.** Exigen además que se solucione la situación los niños y niñas que están por fuera del sistema educativo del país y que se dé la plena garantía de financiar la educación.

Para Fecode, el gobierno sólo hace énfasis en la política ante el mundo de que Colombia sea la más educada. Sin embargo, a nivel nacional no se ha esclarecido la forma como se va a financiar la educación pública para lograr atacar el **déficit presupuestal de 73 billones de pesos.** Le puede interesar: ["Este es el comunicado sobre pre acuerdo con el gobierno nacional"](https://archivo.contagioradio.com/este-es-el-comunicado-de-fecode-sobre-preacuerdo-con-gobierno-nacional/)

Según Dorado, “hay una desigualdad social, falta cobertura de la educación pública, la jornada única no cumple las exigencias de la sociedad y el déficit del sector educativo va a seguir aumentando”. Para Fecode, si este panorama no cambia, **“va a ser imposible que Colombia sea la más educada”.**

<iframe id="audio_18791393" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18791393_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
