Title: La ciencia detrás del cambio climático
Date: 2019-07-01 12:32
Author: CtgAdm
Category: Voces de la Tierra
Tags: Ambiente, Calentamiento global, cambio climatico
Slug: la-ciencia-detras-del-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/7-formas-en-las-que-el-cambio-climatico-puede-matarte.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

Desde hace un tiempo se viene debatiendo con mayor frecuencia sobre los impactos que el cambio climático viene provocando sobre la tierra por la acción humana, pero ¿**Cuáles son los mitos y las realidades entorno a estas alteraciones al clima desde el punto de vista científico**?

Para dilucidar algunas de estas inquietudes, en Voces de la tierra nos acompañaron **Viviana Bohorquez**, ingeniera ambiental y experta en políticas públicas de cambio climático, **Laura Morales** ecóloga especialista en gestión ambiental urbana y **Carlos Felipe Torres** consultor agropecuario en Cambio climático.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F382341612376066%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información ambiental en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en Contagio Radio 
