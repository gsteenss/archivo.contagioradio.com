Title: México: Caravana de migrantes continúa su peligrosa travesía hacia la frontera con EE.UU.
Date: 2018-11-07 16:16
Author: AdminContagio
Category: El mundo
Tags: Caravana Migrante, Frontera con Estados Unidos, mexico
Slug: caravana-migrantes-continua-peligrosa-travesia-hacia-frontera-eeuu
Status: published

###### Foto: Puebla en Línea 

###### 3 Nov 2018 

Anualmente ingresan a México entre **400.000 y 500.000 personas**, cifra que sin duda se incrementará al finalizar 2018, con el ingreso de cinco caravanas provenientes de Centroamérica integradas por más de **13.000 personas** que buscan llegar a la frontera con Estados Unidos, un trayecto de 3169 kilómetros plagado de peligros e incertidumbre.

Con la llegada de la primera caravana a México, el Gobierno acordó alojarla en el estado de **Chiapas al sur del país**, sin embargo **Mariana Zaragoza**, integrante del equipo de trabajo de Asuntos Migratorios de la Universidad Iberoamericana, es enfática en afirmar que los migrantes no están interesados en quedarse en una zona donde “no hay condiciones de vida digna”, razón por la cual han llegado a la capital a reanudar su éxodo hacia la frontera.

La forma más corta de llegar a su destino es a través del **estado de Tamaulipas**, que a pesar de ser la que menos distancia abarca, no está exenta de peligros, es por eso que durante la estadía de la caravana migrante en la capital del país azteca, Saragoza y su equipo realizan una labor de concientización para que los inmigrantes y sus líderes conozcan lo que significa llegar al norte.

**“Nada más basta con  acercarte un poquito a las personas y darte cuenta que es gente muy humilde que realmente cree que llegando  al norte va a mejorar su calidad de vida y tendrá una vida digna”** afirma Zaragoza refiriéndose a los inmigrantes, provenientes de diversos países como Honduras y Guatemala decidieron abandonar sus países de origen y dejar atrás la precaria calidad de vida en búsqueda de nuevas oportunidades.

Además de los largos recorridos bajo el sol, que ponen en peligro la salud de mujeres y niños, conforme los grupos se hacen más pequeños y se dispersan, su vulnerabilidad aumenta en una ruta con **“tramos controlados por el narco y el crimen organizado;** todos sabemos que hay redes de tráfico y trata de personas, sabemos la violencia que se vive por parte de las autoridades y los policías federales” indica Zaragoza.
