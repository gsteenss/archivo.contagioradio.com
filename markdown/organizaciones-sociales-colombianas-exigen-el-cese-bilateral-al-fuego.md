Title: Organizaciones sociales Colombianas exigen el Cese Bilateral al Fuego
Date: 2015-05-22 15:33
Category: Nacional, Paz
Tags: CCEEU, Clamor Social por la Paz, Comunicado: Diálogos con cese bilateral al fuego, Conpaz, DIPAZ, Plataforma Colombiana de Derechos Humanos Democracia y Desarrollo
Slug: organizaciones-sociales-colombianas-exigen-el-cese-bilateral-al-fuego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/fo1-mundo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:LaportadaCanada.com 

###### **Entrevista con [Aura Rodríguez], miembro de la Plataforma Colombiana de Derechos Humanos Democracia y Desarrollo:** 

<iframe src="http://www.ivoox.com/player_ek_4535460_2_1.html?data=lZqgl5madI6ZmKiakpmJd6KplZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYo9rWpYzG0Mnfy8zZqdufjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Después de los últimos sucesos de guerra entre el Gobierno y las FARC-EP, y de que la guerrilla suspendiera el Cese Unilateral al Fuego, **organizaciones sociales de DDHH de Colombia se han organizado para emitir un comunicado** instando a las partes a decretar un Cese Bilateral al Fuego con el **objetivo de parar el derramamiento de sangre y poder continuar con los Diálogos de Paz de La Habana sin hostilidades de por medio.**

**COMUNICADO: DIÁLOGOS CON CESE  BILATERAL AL FUEGO**

Las plataforma y movimientos nacionales abajo firmantes,  quieren manifestar su **profunda preocupación por los hechos de guerra que se vienen presentando entre el gobierno de Colombia y la guerrilla de las Farc-EP** en los que han perdido la vida 10 soldados y 26 guerrilleros en el departamento del Cauca, todos hijos de nuestro país, hechos que han avocado  la decisión del gobierno de reactivar los bombardeos  y de parte   de las Farc-Ep   la decisión de suspender el cese unilateral al fuego.

Si bien se ha **dialogado en medio de la confrontación armada**, el cese unilateral decretado por  las Farc-Ep  en diciembre pasado y la respuesta que en su momento diera el Gobierno nacional de cesar  los bombardeos por un mes prorrogable,  además  del **acuerdo de "desminado"**, han sido signos concretos de avances en el desescalamiento de la confrontación, con el efecto directo de confianza por parte de las comunidades mas afectadas por el conflicto armado interno.

Ante estos dos graves hechos de guerra, que afectan la esperanza de paz en nuestro país,  queremos insistir con vehemencia al **gobierno de Colombia y a la guerrilla de las Farc-EP, en nuestro reclamo de un cese bilateral al fuego**, lo que incidirá de modo directo  en la confianza en el proceso por parte de las comunidades y de las mujeres y hombres de buena voluntad de nuestro país.

No mas soldados ni guerrilleros muertos, no mas afectaciones a la población civil. Las acciones unilaterales o bilaterales que redunden en la protección de la vida y la integridad personal de combatientes y no combatientes es un signo de amor a la vida, **de respeto a Colombia, de aprecio a las comunidades indígenas, afrodescendientes, mestizas**, que habitan los territorios, de reconocimiento a los sectores de la sociedad que apuestan por un país que garantice el bienestar de todas y todos. No es un signo de debilidad.

Recordamos que **la sociedad que quiere la paz fue la que posibilitó la reelección del presidente Santos para su segundo mandato, contra los discursos que avivaban la violencia por parte del otro candidato** con altas posibilidades de ganar las presidenciales. Esas mayorías reclaman la generosidad de las partes. El cese bilateral al fuego será una justa respuesta a ese llamado a la paz que masivamente se ratificó el pasado 9 de abril en las calles de las principales ciudades de Colombia.

De **no avanzarse hacia un cese bilateral, cada muerte evitable seguirá hiriendo la conciencia moral de la sociedad y seguirá sumando a la responsabilidad de quienes pudiendo desescalar la confrontación**, no tienen la voluntad de hacerlo.

Llamamos a los **medios de información a no incentivar los odios entre colombianos con el uso de un  lenguaje que expresa desprecio por los muertos de la guerrilla y exalta  los del ejercito nacional, ** cuando  todos  son colombianos y colombianas, cuyas vidas son invaluables y sus muertes se habrían podido  evitar  con el cese bilateral.

Con profunda preocupación,

Comunidades Construyendo Paz en los Territorios -**CONPAZ**- constituida por 130 organizaciones de base.

Coordinación Colombia Europa Estados Unidos – **CCEEU**-, constituida por 260 organizaciones defensoras de derechos humanos.

**Plataforma Colombiana de Derechos Humanos Democracia y Desarrollo**,  constituida por 76 organizaciones defensoras  defensoras de derechos humamos .

Dialogo Intereclesial por la Paz en Colombia -**DIPAZ**- constituida por  42 organizaciones nacionales y dialogantes internacionales.

**Clamor Social por la Paz** constituida por 50 organizaciones.

**Alianza de Organizaciones Sociales y Afines**, constituida por 165 organizaciones sociales.

Colombia, 22 de mayo de 2015
