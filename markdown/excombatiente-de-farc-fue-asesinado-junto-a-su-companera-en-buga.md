Title: Excombatiente de FARC fue asesinado junto a su compañera en Buga
Date: 2020-11-04 15:44
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: excombatiente, Norte de Santander, ocaña
Slug: excombatiente-de-farc-fue-asesinado-junto-a-su-companera-en-buga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Excombatientes-e1567187945860.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 4 de noviembre sobre las 10:28 de la mañana se **conoció el asesinato de Janier Córdoba, excombatiente de FARC junto a Katherine Álvarez** quién era su compañera sentimental, en Buga, Valle.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1324080443751358466","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1324080443751358466

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El hecho se registró en la carrera 15 con calle 22, barrio Las Palmas, ubicado en la ciudad de Buga, Valle del Cauca, en medio de una vía pública en donde el excombatiente Jainer Córdoba y Katherine Alvarez caminaban, allí fueron abordados por d**os hombres que se movilizaban en una moto, quienes realizaron múltiples disparos en contra de la pareja, causándole 2 heridas de gravedad,** las cuales posteriormente les causaron la muerte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Jainer Córdoba Paz tenía 38 años de edad,** y estaba certificado como ex combatiente FARC bajo la resolución 07 del año 2017, además era integrante del partido FARC y miembro de la Cooperativa de Firmantes del Acuerdo; por su parte de Katherine se conoce que tenía 30 años de edad. ([«Pese a que se ha avanzado, aún queda un largo camino de compromiso con la paz»: Massieu](https://archivo.contagioradio.com/pese-a-que-se-ha-avanzado-aun-queda-un-largo-camino-de-compromiso-con-la-paz-massieu/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte el partido FARC, lamentó estos asesinatos , y r**esaltó que se dieron mientras los y las firmantes de paz de todo el país y regresaban a sus territorios** luego de la peregrinación Por la Vida y por la Paz. ([Así avanza la peregrinación por la Vida y por la Paz](https://archivo.contagioradio.com/peregrinacion-por-la-vida-y-por-la-paz-sigue-su-ruta-hacia-bogota/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y señalaron que a pesar de que la violencia continúa en contra de los y las excombatientes, y se refleja en la cifra de **[237 asesinatos de firmantes luego de la firma del acuerdo de paz](http://www.indepaz.org.co/wp-content/uploads/2020/07/3.-Informe-Especial-Asesinato-lideres-sociales-Nov2016-Jul2020-Indepaz-2.pdf)**, *"seguimos convencidos de la paz a pesar de estos mensajes de los señores de la guerra"*.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
