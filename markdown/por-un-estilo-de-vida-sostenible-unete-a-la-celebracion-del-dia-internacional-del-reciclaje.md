Title: Por un estilo de vida sostenible únete a la celebración del Día Internacional del Reciclaje
Date: 2015-05-14 13:17
Author: CtgAdm
Category: Ambiente, eventos
Tags: 17 de mayo, basura, Basura Cero, Basura Cero Colombia, Bogotá, Día Internacional del Reciclaje, reciclaje, Unesco
Slug: por-un-estilo-de-vida-sostenible-unete-a-la-celebracion-del-dia-internacional-del-reciclaje
Status: published

##### Foto: [bellezanaturaldelsumapaz.blogspot.com]

###### [15 de May 2015]<iframe src="http://www.ivoox.com/player_ek_4496573_2_1.html?data=lZmmmJqbd46ZmKiakp2Jd6KnmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNDmjNrbjcrXuMrg0JDRx5DarcXVjNjc1dnJssrWzcqYh6iXaaO1z8rhx5DFb83VjMjSzsrGtsLXyoqwlZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luisa Fernanda León, Basura Cero Colombia] 

**El próximo 17 de mayo, se celebra el Día Internacional del Reciclaje,** con el objetivo de recordar esta estrategia como una herramienta para proteger el ambiente. Es por eso que en Bogotá se realizan diversas actividades de parte de la organización Basura Cero Colombia, con el objetivo de que la ciudadanía se acerque cada vez más al reciclaje.

El Día Internacional del Reciclaje se trata de una celebración que tiene como fin reorientar el comportamiento de los ciudadanos y ciudadanas para cambiar los espacios urbanos, es por eso que la Organización de las Naciones Unidas para la Educación, la Ciencia y la Cultura  UNESCO, declara este día así, para que en cada país se desarrollen actividades sobre el reciclaje. Este año, por parte de Basura Cero Colombia se realizará **una reciclovia, un maratón del reciclaje  de 1000kg, actividades culturales, deportiva, ecojuegos y además habrá rifas obsequios y premios.**

Luisa Fernanda León, quien hace parte de Basura Cero Colombia, asegura que esta es una posibilidad para promover y fomentar estilos de vida sostenibles en torno a una **movilidad limpia y del reciclaje.**

De acuerdo a Basura Cero Colombia, **actualmente en la ciudad más del 70% de los residuos son orgánicos,  lo que quiere decir que el 30% restante es potencialmente reciclable,** pero de acuerdo a Luisa Fernanda, ese gran parte de ese porcentaje está llegando a los rellenos sanitarios al ser mezclado con el resto de la basura. “Por eso se debe separar desde la casa ya que eso genera impactos negativos en el territorio, cuando se trata de una apuesta por una ciudad sostenible”, León.

La integrante de Basura Cero Colombia, asegura que se ha avanzado en el tema de inclusión de la población recicladora, sin embargo, hay otros aspectos que hace falta fortalecer, como lo son el tema de pedagogía ambiental, que aun tomará años para que las personas decidan empezar a tener un estilo de vida sostenible.

Para mayor información sobre las actividades del Día Internacional del Reciclaje, se puede visitar la página de [Basura Cero](http://www.basuracerocolombia.com/proyecto/dia-mundial-del-reciclaje/) y en twitter [@[BogotaBasura0]{.u-linkComplex-target}](https://twitter.com/BogotaBasura0)
