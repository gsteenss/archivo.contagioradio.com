Title: Corte Constitucional tiene un papel decisivo en la refrendación de los acuerdo de paz
Date: 2016-06-24 15:34
Category: Entrevistas, Paz
Tags: Corte Constitucional, FARC, plebiscito por la paz, refrendación acuerdos de paz
Slug: refrendacion-de-acuerdos-de-paz-queda-en-manos-de-la-corte-constitucional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/corte_constitucional-Fuero_Penal_Militar_contagioradio-e1485870742527.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: cmi] 

###### [24 Jun 2016]

En el marco del anuncio que se hizo en la Habana sobre el cese al fuego bilateral y definitivo, las garantías de seguridad y la refrendación, hay un tema que está discutiéndose en el país y es la constitucionalidad o no de la **ley 156 de 2015 y del mecanismo de refrendación**. La clave está en el **carácter vinculante o no de la consulta** que se realice y de los aspectos que regule la propia Corte.

Para Rodolfo Arango, una de las muestras claras del acuerdo del cese bilateral el pasado 23 de Junio, es que la guerrilla de **las FARC acepta el orden jurídico colombiano** en “*un tema tan neurálgico como es la aprobación de lo que se ha discutido*” y **aceptan “ser leales” a lo que la Corte Constitucional decida.** Ese asunto es de vital importancia y muestra que hay una evolución y **una decisión de parte de esa guerrilla para apegarse a la decisión de la Corte.**

**Por otro lado, Arango plantea que la discusión clave estará en el significado del carácter vinculante y sus alcances**. Puede suceder que triunfe el *si*, pero depende de la Corte si el hecho de que sea vinculante lo define con rango constitucional o con rango de ley, o incluso simbólico, en últimas el [alcance vinculante depende de lo que la corte decida como alcances del *si* en el posible plebiscito](https://archivo.contagioradio.com/?s=plebiscito) y eso estaría ligado a las posibilidades de demanda si se incumplen los acuerdos de paz.

Por ejemplo, plantea el constitucionalista, si en 10 años las FARC, ya convertidos en partido político, ven que se están incumpliendo los acuerdos de paz, podrían demandar ante la [Corte Constitucional](https://archivo.contagioradio.com/cuatro-millones-quinientos-mil-deberan-votar-si-en-plebiscito-para-la-paz/) ese incumplimiento, pero esa posibilidad es la que está por definirse,  y por eso la importancia de lo que en este momento se debate “***el efecto del si tiene que administrarlo la Corte Constitucional***” explica.

La **otra posibilidad es que en el plebiscito triunfe el *no*** y, aunque muchos afirman que si triunfa esa posibilidad todos los acuerdos quedarían con letra muerta, para Arango los efectos no son tan “dramáticos”. Según el ex magistrado, lo que tendrían que hacer tanto el gobierno como las FARC es “repetir el año” y sentarse a modificar los acuerdos para que la gente diga que sí.

Arango concluye que los efectos de un *si* en el plebiscito tampoco son tan dramáticos para los opositores, porque los acuerdos tendrán que apegarse a reformas constitucionales que el congreso de turno tendrá que hacer, es decir, que en su momento se decidirá a modificar o no la constitución, “*tendrán su momento con otras mayorías para modificar o no la constitución*” eso sería vivir en democracia.

<iframe id="audio_12018731" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12018731_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
