Title: El trabajo y la defensa del territorio
Date: 2017-03-23 09:29
Category: Eleuterio, Opinion
Tags: Guatemala
Slug: el-trabajo-y-la-defensa-del-territorio-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/lalucha-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: arainfo 

#### **[Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)** 

###### 23 Mar 2017 

Afincado en España desde hace ya varios años, Rolando, originario de Guatemala, sigue desde siempre haciendo de altavoz para que las luchas centroamericanas se escuchen en Europa y puedan conjuntarse en los mismos reclamos. Es una labor difícil ya que la diferencia del contexto, la cultura y la represión en uno y otro lado son muy grandes. Con todo la lógica neoliberal actúa en todo el planeta si bien con diferentes fuerzas y estrategias, siempre con los mismos objetivos.

Desde que en 2014 se convocara la primera marcha de la Dignidad que congregó a millones de personas de todas partes del Estado en Madrid en lo que fue una jornada histórica de las clases populares de este país, el compañero Rolando ha seguido de cerca el desarrollo de las convocatorias posteriores. Se sumó a este movimiento como y tomó como propias sus reivindicaciones: Pan, Techo, Trabajo, Dignidad. Algo que reclama la gran mayoría de la clases trabajadora del país.

Sin embargo Rolando no puede dejar de mirar siempre hacia el otro lado del charco, allí a donde pertenece, donde están sus raíces y las de los suyos. No puedo evitar pensar que allí, las comunidades nunca demandarían trabajo. Allá las comunidades campesinas se arreglan para trabajar, lo que piden, fundamentalmente es respeto por el territorio. En otras palabras que les dejen en paz. Las mineras, las eléctricas,  las madereras, el monocultivo son las formas de la explotación, el despojo y en muchos casos la muerte. Ante la amenaza de la industria y la agroindustria en sus formas saqueo de los recursos de la tierra, la defensa del territorio es su principal preocupación. Si apuramos más es la defensa del agua, fuente de vida, pilar básico del ecosistema y de la vida humana, el mayor bien, el mayor tesoro, el que hay que defender.

La explotación continua y sin freno de los recursos de la tierra es en sí misma una amenaza al propio planeta. El resultado es la destrucción de grandes territorios naturales , la despoblación de las zonas rurales y campesinas y el aumento de los núcleos urbanos cada vez más masificados, contaminados y controlados. Cuando son expulsados de la tierra, la gente acude a las ciudades para buscar trabajo. El trabajo remunerado en la ciudad, desapegado del territorio y la comunidad, nos introduce en la rueda del consumo, en un estilo de vida individualista donde las metas no son comunes donde la competitividad se impone a la cooperación.

En la propia península Ibérica, cada vez más urbanista, se encuentra la llamada Serranía Celtibérica con una superficie de más de 63 mil km² y una densidad de población de 8 habitantes por  km². Es el territorio más despoblado y con menor número de habitantes por km² de toda Europa.

Resulta complicado comparar situaciones y contextos tan dispares de uno y otro lado del Atlántico, mucho más dar recetas a nadie. No obstante parece más necesario que nunca el que las miradas vayan más allá de nuestros horizontes y de que se tejan redes entre nosotros, como viene haciendo desde hace tiempo el compañero Rolando.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
