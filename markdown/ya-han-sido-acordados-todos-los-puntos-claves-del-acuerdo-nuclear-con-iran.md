Title: Ya han sido acordados todos los puntos claves del acuerdo nuclear con Iran
Date: 2015-04-01 21:19
Author: CtgAdm
Category: El mundo, Política
Tags: EEUU da un día mas para las negociaciones sobre Irán, Negociación acuerdo nuclear Irán
Slug: ya-han-sido-acordados-todos-los-puntos-claves-del-acuerdo-nuclear-con-iran
Status: published

###### Foto:Zoomnews.com 

Este miércoles las **seis potencias e Irán retomaron el diálogo sobre el acuerdo nuclear**. Encabezado por **Kerry  y el ministro de exteriores iraní, Mohamed Javad Zarif**, han dado un día más para concluir las negociaciones.

Debido a que la potencia nuclear no admite ningún pacto sin que se levanten totalmente las sanciones impuestas, la negociación final aún se va a demorar.

La canciller **Ángela Merkel** declaró al respecto que ha sido hecho  **"...un gran trecho del camino..."** y  que este acuerdo brinda una posibilidad para que se   "impida que Irán pueda hacerse con la bomba atómica".

Según **Zarif**, a pesar de que el plazo se ha finalizado ya, y se ha prolongado declaró el martes en la noche que** "...Tuvimos muy buenas conversaciones... Espero que podamos terminar el trabajo el miércoles y espero podamos comenzar con la redacción..."**.

Sin embargo para el ministro de asuntos exteriores **ruso Sergei Lavrov**  todos los **puntos clave ya han sido acordados, declaraciones que negó su homologo estadounidense**.

En la mesa de negociaciones ya está acordado que en 1**0 años el programa nuclear de Irán este a un año de trabajo de poder crear la bomba nuclear**, a cambio se levantarían todas las sanciones contra el país.

El principal escollo ahora mismo es el **calendario para levantar las sanciones** impuesto por la **ONU**. Es decir la fecha de inicio para que se finalicen las sanciones programadas por todos los estados en Naciones Unidas.

El primer ministro israelí **Benjamin Netanyahu**, rebajo el tono de sus declaraciones pero espetó a la comunidad internacional a "..**.Ahora es el momento de que la comunidad internacional insista en un mejor acuerdo...**"
