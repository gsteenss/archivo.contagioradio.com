Title: Kofi Annan: “la justicia no debe verse como un impedimento para la paz”
Date: 2015-02-25 18:45
Author: CtgAdm
Category: Nacional, Paz
Tags: colombia, Cuba, Derechos Humanos, justicia transicional, Koffi Annan, ONU, paz, proceso de paz, vícitmas
Slug: koffi-annan-la-justicia-no-debe-verse-como-un-impedimento-para-la-paz
Status: published

##### Foto: [www.rockefeller-news.com]

<iframe src="http://www.ivoox.com/player_ek_4134269_2_1.html?data=lZaglpeafY6ZmKiak5WJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMrnxNrf1dSPqMafrNTTy5Clss%2FVz5DS0JCns83jzsfWw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### **[Discurso Kofi Annan ]** 

Kofi Annan, exsecretario de la ONU y Nobel de Paz, llegó este lunes a Colombia para hablar sobre el proceso de paz, y conocer mejor los detalles de las conversaciones entre el gobierno de Santos la guerrilla de las FARC.

Este miércoles, el Nobel de Paz, participó en un foro sobre la importancia de las comisiones de paz organizado por su fundación, el centro internacional para la justicia transicional y la Revista Semana.

En el discurso, el ex-secretario de la ONU expreso lo que para él son tres pasos importantes en un  verdadero proceso  de paz, entre ellos se encuentra; la confianza entre las partes que discuten, un dialogo franco e incluyente y un tercer punto que en el que enfatizo y  es las voces de las víctimas. “**A las víctimas les debemos el derecho a que se sepa la verdad, por dura que sea, y garantizarles que no se va a repetir".**

Respecto al tema de la justicia, expreso que la reconciliación es un proceso largo, pero **la justicia no debe verse como un impedimento para la paz,** al contrario, debe ser un socio, a lo que agregó que, “los acuerdos de paz no pueden aceptar la impunidad”.

Por otra parte, **Annan aborto el tema de las  comisiones de la verdad y la ampliación que han tenido luego del conocido informe argentino "Nunca Más", sobre éstas comisiones destaca cuatro elementos que han aplicado comisiones exitosas en el mundo;** Claridad, por qué se va a establecer una comisión de la verdad; Tener como prioridad las realidades locales; tener claro que con esta herramienta no se va a reconciliar una sociedad y como cuarto punto un buen liderazgo que conduzca a una camino de verdad.

Así mismo, afirmó que "**las sociedades completas se basan en la paz y la seguridad, el desarrollo incluyente y el respeto de los derechos humanos**", es por ello, que en el marco de la defensa de los derechos de las víctimas, en primera instancia, es crucial que las voces de las víctimas sean escuchadas, y por otro lado, en la búsqueda de la verdad no se puede tratar de responsabilidades individuales, sino también del Estado y la sociedad en general.

Cabe recordar que Annan estará hasta el jueves en Colombia y luego viajará a La Habana, donde tiene previstos encuentros con autoridades de Cuba.
