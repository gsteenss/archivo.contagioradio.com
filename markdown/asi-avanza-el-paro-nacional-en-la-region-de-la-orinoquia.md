Title: Así avanza el paro nacional en la Región de la Orinoquía
Date: 2016-05-30 00:08
Category: Paro Nacional
Tags: Paro Nacional, paro nacional orionoquía colombiana
Slug: asi-avanza-el-paro-nacional-en-la-region-de-la-orinoquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Paro-Yopal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotos: Trochando Sin Fronteras ] 

###### [30 May 2016]

### Meta

**Miercoles 1ro de Junio **  
En puerto Gaitan se rompieron las negociaciones, arremetió el ejército y el ESMAD, hay niños indígenas sikuanes.

La comunidad movilizada fue enfática en señalar que la Minga es nacional y la negociación es nacional y luego de ese dialogo nacional si se pueden instalar mesas regionales que entre tanto se mantienen en Minga, de presentarse agresión por parte del ESMAD, los indígenas se defenderán con sus arcos y flechas, continua cerrada la vía al campo petrolero de rubiales.

**Martes 31 de Mayo**  
Más de 2000 indígenas sikuani de los 9 resguardos indígenas de Puerto Gaitán, y el resguardo Unuma Vichada, hacen presencia declarando su apoyo a la Minga nacional convocado por la ONIC. Y declarando la necesidad de una mesa de dialogo local que permita tratar temas puntuales de las comunidades indígenas de esta región. Se espera el arribo de otros resguardos en el transcurso de la manifestación.

![Puerto Gaitán](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/fc679c6c-b2de-49f3-b994-3a64bd1351f1.jpg){.aligncenter .size-full .wp-image-24649 width="600" height="800"}

### Vichada 

**Miércoles 1ro de Junio**

Vías principales de acceso al Departamento se encuentran cerradas por las manifestaciones.

**Martes 31 de Mayo**

Sobre el medio día hay amenazas de desalojo a las en el parque principal de la Ciudad de Puerto Carreño, departamento del Vichada por parte de la Policía a los Movilizados.

### Arauca 

**Jueves 2 de Junio**

La olla comunitaria es fundamental, plátano, yuca, carne, pescado, en zonas urbanas y en las carreteras. Se han realizado actividades culturales, en Casanare  y Charte hay cierres temporales de la vía con muestras culturales de la zona.

El transporte público y particular, actividades laborales, de educación y comercio permanecen suspendidas, esperando que se instalen mesas de conversación en Bogotá.

El ejercito reparte panfletos en contra del paro incitando a la población civil para que no se unan a las movilizaciones y se imparten amenazas de desalojos o de judicialización a las personas que se encuentran participando en los puntos de concentración.

**Miércoles 1ro de Junio**

En reunión del Viceministro del interior con alcaldes municipales, se concluyó que la protestas es pacífica y se comprometieron a respetar las movilizaciones siempre y cuando se realicen de acuerdo con los estándares de la fuerza pública es decir "que la gente no haga nada", y que el ESMAD permanecerá pendiente de mantener el orden público.

En horas de la mañana el ESMAD arribó al municipio de Tame acantonándose en el batallón Navas Pardo.

Cerrado el paso a campos petroleros en Arauca: Caño Limón y Caricare.  
**Martes 31 de Mayo**

En horas de la noche llegaron por vía aérea contingentes del ESMAD al municipio de Saravena y se acantonaron en el batallón Reveiz Pizarro.

En el municipio de Tame la fuerza pública ha decomisado información acerca de la Minga y la policía está bloqueando el paso de vehículos hacia Saravena.

En la inspección de Panamá de Arauca, municipio de Arauquita, la Policía Nacional impidió el paso a campesinos por un lapso aproximado de 2 horas, manifestando que la ley les daba hasta 72 horas para verificar la documentación de los carros en los que se transportaban.

Los campesinos que se trasladaban desde el municipio de Fortul hacia el municipio de Tame, han tenido que soportar en tramos cortos de la vía, reiterados retenes en los que los bajan, requisan y verifican antecedentes.

### Casanare 

**Miércoles 1 de Junio**  
Cerrada la vía Yopal-Aguazul  
**Martes 31 de Mayo**

En el sitio denominado 'El Charte', desde esta mañana hace presencia la Policía Nacional, algunos agentes de civil y uniformados han tratado de intimidar al dueño del predio incitándolo a deslegitimar la justa movilización y a que denuncie una “ocupación ilegal” de su propiedad por parte de los manifestantes.

**Lunes 30 de Mayo **  
Desde tempranas horas de este lunes, comunidades campesinas e indígenas de Casanare se movilizan sobre la vía Yopal - Villavicencio. En Puerto Gaitán desde las 11 de la mañana se concentraron líderes de resguardos indígenas, más de 2.500 se movilizan respondiendo al llamado de la Minga Nacional y sumándose al pliego de exigencias de la ONIC y de la Cumbre Agraria.  
[![Paro Yopal.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Paro-Yopal..jpg){.aligncenter .size-full .wp-image-24638 width="960" height="540"}](https://archivo.contagioradio.com/asi-avanza-el-paro-nacional-en-la-region-de-la-orinoquia/paro-yopal-2/)

<div class="_3x-2">

</div>

### 
