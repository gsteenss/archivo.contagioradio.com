Title: En Colombia se encarcela el pensamiento crítico
Date: 2020-01-15 20:30
Author: AdminContagio
Category: Nacional
Tags: pensamiento crítico, Universidad Nacional, Universidad Pedagógica Nacional
Slug: pensamiento-critico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Captura-de-pantalla-2020-01-15-a-las-20.27.22.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Miguel Ángel Beltrán, Cristian David Leiva y Jorge Enrique Freytter narran desde sus propias experiencias las formas en las que el Estado ataca al pensamiento crítico en Colombia.

<!-- /wp:paragraph -->
