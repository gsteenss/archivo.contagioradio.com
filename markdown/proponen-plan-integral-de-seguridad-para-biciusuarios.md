Title: Proponen plan integral de seguridad para biciusuarios
Date: 2017-09-25 13:21
Category: Ambiente, Nacional
Tags: Bicicleta, Biciusuarios, Bogotá, robos de bicicletas
Slug: proponen-plan-integral-de-seguridad-para-biciusuarios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/bicicleta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twenergy] 

###### [25 Sept. 2017] 

En la última semana **las cifras que se manejan de los biciusuarios fallecidos son lamentables, cinco personas han perdido la vida**, entre ellos un niño de 10 años en la Localidad de Bosa, en Bogotá y en lo corrido del año, el resultado es de 45 personas muertas en accidentes ocurridos en las calles de la ciudad. Situaciones que, para el Concejal Antonio Sanguino es muestra de una serie de factores que incrementan la inseguridad de los biciusuarios.

“Desafortunadamente **la administración va detrás de los acontecimientos y los problemas en relación con una agenda para el uso de la bicicleta** en las ciudades. Tenemos serios problemas en la habilitación de ciclorutas, en los cruces semafóricos, en el estado de la malla vial de las ciclorutas y de seguridad”

Al día **en la capital se realizan 150 mil viajes** cifra que con el pasar de los días continúa incrementándose, biciusuarios que además deben verse enfrentados a la agresividad de los otros modos de transporte. Le puede interesar: [Ante intentos de frenar la revocatoria de Peñalosa los bogotanos debemos salir a la calle: Carrillo](https://archivo.contagioradio.com/ante-intentos-de-frenar-la-revocatoria-de-penalosa-los-bogotanos-debemos-salir-a-la-calle-carillo/)

### **Se requiere un plan de seguridad integral para biciusuarios** 

Para el Concejal es importante crear un programa de seguridad vial que incluya:

1.  La adecuación de la red semafórica para que de esta manera no deban competir con los peatones o las motos cuando se van a hacer los cruces viables.
2.  Señalizar las rutas, iluminarlas con alumbrado público adecuado para evitar se produzcan robos de las bicicletas, así como actos de violencia,
3.  Acompañamiento de presencia de la Fuerza Pública a lo largo y ancho de la ciudad.

**“Debe haber un mayor esfuerzo de pedagogía ciudadana,** de tal suerte que la ciudadanía en general valore y respete al biciusuario y que también el biciusuario respete las normas de tránsito, de seguridad y al peatón”.

### **¿Cuál es la propuesta para minimizar los robos de bicicletas?** 

No sería suficiente denunciar el robo porque según Sanguino **el problema inicia con el débil sistema de registro que se tiene de las bicicletas** y sus propietarios y de controles al mercado ilegal de la bicicleta y de las partes.

“Se requiere una acción muy decidida de las autoridades sobre bandas especializadas y dedicadas al robo, comercio ilegal de las bicicletas y de sus partes. Hay que hacer un esfuerzo desde el registro, convirtiéndolo en algo similar a lo que se hace con los vehículos y las motos”.

De igual modo, **se deben establecer mecanismos de control de seguridad mucho más fuertes, concluye el Concejal. **Le puede interesar: ["Peñalosa no da la cara": Comunidades en el sur de Bogotá](https://archivo.contagioradio.com/penalosa-no-da-la-cara-comunidades-en-el-sur-de-bogota/)

<iframe id="audio_21080975" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21080975_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
