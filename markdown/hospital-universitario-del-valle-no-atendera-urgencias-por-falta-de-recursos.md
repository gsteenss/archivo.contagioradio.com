Title: Hospital Universitario del Valle no atenderá urgencias por falta de recursos
Date: 2015-12-11 16:56
Category: DDHH, Nacional
Tags: Crisis en urgencias Hospital Universitario del Valle, Crisis financiera Hospital Universitario del Valle, Crisis Hospital Universitario del Valle
Slug: hospital-universitario-del-valle-no-atendera-urgencias-por-falta-de-recursos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/HUV-Urgencias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El País 

<iframe src="http://www.ivoox.com/player_ek_9674960_2_1.html?data=mpuklp6adI6ZmKiakp2Jd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjNDn0c7hw9GPmc%2Fd18rf1c7YpdPd0JDRx9GPmsLgzcqY0NSPpdXZz8nS1Iqnd4a1kpDi1MzJssTdwtiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Marcela Urrea, U. del Valle ] 

###### [11 Dic 2015 ] 

[La Dirección Médica del Hospital Universitario del Valle y la Subgerencia de Atención Integral Quirúrgica y de Emergencia determinaron que esta institución **no recibirá más pacientes en sus salas de urgencias por insuficiencia de recursos**. Pese a los compromisos pactados por el Ministerio de Salud de solucionar la crisis financiera e impedir el cierre definitivo del Hospital, no ha presionado suficientemente a las EPS para cancelar los **\$60 mil millones** que le adeudan.]

[Marcela Urrea, Representante Estudiantil al Consejo Superior de la Universidad del Valle, asegura que para solventar la crisis financiera del HUV, se le ha exigido a los gobiernos nacional, departamental y local la inyección directa de recursos, pues **los \$2.500 millones girados al Hospital sólo lograron saldar algunos temas de funcionamiento para noviembre y diciembre**, “el HUV necesita cerca de \$20 mil millones mensuales para funcionar (…) las directivas han suspendido la atención de urgencias debido al colapso que enfrentan actualmente”.]

[“La situación en el Hospital no ha dejado de ser crítica (…) ni el Gobierno nacional, ni el departamental han ejercido la suficiente presión a las EPS para exigirles el pago efectivo de la gran deuda que tienen con el HUV”, afirma Urrea, e insiste en que las **condiciones de atención son precarias debido a la falta de insumos para la atención de procedimientos médicos**, el cierre de las salas de urgencias es una situación que alarma a toda la Red Pública Hospitalaria del Valle del Cauca, pues las festividades decembrinas incrementan su demanda.]

[De acuerdo con Urrea, pese a que en octubre el ministro de salud Alejandro Gaviria pactó un compromiso, esta semana **se excusó en que no había podido conseguir un tiquete de avión para asistir a una reunión convocada en Cali**, en la que junto a la electa gobernadora del Valle y algunos estudiantes y directivos del Hospital se haría veeduría a la cancelación de la deuda por parte de las EPS. En contraposición lo que plantea el ejecutivo es que se le haga un préstamo al HUV, lo que no es viable dada su actual situación financiera.   ]

[“Nos preocupa que el próximo año la **intención de la nueva gobernadora y del Gobierno nacional sea liquidar el Hospital**, como ha venido sucediendo con otros del departamento que han sido cerrados para siempre (…) nosotros lo que le estamos exigiendo al Gobierno nacional es que así como tiene la posibilidad de liquidar hospitales públicos que **tenga la valentía de exigirles a las EPS que paguen a los hospitales que son los que atienden a las personas más vulnerables**”, concluye Urrea.]
