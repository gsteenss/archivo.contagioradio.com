Title: Amenazas, asesinatos y desplazamientos a educadores en Colombia
Date: 2020-02-10 19:08
Author: CtgAdm
Category: DDHH, Nacional
Tags: Amenazas, Asesinatos, Educadores, fecode
Slug: amenazas-asesinatos-y-desplazamientos-a-educadores-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/fecode-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ImagendeArchivo

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/libardo-duenas-integrante-asedar-sobre-situacion-de_md_47664306_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Libardo Dueñas | Integrante de ASEDAR

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

La Federación Colombiana de Trabajadores de la Educación (Fecode) se declaró en alerta máxima luego de los hechos de violencia que se han registrado contra educadores en diferentes regiones del país, en los últimos meses; y la falta de garantías por parte del Gobierno para ejercer esta labor.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Nelson Alarcón presidente de la Federación *"son **270 maestros asesinados, desplazados y amenazados** en el último año, en medio de un Gobierno indolente ante los hechos violentos que vive el país"* . (Le puede interesar: <https://archivo.contagioradio.com/paran-340-mil-profesores/>).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Expresidente de Fecode víctima de atentado

<!-- /wp:heading -->

<!-- wp:paragraph -->

El 8 de febrero cerca de las 7:20pm **Carlos Enrique Rivas expresidente de Fecode y actual integrante ejecutivo del mismo, fue víctima de un atentado en la noche del sábado**, en el municipio del Guamo, Tolima.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la Federación, Rivas resultó ileso a pesar de que el vehículo de seguridad en el que se movilizaba recibió varios impacto de arma de fuego. (Le puede interesar: <https://archivo.contagioradio.com/fecode-rueda-prensa/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En 2019 Fecode presentó el informe **"La vida por educar"** ante la JEP y la Comisión de la Verdad **sobre los asesinatos y amenazas de 6.119 docentes entre 1986 y 2010 en el marco del conflicto armado***,* informe que según la Federación no ha generado ninguna reacción por parte del Gobierno.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/fecode/posts/10157418812814930:0","type":"rich","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-rich is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/fecode/posts/10157418812814930:0

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:heading {"level":3} -->

### Amenazas a educadores en el Salado

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una de las situaciones que también Fecode mencionó fue la que ocurre en el Carmen de Bolívar, específicamente en el corregimiento **El Salado, lugar golpeado desde hace 20 años por la violencia y donde actualmente los profesores son víctimas de amenazas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Los docentes de la Institución Educativa Técnica Agropecuaria de El Salado, se encargan de empoderan a los niños de la región en temas de paz y reconciliación, y hoy son víctimas de violencia y persecución por parte de actores armados"* , señaló el presidente de Fecode.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Profesora Sandra Mayerly Baquero

<!-- /wp:heading -->

<!-- wp:paragraph -->

Otro caso fue presentado el 7 de febrero por la **Asociación de Educadores de Arauca (Asedar), quienes denunciaron el asesinato de la educadora Sandra Baquero de 30 años**; *"hasta ahora sabemos que fue víctima de robo, y fue asesinada con arma blanca en su habitación, le robaron la moto y sus pertenencias"*, señaló Libardo Dueñas integrante de Asedar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho se registró en horas de la tarde luego de que la profesora cumpliera su jornada de trabajo en la institución educativa Santa Teresita en la ciudad de Arauca, en donde se desempeñaba como docente de educación primaria.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *" Sandra era dinámica, activa y cumplía con su que hacer en el aula de clase, además promovía el arte y la danza en la institución"*.
>
> <cite> Libardo Dueñas</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

De igual forma Dueñas rechazó los actos violentos que se han venido presentando en contra de los profesores en Arauca, *"los docentes de Arauca especialmente los que se desplazan diariamente a trabajar en zona rural han sido atracados, golpeados, expuestos a enfrentamientos de grupos armados, y desplazados incluso de su zona de trabajo por amenazas"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Le puede interesar: [Fecode rechaza la ola de atentados contra maestros en Colombia](https://www.youtube.com/watch?v=2mG2fetOZHA&feature=youtu.be&fbclid=IwAR0_hdsR9u9jqcq-P30G0oPcR7_YMvcJszX3oAYVpxYTEtiyuhybqrf2JbY))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Un maestro también es un líder social" Fecode

<!-- /wp:heading -->

<!-- wp:paragraph -->

Otro de los casos mencionados por Alarcón fueron en Soledad Atlántico donde 40 educadores fueron amenazados y en Tarazá, Antioquia, hay 12 desplazados, *"compañeros y compañeras viven la guerra, las amenazas y desplazamientos en Catatumbo, Tumaco, Chocó, Córdoba, Bolivar, Putumayo y Guajira. Viven a la espera de un Gobierno indolente que mira a otro lado y no a la problemática donde asesinan a profesores en todo el país".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente el Presidente de Fecode señaló que *"[**un maestro es un**]{}**líder natural, al que la comunidad consulta y escucha**, y como líderes deben ser protegidos y escuchados y no olvidados"*. Y agregó que ante el actuar del Gobierno realizarán un paro nacional por 48 horas el 20 y 21 de febrero, declamando seguridad y garantías en su ejercicio profesional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
