Title: Los riesgos para el ambiente en el posconflicto
Date: 2017-02-02 11:06
Category: Ambiente, Nacional
Tags: Ambiente, deforestación, FARC, Mineria, paz, posconflicto
Slug: los-riesgos-ambiente-posconflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/mineriaypaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo 

###### [2 Feb 2017] 

[**El aumento de la inversión extranjera,** es tal vez una de las principales ideas que vendió el presidente Juan Manuel Santos en torno a los beneficios del proceso de paz. Al tiempo es un punto que preocupa a las comunidades y ambientalistas, que pese a estar de acuerdo con la firma de la paz, no pueden negar, que temen que se salga de un conflicto armado para profundizar los varios conflictos socio-ambientales que existen en el país, e incluso dar pie a otros nuevos.]

[En 2015, Naciones Unidas, de la mano de Ingeominas, el Ideam, y el Instituto Geográfico Agustín Codazzi, **identificaron por medio de un mapa 125 municipios prioritarios** para los programas de posconflicto. Lugares que se vieron como un  reto para la implementación de los acuerdos frente a la protección ambiental, pero en relación a la reforma rural en el acuerdo final. No obstante, no se tuvo en cuenta, lo que para otros sectores es lo realmente preocupante: el modelo extractivista.]

[Para el gobierno, uno de los retos es que el ambiente deje de estar en medio de la confrontación armada y empiece a generar dividendos. El Ministro de Minas insiste en que la minería y el petróleo son los “motores” para financiar el posconflicto, pese a que el Ideam señaló que **en 2015 se perdió un total de 124.035 hectáreas de bosque**s, a causa, justamente, de la minería legal e ilegal, los cultivos ilícitos, la ganadería extensiva, la construcción de infraestructura vial y la extracción de madera Actividades que van en contra de la responsabilidad que tiene Colombia con el mundo al ser el segundo país más biodiverso del mundo.]

[**Aunque el 52% del territorio colombiano está conformado por áreas forestales**, los departamentos que más contribuyen con esa característica, son a su vez los más afectados por las actividades que generan la tala de árboles. En [Caquetá,](https://archivo.contagioradio.com/43-bloques-petroleros-en-caqueta-amenazan-la-amazonia-colombiana/)[Antioquia](https://archivo.contagioradio.com/ya-son-500-las-familias-desalojadas-por-hidroituango/),[Meta](https://archivo.contagioradio.com/audiencia-en-congreso-de-la-republica-por-accion-de-poligrow-en-mapiripan/), Guaviare y [Putumayo,](https://archivo.contagioradio.com/500-campesinos-se-movilizan-para-rechazar-operaciones-de-emerald-energy-en-caqueta/) se concentra el 60% de la deforestación del país. Lugares donde se vienen implementando toda una serie de proyectos petroleros, mineros, hidroeléctricos y de infraestructura que están afectando tanto los derechos de las comunidades, como a la naturaleza.]

### [La destrucción ambiental aumenta con el posconflicto] 

[Expertos aseguran que **luego de un proceso de paz suele aumentarse las causas que afecta el ambiente.** Manuel Rodríguez Becerra, director del Foro Nacional Ambiental explica que por ejemplo en El Salvador o el Congo, la degradación ambiental fue evidente ya que algunos excombatientes se dedicaron a la explotación ilegal, y por otro lado, varias multinacionales que antes no tenían acceso a ciertas áreas, “con la paz desarrollaron actividades que acabaron generando injustificados daños para el medioambiente”.]

[Y es que de acuerdo con el informe del Instituto Cinara de la Universidad del Valle, el país finalizó el 2016 con 115 conflictos socio-ambientales, afectando 12,4 millones de hectáreas. Cifras con las que se concluye que gran parte de la generación de ingresos se hizo con base en los proyectos minero-energéticos, sin contemplar los costos ambientales. Según el estudio, e**l 2015 se cerró con 72 proyectos y el último año, la cifra aumentó a 115,** con lo que se podría pensar que la salida de la guerrilla de los territorios y el afán de aumentar la inversión extranjera podría provocar la expansión de las fronteras extractivas.]

### [Los conflictos por regiones] 

[La investigación liderada por el profesor Mario Alejandro Pérez-Rincón, revela que la zona Andina se caracteriza por conflictos de origen minero, resaltando casos como el del Páramo de Santurbán y el de La Colosa. También hay conflictos en torno a la gestión del agua como con los proyectos de Hidroitango e Hidrosogamoso.]

[En la región Caribe los principales conflictos son generados por las actividades de extracción de energía fósil, como la mina de El Cerrejón y La Loma en la Jagua de Ibirico, además se resalta el desarrollo de infaestructura.]

[Hacia el Pacífico se identificaron 12 conflictos, cuatro asociados a minería, cuatro a biomasa destacándose la palma y la explotación maderera, y tres en infraestructura.  Finalmente en la Amazonía se presentan cinco conflictos, tres vinculados a la minería, uno a petróleo en el corredor Puerto Vega-Teteyé, y el otro, un conflicto internacional entre Colombia y Ecuador, sobre fumigación de cultivos de uso ilícito. [(Le puede interesar: la Comisión de la Verdad y el esclarecimiento de crímenes ambientales)](https://archivo.contagioradio.com/crimenes-ambientales-marco-del-conflicto-armado/)]

<iframe id="doc_57774" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/338227640/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-7KCjn5izXWCpzDusTfBN&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.75"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
