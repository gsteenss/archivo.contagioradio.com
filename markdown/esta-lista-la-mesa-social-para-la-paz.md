Title: Mesa social para la Paz, la nueva apuesta de la sociedad civil
Date: 2015-11-05 14:55
Category: Nacional, Paz
Tags: Angela Maria Robledo, dialogos de paz, Frente Amplio Nacional, Iván Cepeda, Mesa Social para la paz., Red CONPAZ, Redepaz
Slug: esta-lista-la-mesa-social-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/mesa-social-para-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Contagioradio 

###### [5 nov 2015] 

La Mesa Social Para la Paz es **una apuesta que recoge a organizaciones indígenas, campesinos, afros, blancos y toda la ciudadanía que quiera proponer y construir paz** en un escenario similar al que tienen las guerrillas con el gobierno nacional.

En el edificio Downtown se realizó el lanzamiento de la Mesa Social, con la participación de Luis Fernando Arias, Consejero Mayor de la ONIC; Iván Cepeda, Senador del Polo democrático; Ángela María Robledo Representante a la cámara, representantes del Congreso de los Pueblos y del Proceso de Comunidades Negras, en el evento se hizo  **un llamado a la sociedad a participar**  activamente en la construcción de paz y a unirse a esta iniciativa.

Luis Fernando Arias, recalcó: “**la voz de la sociedad colombiana tiene que ser fundamental"** y advirtió** ** que este es un camino ** **para avanzar y no volver atrás, Por otro parte se se refirió al traslado del ** Líder indígena Feliciano Valencia** al Centro de Armonización Indígena Gualanday, el cual consideró un paso importante pero que “no termina ahí”  ya que la finalidad es demostrar su inocencia.

La  mesa contará con la participación de líderes políticos y sociales que han acompañado el proceso de paz, pero **se espera que sean las organizaciones sociales y populares que encabecen la propuesta.**

Por parte de CONPAZ, estuvo María Eugenia Mosquera quién indicó que en el país: los indígenas, negros, campesinos y urbanos tienen un mismo fin en estos procesos de paz que es una **“paz con justicia social y ambiental”**, que solo se logrará con el compromiso de todos. Así mismo **cuestionó el papel del gobierno frente al paramilitarismo** e indicó que en departamentos como el Chocó el gobierno “ha permitido el ingreso” de estos grupos armados.

**Al finalizar la tarde la Mesa Social Para la Paz se trasladará al Centro de Memoria Histórica, Paz y Reconciliación **en donde establecerá una agenda programática con la ciudadanía que participe sobre los puntos a discutir sobre el proceso, además sobre el método de construcción de esta agenda y por último cuál va a ser la ruta de funcionamiento de esta mesa.
