Title: Dilma y Zuckerberg: la foto de la discordia
Date: 2015-04-20 16:58
Author: CtgAdm
Category: Enrique, Opinion
Tags: AccuWheather, Dilma Rouseff, Facebook, internet.org, Marck Zuckerberg, National Security Agency, Universal Hogares, Wikipedia
Slug: dilma-y-zuckerberg-la-foto-de-la-discordia
Status: published

###### Foto: forbes.com 

**Por [[Enrique Amestoy ](https://archivo.contagioradio.com/enrique-amestoy/)]**

*“Ciento veinte millones de niños en el centro de la tormenta”.* Así introduce Eduardo Galeano al ensayo que muchos llegaron a denominar como una biblia latinoamericana y que sin lugar a dudas ha marcado a más de tres generaciones. Aprovecho para emitir un gran insulto a la muerte por llevarse a uno de los referentes de la literatura reflexiva en mi país. 44 años después de publicado ***“Las venas abiertas de América Latina”*** me atrevo a parafrasear a Galeano para decir que son 1200 millones las personas en la India o 200 millones en Brasil o los más de 600 millones que poblamos nuestro continente, que estamos en el centro de la tormenta en este momento. Tormenta tecnológica, de la mano de grandes corporaciones con Facebook a la cabeza*.*

Sorpresa, preocupación, indignación y seguramente muchos adjetivos más, ha generado en muchos de nosotros haber visto, en el marco de la Cumbre de las Américas en Panamá, una foto de la presidenta brasilera Dilma Rouseff junto al fundador de Facebook, Marck Zuckerberg. Posan para presentar el posible acuerdo de llevar el proyecto [internet.org](invento de Zuckerberg) a Brasil. Proyecto que a grandes rasgos consiste en llevar una aplicación móvil a sus usuarios, en base a la alianza de empresas de telefonía con Facebook y otros aliados para dar “gratis” acceso a determinados sitios web, a poblaciones alejadas y con los niveles más altos de pobreza.

Actualmente en Brasil la compañía de Zuckerberg tiene una experiencia en Heliópolis, barrio muy pobre de San Pablo. Allí se anima a los residentes a usar la red social más grande del mundo para promover [negocios propios][](# "Click to Continue > by DeleteAd") (Facebook, las empresas de telefonía aliadas y los gobiernos con quienes hacen los acuerdos)

Comparto plenamente que se trabaje por la inclusión digital en el entendido de que es parte ineludible de la inclusión social. Llevar conectividad a los lugares más humildes es tarea y responsabilidad de los gobiernos. Ya hablamos en otras columnas sobre la inclusión digital en Uruguay que incluye fibra óptica a la casi totalidad de los hogares, el programa “Universal Hogares” que da acceso a la internet en forma gratuita a poblaciones de bajo poder adquisitivo o las 700.000 computadoras que el gobierno ha entregado a alumnos de enseñanza primaria y secundaria.

¿Por qué la indignación con la foto o poner el tema como algo muy peligroso? Porque hay condiciones que no son tan loables. Porque se trata de generar una “internet para pobres”, con contenidos limitados (se habla de acceso a los sitios que la red social determine en acuerdo con las empresas de telefonía con las que ya tiene acuerdos comerciales). Entonces lo que se pone en riesgo es la neutralidad de la red [1](#_edn1), ya se han levantado voces en contra y empresas que se han retirado del pacto [2](#_edn2){#_ednref2} [3](#_edn3){#_ednref3} Y en el caso concreto de Brasil se viola la Ley de Marco Civil de Internet [4](#_edn4){#_ednref4} donde, luego de muchos años de lucha de las organizaciones sociales, se consagran los derechos civiles y la neutralidad de la red.

No podemos permitir que las empresas de telefonia, dueñas en la mayoría de los países  de la conexión a la web, sean quienes determinen a que sitios (16 en la India, por ejemplo), a que velocidad o lo que es peor: a que sitios NO podemos acceder. Además de la ya sabida entrega de información a las agencias norteamericanas: recordemos la denuncia de Snowden donde se señala a Facebook como parte de PRISM[5](#_edn5){#_ednref5} ,  programa de espionaje de la National Security Agency (NSA), o la venta de nuestra actividad, gustos y costumbres a quienes luego nos intentarán seducir con publicidad, llamadas telefónicas o correos electrónicos.

Todos deberíamos saber que lo barato sale caro...

###### [1](#_ednref1)

###### [2](#_ednref2)

###### [3](#_ednref3)

###### [4](#_ednref4)

###### [5](#_ednref5)

 **[[@[eamestoy]**
