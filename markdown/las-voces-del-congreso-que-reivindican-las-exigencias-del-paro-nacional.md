Title: Las voces del Congreso que reivindican las exigencias del Paro Nacional
Date: 2019-11-29 18:09
Author: CtgAdm
Category: Nacional, Paro Nacional, Política
Tags: Cámara de Representantes, Congreso, Iván Cepeda, Iván Duque, Maria Jose PIzarro, paquetazo, Paro Nacional, Roy Barreras, Senado
Slug: las-voces-del-congreso-que-reivindican-las-exigencias-del-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Congreso-se-manifiesta-sobre-el-Paro-Nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio.] 

[El Paro Nacional, que ya completa más de una semana de manifestaciones, no le es indiferente a algunos parlamentarios, que desde el Congreso han sido grandes detractores del exceso de fuerza por parte del Escuadrón Móvil Antidisturbios (ESMAD), reivindicando muchas de las consignas exigidas por los ciudadanos en la calle.]

[Es por esto que el senador del Polo Democrático, Iván Cepeda, radicó una denuncia penal contra el comandante de la Policía Nacional, Óscar Atehortúa Duque, encargado del ESMAD, y otros funcionarios de la Policía Nacional, por lo que consideró “actos que atentan contra el derecho a la vida y la movilización pública pacífica por parte de los ciudadanos”.]

[Este uso excesivo de la fuerza, remite al caso del estudiante Dilan Cruz, asesinado el pasado 25 de noviembre por el disparo de un arma letal utilizada por los agentes del ESMAD, ha generado múltiples manifestaciones de rechazo en contra de las decisiones que el Gobierno de Iván Duque ha tomado en el marco del Paro Nacional.][(Le puede interesar: Las razones para pedir el desmonte del ESMAD a 20 años de su creación).](https://archivo.contagioradio.com/las-razones-para-pedir-el-desmonte-del-esmad-a-20-anos-de-su-creacion/)

### **Voces del Congreso advierten que la protesta no debe ser violentada por el ESMAD**

[El senador Cepeda ha defendido con ahínco el derecho constitucional de la protesta y la necesidad de ejercer un control político desde las calles. Esto se debe a que, “la movilización que ha tenido Colombia en los últimos ocho días es un acontecimiento de carácter político inédito. Esta movilización tiene sus raíces en el nuevo contexto democratizador que proviene del Proceso de Paz y de la influencia de otras propuestas que han surgido en el continente en contra del modelo neoliberal”, explica. ]

[En el caso del senador del Partido de la U, Roy Barreras, el gobierno ha actuado bien al dar “un primer paso” anunciando una conversación nacional. Sin embargo, para el congresista, los pasos siguientes “fueron en reversa”, ya que el presidente Duque no “ha respetado los puntos especificados por los líderes de la protesta social que los dirigentes de los movimientos y partidos políticos estamos exigiendo”. ]

[Para Barreras, lo más importante en este momento es dejar en claro los siguientes puntos: l]a implementación del Acuerdo de paz, en específico la promulgación de las 16 curules para las víctimas, exigir la identificación de los autores intelectuales de la mayoría de asesinatos de líderes sociales, la modificación de la Ley de Financiamiento y el compromiso con una agenda política y social.

### **El respaldo al diálogo desde el Congreso de la República** 

[Para la representante a la Cámara por la Lista de la Decencia,  María José Pizarro, el Paro Nacional ha demostrado que “los jóvenes son los protagonistas de la transformación que avanza en Colombia. La movilización ha logrado que la ciudadanía dialogue, se cuente en los barrios y discuta los problemas de la nación. Sin duda las cosas están cambiando para bien”.]

[Por su parte, la Representante a la Cámara del Partido Verde, Juanita Goebertus, resalta la importancia de “un diálogo serio” que permita “escuchar distintas voces, reconocer errores y tener disposición a corregir”, enfocándose en la importancia de las conversaciones entre el Gobierno y los máximos representantes del Paro Nacional. ]

[El apoyo al paro es respaldado también por los pueblos indígenas, muchos de los cuales son representados por el senador del partido político del MAIS, Feliciano Valencia, quien deja en claro que el “Paro Nacional Indefinido” debe continuar “hasta que se caigan todas las medidas presentadas en el Paquetazo de Duque”. [(Lea también: Holding financiero; la jugadita de Duque privatizando los dineros públicos)](https://archivo.contagioradio.com/holding-financiero-la-jugadita-de-duque-privatizando-los-dineros-publicos/)]

### **Bajar los sueldos de los congresistas** 

[El presidente del Congreso de la República, Lidio García, sorprendió con una declaración en medio del Paro Nacional, al proponer una reducción del 15% al salario de los congresistas y los altos funcionarios del Estado. Esta medida ya se había propuesto en la pasada Consulta Anticorrupción sin llegar a alcanzar el umbral de votos. ]

[Por su parte, la intermitente ola de noticias que llegan desde otros puntos de Latinoamérica comprueba que no es una preocupación local, en Chile las manifestaciones realizadas este año lograron que la Comisión de Constitución del Congreso de ese país aprobara la reducción del 50% del salario de los congresistas. ]

[Para el senador por el Partido Verde, Iván Marulanda “el pueblo colombiano se ha expresado en las calles, quieren que se reduzca el sueldo de los congresistas y hay que acatarlo”, resaltando la importancia del Paro Nacional al señalar que “la gente está cansada de luchar, de sufrir y la está pasando muy mal, tienen muchas dificultades para encontrar empleo, para educarse, para acceder a la salud, y el Estado colombiano no ha solucionado nada”. ]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45280650" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45280650_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
