Title: Ante incumplimientos del gobierno maestros irían a paro nuevamente
Date: 2018-02-05 16:32
Category: Educación, Nacional
Tags: fecode, Ministra de educación, Paro de maestros
Slug: gobierno_incumple_maestros_paro
Status: published

###### [Foto: Archivo] 

###### [5 Feb 2018] 

Nuevamente los incumplimientos del gobierno nacional a los maestros del país, promueven la idea de de un nuevo paro nacional. Así lo ha anunciado FECODE, que desde octubre del año pasado, denunciaban que "**no hay avances concretos" frente a los compromisos pactados con el gobierno Santos.**

"Los maestros de Colombia le cumplimos a la sociedad, pero el gobierno nos ha venido incumplimiendo, y ha desconocido los acuerdos", fueron las  declaraciones a la Fm, del presidente de FECODE, Carlos Enrique Rivas. **Asimismo, ha asegurado que tras los 7 meses de incumplimientos, este miércoles tendrán una reunión donde **se definirán las características del paro.

### **¿Qué no ha cumplido el gobierno?** 

De acuerdo con el presidente de FECODE, "el gobierno no le quiere meter plata al sistema general de participaciones, ha desconocido los acuerdos en materia de deudas, quiere negar los avances de la evaluación de carácter diagnóstico formativo, ha mantenido el tema de los maestros provisionales como cuotas burocráticas del clientelismo, y la ministra de educación no ha sacado los decretos de aumento salarial".

Ante dicha situación, el jueves habrá una reunión con Yaneth Giha, ministra de Educación, para analizar el nivel de cumplimiento y compromiso del gobierno con las exigencias de los maestros, **"vamos a decirle a la ministra que el paro depende de la respuesta del gobierno**", dice Rivas.

Manifiestan también que el gobierno no ha cumplido con la prestación del servicio de salud. Por ejemplo en Antioquia, aunque están los 22 mil millones de pesos necesario, actualmente no están atendido la salud de los maestros. Asimismo, más allá de las condiciones laborales de los maestros **lo que están exigiendo es una educación gratuita y de calidad para todos los niños y niñas.**

"Para el programa 'Ser pilo paga' son 360 mil millones de pesos para dar educación a 36 mil estudiantes, en su mayoría para universidades privadas. Mientras que son ese mismo dinero, podrían ingresar 600 mil estudiantes a la universidad pública", explica.

FECODE recuerda que el paro pasado fue legal y por eso el Estado se sentó a negociar con las y los profesores. Hoy, ante los incumplimientos, "**le decimos a la sociedad que nos apoye para que nos cumplan, si cumplen mantendremos en las aulas de clases".**

###### Reciba toda la información de Contagio Radio en [[su correo]
