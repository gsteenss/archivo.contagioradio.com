Title: La maternidad en la cárceles de Colombia una violación a la vida
Date: 2018-05-28 12:50
Category: Expreso Libertad
Tags: embarazo, FARC, Prisioneras políticas
Slug: maternidad-carceles-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/070417.n.dnt_.doula_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Ilustración: Gary Meader] 

###### [22 May 2018] 

Rocío Cuellar, ex prisionera de las FARC, estuvo recluida en la Cárcel "El Buen Pastor", uno de los principales centros carcelarios en Colombia creado con la finalidad de ser una prisión para mujeres. Sin embargo, en su paso por este centro de reclusión fue víctima de la negligencia del Estado y la administración de la penitenciaría, que puso en riesgo su vida con un embarazo de alto riesgo.

<iframe id="audio_26218935" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26218935_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
