Title: Docentes de Colombia se movilizarán este jueves 26 de febrero
Date: 2015-02-23 20:33
Author: CtgAdm
Category: DDHH, Nacional
Tags: ade, fecode, maestros, paro
Slug: docentes-de-colombia-se-movilizaran-este-jueves-26-de-febrero
Status: published

###### Foto: ADE 

##### <iframe src="http://www.ivoox.com/player_ek_4123781_2_1.html?data=lZaflZycdY6ZmKiakpqJd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLbytjhx9fNs4zq1srZ2MqPpYzgwtiYxcbQsMbnjMrZjc%2FZqdfZ1JCfmJDIqYzaxsffx9fTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [William Agudelo, Asociación Distrital de Educadores] 

La **Federación Colombiana de Trabajadores de la Educación, FECODE,** realizará una movilización nacional el próximo jueves 26 de febrero para radicar su pliego de peticiones ante el Ministerio de Educación.

Según lo expuesto por los y las maestras, el pliego contiene fundamentalmente los mismos puntos por los cuales salieron a paro en mayo del 2014, pero que no han sido atendidos por el MinEducación: **incremento y nivelación salarial, sistema de salud en régimen especial para las y los maestros, y evaluación por competencias.**

Al rededor de **320 mil maestros y maestras** saldrán a movilizarse, e iniciarían un cese de actividades en las próximas semanas si el gobierno no atiende las demandas del sector.

La movilización en Bogotá partirá del **Ministerio de Educación Nacional a las 9 de la mañana**, recorrerá la Calle 26 para tomar la Carrera 7ma hasta la Plaza de Bolívar.
