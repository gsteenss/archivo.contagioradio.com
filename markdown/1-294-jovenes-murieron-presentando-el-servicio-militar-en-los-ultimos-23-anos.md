Title: 1.294 jóvenes murieron prestando el servicio militar en los últimos 23 años
Date: 2016-05-11 17:13
Category: Nacional
Tags: Angélica Lozano, servicio militar obligatorio
Slug: 1-294-jovenes-murieron-presentando-el-servicio-militar-en-los-ultimos-23-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/libreta_militar_-e1463004534745.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Patria] 

###### [11 May 2016] 

Este miércoles en Comisión Segunda de Cámara de Representantes, se discutió el proyecto de Ley 154 de 2015, por medio del cual el gobierno impulsa la ampliación del servicio militar de 12 a 18 meses, pese a que el presidente Juan Manuel Santos se había comprometido con los jóvenes colombianos a acabar con el servicio militar obligatorio, teniendo en cuenta las dos negociaciones de paz que se adelantan con la guerrilla del ELN y la de las FARC.

Por su parte, la representante Angélica Lozano, continúa asegurando que es “publicidad engañosa” anunciar por diversos medios de comunicación y desde el mismo gobierno, que ya no se  necesita la libreta militar para trabajar, pues lo único que se logró fue dar un plazo de 18 meses para que un joven pague su libreta si cuenta con un trabajo.

Lozano, afirma que en julio del 2015, radicó un proyecto de Ley con el que se pretendía eliminar la libreta militar para trabajar, pero este “fue aplastado por las mayorías del gobierno y por instrucción del mismo”, y aunque logró  reunir 80 firmas de congresistas de cámara que apoyaran su iniciativa la presión del Ministerio de Defensa dejó archivado su proyecto.

“Lo justo en nuestro país es que la libreta militar sea como la cédula, es decir gratis… Proponemos un desmonte gradual del Servicio Militar Obligatario, acorde al momento que vive el país”, sostiene la congresista quien cree que las fuerzas militares colombianas deben estar compuestas por personas que tengan vocación militar.

Teniendo en cuenta que desde el gobierno se está impulsado este proyecto de Ley en el que además de ampliar el servicio militar, la libreta se volvería un requisito para obtener el pase de conducción y el pasaporte, en redes sociales se posicionó la tendencia \#NoServicioMilitar con la que jóvenes, políticos y organizaciones demostraron alarmantes cifras para evidenciar por qué no debe continuar en trámite esa propuesta del gobierno y por qué si, debe terminarse con el servicio militar obligatorio.

-   Si una persona es campesino o de estrato 0, 1 o 2 hay 42 veces más posibilidades de ser forzado a ir a la guerra, que alguien de estrato 4, 5 o 6.
-   Sino te graduaste tienes 8 veces más probabilidades de morir en el Ejército.
-   Si eres de estrato 0, 1 o 2, tienes 8 veces más posibilidades de que te lleven en un camión.
-   De 1993 al 2015 se contabilizan 1.294 jóvenes muertos prestando el servicio militar.
-   El 88% de quienes prestaron el servicio militar no continuaron con la carrera militar.
-   Actualmente hay 764.161 jóvenes remisos.
-   74% de los jóvenes que sufrieron daños físicos y /o mentales en el servicio militar fueron soldados regulares, el 11% eran bachilleres y el 15% campesinos.

<iframe src="http://co.ivoox.com/es/player_ej_11496212_2_1.html?data=kpahm5uWdZOhhpywj5aYaZS1kZeah5yncZOhhpywj5WRaZi3jpWah5yncaLiyMrZy8jFb63j28bb0ZCRb6Lgysbb3MaPmsbmxcqYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
