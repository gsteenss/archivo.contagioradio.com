Title: Cuerpos en prisión, mentes en acción
Date: 2018-10-22 11:24
Author: AdminContagio
Category: Expreso Libertad
Tags: carceles, lgtbi, trans
Slug: comunidad-trans-prision-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/tyt-101115.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Cuerpos en prisión mentes en acción 

###### 16 Oct 2018 

En este programa del **Expreso Libertad**, Katalina Ángel fundadora del proyecto **Cuerpos en Prisión Mentes en Acción**, compartió su experiencia al interior de la Cárcel La Picota y la necesidad de generar toda una red de apoyo a la comunidad Trans.

Además explicó los diferentes logros que ha tenido esta comunidad frente a la exigencia del respeto por sus derechos y los retos que aún faltan para brindar garantías a esta población.

<iframe id="audio_29514580" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29514580_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
