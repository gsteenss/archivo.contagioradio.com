Title: "Hay incertidumbre sobre lo que ocurrirá con la JEP": Camila Moreno
Date: 2018-06-18 12:58
Category: Nacional, Paz
Tags: acuerdo de paz, elecciones, JEP
Slug: %c2%a8hay-incertidumbre-sobre-lo-que-ocurrira-con-la-jep%c2%a8-camila-moreno
Status: published

###### [Foto: Contagio Radio] 

###### [18 Jun 2018] 

Las declaraciones del electo presidente, Iván Duque, sobre las modificaciones que se quieren hacer al acuerdo de la Habana, y en especial, respecto a la Jurisdicción Especial de Paz, en términos del componente de Justicia, las sanciones restaurativas y la conexidad del narcotráfico como delito político, han generado un ambiente de incertidumbre sobre lo que ocurrirá con la JEP según Camila Moreno, directora del Centro Internacional de Justicia Transicional.

### **Los Reparos Respecto a las Propuestas de Duque** 

Para Moreno, **Iván Duque puede representar un retraso en "las transformaciones que se veían en el horizonte del país"**, en tanto la reglamentación de la JEP en el congreso ha significado que la bancada del Centro Democrático, de la cual hizo parte Ivan Duque, formule limitaciones a la Justicia Especial en su capacidad de juzgar actores que tomaron parte del conflicto Colombiano.

Un ejemplo de ello es lo ocurrido con el caso Jesús Santrich, en el que se esta buscando que sea la justicia ordinaria y no la Jurisdicción de Paz la que determine si Santrich debe ser extraditado, omitiendo la reglamentación brindada por el acto legislativo 001 que faculta a la JEP para determinar el organismo que debe juzgar los delitos cometidos por integrantes de la FARC según la fecha en que se cometieron dichos delitos. (Le puede interesar: "[Quieren limitar los poderes de la JEP: Iván Cepeda](https://archivo.contagioradio.com/quieren-limitar-los-poderes-de-la-jep-ivan-cepeda/)")

### **La clave estará en el ejercicio de la oposición** 

De otra parte, la participación en el congreso de Gustavo Petro y Ángela Robledo gracias al estatuto de oposición, garantiza que haya una fuerza de defensa en favor del Acuerdo de Paz de 8 millones de personas. Y dado que las reformas que se quieran hacer al acuerdo de la Habana tendrán que pasar por el legislativo, y el control de la Corte Constitucional, Moreno señaló que **Duque tendrá que matizar sus posiciones, y en el término de un año no habrá cambios sustantivos al acuerdo.**

Finalmente, la directora del Centro Internacional de Justicia Transicional indicó, que pese a que se ha planteado el gobierno de Duque como una nueva era de la Seguridad Democrática aún no está claro eso qué significa en términos específicos, pues "el país no es el mismo de hace 8 años" , dependerá del gabinete que elija Iván Duque para gobernar y además tendrá que negociar con el congreso los cambios que desee hacer al Acuerdo de Paz.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="osd-sms-wrapper">

</div>
