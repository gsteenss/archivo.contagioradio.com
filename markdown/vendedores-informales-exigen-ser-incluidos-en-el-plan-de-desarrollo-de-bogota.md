Title: Vendedores informales exigen ser incluidos en el Plan de Desarrollo de Bogotá
Date: 2016-02-29 12:20
Category: Economía, Nacional
Tags: Alcadía de Bogotá, Enrique Peñalosa, Vendedores informales
Slug: vendedores-informales-exigen-ser-incluidos-en-el-plan-de-desarrollo-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/12769575_10153767048850020_581836288_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colectivo Radicales Libres 

##### <iframe src="http://co.ivoox.com/es/player_ek_10616323_2_1.html?data=kpWjk5uXdpShhpywj5aYaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncbfZz8nSxtTWqdSfytPT0dfRpc3Z1JDS2s7Lqc%2Bf1Mrfjc7Sp83pysnc1ZDJsozZzZC9zsbSb8Whhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Julio Espinosa] 

###### [29 Feb 2016] 

En la mañana de este lunes, **cerca de 40 mil vendedores informales de Bogotá decidieron salir a marchar desde diferentes puntos de la capital para llegar hasta la Plaza de Bolívar,** exigiendo soluciones y el respeto a su derecho al trabajo, frente a lo que han significado los últimos operativos realizados por la Alcaldía de Bogotá en los que a diario miles de personas que sostienen a sus familias de las ventas ambulantes, han sido desalojadas.

De acuerdo con Julio Espinosa, vocero de estas personas, la principal exigencia es que el **distrito incluya a los vendedores informales como un capítulo del Plan de Desarrollo** “con presupuesto, con un plan de trabajo y con unas soluciones verdaderas que dignifiquen al trabajador informal”, donde se vea el problema desde lo social y no como espacio público.

“No nos han incluido en el Plan de Desarrollo, **nos tienen como un estorbo social, es un acto de violencia institucional** cuando se niega la existencia de un actor social como los vendedores informales”, dice Espinosa, quien añade que por años se le ha solicitado al distrito realizar un censo de las personas que viven de esta actividad, pero este ha hecho caso omiso.

Según el vocero de los vendedores informales, una sentencia de la Corte Constitucional dice que los vendedores que trabajan en las calles no pueden ser desalojados mientras no existan soluciones puntuales para su situación, sin embargo, pese a que desde la Alcaldía se ha afirmado que si se han planteado opciones, para Julio Espinosa no son más que “soluciones a medias, mandándolos a un rincón de la ciudad donde no van a vender nada, el distrito paga cursos y cosas superficiales que no resuelven el problema de fondo”.

Los vendedores, entregarán su pliego de peticiones en horas de la mañana en la Alcaldía de Bogotá, expresando que lo que desean es prosperar a partir de un ejercicio de economía solidaria, para que **personas discapacitadas, víctimas del desplazamiento forzado y personas de la tercera edad** puedan contar con un trabajo en una ciudad en la que les es difícil buscar empleo.

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
