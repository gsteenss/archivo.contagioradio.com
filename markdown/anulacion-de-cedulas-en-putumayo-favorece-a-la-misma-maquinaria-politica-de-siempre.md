Title: Anulación de cédulas en Putumayo "favorece a la misma maquinaria política de siempre"
Date: 2015-10-08 13:37
Category: Nacional, Política
Tags: Acaparamiento de regalías en Putumayo, Anulación de cédulas, Consejo Nacional Electoral, Misión de Observación Electoral, Politiquería en Putumayo, Putumayo, Transhumancia electoral
Slug: anulacion-de-cedulas-en-putumayo-favorece-a-la-misma-maquinaria-politica-de-siempre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Inscripción-cédulas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: quintopoder 

<iframe src="http://www.ivoox.com/player_ek_8866840_2_1.html?data=mZ2jmJ2YdI6ZmKiak5uJd6KnmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc%2FpzcbQy4qnd4a2lNOYxsqPp4a3lIqum8nZsMLnjMrbjbXZuNbhwt7cjYqWdsfV19Tfx8jJb8KfzcaYz87XscKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Yeison Pava; Mesa DDHH Putumayo] 

###### [8 Oct 2015] 

El **Consejo Nacional Electoral ha anulado cerca de 1 millón 600 mil inscripciones de cédulas** por trashumancia electoral, aproximadamente 10 mil de ellas pertenecen al departamento de Putumayo y a campesinos habitantes de zonas rurales, quienes en su mayoría **nunca han votado y cuyos candidatos pertenecen a sectores sociales organizados** que se podrían ganar las elecciones.

Yeison Pava, abogado integrante de la Mesa de Derechos Humanos de Putumayo, asegura que la mayoría de cédulas anuladas pertenecen a zonas rurales en las que la mayoría de campesinos las  inscribían por primera vez. El abogado agrega que es la primera vez que se anulan tantas inscripciones de cédulas a pesar de que las prácticas de compra de votos son comunes en las contiendas electorales.

Este aumento en la inscripción de cédulas, que supuestamente alertó al CNE, da cuenta del **ejercicio pedagógico con comunidades rurales para afianzar su ejercicio democrático** e impedir que sus cédulas fueran trasladadas a las cabeceras municipales como ha venido ocurriendo, agrega Pava.

El abogado afirma que este tipo de acciones benefician a la **maquinaria politiquera que ha empobrecido a la región**, a través del acaparamiento de las regalías producidas por la extracción petrolera de larga data, en un departamento en el que la cultura politiquera ha visto en la **trashumancia electoral la estrategia para llegar a cargos públicos**.

[![cédulas\_anuladas\_putumayo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/IMG-20151007-WA0000.jpg){.aligncenter .size-full .wp-image-15508 width="405" height="235"}](https://archivo.contagioradio.com/anulacion-de-cedulas-en-putumayo-favorece-a-la-misma-maquinaria-politica-de-siempre/img-20151007-wa0000/)

No obstante la actual crisis económica, social y ambiental por la que atraviesa Putumayo ha hecho que su población **“tome conciencia de la politiquería que se ha robado las regalías y no les reciben dinero a cambio de sus votos”**, concluye Pava.

La exigencia de las comunidades es que el CNE haga una verificación en campo, puesto que **el cruce con bases de datos del SISBEN, la ASPEN y el FOSYGA**, asumidos como criterio para la anulación de las cédulas, **resulta insuficiente**. Por lo que adelantan una interposición de recursos de reposición para que el CNE estudie y verifique estas anulaciones con base en certificados de juntas comunales y planteles educativos.
