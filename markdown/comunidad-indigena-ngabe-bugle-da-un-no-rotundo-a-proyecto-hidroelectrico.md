Title: Comunidad Ngabe Bugle de Panamá da un NO rotundo a proyecto hidroeléctrico
Date: 2015-06-02 15:48
Category: El mundo, Movilización
Tags: Comarca Ngabe Bugle, Comunidades indígenas en Panamá, hidroeléctrica Barro Blanco, Juan Carlos Varela, Panama, proyectos hidroeléctricos en Panamá, Ricardo Martinelli
Slug: comunidad-indigena-ngabe-bugle-da-un-no-rotundo-a-proyecto-hidroelectrico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/5697352907_379e9eabbf_b.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.flickr.com]

<iframe src="http://www.ivoox.com/player_ek_4585980_2_1.html?data=lZqll56cdI6ZmKiak5mJd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIb8rixYqwlYqliMjZz8aYsMzFpsafo9rUzsqPqMKf1tOYsLSPttDo1tPR0ZDFb9Hm0N6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Manolo Zárate, Secretaría de Alianza Estratégica Nacional] 

La comunidad indígena Ngabe Bugle, se encuentra en un **proceso de resistencia en contra del proyecto hidroeléctrico Barro Blanco de la empresa hondureña GENISA,** que afectaría social, cultural y ambientalmente a la comunidad y al campesinado.

Manolo Zárate, ingeniero hidráulico y quien hace parte de la **Secretaría de Alianza Estratégica Nacional de movimientos sociales de Panamá**,  asegura que  desde que se privatizó la institución central que regulaban todo el sistema hidroenergético, ya hay más de 75 proyectos hidroeléctricos y 22 que ya existen. Uno de ellos, es el proyecto Barro Blanco, que usaría el **río Tabasará al suroeste del país para alimentar la represa,** sin tener en cuenta que este es uno de los afluentes que sostiene uno de los manglares más importantes de Panamá.

De acuerdo a Zárate, **aproximadamente 50% de la cuenca del río acoge a las comunidades indígenas Ngabe Bugle, que están amparadas  por la Ley Nacional** y la Constitución, lo que le da autonomía al sector indígena, para que se garantice las riquezas naturales de esta parte de la región y se proteja el esquema de la propiedad colectiva de tierras que es uno de los aspectos de la cosmovisión indígena.

Sin embargo, todo lo anterior lo trastoca la empresa GENISA, pues **se estaría usando tierras de las comarcas indígenas,  donde habitan 250 mil pobladores,** de manera que** **si se continúa con el proyecto es posible que esa situación “termine en un  episodio de violencia”, expresa el ingeniero hidráulico.

Cabe recordar que el presidente panameño había prometido a la Alianza Nacional que si la comunidad no quería la hidroeléctrica esta obra no se realizaría, “el problema es que la empresa tiene la banca detrás porque **el desarrollo de la política energética se ha transformado en una puerta financiera,** y Varela ha tenido que ponerse a la cabeza de un Estado que entregaron totalmente corrompido”, afirma Zárate.

Para manejar este problema, el presidente estableció una **comisión de alto nivel que negociaría con los indígenas, pero esa comisión ha trabajado bajo el sometimiento de bancos,** según el ingeniero.

**La comunidad ha dado un plazo máximo hasta el 15 de junio al gobierno del presidente Valera** para que suspenda el proyecto hidroeléctrico teniendo en cuenta que se han violado 17 reglas ambientales, y varios aspectos culturales. Fue por eso que las obras se paralizaron por orden de la Autoridad Nacional del Ambiente de Panamá, luego de que se comprobara que la compañía había violado las 17 reglas ambientales, por lo que se le permitió resolver algunos de esos problemas puntuales para que no se agrandaran esos efectos ecológicos, sin embargo,  **la empresa se aprovechó de la oportunidad y continuó los trabajos para finiquitar la obra.**

Por todo esto, la comunidad indígena Ngabe Bugle denuncia que la empresa GENISA ha violado los derechos humanos del pueblo, y anunciaron que el gobierno nacional debe anunciar la cancelación del proyecto. Así mismo, la cacique indígena Silvia Carrera, aclara que “ni ahora ni después se negociará ningún recurso natural hídrico, mineral de la comarca Ngabe Bugle y Campesino” y que de “no cancelarse Barro Blanco la cacique hace un llamado a todas las organizaciones de la comarca y del país que se **sumen a la lucha para enfrentar la decisión tomada por el gobierno”.**

 
