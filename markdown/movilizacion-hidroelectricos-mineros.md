Title: Crece la movilización nacional contra la construcción de proyectos hidroeléctricos y mineros
Date: 2017-03-24 09:10
Category: CENSAT, Opinion
Tags: Agua, Ambiente, colombia, Hidroeléctricas, Movilización social
Slug: movilizacion-hidroelectricos-mineros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/unnamed-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ríos Vivos 

###### 3 Mar 2017 

#### **[Por: CENSAT Agua Viva /Amigos de la Tierra Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

El pasado 26 de febrero, se realizaría en Colombia, la primera consulta popular para decidir sobre un proyecto hidroeléctrico. En Cabrera, Cundinamarca, sus pobladores utilizaron este mecanismo de participación ciudadana para defender la vida campesina, el agua y el ambiente frente a la amenaza de un emprendimiento hidroeléctrico a filo de agua que busca realizar Emgesa y que amenaza a gran parte de los municipios de la provincia de Sumapaz. Logrando superar la votación necesaria para que la consulta sea aceptada, el 97,278% de los cabrerunos y cabrerunas dijeron no a la implementación de proyectos minero – hidroeléctricos, blindando así su territorio frente a esta amenaza.

Esa misma semana, el 28 de febrero, los Concejos Municipales de Timaná y Oporapa Huila aprobaron por unanimidad, mediante acuerdos municipales, prohibir la realización de proyectos minero energéticos y de explotación de hidrocarburos en sus municipios. Los cuales seguirían el ejemplo del Consejo Municipal de Pitalito, en este mismo departamento, que el pasado 5 de diciembre del 2016, decidirían mediante Acuerdo Municipal, prohibir el desarrollo de centrales hidroeléctricas y proyectos de gran minería.

Los acuerdos municipales tuvieron como antesala, una larga lucha del movimiento antirepresas del Huila abanderado por diversas organizaciones como Asoquimbo, el Movimiento Ciudadano de Defensa del Territorio, entre otras organizaciones huilenses, que buscan frenar las 17 hidroeléctricas que se quieren construir desde el Macizo Colombiano hasta la Honda y que hacen parte del Plan Maestro de Aprovechamiento del río Magdalena. En ellos también influenció la Sentencia de la Corte Constitucional T-445/16 que ratifica las facultades de dichos entes para restringir y prohibir proyectos minero energéticos que atenten contra el bienestar de los ciudadanos de su jurisdicción, como organismo autónomo y su competencia para regular el uso del suelo y garantizar la protección ambiental.

La construcción de proyectos hidroeléctricos genera enormes impactos ambientales y sociales: desplazamiento, destrucción de la riqueza íctica, alteración de la dinámica natural del río, inundación de suelos fértiles y destrucción de formas de vida tradicionales, entre muchos otras, a la vez que genera una gran conflictividad. Sin embargo, los proyectos hidroeléctricos han sido vendidos como energía limpia y renovable y está presente en los imaginarios de desarrollo y progreso de esta sociedad. Por ello, no ha sido tan fácil construir movimiento social que defienda los ríos y luche contra las represas.

A pesar de todo, esta situación ha venido cambiando poco a poco. A los Acuerdos Municipales y la consulta popular de Cabrera, se suman los triunfos de los opositores de los proyectos hidroeléctricos, encabezados por el Movimientos Ríos Vivos con presencia en varios departamentos de Colombia, la cancelación de las licencias ambientales a los proyectos Cañafisto (Antioquia) y Piedra del Sol (Santander).

El proyecto a filo de agua, Piedra del Sol, de la empresa Isagen, filial de la canadiense Brookfield, propuesto para ser construido en la Provincia de Guanentá, en Santander, afectaría el río Fonce y tendría graves repercusiones para las principales actividades económicas de la región, el turismo de naturaleza y la agricultura, en una región con un agudo estrés hídrico. Los santandereanos, asociados al Movimiento Ríos Vivos, dieron una larga lucha que incluyó movilizaciones, romerías e importantes y rigurosos debates sobre la inviabilidad del proyecto, la sacralidad del agua y las enseñanzas que dejó la negativa experiencia dejada por Hidrosogamoso.

En enero pasado, la ANLA, mediante resolución 001 del 02 de enero de 2017 negaría definitivamente la licencia ambiental para el proyecto Cañafisto, también propiedad de ISAGEN, ubicado en el Río Cauca y que afectaría a 16 municipios del suroeste antioqueño. La medida de la Anla respondía a la apelación presentada por la empresa, a la resolución 1291 del 13 de octubre de 2015 que declaraba inviable ambientalmente el proyecto hidroeléctrico por las graves afectaciones sobre el Bosque Seco Tropical –BST.

Todos estos casos evidencian el terreno ganado por el movimiento antirepresas para deconstruir las nociones positivas de las hidroeléctricas, pero aún queda mucha tela por cortar. Siguen sin ser reconocidas las afectaciones a muchas comunidades impactadas en sus formas de vida por proyectos como El Quimbo, Hidrosogamoso e Hidroituango para sólo mencionar las construidas en la última década. Además continúa la avanzada de nuevos proyectos que se pretenden construir en zonas de gran belleza paisajística, como el río Samaná en Antioquia o en zonas de gran importancia para la regulación hídrica y para la cosmovisión del pueblo indígena Coconuco en la zona de Puracé.

Por todo ello, el Movimiento Ríos Vivos está animando una profunda discusión sobre el modelo minero-energético y en particular sobre las represas convocando a la VII Jornada Nacional en Defensa de sus territorios y contra las represas, sumándose a las acciones de cientos de grupos en todo el mundo, y en particular a las del Movimiento de Afectados por Represas de América Latina – MAR, del cual hace parte. De esta manera se conmemora el 14 de marzo, día Internacional de Acción Contra las Hidroeléctricas y por los Ríos como una forma de respaldar las muchas luchas territoriales en defensa de los ríos libres. Les animamos a sumarse.

¡Ríos Vivos territorios de paz!
