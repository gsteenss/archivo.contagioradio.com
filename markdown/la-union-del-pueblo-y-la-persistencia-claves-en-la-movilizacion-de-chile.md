Title: La unión del pueblo y la persistencia: claves en la movilización de Chile
Date: 2019-11-12 17:54
Author: CtgAdm
Category: El mundo, Movilización
Tags: Chile, entrevista, Manifestaciones, protestas
Slug: la-union-del-pueblo-y-la-persistencia-claves-en-la-movilizacion-de-chile
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CIDH  
] 

Luego de casi cuatro semanas de constantes protestas, este martes, 12 de noviembre, diferentes organizaciones sociales en Chile hicieron la convocatoria a un paro nacional, reclamando una vez más por profundas reformas sociales que garanticen el acceso de los ciudadanos a derechos básicos como salud, pensión, educación y agua. En este contexto, líderes estudiantiles afirmaron que **creer en la fuerza transformadora de la gente e insistir en la organización y la lucha colectiva ha sido clave para lograr cambios**.

### **"Estamos ante un sistema saqueador, que nos ha despojado de los derechos fundamentales"** 

Nicole Rodríguez Aranda, vicepresidenta de la Federación de Estudiantes de la Universidad de Chile, afirmó que luego de más de 20 días en movilización, Chile está viviendo un "estallido social histórico", por cuenta de un sistema que no responde a las necesidades básicas los ciudadanos. Pese a que Rodríguez expresó que aunque los sectores de derecha han dicho que el movimiento social no tiene banderas o posiciones claras, la afirmación es falsa porque sí se han establecido 3 puntos claves en torno a los que protestan.

El primer punto de protesta, y sobre el que el presidente Piñera ya anunció algunos pasos a favor, tiene que ver con **la creación de una nueva Constitución.** Según explicó Rodríguez, los chilenos están regidos por una Carta Magna que fue construida en la dictadura militar (1980) y ha sido protegida en reiteradas ocasiones. En ese sentido, los manifestantes piden que se acceda a la opción de legislar un plebiscito que permita decidir si se quiere crear una Constitución, y cómo hacerlo.

El segundo punto corresponde a las modificaciones profundas que se requiere para garantizar el acceso a derechos básicos, en consecuencia, Rodríguez dijo que **es necesario modificar el sistema pensional, el sistema de salud y educación que están privatizados**, y genera condiciones precarias para quienes no tienen cómo pagar por estos derechos. Asimismo la estudiante resaltó la democratización en el acceso al agua, "porque Chile es el único país del mundo que tiene privatizadas sus fuentes de agua".

Rodríguez aseguró que están "ante un sistema saqueador, que nos ha despojado de los derechos fundamentales", uno de ellos, el derecho a la protesta, que ha sido fuertemente reprimido. En ese sentido, aseguró que el tercer punto en la agenda de movilizaciones es **que el Gobierno se haga responsable políticamente de su actuación en la violación a los derechos humanos a cargo de las fuerzas militares y de Policía.**

### **"No confiamos en las capacidades que tiene este Gobierno de manejar esto bajo una lectura democrática"** 

A lo largo de estas semanas de movilizaciones el gobierno Piñera ha declarado que el país está en medio de una guerra, luego se arrepintió de su declaración agregando que implementaría una agenda de medidas sociales, pero como lo recuerda Rodríguez, el pasado jueves, emitió un comunicado señalando que fortalecería las fuerzas militares y les daría más recursos para su operación. (Le puede interesar: ["Chile: Los efectos de un modelo económico fallido"](https://archivo.contagioradio.com/chile-los-efectos-de-un-modelo-economico-fallido/))

Estas variaciones en su discurso, así como la experiencia misma que han tenido en las calles, les ha dejado claro que la respuesta de Sebastián Piñera, que creían que representaba una derecha democrática no es tal, ha sido autoritario y "ha dejado claro que la derecha no aprendió de la dictadura militar, y está utilizando mecanismos criminales". Los datos del Instituto Nacional de Derechos Humanos (INDH) corroboran este señalamiento: hay 197 personas lesionadas en sus ojos por disparo de escopetas de perdigones y el uso de otras armas, además registraron "5 personas asesinadas por acción directa de agentes estatales".

Adicionalmente, la líder estudiantil señaló que **la Policía está registrando los hogares de algunas personas que participan en las protestas, buscando desactivar los liderazgos que las han impulsado**, lo que en conjunto con las denuncias del INDH, los lleva a aseverar que "ya no confiamos en las capacidades que tiene este Gobierno de manejar esto bajo una lectura democrática".

> ? \[Última actualización\] Cifras recopiladas directamente por el INDH en observaciones a manifestaciones, comisarías y centros de salud desde el jueves 17 de octubre hasta las 13:00 horas del del domingo 10 de noviembre de 2019. [pic.twitter.com/vf0iCtDqzj](https://t.co/vf0iCtDqzj)
>
> — INDH Chile (@inddhh) [November 10, 2019](https://twitter.com/inddhh/status/1193613314217185281?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Lecciones de Chile: Constancia en la movilización e insistir en la organización** 

Rodríguez sostuvo que el aprendizaje más profundo de este 'estallido social' es que si bien no fue organizado por un grupo social en específico, "sí tiene que ver con la resistencia que hemos tenido como pueblo desde la vuelta a la democracia, insistiendo una y otra vez con demandas y reclamos sociales: Pasó en 2006 con la revolución pingüina, pasó en 2011 con la revolución de los universitarios, pasó en 2018 con la movilización feminista y pasa este año con la revolución socio-ambiental".

En los años anteriores resaltó que aunque no se han visto resultados en el corto plazo, la percepción es que se está poniendo en disputa todo un sistema económico que no es difícil de cambiar, pero que en el largo plazo sí genera resultados, si se logra "confiar en la fuerza de nuestros pueblos". (Le puede interesar:["Chile rechaza proyecto minero para proteger pingüinos en peligro de extinción"](https://archivo.contagioradio.com/chile_mineria_pinguinos_rechazo/))

En la misma vía, subrayó como importante "insistir en la organización, en lo colectivo que es lo que se ha querido destruir"; y añadió que es un valor que se ha generado en las recientes movilizaciones "porque nos saludamos, nos miramos y nos cuidamos entre todos". (Le puede interesar: ["Estudiantes chilenos exigen no más deuda por estudiar"](https://archivo.contagioradio.com/estudiantes-chile-deuda/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44296258" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44296258_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
