Title: Documental rinde homenaje a la vida y obra de Marta Rodríguez
Date: 2015-05-12 12:36
Author: CtgAdm
Category: 24 Cuadros
Tags: Cine Club de la Universidad Central, documental biográfico “Transgresión”, Fernando Restrepo Documentalista, Jean Rouch, Jorge Silva documentalista, Los chircales, Marta Rodríguez Documentalista colombiana, Movimiento Universitario de Promoción Comunal (Muniproc), Sociología Sociología en la Universidad Nacional de Colombia
Slug: documental-rinde-homenaje-a-la-vida-y-obra-de-marta-rodriguez
Status: published

###### Foto:Enrique Forero 

###### [12 May 2015] 

*Su filmografía comprende quince documentales, producciones que la han convertido en un referente obligado dentro del género en Colombia y Latinoamérica.*

Como **homenaje a más de 40 años de trabajo y el legado que representa para la memoria histórica a través del audiovisual**, el Cine Club de la Universidad Central abrirá la casa este 12 de mayo al estreno del documental biográfico “Transgresión”, una mirada a la vida y el trabajo de Marta Rodríguez, desde el lente de Fernando Restrepo. Una película que representa “un viaje al interior del corazón, donde se evidencian sentimientos de incertidumbre, alegría, dolor y contradicción”.

Próxima a cumplir 82 años de vida, **Marta Rodríguez** recoge los frutos de un trabajo que inició en la década de los sesenta y que se ha extendido hasta nuestros días, alternado con su activa labor social en comunidades indígenas, afrocolombianas, movimientos estudiantiles y sindicales.

De raíces humildes y con una crianza rural enmarcada por los fuertes contrastes sociales, la documentalista creció con una gran conciencia y un espíritu de activismo que se fue afianzando con sus diferentes experiencias vividas, como el 9 de Abril de 1948 en su época escolar y los diferentes viajes realizados a España y Francia donde tendría sus primeros acercamientos con el pensamiento de izquierda.

La influencia de diferentes maestros conocidos durante su etapa de formación en el exterior la llevaría en su regreso al país, a ingresar como **estudiante de Sociología en la Universidad Nacional de Colombia, siendo alumna del padre Camilo Torres Restrepo**, para luego formar parte del el Movimiento Universitario de Promoción Comunal (Muniproc), fundado por el sacerdote, donde colaboraría con tareas de alfabetización en familias desplazadas en el barrio Tunjuelito al sur de Bogotá.

<iframe src="https://www.youtube.com/embed/aeHFiHg5RhQ" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Es allí donde conoce a los niños trabajadores de **los chircales** (lugares para la elaboración de ladrillos y tejas de manera artesanal), despertando en ella una necesidad de visibilizar y denunciar las difíciles condiciones en que los pequeños eran explotados. La experiencia vivida durante esos años representa el nacimiento de la documentalista, formada profesionalmente en cine y etnografía en Paris bajo la doctrina del antropólogo y cineasta Jean Rouch.

A su regreso al país en 1965, conoce a **Jorge Silva**, con quien compartiría su amor por el audiovisual y de cuya unión daría como frutos, además de sus dos hijos Lucas y Sara, algunas de las producciones más notables de su trayectoria como documentalistas: “Chircales” (1971), “Planas, testimonio de un etnocidio”(1970), “Campesinos”(1975), “La voz de los sobrevivientes”(1981) convirtiéndose en un equipo único que sólo separaría la muerte de Silva ocurrida en 1987, antes de llegar a finalizar “Nacer de nuevo”.

[![Martha Rodriguez](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/n35a13f1.jpg){.aligncenter .size-full .wp-image-8512 width="557" height="550"}](https://archivo.contagioradio.com/documental-rinde-homenaje-a-la-vida-y-obra-de-marta-rodriguez/n35a13f1/)

Marta seguiría trabajando con la misma intensidad y el apoyo de su hijo Lucas, produciendo los documentales "Amapola, flor maldita” y “Los hijos del trueno” (1998) acerca de la incidencia en la cultura de los indígenas del Cauca de los cultivos ilícitos y “La hoja sagrada” sobre el cultivo de la hoja de coca. En 2001 realizaría junto con Fernando Restrepo "Nunca más" y luego "Una casa sola se vence" (2004) y "Soraya", amor no es olvido (2006), documentales acerca del desplazamiento violento vivido por las comunidades afrocolombianas del Urabá chocoano y antioqueño, por parte de los actores armados que actúan en Colombia.

[![Transgresión Documental](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/trangresion.jpg){.alignleft .size-full .wp-image-8487 width="960" height="544"}](https://archivo.contagioradio.com/documental-rinde-homenaje-a-la-vida-y-obra-de-marta-rodriguez/trangresion/)

El documental es el resultado de una relación de 15 años de amistad y realización audiovisual entre la documentalista y Restrepo. La cita es en el **Auditorio Jorge Enrique Molina de la Universidad Central** (antiguo Teatro México) ubicado en la Calle 22 \# 5-85, Bogotá desde las 6 p.m.
