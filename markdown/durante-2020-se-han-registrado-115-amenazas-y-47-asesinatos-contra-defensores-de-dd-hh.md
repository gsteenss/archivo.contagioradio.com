Title: Durante 2020 se han registrado 115 amenazas y 47 asesinatos contra defensores de DD.HH.
Date: 2020-06-03 15:14
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Antioquia, asesinato de líderes sociales
Slug: durante-2020-se-han-registrado-115-amenazas-y-47-asesinatos-contra-defensores-de-dd-hh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Asesinato-de-líderes-sociales.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Tabla-agresiones.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/informe-somos-defensores-2020.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/somos-defensores-informe-2020.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Defensores de DD.HH./ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El informe trimestral presentado por el programa Somos Defensores que recoge las cifras del **Sistema de Información sobre Agresiones contra Personas Defensoras de Derechos Humanos en Colombia (SIADDHH) reveló** que entre enero y marzo de 2020, pese a que existió una reducción en cifras de las agresiones en general, se registró un incremento considerable en los homicidios en contra de los líderes sociales en el territorio nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El estudio distingue **diferentes tipos de agresiones. Amenazas, atentados, detenciones arbitrarias, desapariciones forzadas, judicializaciones y robo de información.** En relación con la cifra general se reportó una disminución entre el primer trimestre del año 2019 donde se registraron 245 agresiones y el mismo periodo del presente año donde se registraron 197. No obstante, se enfatiza en que las circunstancias del confinamiento y la imposibilidad de movilizarse hacia los territorios ha dificultado ostensiblemente la recopilación de la información, por lo cual, la reducción de las cifras no puede entenderse como una merma efectiva de las agresiones.   

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De hecho, **la situación que ha acarreado la pandemia del Covid-19 ha traído efectos nocivos y mayores riesgos para los liderazgos sociales**, si se tiene en cuenta que el confinamiento y las estrictas restricciones de movilidad, han convertido a los líderes en blancos fáciles para los agresores pues se hace más sencillo ubicar el lugar en el que permanecen.

<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":84995,"sizeSlug":"large"} -->

<div class="wp-block-image">

<figure class="aligncenter size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Tabla-agresiones.png){.wp-image-84995}  
<figcaption>
Comparativo de Agresiones entre 2019 y 2020
</figcaption>
</figure>

</div>

<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Más líderes asesinados

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El dato más concluyente del informe es el desmedido incremento de asesinatos en contra de los líderes; **la cifra aumentó en un 88% respecto del primer trimestre del año anterior donde se registraron 25 homicidios frente a los 47 registrados este año**. Esta cifra genera especial alerta pues, según Somos Defensores,  se puede interpretar como la materialización de muchas de las amenazas de las que son víctimas los líderes y defensores de DD.HH.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los departamentos más afectados son **Cauca que registró 9 asesinatos, seguido por Antioquia con 8 casos y Putumayo con 6.** Del total de homicidios registrados en este primer semestre 17 ocurrieron en enero, 16 en febrero y 14 en el mes de marzo. Las víctimas fueron 42 hombres y 5 mujeres. [(Lea también: 2019 fue el año más agresivo contra defensores de DDHH en toda la década: Somos Defensores)](https://archivo.contagioradio.com/el-ano-pasado-fue-el-mas-agresivo-para-defensores-de-dd-hh-de-la-decada-somos-defensores/)

<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":84994,"sizeSlug":"large"} -->

<div class="wp-block-image">

<figure class="aligncenter size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Asesinato-de-líderes-sociales.png){.wp-image-84994}  
<figcaption>
*Informe SIADDHH.*
</figcaption>
</figure>

</div>

<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Predominan las agresiones contra indígenas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**187 fueron los líderes y lideresas víctimas de las distintas formas de agresión en este primer semestre**, la diferencia respecto al número de ataques (197), se explica por la ocurrencia en algunos casos de varias agresiones contra una misma persona.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**El liderazgo más agredido fue el indígena, con 59 casos que corresponden al 32% del total,** **y que se registraron en su mayoría en los departamentos de Cauca y La Guajira.** En segundo lugar se encuentran los defensores de DD.HH. con 27 agresiones que equivalen a un 14%. En tercer lugar se ubican los líderes comunales con un 11% y los líderes comunitarios también con un 11% de las agresiones registradas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otros tipos de líderes afectados fueron líderes campesinos, académicos, aquellos que abogan por las víctimas, líderes sindicales, afrodescendientes y LGTBI.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los responsables de las agresiones contra defensores

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Los grupos paramilitares fueron a quienes se atribuyeron el mayor número de agresiones con un total de 84 casos correspondientes al 45% del total**, la denominación de estos grupos varía según la zona de influencia, entre ellos se encuentran las Águilas Negras Bloque Capital, las Autodefensas Gaitanistas de Colombia –AGC–, los Caparrapos, el Clan del Golfo y La Mafia. **Por otra parte, se responsabilizó a las disidencias de las FARC por la comisión de 15 casos (8%) y a la Fuerza Pública de 13 (7%).** En 70 casos (37%) no se pudo identificar el autor de la agresión, lo cual, dificulta aún más la ya bastante remota tarea de protección de los afectados por parte del Estado. [(Lea también: Las amenazas que enfrentan líderes como Marco Rivadeneira en Putumayo)](https://archivo.contagioradio.com/marco-rivadeneira-el-lider-que-hasta-su-ultimo-dia-defendio-la-sustitucion-voluntaria-de-cultivos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El panorama es desolador y la acción del Estado insuficiente si además se tiene en cuenta los registros de los que lleva cuenta el Instituto de Estudios para el Desarrollo y la Paz (Indepaz) que a la fecha contabiliza un total de [118 líderes asesinados](http://www.indepaz.org.co/paz-al-liderazgo-social/) solo en el presente año.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
