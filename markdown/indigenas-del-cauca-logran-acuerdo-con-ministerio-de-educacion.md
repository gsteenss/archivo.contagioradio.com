Title: Indígenas del Cauca logran acuerdos con Ministerio de Educación
Date: 2018-01-25 18:22
Category: DDHH, Nacional
Tags: Cauca, educacion, indígenas, Ministerio de Educación
Slug: indigenas-del-cauca-logran-acuerdo-con-ministerio-de-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Educación-pueblos-indígenas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [25 Ene 2018] 

Indígenas del Cauca lograron llegar a diferentes acuerdos con el Ministerio de Educación para garantizar la financiación del sistema de educación propia. Entre los compromisos adquiridos por la entidad se encuentra el **pago del 87% del presupuesto para el programa de educación y que se llegue al 100% de los territorios indígenas del país**.

En un principio los indígenas estaban denunciando que el Ministerio de Educación solo estaba dando el 10% de la tipología, que es el valor que calcula el Ministerio sobre el costo de dar educación a cada niño del país, para financiar **la educación de los niños indígenas, mientras que al resto de la población le daba el 100%**.

Edwin Guetio, Consejero Mayor del CRIC, señaló que esta situación se presentó después del cambio del documento COMPES y aseguró que, durante los años 2015 y 2016, se contrató el 100% de la tipología, mientras que en el **2017 hay un cambio en la documentación que solo brinda un 10% en contratación**.

La viceministra Natalia Ruíz, en representación del Ministerios de Educación, le afirmó a los indígenas que el cambio en el monto era producto del **déficit fiscal, que no les permitía cumplir con el 100%, razón por la cual se otorgo ese 10%**, que es administrado por la Secretaría de Educación departamental.

Sin embargo, Guetio afirmó que este argumento no es lógico porque el dinero seguía llegando a la Secretaría de Educación departamental, “no se entiende, por qué, si los recursos llegan a la Secretaría, lo único que esta debe hacer es redirigirlos, hacia la educación de los niños”. (Le puede interesar: ["Incursión de tipo paramilitar dejó 3 muertos y 9 heridos en Argelia, Cauca")](https://archivo.contagioradio.com/incursion_armada_paramilitar_argelia_cauca/)

Los indígenas expresaron que los contratos para la educación deben gestionarse lo más pronto posible para no ser afectados por la ley de garantías, que impide cualquier tipo de contratación con entidades estatales en periodo electoral, **de lo contrario afirmaron que se verían afectados más de 5 mil niños indígenas.**

<iframe id="audio_23383149" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23383149_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
