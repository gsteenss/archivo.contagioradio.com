Title: Informe revela las inhumanas acciones de EE.UU. con migrantes centroamericanos
Date: 2019-07-20 17:48
Author: CtgAdm
Category: El mundo
Tags: Derechos Humanos, Estados Unidos, migración, migrantes
Slug: las-condiciones-de-los-migrantes-se-vuelven-inhumanas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Migrantes-Routers-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Reuters] 

El pasado 02 de julio el **Departamento de Seguridad Nacional de Washington (DHS)** emitió un informe denunciando las condiciones de inseguridad y hacinamiento en los centros de detenciones para migrantes que llegan a la frontera sur de Estados Unidos. (Le puede interesar:  ["México: Caravana de migrantes continúa su peligrosa travesía hacia la frontera con EE.UU."](https://archivo.contagioradio.com/caravana-migrantes-continua-peligrosa-travesia-hacia-frontera-eeuu/))

Esta publicación tiene como propósito denunciar el estado inhumano de las celdas atestadas de personas y la falta de acceso a servicios de primera necesidad. Así mismo, quiere advertir sobre el nivel de riesgos, que se hace cada día más severo, puesto que la ley prevé que son centros de detención temporales (72 horas), pero los migrantes pueden permanecer en ellos varios meses sin que se les resuelva su situación.

### **El grave incumplimiento de las normas de acompañamiento **

Los inspectores del Departamento visitaron el pasado 5 de junio las instalaciones en el Valle del Rio Grande del Texas después de emitir, en mayo de 2019, una alerta sobre la gestión de sobre población. En este proceso descubrieron diferentes formas de maltrato: **los niños no habían  podido bañarse en semanas, y no había acceso a comidas calientes para ellos**.

Algunos adultos permanecían recluidos hasta una semana en espacios en los que tenían que estar de pie, sin que la Aduana y Protección Fronteriza (CBP, por sus siglas en inglés)  garantizara su seguridad higiénica, como lo exigen los estándares de las normas de acompañamiento, detención y búsqueda (TEDS).

De igual modo, según el informe, 826 de los 2669 niños extranjeros no acompañados habían sido retenidos por más de 72 horas, lo que viola un acuerdo federal y la política de la CBP. La misma oficina tendría la obligación de hacer un esfuerzo razonable para proporcionar una ducha para adultos después de 3 días.

Por ejemplo, **durante un mes, 12 personas no tuvieron acceso a una ducha bajo la custodia de CBP**, a pesar de que varios permanecieron allí más de ese tiempo. (Le puede interesar: ["Las órdenes de Trump que agreden a inmigrantes e indocumentados"](https://archivo.contagioradio.com/las-ordenes-de-trump-que-agreden-a-inmigrantes-e-indocumentados/))

Según las estadísticas de la Patrulla Fronteriza, el sector del Río Grande tiene un volumen de casi un cuarto de millón de personas detenidas en los primeros 8 meses del 2019. Este total representa un 124 por ciento de aumento en comparación con el mismo período en el 2018, con el mayor incremento en las familias completas.

### **El Congreso está obligado a intervenir** 

“Nos preocupa que el hacinamiento y la detención prolongada representen un riesgo inmediato para la salud y seguridad de los agentes del DHS, y para los detenidos. En el momento de nuestras visitas, la gerencia de la Patrulla Fronteriza dijo que ya habían ocurrido incidentes de seguridad entre hombres adultos”, resalta el informe.

Tras tomar nota de los hechos, los comités del poder judicial y de supervisión de la Cámara de Representantes fijaron una audiencia para hablar sobre estas instalaciones que se están convirtiendo en una "**bomba de tiempo**". Mientras tanto, los “detenidos” reclaman el incumplimiento de todas las normas, lo que no está garantizando condiciones mínimas de supervivencia.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
