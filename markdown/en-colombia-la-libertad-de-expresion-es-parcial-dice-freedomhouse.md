Title: En Colombia la Libertad de Expresión es "parcial" dice FreedomHouse
Date: 2015-04-30 15:08
Author: CtgAdm
Category: El mundo, Política
Tags: FreedomHouse informe libertad de expresión parcial Colombia, FreedomHouse vigilancia libertad de expresión, libertad de expresión Colombia, Panamá y Colombia libertad de expresión parcial
Slug: en-colombia-la-libertad-de-expresion-es-parcial-dice-freedomhouse
Status: published

###### Foto:Enelmundoestamos.blogspot.com 

Según el informe anual de la organización social de EEUU , **FreedomHouse** sobre la **libertad de expresión** en el mundo **Colombia y Panamá** se sitúan en la franja de países en condiciones de **libertad de expresión parcial.**

**Panamá** habría **bajado un punto** con respecto al año pasado y **Colombia se sitúa en el numero 28** de la lista de países que entrarían a formar parte del grupo de libertad de expresión parcial.

La ONG divide a los países en **tres, los libres (32%), los parcialmente libres (36%) y los no libres (32%)**, dependiendo de los ataques a la libertad de expresión medidos en el ejercicio del periodismo y de la propia libertad para expresarse de sus ciudadanos. **Sin embargo el concepto de libertad que se aplica en el informe es el dictaminado por EEUU**.

Actualmente **Panamá** se encuentra el puesto **24 de 35 países analizados en Sudamérica,** encontrándose por encima de Haití, Argentina,Colombia, México, Venezuela y Cuba.

Además añade que a nivel mundial la **libertad de expresión bajó a su nivel más bajo en los últimos 10 años**, describiendo la situación como:

"...Los grupos armados y bandas criminales emplearon tácticas cada vez más descaradas para intimidar a periodistas, y los propietarios de medios intentaron manipular el contenido noticioso para servir a sus intereses políticos o empresariales..."

**Mapa de países en Sudamérica** clasificados por FreedomHouse:  
\[caption id="attachment\_7887" align="alignleft" width="325"\][![FreedomHouse](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/Panama-libertad-parcial-Freedom-House_LPRIMA20150429_0175_1.jpg){.size-full .wp-image-7887 width="325" height="741"}](https://archivo.contagioradio.com/en-colombia-la-libertad-de-expresion-es-parcial-dice-freedomhouse/panama-libertad-parcial-freedom-house_lprima20150429_0175_1/) FreedomHouse\[/caption\]

<div id="stcpDiv">

</div>
