Title: Condenan a siete militares responsables del asesinato del campesino José Orlando Giraldo
Date: 2019-08-07 20:11
Author: CtgAdm
Category: Judicial, Líderes sociales
Tags: Ejecuciones Extrajudiciales, Valle del Cauca
Slug: condenan-a-siete-militares-responsables-materiales-del-asesinato-del-campesino-jose-orlando-giraldo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/JEP-Fuerza-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Redmás] 

El pasado 5 de agosto el Tribunal Superior de Cali condenó a 34 años de prisión a siete militares responsables del homicidio de **José Orlando Giraldo Barrera**, campesino presentado posteriormente como guerrillero muerto en combate en Golondrinas, al norte de Cali en 2006. Este es uno de los más de 4.000 casos que vinculan a agentes del Estado con ejecuciones extrajudiciales ocurridas durante la primera década del año 2000.

Los condenados, integrantes del **Batallón de Alta Montaña Nº 3 Rodrigo Lloreda Caicedo**, quienes habían sido absueltos en 2018 y que han sido condenados son: Manuel Arturo Pabón Jaimes, los suboficiales Carlos Enrique Martín Díaz, Fidel Angarita y Luis Francisco Galvis Sepúlveda y los soldados profesionales Cristian Daniel Delgado Quasquer y John Jairo Quijano Sánchez.

Martha Giraldo, hija de José e integrante del MOVICE expresó que este veredicto representa un avance en términos de verdad y de justicia y auqnue la sentencia no contempla la vinculación de altos mandos como **el general Carlos Enrique Sánchez Molina,** persona al mando en aquella época del Batallón y quien tendría una responsabilidad intelectual en los hechos.  [(Le puede interesar: 12 casos de ejecuciones extrajudiciales en Casanare serán revisados por la JEP)](https://archivo.contagioradio.com/jep-revisara-12-casos-de-ejecuciones-extrajudiciales-en-casanare/)

La integrante de MOVICE señala lo importante que es reconocer la culpabilidad y valorado las pruebas en contra de los responsables del asesinato de su padre y agrega que **"sienta un precedente para nuestra familia y alienta a continuar, hay que dar la pelea jurídica hasta el final, es importante que el Estado reconozca lo que sucedió".**

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_39648868" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39648868_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
