Title: Los cinco principios de la justicia transicional
Date: 2015-02-17 19:18
Author: CtgAdm
Category: datos, infografia
Tags: justicia transicional en colombia
Slug: los-cinco-principios-de-la-justicia-transicional
Status: published

Según **Federico Andreu**, experto en justicia internacional y mecanismos de justicia transicional, son necesarias una serie de medidas y garantías para que se apliquen modelos de justicia transicional luego de un conflicto armado. A la luz del derecho internacional Andreu explica que deben darse condiciones satisfactorias de Justicia, Verdad, Reparación, Garantías de no repetición y depuración de las estructuras estatales o empresariales que generaron la violencia.

Sin esos principios o con la aplicación de mecanismos  de “Perdón y olvido” es muy probable que la insatisfacción de la sociedad o de las propias víctimas acarreen, en momentos sub siguientes, un debilitamiento de la legitimidad del Estado y la probable aplicación de procesos penales en contra de los responsables de crímenes que sigan en la impunidad.

**[Compartimos los cinco principios ]**

[![infografia justicia transicional](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/02/justicia-transicional-.jpg){.aligncenter .size-full .wp-image-4913 width="626" height="824"}](https://archivo.contagioradio.com/datos/los-cinco-principios-de-la-justicia-transicional/attachment/justicia-transicional/)
