Title: Ordenan libertad para patrullero que disparó a Diego Felipe Becerra
Date: 2016-08-18 18:25
Category: DDHH, Nacional
Tags: Diego Felipe Becerra, policia
Slug: ordenan-libertad-para-patrullero-que-disparo-a-diego-felipe-becerra
Status: published

###### Foto: El Tiempo 

###### [18 Ago 2016] 

El juez 47 de conocimiento **ordenó la libertad del patrullero de la Policía Metropolitana de Bogotá, Wilmer Antonio Alarcón**, justo el mismo día en que la Fiscalía y la Procuraduría habían solicitado que se emitiera el fallo con el que se esperaba dictar condena contra Alarcón por el homicidio agravado del joven Diego Felipe Becerra.

Pese a que **existen los elementos materiales probatorios suficientes para evidenciar la responsabilidad** **del patrullero,** el juez consideró que hubo vencimiento de términos en el proceso que se adelanta contra Alarcón por el asesinato del joven grafitero de 16 años de edad.

**Elementos materiales probatorios, entre documentos y testimonios,** comprobaron que el patrullero si le disparó con su arma de dotación al joven luego de retenerlo junto a sus dos amigos menores de edad.

Cabe recordar que el pasado 18 de julio el patrullero Giovanny Tovar, había admitido que él fue quien llevó** el arma de fuego que apareció **en la escena del** asesinato de Diego Felipe,** para hacer creer que esta pertenecía al joven. Una declaración que para la abogada de la familia, significa que si “hubo una clara alteración de la escena del crimen”.

**De acuerdo con Gustavo Trejos, padre de Diego Felipe en la alteración de la escena del crimen, hay trece personas implicadas**, de las cuales tres son civiles y el resto policías. 9 de ellos han decidido contar la verdad incluyendo el testimonio de subintendente Giovanny Tovar que reconoció que llevó el arma con la que pretendieron justificar el asesinato.

<iframe src="http://co.ivoox.com/es/player_ej_12601941_2_1.html?data=kpejkpadeJKhhpywj5WdaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncajp1NnO2NSPmNPZy9Tgh5encYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
