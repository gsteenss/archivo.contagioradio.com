Title: Ex-dictador Noriega vuelve a ser juzgado por asesinato
Date: 2015-05-05 16:51
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Dictador de Panamá juzgado por asesinato de opositor, Ex dictador de Panamá vuelve a ser juzgado por muerte de un opositor, Manuel Antonio Noruega, Noriega vuelve a ser juzgado por muerte de un opositor
Slug: ex-dictador-noriega-vuelve-a-ser-juzgado-por-asesinato
Status: published

###### Foto:Taringa.net 

El **ex-dictador de Panamá** Manuel Antonio Noriega que actualmente se encuentra en prisión por el asesinato de opositores volverá a ser **juzgado por la muerte de otra personas en 1970**. El juicio se realizará el 21 de mayo por el asesinato del opositor **Heliodoro Portugal,** en el tiempo en el que el ex-dictador era jefe de la Guardia Nacional.

Noriega está cumpliendo una **condena de 60 años,** por la desaparición forzada y el asesinato sistemático de opositores durante su gobierno entre 1983 y 1989. El ex-dictador tiene 81 años y cumple arresto desde 2001, por casos relacionados con la represión de la dictadura que encabezó en contra de la oposición.

En el caso del **asesinato de Portugal**, el **estado Panameño tuvo que pedir perdón e indemnizar a la familia** después de que la Corte Interamericana de Derechos Humanos **(CIDH) condenara al estado por la muerte**.

En **2011 ya fue extraditado de Panamá a Francia** donde cumplió **dos años de prisión** por lavado de dinero durante la dictadura.

Noriega protagonizó una dictadura que causo graves daños en la economía en la política y en lo social. En 1989 Panamá fue invadido por EEUU cometiendo masacres contra la población civil.

En **1992 EEUU lo condenó por relaciones con el Cartel de Medellín** ha una pena de **20 años** y fue confinado en una prisión de Miami, hasta que Francia lo extradito por blanqueo de dinero.
