Title: Sistema de salud pública agoniza en Bogotá: Defensoría del Pueblo
Date: 2016-12-01 12:14
Category: DDHH, Nacional
Tags: Bogotá, crisis de la salud, Salud, sistema de salud
Slug: sistema-de-salud-publica-agoniza-en-bogota-defensoria-del-pueblo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/sistema-salud-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas] 

###### [1 Dic. 2016] 

La Defensoría del Pueblo en la ciudad de Bogotá, estuvo realizando **visitas nocturnas a los hospitales de la capital, más exactamente a los servicios de urgencias de segundo y tercer nivel de complejidad que hacen parte de la red pública del Distrito.**

Los resultados no son nada alentadores. Fueron cerca de 21 instituciones las que arrojaron diversos hallazgos, entre los que se encuentra que **casi la mayoría de las clínicas presentan imposibilidad en sus trámites, demoras en la atención, insuficiencia de camas y no entrega de medicamentos, entre otras graves situaciones.**

En la red hospitalaria del norte de la capital, algunos de los resultados de estas visitas fue haber notado que en **varias de estas instituciones no existe atención al usuario en horarios nocturnos, lo que se traduce en que no hay resolución de trámites por falta de pago, es decir los pacientes deben esperar hasta el día siguiente, retrasando así su salida.**

Así mismo, no hay servicio de sillas de ruedas lo que hace que quienes lleguen a los planteles y requieran usar alguna no puedan hacerlo.

Una de los resultados más preocupantes que arroja este informe, es la **evidencia del déficit de insumos de materiales y medicamentos** como ampicilina o surfacante y en otros casos hay algunos hospitales que incluso no cuentan con suturas.

Sumado a la falta de insumos, se encuentra también la f**alta de personal de trabajo social y atención al usuario nocturno lo que hace que los usuarios no puedan recibir un adecuado servicio y guía frente a los trámites a realizar o el estado de salud de los pacientes.**

Por su parte, al sur de la ciudad, los hallazgos son aún más delicados y numerosos. Por ejemplo, **algunos hospitales no cuentan con equipos para realizar radiología u otros procedimientos.** De igual manera, el número de personal es insuficiente para la cantidad de pacientes que ingresan a diario.

En otros casos, la infraestructura no es la más adecuada para que pacientes y acompañantes puedan permanecer de manera segura. **En hospitales como el de El Tunal se evidenció sobre ocupación y hacinamiento de las áreas de urgencias de un 583%,** lo que podría generar, entre otros temas, un cruce de infecciones tanto en pacientes como en acompañantes.

Este informe, que es la respuesta a una solicitud hecha po el concejal Armando Gutiérrez González del partido liberal, detalla en sus 14 hojas cada una de las instituciones visitadas, sustentando la información en testimonios de trabajadores, pacientes y familiares, quienes a diario deben verse enfrentados a estas críticas situaciones. Le puede interesar: [Eps podrán ser sancionadas si no acatan órdenes de la Supersalud](https://archivo.contagioradio.com/eps-podran-ser-sancionadas-sino-acatan-con-ordenes-de-la-supersalud/)

[Respuesta Defensoría del Pueblo a solicitud hecha por el Concejal Armando Gutiérrez González](https://www.scribd.com/document/332917651/Respuesta-Defensoria-del-Pueblo-a-solicitud-hecha-por-el-Concejal-Armando-Gutierrez-Gonzalez#from_embed "View Respuesta Defensoría del Pueblo a solicitud hecha por el Concejal Armando Gutiérrez González on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_42179" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/332917651/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-RhB5g2hGDhg8jJRaBV3j&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}

<div class="ssba ssba-wrap">

</div>
