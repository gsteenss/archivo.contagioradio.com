Title: El municipio de Arbeláez sale a votar contra las actividades petroleras
Date: 2017-07-06 17:05
Category: Ambiente, Nacional
Tags: Arbeláez, consulta popular
Slug: arbelaez_consulta_popular_contra_petroleo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/arbelaez-e1499378884246.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La guía Cundinamarca 

###### [6 Jul 2017] 

Este 9 de julio el municipio de Arbeláez se incluye en la lista de pueblos colombianos que buscan impedir la continuación de la locomotora minero-energética en los territorios. Particularmente en este caso los pobladores quieren mostrar que no están de acuerdo con las actividades petroleras en **uno de los municipios que hace parte de la región del Sumapaz en Cundinamarca.**

Luis Jaime Ortíz, integrante del Comité por el NO, uno de los tantos que se ha inscrito para hacer campaña por la consulta, explica que su municipio tiene dos bloques petroleros y uno de gas, de manera que señala que con este mecanismo de participación popular lo que se pretende es "poner las cosas al derecho, permitiendo que los ciudadanos ejerzan su derecho a decidir sobre su territorio" y agrega, "**La consulta es vinculante y si gana el no, las empresas no entran", dice con firmeza. **

De manera alternativa, la comunidad prefiere fortalecer otras actividades para promover el progreso económico y social de su municipio. Entre esas propuestas plantean el fortalecimiento del sector agropecuario y la apuesta al ecoturismo, aprovechando la cercanía con la ciudad de Bogotá y la riqueza ambiental con la que cuenta Arbeláez.

### Los obstáculos 

¿Está usted de acuerdo sí o no con que en el municipio de Arbeláez, Cundinamarca, se realicen actividades de sísmica, exploración y lavado de materiales de hidrocarburos y/o minería a gran escala?, es la pregunta que se realizará este domingo. Una consulta que ha estado marcada por contratiempos y obstáculos que buscan impedir la realización de la votación.

Luis Jaime Ortiz, manifiesta que el gobierno ha sido permisivo con las multinacionales como que buscan extraer petróleo como Alange Energy, filial de Pacific Rubiales, y Canacol Energy que pretenden desarrollar un megaproyecto extractivista en la región del Sumapaz. En ese sentido denuncian que **de 24 mesas que usualmente se usan para las votaciones en el municipio, para la consulta popular solo se permitirán 10 mesas para que voten cerca de 9 mil personas.**

"Eso quiere decir que serán 900 personas por mesa, así que tocará hacer filas.  Como reclamamos, en vez de 9, van a instalarse 10 mesas. Le hemos advertido a las personas para defender el agua, hay que madrugar", expresa el ambientalista.

Son **3600 personas** las que deberán responder la pregunta de la consulta para que esta sea válida, y así blindar su territorio de la actividad de hidrocarburos en** el bloque Cor adjudicado en 2010 a la empresa Australian Drilling Associates, y el bloque Cor 33, dado a Alange Energy.**

<iframe id="audio_19666778" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19666778_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
