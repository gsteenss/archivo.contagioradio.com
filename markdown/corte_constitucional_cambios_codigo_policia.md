Title: 6 cambios que la Corte Constitucional le ha hecho al nuevo Código de Policía
Date: 2017-05-31 20:03
Category: DDHH, Otra Mirada
Tags: código de policía
Slug: corte_constitucional_cambios_codigo_policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/esmad-presidente-santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: EFE 

###### [31 May 2017] 

Son varias las demandas que organizaciones sociales y algunos congresistas interpusieron al nuevo código de policía que **desde el 30 de enero entró en vigencia.** A la fecha el código ya cuenta con algunas modificaciones que le ha hecho la Corte Constitucional de acuerdo con las reclamaciones que se han presentado, asegurando que se trata de un código dictatorial y que afecta los derechos de la ciudadanía.

Una vez que el código superó todos los debates en el Congreso de la República,  las críticas apuntaron a que se trataba de unas normas que le otorgaban poderes exorbitantes a los agentes de policía. En medio de esa polémica desde distintos sectores se presentaron demandas contra el nuevo código, de tal manera que a mayo **ya se cuentan 35 acciones jurídicas contra esa normativa.**

De esas demandas, la Corte Constitucional se pronunció, y durante los primeros meses de este año ha modificado varios puntos, por medio de** siete fallos que ha emitido, eliminando o reformando algunos aspectos.**

### Órdenes judiciales 

Uno de los temas más polémicos era la posibilidad de que las autoridades pudieran ingresar a propiedad privada sin orden judicial. Frente a eso, la Corte ordenó que  solo podrá ingresar la policía solo si antes hay una revisión por parte de un juez.

### La protesta 

Se obligó al gobierno a cambiar el artículo 28 en un plazo máximo de dos años, de manera que sea garantizado el derecho a la reunión. Una decisión que se toma al considerar que la regulación de ese derecho no puede modificarse por medio de una ley ordinaria.

### Traslado de personas 

El alto tribunal estableció que una persona solo pueda ser trasladada, si el municipio cuenta con lugares adecuados al momento de retener a alguien. Asimismo, las autoridades deberán demostrar pruebas por las cuales deciden trasladar a un ciudadano.

### Vendedores informales 

El alto tribunal también ordena que se respete los vendedores que ocupan el espacio público en lugares donde tengan permiso de trabajar.  De esta manera queda prohibido desalojarlos o decomisarles su mercancía.

### Habitantes de calle 

Por considerarse como una medida “discriminatoria”, la Corte derogó la posibilidad de que los habitantes de la calle puedan ser trasladados.

### Ambiente 

Aunque se cometan desastres ambientales por actividades ilícitas, la Corte estableció que no se podrá destruir los elementos usados en  dichas acciones.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
