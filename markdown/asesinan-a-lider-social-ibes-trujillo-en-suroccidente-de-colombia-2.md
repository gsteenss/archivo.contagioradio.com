Title: Asesinan a líder social Ibes Trujillo en Suroccidente de Colombia
Date: 2018-07-17 15:02
Category: DDHH, Movilización
Tags: Argelia Cauca, Ibes Trujillo, marcha patriotica
Slug: asesinan-a-lider-social-ibes-trujillo-en-suroccidente-de-colombia-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/ibertrujillo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Hora724] 

###### [17 Jul 2018]

Organizaciones sociales y defensoras de derechos humanos del suroccidente de Colombia denunciaron el asesinato del líder social Ibes Trujillo, desaparecido desde el pasado 10 de julio, en la vereda Agua Blanca, Cauca, **cuando dos hombres y una mujer, que se encontraban armados, se llevaron a Trujillo y una persona más de la finca del líder social**.

El cadáver fue hallado en la rivera del Río Marilópez, en la vereda Las Brisas, del municipio de Suárez. Luego de que más de 400 personas realizaran un recorrido convocado este martes, para localizar al Trujillo. Asimismo las personas informaron que el cuerpo fue hallado en avanzado estado de descomposición, por lo que la Fiscalía General de la Nación asumirá el caso para los procesos correspondientes.

### **El secuestro de Ibes Trujillo** 

De acuerdo con la información recolectada por la Red de Derechos Humanos Francisco Isaías Cifuentes, una vez las personas armadas salieron de la finca con Trujillo y el joven que lo acompañaba, fueron obligados a abordar un vehículo y trasladados a una zona en donde se encontraba un grupo armado no identificado. **Allí, tanto Trujillo como el joven fueron amarrados a un tronco**.

El 11 de julio, en horas de la madrugada el joven fue desamarrado del tronco y recibió la orden de subirse a una chiva en dirección al municipio de Suarez. Hacía las 10 de la mañana, el joven llegó hasta el corregimiento de Timba, en donde dio este relato y afirmó que el líder social seguía amarrado al tronco, cuando él salió del lugar.

Ibes Trujillo era el Físcal del Consejo Comunitario de la Cuenca del  Río Timba Marilópez y director de la empresa comunitaria Brisas del Río Agua Blanca Audacia, además, era integrante de la Coordinación Nacional de Organizaciones y Comunidades Afrodescendientes - CONAFRO, del Proceso de Unidad Popular del Suroccidente Colombiano - PUPSOC y de la Coordinación Social y Política Marcha Patriótica Cauca. (Le puede interesar: ["Continúan los hostigamientos en contra de líderes sociales en el país"](https://archivo.contagioradio.com/dos-lideres-sociales-fueron-victimas-de-hostigamientos-durante-el-fin-de-semana/))

### **Ibes Trujillo fue encontrado por la Guardía Cimarrona**

Deivin Hurtado también se refirió al asesinato de Ibes Trujillo, líder social desaparecido desde el pasado 10 de julio y encontrado muerto, con signos de tortura, el pasado 17 de julio, por el despliegue que **hizo la Guardia Cimarrona acompañada por campesinos**.

Trujillo era un defensor de derechos humanos de las comunidades Afro en el municipio de Suarez y estaba desarrollando la dirigencia política y social de este territorio. (Le puede interesar:["Asesinan a líder social Ibes Trujillo en el suroccidente de Colombia"](https://archivo.contagioradio.com/asesinan-a-lider-social-ibes-trujillo-en-suroccidente-de-colombia-2/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
