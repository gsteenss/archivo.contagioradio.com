Title: Lucero Jaramillo, activa promotora de sustitución de cultivos fue asesinada en Caquetá
Date: 2019-04-09 17:38
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Caquetá, juntas de acción comunal, Plan de Sustitución de Cultivos de Uso ilícito
Slug: lucero-jaramillo-activa-promotora-de-sustitucion-de-cultivos-fue-asesinada-en-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/lideresaasesinadacaqueta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @casa\_la] 

El pasado 7 de abril en horas de la noche**, Lucero Jaramillo, secretaria de la Junta de Acción Comunal (JAC) de Puerto Valdivia, Curillo en Caquetá**, fue asesinada tras recibir 6 impactos de bala mientras se encontraba en el caserío La Novia. La lideresa, quien falleció mientras era trasladada a San José del Fragua para recibir asistencia médica, participaba activamente en los procesos de sustitución de cultivos de uso ilícito en su comunidad.

Según la información que ha trascendido, las autoridades ya han iniciado con las respectivas investigaciones, sin embargo hasta el momento se desconoce quiénes fueron los autores del crimen. Según datos de la Fundación Paz y Reconciliación, después de la salida de las antiguas FARC-EP del territorio, en la zona ha incrementado la presencia de disidencias y grupos paramilitares.

Esta preocupación también la expresó el asesor de la **Confederación Nacional de Acción Comunal Jaime Gutiérrez Ospina,** quien señala que en el departamento, además de evidenciarse la presencia de disidencias de las FARC, y de otros grupos armados que provienen del **Guaviare, Meta y Putumayo,** también se están presentando presiones por parte de las autoridades municipales contra los integrantes de las JAC para que se elijan  a ciertos candidatos de cara a los comicios regionales. [(Lea también: Líder indígena y reclamante de tierras Ebel Yonda, asesinado en Caquetá)](https://archivo.contagioradio.com/el-lider-indigena-y-reclamante-de-tierras-ebel-yonda-ramos-fue-asesinado/)

Por su parte, la organización **Fundación Red Desarrollo y Paz del Caquetá** rechazó el crímen y manifestó que los asesinatos de miembros de las Juntas de Acción Comunal están colapsando el proceso organizativo de la región, pero que a pesar de ello no lograrán "romper el tejido histórico de acciones constructoras de paz que aún en medio del conflicto, heroicamente hemos venido hilando".

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
