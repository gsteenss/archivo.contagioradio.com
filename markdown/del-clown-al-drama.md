Title: Del clown al drama
Date: 2018-03-15 16:52
Category: eventos
Tags: Bogotá, Cultura, teatro
Slug: del-clown-al-drama
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Idiota-3-_preview-e1520632534579.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La congregación teatro 

###### 09 Mar 2018

La Congregación Teatro bajo la dirección de uno de los dramaturgos más prometedores de la primavera teatral en Bogotá Johan Velandia presenta “Del Clown al Drama” llegara para cautivar al público.

Este evento llega en principio con la obra “Idiota”, que cuenta la historia del payaso Polenta idiota, interpretado por Milton Lopezarrubla, quien nos invita a una fiesta de cumpleaños en donde surgirán crisis y preguntas transcendentales.

“Trio, concierto Payaso”, es una apuesta en escena cómica y divertida, en el que un grupo de payasos hacen una exploración bufa alrededor del oficio del actor y la situación contemporánea en nuestro país, acompañados por ritmos musicales como blues, jazz, bolero, la carrilera y la ranchera.

La obra “El Ensayo” es un Thriller Costumbrista que se basa en hechos reales ocurridos en la comuna de Santo Domingo en la ciudad de Medellín un año después de la muerte de Pablo Escobar, donde tres ancianas demostraran que nunca es tarde para cobrar venganza así tengas 73 años.

Por último se presentara “Camargo” que a través del misterio y el suspenso se descubre la vida íntima y los oscuros parajes de uno de los asesinos y violadores en serie de Latinoamérica: Daniel Camargo Barbosa, historia de un hombre que hizo que esta compañía teatral escudriñara en la memoria de nuestro país para sacar a la luz la vida de un asesino.

Cuatro piezas teatrales que cuentan hechos dolorosos, penosos y cómicos que se tendrán lugar en La Sala Fabrica de Hechos Culturales, del 23 al 31 de marzo, cada obra tendrá dos días de función en el horario de las 8:00p.m. Habrá un bono de apoyo: 30.000 general y 80.000 para las cuatro obras.

Si desea conocer más sobre las obras, actores en escena y fechas puede consultar la página web de la SALA aquí.
