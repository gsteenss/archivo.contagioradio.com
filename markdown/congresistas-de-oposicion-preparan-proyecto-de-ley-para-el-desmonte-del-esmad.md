Title: Congresistas de oposición preparan proyecto de ley para el desmonte del ESMAD
Date: 2019-12-07 12:30
Author: CtgAdm
Category: Movilización, Política
Slug: congresistas-de-oposicion-preparan-proyecto-de-ley-para-el-desmonte-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-18.03.08.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Recientemente Temblores ONG publicó un informe en el que recoge los efectos de las actuaciones desmedidas de Escuadrón Móvil Antidisturbios (ESMAD) a 20 años de su creación y en el marco de este Paro Nacional, organizaciones defensoras de derechos humanos han hecho eco de este informe, señalando que la actuación de este cuerpo sigue siendo un factor que condiciona el normal desarrollo del derecho a la protesta. Por esa razón, desde el Congreso surge una iniciativa para desmontar el ESMAD, y crear un cuerpo de Policía que garantice el derecho a la manifestación.

### **¿Por qué desmontar el ESMAD?** 

En el informe de Temblores se describen el asesinato de, por los menos, 34 personas por armas que usa el ESMAD, incluyendo la del joven Dylan Cruz. Según la organización, este cuerpo creado en 1999 ha sido respaldado en sus acciones por los diferentes gobiernos que han estado en la Casa de Nariño, y en sus 20 años de historia, ha significado un riesgo para las manifestaciones citadas por estudiantes, campesinos e indígenas. (Le puede interesar: ["En veinte años el ESMAD ha asesinado a 34 personas"](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas/))

En el marco de las recientes movilizaciones sociales, defensores de derechos humanos señalan que el ESMAD ha hecho uso de sus armas menos letales de forma letal, mediante el empleo de municiones ilegales (como recalzadas) para afectar a los manifestantes. Asimismo, señalan que el empleo de la fuerza es excesivo, no responde a las necesidades de la protesta y en ocasiones, no respeta sus protocolos de actuación. (Le puede interesar: ["ESMAD usó material no convencional y violó protocolos en movilizaciones estudiantiles"](https://archivo.contagioradio.com/esmad-uso-material-no-convencional-y-violo-protocolos-en-movilizaciones-estudiantiles/))

Adicionalmente, según Temblores, por los asesinatos cometidos por el ESMAD aún no se establecen condenas contra sus miembros, razón por la que señalan que hay un silencio cómplice que permite a esta fuerza seguir actuando de forma desmedida contra los manifestantes. (Le puede interesar: ["Las razones para pedir el desmonte del ESMAD a 20 años de su creación"](https://archivo.contagioradio.com/las-razones-para-pedir-el-desmonte-del-esmad-a-20-anos-de-su-creacion/))

### **Se debe garantizar el derecho a la protesta, y evitar que se produzcan violaciones a los derechos humanos** 

El senador Antonio Sanguino manifestó que desde la oposición están construyendo una propuesta que tramitar la discusión sobre el papel que cumple el ESMAD desde un marco normativo, para que se garantice el derecho a la protesta, al tiempo que se eviten violaciones a los derechos humanos y afectaciones a este derecho fundamental. Para iniciar esta discusión, el próximo lunes 16 de diciembre realizarán una audiencia en la que junto a expertos y organizaciones sociales estudiarán experiencias de manejo de la protesta distintas a las que se han vivido en países como[Colombia, Chile o España.](https://archivo.contagioradio.com/unidades-antidisturbios-de-paises-democraticos-no-pueden-patear-la-constitucion-monedero/)

No obstante, el Senador señaló algunos de los puntos que serán claves en la discusión como la revisión de la doctrina que orienta la labor de la Policía y de este tipo de cuerpos especializados para atender los hechos de violencia que se presenten en el marco de la protesta. "Pero no solo revisar la doctrina, sino también los protocolos, porque la Policía no puede estimular los hechos de violencia que se presentan", afirmó, y aseguró que esta institución "debe tener una labor preventiva, y el uso racional y proporcional de la fuerza debe reservarse cuando haya momentos de alteración al orden público o para declaratorias de conmoción interior".

Por último, el Congresista sostuvo que a lo largo de las recientes manifestaciones se ha podido evidenciar que hay diferentes formas de permitir la acción del ESMAD, y "al parecer, el uso desmedido de la fuerza conduce a hechos de violencia en el marco de la protesta, y a una limitación a este derecho fundamental". Sin embargo, la iniciativa legislativa no podría ser presentada en esta legislatura porque las sesiones culminan el próximo 16 de diciembre, razón por la que Sanguino adelantó que la misma sería presentada en marzo de 2020.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
