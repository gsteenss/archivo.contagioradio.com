Title: Prográmese con las movilizaciones por la Paz
Date: 2016-10-12 12:05
Category: Nacional, Paz
Tags: #PazALaCalle
Slug: programese-con-las-movilizaciones-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz64.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 de Oct 2016] 

Durante el día de hoy se realizarán **diferentes movilizaciones en Colombia y el mundo, respaldando los acuerdos de paz firmados entre el gobierno y las FARC-EP**. En Bogotá las manifestaciones contarán con la participación de estudiantes, campesinos e indígenas que harán el corredor de las “2.500 flores por la paz”.

La agenda para la capital iniciará desde las 12:30 del mediodía, a esa hora aproximadamente **5.000 indígenas saldrán del colegio Claretiano hacía la Universidad Nacional** para unirse al movimiento estudiantil y  a los campesinos, en la movilización de hoy.

Sobre las **2:00pm iniciará la organización de la movilización que saldrá de la Universidad Nacional por la calle 26**. En este lugar ya hay delegaciones de diferentes universidades públicas y privadas del país, que estarán en Bogotá hasta el sábado 15 de octubre, cuando se realizará el Encuentro de Estudiantes por la Paz. Lea también ["Comunidad estudiantil se suma a los esfuerzos por Acuerdos Ya"](https://archivo.contagioradio.com/?s=Comunidad+estudiantil+se+suma+a+los+esfuerzos+por+Acuerdos+Ya)

A las 3:00pm la movilización se encontrará con el movimiento de víctimas en el Centro de Memoria, Paz y Reconciliación, se espera que sobre las 4.00pm se encuentren con los demás participantes de la marcha e inicie la organización del **corredor de 2.500 “flores por la paz”.**

En otras ciudades como **Cúcuta**, se llevará a cabo la marcha de los faroles, el punto de encuentro será el Parque Santander a las 7:00pm, en **Miranda – Cauca**  la cita es en el antiguo Seguro Social a las 6:30pm. En **Popayán** se llevará a cabo la actividad ¿Y si nos unimos por la paz?, en el lugar conocido como El Morro. En **Londres** – Inglaterra, el punto de encuentro será la Plaza de Trafalgar, a las 5:00pm.

A lo largo de la semana también se realizarán otras actividades y movilizaciones.

Nacionales:

**Apartadó:** Movilización Por la paz marchamos, el Viernes 14 de octubre, el punto de encuentro será la Alcaldía a las 5:00pm.

**Bucaramanga:** Se realizará la Gran marcha nacional de los faroles, el  Viernes 14 de octubre, el lugar de encuentro será la Plaza Cívica Luis Carlos Galán a las 6:00pm

**Buenaventura:** Se realizará la marcha por la paz y la reconciliación nacional, el  Viernes 14 de octubre el punto de encuentro será la Universidad del Pacifico a las 9:00am

**Carmen de Bolívar:** Se hará la Caminata pacífica el miércoles19 de octubre, el punto de encuentro será la Vereda de Caracolí, la hora esta por conrifar

**Cartagena:** Se llevará a cabo la movilización \#AcuerdosYA  el viernes 14 de octubre, en lugar de encuentro será el Mall Plaza a las 4:00pm

**Medellín:** Se hará la Gran Marcha Colombia pacífica, el 20 de octubre, el lugar de encuentro será el Parque de los Deseos a las 4:00pm

**Sincelejo:** se llevará a cabo el viernes 14 de octubre, la Marcha Blanca por la Paz, el punto de encuentro será la Plaza Cultural Majagual

Internacionales:

**Buenos Aires:** Se llevará a cabo un plantón en el Obelisco el viernes 14 de octubre a las 5:00pm.

En **Bruselas** se hará un plantón a las 5:30pm en el Place de la Monnaie el 14 de octubre

En **Sídney** el 15 de octubre se realizará un plantón en el Corcular Quay, la hora está por confirmarse.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
