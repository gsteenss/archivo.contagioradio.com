Title: Empezaron movilizaciones campesinas tras dos años de incumplientos del gobierno
Date: 2015-08-24 12:54
Category: Movilización, Nacional
Tags: 2013, ANZORC, César Jeréz, Cumbre Agraria Étnica y Popular, Paro Agrario, Polo Democrático Alternativo, Víctor Correa
Slug: empezo-jornada-de-movilizaciones-de-la-cumbre-agraria-tras-dos-anos-de-incumplientos-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Cumbre-Agraria-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [24 Ago 2015] 

Este lunes en todo el país se desarrolla una movilización tras dos años de **incumplimiento de los compromisos pactados por el gobierno durante el Paro Nacional Agrario** del año 2013. Además, los agricultores se movilizan en reclamación al **recorte presupuestal del 2016 para el sector agrario.**

En el mes de junio, voceros de la Cumbre Agraria se reunieron con el presidente Juan Manuel Santos para conocer las razones por las cuales el gobierno le había incumplido a los campesinos, sin embargo, **[la respuesta del presidente fue “vaga”](https://archivo.contagioradio.com/si-no-se-avanza-en-acuerdos-habra-movilizacion-en-agosto-cumbre-agraria/), y no se dio explicaciones frente a la falta de compromiso**, según explicó en ese momento César Jeréz de ANZORC.

Tras la poca atención que prestó el gobierno a las reclamaciones de los agricultores, las organizaciones campesinas que conforman la Cumbre Agraria Étnica y Popular, **en Antioquia los cafeteros han decidido reunirse en el municipio de La Pintada** “haciendo el llamado por un precio de sustentación, un fondo de estabilización que permita la compensación del precio del café”, explica un comunicado de la Cumbre Agraria.

Con esta jornada de movilizaciones, los campesinos esperan que el gobierno responda a sus exigencias o “**de lo contrario el gremio advierte que continuarán las movilizaciones hasta que se otorguen soluciones concretas para el sector**”, dice el comunicado.
