Title: Los tres defensores de DDHH postulados para Unidad de Búsqueda de Personas Desaparecidas
Date: 2017-07-25 16:35
Category: Nacional, Paz
Tags: CCEEUU, Desaparición forzada, MOVICE, Ruta Pacífica de las Mujeres, víctimas
Slug: tres-defensores-de-ddhh-postulados-para-unidad-de-busqueda-de-personas-desaparecidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Diseño-sin-título.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [25 Jul. 2017]

Más de 1000 organizaciones sociales y de mujeres, defensores de derechos humanos, gestores de paz, víctimas, entre otros dieron a conocer a través de una carta la postulación de **Carlos Rodríguez Mejía, Luz Marina Monzón y Henry Ramírez Soler como posibles candidatos** para ocupar el cargo de director/ra de la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD).

En la postulación aseguran que estas tres personas cumplen con los requisitos para ser postuladas y que además tienen las características idóneas para el cargo. Le puede interesar: [En Septiembre se conocerán los nombres de magistrados del Tribunal de Paz](https://archivo.contagioradio.com/en-septiembre-se-conoceran-los-nombres-de-magistrados-del-tribunal-de-paz/)

“Consideramos que **de ser elegida alguna de estas personas, se garantizará el cumplimiento efectivo del mandato de la Unidad**. Esto en el marco de una dirección ética y comprometida con las víctimas, garantizando su participación activa en las diferentes etapas del proceso de búsqueda” manifiesta la carta. Le puede interesar: [Víctimas proponen criterios para escoger integrantes del SIVJRNR](https://archivo.contagioradio.com/criterios-integrantes-del-sivjrnr/)

### **¿Quiénes son los postulados?** 

**Carlos Rodríguez Mejía** ha sido reconocido por su trabajo desde la Comisión Colombiana de Juristas ante la Comisión de Búsqueda de Personas, es un abogado, catedrático y defensor de derechos humanos, que es recordado por su trabajo con las víctimas y organizaciones de DDHH.

Además, **por su trayectoria es conocedor del tema de desaparición forzada** desde el cual participó en la elaboración y adopción del Plan Nacional de Búsqueda de Personas Desaparecidas.

**Luz Marina Monzón** desde su labor como consultora e investigadora ha trabajado temas relacionados con la desaparición forzada. Como magister en Derecho y catedrática ha logrado recoger experiencia que le ha permitido el litigio de casos sobre desaparición forzada en Colombia y ante el Sistema Interamericano de DD.HH.

**Ha representado y asesorado a víctimas de desaparición forzada,** entre ellas el caso de la Comuna Trece en la ciudad de Medellín. Así mismo, tiene un especial trabajo con las mujeres, que la hacen trabajar de manera diferencial el conflicto y el impacto de este sobre las mujeres.

**Henry Ramírez Soler** Con un recorrido de más de 21 años, este sacerdote, miembro de congregación de Misioneros Claretianos, **ha acompañado a familias de víctimas de desaparición forzada en labores de búsqueda, exhumación,** identificación y entrega de manera digna. Adicionalmente, ha logrado reconstruir la confianza entre los familiares y las instituciones nacionales e internacionales aportando así al Protocolo Humanitario de Exhumaciones, experiencia que fue compartida y tenida en cuenta por las partes en la Mesa de Negociación de La Habana.

### [**Víctimas esperan sean tenidas en cuenta estas postulaciones **] 

Las más de 1000 organizaciones que son parte del Movimiento de Víctimas de Crímenes de Estado (Movice), La Ruta Pacífica de las Mujeres, La Coordinación Colombia, Europa, Estados Unidos (CCEEUU) y otras 106 organizaciones de víctimas y ecuménicas, le manifestaron al Comité de Selección que esperan se tengan en cuenta estas candidaturas. Le puede interesar: [La ardua tarea que tendrán los integrantes de la Comisión de la Verdad](https://archivo.contagioradio.com/comienza-convocatoria-para-integrantes-de-la-comision-de-la-verdad/)

“Las cuales son promovidas por organizaciones que desde hace décadas realizan un trabajo arduo por encontrar a todas las personas desaparecidas en Colombia”. Le puede interesar: [Así serán seleccionados las y los jueces para la Jurisdicción Especial de Paz](https://archivo.contagioradio.com/asi-seran-seleccionados-las-y-los-jueces-para-la-jurisdiccion-especial-de-paz/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
