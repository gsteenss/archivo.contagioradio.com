Title: Aumentar impuestos o reducir el gasto para corregir hueco fiscal
Date: 2016-01-06 00:46
Category: Economía, Nacional
Tags: Aumento del IVA, Colombia OCDE, Reforma tributaria
Slug: aumentar-impuestos-o-reducir-el-gasto-para-corregir-hueco-fiscal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/subida-iva.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [5 Ene 2016]

En el más reciente informe presentado por la Comisión de expertos que acompaña la formulación de la reforma tributaria estructural que el ejecutivo busca implementar en 2016, se sugiere al gobierno nacional realizar ajustes a todos los gravámenes, empezando por el Impuesto sobre el Valor Agregado IVA, afectando consecuentemente el bolsillo de todos los colombianos.

En su propuesta de incrementar el IVA en 2 puntos, del 16 al 18%, la comisión aduce que es el porcentaje más cercano a sus similares en América Latina y a los países miembros de la Organización para la Cooperación y el Desarrollo Económicos OCDE, a la que Colombia aspira a ingresar.

El grupo de expertos agrega en su informe que otro de los problemas radica en la falta de recaudo del sistema tributario colombiano en el que se recolecta únicamente el equivalente al 20% del PIB, mientras en otros países de la región corresponde al 21% y más del 30 en las que pertenecen a la OCDE.

De acuerdo con lo manifestado por el ministro de Hacienda Mauricio Cárdenas, el informe de la Comisión es preliminar y debe ser socializado como la base para la discusión del tema, en el que deben intervenir además otros insumos provenientes de la OCDE y el Fondo Monetario Internacional.

El jefe de la cartera, aseguró además que el gobierno no le subirá la tributación a las empresas y buscará reducir el gasto público para lograr cubrir el déficit fiscal, lo que contrasta con el accionar estatal, donde el gasto de funcionamiento ha crecido, de 2010 a la fecha, en más del 50% mientras que la inflación en ese mismo ha sido de alrededor del 22% .

Otro de los puntos que aborda el informe, esta relacionado con el control sobre las entidades sin ánimo de lucro, con un programa que debería incorporar la DIAN en su plan anual de fiscalización, refiriéndose a que hay 71.662 entidades, de las cuales 16.729 son no contribuyentes, y el resto (54.933) pertenecen al régimen tributario especial.

La gran dificultad en ese sentido es la distinción de cuáles entidades pueden clasificar como "Sin ánimo de lucro" y las que no podrían ser gravadas con impuesto a la renta, al patrimonio o a la renta presuntiva entre otros, con lo que provocaría en muchos casos la desaparición de aquellas que no cuenten con la capacidad de cubrir sus obligaciones tributarias.

#####  
