Title: FARC propone crear un Fondo Especial para la Reparación
Date: 2015-02-04 21:07
Author: CtgAdm
Category: Economía, Paz
Tags: DDHH, FARC, proceso de paz, víctimas
Slug: farc-propone-crear-un-fondo-especial-para-la-reparacion
Status: published

##### [Foto: www.pulzo.com]

Por medio de un comunicado, este miércoles la guerrilla de las **FARC** dio a conocer su **propuesta de reparación de las víctimas**, con el objetivo de garantizar el reconocimiento pleno de los derechos.

En el comunicado se propone la conformación del Fondo Especial para la **Reparación Integral (FERI)**, por medio del cual se quiere **planear una reparación integral de las víctimas individuales y colectivas del conflicto,** así mismo se formulará e implementará del “**Plan Nacional para la Reparación Integral de las víctimas** del conflicto y se conformará el “Consejo Nacional para la Reparación" entre otras medidas.

Así mismo, debido a que en las negociaciones de la Habana se está teniendo en cuenta una perspectiva de género, las **FARC proponen reparar a víctimas LGBTI,** ya que ven como necesario  incluir un énfasis sobre estas comunidades que se han visto afectadas  por el conflicto armado.

Por otro lado, muestran su intención de hacer un reconocimiento especial de los **derechos de las comunidades campesinas, indígenas y afrodescendientes** que han sido víctimas de guerra, con el fin de “**reparar los derechos a la vida y a la paz, a la tierra y al territorio, al agua y al medio ambiente** sano, a producir alimentos y a la participación política y social”. Igualmente se tienen en cuenta los derechos de las organizaciones políticas, sociales y sindicales, y finalmente los derechos de las **víctimas del desarrollo y de la política económica.**

Todo esto, para poder incluir la diversidad de las víctimas del conflicto, integrando cada una de condiciones sociales, sexuales y políticas de los colombianos y colombianas afectados por los más de 50 años del conflicto armado.
