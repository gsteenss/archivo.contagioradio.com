Title: Enfrentamientos armados producen el desplazamiento de 85 familias en Chocó
Date: 2020-11-08 18:24
Author: PracticasCR
Category: Actualidad, DDHH
Tags: Chocó, Desplazamiento forzado, Docordó, ELN
Slug: enfrentamientos-armados-producen-el-desplazamiento-de-85-familias-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/docordo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Docordó, Chocó/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 7 de noviembre en horas de la mañana arribaron a Docordó, cabecera municipal del Litoral San Juan, Chocó, **85 familias afrodescendientes de las comunidades de Peñita y Pángala desplazadas producto de la** intensidad de los operativos militares y enfrentamientos entre el Ejército y el ELN.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Operativos del Ejército en Chocó continúan

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según la [Comisión Interclesial de Justicia Paz,](https://www.justiciaypazcolombia.com/) los desplazamiento son producto de los **operativos  militares que por aire, agua y tierra las Fuerzas Armadas desarrollan contra el ELN**, afectando a las  comunidades indígenas y negras de ** Unión Wounaan, Noanamá y a las comunidades afro del  Consejo Comunitario de Nóvita,** lo que llevó al posterior desplazamiento de la comunidad de Peñita y Pángala. [(Lea también: Muere tercer niño indígena por confinamiento forzado producto de enfrentamientos)](https://archivo.contagioradio.com/muere-tercer-nino-indigena-por-confinamiento-forzado-producto-de-enfrentamientos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Santiago Mera, integrante de la Comisión, señaló que ha sido difícil establecer contacto con las familias desplazadas, para establecer su situación actual, **ya que sobre ellas se ha cernido un sentimiento de miedo y zozobra, lo cual; las lleva a no responder ni siquiera su teléfono por miedo a amenazas e intimidaciones.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También agregó que **hoy lunes, desde tempranas horas de la mañana una delegación de la Comisión se estaba dirigiendo a Docordó, con el fin de poder entrevistarse de manera presencial con las familias desplazadas para prestarles apoyo humanitario.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el pasado 4 de noviembre se iniciaron operaciones militares contra el ELN en el Litoral San Juan y al día siguiente, 5 de noviembre desde las 6:00 a.m. continuaron los enfrentamientos por dos horas. Dichos operativos al sur del Chocó y en los límites con el Valle del Cauca han continuado tras la muerte de Andrés Felipe Vanegas, alias “Uriel”, integrante del ELN. [(Lea también: Con operativo contra alias “Uriel”, Gobierno cierra más la puerta a salida negociada con el ELN)](https://archivo.contagioradio.com/con-operativo-contra-alias-uriel-gobierno-cierra-mas-la-puerta-a-salida-negociada-con-el-eln/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En el desarrollo de las mismas fueron detenidos **11 personas a quiénes se les vincula en operaciones con el tráfico de drogas.** Por su parte, habitantes de la zona rechazan estas versiones y señalan que se trataría de **un montaje judicial contra algunos líderes y pobladores a quienes se les querría acusar de apoyar al ELN** y de vincular al narcotráfico.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Las denuncias de la comunidad son que los están señalando falsamanetede ser colaboradores del ELN. Estamos ante un territorio donde hay presencia de la Fuerza Pública, del ELN y de estructuras herederas del paramilitarismo; por eso como en todo territorio donde hay conflicto, la población civil es la más afectada, quedando en el medio de la confrontación y llegando a ser acusada por parte de la institucionalidad de ser "colaboradores".
>
> <cite>Santiago Mera, integrante de la Comisión Intereclesial de Justicia y Paz</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Según la organización de DD.HH, las comunidades étnicas del Litoral San Juan temen el **recrudecimiento de la violencia en sus territorios, por lo que hacen un llamado de carácter humanitario al Estado para respetar el Derecho Internacional Humanitario.** De igual forma llaman a ELN a asumir su responsabilidad humanitaria, por lo que claman, exista un **Acuerdo Humanitario y se desarolle una misión de verificación de la mano de la Defensoría del Pueblo** y organismos humanitarios. [(Lea también: Tras cuatro años de la firma del Acuerdo de Paz, la deuda más grande es con el campo colombiano)](https://archivo.contagioradio.com/tras-cuatro-anos-firma-del-acuerdo-de-paz-la-deuda-mas-grande-es-con-el-campo-colombiano/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El departamento del Chocó, no solo enfrenta el accionar del ELN sino también el de grupos paramilitares, evidenciado en un aumento del control territorial de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) en las áreas rurales del Bajo Atrato y Bojayá.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
