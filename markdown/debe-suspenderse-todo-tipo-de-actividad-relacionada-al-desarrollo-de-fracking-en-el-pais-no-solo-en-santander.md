Title: Debe suspenderse todo tipo de actividad relacionada al desarrollo de fracking en el país, no solo en Santander
Date: 2019-07-12 18:30
Author: CtgAdm
Category: Ambiente, Judicial
Tags: Conocophilips, Exxon Mobil, fracking, Parex
Slug: debe-suspenderse-todo-tipo-de-actividad-relacionada-al-desarrollo-de-fracking-en-el-pais-no-solo-en-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/impactos-del-fracking-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: aimdigital] 

La **Autoridad Nacional de Licencias Ambientales**, (ANLA) decidió suspender la licencia piloto de fracking de Ecopetrol que se llevaría a cabo entre **Barrancabermeja y Puerto Wilches, Santander,** esto en cumplimiento de la decisión del Consejo de Estado de noviembre de 2018 de suspender el marco normativo del fracking en Colombia.

Para Luis Enrique Orduz, abogado de la Corporación Podion y vocero de la Alianza Libre de Fracking la decisión de la ANLA representa la afirmación de **un argumento que se venía sosteniendo desde noviembre del 2018 cuando el Consejo de Estado suspendió esta normativa** en el marco de una acción de nulidad que interpuso la Universidad del Norte en contra de un decreto y una resolución que reglamentaba el fracturamiento hidráulico horizontal en Colombia.

Para la Alianza Libre de Fracking esta resolución debería verse reflejada en la suspensión de las actividades que vienen realizando otras empresas com**o Conocophilips, Exxon Mobil y Parex** en otros lugares del país, "nuestro llamado a las autoridades que entregan permisos y a la ANLA, es que suspendan todos esos procesos porque deben dar aplicación a la medida cautelar", indicó.

Orduz explica, frente a la decisión que pueda tomar el Consejo de Estado con relación al fracking, que no hay una fecha de caducidad, advirtiendo que la medida cautelar interpuesta en 2018 que suspendió diversos trámites para proyectos de exploración de yacimientos no convencionales, es solo una parte del proceso.

> Con la suspensión desde noviembre de 2018 del marco legal que reglamenta los yacimientos no convencionales en Colombia la ANLA, la ANH y las demás instituciones con competencia sobre el asunto deben suspender todo tipo de actividad tendiente al desarrollo de YNC en el país <https://t.co/W9VxVNziUC> [pic.twitter.com/UkewYTcwuy](https://t.co/UkewYTcwuy)
>
> — Luis Enrique Orduz (@leorduz\_89) [12 de julio de 2019](https://twitter.com/leorduz_89/status/1149705736521474048?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **La verdadera decisión entorno al Fracking** 

A lo que realmente hay que prestarle atención según el abogado es a **la decisión que tomen los expertos de la Universidad Nacional** quienes evaluarán los posibles riesgos y beneficios de usar esta técnica. Adicionalmente  el 19 y 29 de julio habrá una audiencia de testimonios y declaraciones que rendirán personas solicitadas por el Ministerio de Minas y Ecopetrol  frente a la implementación del fracking en Colombia. [(Lea también: Consejo de Estado pide nueva comisión de expertos sobre fracking)](https://archivo.contagioradio.com/consejo-de-estado-pide-nueva-comision-de-expertos-sobre-fracking/)

El abogado concluye que mientras se toma una decisión al respecto, la opinión pública debe prestar atención a la región del Magdalena medio, pues lo que suceda en este territorio podría verse replicado en los yacimientos no convencionales de otras regiones de Colombia como en la cordillera oriental.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_38372761" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38372761_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
