Title: El cambio climático, tema central de FESTIVER 2016
Date: 2016-04-18 16:24
Author: AdminContagio
Category: 24 Cuadros, Ambiente
Tags: cine y ambiente, Festival cine Barichara, Festiver 2016, Toto Vega
Slug: el-cambio-climatico-tema-central-de-festiver-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/festiver-e1460992069855.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Prensa FESTIVER 

##### [18 Abr 2016]

En 2016, el Festival de Cine verde de Barichara vuelve en su sexta edición, con el propósito de seguir transformando y educando en materia ambiental a través del cine, teniendo para este año dos temas centrales que son esenciales para el futuro del país y del mundo: el cambio climático y la paz.

Para Toto Vega, co creador del Festival, la riqueza natural de Colombia en contraste con las acciones que atentan contra la biodiversidad, le dan a FESTIVER un "valor enorme" entendiendo que desde el audiovisual se puede promover "la conciencia de lo que tenemos, como lo hacemos y proyectarnos para conservarlo y protegerlo".

De la convocatoria, abierta hasta el próximo 3o de junio, se conformará la ***Competencia oficial***, con las producciones "verdes", entendidas desde la organización como aquellas que abordan "todo lo que afecte al hombre", en los formatos corto y largometraje, tanto ficciones como documentales y animaciones.

Adicionalmente, en la categoría ***Fotosíntesis,  ***pueden participar las producciones que han sido rodadas y que se encuentren en etapa de finalización, para obtener como premio la colorización, mezcla final de sonido y DCP para culminar su película, mientras que en la categoría ***Cogollos verdes ***los estudiantes y nuevos realizadores tendrán la oportunidad de mostrar su talento enviando sus cortometrajes al Festival.

Los guiones con temática verde, podrán hacer parte de la categoría ***Verde que te quiero verde,*** recibiendo como premio una residencia en Barichara donde podrán, con la asesoría de expertos, corregir y optimizar sus trabajos, participando además de todas las actividades a desarrollar durante la semana del Festival, incluidas conferencias, clases magistrales, encuentros y talleres académicos.

Las producciones interesadas en participar deben ajustarse a las [bases de la convocatoria](http://festiver.org/bases/). Los resultados de la convocatoria se publicarán el 1ero de Agosto, en www.festiver.org , portal donde encontrará además el listado de lo que el Festival ha denominado: ***Medicine, remedios para la buena salud del cine, *** que son talleres gratuitos sobre producción audiovisual y sobre medio ambiente.

Barichara, ademas de ser uno de los municipios más bellos de Colombia, aporta a la conservación ambiental desde el tipo de materiales que se utilizan para la construcción de sus viviendas, un ejemplo en la defensa de los recursos naturales, causa común de varias comunidades que habitan en el departamento de Santander.

<iframe src="http://co.ivoox.com/es/player_ej_11210612_2_1.html?data=kpafk5WadZOhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncbXj1dSYuMrLpYampJCzp7i4jbe5s5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
 
