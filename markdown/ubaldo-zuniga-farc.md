Title: Implementación del acuerdo es el centro del debate al interior de FARC
Date: 2020-06-28 11:25
Author: CtgAdm
Category: Actualidad, Paz
Tags: acuerdo de paz, Agencia Nacional de Reincorporación, Rodrigo Londoño
Slug: ubaldo-zuniga-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/partido_farc_afp_12_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Partido Farc

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Recientemente a través de medios de comunicación se dio a conocer la decisión tomada por el partido Fuerza Alternativa Revolucionaria del Común (FARC) frente a la salida de cuatro líderes históricos de su colectividad entre ellos **Andrés París, Benedicto González, Ubaldo Zuñiga y Fabián Ramírez.** Aunque señalan que existen diferencias con respecto a la forma en que se toman decisiones desde la dirección del Partido, afirman que no han sido notificados de su salida.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Ubaldo Zuñiga, director de la cooperativa Economías Sociales del Común - (ECOMUN),** señala que nadie le notificó su salida del partido y que al ser uno de los miembros fundadores del partido y ser integrante del Consejo Nacional de los Comunes no puede ser expulsarlo del mismo por parte del Consejo Político Nacional de la colectividad.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Las cosas se resuelven de forma participativa y democrática, es un sector de la dirección que se hace llamar el núcleo que son los que tienen esa posición frente a cómo se construye un partido, sin embargo, el debate va a seguir dándose"

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Atribuye que estas divisiones surgen en medio de algunas voces que no comparten la forma en que se toman decisiones o cómo se está llevando el proceso de reincorporación, "es muy lamentable que al interior de un partido que está en construcción y que en ese transito algunos no alcancen a asimilar y ser capaces de desprender sus imposiciones, así no funcionan las cosas en la democracia, ese es el fundamental problema" afirma Zuñiga refiriéndose a quienes hicieron parte del Secretariado y que hoy hacen parte del Consejo Político Nacional. [(Le recomendamos leer: ¿Se agota la esperanza de paz de Iván Marquez y 'El Paisa'?)](https://archivo.contagioradio.com/esperanza-paz-de-ivan-marquez/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las diferencias al interior del partido FARC

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Con relación a las diferencias que existen entre las facciones del partido, Zuñiga se refirió en particular a lo que ocurre al interior del **Consejo Nacional de Reincorporación** (CNR), donde trabajan componentes del Gobierno y de FARC adelantando la implementación del Acuerdo y donde se definió una ruta de reincorporación que no incluyó a Ecomún, lo que ha llevado a diferencias con dicho escenario planteado por Pastor Alape, representante del Partido.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Vamos a denunciar esa ruta porque no tiene en cuenta el Acuerdo" explica el presidente de la cooperativa, pues únicamente cubriría a los Espacios Territorial de Capacitación y Reincorporación (ETCR), **mientras que el 70% de la población de excombatientes se encuentra fuera de estas áreas,** "el partido no lo estamos dividiendo, al contrario, estamos tratando de sumar a esa gente que salió de esos espacios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, **Benedicto González firmante de Acuerdo de paz, e integrante del Consejo Nacional de los Comunes (CNC)**, también mencionado sobre su salida al interior del partido, explica que tampoco ha sido informado por parte de la dirección de la colectividad por lo que ha interpuesto recursos legales para tener claridad y una posición oficial al respecto.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a ello, señala que existe "una pretensión en este sentido por parte de un sector de la dirección del [Partido](https://twitter.com/PartidoFARC) (...) cuya motivación y accionar es a todas luces contraria al Acuerdo de Paz" que se ve directamente afectado además del incumplimiento por parte del Estado por lo que señala, son contradicciones internas. [(Lea también: Incumplimientos al Acuerdo de paz ¿Están fragmentando al partido FARC?)](https://archivo.contagioradio.com/incumplimientos-al-acuerdo-de-paz-estan-fragmentando-al-partido-farc/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

González rechazó el apoyo que mostró Rodrigo Londoño a nombramientos como al hijo de Jorge 40 como responsable del tema de víctimas del Ministerio del Interior o la muestra de aprobación del ascenso de oficiales como Nicasio Martinez señalados de tener relaciones con interceptaciones ilegales, ejecuciones extrajudiciales y corrupción administrativa por integrantes del partido como Carlos Antonio Lozada.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Se reafirma el compromiso con la paz

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El firmante de la paz señala que se busca eliminar toda opinión crítica o divergente y que las diferencias - naturales en toda colectividad - "deberían ser resueltas mediante el diálogo y el consenso, y no a través de la exclusión y el revanchismo, negando la posibilidad del debate democrático". [(Lea también: La paz no la detiene nadie, afirman excombatientes de FARC)](https://archivo.contagioradio.com/la-paz-no-la-detiene-nadie-afirman-excombatientes-de-farc/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Urge que estas acciones contrarias al Acuerdo Final de Paz cesen de manera definitiva, que se cambie el rumbo" afirma González mediante su comunicado, coincidiendo con Zuñiga en que es necesario acompañar a los excombatientes que viven en los territorios "donde muchos se sienten abandonados" [(Lea también: Partido FARC solicitará formalmente medidas cautelares de la CIDH a favor de excombatientes)](https://archivo.contagioradio.com/partido-farc-solicitara-formalmente-medidas-cautelares-de-la-cidh-a-favor-de-excombatientes/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Mi compromiso es muy serio frente a la construcción de la paz y la Colombia que siempre hemos soñado y que tengamos intereses diferentes no significa que estemos pensando en volver a las armas", afirma Ubaldo Zuñiga quien señaló que se trata de un debate ideológico, y que el tratar de asociar estas diferencias a un regreso a las armas no tiene nada que ver con la realidad, **"mi compromiso es al 120%, le dedico todo el día al trabajo por la reincorporación y la construcción de la paz haciendo trabajo con las cooperativas, eso es más que claro"**, concluye. [(Le recomendamos leer: "El Acuerdo es mucho más grande que unas personas que se apartan")](https://archivo.contagioradio.com/acuerdo-grande-personas-apartan/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la colectividad del Partido Farc, con respecto a la expulsión de los cuatro dirigentes se ha optado por no dar explicaciones públicas, mientras Rodrigo Londoño afirmó que en su momento “esas decisiones deben ser notificadas personalmente a los afectados para mantener el debido proceso". [(Lea también: El miedo que sentíamos en la guerra, es el miedo que ahora sentimos al salir de casa: excombatientes)](https://archivo.contagioradio.com/el-miedo-que-sentiamos-en-la-guerra-es-el-miedo-que-ahora-sentimos-al-salir-de-casa-excombatientes/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
