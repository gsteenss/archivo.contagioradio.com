Title: ELN pactará otro cese al fuego cuando se "superen" falencias del cese actual
Date: 2017-12-26 10:15
Category: Nacional, Paz
Tags: Cese al fuego ELN, diálogos con el ELN, ELN, Gustavo Bell, mecanismo de verificación
Slug: eln-pactara-otro-cese-al-fuego-cuando-se-superen-falencias-del-cese-actual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Eln-gobierno-cese.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Dic 2017] 

Debido al “colpaso” del Mecanismo de Veeduría y Verificación, el Ejército de Liberación Nacional, realizó un pronunciamiento sobre asuntos que están relacionados con el Cese al Fuego Bilateral y **confirmó su retiro del Mecanismo de Veeduría**. Indicaron que, “las posiciones intransigentes” del Gobierno Nacional, “hace más complejo y riesgoso” continuar con el cese al fuego bilateral. Por su parte, el jefe de la delegación del Gobierno Nacional, informó a través de medios de comunicación que se tienen que tomar correctivos para que el cese continúe.

En una serie de trinos, el Comando Central del ELN afirmó que el Gobierno Nacional **“ha pretendido cambiar el espíritu de lo acordado** en el cese bilateral al fuego y renegociar los acuerdos pactados”. Indicaron que de 40 casos entregados a la instancia nacional, sólo se resolvieron 5 y además, el Gobierno Nacional, en octubre y septiembre, desplegó operaciones ofensivas contra dos campamentos de esa guerrilla en Chocó y Norte de Santander.

También afirmaron que el Gobierno “ha aumentado sus operaciones de copamiento militares y operaciones de registro y control”, en lugares donde se encuentran las fuerzas del ELN. Ante estos hechos, dice el ELN que el Gobierno **“no reconoció sus actuaciones como ofensivas**, por el contrario, reiteró en la Mesa Transitoria, que continuará realizando ataques cada que considere que el ELN actúa por fuera del acuerdo del Cese”. (Le puede interesar: ["Los retos para los nuevos equipos en la mesa de diálogos entre Gobierno y ELN"](https://archivo.contagioradio.com/gustavo-bell-y-antonio-garcia-nuevos-aires-para-la-mesa-en-quito/))

### **Mecanismo de Veeduría y Verificación está estancado** 

En lo que respecta a la salida de este mecanismo por parte del ELN, argumentaron que se ha desconocido el Mecanismo como “la instancia que debe cumplir el objetivo de **prevenir incidentes, responder oportuna** y ágilmente ante las eventualidades o contingencias e informar y calificar incidentes”.

Además, afirmaron que se ha invalidado el Mecanismo debido a que el Gobierno “se ha adjudicado unilateralmente **la potestad de determinar** cuáles hechos o actuaciones del ELN están en contravía del acuerdo del cese sin acudir al Mecanismo de Verificación”.

También hacen alusión a que, **no se ha cumplido la parte esencial del acuerdo** de cese al fuego en la medida en que se ha reducido a la suspensión de operaciones militares, pero no he ha mejorado la situación humanitaria de la población. Esto, de acuerdo con el ELN, va en contravía de lo acordado entre las partes. (Le puede interesar: ["ELN plantea la posibilidad de extender cese al fuego bilateral en 2018"](https://archivo.contagioradio.com/eln-plantea-la-posibilidad-de-extender-cese-al-fuego-bilateral-en-2018/))

En cuanto a las actuaciones de la Fuerza Pública, dice el ELN, que el Gobierno “se ha negado a **reconocer su responsabilidad en asesinatos** y atentados contra la vida de personas civiles” ocasionados por los militares. Dicen que estos hechos no fueron verificados ni calificados por el Mecanismo de Verificación.

### **Nombramiento de Gustavo Bell es un “campanazo” para la negociación** 

Frente al nombramiento del Gustavo Bell como nuevo jefe de la delegación del Gobierno Nacional para la negociación con el ELN en Quito, esa guerrilla reiteró que el nombramiento **coincidió con la retirada de los representantes del ELN** del Mecanismo de Verificación “como protesta por el estancamiento del mecanismo tripartita”.

Para ellos, este nombramiento se entiende como **una medida de urgencia** por parte del Gobierno Nacional “para mejorar su presencia en las conversaciones”. También, lo han calificado como una “campanada de alerta” teniendo en cuenta la situación por la que atraviesan las negociaciones y el cese al fuego bilateral.

Finalmente, el ELN enfatizó en que **cumplirán lo pactado en el cese al fuego** que va hasta el 9 de enero como se pactó. Indicaron que, hasta que no se resuelvan los desacuerdos frente al Mecanismo de Verificación, la delegación del ELN no participará en él. También contemplan la posibilidad de crear otro cese al fuego siempre y cuando se haga con una evaluación previa y “se superen las falencias actuales”.

###### Reciba toda la información de Contagio Radio en [[su correo]
