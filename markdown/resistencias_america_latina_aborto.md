Title: Las resistencias en América Latina desde la interrupción voluntaria del embarazo
Date: 2017-09-27 21:00
Category: Libertades Sonoras
Tags: aborto, Interrupción Voluntaria del Embarazo
Slug: resistencias_america_latina_aborto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/aborto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Asuntos del sur] 

###### [27 Sep 2017] 

En el marco del **Día de Acción Global por un Aborto Legal, Seguro y Gratuito,** en \#LibertadesSonoras hablamos sobre la situación de la interrupción voluntaria del embarazo en América Latina y especialmente de algunas iniciativas para reivindicar los derechos de las mujeres respecto a este tema, en un país donde se realizan** **400.000 abortos al año, de los cuales solo 3400 se hacen de manera segura.

Una de esas iniciativas que han nacido en Colombia se llama '**Parceras', una línea y red feminista de acompañamiento en aborto,** que se lanza este 28 de septiembre, y con quienes hablamos sobre su labor desde la resistencia feminista, entendiendo que la penalización del aborto constituye un tipo de violencia hacia las mujeres.

Asimismo, conocimos otras formas de resistencia en Latinoamérica. Hablamos con otras redes de acompañamiento como '**Las comadres' en Ecuador,  y 'Serena Morena' de Perú.** ¿Por qué es importante crear estas redes de acompañamiento? ¿Cómo hacer llegar la información a mujeres de todos los lugares? ¿Cómo trabajar para despenalizar socialmente la interrupción del embarazo? Fueron algunas de las respuestas que nos compartieron las invitadas.

<iframe id="audio_21139443" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21139443_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
