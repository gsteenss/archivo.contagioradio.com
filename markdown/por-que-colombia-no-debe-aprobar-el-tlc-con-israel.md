Title: ¿Por qué Colombia no debe aprobar el TLC con Israel?
Date: 2016-06-12 08:30
Category: Economía, Nacional
Tags: Israel, Palestina, TLC
Slug: por-que-colombia-no-debe-aprobar-el-tlc-con-israel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/palestina-e1465594097366.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Flicker 

###### 12 Jun 2016 

Actualmente en el Congreso de la República se debate el TLC entre Colombia e Israel, que de acuerdo con la campaña colombiana de Boicot, Desinversión y Sanciones a Israel, BDS, y diversos congresistas, **brindará pocos beneficios a la economía colombiana y en cambio al aprobarlo se estaría apoyando y  legitimando la ocupación ilegal de Palestina** y las violaciones del derecho internacional  humanitario por parte de ese país.

El TLC fue firmado por ambos gobiernos en el año 2013, y ya fue aprobado en primer debate por ocho senadores en la Comisión Segunda del Senado, sin embargo, desde el Congreso también se escuchan voces en contra de este TLC.

Iván Cepeda, senador del Polo Democrático Alternativo, en uno de los debates desarrollados en la semana en la Comisión Segunda, habló sobre las violaciones a los derechos humanos a los que se ven sometidos los palestinos tras la ocupación Israelí. En ese sentido, indicó que la diplomacia colombiana se ha declarado en contra de estas violaciones del Derecho Internacional, de manera que **la postura del gobierno debería ser coherente con lo que ya ha declarado.**

De acuerdo con datos de la DIAN, de las 312 empresas israelíes que exportaron sus productos a Colombia entre agosto de 2014 y agosto de 2015, al menos cinco tienen sus sedes en territorios ocupados ilegalmente por Israel desde 1967. Además, otras empresas israelíes con domicilio tienen presencia en dichos territorios, sin que esto se reconozca públicamente.

Según un estudio realizado por el gobierno colombiano, **dos de los tres rubros de las importaciones colombianas desde Israel sobre las cuales se proyectan aumentos más importante serán aeronaves (principalmente militares) y armas** (con un aumento del de 28,7 %). Esto, en un país que supuestamente se prepara para la paz. Colombia importa de Israel, sobre todo, armamento y otros productos de la industria militar (el 49,6 % del total, en 2010),  probados por el ejército israelí en el marco  de las violaciones masivas y sistemáticas de los derechos humanos de los  palestinos.

Pero además de las implicaciones frente a la situación de Palestina, este tratado contiene disposiciones abiertamente desiguales en favor de los productores israelíes; es decir, sin su equivalente para los productores colombianos. Del mismo modo, fortalecería relaciones comerciales basadas en la exportación de Colombia de productos de poco valor agregado (carbón) y la importación de armas de Israel, asegura BDS.

Según la campana, este **TLC es desfavorable para Colombia, porque el crecimiento de las exportaciones colombianas hacia Israel será mucho menor (2 %)**, que las exportaciones israelíes hacia Colombia; habrá muy poca posibilidad de que productos industriales colombianos compitan en el mercado de ese país. En el sector de las telecomunicaciones, Colombia se abrirá a las empresas israelíes, mientras que Israel se cierra a la participación de las colombianas en su mercado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
