Title: Cerca de 1300 internos de cárcel “La Tramacúa” inician huelga de hambre
Date: 2018-06-27 10:17
Category: DDHH, Nacional
Tags: cárcel la tramacua, DDHH, Huelga de hambre, prisioneros políticos
Slug: internos-tramacua-huelga-de-hambre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Cárcel-La-Tramacua.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Doralnewsonline] 

###### [27 Jun 2018] 

Según un comunicado público del Movimiento Nacional Carcelario emitido este 27 de Junio, por lo menos 1300 internos de este centro penitenciario, ubicado en la ciudad de Valledupar, Cesar, inician una huelga de hambre y suspensión de “todo tipo de actividades” según ellos, porque persisten las violaciones a los Derechos Humanos por parte de las autoridades que controlan la cárcel.

En el comunicado los internos señalan que la única manera de hacer visible la situación de crisis de Derechos Humanos a la que se enfrentan es adoptar las vías de hecho, dado que agotan las posibilidades en el marco de lo legal como las tutelas y sus exigencias no son atendidas.

**Algunas de las situaciones que se han denunciado sobre el funcionamiento de la Tramacúa, tienen que ver con el acceso al agua, que solamente tendrían por dos horas diarias en una ciudad en que las temperaturas ascienden a los 40 grados en épocas de calor.** (Le puede interesar: ["INPEC continúa incumpliendo acuerdos establecidos en la Tramacúa"](https://archivo.contagioradio.com/inpec-continua-incumpliendo-acuerdos-establecidos-en-la-tramacua/))

Otra de ellas, es que este establecimiento carcelario cuenta solamente con capacidad para albergar a dos internos por celda, a pesar de ello **en cada celda son hacinados por los menos tres internos empeorando las condiciones de reclusión.** También han denunciado crisis sanitarias relacionadas con un inadecuado servicio de salubridad.

Por estas razones, los internos del centro penitenciario ya han realizado otras acciones de movilización, y en el pasado han pedido que se cierre la cárcel hasta que no se cumpla con las condiciones adecuadas para albergar allí a los reclusos. **Sin embargo, la demanda que hacen en este caso quienes están privados de la libertad, es que se mejoren sus condiciones inmediatas de reclusión y se garanticen sus Derechos Humanos.**  (Le puede interesar: ["Cierre de cárcel La Tramacúa de Valledupar sería un acto de humanidad"](https://archivo.contagioradio.com/cierre-de-carcel-la-tramacua-de-valledupar-seria-un-acto-de-humanidad/))

###### [Reciba toda la información de Contagio Radio en [[su correo] 
