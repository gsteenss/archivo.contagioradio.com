Title: La propuesta de FARC para salvar la economía popular
Date: 2020-04-01 15:31
Author: AdminContagio
Category: Actualidad, Política
Tags: Economía Informal, Popular
Slug: la-propuesta-de-farc-para-salvar-la-economia-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Emboladores-parte-de-economía-informal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Ang Moreno {#foto-ang-moreno .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 25 de marzo el partido FARC solicitó al Gobierno aplicar un Plan de Choque Social por 100 días para apoyar la economía popular que se ha visto afectada por las medidas tomadas para combatir el Covid-19. Según el senador de la colectividad Carlos Lozada, el objetivo del plan es "que no mueran más colombianos por la situación económica que por el propio virus".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El Covid-19 desnudó la condición económica de muchos compatriotas

<!-- /wp:heading -->

<!-- wp:paragraph -->

El senador sostuvo que la crisis económica no es culpa de Coronavirus, "porque lo que hizo esta pandemia fue desnudar la condición económica de muchos compatriotas que estaba encubierta". Esta idea explicaría las protestas de vendedores informales y personas vulnerables económicamente en Bogotá y otras ciudades del país, que reclamaban condiciones para afrontar la cuarentena sin tener que pasar hambre.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lozada sostuvo que la preocupación del partido es que el Gobierno está tomando medidas fundamentalmente para salvar los grandes capitales de empresas y el sector financiero pero no la vida de la gente. Por esa razón, el plan incluye nueve puntos para apoyar la economía popular. (Le puede interesar: ["No tiene por qué haber inflación de precios, si Gobierno toma medidas urgentes ante Covid 19"](https://archivo.contagioradio.com/no-tiene-por-que-haber-inflacion-de-precios-si-gobierno-toma-medidas-urgentes-ante-covid-19/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En primer lugar, el Plan incluye un aporte a la infraestructura y dotación hospitalaria para afrontar el virus, también propende por la protección de los trabajos evitando despidos, así como cancelaciones y suspensión de trabajos. Propone la gratuidad en servicios públicos por tres meses, moratoria de créditos hipotecarios, de consumo y educativos durante el mismo periodo, y subsidios al pago de pensión de estudiantes de todas las edades en todos los niveles educativos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los últimos puntos incluyen la regulación del mercado financiero y sus tasas de interés, regulación de precios para evitar especulación y aplazamiento del pago de todo tipo de impuestos. Un último pedido incluye algo que ya se estaba pidiendo a nivel global: la renta básica universal, que aseguraría un mínimo ingreso básico para población vulnerable.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lozada añadió que aún después de que pase la emergencia por el coronavirus "tendremos que considerar como sociedad que sectores de la población tengan acceso a un mínimo vital" que les brinde garantías de acceso a derechos y servicios básicos. (Le puede interesar: ["Cinco propuestas para evitar desastres económicos en los hogares colombianos"](https://archivo.contagioradio.com/cinco-propuestas-para-evitar-desastres-economicos-en-los-hogares-colombianos/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los pormenores de la propuesta de FARC para salvar la economía popular

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 30 de marzo, el partido presentó[el documento](https://es.scribd.com/document/454431960/Plan-de-Choque-para-salvar-la-economia-popular) explicando las propuestas y señalando que el dinero necesario para ejecutar las mismas se podría obtener redireccionando recursos del 4x1000 de bancos a sectores vulnerables, reduciendo las exenciones tributarias a grandes capitales ([que cada año se estiman en 17 billones de pesos](https://archivo.contagioradio.com/nueva-reforma-tributaria-significara-crecimiento-economico-para-los-mismos-de-siempre/)) y congelando el pago de la deuda, entre otras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, el senador presentó como un buen ejemplo de este tipo de medidas el plan social presentado por la Alcaldía de Bogotá que incluye recursos para cerca de 350 mil familias clasificadas entre pobres y vulnerables, además de el aplazamiento en el pago de impuestos y la operación de hospitales privados con problemas legales por parte del Distrito.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
