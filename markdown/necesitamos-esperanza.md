Title: Necesitamos esperanza
Date: 2018-04-03 09:24
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: elecciones, OEA, presidenciales
Slug: necesitamos-esperanza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/marcha-de-flores.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 3 Abr 2018 

[Colombia entera, la real y la de las redes sociales, se encuentra ante el epígrafe de una crónica sobre su perdición política. Las propagandas, los tamales, las orquestas, los slogans, los insultos, los piedrazos, los balazos, la casi patológica maña neoliberal de estar de espaldas a la historia, una maña que va configurando individuos que creen que el mundo comenzó con ellos; la ideologización de la corrupción como “el gran problema de la política” que ni siquiera hoy 2018 hace sospechar al más torpe, que si todos prometen luchar contra la corrupción eso debería ser más un signo de sospecha y no de encanto ¿acaso alguien sería tan estúpido de prometer que robará mucho? Claro que no, no obstante, ¿acaso no son muchos los estúpidos que creen durante la campaña en promesas efímeras sin comprender la totalidad del sistema que le circunda?]

[¡¡lee el programa de partido!! Te dicen en muchas partes, pero ¿encontraremos el punto siniestro del programa de un partido donde dicen que nos robarán y que favorecerán sus intereses de clase? Jamás. La perdición de la política es que sea una realidad asumida sin perspectiva histórica, que sea asumida solo como “el momento”, como lo espontáneo, con altísima emocionalidad pero bajísima crítica racional. Probablemente tanta cacofonía sobre algo que le llaman política terminará cuando la efervescencia actual sea reemplazada por otra con una nota de bienvenida que dirá “mundial 2018.”  ]

[De dónde esperanza ante una realidad que ofrece planos de discusión incoherentes, donde prevalece el “yo pienso” “yo creo” por encima de lo que ES, de la realidad histórica, estructural y social del país, donde desde hace rato se viene confundiendo libertad de expresión con opinión política…]

[A decir verdad, es una buena propuesta la que hace Ismael Moreno, director de Radio Progreso (Honduras), quien en medio de una dictadura civil implantada luego del golpe de Estado dado a Manuel Zelaya en 2009 y la continuidad del ultraje con el robo electoral a finales de 2017, ante la boca cerrada y ahí sí poco indignada de la OEA quien no tiene más agenda que Venezuela, Ismael, propone hallar la esperanza en tres elementos:]

1.  [La memoria de los mártires]
2.  [La gente que le sigue apostando al trabajo colectivo, comunitario y alternativo]
3.  [La entereza de la gente más humilde para seguir gestando la alegría en medio de la adversidad y la organización comunitaria en medio de la violencia que la niega.  ]

[¿De dónde esperanza en Colombia?]

[De la memoria de toda la gente asesinada, principalmente líderes sociales que han dejado la sangre sobre la tierra, que no querían morir prematuramente y que piden desde donde estén que el olvido no impregne las intenciones, y que los actos reivindiquen ese nuevo país que no será salvado por ningún candidato, sino por una fuerza colectiva que se alimente de vida, obra, memoria y determinación.]

[¿De dónde esperanza en Colombia?]

[De la gente que sigue trabajando colectiva y alternativamente desde las ciudades y los campos colombianos sin sucumbir ante la crítica que pregona “y eso para qué sirve”, sin decaer ante el derrotismo moral y político que pregona “nada va a cambiar”, sin dejarse destruir por la miserable opinión de ese grupito de periodistas famosos que juegan a ser jueces solo para hacerle el favor a sus amiguitos.  Ante nada de lo anterior las luchas populares han decaído, han soportado lo impensable, han soportado todo y aquí, hoy y ahora continúan en pie. Tomaremos la esperanza de esa gente que a pesar de que la matan o la ultrajan, se sigue organizando, sigue persistiendo, sigue proponiendo su estilo de vida para que cada escenario social sea un foco de resistencia, mucha gente sigue adolorida por lo que pasa en país, no obstante sigue, herida pero junta.]

[¿De dónde esperanza en Colombia?]

[De conocer la organización política de base. No se trata de ir a los cierres de campaña, o a la repartición de mercados del corrupto que bien sabemos solo volveremos a saber de él cuando el noticiero comente su traición; sino que se trata de ir y dar la mano a los líderes sociales, políticos de base que están en una campaña permanente, que no cobran sueldo, que le apuestan a lo colectivo, a lo comunitario, que luchan con entereza contra el orden de privilegios que mantiene a una Colombia sumida en el fracaso administrativo, que continúan alimentando una oposición férrea a la tendencia de ver en la política una oportunidad para enriquecerse, y procuran seguir protegiéndola como ese escenario en el cual se potencian todos nuestros sueños.]

[¿De dónde esperanza en Colombia? Tan solo salga y mire, cambie de calle, vaya al campo, pero no solo al campo de golf como hacen algunos, mejor sienta y analice lo que le rodea… allí se dará cuenta que a pesar de que esa clase privilegiada tiene el país en sus manos; la esperanza es y seguirá siendo la fuerza inagotable de nuestra sociedad.   ]

<iframe id="audio_25007533" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25007533_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
