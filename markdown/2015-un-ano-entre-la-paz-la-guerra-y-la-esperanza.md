Title: Balance 2015: Un año entre la paz, la guerra y la esperanza
Date: 2015-12-31 12:55
Category: Otra Mirada, Paz
Tags: Balance de la paz en 2015, Bojaya, Desminado y descontaminación, Paramilitarismo, Vereda el Orejon
Slug: 2015-un-ano-entre-la-paz-la-guerra-y-la-esperanza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/PAZ.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Imágen Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_9927510_2_1.html?data=mp6fmZqVdI6ZmKialZWJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRdpGlloqfpZDZsozVhqigh6eVs4zZz9nfx5DQpYzkwt%2BY25DQpYzb1srf1MaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [ 31 Dic 2015] 

**La ausencia del cese al fuego bilateral, una amenaza constante**

Una de las  primeras noticias con las que iniciamos el 2015 fue la del balance positivo tras el secuestro del general Alzate en Noviembre de 2014. Según los analistas el proceso salió fortalecido de esa situación y tanto el mismo proceso como la sociedad colombiana en la exigencia  de un cese bilateral de fuego, sin embargo, el estruendo de la guerra pareció más ruidoso que ese grito.

Desde el 20 de Diciembre de 2014 las FARC anunció un cese de operaciones ofensivas, sin embargo, según la denuncia de la propia guerrilla las operaciones de cercos militares obligaron a las unidades a reaccionar para evitar confrontaciones más graves.

Aunque el presidente Santos anunció el cese de bombardeos contra las FARC, este anuncio se dio luego de fuertes e intensos bombardeos que dejaron varios líderes guerrillero muertos, entre ellos “El becerro” combatiente de esa guerrilla durante más de 35 años según lo anunciado por Ivan Márquez.

Luego, en uno de los episodios calificados como críticos para el proceso de conversaciones con las FARC, 11 soldados murieron en un enfrentamiento en Buenos Aires, Cauca, según el relato de la comunidad, [la versión oficial fue muy diferente de la que vivieron los testigos directos.](https://archivo.contagioradio.com/la-noche-de-la-muerte-de-11-soldados-en-cauca-hubo-enfrentamientos-segun-testimonios/.)

Otro hecho lamentable y que demostró lo difícil de conversar en medio de la conforntación fue el bombardeo por parte de las FFMM a un campamento de las FARC en el valle del cauca, zona rural del municipio de Guapi que dejó un saldo de 26 guerrillero muertos, entre ellos 2 integrantes del equipo de paz de las FARC que se encontraban haciendo labores pedagógicas respecto al proceso. En un comunicado público se denunció que se suspendía el cese unilateral. “*la incoherencia del gobierno Santos lo ha logrado, luego de 5 meses de ofensivas terrestres y aéreas contra nuestras estructuras en todo el país.”* Relató el comunicado conjunto*.*

Afortunadamente, luego de varios meses de fuertes confrontaciones con saldos trágicos y lamentables para las personas y para la propia tierra, se logró que se reanudara el cese unilateral y se anunciaran medidas de des escalamiento por parte de las FFMM.

Luego de corrido todo el 2015, tanto el CERAC como el Frente Amplio por la paz coinciden en afirmar que el cese unilateral de las FARC y las medidas de desescalamiento han significado salvar la vida de miles y miles de personas y la tranquilidad de los campesinos y campesinas.

Esta decisión celebrada por muchas personas fue calificada como valiente y acertada por parte de muchos analistas, entre ellos el escritor, sociólogo e integrante de la Comisión de Memoria Histórica del Conflicto y sus Víctimas, [Alferdo Molano](https://archivo.contagioradio.com/cese-unilateral-de-las-farc-es-decisivo-valiente-y-prudente-alfredo-molano/).

A pesar de los miedos, muchas veces fundados, de la sociedad civil planteados por Alfredo Molano y aunque no se ha decretado el cese bilateral y definitivo, la reducción de muertes y el des escalamiento del conflicto son sensibles y muy afortunados. Los meses de Agosto, Septiembre, Octubre, Noviembre y Diciembre han sido catalogados como los meses más pacíficos en la historia reciente de Colombia.

**Medidas de des escalamiento y avances del proceso de conversaciones**

Dos de las más resaltables medidas tomadas de manera bilateral son las operaciones de desminado y descontaminación iniciadas en la vereda el Orejón, territorio del Bajo Cauca antioqueño, el acto de petición de perdón por parte de las FARC en Bojayá y la entrega de restos de 29 personas, reportadas como desaparecidas en Restrepo Meta.

Sin embargo los familiares de las personas entregadas siguen con preguntas que deberán ser contestadas, por ejemplo, ¿en qué circunstancias fueron asesinados? ¿quién o quienes ordenaron la desaparición forzada? ¿Quién se va a encargar de limpiar el bueno nombre de las víctimas que siguen siendo presentadas como guerrilleros muertos en combate?

Por otra parte, en la vereda el Orejón y el municipio de Briceño en Antioquia, una vez iniciadas las actividades tuvieron que suspenderse por la fuerte presencia paramilitar y la actuación amenazante por parte de las unidades militares en la zona. A pesar de que las acciones de limpieza se iniciaron en predios de EPM,  los habitantes de las veredas afirman que es un muy buen punto de inicio pero falta mucho camino por recorrer, según los varias de las personas consultadas por contagio radio, no se culminó ell trabajo de una manera satisfactoria y habrá que definir, en un nuevo cronograma, [las actividades para que se pueda hablar de un trabajo completo y satisfactorio.](https://archivo.contagioradio.com/en-el-orejon-falta-mucho-territorio-por-desminar-y-descontaminar-dicen-habitantes/)

Bojayá, fue testigo de uno de los actos más emotivos y que evidencia que las víctimas están en el centro del acuerdo, habitantes y testigos de CONPAZ a través de sus relatos corroboran la dureza y solemnidad del evento que s[e puede tomar como ejemplo para otras víctimas en el país y para los demás actores victimizantes.](https://archivo.contagioradio.com/sentido-acto-de-perdon-se-realizo-hoy-en-bojaya/)

### **Avances en acuerdos y en derechos de las víctimas** 

Las conclusiones presentadas por la Comisión de Memoria Histórica del Conflicto y sus víctimas son otra de las principales herramientas que se logran con las conversaciones de paz, aunque no son definitivas serán el insumo para una comisión de la verdad que iniciará su funcionamiento una vez se firme el acuerdo final

Sin lugar a dudas el acuerdo sobre el Sistema Integral de Verdad, Justicia, Reparación y No Repetición fue uno de los grandes alcances y que pone al proceso de conversaciones de paz en un punto de No retorno, como lo han afirmado algunos analistas. Sin embargo, el hecho de que se haya realizado una forma protocolaria el 23 de Septiembre y luego un anuncio oficial en el mes de diciembre suscitó muchas dudas y resistencias en organizaciones de víctimas. Lo claro es que este acuerdo es inédito en la historia y podría garantizar que no habrá impunidad si el Estado define los recursos necesarios para su cabal aplicación, como lo explica Enrique Santiago, uno de los juristas que elaboró el acuerdo sobre la Jurisdicción Especial para la paz.

Ahora bien, algunos afirman que no habrá paz completa sin un acuerdo público con el ELN. A este respecto se ha referido Nicolás Rodríguez Bautista, comandante del ELN, en torno a los múltiples avances que se han tenido en la fase privada.

Victor de Currea lugo, analista político afirma que hay grandes avances en casi todos los puntos de la agenda, que tienen que ver con la participación de la sociedad, la política minero energética, la dejación de armas y el cese bilateral de fuegos, sin embargo, una de las principales demoras en el proceso es que el  gobierno nacional congeló las conversaciones durante 15 meses, como lo reveló Antonio García, jefe del ELN en entrevista con De Currea.

Sin embargo, señala De Currea, que es necesario entender que los procesos humanos son de un paso atrás y dos adelante, en esa medida el balance final del proceso con el ELN está avanzando, por los puntos concretos que ya están definidos y porque la sociedad colombiana tiene una mayor conciencia de un proceso de paz en un conflicto que lleva más de 50 años.

A manera de conclusión, las principales críticas a las posibilidades de construcción de paz que se tejen con las FARC y con el ELN están siendo víctimas de un “conejo por parte de las élites”, puesto que se siguen aprobando leyes y planes de desarrollo que están en contravía de los acuerdos firmados, el caso de las ZIDRES o del propio Plan Nacional de Desarrollo son ejemplos concretos de ello.

Pero el meollo del asunto sigue estando en el inicio de conversaciones de paz con el ELN. Según Camilo González Poso, al inicio de conversaciones en una fase pública con esa guerrilla no se le pueden dar más largas, en caso contrario sería muy complicado un escenario favorable a las reivindicaciones sociales que deben tomar todo el impulso necesario.

### **El paramilitarismo es la amenaza latente a la posibilidad de paz** 

El paramilitarismo como uno de los puntos más preocupantes y que podrían significar que los avances del proceso de conversaciones se conviertan en letra muerta, afirma Camilo González, es un problema que debe reconocerse en su magnitud para atacarlo de la manera más eficiente posible.

El excomisionado de paz y las organizaciones de víctimas afirman que la salida no solamente debe identificarse por la vía de la confrontación armada, sino que debe haber un desmonte de las estructuras de financiación y la “red de apoyo” al interior de las Fuerzas Militares por la vía de una reestructuración de las mismas.

Si muere un cabecilla paramilitar en un bombardeo aparecen otros de rangos medios que irán asumiendo la responsabilidad y los negocios, afirma González, por eso “hay que atacar en donde les duele” que son las redes de tráfico de drogas, no atacando al campesino sino desmontando la estructura que gira alrededor de la comercialización de las drogas ilegales.

Para muchos y muchas la posibilidad de paz significará también vida digna, con educación, con salud, con medios de información democráticos entre otros, esperamos que el 2016 sea el año en que vislumbremos el camino que todos y todas tenemos que recorrer y con el que debe darse el más amplio compromiso por parte de todos los sectores sociales, militares y empresariales si realmente proyectamos el 2016 como “el año de la paz”.
