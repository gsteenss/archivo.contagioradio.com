Title: Incertidumbre ante futuro de las circunscripciones especiales de paz
Date: 2017-11-30 17:59
Category: Nacional, Paz
Tags: Circunscripciones de paz, Senado
Slug: circunscripciones_de_paz_senado_incertidumbre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/senado_farc_operación-tortuga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: honduradio] 

###### [30 Nov 2017] 

Continúa sin esclarecerse la aprobación o no de las 16 circunscripciones especiales de paz. Aunque parecía que por un voto definitivamente se había acabado con la posibilidad de que las víctimas tuvieran representación en el Congreso de la República, tras analizarse matemáticamente la votación se podría concluir que no se habrían hundido las circunscripciones en la plenaria del Senado cuando se discutía el texto de conciliación.

Según explica Guillermo Rivera, ministro del Interior la iniciativa fue aprobada con 50 votos a favor teniendo en cuenta el artículo 117 del Reglamento del Congreso y jurisprudencia de la Corte Constitucional. En un principio se creía que habían sido tumbadas porque no se contó con la votación necesaria, sin embargo no se tuvo en cuenta que **fueron contados los excongresistas** **Musa Besaile y Bernardo ´Ñoño’ Elías,** que hoy se encuentran suspendidos por casos de corrupción, y el segundo por parapolítica, homicidio y delitos relacionados con narcotráfico.

**"Mayoría absoluta es frente a los integrantes del Senado. Hoy sólo hay 99 senadores habilitados. **La mayoría son 50 votos, es decir se aprobaron las circunscripciones de paz", señala en su cuenta de Twitter el Mininterior.

En esa línea el ministro Rivera radicó una carta dirigida al Presidente del Senado, Efraín  Cepeda,  para que proyecto el Acto Legislativo que permite crear 16 circunscripciones de Paz sea remitido para su promulgación teniendo en cuenta esa situación, argumentado que la iniciativa **si habría obtenido las mayorías necesarias en el Congreso para que fuera aprobado.**

<iframe src="https://co.ivoox.com/es/player_ek_22404795_2_1.html?data=k5ehkpmbfZahhpywj5WXaZS1kZqah5yncZOhhpywj5WRaZi3jpWah5yncaTgwtrRy8aPkIa3lIqvldXJvoampJDQ0dPLtsbnytjhw5ClsMrVz9_OjbvJtsXZjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En esa línea, la senadora Claudia López, del Partido Verde ha lamentado lo que ha sucedido, "ha sido muy errático y es increíble que la gente celebre que las víctimas no tengan una curul,  en cambio ahora estamos en una leguleyada". Así también lo lamenta el senador Horacio Serpa del Partido Liberal aunque señala que **es claro que por mayorías las circunscripciones fueron aprobadas, "no me queda ninguna duda"**, expresa el congresista.

<iframe src="https://co.ivoox.com/es/player_ek_22404716_2_1.html?data=k5ehkpmbdZehhpywj5WcaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncanj08bQy9SPl8bm0caSlKiPlMLm1c7R0ZCwrcPZ08bZj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **Los senadores que no votaron** 

La indignación por parte de las víctimas y las organizaciones de derechos humanos no se ha hecho esperar y se ha dado a conocer quiénes fueron los senadores que no quisieron apoyar las circunscripciones. Por un lado, **toda la bancada del Centro Democrático y Cambio Radical se abstuvo de votar.**

Por el Partido Liberal tampoco votaron el senador Álvaro Asthon, Luis Fernando Velazco y Sofía Gaviria. Por parte del Conservador no participaron Fernando Tamayo, Nadia Blel, Olga Suárez y Luis Sierra, y de Opción Ciudadana, no lo hicieron Teresita García y Mauricio Aguilar.

Mientras que entre los que no votaron se encuentran, **la senadora Viviane Morales, Hernán Andrade, Juan Manuel Corzo,**  Juan Samy Merheg, Nidia Osorio, Jorge Hernando Pedraza y Juan Diego Gómez.

### **¿Por qué son importantes las circunscripciones?** 

Para Camilo Vargas, coordinador del Observatorio Político**-**Electoral de la Democracia de la MOE, las circunscripciones actúan como un mecanismo de "discriminación positiva" y explica que es preocupante que el Senado no permitiera la aprobación de las circunscripciones pues, "[**no son curules para las víctimas, son para los territorios donde se implementa la Reforma Rural Integral** para que las organizaciones sociales tengan una voz en Bogotá, para emitir alertas e incidir en políticas públicas".]

[Además dice no entender el argumento de algunos congresistas que señalan supuestos riesgos de inseguridad para que allí puedan desarrollarse elecciones, en cambio afirma que las elecciones en Colombia siempre se han dado en el marco de la guerra y no por eso se han cancelado eventos electorales.]

[ Para él esta sería **la oportunidad de que esas regiones tengan representación en el Congreso y así se pueda integrar la política nacional, la regional y local,** y de esa manera se pueda superar el abandono estatal histórico que ha habido en esas zonas del país. (Le puede interesar: [El último suspiro de las Circunscripciones de paz)](https://archivo.contagioradio.com/congresistas_circunscripciones_especiales_de-paz/)]

###### Reciba toda la información de Contagio Radio en [[su correo]
