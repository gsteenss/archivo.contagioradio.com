Title: El 10 de enero se retomarán diálogos con ELN
Date: 2016-11-30 18:18
Category: Paz, Política
Tags: ELN, Gobierno, guerrilla, paz
Slug: 10-enero-se-retomaran-dialogos-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/dialogos-Eln-y-gobierno-e1468532020926.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Vanguardia 

###### [30 Nov 2016] 

Mediante un comunicado, la delegación de paz del gobierno colombiano, informó que hasta el próximo 10 de enero, se retomarán las conversaciones para la instalación de la mesa pública con la guerrilla del ELN.

Durante el tiempo en que se retomen las conversaciones, el gobierno ha instado al ELN a que libere a Odín Sánchez. Ante esa propuesta, la guerrilla dijo mediante un comunicado publicado este martes que el gobierno incumplió con lo acordado el 5 y 6 de octubre. En ese marco, proponen reorientar el acuerdo y negociar la libertad de Sánchez por el indulto de dos guerrilleros.

![Comunicado ELN](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/CycuglMW8AAaxzK.jpg){.size-full .wp-image-33153 .aligncenter width="420" height="507"}

Tras esa situación, hoy el gobierno fija una nueva fecha para continuar dialogando sobre el inicio formal de conversaciones y además asegura que los indultos fueron acordados el 6 de octubre de acuerdo con las condiciones que dispone la ley vigente.

*\#URGENTE*  
*Comunicado de la delegación del Gobierno Nacional en los Diálogos con el ELN*  
*Quito, Ecuador*

*Tras varias semanas de trabajo en la búsqueda de soluciones a las razones por las cuales no se ha dado lugar a la instalación de la mesa formal entre el Gobierno Nacional y el ELN, la delegación del Gobierno expresa su voluntad de continuar alentando este proceso de diálogo con el Ejército de Liberación Nacional, e informa:*

*1. Tras una solicitud del ELN de ir a consultas internas, que fue aceptada por el Gobierno, se ha determinado que las conversaciones para concretar de manera definitiva la fecha de instalación de la mesa pública, se retomarán el próximo martes 10 de enero de 2017.*

*2. El gobierno espera que durante estas semanas, el ELN no sólo realice las consultas solicitadas, sino que, se produzca en el menor tiempo posible la liberación del Sr Odín Sánchez para que esté al lado de su familia.*

*3. El gobierno es categórico ante la opinión pública sobre los indultos a dos de los militantes de ésta guerrilla que fueron acordados el 6 de octubre en Caracas: Los casos que serían objeto de este procedimiento deben cumplir estrictamente con las condiciones que dispone la ley vigente.*

*4. El Gobierno Nacional agradece al gobierno de Ecuador, así como a los países garantes, por el acompañamiento constante y apoyo con el fin de instalar cuanto antes la fase pública del Diálogo para la paz de Colombia.*

*Quito,*  
*Noviembre 30 de 2016*
