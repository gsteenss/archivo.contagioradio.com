Title: Comunidades de Tumaco preparadas para revelar la verdad oculta tras el conflicto
Date: 2019-04-03 17:36
Author: CtgAdm
Category: Comunidad, Memoria
Tags: comision de la verdad, Tumaco
Slug: comunidades-de-tumaco-preparadas-para-revelar-la-verdad-oculta-tras-el-conflicto-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-03-at-8.39.21-AM-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [2019] 

Como parte del trabajo en territorio de la Comisión de la Verdad, los pasados 28 y 29 de marzo en el municipio de Tumaco, se inauguró una de las 24 Casas de la Verdad la cual abarcára 10 municipios de Nariño y estará destinadas a acoger los testimonios que puedan aportar al esclarecimiento del conflicto armado de los últimos 50 años.

La inauguración, que contó con la participación de **las comisionadas Patricia Tobón Yagarí y María Ángela Salazar** también tuvo una amplia acogida de los consejos comunitarios quienes han expresado su voluntad de trabajar junto a la Comisión, así lo manifestó **Euliquio Biohó**[**, líder del consejo comunitario de  Cortina Verde Mandela en el corregimiento de  Espriella**.]

"Tumaco ha sido uno de los centros principales del conflicto armado y de la violencia que ha vivido este país en todas sus épocas, aquí han sucedido dantescos que es necesario que Colombia y el mundo conozcan" afirma Euliquio celebrando la inclusión de una Casa de la Verdad en el municipio.

Sobre la confianza que depositan en la **Comisión de la Verdad**, el líder comunitario señala que "la responsabilidad de la Casa de la Verdad ha recaído en alguien que goza de toda la confianza de la comunidad tumaqueña y sobretodo de los líderes sociales para poder contar la verdad y esa persona es el licenciado Jorge Enrique  García", agrega.

Precisamente** Jorge García, coordinador de la Casa de la Verdad** afirma que se encuentran realizando un gran trabajo de pedagogía en el sector para que las personas reconozcan y diferencien a la Comisión de la Verdad de otras entidades como la JEP o la Unidad de Búsqueda de Personas dadas por Desaparecidas.[(Le puede interesar: Antioquia y Eje Cafetero, listospara trabajar junto a la Comisión de la Verdad)](https://archivo.contagioradio.com/antioquia-y-eje-cafetero-listos-para-aportar-sus-relatos-a-la-comision-de-la-verdad/)

García destaca la relevancia de una Casa de la Verdad en Tumaco donde el conflicto continúa debido en gran parte "a la distancia que existe con el resto del país"  lo que ha derivado en un abandono de carácter histórico, "es una región marcada por un desinterés sistemático del Gobierno que no protege a estas comunidades lo que las hace vulnerables a cualquier tipo de acción ilícita".

Frente al recorte de presupuesto que sufrió la Comisión de la Verdad, indica García que se han establecido alianzas locales y regionales con instituciones oficiales e internacionales que puedan facilitar el proceso de recabar información, "creo que con estas alianzas vamos cubriendo todos los déficit de tipo presupuestal, no es fácil pero avanzamos en esa dirección".

<iframe id="audio_34096968" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34096968_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
