Title: Sin Olvido - Alba Mery Chilito
Date: 2015-01-06 15:29
Author: CtgAdm
Category: Sin Olvido
Slug: alba-mery-chilito
Status: published

###### Foto: Cortesía de la familia 

[Audio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/fs43mpB1.mp3)

El 17 de Febrero de 2013, aproximadamente a las 8:00 de la mañana, Alba Mery Chilito fue asesinada por paramilitares. La lideresa social como todas las mañanas, llevaba a su nieto de 9 años al colegio; al dejarlo allí se dirigió al puesto de ventas que administraba en el parque recreacional, construido en honor a las víctimas de la violencia narco paramilitar, irónicamente estando en este lugar de salvaguarda de la memoria y dignificación de las víctimas, fue atacada por la espalda recibiendo varios impactos de bala que de inmediato la dejaron sin vida.
