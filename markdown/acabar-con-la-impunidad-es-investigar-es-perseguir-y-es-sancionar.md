Title: "Acabar con la impunidad es investigar, es perseguir y es sancionar"
Date: 2015-09-25 12:32
Category: Nacional, Paz
Tags: Acuerdo de Justicia, delegacion de paz, Derechos Humanos, enrique santiago, Human Right Watch, Juan Manuel Santos, justicia, Proceso de paz en la Habana, Punto número 6, Radio derechos humans, Timochenko
Slug: acabar-con-la-impunidad-es-investigar-es-perseguir-y-es-sancionar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/jUSTICIA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:lahora.gt 

<iframe src="http://www.ivoox.com/player_ek_8613053_2_1.html?data=mZuelZWZd46ZmKiak5yJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmosjOxMbWb8Tjz5DZw5DNsdHpz87Rw8mPqdSfytPjx9jYrcjV04qfpZDJt4zkxtfgx8zZrdOf2pCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Enrique Santiago, Abogado] 

###### [24 Sep 2015] 

Simón Trinidad, delitos conexos, revisión de sentencias dictadas, composición del tribunal especial son algunos de los temas que aborda esta entrevista con Enrique Santiago, uno de los gestores del SIJVRNR.

El abogado español Enrique Santiago Romero uno de los gestores del acuerdo de justicia que se firmó el día de ayer en la Habana, desmintió las versiones que giran en torno a la impunidad que este acuerdo brindaría e indicó que las condenas para aquellos partícipes del conflicto están dadas para cumplirse en el marco del derecho internacional y del derecho colombiano.

**Contagio Radio: ¿Qué opina de las declaraciones de José Miguel Vivanco que indican que estos acuerdos pueden evitar que responsables de crímenes paguen cárcel?**

**Enrique Santiago:** He quedado muy sorprendido después de las declaraciones del portavoz de Human Right Watch , me parecen muy precipitadas y muy imprudentes sin siquiera haber tener conocimiento total del acuerdo, porque el resto no se ha publicado.

En segundo lugar creo que es absolutamente errónea la orientación que ha dado en su intervención el señor Vivanco, el sistema está construido precisamente para acabar con la impunidad, y la única forma de acabar la impunidad respecto a los cientos de miles de responsables de crímenes responsables en más de 50 años de conflicto, es establecer un sistema que permita que todos comparezcan ante el.

**Acabar con la impunidad es investigar, es perseguir y es sancionar**, no hay ningún principio de derecho internacional que diga que acabar con la impunidad es encarcelar.

El sistema se ha construido para investigar o esclarecer a través del estímulo y mecanismos de verdad y las sanciones tienen hasta 20 años de cárcel. El sistema no contempla a priori el juzgamiento únicamente de máximos responsables, es más, de lo que se trata es que todas aquellas personas que tengan responsabilidades, sean máximos, intermedios o mínimos responsables, pero responsables ofrezcan la verdad.

En la medida en que se asuman responsables se convierte en obligación del Estado investigar, esclarecer y perseguir para imponer una sanción, con lo cual se cumplirá la función del Estado de castigar.

Las sanciones restaurativas, contempladas en el derecho internacional y en otros códigos penales, tienen principios establecidos sobre estas penas, que contemplan entre otras cosas la necesidad de que las víctimas participen en el proceso de alguna manera y eso es lo que el sistema hará estrictamente. No hay ninguna norma en el derecho internacional que establezca que la sanción debe ser cárcel.

La corte constitucional va a admitir el sistema, porque es más concreto y más complejo de los que se han aprobado nunca en Colombia desde luego y me atrevo a decir que en cualquier proceso de paz, después de la segunda guerra mundial.

Lo que sabemos es que la corte constitucional ha avalado las facciones alternativas, al avalar la constitucionalidad del acto legislativo, también avaló sanciones alternativas en el caso de control de constitucionalidad del artículo 975 del 2005.

Respecto a la corte penal internacional dudo muchísimo que ante un sistema como éste, teniendo en cuenta que de los 28 procesos de paz que se han dado desde que entró en vigor esta corte, este es el primero de esos 28 donde se establece un sistema serio, teniendo en cuenta que 27 se han saldado sin sistemas de justicia transicional y con amplísima amnistías, es decir, sin exigir responsabilidades.

En estos casos la corte penal no ha intervenido, me extrañaría que precisamente en este caso, ante un sistema complejo, detallado, riguroso, respetuoso, con el Derecho Internacional fuera a intervenir, porque contempla sanciones restaurativas. Eso me aparece un escenario improbable, por no decir imposible.

**Contagio Radio: El procurador dice que la amnistía señalada en el protocolo 2 al delito político o delitos conexos, excluye a militares y policías teniendo en cuenta la naturaleza del servicio público, ¿qué opina de esas declaraciones?**

**Enrique Santiago:** El protocolo dos no condiciona la amnistía a delitos políticos y conexos, lo que dice es que en la finalización del conflicto las autoridades aprobarán ley de amnistía más amplia posible, refiriéndose a crímenes que sean amnistiables en el Derecho Internacional.

En el Derecho Internacional y colombiano se establece que los derechos políticos y conexos son amnistiables, los únicos no amnistiables son crímenes de lesa humanidad.

El asunto no es que los militares vayan o no a tener amnistía, los militares no necesitan amnistía porque la insurgencia de las FARC solo van a tener amnistía por los hecho realizados durante la rebelión y con relación a esta, es obvio que los militares no se han conjurado para realizar una rebelión.

Una vez que se conceda esa amnistía a la insurgencia los militares y la insurgencia quedan e igualdad de condiciones, todos ellos serán sometidos por crímenes no amnistiables, es decir crímenes internacionales.

En el caso de los militares, otros crímenes comunes que hubieran cometido no deberían ser amnistiados , porque no han sido cometidos en relación con el conflicto y particularmente creo que si hay crímenes comunes cometidos por militares, por las fuerzas del Estado en relación con el conflicto y eran necesarios para realizar su actuación sí deberían ser amnistiados, pero la mayoría de los crímenes comunes, por no decir todos , que se atribuyen a militares, no han tenido nada que ver ni con el servicio, ni con el conflicto, por tanto yo no veo ningún trato desigual, que menciona el Procurador.

Es un trato igual desde ese punto de vista y en el acuerdo se contempla que a la hora de dilucidar la responsabilidad de los militares se tendrán especialmente en cuenta el contenido de sus normas de procedimiento y órdenes de combate. Por lo cual la responsabilidad podrá ser atenuada en la medida que esas órdenes establezcan una cosa u otra, una cosa distinta es las responsabilidades que se han redactados estas órdenes.

**Contagio Radio: ¿Delitos cómo el narcotráfico serían amnistiables?**

**Enrique Santiago:** Hay que precisar cuando hablamos de narcotráfico a qué nos referimos, hay formas que nunca serían amnistiables, exportar toneladas de cocaína a Estados Unidos , eso no tiene nada que ver con el conflicto y eso no sería amnistiable.

El cobro de impuestos a la hoja de coca, los raspachines, los campesinos humildes pobres que viven cultivando recogiendo la hoja de coca , esos delitos sí podrán ser amnistiados.

En el caso de los militares pasará igual, si acreditan que tienen acusaciones o alguna conducta que pueda relacionarse con narcotráfico, que han tenido necesariamente que cometer para llevar adelante su función pública, cosa que dudo que pueda ocurrir, pero si ocurre estos delitos no deberían ser sancionados, porque habrían sido cometidos por el servicio.

**Contagio Radio: ¿Qué va a pasar con esas sentencias ya dictadas contra militares?**

**Enrique Santiago:** El sistema contempla una sección de revisión de sentencias en dónde a elección de las personas que hayan resultado condenadas con una sentencia firme, podrán pedir la revisión de su sentencia.

En un supuesto que las personas no hayan dicho la verdad y quieran ofrecerla, en ese caso tendrían que formar parte del sistema y en función de cuanta verdad ofrezcan serían sancionados, conforme a lo que se establezca en el sistema.

El personaje que haya sido condenado y entienda que su sentencia debe ser declarada nula porque no se respetaron las normas de procedimiento el debido proceso, ha sido acusado con pruebas falsas , en ese caso la sala de revisión examinará esa sentencia.

**Contagio Radio: ¿Qué podría pasar con el caso de Simón Trinidad quién está recluido en Estados Unidos?**

**Enrique Santiago:** Simón trinidad fue condenado en el cuarto juicio (En U.S.A) fue condenado por conspiración para secuestrar, en el caso de los contratistas (Mercenarios) estadounidenses del avión que fue derribado por las FARC.

Unos contratistas que se dedicaban a realizar espionaje electrónico en el marco del conflicto, por lo cual en **el caso de Simón Trinidad, este sería un caso amnistiable**, porque fue la captura de unos mercenarios.

Lo que ocurre con Simón Trinidad es que no está sometido a la jurisdicción colombiana, se ha sometido a la justicia de Estados Unidos, si estos hechos, esta condena se hubiera dado en Colombia sin duda alguna sería amnistiable.

Como no es el caso y está bajo la jurisdicción de Estados Unidos y este no es parte del acuerdo, esto depende de la buena voluntad del gobierno de EEUU que el gobierno decida, cosa que pueda hacer, por ejemplo acceder a una solicitud de cumplimiento de condena en Colombia para que una vez llegue al país se le aplique la amnistía; hay convenios con cumplimiento de condenas para nacionales colombianos que están procesados en U.S.A o también podría decir que conforme a lo establecido en el 6.5 del protocolo dos, una vez finalizado el conflicto pues procedan a liberar a Simón Trinidad.

En todo caso no hay forma legal de exigirle a U.S.A que lo libere, será gracias a su voluntad política y la disposición de Estados Unidos con el proceso de paz.
