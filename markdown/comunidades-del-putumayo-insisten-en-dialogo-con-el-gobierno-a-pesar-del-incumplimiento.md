Title: Comunidades del Putumayo insisten en diálogo con el gobierno a pesar del incumplimiento
Date: 2016-08-30 18:51
Category: Movilización, Nacional
Tags: Juan Manuel Santos, Protestas en Putumayo, Putumayo, Sustitución de cultivos de uso ilícito
Slug: comunidades-del-putumayo-insisten-en-dialogo-con-el-gobierno-a-pesar-del-incumplimiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/marcha-putumayo-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio.com] 

###### [30 Ago 2016]

Comunidades campesinas, indígenas y afrodescendientes del departamento del Putumayo se movilizaron por las calles de Puerto Asís, tras más de un mes de asambleas permanentes en 24 puntos del departamento. Hasta el momento el gobierno nacional no ha planteado soluciones definitivas a las exigencias en torno a las afectaciones de las **empresas petroleras y la necesidad de implementar planes de sustitución** concertada de cultivos de uso ilícito.

Según Phanor Guazaquillo, integrante de la Mesa de Derechos Humanos del Putumayo, y concejal de Puerto Asís, **hay más de 53 actas firmadas con el gobierno nacional, sin que hasta el momento se hayan cumplido los acuerdos**, por el contrario se ha continuado con la erradicación forzada y se ha permitido el funcionamiento de las empresas petroleras sin que haya reparación de los daños ambientales sobre las fuentes hídricas provocadas por ellas.

Uno de los puntos más criticados por las comunidades y que ha generado más indignación durante esta protesta que completa mes y medio, ha sido la actuación de la policía y de las Fuerzas Militares quienes, además de [reprimir la protesta social de manera violenta causando heridas en 24 manifestantes](https://archivo.contagioradio.com/33-dias-de-movilizacion-pacifica-24-personas-heridas-por-fuerza-publica-en-putumayo/), ha escoltado la extracción petrolera de **AMERISUR con los integrantes del ESMAD.**

Por su parte, en cuanto a la [sustitución de cultivos de uso ilícito](https://archivo.contagioradio.com/gobierno-no-quiere-sustitucion-concertada-de-cultivos-de-uso-ilicito-en-putumayo/), las comunidades habitantes de la zona de reserva campesina del Putumayo rechazan el **desconocimiento del gobierno a la propuesta de sustitución manual, gradual y concertada de cultivos de uso ilícito que ha sido construida por las comunidades campesinas**, y que han venido negociando desde hace más de dos años con el gobierno del presidente Juan Manuel Santos.

Así las cosas la protesta se mantendrá hasta que se establezcan canales que faciliten, no solamente el diálogo directo entre el gobierno y los campesinos, sino también hasta que se llegue a acuerdos para **impedir más daños ambientales con los proyectos minero energéticos y que se afecte la economía campesina con la erradicación forzada.**
