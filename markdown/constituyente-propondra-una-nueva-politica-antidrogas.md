Title: Constituyente propondrá una nueva política antidrogas
Date: 2015-07-03 13:04
Category: Comunidad, Nacional
Tags: Amapola, Constituyente Nacional de cultivadores de Coca, cultivos de hoja de coca, cultivos ilícitos, fumigaciones con glifosato, Marihuana, Mesa regional de organizaciones sociales, plan Colombia, Putumayo
Slug: constituyente-propondra-una-nueva-politica-antidrogas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/20070519manoserradicando2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Señal Radio Colombia 

<iframe src="http://www.ivoox.com/player_ek_4719133_2_1.html?data=lZyem5aXd46ZmKiakp6Jd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbnxcqYxdTSt9Xd1drmx9PYqYzYxpDQ19HYrdfVxdTfx9iPt8af0dfc0tTSqNOZpJiSo5aPstbZ18aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Marcela Muñoz, Mesa Regional de Organizaciones sociales de Putumayo] 

###### [3, Jul 2015]

Diversas organizaciones sociales del país se darán cita del **4 al 6 de julio en Mocoa, Putumayo,** en la **Constituyente Nacional de cultivadores de Coca, Amapola y Marihuana.** Un espacio para el debate entorno a la construcción de una propuesta de política antidrogas, iniciativa que sería enviada a la Habana donde se desarrollan las conversaciones de paz entre el Gobierno y las FARC E-P.

Marcela Muñoz, integrante del equipo técnico de la comisión de sustitución de cultivos de uso ilícito de la Mesa regional de organizaciones sociales del Putumayo, explica que "el objetivo central tiene que ver con compartir las diversas experiencias que se han construido en las regiones, y así **generar insumos para una nueva legislación popular de cultivos, que tenga en cuenta  el territorio y los usos ancestrales**”.

**En 2016 la ONU tendrá una  sesión especial donde se analizará la problemática de las drogas**, para Marcela Muñoz, es importante considerar este espacio como un escenario de preparación para posicionar el sentir de las comunidades que viven alrededor de estos cultivos, teniendo en cuenta que sufren la vulneración de sus derechos humanos por parte de las fuerzas militares, cuando se radica manualmente o por medio de aspersiones aéreas.

Desde la política antidrogas del gobierno nacional, se ve al campesino como un delincuente y no se generan soluciones de orden social que promuevan una verdadera sustitución de cultivos de uso ilícito, donde se le brinden garantías de una vida digna a las comunidades, señala Muñoz, quien añade que **se debe intervenir en ese problema desde lo social y no desde la seguridad.**

Frente al posible uso de otro químico para realizar aspersiones aéreas, en sustitución del glifosato, las organizaciones sociales que participarán en la constituyente, le han indicado al gobierno que "no aceptan que los cultivos sean lavados con algún tipo de veneno", dice la integrante de la Mesa Regional del Putumayo.

Delegados de países como Uruguay, Perú, Ecuador y Bolivia, harán parte del encuentro en el que se espera poner al tanto a las comunidades urbanas, de la forma en que las políticas actuales afectan al campesino, y además se reconozcan las alternativas de paz que existen frente a este tema.

\[embed\]https://youtu.be/WcrTyigS\_aU\[/embed\]
