Title: Así se vivirá la semana contra el Apartheid en Colombia
Date: 2019-04-01 12:41
Category: Otra Mirada
Tags: BDS Colombia, Palestina, Semana contra el apartheid
Slug: semana-contra-apartheid-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/palestina-solidaridad-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 1 Abr 2019 

"**Dejad de armar al colonialismo**", es el lema que este año identifica a la **15a Semana de acciones contra el Apartheid Israelí,** iniciativa** **a la que organizaciones y ciudadanos colombianos se sumaran en solidaridad con el pueblo palestino que sufre las consecuencias de la ocupación en sus territorios.

El propósito del movimiento Boicot Desinversiones y sanciones a Israel (BDS) en Colombia para unirse a la acción mundial **es denunciar la violación sistemática de los Derechos Humanos y al régimen conocido como apartheid**, que se mantiene sobre el pueblo palestino, a partir de una programación de eventos que se realizarán **del 2 al 5 de abril**.

El martes 2 se realizará la charla **Anarquismo y la solidaridad con Palestina**, en La Morada (Kr 19 \# 36-34) desde las 6:00 p.m. evento organizado por Vía Libre Grupo Libertario. (Le puede interesar: [56 niños palestinos fueron asesinados en 2018 por ejército Israelí](https://archivo.contagioradio.com/ninos-palestinos-asesinados-israel-2018/))

El día jueves 4 de abril, el conversatorio **La prisión en Palestina y Colombia**, en la Universidad Nacional de Colombia, Edificio de Sociología, Auditorio Camilo Torres a las 2:00 p.m, organizado por Pulso Violeta. Ese mismo día se adelantará la **presentación de la cartilla Militarismo Israelí en América Latina**, en la sede de la  Asociación Distrital de Educadores (ADE), Calle 25A Nº. 31-30. Hora: 6:00 p.m. a cargo del BDS Colombia.

Y el viernes 5 se realizará la proyección del **documental Gaza** (ganador mejor documental en los premios Goya 2019), y **un foro sobre la situación en Gaza**, en la Universidad Pedagógica de Colombia. Edificio A Salón 321. Hora 5:00 p.m., organizado por la Fundación Centro Cultural Colombo Árabe al-Awda.

###### Reciba toda la información de Contagio Radio en [[su correo]
