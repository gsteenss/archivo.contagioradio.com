Title: Expreso Libertad - Contagio Radio
Date: 2018-02-22 18:09
Author: AdminContagio
Slug: expreso-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/EXPRESO-LIBERTAD-18.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/yufid.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/expreso-libertad.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/expreso-libertad-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/expreso-libertad-2.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[Infografias](https://archivo.contagioradio.com/infografias/)  
[Programas Expreso Libertad](#programa)  
[Videos Expreso Libertad](#media)

EXPRESO LIBERTAD
----------------

Programa que visibiliza a través de la radio las problemáticas e historias que viven los prisioneros  políticos y presos en las cárceles de colombianas

#### [Tweet](https://twitter.com/share?ref_src=twsrc%5Etfw)

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Infographic-Presentation-1024x768.png){width="1024" height="768" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Infographic-Presentation.png 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Infographic-Presentation-300x225.png 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Infographic-Presentation-768x576.png 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Infographic-Presentation-370x278.png 370w"}

**Con el apoyo de: **

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/logos-expreso-libertad-e1560270968692.png){width="500" height="100"}

### Radionovelas Expreso Libertad

<iframe title="Operación Cierre - Expreso Libertad by Contagio Radio" width="1170" height="200" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?visual=false&amp;url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F656317952&amp;show_artwork=true&amp;maxwidth=1170&amp;maxheight=1000&amp;dnt=1&amp;auto_play=false&amp;buying=true&amp;liking=true&amp;download=true&amp;sharing=true&amp;show_comments=true&amp;show_playcount=true&amp;show_user=true&amp;color"></iframe>

### Programas

[![La cárcel una extensión de la guerra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/carcel-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/la-carcel-una-extension-de-la-guerra/)

#### [La cárcel una extensión de la guerra](https://archivo.contagioradio.com/la-carcel-una-extension-de-la-guerra/)

Posted by [Contagioradio](https://archivo.contagioradio.com/author/ctgadm/)[<time datetime="2019-11-18T15:49:04-05:00" title="2019-11-18T15:49:04-05:00">noviembre 18, 2019</time>](https://archivo.contagioradio.com/2019/11/18/)Durante los más de 50 años de conflicto armado en Colombia, la cárcel ha representado un lugar de disputa para los diversos actores armados.[Ir al programa](https://archivo.contagioradio.com/la-carcel-una-extension-de-la-guerra/)  
[![En Colombia han sido asesinados 170 excombatientes tras la firma del Acuerdo de Paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/partido_farc_afp_12_0-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/en-colombia-han-sido-asesinados-170-excombatientes-tras-la-firma-del-acuerdo-de-paz/)

#### [En Colombia han sido asesinados 170 excombatientes tras la firma del Acuerdo de Paz](https://archivo.contagioradio.com/en-colombia-han-sido-asesinados-170-excombatientes-tras-la-firma-del-acuerdo-de-paz/)

Posted by [Contagioradio](https://archivo.contagioradio.com/author/ctgadm/)[<time datetime="2019-11-06T12:46:41-05:00" title="2019-11-06T12:46:41-05:00">noviembre 6, 2019</time>](https://archivo.contagioradio.com/2019/11/06/)Isabela Sanroque, integrante del partido político FARC, nos habló de la difícil situación de las y los excombatientes en los espacios territoriales de capacitación…[Ir al programa](https://archivo.contagioradio.com/en-colombia-han-sido-asesinados-170-excombatientes-tras-la-firma-del-acuerdo-de-paz/)  
[![El lujo de sobrevivir en una prisión de Colombia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/el-lujo-de-vivir-en-una-carcel-1-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/el-lujo-de-sobrevivir-en-una-prision-de-colombia/)

#### [El lujo de sobrevivir en una prisión de Colombia](https://archivo.contagioradio.com/el-lujo-de-sobrevivir-en-una-prision-de-colombia/)

Posted by [Contagioradio](https://archivo.contagioradio.com/author/ctgadm/)[<time datetime="2019-10-31T08:31:43-05:00" title="2019-10-31T08:31:43-05:00">octubre 31, 2019</time>](https://archivo.contagioradio.com/2019/10/31/)reportajes El lujo de sobrevivir en una prisión de Colombia Perder la libertad no es el único precio que pagan los presos al entrar…  
[![Radionovela "Furia Violeta"](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/FURIA-VIOLETA-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/radionovela-furiavioleta/)

#### [Radionovela "Furia Violeta"](https://archivo.contagioradio.com/radionovela-furiavioleta/)

Furia Violeta cuenta la historia de un colectivo teatral Trans, fue la herramienta que encontraron para defender sus…  
[![INPEC violenta a las mujeres visitantes en centros de reclusión](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/inpec-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/inpec-violenta-a-las-mujeres-visitantes-en-centros-de-reclusion/)

#### [INPEC violenta a las mujeres visitantes en centros de reclusión](https://archivo.contagioradio.com/inpec-violenta-a-las-mujeres-visitantes-en-centros-de-reclusion/)

Rocio Barrera, hermana de César Barrera, víctima de un falso positivo judicial, ha sido víctima en múltiples ocasiones…  
[![Universidad Nacional estaría negando el derecho a la educación de presos políticos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/universidad_nacional_16_0-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/universidad-nacional-estaria-negando-el-derecho-a-la-educacion-de-presos-politicos/)

#### [Universidad Nacional estaría negando el derecho a la educación de presos políticos](https://archivo.contagioradio.com/universidad-nacional-estaria-negando-el-derecho-a-la-educacion-de-presos-politicos/)

En este programa del Expreso Libertad familiares de presos políticos denunciaron que la Universidad Nacional estaría negando el…  
[![Radionovela "Sobrevivimos"](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/sovrevivimos-radionovela-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/radionovela-sobrevivimos/)

#### [Radionovela "Sobrevivimos"](https://archivo.contagioradio.com/radionovela-sobrevivimos/)

Sobrevivimos relata la historia de Luis Alberto, un preso político que al ingresar a la Cárcel La Modelo,…  
[![Caso Andino: ¿Qué ha pasado con las víctimas de Falsos positivos judiciales?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/no-falsos-positivos-1-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/caso-andino-que-ha-pasado-con-las-victimas-de-falsos-positivos-judiciales/)

#### [Caso Andino: ¿Qué ha pasado con las víctimas de Falsos positivos judiciales?](https://archivo.contagioradio.com/caso-andino-que-ha-pasado-con-las-victimas-de-falsos-positivos-judiciales/)

César Barrera, padre de César, víctima de un montaje judicial en el caso del atentado del Centro Comercial…  
[![Sin Olvido: Masacres en la Cárcel La Modelo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/crisis-carcelaria1-e1463780527863-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/sin-olvido-masacres-en-la-carcel-la-modelo/)

#### [Sin Olvido: Masacres en la Cárcel La Modelo](https://archivo.contagioradio.com/sin-olvido-masacres-en-la-carcel-la-modelo/)

colectivos de prisioneros politicos, organizados en la Cárcel la Modelo en las diferentes mesas de diálogo denunciaron múltiples…  
[![Las cárceles de Estados Unidos y sus violencias contra las mujeres](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/esta-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/las-carceles-de-estados-unidos-y-sus-violencias-contra-las-mujeres/)

#### [Las cárceles de Estados Unidos y sus violencias contra las mujeres](https://archivo.contagioradio.com/las-carceles-de-estados-unidos-y-sus-violencias-contra-las-mujeres/)

Las mujeres que estuvieron en cárceles, en Estados Unidos, han venido denunciado las fuertes afectaciones del sistema penitenciario…  
[![Mujeres inician camino para abolir sistema carcelario](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/enceuntro-mujeres-carceles-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/mujeres-inician-camino-para-abolir-sistema-carcelario/)

#### [Mujeres inician camino para abolir sistema carcelario](https://archivo.contagioradio.com/mujeres-inician-camino-para-abolir-sistema-carcelario/)

diversas mujeres de distintas latitudes se encontraron para dar el primer paso en la conformación de un movimiento…  
[![La educación: el camino a la resocialización](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Untitled-2-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/la-educacion-el-camino-a-la-resocializacion/)

#### [La educación: el camino a la resocialización](https://archivo.contagioradio.com/la-educacion-el-camino-a-la-resocializacion/)

Desde hace un par de meses, algunos docentes de la Universidad Nacional han puesto en marcha un plan…  
[![La participación política de FARC no puede ser asesinada](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/ETCR-Heiler-Mosquera-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/la-participacion-politica-de-farc-no-puede-ser-asesinada/)

#### [La participación política de FARC no puede ser asesinada](https://archivo.contagioradio.com/la-participacion-politica-de-farc-no-puede-ser-asesinada/)

se han registrado 141 asesinatos a integrantes del partido FARC, quienes ejercían labores de liderazgos en diferentes Espacios…  
[![La Corrupción en las Cárceles de Colombia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/000355000W-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/la-corrupcion-en-las-carceles-de-colombia/)

#### [La Corrupción en las Cárceles de Colombia](https://archivo.contagioradio.com/la-corrupcion-en-las-carceles-de-colombia/)

Muchas de las grandes problemáticas que vive la población reclusa en Colombia, son producto de las redes de…  
[![♫ Podcast "Operación Cierre"](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Formato-Podcast-_-Banner-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/podcast-operacion-cierre-expreso-libertad/)

#### [♫ Podcast "Operación Cierre"](https://archivo.contagioradio.com/podcast-operacion-cierre-expreso-libertad/)

«Operación Cierre», radionovela escrita y producida por Contagio Radio, en la que se relata la historia de 5…  
[![¿Qué pasó con la Ley 975 y el paramilitarismo en Colombia?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/csm_AgenciaNoticias_110815-04_05_ebfad2acb7-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/que-paso-con-la-ley-975-y-el-paramilitarismo-en-colombia/)

#### [¿Qué pasó con la Ley 975 y el paramilitarismo en Colombia?](https://archivo.contagioradio.com/que-paso-con-la-ley-975-y-el-paramilitarismo-en-colombia/)

En el año 2003 en Colombia se realizo la desmovilización de algunas estructuras del paramilitarismo con la Ley…  
[![Expreso Libertad : Frutos Rojos Colibrí](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/yufid-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/expreso-libertad-frutos-rojos-colibri/)

#### [Expreso Libertad : Frutos Rojos Colibrí](https://archivo.contagioradio.com/expreso-libertad-frutos-rojos-colibri/)

Las mujeres Farianas que hacen su tránsito a la reincorporación, están colocando en marcha un proyecto productivo de economía…  
[![Los retos de las mujeres en Libertad](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/presa-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/los-retos-de-las-mujeres-en-libertad/)

#### [Los retos de las mujeres en Libertad](https://archivo.contagioradio.com/los-retos-de-las-mujeres-en-libertad/)

Angela y Esperanza son dos mujeres que, tras salir de la cárcel, se juntaron al colectivo Mujeres Libres,…  
[![La Paternidad al otro lado de las rejas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Paternidad-Carceles-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/la-paternidad-al-otro-lado-de-las-rejas/)

#### [La Paternidad al otro lado de las rejas](https://archivo.contagioradio.com/la-paternidad-al-otro-lado-de-las-rejas/)

César Barrera es el papá de Cesar, una de las 10 personas capturadas por los atentados en el…  
[![Prisión preventiva: ¿Mujeres víctimas de la justicia?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/A_UNO_246756-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/prision-preventiva-mujeres-victimas-justicia/)

#### [Prisión preventiva: ¿Mujeres víctimas de la justicia?](https://archivo.contagioradio.com/prision-preventiva-mujeres-victimas-justicia/)

La Oficina de Washington para Lationamérica (WOLA) y Dejusticia reveló el aumento en la aplicación de la prisión…  
[!["Luché por la libertad de mi hijo ¡y lo logramos!"](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Mateo-gutierrez-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/luche-por-la-libertad-de-mi-hijo-y-lo-logramos/)

#### ["Luché por la libertad de mi hijo ¡y lo logramos!"](https://archivo.contagioradio.com/luche-por-la-libertad-de-mi-hijo-y-lo-logramos/)

En Expreso libertad nos acompaño Aracely León madre de Mateo Gutiérrez, capturado el 27 de febrero de 2017…  
[![Ni las rejas han logrado frenar el empoderamiento de las mujeres](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/presos-politicos-640x410-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/empoderamiento-mujeres-carceles-colombia/)

#### [Ni las rejas han logrado frenar el empoderamiento de las mujeres](https://archivo.contagioradio.com/empoderamiento-mujeres-carceles-colombia/)

Érika Aguirre víctima del falso positivo judicial caso «Lebrija» nos acompañó para hablar del empoderamiento de las mujeres…

### Videos

Expreso Libertad 1/11 videos  
"' data-height='450' data-video\_index='1'&gt;  
1  
![Ecoturismo para la paz](https://i.ytimg.com/vi/KRRh01xRwA0/hqdefault.jpg "Ecoturismo para la paz")  
Ecoturismo para la paz"' data-height='450' data-video\_index='2'&gt;  
2  
!["Universidades Bajo S.O.SPecha"](https://i.ytimg.com/vi/Ozfkuxvu46w/hqdefault.jpg ""Universidades Bajo S.O.SPecha"")  
"Universidades Bajo S.O.SPecha""' data-height='450' data-video\_index='3'&gt;  
3  
![La Roja, la cerveza de la paz](https://i.ytimg.com/vi/xxvu2N_2wxU/hqdefault.jpg "La Roja, la cerveza de la paz")  
La Roja, la cerveza de la paz"' data-height='450' data-video\_index='4'&gt;  
4  
![Aracely León - Seamos libres lo demás ya no importa](https://i.ytimg.com/vi/w-NMq2secjM/hqdefault.jpg "Aracely León - Seamos libres lo demás ya no importa")  
Aracely León - Seamos libres lo demás ya no importa"' data-height='450' data-video\_index='5'&gt;  
5  
![Expreso Libertad : "Maternidad en las cárceles"](https://i.ytimg.com/vi/d7AJEjcwhXA/hqdefault.jpg "Expreso Libertad : "Maternidad en las cárceles"")  
Expreso Libertad : "Maternidad en las cárceles""' data-height='450' data-video\_index='6'&gt;  
6  
![Enfoque de género en las cárceles - Expreso libertad](https://i.ytimg.com/vi/RFPuQGx3k8E/hqdefault.jpg "Enfoque de género en las cárceles - Expreso libertad")  
Enfoque de género en las cárceles - Expreso libertad"' data-height='450' data-video\_index='7'&gt;  
7  
![Lida Urrego, nos cuenta su historia de vida en la cárcel](https://i.ytimg.com/vi/MuN2lZHKqJ4/hqdefault.jpg "Lida Urrego, nos cuenta su historia de vida en la cárcel")  
Lida Urrego, nos cuenta su historia de vida en la cárcel"' data-height='450' data-video\_index='8'&gt;  
8  
![Adela Pérez, narra la situación de las mujeres en las cárceles](https://i.ytimg.com/vi/ht-K8Q3OjyA/hqdefault.jpg "Adela Pérez, narra la situación de las mujeres en las cárceles")  
Adela Pérez, narra la situación de las mujeres en las cárceles"' data-height='450' data-video\_index='9'&gt;  
9  
![Julio Marquetalia, exprisionero de las FARC y artista](https://i.ytimg.com/vi/nkiV48S32gM/hqdefault.jpg "Julio Marquetalia, exprisionero de las FARC y artista")  
Julio Marquetalia, exprisionero de las FARC y artista"' data-height='450' data-video\_index='10'&gt;  
10  
![Crisis de salud en las cárceles de Colombia - Expreso Libertad](https://i.ytimg.com/vi/LUnyqJ75XAk/hqdefault.jpg "Crisis de salud en las cárceles de Colombia - Expreso Libertad")  
Crisis de salud en las cárceles de Colombia - Expreso Libertad"' data-height='450' data-video\_index='11'&gt;  
11  
![Expreso Libertad: Reincorporación a la vida civil](https://i.ytimg.com/vi/x8jiIqqa-NI/hqdefault.jpg "Expreso Libertad: Reincorporación a la vida civil")  
Expreso Libertad: Reincorporación a la vida civil
