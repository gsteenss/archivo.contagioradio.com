Title: Corte Constitucional suspende sesiones virtuales del Congreso y frena al gobierno por decreto
Date: 2020-07-11 12:41
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Congreso Virtual, Corte Constitucional, Estado de Emergencia, Sesiones Virtuales
Slug: corte-constitucional-suspende-sesiones-virtuales-del-congreso-y-frena-al-gobierno-por-decreto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Sesiones-virtuales-Congreso-de-la-República.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Presidente de la Cámara de Representantes en sesiones virtuales del Congreso / Vanguardia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este jueves la Corte Constitucional **declaró la inconstitucional del artículo 12 del** [Decreto Legislativo 491](https://dapre.presidencia.gov.co/normativa/normativa/Decreto-491-28-marzo-2020.pdf) mediante el cual el presidente Iván Duque, había facultado a las tres ramas del poder público a **deliberar y tomar decisiones durante la pandemia** **a través sesiones virtuales.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con una votación de 5 votos a favor y 4 en contra los magistrados declararon la inconstitucionalidad de dicha norma. Según la Corte, **el fallo tiene efectos a futuro, por lo cual,** **en principio, las leyes aprobadas en las sesiones virtuales no se verían afectadas con la decisión.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los argumentos de la Corte para declarar la inconstitucionalidad

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Corte Constitucional motivó su fallo en la **independencia y separación que debe existir entre las ramas del poder público.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Según el expresidente de la Corte Constitucional, Alfredo Beltrán, supeditó la labor del Congreso al Poder Ejecutivo representado por el Gobierno:** «*No se puede dejar que quede a expensas de lo que diga u ordene el presidente*», expresó el exmagistrado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe anotar que la **Ley 5° por la cual se definió el Reglamento del Congreso, no contempla expresamente la posibilidad de sesionar de manera no presencial**, y según el exmagistrado Beltrán, el  Presidente de la República no tiene la potestad legal, ni constitucional para reformar leyes orgánicas como la ley 5° de 1992, lo cual se confirma con el fallo de la Corte.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JERobledo/status/1281632089125814274","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JERobledo/status/1281632089125814274

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### El silenciamiento de las voces opositoras en el Congreso con las sesiones virtuales

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por otra parte, varios miembros del legislativo venían señalando que las sesiones virtuales favorecían el hecho de que las mayorías silenciaran a las voces disidentes en las dos cámaras. **«*Los atropellos a la oposición y a las voces independientes con la simple manipulación de la plataforma tecnológica hacen imposible el control político. Hoy no hay Congreso ni equilibrio de poderes*»**, señaló el senador Roy Barreras respecto al tema. (Le puede interesar: [Hay que hacerle control político al gobierno ya: FARC](https://archivo.contagioradio.com/hay-que-hacerle-control-politico-al-gobierno-ya-farc/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RoyBarreras/status/1255613306917851137","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RoyBarreras/status/1255613306917851137

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JERobledo/status/1255604163242864646?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1255604163242864646%7Ctwgr%5E\u0026amp;ref_url=https%3A%2F%2Fwww.contagioradio.com%2Fepisodio-de-las-brigadas-militares-estadounidenses-es-ejemplo-de-la-politica-exterior-colombiana%2F ","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JERobledo/status/1255604163242864646?ref\_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1255604163242864646%7Ctwgr%5E&ref\_url=https%3A%2F%2Fwww.contagioradio.com%2Fepisodio-de-las-brigadas-militares-estadounidenses-es-ejemplo-de-la-politica-exterior-colombiana%2F

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### «Legislando por Decreto»

<!-- /wp:heading -->

<!-- wp:paragraph -->

Con la declaración de Estados de Emergencia por parte del presidente Duque, justificándose en la pandemia, su gobierno ha quedado facultado para expedir decretos con fuerza de ley, lo cual **le permite omitir el trámite legislativo para la adopción de ciertas medidas.** (Le puede interesar: [Las razones para pedir a la Corte Constitucional la revisión del decreto de estado de emergencia](https://archivo.contagioradio.com/las-razones-para-pedir-a-la-corte-constitucional-la-revision-del-decreto-de-estado-de-emergencia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe destacar que la [Constitución](https://www.constitucioncolombia.com/titulo-7/capitulo-6/articulo-215) prevé la posibilidad de declarar Estados de Emergencia hasta por 30 días, los cuales pueden ser prorrogados sin que sumados excedan 90 días en un mismo año calendario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El presidente Duque a la fecha ha declarado dos Estados de Emergencia. El primero el [17 de marzo](https://dapre.presidencia.gov.co/normativa/normativa/DECRETO%20417%20DEL%2017%20DE%20MARZO%20DE%202020.pdf) y el segundo el [6 de mayo](https://dapre.presidencia.gov.co/normativa/normativa/DECRETO%20637%20DEL%206%20DE%20MAYO%20DE%202020.pdf), lo que lo ha facultado por 60 días para expedir decretos legislativos.** En dicho lapso, según el senador Roy Barreras, Duque ha emitido cerca de 174 decretos con fuerza de ley.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **El Gobierno de manera abusiva había anulado, maniatado y limitado al Congreso para poder gobernar por Decreto tranquilamente.**
>
> **En 29 años desde la expedición de la Constitución de 1991, todos los gobiernos sumados, habían expedido 270 decretos \[con fuerza de ley\] y en 60 días este gobierno expidió 174»**
>
> <cite>Roy Barreras, Senador de la República, tras conocer el sentido del fallo de la Corte Constitucional</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

En relación **con la «anulación del Congreso» que denuncian algunos parlamentarios**, hace apenas unos días el Tribunal Administrativo de Cundinamarca, a través de un fallo de tutela, suspendió el permiso  que había concedido el presidente Duque para el ingreso de Brigadas Militares estadounidenses, por haber pasado por alto la [competencia constitucional](https://www.constitucioncolombia.com/titulo-6/capitulo-4/articulo-173) que tiene el Senado para autorizar el tránsito de tropas extranjeras en el territorio nacional. (Lea también: [Episodio de las Brigadas militares estadounidenses es ejemplo de la política exterior colombiana](https://archivo.contagioradio.com/episodio-de-las-brigadas-militares-estadounidenses-es-ejemplo-de-la-politica-exterior-colombiana/))

<!-- /wp:paragraph -->
