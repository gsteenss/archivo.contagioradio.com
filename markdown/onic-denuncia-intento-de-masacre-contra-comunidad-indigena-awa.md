Title: ONIC denuncia intento de masacre contra comunidad indígena Awá
Date: 2020-07-18 11:14
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Comunidad indígena Awá, grupos armados, ONIC, pueblos indígenas
Slug: onic-denuncia-intento-de-masacre-contra-comunidad-indigena-awa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Comunidad-Indígena-Awá-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Comunidad Indígena Awá / Foto: Minga

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Organización Nacional Indígena de Colombia -[ONIC](https://www.onic.org.co/comunicados-regionales/3964-intento-de-masacre-en-el-resguardo-indigena-awa-de-cuasbil-la-faldada)- denunció que el pasado 14 de julio sobre las 10 de la noche, **un grupo armado ilegal** irrumpió en el Resguardo Indígena Awá «Cuasbíl la Faldada», ubicado en Barbacoas, Nariño; **abriendo fuego contra una vivienda habitada por una familia indígena; dejando a cuatro personas heridas, entre ellas una mujer menor de edad y otra en estado de embarazo.** (Lea también: [Menor Embera de 9 años muere en medio de enfrentamiento armado](https://archivo.contagioradio.com/menor-embera-de-9-anos-muere-en-medio-de-enfrentamiento-armado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las víctimas son personas de la comunidad indígena Awá, entre las cuales se encuentran una menor de 16 y otra de 21 años, y otros dos jóvenes indígenas de 19 y 24 años, uno de los cuales, según un comunicado emitido por la ONIC, **se encuentra gravemente herido.** (Lea también: [Pueblos indígenas hacen llamado urgente para proteger la Línea Negra](https://archivo.contagioradio.com/pueblos-indigenas-hacen-llamado-urgente-para-proteger-la-linea-negra/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, ONIC señaló que este hecho de violencia se suma **al asesinato del líder indígena Rodrigo Salazar del Resguardo de «Piguambí Palangala»** que ocurrió hace menos de una semana en el municipio de Tumaco, Nariño y a las amenazas de las que han sido víctimas otros 122 líderes indígenas de la Unidad Indígena Del Pueblo Awá -UNIPA-.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### SOS de la comunidad indígena Awá y todos los pueblos étnicos

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ante la grave situación de violencia que padecen las comunidades indígenas, recrudecida incluso en medio de la pandemia, la ONIC hizo un **llamado urgente** a las instituciones nacionales, a las autoridades competentes en los territorios, como también a la Comunidad Internacional para que se pronuncien e intervengan frente a estos lamentables hechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, **exigió a los grupos armados el respeto por la vida y la integridad de los pueblos indígenas, enfatizando en que son y seguirán siendo «*pueblos de paz*».** (Le puede interesar: [Los «castigos brutales» con los que Grupos Armados instauran su propio régimen contra el Covid-19](https://archivo.contagioradio.com/los-castigos-brutales-con-los-que-grupos-armados-instauran-su-propio-regimen-contra-el-covid-19/))

<!-- /wp:paragraph -->
