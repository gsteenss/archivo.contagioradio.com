Title: Balance del comité de DD.HH del Paro del Sur- Tunjuelo en Bogotá
Date: 2017-09-28 17:06
Category: Movilización, Nacional
Tags: Paro desde el Sur, relleno doña juana
Slug: balance-del-comite-de-dd-hh-del-paro-del-sur-tunjuelo-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/WhatsApp-Image-2017-09-27-at-10.51.06-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 Sept 2017] 

El comité de derechos Humanos del Paro del Sur-Tunjuelo hizo público su informe de balance del primer día de movilización, enfatizando en que existió un uso desmedido de la violencia por parte de la Fuerza Pública, poniendo en riesgo la vida de las personas que participaron de las diferentes movilizaciones tanto en los barrios de Bogotá, como en las universidades públicas que acompañaron la movilización y **dejando como saldo 15 personas detenidas que ya se encuentran en libertad.**

De acuerdo con el informe, las marchas que se registraron estuvieron marcadas por las  diferentes irregularidades cometidas por integrantes de la Fuerza Pública, **algunas de ellas fueron la presencia de policías sin número de identificación en sus uniformes**, mientras que otros estuvieron de civil. De igual forma denunciaron que se presentó seguimiento a líderes del paro por agentes.

María Ramos, integrante de este comité expresó que se pudo constatar que los números de identificación de los cascos de los policías no concordaba con el de sus uniformes, “vulnerando incluso el código de Policía”. (Le puede interesar: ["Balance del primer día de movilización del Paro Sur-Tunjuelo"](https://archivo.contagioradio.com/balance-el-primer-dia-de-paro-del-sur-tunjuelo-bogota/))

### **Hostigamientos por parte de la Fuerza Pública** 

El informe también señala que integrantes de la **Fuerza pública en varias ocasiones hostigaron tanto a defensores de derechos humanos como a personas** que hacían parte de la movilización, además Ramos afirmó que intentaron detener a participantes de manera injustificada, sin embargo posteriormente fueron puestas en libertad.

El pasado lunes 25 de septiembre, la personería y el comité del paro, se reunieron para acordar mínimos de seguridad durante la marcha, en ese sentido Ramos expresó que todos esos pactos **se incumplieron con  las irregularidades y el accionar desproporcionado de la Fuerza Pública.**

Durante la noche, algunas personas acamparon en la entrada del Relleno Doña Juana y señalaron que allí también hubo presencia de agentes de la Fuerza Pública vestidos de civil, que hicieron inteligencia durante toda la jornada. (Le puede interesar: ["Comité del Paro Sur-Tunjuelo denuncia estigmatización contra líderes"](https://archivo.contagioradio.com/comite-del-paro-sur-tujuelo-denuncia-estigmatizacion-a-lideres/))

### **Detenciones de personas durante el Paro** 

De acuerdo con el comité de derechos humanos del paro, las autoridades obstaculizaron el trabajo de abogados y defensores de derechos humanos, negando información sobre los lugares a los que fueron trasladadas las 15 personas detenidas. Actualmente todas se encuentran en libertad y ninguna fue judicializada.

<iframe id="audio_21160018" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21160018_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
