Title: No pedimos tierras para una sola persona, sino para miles: Minga Indígena
Date: 2019-03-29 15:22
Category: Comunidad, Movilización
Tags: Minga en el Cauca, Minga Nacional por la Vida
Slug: no-pedimos-tierras-para-una-sola-persona-sino-para-miles-minga-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/D2S6rVNXcAEc3FG-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Cric] 

###### [29 Mar 2019] 

Como parte de las exigencias que viene realizando desde hace 19 días la **Minga Nacional por la Vida**, las comunidades indígenas han solicitado cerca de 48.000 hectáreas, petición que ha descartado el Gobierno en cabeza de la ministra del Interior Nancy Patricia Gutiérrez, quien continúa en el Cauca y el presidente Iván Duque, que tras tres semanas de bloqueos no ha atendido al llamado.

Acerca de la mas reciente declaración del presidente Duque y su negativa por visitar el Cauca y reunirse con los pueblos indígenas,  **Aida Quilcué, defensora de DD.HH. de la ONIC**  señala que incluso sin manifestaciones el mandatario no atendió a su llamado, "las organizaciones, en especial el CRIC lo convocaron desde el día en que nos invitó al palacio, proponiéndole lo  importante que era tener una interlocución con él en Cauca, petición que nunca respondió", explica la lideresa.

Frente a la seguridad al interior de la Minga, Quilcué reitera que las autoridades indígenas han hecho guardia, deteniendo a algunos integrantes del ejército que "han circulado al interior de la minga de civil con armas y traje privativo de las Fuerzas Militares" y a otras personas que han intentado transportar cocaína o marihuana, desarmonizando la Minga.

### **Otorgar tierras a las multinacionales pero no a los pueblos** 

Frente a las exigencias que ha hecho la Minga, Aida Quilcué considera que aunque la **Agencia Nacional de Tierras**, tiene todo lo requerido para este tipo de procedimientos, "no habrá tierra para indígenas porque aquí en el Cauca hay una política de racismo y xenofobia" concluye la lideresa refiriéndose a quienes no comparten la presencia de indígenas en el departamento.[(Le puede interesar: Pueblos indígenas de ocho departamentos se suman a la Minga ) ](https://archivo.contagioradio.com/jose-silva-nacion-wayuu/)

Según la consejera esa actitud se vio evidenciada cuando  el Gobierno y los sectores privados se reunieron para discutir sobre los bloqueos exigiendo la judicialización de las comunidades, lo que la lleva a cuestionarse, **"¿El Gobierno es autónomo o aquí hay algunos gremios que mandan al gobierno? ¿Dónde queda la diversidad?"**, inquiere.

Respecto a la ubicación de las tierras, explica que no solo están el Cauca sino también en Caquetá y Putumayo y que la Minga busca que sean destinadas a miles de personas y no solo a un único propietario como permanecen hasta el momento, escenario que compara con la situación que vive la Amazonía donde "le están entregando más de 5.000 hectáreas a las multinacionales".

La consejera de DD.HH  aclara que no es solo es una lucha por la tierra, ni únicamente por los índigenas del Cauca pues las problemáticas que viven los pueblos originarios persisten en toda Colombia, **"por ejemplo si hablamos en temas de derechos humanos, hay grandes problemáticas en el Chocó y en Nariño, nos están matando  y confinando a diario y el Gobierno debe prestar atención"**, explica Aida Quilcué.

<iframe id="audio_33857223" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33857223_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
