Title: Gerente de DESA directamente vinculado con asesinato de Berta Cáceres en Honduras
Date: 2016-05-04 16:54
Category: DDHH, El mundo
Tags: Agua Zarca, Berta Cáceres, DESA, honduras
Slug: desa-asesinato-de-berta-caceres-honduras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/desa-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: rel-uita.org] 

###### [4 May 2016] 

Según el testimonio de Edilson Duarte, de 25 años de edad, quien fue la persona que disparó contra Berta Cáceres la noche del 3 de Marzo, testimonio citado por el diario El Heraldo de Honduras, la versión entregada **vincula directamente a Sergio Rodríguez, gerente de la empresa Desarrollos Energéticos S.A, DESA,** encargada de la construcción del proyecto Agua Zarca al cual se oponía la ambientalista y las comunidades de la región.

Según el diario, la fuente al interior de la Agencia Técnica de Investigación Criminal de Honduras, ATIC, asegura que la investigación conduce a Rodríguez quién habría contratado los servicios de **Mariano Diaz, Mayor de las [Fuerzas Armadas de Honduras](https://archivo.contagioradio.com/militares-berta-caceres/)** para planificar el [asesinato de la dirigente del COPINH](https://archivo.contagioradio.com/condenamos-asesinato-de-mi-hermana-del-alma-amiga-de-berta-caceres/).

Además, la ATIC, habría encontrado que Díaz, fue contactado por Rodríguez a través del teniente retirado de las FFMM de Honduras, **Douglas Bustillo, quien se desempeñó como sub jefe de seguridad de DESA**. Tanto Díaz como Bustillo y Rodríguez fueron detenidos la semana pasada en varios puntos de la capital de ese país y se encuentran a disposición de las autoridades.

Cabe anotar que Bustillo fue acusado por el COPINH y por Berta Cáceres como uno delos autores de las múltiples amenazas recibidas días antes de su asesinato y frente a las cuales las **[autoridades hondureñas no hicieron nada para proteger a la líder o a los integrantes de la organización](https://archivo.contagioradio.com/gobierno-de-honduras-quiere-desviar-investigacion-del-asesinato-de-berta-caceres/)**. Por su parte Díaz fue el encargado de planificar las actividades de inteligencia relacionadas con seguimientos a Cáceres.

Estos avances en las investigaciones se dan luego de [varias declaraciones](https://archivo.contagioradio.com/mision-internacional-pide-acelerar-investigacion-por-crimen-de-berta-caceres/) entre las que se encuentran la del Papa Francisco, la Misión Internacional Justicia para Berta, el parlamento de Alemania y otras que piden celeridad en la [investigación y castigo a los responsables](https://archivo.contagioradio.com/estado-bancos-y-empresas-estarian-detras-del-asesinato-de-berta-caceres-copinh/) y la intervención de una **comisión independiente de la CIDH.**
