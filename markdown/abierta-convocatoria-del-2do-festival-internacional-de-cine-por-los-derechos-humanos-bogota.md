Title: Abierta convocatoria del 2do Festival Internacional de Cine por los Derechos Humanos – Bogotá
Date: 2015-02-05 16:27
Author: CtgAdm
Category: DDHH, Educación, yoreporto
Tags: defensores de derechos humanos, Derechos Humanos, Festival de Cine de Bogotá
Slug: abierta-convocatoria-del-2do-festival-internacional-de-cine-por-los-derechos-humanos-bogota
Status: published

El Segundo Festival Internacional de Cine por los Derechos Humanos - Bogotá  que se realizará entre el 7 y el 11 de abril, es organizado por Impulsos Films en alianza con el Archivo de Bogotá y la Fundación Konrad Adenauer.

En su segunda edición, el festival busca difundir, impulsar e incentivar la realización de trabajos de video de creación amateur. Por ello, y en alianza con la Fundación Konrad-Adenauer-Stiftung (KAS), premiará producciones audiovisuales de una duración máxima de 60 segundos. [Consulte aquí las bases](http://www.festivaldecineddhhbogota.com/bases-y-condiciones/) del concurso.

La convocatoria está dirigida a estudiantes y aficionados de cualquier nacionalidad que quieran dar a conocer producciones audiovisuales de una duración máxima de 60 segundos, donde conceptualicen la promoción y protección de los Derechos Humanos en Colombia y el mundo.

Los interesados en participar deberán ser titulares de los derechos patrimoniales e intelectuales de la obra con la que concursan. La convocatoria estará abierta hasta el día 13 de marzo de 2015 y la inscripción se podrá hacer a través del formulario online en el siguiente enlace: <http://www.festivaldecineddhhbogota.com/formulario-vnr/>

Los encargados de seleccionar las producciones que competirán en la selección oficial serán miembros de un comité de jurados designado por la organización del Festival, que evaluará el contenido temático y la calidad técnica de las obras audiovisuales.

La Fundación KAS premiará a tres participantes con la invitación a una actividad internacional patrocinada por la Fundación, en la que tendrán la oportunidad de presentar sus videos ante un público relevante y discutir las temáticas con expertos y asistentes. La Fundación KAS financiará a los tres ganadores el transporte, la alimentación y el alojamiento para asistir al evento mencionado.

Una vez se determinen qué trabajos harán parte de la selección oficial del Festival, serán publicados en YouTube. El que mayor cantidad de visitas reciba será galardonado con el “Premio del público”. La fecha límite para el conteo de visitas es el 11 de abril de 2015 al medio día. Los otros dos ganadores serán escogidos por los jurados designados por la organización del Festival.

\[caption id="attachment\_4416" align="aligncenter" width="400"\][![IMPULSOS\_KAS](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/02/IMPULSOS_KAS.png){.wp-image-4416 width="400" height="492"}](https://archivo.contagioradio.com/nacional/abierta-convocatoria-del-2do-festival-internacional-de-cine-por-los-derechos-humanos-bogota/attachment/impulsos_kas/) Toda la información relacionada será publicada en la página oficial del Festival: www.festivaldecineddhhbogota.com.\[/caption\]

Redes sociales se darán a conocer detalles y noticias sobre la selección (Twitter: @FestddhhbogotaFacebook: [www.facebook.com/FestivaldecineDDHHbogota](http://www.facebook.com/FestivaldecineDDHHbogota)).

Para obtener mayor información, comuníquese  en Colombia al teléfono (57) 310 550 37 54 o al correo electrónico: festicineddhhbogota@gmail.com.
