Title: Si nos tocan la tierra nos tocan la sangre
Date: 2015-08-25 13:43
Category: Nacional, yoreporto
Tags: Bertha Cáceres, CENSAT, extractivismo, Laura Carvajal, Mujeres y extractivismo, Mujeres y territorio
Slug: si-nos-tocan-la-tierra-nos-tocan-la-sangre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/fotos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Yo Reporto / Johana Jaramillo - @filosituación] 

###### [25 Ago 2015] 

[Entre el 18 y 20 de Agosto, del 2015, mujeres de Colombia y otras partes de Latinoamérica se reunieron en Bogotá y Fusagasugá para la realización del “**Encuentro Mujeres Defensoras de la Vida Frente al Extractivismo**” convocado por CENSAT Agua Viva y Fondo de Acción Urgente América Latina (FAU–AL).]

[Compartir experiencias de lucha contra el **extractivismo y la defensa de la tierra, diagnosticar los impactos diferenciados en la salud**, lo emocional, la movilidad, la militarización, la violencia de género y la criminalización de la mujeres que levantan su voz de lucha, fueron algunos de los temas trabajados.]

[Los patrones de criminalización de mujeres en el marco de la lucha contra el extractivismo, (modelo patriarcal que mercantiliza la naturaleza), arrojan que este es causa de desplazamiento, limita el acceso a bienes naturales, causa pérdida de autonomía económica y es motivo de constantes agresiones.]

[Tuvimos la oportunidad de hablar con Laura Carvajal, quien hace parte de la iniciativa Mujeres, territorio y ambiente del FAU-AL, quien nos comentó las problemáticas de amenazas a lideresas de la Toma del norte del Cauca y el asesinato de compañeros de la Defensa del río Sogamoso en Santander, así como las detenciones arbitrarias que se han presentado durante movilizaciones.]

[Desde el encuentro, las mujeres participantes, exigen que se reconozca y respeten sus formas autónomas y comunitarias y el derecho a decidir sobre sus cuerpos. Plantean su deseo de una tierra libre de agrotóxicos, ríos libres de mercurio y sin semillas transgénicas.]

[También se evidenció que en toda América Latina se encuentran historias de mujeres luchadoras que han sido privadas de su libertad, víctimas de hallanamientos, montajes judiciales, persecución política, violencia sexual.]

[Historias como la de Máxima Acuña en el Perú, víctima del hostigamiento, y privada de su libertad por su oposición a la minería; la de los Mapuche en Chile en su lucha contra la central hidroeléctrica Osorno, también han sido presos producto de montajes judiciales.]

[Entre las asistentes se encontraba: Bertha Cáceres, co-fundadora del Consejo Cívico de Organizaciones Indígenas y Populares de Honduras- COPINH, quien en su lucha contra una hidroeléctrica en Honduras fue privada de su libertad; Gabriela Ruales,  movimiento de mujeres feministas del Ecuador e integrante del Colectivo Yasunidos;  Ángela Cuenca, del Colectivo Casa de Bolivia y co-coordinadora de la Red Latinoamericana de Mujeres Defensoras de Derechos Sociales y Ambientales, y Lorena Cabnal, originaria del pueblo maya-xinka de Guatemala, feminista comunitaria e integrante AMISMAXAJ. Desde Colombia, Emilce Ferrer, de Mujeres del Común, Movimiento en defensa del Río Sogamoso- Ríos Vivos y Ana María Patiño, del Consejo Comunitario de la Toma, en el norte del Cauca.]
