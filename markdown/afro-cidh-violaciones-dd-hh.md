Title: Comunidad afro denunciará ante la CIDH violaciones a los DD.HH.
Date: 2018-10-02 17:39
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: AFRODES, CIDH, comunidades negras, DDHH, PCN
Slug: afro-cidh-violaciones-dd-hh
Status: published

###### [Foto: Contagio Radio] 

###### [2 Oct 2018] 

**La Comisión Interamericana de Derechos Humanos (CIDH)** recibirá este miércoles en audiencia pública, "denuncias de violación de derechos de las comunidades afrocolombianas víctimas del conflicto armado en Colombia", en la que integrantes de poblaciones afro podrán denunciar los abusos de las fuerzas armadas durante el paro cívico de Buenaventura, y expresar su oposición a la política de drogas del gobierno Duque.

### **¿Qué denunciarán ante la CIDH?** 

**El integrante del Proceso de Comunidades Negras (PCN), Víctor Vidal,** sostuvo que durante la audiencia, presentarán los casos de abuso de autoridades durante el paro cívico de Buenaventura para que no se queden en la impunidad, y **expondrán su oposición a los anuncios del Gobierno según los cuales, se reactivará la fumigación con glifosato,** "especialmente en el Pacífico, poniendo en riesgo la biodiversidad de la región".

Adicionalmente, pedirán al Estado que salde la deuda histórica que se tiene con las zonas donde residen las comunidades afro, porque "**históricamente el país sabe que la comunidad, desde los tiempos de la Colonia, ha sufrido racismo estructural y estatal** frente a un presupuesto, que nos recuerda que Colombia nos asume como ciudadanos de segunda clase".

El integrante del PCN recordó el caso de Buenaventura, que es el puerto con mayor actividad económica del país, que genera gran cantidad de riquezas, y aún acudiendo a un paro cívico y, tras lograr acuerdos con el Gobierno, **la calidad de vida sigue siendo "simplemente vergonzosa"**. (Le puede interesar: ["Buenaventura volverá a marchar ante incumplimientos del Gobierno"](https://archivo.contagioradio.com/buenaventura-vuelve-a-marchar/))

### **La Comisión es un escenario válido que en otros tiempos ha sido de mucha ayuda** 

Vidal afirmó que las comunidades afro eran conscientes de que el trabajo con otros actores políticos es importante, y apuntó que l**a Comisión, como actor internacional, tenía alguna influencia en la dirección del Gobierno,** razón por la que es un escenario valido para seguir tramitando sus denuncias. El activista manifestó que la audiencia es un elemento que "ayuda a todo el trabajo que hacemos de la movilización y la denuncia en la zona".

Entre las denuncias que actualmente ha hecho el PCN está la exigencia para **que se respete el derecho a la Consulta Previa, Libre e Informada**, porque es un ejercicio de participación que analiza el impacto de los proyectos económicos en las comunidades y permite que estas determinen su viabilidad; pero actualmente hay un Proyecto de Ley impulsado por Cambio Radical para reglamentar este proceso y limitarlo. (Le puede interesar: ["¿Por qué las Consultas Populares están en riesgo?"](https://archivo.contagioradio.com/consultas-populares-riesgo/))

La audiencia se realizará en el salón Schaden Commons de la Escuela de Leyes en la Universidad de Colorado, Estados Unidos, a las 10:15 de la mañana (9:15 am, hora local), y allí participarán e**l Instituto Internacional sobre Raza, Igualdad y Derechos Humanos (IIREHR), la Asociación Nacional de Afrocolombianos Desplazados (AFRODES)** y representantes del Estado de Colombia. (Le puede interesar: ["Solicitan medidas cautelares a CIDH para proteger a los líderes sociales"](https://archivo.contagioradio.com/solicitan-medidas-cautelares-a-cidh-para-proteger-a-los-lideres-sociales/))

<iframe id="audio_29075823" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_29075823_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
