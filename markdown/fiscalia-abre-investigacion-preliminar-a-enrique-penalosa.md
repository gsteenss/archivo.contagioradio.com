Title: Fiscalía abre investigación preliminar contra alcalde Enrique Peñalosa
Date: 2016-06-08 18:12
Category: Judicial, Nacional
Tags: Bogotá, Doctorado de peñalosa, Enrique Peñalosa
Slug: fiscalia-abre-investigacion-preliminar-a-enrique-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Enrique-Peñalosa-e1461273574548.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Lafarge Holcim Foundation] 

###### [8 Jun 2016]

La Fiscalía General de la Nación decide abrir investigación preliminar contra el alcalde de Bogotá, Enrique Peñalosa, tras la demanda presentada por el abogado Augusto Ocampo en la que se acusa al mandatario de falsedad en documento privado, por haber acreditado en su hoja de vida estudios de maestría y doctorado en el exterior, que según parece no ha cursado. En la demanda se solicita que la Procuraduría investigue esta conducta punible, pues los estudios también aparecen citados en libros de la autoría de Peñalosa y en algunas de sus conferencias. De acuerdo con el abogado la sanción disciplinaria tendría que ser la destitución del alcalde.

Noticia en desarrollo...  
 
