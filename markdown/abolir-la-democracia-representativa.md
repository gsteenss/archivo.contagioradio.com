Title: Abolir la democracia representativa
Date: 2017-06-03 09:12
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Cajamarca, ESMAD
Slug: abolir-la-democracia-representativa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/democracia-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: *El grito,* de Edvard Munch. 1893. Litografía. 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 3 Jun 2017 

[La democracia es uno de esos conceptos, que al igual que la frase]*[“páseme ese trapo”]*[sirve para absolutamente todo. A los que vemos prensa nacional, internacional o simplemente movemos un poco los dedos para que aparezcan y aparezcan noticias y memes en las redes, creo que nos tiene hostigados tanta verborrea sobre la democracia con tanta infamia que le acompaña.]

[Claro, hay muchos países que no tienen democracia, como por ejemplo Arabia Saudita, el aliado más importante de Estados Unidos (más importante que incluso Israel) en medio oriente, muuuuuucho más dictatorial que Al-assad en Siria o el mismísimo Maduro. ¡¡Claro!! hay otros como Corea del Norte con un régimen totalitario, pero a la vez de manera irónica, le enseña al mundo que para hacerle frente al imperialismo de occidente hay que armarse hasta los dientes y]**ser ante todo:** **lo contrario a occidente**[… ¿Qué esperábamos? ¿no era obvio?  ]

[Nos enseñaron hace unos años que democracia era el gobierno del pueblo. Después comprendimos que eso de “pueblo” era simplemente un epíteto muy útil para cualquiera que quisiera convertir un interés particular en un interés general. Luego nos trataron de convencer de que el equilibrio de poderes era la solución ante el riesgo de una acumulación de poder; nos hicieron aprender de memoria que un Estado Nacional tenía el poder legislativo, el ejecutivo y el judicial.]

[Con el tiempo nos dimos cuenta que inclusive los que dominaban el campo económico, querían convertir a la democracia en economía de mercado, pero en su fracasado intento se pegaron varios tiestazos que el mundo conoció como “las crisis del capitalismo”... la última y más reciente: la del 2008.   ]

[Muchos apologistas del apodo “democracia”, tampoco lograron comprender que democracia no es el gobierno del pueblo, porque “pueblo” somos todos y ninguno a la vez, el pueblo eligió a Hitler, el pueblo elige a los neofascistas israelíes que exterminan palestinos, el “pueblo” eligió al señor mantequilla de Uribe (mantequilla porque se desliza de la mano de la justicia), el “pueblo” es simplemente el grupo de seguidores de un individuo… ¿pero y los demás? ¿esos no son pueblo? ¿esos no son “el país”?]

[Por eso los políticos de derecha dicen que el acuerdo de paz no le conviene al país. Por eso progresistas con marketing sabroso como Claudia López dicen que al país no le conviene la izquierda. Por eso, todos los que están hoy en el poder, les interesa que confiemos en el sistema electoral, pues es éste, el candado que ha cerrado la posibilidad de que la democracia deje de ser un apodo y realmente sirva para algo más que ratificar a tanto ladrón inescrupuloso en el poder.]

[La democracia representativa debe ser abolida, porque votando celebramos el sistema de esos pocos políticos que son a la vez empresarios y terratenientes o amigos de los empresarios y terratenientes y que toman las decisiones sobre el presupuesto fiscal del Estado: una platica que proviene] **exclusivamente**[ del trabajo de los colombianos.]

[La democracia representativa debe ser abolida, porque al votar, los individuos entregan su libertad política, es decir, la facultad de decidir sobre su destino dejando la decisión a un representante que lo engañó con marketing calibrado a  la medida de cada clase social ¡¡y esto sí que es importante!!]

[Mientras unos se dedican a critican a los que reciben lechona, guaro o kits escolares a cambio de votos, otros se dedican a entrar a foros, cócteles o charlas que no sirven más allá de un par de aplausos y dos posteos en Facebook (la foto y la frase)…Pan y circo continúan, es sólo que éste se reparte según la clase social… yo la verdad en ese triste escenario, respeto más al que vota porque le dieron lechona, que a aquel que lo embaucaron con retórica academicista o cursos sobre “cómo votar” hechos por el canal Caracol… al menos la lechona tiene algo de carne.  ]

[La democracia representativa debe ser abolida sencillamente porque se ha demostrado que no funciona. La representación es la anulación del poder político de los individuos en manos de unos pocos que han sellado el sistema a su favor. La representación es el engaño más grande para cubrir la mala distribución de la riqueza que producen exclusivamente los trabajadores.]

[La representación es el fracaso del régimen existente en Colombia, pues en ella han perecido años y años de desarrollo. La representación es un engaño tan grande que quienes la defienden, de la mano de los medios masivos, se encargan de responsabilizar de su absoluto fracaso, a quienes no confían en ella, a quienes escribimos contra ella, a quienes no votamos, porque sencillamente algunos supimos primero que Odebrecht, que apoyes a quien a apoyes, la decisión ya fue tomada antes… el presupuesto será desviado en adjudicación de contratos.  ]

[No obstante, la democracia representativa tiene una enemiga muy fuerte y que ha sido últimamente asumida por amplios sectores de los actores sociales en Colombia:]**la democracia directa.**

[¿Qué pasó cuando la gente decidió hacer una votación en el Municipio de Piedras contra el extractivismo minero a gran escala?… desde el Estado les dijeron: eso es ilegal.]

[¿Qué pasó cuando la gente decidió parar para pedir por sus derechos? Desde los medios masivos los grandes gurús repetían y repetían: “están afectando la economía del país, están afectando al pueblo con su paro”]

[¿Qué pasó cuando ganó la consulta en Cajamarca Tolima? El ministro de minas dijo: “no la reconoceremos”. Y desde Caracol Radio indignados decían: “pero esto va a espantar la inversión, ahora van a coger las consultas por deporte, por demagogia, se va a perjudicar el país”]

[¿Qué pasó con el paro cívico en Buenaventura?: los medios se dedicaron a señalar que ese paro tenía “todo, menos de cívico” “que las empresas estaban perdiendo, que perdía la economía” no obstante con las revueltas en Venezuela, no condenaron la quema de personas chavistas, ni los bloqueos, ni a los capuchos de las universidades, ni los saqueos, ni a los manifestantes arrojando excremento, ni siquiera condenaron a la “mujer maravilla”. ¿se imagina que en Colombia le arrojaran bombas de excremento al Esmad? El moralismo no los hubiera dejado vivir a los hipócritas de Caracol y RCN.]

[En conclusión ¿Qué pasa finalmente cuando la gente decide asumir su libertad política en Colombia? Es oprimida por las leyes que hacen los congresistas, por el Esmad en la calle y por lo medios masivos con la estocada simbólica de sus discursos mamertos de derecha.]

[Al poder dominante en Colombia no le conviene la democracia directa, no le conviene porque sabe que con la democracia representativa pueden seguir haciendo lo que se les venga en gana. No les conviene porque la democracia directa es decirles: “No los necesitamos”. No les conviene sencillamente porque la democracia para ellos es un apodo por el cual están dispuestos a obligarnos a votar… ¿no era obvio?]**A mayor fortaleza del sistema electoral colombiano, mayor el poder de la dominación política, por consiguiente, menor posibilidad de libertad política, es decir: mayor represión para los actores sociales que prefieren decidir, antes que sólo votar.** [ ]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
