Title: Concejo de Bogotá aprobó prohibición del asbesto en primer debate
Date: 2019-06-04 18:25
Author: CtgAdm
Category: Ambiente, DDHH
Tags: asbesto, colombia sin asbesto
Slug: concejo-de-bogota-aprobo-prohibicion-de-asbesto-en-primer-debate
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/asbesto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Greenpeace] 

El pasado 3 de mayo, la Comisión de Gobierno del Concejo de Bogotá aprobó unánimemente un proyecto de acuerdo que pretende prohibir el uso del asbesto en las obras públicas de la capital, la región donde más se produce residuos de este material tóxico en el país.

El acuerdo denominado 170 propone varias medidas, entre ellas, crear un inventario distrital para el seguimiento a la exposición del asbesto, remplazar este material por nuevos elementos, promover el uso de materias primas que no atenten contra la salud pública, prohibir el uso de este material y finalmente, impulsar una campaña de información sobre los riesgos del uso y la exposición al asbesto en la salud de las personas y el ambiente.

"Desde este Proyecto de Acuerdo, se busca que Bogotá sea una ciudad más en Colombia donde se protege la vida y la salud de los colombianos, evitando que en futuro se construya obras, por ejemplo, colegios y viviendas de interés social, con elementos que puedan tener asbesto", afirmó Daniel Pineda, integrante del **movimiento Colombia sin asbesto y director de la Fundación Ana Cecilia Niño.**

Según un estudio del Instituto de Hidrología, Meteorología y Estudios Ambientales (IDEAM), el Distrito Capital es el departamento que más produce residuos de asbesto, con 1.324 toneladas de este material generado en 2016, seguido por el Valle del Cauca con solo 132 toneladas. (Le puede interesar: "[Histórico respaldo en Congreso a prohibición del asbesto](https://archivo.contagioradio.com/historico-respaldo-en-congreso-a-eliminacion-de-asbesto/)")

Tal como lo indicó Pineda, el proyecto recibió el apoyo unánime de las distintas bancadas del Concejo de Bogotá, incluyendo el Partido Verde, el Partido de la U, los Libres y el Centro Democrático. Por tal razón, el director expresó que "hay un muy buen ambiente" en el Concejo en torno al proyecto que podría dar paso a que sea aprobado en plenaria.

Sin embargo, el proyecto enfrenta resistencia por la administración del Alcalde de Bogotá Enrique Peñalosa que manifestó en un comunicado que el proyecto limitaría, de manera inconstitucional, "[el libre desarrollo de la actividad económica" en la capital.]

Al respecto, Pineda sostuvo que la posición del Alcalde podría estar relacionado con dinero que recibió durante su campaña. "Tenemos conocimiento de que su campaña probablemente fue financiada por Incolbest del Grupo Name, el cual es una compañía que usa asbesto para sus productos", expresó el director.

### **Las regiones se unen en contra del asbesto**

Varios municipios de Boyacá e incluso la administración de dicho departamento han tomado la decisión de prohibir el uso de asbesto en la construcción de obras públicas. Pineda espera que esta manifestación de las regiones influye en la votación de los senadores sobre el proyecto de ley que prohibiría este material a nivel nacional e impediría su exportación.

"Si a nivel de los municipios y territorios se están tomando decisiones que el gobierno a nivel nacional no ha podido tomar por falta de voluntad política, este es un grito de atención y un ejemplo para otros municipios y para el Congreso", afirmó. (Le puede interesar: "[Boyacá, el primer departamento en prohibir el uso de asbesto en el país](https://archivo.contagioradio.com/boyaca-el-primer-departamento-en-prohibir-el-uso-de-asbesto-en-el-pais/)")

<iframe id="audio_36727972" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36727972_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
