Title: Taponamiento del río Jiguamiandó en Chocó afecta a más de 6000 personas
Date: 2015-01-30 22:22
Author: CtgAdm
Category: Ambiente, Comunidad, Resistencias
Tags: Bajo Atrato, paz, Río Jiguamiandó, territorio
Slug: taponamiento-del-rio-jiguamiando-en-choco-afecta-a-mas-de-6000-personas
Status: published

###### Foto: JusticiaYPaz 

<iframe src="http://www.ivoox.com/player_ek_4020908_2_1.html?data=lZWfkp6UfI6ZmKiak5aJd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLi1srZjanJssrnjKfZw9PIs8%2Bf08rd1MrXqc%2FowtPhx5DQqcjVzZDRx9GPh9Di1MrX0ZCxpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Manuel Denis ] 

De acuerdo a Manuel Denis, representante legal del consejo comunitario del Río Jiguamiandó en el Bajo Atrato chocoano, se cree que el taponamiento del río ya lleva alrededor de los **21 kilómetros, cuando el problema había empezado con 300 metros en el año 2000**.

Pese a que al fallo emitido en el 2012 por la Corte Constitucional, que ordena el destaponamiento del río ([ver fallo de tutela)](http://justiciaypazcolombia.com/IMG/pdf/senetencia_rio_jigua.pdf), Manuel Denis, denunció que el gobierno ha sido negligente y hasta la fecha no se le ha dado una respuesta a la comunidad de parte del INVIAS,  razón por la cual, exigen una reunión con el vicepresidente Germán Vargas Lleras para que se logre una solución para las más de 6.292 personas que pertenecen a las 11 comunidades afectadas por el taponamiento del río.

El estancamiento, **ha generado la pérdida de siembras** como fuente de trabajo y sustento, **desplazamiento, enfermedades graves** como malaria, paludismo, neumonía e infecciones de piel y la inutilización del río como vía de transporte desde el año 2003.

“Ya no se cultiva nada, se están generando enfermedades… **si este año no logramos destaponar el río no podrá volver la población desplazada que debe retornar**”, señala Denis, quien añade “los daños ambientales son inmensos, no hay forma de que puedan repararnos”.

Algunas de las comunidades cercanas a la cuenca del río **Jiguamiandó hacen parte de la red CONPAZ**, y al verse afectadas de esta manera, Denis asegura que **es difícil construir la paz desde los territorios, si no se les han restablecido sus derechos.**

\[embed\]http://youtu.be/UKS4xakNLZ4\[/embed\]
