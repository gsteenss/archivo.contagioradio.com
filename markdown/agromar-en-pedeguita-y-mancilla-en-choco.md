Title: Empresa Agromar provoca desastre ambiental en Chocó
Date: 2017-06-20 13:48
Category: Ambiente, Voces de la Tierra
Tags: Agromar, campesinos, Chocó, deforetacion, Justicia y Paz, ocupaciones ilegales
Slug: agromar-en-pedeguita-y-mancilla-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/daños-en-curvardo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión Intereclesial Justicia y Paz] 

###### [20 Jun 2017]

Por lo menos 30 hectáreas de bosques primarios y fuentes de agua **fueron arrasadas por la empresa Agromar en el departamento del Chocó** y el territorio colectivo de Pedeguita y Mancilla, cercano a Belén de Bajirá. La comunidad denuncia que la actividad empresarial es ilegal y no se consultó a los legítimos propietarios, además no hay reacciones por parte de las autoridades de policía o militares a pesar de la denuncia.

La Comisión Intereclesial de Justicia y Paz había denunciado en reiteradas ocasiones que la empresa Agropecuaria Campesina Agromar S.A. “**ha arrasado bosques primarios y bienes para agronegocios ilegales**”. Estas ocupaciones ilegales continúan hoy y se traducen en nuevas formas de despojo donde el Estado ha sido incapaz de proteger los derechos de los pobladores de territorios como en Pedeguita y Mancilla en el Chocó. (Le puede interesar: "[Constructoras ponen en riesgo la "reserva forestal Río Blanco"](https://archivo.contagioradio.com/conctructoras-ponen-en-riego-la-reserva-forestal-rio-blanco/))

Según el señor Heleodoro Polo, quien cultiva plátano en sus predios en este territorio, “la empresa Agromar, en complicidad con el Consejo Comunitario de Pedguita y Mancilla, **están tumbando todo el bosque y se han metido de manera arbitraria a mi territorio** y al de 40 familias más”.

De acuerdo con la Comisión “la junta del Consejo Comunitario de Pedeguita y Mancilla **firmó sin consulta ni consentimiento de la comunidad la entrega de 20.000 hectáreas de tierra por un periodo de 100 años** para favorecer a la empresa Agromar”. Igualmente manifestaron que “En la propiedad del líder comunitario Heleodoro Polo Meza, 11 hombres realizaron adecuaciones en un área de 2 hectáreas para la siembra de plátano de exportación”.

### **Ocupaciones se hacen en medio de amenazas** 

Heleodoro Polo manifestó que en abril de este año 7 hombres armados ingresaron a su propiedad, que es Zona de Biodiversidad, **para continuar con el proceso de adecuación de tierras para la siembra de plátano de exportación**. Según Polo, “estos hombres vinieron a meterse a mi tierra con machetes desconociendo que soy propietario legítimo y legal”. (Le puede interesar: "[A 5 años de la ley de restitución, el despojo de tierras continúa](https://archivo.contagioradio.com/a-5-anos-de-la-ley-de-restitucion-el-despojo-de-tierras-continua/)")

Para la Comisión Intereclesial de Justicia y Paz “nada se ha hecho, ante la solicitud de intervención institucional, para evitar daños a la propiedad, el ambiente y la integridad personal de la familia Polo”. La Comisión ha reiterado en varias oportunidades la preocupación de que se “**estén tumbando los bosques destruyendo siembras de autoconsumo y fomentando el despojo** cuando se miden las fincas de familias propietarias para implementar proyectos de plátano y palma aceitera”.

### Empresa AGROMAR y sus negocios con el Concejo Comunitario 

En Junio de 2016 la empresa, representada por Bernardo José Guerra Genes firmó un convenio con Baldoino Mosquera, representante legal del Concejo Comunitario, que contempla la entrega por 100 años de 20.000 hectáreas a cambio de 3 millones de pesos por hactárea, de los cuales el 50% irían a las arcas de la entidad territorial. Los beneficios también incluirían a particulares con un 25% de ganancias.

Sin embargo esa operación también fue inconsulta y violatoria del procedimiento establecido en la Ley 70 que ampara esos territorios. Adicionalmente el señor Mosquera habría avalado el arrasamiento de los cultivos de pan coger de 2 familias hace cerca de 2 años. En su momento los agresores afirmaron que tenían el aval de Mosquera.

### Cuestionamientos a AGROMAR 

Varios son los directivos de la empresa que han sido señalados o enfrentan procesos por narcotráfico o vínculos con paramilitares. Por ejemplo, Guerra Genes fue señalado por un testigo de estar comprometido en el crimen de dos ciudadanos de Suecia y Finlandia en Turbo en 1992.

Ameth Abuchar, otro de los directivos de AGROMAR tuvo que comparecer en 2012 en audiencia prelimnar ante la justicia de panamá, puesto que es acusado de delitos contra la seguridad pública y tráfico de drogas.

También la Defensoría del Pueblo realizó un informe en que alerta del peligro de afectaciones territoriales en Pedeguita y Mancilla y señala que hay empresarios vinculados con operaciones armadas ilegales y despojo de tierras que están interesados en ese territorio. Entre los empresarios están Juan Guillermo Gonzáles, Luis Felipe Molano y Javier Restrepo.  [Ver denuncia de Comisión de Justicia y Paz ](http://justiciaypazcolombia.com/Por-la-fuerza-consolidan-agronegocios-en-predios-colectivos)

<iframe id="audio_19371981" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19371981_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
