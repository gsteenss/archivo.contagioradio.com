Title: Nominación a 'Nobel de DDHH' es un llamado a proteger líderes sociales
Date: 2018-10-26 14:21
Author: AdminContagio
Category: Entrevistas
Tags: afros, Marino Córdoba AFRODES, Operacion genesis
Slug: nominacion-marino-cordoba-martin-ennals
Status: published

###### Foto: Atlanta Black Star 

###### 26 oct 2018 

La nominación de **Marino Córdoba** al premio **Martín Ennals Award**, considerado como el Nobel de los Derechos Humanos, además de reconocer su labor en defensa de las comunidades afro del pacífico, representa **un llamado de atención por la grave situación que enfrentan los líderes sociales en Colombia**.

Córdoba, quien es sobreviviente de la operación Génesis en 1997 y fundador de la Asociación Nacional de Afrocolombianos Desplazados AFRODES, afirma que la distinción "**es un gran reconocimiento de la comunidad internacional a labor, la lucha, la situación de nuestras comunidades que hacen para tratar de exaltar un poco lo que esta pasando en nuestro país**" siendo uno de los 3 defensores que ostentan el galardón a entregarse el próximo 13 de febrero en Ginebra, Suiza.

Sin embargo, apunta que el llamado de atención que llega de la mano con la nominación, va dirigido a la institucionalidad del Estado "por **la precaria situación en la que los defensores de Derechos humanos ejercen esa labor** importante en defensa de sus comunidades" quienes son estigmatizados, perseguidos, asesinados por defender la paz.

Frente al olvido y marginalidad estatal en regiones afectadas históricamente por el conflicto armado, Córdoba asegura que hoy pese a la firma de los acuerdos de paz "**la guerra sigue tomando fuerza en esos territorios y las comunidades siguen a merced de los violentos**", en relación con la continua presencia de actores armados que amenazan la vida de los pueblos indígenas, campesinos y comunidades afro.

"No es justo que nosotros sigamos padeciendo un conflicto que sigue tomando fuerza en estas regiones y que el resto de la sociedad desconoce o que simplemente ignora porque no se esta desarrollando en otros lugares donde la sociedad privilegiada colombiana vive, **nosotros somos colombianos, somos ciudadanos de este país y no es justo que haya esa indiferencia frente al desarrollo de la guerra**" manifiesta el líder social.

Para Marino el tema que motiva este tipo de reconocimientos es el derecho a la vida "por el cual nosotros luchamos, trabajamos, llamamos la atención  y **ese derecho a la vida esta siendo vulnerado, restringido, tenemos que seguir haciendo todo lo posible para que haya mayor solidaridad frente a esta situación** a nivel internacional pero también haya más atención del estado colombiano" en relación con las garantías de protección y respeto para los hombres y mujeres "que entregamos todo para ser visibles realidades invisibles".

<iframe id="audio_29636788" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29636788_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
