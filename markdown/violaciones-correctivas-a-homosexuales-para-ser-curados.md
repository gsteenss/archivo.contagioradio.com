Title: Violaciones correctivas a homosexuales para ser ''curados''
Date: 2015-06-05 14:05
Category: Escritora, Opinion
Tags: Homosexualidad, LGBT, Massooa Salone, Ramesh Tawadkar, Sizakele Sigasa, Sudafrica, violación correctiva
Slug: violaciones-correctivas-a-homosexuales-para-ser-curados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/protesta-575x323.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La pronvincia.es 

#### **[[Escritora Encubierta ](https://archivo.contagioradio.com/escritora-encubierta/) ]** 

Gran polémica está causando en el Internet la noticia de un joven homosexual que fue obligado a mantener relaciones sexuales con su madre para curar su orientación sexual. Se le llama ''violación correctiva'', y al parecer es un acto muy común en la India. Basta sólo una sospecha para que los padres de la víctima pacten la violación y escojan a algún familiar que la lleve a cabo. Así lo denuncia la organización de Lesbianas, Gays, Bisexuales y Transexuales de Telangana al sur de dicho país.

Ésta entidad ha dado a conocer 15 casos parecidos, aunque se presume que hay muchos más que no han salido a la luz debido a que las víctimas prefieren callar por la naturaleza traumatizante de estos crímenes horrorosos. Además, el contexto jurídico y social no ayuda a motivar la denuncia puesto que India es uno de los países donde hay más rechazo a la homosexualidad, la cual puede ser penalizada hasta con años de cárcel alegando que es una enfermedad importada por la cultura occidental. Y, por si fuera poco, a principios de este año el ministro Ramesh Tawadkar anunció que hay planes para abrir centros que ofrezcan tratamiento para hacer normales las personas de la comunidad LGBT.

Las violaciones correctivas no sólo ocurren en el país indio, esta práctica brutal constituye una crisis en Sudáfrica, donde las víctimas son únicamente las mujeres, siendo éste un crimen doble; por el género y por su orientación sexual. A diferencia de India, los actos no son acordados por familiares, cualquier persona de la calle puede llevarlo a cabo sin importar si conoce o no a la víctima.

Un caso ejemplar fue el de Sizakele Sigasa y Massooa Salone, pareja lésbica que se encontraba en un bar cuando un grupo de hombres empezó a abuchearlas y gritarles ''marimachos''. Las mujeres fueron violadas múltiples veces, torturadas, amarradas con sus interiores y asesinadas a disparos en la cabeza. Nadie fue condenado. Otro caso bastante sonado y que recorrió el mundo fue el de la futbolista del equipo nacional y activista LGBT, Eudy Simelane, quien fue violada por un grupo de personas, golpeada y posteriormente asesinada a 25 puñaladas. El caso de Eudy es considerado inusual puesto que por primera vez los culpables fueron condenados con pena de cárcel.

Las estadísticas dicen que semanalmente 10 mujeres lesbianas son violadas en Sudáfrica. Tan sólo una de cada 25 violaciones llegan a los tribunales y contados con los dedos de la mano son los casos en los que los criminales llegan a ser condenados. Las mujeres que logran sobrevivir y que han contado sus testimonios señalan que los agresores emplean frases como ''Después de todo lo que vamos a hacerte vas a ser una mujer de verdad y nunca volverás a actuar de este modo'', Estas personas generalmente creen que les están haciendo un favor a las mujeres y que están curándolas.

Es desalentador ver cómo el país que en su momento luchó por el derecho a la libertad, igualdad, el respeto, y la diversidad, ahora está abandonando sus ideales post-apartheid y se está convirtiendo en un territorio hostil que acolita la realización de tan repugnante y denigrante acto. Las lesbianas no son las únicas afectadas, 500.000 es el número de mujeres agredidas por año, todas víctimas del imponente machismo africano, y mientras tanto, las autoridades no se pronuncian ni toman medidas al respecto.

El continente americano tampoco se salva. Muy conocidas son las clínicas de deshomosexualización ecuatorianas que, por una suma de dinero propinada por parientes, los homosexuales son sometidos a torturas como electroshocks, privación del sueño y la comida, humillación y violaciones. Afortunadamente, las autoridades se han encargado de clausurar estos repulsivos lugares.

¿Hasta dónde puede llegar la homofobia de una persona para realizar estos actos atroces? Los homofóbicos pueden creer que los homosexuales son unos enfermos, pero realmente, en calidad de actos, los únicos enfermos son ellos. Ninguna persona en el mundo debe ser víctima de violaciones y agresiones de este tipo, menos una que no ha hecho otra cosa más que amar a alguien de su mismo sexo.

##### Si después de leer todo esto, usted se siente afligido y con ganas de hacer algo, le invito a poner su grano de arena y firmar para que se acabe este crimen: <http://www.avaaz.org/es/stop_corrective_rape/97.php?cl_tta_sign=9ed85ad7f02f1bc21d20d396c03d5343> 
