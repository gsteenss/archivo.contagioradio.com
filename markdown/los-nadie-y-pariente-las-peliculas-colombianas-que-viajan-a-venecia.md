Title: Los nadie y Pariente, las películas colombianas que viajan a Venecia
Date: 2016-07-28 11:15
Author: AdminContagio
Category: 24 Cuadros
Tags: 73 Festival de Cine de Venecia, Cine Colombiano, Los nadie película colombiana, Película Pariente
Slug: los-nadie-y-pariente-las-peliculas-colombianas-que-viajan-a-venecia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/pelis-venecia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  

##### [28 Jul 2016] 

Los organizadores de la **edición 73 del Festival Internacional de Cine de Venecia**, que tendrá lugar entre el 31 de agosto y el 8 de Septiembre próximos, han confirmado la participación de dos largometrajes nacionales, **"Los nadie"** del antioqueño Juan Sebastián Mesa y **"Pariente"** del santandereano Ivan D. Gaona.

Por un lado **"Los nadie"** competirá en el Festival por los premios León del Futuro (Premio Luigi de Laurentiis a mejor ópera prima) que entrega 100.000 dólares, el premio de la audiencia de **la Semana de la Crítica** consistente en 5.000 euros y el Mario Serandrei-Hotel Saturnia a mejor contribución técnica.

La cinta que tendrá su estreno el 15 de septiembre, narra la historia de Pipa, Camilo, Ana, Manu y ‘El Mechas’, 5 jóvenes que se conocen en las hostiles calles de la ciudad de Medellín, lugar que utilizan como escenario para desarrollar su arte, encontrando en allí un refugio y una oportunidad para escapar de su realidad.

[![Los nadie](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/cc5c14cea17a9b702192fc91210a8b19.jpg){.aligncenter .size-full .wp-image-26925 width="560" height="373"}](https://archivo.contagioradio.com/los-nadie-y-pariente-las-peliculas-colombianas-que-viajan-a-venecia/cc5c14cea17a9b702192fc91210a8b19/)

La otra cinta nacional en competencia es el largometraje "Pariente" en la Sección **Venice Days **(Giornato Degli Autori), categoria que nació en el año 2004 con la intención de centrar la atención en el cine de alta calidad sin ningún tipo de restricción, con especial cuidado en la innovación, investigación, originalidad e independencia.

**"Pariente"** tiene como protagonista a Willington, un camionero transportador de caña de azúcar en el municipio de Güepsa, Santander, quien busca recuperar el amor de su ex novia Mariana, al tiempo que ella pretende casarse con su nuevo novio, después de su premier mundial en Venecia, la cinta será estrenada en salas colombianas el 13 de Octubre.

![Pariente](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/pariente2_0.jpg){.aligncenter .wp-image-26926 width="554" height="362"}

La Semana de la Crítica, sección paralela del **Festival Internacional de Cine de Venecia**, se ha venido consolidado como un espacio, que durante más de tres décadas, ha servido para descubrir grandes talentos en ascenso como Abdellatif Kechiche, Harmony Korine y Pablo Trapero, quienes tuvieron la oportunidad de presentar allí sus primeros trabajos, esta será la primera vez que una producción colombiana hace parte de este grupo y podrá verse en esta sección en Venecia.
