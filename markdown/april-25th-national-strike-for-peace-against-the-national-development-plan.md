Title: April 25th :  National strike for peace against the National Development Plan
Date: 2019-04-24 14:53
Author: CtgAdm
Category: English
Tags: National Development Plan, Strike
Slug: april-25th-national-strike-for-peace-against-the-national-development-plan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/CUT-Paro-Nacional.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**On April 25^th ^there will be a large national strike in which labor unions, political, social and peasant movements will participate.** This demonstration will have as purpose the fulfillment of the agreements with the different sectors (that still incomplete), the defense of peace and the adjustment of the National Development Plan (PND) which is the priority of the majority of Colombians.

**As explained by the Confederation of Workers’ Attorney Fabio Arias**, all sectors that have joined this call understand the system of negotiation of Government and object it. They  are united around the defense of peace.

In that sense, one of the main issue of struggle will be to avoid amendments to the Statutory Law of the Special Jurisdiction for Peace (JEP), and the rejection of presidential objections.

The second claim is related to the accomplishment of the agreements done by the Colombian State with popular sectors. Same demands regarding this strike motivated also the Minga: the protest movement  advanced by the Indigenous of Cauca and other departments more than two weeks ago. **In this sense, the contest of the trade union will be focused on the guarantees of respect to the agreement with professors, students, pensioners, workers, transporters and peasants.**

**Finally, the strike will seek to modify the National Development Plan in order to erase all the articles and provisions present in the Investment Plan in disadvantage to the interests of the majority of Colombians.**

These claims seem quite difficult to be addressed by the Duque government, which after a 22-day of Minga still does not attend the territory to dialogue.  Arias has certain conviction on this mobilization established on Thursday, April 25^th.^Peasant sectors as the Cumbre Agraria andCongreso de los Puebloshave already joined it. Moreover Arias is expecting the arrival of students, transporters, entrepreneurs, coffee farmers (plunged into a deep crisis due to the price of grain) and other citizen sectors.
