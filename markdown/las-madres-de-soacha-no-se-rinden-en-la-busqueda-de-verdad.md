Title: Las Madres de Soacha no se rinden en la búsqueda de verdad
Date: 2019-10-19 08:17
Author: CtgAdm
Category: DDHH
Tags: crimen de lesa humanidad, JEP, madres de soacha, verdad
Slug: las-madres-de-soacha-no-se-rinden-en-la-busqueda-de-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/MADRES-DE-SOACHA-EN-LA-JEP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:JEP] 

Este jueves 18 de octubre desde las 8 a.m. las madres y familiares de **19  víctimas de ejecuciones extrajudiciales de Soacha**, se reunieron en una audiencia pública [ante la Jurisdicción Especial para la Paz (JEP), con la intención de presentar sus argumentos frente a las 31 versiones rendidas por comparecientes de la Brigada Móvil XV y el Batallón de Infantería “Francisco de Paula Santander”, vinculados a este caso, así mismo **exigieron que este hecho sea declarado crimen de Estado y de lesa humanidad.**]

Durante la audiencia, las madres[ exigieron]a los magistrados de la JEP celeridad con todas las preguntas que  aún están sin resolver, **incluso, tras 11 años de haberse cometido este hecho y**  manifestaron su incomodidad por los 20 minutos que tuvieron para intervenir en comparación a la cantidad de horas que tuvieron los militares para hablar.

[Según [**Fernando Rodríguez Kekhan**, abogado del Comité de Solidaridad con los Presos Políticos (CSPP)]

[Otra de las  exigencias que hacen las madres de Soacha es que en estos escenarios se logren aportes diferentes a los que ya están registrados en los folios de la investigación,"en el proceso hemos llegado a un 25% o 30% de verdad; falta mucho por decir por parte de lo comparecientes, y eso es lo que ellas están exigiendo, que se diga la verdad plena" añadió Rodríguez.]

Hasta el momento la JEP ya hizo el llamado a **15 miembros de las Fuerzas Militares,** 2 oficiales, 1 suboficial y 11 soldados, quienes responderán por los delitos de **concierto para delinquir, homicidio y desaparición forzada,** "e[llas han sentido y podido verificar que están siendo atendidas, pero esto es solo el 15% del proceso["] resaltó Rodríguez. ]

Asimismo, de las 155 versiones sobre casos de ejecuciones extrajudiciales que ha recibido el Tribunal , 31 de estas tiene que ver con las ejecuciones extrajudiciales de Soacha; por tal razón, esta es solo la primera audiencia que permitirá a la Jurisdicción corroborar las versiones que dieron los militares que buscan ser amparados con beneficios judiciales y al mismo tiempo continuar con el camino hacia el esclarecimiento de los hechos cometidos y las responsabilidades de autores intelectuales.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]

 
