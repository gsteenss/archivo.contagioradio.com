Title: 51 internos de todo el país se unen a huelga de hambre por incumplimientos del gobierno
Date: 2016-01-28 21:18
Category: DDHH, Nacional
Tags: crisis carcelaria, Eron Picota, INPEC, presos politicos, proceso de paz
Slug: internos-de-la-picota-ya-cumplen-7-dias-en-huelga-de-hambre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Crisis-humanitaria-cárceles.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

<iframe src="http://www.ivoox.com/player_ek_10246574_2_1.html?data=kpWflpuZe5Whhpywj5WaaZS1lJWah5yncZOhhpywj5WRaZi3jpWah5yncari1crf0NTXb8XZjNHOjbXNp9DowpDmw5DHuc7kzcrbjZyPqIa3lIqupsbXb8bijM3ix9HLpYzYxpDVj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [28 Ene 2016.]

Sin agua, sin medicamentos, sin recibir luz natural, y en total hacinamiento, viven los internos del centro penitenciario el ERON Picota, cuya situación ha generado que algunos prisioneros ya cumplan siete días de huelga de hambre exigiendo atención por parte del Estado.

Pese a que dentro de los acuerdos pactados en la Habana, el Gobierno se comprometió a realizar brigadas de salud en todas las cárceles del país, la situación continúa empeorando y los internos que se encuentran en grave estado de salud no reciben ningún tipo de atención médica.

Esta situación ha hecho, que varios presos integrantes de la guerrilla de las FARC, hayan iniciado una huelga de hambre desde el pasado lunes, y algunos hayan decidido cocerse la boca como símbolo de protesta. De hecho, de acuerdo a los internos, la huelga se estaría extendiendo a todas las cárceles del país donde la situación es igual de humillante tanto 5para los reclusos, como para las familias que quieren visitarlos pero que también terminan siendo víctimas del sistema carcelario en Colombia.

“Es una tortura y una humillación… se atenta contra la dignidad humana”, expresa el profesor de la Universidad Nacional, Miguel Ángel Beltrán, preso político que lleva 6 meses sin recibir luz natural, no lo dejan ingresar prensa o libros, las visitas conyugales las debe atender en el piso, y además, no le suministran agua suficiente, como le sucede al resto de los internos.

Quienes adelantan la huelga, han asegurado que esta continuará hasta tanto, el Estado y específicamente el INPEC haga cumplir los acuerdos pactados en el marco del proceso de paz.

Mientras tanto,  en otros centros carcelarios del país como en Popayán en la cárcel de San Isidro, se inicia desde este jueves una jornada de desobediencia pacífica, “hasta que se brinde una solución definitiva frente al mal manejo y mala preparación de los alimentos recibidos”, anuncian en un comunicado los internos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
