Title: Jesús Díaz, comunero indígena del Resguardo Las Delicias es asesinado
Date: 2019-02-12 18:07
Author: AdminContagio
Category: Comunidad, DDHH
Tags: ACIN, Asesinato de indígenas, Norte del Cauca
Slug: jesus-diaz-comunero-indigena-del-resguardo-las-delicias-es-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/CRIC-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CRIC 

###### 12 Feb 2019

[El pasado 9 de febrero fue asesinado el comunero indígena Jesús Albeiro Díaz Ulcué de 40 años mientras se encontraba en su zona de trabajo de minería artesanal en la vereda **San Antonio de Santander de Quilichao.** Díaz era habitante del resguardo de Las Delicias en el municipio de Buenos Aires, Cauca.]

[Según relata Edwin Capaz, coordinador derechos humanos de la Asociación de Cabildos Indígenas del Norte del Cauca (ACIN)  Jesús Díaz fue interceptado por tres desconocidos mientras se encontraba en su zona de trabajo en la vereda de San Antonio para luego ser trasladado a la fuerza hasta las afueras de la vereda.[(Le puede interesar: Recuperar su territorio le está costando la vida a indígenas en Cauca)](https://archivo.contagioradio.com/nos-estan-asesinando-llamado-de-comunidades-indigenas-del-cauca/)  
  
Su cuñado, testigo de los hechos, logró escapar y dar aviso a las autoridades indígenas quienes encontraron el cuerpo de Díaz en un lago cercano al sitio conocido como Aguas Limpias con seis impactos de arma de fuego. Para avanzar con la investigación, la autoridad indígena ha llamados a algunos habitantes de la zona para que aporten con su testimonio y pueda darse con el paradero de los responsables.  
  
“**En el 2018 llegaron a ser 46 asesinatos a indígenas, hubo un incremento sustancial y que ya en enero superamos lo visto el año pasado, alrededor de siete asesinatos más en enero del 2019 y está repuntando la cifra en febrero”** afirma Capaz quien señala que de seguir la tendencia se superaría este año los registros del año pasado.]

[Jesús Albeiro, minero y padre de seis hijos, no ejercía una labor de liderazgo dentro de la comunidad de Las Delicias, tampoco había recibido amenazas con anterioridad pero su muerte al igual que toda muerte violenta dentro del territorio indígena tiene una connotación que afecta de manera negativa la armonía comunitaria” explica Capaz quien también informó que **este crimen ha aumentado la zozobra en los habitantes del resguardo.**]

[El Coordinador de DD.HH explica que en el sector de Buenos Aires y Suárez, hay presencia de unidades del ELN y de las disidencias de las Farc, sin embargo manifestó la esperanza de que el asesinato no haya sido cometido por alguno de estos grupos armados pues esto significaría un recrudecimiento de la violencia en la región teniendo en cuenta la historia de violencia que se ha vivido en el norte del Cauca.]

###### Reciba toda la información de Contagio Radio en [[su correo]
