Title: Referendo uribista va más allá de una simple venganza contra el sistema judicial: Beltrán
Date: 2019-06-18 11:33
Author: CtgAdm
Category: Entrevistas, Política
Tags: Consejo de Estado, Corte Constitucional, Corte Suprema de Justicia, referendo
Slug: referendo-uribista-va-mas-alla-de-una-simple-venganza-contra-el-sistema-judicial-beltran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Referendo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @RaJHeP] 

En los más recientes días a resurgido la propuesta de legitimar un  'Estado de Opinión', un término usado durante el Gobierno por el actual senador, Álvaro Uribe, quien ha propuesto la creación de un referendo que desmontaría la rama judicial y acabaría con la **Jurisdicción Especial para la Paz (JEP)** creando una única súper corte bajo la influencia del ejecutivo; con lo que se atacaría la separación de poderes, uno de los pilares del Estado de Derecho.

### Referendo: se utiliza un medio democrático con una finalidad antidemocrática

**El profesor y exmagistrado de la Corte Constitucional, Alfredo Beltrán** señaló que esta iniciativa está siendo impulsada por sectores a los que no les ha gustado que la Corte Suprema en el ejercicio de sus funciones "haya investigado a funcionarios del Estado y servidores públicos que a pesar de tener fuero político" hayan sido castigados con penas carcelarias o se les haya privado de su investidura parlamentaria a través del Consejo de Estado.

Otra de las razones por las que se buscaría promover este referendo es que estos mismos sectores no ven con agrado que en los 28 años que tiene de existencia la Corte Constitucional, "haya hecho lo posible por transformar la "pléyade" de derechos fundamentales consagrados en ella y que haya defendido sus derechos colectivos".

De igual forma indicó que las previas objeciones a la JEP impulsadas por el presidente Duque y frenadas por la Corte Constitucional, avalando que puedan escucharse en este mecanismo judicial los testimonios de declarantes que pueden identificar responsables de delitos y contar la verdad del conflicto armado.

### El Estado de Opinión ha sido usado por las dictaduras del mundo

El exmagistrado agregó sobre el Estado de Opinión que de plantearse este referendo las cortes serían "suprimidas de tajo para reemplazadas por una que tendría profunda influencia del Ejecutivo", lo que dista de ser un Estado de derecho que implica "reflexión, controversia y discusión" para la adopción de decisiones judiciales.

Agregó que la opinión cambia con facilidad, por lo que no se sabrían los alcances de la misma**, "las reglas fijas del derecho están diseñadas para resultados inciertos, aquí habría opiniones inciertas para reglas que no sabemos a dónde vamos a conducir".** [(Lea también: La conducta guerrerista del senador Uribe)](https://archivo.contagioradio.com/conducta-guerrerista-senador-uribe/)

Con relación a los referendos anteriores propuestos en 2003 y 2006, indicó que  la diferencia radica en que "una cosa es la mayoría fluctuante en unas elecciones para conformar los órganos del poder y otra intentar modificar las normas al calor de los acontecimiento procurando que se cree previamente un estado de malestar".

<iframe id="audio_37274535" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37274535_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
