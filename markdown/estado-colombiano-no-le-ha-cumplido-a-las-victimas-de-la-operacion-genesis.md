Title: Estado colombiano no le ha cumplido a las víctimas de la Operación Génesis
Date: 2015-01-24 00:09
Author: CtgAdm
Category: DDHH, Resistencias
Tags: cacarica, DDHH, Operacion genesis, Paramilitarismo
Slug: estado-colombiano-no-le-ha-cumplido-a-las-victimas-de-la-operacion-genesis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/amp-logo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: es.amnesty.org 

El 20 de Noviembre de 2013, la **Corte Interamericana de DDHH** dictó sentencia contra el Estado colombiano por su responsabilidad en las **violaciones a los Derechos Humanos cometidas durante el desarrollo de la Operación Génesis** en la Cuenca del Río Cacarica en 1997. Un año después de esa sentencia las víctimas siguen esperando que el Estado cumpla con las órdenes de la Corte Interamericana.

Hoy, en un ejercicio de memoria, las mujeres de **CLAMORES**, grupo que decidió quedarse en Turbo y no retornar afirma que necesitan reconstruir el monumento que han sostenido durante 17 años con sus propias manos, con su propio trabajo y esfuerzo. “Necesitamos volver a poner los nombres” afirma Josefina, una de las integrantes del grupo, que además exige que el Estado les cumpla, les de la reparación tal como ellas la quieren.
