Title: President Duque faces mounting pressure over military operation that killed 8 children
Date: 2019-11-08 17:01
Author: CtgAdm
Category: English
Tags: child recruiment, FARC dissident groups, Guillermo Botero, Minister of Defense, President Iván Duque
Slug: president-duque-faces-mounting-pressure-over-military-operation-that-killed-8-children
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-1-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: @FuerzasMilCo ] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[The administration of President Iván Duque is facing a storm of criticism after a senator of the opposition, Roy Barreras, denounced a military operative against a rebel dissident group that killed at least eight children between the ages of 12 and 17. The Minister of Defense Guillermo Botero stepped down hours after the scandal broke, but did not address the controversy in his resignation letter. ]

[During a censure motion hearing against the defense minister, Senator Barreras denounced that, according to information provided by the Medical Forensics Agency, the bombardment of a FARC dissident camp on Sept. 2 left at least eight minors dead: Abimiller Morales, 17 years old; Willmer Castro, 17; Diana Medina, 16 años; José Rojas Andrade, 15; Jhon Edinson Pinzón, 17; Ángela María Gaitán, 12; and Sandra Patricia Vargas, 16. According to the senator, this information was hidden from the public. ]

[According to Herner Carreño, the municipal ombudsman of Puerto Rico, Caquetá, the children were recruited in the hamlets of Pringamoso, Villa Hermosa Alta and La Flor. The analyst Víctor de Currea Lugo said that the former defense minister should be tried for his role in the operations, given that the ombudsman had warned months prior in a security council meeting that the children killed in the bombardment had been forcibly recruited by the armed group. ]

[The Minister of Defense Botero ultimately resigned to avoid what was expected to be the first censure motion to pass in Congress. President Duque announced on Twitter that General Luis Navarro, commander of the Military Forces, would be the new defense minister. General Navarro defended the operation by saying the military was not aware of the presence of children at the camp.]

[De Currea Lugo pointed out that it was public knowledge that these children had been recruited and claimed that the attack had been premeditated. “The international humanitarian law is clear in the civilians that take military decisions within a conflict are equally responsible and in this sense, Botero must respond,” said the analyst.]

[He also warned that the root of the problem is not the former defense minister, but the military doctrine of the government that allows for these crimes to happen, such as the assassination of Dimar Torres in the Catatumbo region and Flower Trompeta in Cauca. Juan Sebastián Soris, of the platform Justapaz, added that the government is responsible for these deaths despite the president’s attempt to blame the guerrilla group for recruiting children. “They are kids that the state failed, were recruited and later presented as great combat kills, the success of an operation,” Soris said.]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
