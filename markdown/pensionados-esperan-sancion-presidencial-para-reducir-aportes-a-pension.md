Title: Pensionados esperan aprobación presidencial a ley que defiende sus derechos
Date: 2017-07-18 13:53
Category: DDHH, Paz
Tags: Congreso, Leyes, Pensionados, pensiones, sanción presidencial
Slug: pensionados-esperan-sancion-presidencial-para-reducir-aportes-a-pension
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Pensionados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Confidencial Colombia] 

###### [18 Jul 2017] 

Las y los pensionados del país esperan que el día de hoy sea sancionada por el presidente de la República, Juan Manuel Santos, **la ley que reduce los aportes de salud de esta población de un 12% al 4%.** Ellos y ellas han convocado a una movilización en espera de mejores condiciones para el sector de los pensionados en Colombia.

Victoria Escobar, vocera del Colectivo Feminista Pensionadas por los Derechos, manifestó que **“desde 1993 ha habido una afectación a los jubilados que pagamos el 12% de nuestros ingresos para la salud y la pensión**”. Este pago, según ella, aumenta con las tarifas del Índice de Precios de Consumo IPC y el poco incremento del salario mínimo hace que el cobro sea aún mayor. (Le puede interesar: ["Sistema de pensiones favorece a los más ricos"](https://archivo.contagioradio.com/las-pensiones-en-colombia-benefician-a-las-entidades-privadas-desigualdad-en-el-pais-ambito-laboral-jubilacion/))

Escobar afirmó que los pensionados llevan **3 años exigiéndole al Congreso de la República que apruebe el proyecto de ley.** Con el apoyo del Polo Democrático lograron que esto sucediera y sólo hace falta que la sanción presidencial para la entrada en vigencia de la ley.

**El cobro de las pensiones es un tema que afecta a toda la población**

Escobar fue enfática en manifestar que toda la población tiene que ver con el tema pensional. Hoy en día, **Colombia tiene grandes problemas en el sector laboral que perjudican a las personas en edad de jubilación** que no llegan a tener condiciones de vida de calidad. Es por esto que es de gran importancia que el presidente de la República “cumpla con lo que prometió en su campaña electoral”. (Le puede interesar: ["OCDE propone igualar edad de jubilación de hombres y mujeres"](https://archivo.contagioradio.com/ocde-propone-igualar-edad-de-jubilacion-de-hombres-y-mujeres/))

Adicionalmente y como lo afirman medios de información como Noticias Uno, según el Banco Interamericano de Desarrollo BID "la mayoría de los colombianos aporta al sistema de pensiones pero **no logra cotizar lo necesario para recibir una pensión**". Esto hace que, sus aportes subsidien pensiones de "exfuncionarios que reciben 40 billones de pesos".

Escobar manifestó que “para la movilización hemos contado con el respaldo de mucha gente en cuanto ha habido una gran participación en los debates en el Congreso”. Dijo además que “**desde hace 23 años no se había avanzado tanto en la discusión de las mejoras del sistema pensional** para que las personas salgan de la desesperanza en la que se encuentran”.

Cabe resaltar que las mujeres pensionadas esperan que hoy también sean firmadas las leyes que buscan reducir el tiempo para alcanzar la edad de jubilación de **aquellas que ganan menos de dos salarios mínimos.**

<iframe id="audio_19868626" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19868626_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
