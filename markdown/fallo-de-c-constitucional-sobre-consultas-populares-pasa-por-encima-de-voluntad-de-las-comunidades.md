Title: Comunidad de Cumaral llevaría fallo de la C. Constitucional a instancias internacionales
Date: 2018-10-12 16:53
Author: AdminContagio
Category: Ambiente, Movilización
Tags: consultas populares, Corte Constitucional, Cumaral
Slug: fallo-de-c-constitucional-sobre-consultas-populares-pasa-por-encima-de-voluntad-de-las-comunidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/consulta-popular-e1548961936100.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Archivo Particular ] 

###### [12 Oct 2018] 

Tras el pronunciamiento de la Corte Constitucional sobre la consulta popular de Cumural, Jerson López, integrante del comité impulsor por el no, afirmó que esa decisión ha generado tristeza e indignación en los habitantes de este municipio, debido a que no se respetó la determinación del constituyente primario. **Sin embargo, señaló que están evaluando en la comunidad, si llevar este proceso a instancias internacionales.**

López aseguró que ese fallo va en contravía de la Constitución y viola la soberanía nacional, que tendría como principal interés "el beneficio del capital financiero internacional por medio de las multinacionales". (Le puede interesar:["Gobierno quiere hacerle mico a las consultas populares con el presupuesto general de la Nación"](https://archivo.contagioradio.com/gobierno-quiere-hacerle-mico-a-las-consultas-populares-con-el-presupuesto-general-de-la-nacion/))

De acuerdo con el abogado Rodrigo Negrete, esta sentencia por parte de la Corte ya se veía venir, debido a que desde un principio y luego del cambio de juristas, se afirmó que uno de los temas más importantes que se tratarían sería la limitación de las consultas en los territorios.

Asimismo aseveró que si bien no se conoce aún el fallo publicamente, este no tendría afectaciones sobre las consultas que ya se realizaron, solamente  es vinculante a la consulta popular realizada en Cumaral y a las próximas **consultas que se pongan en marcha, la más cercana sería la del próximo 21 de octubre en Fusa, Cundinamarca**.

López expresó que hace un llamado al movimiento ambientalista y a las comunidades del país en donde se pretende realizar consultas populares a que unan esfuerzos y de esa manera "resistir" por la defensa de los territorios.

###### Reciba toda la información de Contagio Radio en [[su correo]
