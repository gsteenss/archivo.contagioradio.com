Title: Electric Mistakes en Sonidos Urbanos
Date: 2016-09-14 12:29
Category: Sonidos Urbanos
Tags: bandas, Cultura, Música, rock
Slug: electric-mistakes-en-sonidos-urbanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Electric-Mistakes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [Electric Mistakes](https://www.facebook.com/electricmistakes) 

El rock mestizo junto a la vibración imponente del uso del error y la intuición en la música, se hizo presente en esta sesión de S**onidos Urbanos con Electric Mistakes**; una banda bogotana integrada por **Laura Perilla** (batería) y **Juan Hernández** (voz, guitarra) que a través de sonidos fusionados entre rock, ranchera, champeta, boleros, y corridos, expresa la fuerza de la libertad, de la lucha por los sueños y las apuestas musicales de las bandas locales; evocando su amor por los animales, en memoria de los perros y gatos que han crecido a su lado, como Chavela y Mandarina.

No se pierda las historias, confesiones, y la música de **Electric Mistakes, una banda** que lo pondrá a bailar sin parar.

<iframe id="audio_12905290" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12905290_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
