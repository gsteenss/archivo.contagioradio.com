Title: "Duque must be clear about his stance on the peace deal": Retired General
Date: 2019-05-30 12:17
Author: CtgAdm
Category: English
Tags: Duque, military forces
Slug: duque-must-be-clear-about-his-stance-on-the-peace-deal-retired-general
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/150217-Foto-Reunion-Suiza-01.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

A recent New York Times report questioned the military command's decision to order an increase in the number of combat kills. According to sources consulted by the newspaper, these type of instructions would be paving the way for the return of extrajudicial killings, which piqued during the years 2002 to 2008.

This report adds to growing concerns over the actions of the Armed Forces, especially following the high-profile case of the extrajudicial killing of a FARC ex-combatant, Dimar Torres. The retired Gen. Rafael Colón argued that during this transitional phase in the country, in which Colombia attempts to implement a monumental peace deal, it's fundamental that those in power pressure the Military to support this process.

### **Dimar Torres: The dark side of the Armed Forces** 

Once it was revealed that the ex-combantant Dimar Torres was extrajudicially killed by members of the Vulcano Task Force in the Norte de Santander region, the military unit's commandar Diego Luis Villegas Muñoz made a public apology, in which he issued a public apology for what he assumed was a crime committed by his troops.

This declaration wasn't well received by the highest ranks of the Military Forces, which allegedly considered forcing the brigadier general into retirement. In one leaked audio, a high-ranking official condemned the Villegas Muñoz's apology. (Related: "[Academics around the world ask Duque for serious human rights policy](https://archivo.contagioradio.com/more-than-250-academics-criticize-duque-for-violence-against-activists/)")

The retired general characterized the actions of General Villegas as "noble" and "very commendable," adding that the declarations recorded in the audio are reproachable "because in no way are they in line with the teachings we, those who wear the uniform, receive." On the contrary, the retired military officer critized the stance that military high command taken on issues such as the protection of social leaders and support for the peace deal.

### **"Duque must be clear about his stance on the peace deal"** 

Colón also criticized the president for his lack of clarity on his position toward the peace deal, which he argued has a direct effect on the stance of the troops because the president acts as the commander-in-chief of the Military Forces and therefore, exerts power over them.
