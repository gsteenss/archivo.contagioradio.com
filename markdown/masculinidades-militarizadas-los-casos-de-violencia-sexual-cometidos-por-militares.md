Title: Masculinidades militarizadas: los casos de violencia sexual cometidos por militares
Date: 2020-07-05 19:19
Author: AdminContagio
Category: Columnistas invitados, Nacional, Opinion
Tags: Embera Chami, Embera Katio, FFMM, Fuerzas militares, militares, Militarización de territorios, MMV
Slug: masculinidades-militarizadas-los-casos-de-violencia-sexual-cometidos-por-militares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/WhatsApp-Image-2020-06-29-at-5.04.30-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: Gabriel Galindo/ Contagio Radio {#foto-por-gabriel-galindo-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### [Por: **Diana Salcedo López**, **Directora** **Liga Internacional de Mujeres por la Paz y la Libertad** y **Verónica Recalde Escobar**, **Investigadora LIMPAL Colombia**](https://www.limpalcolombia.org/es/)

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### El contexto de la guerra, y la subsecuente codificación bélica cultural, ha desgarrado a Colombia a lo largo de la historia del conflicto armado.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La securitización del país - creada primero como un discurso salvador y después como una práctica cotidiana normalizada -, ha abierto el camino para varios imaginarios sociales que buscan legitimar la militarización - entendida como el imaginario del soldado/héroe como pilar de la comunidad y motor de la seguridad nacional -, de la híper-virilidad como forma hegemónica y performativa de la masculinidad, y de la violencia como método plausible para defender a la nación de las amenazas a la ley y el orden.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así, poco a poco, la normalización de la militarización en la vida, cuerpos y comunidades ha ocupado su espacio en la economía y en la sociedad, configurando una superposición de las Fuerzas Armadas y del sector seguridad en la sociedad colombiana, al punto de que bajo estos imaginarios se justifican, minimizan y encubren acciones violentas y prácticas contrarias a la misión constitucional de protección de las FFMM.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### En las últimas semanas, Colombia ha revivido lo que durante muchos años ha sucedido en silencio: el caso de la violación a una niña indígena de la comunidad Emberá Katio por parte de un grupo de militares en el departamento de Risaralda.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Días después se dio a conocer que miembros del Ejército Nacional violaron a otra menor de 15 años, perteneciente a la etnia Nukak en Guaviare.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estos son apenas dos casos que vieron la luz como preludio de una base sistemática y estructural de violencias contra las mujeres por parte de efectivos de las Fuerzas militares. Esta violencia sistemática se corrobora con las declaraciones del general Eduardo Zapateiro, quien reconoció que desde el 2016 se han identificado 118 casos de violaciones y actos abusivos contra menores de edad por parte de miembros activos de la fuerza pública, algunos de ellos todavía vinculados al servicio. Además, la institución militar encubre los casos a como dé lugar para mantener el estatus de heroicidad construido. Esto se evidencia en la decisión de destituir a Juan Carlos Díaz Díaz, el Sargento que denunció a sus subalternos por la violación de la niña emberá katio, citando como razón la imagen de la institución.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Por lo que representan las Fuerzas del Estado y sus miembros en el imaginario colectivo, muchas de estas conductas violentas son ejercidas bajo estrictos códigos de silencio que no solo afecta a las mujeres, también sume a las familias y comunidades en miedo e inseguridad.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La performatividad de la masculinidad híper-viril, violenta y militarizada está vestida de soldado y porta armas, poniendo en entredicho las garantías, la protección y la dignidad misma de las víctimas, sus familias y sus comunidades. Adicionalmente, la militarización del país reproduce las condiciones precarias que habilitan la presencia de las Fuerzas Armadas en zonas que, sistemáticamente, han sido marginalizadas del acceso a la justicia y del goce pleno de derechos, especialmente para las mujeres.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Ejército Nacional se ha instalado en todo el país como un mecanismo gubernamental de protección y seguridad, basado en la idea de garantizar “la presencia del Estado” solo con soldados (no con personal médico, educativo o de infraestructura). En el discurso del gobierno y los altos mandos de las FFMM, los “héroes de la patria” se han instalado en nuestros territorios como “garantía” de mantenimiento de la ley y el orden, pero en la praxis esta instalación y exaltación del imaginario del soldado/héroe ha facilitado, además, que mujeres (generalmente empobrecidas y racializadas) queden a merced de estos cuerpos militares legales e ilegales, que con su accionar reproducen la precariedad en la que viven.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicional a la normalización del imaginario militarista, la violencia simbólica del machismo, instaurada en la sociedad colombiana, se abre paso para corporizarse en manos de la institución más celebrada por su “esfuerzo para mantener la seguridad y la paz”. Los 118 casos de violencia sexual cubren la superficie visible de una estructura que ha permitido por años la reproducción de códigos machistas y violentos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### No es suficiente que salgan a la luz estas violencias, también es absolutamente necesario que se rindan cuentas de los crímenes y que se reconfigure la estructura misma que produjo soldados con ínfulas de autoridad y poder sobre los cuerpos de las mujeres.

<!-- /wp:heading -->

<!-- wp:paragraph -->

No es aislado que los abusos de las Fuerzas Armadas sean sistemáticos y que afecten desproporcionalmente a las mujeres en condiciones de profundas desigualdades; es más bien sintomático de un sistema que habilita y legitima la violencia en todas sus formas. Tampoco es aceptable que el gobierno nacional enmarque estos casos en la analogía de la “manzana podrida”, como si fueran unos cuantos soldados quienes decidieron violentar a las víctimas, desconociendo el rol de las Fuerzas Militares en la configuración de una masculinidad violenta y militarizada, que se socializa entre los hombres que reciben entrenamiento militar y a quienes debería garantizárseles el derecho a la objeción de conciencia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Sin embargo, en la práctica, quienes desobedecen órdenes criminales o denuncian violaciones, son sancionados o retirados de la institución, como ocurrió con el Sargento Viceprimero Juan Carlos Díaz que reportó a los 7 soldados que violaron a la menor de 13 años.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Casos como los de la niña emberá katio y otros, caen en cruel impunidad e invisibilidad, investigaciones insuficientes, dejando de lado que en casos como este se configuran también otros delitos en el marco del DIH. De acuerdo con una de las abogadas de la Mesa Nacional de seguimiento a la ley 1257, en el caso de la niña emberá se cometieron otros delitos como la tortura, acceso carnal violento con incapacidad de resistir, agravado en concurso heterogéneo con secuestro. Adicionalmente, según la apreciación de la abogada, el caso no se puede analizar como un hecho aislado del contexto en el que ocurrieron las circunstancias de tiempo modo y lugar -esta es una zona de conflicto armado con presencia de distintos grupos armados legales e ilegales, en disputa por los grupos armados-.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, estos delitos no solo se violentan a las mujeres y niñas, también a las comunidades y territorios. Por este motivo, deberían ser judicializados bajo la justicia propia de los pueblos y reconociendo la violación de los derechos territoriales étnicos a través de la violencia sexual. Mientras esto sucede, las mujeres víctimas de estas violencias y las organizaciones de mujeres seguiremos gritando por su dignidad, hasta que las fuerzas nos resistan.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6,"textColor":"vivid-cyan-blue"} -->

###### [Vea mas: nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/) {#vea-mas-nuestros-columnistas .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->
