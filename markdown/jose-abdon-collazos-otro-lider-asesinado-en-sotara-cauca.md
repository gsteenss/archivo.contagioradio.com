Title: José Abdon Collazos otro líder asesinado en Sotará Cauca
Date: 2016-12-05 13:55
Category: DDHH, Nacional
Tags: Asesinatos de indígenas en Colombia, asesinatos de líderes sociales, Implementación de Acuerdos
Slug: jose-abdon-collazos-otro-lider-asesinado-en-sotara-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/marchaP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cartel Urbano] 

###### [5 Dic 2016] 

En el marco de los acuerdos de paz, continúan los asesinatos y ataques contra líderes y lideresas en distintos territorios del país. El pasado 3 de Diciembre a la 1:00pm hombres armados abordaron a **José Abdon Collazos y le propinaron 5 impactos con proyectil de arma de fuego.**

Con este integrante de la Mesa de Víctimas de Sotará Cauca, quien también hacía parte de la Marcha Patriótica son **más de 127 los líderes y lideresas asesinadas en los últimos meses, y aún las autoridades no esclarecen estos hechos. **

Integrantes de la Red de Derechos Humanos de la Marcha Patriótica aseguraron que los hechos ocurrieron cuando José Abdon **se dirigía a su casa en la vereda San Isidro del municipio de Sotará. **Le puede interesar: [Líderes indígenas, blanco de asesinatos y amenazas.](https://archivo.contagioradio.com/lider-indigena-del-cauca-amenzado-por-los-urabenos/)

El 5 de Diciembre se llevó a cabo el sepelio de este líder comunitario, familiares y amigos cercanos denuncian que hasta el momento no han recibido respuesta alguna por parte de las autoridades, e incluso afirman que **ni el Gobierno departamental ni nacional han iniciado las labores investigativas correspondientes.**

###### Reciba toda la información de Contagio Radio en [[su correo]
