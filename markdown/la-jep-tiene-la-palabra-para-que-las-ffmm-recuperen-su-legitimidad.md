Title: La JEP tiene la palabra para que las FFMM recuperen su legitimidad
Date: 2020-06-09 07:00
Author: CtgAdm
Category: Actualidad, Otra Mirada
Tags: Ejército Nacional, víctimas
Slug: la-jep-tiene-la-palabra-para-que-las-ffmm-recuperen-su-legitimidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/coronel-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En medio de las acciones violentas que se han visto en los territorios producto de la intervención de las [Fuerzas Militares](https://archivo.contagioradio.com/persecucion-y-desplazamiento-campesino-el-resultado-de-incumplimiento-del-gobierno-en-guaviare/)en acciones de erradicación forzada; **cobran fuerza los procesos de 2.680 miembros de la fuerza pública que se han sometido a la Juristicción Especial para la Paz**. Uno de los más sonados es el **[Caso 003](https://www.jep.gov.co/Infografas/cifras-mayo-29.pdf), el cual recopila 221 versiones sobre ejecuciones extrajudiciales.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hasta el pasado **29 de mayo la JEP confirmó que ha recibido 1.840 solicitudes de miembros de la fuerza pública que desean someterse a la Jurisdicción** y de alguna manera aportar su [verdad](https://comisiondelaverdad.co/actualidad/noticias/es-necesario-que-la-sociedad-reconozca-que-los-militares-tambien-fuimos-victimas) y esclarecer sobre las participaciones de las FFMM durante los años mas lamentables y doloroso de la violencia en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Intenciones que no solo se han visibilizado en la JEP, sino también en la Comisión de la verdad, **en donde han recibido diferentes [informes](https://archivo.contagioradio.com/informe-relata-el-rol-de-la-inteligencia-militar-en-los-crimenes-de-estado/) de los hechos violentos, en donde los uniformados han sido responsables;** la gran mayoría de estos entregados por organizaciones de Derechos Humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma el Ejército presentó el pasado 7 de noviembre un[informe](https://comisiondelaverdad.co/actualidad/noticias/hemos-creado-una-confianza-mutua-con-las-fuerzas-armadas-carlos-ospina)titulado, *"Luces para la Verdad: violaciones a los derechos humanos e infracciones al derecho internacional humanitario contra militares y sus familias*"; **documento cuestionado por diferentes organizaciones al señalar que este desconocía otras víctimas y el verdadero papel de la institución** en el conflicto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El mayor (r) **César Maldonado, presidente de la Fundación Comité de Reconciliación** ONG, organización formada por 1.305 integrantes de las FFMM comparecientes ante la JEP, señaló que el primer paso para recuperar la **legitimidad de las FFMM es el reconocimiento y luego la verdad.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Si bien la institución del Ejército no va reconocer las ejecuciones extrajudiciales como una política instaurada al interior de la institución, si debe reconocer y ponerle el pecho a la responsabilidad de esto"*, acción que se logra según el Mayo (r), al dejar de negar la existencia de este tipo de acciones y reconocer la verdad, así como la responsabilidad de los militares en esta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó, que él ha visto una disposición de parte de Ejército para *"poner la cara de lo que ocurre en el conflicto, de saber que llegó el momento de reconocer a las víctimas y lo que pasó"*. (Le puede interesar leer: [Militares respaldan a la JEP y mantienen su compromiso con la verdad](https://archivo.contagioradio.com/militares-respaldan-jep/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Maldonado también destacó que ante la situación actual el Ejército debe estructurar una manera de ponerle frente al país, *"**el Ejército no puede seguir escondiendo la mano como institución y debe reconocer que lo que hicieron sus hombres no lo hicieron como individuos sino como colectivo** integrante de la Fuerza Pública en el marco de operativos".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La JEP nos dio a las FFMM la posibilidad de decir la verdad sin miedo"

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Mayor (r) Maldonado señaló que él en cabeza de la Fundación defiende a la Jurisdicción,"***la JEP salió como un beneficio antes las condena impagables que nos habían dado la jurisdicción ordinaria**, además nos da la posibilidad de poder solucionar, reconocer y aclarar la verdad vivida en el conflicto sin miedo"*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**La JEP no solamente es una solución para quienes somos comparecientes,** sino para que la el país conozca la verdad y avanzar hacia un proceso de paz completa".*
>
> <cite>*César Maldonado | Mayor retirado del Ejército*</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Asimismo afirmó que para el los reconocimientos en distintos informes de la responsabilidad de las Fuerzas Miliares es doloroso, *"pero que ayuda para reconocer errores y poder entender como institución en que está fallando, y así poder reinventarse y lograr ser más eficientes, transparentes y respetables por la sociedad*".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y concluyó diciendo que a pesar de los esfuerzos por un cambio, esté se ha quedado corto, y más aún cuando si se siguen asumiendo los enemigos como personales, algo *"que se debe dejar de lado, que se debe pensar diferentes y aún más en el marco de un proceso de paz".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Afirmación presentada por Maldonado ante las amenazas que han recibido como Fundación e incluso como individuos, a pesar de ello reconoció que *"se debe de dejar persistir en el mismo error, porque cada día se traba por la búsqueda de la verdad y porque cada vez más militares se acojan a entregar la suya verdades fortaleciendo así el camino al cambio".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**Si cometimos errores pero hoy que queremos demostrarle al país que los vamos asumir**, y aún mas importante que queremos cambiar esos errores y trascender* "

<!-- /wp:quote -->

<!-- wp:paragraph {"fontSize":"small"} -->

*Escuche la entrevista completa en el diálogo con el Mayor (r) Maldonado y el Coronel (r) Velásquez.*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F287060888993982%2F&amp;show_text=true&amp;width=734&amp;height=523&amp;appId" style="border:none;overflow:hidden" scrolling="no" allowtransparency="true" allow="encrypted-media" allowfullscreen="true" width="734" height="523" frameborder="0"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
