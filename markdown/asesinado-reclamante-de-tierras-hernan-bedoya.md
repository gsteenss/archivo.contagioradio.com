Title: Asesinado líder reclamante de tierras Hernán Bedoya en Chocó
Date: 2017-12-08 15:14
Category: DDHH, Nacional
Tags: Conpaz, Curvarado, Empresarios palmeros, paramilitares, Pedeguita y Mancilla
Slug: asesinado-reclamante-de-tierras-hernan-bedoya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/hernan-bedoya-curvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [08 Dic 2017] 

**Hernán Bedoya**, reconocido líder reclamante de tierras el Chocó y del territorio colectivo de Pedeguita y Mancilla, quien había denunciado asociaciones ilegales, contratos fraudulentos y presencia de paramilitares en la región fue asesinado. El hecho se presentó hacia la 1:15 Pm en el caserío de Playa Roja, en el punto conocido como El Acopio.

Según la denuncia Hernán se dirigía a su vivienda en Pedeguita y Mancilla, cuando neoparamilitares de las autodenominadas Autodefensas Gaitanistas de Colombia AGC le dispararon 14 veces, con arma de fuego 9 milímetros, provocando inmediatamente su muerte. se trata del segundo asesinato de líderes de Curvaradó y Bajo Atrato en menos de 10 días. [Lea también: Asesinaron a Mario Castaño, lider de Curvarado](https://archivo.contagioradio.com/asesinan-a-mario-castano-lider-reclamante-de-tierras-en-el-choco/)

Según la Comisión de Justicia y Paz, **este asesinato beneficia a personas que ocuparon las tierras de mala fe, desde 1996 con las operaciones militares y paramilitares** como septiembre negro, en las que habrían participado integrantes de la Brigada XVII del Ejército y paramilitares de las ACCU.

### **La lucha de Hernán Bedoya** 

Este líder social se había opuesto a la implementación de los proyectos agroindustriales financiados con recursos de la Agencia Nacional de Tierras (ANT) y ejecutados por La Sociedad Anónima denominada Asociación Agropecuaria Campesina **AGROMAR S.A, administrados por el cuestionado representante legal Baldoyno Mosquera Palacios.**

Su finca hacía parte de los predios donde se busca imponer proyectos agroindustriales en el territorio colectivo de Pedeguita y Mancilla "de manera inconsulta y desconociendo la medida cautelar que protege el territorio colectivo, proferida por el Tribunal de Antioquia Sala Civil Especializada en Restitución de Tierras, elevaron convenio para la explotación agroindustrial del 41% del territorio colectivo (20.000 Hectáreas), con una temporalidad de 100 años", explica la organización de DDHH.

Al ser uno de los líderes de la zona y al enfrentarse directamente con los intereses de empresarios, según la Comisión de Justicia y Paz, **desde 2015 había sido blanco de amenazas por parte de las AGC, las mismas que se agudizaron este año;** sin embargo, y pese a las reiteradas denuncias, solo le fueron otorgadas como medidas de protección por la **Unidad Nacional de Protección un chaleco antibalas y un teléfono celular.**

Con este asesinato son tres lo integrantes de la red CONPAZ que han sido asesinados durante 2017, Emilsen Manyoma, Mario Castaño y Hernan Bedoya. Además se suman las amenazas contra Ligia María Chaverrra, lideresa y matriarca y una de las promotoras del retorno y la exigencia de restituciones de tierras en el Bajo Atrato. L[ea también: Denuncian amenazas contra María Chaverra, lider del Bajo Atrato](https://archivo.contagioradio.com/denuncian-amenazas-contra-ligia-maria-chaverra-lideresa-del-choco/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

######  
