Title: Enrique Peñalosa desconoce Reservas en los Cerros Orientales
Date: 2017-02-09 15:48
Category: Ambiente, Nacional
Tags: Cerros Orientales de Bogotá, Enrique Peñalosa, reserva Van der Hammen
Slug: enrique-penalosa-sigue-desconociendo-reservas-forestales-en-los-cerros-orientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/cerros-orientales-e1486663422599.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Rodolfo Franco] 

###### [9 Feb 2017 ] 

Durante la instalación de la exposición [“Oriéntate: los Cerros Orientales son nuestro norte”, que tiene como objetivo resaltar el valor histórico, cultural y ambiental de esta importante, el alcalde Enrique Peñalosa, irrumpió en el espacio para hablar de los proyectos urbanísticos que hacen parte de sus políticas de gobierno y pretenden ser construidos en la reserva. Frente a ello l]a Mesa de los Cerros Orientales, manifestó su descontento y calificó la situación como una “manipulación de la Alcaldía Peñalosa”.

Las organizaciones señalaron que el Instituto Distrital de Patrimonio, quien organizó el evento, había dicho que el espacio no estaría dispuesto para intereses políticos y sin embargo, Peñalosa se tomó la palabra para exponer el **“corredor ecológico” entre el Sendero de las Mariposas y el Parque El Rocío, la ALO y la línea de alta tensión Nueva Esperanza.**

Las organizaciones ambientalistas, agrupadas en la Mesa, afirmaron que distintos estudios demuestran que “los senderos contrafuegos no funcionan”, pues se trata de un camino de cemento en medio de una zona de páramo y subpáramo que **“podría deteriorar gravemente el suelo, a lo que se suma que alrededor podrían consolidarse asentamientos”. **([Le puede interesar: Continúan construcciones en zonas de conservación de cerros orientales](https://archivo.contagioradio.com/cerro-de-los-alpes-golpe-al-ecosistema-de-cerros-orientales/))

También llamaron la atención sobre el desacato por parte de la alcaldía de Enrique Peñalosa, al Acuerdo 030 de 1976 el cual dispone que los Cerros orientales son una Zona de Reserva Protectora y el fallo constitucional sobre los Cerros Orientales, expedido en 2012, el cual dictamina una zona de exclusión y la legalización de algunos asentamientos en los Cerros, debido a ello **“los derechos de los habitantes de los sectores populares perviven en limbo jurídico y el territorio se sigue deteriorando”.**

### Alcaldía Peñalosa desconoce la norma 

Ambientalistas advirtieron que Enrique Peñalosa, desconoce con ello el **Plan de Manejo de la Franja de Ocupación Pública-Prioritaria, elaborado por la Bogotá Humana,** el cual propone la adecuación de 19 parques ecológicos, 80 Km. de senderos, 5 malokas indígenas, 100 huertas comunitarias, un gran parque del agua y emprendimientos comunitarios para 1000 habitantes de Cerros Orientales.

Selene Lozano, integrante de la Mesa, manifestó que la administración Peñalosa “intenta revivir licencias en los Cerros de las grandes constructoras”, para distintos proyectos que afectarían la Reserva Thomas Van Der Hammen y “omite medidas preventivas” frente proyectos como la vía Perimetral del Oriente y la línea de alta tensión Nueva Esperanza, lo que **“empeoraría gravemente la conectividad de los Cerros Orientales”.**

### Problemática en los Cerros no es sólo de ahora 

Por otra parte, el abogado y ambientalista Rodrigo Negrete reveló que está problemática no es sólo de ahora, pues “desde el año 1974 esta prohibido construir” y sólo hasta ahora **“acatan la norma y proceden a demoler muchas mansiones ilegales construidas en los cerros”. **([Le puede interesar: Construcción de cinco viviendas de lujo tienen en riesgo cerros orientales de Bogotá](https://archivo.contagioradio.com/construccion-de-cinco-viviendas-de-lujo-tienen-en-riesgo-cerros-orientales-de-bogota/))

El abogado señaló que la única actividad permitida en los Cerros es la “obtención de frutos secundarios del bosque, cosas que no modifiquen los suelos ni su uso”. Además resaltó que "sorprende el silencio del Ministerio de Ambiente y la CAR" frente a las irregularidades en el área de 973 hectáreas de exclusión, de las cuales 500 ha. Corresponden a los barrios asentados en los cerros y las otras 473 ha. aún no han sido intervenidas, pero están siendo licitadas para obras de infraestructura.

También se refirió al hecho de que en 2005, Sandra Suarez, Ministra de Ambiente asesorada por Edmundo del Castillo, permitiera la figura de derechos adquiridos de particulares, constructoras y **personas que tenían proyectos urbanísticos en la Franja de Adecuación de los cerros.**

Negrete aseguró que lo que pretende la actual alcaldía es “desdibujar el área de Reserva Forestal Protectora”, añadió que la ciudadanía y organizaciones deben estar atentas y sacar provecho de hechos como la pérdida de vigencia de la licencia otorgada en 1999 para la construcción de la Avenida Longitudinal de Oriente, que pretende **“partir el Humedal la conejera en 2 e interviene otros Humedales”.**

Por último, la Mesa de Cerros Orientales invitó durante la exposición, a toda la ciudadanía, organizaciones comunitarias, ambientalistas y estudiantes a visitar la exposición “Oriéntate: los Cerros Orientales son nuestro norte”, a observarla “**críticamente, a indagar sobre las propuestas y a posicionarse activamente** frente a la ciudad en la que queremos vivir”.

###### Reciba toda la información de Contagio Radio en [[su correo]
