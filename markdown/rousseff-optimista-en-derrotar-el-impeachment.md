Title: Rousseff optimista en derrotar el impeachment
Date: 2016-06-09 16:24
Category: El mundo, Política
Tags: Al jazeera Dilma Rousseff, Dilma Roussef impeachment, Golpe de estado Brasil
Slug: rousseff-optimista-en-derrotar-el-impeachment
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Dilma-aljazeera.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Al Jazeera 

##### [09 Jun 2016]

La presidenta brasileña Dilma Rousseff, se mostró optimista ante la posibilidad de tumbar el proceso de juicio político que se adelanta en su contra por el que actualmente se encuentra apartada de su cargo.

En entrevista concedida a la cadena Al Jazeera, Rousseff considera posible cambiar la correlación de los votos en la comisión senatorial, al contar en este momento con 22 de los 28 que necesitaría para evitar su destitución, afirmando que "no es tan imposible conseguir 6 votos".

La mandataria expresó su percepción sobre una transformación en el sentimiento de la población desde que fue confirmada su separación del cargo para adelantar su defensa, quienes en su criterio, se muestran en contra del impeachment y lo han manifestado saliendo a las calles en varias oportunidades.

Una vez más, la Presidenta reiteró que desde su victoria en las elecciones de 2014, existe la intención de sacarla del cargo, impulsada por sus oponentes políticos, en parte por negarse a detener las investigaciones en el escándalo por corrupción conocido como 'Lava Jato' del cual ha existen múltiples pruebas que les incriminan.

En el diálogo, Rousseff recordó los logros y las conquistas sociales alcanzadas durante su primer mandato y el de su antesesor Luis Inácio Lula y aseguró que únicamente el pueblo que la eligió con mas de 54 millones de votos, tiene la potestad de retirarla de su cargo.

**Sobre el proceso**

La Comisión que adelanta el juicio político, sesionó durante este miércoles con las comparecencias de los primeros testigos acusadores, quienes realizaron sus descargos en un debate que duró 15 horas culminando la madrugada del jueves, hecho que provocó la decisión de postergar la sesión del día para el próximo lunes donde comparecerán los defensores de la mandataria.
