Title: Procurador quiere censurar besos y abrazos en los colegios
Date: 2015-08-21 15:44
Category: DDHH, Nacional
Tags: Colegios de Colombia, Comunidad LGBTI, Manuales de Convivencia, Procurador Alejandro Ordoñez, procuraduria, Sergio Urrego
Slug: prohibir-manifestaciones-afectivas-entre-estudiantes-nueva-hoguera-medieval-del-procurador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/procurador_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elpais 

<iframe src="http://www.ivoox.com/player_ek_7295328_2_1.html?data=mJeml5iWfI6ZmKiakpyJd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPjyc7Py9ePscLiysvS1dnFp8rjz8rgjcbKqcToytvO1ZDJstXmxpDS1dnZqMrVz9nS1ZCJiZOZmZWah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [German Humberto Rincón, Abogado] 

###### [21 Ago 2015]

El procurador general de la nación Alejandro Ordoñez envío una **propuesta a la Corte Constitucional para que en los Manuales de Convivencia se prohíban manifestaciones afectivas entre estudiantes**, y al mismo tiempo anunció que cerrará las investigaciones que cursan en contra de los concejales implicados en el carrusel de la contratación en Bogotá.

El abogado German Humberto Rincón, afirma que esta “*nueva hoguera medieval de fundamentalismo religioso antidemocrático del Procurador*”, se da en el marco de la **acción de tutela que presentó la madre de Sergio Urrego, relacionada con las disposiciones de los Manuales de Convivencia** y que en este momento se encuentra en debate procesal en la Corte, con el 100% de aprobación de la Defensoría del Pueblo y el total rechazo por parte de Ordoñez.

La propuesta enviada por el Procurador, según Rincón, representa una **violación a la persona como eje fundamental de la Constitución colombiana**, un desconocimiento a los derechos sexuales de los seres humanos y un abuso de poder por parte de Ordoñez, pues **el funcionario no es quien para controlar los cuerpos de los jóvenes y mucho menos de los 45 millones de colombianos que no podemos vivir como él considera correcto.**

Rincón, asegura que el Procurador usa de su poder disciplinario contra sus contradictores políticos para acusarlos y en contraposición exonerar a los congresistas que votaron a su favor en acciones disciplinarias, tal como es evidente con su decisión de suspender las investigaciones de los concejales acusados de participar en el carrusel de contrataciones.
