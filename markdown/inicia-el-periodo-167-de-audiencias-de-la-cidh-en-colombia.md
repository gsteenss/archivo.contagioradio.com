Title: CIDH sesionará en Colombia hasta el 2 de marzo
Date: 2018-02-21 16:39
Category: DDHH, Nacional
Tags: CIDH, colombia, Defensa de los derechos humanos
Slug: inicia-el-periodo-167-de-audiencias-de-la-cidh-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Afadevig-en-la-CIDH-Colombia.mp4_snapshot_01.30.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Waynakuna.org] 

###### [21 Feb 2018] 

Inicia en Colombia el 167 periodo de sesiones de la Comisión Interamericana de Derechos Humanos, **serán en total 30 audiencias desde el 22 de febrero hasta el próximo 2 de marzo**. Así mismo este espacio contará con un escenario académico en donde se espera la participación de defensores de derechos humanos, estudiantes y activistas.

Este jueves 22 de febrero, se llevará a cabo la ceremonia inaugural, que contará con la participación del presidente Juan Manuel Santos e inmediatamente después se realizará un encuentro con los jueces que hacen parte de la JEP. De acuerdo con Paulo Abrāo, Secretario Ejecutivo de la CIDH, **la intensión es acompañar y apoyar los acuerdos de paz entre la FARC y el gobierno nacional y su implementación**.

### **Las actividades que se realizarán** 

Los escenarios y audiencias públicos se realizarán de lunes a viernes en las mañanas. Entre los temas que se sesionaran se encuentran la audiencia sobre los 25 años de la Comisión de la Verdad de El Salvador, la audiencia sobre el cumplimiento de recomendaciones a República Dominicana, otra de las audiencias más importantes será la revisión de **denuncias hechas en Honduras por violaciones a los derechos humanos en el marco de las últimas elecciones**.

En el caso de Estados Unidos la CIDH sesionará en dos temas: **el control y comercio de armas y la situación de los derechos humanos de los Dreamers**. Venezuela también contara con diferentes audiencias sobre seguimiento a denuncias en torno a la crisis política, económica y social que vive el país.

Brasil contará con tres audiencias de seguimiento: una sobre la situación carcelaria del país, los derechos de la población reclusa y otra sobre la actuación de la Fuerza Pública en torno a ejecuciones extrajudiciales. Para Argentina se realizará una audiencia sobre el derecho a la protesta y el accionar de la Policía y para Perú se hará la audiencia sobre los motivos **por los cuales se otorgó el indulto al ex presidente Alberto Fujimori. **(Le puede interesar: ["Indulto a Fujimori es ilegal según organizaciones defensoras de DD.HH"](https://archivo.contagioradio.com/indulto_alberto_fujimori_peru_impunidad/))

La CIDH también manifestó que ha hecho un seguimiento a la desaparición de los 43 normalistas de Ayotzinapa, en México y en el marco de este hecho harán una audiencia sobre la búsqueda de desaparecidos, la relación con los responsables y la reparación a las víctimas.

Sin embargo, en esta ocasión la CIDH no tendrá audiencias sobre las problemáticas en Colombia, debido a que no toma decisiones sobre temas del país en el que se estén realizando las sesiones, no obstante Paulo Abrāo señaló que “e**sta es una oportunidad única para los ciudadanos colombianos que trabajen en la defensa de los derechos** humanos, en el acompañamiento de las actividades que se desarrollen”.

**El arduo camino de la defensa de los derechos humanos**

De igual forma este año se conmemoran los 70 años de la declaración Americana de Derechos y Deberes del hombre, aprobada en la novena conferencia internacional americana, realizada en Bogotá en 1948. Uno de los pilares fundamentales que da paso a la creación de otras instancias internacionales que hacen veeduría y seguimiento al cumplimiento y defensa de los derechos humanos.

Este periodo de sesiones culminará con una rueda de prensa el jueves 1 de marzo en donde se expondrán los resultados principales y para Paulo Abrāo, se espera que una vez finalicen se logre avanzar en la protección y defensa de los derechos humanos. (Le puede interesar: ["Los retos de la CIDH para el 2018, entrevista con el presidente José Eguiguren"](https://archivo.contagioradio.com/los-retos-de-la-cidh-para-este-2018-entrevista-con-el-presidente-jose-eguiguren-praeli/))

[Calendario 167 Audiencias Es](https://www.scribd.com/document/372569951/Calendario-167-Audiencias-Es#from_embed "View Calendario 167 Audiencias Es on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_68390" class="scribd_iframe_embed" title="Calendario 167 Audiencias Es" src="https://www.scribd.com/embeds/372569951/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-je5jWvAx2rOim5mt1fzA&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="1.2941176470588236"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
