Title: Industria de alimentos ultraprocesados "interfiere" en políticas de salud pública en Colombia
Date: 2019-12-18 17:31
Author: CtgAdm
Category: Nacional
Tags: alimentación, campesinos, Min. Salud, sobrepeso, ultraprocesados
Slug: industria-de-alimentos-ultraprocesados-interfiere-en-politicas-de-salud-publica-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Min.-salud.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/1366_2000.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@FIANColombia] 

Este 17 de noviembre se convocó un plantón en el Ministerio de Salud  liderado por Food First Information and Action Network Colombia (FIAN Colombia), en respuesta a la reunión que se desarrollaba entre el Gobierno y empresarios de la industria de alimentos ultraprocesados y que tenía como centro el Pacto por el Crecimiento- Alimentos Procesados. La protesta se da con la finalidad de buscar justicia alimentaria y que el Gobierno escuche también a campesinos encargados de la alimentación a base de productos naturales.

Según Andrea Rodríguez, Coordinadora de FIAN Colombia, esta movilización pacífica se da como rechazo al Pacto que ha firmado el Gobierno  con la industria que produce comida chatarra, *“Consideremos que es una clara interferencia de la industria en políticas de salud pública, algo que no solo se debe rechazar como sociedad civil sino también como organizaciones que trabajamos por el derecho a la salud". *

Y agregó,*"No es posible que la industria que está generando afectaciones a la salud de los colombianos especialmente a niños y niñas vendiendo productos nocivos para la salud, esté sentada en este momento con el Gobierno en una mesa de negociación donde están generando alianzas y acciones para promover el consumo de estos productos con publicidad y mercadeo masivo*". (Le puede interesar:[La alimentación en las cárceles otra forma de torturar a la población reclusa](https://archivo.contagioradio.com/alimentacion-carceles-torturar-poblacion-reclusa/))

### **¿De qué trata el Pacto de los alimentos ultraprocesados ?**

Este pacto hace parte de un paquete de 12 acuerdos que el Gobierno ha firmado con esta industria; y que señala varios "cuellos de botella", un de ellos según Rodríguez, es que  la sociedad exija medidas de salud pública como el etiquetado frontal de advertencia para los productos comestibles ultraprocesados, "Ven como cuello de botella que se exija al Congreso la regulación de la venta de estos productos, que nos son nada saludables en su mayoría y aportan al aumento en el índice de obesidad en el país"

Asimismo señaló este acuerdo tiene insuficientes puntos para la lucha contra la obesidad y generar hábitos de vida saludable a través de estrategias que según FIANColombia, "La autorregulación de la industria y las medidas que han desarrollado para aumentar el deporte, la buena alimentación y demás, acciones que no son suficientes, porque es más fuerte la publicidad del consumo que de la conciencia". (Le puede interesar:[El regreso del glifosato, una amenaza para campesinos en zonas cocaleras](https://archivo.contagioradio.com/el-regreso-del-glifosato-una-amenaza-para-campesinos-en-zonas-cocaleras/))

El sector de alimentos procesados agrupa más de 16.347 empresas en los sectores de lácteos, carnes, confitería, aceites y productos de molinería, entre otros, los nombres más destacados dentro de estos son; Sector Agro industrial de la Caña, Asograsas, Consejo Nacional Gremial, entre otros como la Agencia de Desarrollo Rural (ADR), y la Unidad de Planificación Rural Agropecuaria (UPRA); que a pesar de estar allí no han logrado que se de una mesa de conversación entre campesinos y campesinas y el Gobierno,"**Consideramos que el Gobierno se debe sentar a hablar de estos temas con la gente que en este país está produciendo los alimentos reales y naturales, personas a las que se les niega el dialogo" afirmó Rodríguez. **

 

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45775500" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45775500_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
