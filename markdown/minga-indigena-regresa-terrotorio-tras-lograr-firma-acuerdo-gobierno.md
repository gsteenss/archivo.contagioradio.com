Title: Minga indígena regresa a su territorio tras lograr firma de  acuerdo con gobierno
Date: 2018-11-16 13:20
Author: AdminContagio
Category: Movilización, Nacional
Tags: Minga por la vida, ONIC
Slug: minga-indigena-regresa-terrotorio-tras-lograr-firma-acuerdo-gobierno
Status: published

###### Foto: ONIC 

###### 16 Nov 2018 

Tras casi una semana de estancia en la capital del país **sin contar con alimentación o estadía dignas**, más de 452 integrantes de los pueblos indígenas **Embera Katio - Eyabida, Dóbida, Wounaan y Zenú de los Municipios de Riosucio y Carmen del Darién del Chocó** regresarán a sus territorios  tras firmar un acuerdo entre el gobierno indígena y el gobierno nacional que esperan se cumpla según lo pactado.

Según **Óscar Montero, asesor de la Consejería de los Derechos de los Pueblos Indígenas** dentro de lo acordado en temas de seguridad se pactó específicamente prestar atención a la prevención de **minas anti persona** y de igual forma generar diálogos con el ELN para mitigar la siembra de dichos artefactos en el territorio. También se plantearon propuestas de protección a través de la Unidad Nacional de Protección para 13 líderes indígenas que se encuentran amenazados para declararlos como sujetos de reparación.

Respecto a los temas de gobierno y territorio, entidades como el **Ministerio de Cultura, la Agencia Nacional de Tierras y Parques Nacionales** realizarán una serie de ejercicios para delimitar el territorio y constituir cuatro resguardos indígenas que “tienen más de 14 años de lucha por ser reconocidos” resaltó Montero.

En cuestiones sociales se concertó que el 17 de diciembre una **Comisión Intersectorial** visitará a la comunidad de Río Sucio, Chocó para realizar brigadas de salud, verificaciones humanitarias en conjunto con las Naciones Unidas y revisar los temas de alimentación escolar junto al ICBF.

A pesar de la firma de un acuerdo, la ONIC rechazó las muestras de discriminación contra los más de 462 indígenas, entre ellos 10 mujeres embarazadas y 15 niños, que llegaron desde el Chocó y fueron retenidos de manera ilegal a la entrada de Bogotá, **obligándoles a permanecer a la intemperie sin ninguna razón de peso** en la noche del 10 de Noviembre.

El Gobierno se comprometió a regresar a las comunidades a su región con garantías de inmunidad y seguridad, sin embargo ya hubo incumplimientos pues los buses que llevarían de regreso a los mingueros llegaron a medianoche del día jueves y no a las 9:00 pm como se había acordado, “lo que generó intranquilidad e incredulidad en la institucionalidad para cumplir con los acuerdos a los que se llegaron” denunció el delegado quien también agregó, **“esperamos que las comunidades lleguen a buen término a sus territorios, que no haya re victimización ni represalias de los grupos armados”.**
