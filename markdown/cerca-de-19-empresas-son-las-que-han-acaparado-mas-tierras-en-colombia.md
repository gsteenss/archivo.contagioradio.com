Title: Cerca de 19 empresas son las que han acaparado más tierras en Colombia
Date: 2016-02-03 08:19
Category: Economía, Nacional
Tags: Ley Urrutia, Ministerio de Ambiente, Plan Nacional de Desarrollo, Zidres
Slug: cerca-de-19-empresas-son-las-que-han-acaparado-mas-tierras-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Monocultivos-e1454517273281.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio 

<iframe src="http://www.ivoox.com/player_ek_10302311_2_1.html?data=kpWgkpeXdZKhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaTZ08jOjcnJb5KtjMra0tfJt8LnjNjc0JDQpdSf0trSjc3FsozVxMbdw9fFqNCfzoqwlYqlddSf1c7Sj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Wilson Arias] 

###### [02 Feb 2016]

Para el ex representante a la Cámara, Wilson Arias Castillo, la Ley de **[ZIDRES](https://archivo.contagioradio.com/?s=zidres)** es la peor contra reforma agraria que se ha realizado en el país y está formulada en clave **de extranjerización de la tierra por una parte y de agro exportación por la otra.** Tanto las [investigaciones de Arias](https://archivo.contagioradio.com/?s=wilson+arias), como las de senadores como Castilla, Robledo y Cepeda han dado cuenta ante la contraloría de más de 120 mil hectáreas acumuladas fraudulentamente, pero podrían ser 2 millones según lo manifestado por el propio presidente de FEDEGAN citado por Arias.

Monica Semillas, Cargil, Poligrow con empresas chinas, argentinas, norte americanas entre otras, ingenios como Mayaguez, Manuelita, Riopaila y las familias Santos, Valencia Iragorri, Sarmiento Angulo, Ocampo, Lafourie y Lizarralde hacen parte del **selecto grupo de familias que se habrían apropiado de cerca de 2’000.000 de hectáreas** en el marco de operaciones fraudulentas de acaparamiento antes de la promulgación de la ley ZIDRES.

Aunque las denuncias ante la **contraloría y el primer informe dan cuenta de 126 mil hectáreas**, habría que sumar las encontradas por el grupo de senadores del Polo Democrático y también habría que esperar un segundo informe del organismo sobre este aspecto. Para Arias, lo preocupante es que por la vía de la ley se justifiquen estas operaciones y se haga una especie de **“blanquamiento” del acaparamiento ilegal de tierras.**

Arias señala que hay dos vías para contrarrestar esta situación y evitar un símil con la situación en África en donde, según el ex congresista, la gente se muere de hambre en medio de las plantaciones de comida.

Una de las vías tiene que ver con la **movilización social de los campesinos de esas regiones y la solidaridad desde los sectores urbanos**, y otra, la vía judicial con las demandas que se han interpuesto y a la espera de que la **Corte Constitucional se manifieste frente a la ilegalidad de las ZIDRES.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
