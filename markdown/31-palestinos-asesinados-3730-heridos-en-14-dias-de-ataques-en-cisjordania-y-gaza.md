Title: 31 palestinos asesinados, 3730 heridos en 14 días de ataques en Cisjordania y Gaza
Date: 2015-10-14 14:34
Category: DDHH, El mundo
Tags: Bejamin Netanyahu, Cirsjordanía, Ejército de Israel, Gaza, Hamás, Intifada Palestina, Israel, OLP, Palestina, Represión en territorios palestinos, Territorios Ocupados
Slug: 31-palestinos-asesinados-3730-heridos-en-14-dias-de-ataques-en-cisjordania-y-gaza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Cisjordanía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Palestina Libre ] 

###### [14 Oct 2015] 

[Las incursiones militares en territorios ocupados por colonos israelíes en Cisjordania y Gaza han cobrado la vida de 38 personas, 31 de ellas palestinas y 7 israelíes, dejando heridas a otras 3730, en lo que va corrido de octubre. Frente a este panorama el Gobierno de Israel anunció el **aumento de las medidas de seguridad en los territorios palestinos**.]

[Benjamin Netanyahu primer ministro israelí, ayer en sesión extraordinaria en el Parlamento, aprobó medidas que incluyen el **cierre y destrucción de barrios palestinos** en Jerusalén, el **despliegue de 4 batallones israelíes fuertemente armados** en distintas ciudades y el **confiscamiento de propiedades** para quienes se opongan a las acciones del Ejército. Así mismo, se bloqueará la entrada de palestinos a las mezquitas.  ]

[El gobierno palestino aseguró en un comunicado que estas medidas "Constituyen una **violación de las leyes internacionales** y pueden aumentar la escalada de violencia en los territorios palestinos". Mientras que los civiles palestinos han salido a las calles para protestar contra la **discriminación y represión ejercida por el Ejército israelí**. ]

Durante todo el día miles de personas se han solidarizado con la población palestina en las redes sociales con \#PalestinaResiste, \#IntifadaPalestina y \#TodosConPalestia, para rechazar la arremetida de las FFMM y de seguridad de Israel.
