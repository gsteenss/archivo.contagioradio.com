Title: Identidad de género: Más que un "trastorno", una decisión
Date: 2015-09-25 23:38
Category: closet, LGBTI
Tags: Closet Abierto, Comunidad LGBTI, Derechos Humanos, Discriminación, Identidad de Género, Sigla T, Transexuales, Transformistas, Transgénero, Travestis
Slug: identidad-de-genero-mas-que-un-trastorno-una-decision
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/MG_9314.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

##### <iframe src="http://www.ivoox.com/player_ek_6734853_2_1.html?data=l5yglp2Zd46ZmKiakpaJd6KpmYqgo5mXcYarpJKfj4qbh46kjoqkpZKUcYarpJLAy8zQpYzIjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Programa 23 de Febrero] 

Closet Abierto abre espacio para las personas de la sigla T (Transgénero, Transexuales, Travestis, Transformistas) que son las personas más vulnerables dentro de la comunidad LGBTI enfrentando problemáticas más allá del matrimonio y la adopción, las personas trans son las más discriminadas por el hecho de cambiar su género.

El programa es enfocado hacia personas transexuales, la invitada Deisy Olarte, que ya hizo su tránsito de hombre a mujer, explica las complicaciones que trae consigo el hecho de cambiar su género después de tener una vida establecida, a nivel personal, familiar, social y laboral, empezando por el hecho de que para que su tránsito sea atendido por las E.P.S deben declarar un trastorno de disforia de género.

Tanto en materia social como en el ámbito de derechos legalmente constituidos, existen vacíos que no permiten que las personas trans vivan dignamente, la salud y el empleo son los dos factores que más los afectan por ser zonas de mayor exclusión, la invitada nos demuestra a partir de su experiencia que Colombia es un país que todavía necesita replantear muchos contextos que desfavorecen a las minorías.
