Title: Sin Olvido - Parménides Cuenca Cortes
Date: 2015-01-05 14:50
Author: CtgAdm
Category: Sin Olvido
Slug: parmenides-cuenca-cortes
Status: published

###### Foto: Parménides Cuenca Cortes Sin Olvido. 

###### [Descargar Audio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/fsOcGuvg.mp3) 

Parménides Cuenca Cortes, nació en el año (1934) en Palermo Huila, prestó servicio militar y a su regreso, se vinculó a las luchas agrarias al lado de los campesinos; fue un dirigente social y político, constructor del Partido Comunista, Unión Nacional de Oposición Uno, Frente Democrático, sindicatos, juventudes y organizaciones de mujeres. Ocupo cargos de dirección, fue presidente de la Unión Patriótica en el momento de su surgimiento en el Castillo Meta, a la hora de su muerte era funcionario de la Alcaldía Municipal.  
El 4 de Noviembre de (1988) fue víctima de un atentado ejecutado por Paramilitares en conveniencia con policías y militares, donde falleció.  
Parménides fue fiel a sus principios y convicciones, amaba su partido, a la gente, siempre estuvo presto a servirle a quien lo necesitaba, compartía lo que tenía con sus amigos y compañeros. Después de (25) años de su asesinato, en octubre de (2013) fue detenido un reconocido paramilitar del Castillo Meta; Ever Salazar Alias lechona quien esta privado de la libertad. Estamos a la espera que se haga justicia.  
**Parménides Cuenca Cortes en la Memoria.**
