Title: Boletín informativo junio 25
Date: 2015-06-25 17:15
Category: datos
Tags: Cauca, Cumbre agraria se reune con el presidente Santos, Desastre ambiental Tumaco Nariño, Documentación sobre crímenes de Israel contra Palestina, el Mango Argelia Cauca, Habitantes sacan a la policia en el mango, presidenta del Consejo Comunitario Alto Mira y Frontera, Sara Valencia
Slug: boletin-informativo-junio-25
Status: published

[*Noticias del Día:*]

<iframe src="http://www.ivoox.com/player_ek_4688677_2_1.html?data=lZulmpube46ZmKiakpqJd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZeZcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-En el municipio de **Tumaco**, Nariño, se vive un **desastre ambiental** por cuenta del atentado perpetrado por la guerrilla contra un **oleoducto** de Ecopetrol. Pese a que algunos medios han informado que las autoridades y la empresa atienden la tragedia, lo cierto es que “**el plan de contingencia de Ecopetrol no ha servido y nadie ha mostrado una acción concreta de ayuda a la comunidad**” dice **Sara Valencia**, presidenta del **Consejo Comunitario Alto Mira y Frontera**, en Tumaco.

-Hoy las organizaciones de la **Cumbre Agraria** se reunieron con el presidente **Juan Manuel Santos** para evaluar las razones del **incumplimiento** y la **dilación** por parte del gobierno nacional. Según **José Santo Caicedo**, vocero del **Proceso de Comunidades Negras**, es necesario que se establezcan **mecanismos ciertos** y **eficaces** para que se logre avanzar y así **evitar una nueva etapa de movilizaciones** que serían en Agosto de este año.

-Este 25 de Junio, el ministro de Relaciones Exteriores de la Autoridad Palestina, **Riyad Maliki**, entregó documentación sobre **crímenes de Israel** al **fiscal de la CPI, Fatou Bensouda**. Según la agencia de noticias Ma’an. La **documentación incluye crímenes cometidos desde Junio de 2014 hasta mayo de 2015**. En este margen de tiempo, que es el que Palestina puede demandar ante la CPI, está incluida la **ofensiva israelí en masa** contra civiles palestinos que viven en el distrito de **Hebrón,** la **guerra en Gaza** del verano pasado, estadísticas sobre la **invasión a territorio palestino** en los asentamientos, los **prisioneros palestinos** en las cárceles de Israel y las **torturas** a las que son sometidos.

-La comunidad del corregimiento del **Mango** en **Argelia**, Cauca, asegura que **la actual situación es “muy difícil, tensa y que podría empeorar”**, teniendo en cuenta la llegada de **800 efectivos de la policía** que intentan retomar el control de la zona. Adicionalmente, denuncian el **accionar de los medios masivos de información** desde los cuales, se está **criminalizando la acción de resistencia** que están llevando a cabo los pobladores, con el único objetivo de **exigir que la Policia Nacional** deje de **usar a la población civil como trinchera**, como lo dice Orlando, habitante de la comunidad de El Mango.
