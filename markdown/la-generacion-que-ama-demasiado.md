Title: La generación que ama demasiado
Date: 2017-07-19 06:00
Category: Opinion
Tags: redes sociales
Slug: la-generacion-que-ama-demasiado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/aman-demasiado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [elzo-meridianos]

#### [**Por: Ángela Galvis Ardila**] 

###### 19 Jul 2017 

Si me pidieran definir esta generación, sin lugar a dudas se me vendrían a la cabeza varias definiciones, pero una que me inquieta mucho y, por tanto, podría encabezar la lista, es: la generación que ama demasiado. Aman a los desconocidos; aman las cosas; aman hacer o dejar de hacer; aman una foto; aman los planes; aman los chistes, aman el carro, aman el vestido, aman, aman y aman; aman a todos y a todo, y como si fuera poco, no aman solamente, aman demasiado.

Debo confesar que esta era posmoderna me ha vuelto una mujer abrazadora, envío abrazos a diestra y siniestra, pero por fortuna, o por infortunio (solemos pensar que lo que hacemos es lo mejor), no me tocó la era del amor desenfrenado. Mis te amo han estado reservados exclusivamente a seres vivientes, que de alguna manera u otra entiendan mi sentimiento y mis formas de expresarlo. Pero no solo eso, también son o han sido seres muy cercanos a mis afectos y a mi corazón. La verdad no logro imaginar diciendo, por ejemplo, amé tu comentario; amo ir de compras; amé como saliste en esa foto, es más, ni siquiera puedo decirlo de un lugar a donde uno amó la vida, como inmortalizó la gran Mercedes Sosa, a lo sumo, tendré buenos recuerdos y sentiré felicidad o nostalgia al verlo, ¿pero amarlo? hasta allá no llegan mis sentimientos, digo yo, o mi amargura, dirán otros.

Y dentro de estos te amo globalizados y efímeros, las redes sociales se han convertido en esa clase de escenario casi perfecto para comprender lo que ello significa, pues es allí en donde principalmente se logra sentir que se conoce a un mar de desconocidos a quienes fácilmente se les llega a escribir que se les ama y que cada cosa que hacen o dicen es objeto de ese sentimiento, así al segundo ya ni se acuerden de su grandes hazañas; y no se acuerdan porque vivimos tiempos de sentimientos ligeros, de afectos pasajeros que no resisten el menor soplo porque se derrumban. Cada día llega un objeto de un te amo nuevo que va de acuerdo a la tendencia que la red social imponga, que el medio de comunicación diga, que la moda dicte, porque gregaria como es la especie humana, ha caído en la trampa de creer que lo que dice o hace la mayoría es lo correcto y, si no es, no importa, lo importante es encajar, ser por los menos aceptados si no admirados. Convertimos el amor en una pose en un mundo plagado de apariencias. Ahora es James Rodríguez a quien todos aman: lo aman a él, aman que se haya ido del Real Madrid, aman que esté en el Bayern Múnich; aman que ya se haya puesto la camiseta; mañana será otro famoso o desconocido quien ocupe el lugar de James, y así cada día irán dirigiendo su amor y de paso sus te amo.

Y entonces, me pregunto: ¿a qué sentimiento irán a recurrir cuando de verdad conozcan el amor, cuando por los poros de la piel se les desborde el sentimiento, cuando el corazón les galope a mil, y no por algo material o por una acción sin mayor trascendencia, sino por unos ojos que no quieran dejar de mirar o por una manito que quepa dentro de las suyas? Soy una convencida de que las palabras se gastan por usarlas tanto, se vuelven vacías, pierden su encanto; no hay como un te amo susurrado, en privado, que sorprenda, que brinde calma, o uno a grito herido y que estremezca las vísceras, pero que en cualquier caso sea especial, único y tenga como destinatario a alguien que por sus venas corra sangre. Siento pena por aquellos que no llegarán a conocer lo que significa guardar los te amo para quienes nos hacen henchir el corazón, y por supuesto, sé que ellos sentirán pena de mí por no hacer parte de esta generación que ama demasiado.

------------------------------------------------------------------------

Ver: [El fantasma en el espejo de [Ángela Galvis Ardila]](https://archivo.contagioradio.com/el-fantasma-en-el-espejo/)
