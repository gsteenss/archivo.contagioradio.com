Title: En el Congreso y en la calle estudiantes defienden la lucha por la educación
Date: 2019-04-10 22:05
Author: CtgAdm
Category: Educación, Movilización
Tags: audiencia pública, educacion, estudiantes, ley 30
Slug: congreso-calle-estudiantes-lucha-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/estudiantes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Este miércoles se adelantó en el Congreso audiencia pública en la que se discutieron las modificaciones a la Ley 30 de educación superior; sería la tercera vez que los estudiantes son parte de este tipo de eventos, pues participaron en la audiencia pública sobre el desmonte del ESMAD y el debate de control político sobre Generación "E". En esta oportunidad, la reforma es promovida por Cambio Radical y contiene propuestas básicas que no satisfacen las necesidades de los estudiantes, pero el movimiento joven se sumará al debate como una forma de seguir impulsando una transformación en el sistema educativo del país.

**Cristian Guzmán, vocero de la Unión Nacional de Estudiantes de Educación Superior (UNEES)**, explicó que la intención de Cambio Radical es reformar los artículos 86 y 87 de la Ley 30 de educación superior, sin embargo, resaltó que este Proyecto no alteraría el sistema educativo ni la regla fiscal que aporta apenas lo necesario para que las instituciones de educación superior públicas cubran los gastos básicos de operación.

Pese a esta situación, los estudiantes buscaban aprovechar la audiencia para establecer consensos sobre lo que debería implicar una reforma a la Ley 30; así como sensibilizar a la sociedad sobre la necesidad de brindar mayores recursos a la educación, y encontrar en esta inversión una posibilidad para cerrar la brecha de desigualdad que existe en el país. (Le puede interesar: ["Más de 1.500 estudiantes se dieron cita en Cauca para hablar de educación"](https://archivo.contagioradio.com/estudiantes-cauca-educacion/))

### **Gobierno Duque: 2 acuerdos pactados con el movimiento social (2 incumplimientos)** 

El año pasado los estudiantes vivieron un momento cumbre de movilización, motivados por el evidente hueco financiero en las universidades que tenía a varias instituciones al borde de clausurar programas. Las manifestaciones lograron cerrar un acuerdo con el Gobierno Nacional, pero Guzmán señaló que hasta el momento "gran parte de lo que debería estar cumplido no ha pasado"; al igual que ocurrió con la Minga del suroccidente, con la que se comprometió a dialogar este martes pero desistió del pacto.

En ese sentido, el integrante de la UNEES recordó que entre las reivindicaciones del paro nacional del próximo 25 de abril, estará el apoyo a la educación superior pública, y la unión de diferentes sectores sociales que pujan por lograr el cumplimiento de los compromisos alcanzados con el Gobierno, la implementación del Acuerdo de Paz, y contra el Plan Nacional de Desarrollo. (Le puede interesar: ["Duque tendrá que hablar con la Minga nacional del 25 de abril: CRIC"](https://archivo.contagioradio.com/duque-tendra-que-hablar-con-la-minga-nacional-del-25-de-abril-cric/))

### **A luchar por una educación del tamaño de sus sueños** 

Recientemente se realizaron los exámenes de ingreso a la Universidad Nacional, según los cálculos del exrector de la institución, Ignacio Mantilla, solo 1 de cada 10 aspirantes que se presentan lograrán un cupo; y de aquellos que logran ingresar al año cerca de un 7% desertan de sus programas académicos entre otras cosas, por falta de condiciones para continuar con sus estudios. Para Guzmán, estas situaciones definen algunas de las razones para seguir en la búsqueda de una educación al nivel de los sueños de las familias colombianas.

Para lograr una modificación de ese calibre, los estudiantes buscarán articulaciones con otros sectores que les permitan tener mayor incidencia en la movilización social y legislativa. Precisamente una de las opciones contempladas será el referendo por la educación, que la declararía como un derecho fundamental. (Le puede interesar:["Estudiantes de la Capital se reencuentran por la educación superior"](https://archivo.contagioradio.com/estudiantes-educacion-superior/))

<iframe id="audio_34336320" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34336320_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
