Title: Espionaje de Ejército colombiano no puede seguir impune: Organizaciones de EE.UU.
Date: 2020-05-26 21:39
Author: AdminContagio
Category: Actualidad, El mundo
Tags: Ejército, Seguimientos
Slug: espionaje-de-ejercito-colombiano-no-puede-seguir-impune-organizaciones-de-ee-uu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Inteligencia-militar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @CarlosHolmesTru {#foto-carlosholmestru .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Quince organizaciones defensoras de DD.HH., de la sociedad civil y académicas enviaron una carta al embajador de Estados Unidos en Colombia y al Departamento de Estado en la que expresaban su preocupación respecto al reciente caso descubierto de espionaje y perfilamiento a defensores de DD.HH., abogados, periodistas y lideres sociales en Colombia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Inteligencia militar y espionaje a 'oposición', flagelos que se repiten

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las organizaciones recuerdan hechos como el asesinato de Alvaro Gómez Hurtado, el escándalo de las chuzadas del Departamento Administrativo de Seguridad (DAS) , la operación Andrómeda y la creación de la lista de 'oposición' en Twitter que demuestran la forma en que algunas instituciones han hecho uso de la inteligencia militar, contra quienes considera que son sus 'enemigos'.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para la codirectora de Latin America Working Group (LAWG) Lisa Haugaard, estos hechos son preocupantes porque los escándalos siguen pasando y se siguen viendo casos de seguimiento y espionaje a "importantes actores democráticos en Colombia". Aunque la historia se repite, las organizaciones cuestionan la incapacidad del Estado para aprender de sus errores y corregirlos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, Haugaard declara que aunque se dieron los escándalos, y se cerró el DAS, nuevamente se presentan estos hechos. Razón por la que manifiesta como necesario un cambio en la doctrina militar, así como investigaciones que determinen los culpables de usar la inteligencia para perseguir a personas que tienen labores legítimas en una sociedad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con la mención sobre la lista de oposición del Ejército revelada por [Cuestión Pública](https://twitter.com/cuestion_p/status/1237382254952763392) en 2019 las organizaciones se preguntan sobre lo que para las FF.MM. es la opsición. El tema no es menor, porque parecería responder a la doctrina del enemigo interno que considera como enemigos a líderes sociales, defensores de DD.HH., jueces, periodistas y organizaciones civiles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por lo tanto, la integrante de LAWG señala como importante que todos los agentes del Estado entiendan la importante función que cumplen estas personas en la democracia. (Le puede interesar: ["Seguimientos contra periodistas son propios de regímenes totalitarios: FLIP"](https://archivo.contagioradio.com/seguimientos-contra-periodistas-son-propios-de-regimenes-totalitarios-flip/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Colombia ha tenido avances, pero la quieren regresar al pasado

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el documento se menciona la revisión de la doctrina militar realizada durante el gobierno Santos, y se señala la necesidad de continuar esta revisión con el acompañamiento de instituciones como Naciones Unidas y organizaciones defensoras de DD.HH. Por su parte, Haugaard marca como un avance el Proceso de Paz, por lo que dice que es "frustrante" cuando hay sectores al interior de las Fuerzas Militares que quieren volver al pasado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ello, asegura que el presidente Duque debe asumir la responsabilidad en este tipo de conductas y dar por terminadas estas prácticas para siempre. En cuanto al papel de la ayuda que brinda en términos de inteligencia Estados Unidos, manifiesta que miembros del Congreso también están preocupados ante la situación, y buscando salidas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Otras recomendaciones hechas por las organizaciones

<!-- /wp:heading -->

<!-- wp:paragraph -->

Además de la revisión en la doctrina militar, las organizaciones invitaron al gobierno colombiano a proteger aquellas personas que fueron objeto de seguimientos y brindarles el acompañamiento necesario para superar esta situación. También manifestaron la necesidad de eliminar los archivos obtenidos de estas personas, y revelar el alcance de las operaciones a la sociedad, así como las personas detrás de los seguimientos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar:

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
