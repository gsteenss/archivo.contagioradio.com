Title: Comunicación desde los territorios
Date: 2014-12-16 15:44
Author: CtgAdm
Category: Video
Slug: comunicacion-desde-los-territorios
Status: published

\[embed\]https://www.youtube.com/watch?v=9wjMEWabp7g\[/embed\]  
Sistematización visual de talleres de comunicación realizados por Contagio Comunicación Multimedia, en comunidades rurales de la red de Comunidades Construyendo Paz en los Territorios, CONPAZ.  
CONPAZ es una Iniciativa de protección territorial y de apuestas por un nuevo país con democracia, justicia social y ambiental de comunidades afrodescendientes, indígenas y mestizas y cuenta con un equipo de comunicadores en cada una de las organizaciones que son parte de ésta. Contagio apoya el proceso de formación en comunicación de CONPAZ y divulga sus contenidos.
