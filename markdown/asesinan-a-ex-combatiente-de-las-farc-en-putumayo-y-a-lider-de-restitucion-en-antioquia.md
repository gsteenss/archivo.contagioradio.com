Title: Asesinan a ex combatiente de las FARC en Putumayo y a líder de sustitución en Antioquia
Date: 2017-10-30 13:39
Category: DDHH, Nacional
Tags: Antioquia, asesinato de líderes sociales, Briceño, Sustitución de cultivos de uso ilícito
Slug: asesinan-a-ex-combatiente-de-las-farc-en-putumayo-y-a-lider-de-restitucion-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/DNZDctpXcAAilTd.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter] 

###### [30 Oct 2017] 

En las últimas horas se conoció la información sobre el asesinato de Martín, integrante del Frente 32. El hecho habría ocurrido este 29 de Octubre hacia las 9 de la noche, sin que se conozcan más detalles del hecho. Minetras que en Antioquia habría sido asesinado Ramón Alcides García, líder del proceso de sustitución de cultivos de uso ilícito en la región. **Ambos hechos han sido calificados como una muestra de la falta de garantías en la implementación del acuerdo, tanto para excombatiente como para civiles.**

En el caso de Martín, excombatiente, la información la proporciona REC SUR, en la vereda La Pedregosa, inspección de José María del municipio de Puerto Guzmán, en Putumayo. El medio de información señala que la comunidad tiene una preocupación puesto que es **muy notoria la presencia de estructuras militares desconocidas muy cerca de puestos de control de a Fuerza Pública.**

Para las comunidades este hecho es muestra de la falta de voluntad por parte de la Fuerza Pública para prevenir este tipo de hechos y brindar las garantías de seguridad a quienes culminan el proceso de dejación de armas como en el caso de Martín, quien ya había cumplido los requisitos de Ley para quienes hacían parte de la guerrilla de las FARC y ahora conforman el partido político.

### Asesinato de Ramón García, líder de sustitución de tierras en Briceño Antioquia 

Por otra parte, según la información entregada por Fabio Muñoz, líder del municipio de Briceño, **Ramón Alcides García era uno de los** **más prominentes líderes del proceso de sustitución de cultivos de uso ilícito en la región,** concretamente en el alto del Charí. Además era integrante de Marcha Patriótica.

Según la denuncia de varias organizaciones presentes en la zona, el asesinato se dio el pasado 27 de Octubre hacia las 2 de la tarde. “El asesinato se da luego de que Ramón Alcides García regresaba del municipio de San Andrés de Cuerquia tras recibir el pago mensual contemplado en el proceso de sustitución voluntaria”.

Aunque los móviles del asesinato se desconocen hasta el momento y no hay investigaciones contundentes que permitan establecer lo sucedido, el hecho es un llamado de alerta frente a la situación que afrontan los campesinos en regiones en las que existen los cultivos de coca y hay líderes que se enfrentan a estructuras criminales que subsisten de ese negocio.

###### Reciba toda la información de Contagio Radio en [[su correo]
