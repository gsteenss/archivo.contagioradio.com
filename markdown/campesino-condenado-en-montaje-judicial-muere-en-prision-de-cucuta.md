Title: Muere en prisión campesino condenado tras montaje judicial
Date: 2015-02-13 19:54
Author: CtgAdm
Category: DDHH, Nacional
Tags: carcel, Derechos Humanos, presos, prisioneros, tramacua, violacion
Slug: campesino-condenado-en-montaje-judicial-muere-en-prision-de-cucuta
Status: published

###### [Foto: cnabogota.blogspot] 

<iframe src="http://www.ivoox.com/player_ek_4080063_2_1.html?data=lZWlkpWad46ZmKiak5mJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNDm1drfw5DKaaSnhqax1c7HpYampJDd1c7Hs82ZpJiSpJjLrcTVzsrb1sqPvYzqhqigh6aopYzjzs7gy4qnd4a2lNOYz4qnd4a1msmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Yuly Henriquez, Lazos de dignidad] 

Jesus Miguel Velandia León tenía 49 años cuando fue capturado en la vereda La Cristalina (Rionegro, Santander), acusado de Rebelión, en noviembre del 2010. No sabía leer ni escribir, y como consecuencia de una mala asesoría jurídica del abogado de oficio, se declaró culpable para acceder a beneficios de reclusión.

En 2012 se le detectó cáncer de páncreas, y durante los últimos 3 años, Jesus Miguel y las organizaciones de Derechos Humanos que acompañan prisioneros políticos en Colombia, dieron una dura batalla porque se le garantizara su derecho a la salud. Sin embargo, el Estado no garantizó el debido tratamiento que requería su estado, una atención oportuna y eficaz y que ayudara a mantener con vida digna al prisionero. Jesus Miguel murió el 11 de Febrero de 2015, a los 54 años de edad, en situación de reclusión en la cárcel de Palo Gordo, Santander. No pudo pasar sus últimos días con su familia, pues el INPEC consideró que su enfermedad no era incompatible con el estado de reclusión.

Para la abogada Yuly Henriquez, coordinadora nacional de Lazos de Dignidad, el caso de Jesus Miguel se repite de manera sistemática en contra de la totalidad de los prisioneros y las prisioneras políticas en Colombia.

Otro caso de tortura por omisión médica la vive actualmente Alberto Carrillo Pinto, prisionero político de guerra recluido en La Tramacúa, llamada por los internos "La Guantánamo de Colombia". Alberto Castillo tiene una herida de guerra en la pierna desde el momento en que fue capturado y recluido el 11 de agosto del 2013, que no ha sido tratada efectivamente, generándole infecciones y dolor permanentemente.

Estando en esa condición de salud e indefensión, la pasada noche del 3 de febrero guardias del INPEC pusieron un frasco de gas pimienta en la celda de Alberto, lo que le generó nuevas heridas. Para la abogada Henriquez, esta forma de tortura es el resultado de los reclamos de los prisioneros políticos que exigen atención médica. "El tratamiento del Estado, ademas de la omisión, es la represión contra las personas que reclaman su derecho a la integridad personal y a la salud".

Las situación de las prisioneras políticas no dista mucho de esta realidad. Las mujeres recluidas en el Buen Pastor de Bucaramanga denunciaron el 13 de febrero cómo a la tortura física se le suma la tortura psicológica. Guardias del INPEC les realizan requisas corporales invasivas, las obligan a desnudarse e inclinarse frente a ellos con la excusa de buscar estupefacientes, como ocurrió en días pasados con la prisionera política Yesenia Pabón.

"Las organizaciones de Derechos Humanos llevamos un largo tiempo exigiéndole al Estado que resuelva el problema de fondo en las cárceles y que cese el comportamiento represivo contra prisioneros y prisioneras políticas", señaló la abogada. Desde 1998 la Corte Constitucional declaró un estado de cosas inconstitucionales en las cárceles de Colombia, y la respuesta del Estado ha sido construir -con dinero de los EEUU- nuevas prisiones e imponer un nuevo modelo que mal llamó "nueva cultura penitenciaria", que se basa en la seguridad, la restricción de Derechos fundamentales y la construcción de más cárceles, siguiendo el modelo Estadounidense al que sólo le interesa el vínculo con lo económico. "El Estado busca acumular en las cárceles a los seres humanos, como en bodegas, con el propósito de llenar los bolsillos de los entes privados", concluyó Henriquez.
