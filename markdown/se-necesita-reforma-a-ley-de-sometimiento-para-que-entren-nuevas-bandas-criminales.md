Title: Se necesita reforma a ley de sometimiento para que entren otras bandas criminales
Date: 2018-07-18 12:43
Category: Nacional, Política
Tags: Antioquia, Carlos Chatas, Comuna 13, Juan Carlos Mesa, La Oficina, Medellin, Valle de Aburra
Slug: se-necesita-reforma-a-ley-de-sometimiento-para-que-entren-nuevas-bandas-criminales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/la-fm.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La FM] 

###### [18 Jul 2018] 

De ser acogida la petición de Carlos Mesa, alias “Carlos Chatas” líder de la estructura La Oficina en Medellín, de ser parte de la Ley de Sometimiento a bandas Criminales, Antioquia podría ser la gran beneficiada, ya que, de acuerdo con Fernando Quijano, esta organización controla el 80% de la ciudad de Medellín y cuenta con más **de 13 mil personas, 350 bandas criminales en la ciudad de Medellín y más de 500 en el resto del Valle de Aburrá.**

En la carta enviada por Mesa al presidente Santos y al electo, Iván Duque, afirma que su entrega haría parte del proceso colectivo **del 70% de este grupo que entraría a la Ley de sometimiento.** Sin embargo, de acuerdo con Quijano, se debe abrir el debate frente a si la normativa podría acogerlos o necesitaría una reforma urgente para llevar a cabo este proceso.

Esto debido a que actualmente dentro de esta Ley solo entran las bandas criminales caracterizadas por el Consejo Nacional de Seguridad como grupos armados y actualmente esos grupos son el Ejército de Liberación Nacional, el Clan del Golfo, Los Pelusos, Los Puntilleros y las disidencias de las Fuerzas Armadas Revolucionarias de Colombia.

“La carta es importante porque no solo habla de someterse el, sino de facilitar el sometimiento de la dirección colegiala, **segundo deja en claro que está inactivo y tercero plantea que no tiene nada que ver con lo que está pasando en la Comuna 13**” afirmó Quijano y agregó que ahora tanto el alcalde de Medellín, como los alcaldes del resto del Valle de Aburra logren generar una presión para hacer las reformas que se necesiten en la Ley de Sometimiento.

### **La Oficina y su control territorial en Antioquia** 

Asimismo, Quijano manifiesta que, si bien es cierto que con este proceso no se vaya a acabar con el negocio del narcotráfico en Antioquia, si se daría un gran paso sobre el control territorial que tiene este grupo sobre las comunas que en Medellín **representa un 80%, mientras que en el Valle de Aburra sería del 70%**. Hecho que ayudaría a disminuir la violencia en el territorio, separaría la política del crimen urbano y un sector de la institucionalidad que hace parte de estas estructuras.

Esta organización actualmente, estaría conformada por dos líneas: la primera es la línea 80 mayoritaria que la conformarían la dirección colegiada de grupos armados del Valle de Aburra y sus alrededores, y la segunda línea o línea 20, estaría conformada por las bandas criminales de “los Pesebreros” y “La Terraza”.

Dentro de sus filas contarían con personas encargadas no solo de ser “gatilleros” sino también de quienes alertan sobre operativos, conocidos como “campaneros”, quienes realizan las extorsiones y ejercen el control en las comunas. (Le puede interesar: ["Comunidades rurales celebran Ley de Sometimientos")](https://archivo.contagioradio.com/comunidades-celebran-ley-de-sometimiento/)

<iframe id="audio_27124286" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27124286_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
