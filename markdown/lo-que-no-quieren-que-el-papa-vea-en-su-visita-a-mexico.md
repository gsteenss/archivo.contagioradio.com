Title: Lo que no quieren que el Papa vea en su visita a México
Date: 2016-02-12 12:49
Category: El mundo, Política
Tags: Papa Franciso en México, Visita Papa a México
Slug: lo-que-no-quieren-que-el-papa-vea-en-su-visita-a-mexico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Ciudad-de-México.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sin Embargo ] 

<iframe src="http://www.ivoox.com/player_ek_10414281_2_1.html?data=kpWhk5mWfJKhhpywj5aVaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5ynca3jjNbix5DSs4zl1s7S1MrSb9LpxpDSzpC0pdHVjNvSw5DJsozn1pDjy9jNuMKfwpC6h6iXaaKt2c7Qj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Héctor Cerezo] 

###### [12 Feb 2016]

Este viernes el Papa Francisco llega a México en medio de fuertes denuncias de diversas organizaciones sociales que aseguran que autoridades gubernamentales **están retirando a los habitantes de calle de todos los lugares por los que el sumo pontífice va a pasar**. Así mismo en zonas densamente pobladas y con altos índices de pobreza han pintado las calles “para aparentar una ciudad que no es la Ciudad de México, **una ciudad profundamente desigual y con mucha discriminación**”, como lo asevera Héctor Cerezo, integrante del ‘Comité Cerezo’.

Los **alarmantes índices de desaparición forzada de jóvenes y periodistas** en estados como Veracruz, en el que en los últimos 5 años fueron asesinados 23 periodistas, también están tratando de ser ocultadas por las autoridades,  tras la llegada del Papa, quien aseveró que **no va a reunirse con las víctimas de este delito ni del de violación sexual**, afirma Cerezo.

En relación con el más reciente informe de desaparición publicado en México, Cerezo asegura que la cifra oficial de 27 mil personas desaparecidas se ha manejado desde que Enrique Peña Nieto asumió la presidencia, un índice que además de no discriminar el actor infractor ni los tipos de desapariciones, pretende señalar una reducción en los casos, mientras que **la documentación de diversas organizaciones de derechos humanos prueba su incremento** por parte de militares y policías mayoritariamente.

“El 90% de las personas desaparecidas no hacen parte de ese informe oficial” afirma Héctor y agrega que **“no hay una base de datos confiable que permita comprender la gravedad de la desaparición forzada en México”** que de acuerdo con su documentación ya supera la cifra de 30 mil.

Actualmente se discuten en el Congreso mexicano cuatro iniciativas de ley, una de ellas elaborada por organizaciones defensoras de los derechos humanos, que se esperan se conviertan en una ley general contra la desaparición forzada, basada en los estándares internacionales aprobados en la materia, teniendo en cuenta que como concluye Cerezo **no se evidencia “un esfuerzo real por parte del Estado mexicano para buscar a las personas desaparecidas** ni por hacer justicia ante las ejecuciones que se están presentando”.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
