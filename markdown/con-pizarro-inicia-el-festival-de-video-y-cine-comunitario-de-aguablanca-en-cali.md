Title: Con "Pizarro" inicia el Festival de Video y Cine comunitario de Aguablanca en Cali
Date: 2015-10-13 16:22
Author: AdminContagio
Category: 24 Cuadros
Tags: Distrito de Aguablanca, FESDA, Festival de Video y Cine comunitario Distrito de Aguablanca, Maria Jose PIzarro, Marta Rodríguez Documentalista colombiana
Slug: con-pizarro-inicia-el-festival-de-video-y-cine-comunitario-de-aguablanca-en-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/fesda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [13, Oct 2015]

El distrito de Aguablanca, lugar donde habita cerca del 30% de la población de la ciudad de Cali, se transforma desde hoy con la 7ma edición del **Festival de Video y Cine Comunitario FESDA 2015**, un evento que busca visibilizar las iniciativas culturales, de uno de los sectores más estigmatizados de la capital Vallecaucana.

Serán 4 días, en los que propios y extraños, podrán compartir conocimientos y experiencias a partir de la consigna "**Comunidad y Memoria son un solo rollo**", dando espacio a la importancia que tiene la recosntrucción de la memoria para encontrar los caminos que lleven al país a la paz y la reconciliación.

![fesda2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/fesda2.jpg){.aligncenter .wp-image-11248 width="343" height="530"}

El Festival abre su programación con el panel "**La importancia de la realización audiovisual comunitaria en la construcción de memoria como herramienta de resistencia social**’, a partir de la proyección del documental "**Pizarro**", con participación de María José, hija del caído líder del M19, el crítico Omar Rincón, Eduardo Montenegro y Mauricio Acosta, que se realizará en la cinemateca del Museo la Tertulia.

**16 producciones colombianas** conforman la selección oficial, participando en las categorías Documental, Ficción, Experimental –Video clip o animación y Video universitario con comunidades. **5 producciones internacionales** y una **retrospectiva** del trabajo de la documentalista **Marta Rodríguez**, complementan la programación de exhibición que se desarrollará en 14 espacios,cerrados y al aire libre del distrito.

FESDA 2015, incluye algunos talleres como "Documental autobiográfico" con participación de María José Pizarro, ‘Apreciación Audiovisual’ (En el Centro de Menores El Buen Pastor), ‘Reconstruyendo mi imagen del Distrito de Aguablanca’,entre otros,  impartidos por los colectivos Digna Rabia - Bogotá, Pasolini - Medellín, Full Producciones - Medellín, Montes de María Línea 21 – Carmen de Bolívar, Made in Chocó, Chocó.

Como aporte a la memoria, el Festival incluye la la **Actividad por la Protección y el respeto por la vida**, en conmemoración de la memoria de Jonathan Goapacha y Andres Rodriguez, jóvenes asesinados impunemente. Un acto por etapas que busca además reclamar justicia, protección para los familiares de las dos víctimas y despertar sentimientos de reconciliación y esperanza.

El cierre y premiación del evento tendrá lugar en Cine Colombia del Centro Comercial Rio Cauca, el sábado 17 en horas de la tarde. Contagio Radio, estará acompañará en el cubrimiento del Festival, con entrevistas y reportes desde el Distrito de Aguablanca.

[FESDA](https://es.scribd.com/doc/284835461/FESDA "View FESDA on Scribd")

<iframe id="doc_48781" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/284835461/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>  
[  
](https://archivo.contagioradio.com/comunidad-y-memoria-un-solo-rollo-en-agua-blanca/fesda2/)

 

 
