Title: Torturas de la CIA son minimizadas por el gobierno de Estados Unidos
Date: 2014-12-11 15:32
Author: CtgAdm
Category: El mundo
Slug: torturas-de-la-cia-son-minimizadas-por-el-gobierno-de-estados-unidos
Status: published

Foto: periodistadigital

La técnica conocida como el “Submarino”, ha sido una de las más usadas por la CIA contra sospechosos de terrorismo, y consiste en el ahogamiento al punto de la muerte para luego intentar sacar respuestas. Sin embargo existen otras prácticas, señaladas en el informe como la administración de medicamentos para impedir el sueño, o para provocar daños estomacales, e incluso medicamentos que borran la memoria, son algunas de las denuncias de las víctimas. Sin embargo, las consecuencias de estos actos no llegarán más allá de un escándalo mediático de 2 semanas según lo señala el analista Cesar Torres.

 
