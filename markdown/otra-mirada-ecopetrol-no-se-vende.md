Title: Otra Mirada: Ecopetrol no se vende
Date: 2020-07-30 21:13
Author: AdminContagio
Category: Nacional, Otra Mirada, Programas
Tags: Ecopetrol, Movilizaciones, Venta de Ecopetrol
Slug: otra-mirada-ecopetrol-no-se-vende
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Ecopetrol-scaled.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @oleoductos

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

3Treinta y seis son los días que cumplen, tanto miembros de la Unión sindical obrera (USO) encadenados al monumento de Ecopetrol en Bogotá, como las protestas que vienen realizando los trabajadores desde los territorios para impedir la venta de Ecopetrol y de sus filiales. (Le puede interesar: [La USO completa 30 días de huelga por la defensa del patrimonio](https://archivo.contagioradio.com/la-uso-completa-30-dias-de-huelga-por-la-defensa-del-patrimonio/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los trabajadores y manifestantes que hicieron parte de este conversatorio y dieron un contexto desde sus territorios fueron William Silgado, presidente USO oleoductos Magdalena Medio desde Bucaramanga, Javier Angarita, sindicalista desde Yopal, Casanare, Óscar Martínez desde Mariquita, Tolima, Hernando Silva, desde El Machín de la resistencia, Bogotá y Javier Angarita, trabajador de la planta Araguaney.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los participantes explican la situación actual, las razones por las cuales no se debe vender la estatal petrolera Ecopetrol y los motivos para tomarse por las diferentes plantas, uniéndose a las movilizaciones que se vienen realizando en diferentes sectores del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez, explican qué garantías existen para quienes protestan sabiendo que en medio de la pandemia, la situación es mucho más complicada y agregan que ha habido obstaculización por parte de la empresa, utilizando las medidas tomadas por el gobierno debido a la crisis por la Covid-19 para impedir que los trabajadores se movilicen.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, explican los avances en las conversaciones con la empresa y cómo esperan que los acuerdos a los que se lleguen se vean reflejados en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El principal mensaje de cada uno de los ponentes es que debemos unir las voces y la bandera como colombianos por la defensa del país e invitan a la movilización nacional para demostrar que lo público no se vende.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Programa del 29 de julio: [Otra Mirada: Matrícula cero por el derecho a la educación](https://www.facebook.com/contagioradio/videos/210520340359898))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Haga click aquí](https://es-la.facebook.com/contagioradio/videos/675277499723741/) para escuchar el análisis completo

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
