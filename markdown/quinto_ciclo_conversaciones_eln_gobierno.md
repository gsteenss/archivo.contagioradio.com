Title: Próxima semana podría iniciar quinto ciclo de conversaciones ELN-Gobierno
Date: 2018-03-09 13:08
Category: Paz
Tags: Cese unilateral fuego, ecuador, ELN, Quito
Slug: quinto_ciclo_conversaciones_eln_gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [09 Mar 2018] 

Hoy inicia el cese unilateral por parte de la guerrilla del ELN, que durará 5 días. Una acción que busca restablecer la confianza en el proceso de paz que se a**delantaba en Quito y con lo que también se quiere motivar el inicio del quinto ciclo de conversaciones que podría iniciar la próxima semana**, de acuerdo con las afirmaciones de Luis Emil Sanabria, director de Redepaz.

“Le estamos diciendo al gobierno, en cabeza del presidente Juan Manuel Santos, y al ELN, en cabeza del COCE que **ya están todas las condiciones que habían solicitado para reactivar el quinto ciclo de conversaciones**, que con la entrada del cese unilateral del ELN, permite que la semana entrante, ojalá, podamos tener buenas noticias de la mesa”, señaló Sanabria.

De igual forma, organizaciones sociales y ciudadanas, están realizando diferentes actividades para apoyar el proceso de paz. “R**espaldamos esta mesa de conversaciones y queremos que se instale porque queremos participar**, llevar ideas y propuestas para superar el conflicto armado”, manifestó Sanabria.

Una de esas actividades inició este viernes en la Plaza Simón Bolívar, en donde con una serenata al proceso de paz se pretende impulsar la retoma del diálogo y celebrar el inicio del cese unilateral que, según Sanabria, **se da por primera vez en la historia colombiana en el marco de unas elecciones a la Cámara y al Senado, en paz**. (Le puede interesar:["ELN anuncia cese  de operaciones militares desde el 9 hasta el 13 de marzo"](https://archivo.contagioradio.com/eln-anuncia-cese-operaciones-militares-ofensivas-desde-9-13-marzo/))

### **Ecuador apoya los diálogos de paz** 

A través de un comunicado de prensa, el gobierno de Ecuador reiteró su voluntad de poner a disposición del presidente Juan Manuel Santos todo lo que Colombia requiera para lograr “la paz completa”.

En ese sentido afirmaron que unen su voz al “clamor de la comunidad internacional, de la iglesia católica y el Consejo Mundial de Iglesias, de la Organización de Naciones Unidas y del propio pueblo colombiano” **para que se realicen los esfuerzos necesarios que conduzcan al desescalamiento del conflicto** y el cese de hostilidades que afectan a la población civil.

<iframe id="audio_24311841" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24311841_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
