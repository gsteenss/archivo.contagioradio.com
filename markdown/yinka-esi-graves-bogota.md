Title: Yinka Esi Graves en Bogotá
Date: 2018-06-04 11:00
Category: Cultura
Tags: Bogotá, danza, flamenco
Slug: yinka-esi-graves-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/112917-DoublePlusWeek1-YinkaEsiGraves-byScottShaw-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Scott Shaw 

###### 04 Jun 2018 

Luego de su paso por Medellín y Quibdó, la Corporación Colombiana de Teatro, presenta los días 7 y 8 de Junio a la bailarina británica afro-flamenca Yinka Esi Graves, quien impartirá en Bogotá un taller sobre su técnica de hibridación corporal, y presentará su último trabajo artístico.

Graves, inicio a bailar desde los 5 años y a los 21 comenzó su formación artística. Estudió flamenco en Sevilla, España en la mundialmente reconocida escuela: “Amor de Dios”, con artistas tales como: La Lupi, Manuel Reyes y Pepa Molina, "mi interés por el flamenco nació desde el hacer y se ha ampliado con los años" asegura la artista.

Dentro de su larga trayectoria se resalta su participación en el ballet Alvin Ailey, donde acompañó al bailarín principal de la compañía newyorkina Asha Thomas, también ha participado en eventos de gran importancia como Châteauvallon CND (Francia), Mes de Danza (Sevilla), y su más reciente participación en el Dance Umbrella’s programa ‘Out of the System’ (UK).

Acerca del taller Yinka resalta lo particular que tiene este tipo de danza para que los participantes logren expresarse corporalmente "lo que me parece maravilloso del flamenco es que realmente cuando ya entendemos lo que es el compás y el ritmo y podemos entrar en ello permite luego que cada persona encuentre su forma de expresarse"

El taller de Rítmica Corporal, dirigido para todo público, será dictado los días 7 y 8 de junio de 9:00 am a 12:00m en las instalaciones de la Corporación Colombiana de Teatro, sus asistentes podrán tener un encuentro personal con la música, el canto y el baile, que al ser articuladas con lo africano y el flamenco, abordarán la conciencia del cuerpo, el ritmo, el suelo y la energía en el proceso de la danza en escena.

<iframe src="https://player.vimeo.com/video/69828724" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### [Yinka Esi Graves PROMO](https://vimeo.com/69828724) from [Pedram Yazdani](https://vimeo.com/pedramy) on [Vimeo](https://vimeo.com).

Además del taller, Yinka presentará su más reciente repertorio titulado “Afroantillanias entre Montañas” flamenco “Afrodiasporas, diálogos culturales y corporales”, esta compuesta por una pieza corta extraída del espectáculo que realiza junto a la bailarina de danza contemporánea Asha Thomas, un encuentro "donde podíamos indagar en nuestro interés como mujeres negras en el flamenco", y dos palos flamencos en los que busca que la gente "sintiera que ya dentro del flamenco hay algo africano"

La presentación, que además cuenta con el acompañamiento del canté de la Colombiana Clara Rozo, busca reflexionar sobre el ser humano hibrido, la heterogeneidad cultural, y la imprevisibilidad, en búsqueda de un pensamiento crítico, donde el poder y la resistencia son protagonistas.

El encuentro iniciará a las 8:00p.m en la Sala Seki Sano ubicada en la Calle 12 \# 2 – 65, con un costo de \$15.000 para estudiantes y \$25.000 para público general. Un evento organizado por Bailes Afroantillanos Escuela de Danza Teatro y Sankofa, con el apoyo de la Corporación Colombiana de Teatro.

 

<iframe id="audio_26432962" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26432962_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
