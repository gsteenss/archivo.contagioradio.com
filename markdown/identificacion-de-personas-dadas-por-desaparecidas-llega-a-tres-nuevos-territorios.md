Title: Identificación de personas dadas por desaparecidas llega a tres nuevos territorios
Date: 2019-09-17 18:30
Author: CtgAdm
Category: Nacional, Paz
Tags: 1 de Mayo Bogotá Ciudad Bolívar, Antioquia, Bogotá, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: identificacion-de-personas-dadas-por-desaparecidas-llega-a-tres-nuevos-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Desaparecidas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

La Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD) junto al Institutode Medicina Legal y la Fiscalía General pondrán en marcha la nueva fase del proyecto que busca impulsar el proceso de identificación de cuerpos de personas que pudieron desaparecer en el marco del conflicto armado en tres nuevas regiones: **Bogotá, el Caribe y el noroccidente del país,**  llegando en la actualidad a cinco regiones de Colombia.

La iniciativa que llegará en particular a las ciudades de Bogotá, Medellín y Barranquilla, realizará un diagnóstico al proceso de identificación de los cuerpos que fueron sometidos a necropsias en diferentes instituciones del Estado y que no han sido identificados ni entregados a sus familiares.

Este proceso se logrará a través de la consolidación de la información disponible que se encuentra en expedientes físicos y digitales de necropsia, archivos, libros y bases de datos, para luego desarrollar planes de acción orientados a identificar y entrega de los cuerpos que podrían corresponder a personas que desaparecieron en el marco del conflcito armado y que continúan en instituciones del Estado o en cementerios.

### Continúa proyecto piloto en la búsqueda de personas desaparecidas

Este plan es la continuación del proyecto piloto que inició la UBPD junto al Instituto de Medicina Legal y la Fiscalía en julio cuando comenzó a impulsarse el proceso de identificación de cerca de 500 cuerpos en Nariño y 1.600 en Norte de Santander. [(Le puede interesar: Unidad de Búsqueda comienza piloto de identificación de 2.100 cuerpos en Norte de Santander y en Nariño)](https://archivo.contagioradio.com/unidad-busqueda-piloto-narino-norte-santander/)

La Unidad de Búsqueda de Personas dadas por Desaparecidas tiene previsto llegar al resto del país a partir de octubre con esta articulación entre instituciones que permitirá responder a los familiares de las víctimas conocer el paradero de sus seres queridos. [(Lea también: En 2019 Unidad de Búsqueda de Desaparecidos llegará a 17 territorios)](https://archivo.contagioradio.com/unidad-de-busqueda-de-desaparecidos/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
