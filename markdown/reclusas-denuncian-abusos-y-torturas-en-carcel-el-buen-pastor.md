Title: Reclusas denuncian abusos y torturas en cárcel "El Buen Pastor"
Date: 2016-03-28 13:17
Category: DDHH, Nacional
Tags: Cárcel El buen Pastor, Crisis carcelaria colombia
Slug: reclusas-denuncian-abusos-y-torturas-en-carcel-el-buen-pastor
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/carcel-mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [28 Mar 2016] 

Reclusas del Patio 9 de "El buen Pastor" de Bogotá, denuncian que desde el pasado 22 de marzo se vienen presentando una serie de arbitrariedades, abusos y maltratos por parte de la guardia del INPEC, quienes de manera arbitraria estarían aplicando un "régimen penitenciario" en un centro carcelario.

De acuerdo con lo manifestado por las mujeres, desde el pasado martes se les estaría impidiendo acceder a sus celdas, manteniéndolas en el patio durante todo el día, como una forma de represión por atreverse a denunciar las condiciones indignas bajo las que están sometidas en el sitio de reclusión.

Las pésima calidad de la comida y los horarios de alimentación, la deficiente atención médica y el hacinamiento, mantienen a las reclusas del patio 9 en lo que califican como un "cementerio de vivos", subrayando que independientemente de los delitos que pueden haber cometido son seres humanos y no animales y como tal pueden exigir sus derechos.

En el texto de la denuncia se pone de manifiesto la preocupación por la suerte de la vocera de las reclusas Cristina Barrantes, quien habria sido conducida el 26 de marzo a la OT, junto a sus compañeras Jeniffer Villota y Rubí Cuervo, donde habrían sido golpeadas y torturadas con descargas eléctricas por la guardia.

Compartimos el texto enviado desde la cárcel "El buen pastor" y la declaración de dos de las denunciantes.

###### 28 De Marzo de 2016

**S.O.S MUJERES RECLUSAS DE BUEN PASTOR BOGOTÁ**

El 22 de marzo en el patio 9 de la Reclusión de Mujeres Buen Pastor Bogotá , en horas de la mañana las Guardianas del INPEC impiden  a las reclusas estar en las  celdas  como medida represiva ante el decomiso de un teléfono móvil a una de ellas, es decir , aplican un régimen penitenciario a un establecimiento carcelario, transgrediendo la ley,  obligándolas a estar todo el día en el centro del patio sin poder entrar y salir de las celdas. Decidimos entonces como protesta no recibir alimentos.

Las cuadros de mando en cabeza de la Capitán Gertrudis y la Cabo Ivonne Cruz de forma arbitraria deciden llevar a calabozo a la Vocera Cristina Barrantes , lo cual trata de ser impedido por todas nosotras de manera no violenta. Ante los hechos hace presencia en el patio la Directora Ana Sofía Hidalgo, quien argumenta de forma mentirosa ante la Dirección General INPEC, que estamos amotinadas y requiere la presencia de los especialistas en tratos degradantes CORES.

Además castiga a todas en el patio sin dejarnos ingresar a las celdas y amenaza con que este patio será de ahora en adelante un patio con régimen penitenciario,  lo cual NO está permitido en ley. Las 3 compañeras que están en calabozos desde ese día hasta la fecha , fueron golpeadas en la noche del 26 de marzo por denunciar ante medios de comunicación lo que está sucediendonos.

La interna compañera Claudia Barrantes por ser nuestra vocera recibió descargas eléctricas método de tortura llamado picana o taiser, igual a Jeniffer Villota y Rubí Cuervo.

Solicitamos a los entes de control, organizaciones de DDHH nos ayuden con los abusos que las guardianas del INPEC en contra nuestra junto a su cúpula de mandos Capitán Gertrudis , Capitan Criz ,Cabo Ivonne Cruz empezando por la Directora. Ya hace un par de meses salió a la luz pública los abusos realizados contra las internas del patio 2 sin que hasta la fecha las investigaciones terminen en algo.

Solicitamos no vayamos a ser trasladadas , estigmatizadas u objeto de agresiones y provocaciones por parte del INPEC por atrevemos a denunciar y EXIGIR RESPETO A NUESTRA  DIGNIDAD DE MUJER .

ATT: RECLUSAS BUEN PASTOR BOGOTÁ - PATIO 9

<iframe src="http://co.ivoox.com/es/player_ej_10956087_2_1.html?data=kpWml5uUfJihhpywj5WYaZS1lJ2ah5yncZOhhpywj5WRaZi3jpWah5yncbPZxNHi1cbXb8XZz9rbxc7FsozVw9rg0diPvYzo0Nfh19fFt4zZz5DQh6iXaaKl08jSzpCJdpO5zZDP18qRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
