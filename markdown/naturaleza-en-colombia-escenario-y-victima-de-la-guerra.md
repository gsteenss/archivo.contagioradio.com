Title: Naturaleza en Colombia, escenario y víctima de la guerra
Date: 2020-02-07 09:20
Author: CtgAdm
Category: Ambiente, Nacional
Tags: colombia, naturaleza
Slug: naturaleza-en-colombia-escenario-y-victima-de-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/GAB0856.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/rosa-maria-mateuz-sobre-declaracion-del-ambiente-como_md_47431097_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Rosa Maria Mateuz | Abogacada (CAJAR)

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Por iniciativa de la magistrada **Belkis Izquierdo** la **Jurisdicción Especial para la Paz** (JEP) reconoció a la naturaleza como víctima del conflicto armado en Colombia, destacando los hechos de violencia en los **territorios indígenas y afros** de regiones como Tumaco, Barbacoas y Ricaurte en Nariño. (Le puede interesar: <https://archivo.contagioradio.com/ni-ecopetrol-ni-contratistas-atienden-derrame-de-petroleo-en-cuerpo-hidrico-de-barrancabermeja/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Izquierdo es integrante de la JEP, coordina la Comisión Étnica y Racial además pertenece al pueblo Arhuaco, y lleva casi dos años trabajando por la defensa de la comunidades en estos tres territorios, acción representada en el caso 002**, donde no solo señalan las acciones violentas cometidos por las FARC-EP y la Fuerza Pública a la población sino hace una acotación a daños socio-ambientales y territoriales en estos municipios**.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Explotación de combustible fósil uno de los mayores victimarios de la naturaleza

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la abogada Rosa María Mateus integrante del Colectivo de Abogados José Alvear Restrepo (CAJAR) este anuncio de la JEP permitirá de alguna manera que *"e**l  país se entere de lo que está pasando** y cómo las comunidades se han visto afectadas por las agresiones a sus fuentes naturales"* .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma añadió que como defensora por años de las comunidades víctimas de crímenes ambientales, *"esperaríamos que se escuche de manera efectiva, real y transparente a las comunidades; porque ellas ya han señalado cuáles son las medidas de reparación"*, y agregó el papel importante que deben cumplen los indígenas y afros habitantes de estos 3 municipios, *"**ellos son los únicos testigos de las atrocidades ambientales en los territorios, por eso son ellos los que deben dar testimonio ante la JEP**"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicional Mateus afirmó que debe haber voluntad de reparación y cambios por parte de los responsables de las afectaciones ambientales, *"los diferentes actores tienen que tener voluntad de cumplir con las medidas de reparación, y la JEP tienen que hacerse estudios de lo que significarían las medidas impuestas a los agresores"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Son muchas las agresiones a la naturaleza en el marco del conflicto armado

<!-- /wp:heading -->

<!-- wp:paragraph -->

El principal detonante de violencia climática en Colombia es la explotación de combustibles fósiles, sin embargo acciones como voladura de oleoductos, bombardeos, fumigaciones, devastación de la selva y contaminación de los ecosistemas, han sido detonantes de un sin número de agresiones a la naturaleza. (Le puede interesar: <https://archivo.contagioradio.com/bosque-de-palma-de-cera-simbolo-nacional-nuevamente-en-riesgo/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Mateus este es paso más en el reconocimiento de la naturaleza como sujeto de derechos, y de reconocer que "nosotros no somos nada sin la naturaleza"; por último agregó que la crisis climática que se presenta en el mundo no ajena a nuestro país y que *"es momento de acabar con nuestras cosmovisiones, nuestras formas de abordar el mundo y consumir*; *el primer* *paso es recociendo que somos nosotros victimarios de la naturaleza y que es momento de reparla"*

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
