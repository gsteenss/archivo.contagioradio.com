Title: Incertidumbre en mesa ELN-Gobierno con la presidencia de Iván Duque
Date: 2018-06-26 16:32
Category: DDHH, Política
Tags: ELN, Gobierno, Iván Duque, Mesa de La Habana
Slug: se-sentara-duque-a-dialogar-en-la-mesa-con-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/ivan-duque-y-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ELN] 

###### [26 Jun 2018] 

Hasta el próximo 2 de julio se retomarán los diálogos de paz entre el ELN y el gobierno Nacional, de acuerdo con el analista Víctor de Currea,** serán el escenario para comprobar si el presidente  electo Iván Duque tiene voluntad de seguir con el proceso** **de paz**.

Para el analista, el país está en una fase de transición que tiene grandes riesgos “por un lado se confirma el temor de algunos sectores de la sociedad que la paz es una política de gobierno y no una política de Estado”, y por el otro, de Currea afirmó que quienes **votaron por Duque respaldaron un modelo de paz que no busca el desarme de las guerrillas por vías dialogadas.**

### **¿Qué hará Duque con la mesa de diálogos?** 

Ante ese panorama, el analista señala que hay diferentes formas de hacer que el proceso fracase. **La primera de ellas tiene que ver con colocarle demandas al interlocutor imposibles de cumplir**, como lo ha hecho Duque solicitándole al ELN que reúna a sus tropas en puntos específicos del país. Esta petición para de Currea no solo “reventaría a la mesa”, sino que además, envía un mensaje de radicalización e intento por renegociar los mínimos de este proceso en los medios.

“Hay un tanteo por parte de Duque de las fuerzas de la mesa, no creo que vaya a romper la mesa en la primera ronda de cambio, pero tampoco la dejará tal cual cuando llegue la segunda rueda” afirma de Currea. (Le puede interesar:["Las condiciones para continuar la mesa con el ELN"](https://archivo.contagioradio.com/las-condiciones-para-continuar-la-mesa-de-dialogos-con-el-eln/))

La otra forma de hacer que la mesa vaya decayendo tiene que ver con que Duque mantenga la mesa, pero congelada y sin trabajo, generando un desgaste político y procedimental. Finalmente estaría la solución militar, que buscaría ablandar al ELN, que para el analista sería **“tonto” debido a la guerrilla a estado inmersa en condiciones de guerra más de 50 años y podría retornar el accionar violento**.

Pese a estos escenarios, el ELN manifestó a través de un comunicado de prensa, que se mantendrán en la mesa de conversaciones y señalaron que esperan que el aplazamiento de la fecha de inicio del sexto ciclo de conversaciones sea porque existe una voluntad por continuar el trabajo en La Habana.

<iframe id="audio_26744247" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26744247_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
