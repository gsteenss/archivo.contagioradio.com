Title: Comunidad de San José de Apartadó atemorizada por presencia de grupo paramilitar
Date: 2016-09-16 16:40
Category: DDHH
Tags: Comunidad de Paz de San José de Apartadó, paramilitares
Slug: comunidad-de-san-jose-de-apartado-atemorizada-por-presencia-de-grupo-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/san-jose.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 de Sept 2016] 

Varias **ráfagas de fusil fueron escuchadas por los habitantes de la comunidad de San José de Apartodó, el pasado 15 de septiembre sobre las 9:30 de la noche.** De acuerdo con la comunidad este hecho ratificaría por un lado las amenazas de grupos paramilitares como las Autodefensas Gaitanistas Colombianas que han venido haciendo presencia en el territorio y por el otro, la poca acción de las Fuerzas Militares.

Los habitantes comentan que **los disparos se realizaron muy cerca a la estación de policía, sin que esta o las Fuerzas Militares reaccionaran**, a pesar del largo periodo de tiempo durante el que se escucharon las ráfagas. De igual forma, los pobladores han denunciado que [hay evidencias de presencia paramilitar en el lugar](https://archivo.contagioradio.com/familias-confinadas-en-sus-fincas-por-ataques-paramilitares-en-guacamayas/) y no se ha realizado ninguna investigación o verificación por parte de las autoridades.

De acuerdo con Fredy Vidal, presidente de la Junta de Acción Comunal de San José de Apartadó, el arribo de este grupo paramilitar a la comunidad fue un anuncio que las Autodefensas Gaitanistas de Colombia le hicieron a los campesinos de la región, **en donde advirtieron que “llegaron para quedarse”.**

Esta zona del país ha tenido presencia histórica de todos los actores del conflicto armado, quienes en múltiples acciones [han violado los derechos de la comunidad de San José de Apartado](https://archivo.contagioradio.com/cuando-podra-vivir-en-paz-la-comunidad-de-san-jose-de-apartado/). Anteriormente el grupo armado ingreso a San José de Apartado, **el 17 de abril, en donde pintaron mensajes alusivos a las AGC**. Posteriormente el grupo hizo presencia en la vereda el Porvenir, el 7 de septiembre y el 10 de septiembre en la vereda La Hoz.

La comunidad pone en alerta al Estado Colombiano, frente a la gravedad de los hechos ocurridos y la evidencia de la permanencia de los grupos paramilitares en esta zona, para que se tomen medidas, ya que **actos como estos pone en inminente riesgo la vida, dignidad e integridad de los líderes y defensores de derechos humanos que habitan el corregimiento.**

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
