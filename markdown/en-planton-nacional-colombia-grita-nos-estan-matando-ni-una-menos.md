Title: En Plantón Nacional, Colombia grita ¡Nos están matando, Ni Una Menos!
Date: 2017-04-27 12:50
Category: Mujer, Nacional
Tags: feminismo, genero, mujeres
Slug: en-planton-nacional-colombia-grita-nos-estan-matando-ni-una-menos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/18121974_10155280151148044_3601260956402662099_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [27Abr. 2017] 

A raíz de las recientes denuncias sobre violencia contra las mujeres y niñas en Colombia, cientos de mujeres y hombres se reunirán en las fiscalías de diversas ciudades y municipios de todo el país a las 4 de la tarde **para romper el silencio y gritar “No más Feminicidios”.** Según cifras de Medicina Legal en lo que va corrido de 2017, son 204 casos en los que las mujeres han sido las víctimas.

Este Plantón Nacional ha sido convocado a través de redes sociales para exigir justicia por todas las mujeres y niñas como Sara, Yuliana, Edtih, Claudia, Rosa Elvira y muchas mujeres más víctimas del “machismo, odio y misoginia que hay en Colombia” asegura **Laura Torres quien es integrante de la Mesa por el derecho de las mujeres a una vida libre de violencias.**

“Es momento de que sentemos nuestra voz de protesta ante estos casos que tienen día a día una protagonista distinta que es una mujer o una niña menos en este país, que básicamente es resultado de la inoperancia de las instituciones y de la indiferencia de la sociedad” dijo Torres. Le puede interesar: [Nos siguen matando aunque ya no sea noticia](https://archivo.contagioradio.com/nos-siguen-matando-aunque-ya-no-sea-noticia/)

Otra de las exigencias que se escucharán en este plantón estarán encaminadas a pedir justicia y a las instituciones del Estado que **se garantice a todas las mujeres el derecho a una vida libre de violencias. **Le puede interesar: [Comisarías, jueces y fiscales desestiman denuncias de mujeres](https://archivo.contagioradio.com/comisarias-jueces-y-fiscales-desestiman-denuncias-de-mujeres/)

**“Exigimos a todas las instituciones del Estado que se garantice la implementación de la Ley Rosa Elvira Cely y de la Ley 1257** porque de nada nos sirve tener unas leyes en donde se muestra un avance en garantía de derechos pero que en la realidad es otra y las cifras demuestran que no están haciendo lo que deben hacer” aseveró Torres.

**Los plantones se realizarán en más de 14 ciudades** dentro de las que se encuentra Popayán, Barrancabermeja, Cali, Santa Marta, Bogotá entre otras, donde se ha convocado a apoyar con pancartas, pitos y música. Le puede interesar: [“El Estado es responsable del feminicidio de Claudia”](https://archivo.contagioradio.com/el-estado-es-responsable-de-lo-que-le-sucedio-a-claudia/)

“La idea es que todos, hombres, niños, niñas puedan ir a las instituciones a exigir que nos dejen de maltratar, de matar. **Que necesitamos una vida libre de violencias. Esto ya no es cuestión de feminismo sino de sociedad”** recalcó Torres.

### **El plantón en cifras.** 

Según el Instituto Colombiano de Bienestar Familiar en lo que va corrido del 2017 han recibido 2500 denuncias por abuso contra menores y Medicina Legal aseguró que entre enero de 2014 y diciembre de 2016 cerca **de 30.503 niñas han sido víctimas de abusos sexuales.**

<iframe id="audio_18383176" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18383176_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
