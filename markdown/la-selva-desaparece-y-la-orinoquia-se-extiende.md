Title: La selva desaparece y la Orinoquía se extiende
Date: 2015-03-13 12:26
Author: CtgAdm
Category: Ambiente y Sociedad, Opinion
Tags: Plan Nacional de Desarrollo, Sistema de Información Ambiental Territorial, Vaupes
Slug: la-selva-desaparece-y-la-orinoquia-se-extiende
Status: published

###### Foto: U.Nacional 

#### Por [**[Margarita Flórez](https://archivo.contagioradio.com/margarita-florez/) - Directora de [Ambiente y Sociedad](http://www.ambienteysociedad.org.co/es/inicio/)**] 

Una de los temas que causa mayor sorpresa cuando se lee el Plan Nacional de Desarrollo es que dentro de la división regional, la cual se advierte en el texto no contraviene la descentralización, y las aspiraciones regionales propias, es que los departamentos de Guainía y Vaupés ingresan de manera oficial a la zona Llanos. Allí se explica cuales son las causas de la falta de desarrollo, y se señala que el crecimiento verde mediante una gestión integra permitirá que se combinen biomas, ecosistemas, tipos de desarrollo, culturas indígenas población prevalente en la zona junto a empresarios del agro, en la gran planicie como se nos explica.

Es decir que las informaciones que contiene los sistemas de información SIAC –AC, Sistema de Información Ambiental Territorial de la amazonia colombiana del Instituto Sinchi parecen haber perdido su actualidad, y tendrían que renovarse para adaptarse a la nueva caracterización que no se sabe si fue consultada con los especialista amazónicos, y concuerda parcialmente con la región ofrecida como objeto de conservación para los diversos proyectos de mitigación en curso en el país, que se negocian para la amazonia colombiana, y se entiende contribuyen a restaurar suelo degradados y deforestados en ese ecosistema.[Ver mapa aquí](http://geosemantica.siatac.co/collections/map_viewer.aspx?id=1732fd1a-0711-4c2b-919b-c5f153ca1841&c=2)

A estas horas todavía tampoco se conocen los criterios científicos para definir estas zonas, ni las de altillanura ni las nuevas incluidas en el PND con criterios que parecen corresponder a áreas para distribución de regalías antes que obedecer a criterios ecosistémicos, y culturales. El proceso de intervención se liga a la agroindustria ya probado[ ]{.Apple-converted-space}para el caso de la altillanura y pareciera es una de las actividades previstas para una de las zonas más conservadas de nuestro país.

Todo lo anterior coexiste espacialmente con el nuevo anuncio gubernamental de impulsar un territorio trinacional de conservación entre Venezuela, Brasil, y Colombia, iniciativa que también será lanzada en la COP21 como contribución regional. Si vamos de anuncio en anuncio sobre zonas de conservación, que se traslapan con las que transforman su vocación, y ceden al[ ]{.Apple-converted-space}ordenamiento desde los sectores económicos extractivos y de los negocios del agro no sabemos a ciencia cierta cuál es la racionalidad de tratar de forzar actividades anatagónicas, y de convencer al mundo que estos dobles usos son viables.

Sobre este punto anotamos que el proceso de levantamiento de las reservas forestales para fines de agroindustria o de minería sigue su camino de manera acelerada, de tal suerte que ya uno no sabe cuál es la tierra que se titulará en el próximo futuro. Y el ordenamiento del territorio se construye y determina desde el desarrollo, copiando modelos con enormes impactos como el caso del cerrado brasileño, los cuales se colocan dentro del imaginario como las metas a las cuales debemos llegar.
