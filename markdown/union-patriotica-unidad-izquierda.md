Title: Se teje de nuevo la unidad de la izquierda en congreso de la Unión Patriótica
Date: 2017-06-23 16:07
Category: Nacional, Política
Tags: Claudia López, ELN, FARC, Gustavo Petro, Unión Patriótica
Slug: union-patriotica-unidad-izquierda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/union-patriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unión Patriótica] 

###### [23 Jun 2017]

Durante la instalación del sexto congreso de la Unión Patriótica se pudo evidenciar que hay una inquietud bastante grande por lograr un encuentro en lo programático, así como la definición expresa de **Gustavo Petro y Claudia López de renunciar a las candidaturas en aras de una convergencia amplia y programática**.

Al congreso asistieron precandidatos presidenciales, Piedad Cordoba, Gustavo Petro, Clara López, Claudia López, Aida Avella, Carlos Caicedo y sectores políticos como Marcha Patriótica, Voces de Paz y un representante de las FARC. No asistieron Jorge Robledo, Sergio Jaramillo y Humberto de la Calle. [Lea también: La UP quiere demostrar que hay otra forma de gobiernar](https://archivo.contagioradio.com/la-union-patriotica-quiere-demostrar-que-hay-otra-forma-de-gobernar/)

Según Pavel Santodomingo, la Unión Patriótica siempre ha apostado por la unidad de los sectores progresistas y que buscan la construcción de la paz y el mensaje fue contundente en torno a la necesidad de unir las fuerzas en torno a la implementación del proceso de paz, no solamente de las FARC sino también del ELN que envió un saludo al espacio.

“Hay una necesidad de victoria” en torno a la necesidad de no retroceder en lo que se ha avanzado del proceso de paz y hay un  propósito de ganar, no solamente en el senado o en la cámara sino también y “sobre todo en la presidencia de la república” de **cara a un pacto de élites representado en la candidatura de Vargas Lleras y la alianza de Uribe con Pastrana.**

Fue tan contundente el mensaje que algunos plantearon la posibilidad de renunciar a las candidaturas, pensando en la manera de conquistar el sentir de la mayoría de las personas en Colombia. Según santodomingo “es necesario ampliar las convergencias hacia otros sectores que no son necesariamente de izquierda” pero que están en la línea de defender los acuerdos y avanzar en ello.

Explícitamente Claudia López y Gustavo Petro plantearon renunciar a sus candidaturas en pro de una alianza mucho mayor, una convergencia sobre unas bases programáticas que es necesario comenzar a construir.

### **El aporte de sectores juveniles es clave** 

Para las personas presentes, la clave o la apuesta es encontrar acuerdos programáticos o rutas a seguir “si tenemos una base programática común las caras y los nombres y los rostros van a tener que ceder” y en ello la UP tendrá que poner de presente otros liderazgos juveniles y una apuesta generacional seria que dependa de propósitos colectivos.

“Nosotros como un sector importante de la política en Colombia tenemos que basarnos en apuestas, ideas” todo ello en la vía de lograr que se rompan los estereotipos de la izquierda que impiden otros ejercicios democráticos “los egos hacen daño” expresa Santodomingo quien sería uno de los nuevos rostros en la política de la UP junto con Alejandra Gaviria, ambos hijos de víctimas de la UP.

<iframe id="audio_19440057" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_19440057_4_1.html?c1=ff6600"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
