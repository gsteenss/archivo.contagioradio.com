Title: Mi voz cuenta, informe del clima escolar LGBT en Colombia
Date: 2016-11-01 13:36
Category: LGBTI, Nacional
Tags: Derechos, gais, gays, Heterosexuales, homosexuales, Lesbianas, LGBTI, paz, Sergio Urrego
Slug: mi-voz-cuenta-informe-del-clima-escolar-lgbt-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/mi-voz-cuenta-lgbti.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [1 Nov. de 2016] 

**Cerca del 67 % de un total de 500 estudiantes que fueron encuestados, manifestaron haberse sentido inseguros en su colegio debido a su orientación sexual** y de este porcentaje un 23 % faltó a clase, siendo la causa principal decir que no se sentían cómodos o por el contrario desprotegidos en la institución. Así lo asegura una **encuesta de clima escolar LGBT en Colombia, llamada “Mi voz cuenta: experiencia de adolescentes y jóvenes lesbianas, gais, bisexuales y trans en el ámbito escolar”**.

Colombia Diversa y Sentiido fueron las organizaciones que se reunieron para realizar dicha encuesta retomando un ejercicio que es realizado por las escuelas en Estados Unidos. Y al cual fueron invitados a hacer parte.

La dinámica utilizada para esta encuesta fue un cuestionario en internet y las redes sociales para que pudiese llegar a estudiantes que se reconocen como LGBT y así pudieran participar diligenciándola.

El ejercicio de análisis de dicha encuesta, arrojó diversos resultados que dieron origen a apartados en temas como **inseguridad en su colegio, comentarios discriminatorios, formas de violencia (verbal, ciberacoso, física, burla o bullying), apoyo de los hogares y las escuelas y consecuencias de las diversas formas de violencia de las que son víctimas la comunidad LGBT.**

Emily Quevedo, pedagoga de Colombia Diversa, aseguró que gracias al uso de la encuesta pudieron evidenciar que muchos integrantes de la comunidad LGBT, que están en las escuelas afirman que han determinado no asistir más al colegio o que no encuentran confianza al estar en la escuela.

**Panorama desalentador**

Según la pedagoga de Colombia Diversa, **el panorama que viven niños, niñas, jóvenes y adolescentes es “desalentador” porque “sus derechos no están siendo garantizados en las escuelas por diversas razones”.**

Según Quevedo, dos son las grandes razones para que la comunidad LGBT que está en las escuelas, no pueda sentirse acogido o decir abiertamente que tipo de identidad sexual o de género tienen “una es lo conservadora que es nuestra sociedad y la segunda es que existe un amplio desconocimiento en el país acerca de cuáles son los derechos que hay que garantizar”.

Además agrega, que “**como cultura y como sociedad colombiana no conversamos sobre el tema lo que hace que no haya un nuevo lenguaje cotidiano común. Estamos muy desinformados y tenemos muchos mitos y especulaciones en torno al tema LGBT”**. Le puede interesar:** **[¿Es necesaria la comunidad Lgbti para generar progreso?](https://archivo.contagioradio.com/es-necesaria-la-comunidad-lgbti-para-generar-progreso/)

**Escuela como espacio de construcción**

Contrario a lo que se esperaría, la escuela es uno de los lugares donde no existen políticas estructurales fuertes que acompañen a los maestros y a todo el personal adulto para trabajar el tema.

Adicional, Quevedo agrega que no hay unas políticas de formación a quienes se convertirán en futuros docentes “las universidades que forman maestros, psicólogos, trabajadores sociales, no tienen dentro de sus currículos de formación clases que puedan darle instrumentos a la gente para saber cómo abordar el tema”.

**Derechos para todos y todas**

El Ministerio de Educación y el Congreso de la República crearon en 2013 **la ley 1620 y el Decreto reglamentario 1965** el cual otorga responsabilidades a las escuelas, a las Secretarías de Educación y al mismo Ministerio de Educación para que se generen acciones de prevención, de atención  y seguimiento a todas las discriminaciones.

Al respecto de este tema Emily señaló que **“se ha tejido un manto de desinformación frente a esta ley creyendo que es solo para personas de la comunidad LGBTI en la escuela y no es así.** La ley habla de la discriminación para personas LGBT pero también para personas que pertenecen a grupos étnicos, a las mujeres, a las personas con discapacidad que son “minorías” que siempre están siendo señaladas en la escuela, porque son distintas a lo que conocemos como la media de la sociedad.

**Sociedades más respetuosas**

La integrante de Colombia Diversa puntualizó que el trabajo por hacer luego de conocida esta encuesta es amplio, pero que podría comenzarse por el respeto “**si tú eres criado en casa y la sociedad te dice que debes respetar al otro en su individualidad y que cada uno funciona con unos derechos y unas libertades que nadie puede vulnerar, independientemente de lo que alguien piense de ellas, la cuestión se convierte en más sencilla”. **Le puede interesar: [2015 el año más violento contra la comunidad Lgbt](https://archivo.contagioradio.com/2015-el-ano-mas-violento-contra-la-comunidad-lgbt/)

###### Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU)
