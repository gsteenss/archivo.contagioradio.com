Title: Denuncian asesinato y desaparición de integrantes de Zona de Reserva Campesina Perla Amazónica
Date: 2019-02-26 12:19
Category: Comunidad, Nacional
Tags: Asesinatos de campesinos, Desaparición forzada, Zona de reserva Campesina del Putumayo
Slug: denuncian-asesinato-y-desaparicion-de-habitantes-de-la-zona-de-reserva-campesina-perla-amazonica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Zona-de-Reserva-Campesina-Perla-Amazónica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 26 Feb 2019

Hacia las 6: 00 pm del pasado 23 de febrero, en el centro urbano de Puerto Asís, Putumayo se produjo la desaparición de **Milena García y su esposo Darwin Reyes,  beneficiarios del Programa de Sustitución voluntaria de cultivos de uso ilícito** y afiliados a la comunidad de San Salvador, Zona de Reserva Campesina Perla Amazónica,  (ZRCPA).

Los dos habitantes de la ZRCPA, fueron sacados de la zona urbana y trasladados hacia la zona conocida como Tres Bocanas o Km9, zona rural de Puerto Asís, donde habrían sido asesinados y sus cuerpos arrojados al río Putumayo, el cuerpo de Milena García fue hallado el domingo 24 en la comunidad de Peñasora a orillas del afluente, mientras que los familiares de Darwin continúan su búsqueda, temiendo que al igual que Milena, haya sido asesinado.

**El lugar donde desaparecieron los dos habitantes de la ZRCPA ha sido históricamente un corredor con presencia de estructuras paramilitares**, que a su vez cuenta con operaciones de control de la Fuerza Pública.[(Lea también Asesinan a líder social de la Zona de Reserva Campesina de la Perla Amazónica, en Putumayo). ](https://archivo.contagioradio.com/asesinan-a-lider-social-de-la-zona-de-reserva-de-la-perla-amazonica-en-putumayo/)

A pesar de las continuas denuncias por parte de organizaciones sociales y defensoras de DD.HH, quienes exigen una respuesta eficaz por parte del Estado y garantizar la vida de los habitantes de las comunidades rurales de Putumayo, estas no se han visto evidenciadas en el terreno.

###### Reciba toda la información de Contagio Radio en [[su correo]
