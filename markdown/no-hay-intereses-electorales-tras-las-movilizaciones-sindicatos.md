Title: No hay intereses electorales tras las movilizaciones: Sindicatos
Date: 2017-05-16 12:20
Category: Movilización, Nacional, Uncategorized
Tags: Annelen Nicut, CUT, paro trabajadores
Slug: no-hay-intereses-electorales-tras-las-movilizaciones-sindicatos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/parotrabajadores-e1494955113483.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 mayo 2017] 

Ante las especulaciones de algunos sectores de la sociedad, quienes denuncian que el paro de trabajadores tienen intereses políticos y electorales, los sindicatos han manifestado que **la movilización social tiene el interés político de que se cumplan los acuerdos que ha hecho el gobierno.**

El paro de trabajadores estatales continúa con la toma a Bogotá en que participan el Comando Unitario de Paro conformado por sindicatos como la CUT, la CTC y la CGT y delegaciones de todo el país. En las calles le **exigen al gobierno regular la carrera administrativa y aumentar los salarios** de los trabajadores del sector público. Le puede interesar: ["Inicia el paro cívico cultural en Buenaventura"](https://archivo.contagioradio.com/inicia-el-paro-civico-cultural-de-buenaventura/)

### **No hay "interés electoral" detrás de la ola de protestas y paros en Colombia** 

Según Fabio Arias, miembro de la directiva general de la CUT, "los intereses políticos que tenemos con el paro son que el gobierno nos cumpla los acuerdos que ya se habían concebido". Para los trabajadores, los intereses electorales y políticos no hacen daño puesto que cualquiera que sea el gobierno de turno, **debe cumplirle a los trabajadores estatales los convenios que acuerda cada dos años.**

Las solicitudes y los reclamos que tienen los trabajadores estatales frente al Estado, debe hacerse cada dos años por medio de una negociación colectiva. **El último acuerdo se hizo en 2015 y no se cumplió.** Arias afirma que "las nominas, el clientelismo y la politiqueria no pueden ser las reglas de juego con las que se debata la situación laboral del país". Le puede interesar: ["Gobierno hace oídos sordos al paro en Chocó"](https://archivo.contagioradio.com/gobierno-hace-oidos-sordos-al-paro-en-choco/)

Frente a los cuestionamientos sobre los intereses electorales, Arias manifestó que **"no se está impulsando ninguna campaña política con las movilizaciones"**. Sin embrago, le conviene más a los trabajadores un gobierno de turno de corte progresista que se comprometa más a cumplir con los acuerdos.

###### Reciba toda la información de Contagio Radio en [[su correo]
