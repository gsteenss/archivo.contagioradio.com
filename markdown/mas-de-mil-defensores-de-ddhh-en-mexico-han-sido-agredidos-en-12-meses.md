Title: Más de mil defensores de DDHH en México han sido agredidos en 12 meses
Date: 2016-08-24 14:17
Category: El mundo, Nacional
Tags: Derechos Humanos, Enrique Peña Nieto, mexico, Represión
Slug: mas-de-mil-defensores-de-ddhh-en-mexico-han-sido-agredidos-en-12-meses
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Derechos-humanos-Mexico-e1472065664418.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Proceso.mx 

###### [24 Ago 2016]

‘Defender los derechos humanos en México: La normalización de la represión política’, es el más reciente informe donde se visibiliza los riesgos, amenazas y asesinatos que padecen las defensoras y defensores de derechos humanos, gracias al fortalecimiento de una política de represión por parte del gobierno de Enrique Peña Nieto, que ha generado que por día se registren cerca de **3 agresiones contra defensores de derechos humanos en ese país.**

“**El gobierno actual focalizó la violencia política hacia los colectivos, organizaciones sociales, populares y hacia los defensores de derechos humanos”,** dice Alejandro Cerezo, explicando el por qué del nombre del informe, donde se registran cerca de 1000 casos de violaciones a los derechos humanos de los defensores tales como: hostigamiento, vigilancia y persecución, amenazas, desaparición forzada, ejecuciones extrajudiciales, detención arbitrarias, prisión política y tortura, durante 12 meses.

**El Estado de Guerrero, Michoacán, Oaxaca, y la capital del distrito Federal** son los lugares donde los índices de violencia en contra de defensores de derechos humanos son mayores, debido a los altos niveles de impunidad que en México representan el 96% de los casos.

Luis Raúl González, vocero de la Comisión Nacional de los Derechos Humano, CNDH, había asegurado hace algunos meses que “el incremento se debe a los altos índices de impunidad registrados en nuestro país en los últimos años, así como a **la omisión de los tres niveles de gobierno de establecer protocolos de investigación** en los que sea obligatorio y prioritario identificar a las personas defensoras víctimas de delitos”.

De acuerdo con Cerezo, esta situación se da porque en lugar de haber una política pública integral de protección a los líderes sociales, la política del gobierno de Peña Nieto es la represión, explica el integrante del Comité Cerezo. “**La permisión y la firma de contratos entre el Estado mexicano y las empresas** es lo que sigue permitiendo las violaciones a los defensores de derechos humanos “.

En ese sentido, la CNDH ha recomendado al gobierno que **la Procuraduría General de la República, los secretarios de la Defensa Nacional y de Marina, los gobernadores, comisionado Nacional de Seguridad y  los** **fiscales Generale**s de Justicia, atiendan integralmente toda intimidación, agresión y afectación de cualquier tipo que atente contra la labor de defensa de los derechos humanos.

<iframe src="http://co.ivoox.com/es/player_ej_12653361_2_1.html?data=kpejl5iXepKhhpywj5aaaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5yncaLgxs_O0MnWs4y3xtfS3NSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
