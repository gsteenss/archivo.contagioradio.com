Title: La ONU advierte que la situación de los pueblos indígenas en el mundo es peor que hace 10 años
Date: 2017-08-10 12:19
Category: DDHH, El mundo
Tags: Día internacional de los Pueblos Indígenas
Slug: el-mundo-sigue-rezagado-con-respecto-a-los-derechos-de-los-indigenas-10-anos-despues-de-su-declaracion-historica-advierten-expertos-de-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Nasa-e1502385039236.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vice] 

###### [10 Ago 2017] 

En medio de un complejo panorama a nivel mundial, este 9 de agosto se conmemoró el Día Internacional de los Pueblo indígenas. Un día en el que la ONU advirtió sobre la grave situación en la que se encuentran estas comunidades, amenazadas especialmente por la pérdida de sus tierras y de los derechos sobre los recursos naturales que, entre los gobierno y multinacionales les han arrebatado; además del racismo, la discriminación y la desigualdad en el acceso a los servicios básicos, incluidas la sanidad y la educación.

[[[Y es que según cifras,  los 370 millones de indígenas, que actualmente viven en 90 países, representan menos del 5% de la población mundial, pero constituyen]]]**[[[**el 15% de los más empobrecidos y la tercera parte de los 900 millones de personas que viven en condición de indigencia**]]]**[[[en las zonas rurales.]]]

En ese marco, la ONU ha señalado que los pueblos indígenas se enfrentan a mayores problemas y violaciones de derechos que hace 10 años, pese a que existe una declaración con la que los países se han comprometido a garantizar su vida. “Los pueblos indígenas siguen sufriendo. Cuando se dispone de datos estadísticos, se aprecia claramente que están excluidos en todos los aspectos, enfrentándose a niveles de pobreza desproporcionadamente mayores, una menor esperanza de vida y peores resultados educativos”, dice la ONU.

Teniendo en cuenta esa situación se ha instado a los Estados a “**transformar las palabras en hechos para acabar con la discriminación, la exclusión y la falta de protección que la tasa de asesinatos cada vez mayor de defensores de los derechos humanos”**. A su vez, para que se logren cambios estructurales, se ha recomendado reconocer e impulsar “las formas de gobierno y representación de los pueblos indígenas para establecer un diálogo constructivo y el compromiso con las autoridades nacionales e internacionales, los cargos públicos y el sector privado”.

### La situación en Colombia 

En el país la situación sigue siendo compleja. El gobierno nacional ha detectado que de los 102 pueblos indígenas, 32 se encuentran en grave peligro de exterminio especialmente[[[ por hambre, como lo ha manifestado las Naciones Unidas. Según cifras de ese organismo,]]]**[[[**el 70% de niños y niñas indígenas del país sufren de desnutrición crónica**]]]**[[[, específicamente de regiones como Atlántico, Pacífico, Orinoquía y Amazonas.]]]

[[[En Colombia la mitad de los]]]**[[[**cerca de 1,37 millones de indígenas colombianos viven en la 'pobreza estructural'.** **Por ejemplo,** **en la Orinoquía,** **uno lugares más afectados,** **7 de cada 10 niños y niñas indígenas del Casanare **]]]**[[[sufre de desnutrición crónica, y en la Guajira ya van más de cinco mil infantes fallecidos por distintas enfermedades producto de factores como la presencia empresas mineras, el olvido estatal y la corrupción, como lo han denunciado los lideres indígenas de la zona.]]]

No obstante el nuevo panorama del posconflicto brinda nuevas posibilidades para estas comunidades. La Organización Nacional Indígena de Colombia, ONIC, conmemora este 9 de agosto de cara a la construcción de paz. “Para los pueblos indígenas «paz» no se trata simplemente de la ausencia de conflicto. Vivir en paz para los pueblos indígenas de Colombia significa reivindicar el derecho que ellos reclaman de permanecer en sus territorios originarios, de mantener vivas sus manifestaciones culturales, la lengua y sus tradiciones ancestrales”, aseguró Hélène Papper, Directora del Centro de Información de Naciones Unidas (CINU) en Colombia.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
