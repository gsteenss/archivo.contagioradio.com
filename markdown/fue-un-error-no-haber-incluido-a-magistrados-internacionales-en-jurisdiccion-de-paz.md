Title: Fue un “error no haber incluido a magistrados internacionales” en jurisdicción de paz
Date: 2016-11-15 11:50
Category: Entrevistas, Judicial
Slug: fue-un-error-no-haber-incluido-a-magistrados-internacionales-en-jurisdiccion-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Enrique-Santiago.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [15 Nov 2016]

El abogado Enrique Santiago, asesor de la mesa de conversaciones de paz entre el gobierno y las FARC hizo un balance del nuevo acuerdo en torno a la Jurisdicción de Paz y aseguró que fue un error no haber incluido a **magistrados internacionales, a pesar de que Colombia hace parte de tratados internacionales la avalan**. Sin embargo recordó que está vigente la figura Amicus Curiae que podrá ser solicitada por las partes.

Según Enrique Santiago, también **quedaron incluidos en el acuerdo cerca del 67% de las precisiones y exigencias de los promotores del NO**, y que el gobierno asumió como propias las exigencias en materia de Jurisdicción de quienes se manifestaron en contra del acuerdo inicial.

El abogado resaltó la disposición de las partes y los puntos en que los integrantes del equipo de paz de las FARC cedieron frente a lo que se estableció en el acuerdo inicial. ([Le puede interesar Así se seleccionan los jueces de la JEP](https://archivo.contagioradio.com/asi-seran-seleccionados-las-y-los-jueces-para-la-jurisdiccion-especial-de-paz/))

### **Duración de la Jurisdicción de paz** 

El jurista también aclaró algunos de los puntos en torno a la **duración de la Jurisdicción** de paz y reafirmó que la **duración será de 15 años para todos los procesos**. Explicó que el plazo para presentar los casos es de 5 años para todas las partes, un plazo de 10 años para las primeras decisiones y de 15 en caso de aplicación de condenas. Además resaltó que la sala de estabilidad tendrá una duración indefinida. ([Lea también Militares respaldan la Jurisdicción Especial de paz](https://archivo.contagioradio.com/militares-detenidos-respaldan-el-proceso-y-la-jurisdiccion-especial-de-paz/))

Por otra parte aseguró que el acuerdo, **tal como quedó es definitivo y que no está sujeto a nuevos cambios**, es decir, que ya se atendieron las exigencias de los promotores del NO y no se dará lugar a nuevas intervenciones en el acuerdo, que además **reposará en las instituciones internacionales a través de su consignación en Berna**, Suiza, una vez se defina el nuevo mecanismo de implementación

Sobre este punto, el abogado ha manifestado que el mecanismo más práctico sería el congreso de la república, dado que es más rápido que los cabildos abiertos u otro plebiscito.

<iframe id="audio_13768282" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13768282_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
