Title: Habitantes de Morales Cauca se desplazan por miedo a combates armados
Date: 2019-09-01 18:09
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cauca, Desplazamiento, Desplazamiento forzado, ejercito
Slug: habitantes-cauca-desplazan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/cauca-desplazados.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:]@sucesoscauca 

 

En el municipio de Morales en el Cauca, un grupo de familias se desplazó forzadamente hacia el sector de la vereda Pan de Azúcar luego de recibir el anuncio por parte de un grupo disidente de Las Farc, donde informaron que tomarían acciones contra las tropas del ejercito presentes en esta zona.(Le puede interesar:[Desplazamiento forzado y aumento en la violencia: Las preocupaciones del CICR Colombia](https://archivo.contagioradio.com/desplazamiento-forzado-cicr-colombia/))

Los desplazamientos se produjeron en el sector del Socorro en el municipio de Morales, luego de combates entre el grupo armado y el ejercito, que ha generado tanto temor que quienes aún se mantienen en esta zona han preferido no salir de sus casas por protección y miedo a cualquier ataque.

Así mismo más de 120 estudiantes no han podido desplazarse a sus zonas de estudio.

La alcaldía de Morales indicó que en las próximas horas visitará la zona para activar los protocolos de protección y atención a las familias.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
