Title: Exigen activar mecanismos de alerta para identificar 286 cuerpos en cementerios de Nariño
Date: 2020-03-11 15:42
Author: CtgAdm
Category: Actualidad, Memoria
Tags: Ipiales
Slug: exigen-activar-mecanismos-de-alerta-para-identificar-286-cuerpos-en-cementerios-de-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Cementerio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/cesar-santoyo-identificacion-cuerpos-narino_md_48843803_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; César Santoyo, investigador del Colectivo Orlando Fals Borda

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Cementerio Tumaco, Nariño / Orlando Fals Borda

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La intervención de diferentes organizaciones sociales en tres cementerios de Nariño ubicados en Pasto, Ipiales y Tumaco permitió el hallazgo de 286 cuerpos sin identificar entre los que habría victimas de ejecuciones extrajudiciales y desaparición forzada según informó el [Colectivo Orlando Fals Borda.](https://twitter.com/colectivo_ofb) A su vez, han alertado sobre el deterioro de los restos hallados y que podrían **afectar directamente la identificación de los restos, en particular en Tumaco, donde según el Registro Único de Victimas se reportan 5.119 víctimas directas e indirectas de desaparición forzada desde 1985.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Identificación de los cuerpos en Nariño está en riesgo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Tal como explica César Santoyo, investigador del Colectivo Orlando Fals Borda, la mesa de desaparecidos de Nariño ha denunciado de forma reiterativa que históricamente no ha existido un adecuado tratamiento del cementerio, y sus condiciones fitosanitarias y administrativas han llevado a solicitar un cierre preventivo con el fin de intervenirlo y establecer una ruta (mediante procedimiento integral) para salvaguardar los cuerpos de las personas N.N. que continúan en proceso de identificación en medio de un rápido deterioro. [(Lea tambien: En cementerio de Tumaco podrían haber más de 150 personas sin identificar)](https://archivo.contagioradio.com/en-cementerio-de-tumaco-podrian-haber-mas-de-150-personas-sin-identificar/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Como principal diagnóstico de nuestras y nuestros antropólogos hemos encontrado una situación de deterioro ambiental y forense de las personas sin identificar en este cementerio"

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

S**antoyo explica que sumado a las malas condiciones en las que se encuentran los cementerios, los factores geomorfológicos del suelo y las condiciones climáticas de Nariño, resultan adversas para la preservación de los cuerpo** en un departamento donde la pluvialidad es abundante, y el número anual de días con lluvia oscila entre los 100 y 200.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La condición de los 426  cementerios del país es similar, pues tampoco existe un control, registro, ni condiciones de salubridad, "en cualquier sentido la preservación de los cuerpos es una de las prioridades principales que existe en Tumaco, Ipiales y Pasto", explica Santoyo. [(Le puede interesar: Militares intervinieron cementerio de Dabeiba hace dos años)](https://archivo.contagioradio.com/militares-intervinieron-cementerio-de-dabeiba-hace-dos-anos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque aún no se han podido establecer las causas de la muerte de las personas halladas, desde el Colectivo Orlando Fals Borda alertan sobre la situación de violencia que ha vivido históricamente el litoral en municipios como Tumaco, donde se registraron 98 homicidios en 2018 y en 2019 pese a existir una reducción del fenómeno, fueron asesinadas 52 personas, según la Fundación Ideas para la Paz. [(Le puede interesar: Tumaco, municipio con mayor número de líderes asesinados desde 2016)](https://archivo.contagioradio.com/cauca-narino-2/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**El trabajo que vienen realizando la Mesa de Desaparecidos de Nariño, junto a organizaciones como el Colectivo Orlando Fals Borda y la Fiscalía también es estudiado por la Jurisdicción Especial para la Paz (JEP),** que podría adelantar un proceso que permita solicitar medidas cautelares para el lugar, mientras la Unidad de Búsqueda de Personas dadas por Desaparecidas se encargará de asesorar el proceso de archivo y preservación documental de un lugar donde desde 2018 se había advertido que podría existir la posibilidad de que al menos 150 personas estén sin identifcar. [(Le puede interesar: Campesino habría sido asesinado por policías en Tumaco)](https://archivo.contagioradio.com/campesino-habria-sido-asesinado-por-policias-en-tumaco/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
