Title: Cerca de 600 indígenas y campesinos fueron desplazados en Córdoba: ONU
Date: 2019-04-14 17:07
Author: CtgAdm
Category: Comunidad
Tags: combates, cordoba, desplazamientos
Slug: desplazan-600-indigenas-y-campesinos-en-tierraalta-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/desplazados-Choco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto Archivo 

Según informó la **Oficina de Coordinación de Asuntos Humanitarios de Naciones Unidas (OCHA)** en Colombia, enfrentamientos entre el ejército y grupos armados organizados (GAO) que operan en el Departamento de Córdoba, h**an ocasionado el desplazamiento forzado de 589 personas**.

La información del organismo global, registra que fueron **cuatro comunidades indígenas y campesinas del municipio de Tierraalta, compuestas por 143 familias**, las que se desplazaron hacia el centro poblado de la comunidad de Siembra, que hace parte del Resguardo Indígena Embera Katío del Alto Sinú".

Desde la OCHA, afirman que del total de personas en situación de desplazamiento que se registraron, **27 de las familias que sufrieron mayor afectación por encontrarse cerca del lugar donde se registraron los combates**. Ademas alerto sobre la falta de espacios para albergar al total de las familias que tuvieron que dejar sus hogares.

La denuncia se suma a la del pasado viernes, donde la misma dependencia de **la ONU reporto el desplazamiento de otras 729 personas en el departamento de Nariño**, por cuenta de enfrentamientos similares que tuvieron lugar entre el 3 y el 7 de abril (Le puede interesar: [Asesinan a rector indígena AquileoMecheche en Rio sucio, Choco](https://archivo.contagioradio.com/asesinan-aquileo-mecheche-rio-sucio/))

###### Reciba toda la información de Contagio Radio en [[su correo]

 
