Title: Ya están conformados equipos de ELN y Gobierno para diálogos en Quito
Date: 2016-10-26 16:47
Category: Nacional, Paz
Tags: ELN, Mesa en Quito, Mesa Social para la paz.
Slug: conformados-equipos-de-eln-y-gobierno-para-dialogos-en-quito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/eluniversal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Universal] 

###### [26 de Oct 2016] 

El gobierno del presidente Santos y el Ejercito de Liberación Nacional **dieron a conocer los nombres de las personas que participaran en sus respectivos equipos negociadores**. Por parte del gobierno el grupo lo encabesazará Juan Camilo Restrepo, mientras que por el lado del ELN lo hará Pablo Beltrán. La instalación de la mesa se realizará el 27 de octubre en Quito

El equipo de conversaciones del gobierno estará acompañado por **Luz Helena Sarmiento, Juan Sebastían Betancur, Juan Mayr, el General Eduardo Herrera Barbel y Alberto Fergusson**. Un equipo que en conjunto reúne especialistas en los temas más importantes de esta mesa como lo son el Ambiental.

Referente a Juan Camilo Restrepo, el analista Víctor de Currea menciona que es un hombre **“al que le cabe el país en la cabeza”** debido a su trayectoria política, además de p**osee conocimientos precisos sobre el problema del agro y su agenda en Colombia** debido a que es ex ministro de Agricultura. Sin embargo, De Currea expone que también representa a un sector de las élites que quiere negociar.

De acuerdo con De Currea, que haya una mujer en el grupo de negociadores  es un “muy buen primer paso  porque significa que se comprende la importancia de las mujeres y su participación en estos escenarios”. **Luz Helena Sarmiento** es geóloga y ha tenido una trayectoria política dedicada al ambiente,  fue ministra de Ambiente en el primer periodo del presidente Santos y ex directora de la ANLA.

Por otro lado, el nombramiento de **Sebastían Betancur,** actual director de Ideas para la Paz, es para el analista una postura desde la institución y su visión de paz. Para finalizar considera que la elección del  general **Herrera Barbel,** se debe a su trayectoria militar y a la participación del mismo durante la fase preliminar. No obstante considera que hicieron falta en esta mesa Frank Pearl y del politólogo José Noé Ríos.

De otro lado, el **equipo negociador del ELN estará conformado por** Aureliano Carbonel, Bernardo Téllez, Vivían Henao, Consuelo Tapias, Isabel Torres, Silvana Guerrero, María Helena Buitrago, Miriam Barón, Gustavo Martínez, Tomas García, Carlos A. Reyes, Eduardo M. Pérez, Alirio Sepúlveda, Alejandro Montoya, Camilo Hernández, Oscar Serrano, Manuel Cárdenas y Marcos Suarez.

Frente a la delegación del ELN, Víctor de Currea, considera que son muchas las caras nuevas que podrán verse. Sobre Pablo Beltrán expone que es una persona que ha demostrado ser mediador y manejar su tono de acuerdo a lo álgido de los problemas que se puedan presentar  **“considero que es una persona que está comprometida con la paz y buscando una salida negociada”**.

No obstante, en esta mesa de conversaciones hay un tercer elemento, **la sociedad civil** que tendrá participación desde la Mesa Social para la Paz, un escenario que intentará acumular las propuestas de diversos sectores de la sociedad para llevarlas a la mesa en Quito. Le puede interesar:["Organizaciones sociales proponen diálogo nacional en mesa ELN-Gobierno"](https://archivo.contagioradio.com/organizaciones-sociales-proponen-dialogo-nacional-en-mesa-eln-gobierno/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
