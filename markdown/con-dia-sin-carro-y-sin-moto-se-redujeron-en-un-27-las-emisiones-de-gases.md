Title: "Día sin carro y sin moto" redujo en un 27% la contaminación en el aire
Date: 2015-09-23 13:31
Category: Ambiente, Nacional
Tags: Día sin carro, Encuentro de las Américas ante el cambio climático, Gustavo Petro, Secretaría de Ambiente, Secretaria de Gobierno, Secretaria de Movilidad, sistema SITP
Slug: con-dia-sin-carro-y-sin-moto-se-redujeron-en-un-27-las-emisiones-de-gases
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/DIA-SIN-CARRO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### www.youtube.com 

###### [23 sep 2015] 

Durante el "Día sin carro y sin moto" en Bogotá, se  **dejó de emitir 13.510 toneladas de dióxido de carbono, es decir se que se redujeron en un 27% las emisiones**, superando las cifras de las jornadas anteriores en febrero y abril, según señaló la Secretaria de Ambiente, Susana Muhamad.

Para el Alcalde Gustavo Petro, en el balance entregado se evidencia que la reducción del uso del carro "no solo es una medida descongestionante, sino de mitigación del cambio climático".

Además la Secretaria de Gobierno, Gloria Flórez, aseguró que se incrementó en un 40% el número de peatones con respecto a un día habitual en la ciudad, así mismo, según señaló la Secretaria de Movilidad, María Constanza García, otro de los logros fue que **se pasó de velocidades promedio de 17 km por hora a 27 km por hora.**

El alcalde indicó que los resultados del evento se presentarán en la '**Carta de Bogotá', que  finalmente será entregada en París para la COP21** que se realiza en el mes de diciembre donde **Petro** **propondrá que la medida de 'Día sin Carro' se realice mundialmente** con el fin de contrarrestar los efectos del cambio climático.

[![12042785\_1638715063012666\_3409413506651892187\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/12042785_1638715063012666_3409413506651892187_n.jpg){.aligncenter .size-full .wp-image-14537 width="540" height="540"}](https://archivo.contagioradio.com/con-dia-sin-carro-y-sin-moto-se-redujeron-en-un-27-las-emisiones-de-gases/12042785_1638715063012666_3409413506651892187_n/)
