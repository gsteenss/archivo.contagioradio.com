Title: Inician los encuentros entre la Comisión de la Verdad y los periodistas en Colombia
Date: 2018-10-10 13:02
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: acuerdo de paz, Comisión para el Esclarecimiento de la Verdad, víctimas
Slug: inician-los-encuentros-entre-la-comision-de-la-verdad-y-los-periodistas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/marcha-de-las-flores101316_264.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [10 Oct 2018] 

El próximo viernes 12 de octubre, arrancaran en Valledupar los encuentros regionales entre la Comisión de la Verdad y periodistas del país, que tienen como finalidad realizar un proceso de sensibilización e **instalar en las agendas territoriales el tema de la verdad y así,  dar a conocer el mandato, las funciones ** y alcances de la Comisión durante sus 3 años de ejercicio.

Este mismo espacio se pondrá en marcha en otras 10 regiones del país y constará de 3 etapas, la primera se realizará este año, la segunda en el 2019 y la tercera en el 2021, cuando se entregue el informe final de la Comisión. (Le puede interesar: ["De Roux pide a las organizaciones sociales acompañar a la Comisión de la Verdad"](https://archivo.contagioradio.com/hay-que-acompanar-la-comision-de-la-verdad-de-roux/))

Martha Martínez, jefe de prensa de la Comisión para el Esclarecimiento de la Verdad, señaló que este espacio permite darle las herramientas necesarias a los periodistas para que puedan hacer un cubrimiento responsable sobre el proceso de construcción del relato de la verdad, a partir de una apropiación del mismo, "desde la Comisión tenemos la responsabilidad **de dar nuevas herramientas de cómo abordar a las víctimas, sin causar un nuevo daño"**.

Además, la Comisión de Verdad pretende gestar, con este espacio, una red de alianzas para la generación de contenidos que permitan identificar historias en los territorios sobre el conflicto armado y los inconvenientes que se puedan encontrar para llevar a cabo esta tarea desde los periodistas.

Este mismo espacio se llevará acabo en Quibdó, Chocó el 18 de octubre, en Villavicencio el 19 de octubre, en Buenaventura el 20 de octubre, en Barranquilla el octubre 26, en Popayan el 1 de noviembre, en Bogotá el 2 de noviembre, en Barrancabermeja el 8 de noviembre, en Medellín el 9 de noviembre, en Ibague el 10 de noviembre, en Cúcuta el 15 de noviembre. **El 16 de noviembre se llevará a cabo un encuentro con medios nacionales y el 17 con medios independiente en la capital del país. **

###### Reciba toda la información de Contagio Radio en [[su correo]
