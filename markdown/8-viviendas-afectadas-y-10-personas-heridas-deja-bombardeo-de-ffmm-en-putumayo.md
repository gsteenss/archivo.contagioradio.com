Title: Bombardeo de FFMM en Putumayo habría dejado 8 viviendas afectadas y 10 heridos
Date: 2015-08-05 11:47
Category: DDHH, Nacional
Tags: Cese unilateral de fuego, Frente Amplio por la PAz, Mesa de Organizaciones Sociales del Putumayo, Puerto Asís, Putumayo
Slug: 8-viviendas-afectadas-y-10-personas-heridas-deja-bombardeo-de-ffmm-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/PUTUMAYO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_6077236_2_1.html?data=l5WkmZeXeo6ZmKiakp6Jd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRfIzqytvWx9PIpdSfwsvSxdnFqMLnjN6Yk5WPtMbm1NTbw9iPrMbmysnO1ZDIqcvVjMfcz8fFto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Yuri Quintero, Mesa de Org Sociales] 

###### [5 Agosto 2015] 

Según la Mesa de Organizaciones Sociales del Putumayo, la madrugada de este miércoles, se presentó un bombardeo en la vereda Diospeña, del cabildo indígena San Marcelino del municipio de San Miguel a 4 horas de Puerto Asís en el departamento del Putumayo, muy cercana a la frontera con Ecuador. Las primeras versiones de la comunidad dan cuenta de **8 casas averiadas y 10 personas heridas entre ellas algunos niños.**

Yuri Quintero, integrante de la Mesa de Organizaciones Sociales y encargada de DDHH, afirma que los pobladores de la zona denunciaron que el bombardeó se dio hacia las 3:30 am y a escasos 200 metros de las viviendas y muchas personas se vieron afectadas por la onda expansiva de las explosiones. En horas de la mañana **se desplaza una comisión de la sociedad civil** hacia el lugar para intentar establecer la situación actual de las personas del resguardo.

Uno de los problemas que enfrenta esta comisión es que, al parecer, miembros de las FFMM están acordonando la zona y han impedido el paso de varios presidentes de Juntas de Acción Comunal que intentan llegar al sitio para auxiliar a los heridos. Además, **de la comisión no hacen parte instituciones del Estado como la Defensoría o la Personería**, puesto que no tienen autorización para acudir al sitio. Vea también[Acciones armadas provocan desplazamiento de familias indígenas en Putumayo](https://archivo.contagioradio.com/armados-provocan-desplazamiento-de-familias-indigenas-en-putumayo/)

Se espera que hacia las horas de la tarde se pueda tener más información acerca de los hechos y el estado de las personas heridas. Noticia en desarrollo...
