Title: Hidroeléctrica el Edén acaba con el agua de Bolivia, Caldas
Date: 2017-03-16 15:14
Category: Ambiente, Voces de la Tierra
Tags: Bolivia, Caldas, Hidroeléctricas, ríos, sin agua
Slug: sin-agua-corregimiento-en-caldas-por-cuenta-de-la-hidroelectrica-el-eden
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/idroeléctrica-el-Edén.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Féderico Castro] 

###### [16 Mar 2017] 

En los territorios de un pequeño corregimiento de Caldas, llamado Bolivia, hace unos años corrían manantiales de agua entre matorrales verdes, en los que sus campesinos cultivaban café, caña, plátano y se dedicaban a la vida del campo. **Hoy, de ese edén no queda casi nada por cuenta de los daños causados por la hidroeléctrica El Edén,** que tiene a sus habitantes sufriendo de calor y en las calles luchando para que el agua vuelva a sus hogares.

La historia se remonta al año 2010, cuando en el departamento de Caldas se optó por emprender la **generación de energía eléctrica utilizando una de las mayores riquezas de Colombia, el agua**, aprovechando las dos cordilleras que atraviesan este lugar del país. Le puede interesa: [Ríos más biodiversos del mundo se encuentran en peligro por hidroeléctricas](https://archivo.contagioradio.com/biodiversidad-de-los-rios-en-peligro-por-represas/)

Como por arte de magia, para el año 2012 llegó al territorio **la Central Hidroeléctrica El Edén, y sin consulta alguna comenzó a construir por debajo de las casas de los campesinos** de Bolivia un túnel, lo que con el tiempo ha desencadenado en el daños a las casas, descenso del agua en algunos lugares del corregimiento y que en la actualidad ya no existan más 21 quebradas.

La Balastrera es el nombre de una de las quebradas que anteriormente suplía de agua a las familias, tanto para las labores del hogar, así como para la agricultura con 10.5 litros de agua por segundo, **hoy ya no lo puede hacer, están secas, incluso en estas épocas que son de invierno.**

Como si esto no fuera suficiente, a medida que el tiempo ha pasado, la comunidad cada vez se queda más sin agua, haciendo que **los manantiales ya no puedan surtir a varios acueductos comunitarios como el de Patio Bonito en esta vereda.**

“Hay varias quebradas que están secas, varios acueductos” relata Rubén Zuluaga, presidente de la veeduría del Proyecto de la Hidroeléctrica El Edén. Le puede interesar: [Sierra Nevada de Santa Marta en riesgo por construcción de Hidroeléctrica](https://archivo.contagioradio.com/sierra-nevada-de-santa-marta-en-riesgo-por-construccion-de-hidroelectrica/)

Aunque Corpocaldas había impuesto una medida de suspensión a esta hidroeléctrica, tiempo después, en unos estudios, que hasta la fecha la comunidad no conoce, se manifestó que las problemáticas de abastecimiento han sido ocasionadas por el fenómeno del niño, por lo que **Corpocaldas optó por levantar la medida y permitir que la hidroeléctrica continuara su trabajo.**

Con un tono caldense muy marcado, Ruben recuerda que **el 2016 fue “un año muy crítico, hubo gente que le tocó desplazarse de la región por falta de agua, se murió ganado”. **Le puede interesar: [Micro-hidroeléctricas en Caldas han secado 19 fuentes de agua](https://archivo.contagioradio.com/construccion-de-hidroelectricas-deja-sin-agua-a-90-familias-en-caldas/)

Situaciones que llevaron a la comunidad a reunirse con Corpocaldas para manifestarles los graves problemas que están dejando en su corregimiento, sin recibir respuesta alguna de la institución más que decir “nosotros no somos responsables de eso” dice Rubén.

Ahora, las familias deben ir a buscar los carrotanques que llegan a sus territorios para poder recoger el agua “son 38 tanques plásticos los que ubicó la hidroeléctrica. Son 2 horas las que tenemos de agua al día” manifestó Ruben.

La respuesta hasta el momento de la hidroeléctrica ha sido realizar arreglos a diversas vías y estructuras, dádivas que para la comunidad no recompensan el daño que ha generado la empresa **“el costo beneficio es muy alto, no hay recompensa que justifique que una región se quede sin agua, porque el agua es vida, sin agua uno no puede vivir”** asevera Ruben.

Esta comunidad ha tocado puertas y lo seguirá haciendo, para poder detener los daños que viene haciendo la hidroeléctrca desde hace más de 5 años a este lugar de Colombia, que de Edén ya poco le queda.

<iframe id="audio_17589766" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17589766_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
