Title: Alfonso Romero Buitrago el capitán que le apuesta a la verdad
Date: 2020-01-03 06:16
Author: CtgAdm
Category: Nacional
Tags: Alfonso Romero Buitrago, comision de la verdad, Ejecuciones Extrajudiciales, JEP, No Repetición, Reconciliación
Slug: alfonso-romero-buitrago-el-capitan-que-le-apuesta-a-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-27-at-4.07.34-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-27-at-4.07.34-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/festivales-de-la-memoria.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Tras la firma de los Acuerdos de Paz y la creación de la Jurisdicción Especial para la Paz y la Comisión de la Verdad, se abrió una ventana para que quienes hicieron parte del conflicto armado pudieran relatar su verdad y reparar a las víctimas de esos hechos. El capitán **Alfonso Romero Buitrago**, retirado hace 14 años, es uno de los integrantes de las Fuerza Militares que ha decidido comparecer ante este instancia  y desde su voluntad, participar en encuentros con víctimas y así construir otro relato de país.

### **La deuda de la Fuerzas Militares con la verdad en Colombia** 

El Capitán **Alfonso Romero** fue enviado al Batallón Voltigeros de la Brigada XVIII,  que operaba en Dabeiba Antioquia en 1993. Sin embargo, y pese a tener las intenciones de servir a su país, lo que se encontró allí fueron los diversos tratos humillantes a los que son sometidos los soldados y las violaciones de derechos humanos por parte de altos mandos.

"Yo pude evidenciar humillaciones, uno no se podía quejar para nada, en esa época uno se aguantaba. Además pensaba en todo el esfuerzo que hicieron los papás  para poderle cumplir el anhelo a uno" afirma Romero, quien para la  década de los 90 pudo constatar la relación entre Fuerzas Militares, grupos paramilitares y  la orden de destruir al enemigo y darle de baja.

> *"Mi primera unidad fue en Urabá, estuve en el área de Carepa hacia Dabeiba, en ese momento las Convivir estaban de Apartadó hasta Currulao".*

En ese periodo de tiempo, el Urabá chocoano y el Bajo Atrato sufrieron múltiples hechos de violencia como la Operación Génesis, comandada por el General Rito Alejo del Río, la Operación Septiembre Negro, los éxodos de desplazamientos de las veredas de Antazales y La Balsita, en Dabeiba, entre otros, en todas ellas organizaciones como la Comisión de Justicia y Paz ha cuestionado la participación de la Fuerzas Militares y el accionar conjunto que tuvieron con grupos Paramilitares, un escenario escalofriante para la violencia.

Es en ese contexto que Romero asegura "ver que personas hacían cosas malas", sin embargo,  para ese entonces ya era capitán, rango que le permitió decir "no, qué pena yo no voy a hacer eso", negaciones que posteriormente fueron castigadas por sus mayores con amenazas en contra de su vida.

Ver: [Las memorias de Dabeiba: En busca de la verdad](https://archivo.contagioradio.com/las-memorias-de-dabeiba-en-busca-de-la-verdad/)

"**Hice las denuncias por conducto regular,  pregunté al coronel, al comandante de la Brigada Móvil, después voy al general Montoya**, al general de la Brigada XVII, el general Zapata y  al ver que todo el mundo se tapa con la misma cobija, uno queda desarmado, no halla uno que hacer".

Posteriormente Romero es acusado y encarcelado por delitos que señala no cometió y que asegura, de haberlo hecho, "hoy en día sería coronel", asimismo, asevera que en su registro no existe "ni una sola violación a derechos humanos", no obstante, denunciar los abusos de poder de sus superiores le mereció un castigo.

Para la esposa del capitán, cuando **Alfonso Romero** fue sentenciado resultó un golpe muy duro, "uno siempre busca el bienestar para la familia y esa no fue mi situación. Cuando fui privado de la libertad, me hicieron la guerra allá dentro, mi compañero de armas me hizo la vida imposible. Luego murió mi esposa estando yo en prisión, no es que yo tengo odio contra el Ejército, sino que hay que contar la verdad".

\[caption id="attachment\_78654" align="aligncenter" width="1024"\]![Capitán Alfonso Romero](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-27-at-4.07.34-PM-1024x576.jpeg){.wp-image-78654 .size-large width="1024" height="576"} En entrevista con Contagio Radio\[/caption\]

### **Los nexos del paramilitarismo y el Ejército en el Urabá** 

Es en esa medida que el Capitán Romero ha acudido al Sistema Integral de Verdad Justicia Reparación y no Repetición para contar lo que vivió durante su periodo  en el Urabá, y aportar a que se esclarezcan hechos de violencia cometidos por el Ejército.

"**Trabajaban con los paramilitares**, hacían operaciones conjuntas, empezando por el general Zapata, lo veía en camioneta junto a los paramilitares y después que sacaron a la guerrilla; para nadie era un secreto que allá operaban las autodefensas. En el Cañón de La Llorona por ejemplo, las personas dicen que los paramilitares llegaban, mataban a la gente y las tiraban al Riosucio, todo eso es real, no se lo han inventado".

De igual forma, tras los recientes hallazgos de fosas comunes en Dabeiba, Romero considera que la Fiscalía debe revisar la zona "allí se lo comían a uno entero, yo fui uno de los últimos en salir, pero  uno de los que se quedó me contó que en la noche la guerrilla llegó con una volqueta y descargó cerca de 60 cuerpos de sus filas. Además, es probable que también hayan cuerpos de paramilitares y campesinos, y el Ejército no ha sido el único que han enterrado cuerpos, también existen soldados desaparecidos".

### **El temor tras la verdad** 

Con estos aportes que Romero ha realizado se exponen y evidencian las cadenas de mando tras distintos casos de ejecuciones extrajudiciales en Dabeiba y se clarifican las formas en las que operó el Ejército en conjunto con paramilitares, razón por la cual el capitán tiene claro que habrán represalias.

*"Yo sé que ellos van a amenazarme más adelante, cuando yo estuve privado de la libertad, el mayor Guzmán me mandaba saludes, le decían el Tigre, "saludos le mando el Tigre", lo amedrentaban a uno de esa forma, creo que me irán a amenazar o se irán contra la familia"*

A su vez, Alfonso Romero considera que también es una víctima del conflicto, "si hubiera hablado en mi momento, yo no estaría contándole lo que habría pasado, porque como todos ven, aquí nadie puede salir a los medios porque al otro día ya amanece muerto; por ejemplo los líderes, como los que he conocido junto a la Comisión de Justicia y Paz, ¿Por qué están amenazados? porque ellos abogan por la población más vulnerable". [(Le puede inteesar: La IV edición Festival de las Memorias tendrá lugar en La Balsita, Dabeiba)](https://archivo.contagioradio.com/festival-de-las-memorias-balsita-dabeiba/)

De otro lado, frente al accionar del Ejército, el capitán expresa que no ha recibido respaldo de esta institución, "ellos le dieron la espalda a soldados que fueron condenados a 30 o 40 años por cosas que no cometieron, claro, hay unos que sí hicieron cosas, pero hay otros que están afuera como comandantes, coroneles, generales que les da miedo que haya una Comisión para la Verdad (CEV) o una Jurisdicción Especial para la Paz (JEP) , empezando por el expresidente Uribe".

### **"Hay que apostarle a  la verdad" afirma Alfonso Romero** 

Romero señala que a pesar del riesgo es importante que más integrantes del Ejército o responsables del conflicto armado le apuesten a la verdad, "esto sirve para mostrarle a más de un militar escondido que no le dé miedo, si hay personas que yo he conocido, por allá en Bajirá, líderes de 70 años que son capaces de enfrentarse a esos grupos armados y decirles: 'no me voy de aquí, de mi tierra así me maten', ¿por qué no lo puede hacer un militar? No con odio, porque para eso estamos en reconciliación, es con la verdad que se habla, desarma los corazones y  cambia este país, pero hay una fuerza del otro lado que no quiere".

> *"Podrán callar uno, dos, tres pero hay muchas personas que están ayudando para que todo salga a la luz. Por ellos uno debe decir la verdad ante todo".*

Además Romero también manifiesta que es necesario que haya una verdad para iniciar un proceso de reconciliación,  "las víctimas solo quieren saber dónde dejaron a sus seres queridos, es muy duro no saber por qué los mataron, donde están, no solamente deben hacerlos personas exmilitares como yo o excombatientes, también quienes financiaban, ellos deben venir a contar su historia, porque patrocinaron  este conflicto. Somos todos colombianos, he compartido con exparamilitares y excombatientes de las FARC y no por eso quiere decir que yo sea ahora uno de ellos, estamos en un proceso de paz y todos somos hermanos colombianos, para mí es de mucha importancia eso"

Finalmente el capitán Alfonso Romero ratifica su compromiso con la verdad y alienta a que más personas se vinculen a la construcción de una Colombia en paz, "a mí no me da miedo contar todo lo que pasó y eso es lo que debería hacer más de un militar o excombatiente, para eso está la CEV y por eso es importante el perdón, en el latín se dice *par donare*, y significa para dar, y dejar ir, dejar ir ese odio entre la víctimas y el victimario".

\[caption id="attachment\_78729" align="aligncenter" width="800"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/festivales-de-la-memoria.jpg){.wp-image-78729 .size-full width="800" height="471"} Festival de las Memorias Z.H La Balsita- Dabeiba Antioquia / Laura Blanco - Contagio Radio\[/caption\]

### Usted también ha decidido participar en los Festivales de las Memorias impulsado por la Comisión JYP ¿Cómo ha sido encontrarse con las víctimas? 

Para mí ha sido muy gratificante y a la vez muy triste,  porque uno no se da cuenta, y no sabe la realidad de esas personas, es muy triste escuchar versiones de personas quienes fueron desalojadas y es que era así, "si no se van los matamos", como me hicieron a mí, "si usted abre la boca se le llena de moscos", yo por eso no me hice los exámenes médicos en el Ejército, por miedo a que me mataran, ahorita hay organizaciones que están a favor de las personas y uno escucha esos relatos y dan ganas de llorar y  aunque no fui victimario, yo les hablo a ellos y hablamos de todo, de reconciliación, de perdón, imagínese, estamos en un camino de paz y si no hay perdón ni reconciliación, no hay familia. Ver: [Festival de las Memorias en Alto Guayabal: un encuentro para construir la paz](https://archivo.contagioradio.com/festival-de-las-memorias-en-alto-guayabal-un-encuentro-para-construir-la-paz/)

### Eso ha sido lo más difícil y ¿qué ha sido lo más gratificante? 

Escuchar los relatos para mí es novedoso,  reunirme con excombatientes y  víctimas, escucharlos es algo que le entra a uno por los poros, uno siente tristeza de ver cómo tienen a esa población tan vulnerable o incluso el mismo Gobierno, cuando multinacionales entran a quitarles las tierras a sus personas, eso hay de todo. Alfonso Romero

Para mí ha sido muy chévere compartir con ellos y uno también lleva un mensaje, uno tiene que aprender a perdonar, yo no tengo nada contra el Ejército sino con aquellos integrantes que hicieron lo malo, muchas veces el Ejército pasa por ahí y en vez de ver a la población civil, andan como resentidos, no son capaces de saludar a la gente. todo eso me ha servido para reflexionar, y para charlar con compañeros y personas en las redes sociales que dicen que este proceso de paz no sirve, yo creo que si sus familias estuvieran como lo están las de las comunidades,  se pondrían la mano en el corazón y aportarían a la paz. Alfonso Romero.

https://www.youtube.com/watch?v=l8vw6g9jKV4

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
