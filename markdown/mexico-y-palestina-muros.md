Title: Mexico y Palestina dijeron No a los muros "World without Walls"
Date: 2017-11-22 13:46
Category: Onda Palestina
Tags: Apartheid Israel, BDS, Palestina
Slug: mexico-y-palestina-muros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/bds.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Stop The Wall 

##### 22 Nov 2017 

El pasado 9 de noviembre organizaciones del país mesoamericano y asiático celebraron lo que denominan el Día Sin Muros, una acción que lideraron a nivel global y que fue seguida en varias ciudades de Alemania, Irlanda, Estados Unidos, Chile y Brasil. Dentro de las organizaciones convocantes se encontraban las palestinas Stop The Wall (Paren el Muro) y el Comité Nacional de la campaña de Boicot Desinversiones y Sanciones. Por parte de los mexicanos se encontraba la organización Corsopal y el Observatorio de Derechos Humanos de los Pueblos.

En un comunicado conjunto las organizaciones de ambos países declararon que: “nos reconocimos mutuamente en la injusticia compartida que estamos sufriendo y en nuestra lucha constante por lograr la justicia, la igualdad y la dignidad. Esta conexión fue solo el comienzo: la idea de \#WorldwithoutWalls ha inspirado y reunido a muchos más movimientos. En los últimos meses, más de 370 organizaciones de alrededor de 30 países de todo el mundo se han unido a la convocatoria”.

En el acto realizado en la frontera entre México y Estados Unidos, Jamal Juma de Stop the Wall planteó: “quieren convertirnos es en esclavos, y no vamos a permitirlo. Seguiremos con nuestra lucha en contra de los poderes y las empresas que están construyendo estos muros”. Además dio un dato estremecedor: hoy hay 65 muros alrededor del mundo para oprimir a los pueblos y 70% de ellos están construidos con tecnología israelí.

En esta emisión de Onda Palestina tendremos noticias de la ocupación, arte, entrevistas y la mejor música Palestina, a continuación puedes escuchar el programa completo.

<iframe id="audio_22231781" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22231781_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
