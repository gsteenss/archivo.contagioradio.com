Title: Así se vive la fiesta de "La Paz sí es contigo" en el Huila
Date: 2016-07-15 12:48
Category: Movilización, Nacional
Slug: asi-se-vive-la-fiesta-de-la-paz-si-es-contigo-en-el-huila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/pitalito1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [15 de Jul] 

Ya se esta viviendo la fiesta de "La Paz sí es contigo", iniciativa que pretende generar al interior de la sociedad colombiana una movilización por el voto al sí del plebiscito por la paz y **abrir escenario de participación y discusión sobre los acuerdos de la Habana**.

En Pitalito, Huila se dio inició al Festival Surcolombiano por la Paz con la participación de más de 3.000 personas que en este momento recorren las calles con mensajes como **"venga construyamos la nueva Colombia**" o **"paz en los territorios vamos todos por la paz"**.

   
\
