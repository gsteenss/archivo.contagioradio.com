Title: "Carlos Negret llega a la Defensoría del Pueblo a aprender de derechos humanos"
Date: 2016-08-17 14:48
Category: DDHH, Entrevistas
Tags: Carlos Negret, Defensoría del Pueblo, Derechos Humanos
Slug: carlos-negret-llega-a-la-defensoria-del-pueblo-a-aprender-de-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Defensoria-del-pueblo1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [elregiodeporte]

###### [17 Ago 2017]

En medio de **una polémica votación en la que aparecieron 158 votos, cuando únicamente había posibilidad de que votaran 154 representantes a la Cámara,** Carlos Negret, ex secretario del partido de la U, fue elegido como nuevo defensor del pueblo, pese a las múltiples críticas por parte de organizaciones defensoras de derechos humanos y  de algunos representantes que prefirieron no hacer parte de la votación como la bancada del Polo Democrático Alternativo.

“Situaciones como las de ayer son francamente bochornosas, lastimosamente la elección del defensor del pueblo estaba cantada”, dice Ángela María Robledo, representante por la Alianza Verde, quien además de denunciar que dos congresistas ingresaron dos votos de más y por eso debió repertirse la votación, asegura que “todo estaba armado y terminó siendo una farsa”. **Al parecer, Negret ya conocía el resultado de la votación** y por eso, mientras se encontraba en urgencias en la clínica Country, a donde debió desplazarse por una urgencia de salud mientras estaba presente en la votación, **apareció una carta firmada por él en la que aceptaba el cargo como Defensor del Pueblo.**

Desde el principio hubo irregularidades en la forma como fue presentada la terna para defensor del pueblo por parte de la presidencia de la república, así como sucedió hace 4 años con la elección de Jorge Armando Otálora. Esta vez, mientras Negret tuvo cerca de 3 meses para hacer campaña, los demás candidatos, (Caterina Heyck Puyana, ex directora Nacional de Asuntos Especializados de la Fiscalía y Andrés Santamaría, presidente de la Federación Nacional de Personeros) no tuvieron garantías para hacer sus campañas pues apenas contaron con un día para poder reunirse con las bancadas del congreso.

Carlos Negret es abogado de la Universidad Javeriana graduado en 1991. En su [hoja de vida,](https://es.scribd.com/document/321485393/HV-Terna-Defensor-Pueblo-Carlos-Alfonso-Negret)se evidencian contratos por prestación de servicios tanto en el sector público como en el privado, especialmente en temas financieros, pero muy poco o casi nada evidencia experiencia en derechos humanos, pues **apenas cuenta con una especialización en Derecho Internacional de los Derechos Humanos que terminó en el año 2015.**

Además, según la Silla Vacía, es amigo del vicepresidente Germán Vargas Lleras y hermano de Felipe Negret, reconocido abogado liquidador de empresas del país y presidente de la Corporación Taurina de Bogotá.

Para Carlos Guevara, coordinador del Programa Somos Defensores, se trató de una decisión política que obedece a una campaña electoral  y al pago de favores burocráticos. “No es un hombre con altos estándares de trabajo en derechos humanos, como debería ser teniendo en cuenta el posacuerdo”, explica Guevara, a quien le preocupa que la familia de Carlos Negret históricamente ha sido dueña de varias tierras en el departamento del Cauca y que desde hace años, la familia Negret, tiene pleitos con las comunidades indígenas de esa zona del país.

De ahí, que la representante Robledo, señale que **es “gravísimo” que los defensores del pueblo electos, lleguen a aprender de derechos humanos con el cargo**. Negret ni siquiera cumple con los tiempos ni requerimientos estipulados en la Constitución para asumir la tarea, pues no cuenta ni con la experiencia en derechos humanos ni con los 15 años de ejercicio en el sector de la justicia.

“**Se deben generar mecanismos distintos y transparentes para que quienes llegue a ocupar estos cargos sean personas idóneas**, pues se trata de una magistratura moral y ética”, expresa la representante, quien añade que en los próximos días se espera interponer las demandas necesarias en contra de esta decisión.

Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
