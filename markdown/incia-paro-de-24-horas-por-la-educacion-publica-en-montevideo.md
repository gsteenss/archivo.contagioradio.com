Title: Inicia paro de 24 horas por la educación pública en Montevideo, Uruguay
Date: 2015-10-14 14:31
Category: Educación, El mundo
Tags: Educación pública en Uruguay, FENAPES, FEU Uruguay, Ley de Presupuesto Nacional Uruguay, Movilización en Montevideo, Paro en Uruguay, PIB Uruguay, PIT-CNT, Radio de derechos humanos
Slug: incia-paro-de-24-horas-por-la-educacion-publica-en-montevideo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Montevideo1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FENAPES ] 

###### [14 Oct 2015]

[La Federación Nacional de Profesores de Enseñanza Secundaria FENAPES, junto con la Federación de Estudiantes Universitarios FEUU y el Plenario Intersindical de Trabajadores - Convención Nacional de Trabajadores PIT-CNT de Uruguay, adelantan una **jornada de paro y movilización** ante el Parlamento uruguayo, en el marco de la discusión de la Ley de Presupuesto General que hoy aprobará la **asignación de 2016 para educación**.]

[FENAPES que paralizó funciones desde el pasado miércoles 23 de septiembre, denuncia en un comunicado que las actuales asignaciones presupuestales traen consigo una **inversión cada vez menor en los sectores estatales y mayores exenciones fiscales para el gran capital**.]

[Actualmente, “No hay propuestas de grandes aumentos salariales para los docentes, pero si apuestas a modalidades virtuales”, “**Somos uno de los países que más invertimos en defensa**” y “Continuamos siendo uno de los que menos invierte en materia educativa”.]

[“Una vez más reafirmamos nuestro compromiso irrestricto en la defensa de la educación pública de nuestro país, **la pelea seguirá siendo de largo aliento y deberá contar con miles en las calles**”, aseguran.]

[De acuerdo con FENAPES, la actual Ley de Presupuesto Nacional pretende **reducir en un 2,7% la inversión del PIB para el sector público**, abriendo espacio para privatizaciones, como parte de un acuerdo pactado entre el FMI y Uruguay en los años 80, cuyo paquete de medidas “Tenían como objetivo **liberalizar las economías y privatizar las empresas estatales**”.]
