Title: Continúan fumigaciones con glifosato sobre comunidades del Río Naya
Date: 2015-07-28 12:03
Category: DDHH, Nacional
Tags: coca, cultivos de uso ilítico, fumigaciones con glifosato, fumigaciones indiscriminadas, Gobierno Nacional, Ministerio de Salud, Naya
Slug: continuan-fumigaciones-con-glifosato-sobre-comunidades-del-rio-naya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/bigwood_colombia090.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [colombiadrogas.wordpress.com]

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Isabelino-Valencia.mp3"\]\[/audio\]

##### [Isabelino Valencia, Consejo Comunitario de la Cuenca del Río Naya] 

###### [28 de Julio 2015]

**El pasado sábado las comunidades que habitan en la cuenca del Río Naya, nuevamente fueron víctimas de las fumigaciones con glifosato,** lo que sus habitantes señalan como un atropello a la vida, al regar con veneno los ríos y los cultivos de pan coger.

Según cuenta Isabelino Valencia, fundador y miembro de Consejo Comunitario de la Cuenca del Río Naya, **desde el 2008 se ha planteado al gobierno una propuesta de sustitución de cultivos de uso ilícito** por proyectos alternativos, fue por eso, que los pobladores del Naya **decidieron sembrar 40 parcelas de cacao**, sin embargo, “la fumigación arrasó con todo”, expresa el líder comunitario.

Pese a que desde el gobierno se había dado la orden de parar las fumigaciones aéreas, actualmente se está realizando aspersiones aéreas de una manera “**indiscriminada, inconsulta y desprevenida** donde ya casi no hay coca", dice Isabelino, quien agrega que los habitantes piensan que puede tratase más bien de una **estrategia del mismo gobierno para desplazar a las comunidades y despojarlos de sus tierras.**

Esta fumigación afectó todo el territorio, es decir los 20600 pobladores que habitan en la cuenca del río, que alimenta a todas las comunidades de esa zona del país.

Desde la población se pide un pronunciamiento de la comunidad internacional para que se detengan las aspersiones aéreas. “**No nos oponemos a la erradicación necesitamos que se haga de una manera concertada, con una propuesta  de vida”**, señala Valencia.
