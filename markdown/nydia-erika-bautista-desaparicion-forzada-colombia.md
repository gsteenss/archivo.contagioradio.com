Title: Nydia Erika Bautista: rostro de la desaparición forzada en Colombia
Date: 2018-08-30 11:14
Category: Sin Olvido
Tags: desaparecidos, Desaparición forzada Colombia, Nydia Erika Bautista
Slug: nydia-erika-bautista-desaparicion-forzada-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/nydia-erika-bautista.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 30 Ago 2018 

La historia de la desaparición forzada es inhumana, vergonzosa y múltiple. Una historia llena de rostros, con infinidad de verdades ocultas y de familias que se movilizan para que nunca más vuelva a ocurrir, rostros que representan dolor, pero también la lucha y la búsqueda de verdad y justicia.

Con el paso de los años, uno de los rostros que se ha configurado como bandera de la detención y la desaparición forzada en Colombia es el de Nydia Erika Bautista; desaparecida el 30 de agosto de 1987, una joven bogotana de 33 años, socióloga y economista, lectora y estudiosa dedicada. Dirigió el periódico El Aquelarre, participó en 1984 en el sindicato del Instituto Nacional de Radio y Televisión (INRAVISIÓN) de forma activa. En 1986 fue detenida por militares de la Brigada 3 del Ejército Nacional, siendo torturada durante dos semanas, obligándole a firmar una declaración de su vinculación al Movimiento 19 de abril.

Ese 30 de agosto se encontraba junto a su hijo Erik Arellana, que para entonces tenía 12 años de edad, en el barrio Casablanca al suroccidente de Bogotá. Eran aproximadamente a las 6 de la tarde, cuando al salir de la celebración de la primera comunión de su hijo y su sobrina Andrea Torres Bautista, hija de su hermana Yaneth, acompañó a una amiga a coger el bus. En ese momento fue detenida por un grupo de hombres armados, inscritos a la Brigada 20 del ejército, quienes la llevaron a una finca en la zona de Quebradablanca en el municipio de Guyabetal, lugar en el que mantuvieron a Nydia en cautiverio, torturándola y agrediéndola sexualmente, trece días después su cuerpo se encontró en la vía Bogotá- Villavicencio, en estado de descomposición lo que imposibilito la identificación, durante 3 años su familia no supo nada de ella.

El sargento Bernardo Alfonso Garzón perteneciente al Batallón de Inteligencia y Contrainteligencia “Charry Solano” de la Brigada 20, declaró el 22 de enero de 1991 ante la Procuraduría que dicha unidad fue responsable de la desaparición Nydia Erika, con el conocimiento y aprobación del coronel Álvaro Velandia Hurtado, responsabilidad que le sería sindicada hasta septiembre de 1995; Sin embargo, irónicamente, ese mismo año el uniformado fue condecorado en una ceremonia militar, y sería únicamente hasta 2009, y por presión nacional e internacional que fue destituido, en decisión confirmada por la Sala Plena del Consejo de Estado.

A pesar de ello, el crimen, sigue en impunidad, gracias a que Velandia Hurtado ha apelado a recursos jurídicos para no ser detenido. Incluso en 2013 se anuló la orden de destitución pese a la gravedad de las imputaciones por detención, desaparición, tortura y asesinato. La Procuraduría señalo que tenía vínculos con paramilitares y lideraba el grupo Muerte A Secuestradores (M.A.S).

También se relacionaron los nombres de los suboficiales Julio Roberto Ortega Araque, Luis Guillermo Hernández González y Mauricio Angarita, sobre quienes la Fiscalía General consideró en su momento tener pruebas suficientes de su participación por lo que fueron privados de su libertad. Sin embargo la defensa de los uniformados alegó la decisión por considerarla injustificada, debido a que el ente investigador había declarado su preclusión en 2004. Por esta razón, en 2017 el Consejo de Estado condenó a la Fiscalía, y ordeno indemnizar a los militares por daños morales y materiales, dejando de paso al caso de Nidya Erika Bautista sin responsables.

Frente a la búsqueda de la verdad y la justicia la familia de Nydia nunca ha desistido desde el momento en el que se encontraron sus restos. Un camino que les ha costado pasar por la estigmatización, la represión, las amenazas y la violencia, situación que viven miles de familiares de víctimas de desaparición en Colombia, llevándolos incluso al exilio, como ocurrió con su hijo Erik, quien vivió en Alemania hasta el año 2006, teniendo que volver a salir del país apenas 8 años después, al ser hostigado por desconocidos en 2013.

A pesar de la violencia, el dolor y el destierro, su familia no ha desistido en la lucha por los derechos humanos, de las mujeres y todos los familiares de las víctimas de la desaparición forzada. Durante su vida además de ser una mujer intelectual, Nydia construyó escuelas y jardines en Bosa y realizó trabajo social y de sanidad a integrantes del M-19.

En su nombre fue creada en 1999 la Fundación Nydia Erika Bautista, por su hermana Yaneth Bautista, una entidad con enfoque de género, que pretende conmemorar a las mujeres colombianas que militan con coraje por la defensa de los derechos humanos y están contra la desaparición forzada, junto a ello, agrupa mujeres sobrevivientes víctimas de este delito y las convierte en sujetos sociales; labor que le mereció el Premio de Derechos Humanos Antonio Nariño.

Rinde homenaje también Andrea Torres Bautista, hija de Yaneth y sobrina de Nydia, quien es coordinadora del área jurídica de la Fundación. Entre sus albores está defender a los familiares afectados por el delito de la desaparición, pero más allá de eso, los acompaña en su dolor. Por último, su hijo Erik Arellana, dedicado al arte, por medio de la poesía ha logrado disuadir la angustia, el dolor, la soledad y el odio, en una lucha llena de significados, así ha conmemorado a Nydia por medio del libro “Tránsitos de un hijo al alba” y “Memorias vividas en cuadernos de viaje” así como en sus trabajos fotográficos.

Cada 30 de agosto, su imagen vuelve a las calles como representación de esos rostros desaparecidos por la violencia y revictimizados por injusticia del país. En la conmemoración del Día Internacional de las Víctimas de desaparición forzada, este y los miles de casos que reposan en los expedientes judiciales, reflejan además los desafío, retos y alcances de la paz, así como las exigencias institucional y legislativa al Estado colombiano, sobre las cuales no ha respondido. Los familiares y la sociedad colombiana solidaria seguirá preguntándose ¿Quién y cómo se tendrá en cuenta la verdad? ¿Dónde están los desaparecidos? ¿Habrá justicia?

Nydia Erika Bautista, en la memoria

Nydia Erika Bautista... Sin Olvido
