Title: Un aumento decente del salario mínimo tendría que ser del 14%
Date: 2020-12-02 09:07
Author: CtgAdm
Category: Actualidad, Economía
Tags: CUT, desempleo, Gobierno, Salario Mínimo
Slug: un-aumento-decente-del-salario-minimo-tendria-que-ser-del-14
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Salario-mINIMO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: pixabay / salario mínimo

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el 30 de noviembre comenzaron las negociaciones entre centrales obreras, carteras del Gobierno y gremios empresariales para definir el salario mínimo del 2021, una conversación que se dará en medio de una crisis económica que con la pandemia desnudó los problemas del modelo económico actual y que hoy requiere soluciones en particular para la pequeña, la mediana empresa y sus trabajadores.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El **profesor Diego Guevara de la Escuela de Economía de la Universidad Nacional** considera que un tema que será determinante serán los costos del salario, esto teniendo en cuenta que incluso se está pidiendo que llegue a 1 millón de pesos y que el subsidio de transporte suba a 120.000 pesos, la misma cifra que exigía en el 2019 y que finalmente concluyó en 877.802 pesos y 102.853 pesos para subsidio de transporte.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras sindicatos apuntan a un incremento del 14%, los gremios sostienen que ir más allá de un 3% ”pondría en riesgo la recuperación económica" pues sostienen que la mano de obra sería muy cara y si se sube mucho el salario no se podrá contratar mucha gente", al respecto el profesor aclara que esto sería cierto si el aumento del salario fuera del 50% o el 80%, sin embargo **resalta que lo que se propone desde las centrales es una cifra relativamente decente, si se tiene en cuenta el contexto de crisis.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"El hecho de que se baje el salario, como han propuesto algunos economistas tampoco es garantía de que vaya a dar más empleos, si no hay una estructura productiva, al final esos excedentes que se ahorran se los va llevar el empresario". Agrega, que en esta negociación tripartita entre gremios, centrales trabajadores y el Gobierno, este debe asumir una posición en medio de una crisis económica de tal magnitud.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, **Jorge Cortés, vicepresidente de la [Central Unitaria de Trabajadores de Colombia (CUT),](https://twitter.com/cutcolombia)** afirma que han hecho una valoración del momento que se vive que les permite concluir que no se trata de una crisis que obedezca únicamente a la pandemia, sino que se trata de un problema que se ha acumulado durante al menos 30 años, "es una crisis estructural generada por un modelo que hoy afecta profundamente a la economía".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez advierte que otra de las exigencias más alarmantes es la de derogar el decreto 1174 que acabaría con el salario mínimo como se conoce al igual que las pensiones, horas extras y sistema de salud que en la actualidad reciben los trabajadores y que llevaría a establecer un salario por horas según el salario que se establezca en estas negociaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras economistas de la Universidad de los Andes proponen reducir el salario mínimo de los trabajadores en un 20%, desde las centrales reiteran que es responsabilidad del Gobierno resolver dicha crisis y que dichas propuestas solo generarían **"mayor miseria, precariedad y desconocimiento de derechos".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por otro lado señalan que la solución está en que el Gobierno debe concentrarse en apoyar **a las medianas y pequeñas industrias a través de recursos económicos** que permitan un subsidio a estas y sus trabajadores (...) así como ha querido entregarle dineros gratuitos a Avianca, contamos con recursos, por eso hablamos de la renta básica". [(Le puede interesar: Sí hay recursos para Avianca pero no para sectores más vulnerables)](https://archivo.contagioradio.com/si-hay-recursos-para-avianca-pero-no-para-sectores-mas-vulnerables/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"La solución de los economistas siempre es flexibilizar los salarios, sin aportes a pensión, sin cajas de compensación, pero si no hay condiciones se termina precarizando el trabajo", concluye el profesor de la Universidad Nacional que considera será un debate álgido y candente y que podría extenderse hasta las últimas fechas del calendario y no será una negociación rápida

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Desempleo será un punto neurálgico en el debate del salario mínimo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Es de resaltar que **según el DANE, la tasa de desempleo registrada en octubre fue de 14,7 %, una cifra superior a la registrada en el mismo mes de 2019, cuando fue de 9,8 %, pero menor a la de septiembre de 2020 con 15,8 %.** Con relación a quienes en medio de la pandemia se han quedado sin empleo o han tenido que acudir a la informalidad, estadística que para septiembre alcanzaba el 47,6% según el DANE.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El profesor Guevara, advierte que en medio de la pandemia muchos trabajos se perdieron, empresas cerraron y algunas actividades ya no funcionan, lo que hará más difícil volver a un cifra de desempleo que se ubique por debajo del 10% como ocurría con anterioridad y más si el Gobierno decide apuntar a políticas de austeridad porque "para ellos el pago de la deuda es sagrada y lo harán por encima del gasto público".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Cuando hay momentos de auge dicen que no pueden subir el salario porque hay inflación y cuando hay crisis dicen que no se puede porque genera desempleo".
>
> <cite>Diego Guevara</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Para febrero de 2020, el DANE había anunciado que la tasa de desempleo a nivel nacional fue de 12,2%, mientras que en el mismo mes del 2019 fue de 11,8%, algo que el profesor destaca y es que incluso desde antes de la pandemia el desempleo ya venía creciendo, sin embargo no se le había prestado atención, pues sectores como el minero energético y financiero seguían creciendo, incluso resalta que tras la llegada del la Covid- 19, solo este último siguió fortaleciéndose en medio de la crisis.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras los datos entregados a lo largo de la reunión en la que estuvieron presentes técnicos del Departamento Administrativo Nacional de Estadística (DANE), y las carteras de Trabajo y Hacienda apuntan a que la productividad durante el año se ubicó en -0,6 %, las centrales rechazan estas cifras, es necesario resaltar que el salario mínimo del 2020 se negoció con una productividad de 0,21 %, una cifra considera por las centrales obreras, inferior a la esperada. [(Lea también: Si usted gana más de \$137.350 ya está por encima de la línea de pobreza: DANE)](https://archivo.contagioradio.com/si-usted-gana-mas-de-137-350-ya-esta-por-encima-de-la-linea-de-pobreza-dane/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué sectores se deben impulsar? Ese es el verdadero debate

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, **Daniel Ossa, estudiante doctoral en economía de la Universidad de Utah** explica que si bien esta cifra es negativa "no quiere decir que los trabajadores estén siendo menos productivo sino que la economía en su conjunto está siendo menos efectiva", una cifra que por cierto se deriva de de la cantidad de la población ocupada por la del Producto Interno Bruto, que para el segundo trimestre de 2020 fue de 52.504 millones de euros.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Si se revisan los salarios mínimos con relación a otros países de Suramérica, Colombia se ubica por debajo de países como Uruguay, Ecuador, Chile, Paraguay, Argentina y Perú. Naciones como Uruguay que ocupan el primer lugar en este listado otorgan un salario mínimo mensual equivalente a casi 430 dólares mientras el de Colombia se acerca a los 268 dólares. [(Lea también: Presupuesto Nacional 2021: Antesala de reforma tributaria y ataque a la paz)](https://archivo.contagioradio.com/presupuesto-nacional-2021-antesala-de-reforma-tributaria-y-ataque-a-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ossa hace un llamado a revisar cuáles son las apuestas que está haciendo el Gobierno, y cuáles son los sectores a los que se le está invirtiendo como lo son la mega minería, la extracción de recursos o la ganadería, sectores que demandan muy poca mano de obra, **mientras que otros como la agricultura o la manufactura, intensivos en mano de obra deben ser impulsados**.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
