Title: Ni las rejas  han logrado frenar el empoderamiento de las mujeres
Date: 2019-05-28 16:55
Author: CtgAdm
Category: Expreso Libertad
Tags: crisis carcelaria, Falsos Positivos Judiciales, mujeres reclusas
Slug: empoderamiento-mujeres-carceles-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/presos-politicos-640x410.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: e-consulta 

Las mujeres privadas de la libertad en el país han alcanzado logros significativos para el movimiento carcelario. Algunos de los más importantes son **el cierre de la Torre 9 de la Cárcel de máxima seguridad La Tramacúa** y el haber generado dinámicas de aprendizaje al interior de los patios en los que se encuentran recluidas a pesar de las vulneraciones que enfrentan en el día a día.

**Érika Aguirre,** víctima de falso positivo judicial conocido como el **caso "Lebrija"**,nos acompañó en los micrófonos del Expreso Libertad, para relatar experiencias cómo su primer encuentro con las mujeres en las cárceles, y cómo poco a poco, además de reconocerse prisionera política, inició un trabajo en conjunto con las demás reclusas para hablar de derechos en la **penitenciaria de Chimitá**.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F329502614387698%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
