Title: "Colombia acumula más de 10 solicitudes sin aceptar de  Relatores de la ONU"
Date: 2020-07-08 17:08
Author: CtgAdm
Category: Actualidad, Entrevistas, Líderes sociales
Tags: Duque, Michel Forst, ONU, Relaciones Internacionales, relatores de la Onu
Slug: colombia-acumula-mas-de-10-solicitudes-sin-aceptar-de-relatores-de-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/un-flag-internationality-foreign-trade.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En medio de la pandemia **ha sido evidente el incremento de la violencia y el riesgo de los liderazgos sociales en Colombia**, al mismo tiempo que los intereses del Gobierno se enfocaban en la contención de la pandemia, la erradicación forzada y la militarización de los territorios como solución para todo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto la comunidad internacional ha hecho evidente su preocupación, frente a los llamados de colectivos defensores de la vida en Colombia que solicitan el apoyo externo, **al mismo tiempo que el Gobierno pareciera ser de oídos sordos y selectivo frente a los mensajes internacionales que adopta.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Reflejo de ello y como lo señala la **Comisión Colombiana de Juristas (CCJ),** en el informe de las organizaciones de derechos humanos sobre el primer año de Duque, titulado [El Aprendiz del Embrujo](https://www.coljuristas.org/nuestro_quehacer/item.php?id=259). Señala que, *[**"desde febrero de 2010 Colombia no ha recibía a**]{}**ninguno de los relatores de la ONU, a pesar de que habían más de diez solicitudes de visita presentadas**"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, **Gustavo Gallón, Director de la CCJ**, señaló que esta es una conducta reiterada por parte del Gobierno tanto del actual y anterior. Sin embargo destacó que, *"**extrañamento Duque aceptó la invitación que le hizo uno de los relatores de la ONU***, ***[Michel Forst](https://archivo.contagioradio.com/cinco-claves-para-entender-el-informe-de-michel-forst-sobre-la-situacion-de-dd-hh-en-colombia/) en el 2018** para analizar la situación de defensores y defensoras de Derechos Humanos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar de esto cuándo en el 2019 el relator solicitó su ingreso al país para finalizar el informe y ver el progreso y compromiso a sus primeras recomendaciones, el [Gobierno no se lo permitió](https://archivo.contagioradio.com/gobierno-duque-no-da-respuesta-a-relatores-de-la-onu-para-ejecutar-su-trabajo/?fbclid=IwAR1pSx3yMAdzGTJuOPIL6aoxhp3BPGlMQE71P-spvyDGIMCjnNm3TgRwh1Y), ***"la voz popular dice que al Gobierno le molestó la rueda de prensa que dio al terminar su visita en diciembre de 2018"***, señaló Gallón.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que Forst tuvo que presentar su informe sin haber podido verificar si hubo algún avance, en donde destacó como una de las conclusiones que *"**Colombia es un país muy peligroso para defensoras y defensores de Derechos Humanos"***. ([Colombia, el segundo país más peligroso para defensores que trabajan temas empresariales](https://archivo.contagioradio.com/colombia-el-segundo-pais-mas-peligroso-para-defensores-que-trabajan-temas-empresariales/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"El Gobierno responde de una manera grosera ante el informe que había presentado el relator, haciendo correcciones absurdas. **El gobierno le discutió todo al relator sin ninguna consideración, hasta el color de la camisa"***
>
> <cite>**Gustavo Gallón| Director de la CCJ**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Ante esto el abogado señala, *"**esa es la respuesta grosera por parte de un Gobierno que tiene una situación de Derechos Humanos gravísima** desde hace años, y que además trata de mostrar una buena cara a la comunidad internacional".* ([Incumplir acuerdos internacionales trae consecuencias": Eurodiputados sobre Colombia](https://archivo.contagioradio.com/incumplir-acuerdos-internacionales-trae-consecuencias-eurodiputados-sobre-colombia/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### 10 relatores de la ONU sin ingreso en los Gobierno de Duque y Santos

<!-- /wp:heading -->

<!-- wp:paragraph -->

Gallón destacó que han sido ocho los relatores de la ONU que no han podido entrar al país, y 10 en total si se suman los que solicitaron la entrada en el gobierno de Juan Manuel Santos, *"**el país se está privando del beneficio de contar con la observación de estos expertos internacionales**, que además son personas que trabajan de forma voluntaria, eso quiere decir que no reciben sueldo de las Naciones Unidas".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicional, señaló que el trabajo que hacen los y las Relatores de la ONU en el mundo, es hacer observaciones críticas y responsables, procurando mejorar la situación de los países, *"trabajo beneficio para toda la comunidad, porque necesitamos que nos aconsejen, que nos pongan de precedentes muchas cosas y nos ayuden a resolverlas" .*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**Lo que ocurre en Colombia se puede leer como un reflejo de lo que viene desarrollando el gobierno de Trump** con la comunidad internacional, ejemplo de ellos es el anunció que hizo en el 2018 de su retiro del Consejo de Derechos Humanos de las Naciones Unidas"*
>
> <cite>**Gustavo Gallón| Director de la CCJ**</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### *"A pesar de todo hemos avanzado en DDHH"*

<!-- /wp:heading -->

<!-- wp:paragraph -->

Pese a que el panorama general global y nacional es desalentador en los últimos meses, Gallón señala que se deben reconocer avances, *"tal vez no los que se quisieran o esperaban , pero sí ha habido una curva progresiva en materia de desarrollo de los Derechos Humanos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Reflejo de ello es el trabajo conjunto de varias organizaciones en Colombia que lograron que se instaurara de manera permanente la Oficina de la Alta Comisionada de Naciones Unidas para los Derechos Humanos; *"con gran dificultad lleva 23 años de establecida en el país, y es mucho lo que ha aportado y mucho lo que le falta por entregar".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último el abogado destaca que la pandemia agudizó las visitas y la interventoría internacional, pero por otro lado, *"la pandemia tiene una virtud y es que ha puesto en precedente aspectos muy graves en materia de DDHH, como la pobreza y liquidez, convirtiéndolos en un tema de análisis de muchas más personas que aportar a la discusión"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Puede profundizar en este tema viendo. Otra Mirada: Colombia y sus relaciones internacionales.*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D282066349783725&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
