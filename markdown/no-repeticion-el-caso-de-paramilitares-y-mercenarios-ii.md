Title: No repetición: El caso de paramilitares y mercenarios (II)
Date: 2016-01-23 21:16
Category: Opinion, Ricardo
Tags: Paramilitarismo, uraba
Slug: no-repeticion-el-caso-de-paramilitares-y-mercenarios-ii
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/mercenarios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Ricardo Ferrer Espinosa](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/)  - @ferr\_es.**

###### 23 Ene  2016.

**Parte II: Las víctimas de mercenarios y paramilitares deben organizarse**

Nuestro silencio solo favorece a los victimarios. Debemos dar voz a los expoliados, desplazados, exiliados.

Las multinacionales, desde la apertura económica de César Gaviria Trujillo, entraron a Colombia acompañados de sus propios esquemas de seguridad, que terminaron agrediendo a la dirigencia sindical y nunca hubo controles por parte de la Superintendencia de vigilancia y seguridad privada.\[1\]

Posteriormente aparecieron grupos de mercenarios que enseñaron las técnicas terroristas para desplazar la población en territorios de alto valor económico codiciados por los gremios y grupos empresariales.

Lejos de Colombia, el caso más paradigmático del siglo pasado es la expulsión del pueblo palestino de su territorio ancestral, evento que se prolonga en el siglo 21.

Como sacada de un manual militar, la metodología criminal para desplazar población se aplicó rigurosamente en Urabá y en el Chocó: las familias escapaban cuando miraban la cabeza estacada, el cuerpo flotando en el río Atrato,  o el cuerpo insepulto, a 300 metros de una base del ejército.

Por mera supervivencia, las víctimas de las actividades mercenarias y paramilitares deben unirse en la memoria, en la catarsis de saber qué fuerzas los agredieron y sobre todo, evitar que estos engendros se reproduzcan.

La memoria debe hacerse pública, local, regional, nacional e internacional. Después de violar niñas y masacrar población civil en Colombia, el mercenario quiere volver tranquilo a su casa en Estados Unidos, Reino Unido, Sur África o Israel. Nuestra meta es que los vecinos de estos sicarios internacionales los identifiquen y los rechacen. Que les quiten su aureola de valientes y obtengan rechazo social.

Un objetivo de Relaciones Públicas: Informar a la opinión pública de los países en donde funcionan las matrices del sicario mercenario. La tarea no es fácil porque las actividades mercenarias gozan de aceptación en la cultura del cine, en el imaginario de algunos soldados y en las mentes infantiles. Si alguien piensa que exagero, recuerde la serie “Los magníficos” (Equipo A) que vimos en la TV durante la década de los 70. El argumento, con nuevos actores, volvió a las pantallas en el 2015. No es casual que un grupo de mochacabezas en Colombia haya tomado ese nombre.  Series como “Los magníficos” legitimaron la actividad paramilitar.

La meta es acabar con el prestigio y la impunidad que gozan hasta ahora las compañías militares privadas.

Hasta ahora es lenta y tímida  la acción de Naciones Unidas. El tema "no está de moda" como las campañas contra las minas o el cambio climático.

Las embajadas de los países que protegen actividades mercenarias deben recibir comunicados de protesta, y deben sentir el rechazo a sus métodos empresariales.

Las víctimas del terrorismo de estado tenemos el deber de investigar, documentar, reproducir los documentos, motivar debates y arrinconar a los legisladores locales para que proscriban definitivamente esta delincuencia tolerada.

No basta la ley. Necesitamos pedagogía de la solidaridad, del derecho, de la justicia, de la autodeterminación de nuestros pueblos. Si pensamos de este modo, concluimos que para cambiar la pedagogía debemos cambiar el tinglado de un estado injusto.

Nuestra pedagogía debe ser la libertad. Tenemos derechos. A la vida, a la libertad y a nuestra dignidad. Y esto deben entenderlo los ciudadanos de los países que exportan sicarios - mercenarios.

La pedagogía de la libertad y la dignidad comunitaria, serán el mejor remedio contra las bandas organizadas de banqueros, empresarios, militares y mercenarios, empeñados en globalizar la fuerza bruta. ([Ver parte I](https://archivo.contagioradio.com/paramilitares-y-mercenarios/))

###### \[1\] <http://www.supervigilancia.gov.co/?idcategoria=1706> 
