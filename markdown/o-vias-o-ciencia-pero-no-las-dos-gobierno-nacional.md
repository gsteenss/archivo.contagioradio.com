Title: O vías o Ciencia, pero no las dos: Gobierno Nacional
Date: 2017-02-24 06:00
Category: Opinion, Paola
Tags: educacion, Santos
Slug: o-vias-o-ciencia-pero-no-las-dos-gobierno-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/santos-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ministerio TIC 

#### **Por [[Paola Galindo](https://archivo.contagioradio.com/paola-galindo/)]** 

###### 24 Feb 2017

Esta semana las Comunidades académicas y científicas del país se sorprendieron, amargamente, por un anuncio del Presidente: en la 87° Cumbre de Gobernadores en la ciudad de Cali, Santos propuso el descongelamiento de recursos del Fondo de Ciencia, Tecnología e Innovación del Sistema General de Regalías para destinarlo a vías terciarias en el marco de la implementación del punto 1 del Nuevo Acuerdo. Días después, el Ministro de Hacienda Mauricio Cárdenas, anunció la presentación de un Acto Legislativo vía Fast Track mediante el cual se destinarían cerca de 1,3 billones de pesos para el programa 51/50, lo que se traduce en construir, mejorar o rehabilitar 50 kilómetros de vías terciarias en 51 municipios afectados por el conflicto armado.

El problema de esta propuesta es evidente, pues se restan recursos del sector de Ciencia y Tecnología – lo que, según Moisés Wasserman, equivaldría al triple del presupuesto actual de Colciencias- para invertirlos en 2.500 kilómetros de vías tercias si se entiende que la fórmula 51/50 implica la construcción de 50 kilómetros por municipio priorizado. Sorprende de esta iniciativa la michicatería del gobierno nacional a la hora de implementar los acuerdos de La Habana, acuerdos que generan la obligación al Estado de diseñar y ejecutar un Plan Nacional de Vías Terciarias y un Plan Nacional de Educación Rural que contemple el fortalecimiento y promoción de la investigación para el sector agropecuario además de la ampliación de la oferta en educación superior en modalidades técnicas, tecnológicas y universitarias. Ambas cosas, por su importancia para la paz y para el desarrollo del país, necesitan de un volumen importante de recursos si existe la voluntad política de llevarlas a cabo.

De acuerdo al documento CONPES 3857 de 2016 sobre lineamientos de política para la gestión de la red terciaria, existen en el país 142.284 kilómetros de red vial con estas características ubicada principalmente en áreas rurales. El programa que propone el gobierno y que resta un monto importante del presupuesto para la investigación en Colombia, sólo abarcaría en términos de construcción, rehabilitación y mejoramiento el 1,75% del total de kilómetros de la red vial terciaria, porcentaje que, por lo bajo, en nada resuelve la precaria situación de este tipo de infraestructura, fundamental para el desarrollo de la Reforma Rural Integral en tanto conecta las áreas productoras de alimentos, asociadas a economías campesinas, y las áreas de sustitución de cultivos de coca, con los espacios de comercialización y en general con el mercado nacional. Debe anotarse también, que una eventual inversión por parte del gobierno nacional sobre este tipo de infraestructura, deberá contemplar necesariamente la realización de un inventario de vías terciarias y del estado de las mismas, dado que a la fecha no se cuenta con uno a pesar de existir la obligación en la ley 1228 de 2008. Contar con ello mejoraría el impacto de las inversiones y facilitaría la articulación de esfuerzos entre las entidades territoriales y las entidades del orden nacional para el mejoramiento del proceso de contratación y de ejecución en lo que a este tema se refiere, evitando también el flagelo de la corrupción, fantasma de la gestión pública.

No se trata de administrar la escasez e imponer falsas disyuntivas, se trata de disponer los recursos del Estado, públicos por demás, para la adecuada financiación de dos necesidades insoslayables: vías e investigación para los territorios rurales de nuestro país, con ello se cumpliría parcialmente la promesa que inauguraba este gobierno hace unos años, “paz, equidad y educación”.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
