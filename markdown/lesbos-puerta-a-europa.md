Title: Lesbos, puerta a Europa
Date: 2016-02-29 09:51
Category: Eleuterio, Opinion
Tags: Grecia, lesbos, Refugiados
Slug: lesbos-puerta-a-europa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/refgugiados-europa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Georgios Giannopoulos 

#### **[Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)** 

###### 29 Feb 2016

Cada minuto 30 personas se ven obligadas a abandonar su país y buscar refugio en otras tierras. Hablamos de más de 60 millones de refugiados, cifras que superan a las de la II Guerra Mundial; tal es la situación en nuestro mundo actual. También hay quienes se desplazan porque son nómadas, en este caso nómadas solidarios como nuestro compañero Andreas, activista griego miembro del proyecto “Nómadas del mar”, un barco colectivo que viaja allí donde puede establecer contacto con otros movimientos autónomos y solidarios. Esta vez su rumbo les llevó hasta la isla de Lesbos, en Grecia. Allí han trabajado como voluntarios, además de realizar actuaciones diarias de música, circo y acrobacias para animar los días de espera y sufrimiento a los refugiados que llegan a la isla.

En situaciones extremas como ésta sale a relucir el verdadero carácter de la gente. Hay gente que se aprovecha y gente que muestra su solidaridad. “Al principio hubo mucho rechazo por parte de la gente local. Luego la avalancha de voluntarios y policías, además de los refugiados, ha provocado un auge de la economía en la isla. Viviendas que estaban vacías, ahora están todas alquiladas a precios mucho mayores que antes, igual que los coches. Los hoteles y los bares están llenos. Ha habido un momento que en los quioscos una botella de medio litro de agua ha pasado de valer 50 céntimos a dos euros. Las compañías de teléfonos montan sus puestos delante de los campamentos para vender móviles y tarjetas a los refugiados. La llegada de un sinfín de ONGs, instaladas tanto en la costa como en la capital, ha supuesto muchos puestos de trabajo para la gente local.”

También existen varios colectivos autónomos que han instalado sus tiendas y sus jaimas y dan acogida a los refugiados. Se trata de voluntarios que van por libre, montan campamentos y hacen comedores de forma gratuita para los refugiados. Uno de estos colectivos es Pikpa, se trata de un proyecto autónomo que lleva funcionando desde hace más de tres años. “Prestan ayuda a la gente que ha perdido a sus familiares en la travesía, también a niños que llevan varios meses allí acogidos, embarazadas que acaban de parir, e incluso refugiados políticos.” La situación de los menores es bastante delicada. Se calcula que hay unos 50 mil niños que han desaparecido por Europa y se encuentran a merced de las mafias del trabajo y la explotación sexual. Pikpa acoge también a muchos refugiados kurdos cuya situación política es más complicada, en parte por el enfrentamiento que su pueblo mantiene con Turquía. “En torno a Mitilini funcionan otros colectivos como Musaferat, que además de prestar ayuda a los refugiados hace campaña contra los CIEs y No Border Kitchen, que cada día hace comida para los refugiados y voluntarios en esta parte de la costa. Al norte de la isla actúa el proyecto Plátanos recibiendo las lanchas, ayudando a salir del agua a los refugiados, proporcionándoles ropa seca y comida y llevándolos a la capital en autobuses o coches particulares.”

La otra presencia numerosa y excepcional que ha llegado a la isla es la de Frontex, policía de fronteras de la UE, y los ejércitos de varias partes de Europa. “Hay policía de Polonia y Alemania, barcos militares rusos, búlgaros, italianos y portugueses. Su actuación en cuanto a prestar ayuda a los refugiados sencillamente no existe. Toda esta policía y militares de otros países tienen los mismos derechos que las fuerzas locales, a veces más, por lo que el Estado griego tiene poco que decir en las islas.”

La situación de Grecia como país fronterizo y zona de paso entre Europa y Oriente han supuesto muchos problemas internos. El cierre de fronteras en otros países europeos como Alemania creará nuevas situaciones de dificultad. “Ya se habla de levantar campos de refugiados a las afueras de las grandes ciudades griegas como Atenas con capacidad para 400 mil personas y Tesalónica que sería algo más pequeño. En 1922, un millón y medio de refugiados tuvieron que abandonar Turquía para ir a vivir a Grecia lo que supuso una importante trasformación social del país. Hoy no sabemos qué va a pasar.”
