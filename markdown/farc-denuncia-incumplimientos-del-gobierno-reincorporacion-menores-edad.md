Title: FARC denuncia incumplimientos del gobierno en reincorporación de menores de edad
Date: 2017-01-29 11:15
Category: Nacional, Paz
Tags: FARC, Menores de edad, Timoleon Jimenez
Slug: farc-denuncia-incumplimientos-del-gobierno-reincorporacion-menores-edad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/niños-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [29 Ene 2017]

A través de su cuenta en twitter, el jefe máximo de las FARC, Rodrigo Londoño señaló algunos de los hechos que evidencian los **incumplimientos del gobierno** frente a la reincorporación de menores de edad.

El primero de los puntos que señala Londoño, es que dos adolescentes de nombres, Natalia y Loreisi, que se desvincularon del campamento en el 34 Frente de las FARC, denunciaron incumplimientos, lo que hizo que fuera el mismo **Frente el que tuviera que conseguir la matricula en el colegio y los uniformes** para que las jóvenes terminen sus estudios.

> 1/5 Natalia y Loreisi, de los primeros adolescentes que se desvincularon del campamento en el 34 Frente, manifiestan incumplimiento.
>
> — Rodrigo Londoño (@TimoFARC) [28 de enero de 2017](https://twitter.com/TimoFARC/status/825422523433107457)

<p style="text-align: center;">
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
> 2/5 Al Frente 34 de las [@FARC\_EPueblo](https://twitter.com/FARC_EPueblo) le toco conseguir quien las matriculara en el colegio y diera uniformes para que terminen sus estudios — Rodrigo Londoño (@TimoFARC) [28 de enero de 2017](https://twitter.com/TimoFARC/status/825422682002821120)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
> 3/5 [@FARC\_EPueblo](https://twitter.com/FARC_EPueblo) habló con las adolescentes para que se mantuvieran fuera de campamentos, pese a incumplimiento del Gobierno. No querían
>
> — Rodrigo Londoño (@TimoFARC) [28 de enero de 2017](https://twitter.com/TimoFARC/status/825422830955274240)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
De acuerdo con Timoleón, las FARC habló con las adolescentes para que se mantuvieran fuera del campamento, pese a que no contaban con las garantías para ingresar al colegio. Incluso asegura que **los familiares** de las jóvenes, que cuenta con muy bajos ingresos económicos, **han tenido que pagar los gastos de transporte y hospedaje para poder visitarlas.**

Así mismo, afirma que otras tres jóvenes que se desvincularon junto a Natalia y Loreisi, se encuentran en situación de abandono, y sin ningún tipo de atención por parte del gobierno, pese a los compromisos pactados.

> 4/5 Familiares de las dos adolescentes han corrido con los gastos de transporte y hospedaje para poder visitarlas. Son familias pobres.
>
> — Rodrigo Londoño (@TimoFARC) [28 de enero de 2017](https://twitter.com/TimoFARC/status/825422946923585541)

<p style="text-align: center;">
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
> 5/5 Tres jóvenes más que se desvincularon con ellas están en situación de abandono. ¿Quien incumple? — Rodrigo Londoño (@TimoFARC) [28 de enero de 2017](https://twitter.com/TimoFARC/status/825423069808312322)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Esa situación que señala el comandante de las FARC, se une a las conclusiones del informe de la Comisión de veeduría que hace el monitoreo y seguimiento a este proceso, y que manifestó que [el protocolo de reincorporación para menores de las FARC ha sido "improvisado",](https://archivo.contagioradio.com/el-protocolo-de-reincorporacion-para-menores-que-salieron-de-farc-ep-fue-improvisado/) y no se está contando **con una adecuada planeación** para recibir a las niñas, niños y adolescentes que estaban en la guerrilla.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
