Title: Colombia no avanza en garantía de DDHH: ONU
Date: 2016-11-04 14:55
Category: DDHH, Nacional
Tags: derechos civiles, Derechos LGBT, Derechos Politicos, Gineba, ONU, paz
Slug: colombia-no-avanza-en-garantia-de-ddhh-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/ConsejoDDHH-ONU.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Crbz ] 

###### [4 Nov de 2016] 

El pasado 19 y 20 de Octubre, el Comité de DDHH de la ONU se dio cita en Ginebra para realizar el **séptimo examen de Colombia ante el Comité de Derechos Humanos**, órgano de las Naciones Unidas que está conformado por 18 expertos independientes que supervisan la aplicación del Pacto Internacional de Derechos Civiles y Políticos. **Le puede interesar:** [Las 11 recomendaciones sobre derechos humanos de la ONU a Colombia](https://archivo.contagioradio.com/las-11-recomendaciones-sobre-derechos-humanos-de-la-onu-a-colombia/)

Luego de esos dos días de reuniones **se han conocido las observaciones finales a dicho informe**. En estas reconocen la oportunidad de reanudar el diálogo constructivo con la delegación del Estado parte sobre las medidas adoptadas durante el periodo que se examina.

Así mismo, asegura el comunicado, que reconocen las numerosas acciones emprendidas en materia legislativa para avanzar en la garantía de los derechos civiles y políticos en Colombia.

Sin embargo, **el Comité manifiesta serias preocupaciones frente a algunos puntos en los que no ha habido avances y por los que se sigue violando el Pacto Internacional de Derechos Civiles y Políticos.**

### **Preocupaciones del Comité y recomendaciones** 

Luego de revisar con detenimiento la información entregada por el Estado colombiano y diversas organizaciones sociales presentes en Ginebra, **el comité asegura que se debe continuar trabajando para adoptar las medidas necesarias** y aplicar plenamente los dictámenes en los que el Comité haya establecido que hubo una violación del Pacto.

Aunque el Comité celebra los avances sucedidos en materia de paz en Colombia, asegura que **el Estado debe continuar e intensificar sus esfuerzos para prevenir la comisión de violaciones de los derechos y buscar la verdad, justicia y reparación integral de las víctimas del conflicto armado interno.**

Asegura además, que el Estado debe redoblar sus esfuerzos para garantizar que ninguna de las graves violaciones de derechos perpetradas por miembros de grupos paramilitares desmovilizados quede impune.

**Otra de las preocupaciones y en las que el Comité de DDHH de la ONU fue enfático,  es lo que acontece con la comunidad LBGT.** Según los expetos el Estado de Colombia debe continuar e incrementar sus esfuerzos para combatir los estereotipos y prejuicios contra esta comunidad y para garantizar que: se prevengan los actos de discriminación y violencia en su contra; se investigue, procese y sancione a los autores de actos de violencia en su contra; y se otorgue atención y reparación integral a las víctimas.

En el documento de 11 páginas, **el Comité realizó recomendaciones en temas como la violencia contra la mujer y violencia sexual, la desaparición forzada de personas, las detenciones ilegales, la erradicación forzada de cultivos de uso ilícito, el reclutamiento militar y los derechos de las personas afrodescendientes e indígenas.**

Sin embargo, la información proporcionada por la sociedad civil y organizaciones sociales dan cuenta de que faltan mayores esfuerzos para garantizar los derechos civiles y políticos de los colombianos, así como para prevenir y sancionar la violación de estos, lo que preocupó de sobremanera al grupo de expertos que conforman el Comité.

El próximo informe periódico, que espera cuente con mayores avances que el presentado para este año, deberá ser entregado al órgano internacional a más tardar el 4 de noviembre de 2020. **Le puede interesar:** [ONU discute tratado para que empresas respeten derechos humanos](https://archivo.contagioradio.com/onu-discute-tratado-para-que-empresas-respeten-derechos-humanos/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
