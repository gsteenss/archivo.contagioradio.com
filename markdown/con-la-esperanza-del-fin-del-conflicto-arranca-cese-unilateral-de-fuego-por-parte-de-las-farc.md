Title: Con la esperanza del fin del conflicto arranca cese unilateral de fuego por parte de las FARC
Date: 2015-07-21 18:46
Category: Paz, Política
Tags: cese al fuego unilateral FARC-EP, Frente Amplio por la PAz, Iván Cepeda, veeduría cese al fuego unilateral
Slug: con-la-esperanza-del-fin-del-conflicto-arranca-cese-unilateral-de-fuego-por-parte-de-las-farc
Status: published

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_5053993_2_1.html?data=lpWilZ6dd46ZmKiakpeJd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjK7jw9OPh8bkxsnOjdjTptPZjM7by8jNs4zYxtGYxcrXqYzVzZDT18rLs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ivan Cepeda, senador Polo Democrático] 

###### [21, Julio, 2015] 

El Frente Amplio por la Paz, el Movimiento de Constituyentes por la Paz, las Iglesias y los países garantes serán algunas de las organizaciones que harán veeduría del Cese Unilateral de las FARC que tuvo su inicio este 20 de Julio y que se espera termine con un Cese Bilateral de fuego y el fin del conflicto.

Iván Cepeda señala que por parte de las organizaciones está todo el compromiso de hacer veeduría y el Frente Amplio por la paz entregará a los países garantes todos los informes para que se hagan los trámites pertinentes ante la ONU y la UNASUR que ya definieron sus delegados. El congresista agrega que ya hay una experiencia de veeduría y que en este sexto cese unilateral se harán todos los esfuerzos necesarios para que prospere hacia un cese bilateral.

A pesar de que el gobierno anunció que se iniciarán las medidas para el desescalamiento del conflicto, aun no se conocen públicamente, para Iván Cepeda, una de esas medidas puede consistir en el cese de los bombardeos, pero también se debe acompañar de medidas efectivas para que no persista la judicialización de líderes sociales como los jóvenes del congreso de los Pueblos que fueron arrestados y están siendo judicializados.
