Title: El próximo 5 de octubre "pasarán al tablero" autoridades que protegen líderes: Iván Cepeda
Date: 2018-09-06 17:56
Author: AdminContagio
Category: DDHH, Nacional
Tags: genocidio, Iván Cepeda, Iván Duque, lideres sociales
Slug: 5-octubre-lideres-cepeda
Status: published

###### [Foto: @IvanCepedaCast] 

###### [6 Agosto 2018] 

En el debate de control político sobre el asesinato de líderes sociales, congresistas de diferentes partidos se reunieron para pedir al Gobierno que tome medidas concretas para enfrentar este flagelo, e invitaron a los ciudadanos a movilizarse por la defensa de la vida.

El debate, que coincidió con el cierre de la manifestación estudiantil por recursos para la educación y respeto a la vida de los líderes, fue animado por las intervenciones de los integrantes de la lista por la Lista por la Decencia, el Polo, Alianza Verde y FARC, como afirmó el congresista **Iván Cepeda.**

### **Los puntos claves del debate** 

Los congresistas pidieron al Gobierno que se reconozca la sistematicidad en los asesinatos de líderes sociales, porque **"hay unos patrones en este fenómeno que son evidentes"** aseguro Cepeda, presentes en la persecución a la Unión Patriótica, la Marcha Patriótica, Colombia Humana, el asesinato a ex combatientes de la FARC y la agresión a defensores y defensoras de Derechos Humanos.

También pidieron que se revele qué es verdaderamente el grupo llamado **Aguilas Negras,** **"porque hay elementos que nos dan para pensar que se usa como una especie de camuflaje o de maquillaje para operaciones en cubierta de militares o policía"** afirmó el Senador. Igualmente, la Bancada de Oposición insistió sobre el carácter operativo de las fuerzas militares en la región y pidieron garantías para los movimientos y organizaciones políticas.

Adicionalmente, los congresistas señalaron que muchos de los líderes amenazados y perseguidos trabajan en la implementación del Acuerdo de Paz en "muchos lugares del país", así como por la Reforma Rural Integral, la sustitución de cultivos de uso ilícito y el proceso organizativo como las Juntas de Acción Comunal. (Le puede interesar: ["21 líderes han sido asesinados en lo corrido del Gobierno Duque"](https://archivo.contagioradio.com/durante-duque-asesinados-21-lideres/))

### **"Estamos asqueados de declaraciones de carácter formal o de pactos que no se traducen en hechos concretos"** 

Cepeda sostuvo que hay recursos y políticas públicas que pueden ser llevadas a la práctica, y con ellas se detendría el "genocidio" que se está viviendo; por lo tanto están **"asqueados de declaraciones de carácter formal, de declaraciones de pésame o de pactos que son útiles, por supuesto, pero que no se traducen en hechos concretos".** (Le puede interesar: ["Pacto por la vida se firmó excluyendo a líderes que la tienen en riesgo"](https://archivo.contagioradio.com/pacto-vida-excluyendo-lideres/))

El Senador confirmó que  harán una verificación sobre los efectos del debate el próximo **5 de octubre en Popayán,  donde esperan "pasar al tablero a las autoridades en el territorio"**; y concluyó que si el problema del ataque a los liderazgos sociales no encuentra una respuesta pronta por parte del Gobierno, habrá una ola de movilizaciones en todo el país.

<iframe id="audio_28397191" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28397191_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
