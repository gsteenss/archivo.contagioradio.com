Title: Former Army commander to testify before peace tribunal in February
Date: 2019-12-29 15:50
Author: CtgAdm
Category: English
Tags: extrajudicial killings, false positives, Mario Montoya Uribe, Operation Orion
Slug: former-army-commander-to-testify-before-peace-tribunal-in-february
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/general_mario_montoya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Archivo Particular] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[Mario Montoya Uribe, former commander of the National Army, will testify on the “false positives” scandal before the Special Peace Jurisdiction (JEP) on February 12, 2020, the transitional justice system recently announced.]

[According to the Attorney General’s Office, Uribe is investigated for the extrajudicial killings of Daniel Pesca Olaya, Eduardo Garzon Paez and Fair Leonardo Porras, who lived in Soacha. Human Rights Watch previously reported that at least 2,500 civilians were killed during his time as Army commander, from 2006 to 2008.]

[Montoya will also answer questions about human rights violations committed during Operation Orion in 2002, which left at least 100 people disappeared. Montoya led this operative in the Comuna 13 of Medellín.]

[The JEP has received 202 testimonies to date — 162 oral and 40 written — in the 003 case, which opens an investigation into the false positive cases. At least 11 testimonies have mentioned the former general’s roles in these crimes.]

[Other high-ranking officials have already testified, the JEP added, including retired General Paulino Coronado, former commander of the 30th Brigade, General Miguel David Bastidas, former second-in-command of the Artillery Battalion No. 4 and retired general Henry Torres Escalante, former commander of the 16th Brigade.]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
