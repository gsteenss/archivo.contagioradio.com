Title: ¿Qué hacer si considera injusta alguna sanción o procedimiento del código de policía?
Date: 2017-08-04 16:23
Category: DDHH, Movilización
Tags: código de policía, cultura ciudadana, derechos de ciudadanos, policías
Slug: que-hacer-si-considera-injusta-alguna-sancion-o-procedimiento-del-codigo-de-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/policia-represion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Youtube] 

###### [04 Ago 2017] 

La ciudadanía ha venido denunciando que el nuevo código de Policía incluye sanciones injustas e incomprensibles. Algunos representantes a la Cámara y Senadores de la República, **han iniciado una campaña de pedagogía para que las personas conozcan sus derechos** ante casos que supongan arbitrariedades y abusos de poder de los uniformados.

Para la Representante a la Cámara Ángela María Robledo, **“el código de Policía es represivo y no tiene espíritu de un código de buen comportamiento para el ciudadano”.** Según ella, hay más de 100 comportamientos que van a recibir multas desde los 92 mil pesos hasta los 800 mil pesos.

Ella fue enfática en manifestar que las multas tienen un **“enorme margen de arbitrariedad para el Policía** que va a estar frente a un ciudadano que en la mayoría de los casos será un habitante de calle, un vendedor ambulante o estudiantes de ciertas universidades y localidades que son a quienes les andan más duro”. (Le puede interesar: ["Las 26 demandas al código de Policía que debería resolver la Corte Constitucional")](https://archivo.contagioradio.com/las-26-demandas-sobre-el-codigo-de-policia-que-deberia-resolver-la-corte-constitucional/)

### **Derechos de los ciudadanos estipulados en el código de Policía** 

Robledo argumentó que en el código está estipulado que los ciudadanos pueden grabar cualquier procedimiento policivo en la medida que la grabación **“puede servir como prueba ante cualquier abuso o irregularidad”.** Igualmente, “será el inspector de Policía el que determine una infracción y el tipo de sanción”.

Adicionalmente, los ciudadanos podrán exigir compensar la multa **“vía justicia restaurativa** pues podrán realizar alguna tarea comunitaria para no tener que pagar la multa”. Un mecanismo adicional, que según Robledo la ciudadanía debe impulsar, es la creación de un comité que haga seguimiento a las actuaciones de los uniformados como lo son abusos de poder.

Robledo hizo énfasis en que “es necesario que se haga una campaña pedagógica que sirva para empoderar al ciudadano para que el código no sea una herramienta de confrontación ciudadana y que poco a poco se refuerce la tarea educadora, persuasiva y pedagógica de los Policías”. (Le puede interesar: ["6 cambios que la corte constitucional le ha hecho al nuevo código de policía"](https://archivo.contagioradio.com/corte_constitucional_cambios_codigo_policia/))

### **¿Qué hacer si considera que una multa es injusta?** 

Tenga en cuenta que el código de Policía contempla que un ciudadano puede apelar al artículo 222 parágrafo 1 cuando considere que la multa es injusta. "En contra de la orden de policía o la medida correctiva, procederá el recurso de apelación... y se remitirá al inspector de Policía dentro de 24 horas." **La norma establece que la apelación se resolverá en 3 días hábiles** y el ciudadano debe ser notificado.

De igual forma, es importante recordar que el nuevo código, en el artículo 21, establece que **las actividades que realicen los Policías son de carácter público** por lo que las personas tienen el derecho a grabar cualquier procedimiento. "La autoridad que impida la grabación sin justificación legal, incurrirá en causal de mala conducta".

### **“Dentro del código hay normas absurdas”** 

Robledo dijo que en redes sociales ella ha manifestado que hay condiciones que hacen imposible cumplir y limitar ciertos comportamientos. Puso como ejemplo **la multa por empujar al ingresar a un servicio público**. “En algunas estaciones de Transmilenio es imposible cumplir con ese comportamiento porque no hay otra forma de entrar”. (Le puede interesar: ["Corte Constitucional restringe los "super poderes" del código de Policía"](https://archivo.contagioradio.com/corte-constitucional-restringe-los-super-poderes-del-codigo-de-policia/))

Igualmente, afirmó que el tema de las multas a los colados en los sistemas públicos **es mejor resolverlo con cultura ciudadana** “para que acudir a la ley no se haga de manera represiva sino persuasiva”. Estos argumentos llevaron a la Representante a indicar que “el código no resuelve los problemas de comportamiento y no establece reglas de respeto y convivencia".

Finalmente, Robledo manifestó que **serán las alcaldías locales las encargadas del recaudo del dinero de las multas** “que se debe reinvertir en función de fortalecer las tareas de pedagogía para fortalecer la convivencia”. Agregó que la ciudadanía debe reclamar que haya transparencia en los recaudos y en los procedimientos que ejecutan los Policías.

<iframe id="audio_20173491" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20173491_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
