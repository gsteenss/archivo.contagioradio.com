Title: Profesores de universidades públicas protestan con huelga de hambre
Date: 2018-10-18 13:15
Author: AdminContagio
Category: Educación, Movilización
Slug: profesores-huelga-de-hambre
Status: published

###### [Foto: @pedagogicaradio] 

###### [18 Oct 2018] 

**Cuatro profesores** de diferentes universidades del país **iniciaron este lunes una huelga de hambre** exigiendo al Gobierno Nacional que otorgue mayor presupuesto para la educación superior pública. Los docentes realizan la acción como apoyo a las movilizaciones estudiantiles que se han desarrollado desde la semana pasada.

Los manifestantes son **Adolfo Atehortúa**, ex- rector y profesor de la Universidad Pedagógica Nacional (UPN); **Luis Aguirre**, profesor de la Universidad Industrial de Santander (UIS);  **Luis Marín**, de la Universidad del Quindío y **Juan Yepes** de la Universidad de Caldas; quienes iniciaron una huelga de carácter indefinido. (Le puede interesar: ["¿Por qué están marchando los estudiantes?"](https://archivo.contagioradio.com/marchando-los-estudiantes/))

El profesor Atehortúa había realizado una acción similar el año pasado, con un ayuno por 12 horas en la UPN, para rechazar actos de violencia que se habían cometido al interior de la institución. Durante 2017, año en el que finalizó su rectoría, Atehortúa denunció en repetidas ocasiones la difícil situación de la educación superior pública, y afirmó que **de no invertirse recursos, estaría en crisis para 2018 y en colapso para 2019.**

Para el docente, la crisis es hoy una realidad, y los estudiantes están marchando y exigiendo los recursos para cambiar esa situación; por eso, los 4 profesores decidieron hacer el ayuno permanente e indefinido invitando a la reflexión sobre la situación de las Instituciones de Educación Superior (IES), y **en apoyo del esfuerzo que están haciendo los estudiantes para tener una educación con calidad y mayor cobertura.**

### **Duque prometió un billón más para educación, pero se requiere más** 

Sobre la propuesta de Duque de agregar un billón de pesos al sector educación producto de las regalías por la explotación del petróleo, Atehortúa aclaró que estos recursos no son fijos, porque el Presidente dijo que habría que buscarlos, no que ya estuvieran listos. Además, recordó que el mandatario se refirió a la educación superior, y no a la educación superior pública; y concluyó que sería 1 billón para 2 años, es decir 500 mil millones por año.

Por consiguiente, lo que **el Presidente debería hacer es establecer una mesa de diálogo con los estudiantes para hablar de una crisis que "no se resuelve de la noche a la mañana"**, que requiere soluciones estructurales y adiciones que vayan a la base del presupuesto, y no aumentos extraordinarios ocasionales. (Le puede interesar: ["Profesores se suman a la defensa de la educación pública"](https://archivo.contagioradio.com/profesores-defensa-educacion-publica/))

<iframe id="audio_29426100" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29426100_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]

<div class="osd-sms-wrapper">

</div>
