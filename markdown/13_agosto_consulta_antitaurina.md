Title: Consulta Antitaurina en Bogotá se realizaría el 13 de Agosto
Date: 2017-05-19 12:30
Category: Animales, Nacional
Tags: Bogotá, Consulta Antitaurina, corridas de toros
Slug: 13_agosto_consulta_antitaurina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/CONSULTA-ANTITAURI-e1502312696803.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pulzo 

###### [19 May 2017] 

La Alcaldía de Bogotá anunció que el próximo 13 de agosto la ciudadanía tiene la posibilidad de salir a las urnas a votar la consulta antiaurina para decidir si se acaban o no las corridas de toros en Bogotá. La convocatoria se hace luego de que la Corte ordenó la legislación sin dilacioines.

Natalia Parra Osorio, directora de la Plataforma ALTO y promotora de la consulta antitaurina, asegura que celebra la democracia, sin embargo **preocupa que no se haya logrado un espacio de discusión previo con las organizaciones animalistas** promotoras de la iniciativa.

En ese sentido, los animalistas se muestran preocupados pues aunque los promotores habían solicitado al distrito que la consulta se realizara junto a las elecciones de marzo de 2018, de hacerse en menos de dos meses, se incurriría en costos extra, y no se está garantizado verdaderamente la participación ciudadana.  [(Le puede interesar: Animalistas solicitan consulta antitaurina para 2018)](https://archivo.contagioradio.com/animalistas_consulta_antitaurina_enrique_panalosa/)

### **Falta la Registraduría y el Ministerio de Hacienda** 

De acuerdo con Parra, **es muy difícil lograr umbrales por fuera de elecciones ordinarias, y por ende se esperaba que la alcaldía analizara la forma de que dicha consulta** se realizara en ese contexto de elecciones.

Por el momento los animalistas se encuentran a la espera del pronunciamiento de la Registraduría para saber si en menos de dos meses alcanza a organizar la logística de las votaciones, y también se espera el anuncio de parte del Ministerio de Hacienda frente al desembolso de los recursos.

Cabe recordar que son 1,9 millones de habitantes los que deben salir a las urnas para que la consulta popular sea válida, y según la Administración Distrital, **costaría \$35 mil millones. Nos obstante si se realizara en el 2018 los costos disminuirían casi en un 90%.**

###### Reciba toda la información de Contagio Radio en [[su correo]
