Title: A paro los sindicatos del INPEC
Date: 2017-05-11 12:48
Category: DDHH, Nacional
Tags: carcel, hacinamiento, INPEC
Slug: guardias-no-permitiran-ingresos-de-mas-reclusos-a-las-carceles-hacinamiento-supero-el-limite
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/4311d4fc83be00cbf3421dedd2c0b2b3_0-e1494524689528.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [10 Mayo 2017] 

A la jornada de paros que vive el país se suma la protesta de los trabajadores del sistema penitenciario y carcelario que completan 73 días de operación reglamento. **A partir de hoy no se permitirá el ingreso de más internos a las cárceles ni se realizarán traslados para diligencias judiciales.** Los únicos traslados se harán, serán por atención médica.

Según explica Horacio Bustamante, miembro de La Unión de Trabajadores Penitenciarios, la media se toma motivada por el hacinamiento de los recluidos, la falta de personal penitenciario y la corrupción que se vive en los centros carcelarios. Bustamante aseguró que **"a las cárceles ya no les cabe un alfiler".** En la Cárcel del Distrito Judicial de Villahermosa en Cali, por ejemplo, hay recluidas 6600 personas en una cárcel que tiene capacidad útil para tan solo 1600 presos. Le puede interesar: ["En las cárceles de Colombia hay pico y placa para poder dormir"](https://archivo.contagioradio.com/en-las-carceles-de-colombia-existe-pico-y-placa-para-poder-dormir/)

**Los trabajadores demandan la humanización del sistema para poder garantizarle a los recluidos una vida digna y un trabajo digno para los guardias.** Ante esto, los trabajadores piden que el Plan Reglamento que se acordó con el gobierno en el 2016 y que incluye las peticiones del gremio, se ponga en marcha. El Ministerio de Hacienda ya había dicho que no hay recursos suficientes para invertir en el sistema carcelario y penitenciario.

Pese a las alarmas que se desligan de los informes de 2017 que hizo *Prision Insider*, organización que se encarga de analizar las situaciones carcelarias del mundo, **las cárceles en Colombia son las que peores condiciones tienen en América Latina.** Le puede interesar:["La alarmante situación de las cárceles colombianas"](https://archivo.contagioradio.com/39173/)

Según la organización, en América Latina es cada vez más común que las comisarías de Policía sean usados como lugares de privación de la libertad. **Las bandas armadas son las encargadas del control de los establecimientos y hay una corrupción generalizada por parte del personal o de las pandillas que controlan la prisión.**

Según Bustamante, "la cárcel La Modelo en Bogotá cuenta únicamente con 2 guardianes por cada 1000 presos." Por esto solicitan que se incremente el número de personal y que se les garantice un trabajo digno.

![pi5\_amerique\_latine\_esp](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/pi5_amerique_latine_esp.jpg){.alignnone .wp-image-40375 width="611" height="436"}

###### [Foto: Prison Insider] 

### **¿CUÁLES SON LAS EXIGENCIAS DE LOS TRABAJADORES?** 

1\. Que se cumplan los acuerdos con el gobierno que están expuestos desde 2014.

2\. Que se garantice la vida y la integridad física de los internos y de los guardianes. El hacinamiento, que es el mal endémico del sistema carcelario y penitenciario restringe las posibilidades de proteger a los internos y pone en riesgo a los trabajadores.

3\. Que mejoren las condiciones laborales. De 24 horas que trabajan descansan la misma cantidad de horas porque no hay el personal suficiente.

4\. Que se amplíen los cupos del personal penitenciario.

Según Bustamante, "el Ministerio de Justicia considera que nuestras peticiones son extorsivas cuando solo estamos exigiendo que nos den garantías para trabajar".

<iframe id="audio_18632852" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18632852_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
