Title: Paro Nacional: una expresión ciudadana de contundente movilización
Date: 2019-11-23 14:04
Author: CtgAdm
Category: Nacional, Paro Nacional
Tags: Bogotá, Cali, Paro Nacional, Represión del ESMAD
Slug: paro-nacional-una-expresion-ciudadana-de-contundente-movilizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-21-at-12.53.42-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-23-at-1.51.05-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-23-at-1.57.58-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Desde el pasado 21 de noviembre miles de personas se unieron al Paro Nacional, saliendo a las calles de numerosas ciudades del país para exigir un cambio real y profundo en las medidas del gobierno Duque, y pese a que la represión por parte del **Escuadrón Móvil Antidisturbios (ESMAD)**, la Policía y los desmanes en diversos puntos de la ciudad han amenazado con desviar la atención de la intención de la movilización a través de actos de violencia,  la ciudadanía continúa manifestándose de forma pacífica.

Frente a los sucesos ocurridos desde el jueves, la **Comisión Interamericana de Derechos Humanos** se manifestó señalando que tenían conocimiento de  la detención de al menos 112 manifestantes y 40 personas heridas en Bogotá, debido al uso excesivo de la fuerza por parte de integrantes del ESMAD, reiterando que la acción de la Policía debe centrarse en la contención de actos de violencia, garantizando el derecho a la protesta.

Asimismo, la Campaña Defender La Libertad, a las 12:00 de la noche del 21 de noviembre, informó que se había reportado 230 casos de retenciones a ciudadanos y 24 policías heridos, situación que llevó a las autoridades a decretar el toque de queda en ciudades como Cali, Manizales, Pasto y Popayán, Allison Morales, representante estudiantil de la Universidad del Valle se refirió al uso desmedido de la fuerza asegurando que, "la presencia del Ejército y la Policía en las calles genera un desasosiego en las personas".

Según Jonathan Centeno, ciudadano de Popayán que se unió al paro, salieron a las calles cerca de 40.000 personas. No obstante, después de 10 horas de jornada pacífica, en el centro de la ciudad y debido a los enfrentamientos, se presentaron diversos desmanes que según los registros han dejado hasta el momento 3 personas desaparecidas, 18 detenidas y 27 heridas.

A los hechos de violencia que se presentaron, se debe sumar la numerosa evidencia que se ha conocido a  través de redes sociales, que muestra cómo agentes del ESMAD fomentaron afectación al orden público y la destrucción de propiedades.  [(Le puede interesar: Allanamientos a colectivos y jóvenes promotores del Paro Nacional)](https://archivo.contagioradio.com/allanamientos-a-colectivos-y-jovenes-promotores-del-paro-nacional/)

> [\#CaracolMiente](https://twitter.com/hashtag/CaracolMiente?src=hash&ref_src=twsrc%5Etfw) Porque no muestran estas cosas [@NoticiasCaracol](https://twitter.com/NoticiasCaracol?ref_src=twsrc%5Etfw) Ustedes le mientes al país. [pic.twitter.com/V3IVAocEIc](https://t.co/V3IVAocEIc)
>
> — Ingri Paola (@IngriPaola12) [November 22, 2019](https://twitter.com/IngriPaola12/status/1197948119364833280?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### Tras el cacerolazo, el paro continúa

Ese 21N en horas de la noche, la ciudadanía respondió a la represión con un cacerolazo que se escuchó en todo el país, desde las 8:00 pm hasta altas horas de la noche, "el cacerolazo fue una manera de responder de la ciudadanía, rechazando no solo al Gobierno sino a la situación en que se encuentra el país", manifestó Alexandra González del Comité de Solidaridad con los Presos Políticos. [(Le puede interesar: Especial sobre el Paro Nacional) ](https://archivo.contagioradio.com/paro-nacional/)

Posteriormente el 22 de noviembre, se registraron nuevos enfrentamientos entre manifestantes y unidades del ESMAD en la avenida Ciudad de Cali, el Portal de las Américas y la autopista sur, hasta el medio día, según la Campaña Defender La Libertad, se reportaron aproximadamente 50 retenidos en la ciudad de Bogotá. Además, se informó que en los puntos de concentración se presentaron heridos.

Desde las 4:00 pm de la tarde, los ciudadanos se congregaron una vez más en la Plaza de Bolívar y en el monumento de Los Héroes, pese a ser una expresión pacífica, una vez más el ESMAD hizo uso de los gases lacrimógenos reprimiendo la movilización. De igual forma, durante toda la jornada de movilización la ciudadanía denunció violaciones a derechos humanos en sectores como Kennedy, en donde gases lacrimógenos afectaron jardines infantiles y se generaron alteraciones al orden público con saqueos. La Alcaldía anunció la implementación de ley seca en Bogotá desde las 12:00 del medio día viernes hasta el sábado 23 de noviembre, además estableció el toque de queda en las localidades de Ciudad Bolívar, Kennedy y  Bosa desde las 8:00 pm y toda la ciudad a partir de las 9:00 pm.

### Se espera una respuesta sensata del Gobierno 

Tras la movilización del día jueves, en horas de la noche el presidente Iván Duque se dirigió al país a través de un pronunciamiento en el que pese a la jornada de movilización pacífica que se vivió y el mensaje de rechazo con relación a sus políticas, únicamente se refirió a los actos aislados de vandalismo.

Al respecto, el senador Antonio Sanguino quien hace parte de la Gran Mesa Nacional de Concertación manifestó que ante el paro, "el presidente no se pronunció de fondo y aún no ha sabido escuchar esa voz que insiste en las equivocaciones del Gobierno en materia de sus propuestas".

Por su parte Centeno expresó que "la palabra que pronunció no la práctica, afirmó que nos escuchó pero no escuchó las solicitudes que hicimos con anterioridad así que acompañamos que exista una reunión con el presidente, pero estamos convocado más que al presidente a la población".

Finalmente, la Central Unitaria de Trabajadores manifestó mediante un comunicado que "esta amplísima expresión de inconformidad y rebeldía desarrollada de forma pacífica, tendrá que asimilarla el gobierno nacional", frente a ello, declararon, además, que permanecerán pendientes a los diálogos con Presidencia pero prestos a desarrollar nuevas acciones en la calle si por parte del Gobierno se mantiene "la desatención a los reclamos".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
