Title: Camioneros se unen al paro de Buenaventura
Date: 2017-05-24 12:53
Category: Movilización, Nacional
Tags: Camioneros, movilizaciones Buenaventura, Paro Buenaventura
Slug: camioneros-se-unen-al-paro-de-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/paro_camionero-e1495648397353.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Patria ] 

###### [24 may 2017]

Ante los incumplimientos del gobierno a los bonaverenses, el gremio de camioneros que viajan hacia Buenaventura, para hacer trabajos de carga y descarga, manifestaron que se **unirán en solidaridad al paro para exigirle al Presidente que se siente a dialogar con la población.**

Oscar González, líder camionero de Finalco, afirmó que no van mover los vehículos “hasta que el gobierno le cumpla a Buenaventura todos los acuerdos”. Los camioneros han manifestado que así como fueron engañados los pobladores de Buenaventura, ellos también han sido engañados y que además **el impacto de la problemática que vive esta región del país también la sienten ellos.** Le puede interesar: ["No levantaremos el paro si no se decreta la emergencia: Buenaventura"](https://archivo.contagioradio.com/no-levantaremos-el-paro-si-no-se-decreta-la-emergencia-buenaventura/)

González dijo que “cuando llegamos a Buenaventura no podemos consumir agua potable porque no hay, por eso creemos que las causas de las manifestaciones **son justas y las tenemos que apoyar”.** De igual forma aclararon que no es un paro de camioneros si no que se unen al paro en solidaridad con Buenaventura.

En el paro que completa ya 9 días, 5 mil vehículos de carga pesada se encuentran parados en la zona de Lobo Guerrero y Media Canoa en el Valle del Cauca. El gremio ha dicho que resistirán en el paro hasta que **“el gobierno se amarre los pantalones y les cumpla las promesas de campaña que llevan años sin cumplirse”.** Le puede interesar: ["Buenaventura sigue en paro pese a represión, militarización y toque de queda"](https://archivo.contagioradio.com/buenaventura-sigue-en-paro-a-pesar-de-la-represion-la-militarizacion-y-el-toque-de-queda/)

<iframe id="audio_18883427" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18883427_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
