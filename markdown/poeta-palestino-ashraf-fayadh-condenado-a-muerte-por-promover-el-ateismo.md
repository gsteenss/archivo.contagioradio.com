Title: Poeta palestino Ashraf Fayadh condenado a muerte por "promover el ateísmo"
Date: 2015-11-23 14:03
Category: DDHH, El mundo
Tags: Ashraf Fayadh poeta palestino, Mona Kareem activista derechos humanos, Organización Edge of Arabia, Poeta palestino condenado a muerte
Slug: poeta-palestino-ashraf-fayadh-condenado-a-muerte-por-promover-el-ateismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/poeta-e1448304894393.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [23 Nov 2015]

Un tribunal saudí condeno a muerte al poeta palestino Ashraf Fayadh, por el contenido de sus trabajos con los que según el ente acusador propaga "el ateísmo y los pensamientos destructivos en la sociedad", en particular por lo consignado en su libro "Instructions Within" publicado en 2008.

En una entrevista con el diario británico "The Guardian", el  poeta de 35 años manifesto encontrarse en estado de "Shock" ante la decisión, pero asegura que estaba dentro de lo esperado "aunque no hice nada que merezca la pena de muerte", sentencia que podrá apelar en los próximos 30 días y así salvar su vida.

El historial de detenciones a Fayadh, inició en agosto de 2013 luego de la denuncia de un lector quien sostenía que su obra "promovia el ateismo", acusación de la que se libró tras pagar fianza al día siguiente. En enero de 2014, Fayadh volvería a prisión donde la Mutaween, policía religiosa saudí, le retiró su documento de identidad.

En mayo de 2014, y en medio de una serie de irregularidades incluyendo la ausencia de representación legal y el cambio del juez y el fiscal del caso,  fue condenado en una primera sentencia a cuatro años de prisión y 800 latigazos, según denuncia la activista por los derechos humanos Mona Kareem, quien lideró una petición para la liberación del poeta.

“Condenamos los actos de intimidación hacia Ashraf Fayadh como parte de una campaña mayor, incitando al odio contra los escritores y utilizando el Islam para frenar la libertad de expresión”, se puede leer en la petición que fue firmada por varios nombres de la cultura saudí.

Amigos cercanos al poeta, aseguran que la razón real de su detención, sería un video hecho por este donde se denuncia el maltrato con latigazos por parte de la policía a un hombre en público, así como por su apariencia (cabello largo) y por fumar. Para Kareem también estaría relacionada con la discriminación hacia los refugiados como lo es Fayadh.

“El libro al que se refieren hablaba simplemente de mí experiencia como refugiado Palestino, sobre temas culturales y filosóficos. Pero los extremistas religiosos han considerado que eran ideas destructivas en contra de dios" asegura el escritor, quien además ha sido curador de exposiciones en la Bienal de Venecia y en Jeddah y es miembro de la organización artística británico-saudí, "Edge of Arabia".

El caso del poeta se suma al de otros como el bloguero Raif Badawi, condenado a 10 años de prisión y 1.000 latigazos por insultar el Islam. Bajo el título "Amnesty international : Save the palestinian poet and artist Ashraf Fayadh" se ha creado en red una [petición](https://secure.avaaz.org/en/petition/Amnesty_international_Save_the_palestinian_poet_and_artist_Ashraf_Fayadh/) por su liberación.

##### Con información de www.elespañol.com
