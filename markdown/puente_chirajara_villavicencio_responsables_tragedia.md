Title: ¿Quién responde por la tragedia del puente Chirajara?
Date: 2018-01-16 13:26
Category: Nacional
Tags: Germán Vargas Lleras, Luis Carlos Sarmiento Angulo, Puente Chirajara, Santos, Villavicencio
Slug: puente_chirajara_villavicencio_responsables_tragedia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/56b40627dcce0.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Portafolio] 

###### [16 Ene 2018] 

Mucho se ha hablado de la tragedia que consume a nueve familias que perdieron a sus seres queridos por el derrumbe del puente Chirajara, pero poco se ha mencionado sobre los posibles responsables. Por el momento, el Gobierno Nacional pide una respuesta clara de Coviandes, sin embargo, desde otros sectores se señala que la responsabilidad también recae sobre el ex vicepresidente Germán Vargas Lleras y el grupo Sarmiento Angulo.

Al puente solo le faltaban 20 metros de construcción, pero el lunes el **costado occidental de la construcción ubicada en la vía Bogotá-Villavicencio, **se desplomó al medio día de este lunes y cayó al abismo de 280 metros, dejando nueve personas muertas, ocho heridas y otras tres aún se encuentran debajo de los escombros.

**Giovany Monroy Zapata, Jaír Alexánder Castro, Jorge García Beltrán, Jesús Elkin Salas, Alberto Antonio Calle Ortiz, Julio Roberto Salgado Alfonso, Gildardo Jiménez, Giovany Quiroz y José Álvaro Bertel,** son los nombres de las víctimas.

La tragedia pudo ser peor ya que en la obra trabajaban alrededor de 200 empleados, y en ese momento en el lugar se encontraban 150 trabajadores. Según las autoridades y los testigos, usualmente son cerca de 75 personas las que desarrollan sus actividades en ese tramo del puente, pero a esa hora solo se encontraban 20, pues muchos estaban en su hora de almuerzo.

### **Vargas Lleras, Santos y Sarmiento** 

El CTI de la Fiscalía es el encargado de investigar y hallar las causas de semenjante tragedia que hoy enluta a al menos nueve familias. El Ministro de Transporte, Germán Cardona dijo en una rueda de prensa **que quien debe responder es el concesionario, es decir Coviandes, quienes deberían "cubrir los costos del puente y el apoyo a las familias de las víctimas"**

Asimismo, la Fiscalía deberá investigar la forma como se llevaron a cabo los diseños por parte de la** firma Área,** y también deberá estudiar la responsabilidad que pudiera tener la **constructora Gisaico**.

Mientras tanto lo único claro es que quienes hace unos meses salían orgullosos de los avances de las obras de la doble calzada Bogotá - Villavicencio, hoy poco se han manifestado. Luis Carlos Sarmiento Angulo, **principal accionista del concesionario; **Germán Vargas Lleras, y el presidente Santos, en repetidas ocasiones se tomaron fotos en la inauguración de algunos de los tramos de la construcción.

Germán Vargas Lleras, cuando fue vicepresidente, sobrevoló la zona de las obras acompañado de la Ministra de Transporte de ese entonces, Natalia Abello Vives, el presidente del Grupo Aval, Doctor Luis Carlos Sarmiento Angulo, el Gobernador de Cundinamarca, Álvaro Cruz Vargas, el Gerente General de Coviandes, entre otras personalidades de carácter nacional, cuando la mega obra alcanzaba una ejecución del 65%.

“De todas las obras en Colombia esta es la que ha demandado mayor tecnología y más avances en materia de ingeniería”, fueron algunas de las declaraciones de Vargas Lleras. Además en las publicaciones de Instagram del hoy candidato a la presidencia, en las que saca pecho por su labor en la vicepresidencia y en el Ministerio de Vivienda, tiene una imagen con Santos y Sarmiento Angulo cuando fue inaugurado un tramo de la calzada.

[![foto lleras](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/foto-lleras-401x600.png){.wp-image-50562 .size-large .aligncenter width="401" height="600"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/foto-lleras-e1516125691497.png)

Por otra parte, hace apenas dos meses el presidente Juan Manuel Santos entregaba dos nuevos tramos en operación en la doble calzada Bogotá-Villavicencio. Ese día, el mandatario recorrió en un vehículo junto con el empresario Luis Carlos Sarmiento Angulo, la nueva calzada, y **aprovechó para felicitar al empresario por la "calidad" de las obras adelantadas**. "No hay nada que envidiarles a los túneles y puentes en el mundo", expresó en su momento Santos.

https://www.youtube.com/watch?time\_continue=1&v=3hGzlu-ADBY

El  Gobierno había suscrito el acta de inicio de obras para el tramo entre Chirajara y Villavicencio, con una inversión de \$2,1 billones. Allí se construirían 22,6 kilómetros más de doble calzada, dentro de ese monto, el tramo en el que perdieron la vida los trabajadores, tuvo una inversión cercana a los **\$72.000 millones, lo que quiere decir que al menos la mitad de ese dinero también se fue al abismo.**

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU)
