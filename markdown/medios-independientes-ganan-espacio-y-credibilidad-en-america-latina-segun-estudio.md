Title: Medios independientes ganan espacio y credibilidad según estudio
Date: 2017-08-01 14:04
Category: El mundo, Otra Mirada
Tags: Informe de Medios, Internet, Medios independientes
Slug: medios-independientes-ganan-espacio-y-credibilidad-en-america-latina-segun-estudio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/adult-1867751_1280.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pexels] 

###### [01 Ago. 2017]

Según un estudio publicado por SembraMedia en Asocio con Omidyar Network llamado *Punto de Inflexión* a hoy el **crecimiento, el impacto y las amenazas para los medios de comunicación independientes en América Latina siguen en aumento**. Según este informe que consultó 100 medios de comunicación en Argentina, Brasil, Colombia y México si bien estos son cada vez más influyentes por los temas que trabajan pero se ven expuestos a amenazas y presiones.

**Felipe Estefan, director para América Latina de Omidyar Network** dijo que “hicieron un estudio y un análisis de los datos recogidos de manera profunda y con base a eso crearon el reporte que muestra que estos medios están mostrando, descubriendo y reportando actos de corrupción, temas de DD.HH. y con su voz independiente están llegando a más personas”.

### **Medios alternativos censurados y autocensurados** 

El trabajo que están realizando los medios alternativos o digitales, según los datos arrojados por este informe lo hacen a un alto precio que va desde **ataques cibernéticos, censura para poder conseguir financiación por los temas trabajados** o autocensura al recibir amenazas, que en algunos casos resultan con el asesinato de los y las periodistas.

“Por ejemplo, en Colombia el 80% de los medios a los que entrevistamos dicen haber sufrido ataques cibernéticos, **casi dos tercios dijeron haber sufrido algún tipo de acto de violencia o amenaza,** entonces uno entiende porque eso hace que los medios, particularmente en zona rural y fuera de las capitales, tengan que pensar dos veces antes de hacer reportajes investigativos”. Le puede interesar: [Medios alternativos: víctimas de allanamientos en Argentina](https://archivo.contagioradio.com/medios-comunitarios-allanados-en-argentina34309/)

### **¿Por qué hay tantos medios digitales si existen tantas barreras?** 

Para Estefan, los periodistas que trabajan en estos medios **tienen una pasión muy grande por hacer periodismo**, sienten que este oficio es una parte esencial de la democracia y que a través de los medios digitales pueden denunciar lo que hace, por ejemplo, los gobiernos al no estar financiados por ellos. Le puede interesar: [Tres grupos económicos tienen la mayoría de los medios de comunicación en Colombia](https://archivo.contagioradio.com/tres-grupos-economicos-tienen-la-mayoria-de-los-medios-de-comunicacion-en-colombia/)

“El contar las historias para informar a la ciudadanía y asegurarse que monitorean las actividades de aquellos que están en el poder, es un rol importante de estos medios. En América Latina no hemos tenido la oportunidad de escuchar voces independientes en parte porque los medios están mantenidos por las personas más importantes y poderosas de los países”.

Además, dice que de las entrevistas pudieron deducir que **los periodistas han entendido que juegan un papel muy importante en las sociedades,** en el fortalecimiento de las democracias de las cuales son parte y se sienten tan apasionados de jugar ese papel que “están dispuestos a luchar contra los retos y los ataques que reciben”.

### **Aún es difícil para los medios libres acceder a financiación** 

Muchos de los medios en los que trabajan periodistas independientes que buscan cubrir los temas críticos que enfrentan los distintos países de la región están accediendo a un público mayor y están desarrollando negocios rentables y sostenibles de impacto.

**Más de un 70% de las empresas incluidas en este estudio iniciaron sus operaciones con menos de US\$10.000**, y un 12% ya está generando al menos medio millón de dólares al año en ingresos. Sin embargo, aún muchos no pueden sostenerse dada la falta de financiación a la que pueden acceder.

“Las cifras nos demuestran que hay una brecha muy significativa aún en los medios que se crean y los medios que verdaderamente tienen un éxito comercial y en parte es porque el sector de los medios de comunicación está teniendo que experimentar con muchos modelos de generación de recursos para asegurar su independencia editorial”. Le puede interesar: [Amenazan a medios de comunicación alternativa en Colombia](https://archivo.contagioradio.com/amenazan-a-medios-de-comunicacion-alternativa-en-colombia/)

### **Casi 40% de quienes han fundado medios alternativos digitales son mujeres** 

Economía Femini(s)ta en Argentina, Genero e Número de Brasil, Contagio Radio y Mprende en Colombia y Aristegui Noticias en México son algunos de los medios encuestados en los que sus fundadoras fueron mujeres o que dentro del equipo contaron con la participación femenina.

“Lo que esto demuestra es que las mujeres no han tenido tradicionalmente un rol de liderazgo en los medios. Aun cuando han tenido grandísimo talento y han podido demostrar mucho éxito como periodistas y administradoras no siempre han podido ascender. Por ello, **las mujeres están liderando la creación de estos medios independientes”.**

La cifra del 40% hace referencia a los medios en los que han contado con una única mujer como fundadora, además el 60% tienen por lo menos 1 mujer como fundadora en sus equipos. Le puede interesar: [“Medios de comunicación deben reflejar la diversidad y la realidad de las mujeres”](https://archivo.contagioradio.com/medios-de-comunicacion-deben-ser-reflejo-de-la-diversidad-de-mujeres/)

### **¿Claves para generar mayores impactos?** 

Dice Estefan que es importante que los medios de comunicación independientes continúen jugando el papel que han estado jugando hasta ahora, **involucrando a sus audiencias en la creación de contenidos,** permitiendo a un grupo más diverso de periodistas acceder a las perspectivas del tipo de historias que están contando.

Por su parte, en términos de creación de empresa lo que se debe hacer es que los medios independientes puedan **seguir explorando y experimentando con nuevas formas de generación de recursos** “ya sea ayudando a manejar contenidos editoriales, creando eventos, haciendo campañas con las audiencias, entre otras estrategias”.

### **Resultados para Colombia** 

![Infografia Medios](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Infografia-Medios.png){.alignnone .size-full .wp-image-44523 width="807" height="585"}

<iframe id="audio_20112869" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20112869_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]{.s1}
