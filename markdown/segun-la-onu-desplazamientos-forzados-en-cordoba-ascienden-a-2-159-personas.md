Title: Según la ONU, ya son 2.159 personas desplazadas forzadamente en Córdoba
Date: 2019-03-30 19:24
Category: Comunidad, DDHH
Tags: Desplazamiento Forzoso, Sur de Córdoba
Slug: segun-la-onu-desplazamientos-forzados-en-cordoba-ascienden-a-2-159-personas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/desplazamiento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [30 Mar 2019] 

El más reciente informe de la La Oficina de la ONU para la Coordinación de Asuntos Humanitarios (OCHA) corrobora, que a raíz de los enfrentamientos entre disidencias de las FARC y el Clan del Golfo, el número de personas desplazadas a la fuerza  de las veredas Santa Rosa en Puerto Libertador, Córdoba y La Flecha en Ituango, Antioquia aumentó a 732 familias y 2159 personas desde el 22 de marzo.

Acorde a la Personería de Puerto Libertador, las familias desplazadas se han trasladado a un total de nueve veredas: Santa Rosa, La Flecha, Soledad, Santa Bárbara, Rogero y Río Sucio, Jagua, Mutatá y el Resguardo indígena Cañaveral  y han llegado al casco urbano del corregimiento de Juan José.

Tal como señala el informe, Juan José no cuenta con un albergue para emergencias de dicha magnitud lo que dificultad la situación de las familias quienes se están quedando en la casa parroquial,  la casa indígena del corregimiento y en hogares de familiares o conocidas, mientras quienes continúan llegando se han ubicado en albergues improvisados.

Según el informe, "desde agosto de 2011 no se registraba un desplazamiento de tantas personas", en aquella época más de 1.300 personas se desplazaron de siete veredas en Tierralta-Córdoba, como consecuencia de una masacre perpetrada por Grupos Armados. [(Lea también Desplazamiento forzado y aumento en la violencia: Las preocupaciones del CICR Colombia)](https://archivo.contagioradio.com/desplazamiento-forzado-cicrlombia/)

### **Necesidades** 

Las familias desplazadas se encuentran en una situación de hacinamiento y  no cuentan con colchonetas, ni cobijas, además están continuamente expuestas al sol y al agua, no cuentan con iluminación nocturna y en muchos casos no cuentan con acceso a agua potable y elementos de aseo personal.

![WhatsApp Image 2019-03-30 at 12.27.23 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-30-at-12.27.23-PM-646x800.jpeg){.alignnone .size-medium .wp-image-63898 width="646" height="800"}

De igual forma es necesaria la atención psicosocial para las familias quienes han evidenciado altos niveles de ansiedad, tristeza y llanto, mientras que algunos corren el riesgo de contraer enfermedad transmitidas por la proliferación de mosquitos; también se han identificado brotes aislados de paludismo, casos de diarrea, dolores de cabeza y estomacales, además se requiere atención inmediata para mujeres en estado de embarazo.

### **Contaminación de minas antipersona** 

Debido al accionar de los grupos armados en todos los municipios del sur de Córdoba, se ha presentado una alta contaminación con minas antipersona precisamente en las veredas de las que se desplazaron las personas, como precedente está el accidente que sufrió un habitante de la vereda Rogero quien resultó herido mientras el animal domestico que lo acompañaba falleció tras pisar la mina.

A partir del primero de abril, diversos conglomerados como la Diócesis de Montelíbano en coordinación con la Diócesis de Santa Rosa de Osos y Cáritas Colombiana, el Programa Mundial de Alimentos y La Unidad de Atención y Reparación Integral de Víctimas junto a la Alcaldía municipal y el Ejército harán parta de la entrega de ayuda humanitaria a la población, repartiendo mercados, objetos de aseo y suministro de agua.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
