Title: Tenga en cuenta estas recomendaciones para ejercer su derecho a la protesta
Date: 2019-10-31 15:00
Author: CtgAdm
Category: Entrevistas, Movilización
Tags: Derechos Humanos, ESMAD, estudiantes, protesta
Slug: tenga-en-cuenta-estas-recomendaciones-para-ejercer-su-derecho-a-la-protesta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Este jueves el movimiento estudiantil se movilizará en más de 20 ciudades del país para expresar su rechazo a la actuación del Escuadrón Móvil Antidisturbios (ESMAD) durante la protesta, y exigir el respeto de los recursos para la educación superior pública. Las y los estudiantes han denunciado que a lo largo de las últimas semanas, en el marco de la protesta social, se han cometido irregularidades y procedimientos que no respetan los derechos de los y las ciudadanas por parte de la fuerza pública, Contagio Radio habló con una defensora de DD.HH. que dió **pistas para el autocuidado durante las manifestaciones.**

### **"Ejerzan su derecho a la protesta sin miedo, no tenemos por qué tener miedo"** 

Valentina Ávila, integrante de la Red Distrital Universitaria de Derechos Humanos señaló que para marchar se deben tener en cuenta algunos elementos claves: Asistir a las manifestaciones con ropa cómoda para poder desplazarse de manera fácil, llevar el celular cargado para poder comunicarse y tener documentos de identificación. Adicionalmente, recomendó la creación de triadas, es decir, grupos de tres personas que comparten sus contactos y estarán juntos en las marchas para velar por su seguridad.

"Una recomendación es no consumir bebidas alcohólicas ni sustancias psicoactivas, para cuidar las vida e integridad de las personas", porque dichos consumos reducen la capacidad de reacción y pone a las personas en situación de vulnerabilidad, además que "puede ser una causal para que la fuerza pública genere traslados por protección", señaló Ávila. Ante cualquier intervención de la fuerza pública, la activista recomendó no correr, e identificar a las personas defensoras de derechos humanos, para que sean ellos quienes corroboren la situación.

Y en caso de cualquier agresión por parte de los uniformados,  identificar la placa de los policías (que deben ser los mismos números en su casco y chaleco) para hacer las respectivas denuncias. La defensora de DD.HH. resaltó que en caso de presenciar procedimientos irregulares, el Código de Policía permite grabar con video el hecho, y sostuvo que los y las jóvenes deben ejercer su derecho a la protesta sin miedo. (Le puede interesar: ["ESMAD usó material no convencional y violó protocolos en movilizaciones estudiantiles"](https://archivo.contagioradio.com/esmad-uso-material-no-convencional-y-violo-protocolos-en-movilizaciones-estudiantiles/)")

### **En caso de ser llevado a un Centro de Traslado por Protección (CTP)...** 

En caso de que uniformados realicen traslados de personas por protección, Ávila dijo que **se debía buscar a un defensor de DD.HH. o una persona del Ministerio Público** (es decir, integrantes de la Personería de Bogotá identificados con chaquetas rojas) para registrar el procedimiento. Si no se encuentra este apoyo, este procedimiento también se puede grabar, así como al funcionario que lo realiza. (Le puede interesar: ["Nuevamente el ESMAD olvida que tiene que cumplir protocolos de intervención"](https://archivo.contagioradio.com/esmad-olvida-que-tiene-protocolos-intervencion/))

La integrante de la Red Distrital Universitaria de DD.HH. explicó que el gran problema en estas situaciones es que "tenemos un Código de Policía laxo, que es ambiguo sobre las razones para hacer un movimiento al Centro de Traslado por Protección" (antes llamados UPJ). En ese sentido, es posible que se produzcan detenciones arbitrarias, en cuyo caso, las personas serán trasladadas al Centro de Atención Inmediata (CAI) más cercano, donde se revisará el supuesto material probatorio y se identificará si alguno de los detenidos incurrió en algún delito.

Ávila manifestó que las personas que pasen por este proceso tienen derecho a permanecer en silencio, "no tiene porque responder a indagatorias o provocaciones"; como ciudadanos tienen la responsabilidad de identificarse, pero para hacerlo, pueden presentar un documento que tenga foto y número de cédula. No obstante, la defensora de DD.HH. llamó a mantener la calma, puesto que la Fuerza Pública no suele tener material probatorio para sus detenciones, entonces "lo que hacen es quitar algunas horas de su tiempo para amedrentarlos, ese es el objetivo fundamental".

Para agilizar la salida de los CTP, Ávila pidió que las personas detenidas comunicarse con sus familias y confíar en el trabajo que realizan los defensores de DD.HH. y el Ministerio Público. En el caso de que se presenten una eventual judicialización, las personas tendrían que ser presentadas ante un juez de Control de Garantías en un periodo no mayor a 36 horas, en cuyo caso, se puede acudir a colectivos de  abogados para ser asesorados, pero la Activista recordó que "estos procedimientos nunca culminan porque no suele haber pruebas".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44016606" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44016606_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
