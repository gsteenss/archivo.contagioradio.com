Title: Asesinan a Luis Alfonso Giraldo, líder social de La Carmelita, Putumayo
Date: 2017-12-04 17:14
Category: DDHH, Nacional
Tags: asesinato de líderes sociales, ETRC Ariel Aldana, Putumayo, Tumaco
Slug: asesinan_luis_alfonso_giraldo_camelita_putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/la-carmelita.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  La FM] 

###### [4 Dic 2017] 

De acuerdo con información de la Red de Comunicadores Populares del Sur, en la madrugada de este lunes fue asesinado el líder social Luis Alfonso Giraldo en el corregimiento de La Carmelita,  Puerto Asís en el departamento de Putumayo. Según las primeras versiones que maneja dicho medio, encapuchados que llegaron a la comunidad habrían **le habrían disparado a Giraldo, y además habían amenazado a otro líder social de la zona.**

Se trata del **presidente de la Junta de Acción Comunal de la vereda La Brasilia ubicada en el corregimiento La Carmelita.** Aunque las comunidades y las organizaciones sociales de la zona continúan sin conocer detalles del hecho, evidencian mucha preocupación por la situación de inseguridad que se vive en la zona, y especialmente piden que garantías para los líderes sociales de la región.

Ante el hecho señalan que la zona donde fue asesinado el líder social tiene el control territorial la Policía y el Ejército Nacional, dada la proximidad con el Espacio Territorial para la Capacitación y la Reincoporación La Carmelita, en donde se agruparon los excombatientes de los frentes 32, 48 y 49 de las FARC.

### **Asesinan a un joven en ETCR Ariel Aldana en Tumaco** 

Además del asesinado de Luis Alfonso, en Tumaco, en el Espacio Territorial Ariel Aldana,  este fin de semana **fue asesinado el joven Richard Paui Canticus** cuando se llevaba a cabo una reunión con la comunidad de La Variante y la Cooperativa de Vivienda 24 de mayo.

Así lo denunció un comunicado de la ETCR Ariel Aldana donde aseguran que los pobladores de la zona escucharon dos disparos aproximadamente a 30 metros de la puerta de donde se realizaba esa reunión, que también se llevaba a cabo cerca al Ejército y a la Policía.

“Es inaudito que en pleno centro poblado, **frente a toda la comunidad, y en el mismo lugar donde Ejército y Policía se encuentran custodiando la vía,** que por lo demás es una ruta absolutamente militarizada, se cometa tan horrendo homicidio a plena luz del día”, dice el comunicado.

En ese sentido exigen una mayor atención por parte de las autoridades de la zona, y que se preste el apoyo social y humanitario a la región y no solo  pie de la fuerza pública. “Llamamos al Estado colombiano a sus fuerzas militares, policiales, organismos judiciales y de la administración pública a activar las alarmas y adoptar acciones concretas e integrales a la tan compleja situación de seguridad de quienes verdaderamente le apostamos a la paz en todo el territorio nacional”, señalan.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
