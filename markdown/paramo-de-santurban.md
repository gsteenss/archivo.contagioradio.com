Title: ¡No se vende! Hasta Bogotá llegaron para defender el Páramo de Santurbán
Date: 2019-10-17 15:38
Author: CtgAdm
Category: Ambiente, Movilización
Tags: Alberto Carrasquilla, Audencia Publica, Páramo de Santubán
Slug: paramo-de-santurban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/PARAMO-DE-SANTURBAN-e1461876510933.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Paramo-de-Santurban-e1472756604235.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Páramo-de-Santurbán-foto-de-Jorge-William-Sánchez-Latorre-e1498070474455.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/paramo-de-santurban.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/SANTURBAN-Redacción-Pares.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/paramo-de-santurban-.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Iván Marulanda] 

Este 17 de octubre defensores ambientalistas y habitantes del departamento de** Santander volvieron a las calles en Bogotá para expresar su rechazo en contra de la licencia ambiental otorgada a la empresa minera árabe Minesa**, en el Páramo  de Santurbán desde el 2015, que se ubicaría por encima de las bocatomas del acueducto metropolitano de Bucaramanga, afectando a ecosistemas, habitantes de zonas rurales y de la ciudad.

### Una Audencia Publica de Páramos

Asimismo, la movilización buscó hacer un llamado a las instituciones ambientales como  la Autoridad Nacional de Licencias Ambientales para que en la** audiencia pública sobre delimitación de Páramos**, se comprometieran a realizar un espacio abierto con la comunidad en donde se expongan las consecuencias de la licencia sobre el Páramo de Santurbán en el marco del debate de control político que se le realizará la primera semana de noviembre a Rodrigo Suarez, director de de la ANLA y al ministro de Ambiente, Ricardo Lozano.

Mayely López, vocera del Comité por la Defensa del Páramo, expresó que la licencia sería  “**un acto administrativo con el cual se pretende fraccionar las montañas y las delimitaciones territoriales de zonas protegidas**" y afirmó que "a este acto, que supuestamente tendría que salir en diciembre, todavía le faltan estudios de impacto de mitigación y estudios a perpetuidad sobre cuáles serían las aguas subterráneas afectadas o  en general las consecuencias para el ambiente". (Lea también: [No se puede fraccionar el Páramo de Santurbán&\#187;: Comité para la Defensa](https://archivo.contagioradio.com/no-se-puede-fraccionar-paramo-santurban-comite-la-defensa/))

La movilización también rechazó la designación de Alberto Carrasquilla, ministro de Hacienda, como representante ministerial ad hoc para emitir un concepto al interior del Consejo Técnico Consultivo de la ANLA sobre el destino del Páramo de Santurbán, ya que para ambientalistas Carrasquilla estaría protegiendo los intereses de la multinacional Minesa y el proyecto de exploración minera de oro y cobre denominado Soto Norte, que se desarrollaría entre los municipios de California y Suratá. (Le puede interesar también: [Razones por las que Carrasquilla no es idóneo para decidir sobre Santurbán](https://archivo.contagioradio.com/razones-de-los-santandereanos-no-a-carrasquilla/))

### Una mirada hacia las Comunidades de Santurbán

La activista agregó que **las delimitaciones de los páramos se deben hacer “con la mirada de las comunidades afectadas”** porque finalmente, aún si Minesa está hablando de garantías laboral y de enriquecimiento de estructuras por el bienestar de las comunidades con una ganancia en termino de regalías del 0,2%, permanecerían las afectaciones ambientales sobre las cuales no hay medidas de recuperación por parte de las autoridades institucionales.

**“¿De verdad queremos realizar una afectación ambiental permanente para una ganancia que no va a mejorar realmente la calidad de vida de las comunidades, en cambio desempeñar ganancias por el bienestar de unos pocos que incluso se la llevan en otro país?”**

Finalmente la activista afirmó: “seguiremos movilizándonos las veces que sean necesarias para garantizar la protección, no solo del Páramo de Santurbán sino de todos los territorios que necesitan la defensa por parte de la movilización cívica porque ese tema es algo que nos afecta a todos”.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_43274598" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_43274598_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
