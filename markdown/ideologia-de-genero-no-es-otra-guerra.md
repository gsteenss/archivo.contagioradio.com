Title: No es otra guerra
Date: 2016-10-28 13:34
Category: Opinion
Tags: ideologia de genero, mujeres
Slug: ideologia-de-genero-no-es-otra-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/ideologia-de-genero-no-es-otra-guerra-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### [Por [**Ximena Correal**  @](https://twitter.com/_Surrealista_)[[\_Surrealista\_](https://twitter.com/_Surrealista_)

###### 28 Oct 2016

["]*[A todos los ateos, gays, lesbianas, ambientalistas extremos, feministas, evolucionistas, Cristo les invita a que asistan este domingo a la iglesia cristiana más cercana para empezar una nueva vida en Cristo. Porque llegará el día en el que Dios pondrá a su diestra a los salvados y a la izquierda – si a la izquierda – a los malditos. La derecha evangélica llegó a Colombia y llegó para quedarse]*[", escribió en Las 2 Orillas el pastor Alejandro Ortiz. Leí estas palabras como una “condena”, una amenaza, una sentencia de muerte de alguien que parece estar más cerca de Dios y que se encarga de intimidar con sus palabras a quienes desde su perspectiva, no lo están o ni siquiera lo contemplan.]

[Por estos días la tan sonada “**ideología de género**” no ha hecho sino continuar con la campaña desinformativa contra los derechos humanos de las mujeres. Como si las feministas tocáramos de casa en casa llevando el Segundo Sexo de Beauvoir: “Señora, ¿Ha leído usted el Segundo Sexo?, ¿tiene tiempo y le cuento de qué se trata?” O como si nos ubicáramos en las estaciones de los alimentadores de Transmilenio ofreciendo libros para “ideologizar” a sus niños y enseñarles el correcto camino: “¿Triste? ¿Deprimido? Aquí le ayudamos a encontrarse”; o peor, como si le dijéramos al presidente que hay que “purgar” el país de la gente que cree en dios y tiene fe: “Nos reuniremos con el Presidente Santos y juntos purgaremos el Acuerdo final de la ideología cristiana, mormona, de las creencias en general”.]

[Esto ha pasado, pero claramente no por parte de las defensoras y defensores que respaldábamos el Acuerdo logrado a la fecha del plebiscito. Frente a ello algunos puntos importantes a retomar:]

[Primero, visibilizar que históricamente las mujeres y las personas LGBT hemos sufrido impactos diferenciales dentro y fuera del conflicto armado no debería ser un problema ni mucho menos una guerra, particularmente, eso me llevaría a dejarme de hablar con más de la mitad de mi familia y a perder a amigas que quiero mucho, y no lo haré.]

[Segundo, la llamada “ideología de género” en los Acuerdos no está. Sí, el ex Procurador dijo que estaba camuflada, que eso estaba ahí metidito, entre líneas, y que básicamente había que hacer una exégesis y pedirle a todos los santos (no a la familia del presidente) que lo sacaran a flote: “]*[hay otras utilizaciones, cuando se habla por ejemplo de la cultura sexista, cuando se habla de las familias patriarcales; cuando se refiere a la imposición de estereotipos \[...\] Es en últimas la almendra de la ideología del género, sin relación con el sexo. Cuando se dice que el hombre no nace hombre, sino que se hace. Ese es el concepto que se hace implícito con las utilizaciones de la palabra género]*[”.]

[A él y a muchas personas, casos emblemáticos como los de Lucía en Argentina y Rosa Elvira en Colombia, deberían aportarles a una lectura crítica de lo que significan e implican el sexismo, los estereotipos y los roles sociales que se han construido para mujeres y hombres, y que les ubican como tal.]

[Tercero, ¿Quién dijo que Colombia era un Estado Laico? eso quedó de apellido y decoración porque evidentemente las religiones ejercen una clara influencia en las decisiones de Estado. Basta con decir que la “ideología de género” además de un intento por desvirtuar las luchas del movimiento feminista y por la diversidad; es también una apuesta política por posicionar una agenda ultraconservadora con claros matices cristianos. Este término nace directamente del Vaticano en los 90s con Juan Pablo II y es ratificada por los Papas Ratzinger y Francisco, señalando la importancia de defender la familia conformada por un hombre y una mujer, y la heterosexualidad obligatoria.]

[Cuarto, utilizar y tergiversar información a los fieles de una u otra religión no es justo. Si criticaban la amplia publicidad que tuvo por ejemplo, la campaña por el sí al Acuerdo; hay que cuestionar el hecho de usar diariamente los atrios de las iglesias para invitar a sus fieles a votar por el No y desinformar sobre el proceso de paz con las FARC-EP, porque (desde su punto de vista), el actual Acuerdo destruirá la familia, implicará una paz con impunidad y promoverá el]*[castrochavismo]*[y la homosexualidad. Con esto, qué más pedagogía de paz.]

[Y quinto, el pastor ya citado dice: “]*[Sí estamos en una guerra, una de naturaleza espiritual. No hay con nosotros negociación posible]*[”. Y bueno, quizá para él la guerra es necesaria (su guerra), pero para muchas personas (incluso muchas que votaron No), ninguna guerra debe mantenerse. Esto se demostró en las miles de propuestas enviadas a La Habana por la sociedad civil, en los Foros regionales en los que se consultó a la ciudadanía y a las víctimas, con los mismos votos del plebiscito; y hoy, se sigue demostrando en la calle, en las marchas, en las redes sociales, en fin, en la vida cotidiana.]

[Así, con todos estos debates que siguen gestándose sobre la familia, el género y demás (debates que hoy se funden con temas considerados gruesos, como la paz); es fundamental repensar el ejercicio de la oposición, así como los alcances que siguen teniendo las diferentes religiones en el país, religiones que con sus discursos, siguen seduciendo a tantas personas.]

###### Comunicadora Social, magister en Estudios Culturales (sólo falta el papelito) y defensora de derechos humanos. 
