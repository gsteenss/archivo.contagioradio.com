Title: Piden a Messi y Argentina no jugar amistoso contra Israel
Date: 2018-06-05 12:53
Category: El mundo, Otra Mirada
Tags: Apartheid Israel, BDS, futbol, Palestina
Slug: messi-amistoso-israel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/Messi-bds-e1528217833807.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Ilustración: Carlos Latuff 

###### 05 Jun 2018 

Desde el movimiento de Boicot des inversiones y sanciones a Israel BSD Argentina, se viene haciendo un llamado para que la selección nacional de ese país y su máximo referente Lionel Messi no asistan al encuentro amistoso del próximo 9 de junio en la ciudad de Jerusalén en el marco de la conmemoración de los 70 años de la creación del Estado de Israel.

Utilizando la etiqueta \#ArgentinaNoVayas, los activistas en solidaridad con el pueblo palestino instan a la Asociación de Fútbol Argentino AFA y la selección a desistir del encuentro, asegurando que "aún están a tiempo de patear contra el apartheid" y que se sumen al boitcot cultural y deportivo al que se han unido celebridades como Roger Waters, Elvis Costello, Brian Eno, Peter Gabriel y Stephen Hawking, entre otros.

Según advierte el movimiento BDS, la selección argentina recibiría por el encuentro una suma cercana a los 9 millones de dólares, y posteriormente Israel cubrirá los gastos de desplazamiento a Moscú del equipo dirigido por Sampaoli. Adicionalmente aseguran que se dará  seguridad de Mossad a Leo Messi, quien en diciembre pasado firmó un acuerdo de patrocinio con la firma de electrónicos israelí Sirin Labs.

**Llamado de los niños **

Según la información publicada, el encuentro entre Argentinos e Israelíes tendría lugar en el estadio Teddy Kollek,  ubicado en el barrio Malha al sur de Jerusalén, territorio que fue destruido hace 70 años durante la guerra árabe-israelí. En ese escenario deportivo milita el club Beitar Trump, que adopto su nombre en homenaje al presidente estadounidense que decidió instalar la sede diplomática del país en Jerusalén.

Por esa razón, 70 niños descendientes de algunos de los exiliados por la guerra, firmaron una carta para pedirle a su ídolo Lionel Messi que no juegue este partido amistoso. “Como se nos ha dicho, vienes a jugar con tus amigos a Malha, en un estadio construido sobre nuestra aldea destruida”, lamentan en el texto.

“Nuestra felicidad se convirtió en lágrimas y se rompieron nuestros corazones. ¿Es acaso lógico que Messi, el héroe, vaya a jugar en un estadio construido sobre las tumbas de nuestros ancestros?”, preguntan en la carta los niños palestinos. “Nosotros, en representación de nuestros amigos y amigas, rezamos a Dios para que conceda nuestro deseo de que Messi no rompa nuestros corazones”, agrega la misiva.

Aunque el jugador del Barcelona no se ha pronunciado públicamente al respecto, lo más probable es que tanto el como la selección argentina participen del encuentro deportivo previo a su llegada a territorio ruso para enfrentar la Copa del mundo en la que debutarán el sábado 16 de junio enfrentando a Islandia.

Actualización:

En la tarde de este martes, se conoció por la prensa argentina que el compromiso fue suspendido por pedido de los jugadores de quedarse entrenando en Barcelona, ciudad donde la selección se encuentra concentrada.

###### \*Con información de América XXI y BDS Argentina 
