Title: Atentan contra la vida de Alfredo Barón sindicalista en Valledupar
Date: 2017-02-10 16:59
Category: DDHH, Nacional
Tags: Ataques a sindicalistas, Fuerza Pública
Slug: atentan-contra-la-vida-de-alfredo-baron-sindicalista-en-valledupar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/sindicalisras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Parentesis ] 

###### [10 Feb 2017] 

Alfredo Barón Sánchez, integrante de SINALTRAINAL en Valledupar y del Congresos de los Pueblos, fue víctima de un atentado el pasado 9 de febrero, cuando un **hombre le disparo en su contra, mientras el defensor caminaba hacia su casa, por fortuna Barón salio ileso de este ataque.**

**Ese mismo día en la mañana, Alfonso Barón participó en la segunda sesión de la Mesa Departamental de Derechos Humanos y Territorios** en la vereda El Hatillo, del Corregimiento de La Loma, en el Departamento del Cesar, en la que participan funcionarios de la oficina del alto comisionado para los derechos humanos en Colombia de las Naciones Unidas, la Defensoría del pueblo, la Misión de apoyo al proceso de paz de la OEA y varias organizaciones sociales y defensoras de derechos humanos que hacen presencia en la región.

De acuerdo con el comunicado de prensa de SINALTRAINAL, es relevante que en las dos ocasiones en las que se ha reunido la Mesa, se han presentado situaciones de agresión hacia quienes participan. El 13 de enero de 2017, hacia las 5:00 p.m, culminada la primera sesión, uno de los participantes, el señor Orlando Salcedo Restrepo, también miembro del Congreso de los Pueblos, recibió una llamada telefónica amenazante en la que le advirtieron **“guerrilero hijueputa qué hacías hoy en la reunión en la OEA… cuántos guerrilleros habían en esa reunión”.**

El Equipo Jurídico de Pueblos y SINALTRAINAL expresaron que “**existe una gran preocupación debido al incremento a las violaciones de derechos humanos en la región**, que incluyen asesinatos, empadronamiento, agresiones y judicialización contra líderes y lideresas de la zona”. Le puede interesar:["Asesinato de Herminia Olarte debería ser el último"](https://archivo.contagioradio.com/el-estado-sigue-guardando-silencio-ante-presencia-paramilitar/)

Ambas organizaciones señalaron que estas **acciones demuestran una evidente intención silenciamiento y desarticulación o debilitamiento de los espacios** que buscan sistematizar y hacer seguimiento a estas graves problemáticas que se vienen visibilizando, a lo que suman la pasividad por parte de la Fuerza Pública y las autoridades en estos crímenes. Le puede interesar: ["Asesinato de reclamante de tierras pudo haberse evitado"](https://archivo.contagioradio.com/asesinato-de-reclamante-de-tierras-pudo-haberse-evitado/)
