Title: "cambiar el clima y el sistema que ha generado su crisis" COP 20
Date: 2014-12-24 01:56
Author: AdminContagio
Category: Ambiente, Resistencias
Tags: Ambiente, Bosques, cambio climatico, COP20
Slug: cambiar-el-clima-y-el-sistema-que-ha-generado-su-crisis-cop-20-2
Status: published

###### Foto: pactodeunidad.org 

Del 1ro al 12 de diciembre se está llevando a cabo el COP 20 "Conferencia 2014 por un cambio climático" en Perú. En ella, se dialoga sobre los problemas que tiene la humanidad en torno al medio ambiente, para **presentar propuestas, y no solo "declaraciones políticas"**, para promover acciones y cambiar el clima y el sistema que ha generado sus crisis.

Diferentes organizaciones y delegaciones de países alrededor del mundo están presentando en el COP 20, iniciativas de desarrollo sostenible priorizando la territorialidad colectiva de las comunidades. "**Para proteger los bosques primero hay que proteger la tierra**, y eso muchas veces significa titulaciones colectivas a las comunidades", señaló Analis Romoser.

Una experiencia que se ha presentado desde Colombia, tiene que ver con la **resistencia de las comunidades del Chocó, contra la expansión de la palma aceitera**. "La resistencia de estas comunidades ha sido un gran ejemplo para los y las asistentes al 20 COP2", concluyó Rosmer.
