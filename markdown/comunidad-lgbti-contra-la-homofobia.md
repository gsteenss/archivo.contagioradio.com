Title: Comunidad LGBTI contra la homofobia
Date: 2015-05-19 18:41
Author: CtgAdm
Category: closet, LGBTI
Tags: 24 cuadros, Cinemateca Distrital, Closet Abierto, Comunidad LGBTI, Día Internacional contra la homofobia, Homosexualidad, Red Somos, Secretaría de Integración Social
Slug: comunidad-lgbti-contra-la-homofobia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/DSC4494-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

#####  <iframe src="http://www.ivoox.com/player_ek_4518885_2_1.html?data=lZqemp2ceY6ZmKiakp2Jd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYy6xtHW0sqPhcTZ18rR0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Entrevista: Felipe Acevedo ] 

##### **[19 May 2015]** 

En el marco de la celebración del día internacional contra la homofobia, se estrena en Bogotá el Documental[ **"Redes Sociales de Afecto y Apoyo para las Personas de los Sectores LGBTI, *mis derechos, tus derechos*"**] en la **Cinemateca Distrital** a las 7 P.M. con el objetivo de visibilizar el trabajo que llevan a cabo las redes sociales de afecto y apoyo para la comunidad LGBTI.

**Felipe (Fepo) Acevedo,** tallerista de la corporación **Red Somos,** habla acerca de 18 redes que funcionan desde lo comunitario, **Intégrate Fontibón, Red Bogotana de Hombres Gay, Mamás Lesbianas, Personas de Colores, Diversidad Humana**, entre otras, que tienen la necesidad de formalizar, visibilizar y fortalecer los procesos que se llevan a cabo a nivel distrital por medio de un grupo focalizado de personas. Las redes sociales de apoyo parten desde necesidades básicas hacia otras necesidades adicionales que también lleguen afectar a las personas del sector, como discapacidades auditivas, otras, por su parte están enfocadas en creencias religiosas que apoyan por medio de procesos espirituales.

El tallerista abre la invitación para asistir a la presentación de este documental que expone la realidad de las personas de la comunidad **LGBTI** por medio de testimonios. "Nos parece muy importante que los temas **LGBTI** tengan espacios en una ciudad como esta, por medio de acciones participativas como asistir a ver un documental sobre los procesos que se llevan a cabo en las localidades".

\
