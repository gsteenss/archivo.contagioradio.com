Title: Sentencia contra Plazas Vega es “una decisión en firme”
Date: 2015-10-21 16:10
Category: DDHH, Entrevistas
Tags: Abogada de las víctimas del Palacio de Justicia, Caso del palacio de Justicia, Derechos Humanos, Desaparecidos palacio de justicia, Hallazgos restos desaparecidos del palacio de justicia, Liliana ávila, plazas vega, Radio derechos Humanos, Sentencia ex coronel Plazas Vega
Slug: sentencia-contra-plazas-vega-es-una-decision-en-firme
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Sin-Olvido-Palacio-de-Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_9116398_2_1.html?data=mpaemJidfI6ZmKialZWJd6KmkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8bi1crbxc7Fb8XZjMrlj8jTttDixtGYstHFvsLnjLvSycaPqdSfhqqfh52UaZq31tPOjcnJp8rnyoqwlYqmd8%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Liliana Ávila, Abogada] 

###### [21 oct 2015]

El hallazgo de los cuerpos de Cristina del Pilar Guarín, Luz Mary Portela y Lucy Amparo Oviedo después de 30 años de espera de sus familiares es una esperanza para ellos, **sin embargo la búsqueda e identificación de todos los desaparecidos del Palacio de Justicia es un proceso al que aún le falta mucho por avanzar y el cual presenta múltiples irregularidades.**

**Por ejemplo, la abogada  de las víctimas, Liliana Ávila denunció que hay presencia militar** en los trabajos de exhumación, sin que los militares tengan función alguna en esos escenarios, lo cual pone en riesgo el curso normal de las investigaciones y la vida de quienes participan de este proceso. A lo largo del proceso también se han evidenciado amenazas contra fiscales, víctimas y abogados y esta etapa, lastimosamente, no ha sido la excepción.

Un caso reciente de presencia militar se presentó “en la exhumación de uno de los cuerpos, el de Carlos Horacio Urán, magistrado auxiliar del Consejo de Estado de ese entonces”, lo cual es “una presión relacionada con intimidar el escenario del desarrollo de estas diligencias” y se valora si frente a este hecho se presenta denuncia.

Liliana Ávila, integrnate de la Comisión de Justicia y Paz, indica que las investigaciones que tendrá que adelantar la fiscalía son para “**saber en que circunstancias ocurrieron estas muertes**” y "tendrán que abrirse nuevas líneas de investigación" porque ahora surgen dudas en torno a los cuerpos de las personas a las que los restos identificados no pertenecen.

### **Caso Coronel Plazas Vega:** 

Con el hallazgo de los restos de estas tres personas, el ex-coronel Plazas Vega ha indicado que esto confirma que “ha dicho la verdad”, ante lo cuál la representante de las víctimas indica que **este hallazgo de la Fiscalía “nada tiene que ver” con su caso**, ya que la condena de el ex-coronel es por** la desaparición Carlos Augusto Rodríguez, administrador de la cafetería del Palacio de Justicia e Irma Franco, militante del M19.**

Avila reitera que “estas dos personas salieron vivas, bajo custodia de agentes estatales” y existe “arsenal probatorio que demuestra que fueron desaparecidas por agentes estatales al mando de Alfonso Plazas Vega”. A este respecto René Guarín también afirmó que el "Plazas Vega no se puede montar sobre los restos de Lucy, Cristina y Luz Mary, para pedir su libertad".

Este nuevo hallazgo en nada desvirtúa el hecho que,por ejemplo se dio con el magistrado Urán quien “estaba en el palacio, fue sacado bajo custodia de agentes estatales, ejecutado y luego lo ingresaron al Palacio para mostrar a la opinión pública que había muerto en el intercambio de disparos de toma y retoma”, indica Ávila.

**Así la sentencia de Plazas Vega es “una decisión en firme”** que no contempla apelación, además que tiene la construcción de los hechos y material probatorio de que hubo desaparecidos “l**o que le corresponde al Estado es ejecutarla y hacerla cumplir**”, ya que a un año de la sentencia de la Corte Interamericana de Derechos Humanos para que “se adelantaran labores de búsqueda de todas las personas desaparecidas”, aún no se cumplen, mientras los “**padres de los desaparecidos siguen muriendo sin recibir atención**”, es “urgente que el estado cumpla sus obligaciones”, afirma Liliana Ávila, defensora de las víctimas del palacio de Justicia.

### **Lo que sigue para los familiares de las Víctimas:** 

En la construcción y el desarrollo jurídico de los organismos internacionales e internacionales se establece que “**el elemento de ocultamiento deliberado de la persona**” el cual se presenta en este caso es “desaparición forzada”, “simplemente pasan a una categoría de encontrados pero no desvirtúa que fueron extraídos de protección jurídica durante 30 años”.

Frente a los desaparecidos del palacio de Justicia la [Corte Interamericana de Derechos Humanos dio la orden](https://archivo.contagioradio.com/corte-interamericana-de-ddhh-condeno-al-estado-colombiano-por-desaparecidos-del-palacio-de-justicia-2/) para que “s**e adelanten labores de búsqueda de todas las personas desaparecidas**”, esto con la posibilidad de que un equipo internacional pueda ejercer veeduría de las labores de “búsqueda e identificación a raíz de los nuevos hechos”.

Para continuar con la labor de búsqueda e identificación de los desaparecidos los familiares establecieron **contacto con las Madres de la Plaza de Mayo** y con el Equipo Argentino de Antropología Forense, **primero como un respaldo ético a las familias de los desaparecidos y un respaldo técnico que garantizará resultados óptimos en lo que falta por hacer en materia de investigación forense.**
