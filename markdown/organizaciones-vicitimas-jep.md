Title: Victimas piden respeto a la JEP y su independencia judicial
Date: 2018-05-21 12:57
Category: Paz, Política
Tags: conflicto, Fiscal, JEP, santrich, víctimas
Slug: organizaciones-vicitimas-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/victimas-la-chinita.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 21 May 2018 

Dos comunicaciones, una de organizaciones de víctimas que habitan en los territorios remotos de Colombia y otra de organizaciones de derechos humanos respaldan la decisión de la Jurisdicción Especial de Paz, de avocar conocimiento del caso de Xeusis Hernández, más conocido como Jesús Santrich.

En [la misiva](https://comunidadesconpaz.wordpress.com/2018/05/20/la-jep-esta-garantizando-los-derechos-de-todos-sin-excepcion-respaldemos-su-mision/) las víctimas de la Red de Comunidades Construyendo Paz en los territorios CONPAZ, señalan que la JEP está garantizando los derechos de todos sin excepción, por lo que hacen una invitación a respaldar su misión.

Luego de recordar que desde el año 2002, más de 160 comunidades directamente afectadas por el conflicto armado, se han referido al derecho restaurador, llaman al Fiscal General de la Nación Néstor Humberto Martínez a respetar la independencia judicial y la nueva institucionalidad creada por la firma de los Acuerdos del Teatro Colón, e invitan al Procurador General dadas sus funciones constitucionales a escuchar y respaldar la voz de las víctimas para que la JEP no sea desmoronada.

Así mismo CONPAZ llama la atención al gobierno nacional por su pronunciamiento frente al caso Santrich recordando que las víctimas están en el centro del Acuerdo de paz con las FARC y que sus funcionarios deben respetar lo acordado. Por último, creen que la JEP ha está dando muestras de eficacia, de independencia y garantía para los derechos de todas y todos los afectados por el conflicto armado, así como para los responsables de crímenes graves y sistemáticos.

**Organizaciones de DD.HH también se pronuncian **

Un pronuciamiento similar de respaldo a la JEP y su independencia presentaron [diversas ONG de Derechos humanos](https://www.scribd.com/document/379793556/Respaldamos-Mision-de-La-JEP), en el que aseguran que con sus intervenciones el Fiscal,  desconoce el proceso de paz, centrado en respetar los derechos de las víctimas,  el mandato de la JEP amparado constitucionalmente y el valor de este mecanismo de justicia transicional para enfrentar la impunidad existente,

Así mismo aseguran que las apreciaciones del funcionario contra la JEP, e incluso la postura de los ministerios del Interior y de Justicia en el caso referido,  "están en contravía del principio de independencia judicial y debido proceso".

Se cuestiona además que tal posición que se asume justamente cuando organizaciones de víctimas "presentan sus informes y están en curso una serie de solicitudes de acogimiento de testigos y de responsables en la comisión de crímenes contra la humanidad, terceros, ex militares y exparamilitares, y otros en solicitud de revisión de sus procesos",

En su apreciación, con lo anterior se demuestra que la verdad es central para la paz y la reconciliación, y que la rendición de cuentas de los responsables y la expectativa de miles de víctimas del conflicto son grandes retos de la JEP.

Finalmente, elevaron una invitación a la sociedad colombiana para que se informe adecuadamente sobre la Jurisdicción Especial de Paz y valore su misión para enfrentar la impunidad que la justicia ordinaria no enfrentó eficazmente, y a quienes ejercen cargos de gran responsabilidad en el Estado y gobierno a "actuar fortaleciendo la autonomía institucional y la complementariedad con esta nueva institucionalidad como la JEP y el sistema Integral de Verdad, Justicia y Reparación".

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
