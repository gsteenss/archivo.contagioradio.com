Title: No cesa violencia contra resguardos indígenas en Buenaventura
Date: 2018-06-27 18:06
Category: DDHH, Nacional
Tags: Derechos Humanos, guardia indígena, ONIC, tortura
Slug: violencia-contra-resguardos-indigenas-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/la-delfina-indigenas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [27 Jun 2018] 

A la tortura de dos indígenas pertenecientes al resguardo indígena La Delfina, en Buenaventura, Valle del Cauca, se suma el asesinato de dos integrantes de la comunidad, hecho que provocó el desplazamiento de los habitantes de la comunidad hacía un punto de concentración en el cual se pudiera establecer un cordón de seguridad.

En un comunicado, **la ONIC denunció que José Gustavo Parra Gutiérrez y Sebastián Velásquez Gutiérrez, integrantes del pueblo ‘Nasa Kiwe’, fueron víctimas de torturas** al ser sometidos a golpes y, posteriormente colgados a un árbol mediante unas esposas que usualmente porta la Policía Nacional.

Los miembros de la guardia indígena que fueron torturados estaban encargados del cordón de seguridad establecido al rededor de la escuela de La Delfina, lugar al cual  decidieron movilizarse cerca de 130 familias, tras los hechos de violencia que han vivido otros integrantes de la comunidad.

Según relata el comunicado de la ONIC, los dos guardias estaban próximos a completar su turno, cerca de las 12 de la noche del lunes 25 de junio, cuando **fueron interceptados por hombres encapuchados, quienes los despojaron de sus bastones de mando, procedieron a secuestrarlos y posteriormente torturarlos.**

La comunidad fue alertada cuando llegó el reemplazo de la guardia y encontró solo sus bastones, momento en que iniciaron su búsqueda; posteriormente, hallaron a los dos guardias colgados de un árbol. Aunque ambos miembros del pueblo Embera fueron golpeados, se encuentran en condición estable de salud.

**Según el testimonio de Luis Campos, integrante de la Comisión de DDHH de la ONIC el rapto y posterior tortura se produjo a 500 metros de una base militar, y a una estación de policía ubicada a 5 km del resguardo.** Sin embargo, no hubo presencia de fuerzas del Estado en el momento en que se alertó sobre el secuestro.

La ONIC no tiene indicios sobre quiénes pudieron cometer este delito, dadas las condiciones de luz que tenia el lugar en el cual ocurrieron los hechos. Pese a esto, ambos miembros de la guardia indígena señalaron fueron más de 4 personas encapuchadas que no se identificaron como parte de ningún grupo.

### **Hay zozobra en la comunidad** 

El cordón de seguridad al rededor de la escuela de La Delfina se estableció desde el 7 de junio, día en el que cerca de 130 familias se desplazaron a la parte baja del resguardo, -lugar en el que se encuentra la escuela- temiendo por sus propias vidas dado que, habían sido encontrados los cuerpos de dos miembros del pueblo indígena que, luego se confirmó, habían sido asesinados.

Hechos a los que se suman los impactos de bala contra una de las paredes de la casa en la que estaba la secretaria del resguardo y al atentado sufrido por el rector de la escuela de la comunidad. (Le puede interesar: ["Asesinan a 3 integrantes de la ONIC en el departamento de Valle del Cauca"](https://archivo.contagioradio.com/asesinan-a-3-indigenas-integrantes-de-la-onic-en-el-departamento-del-valle-del-cauca/))

En correspondencia con estos hechos, Campos cree que puede haber una correlación entre los atentados que han sufrido en La Delfina y los intereses de empresas como Ecopetrol, Ferrocarril del Pacífico, la Empresa de Energía del Pacifico y la doble calzada Buenaventura-Buga.  Razones por las que se puede ver afectada la permanencia de la comunidad en el territorio

Aunque la ONIC está a la espera de la realización de un Concejo de Seguridad, y se encuentran evaluando las diferentes medidas para garantizar la vida digna de todos los resguardos del pacífico, **sigue la zozobra en la comunidad, pues, como lo asegura Luis: "No saben de dónde provienen las amenazas".**

<iframe id="audio_26765780" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26765780_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo] 
