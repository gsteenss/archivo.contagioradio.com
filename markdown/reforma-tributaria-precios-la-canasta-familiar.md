Title: Así deja la Reforma Tributaria los precios de la canasta familiar
Date: 2016-12-27 14:52
Category: DDHH, Nacional
Tags: Canasta familiar, IVA 19%, Reforma tributaria, salario minimo
Slug: reforma-tributaria-precios-la-canasta-familiar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/mercado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Paravivir] 

###### [27 Dic 2016] 

A la reforma tributaria se le agotó el tiempo, el proyecto de ley con el que el Gobierno espera recaudar \$7,5 billones, que aumenta los precios de la canasta familiar y fue aprobado la semana pasada, entra a conciliación en las comisiones de Cámara y Senado, **después del encuentro se definirá el texto que pasará a sanción presidencial para convertirse en ley. **

El incremento del IVA al 19% que entrará en vigor a partir del 1 de Febrero aumenta de forma considerable los precios de productos básicos de la canasta familiar. Para saber cuánto será el aumento de cada producto  los ciudadanos pueden hacer en casa la cuenta con una sencilla regla de tres.

### **Haga cuentas** 

Tomaremos como ejemplo un aceite de 1000 cm3 que cuesta \$5.600 de los cuales \$896 equivalen al valor del IVA de 16% que pagamos los colombianos por ese producto en 2016.

Teniendo ese valor ahora haremos la misma operación con el 19% que será el IVA para el 2017, \$5.600 x 19 / 100 = \$1.064, a este último resultado se le resta el IVA de 16%, lo que nos da un total de \$168 y ese sería el aumento neto al producto, es decir en 2017 ese mismo aceite costará \$5.768.

Así entonces, en 2017 una libra de café pasará de costar \$8.100 a \$8.350, una libra de mantequilla pasará de \$6.500 a \$6.700, una panela de 500g que vale \$2.000 costará \$2.100, 1 kilo de arroz pasará de \$4.000 a \$4.200, 1 kilo de azúcar blanca que cuesta \$3.000 costará \$3.100 y una libra de lentejas que vale \$2.900, costará \$3.000.

Además, productos como **papel higiénico, toallas higiénicas, tampones, shampoo, detergentes y desodorantes, tendrán un aumento cercano a los \$500.**

12 rollos de papel higiénico que costaban \$14.700 costarán \$15.200, dos kilos de detergente para ropa que costaban \$14.000 costarán \$14.500, el jabón lava loza de 900g que costaba \$6.800 ahora costará \$7.000, 3 unidades de jabón corporal que costaban \$5.600 pasarán a costar \$5.800, un shampoo de 500ml que costaba \$17.600 pasará a costar \$18.200.

Por otra parte, productos de cuidado personal como los tampones, en presentación de 18 unidades que cuestan \$23.900 costarían \$24.700, una caja de toallas higiénicas por 40 unidades que vale \$15.700 costaría \$16.200, un paquete de 4 máquinas de afeitar que valen \$10.000 pasarían a costar \$10.300 y por último un desodorante pasará de costar \$13.700 a \$14.200.

### **¿Cuáles son las propuestas?** 

Frente a ello, distintas organizaciones e incluso senadores como Alexander López del Polo Democrático, han convocado a campañas ciudadanas para frenar el proyecto, **“la idea es protestar a través de las redes sociales y de todas las formas de expresión como movilizaciones pacificas**, asistiendo al Congreso el miércoles 28 de Diciembre y llamar a cada representante y senador para que no asista y no vote esta reforma” puntualizó López.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
