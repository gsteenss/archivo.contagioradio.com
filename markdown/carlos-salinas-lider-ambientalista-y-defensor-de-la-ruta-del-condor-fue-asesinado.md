Title: Carlos Salinas, líder ambientalista y defensor de la Ruta del Cóndor fue asesinado
Date: 2019-11-09 15:24
Author: CtgAdm
Category: Ambiente, Líderes sociales
Tags: ambientalistas asesinados, asesinato de líderes sociales, Tolima
Slug: carlos-salinas-lider-ambientalista-y-defensor-de-la-ruta-del-condor-fue-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Carlos-Aldairo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

**Carlos Aldairo Arenas Salinas**, líder campesino del páramo de Santa Isabel y promotor de la Ruta del Cóndor fue asesinado por dos hombres que habrían llegado hasta la finca El África en la vereda Totarito en el departamento de Tolima. Según informaron medios locales, el hecho habría ocurrido la noche del pasado 8 de noviembre.

Por el momento no se conoce más información ni los motivos por los que se atentó contra el líder campesino a quien conocían de cariño con el apodo de "Cejas", sin embargo redes de ambientalistas señalan que podría tratarse de un grupo armado quienes serían los responsables del asesinato.

### Un líder que trabajó por los nevados

**Carlos Salinas trabajó en la Ruta del Cóndor**, proyecto eco turístico y comunitario que promueve la protección de los recursos naturales del **Parque Nacional Natural Los Nevados** ubicado en la Cordillera Central del país, integrando los departamentos de Caldas, Risaralda, Quindío y Tolima.

En la actualidad, **los glaciares del país están en vía de extinción, pasando de tener una extensión de 374 kilómetros cuadrados a 45,3, es decir, Colombia ha perdido el 84% de su área glaciar**, hogar de numerosas especies que protegía el líder campesino como parte de su labor como ambientalista. [(Le puede interesar: Son defensores ambientales no enemigos del Estado)](https://archivo.contagioradio.com/defensores-ambientales-o-enemigos-del-estado/)

Según el informe de la organización Global Witness, después de Filipinas, **Colombia  fue el segundo país donde ocurrieron más asesinatos de activistas medioambientales durante el 2018, registrando el homicidio de 24 activistas y defensores del medio ambiente.** [(Lea también: Gobierno Colombiano sigue diciendo no a la protección de líderes Ambientales)](https://archivo.contagioradio.com/gobierno-niega-acuerdo-escazu/)

Noticia en desarrollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
