Title: ¡A la Corte Penal Internacional para exigir protección a líderes!
Date: 2019-04-05 17:00
Author: AdminContagio
Category: DDHH, Política
Tags: asesinato de líderes sociales, Corte Penal Internacional, Fatou Bensouda, La Haya, Quinto Caso
Slug: la-corte-penal-internacional-exigir-proteccion-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-56.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Jorge Rojas] 

###### [5 Abr 2019] 

Con pancartas, arengas y un mensaje muy claro, más de 400 ciudadanos colombianos y de varias nacionalidades europeas arribaron a La Haya, en Holanda, para denunciar los asesinatos sistemáticos de líderes sociales en Colombia ante la Corte Penal Internacional (CPI). El alto tribunal recibió a **un comité de 40 delegados de la marcha**, quienes presentarán un informe sobre los casos de dichas agresiones a la fiscal Fatou Bensouda.

Actualmente, el ente acusador tiene en curso un exámen preliminar sobre **cuatro casos de crímenes en Colombia relacionados con el conflicto armado**, entre ellos, las ejecuciones extrajudiciales conocidas como "falsos positivos", desplazamientos forzosos, delitos sexuales y la promoción y expansión de grupos paramilitares.

Como explicó Diana Sepúlveda, integrante del capitulo alemán de la organización **Unidos por la Paz**, se espera que tras recibir esta denuncia, el alto tribunal inicie una indagación preliminar en lo que han denominado el "**Quinto Caso**".

"Lo que esperamos de la CPI es que se empiece a realizar estudios exploratorios sobre el Quinto Caso, o sea el tema de los asesinatos sistemáticos de líderes y lideresas sociales. Esperamos una respuesta ante los documentos que se entregaron", indicó Sepulvada. (Le puede interesar: "[Líderes sociales denunciarán internacionalmente sistematicidad de agresiones en su contra](https://archivo.contagioradio.com/lideres-sociales-denunciaran-internacionalmente-sistematicidad-agresiones/)")

Durante la marcha que inició en París el pasado 28 de marzo, los asistentes portaron una tela negra grabada con el nombre de los **472 líderes asesinados desde la firma del Acuerdo de Paz**. Mientras que la CPI estudia la denuncia presentada, Sepulveda indicó que la red de activistas promotores de la movilización, continuará una campaña para visibilizar tales agresiones a nivel internacional.

<iframe id="audio_34130144" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34130144_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
