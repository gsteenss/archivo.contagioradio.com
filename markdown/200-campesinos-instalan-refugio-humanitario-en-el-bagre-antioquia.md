Title: 200 campesinos instalan refugio humanitario en El Bagre, Antioquia
Date: 2016-06-29 16:27
Category: DDHH, Nacional
Tags: campamento refugio humanitario, El Bagre Antioquia, Paramilitarismo
Slug: 200-campesinos-instalan-refugio-humanitario-en-el-bagre-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/El-bagre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: APR ] 

###### [29 Junio 2016] 

Debido a la persistencia del paramilitarismo y a las continúas violaciones a los derechos humanos, **cincuenta familias campesinas instalan un campamento refugio humanitario** en el corregimiento de Puerto López del municipio de El Bagre, Antioquia, en el que también hacen presencia organizaciones del orden nacional e internacional que brindan acompañamiento a las comunidades.

Según Mauricio Sánchez, de la 'Asociación de Hermandades Agroecológicas y Mineras de Guamocó', se instaló el refugio humanitario "por los hechos de violencia que siguen azotando la región (...) **siguen asesinando a nuestros campesinos líderes que trabajan en pro del desarrollo comunitario** y en favor de la paz".

Sánchez afirma que las familias han recibido apoyo de comerciantes de la zona, pobladores y organizaciones que han donado alimentos, mientras **ninguna institución del estado ha hecho presencia en esta región** en la que se siguen presentando asesinatos, como el ocurrido este miércoles a las 6 de la mañana, en el que la víctima fue el coordinador del Comité de Mototaxistas.

Las comunidades aseguran que levantarán el campamento cuando el Gobierno nacional, la Fuerza Pública y las instituciones estatales hagan presencia en el corregimiento y brinden [[condiciones de seguridad](https://archivo.contagioradio.com/comunidades-instalaran-campamento-humanitario-frente-a-hostigamiento-paramilitar/)] para sus habitantes y **atención integral para las familias que subsisten de la minería artesanal y de los cultivos de coca**.

<iframe src="http://co.ivoox.com/es/player_ej_12069572_2_1.html?data=kpedmJ6Ze5Ohhpywj5aWaZS1lpaah5yncZOhhpywj5WRaZi3jpWah5ynca7V1tfWxc7Tb7SZpJiSo5bSp8nZ25CajabXs8TdwsjWh6iXaaOnz5DRx5CsqdPhwtPRw8nJt4y1yNfcx8jTsI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
