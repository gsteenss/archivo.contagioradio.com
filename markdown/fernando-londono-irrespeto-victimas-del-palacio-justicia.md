Title: Fernando Londoño irrespetó a víctimas del Palacio de Justicia
Date: 2016-11-15 14:50
Category: DDHH, Nacional
Tags: Alfonso Plazas Vega, entrega de restos del palacio de justicia, Fernando Londoño, Palacio de Justicia, toma y retoma del palacio de justicia, Víctimas del Palacio de justicia
Slug: fernando-londono-irrespeto-victimas-del-palacio-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/SinOlvido.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sin Olvido] 

###### [15 Nov. 2016] 

**“Para contar la película, bastaba con reconocer el inconfundible y característico trasero de una señora, o el caminado sin paragón de un señor, o la parte posterior de la cabeza del otro”** así se refirió el ex ministro Fernando Londoño a propósito de las víctimas del Palacio de Justicia.

Las aseveraciones se hicieron en el marco del evento de presentación del libro del coronel Alfonso Plazas Vega quien fuera vinculado por la desaparición de varias personas en la toma y retoma del Palacio de Justicia sucedida el 6 y 7 de Noviembre de 1985.

El libro, titulado “Manteniendo la democracia Maestro” fue lanzado el martes 8 de noviembre y fue la oportunidad para **Fernando Londoño de hacer chistes pesados en torno a lo que ha sido el proceso de las víctimas en este caso**. Le puede interesar: [Medicina Legal confirma que víctima de la retoma del palacio fue asesinada](https://archivo.contagioradio.com/medicina-legal-confirma-que-victima-de-la-retoma-del-palacio-fue-asesinada/)

Dicho libro cuenta con un prólogo en el que el ex procurador Alejandro Ordoñez se destajo para hablar en contra del acuerdo de paz que se lleva a cabo en Colombia.

Esta es otra de las veces que el ex ministro Fernando Londoño ha negado a los desaparecidos del Palacio de Justicia. Por ello, los familiares en la voz de René Guarín, hermano de una de las personas desaparecidas en el Palacio de Justicia aseguró que lo que ha hecho Londoño sería algo así como “si yo dijera que él se echó tempera roja en la cara el día de su atentado para simular que tenía sangre. **Lastimosamente en Colombia todavía hay gente así, que se niega a una concordia a un acuerdo de paz”.**

Es de recordar que la Corte Interamericana de Derechos Humanos condenó al Estado por la desaparición de 11 personas en 1985 en el caso del Palacio de Justicia. Le puede interesar: [“No entierro a Luz Mary Portela, la siembro como semilla de verdad y justicia” Rosa Milena Cárdenas León](https://archivo.contagioradio.com/no-entierro-luz-mary-portela-la-siembro-como-semilla-de-verdad-y-justicia-rosa-milena-cardenas-leon/)

El evento también contó con la presencia de líderes e integrantes de la derecha como José Obdulio Gaviria y Alfredo Rangel.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

###### Con información de Noticias Uno.

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
