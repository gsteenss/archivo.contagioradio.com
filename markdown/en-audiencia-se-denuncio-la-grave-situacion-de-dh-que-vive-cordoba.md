Title: En audiencia se denunció la grave situación de DD.HH. que vive Córdoba
Date: 2019-09-26 19:07
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asociación de Campesinos del Sur de Córdoba, audiencia pública, cordoba, PDET, PNIS
Slug: en-audiencia-se-denuncio-la-grave-situacion-de-dh-que-vive-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/cordoba-por-la-vida.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/EFT-ae0WkAMKR0q.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @SandinoVictoria] 

El pasado miércoles 25 de septiembre se desarrolló la **audiencia Córdoba por la Vida**, organizada por congresistas para escuchar las voces de líderes del departamento, sobre la grave situación de derechos humanos que aqueja al territorio. Los líderes expresaron que para encontrar soluciones a la crisis se requiere coordinación institucional, cumplir con los planes de sustitución y desarrollo, así como escuchar a las comunidades.

### **Las denuncias sobre el sur de Córdoba que escucharon los congresistas** 

Victoria Sandino, senadora del partido FARC, afirmó que para ellos fue importante escuchar las voces de líderes y lideresas provenientes de Tierralta, Montelíbano, Puerto Libertador y San José de Uré. Los y las líderes resaltaron su preocupación sobre la situación de seguridad en el territorio, puede luego de la firma del Acuerdo de Paz se cuentan 32 líderes y lideresas asesinados en el sur de Córdoba, con el agravante de que en algunos de estos casos se han presentado signos de tortura previa al homicidio. (Ver: [Diálogo por la Verdad: un alto a la violencia en Córdoba)](https://archivo.contagioradio.com/dialogo-por-la-verdad-un-alto-a-la-violencia-en-cordoba/)

La senadora afirmó que también hay una práctica que se está usando, y fue parte del pasado del departamento, que consiste en quemar la tierra y viviendas de los líderes que son asesinados. A la amenaza constante contra la vida, los líderes resaltaron el incumplimiento por parte del Gobierno del Plan de Desarrollo con Enfoque Territorial (PDET) y  el Plan Nacional Integral de Sustitución (PNIS).

Las comunidades señalaron que pese a que las familias cumplieron con la sustitución y erradicaron los cultivos de uso ilícito en un 99% de los casos, el Gobierno en algunas ocasiones solo ha cumplido con el primer pago. Ante la falta de pagos y la ausencia de asistencia técnica para encontrar otros cultivos con los cuales las familias puedan encontrar su sustento se desarrolló un paro campesino en la primera mitad del año.

### **¿Qué han hecho las instituciones encargadas de solucionar estos hechos?** 

Sandino aseguró que Emilio Archila, uno de los encargados de supervisar y garantizar que los planes pactados en el marco del Acuerdo de Paz se cumplan, no asistió al evento y decidió enviar un representante. De la misma forma, los y las líderes criticaron que alcaldías, gobernación e instituciones de orden nacional como el Ministerio de Interior tengan medidas para proteger la vida; pero las mismas no están articuladas, no cuentan con la participación de las comunidades y en la práctica, no están funcionando.

Por esa razón, uno de los compromisos establecidos por parte de los congresistas fue la creación el próximo 3 de octubre de una mesa intersectorial en la que estarán los y las líderes del departamento, las instituciones encargadas de la seguridad y el cumplimiento de los PDET´s y PNIS, y en la que tendrán lugar los congresistas para hacer seguimiento a los acuerdos que allí se alcancen. Otro compromiso fue la realización de una brigada humanitaria que se realizará en el territorio, y pronto se dará a conocer la fecha de su desarrollo, para brindar un acompañamiento a los y las líderes sociales.

<iframe id="audio_42398185" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_42398185_4_1.html?c1=ff6600"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
