Title: En enero regresan las corridas de toros a Bogotá
Date: 2016-10-21 13:23
Category: Animales, Nacional
Tags: Animales, Bogotá, corridas de toros, Plaza de Toros, Toreo, Toros
Slug: enero-vuelven-los-toros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Toros-Regresan-e1477072188751.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Universal] 

###### [21 Oct de 2016] 

Contrario a lo que habían asegurado organizaciones animalistas como la Plataforma ALTO (Animales libres de Tortura), **las corridas de toros regresarán a la capital en el mes de enero de 2017.** Según el Instituto de Recreación y Deporte (IDR), el consorcio encargado de realizar la temporada del próximo año será Colombia Taurina. Esta entidad deberá entregar al distrito como mínimo un 13,1% de los ingresos que se recojan en boletería.

**Fueron 4 años en los que la capital de Colombia estuvo sin corridas de toros**, desde 2012, fecha en la que fueron suspendidas por el entonces alcalde de Bogotá, Gustavo Petro. Sin embargo y como acato al fallo de la Corte Constitucional, por medio del cual se le ordenó a la ciudad iniciar los trabajos para adecuar la Plaza de la Santamaría  para las actividades taurinas, **la administración actual ha anunciado que ésta será entregada el 17 de Enero, y posteriormente se dará inicio a la temporada que irá hasta marzo.** Le puede interesar: [Desde el Congreso buscan alejar a los niños de la violencia de la tauromaquia](https://archivo.contagioradio.com/desde-el-congreso-buscan-alejar-a-los-ninos-de-la-violencia-de-la-tauromaquia/)

En el mes de Junio, la directora de la Plataforma ALTO, Natalia Parra aseguró en Contagio Radio que “si se está hablando de cumplir los mandatos de la Corte, entonces **deben cumplirse todos en términos de la entrega de la plaza y también deberá cumplirse con la sentencia 666, que ordena que los animales no sean víctimas de dolor y sufrimiento en estos espectáculos**”. Leer ésta entrevista completa [aquí](http://bit.ly/2dtRxVh).

Al respecto de esta decisión, la cartera de Protección y Bienestar Animal del Distrito, ha manifestado que no van a hacer ninguna maniobra dilatoria para impedir la sentencia y que se muestran respetuosos de la norma.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
