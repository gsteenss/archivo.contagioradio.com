Title: "Existe una persecución sistemática contra matrimonios de parejas del mismo sexo": Germán Perfetti
Date: 2015-06-23 15:48
Category: LGBTI, Nacional
Tags: Closet Abierto, constitucion, Corte Constitucional, Derechos Humanos, Germán Rincón Perfetti., LGBTI, matrimonio igualitario, Persecución Sistemática, Procuraduría General de la Nación, Sentencia T-444, Unión Solemne
Slug: existe-una-persecucion-sistematica-sobre-matrimonios-de-parejas-del-mismo-sexo-german-rincon-perfetti
Status: published

###### Foto: Contagio Radio 

##### <iframe src="http://www.ivoox.com/player_ek_4674797_2_1.html?data=lZuklpyde46ZmKiakpmJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi8bmzoqwlYqldc%2BfscrfyMrYuMqhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Germán Rincón Perfetti] 

#####  [23 Jun 2015]

La **Corte Constitucional** solicitó la declaración del **Procurador general de la nación** por denuncias de persecución a parejas del mismo sexo. Según integrantes de la comunidad LGBTI la Procuraduría ha solicitado información adicional al momento de llevar a cabo el proceso de **unión solemne** en las notarías, violando el derecho a proteger la intimidad establecido en la sentencia **T-444 de 2014**.

“La **Procuraduría General de la Nación** ha pedido a todos sus procuradores delegados y regionales, que informen cualquier movimiento que haya en espacios judiciales o notarías relacionados con matrimonios igualitarios" asegura **Germán Perfetti** abogado defensor de derechos de la comunidad **LGBTI**.

Según el abogado existe una **persecución sistemática** y permanente sobre los matrimonios para parejas del mismo sexo, teniendo en cuenta que nunca antes se había solicitado información de este tipo y mucho menos a parejas conformadas por un hombre y una mujer.

En esta medida se evidencia una falta a la **Constitución**, al ser la **Procuraduría General** **de la Nación** la entidad encargada de velar por los derechos humanos, acción contraria a esto se está aprovechando de la influencia que tiene sobre las notarías para irrumpir en procesos de unión solemne para personas homosexuales, asegura el abogado.

**Perfetti** hace un llamado a notarios y notarias para que dejen de ser "tinterillos legales" y apliquen la hermenéutica jurídica, que se comporten como abogados y abogadas para interpretar de forma adecuada la sentencia de la **Corte Constitucional** procediendo a efectuar 100% estas **uniones solemnes**.
