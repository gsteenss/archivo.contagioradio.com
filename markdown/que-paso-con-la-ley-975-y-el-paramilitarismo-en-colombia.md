Title: ¿Qué pasó con la Ley 975 y el paramilitarismo en Colombia?
Date: 2019-07-17 11:36
Author: CtgAdm
Category: Expreso Libertad
Tags: colombia, Expreso Libertad, Ley 975, Paramilitarismo
Slug: que-paso-con-la-ley-975-y-el-paramilitarismo-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/csm_AgenciaNoticias_110815-04_05_ebfad2acb7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

En el año 2003 en Colombia se realizo la desmovilización de algunas estructuras del paramilitarismo con la Ley 975 de Justicia y Paz. Sin embargo, muchos fueron los fracasos de esa política que hoy deja un saldo de impunidad sobre las víctimas de estas estructuras y la construcción de memoria histórica para el país.

<div>

</div>

<div>

Escuche en este programa del Expreso Libertad, los errores de la Ley 975 y las implicaciones para Colombia.

</div>

<div>

</div>

<div>

\[embed\]https://www.facebook.com/contagioradio/videos/1112697968926337/\[/embed\]

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 

</div>
