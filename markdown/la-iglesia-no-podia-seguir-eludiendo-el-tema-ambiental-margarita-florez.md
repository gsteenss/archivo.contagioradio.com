Title: La iglesia no podía seguir eludiendo el tema ambiental: Margarita Flórez
Date: 2015-06-19 13:16
Category: Ambiente, Nacional
Tags: Ambiente, Ambiente y Sociedad, encíclica, extracción de recursos naturales, Leudato si, Margarita Flórez, Papa Francisco, política económica
Slug: la-iglesia-no-podia-seguir-eludiendo-el-tema-ambiental-margarita-florez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/PAPA_ENCICLICA_TERRA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.interris.it]

<iframe src="http://www.ivoox.com/player_ek_4664015_2_1.html?data=lZujlpWVeY6ZmKialJWJd6KolJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKfyszZx9jNpYzi0JDd0cmJh5SZoqnOjdjJq9bd05DSztrIrcbixdSYx9GPuMbhwpDOz8fNqc%2FowtGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Margarita Flórez, directora Ambiente y Sociedad] 

**“El ambiente un tema que no puede eludir la iglesia, y el Papa supo asumirlo”** dice Margarita Flórez, directora de Ambiente y Sociedad, sobre la encíclica de Bergoglio, de 191 páginas denominada “Luedato si”.

Para ella, es importante que el Papa haya retomado la problemática ambiental, y asegura que ahora los líderes religiosos deben conducir a sus organizaciones para que se generen **cambios en los estilos de vida de las personas,** para que realmente se logre mitigar los impactos en la tierra.

Para Flórez, aunque “el Papa no puede meterse en la política económica”, si es crucial que los líderes religiosos tengan un mandato en el tema ambiental, a partir de una postura ideológica y moral.

El principal punto al que se refiere el sumo pontífice, para la directora de Ambiente y Sociedad, es sobre el estilo de vida de los feligreses o los ciudadano, para que participen y transformen sus patrones de consumo, que generan el ciclo consumista desarrollado por **“una actitud cultural que nos impulsa a desechar, votar, consumir**, es decir la conducta de lo rápido, lo consumible, desperdiciable”, dice Flórez.

Por otro lado, la directora de Ambiente y Sociedad, dice estar de acuerdo con el Papa sobre los pocos resultados ambientales que han generado las cumbres mundiales, debido a que los acuerdos siempre quedan en el papel, y los gobiernos nunca cumplen. En ese sentido, resalta que en el caso colombiano, pese a que el país ha firmado varios tratados internacionales frente a la situación ambiental, lo cierto es que esos acuerdos no se reflejan en las políticas gubernamentales y por ende **continúa la competencia entre la sostenibilidad ambiental y el desarrollo económico.**

En su encíclica, el Papa, asegura que existe una “deuda ecológica” por parte de los países industrializados con los más pobres, y así mismo, afirma que el tema de las desigualdades no se  puede resolver solo desde la economía y la ciencia.  Frente a esto, Margarita Flórez, señala que **no se puede solucionar esa problemática utilizando un dinero que sale de la extracción de recursos,** ya que sería contradictorio.

Finalmente, una de las anotaciones más importantes que hace el Papa y a las que se refiere la directora de la organización ambiental, es que pese a que existen remedios para contribuir al ambiente, estos suelen crear **“elitismo ambiental”,** como lo llama Flórez, ya que únicamente las personas que tienen los recursos económicos, son quienes pueden vivir en lugares amigables con la naturaleza y pueden consumir productos en favor de la misma.
