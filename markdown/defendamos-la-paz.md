Title: Defendamos la paz: Tejido de esperanza para los territorios
Date: 2019-06-18 07:40
Author: Foro Opina
Category: Opinion
Tags: Bancada de la paz, implementación del Acuerdo de Paz, paz
Slug: defendamos-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DefendamosPaz.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Corte-Constitucional.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Captura-de-pantalla-126.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/defendamos-la-paz.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/defendamos-la-paz-foro.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Defendamos-la-Paz-26-J.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:[[**Sandra Ramírez**]

##### [\*Por la Fundación Foro Nacional por Colombia] 

[En febrero del presente año un grupo de congresistas de diferentes orillas políticas (la denominada Bancada de la paz), ex negociadores de La Habana, diversas organizaciones sociales y un número importante de connotados representantes de la sociedad civil promovieron la gestación del **Movimiento Defendamos la Paz** (DLP) como una medida necesaria para blindar a la paz de sus detractores, empezando por el actual gobierno. Su misión en extenso versa sobre la implementación del Acuerdo de Paz, la defensa de la Jurisdicción Especial para la Paz (JEP) y el Sistema Integral de Verdad, Justicia, Reparación y no Repetición (SIVJRNR), la definición de acciones contra los asesinatos de lideresas y líderes sociales y el logro de la paz integral.]

[Haciendo uso de pronunciamientos que se vuelven tendencia en las redes sociales, DLP tiene en el momento capítulos regionales en Boyacá, Valledupar, Atlántico, Santander, Antioquia, Valle, Putumayo, Meta y grupos de impulso en Sucre, Barrancabermeja, Risaralda, Buenaventura y Bolívar. De igual forma, sectores como los jóvenes, ambientalistas y cristianos han generado sus propias dinámicas unidos bajo las mismas consignas, y el 13 de junio se lanzó el capítulo internacional. Así DLP se extiende como una mancha reivindicadora con tintes de esperanza buscando contrarrestar los riesgos inminentes que presenta la implementación de los acuerdos, como lo ha advertido la comunidad internacional. El largo proceso de las objeciones a la JEP y la desfinanciación de la paz en el Plan Nacional de Desarrollo, son dos evidencias de la escasa voluntad política del gobierno Duque para construir una paz estable y duradera.]

[**La agenda de DLP tiene ahora dos grandes retos**: por un lado, revivir las dieciséis circunscripciones especiales transitorias de paz en la Cámara de Representantes, hecho que de lograrse daría una muy buena señal a las víctimas en cuanto a la reivindicación de sus derechos. Por ello, este diecisiete de junio se lanza la campaña Un millón de firmas para las víctimas que, junto a la tutela interpuesta para recuperar las curules -amparada en el fallo de la Corte Constitucional respecto a la mayoría absoluta que prevaleció en la decisión frente a la JEP-, son dos mecanismos estratégicos orientados hacia el mismo fin.]

[El otro desafío inminente para DLP es lograr incidir en el desarrollo de acciones que viabilicen la paz en los territorios. Es allí donde se juega la paz completa como expresión de justicia social, de inclusión, de generación de oportunidades y de reconciliación. Tarea difícil con lo que está ocurriendo en muchas  zonas del país: el asesinato sistemático de líderes y lideresas sociales, de defensores y defensoras de derechos humanos, que cuestiona la eficiencia de todo el aparato creado para la protección de líderes/as y pone un manto de duda sobre la capacidad de nosotros/as, como colombianos y colombianas, de pasar la página de la guerra.  ]

[**La paz no puede continuar costando tantas vidas**.  Desde el 2016, año de la firma de los acuerdos, 566 líderes/as sociales y defensores/as de derechos humanos han sido asesinados, según los informes de Indepaz. Y con estos actos de barbarie no sólo se pierden vidas humanas, también se atenta contra ese capital social y político de las comunidades que defienden los territorios, la cultura, las ideas, el bien común.]

[Finalmente, no es posible lograr la paz si no se abordan las causas estructurales del conflicto. La débil presencia del Estado y la escasa y lenta oferta institucional en las zonas más afectadas en lo que se refiere a la implementación de la reforma rural y otros programas encaminados a una política integral de paz, son también entendidas como ausencia de interés en la implementación de los acuerdos.]

[Como movimiento, DLP tendrá una tarea crucial de cara a las próximas elecciones regionales y es posicionar la paz como un bastión fundamental de los planes de desarrollo. Esta apuesta política será esencial para impulsar el cumplimiento de lo acordado.]

#### **La Fundación Foro Nacional Por Colombia** 
