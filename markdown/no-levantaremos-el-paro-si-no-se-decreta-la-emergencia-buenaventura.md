Title: “No levantaremos el paro si no se decreta la emergencia” Buenaventura
Date: 2017-05-23 13:48
Category: DDHH, Nacional
Tags: buenaventura, paro, Paro cívico
Slug: no-levantaremos-el-paro-si-no-se-decreta-la-emergencia-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Buenaventura-paro1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Conferencia Episcopal de Colombia] 

###### [23 May. 2017] 

Pese al anuncio del gobierno en el que ha asegurado movilizará una comisión hacia el municipio de Buenaventura para poner fin al paro, el comité del Paro Cívico manifestó que seguirá en paro hasta que se cumpla una de sus primeras solicitudes, declarar la emergencia económica y social “si no llega ese decreto quedamos en las mismas” relató **Manuel Bedoya, presidente de la Organización Nacional de Pescadores Artesanales.**

Según el lider de la movilización no van a seguir creyendo en “la carreta” de todo el mundo de que ya están cumpliendo “eso no es cierto, aquí la gente se sigue muriendo por falta de hospital, de agua, estamos desesperados por la contaminación del carbón y por la situación de la educación”.

Sin embargo, el Gobierno nacional, en cabeza de su secretario de general, Alfonso Prada, manifestó que no van a declarar la emergencia social, según él porque no están dadas las condiciones para tal decreto y puso de ejemplo la situación de Mocoa, para Prada esto solo puede hacer cuando suceden hechos sobrevinientes inesperados no para problemas estructurales.

### **"Ya negociamos hace 3 años" y el gobierno no ha cumplido** 

Por lo que Bedoya añade **“si no traen el decreto de emergencia social y económica, pues que más bien no vengan**, porque nos vamos a sentar en unas mesas a negociar. ¿A negociar qué?, si ya negociamos hace 3 años, pasamos el pacto para la educación que vale 2 billones”.  Le puede interesar: ["Gobierno hace oídos sordos al paro del Chocó"](https://archivo.contagioradio.com/gobierno-hace-oidos-sordos-al-paro-en-choco/)

La única opción que hasta el momento ha dado a conocer el comité del paro cívico, además del decreto de emergencia social y económica es que se constituya una figura legal en la que no se tengan que hacer licitaciones para desembolsar el dinero.

### **Hay plantas de procesamiento del agua pero no hay tuberías para conducirla** 

Según el Gobierno nacional en ese municipio están “prácticamente” terminadas las plantas que llevarán el agua de manera ininterrumpida a la población y se prevé una inversión de \$170 mil millones en lo que se ha llamado el Plan Maestro de Agua.

Pese a ello, Bedoya asevera que eso no podrá darse dado que **no se ha realizado la instalación de las tuberías, ni de las bocatomas “eso de que dicen que llegan los tanques no, las bocatomas no van a empezar, porque la tubería para transportar el agua ya se pudrió** porque no había diseños. Tiene que comprar otra tubería, eso vale un poco de plata, eso no se hace en 1 o 2 años”.

### [**Así continúa el Paro Cívico**] 

Este martes se definió que ingresen Buenaventura camiones con alimentos para poder de tal manera abastecer al municipio. Adicionalmente, los camioneros han manifestado que se solidarizarán con el paro cívico sumándose éste.

“Los camioneros también nos van a colaborar, ayer dijeron que no iban a traer mercancía hasta Buenaventura, vamos a ver. Ya con ese apoyo al Gobierno le toca, porque les están tocando los talones a la economía que entra por Buenaventura” recalcó el líder de los pescadores artesanales.

Según una investigación realizada por la periodista y columnista María Elvira Bonilla, **en Buenaventura figuran solamente 12 empresarios como dueños del 60% de las exportaciones** que realiza este puerto, que logra generar 2 mil millones de pesos al año.

Además dice que, son cerca de 6**00.000 contenedores al año los que salen del puerto, el 80% del café que se exporta sale desde Buenaventura.** En contraposición este municipio tiene un 90% de su población en la miseria. Le puede interesar: [“Si no cerramos el puerto, el Gobierno no nos presta atención” habitantes de Buenaventura](https://archivo.contagioradio.com/si-no-cerramos-el-puerto-el-gobierno-no-nos-presta-atencion-habitantes-de-buenaventura/)

<iframe id="audio_18856782" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18856782_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>

 
