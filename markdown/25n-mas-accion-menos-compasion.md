Title: 25N "más acción, menos compasión"
Date: 2019-11-25 18:13
Author: CtgAdm
Category: Mujer, Nacional
Tags: 25N, genero, Igualdad, mujer, violencia
Slug: 25n-mas-accion-menos-compasion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-25-at-5.54.21-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

 

***Libres de violencia*** es la consigna de la movilización que se da en todo el país en conmemoración al **Día internacional para la eliminación de la violencia de género**; un hecho que atenta contra miles de  mujeres y niñas violando sus derechos, no solo en Colombia sino en todo el mundo y que en muchas ocasiones permanece oculto e impune. (Le puede interesar: [Cinco propuestas de mujeres que le apuestan a la paz](https://archivo.contagioradio.com/cinco-propuestas-de-mujeres-que-le-apuestan-a-la-paz/))

Según ONU Women una de cada tres mujeres en el mundo ha sufrido violencia física y/o sexual en el desarrollo de su vida de pareja o fuera de esta; asimismo resaltaron otros tipos de violencia como la económica, laboral, institucional y simbólica, esta última hace referencia a estereotipos, mensajes, valores o signos que se transmiten y favorecen el hecho de que **se den relaciones basadas en la**  subordinación de las mujeres en la sociedad. (Le puede interesar:[Tortura y violencia sexual contra mujer privada de la libertad en Jamundí](https://archivo.contagioradio.com/tortura-y-violencia-sexual-contra-mujer-privada-de-la-libertad-en-jamundi/))

Una de las mayores de violencias que se registran en Colombia y que son casi imperceptibles son los **micromachismos, reflejo de una sociedad  donde la igualdad de género no es todavía una realidad,**  se manifiestan en el lenguaje cotidiano, al usar expresiones como, “Para ellas todo”, “No llores como una nena” , "nadie la mandó vestirse así", "seguro es una mujer ", "limpié la casa y  lavé los platos por ti", "qué poco femenina", entre otras connotaciones que pasan muchas veces desapercibidas y que reflejan la necesidad de un cambio y un problema que se debe solucionar para obtener una igualdad real entre las personas.

### ¿Qué hacer para mitigar la violencia de género?

Muchos dicen estar totalmente en contra de que se ejerza cualquier tipo de violencia contra las mujeres, pero como manifiesta Natalia Torres, representante de la articulación feminista 25N es necesaria, "más acción, menos compasión", y resalta que hay dos tipos de actos que pueden hacerse para mitigar este hecho, uno es la prevención y el segundo es la sensibilización, ligado directamente con la educación. (Le puede interesar: [Las cárceles de Estados Unidos y sus violencias contra las mujeres](https://archivo.contagioradio.com/las-carceles-de-estados-unidos-y-sus-violencias-contra-las-mujeres/))

Por otro lado, Torres resalta que es fundamental una respuesta institucional al igual que reformas legales para evitar la impunidad, "es necesaria una reforma del código penal, muchas veces la víctima es criminalizada, y esto porque más de la mitad de la población está en contra que las mujeres y hombres asuman otros roles que no sean los tradicionales, es decir, las mujeres dedicadas al cuidado de los hijos y el hogar y los hombres en el trabajo".

### "Somos un rostro colectivo"

Algunos de los principales temas para la movilización la feminista son, "**el Estado no me cuida",** haciendo referencia a que el Estado es creador y reproductor de la violencia contra los cuerpos de las mujeres, esto desde las instituciones, las leyes y la poca atención hacia las denuncias."**El Estado cómplice"**, ligado directamente con que el Estado no atiende oportunamente las acciones violentas, y no tiene políticas de inclusión laboral y académica. Y la última consigna "**somos un rostro colectivo"**, "a pesar de que el daño es individual, somos una sola moviéndonos de distintas organizaciones, levantando la voz por el mismo objetivo que es mitigar la violencia".

El último estudio de la Universidad de Oxford sobre violencia de género no incluyó a Colombia en los 20 países con mayor violencia. A pesar de ello, un estudio de la Universidad de los Andes alerta que los hombres y mujeres que desde su educación no han tenido la oportunidad de cuestionar los roles de género reproducen con mayor probabilidad actitudes machistas. Frente a eso el gobierno y las organizaciones deben desarrollar estándares, directrices y herramientas educativas que fomenten la igual de género en la sociedad.

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
