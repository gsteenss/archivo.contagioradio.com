Title: Formalización de conversaciones con el ELN está a solo un 3%
Date: 2015-09-09 20:20
Category: Entrevistas, Nacional, Paz
Tags: Animalistas, colombia, Consulta Antitaurina, conversaciones de paz con el ELN, Conversaciones de paz con las FARC, ELN, FARC, Juan Manuel Santos, Presidente Juan Manuel Santos
Slug: formalizacion-de-conversaciones-con-el-eln-esta-a-solo-un-3
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/gabino2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio, canal capital 

###### [9 Sep 2015]

En entrevista exclusiva para Canal Capital y Contagio Radio el máximo comandante del Ejercito de Liberación Nacional, ELN, afirmó que la fase formal de conversaciones de paz con el gobierno nacional está muy cerca. Resalta que solamente falta un 3% y que espera tener "muy buenas noticias" en los próximos días.

\[embed\]https://www.youtube.com/watch?v=5QkS59RkcJw&feature=youtu.be \[/embed\]  
Nicolás Rodríguez Bautista "Gabino", afirma que el acuerdo de confidencialidad no le permite profundizar más en el tema de los avances de las conversaciones de paz. Afirmó que respalda las luchas por la defensa de los derechos de los animales, que apoya la consulta antitaurina y que el movimiento animalista debe sumarse a quienes trabajan por la paz.

"Gabino" resalta también que la defensa de la naturaleza y el ambiente son esenciales para construir la paz, puesto que los recursos naturales son una primera víctima del sistema económico y que este tema está íntimamente ligado a las conversaciones de paz que se formalizarían en los próximos días.

Su vida en las selvas colombianas, el uso de su tiempo libre, y sus acompañantes más cercanos, sus mascotas Afrodita y Zeus y sus canciones muestran la otra cara del comandante del ELN que ahora le apuesta al camino de la construcción de la paz desde la vía del diálogo.

"Sigo reafirmando mi optimismo y espero que el ELN y el gobierno colombiano tengamos para la sociedad buenas noticias en los próximos dias... pero reafirmo mi optimismo y la convicción de que pronto tendremos buenas noticias" afirma Rodríguez Bautista.
