Title: Víctimas no están recibiendo óptima atención en salud mental: MSF
Date: 2017-08-21 10:04
Category: DDHH, Nacional
Tags: conflicto armado, Salud, víctimas, violencia sexual
Slug: victimas_salud_medico_sin_fronteras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/msf205686_medium.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marta Sosznyska / MSF] 

###### [21 Ago 2017] 

Como lo han venido denunciado diversas comunidades de Colombia, el proceso de paz, si bien ha significado un avance, la permanencia de otras estructuras ilegales armadas, impiden que los habitantes de las regiones del país puedan sentir verdaderos tiempos de paz. La violencia sigue siendo una constante en el diario vivir.

Así lo concluye el reciente informe de 'Médicos Sin Fronteras', denominado  **"A la sombra del proceso de paz", que precisamente da cuenta de como** Colombia sigue sufriendo altos niveles de violencia a pesar del acuerdo entre las FARC y el gobierno, y la falta de atención en salud física y mental para las víctimas.

"Este tipo de violencia tiene un claro impacto en la salud física y mental de las poblaciones de los municipios de **Buenaventura (Valle del Cauca)** y **Tumaco (Nariño)**", afirma **Juan Matías Gil, jefe de misión de Médicos Sin Fronteras (MSF) en Colombia, y agrega, **"Aunque las situaciones y necesidades de los pacientes vistos por MSF en estas ciudades no pueden ser extrapoladas directamente al resto del país, pueden ser consideradas como una aproximación plausible de la realidad en áreas urbanas y rurales de muchas provincias de Colombia".

De acuerdo con el documento la presencia e influencia cada vez mayor de las organizaciones criminales y de otros grupos armados provocan un gran número de amenazas, homicidios selectivos, secuestros, desapariciones, acoso, extorsión y restricción de movimientos.

### Deficiencia en la asistencia en salud 

Los datos médicos recogidos en el informe indican que durante 2015 y 2016 en **Buenaventura y Tumaco, el equipo de MSF atendió a 6.000 pacientes por** la exposición a eventos violentos y factores de riesgo, que provocó en la población depresión (25%), ansiedad (13% ), trastornos mentales (11%) y estrés postraumático (8%).

Frente a ello, "**Hay un déficit de servicios de salud mental a nivel primario, a pesar de las necesidades significativas de la población** y la existencia de un marco legal de atención, asistencia y reparación integral para las víctimas de los conflictos armados internos", explica Juan Matías Gil.

Lo anterior debido a la insuficiente presencia del Estado colombiano, pues la atención integral en salud mental sólo puede encontrarse en las principales ciudades. Los centros de salud de las poblaciones más pequeñas o apartadas no cuentan con estos servicios, por ejemplo "no hay psiquiatra en Buenaventura”, dice Brillith Martínez, psicóloga de MSF en este municipio.

Frente a ese panorama, la situación para las víctimas de violencia sexual, resulta aún peor. Los datos de MSF muestran que **"sólo el 9% de los casos de violación fueron tratados dentro de las 72 horas posteriores al incidente,** lo que limita la eficacia del tratamiento médico y aumenta el riesgo de enfermedades de transmisión sexual y embarazos no deseados".

Finalmente, el informe insta al gobierno a fortalecer su actuación sobre las garantías para el acceso a la atención médica de las víctimas de abusos sexuales, y además brinde las garantías para que se atienda de forma efectiva la salud física y mental de los supervivientes de violencia sexual.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
