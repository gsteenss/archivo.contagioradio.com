Title: Organizaciones se ofrecen como veedoras del cese bilateral entre gobierno y ELN
Date: 2017-08-17 14:29
Category: Nacional, Paz
Tags: ELN, Gobierno, Proceso de paz en Quito
Slug: organizaciones-se-ofrecen-como-veedoras-del-cese-bilateral-entre-gobierno-y-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/PAZ-ELN-CANCILLERIA-Ecuador-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cancillería Ecuador] 

###### [17 Ago 2017] 

Un conjunto de más de 60 organizaciones sociales, plataformas políticas y personas manifestaron, a través de una carta, su apoyo a la tercera ronda de diálogos entre el ELN y el gobierno Nacional, **expresando la disposición que tienen para participar activamente, y desde la sociedad también en la verificación de un cese de hostilidades**.

Luis Eduardo Celis, analista político e impulsor de esta iniciativa, señaló que la intención de la carta es expresar la voluntad que existe desde la sociedad por mediar y acompañar este proceso “**les deseamos los mejores logros en esta cuarta semana de la tercera ronda de diálogos y esperamos que logren avanzar en la dinámica de acciones humanitarias**, en el acuerdo del cese bilateral del fuego y les manifestamos que estamos listos para participar”

La carta se da a conocer en un momento previo a la llegada del Papa Francisco al país, motivada también en la posibilidad de que dicho cese de hostilidades se produzca para esos días, **e incluso se extienda a un plazo más prolongado en el tiempo**. (Le puede interesar: ["Diálogos de paz ELN-Gobierno debe estar acompañado de acciones desde las montañas"](https://archivo.contagioradio.com/el-reto-del-tercer-ciclo-de-conversaciones-entre-eln-y-gobireno-cese-bilateral/))

De igual forma Celis manifestó que se han llevado a cabo 40 encuentros con la ciudadanía para plantear la **propuesta de participación desde la sociedad en el proceso de paz entre el gobierno Nacional y el ELN**. (Le puede interesar: ["ELN está listo para firmar el cese bilateral"](https://archivo.contagioradio.com/42291/))

<iframe id="audio_20389725" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20389725_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

CARTA ABIERTA A GOBIERNO COLOMBIANO Y AL ELN.

17 de agosto de 2017  
Sr. Juan Camilo Restrepo, jefe de la delegación negociadora del Gobierno Colombiano.  
Sr. Pablo Beltrán, jefe de la Delegación negociadora del ELN.  
Cordial saludo.  
En el desarrollo de la cuarta semana de trabajo de la III ronda de diálogos y negociaciones entre el gobierno Colombiano y el ELN, los suscritos, ciudadanas, ciudadanos y organizaciones que trabajamos por un cierre del conflicto armado y unos pactos de paz que permitan a Colombia avanzar en democracia y equidad, queremos manifestarles lo siguiente:  
Estamos listos para concurrir a un proceso de participación, con garantías y rigor, desde una pluralidad social, gremial e institucional, con el fin de intercambiar y concertar sobre transformaciones necesarias para construir este acuerdo de paz que nos acerque hacia una paz completa.  
La sociedad espera del gobierno y del ELN compromisos concretos para avanzar en la garantía de los derechos humanos y el derecho internacional humanitario. El fin del conflicto es un paso indispensable para el momento que vive Colombia.  
Ofrecemos nuestra disposición para participar en un mecanismo de verificación ciudadana al cese bilateral del fuego que pacten las partes.  
Antes de la venida del Papa Francisco es posible anunciarle al país los mayores avances y en lo posible un acuerdo en el punto de dinámicas humanitarias y cese al fuego.  
Sabemos y nos consta que gobierno Colombiano y ELN, trabajan de manera decidida y comprometida por avanzar, en medio de lógicas que aun siendo muy distantes, pueden encontrar un campo común para avanzar en este proceso de diálogos y negociaciones, que nos acerque a una Paz Completa.  
ABC Paz.  
ADEL-Nariño.  
Avina  
Alianza Social Independiente – ASI.  
Asociación Colectivo Mujeres al Derecho  
Asociación de Abogados Laboralistas del Valle  
Asociación de profesores de la Universidad Libre de Cali  
Boyapaz – Programa de Desarrollo y Paz del Occidente de Boyacá  
Campaña Por una Paz Completa  
Casa de la Mujer  
Casa de Paz  
Campaña Colombiana contra Minas  
Consornoc  
Cordupaz  
Corporación Viva la Ciudadanía  
Comisión Intereclesial de Justicia y Paz  
Corporación CSOCIANCOL  
Consejo Latinoamericano de Iglesias - CLAI  
Corporación Programa de Desarrollo y Paz del Magdalena Centro  
Corporación Latinoamericana Sur  
Comunidades Construyendo Paz en los Territorios - CONPAZ  
Ciederpaz-  
Dialogo Intereclesial por la Paz - Dipaz  
Huipaz. Programa de Desarrollo del Huila y Piedemonte Amazónico  
Interteam  
Grupo Ecuménico de Mujeres Constructoras de Paz - GemPaz  
Red Nacional de Programas de Desarrollo y Paz - Redprodepaz  
Cinep Programa por la Paz  
Corporación Humanas  
Corporación Ciase  
Colectivo de Pensamiento y Acción Mujeres, Paz y Seguridad  
Corporación Claretiana Norman Pérez Bello  
Colectiva de Mujeres Refugiadas, Exiliadas y Migradas en España  
Comunidad Teológica Latinoamericana y Caribeña - CETELA  
Instituto Interamericano de Responsabilidad Social y Derechos Humanos – IIRESODH  
Indepaz  
Instituto Popular de Capacitación – IPC.  
Iglesia Presbiteriana de Colombia  
Justapaz  
Fundación Paz y Reconciliación  
Fundación Cultura Democrática  
Fundación Ciudad Abierta  
Fundación Progresar  
Fuerza Común  
No  
Juventud Comunista – Juco  
Observatorio Internacional de la Diaspora  
Pensamiento y Acción Social – PAS.  
Puentes para la Paz  
Punto de Encuentro por la Paz y la Democracia  
Programa de Desarrollo y Paz del Bajo Magdalena  
Programa de Desarrollo y Paz del Magdalena Medio  
Mesa Sucreña por la Paz  
Mujeres Mediadoras  
Mencoldes  
Observatorio de Realidades Sociales de la Arquidiócesis de Cali  
Red de Pobladores de la Redprodepaz.  
Red Caquetá Paz  
Redepaz  
Rodeemos el Diálogo  
Ruta Pacifica de Mujeres  
SI Ambiental  
Sin Olvido  
Teusaquillo Territorio de Paz  
Unión Patriótica.  
Vallenpaz  
Vamos por los Derechos  
Tierrapatria  
Horacio Serpa – Senador de la República.  
Iván Cepeda – Senador de la República  
Alirio Uribe – Representante a La Cámara.  
Antonio Sanguino – Presidente-Vocero Partido Verde  
Alonso Tobón – Presidente de la Alianza Social Independiente  
Jaime Caycedo – Secretario General del Partido Comunista  
Alejo Vargas  
Ana Teresa Bernal  
Álvaro Jiménez  
Adelaida Jimenez  
Alonso Ojeda – Vicepresidente del Comité Permanente de Derechos Humanos  
Agustín Jiménez  
Andrei Gómez Suárez  
Blanca Valle Zapata - Socióloga  
Betty Giedelmann – Bacterióloga  
Camilo González  
Carlos Velandia – Gestor de Paz  
Carlos Fonseca – Director Corporación Simbiosis  
Carlos Medina – Maestro de la Universidad Nacional  
Camilo González  
Camilo Ospina – Politólogo  
Carlos Alberto Benavidez  
Darío Villamizar  
Esperanza Hernández  
Fernando González  
Gloria Ulloa – Presidenta para América Latina y el Caribe Consejo Mundial de Iglesias  
Gabriel Becerra – Secretario de la Unión Patriótica  
Gilberto Herrera Stella - Agricultor  
Gladys Macías – Presidencia Colegiada de Redepaz  
Harold Ruiz – Asesor de Paz de la Gobernación de Nariño  
Hernando Hernández  
Jairo Gómez - Periodista  
José Aristizábal  
Jorge Gómez  
Jaime Zuluaga Nieto  
Juan De Dios Silva-Práctico Agrícola  
León Valencia  
Pedro Santana  
Miguel Galvis – Veedor Nacional de la Alianza Social Independiente  
Monica Yarima Lara Agudelo – Psicóloga  
María del Pilar Suarez - Socióloga  
Norma Inés Bernal  
Jesús Vargas - Presidencia Colegiada de Redepaz  
Katherine Torres  
Luis Ignacio Sandoval - Presidencia Colegiada de Redepaz  
Luis Emil Sanabria - Presidencia Colegiada de Redepaz  
Manuel Guzman Hennessey – Maestro de la Universidad del Rosario  
Mario Aguilera – Maestro del IEPRI de la Universidad Nacional  
Medardo Correa – Investigador Social  
Nelson Cruz  
Padre Pedro Torres  
Pilar Trujillo Uribe  
Rafael Colina  
Rosa Emilia Salamanca  
Ramiro Serna Jaramillo - Economista
