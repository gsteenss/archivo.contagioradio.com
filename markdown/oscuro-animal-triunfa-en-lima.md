Title: 'Oscuro animal' triunfa en Lima
Date: 2016-08-16 16:23
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine Colombiano, Festival de cine de Lima, Oscuro animal, Películas colombianas
Slug: oscuro-animal-triunfa-en-lima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Oscuro_animal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Oscuro Animal Film 

##### [16 Agos 2016] 

La cinta colombiana **Oscuro animal**, se llevó el reconocimiento a mejor película de la  edición 20 del **Festival de Cine de Lima**, que tuvo lugar entre el 5 y el 13 de agosto, sumando un premio más a los obtenidos en Rotterdam, Cartagena de Indias o la Riviera Maya.

En ceremonia realizada el sábado en el Gran Teatro Nacional de Lima, la cinta nacional recibió el premio por parte del jurado compuesto por el argentino Ezequiel Acuña, el colombiano Ciro Guerra, el peruano Alonso Cueto, la mexicana Daniela Michel y el brasileño Karim Aïnouz.

El largometraje del director, montajista, y productor **Felipe Guerrero**, narra el viaje emprendido desde la selva a la ciudad por tres mujeres, que escapan de la guerra rural colombiana, se impuso ante otras **18 producciones** que compitieron por el rubro de ficción, provenientes de países como Brasil, Chile y México.

Al máximo galardón de la competencia, se une el premio de la crítica internacional, por decisión del jurado compuesto por el crítico peruano Claudio Cordero, el argentino Leonardo D'Esposito y la uruguaya Rosalba Oxandabarat, quienes elogiaron la cinta destacando su capacidad de dar "voz, a través de imágenes de sutil belleza, a las víctimas de la violencia armada en América Latina."

<iframe src="https://www.youtube.com/embed/8hMtsgVyHHA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
