Title: COPINH exige que bancos internacionales dejen de financiar proyecto Agua Zarca
Date: 2016-04-25 16:58
Category: El mundo, Movilización
Tags: Berta Cáceres, Copinh, honduras
Slug: copinh-exige-que-bancos-internacionales-dejen-de-financiar-proyecto-agua-zarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Agua-Zarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Lo que somos 

###### [25 Abr 2016] 

El próximo 28 de abril el Consejo Cívico de Organizaciones Populares e Indígenas de Honduras, COPINH, realizará un plantón frente a las instalaciones de los bancos FMO y Finn Fund, financiadores de DESA, la empresa que desarrolla el proyecto hidroeléctrico Agua Zarca, que según el COPINH, ha violentado los derechos de la población indígena Lenca y además, serían quién estuvo detrás del asesinato de la lideresa indígena, Berta Cáceres.

El objetivo es exigir que estas las entidades bancarias retiren el apoyo financiero al proyecto hidroeléctrico, que de acuerdo con el COPINH “ha traído destrucción y muerte al pueblo Lenca y amenaza a nuestro sagrado Río Gualcarque”. **El FMO financia a DESA con 15 millones de dólares; el Banco Finn Fund de Finlandia lo hace con 5 millones de dólares; y el Banco Centroamericano de Integración Económica, BCIE, provee para el proyecto 24.4 millones de dólares.**

Así mismo, desde el COPINH se denuncia que el pasado 15 de abril, empleados de DESA atacaron a miembros de esa organización y participantes en el Encuentro Internacional Berta Cáceres Vive, “el grupo de personas atacantes incluye hombres violentos que Berta Cáceres había denunciado como paramilitares de DESA poco tiempo antes de su asesinato, durante el ataque, **hicieron referencia de haber matado a Berta y que ahora buscan a Tomás Gómez, el actual Coordinador General del COPINH”,** dice el comunicado, donde además se denuncia que tras ese ataque salieron heridas más de 10 personas.

Pese a que las entidades bancarias han suspendido sus pagos al Proyecto Agua Zarca, no han cancelado su financiamiento ni aceptado algún tipo de responsabilidad por las agresiones contra la **comunidad indígena a la cual se le ha violado su derecho a la consulta previa, libre e informada contenida en el Convenio 169.** Es por ello que el COPINH exige la cancelación definitiva y permanente de la financiación de los Bancos FMO, Finnfund, y BCIE al Proyecto Agua Zarca.

La primera jornada de movilización se realizó el pasado viernes 22 de Abril a la frente a la sede del FMO en los Países Bajos, y la siguiente se llevará a cabo el 28 de Abril a las 12 del medio día. En ese marco de denuncia, actualmente integrantes del COPINH visitan los Países Bajos, Finlandia, Bruselas, Alemania y España para exigir al **FMO, Finn Fund, y Voith Hydro que se retiran del Proyecto hidroeléctrico.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
