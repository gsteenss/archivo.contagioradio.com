Title: Plataformas de DDHH expresaron su respaldo a la Minga
Date: 2020-10-17 18:12
Author: AdminContagio
Category: Actualidad
Slug: plataformas-respaldo-minga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Minga-Nacional-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En un comunicado público las plataformas de DDHH que agrupan a más de 500 organizaciones colombianas y de Estados Unidos y Europa manifestaron su respaldo a la Minga adelantada por comunidades indígenas y algunas campesinas han emprendido buscando un diálogo directo con el gobierno Duque. Esta declaración cobra importancia dada la creciente estigmatización que ha señalado que la Minga está infiltrada por organizaciones al margen de la ley.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el comunicado las organizaciones de DDHH señalan que hay una serie de respuestas violentas a las exigencias de diversos sectores sociales por parte del gobierno nacional y que por ello se *"ponen en alerta para proteger el derecho a la protesta y exigirle al gobierno el diálogo político que permita acuerdos serios para la prevalencia del Estado social de derecho"* [Lea también: Avanza la Minga hacia Bogotá](https://archivo.contagioradio.com/7-000-indigenas-de-la-minga-buscan-un-dialogo-con-duque-en-bogota/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Nos ponen en alerta para proteger el derecho a la protesta y exigirle al gobierno el diálogo político que permita acuerdos serios para la prevalencia del Estado social de derecho, la superación de la crisis social que por décadas han padecido los pueblos indígenas y las comunidades rurales, y la ruta para la implementación del Acuerdo de Paz y el desmantelamiento efectivo de todos los actores que generan violencia.

<!-- /wp:quote -->

<!-- wp:paragraph -->

Otra de las preocupaciones manifestadas por las plataformas de DDHH de Colombia, Estados Unidos y Europa tiene que ver con el asesinato de líderes sociales, entre ellos más de 70 líderes indígenas en el gobierno de Duque.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También señalaron como preocupante la acumulación de poder en el ejecutivo y el reiterado incumplimiento a los acuerdos a los que han llegado otros gobiernos tanto con las comunidades indígenas como con otros sectores entre ellos el acuerdo de paz con las FARC.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Plataformas de DDHH preocupadas por la estigmatización a la Minga

<!-- /wp:heading -->

<!-- wp:quote -->

> **"Recordamos que hoy, como en el pasado, los señalamientos de infiltración de la movilización social ponen en grave riesgo la integridad física y la seguridad de las y los marchantes.** La protesta social es un derecho fundamental para la democracia y un mecanismo legítimo de exigencia ciudadana que debe ser respetado."

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por último hicieron un llamado al gobierno Duque para que atienda las exigencias de la movilización y establezca mecanismos de diálogo y de cumplimiento de los acuerdos alcanzados, así como bridarle a los marchantes todas las garantías para el ejercicio del derecho a la protesta. [Lea también: Distrito no podía seguir dando la espalda a la Minga](https://archivo.contagioradio.com/gobierno-distrital-no-podia-seguir-dando-la-espalda-a-la-minga/)

<!-- /wp:paragraph -->
