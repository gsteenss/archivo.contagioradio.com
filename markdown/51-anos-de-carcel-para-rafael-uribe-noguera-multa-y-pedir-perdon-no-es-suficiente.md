Title: Cárcel, multa y pedir perdón no es suficiente en caso Rafael Uribe Noguera
Date: 2017-03-30 13:39
Category: Libertades Sonoras, Mujer
Tags: Condena, feminicidio, Rafael Uribe Noguera, Yuliana Samboní
Slug: 51-anos-de-carcel-para-rafael-uribe-noguera-multa-y-pedir-perdon-no-es-suficiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Yuliana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Semana] 

###### [30 Mar. 2017 ]

**Como una condena ejemplar pero que no es suficiente**, consideraron las organizaciones sociales y la familia de la pequeña niña de 7 años, Yuliana Samboní, **los 51 años y 8 meses de cárcel a los que fue condenado Rafael Uribe Noguera.** Además, se le impuso una multa de 100 salarios mínimos. **La Fiscalía y la familia ya apelaron la decisión**, en la que insisten se le dé a este asesino la condena más alta con la que cuenta Colombia que es de 60 años.

Casi 4 meses tuvieron que esperar los familiares de Yuliana para conocer esta condena, que en todo caso **no implica que Rafael Uribe Noguera no pueda acceder a beneficios por trabajar o estudiar en la cárcel**. Panorama que podría reducir a 30 años la pena privativa de la libertad. Le puede interesar: [El mundo grita, ni una menos viva nos queremos](https://archivo.contagioradio.com/el-mundo-grita-ni-una-menos-vivas-nos-queremos/)

Danny Ramírez, secretaria técnica de la Mesa por la Ley 1257 de 2008 aseguró que si bien la sanción es un caso ejemplar frente a las violencias que están viviendo las mujeres en el país, nunca es suficiente en estos casos.

**“Creemos el tiempo sería justo, quizás la sanción económica es muy flexible pero también sería bueno que la sanción fuera pensada en términos pedagógicos**. Creo que el Estado tiene responsabilidad y sería bueno que la condena fuera ejemplarizante. Este caso lo que nos muestra es que hay fallas en los procesos de formación, de la familia, de los privilegios económicos que tiene este señor” agregó Danny.

### **¿Por qué 60 años y no 51 y 8 meses?** 

**Para Danny la apelación tiene como objetivo no dar cabida a beneficios que pueda tener Rafael Uribe Noguera después** “si es tan ejemplarizante como ha dicho la Fiscal en este caso, pues tiene derecho a tener la mayor condena por el hecho, no podemos ser flexible socialmente en eso. Apelamos a la mayor condena para que sea verdaderamente ejemplarizante”.

Para la activista, **emitir una condena de 60 años manda un mensaje a todos los victimarios o potenciales victimarios sobre que en Colombia tenemos unas medidas** que realmente son fuertes “no vamos a ceder, los victimarios de mujeres deben pagar la mayor condena y mucho más en un caso tan aberrante como lo que pasó con la niña Samboní”.

Al salir de los Juzgados de Paloquemao y conocer la sentencia, **el padre de Yuliana, Juvencio Samboní aseguró que esperaba una mayor condena** y que ahora queda ver qué pasa con la apelación hecha por la Fiscalía. Le puede interesar: [La tristeza nos invade el corazón por el caso de Yuliana y de muchas mujeres en Colombia](https://archivo.contagioradio.com/la-tristeza-nos-invade-por-caso-de-yuliana/)

Rafael Uribe, de 39 años de edad fue sentenciado por feminicidio agravado, acceso carnal violento agravado y secuestro simple.

### **La reacción de la sociedad** 

Desde que se conoció este atroz crimen, **la sociedad colombiana ha expresado su repudio.** En algunos casos asistieron a la clínica donde estaba internado Rafael Uribe Noguera para golpearlo, y en otros casos han expresado su respaldo a la cadena perpetua y a la pena de muerte.

Sin embargo, **Danny manifiesta que la reacción de los colombianos frente a este caso y otros hechos de violencia que ha vivido el país debe permitir “evaluarnos como sociedad** y la evaluación que haríamos de este caso es que somos una sociedad que se ha acostumbrado al castigo y que en términos generales es hipócrita, pues sanciona esto, pero no más hechos similares que acontecen en el país”.

Según cifras oficiales, **en Colombia diariamente aproximadamente 22 niñas son víctimas de violencia y cada día son abusadas sexualmente por lo menos 21 niñas entre los 10 y 14 años.**

Otro de los puntos que abordó Danny es que la familia, las escuelas y las universidades deben educar pensando en el respeto hacia el otro y hacia las mujeres, además dijo que los medios de comunicación no pueden seguir siendo simples amplificadores de los estereotipos.

**“La sociedad colombiana y los medios debemos salir de la coyuntura de noticias**, esta problemática debemos verla como un tema más estructural y analizarlo” puntualizó Danny.

### **Perdón de Rafael Uribe no es suficiente y desconoce a Yuliana** 

Ante la breve carta que fue dada a conocer por Rafael Uribe, en la que pide perdón por "el 4 de diciembre de 2016, Danny dice que, dentro de su condición humana, de victimario condenado envía un mensaje que es políticamente correcto a la sociedad y a la familia.

“La carta está hecha en un plano moralista y esto no es de moral, esto va mucho más allá. Él dice, nosotras miramos como lo recibimos, pero eso no es suficiente. **No es suficiente golpear, matar y luego decir que me perdonen que lo hice en un momento no adecuado,**(…) ese mensaje no es suficiente por el hecho tan atroz que cometió, porque eso lo que sigue es reproduciendo que nos golpean, nos maltratan, pero luego con flores, besos y pidiendo perdón un día lo siguen haciendo” concluyó Danny.

Los hermanos de Rafael Uribe Noguera, Catalina y Francisco actualmente están siendo investigados en este caso por favorecimiento y manipular pruebas. Además tienen prohibido salir del país.

<iframe id="audio_17867930" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17867930_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
