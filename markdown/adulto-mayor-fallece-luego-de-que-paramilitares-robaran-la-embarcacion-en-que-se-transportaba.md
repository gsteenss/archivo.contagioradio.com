Title: Adulto mayor fallece luego de que paramilitares robaran la embarcación en que se transportaba
Date: 2015-09-07 17:15
Category: DDHH, Nacional
Tags: comunidades negras, Conpaz, Cumbre Agraria Étnica y Popular, PCN, Río Naya, Terrotorio Colectivo del Naya
Slug: adulto-mayor-fallece-luego-de-que-paramilitares-robaran-la-embarcacion-en-que-se-transportaba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/naya_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: jenzera.org 

###### [7 Sep 2015]

El pasado domingo 6 de septiembre, paramilitares abordaron dos embarcaciones públicas que se dirigían desde el puerto de **Buenaventura hacia el territorio colectivo del Naya**. Los paramilitares hurtaron las pertenencias de los pasajeros, robaron las embarcaciones y obligaron a las personas a lanzarse al río, **hecho por el cual un adulto mayor murió ahogado y otra persona por un infarto debido al alto nivel de nerviosismo.**

Según la denuncia de la Comisión de Justicia y Paz, organización de DDHH que trabaja en la región, el hecho se presentó hacia las 8 de la mañana en la **boca del Río Cajambre, lugar de tránsito obligado para quienes se dirigen desde o hacia el territorio colectivo de las comunidades negras en el departamento del Cauca**.

**En el relato se evidencia que los hombres armados, 5 en totalidad, a bordo de embarcaciones con motores de alta potencia, interceptan el transporte de pasajeros**, que aumenta los fines de semana, roban las pertenencias de las personas y luego roban también las embarcaciones. En lo corrido del 2015 ya se suman 4 los hechos de esta naturaleza con dos personas muertas por la acción de los paramilitares.
