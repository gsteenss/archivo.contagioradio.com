Title: Se están activando mecanismos para la implementación de Acuerdo de Paz
Date: 2016-12-01 12:32
Category: Paz, Política
Tags: #AcuerdosYA, Mecanismos de implementación
Slug: activados-mecanismos-para-la-implementacion-de-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [1 Dic. 2016] 

Después de la refrendación de los Acuerdos de Paz que se dio el día de ayer en el Congreso, viene la implementación. Aún se está a la espera de la decisión de la Corte Constitucional y hay una cierta incertidumbre frente a saber si el **día D, se daba inmediatamente se refrendaran los acuerdos o luego de que se aprobara la ley de amnistía.**

Para el abogado Enrique Santiago, quien es el asesor jurídico de las FARC-EP, el día D es hoy **“una vez que entre en vigencia el Acuerdo por la Paz, se considera que es el día D”**. No obstante, este día está vinculado con muchos sucesos, por un lado es el día en que entraría en vigencia el acuerdo final, inicia el proceso de traslado a las zonas verdales y por otro lado es el día en que se aprobarían los indultos para todos los presos que tengan delitos indultables y se aprobaría la le de amnistía.

Frente a estos dos puntos, en una rueda de presa  el Ministro del Interior Juan Fernando Cristo anunció que la **Ley de Amnistía se estaría presentando la próxima semana ante el Congreso**, y que se encuentran a la espera de la decisión de la Corte sobre el Fast Track. De igual modo señalo que ya se está dando el proceso para conformar la comisión de seguimiento a la implementación.

En este orden de ideas a partir de hoy, comienza la cuenta regresiva para que los guerrilleros se preparen para su traslado a las 23 zonas veredales y se inicie el proceso de dejación de armas.  El próximo paso sería el día D+5, en donde los guerrilleros empezarían a trasladarse a las zonas. Le puede interesar: ["El Centro Democrático no va a vacilar en métodos para frenar el Acuerdo de Paz"](https://archivo.contagioradio.com/el-centro-democratico-no-va-a-vacilar-en-metodos-para-frenar-el-acuerdo-de-paz-ivan-cepeda/)

No obstante, el analista político Carlos Lozano teme que pese a que se activen los mecanismos de la implementación de los acuerdos **no exista compromiso por parte de las instituciones para garantizar la consolidación de los acuerdos**. Preocupación que surge de la falta de interés que el Estado ha demostrado con la persecución al movimiento político Marcha Patriótica.

“Genera desconfianza  ver que si no van a cumplir en un elemento tan importante como la protección de los dirigentes de la izquierda, pues **menos van a cumplir en otras cosas que tienen que ver con los compromisos asumidos en la Habana**, temas fundamentales como el agrario, político, en la reintegración de los combatientes a la vida política, necesitan recursos”.

<iframe id="audio_14384017" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14384017_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
