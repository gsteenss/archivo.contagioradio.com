Title: "Serbia y Croacia no cometieron genocidio" CIJ
Date: 2015-02-03 17:53
Author: CtgAdm
Category: DDHH, El mundo
Tags: Croacia, guerra de los balcanes, Serbia, TPI para Ex Yugoslavia
Slug: serbia-y-croacia-no-cometieron-genocidio-cij
Status: published

###### Foto:Foro.rkka.hv 

La Corte Internacional de Justicia **(CICJ) se pronunció hoy en contra de las acusaciones formales de Serbia y Croacia de cometer  actos de genocidio** en el transcurso de la guerra (1991-1995) entre ambos países, dentro de la desmembración de la ex república de Yugoslavia.

El tribunal puntualiza **que ambos países están acusados de cometer masacres, y provocar desplazamiento forzado y refugiados, en las dos poblaciones,** aunque reconoce que Croacia sí cometió un crimen masivo contra población serbia.

Después de 16 años la Corte despeja las dudas sobre las acusaciones vertidas por ambos países y que son actualmente el último escollo en la reconciliación.

Las demandas de ambos estados se remontan a 1999, pero en los últimos tiempos ambos habían mostrado su interés en retirar las acusaciones, aunque debido a presiones de sectores nacionalistas esto no había sucedido.

En total  fueron **asesinados 10.000 croatas y 7.000 serbios**. Parte de los 7.000 croatas internados en campos de concentración, 2.000 serbios desaparecidos y 230.000 desplazados.

Los hechos destacados de la guerra fueron el **asedio de la ciudad croata fronteriza de Vukovar por parte de fuerzas serbias y la Operación Tormenta lanzada por el gobierno croata** contra fuerzas serbias, apoyada por operaciones de inteligencia de los EEUU.

La Corte solo  ha pronunciado una sentencia favorable sobre **genocidio, en Srebrenica, Bosnia, por el asesinato sistemático y con el objetivo de exterminar una etnia (la musulmana) de 8.000 varones**. Acusa a las fuerzas Serbias pero no al Estado Serbio, aunque si responsabiliza al mismo por no haberla impedido.

La definición de Lemkim redactada en 1939 califica como genocidio el "..**.Exterminio o eliminación sistemática de un grupo social por motivo de raza, de religión o de política.**.."
