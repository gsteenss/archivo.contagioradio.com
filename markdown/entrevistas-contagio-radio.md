Title: Entrevistas Contagio Radio
Date: 2020-01-28 15:42
Author: AdminContagio
Slug: entrevistas-contagio-radio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/jugar.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/entrevistas-contagio-radio.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/video-.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EN-VIVO-gif.gif" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/entrevistas-contagio-radio-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/entrevistas-contagio-radoi.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/play.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/boton-de-reproduccion.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/entrevistas-contagio-radoi-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/ENTREVISTA.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/PLAY-AMARILLO.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/headphones-690685_1920.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/entrevistas-contagio-radio-2.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Entrevistas
-----------

![jugar](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/elementor/thumbs/jugar-ok23rb48e45x73azoc8rqpdw4w82sm4x46mg1luc8k.png "jugar")

Dale Play ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/jugar.png){width="54" height="54"}

Desde la voz de nuestro invitados el análisis y la opinión de temas de interés nacional e internacional. Dale Play

[  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/logo-contagio-radio-web-1.png){width="201" height="76"}](https://archivo.contagioradio.com/)

#### Orlando Moya, sobre epidemia en comunidad Wounan

https://www.ivoox.com/orlando-moya-sobre-epidemia-comunidad-wounan\_md\_47316417\_wp\_1.mp3

Orlando Moya sobre la crisis sanitaria que se vive en Bajo Baudó, Chocós donde más de 300 niños han sido atendidos en tres comunidades

#### José Antequera, Director del centro de memoria, paz y reconciliación

https://www.ivoox.com/jose-antequera-director-del-centro-memoria-paz\_md\_47314110\_wp\_1.mp3José Antequera, director del Centro de Memoria, Paz y Reconciliación sobre la expulsión de Colombia de la Coalición Internacional de Sitios de Consciencia

#### Cesar Grajales, Director Nacional Premios Diakonia  

https://www.ivoox.com/cesar-grajales-director-nacional-premios-diakonia\_md\_47314061\_wp\_1.mp3

Visita de incidencia por Estados Unidos de los ganadores del Premio Nacional a la Defensa de los Derechos Humanos en Colombia

#### Julian González sobre el observatorio de la JEP

https://www.ivoox.com/julian-gonzales-sobre-observatorio-ante-jep\_md\_47312947\_wp\_1.mp3

En Bogotá se lanza el Observatorio sobre la Justicia Especial para la Paz (JEP)

#### Davin Hurtado de COCCAM sobre sustitución de cultivos de uso ilicito

https://co.ivoox.com/es/davin-hurtado\_md\_47057335\_wp\_1.mp3Desconocimiento del Gobierno de la situación que viven los líderes sociales e incumpliendo a los acuerdos en el tema de sustitución de uso ilícito

#### Sergio de Zubiria -75 años de Auschwitz

https://co.ivoox.com/es/sergio-zubiria\_md\_47010527\_wp\_1.mp3

A 75 años de la liberación de prisioneros del campo de concentración de Auschwitz cobra relevancia el conocer la historia y así garantizar la no repetición

#### Junior Maldonado de ASCAMCAT - Asesinato de líder en Catatumbo  

https://co.ivoox.com/es/junior-maldonado\_md\_47010514\_wp\_1.mp3%5D

Sobre el asesinato del líder Fernando Quintero, presidente de la Junta de Acción Comunal (JAC) de la vereda Guasiles Norte en Convención, 

#### Alexander Butrion de Asoinca sobre la situación de educación en zonas

https://co.ivoox.com/es/alexander-butrion\_md\_47010461\_wp\_1.mp3%5D

A propósito de las cifras del Banco Mundial sobre la inversión para la educación en Latinoamérica, ¿cuál es la situación en Colombia?

#### Alejo Vargas - Proyectos importantes para la agenda

https://co.ivoox.com/es/alejo-vargas\_md\_47010435\_wp\_1.mp3

El profesor Alejo Vargas analiza como fundamental en 2020 la reforma política electoral  y habla sobre los movimientos que habrá este año en el Congreso.

Archivo de entrevistas
----------------------

<iframe src="https://co.ivoox.com/es/player_es_podcast_168710_1.html" width="100%" style="border: 1px solid #0e3775;" height="440" frameborder="0" allowfullscreen="0" scrolling="no"></iframe>

Entradas recientes
------------------

</i>","nextArrow":"","autoplay":false,"autoplaySpeed":5000,"rtl":false}' dir="ltr"&gt;  
[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)  
Salir a las calles, lo único que queda para detener las reformas Ante las reformas y la ‘nueva’ configuración en el Congreso,...  
[](https://archivo.contagioradio.com/salir-a-las-calles-lo-unico-que-queda-para-detener-las-reformas/)  
[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)  
En Patía, Cauca, siembran las semillas de la esperanza para salvar el bosque seco tropical Comunidades de El Pilón, Galindez y Palo Verde participaron en...  
[](https://archivo.contagioradio.com/en-patia-cauca-siembran-las-semillas-de-la-esperanza-para-salvar-el-bosque-seco-tropical/)  
[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)  
Campesino habría sido asesinado por policías en Tumaco La comunidad del Río Mejicano, en Tumaco-Nariño, denunciaron que el...  
[](https://archivo.contagioradio.com/campesino-habria-sido-asesinado-por-policias-en-tumaco/)  
[Nacional](https://archivo.contagioradio.com/categoria/nacional/)  
Javier Girón Triviño, indígena Nasa asesinado en Santander de Quilichao La Organización Nacional Indígena (ONIC), denunció el asesinato de Javier...  
[](https://archivo.contagioradio.com/asesinan-al-guardia-indigena-javier-giron-trivino/)  
[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)  
Estudiantes logran histórico acuerdo en U.Distrital Estudiantes de la Universidad Distrital lográn historico acuerdo para la...  
[](https://archivo.contagioradio.com/estudiantes-logran-historico-acuerdo-en-u-distrital/)  
[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)  
Carbones del Cerrejón incumple sentencia que protege a comunidades y al Arroyo Bruno En audiencia de seguimiento a la sentencia 698 de la...  
[](https://archivo.contagioradio.com/carbones-del-cerrejon-incumple-sentencia-que-protege-a-comunidades-y-al-arroyo-bruno/)  
[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)  
La llegada de un «camaleón rabioso» a la Fiscalía Muchos califican a Barbosa como camaleón, en la nota, distintas...  
[](https://archivo.contagioradio.com/la-llegada-de-un-camaleon-rabioso-a-la-fiscalia/)  
[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)  
Organizaciones de DDHH denuncian vigilancia y hostigamiento con drones La Comisión Colombiana de Juristas denunció que ha sido víctima...  
[](https://archivo.contagioradio.com/organizaciones-de-ddhh-denuncian-vigilancia-y-hostigamiento-con-drones/)  
[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)  
En Colombia los niños crecen hablando de muerte: Oxfam Este jueves organizaciones sociales presentaron un informe de seguimiento a...  
[](https://archivo.contagioradio.com/en-colombia-los-ninos-crecen-hablando-de-muerte-oxfam/)  
[Ambiente](https://archivo.contagioradio.com/categoria/ambiente/)  
No solo es el derrumbe de un barranco en Calarcá En la vereda El Crucero lo que se está derrumbando...  
[](https://archivo.contagioradio.com/no-solo-es-el-derrumbe-de-un-barranco-en-calarca/)  
[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)  
El uribismo sigue culpando a ‘las FARC’ de lo que pasa en Colombia La declaración de la Ministra de Interior sobre la implementación...  
[](https://archivo.contagioradio.com/el-uribismo-sigue-culpando-a-las-farc-de-lo-que-pasa-en-colombia/)  
[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/)  
Visita del CPI en Colombia respalda labor de la JEP CPI presenta su respaldo a la JEP esperando que los...  
[](https://archivo.contagioradio.com/visita-del-cpi-en-colombia-respalda-labor-de-la-jep/)  
[Líderes sociales](https://archivo.contagioradio.com/categoria/ddhh/lideres-sociales-ddhh/)  
Las enseñanzas de ‘don Temis’ son símbolo de resistencia en el barrio Isla de la Paz Durante los pasado 27 y 28 de enero, la comunidad...  
[](https://archivo.contagioradio.com/las-ensenanzas-de-don-temis-son-simbolo-de-resistencia-en-el-barrio-isla-de-la-paz/)  
[Nacional](https://archivo.contagioradio.com/categoria/nacional/)  
Desconocimiento e incumplimiento del Gobierno con la sustitución de cultivos de uso ilícito Comentario del director del Programa Nacional de Sustitución ignora los...  
[](https://archivo.contagioradio.com/coccam-exige-garantias-para-las-familias-que-trabajan-por-la-sustitucion-de-cultivos/)  
[Comunidad](https://archivo.contagioradio.com/categoria/ddhh/comunidad/)  
Taitas del Amazonas le dicen basta a los impostores del Yagé Chamanes y sabedores de Colombia exigen que se garantice la...  
[](https://archivo.contagioradio.com/taitas-del-amazonas-le-dicen-basta-a-los-impostores-del-yage/)  
[  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/logo-contagio-radio-web-1.png){width="201" height="76"}](https://archivo.contagioradio.com/)
