Title: Cine y video por la Paz en la Comuna 13 de Medellín
Date: 2015-09-28 12:42
Author: AdminContagio
Category: 24 Cuadros, eventos
Tags: 5to Festival de Cine y Video Comunitario Comuna 13, Festival Cine comuna 13 Medellin
Slug: cine-y-video-por-la-paz-en-la-comuna-13-de-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/12038022_910864052284774_5186751142751667237_n-e1443466711616.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [28, Sep 2015]

Con el conversatorio "Una Mirada al posconflicto desde el cine en América Central¨, se dará inicio al **5to Festival de Cine y Video Comunitario Comuna 13** "La otra historia"; un espacio que busca proponer discusiones frente al tema de la Paz y el Posconflicto, desde el lenguaje audiovisual.

Desde el lunes 28 de septiembre hasta el próximo 3 de octubre, espacios universitarios, barriales y comunitarios de la capital antioqueña, servirán de escenario para el desarrollo de la programación, de exhibición y académica del encuentro que tiene como epicentro la comuna 13 de la ciudad de Medellín.[![logo comuna](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/11406834_866267930077720_130734974736980777_n.png){.wp-image-14851 .alignright width="170" height="169"}](https://archivo.contagioradio.com/sonidos-del-metal-y-la-psicodelia-ira-profana/11406834_866267930077720_130734974736980777_n/)

El propósito principal del Festival, es propiciar un espacio de participación y de intercambio de experiencias y saberes originadas en los ambitos regional, nacional e internacional, teniendo para esta edición a Honduras, Nicaragua, Guatemala y El Salvador como países invitados de honor.

Las películas, cortos, y documentales que hacen parte de la programación del evento, aportan desde diferentes miradas y perspectivas, a la construcción de un concepto tan complejo como lo es la **Paz**, dentro de las categorías: documental, ficción, video clip, animación y video experimental.

Adicionalmente, el Festival busca cambiar la percepción que se tiene a nivel local, municipal, regional, nacional e internacional de la comuna 13 de Medellín, mediante la difusión de piezas audiovisuales que den cuenta de los diferentes procesos sociales, culturales, políticos y de re construcción del tejido social en la comuna.

La intervención en espacios públicos y privados para el desarrollo de las diferentes actividades programadas, servirá para promover la identidad colectiva, la movilización comunitaria dentro de las "fronteras invisibles" creadas como método de control, y la creación audiovisual por parte de los miembros de la comunidad gracias a los procesos de formación y asesoría colectiva que puedan ser proyectados en otros eventos similares.

<iframe src="https://www.youtube.com/embed/yv3n2SqXnpI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Consulte la programación completa del Festival [Aquí](http://festival2015.wix.com/5festivalcomuna13#!programacin/cev5)

 {#section .font_2}
