Title: Director del Banco Mundial justifica el asesinato de Berta Cáceres
Date: 2016-04-28 12:46
Category: Ambiente, DDHH, El mundo
Tags: Banco Mundial, Berta Cáceres, Hidroeléctricas
Slug: banco-mundial-justifica-el-asesinato-de-la-lideresa-berta-caceres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Jim-Yong-Kim-Banco-Mundial.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Financiero ] 

###### [28 Abril 2016]

Como un "incidente" calificó el presidente del Banco Mundial [Jim Yong Kim, el asesinato de la lideresa indígena Berta Cáceres, durante el ']Seminario Teológico de la Unión' en Nueva York. En su discurso habló del [[cambio climático](https://archivo.contagioradio.com/?s=cambio+climatico+)] pero sobrepuso la "necesidad de energía eléctrica", asegurando que no satisfacerla constituye "una violación a los derechos humanos".

["No se puede hacer el tipo de trabajo que estamos tratando de hacer y que no sucedan algunos de estos incidentes", aseguró Kim, insistiendo en que las [[hidroeléctricas son la alternativa](https://archivo.contagioradio.com/?s=hidroelectricas+)] a la "terrible y horrible situación del cambio climático",] desconociendo que la implementación de proyectos hidroeléctricos ha resultado en violaciones a los derechos humanos e impacto ambientales negativos en distintas partes del mundo.

Cabe resaltar que el asesinato de [[Cáceres](https://archivo.contagioradio.com/?s=berta+caceres+) ]se dio en el marco de la lucha que adelantaba con comunidades indígenas hondureñas para oponerse al proyecto hidroeléctrico Aguazarca, promovido por la empresa DESA y financiado por los bancos extranjeros FMO y Finn. Por otra parte, Greenpeace ha asegurado que estos proyectos aportan **por lo menos el 4% de los gases de efecto invernadero**, contaminan el agua, provocan disminución en los caudales y generan bajos niveles de pesca.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
