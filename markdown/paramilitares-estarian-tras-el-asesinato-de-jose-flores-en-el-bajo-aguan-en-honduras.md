Title: Paramilitares estarían tras el asesinato de José Flores en el Bajo Aguan en Honduras
Date: 2016-10-19 13:31
Category: DDHH, El mundo
Tags: Bajo Aguan, honduras, Jose Flores, Movimiento Campesino Unificado del Aguan, paramilitares
Slug: paramilitares-estarian-tras-el-asesinato-de-jose-flores-en-el-bajo-aguan-en-honduras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/muca-honduras-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: laprensa] 

###### [19 Oct 2016]

En horas de la tarde de este 18 de Octubre fue asesinado José Angel Flores, presidente del Movimiento Campesino Unificado del Aguan. El hecho se perpetró minutos después de terminar una reunión con los campesinos de la región en la que se estudió la situación en que terratenientes y paramilitares desplazan forzadamente y realizan **negocios de narcotráfico encubiertos tras las plantaciones de palma de aceite.**

Según Vitalino Álvarez, integrante de la junta directiva del Movimiento Unificado Campesino del Aguán, el asesinato de Flores se dio después de varios intentos fallidos de asesinato y a pesar de las medidas cautelares de la CIDH que benefician a los líderes de la organización campesina. En el mismo hecho fue asesinado también Silmer Dionisio George, líder campesino de la región con el que **se completa la escandalosa cifra de 150 campesinos asesinados desde 2009.** Lea También ([Así está la situación de Derechos Humanos en Honduras](https://archivo.contagioradio.com/?s=honduras))

Álvarez denuncia que **el control paramilitar es permanente en la región** y que incluso se han cambiado mandos militares para entregar el control a los terratenientes como Miguel Facussé que es asesorado por paramilitares colombianos. Además agregan que están seguros de que el negocio de la palma de aceite es apenas una cortina para esconder el negocio del narcotráfico, puesto que el aterrizaje de avionetas es permanente en las propiedades del empresario.

En la región del Bajo Aguan, departamento de Colon, costa atlántica de Honduras, se vienen denunciando diversas actuaciones de tipo paramilitar por parte de empresarios palmeros, una de ellas es la **apropiación ilegal de las tierras de los campesinos con complicidad de las autoridades militares y civiles, la intimidación y las amenazas** con el uso de personas armadas y ejércitos privados al servicio de los intereses empresariales.

Además, Álvarez asegura que hay personajes en la región que tienen nexos con el senador Alvaro Uribe que ha visitado la región en varias ocasiones. En entrevistas anteriores los integrantes del MUCA han denunciado que las visitas del congresista colombiano han estado seguidas por una serie de **hechos violentos contra los campesinos que se están organizando para defender sus territorios.** (Lea Tambien [Organismos financieros deben dejar de financiar proyectos en Honduras](https://archivo.contagioradio.com/copinh-exige-que-bancos-internacionales-dejen-de-financiar-proyecto-agua-zarca/))

Tras estos asesinatos la preocupación por la situación de Derechos Humanos en Honduras preocupa puesto que tras el golpe de Estado perpetrado contra Manuel Zelaya se han incrementado los asesinatos, las amenazas y los hostigamientos que permanecen, en su mayoría, en la impunidad, así lo relató la **Comisión Interamericana de DDHH** en su informe entregado en febrero de 2016. (Lea también [Prevalece la impunidad en el asesinato de Berta Cáceres](https://archivo.contagioradio.com/?s=berta+caceres))

<iframe id="audio_13389208" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13389208_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
