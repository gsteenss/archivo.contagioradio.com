Title: Claudia López es lo mismo que Peñalosa en movilidad: Carrillo y Arias
Date: 2020-03-04 00:33
Author: AdminContagio
Category: Actualidad, Política
Tags: Bogotá
Slug: claudia-lopez-es-lo-mismo-que-penalosa-en-movilidad-carrillo-y-arias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Claudia-Lopez.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/carlos-carrillo-sobre-politicas-alcaldesa-frente-a_md_48518878_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Carlos Carrillo | Consejal de Bogotá

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Este martes 3 de marzo, en un comunicado, el concejal de Bogotá Carlos Carrillo y el senador Wilson Arias se preguntaron si la alcaldesa Claudia López será la continuación de las políticas de Peñalosa en términos de movilidad. El comunicado se produce tras el aumento en 100 pesos de la tarifa de Transmilenio, y que aparentemente, no se activará la tarifa estudiantil durante este mandato.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1234798937061646338","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1234798937061646338

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **"El aumento a las tarifas de Transmilenio es una cachetada a los bogotanos"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Concejal por el Polo Carlos Carrillo señaló que hay temas que rescatar de la actual administración, como el cumplimiento de las promesas en términos ambientales, pero en cuanto a infraestructura y movilidad, parece que Claudia López es la continuación de Enrique Peñalosa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La declaración surge teniendo en cuenta **que durante la administración Peñalosa la tarifa aumentó 600 pesos**, y cumplidos apenas 3 meses desde que inició su mandato, López decreta el aumento en 100 pesos a la tarifa. (Le puede interesar: ["Las cinco modificaciones que tendría incluído el proyecto Metro de Bogotá"](https://archivo.contagioradio.com/modificaciones-proyecto-metro-de-bogota/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/HOLLMANMORRIS/status/1234469442136006660","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/HOLLMANMORRIS/status/1234469442136006660

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Para Carrillo, este aumento es "una cachetada a los bogotanos" que pagan por el servicio de transporte más caro de latinoamérica y a diario tienen que padecerlo, porque su capacidad está rebasada. Adicionalmente, el Concejal cuestionó que la alcaldesa no acepte que fue su decisión este aumento, cuando fue ella quién firmó el documento que lo generaba.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El transporte público puede dejar de ser un negocio de unos pocos y comenzar a beneficiar a toda la ciudad

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DavidRacero/status/1233505323132096516","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DavidRacero/status/1233505323132096516

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Mientras en Bogotá se aumenta la tarifa, otros lugares como Luxemburgo han dado paso a la gratuidad en el transporte público como una forma de incentivar el uso de este servicio y mejorar así las condiciones ambientales, evitando el uso de vehículos particulares.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, para Carrillo, en Bogotá es difícil que esto ocurra porque no hay voluntad política y quienes dirigen la ciudad así como el país "están jugados por una política que privilegia a los privados". Según se ha denunciado, el 95% de las ganancias del sistema queda en manos privadas mientras solo el 5% entra al Distrito, teniendo este que invertir en infraestructura.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, el Concejal sostuvo que hasta que no se cambie el modelo de negocio y no haya operadores que utilicen el transporte público como una forma de lucro muy rentable, será imposible en dar un paso como el de Luxemburgo para proteger el ambiente de Bogotá. (Le puede interesar: ["Protocolo de Claudia López para la protesta sin resultados satisfactorios: Personería"](https://archivo.contagioradio.com/protocolo-de-claudia-lopez-para-la-protesta-sin-resultados-satisfactorios-personeria/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El metro, y otras formas de evaluar qué tanto de Peñalosa tiene Claudia López**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Durante la administración Peñalosa, el argumento más usado para defender la construcción del metro elevado era que hacía lo mismo que uno subterráneo, era más rápido de construir y más barato. En el debate sobre el tema que ocurrió en el Concejo el pasado lunes 2 de marzo, se denunció que no tendría la misma capacidad de un sistema subterráneo, su construcción tampoco sería la más rápida y mucho menos barata.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Carrillo, lo relevante de este tema no es si el metro elevado se demora 7, 10 o 15 años, porque lo importante es que el Proyecto costará 4.500 millones de dólares o más, "y eso impediría la construcción de una red de metro en Bogotá", que es el sistema de transporte más efectivo para una capital con más de 8 millones de habitantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según se denunció en el [comunicado](https://comunicados.wilsonarias.net/2020/03/es-claudia-lopez-la-continuidad-de.html), otro punto de coincidencia entre Peñalosa y López, es el no tener en cuenta la tarifa estudiantil para ingresar al sistema Transmilenio. Dicha tarifa se reglamentó en el Acuerdo 615 de 2015, pero no aparece consignada en el primer borrador del Plan Distrital de Desarrollo presentado por la Alcaldesa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para el concejal, será con la presentación final del Plan que se sepa finalmente cuánto de Peñalosa tendrá López en su administración, por lo que habrá que esperar para ver si temas como el sistema distrital de cuidado, entre otros aparecen reglamentados. Sin embargo, sostuvo que en materia de infraestructura y movilidad, no espera nada distinto a lo ocurrido en los últimos cuatro años.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
