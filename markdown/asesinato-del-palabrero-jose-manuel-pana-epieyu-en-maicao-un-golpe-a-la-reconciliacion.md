Title: Asesinato del palabrero José Manuel Pana Epieyú en Maicao, un golpe a la reconciliación
Date: 2019-09-15 15:18
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Asesinato de líderes indígenas, Chocó, La Guajira, Maicao, Pueblo Embera Katio
Slug: asesinato-del-palabrero-jose-manuel-pana-epieyu-en-maicao-un-golpe-a-la-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/José-Manuel-Pana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Nación Wayuú] 

El pasado 13 de septiembre fue asesinado **José Manuel Pana Epieyú, autoridad tradicional de Karaquita**, resguardo étnico de la zona rural de Maicao. Los hechos sucedieron según testigos, en la vía que comunica a este municipio con Riohacha, en La Guajira.

Según información preliminar, Jose Manuel fue abordado por hombres armados cuando se movilizaba en un vehículo del que fue obligado a bajarse. Sin que se conozcan mayores detalles sobre el asesinato del líder indígena, autoridades afirman que ya se ha abierto una investigación para esclarecer el crimen. [(Le puede interesar: En una semana amenazan dos veces la organización Fuerza Mujeres Wayúu)](https://archivo.contagioradio.com/amenazan-dos-veces-fuerza-mujeres-wayuu/)

José Manuel fue miembro de la Junta Autónoma Mayor de Palabreros por los Derechos Humanos y era **reconocido como un líder entre las comunidades indígenas por su labor como palabrero o *pütchipü,* responsables de solucionar los conflictos entre las familias a través del diálogo.** [(Lea también: José Ceballos, líder y docente Wayúu fue asesinado en Riohacha, La Guajira)](https://archivo.contagioradio.com/jose-ceballos-lider-y-docente-wayuu-fue-asesinado-en-riohacha-la-guajira/)

"Un palabrero es aquella persona que tiene ese don, empírico, ese derecho de conciliar y dirimir los conflictos, por ende es una persona de respeto, de mucho aprecio y de consideración entre nosotros" explica José Silva de Nación Wayúu, quien señala que el asesinato de esta autoridad tradicional, deja un gran vacío en la comunidad pues "cada persona, cada anciano que muere se lleva una parte del legado wayuú".

### Continúan ataques contra la población indígena

De igual forma el pasado 12 de septiembre, en horas de la mañana en la Comunidad Brisas, del Pueblo Embera Katio, en el Municipio de Bagadó, Chocó, el docente y guardia indígena, Misael Queragama Tequia fue atacado por integrantes de grupos armados ilegales, según la información brindada por la ONIC.

“Asesinar a un Palabrero es atentar contra la reconciliación de La Palabra, La Palabra Wayuu. Rechazamos y demandamos celeridad en la investigación bajo el respeto de la Cultura Wayuu” señalaron desde la Organización Nacional Indígena de Colombia (ONIC) quienes condenaron el asesinato del palabrero **José Manuel Pana Epieyú.**

En medio de los hechos, Misael fue herido por un impacto de bala que ingresó por la parte inferior de su abdomen y salió por su pierna derecha, como resultado de esta herida, su estado de salud es crítico, según alerta el comunicado de la organización indígena.

Como consecuencia de estos hechos,  los integrantes de la comunidad Brisas se encuentran confinados y en una situación de desabastecimientos de elementos de primera necesidad como alimentación y servicios de Salud.

Noticia en desarrollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_41571649" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_41571649_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
