Title: Could political candidates be behind the wave of electoral violence?
Date: 2019-09-17 11:56
Author: CtgAdm
Category: English
Tags: Electoral violence, local elections, Peace and Reconciliation Foundation
Slug: could-political-candidates-be-behind-the-wave-of-electoral-violence
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Violencia-Electoral.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Contagio Radio] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[According to the Peace and Reconciliation Foundation (Pares), violence against candidates and precandidates in the upcoming local elections on Oct. 27 is on the rise. Of the 20 assassinations of candidates committed during the last 10 months of the electoral calendar, seven occurred during the last two months. This indicates that as the election approaches, the rate of murders has increased.]

[Acts of electoral violence have been documented in 24 provinces, that is, about 75% of the country. Yet, they remain concentrated in only five provinces: Cauca, Valle del Cauca, La Guajira, Tolima and Antioquia. For Daniela Gómez, coordinator of Governability and Peace Democracy at Pares, this phenomenon is more than just violence perpetrated by illegal armed actors. It’s a mechanism, she argued, used by political contenders to win over the competition. This explains why in provinces such as Tolima there are a great number of threats, but few illegal armed actors.]

[Pares suggested that the increase in assassinations, assassination attempts and forced disappearances could be linked to the advanced stage of the elections. Official candidates are easier to identify as well as those that appear to be strong contenders.]

### **Despite pacts for peace, electoral violence increased**

[Gómez indicated that the electoral dynamics differ from those in the national races because the pacts for non-violence have little effect on the violence that happens on the ground. One example is Bernardo Betancurt, the mayoral candidate of Tibú, Norte de Santander, who was assassinated despite having signed a non-violence pact.]

[The researcher argued that at this point, at only a few weeks from the elections, it’s too late to create any structural change to prevent these mechanisms of violence from operating, but it is necessary to readjust the budget and reinforce the security details of the candidates, particularly for those in regions at high risk.]

[Her report also revealed that government coalitions and the opposition are the sectors most affected by this violent wave. Gómez explained that “these are the sectors that are on the rise and gaining strength since the past elections.” Still, she added that analyzing individual attacks, the political party most affected is the Party of the U, which indicates that candidates of independent parties have also been victims of electoral violence.]

[Aunque destacan que la violencia podría estar vinculada al accionar de los mismos candidatos, el informe señala a su vez que los responsables de los actos de violencia electoral siguen sin ser identificados, algo que aplica para el 71% de las víctimas, mientras que el 29% restante corresponde a la autoría de grupos como Las Águilas Negras, las autodenominadas Autodefensas Gaitanistas de Colombia, el ELN, Muerte Enemigos de la Patria (MEP), Caparrapos, Fuerza Pública y disidencias de las FARC.]

[Although the report said that the violence could be linked to the candidates themselves, it also highlighted that those responsible for most of the crimes — about 71% of them — have yet to be identified. In the rest of cases — about 29%, armed groups, such as the Black Eagles, the Gaitanista Self-Defense Forces of Colombia, Death Enemies of the Homeland (MEP), Caparrapos, the military and FARC dissident groups. ]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
