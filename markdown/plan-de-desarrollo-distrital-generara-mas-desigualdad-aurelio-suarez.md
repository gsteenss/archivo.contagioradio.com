Title: "Plan de Desarrollo Distrital generará más desigualdad" Aurelio Suárez
Date: 2016-06-01 17:57
Category: Nacional
Tags: Peñalosa, Plan de Desarrollo
Slug: plan-de-desarrollo-distrital-generara-mas-desigualdad-aurelio-suarez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/peñaloser-e1464821466726.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  ctdpbogota.org] 

###### [1 Junio 2016 ]

El pasado 31 de mayo se aprobó el Plan de Desarrollo Distrital propuesto por el Alcalde Peñalosa que tiene dentro de su articulado puntos como la venta de la empresa pública ETB, el pago del alumbrado público y la posible urbanización de la Reserva Van Der Hammen.

De acuerdo con Aurelio Suarez, analista económico, hechos como el de urbanizar la Reserva Van Der Hammen, dedicar el **49% del presupuesto a la movilidad** con prioridad a las troncales de Transmilenio, volver a **los colegios en concesión o tener infraestructuras hospitalarias con anexos público -  privadas** correspondería a la dinámica de crear negocios incluso en medio de la crisis para darle  espacio a nuevos nichos de negocio al capital. "En conclusión es un plan de desarrollo donde los grandes protagonistas no son los ciudadanos sino es el gran capital financiero, el gran capital internacional, son los grandes operadores de los negocios que hay en Bogotá" asevero el analista.

**Aprobación de la  urbanización de la Reserva Van Der Hammen **

Según  Suarez, la aprobación de **la urbanización de la reserva Van Der Hammen se dio de una forma eufemística o figurad**a, debido a que en el Plan de Desarrollo Distrital formulado por el alcalde Peñalosa, se expone que habrá  desarrollo de habitad en el Distrito Norte, que implícitamente hace [referencia a la instalación y desarrollo urbanístico en áreas de la  Reserva Van Der Hammen](https://archivo.contagioradio.com/reserva-thomas-van-der-hammen-y-su-futuro/). Sin embargo, Suarez afirma que no podía decirse tan claramente debido a que la responsabilidad de la urbanización recae en la Corporación Autónoma Regional y no sobre el Concejo Distrital y[que esta medida es solamente la inclusión en el Plan de Desarrollo para su puesta en ejecución.](https://archivo.contagioradio.com/los-enredos-de-los-funcionarios-que-estan-detras-de-la-urbanizacion-de-la-van-der-hammen/)

La propuesta del mega proyecto urbanístico de Peñalosa cuenta con la construcción de más de 5.924 hectáreas en las que se incluyen  **1395,16 de la Reserva de Thomas Van Der Hammen.**

**Pérdida del Patrimonio Público**

El Plan de Desarrollo Dístrital aprobado por el Concejo c[ontiene varios puntos que generan pérdidas del patrimonio público](https://archivo.contagioradio.com/distrito-reducira-de-164-a-40-los-centros-basicos-hospitalarios/), de acuerdo con el analista, entre estos se encuentra el pago por el alumbrado público, el pago de impuestos para no tener pico  y placa o la instalación de peajes urbanos, que en conjunto son medidas que restringen el espacio y lo privatizan o que seden la responsabilidad de la financiación distrital a la ciudadanía. Con estas medidas se estima que el Plan de Desarrollo incremente los impuestos en 5,5 billones de pesos y se evalúa que al finalizar el periodo del alcalde  exista una recarga de **\$800.000 mil pesos más, en un salario mínimo, por el pago de los tributos y los impuestos.**

Por otro lado el Plan de Desarrollo también contempla la re ubicación de parqueaderos y otros proyectos como la cárcel Modelo, la cárcel Buen Pastor y Corabastos.

**Movilidad**

44 billones del total del presupuesto del Plan de Desarrollo se destinarán a la movilidad, que se enfocaran en la construcción de troncales de transmilenio, [la financiación de una parte del metro, la construcción de vías en concesión publico - privadas](https://archivo.contagioradio.com/plan-de-desarrollo-de-penalosa-no-tiene-nada-que-ver-con-la-paz-hollman-morris/) como la Avenida Logitudinal de Occidente, la calle 13, entre otras. A su vez, se propone incluir peajes en las vías de acceso, en las avenidas circunvalares y de alta velocidad de Bogotá, para así recaudar un monto de 55.000 millones de pesos al año. Por ahora los peajes se ubicaran en la Autopista Norte y Sur, Carrera 7a, calle 13, vía a **Villavicencio y la Calera y las   tarifas tendrán un costo de particulares \$2.000 y camiones de carga: entre \$3000 y \$5000**.

De acuerdo con el analista es importante tener en cuenta que detrás de los operadores privados del SITP y Transmilenio y detrás de los constructores esta el capital financiero, por ejemplo todos los operadores del Sistema Integrado de Transportes le están debiendo a la banca nacional más de **2,3 billones de pesos,** luego al final de cuentas quién se beneficia es el capital financiero y me refiero fundamentalmente a los grades grupos económicos del país, Peñalosa es un hombre del Banco mundial y del grupo financiero internacional y el Plan de Desarrollo se ajusta a esas necesidades.

**Venta de la empresa pública ETB**

La veta de la empresa pública ETB de acuerdo con Aurelio Suarez, es injustificable debido a su importancia y potencialidad para crecer, a su vez, afirma que las condiciones de cableado y redes de fibra óptica están en la capacidad de [brindar un mejor servicio incluso que el de otras empresas.](https://archivo.contagioradio.com/bogotanos-se-manifestaron-contra-la-venta-de-la-etb-frente-al-concejo/) La ETB esta evaluada para su venta en **2,5 billones de pesos con lo que se pretende financiar, según el alcalde Peñalosa, infraestructura para el plan de movilidad, salud y educación.**

El analista concluye que este Plan de Desarrollo servirá para aumentar la desigualdad social - "La ciudad va hacia una segunda oleada de usurpación del patrimonio público en manos del gran capital financiero y de quienes lo operan y le sirven agentes en la ciudad"- y considera que será la movilización social y **las acciones ciudadanas las que puedan reversar las políticas del actual alcalde.**

<iframe src="http://co.ivoox.com/es/player_ej_11747801_2_1.html?data=kpaklpycdJKhhpywj5eVaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncaLp08rZy9SPl9bV08rnjZKPhc%2FVzc7g1saRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
