Title: Paro estudiantil de la UPTC logra acuerdo sobre definición en costo de matrículas
Date: 2017-10-03 17:11
Category: Educación, Nacional
Tags: Movilización estudiantil, UPTC
Slug: paro-estudiantil-de-la-uptc-acuerdo-sobre-definicion-en-costo-de-matriculas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-03-at-4.59.51-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Rebelde medios Alternativos] 

###### [03 Oct 2017] 

Tras dos semanas de Asamblea Permanente los estudiantes de la Universidad Pedagógica y Tecnológica de Colombia (UPTC) decidieron retornar a la normalidad académica, luego de que la institución **aplazara hasta el primero de diciembre el reajuste en el cobro de las matrículas** y se conozca públicamente el estudio socioeconómico que se realizará para este.

De igual forma los estudiantes **lograron acordar con las directivas un canal de participación más directo con toda la comunidad que consistirá en usar las asambleas** de forma vinculante y en esos escenarios construir una propuesta alternativa de reajuste de matrícula para que las personas que ingresen a la Universidad no se vean afectadas.

A su vez, los estudiantes están debatiendo con las directivas estrategias para recuperar las dos semanas de paro, de forma tal que no se vea afectado el calendario académico, debido a que tan solo llevan cursadas las 6 primeras semanas del semestre. (Le puede interesar: ["Reajuste en costo de matrículas motiva asamblea permanente en UPTC")](https://archivo.contagioradio.com/reajuste-en-costo-de-matriculas-motiva-asamblea-permanente-en-la-uptc/)

De acuerdo con Camilo Tavera, durante las semanas de movilización, la administración de la Universidad sí ejerció presión para que se levantar la asamblea permanente o de lo contrario suspender actividades académicas, situación que impactaría negativamente a los estudiantes **“la universidad siempre ha tratado de desmovilizar a los estudiantes a partir de esas amenazas,** nosotros creemos que es preocupante pero esperamos que sea tomado en cuenta el tema de las semanas que hemos perdido” afirmó Tavera.

Los estudiantes habían entrado en la dinámica de movilización, luego de conocer el reajuste de las matrículas escolares que entraba a regir para los nuevos estudiantes y que provocaba alzas excesivas en los costos, para estudiantes que pertenecen a los estratos 1,2 y 3 que no podrían acceder a la universidad.

<iframe id="audio_21246995" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21246995_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
