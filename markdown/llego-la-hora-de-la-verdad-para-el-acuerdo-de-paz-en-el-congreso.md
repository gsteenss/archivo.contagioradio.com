Title: Llegó la hora de la verdad para el acuerdo de paz en el Congreso
Date: 2017-05-24 15:31
Category: Nacional, Paz
Tags: acuerdo de paz, Congreso de la República, Fast Track
Slug: llego-la-hora-de-la-verdad-para-el-acuerdo-de-paz-en-el-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [24 May 2017] 

La Comisión del Congreso por la Paz, dio a conocer las dos propuestas que intentan acelerar la implementación de los Acuerdos de Paz, vía Fast Track. La primera consiste en **votar en bloque cada uno de los puntos,** a partir de las mayorías en el Congreso y la segunda pretende presentar proposiciones para votar negativamente las propuestas que violen el espíritu del acuerdo.

Para el senador Iván Cepeda, aunque estas iniciativas son un esfuerzo por “**subsanar los problemas que ha creado el fallo de la Corte Constitucional** en la implementación rápida, eficaz e integral de los acuerdos” la verdad se sabrá a la hora de proponer y votar en los debates y plenarias, será allí donde se evidencie la voluntad real de las bancadas que han afirmado respaldar el acuerdo. Le puede interesar: ["Corte Constitucional somete al proceso de paz a más Obstáculos"](https://archivo.contagioradio.com/corte-constitucional-somete-al-proceso-de-paz-a-mas-obstaculos/)

Además, estos debates se acercan cada vez más a las elecciones de 2018, sin embargo Cepeda manifestó que espera que los intereses que se generan en las campañas no estén por encima de la implementación de los acuerdos e hizo un llamado a la **responsabilidad en las acciones que se tomen,** porque las mismas **“serán vitales en garantizar la paz o llevar al proceso a una situación difícil**”.

Cepeda también señaló que se está generando una preocupación más para los Acuerdos, con la nueva conformación de la Corte Constitucional, con la llegada de Cristina Pardo y Carlos Bernal, “**ha comenzado a manifestarse con decisiones que pueden terminar haciéndole un grave daño a los contenidos del Acuerdo de Paz**”.

De otro lado afirmó que existen todas las posibilidades para que una alternativa distinta confronte a los sectores de poder tradicional del país, pero que ello depende del nivel de acción de las organizaciones sociales y de las plataformas que luchan por la paz. Le puede interesar: ["Estas son las alternativas del presidente Santos para salvar la Paz"](https://archivo.contagioradio.com/estas-son-las-alternativas-del-presidente-santos-para-salvar-el-proceso-de-paz/)

### **La movilización social una fuerza que salvaguarda la paz** 

El proceso de paz entre la guerrilla del as FARC-EP y el Gobierno Nacional ha tenido diferentes momentos de fuertes tensiones, no obstante, Jairo Estrada vocero de Voces de Paz, ha expresado que **este podría ser el más difícil**, razón por la cual han hecho un llamado a la ciudadanía para que retorne a las calles a defender la paz.

De acuerdo con el senador Iván Cepeda, “la movilización ciudadana no es una expresión demagógica, sino la **fuerza que se requiere hoy para poder hacer que el Estado Colombiano y sus instituciones respeten el Acuerdo de Paz**”. Le puede interesar: ["Con movilizaciones y pactos políticos haremos frente a la decisión de la Corte"](https://archivo.contagioradio.com/con-movilizaciones-y-pactos-politicos-haremos-frente-a-decision-de-la-corte-ivan-cepeda/)

<iframe id="audio_18883901" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18883901_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
