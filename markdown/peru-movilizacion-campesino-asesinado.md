Title: Movilizaciones en Perú por asesinato de campesino
Date: 2016-11-08 14:25
Category: El mundo, Otra Mirada
Tags: Mineria, Perú, violencia policial
Slug: peru-movilizacion-campesino-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/fotonoticia_20161108175315_640.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ruters 

##### 8 Nov 2016 

Organizaciones sociales, políticas y comunidades indígenas continuan en **movilización para reclamar al gobierno por el asesinato de un segundo campesino a manos de la policía** peruana, en medio de la violenta represión a la protesta de las comunidades afectadas en sus territorios por la extracción petrolera en la Sierra norte de ese país.

El más reciente asesinato fue reportado el día domingo **en la provincia de Pataz**, cuando un grupo de **uniformados custodiaban la minera Horizonte**, compañía que trabaja en la zona, contra la cual se levantaron los manifestantes, donde **resultaron además 21 personas heridas**.

Por el derecho a **evitar que sean contaminadas sus fuentes hídricas y se respete su territorio**, los nativos amazónicos habian iniciado una huelga, exigiendo una indemnización por los daños causados por el Oleoducto Nor Peruano, ahora los índigenas **esperan una delegación del gobierno dispuesta a llegar a algunos acuerdos**. Le puede interesar: [Se mantienen las fuertes protestas en Perú contra el proyecto "Tía María"](https://archivo.contagioradio.com/se-mantienen-las-fuertes-protestas-en-peru-contra-proyecto-minero-tia-maria/)

Entre tanto algunos parlamentarios han condenado los hechos, **acusando al gobierno por su incapacidad para resolver los conflictos de manera pacífica**, presentando adicionalmente proyectos para **acabar con la norma que legitima el accionar de los policias si se sienten amenazados por los manifestantes** y **anular los convenios por los cuales los oficiales públicos prestan seguridad a empresas mineras,** en proyectos como Yanacocha y Antanina entre otros.

### **Otro campesino asesinado** 

El homicidio del comunero se presenta tras el ocurrido en pasado mes de octubre, donde el campesino **Quitino Cereceda** **cayó en la región surandina de Apurímac**, tras recibir un impacto de bala en la cabeza, en medio de los enfrentamientos con la Policía por el control de una carretera en Cotabambas, durante un paro que acataban las comunidades contra la mina Las Bambas. Le puede interesar: [Denuncian que trabajadores de minera Yanacocha golpearon a Máxima Acuña](https://archivo.contagioradio.com/trabajadores-de-minera-yanacocha-golpearon-a-maxima-acuna-a-su-esposo/).
