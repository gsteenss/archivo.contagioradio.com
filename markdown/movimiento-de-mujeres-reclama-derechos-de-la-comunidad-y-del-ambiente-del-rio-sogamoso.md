Title: Mujeres reclaman derechos de la comunidad y del ambiente del Río Sogamoso
Date: 2015-03-17 21:35
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, cambio climatico, Derechos, Gobierno, hidrosogamoso, ISAGEN, mujeres, Santander
Slug: movimiento-de-mujeres-reclama-derechos-de-la-comunidad-y-del-ambiente-del-rio-sogamoso
Status: published

##### Foto: Vanguardia.com 

<iframe src="http://www.ivoox.com/player_ek_4227156_2_1.html?data=lZefmZaZeo6ZmKiakp2Jd6KnlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdbextfS1ZDRpdPXycbbjcrSb8XZx8rb1caPqMbgjLeSpZiJhaXjjLjcycbRs9TjjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Claudia Patricia Ortíz, Movimiento Prodefensa del río Sogamoso] 

Las mujeres del sector La Playa, municipio de Betulia, Santander, en compañía del Movimiento en defensa del Río Sogamoso y del Movimiento Ríos Vivos de Colombia, realizaron una marcha hacia Bucaramanga, en defensa de la comunidad y el ambiente que se han visto afectados por la represa Hidrosogamoso, construida por la empresa ISAGEN.

Se trata de la **Quinta Jornada en Defensa de los Territorios y la Vida,** donde las mujeres exigen a la gobernación de Santander y a ISAGEN, la reparación integral a las familias que piden la reubicación, debido a que **no quieren “seguir oliendo gas metano y ácido sulfhídrico”**, producidos por la contaminación en el río por construcción de la represa.

Además resaltan que las mujeres, los niños y niñas, son quienes más se han visto afectados por la contaminación del río Sogamoso, pues ahora es “**un río donde los niños ya no se pueden bañar por que se enferman, mucho menos pueden tomar de esa agua”,** afirma Claudia Patricia Ortiz, representante del Movimiento Prodefensa del río Sogamoso y del Movimiento Ríos Vivos de Colombia.

Ortíz, asegura que están reclamando la garantía de los derechos colectivos, “ya que hay total complicidad del gobierno, mas cuando **el 57.6% de ISAGEN pertenece a los colombianos”,** dice la representante del Movimiento Ríos Vivos, quien agrega que la comunidad está en desacuerdo con la venta de  la empresa.

“Nosotros no estamos de acuerdo en que ISAGEN y el gobierno local y departamental construyan  vías, viviendas y acueductos, esa no es la reparación que queremos, porque eso solo genera que aumenten los impuestos y los servicios públicos,  **en  cambio no hay trabajo, ni un ambiente sano para que las familias se desarrollen”.**

**La comunidad instauró una acción popular en 2014** con el fin de reclamar los daños causados al medio ambiente y los derechos de reparación colectiva, pues denuncian que las autoridades intentan ocultar las consecuencias sobre la pesca y los cultivos, con el fenómeno del cambio climático.
