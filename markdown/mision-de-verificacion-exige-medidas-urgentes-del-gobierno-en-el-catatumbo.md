Title: Misión de verificación exige medidas urgentes del gobierno en el Catatumbo
Date: 2018-05-22 13:18
Category: DDHH, Nacional
Tags: Catatumbo, ELN, estructuras paramilitares
Slug: mision-de-verificacion-exige-medidas-urgentes-del-gobierno-en-el-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/cisca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cisca] 

###### [22 May 2018] 

Finalizó la misión de verificación de la crisis humanitaria del Catatumbo, que deja como balance **la urgencia de un diálogo entre los grupos armados del territorio y el compromiso de los mismos, para no violar los derechos humanos de las comunidades** que allí se encuentran, y la necesidad de que el gobierno Nacional ponga en marcha políticas públicas que logren superar el olvido estatal hacia esta región.

De acuerdo con Ismael López, quien hizo parte de esta misión, “es innegable el abandono y la ineficiencia del Estado, se necesitan los servicios de salud, de educación, no hay servicios públicos en las veredas apartadas del territorio”. **Situación que estaría provocando que ingresen más personas a esos grupos armados, bajo falsas esperanzas de mejorar su situación social**.

Además, el aumento de los enfrentamientos entre estructuras armadas ha generado desplazamientos internos en el territorio hacia diferentes campamentos humanitarios. La misión logró conocer 7 de ellos, **2 ubicados en el municipio de Playa de Belén, 2 en el municipio de Acarí, en San Calixto hay 2, en el municipio del Tarra hay uno al igual que en el municipio de Tibú**.

Frente a la cantidad de personas que se encuentran allí refugiadas, López afirmó que oscilan entre las **250 a las 1.000 personas, dependiendo de la presencia de esos grupos en los municipios**. Asimismo, tanto la misión como las personas en los campamentos, le están exigiendo al gobierno que brinde las ayudas necesarias para quienes se encuentran en estos lugares y garantice el retorno de los mismos a sus hogares.

### **Los campamentos humanitarios están en difíciles condiciones de habitabilidad** 

Otra de las denuncias hechas por las comunidades que logró constata la misión, son las precarias condiciones en la que se encuentran los campamentos. De acuerdo con López, en algunos de estos lugares ya hay brotes de diarrea producto del agua que están consumiendo allí las personas. También se han denunciado afectaciones a la piel.

**“Es preocupante la situación a la que están expuestas en este momento esas comunidades a cualquier problema de salud o al fuego cruzado”** afirmó López y agregó que algunos de los caminos entre los campamentos hacia otros lugares del municipio habrían sido minados por esos mismos grupos armados. ([Le puede interesar: "Avanza misión humanitaria por la región del Catatumbo"](https://archivo.contagioradio.com/avanza-mision-humanitaria-por-la-region-del-catatumbo/))

### **El trabajo del movimiento social** 

Las diferentes organizaciones sociales que acompañaron esta misión, se comprometieron a generar una unión mucho más fuerte entre las plataformas y sectores que trabajan en el territorio, para establecer una hoja de ruta, que sirva de guía para tener una base concreta de la realidad de la región.

<iframe id="audio_26116979" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26116979_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
