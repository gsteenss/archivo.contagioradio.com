Title: Microeconomías colombianas las más afectadas por el aislamiento del Covid-19
Date: 2020-03-18 16:00
Author: CtgAdm
Category: Actualidad, Nacional
Tags: cuarentena, microeconomias
Slug: microeconomias-colombianas-las-mas-afectadas-por-la-cuarentena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/vendedores-ambulantes-e1454346093211.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En los diferentes pronunciamientos por parte del Estado y el Distrito han abordado los temas sociales y económicos, **pero han dejando en un segundo plano a las microeconomías compuestas casi en un 60%** por vendedores informales o ambulantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Quienes han optado en su mayoría por rechazar las medidas del Gobierno, y salir a las calles en busca del sustento diario, **Pedro Fonseca vocero de vendedores informales en Bogotá** señaló, "*los vendedores vivimos del diario y al no tener ventas o vernos obligados a quedarnos en la casa, nos tenemos más opción que pensar si ponemos primero la salud o el sustento de la familia".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó, *"la realidad es que en el sector informal a pesar de que somos la economía más grande del país no hemos tenido una respuesta real de los Gobiernos, una respuesta que vaya más allá de una lucha por el diario vivir"*; y afirmó que en temas de apoyo económico *"siempre los requisitos son absurdos"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Algunos programas nunca se ha llevado a cabo porque el vendedor simplemente no cumple con los requisitos bancarios*, **las cosas son muy bonitas en el papel pero cuando uno llega a la realidad se enfrenta con discriminación y olvido estatal**"
>
> <cite> **Pedro Fonseca | Vocero de vendedores informales** </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Argumento que respaldado por **Pedro Luis Ramírez Barbosa miembro de la Unión Nacional de Economía Informal**, señalando que en "*medio de la crisis supermercados, bancos y grandes empresas se ven beneficiadas mientas que las economías informales son terriblemente afectadas por la falta de personas en la calle y la desconfianza de salir por miedo al contagio"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y resaltó que el problema no es el aislamiento de la comunidad, sino las medidas del Gobierno que resultan ***"insuficientes para los más de 176.000 vendedores informales que tiene como único sustento las calles de Bogotá"*** (Le puede interesar: <https://archivo.contagioradio.com/urbana-amenaza-cachivacheros/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"El Gobierno Distrital ha presentando planes de atención económica para ellos pero, estos no tienen nada novedoso, lo cual tampoco significaría grandes cambios en el sustento familiar, al contrario la falta de venta y de clientes causaría un déficit económico en las micro economías de los vendedores informales"*, destacó Ramírez.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que antes de que se tomarán las medidas de restricción por el coronavirus estaban planteando una movilización, "los vendedores a pesar de la cuarentena buscará la forma de exigí al distrito garantías para su abastecimiento económico, más allá de las políticas de persecución policial, ello harán llegar a la alcaldía las solicitudes como mecanismo de protección a este tipo de economías". (Le puede interesar: <https://www.justiciaypazcolombia.com/fumigacion-terrestre-con-glifosato-en-zrcpa/> )

<!-- /wp:paragraph -->
