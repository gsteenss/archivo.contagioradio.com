Title: Reforma política está "en riesgo de muerte"
Date: 2017-11-08 11:38
Category: Nacional, Paz
Tags: acuerdos de paz, Reforma Política
Slug: reforma-politica-esta-en-riesgo-de-muerte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/ausentismo-camara.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [08 Nov 2017] 

Luego de la aprobación de la Reforma Política en la Cámara de Representantes, esta deberá continuar su trámite en la Comisión primera del senado y luego la plenaria final, de acuerdo con Alirio Uribe, representante por el Polo, **el temor que ahora se tiene sobre este proyecto de ley es que alcance a los tiempos del Fast Track al que solo le quedan 3** semanas.

El representante manifestó que se realizaron dos votaciones muy rápidas para aprobarla, sin embargo, frente a los temores por las posibilidades de acabar con el espíritu del proyecto de ley que buscaba dar paso a una apertura democrática, Uribe señaló que “**el gobierno no tuvo en cuenta las recomendaciones de la MOE**, que propuso cambios estructurales” y en esa vía podría estar en riesgo de muerte con los trámites que le faltan.

“No vemos la suficiente voluntad política del senado para mover los diferentes proyectos de implementación de los acuerdos de paz” afirmó el representante y agregó que han existido casos en donde se han aprobado más de 200 artículos en un solo día, o varias normas. (Le puede interesar: ["La paz territorial no se siente en los territorios: Misión Internacional"](https://archivo.contagioradio.com/la-paz-territorial-no-se-siente-en-los-territorios-mision-internacional/))

### **Las circunscripciones de paz siguen andando** 

Sobre las circunscripciones de paz, Uribe manifestó que está ya están en último debate y que podrían ser aprobadas hoy. Frente a los cambios que ha sufrido este articulado, el representante señaló que se ha podido acordar que estas curules son exclusivamente para víctimas del conflicto, **es decir que la condición para poder ocuparla será ser reconocido como víctima**.

De igual forma, los representantes se encuentran debatiendo si se vota o no en los cascos urbanos de ciertos municipios. Sin embargo, el representante expresó que continúa la incertidumbre sobre cómo blindar las circunscripciones de estructuras incrustadas **en el poder, que han manejado alianzas bajo intereses de corrupción y de grupos paramilitares**.

“Como son zonas inhóspitas donde no ha tenido mucha presencia el Estado, y ha existido el conflicto, hay un alto riesgo de que estas circunscripciones puedan terminar copadas por grupos armados ilegales o partidos tradicionales” afirmó Alirio Uribe. (Le puede interesar: ["Gobierno tiene opciones para implementar el acuerdo si el gobierno no lo hace: E. Santiago"](https://archivo.contagioradio.com/gobierno-tiene-opciones-para-implementar-el-acuerdo-si-el-congreso-no-lo-hace-e-santiago/))

<iframe id="audio_21963097" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21963097_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
