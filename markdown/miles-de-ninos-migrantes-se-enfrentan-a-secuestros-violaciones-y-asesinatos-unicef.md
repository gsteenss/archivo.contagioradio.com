Title: Miles de niños migrantes se enfrentan a secuestros, violaciones y asesinatos: UNICEF
Date: 2016-08-24 12:41
Category: DDHH, El mundo
Tags: Frontera con Estados Unidos, Infancia, migraciones, Unicef
Slug: miles-de-ninos-migrantes-se-enfrentan-a-secuestros-violaciones-y-asesinatos-unicef
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Niños-migrantes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Animal Político] 

###### [24 Ago 2016] 

De acuerdo con el más reciente informe publicado por el Fondo de las Naciones Unidas para la Infancia, los miles de niños, niñas y jóvenes que han migrado durante los últimos dos años desde Centroamérica hacia los Estados Unidos, han sido **víctimas de secuestros, violaciones y asesinatos**. El primer semestre de 2014, por lo menos 44.500 menores fueron detenidos en la frontera estadounidense, en 2015 fueron 18.500 y hasta junio de 2016 se registraron casi 26 mil.

El informe revela que el 40% de los menores que son detenidos sin compañía de un adulto carecen de un abogado en las audiencias de[ [inmigración de los Estados Unidos](https://archivo.contagioradio.com/organizacion-y-voto-las-claves-de-los-inmigrantes-en-eeuu-para-garantizar-sus-derechos/)], por lo que corren mayor riesgo de ser deportados, pues **no se les garantiza el derecho a contar con un abogado de oficio** y pueden pasar varios meses detenidos sin que les definan su situación, por lo que UNICEF asegura que es urgente impedir la detención de los menores sobre la base de su situación en materia de migración.

Durante los primeros seis meses de este año en la frontera con México fueron detenidos no menos de 26 mil menores de edad sin compañía de una persona adulta, y por lo menos 29.700 ciudadanos que viajaron en familia, en su mayoría mujeres con hijos pequeños; se conoce además que otros miles **no alcanzaron a llegar a la frontera porque fueron detenidos, secuestrados, sometidos a la trata o asesinados**.

La mayoría de los niños, niñas y jóvenes que migran sin sus familias, provienen de Honduras, Guatemala y Salvador, en dónde **los niveles de pobreza extrema cobijan a más del 50% de la población y las tasas de homicidio superan el promedio mundial**, por lo que como asegura el Director Ejecutivo Adjunto de UNICEF, el alto flujo de migración y las violaciones a los derechos humanos que se generan, deben llevar a que se atiendan las [[problemáticas sociales y económicas de estas naciones](https://archivo.contagioradio.com/solo-el-85-de-los-indigenas-latinoamericanos-accede-a-la-secundaria/)].

<iframe id="doc_56768" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322080381/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-MVZwAsFQ6eai9F78r1me&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
