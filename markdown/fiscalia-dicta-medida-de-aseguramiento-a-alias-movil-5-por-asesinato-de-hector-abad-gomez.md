Title: Fiscalía dicta medida de aseguramiento por asesinato de Héctor Abad Gómez
Date: 2015-11-19 17:35
Category: Judicial, Nacional
Tags: alias Móvil 5, está siendo procesado por su presunta participación en la masacre de Puerto Bello (Antioquia), Héctor Abad Gómez, Los Urabeños, Manuel Salvador Ospina Cifuentes alias ‘Móvil 5’, Paramilitarismo
Slug: fiscalia-dicta-medida-de-aseguramiento-a-alias-movil-5-por-asesinato-de-hector-abad-gomez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/hector-abad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [19 nov 2015] 

La Fiscalía General de la Nación profirió medida de aseguramiento en contra de Manuel Salvador Ospina Cifuentes, alias ‘Móvil 5’, hombre de confianza de los hermanos Carlos y Fidel Castaño, por el asesinato del médico y defensor de derechos humanos Héctor Abad Gómez, ocurrida el 6 de agosto de 1987.

A Ospina Cifuentes se le atribuyen los asesinatos del médico y profesor de la Escuela Nacional de Salud Pública y de Medicina de la Universidad de Antioquia, de su colega Leonardo Betancur y de Luis Felipe Vélez, presidente de la Asociación de Institutores de Antioquia, asesinado horas antes, crimenes declarados de lesa humanidad en 2014.

‘Móvil 5’, está siendo procesado por su presunta participación en la masacre de Puerto Bello (Antioquia), entre otras acciones criminales. También se maneja la hipótesis de que fue Ospina Cifuentes quien asesinó al jefe paramilitar Carlos Castaño.

Después de la disolución de las AUC, ‘Móvil 5’ se integró a la banda criminal de ‘El clan Úsuga’ o ‘Los Urabeños’, en donde continuó delinquiendo. De acuerdo al portal Verdad Abierta, su captura por parte de las autoridades en 2014, “ayudaría a esclarecer las muertes de los hermanos Castaño”.

Se espera, que con la medida de aseguramiento a Ospina Cifuentes, se esclarezcan los hechos alrededor de la muerte de Abad Gómez, ocurrida ya hace 28 años, descrita en la obra "El olvido que seremos" de su hijo Hector Abad Faciolince y el documental "Carta a una sombra" de su nieta Daniela Abad.
