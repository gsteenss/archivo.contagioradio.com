Title: Estudiantes denuncian corrupción en Universidad Surcolombiana
Date: 2017-02-22 16:27
Category: Educación, Nacional
Slug: estudiantes-denuncian-corrupcion-en-universidad-surcolombiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/surcolombiana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Polinizaciones] 

###### [22 Feb 2017] 

Estudiantes de la Universidad Surcolombiana, en Neiva - Huila, exigen investigaciones y resultados a las autoridades, frente a 4 casos de corrupción que hay en la institución, **dos de ellas envuelven  al rector de la Universidad Pedro Reyes y el ex secretario general Johan Steed Ortíz.**

Los dos funcionarios han sido señalados por su relación con **David Cangrejo, ex director de la entidad de salud Cármen Emilia Ospina, actualmente investigado por irregularidades registradas en la prestadora del servicio médico** que tendrían que ver con desvío de dineros públicos e influencia mal habida en la elección del rector de la Surcolombiana.

Las dudas de los estudiantes radican en dos situaciones concretas. La primera es una entrevista de David Cangrejo en la que afirma haber elegido al rector Pedro Reyes, **“allá está el rector Pedro Reyes, una persona elegida por el Cangrejismo”,** esta expresión seria el reconocimiento de un delito, ya que un servidor público no puede financiar campañas ni mucho menos influir en una elección al interior de una institución educativa pública.

La segunda situación tiene que ver con filtración de un audio en donde se escucha al entonces secretario general de la universidad Johan Steed Ortíz, **decir que transportaba dineros, que de acuerdo con los estudiantes “eran coimas”** de David Cangrejo, Ortíz renunció el pasado 11 de febrero por presión del Consejo Superior de la Universidad.

Por otro lado, los estudiantes también denuncian que trabajadores de la universidad, como **decanos de diferentes facultades, habrían influenciado la elección del representante de los egresados** ante el Consejo Superior. Le puede interesar: ["Reforma a la educación se pasaría vía Fast Track"](https://archivo.contagioradio.com/reforma-al-sistema-de-educacion-se-pasaria-via-fast-track/)

Finalmente la comunidad estudiantil alertó sobre **contratos que se firman con la Universidad y aparecen a nombre de terceros**, ejemplo de ello es el convenio que se hizo entre Zona Franca y la Universidad Surcolombiana en donde finalmente la empresa que firma la cotización es Mota y Medina. Le puede interesar: ["Yaneth Giha Tovar entra a la dirección del Ministerio de Educación"](https://archivo.contagioradio.com/yaneth-giha-tovar-entra-a-direccion-de-ministerio-de-educacion/)

Para la comunidad estudiantil, la corrupción y la falta de democracia causan problemáticas estructurales y coyunturales como que sean **los estudiantes los que ilegalmente deban pagar el internet, la falta de pupitres y el mal funcionamiento del comedor.**

###### Reciba toda la información de Contagio Radio en [[su correo]
