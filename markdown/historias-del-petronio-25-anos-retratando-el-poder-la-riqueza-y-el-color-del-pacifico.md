Title: Historias del Petronio: 25 años retratando el poder, la riqueza y el color del Pacífico
Date: 2019-08-15 17:26
Author: CtgAdm
Category: Cultura, Nacional
Tags: Fotografia, Música, Pacífico colombiano, Petronio Álvarez
Slug: historias-del-petronio-25-anos-retratando-el-poder-la-riqueza-y-el-color-del-pacifico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Jorge.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Petronio-Álvaro-Isarraga-2.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Petronio-Álvaro-Isarraga-3.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Petronio-Álvaro-Isarraga-4.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Petronio-Álvaro-Isarraga-6.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Petronio-Álvaro-Isarraga-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Idarraga-Petronio.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/IdarragaPetronio.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

En medio de las múltiples expresiones artísticas del **Festival de Música Petronio Martínez,** se encuentra Jorge Idarraga, un fotógrafo de Buenaventura quien viene ejerciendo su oficio desde hace más de 25 años, exaltando las bondades de una región rica en matices como lo es el Pacífico colombiano.

A través de su proyecto: Retratos del Petronio, Jorge Idarraga viene trabajando desde 2014 en cada una de las versiones del festival, dando una imagen a los músicos que hacen parte del festival y que vienen de los lugares más apartados y más lejanos del litoral, llegando a **capturar a través de su lente a más de 500 artistas.**

Desde sus inicios, expresa Idarraga, decidió no reproducir a través de su fotografía, los cliches que se tienen del Pacífico ni sus conflictos, "como yo vivo allá me doy cuenta que existen otras realidades y me propuse capturarlas" destacando de su cultura poderosa, fuerte, rica y con mucho color.

\

### "He puesto mi fotografía al servicio de la cultura del Petronio y el Pacífico" 

**"Queremos crear diálogos entre los diferentes eventos afro del país"**, afirma Idarraga quien ha llevado su proyecto fotográfico hasta el Eje Cafetero, al Festiva de Tambores en San Basilio de Palenque en Bolívar, o la Fiesta de  San Pacho en Quibdó, Choco. [Petronio Álvarez: La música como bien universal, la música de todas y todos](https://archivo.contagioradio.com/petronio-alvarez-la-musica-como-bien-universal-la-musica-de-todas-y-todos/)

El fotógrafo afirma que el retrato es una de las modalidades más complejas de la fotografía por lo que cada uno trata de crear una situación de confianza que les permita a sus modelos expresarse naturalmente y "capturar con naturalidad esa expresión además de entablar un diálogo amistad".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
