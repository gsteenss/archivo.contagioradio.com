Title: Comunidades del Cauca se sienten en riesgo ante la escalada de violencia
Date: 2020-04-27 15:02
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Cauca, CNA, ejercito
Slug: en-cauca-toda-la-comunidad-se-siente-en-riesgo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/cauca-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El 26 de abril se registro un ataque armado en [Buenos Aire](https://archivo.contagioradio.com/desplazamientos-reten-ilegal-argelia-cauca/)s, Cauca, que dejó **tres personas muertas y 4 heridas**; el hecho se sucedió en el parque de Munchique, corregimiento de Honduras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las autoridades del pueblo negro del Norte del Cauca, Guardia Cimarrona; la acción armada se evidenció sobre las 9 p.m cuando *"un carro irrumpió en la comunidad de Munchique, territorio ancestral del consejo comunitario Cerró Teta, municipio de Buenos Aires cauca".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las agresión fueron hacia algunas personas que se encontraban reunidas en un establecimiento público, *"****sujetos, fuertemente armados llegaron disparando indiscriminadamente contra la multitud, arrojando una granada*** *(...) **al parecer esta acción es en retaliación al no cumplimiento con las medidas de aislamiento preventivo por la pandemia del covid-19**".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Días previos a este hecho la comunidad había denunciado varios panfletos, audios y mensajes en redes sociales, que advertían sobre medidas de contención por parte de disidencias de las FARC para aquellas personas que incumplieran la cuarentena.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Lamentamos que actores armados seguen la vida de nuestros hermanos y reclamamos acciones preventivas de las autoridades para evitar estos hechos"*
>
> <cite>Guardia Cimarrona </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Según líderes del territorio este hecho en especial, es responsabilidad del frente Jaime Martínez, integrado por [disidencias de las Farc](https://archivo.contagioradio.com/tres-diferencias-entre-disidencias-de-farc-recorriendo-las-guerras-en-colombia/); las víctimas fueron **Weimar Arará**, **Armando Montaño, y Humberto Solís.** 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte los **heridos** fueron, **John Alexander Solís Ambuila** (31 años); **Dayana Carabalí González** (24 años); Carlos Mario Arará Vásquez (28 años) y **John Janer Trujillo Montaño (**25 años); de los cuales hasta el momento se desconoce su estado de salud y la gravedad de las heridas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Desplazamientos, militarización, paramilitarización, erradicación y más afectan al Cauca

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CapazMauricio1/status/1243909084157554688","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CapazMauricio1/status/1243909084157554688

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En diálogo con Contagio Radio, **Ernesto Roa, Presidente del Coordinador Nacional Agrario** (CNA), señaló *"antes era un poco más fácil determinar la población en riesgo de desplazamiento, **hoy es casi imposible definir un número, porque en el Cauca todos tienen miedo, se sienten en riesgo y buscan la forma de salvar su vida, así esto signifique dejar todo de lado**"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para el presidente del CNA, a pesar de la coyuntura mundial por la pandemia, los territorios como el [Cauca](https://www.justiciaypazcolombia.com/en-condiciones-de-cuarentena-obligatoria-para-todos-los-colombianos-se-recrudece-el-conflicto-armado-en-varias-regiones-del-cauca/), se siguen enfrentando a **la peor pandemia y es *"el genocidio, el exterminio y el uso de la violencia en contra del movimiento social".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo que pasa en Buenos Aires se suma a una serie de asesinatos que se han venido presentando en los territorios donde hay cultivos de uso ilícito, minería ilegal, megaproyectos; esto a pesar de ser zonas altamente militarizadas, *"por ello responsabilizamos por acción u omisión al Estado frente a todos los hechos que han venido ocurriendo".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo enfatizo, que los cerca de 10 asesinatos en menos de mes y medio, son responsabilidad de las disidencias de las FARC, quienes tienen una estrecha relación con el narcotráfico y por lo tanto una persecución por quienes incentivan procesos de sustitución.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Nosotros creemos que h**ay una complicidad entre las disidencias y la fuerza pública, que han declarado la guerra a estos procesos que han cometido el único delito de defender los territorios** y estar en contra de la explotación de la naturaleza".*
>
> <cite> **Ernesto Roa | Presidente del Coordinador Nacional Agrario** (CNA) </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por otro lado Roa afirmó que hay un sin número de familias que han salido desplazadas, y otras que lo han intentado, ***"muchos no han podido salir pero tienen la restricción de hacerlo bajo amenazas de muerte".*** A esto se suma la militarización de los territorios, la persecución y la erradicación por orden del Gobierno.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Esperanza, lucha y resistencia campesina

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por último el Presidente de la CNA, agregó que como organización campesina su apuesta siempre ha sido soberanía e independencia alimentaria en unión a la defensa y el cuidado del territorio, *"el no compartir con multinacionales la explotación de los bienes de la naturaleza y el narcotráfico, nos pone en el ojo del huracán por eso siempre hemos sido una piedra en el zapato para los grupos armadas".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que como organización **hoy el mensaje es de esperanza, lucha y resistencia,** *"el campesino juega un papel estratégico, demostrando que en medio una crisis producimos no solamente alimento, sino conocimiento"*. }

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Hoy estamos llamados a seguir defendiendo el territorio, seguir defendiendo la vida y seguir produciendo y recuperando nuestras tradiciones y costumbres como campesinos y campesinas"*
>
> <cite> **Ernesto Roa | Presidente del Coordinador Nacional Agrario** (CNA) </cite>

<!-- /wp:quote -->
