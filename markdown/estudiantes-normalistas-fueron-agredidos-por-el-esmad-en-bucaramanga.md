Title: Pretenden judicializar a estudiantes de Bucaramanga por defender el bosque del colegio
Date: 2018-02-13 10:45
Category: DDHH, Nacional
Tags: Bucaramanga, Escuela Normal de Bucaramanga, ESMAD
Slug: estudiantes-normalistas-fueron-agredidos-por-el-esmad-en-bucaramanga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Normal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Olga Materón] 

###### [13 Feb 2018] 

Estudiantes de la Normal Superior de Bucaramanga, que defendían el bosque de 200 árboles al interior de la institución educativa ante la posible construcción de una rotonda denunciaron miembros de la Policía Nacional y el ESMAD los golpearon y **retuvieron a 3 estudiantes de la Universidad Industrial del Santander y a una madre de familia**.

De acuerdo con la denuncia del Equipo Jurídico Pueblos, los hechos se presentaron el pasado 12 de febrero, cuando estudiantes y padres de familias realizaban un “muro de resistencia” para evitar que talaran la zona verde del colegio, en donde se construirá el intercambiador vial de la carrera 27 con Quebradaseca. Este espacio de **bosque que tiene 4.000 metros, es considerado como uno de los pulmones de Bucaramanga**.

<iframe src="https://co.ivoox.com/es/player_ek_23735652_2_1.html?data=k5iklZqaeZOhhpywj5WbaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5yncbPjw87b1dTSb6Xpwtfhx4qWh4zkzcbhw8vTts7VjM_i2MrSrc2fxcqYpNrHpdPVzsbbycaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Robinson Duarte, representante de la plataforma juvenil de Bucaramanga, manifestó que tanto el ESMAD como la Policía Nacional agredieron a menores de edad y hubo un uso excesivo de la violencia hacia las personas que se encontraban allí. Además, **afirmó que las personas capturadas no estaban realizaron actos de violencia, sino que estaban grabando las violaciones a derechos humanos**.

### **El operativo del ESMAD y La Policía Nacional** 

Los alumnos denunciaron que desde las 10 de la mañana el Escuadrón Móvil Antidisturbios de la Policía Nacional y de la MEBUC, hicieron presencia en las instalaciones de la Escuela Normal Superior de Bucaramanga; bajo el mando del Teniente Coronel César Buitrago, comandante del Distrito de Policía de Bucaramanga y del Mayor Luis Fonseca. (Le puede interesar: ["Imputado Coronel de la Policía por asesinato de Nicolar Neira")](https://archivo.contagioradio.com/imputado-coronel-de-la-policia-por-asesinato-de-nicolas-neira/)

En estos hechos fue detenida **Lida Magali Olmos**, de 35 años de edad, madre de tres menores estudiantes de la Normal, **Jeisson Blanco Moreno**, estudiante de la Universidad Industrial de Santander (UIS), de 20 años de edad, de cuarto semestre de Derecho, **Nelly Nuñez**, de 21 años de edad y quien cursa sexto semestre de Derecho en la UIS y **Dayana Gonzáles**, de 18 años y estudiante de Filosofía, también de la UIS.

De igual forma, los estudiantes señalaron que hacia las 4:15 pm fueron expulsados de la Institución tres policías de civil infiltrados en la jornada de protesta.  Posteriormente la altura de las 5:00 pm estudiantes de grado cero fueron violentados por un policía que intentaba agredir a un estudiante de grado superior, poniendo en riesgo la integridad de los menores. Paralelamente, en el exterior del Colegio, un agente del ESMAD agredió a un estudiante de la UIS cuando le lanzó un gas lacrimógeno en el rostro.

### **La judicialización de las personas capturadas** 

Los estudiantes y madre de familia detenidos, fueron puestos a disposición de una Fiscalía de Bucaramanga y se espera que hoy sean presentados ante un juez de control de garantías, para efectos de legalización de su captura, imputación de cargos y probablemente imposición de medida de aseguramiento **por el punible de Violencia contra servidor público**.

<iframe src="https://co.ivoox.com/es/player_ek_23736117_2_1.html?data=k5iklZuVdZihhpywj5WZaZS1kZuah5yncZOhhpywj5WRaZi3jpWah5ynca3Z0NPO1MnTb6vVytLS1YqWh4zVw9TUw8nTb8XZx8rb1dTWcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El abogado Leonardo Jaimes, del Equipo Jurídico Pueblos señaló que este es u n acto de judialización de la protesta pública “es absurdo, lo que se planeta es que la **Policía en estos casos actúa de manera desproporcionada**, además precisando que el operativo que hicieron era ilegal, debido a que la licencia para realizarlo estaba vencída”. (Le puede interesar:["ESMAD desaloja a campesinos de Sabanalarga en Atioquia"](https://archivo.contagioradio.com/esmad-desaloja-a-campesinos-de-sabanalarga-en-antioquia/))
