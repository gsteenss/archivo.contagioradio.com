Title: Ciudad Bolivar, la localidad de Bogotá con mayor número de 'exterminio social'
Date: 2016-04-20 16:31
Category: DDHH, Nacional
Tags: ciudades colombia, Limpieza social, Paramilitarismo
Slug: limpieza-social-una-muestra-de-la-degradacion-de-la-guerra-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/limpieza-social.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ojo al Sancocho] 

###### [20 Abril 2016 ]

Recientemente en distintas regiones del país [[han vuelto a circular panfletos](https://archivo.contagioradio.com/8-asesinatos-en-menos-de-40-horas-en-putumayo/)] en los que con nombres propios y adjetivos denigrantes, [[Águilas Negras](https://archivo.contagioradio.com/?s=aguilas+negras+)] y [[Autodefensas Gaitanistas](https://archivo.contagioradio.com/?s=autodefensas+gaitanistas)], amenazan de muerte a hombres y mujeres, anunciando la llegada de la mal llamada 'limpieza social'; fenómeno que se expandido en 356 municipios de Colombia desde los años 60, gracias al **profundo silencio estatal, a la legitimidad social y a la falta de tipificación jurídica**, según refiere Carlos Mario Perea, historiador integrante del IEPRI.

Perea asegura que no es acertado llamar 'limpieza social' al **"fenómeno de homicidios sistemáticos contra identidades socialmente estigmatizadas"** que se ha afincado en las principales ciudades de Colombia, entre ellas Bogotá, Cali, Pereira y Barranquilla, como un **"ejercicio de poder para regular la convivencia a través de la muerte"**, porque termina legitimándose; para el historiador y diversas organizaciones de derechos humanos, se trata de "exterminio o aniquilamiento social".

Pese a que en el marco de esta "práctica horripilante, que quiebra los fundamentos del Estado social de derecho", han sido asesinadas por lo menos 5000 personas desde los años 80, y según afirma Perea, **el único debate que ha habido en el Congreso se dio en 1987**, a mediados de los 90 se publicaron dos libros sobre el tema y desde entonces, tanto el Estado como la academia no han vuelto a hablar del asunto que se ha convertido en un **"fenómeno de la vida cotidiana de muchos barrios populares"**.

El mayor número de casos de 'exterminio social' en Bogotá, se han presentado en Ciudad Bolívar, localidad que fue analizada por el historiador, para la concreción del [[más reciente informe del CNMH](https://archivo.contagioradio.com/la-limpieza-social-una-violencia-mal-nombrada-informe-del-cnmh/)],en el que se establece que los grupos de exterminio han cometido el 77% de los asesinatos, los paramilitares el 18% y la guerrilla el 3%; contando con el apoyo y tolerancia de juntas de acción comunales, comerciantes y miembros de la Policía.

La mal llamada 'limpieza social' **"no es una expresión más del conflicto armado"**, afirma Perea e insiste en que hay que comprenderla en la especificidad que demarca la urbanidad, teniendo en cuenta que el impacto de la confrontación armada "ha impedido que se vean otros escenarios y otras violencias" como las que suceden en el día a día de las ciudades y que demuestran la "degradación de la guerra en Colombia".

La pregunta entonces es **qué va a suceder con la violencia urbana en un eventual escenario de posconflicto**, expresa el historiador, porque sí de un lado en los barrios hay un sector que legítima el exterminio social, del otro hay organizaciones y personas que trabajan para que cese la 'limpieza social' y no siga operando en "total impunidad".

El historiador concluye asegurando que este fenómeno debe llegar a ser un problema de debate público y preocupación estatal, pues **tanto la legitimidad como el silencio han permitido su consolidación y propagación**, por lo que extiende el llamado para que se cree una política pública que permita enfrentarlo, pues sólo la sanción pública, estatal y judicial llevará a que la gente comprenda que esta práctica horripilante no puede seguirse cometiendo.

<iframe src="http://co.ivoox.com/es/player_ej_11242822_2_1.html?data=kpaflpecdpOhhpywj5eXaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZCxpdPd0JC9x9fJpY6fqqq9tK6RaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
