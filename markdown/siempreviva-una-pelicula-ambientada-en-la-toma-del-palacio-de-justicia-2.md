Title: "Siempreviva" una película ambientada en la toma del Palacio de Justicia
Date: 2015-05-11 07:36
Author: AdminContagio
Category: 24 Cuadros
Tags: Adaptación al cine, Ana Piñeres, Andrea Gómez, Andrés Parra, Bogotá, Clara María Ochoa, CMO Producciones, crowdfunding siempreviva, Enrique Carriazo, holocausto del Palacio de Justicia, Klych Lopez, La Chévre, M19, Manuel Arias, Miguel Torres, Siempreviva obra de teatro, Siempreviva película, toma del palacio de Justica
Slug: siempreviva-una-pelicula-ambientada-en-la-toma-del-palacio-de-justicia-2
Status: published

[***Klych Lopez** dirige “**Siempreviva**” adaptación cinematográfica de la obra teatral de **Miguel Torres**.*]

El dramaturgo Miguel Torres, escribió la pieza teatral costumbrista ambientada en los días de la toma del palacio de Justica por parte del M19 y la posterior retoma a sangre y fuego del ejército colombiano en 1985. Una puesta en escena que cautivo al público en cada una de sus funciones desde los años 90 hasta nuestros días.

Desde entonces en Clara María Ochoa, co productora de la cinta, nació un interés por llevar una versión cinematográfica desde las tablas a la gran pantalla. Deseo que se convertiría en realidad en el año 2014 cuando finalmente encontró eco en su socia y también productora de la cinta Ana Piñeres, y junto al guionista Manuel Arias iniciaron el proceso de adaptación, con el visto bueno del autor.

Un viejo conocido de CMO, casa productora de la que Piñeres es vicepresidenta, es el encargado de llevar “La Siempreviva” a la pantalla. Se trata del director colombiano Klych López, para quién será además su ópera prima (en lo que a largometrajes de ficción se refiere), luego del reconocimiento alcanzado por su trabajo en éxitos televisivos como “Correo de Inocentes” y “La promesa”, las dos nacidas en la misma empresa.

Más allá de ser un largometraje que conmemora 30 años de lo ocurrido en el holocausto del Palacio de Justicia, "Siempreviva" busca traer a la memoria lo ocurrido entre el 6 y 7 de noviembre de 1985 en pleno corazón de Bogotá y un homenaje a los caídos ; la película busca hacer un llamado al mundo sobre actos como estos en los que el limitado avance en las investigaciones favorece a la impunidad y a la violación de derechos para los inmolados, los desaparecidos y sus familias.

De acuerdo con Ana Piñeres, en entrevista con Contagio Radio "Es importante que las nuevas generaciones sepan que eso sucedió en Colombia precisamente para no repetir la historia hay que recordarla" por lo que esta película en sus palabras "Es un documento a la memoria histórica, un documento de patrimonio audiovisual ".

Trasladar las escenas, los personajes y los parlamentos del teatro al lenguaje cinematográfico supuso un reto para el equipo de producción. Sumado a los elementos aportados por datos de investigación que, con el paso del tiempo, se han conocido desde la época en que fué escrita hasta nuestros días "el texto de Miguel torres estaba intacto desde que lo hizo en los años 90 y nosotros quisimos impregnarle un poco de todas estas nuevas investigaciones alimentandolo tambien en los ensayos con los actores y creemos que la adaptación es una bocanada de aire y de mucha cinematografía"

<iframe src="https://player.vimeo.com/video/109371568" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Rodada en la candelaria, en un casona cercana al Palacio de Justicia, "Siempreviva" se encuentra actualmente en su etapa de postproducción, para la cual los realizadores ha optado por la estrategia del crowdfunding para su financiación, deacuerdo con Piñeres se debe a que "las empresas a veces no quieren relacionar sus marcas con películas como alma y arriesgadas que le quieren decir al país hagamos un pare por que la paz tambien se construye desde la verdad, desde contarnos la historia y dejarnos de decir mentiras".

La empresa "[La chévre](http://bit.ly/1EiN2yQ)" en su plataforma web acogió el proyecto donde las empresas, instituciones y particulares pueden realizar su donación y apoyar "Siempreviva" en su última etapa, donde también pueden encontrarse los incentivos de acuerdo al monto aportado.

\[caption id="attachment\_8346" align="aligncenter" width="1024"\]![Siempreviva1\_zps4ewenptf](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Siempreviva1_zps4ewenptf.jpg){.wp-image-8346 .size-full width="1024" height="684"} Elenco y equipo humano de "Siempreviva" encabezado por Enrique Carriazo, Andrés Parra y Andrea Gómez.\[/caption\]

**SINOPSIS**

La economía no va bien y en unos meses Lucía perderá la casa hipotecada donde vive con cuatro inquilinos y sus dos hijos. Julieta, la hija menor, es la salvación, está terminando sus estudios para ser abogada, pero la mañana del 6 de noviembre de 1985 Julieta sale a su trabajo en el Palacio de Justicia y nunca regresa. Lucía inicia una búsqueda infructuosa por saber el paradero de su hija, pues hay testigos que dicen que la vieron salir con vida después de que el Palacio se consumió en llamas por una toma guerrillera. Basada en hechos reales ocurridos en Colombia en 1985.
