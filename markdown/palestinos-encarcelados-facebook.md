Title: Palestinos son encarcelados por dar likes en facebook
Date: 2017-11-30 15:28
Category: Onda Palestina
Tags: Acuerdo entre Vaticano y Palestina reconoce dos estados, Apartheid, Apartheid Israel
Slug: palestinos-encarcelados-facebook
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/bds1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: theintercept.com 

##### 29 Nov 2017 

El comité palestino de prisioneros y ex prisioneros denunció que desde el año 2015 hasta la actualidad las fuerzas de ocupación israelíes han arrestado a alrededor de 280 personas, por haber dado “likes”, realizado comentarios, publicaciones o compartir contenido “pro-palestinos” en la red social Facebook.

Entre las personas arrestadas se cuentan hombres, mujeres y hasta niños que venían siendo monitoreados permanentemente por una unidad especial de las fuerzas de seguridad israelíes que realizan barridos al uso de las redes sociales. En total en las cárceles , 10 son niñas, 300 niños, 11 senadores y alrededor de 450 palestinos están en prisión bajo la figura de detención administrativa, es decir sin que se les haya realizado un juicio.

En esta emisión de Onda Palestina hablaremos de las elecciones en que participarán Hammas y Fatah el próximo año, sobre la selección palestina de fútbol que supera por más de 20 años a la selección israelí en las clasificaciones oficiales de la FIFA, la entrevista a Francisco Javier Fernández, integrante de Palestina Animal League en defensa de los animales, más de la campaña contra el Giro d’ Italia en Israel, y nuestra sección especial de cultura todos los finales de mes: Poesía en Onda Palestina.

<iframe id="audio_22382500" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22382500_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
