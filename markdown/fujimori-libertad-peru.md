Title: Partido "Fuerza popular" buscaría cambiar Magistrados para liberar a Fujimori
Date: 2017-05-26 15:50
Category: El mundo, Otra Mirada
Tags: Fujimori, habeas corpus, Perú
Slug: fujimori-libertad-peru
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/292715.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Archivo 

###### 26 May 2017 

La mayoría en el Congreso peruano, integrada por senadores del fujimorista partido Fuerza Popular, **buscaría cambiar a cuatro magistrados de la Corte Constitucional** aprovechando la denuncia que involucra a los jueces con el caso conocido como **"El frontón"**, y así alterar la composición del Tribunal Constitucional (TC), en poder del cuál esta el destino del condenado ex presidente Alberto Fujimori.

Por una denuncia presentada el 25 de abril ante el congreso de la República, los magistrados **Manuel Miranda Canales, Marianella Ledesma Narváez, Carlos Ramos Núñez y Eloy Espinosa-Saldaña**, han recibido "Acusación constitucional por infracción a la Constitución y delito de función, prevaricato". De prosperar el recurso, **los jueces serían destituidos por el congreso**.

"Fuerza Popular" estaría en capacidad de **promover que se nombren los reemplazos de los magistrados**. Se necesitan, mayoría simple, es decir, 87 votos para nombrar a un juez del Tribunal y **Fuerza Popular tiene 72 legisladores**. Si se nombraran a cinco magistrados, toda esta situación cambia porque el número de votos sería menor y el fujimorismo podría poner dos fichas más en el tribunal.

Actualmente, el fujimorismo cuenta con uno o dos magistrados afines en el Tribunal Constitucional. Con reservarse tres de las cinco plazas en disputa, **lograría la mayoría en la máxima instancia constitucional**, lo que permitiría, con una votación simple declarar fundado un hábeas corpus que **anule la condena a Alberto Fujimori y ordene dejarlo en libertad**.

La presente situación tiene como antecedente lo ocurrido en mayo de 1997, cuando los magistrados Delia Revoredo de Mur, Manuel Aguirre Roca y Guillermo Rey Terry **fueron destituidos por no avalar la candidatura de Fujimori** a una segunda reelección consecutiva, apoyados en su carácter inconstitucional. Aprobación que finalmente recibiría de quienes los reemplazaron en el Máximo Tribunal.

El objetivo ahora, sería **tener en el Tribunal Constitucional jueces *ad hoc*** que voten por la inmediata libertad de Fujimori, sin importar los argumentos legales para anular la condena por corrupción y crímenes de lesa humanidad. Le puede interesar: [Latinoamérica se une para enfrentar impactos socioambientales de inversiones chinas](https://archivo.contagioradio.com/latinoamerica_inversiones_chinas/).
