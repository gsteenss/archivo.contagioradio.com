Title: El  fundado temor que suscita Uribe
Date: 2018-02-08 09:35
Category: Abilio, Opinion
Tags: Agroingreso seguro, Alvaro Uribe, Claudia Morales, Las Convivir, Mario Uribe, Mauricio Santoyo
Slug: el-fundado-temor-que-suscita-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/ALVARO-URIBE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Neil Palmer 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 8  feb 2018 

[No se trata de especular  aquí sobre si “EL” violador de la periodista Claudia Morales es o no el senador y ex presidente  Alvaro Uribe. Lo que sí se destaca es que las pistas que la periodista da de  su agresor,  coinciden con su  perfil: fue jefe suyo,  “un hombre relevante de la vida  nacional.  Ahora lo sigue siendo y, además,  hay otras evidencias que amplían su margen de peligrosidad”.  Quien la violentó, tenía el poder de afectar laboralmente a su padre y truncar su carrera. ]

[Se trata de alguien innombrable, blindado por su poder, astuto, capaz de destruir, de remover  cargos y  obstruir carreras, con evidencias de su peligrosidad, pero que además lo ampara  la impunidad, por lo que, con razón, decide ella no denunciar.]

[Esos mismos rasgos, pero con nombre propio, preocupan a colegas y amigos del  joven abogado, candidato a la Cámara por Bogotá,  Manuel Garzón, con quien se empezó a meter recientemente el senador y ex presidente  Álvaro Uribe.  Desde  su cuenta  de]*[twitter]*[ lo señaló de  “terrorista”, por hacer parte de la  lista  del partido  FARC, en ejercicio de su derecho a la participación política.]

[Con esta acusación el ex presidente  elevaba otra cortina de humo ante el inicio de la fase de juicio del proceso que se sigue en Medellín contra su hermano Santiago Uribe, tras haber sido acusado por la fiscalía, del crimen de  Camilo Barrientos en Yarumal, Antioquia, siendo  miembro del grupo paramilitar conocido como  “Los 12 Apóstoles”.  Manuel  fue uno de los abogados de las víctimas.]

[La participación del hermano del ex presidente, se suma a una larga lista de hechos que coinciden con “los antecedentes de peligrosidad” de los que habla la periodista Claudia Morales, y que hacen que el señalamiento al abogado Manuel Garzón, revista la mayor gravedad.]

[Recientemente el tribunal Superior de Medellín, compulsó copias a la fiscalía para que investigue al ex presidente por su posible responsabilidad en las masacres de El Aro, La Granja y en la ambientación  del crimen del  defensor de derechos humanos José María del Valle, mientras se desempeñaba como gobernador de Antioquia, al hallar “[suficientes elementos de juicio](http://www.eltiempo.com/justicia/investigacion/piden-investigar-a-alvaro-uribe-por-masacres-del-aro-y-la-granja-179464)”. ]

[Ya antes,  la sala de “Justicia y Paz” del mismo tribunal, había solicitado investigarlo tanto por hechos sucedidos cuando era gobernador de Antioquia como cuando era el presidente de Colombia.  [El portal Verdad Abierta](http://www.verdadabierta.com/component/content/article/83-juicios/4873-sala-de-justicia-y-paz-pide-investigar-a-alvaro-uribe-por-paramilitarismo), en su momento, hizo un pormenorizado análisis de la decisión de la Sala de Justicia y Paz del tribunal .  ]

[La creación de convivir como “germen del paramilitarismo”;  la base paramilitar en la finca “Las Guacharacas” propiedad de los Uribe Vélez;  la “pacificación de Urabá” en coordinación con “el general Rito Alejo del Río y los paramilitares de la región”;  el primer proyecto de ley de Alternatividad Penal  a paramilitares  "que aseguraba prácticamente su impunidad a pesar de las graves violaciones a los derechos humanos y al derecho internacional humanitario”.]

[También el caso del general Mauricio Santoyo, condenado por la justicia de Estados Unidos, que según la Sala, “estuvo a su servicio como Gobernador de Antioquia y luego como oficial de seguridad en la Presidencia de la República. Sus vínculos con los paramilitares están ya establecidos y no pudo ser su asesor de seguridad sin su asentimiento”.  Según la decisión, Uribe Vélez estaba también detrás de  los condenados directores de Das Jorge Noguera y María del Pilar Hurtado “investigados por sus vínculos con los paramilitares y otros crímenes”.]

[Otros hechos de conocimiento público y que implicaron condenas para sus subalternos inmediatos ocurrieron en su mandato: los falsos positivos, por lo que están en la mira de la [Corte Penal Internacional militares de alto rango](https://colombia2020.elespectador.com/jep/29-generales-y-coroneles-en-la-mira-de-la-cpi-por-ejecuciones-extrajudiciales);  la compra de votos para su  reelección, conocido como “[Yidis Política](https://www.elespectador.com/noticias/judicial/condenados-yidispolitica-articulo-555143)” por la que pagan prisión dos ex ministros y su secretario privado de presidencia; el otorgamiento ilegal de recursos del programa “[Agroingreso seguro](http://www.eltiempo.com/archivo/documento/CMS-14260975)” por el que está condenado su ministro de agricultura [los vínculos con paramilitares de su primo y mentor político Mario Uribe](http://www.eltiempo.com/archivo/documento/CMS-8936007);  el proceso de “desmovilización paramilitar” por el que tiene [medida de aseguramiento su Alto Comisionado de Paz](https://www.elespectador.com/noticias/judicial/corte-constitucional-dejo-firme-medida-de-aseguramiento-articulo-586842).]

[Citando a Fidel Cano, el magistrado Pinilla en la sentencia aludida de la Sala de Justicia y Paz afirmó que “No es posible estar dentro de una piscina y no mojarse”. Lo cierto es que el ex presidente ha sabido secarse y salir airoso de cada denuncia y de cada escándalo.]

[Sin duda, alguien como “Él” con un “margen de peligrosidad” tiene toda la capacidad de destruir cualquier proyecto de vida y salir incólume, una de las fundadas razones por las que la periodista Claudia Morales ejerza  su derecho a aguardar silencio.]

[Así mismo, alguien como Uribe por las graves implicaciones en delitos, como de las que hablan las decisiones del Tribunal de Antioquia hacen que los señalamientos al abogado y candidato Manuel Garzón deban ser tomados con el mayor cuidado.  El abogado, por su parte, decidió responder al ex presidente diciendo  que “no caeremos en sus provocaciones y su odio” y denunció ante las instancias judiciales  el peligroso e injurioso señalamiento.]

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio
