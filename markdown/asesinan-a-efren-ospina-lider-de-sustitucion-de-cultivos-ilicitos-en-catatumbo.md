Title: Asesinado Efrén Ospina, lider de sustitución de cultivos de uso ilícito en Catatumbo
Date: 2020-02-10 09:33
Author: CtgAdm
Category: DDHH, Nacional
Slug: asesinan-a-efren-ospina-lider-de-sustitucion-de-cultivos-ilicitos-en-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/Faro_del_Catatumbo-e1526587021569.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado sábado se conoció el asesinato de Efrén Ospina  
Velázquez, en el municipio de Tibú, en el departamento de Norte de Santander.  
Efren era uno de los más reconocidos líderes comunitarios por impulsar el  
programa de sustitución de cultivos ilícitos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la información previa hombres armados que se movilizaban en dos motocicletas llegaron hasta la finca de habitación de Efrén Ospina y en donde fue perpetrado el asesinato. Hasta el momento se desconocía de amenazas en su contra y no hay mayores avances en la investigación por parte de la policía o la fiscalía.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Norte de Santander y el Catatumbo siguen siendo de las zonas con mayor número de asesinatos de líderes

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 10 de enero, también en Tibú, fue asesinado el líder Tulio César Sandoval, integrante de la Coordinadora de Cultivadores de Coca, Marihuana y Amapola (Coccam) de Tibú y del movimiento Marcha Patriótica, además, el pasado 20 de enero fue asesinado en el municipio de Convención Fernando Quintero Mena, exconcejal y presidente de la junta de acción comunal de la vereda Guasiles. Lea también [(55 hechos de violencia contra líderes sociales)](https://archivo.contagioradio.com/en-enero-se-registraron-55-hechos-de-violencia-politica-contra-lideres-politicos-sociales-y-comunales/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Norte de Santander, junto con Bolívar y Cauca siguen siendo de las zonas más afectadas por el asesinato de líderes sociales sin que hasta el momento se reporten avances sustanciales en las investigaciones para frenar esta situación que enluta al país día a día, pues hasta la fecha han sido asesinados cerca de 35 líderes y liderezas sociales en lo que va corrido del 2020. [Le puede interesar: Proponen ruta para protección de líderes sociales en el Catatumbo](https://twitter.com/AscamcatOficia/status/1225636713965342721)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
