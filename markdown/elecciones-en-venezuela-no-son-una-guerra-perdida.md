Title: Elecciones en Venezuela no son "una guerra perdida"
Date: 2015-12-07 15:24
Category: El mundo, Entrevistas
Tags: Carlos Aznarez, Elecciones parlamentarias en Venezuela, Movimiento social, Nicolas Maduro, oposición en Venezuela, Resumen Latinoamericano, Revolución Bolivariana
Slug: elecciones-en-venezuela-no-son-una-guerra-perdida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Elecciones-en-Venezuela.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eltiempo.hn 

<iframe src="http://www.ivoox.com/player_ek_9627262_2_1.html?data=mpufmZeado6ZmKiak5aJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic3ZxMjW0dPJt4zZz5DDx9PJvtbZzcaY0NSPt9DijIqflNrSpYzb1srf1MaPtMbmxc7Rw4qWdo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Aznarez, Resúmen Latinoamericano] 

###### [7 Dic 2015]

“En estos días se tiene que pensar como se puede recomponer este momento, para convertirlo en un batalla perdida y no una guerra perdida”, asegura Carlos Aznarez, director del medio Resumen Latinoamericano ante los resultados de las elecciones parlamentarias en Venezuela que dejaron la Asamblea Nacional con  99 delegados en representación de la MUD contra 47 representantes del PSUV.

Ante este panorama, Aznarez, afirma que “evidentemente la guerra económica logró minar la confianza y el espíritu de lucha del pueblo venezolano que ha aguantado históricamente”, asegurando que con el resultado de las elecciones parlamentarias gana la derecha y la imperialismo norteamericano, generándose un retroceso frente a lo que se ha logrado en la reconstrucción del movimiento popular en América Latina.

Para el PSUV aun queda la posibilidad de las 19 curules que aun no están definidas,  sin embargo, Aznarez agrega que “el 25%  de abstencionismo le ha quitado una importante posibilidad de tener más diputados al chavismo”, pese a que se ha demostrado que el sistema electoral venezolano representó un triunfo de la democracia, teniendo en cuenta que el 74% de los venezolanos salieron a las calles a votar, evidenciando además que no hubo tal fraude electoral como lo había denunciado la oposición que salió ganadora.

“El imperio nos dio un recreo mientras invadía medio oriente” pero no se supo aprovechar, explica el director de Resumen Latinoamericano, quien indica que tanto en Venezuela como en los demás países progresistas de Latinoamérica, han existido algunos errores internos que han permitido que se den este tipo de resultados, pero lo importante, para él, es “seguir pensado en la movilización y en la revolución” pues “La oposición viene con un espíritu revanchista y lo que puede suceder ahora es mucho peor”.

“Hay una esperanza que es no renunciar a todo lo que el gobierno de la revolución ha dado (…) No nos podemos conformar con el capitalismo, sabemos que es muerte, exclusión y pobreza”, concluye Aznarez.

Otros analistas también resaltan la actitud profundamente democrática del presidente Nicolás Maduro que ha tenido que afrontar toda serie de críticas y descalificaciones, pero que ayer, justo después de conocer los resultados salió a rec0nocer la derrota y a plantear que se debe entrar en una nueva etapa de la revolución.
