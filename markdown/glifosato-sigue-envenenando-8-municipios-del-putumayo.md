Title: Glifosato sigue envenenando 8 municipios del Putumayo
Date: 2016-04-27 17:24
Category: DDHH, Nacional
Tags: cultivos ilícitos, Glifosato, Putumayo
Slug: glifosato-sigue-envenenando-8-municipios-del-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Glifosato-Putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gobierno Mayor Org ] 

###### [27 Abril 2016 ]

En los municipios del Medio y Bajo Putumayo, entre ellos Orito, Puerto Guzmán, Puerto Asís, Puerto Caicedo, Valle del Guamuez y San Miguel, crecen la zozobra y preocupación, entre las comunidades campesinas, indígenas y afrodescendientes, por la **erradicación de cultivos ilícitos con glifosato que vienen adelantando tropas del Ejército Nacional**.

Entre las principales consecuencias las comunidades denuncian **conflictos entre militares y pobladores, allanamientos ilegales, detenciones masivas, quema de viviendas, señalamientos y retención en los caminos veredales**. En Puerto Guzmán "se encontraron documentos de propaganda militar de la Brigada Móvil 13, informando la búsqueda de personas señaladas como parte de la Red de Apoyo al Terrorismo", señala el más reciente comunicado, la Red de Derechos Humanos de Marcha patriotica.

Las comunidades insisten en la necesidad de **implementar las propuestas de sustitución que se viene construyendo** entre el Gobierno y las organizaciones sociales de Putumayo, con el fin de evitar nuevos desplazamientos y poder cumplir con el retorno y buen vivir en el Campo.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
