Title: Delegación Asturiana denuncia la reparamilitarización del territorio Colombiano
Date: 2017-03-03 23:27
Category: Nacional, Paz
Tags: implementación acuerdos de paz, Paramilitarismo
Slug: delegacion-asturiana-denuncia-la-reparamilitarizacion-del-territorio-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/paramilitares-Caucasia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

###### [03 Mar 2017] 

Como "alarmante" calificó la Delegación Asturiana la situación de Colombia y la implementación de los acuerdos de Paz. El grupo integrado por 15 personas, entre eurodiputados, periodistas y defensores constató la **retoma del control territorial por el paramilitarismo, la persecución y asesinato a líderes y defensores de derechos humanos y las complejas condiciones en zonas veredales transitorias de las FARC**.

### **Paramilitarismo en Colombia** 

La Delegación Asturiana, después de su recorrido por varias regiones del país, recolectó información suficiente para constatar "el temor de las comunidades por el **avance de los grupos paramilitares en las zonas que está dejando libres las FARC"**.

De acuerdo con Tania González, Eurodiputada e integrante de la delegación, "pese a que el Ministro de Defensa niegue la presencia paramilitar, **en cada uno de los territorios en donde estuvieron, recibieron testimonios del proceso de reparamilitarización**" al igual que denuncias sobre desaparición forzada, reclutamiento forzado y un accionar en conjunto con Fuerzas Militares. Le puede interesar: ["Paramilitares arremeten en diferentes regiones del país"](https://archivo.contagioradio.com/paramilitares-arremeten-contra-diferentes-regiones-del-pais/)

Para la Delegación estos hechos evidencian que además de complicidad y criminalidad ligada al Estado hay falta de voluntad política por parte de las instituciones gubernamentales y las Fuerzas Militares para desmantelar a estas estructuras armadas y que **"pone seriamente en peligro las aspiraciones de paz"**. Le puede interesar: ["Sí hay paramilitares en el Catatumbo: Comisión de verificación"](https://archivo.contagioradio.com/comision-de-verificacion-en-catatumbo-verifico-denuncias-sobre-presencia-paramilitar/)

Además, la delegación informó que también recibió **denuncias sobre operaciones en conjunto entre paramilitares y empresas,** como Indupalma, para acaparar más tierras y aumentar su control en las zonas en donde tienen presencia.

### **"La paz real en Colombia sigue siendo una aspiración"** 

Otra de las situaciones que preocupa a la Delegación, es la constatación de los asesinatos sistemáticos a líderes sociales y defensores de derechos humanos, González afirmó que "desde el 2016 y los dos primeros meses del 2017, los **asesinatos, amenazas, desplazamientos y otras vulneraciones a los derechos humanos persisten y se agudizan**". Le puede interesar:["120 defensores de DDHH asesinados en 14 meses en Colombia: Defensoría"](https://archivo.contagioradio.com/120-defensores-asesinados-defensoria/)

A su vez, la Delegación Asturiana indicó que los crímenes tiene la intensión de "**destruir las organizaciones sociales que exigen justicia social,** desestimular la exigencia para la restitución de tierras usurpadas, a través del terror, acelerar el proceso de apropiación de tierras baldías y acallar a quienes se manifiestan contra los grandes proyectos minero-energéticos, **la defensa de derechos ancestrales como los indígenas o de los derechos de las comunidades LGTBI**.

De igual forma la Delegación señaló que la orden de erradicación forzada de cultivos ilícitos, por parte del Gobierno, no colabora con la situación actual de los campesinos y sí **"agudiza el conflicto social"**, debido a que son las mismas comunidades las que están exigiendo los planes estructurales para la sustitución de cultivos. Le puede interesar: ["Asesinada lideresa de ASOKINCHAS en Medellín"](https://archivo.contagioradio.com/asesinada-lideresa-asokinchas-medellin/)

### **Zonas Veredales** 

La Delegación Asturiana, después de conocer las zonas veredales de la Fila, la Elvira, Gallo y Planadas, expresaron su preocupación frente a los retrasos e insuficiencias en **materia de infraestructuras, y las condiciones "infrahumanas"** en las que se encuentran las y los guerrilleros de las FARC. Le puede interesar:["Nueva ola de solidaridad en Zonas Veredales Transitorias"](https://archivo.contagioradio.com/nueva-ola-de-solidaridad-con-las-zonas-veredales-transitorias/)

En ese sentido, evidenciaron que una de las causas de estas problemáticas en las zonas veredales, son las denuncias que han hecho diferentes organizaciones sobre **la corrupción que se está dando con los dineros para la paz** y la adjudicación de presupuestos y contratos a empresas que no están en la capacidad de responder y que han retrasado el avance en los puntos de normalización.

Otra situación que para la Delegación Asturiana amerita una respuesta urgente por parte del gobierno, es la implementación concreta de la ley de Amnistía, que no se ha impartido con la mayor celeridad, **ni ha beneficiado a los más de 4.000 guerrilleros** que se encuentran en las cárceles. Le puede interesar: ["Inició proceso de dejación de armas y es irrevercible: FARC"](https://archivo.contagioradio.com/ya-comenzo-el-proceso-de-dejacion-de-armas-y-es-irreversible/)

### **¿Qué viene ahora?** 

La Delegación Asturiana concluyó que ya se ha reunido con diferentes autoridades estatales como el Ministerio de Defensa y la ONU, que están en pleno conocimiento de cada una de las denuncias recolectadas por ellos.

Finalmente la Delegación insta al gobierno colombiano a atender las demandas que recolectaron, impulsar las Zonas de Reserva Campesina y salvaguardar los derechos y garantías de las poblaciones al territorio, la vida digna y la justicia; y agregan que el **gobierno debe ser consecuente con lo pactado en La Habana y avanzar en cada uno de los puntos que allí acordó.**

###### Reciba toda la información de Contagio Radio en [[su correo]
