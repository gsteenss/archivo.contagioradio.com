Title: “Pizarro”, memoria audiovisual de un padre en ausencia
Date: 2015-04-20 17:45
Author: CtgAdm
Category: 24 Cuadros, Política
Tags: Ambulante Colombia, Carlos Pizarro Leongómez, Centro de Memoria Paz y reconciliación, Fundación Carlos Pizarro Leongomez, La Popular, M-19, Movimiento 19 de Abril, Penguin Random House, Señal Colombia, Señal Colombia dirigida por Simón Hernández
Slug: pizarro-memoria-audiovisual-de-un-padre-en-ausencia
Status: published

*Simón Hernández, dirige el documental que tendrá su pre-estreno el próximo viernes.*

A pocos días de un cuarto de siglo del asesinato de **Carlos Pizarro Leongómez**, último comandante del **Movimiento 19 de Abril** M19 y excandidato presidencia por la Alianza Democrática resultante de la desmoviliación del grupo guerrillero; los pasos de María José , una de sus hijas, por recuperar su memoria son recogidos a través del documental.

“Pizarro”, una co-producción de **La Popular** y Señal Colombia dirigida por Simón Hernández, da cuenta de la búsqueda de la mujer por construir al fantasma y desenterrar la historia de su padre, asesinado en extrañas circunstancias cuarenta y cinco días después de haber firmado la paz con el Gobierno Colombiano.

Trailer  
<iframe src="https://player.vimeo.com/video/71510554" width="500" height="275" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[TEASER PIZARROdocmontevideo](https://vimeo.com/71510554) from [simon hernandez](https://vimeo.com/user909161) on [Vimeo](https://vimeo.com).

El documental, resultado de 6 años de duro trabajo, será presentado el próximo viernes 24 de Abril a las 7 de la noche en el **Centro de Memoria Paz y reconciliación** de Bogotá; un evento organizado en asocio con la **Fundación Carlos Pizarro Leongomez**, **Ambulante Colombia**, **Penguin Random House**.

Además de la presentación audiovisual el domingo 26 de abril, fecha del asesinato de Pizarro, se llevará acabo en el Cementerio Central un acto colectivo de memoria con la inhumación de sus restos.
