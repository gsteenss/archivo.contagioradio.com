Title: Indígenas y Afros del país exigen participación directa en Pacto Político Nacional
Date: 2016-10-04 16:50
Category: Nacional, Paz
Tags: Acuerdos de paz en Colombia, Comisión Interétnica, Plebiscito
Slug: victimas-del-pais-exigen-participacion-directa-en-pacto-politico-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/indigenas-cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

###### [4 Oct 2016] 

Hoy la Comisión Étnica para la Paz emitió un comunicando manifestando su **aprobación a los Acuerdos de paz, su disposición para la pronta implementación de los mismos en sus territorios y su exigencia de participación directa** en la [conformación del nuevo Pacto Nacional.](https://archivo.contagioradio.com/pacto-nacional-podria-ser-la-oportunidad-de-unificar-procesos-de-paz/)

En el documento hicieron un llamado a las partes, a garantizar la implementación de los acuerdos alcanzados en La Habana, y reiteraron el llamado a otros pueblos y comunidades étnicas del país, víctimas, organizaciones de derechos humanos y sociedad en general a defender la paz como un derecho de la sociedad colombiana.

"En las zonas y regiones que concentran un mayor número de víctimas del conflicto armado gano el Sí en el plebiscito, por ello **es fundamental que el nuevo Pacto incluya a estos sectores sociales azotados por el conflicto y garantice la no repetición de nuevos hechos de violencia".**

Finalmente la Comisión manifiesta la importancia de avanzar en el **proceso de [negociación con ELN para la construcción de la Paz completa, estable y duradera.](https://archivo.contagioradio.com/comision-etnica-se-ofrece-como-mediadora-para-reanudar-negociacion-gobierno-eln/)** Convocan a comunidades y sociedad en general a movilizarse y continuar la construcción de Paz.

La Comisión Étnica durante más de 3 años participó activamente de los debates sobre la construcción de la paz, realizó encuentros, foros, congresos y pronunciamientos que tuvieron como resultado una serie de propuestas para la inclusión en la agenda de Paz, gracias a ello **el 24 de agosto del 2016 se logró la instalación de la Mesa Étnica en La Habana.**

###### [Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU)] 

<div class="yj6qo ajU">

</div>
