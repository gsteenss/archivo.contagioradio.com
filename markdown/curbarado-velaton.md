Title: Una luz por los líderes sociales en Curbaradó
Date: 2018-07-31 18:08
Category: Comunidad, Sin Olvido
Tags: memoria, Sin Olvido
Slug: curbarado-velaton
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/sin-olvido-curvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 31 Jul 2018 

#### **Por Vanessa Bustos ** 

#### Zona humanitaria las camelias, cuenca del rio curbaradó, Chocó 

6:30 pm Cae el sol. Se oscurece la comunidad. Se acaban los partidos de futbol, los jugadores cansados reclaman su premio (que es una gaseosa) después, se bañan, algunos en sus casas, otros prefieren hacerlo en el pozo. Los niños dejan de jugar a las cogidas y con la oscuridad de la noche se reúnen en cualquier lugar donde haya algo divertido por hacer, en este momento suenan unos merengues a todo volumen en la casa de la matriarca y por supuesto los niños llegan allí. Algunas mujeres cenan y vuelven al punto de encuentro donde un fogón de leña sosteniendo un canelazo comienza a hervir.

7:30 pm Se toca la campana y llegan los que faltaban, todos reunidos en la casa de la memoria se ríen, participan y graban a los niños que cantan canciones a la vida, a la niñez y a la paz, también hacen obras de teatro que reflejan la fuerza con la que lucha la comunidad y claro, no puede faltar el baile, suenan chirimías, champetas y perreo que hacen parar a los mejores bailarines quienes improvisan bailes sin ningún temor ante los ojos expectantes de los adultos que se encuentran en la bancas que penden de las paredes del lugar, aunque algo cansados, aún hay energías para chiflar, aplaudir y hablar.

![sin olvido curvarado 6](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/sin-olvido-curvarado-6-800x533.jpg){.alignnone .size-medium .wp-image-55266 width="800" height="533"}

8:30 Hoy el silencio y el miedo que querían asomarse por la puerta son olvidados, pues un SIN OLVIDO de fondo anaranjado y de letra negra billa en una de las columnas de la casa de la memoria, que en su nombre alberga la fuerza y la esperanza de aquellos que respiran y que dejaron de respirar. Se encienden velas, cada uno pasa al frente y la quema expresando para todos sus motivos; los niños son los primeros en participar y cada luz que nace ilumina esos rostros pequeños de ojos grandes que invocan tímidamente el amor, la felicidad, la educación, la paz y por supuesto la unión para su comunidad, los adultos motivados empuñan velas dedicándolas a todos los que ya partieron, los que están y los que vienen, por la justicia para su gente y su territorio, por las ganas de seguir viviendo, por la libertad, por lo sagrado, por esos bailes, trovas y cuentos que representan la historia, la resistencia y el amor, por el trabajo, alimento y vida, por la vida, por la vida, por la vida de cada uno.
