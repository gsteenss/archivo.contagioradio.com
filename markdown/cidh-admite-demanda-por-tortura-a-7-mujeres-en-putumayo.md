Title: CIDH admite demanda por tortura a 7 mujeres en Putumayo
Date: 2017-10-26 16:20
Category: DDHH, Nacional
Tags: audiencias CIDH, CIDH, militares, paramilitares, violencia sexual
Slug: cidh-admite-demanda-por-tortura-a-7-mujeres-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/cidh-e1509052767369.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Periódico AM] 

###### [26 Oct 2017] 

La corporación Justicia y Dignidad dio a conocer la decisión de la Comisión Interamericana de Derechos Humanos (CIDH) de **admitir la denuncia por desaparición forzada, tortura y violencia sexual** a 7 mujeres en Colombia, un crimen cometido por militares y paramilitares en 2004.

Los hechos ocurrieron el 25 de octubre de 2004 en la vereda el Picudo de Puerto Caicedo, Putumayo. Allí, miembros del Plan Energético Vial No.9 del Batallón de Infantería No.25 del Ejército de Colombia, en conjunto con paramilitares de las Autodefensas Unidas de Colombia,  "iniciaron un operativo que duró varios días e implicó **el secuestro, amenazas físicas y psicológicas**, lesiones con arma corto punzante, amarres, disparos con arma de fuego, privación de alimentos, tortura y abuso sexual de al menos siete mujeres campesinas”.

De acuerdo con la Corporación, a una de las mujeres, específicamente a **Rosalia Benavides Franco**, “luego de desaparecerla, someterla a múltiples vejámenes la ejecutaron extrajudicialmente y secuestraron a su hijo de dos años”. Igualmente, al joven de 15 años, Juan Guillermo Gutierrez, lo secuestraron, lo desaparecieron y lo asesinaron. (Le puede interesar:["La Fiscalía debe investigar los crímenes de lesa humanidad cometidos por el DAS"](https://archivo.contagioradio.com/victimas-del-das/))

### **Investigación judicial en el país tardó 6 años en empezar** 

Justicia y Dignidad argumentó que, **a pesar de que existía material probatorio suficiente** para realizar las diligencias judiciales a tiempo, sólo a través de solicitud de la Corporación, la Fiscalía Segunda Especializada de Mocoa en Putumayo, inició en 2010 la indagación preliminar.

Cuando el caso fue asignado a la Fiscalía 70 especializada de la Unidad de DDHH de Cali y teniendo de presente las “múltiples declaraciones de soldados que fueron testigos de los hechos y pese **al reconocimiento expreso de los hechos** del sargento Sierra Daza”, la Fiscalía no resolvió la situación jurídica del implicado hasta luego de 12 años de ocurridos los hechos.

Igualmente, hecha la valoración psicológica de las mujeres afectadas, teniendo en cuenta el Protocolo de Estambul, que es un manual para la investigación y la documentación eficaz de la tortura y tratos crueles, **“no existe hasta la fecha una decisión razonada** de establecimiento y sanción de la responsabilidad de autores y participes en los atroces hechos de violencia sexual”. (Le puede interesar: ["La impunidad en crímenes de Estado es del 90%: Alejandra Gaviria"](https://archivo.contagioradio.com/la-impunidad-en-crimenes-de-estado-es-del-90-alejandra-gaviria/))

Pese a la magnitud y gravedad de los hechos, la Corporación indicó que “los familiares de las víctimas y las mujeres víctimas de violencia sexual **no han sido vinculados a ningún programa de atención médica**, terapéutica o psicosocial, tal como lo ordena el dictamen de Medicina Legal”.

Por esto, responsabilizaron al Estado de **incumplir con sus obligaciones constitucionales** de reconocimiento de los derechos y del establecimiento de las garantía de verdad, justicia y reparación.

### **CIDH indicó que la investigación sigue pendiente** 

Ante la lentitud en la investigación en el país, la CIDH emitió su informe 109/17 el 7 de septiembre de 2017 donde, de acuerdo con la Corporación,  indicó que, a más de 12 años de iniciadas las investigaciones, **no hay información “que indique que el Estado habría completado** una investigación respecto de todos los presuntos responsables y una investigación sigue pendiente en la etapa de indagación”.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="osd-sms-wrapper">

</div>
