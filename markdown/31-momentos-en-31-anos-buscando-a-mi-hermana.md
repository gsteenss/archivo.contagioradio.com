Title: Rene Guarín, 31 años buscando a su hermana
Date: 2016-09-11 07:53
Category: Sin Olvido
Tags: crímenes de estado, Cristina del Pilar Guarín, Desaparecidos del Palacio de Justicia
Slug: 31-momentos-en-31-anos-buscando-a-mi-hermana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/rene-guarin.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  Foto: Gabriel Galindo 

##### 9 Sep 2016 

<iframe id="audio_12858792" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12858792_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Desde el día 6 de noviembre de 1985, cuando a sangre y fuego el Ejército colombiano retomó el control del **Palacio de Justicia** en Bogotá, tras intensas horas de confrontación con el M19, en el desarrollo de la operación "Antonio Nariño por los Derechos del Hombre" planeada y ejecutada por esa guerrilla, para **la familia Guarín Cortés**, y otras diez familias colombianas, la vida dejó de ser la misma.

**Cristina del Pilar**, hija del matrimonio de don José y la señora Elsa y hermana de René, realizaba por esos días un remplazo como cajera de la cafetería del palacio, una casualidad que costaría además del fin de su existencia, la tranquilidad de sus padres que nunca dejaron de buscarla mientras hubo vida en sus cuerpos. Un legado que transfirieron a su hijo quien durante **31 años** ha persistido en hallarla, viva o muerta,  y conocer la verdad.

A pesar de la intencionalidad del aparato político, militar y judicial del Estado, en negar, desviar, dilatar y entorpecer los procesos por cada uno de los desaparecidos del Palacio de Justicia, a pasos muy lentos van surgiendo las certezas, y hoy además de reconocer públicamente sus errores, deben garantizar que aquellos a quienes aún esperan en sus hogares regresen para ser despedidos dignamente.

El 9 de septiembre, día del que sería su cumpleaños 58, y luego de once meses de retrasos, [9 restos óseos de Cristina del Pilar](https://archivo.contagioradio.com/cristina-del-pilar-guarin-31-anos-de-viacrucis/), fueron entregados por la Fiscalía a Rene Guarín, cerrando en sus propias palabras "una etapa de su vida, quedando pendiente aún la de la verdad"; saber ¿si la mataron y luego la desaparecieron o si la desaparecieron y luego la mataron? respuestas que podrían tomarle otros 31 años de lucha y persistencia.

Es el cierre de tantos años de golpear puertas, de exigir justicia aquí y ante el mundo, de perder tanto y ganar tan poco. Momentos de conquistas y derrotas que el mismo René relata en el siguiente especial.
