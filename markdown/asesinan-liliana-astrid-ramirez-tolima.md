Title: Asesinan a Liliana Astrid Ramírez, docente y lideresa en Tolima
Date: 2017-10-19 11:05
Category: DDHH, Nacional
Slug: asesinan-liliana-astrid-ramirez-tolima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/PARAMILITARES-cordoba.gif" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: minuto30] 

###### [19 Oct 2017]

Según las primeras versiones, la profesora **Liliana Astrid Ramírezfue asesinada esta mañana** cuando se bajaba de un taxi en la entrada del colegio de la vereda San Miguel en zona rural de Coyaima. Los sicarios que se movilizaban en una moto huyeron del lugar luego de disparar en repetidas ocasiones contra la docente con rumbo desconocido dado que no hubo reacción por parte de las autoridades.

La denuncia inicial relata que **varios de los docentes y rectores de ese centro educativo han sido amenazados de muerte** en repetidas ocasiones, se han presentado las denuncias correspondientes sin que haya reacciones o resultados de las investigaciones por estos hechos, es decir que hasta el momento no se conocen las intenciones o razones de dichas amenazas.

Adicionalmente se ha podido establecer que hace pocos minutos se está realizando el levantamiento de la profesora y se estarían estableciendo los primeros indicios de las razones de este asesinato que es interpretado, en principio, como un atentado contra el derecho a la eduación y contra los liderazgos de la región.

Noticia en desarrollo…

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
