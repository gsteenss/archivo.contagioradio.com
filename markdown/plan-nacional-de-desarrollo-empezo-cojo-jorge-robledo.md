Title: “Plan Nacional de Desarrollo empezó cojo”, Jorge Robledo
Date: 2015-02-06 21:56
Author: CtgAdm
Category: Economía, Entrevistas
Tags: economia, Jorge Robledo, Plan Nacional de Desarrollo, PND
Slug: plan-nacional-de-desarrollo-empezo-cojo-jorge-robledo
Status: published

##### Foto: La Patria 

<iframe src="http://www.ivoox.com/player_ek_4050042_2_1.html?data=lZWikpWYdo6ZmKiakpyJd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56nic2fsdHO0JCypcTd0NPOzpDIqYy4xtjO1NfTsM3jjMra0sreaaSnhqegjcjTrtCZppeSmpWJfaWZk6iYrNTWq46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jorge Robledo] 

El Plan Nacional de Desarrollo (PND), 2014-2018, que tenía un déficit de \$58 billones, **tuvo que ser reducido a \$700 billones, es decir que el presupuesto inicial se redujo \$90 billones**. Para el senador del Polo Democrático Alternativo, Jorge Enrique Robledo, esto significa que serán \$22 billones menos por año, lo que quiere decir que, “de entrada el PND ya empezó cojo”, expresa el congresista.

Para Robledo, el segundo PND del presidente Santos, no tuvo en cuenta la crisis económica dada por las dificultades que presenta el sector minero, por lo que este plan de desarrollo no le cambia nada al modelo neoliberal, “**es una especie de soberbia autista”**, afirmó.

De acuerdo a Simón Gaviria, director del Departamento Nacional de Planeación, el PND cuenta con las 136 exigencias de la OCDE (Organización para la Cooperación y el Desarrollo Económicos); sin embargo, esto no significa otra cosa que “**los gringos, las potencias industriales, el banco mundial, el Fondo Monetario Internacional, son quienes definirán la suerte de Colombia”**, enmarcado en los intereses de unos pocos, expresa Robledo.

“**La primera razón de la pobreza y el sufrimiento es el propio modelo económico**, se destruye el agro y la industria como si nada, en eso el PND sigue igual”, indica, además “el faltante principal va a estar en los gastos sociales- y añade irónicamente que- **hasta la mermelada también se verá afectada** al reducir en \$90 billones el presupuesto”.
