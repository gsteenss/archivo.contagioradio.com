Title: Otra Mirada: 21 años sin Jaime Garzón
Date: 2020-08-19 11:26
Author: PracticasCR
Category: Otra Mirada, Programas
Tags: Jaime Garzón
Slug: otra-mirada-21-anos-sin-jaime-garzon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Jaime-Garzón-e1457652708929.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Con La Oreja Roja

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El 13 de agosto es el aniversario de la muerte de Jaime Garzón, fecha en la que  familiares, amigos, organizaciones y muchos colombianos recuerdan lo dicho, lo contado por Garzón; palabras que, entre chistes, hicieron a miles de personas conscientes sobre lo que estaba pasando en el país, e inspiraron a muchos a seguir luchando por acabar con la corrupción y la violencia en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por eso, hoy es valioso ver cuál es su legado tanto para la sociedad colombiana, como para quienes continúa el trabajo dejado por el humorista Garzón después de 21 años. (Le puede interesar: [Medios independientes son la otra víctima de la Fuerza Pública en la movilización](https://archivo.contagioradio.com/medios-independientes-son-la-otra-victima-de-la-fuerza-publica-en-la-movilizacion/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el conversatorio de Contagio Radio estuvieron Eduardo Arias, periodista y libretista de Zoociedad, Sebastián Escobar, abogado del caso Jaime Garzón, Fabiola León, periodista, docente y analista de medios y Diego Alarcón, periodista y director de proyectos en Riaño Producciones. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Jaime Garzón se caracterizaba por crear e interpretar diversos personajes con los que creaba empatía con los colombianos, por lo que, los invitados explican cómo era ese proceso de trabajo y cómo lo hacen ahora quienes siguen sus pasos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Comparten cómo el humor se viene utilizando como una herramienta para comunicar. y termina siendo una virtud utilizada por el mismo Garzón para acercar a mucha gente a la realidad del país, haciendo posible el escrutinio social. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente comentaron cómo se deberían armar los medios ahora y cuál ha sido el aporte de Garzón para el periodismo y añaden que se debe seguir incentivando a los jóvenes, cambiando el discurso tradicional y que estos no limiten su forma de informarse.  

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Si ustedes los jóvenes no asumen la dirección de su propio país, nadie va a venir a salvarlo. ¡Nadie!"
>
> <cite>Jaime Garzón</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Si desea escuchar el programa del 13 de agosto [Haga click aquí](https://www.facebook.com/contagioradio/videos/297309114909299)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Si desea escuchar el análisis completo

<!-- /wp:paragraph -->

<!-- wp:html /-->

<!-- wp:block {"ref":78955} /-->
