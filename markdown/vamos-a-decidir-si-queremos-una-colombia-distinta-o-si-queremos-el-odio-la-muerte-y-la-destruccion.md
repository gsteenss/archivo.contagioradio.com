Title: "Vamos a decidir sí queremos una Colombia distinta o sí queremos el odio, la muerte y la destrucción"
Date: 2016-07-19 16:18
Category: Nacional, Paz
Tags: Diálogos de paz Colombia, plebiscito por la paz, refrendación acuerdos de paz
Slug: vamos-a-decidir-si-queremos-una-colombia-distinta-o-si-queremos-el-odio-la-muerte-y-la-destruccion
Status: published

###### [Foto: Archivo ] 

###### [19 Julio 2016]

Esta decisión le abre el paso a un certamen democrático que definirá el futuro de los próximos años para todos los colombianos, para quienes han sufrido en carne propia los estragos del conflicto armado, para quienes están en los campos, pero también para todos los habitantes de las grandes urbes; **un futuro distinto, exento del sufrimiento, del miedo y de la zozobra que implican todas las formas de violencia y despojo** que hemos padecido durante tantas décadas en Colombia, es eso lo que hay que definir el día en que se vote el plebiscito por la paz, asegura el Senador Iván Cepeda en relación con la más reciente decisión de la Corte Constitucional.

Tendremos que decidir sí queremos un futuro diferente, sí queremos ir hacia adelante, hacía una Colombia distinta o sí queremos el pasado, el odio, la muerte, la destrucción, eso es lo que definiremos en este plebiscito, **quienes votaremos por el si entendemos la paz con reformas sociales y transformaciones de fondo**, que impliquen no solamente el cese de hostilidades sino cambios en términos económicos, políticos y sociales, agrega, e insiste en que es un deber ético de toda la ciudadanía participar en el plebiscito, teniendo en cuenta que la única restricción que hay es que no se utilice la campaña del plebiscito con fines de proselitismo electoral particular.

Por su parte Ariel Avila, subdirector de la Fundación Paz y Reconciliación, afirma no estar de acuerdo con la decisión de la Corte Constitucional, pues implica que después de la firma habrá que esperar tres meses, sin que se pueda ir adelantando la implementación de los acuerdos, es decir, **sin que las FARC se concentren y sin que se adelanten los planes piloto de sustitución de cultivos ilícitos y de desminado humanitario**.

El proceso de paz está hecho para lo que puede dar no para solucionar todos los problemas en Colombia, agrega Avila e insiste en que las últimas encuestas demuestran que **la población empieza a comprender que un asunto es el gobierno Santos y otro, la paz**; sin embargo, falta mucha pedagogía, sobre todo en los escenarios urbanos, tanto para los guerrilleros como para la ciudadanía, pues de perderse el plebiscito se echaría a perder todo el proceso de paz, de allí la importancia de que la Corte aclare la participación de los partidos políticos, el carácter vinculante y el papel del Congreso.

"Con alegría vamos a salir a votar por el si", la tarea pedagógica por el plebiscito es fundamental, es por el si que hemos ambientado con el trabajo con el Frente Amplio por la Paz, con Poder Ciudadano, con las Zonas de Reserva Campesina, con la ONIC y con los jóvenes, tantas organizaciones, hombres y mujeres con los que hemos trabajado durante años para lograr que la paz sean un mandato ciudadano, popular y democrático como **un primer paso para poder vivir en una Colombia con digna, incluyente y diversa** que nos permita participar sin ningún riesgo, asevera la Representante Angela María Robledo.

<iframe src="http://co.ivoox.com/es/player_ej_12274737_2_1.html?data=kpefmZmbd5ihhpywj5WbaZS1kZyah5yncZOhhpywj5WRaZi3jpWah5yncarqhqigh6aVsoy3xtXSxsaJdqSfsdTZ0ZCoqc7jxNeSpZiJhZLoysjcj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe src="http://co.ivoox.com/es/player_ej_12274841_2_1.html?data=kpefmZmceJKhhpywj5WdaZS1kZmah5yncZOhhpywj5WRaZi3jpWah5yncaLmysrZjabarc3VhpewjavZssXVxM6SpZiJhpTijLXO3JDdb7PZxNTbxc7QrcLXyoqwlYqmd8-hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe src="http://co.ivoox.com/es/player_ej_12274822_2_1.html?data=kpefmZmcdpOhhpywj5WbaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncaLiyMrZw5C2s8Pgxsncj5C2qdGijKbZy8bSvsKft8rfxsqRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]  ] 
