Title: ELN niega su participación en masacre de Samaniego en Nariño
Date: 2020-08-17 15:11
Author: AdminContagio
Category: Nacional
Slug: eln-niega-su-participacion-en-masacre-de-samaniego-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Image-2020-08-17-at-1.34.53-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Samaniego-ELN.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

[Foto: Caracol Radio]{.has-inline-color .has-cyan-bluish-gray-color}

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este lunes festivo, 17 de Agosto, se conoció un comunicado en el que la guerrilla del Ejército de Liberación Nacional, ELN, desmiente las afirmaciones que se han hecho sobre su posible participación en la masacre de los 9 jóvenes en Samaniego, Nariño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El comunicado de tres puntos, asegura que el audio en el cual se hace referencia en esta nota no es de autoría de esa guerrilla, ni tampoco un panfleto en el que Parmenio Cuellar afirma que la autoría de la masacre se le atribuye al ELN. Lea también: [Asesinados nueve jóvenes en Samaniego Nariño en una nueva masacre](https://archivo.contagioradio.com/nueva-masacre-en-narino-fueron-asesinados-9-jovenes/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente denuncian que en esa región de Nariño hay fuerte presencia militar y que además habría una alianza del ejército y la policía con organizaciones al servicio del narcotráfico.

<!-- /wp:paragraph -->

<!-- wp:image {"id":88366,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Image-2020-08-17-at-1.34.53-PM.jpeg){.wp-image-88366}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Cabe recordar que Nariño, es uno de los departamentos que enfrentar grandes problemas de violencia por disputa de tierras y que además ha causado múltiples asesinatos a defensores, líderes de Derechos Humanos y comunidades indígenas; así como masacres y el fuego cruzado entre diferentes grupos armados que dejan en medio a la comunidad campesina y que evidencian aún más el olvido por parte del Gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Olvidó que según Indepaz, ubica el departamento de Nariño como el tercero más peligroso y con el mayor número de asesinatos desde la firma del Acuerdo de Paz con 84 casos registrados. ( Otra Mirada: [Nariño resiste ante el olvido estatal](https://archivo.contagioradio.com/otra-mirada-narino-resiste-ante-el-olvido-estatal/)).

<!-- /wp:paragraph -->
