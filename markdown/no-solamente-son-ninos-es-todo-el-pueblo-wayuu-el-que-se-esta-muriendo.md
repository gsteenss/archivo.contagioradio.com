Title: "No solamente son niños, es todo el pueblo Wayúu el que se está muriendo"
Date: 2016-07-26 14:58
Category: DDHH, Nacional
Tags: arroyo Bruno, El Cerrejón, enfoque diferencial, La Guajira
Slug: no-solamente-son-ninos-es-todo-el-pueblo-wayuu-el-que-se-esta-muriendo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/La-guajira-e1469562410913.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zona cero 

###### [26 Jul 2016] 

Nuevamente **otros dos niños de la comunidad indígena Wayuú del departamento de La Guajira** han muerto esta semana por desnutrición y falta de atención efectiva por parte del gobierno, como lo asegura Jackeline Romero, integrante Fuerza Mujeres Wayúu, quien añade que la comunidad sigue viviendo esta situación pese a los múltiples anuncios mediáticos que ha hecho Juan Manuel Santos.

En total, **según cifras oficiales se trata de 40 menores de edad que han fallecido en lo corrido de este año,** sin embargo de acuerdo con la organización de mujeres, no se trata únicamente de los niños y niñas, pues detrás de ellos hay toda una familia que está sufriendo las mismas condiciones de hambre y sed, por lo que la cifra total puede ser mucho más alarmante.

“Debe haber garantías también para los padres que deben caminar entre 10 y 15 km para llegar a un centro de asistencia para que sus hijos seas atendidos, sin haber comida algo o haber tomado agua. Y mientras atienden al menor la madre se debe quedar en la calle”, dice Romero, y agrega que el único problema no es la desnutrición, sino también las enfermedades respiratorias por las cuales muchos indígenas están muriendo.

La comunidad denuncia que no existe una integralidad en la atención a las familias, no hay un verdadero enfoque diferencial, no hay acceso a servicios públicos y en cambio hay una total incoherencia gubernamental, pues mientras los indígenas y demás comunidades siguen muriendo de sed a falta de agua, **se sigue otorgando licencias ambientales a empresas como El Cerrejón para que desvíe arroyos y ríos de los cuales se proveen las familias.**

“El gobierno no puede seguir metiéndonos el tema de la corrupción y el cambio climático. Eso existe, pero no es la causa estructural, lo más importante es la falta de implementación de **programas integrales con enfoque diferencial, debe haber una articulación real de la cultura**”.

Sumado a eso, Romero afirma que el gobierno ha sido permisivo con la actividad minera de El Cerrejón que hoy tiene licencia para desviar el arroyo Bruno, una de las fuentes de agua más importantes de la población Wayúu. “**El gobierno nos está asesinando con su falta de implementación de políticas.** Va a seguir muriendo una generación de hambre y sed. Si se sigue entregando nuestros recursos hídricos no habrá garantía de vida”, concluye la líder indígena.

<iframe src="http://co.ivoox.com/es/player_ej_12345010_2_1.html?data=kpeglpqUdZGhhpywj5eUaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5yncavVxNDSzs7SqYzG0NLS1NSJdqSfp9rS1N_Fb67py8rfx9iPm8Lthqigh6eluY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
