Title: "El perdón es recordar sin odio y sin rencor" Imelda Daza
Date: 2016-06-23 11:39
Category: Nacional, Paz
Tags: cese bilateral del fuego, conversaciones de paz, FARC
Slug: el-perdon-es-recordar-sin-odio-y-sin-rencor-imelda-daza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/imelda-daza-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: tranastidning.se] 

###### [23 Jun 2016]

De acuerdo con Imleda Daza, vocera de la Unión Patriótica, el anuncio de la firma del cese bilateral entre el gobierno de Colombia y la guerrilla de las FARC-EP hace de este día un día histórico debido a que **se pone fin  a una cruenta guerra  de muchas décadas y horrores.**

Sin embargo expone que es necesario que se den condiciones específicas para que el postacuerdo en Colombia se de en forma digna para las víctimas del conflicto armado. Por un lado, habla de la necesidad de que los medios de comunicación desmonten el discurso guerrerista que se ha incrustado en la sociedad, "es lamentable que ese discurso que inoculo en los grandes medios de información todavía no se haya desmontado y cambiado, es hora de la paz y la verdad. Para mi una condición indispensable para el futuro de Colombia es conocer la verdad sobre lo que pasó en el país".

Sobre el reto de la verdad que afrontará la sociedad colombiana, Daza mencionó que es importante saber quienes fueron los responsables directos, intencionales e intelectuales de tanta barbarie, "queremos saber sus nombres y sus apellidos, no para la venganza, es para perdonar, **queremos saber quienes fueron para saber a quien tenemos que perdonar**. El perdón no es olvido, el perdón es recordar sin odio y sin rencor, es difícil es un aprendizaje necesario que tenemos que desarrollar. El perdón hará posible la reconciliación.  Tiene que ver con la tolerancia."

A su vez, insistió en **una paz completa que no puede ser posible si no se inician los diálogos de paz con la guerrila del ELN**, "es un menester trabajar en esas negociaciones para que se aceleren de acuerdo con los principios establecidos que ya se conocen, es preocupante la lentitud con la que se esta empezando ese proceso, sin embargo esperamos que la experiencia que ya tenemos sirva para dar tramite a ese proceso".

En cuanto al desmonte del paramilitarismo Emilda Daza aseguro que sigue operando en la costa Atlántica, "Doy fe de que es así, no con la misma fuerza o estructuras con que funcionaron antes, pero siguen pendientes de sabotear lo que queremos construir, es preocupante que el gobierno no muestre señales claras frente al desmonte del paramilitarismo".

Daza ha sido una de las mujeres que ha vívido en carne y hueso el dolor de la guerra tras el exilio que vivió durante **28 años y la perdida de muchos de sus compañeros y compañeras asesinados o desaparecidos durante la persecución política al partido Unión Patriótica**, motivo por el cual considera que este anuncio le trae sentimientos encontrados

"hay mucha nostalgia, hay muchos recuerdos que ahora se vienen todos juntos, la memoria de los compañeros, de los amigos de quienes murieron y les arrebataron la vida por la guerra, en ese horror de la guerra y de nuestra incapacidad para vivir en medio de la diversidad. Se siente también  muchísima alegría al pensar que a partir de ahora es posible reconstruir el país desarrollar la democracia, reconciliarnos, para entre todos y todas **aprender a vivir de nuevo, a vivir en armonía y cordialidad, en medio de una diversidad que solo nos enriquece,  sino que además, no tiene porque impedirnos vivir bien entre los colombianos"**

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU).]

###### 
