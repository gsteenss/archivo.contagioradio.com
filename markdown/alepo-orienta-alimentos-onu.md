Title: Alepo tiene alimentos para sobrevivir una semana más
Date: 2016-11-10 15:22
Category: El mundo, Otra Mirada
Tags: Alepo, alimentos, ONU, Siria
Slug: alepo-orienta-alimentos-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/1062767620-e1478808785779.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Sputnik Mundo 

##### 10 Nov 2016

La Organización de las Naciones Unidas ONU, advirtió que **los alimentos enviados en el mes de Julio para distribuir en Alepo oriental, se están agotando** al punto que la semana entrante no habrán raciones para atender a las miles de familias que hoy padecen hambre por cuenta de la guerra.

El estado de sitio en que se encuentra la ciudad, por la acción de las fuerzas gubernamentales apoyadas por la aviación rusa, **no ha permitido el ingreso de más comida desde hace más de 4 meses**, desde entonces los 275.000 civiles que se estiman habitan en la zona han sobrevivido racionando las porciones alimentarias.

Funcionarios de la ONU, hacen un llamado de urgencia para las partes en conflicto y las potencias que las apoyan **permitan durante las próximas horas la entrada de equipos humanitarios a esa parte de la ciudad**, única de las 18 zonas sitiadas que no ha podido recibir ayudas en los últimos meses, y que preocupa aun más al aproximarse la llegada del invierno.

La situación de un cuarto de millón de personas debe resolverse a mas tardar la semana que viene, para lo cual se ha establecido un **plan de ayuda para Alepo**, que incluye la distribución de la comida, **la evacuación de enfermos y heridos y la entrada de personal médico a la ciudad**, apuesta cuya implementación depende de la voluntad de las partes involucradas y sus aliados internacionales.

 

Para que los convoyes de la ONU puedan ejecutar el plan, **las partes deben garantizar su seguridad para transitar, 72 horas para prepararse y tiempo suficiente para hacer las evacuaciones y la distribución de los enseres y alimentos** que se encuentran en Turquia y en Alepo Occidental donde la ONU tiene reservas almacenadas. Le puede interesar: [22 niños muertos por bombardeos en Siria](https://archivo.contagioradio.com/22-ninos-muertos-por-bombardeos-en-siria/)

 
