Title: Asesinan en Patía, Cauca al excombatiente Dago Galíndez
Date: 2019-10-23 11:35
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato de excombatientes, Cauca, Patía
Slug: asesinan-en-patia-cauca-al-excombatiente-dago-galindez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Dago.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: HombreRobot] 

El pasado 23 de octubre en Patía, Cauca, fue asesinado con arma de fuego,** Dago Hernán Galíndez Chicangana**, excombatiente quien se había acogido al Acuerdo de Paz. Junto a él también se encontraba Eduar Andrey Baos Imbachí de 36 años quien también fue asesinado de cuatro impactos de bala. Los homicidios contra excombatientes de la antigua guerrilla de las  FARC hoy ascienden a más de 137.

Dago Galíndez tenía 36 años de edad, era oriundo del municipio de Icononzo, Tolima y según el senador Juan Carlos Lozada de FARC, hizo parte de la compañía José María Carbonell. [(Lea también: Ánderson Pérez, excombatiente y comunicador fue asesinado en Cauca](https://archivo.contagioradio.com/anderson-perez-excombatiente-y-comunicador-fue-asesinado-en-cauca/)

Según testigos de los hech**os, integrantes del grupo armado que estaría tras este ataque y del que aún no se ha podido establecer su identidad,** impidieron que la comunidad ayudara a las víctimas que convulsionaban tras ser impactados por las balas. Los cuerpos de Dago y Eduar fueron trasladados por Medicina Legal a Popayán para ser analizados.

Fuentes del Partido FARC señalan que antes de realizar pronunciamientos oficiales, realizarán las investigaciones pertinentes para esclarecer los motivos por las que pudo ser asesinado Dago Hernán y los responsables detrás de su muerte. [(Le puede interesar: No avanzan investigaciones sobre asesinatos de excombatientes)](https://archivo.contagioradio.com/no-avanzan-investigaciones-sobre-asesinatos-de-excombatientes/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
