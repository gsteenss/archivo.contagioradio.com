Title: Dilma Rousseff es destituida en medio de rechazo de la comunidad internacional
Date: 2016-08-31 15:23
Category: El mundo, Otra Mirada
Tags: Brasil, Dilma Rouseff, Dilma Roussef impeachment
Slug: las-reacciones-en-america-latina-tras-la-destitucion-de-dilma-rousseff
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Destitución-Dilma.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Brasil De Fato ] 

###### [31 Ago 2016] 

Con 61 votos a favor y 20 en contra el Senado **destituyó definitivamente a la mandataria de Brasil** Dilma Rousseff, quién estaba apartada del cargo desde el pasado mes de mayo, por cuenta del proceso de impeachment en el que fue acusada del delito de responsabilidad fiscal. Para diversos sectores este proceso estuvo plagado de irregularidades y se trató de un golpe de Estado motivado por la clase política, en detrimento de las garantías sociales conquistadas.

Ecuador a través de un comunicado condenó estos acontecimientos que constituyen una "flagrante subversión del orden democrático (...) un [[golpe de Estado solapado](https://archivo.contagioradio.com/esto-no-es-un-juicio-politico-es-un-golpe-de-estado-dilma-rousseff/)] \[en el que\] **políticos adversarios y otras fuerzas de oposición** **se confabularon contra la democracia para desestabilizar al Gobierno** y remover de su cargo de forma ilegítima a la Presidenta. Según la comunicación, el mandatario ecuatoriano llamará a consultas Horacio Sevilla, su embajador en Brasil.

El Gobierno cubano también rechazó categóricamente este golpe de estado parlamentario, asegurando que "la separación del gobierno de la Presidenta, **sin que se presentara ninguna evidencia de delitos** de corrupción ni crímenes de responsabilidad (…) constituye un acto de desacato a la voluntad soberana del pueblo que la eligió". Rechazo que también manifestaron los gobiernos de Bolivia y Venezuela, quien por su parte decidió congelar las relaciones políticas y diplomáticas con Brasil.

Mientras en ciudades como Brasília, Belo Horizonte, Rio de Janeiro, Curitiba y São Paulo diversas organizaciones, articuladas en los Frentes Brasil Popular y Pueblo sin Miedo, **se movilizaron durante las sesiones de impeachment**, el Consejo Latinoamericano de Ciencias Sociales emitió un comunicado en el que aseguró que "hoy es un día de luto para Brasil y toda América Latina (...)[ [se cometió una de las mayores infamias](https://archivo.contagioradio.com/los-ultimos-dias-de-rousseff-como-presidenta-de-brasil/)] de la historia democrática latinoamericana"

De acuerdo con João Pedro Stedile, dirigente del Movimiento de los Trabajadores Rurales Sin Tierra MST, las élites brasileñas derrumbaron el Gobierno de Dilma porque necesitan campo libre para imponer un nuevo plan neoliberal que pueda recuperar los grados de explotación sobre la clase obrera y apropiarse de los recursos que podrían invertirse en salud o educación, por lo que **el primer reto es resistir desde la lucha de masas a la ofensiva neoliberal** hasta que se logre imponer un nuevo proceso democrático.

https://www.youtube.com/watch?v=EhQCSF5GlT8

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
