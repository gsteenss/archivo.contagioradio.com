Title: LGBTI
Date: 2014-11-25 15:43
Author: AdminContagio
Slug: lgbti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/LGBTI.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/lgbti.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/LGBTI.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/lgbti_flag-e1478191764857.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/lgbti.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/LGBTI-e1479146333517.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/lgbti-e1494435755511.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/LGBTI-e1479146333517-770x400.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

LGTBI
-----

[](https://archivo.contagioradio.com/comunidad-lgtbi-discriminada-bucaramanga/)  

###### [Comunidad LGTBI víctima de discriminación por parte de grupo de religiosos en Bucaramanga](https://archivo.contagioradio.com/comunidad-lgtbi-discriminada-bucaramanga/)

[<time datetime="2018-09-18T15:34:37+00:00" title="2018-09-18T15:34:37+00:00">septiembre 18, 2018</time>](https://archivo.contagioradio.com/2018/09/18/)Grupo de religiosos convocó un plantón para impedir el hacer uso de una biblioteca pública a comunidad LGTBI de Bucaramanga.[Leer más](https://archivo.contagioradio.com/comunidad-lgtbi-discriminada-bucaramanga/)  
[](https://archivo.contagioradio.com/irlanda-decide-hoy-sobre-el-matrimonio-homosexual/)  

###### [Irlanda decide hoy sobre el matrimonio homosexual](https://archivo.contagioradio.com/irlanda-decide-hoy-sobre-el-matrimonio-homosexual/)

[<time datetime="2015-05-22T14:24:30+00:00" title="2015-05-22T14:24:30+00:00">mayo 22, 2015</time>](https://archivo.contagioradio.com/2015/05/22/)Foto:Elconfidencial.com Irlanda se ha convertido en el primer país del mundo que organiza un referéndum sobre la legalización del matrimonio homosexual, o entre personas del mismo sexo. El Referéndum ha sido convocado después de que[Leer más](https://archivo.contagioradio.com/irlanda-decide-hoy-sobre-el-matrimonio-homosexual/)  
[](https://archivo.contagioradio.com/comunidad-lgbti-contra-la-homofobia/)  

###### [Comunidad LGBTI contra la homofobia](https://archivo.contagioradio.com/comunidad-lgbti-contra-la-homofobia/)

[<time datetime="2015-05-19T18:41:40+00:00" title="2015-05-19T18:41:40+00:00">mayo 19, 2015</time>](https://archivo.contagioradio.com/2015/05/19/)Foto: Contagio Radio   Entrevista: Felipe Acevedo  19 May 2015 En el marco de la celebración del día internacional contra la homofobia, se estrena en Bogotá el Documental "Redes Sociales de Afecto y Apoyo para[Leer más](https://archivo.contagioradio.com/comunidad-lgbti-contra-la-homofobia/)  
[](https://archivo.contagioradio.com/bogota-marcho-contra-la-homofobia/)  

###### [Bogotá marchó contra la homofobia](https://archivo.contagioradio.com/bogota-marcho-contra-la-homofobia/)

[<time datetime="2015-05-17T22:04:59+00:00" title="2015-05-17T22:04:59+00:00">mayo 17, 2015</time>](https://archivo.contagioradio.com/2015/05/17/)En Bogotá, la comunidad LGBTI salió a las calles a manifestar de forma pacifica su rechazo a la homofobia, entre gritos y arengas y la compañia de la batucada "Toque Lésbico", la comunidad caminó desde el[Leer más](https://archivo.contagioradio.com/bogota-marcho-contra-la-homofobia/)
