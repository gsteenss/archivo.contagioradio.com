Title: Recursos de Ciencia y Tecnología no se están invirtiendo en investigación
Date: 2017-03-23 17:41
Category: Educación, Nacional
Tags: Ciencia y Tecnología, Investigación
Slug: fondo-de-ciencia-y-tecnologia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/investigación-e1503513943790.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Protocolo Foreign Affairs & Lifestyle] 

###### [27 Mar 2017] 

La auditoría que realizó la Contraloría a 33 proyectos que hacían parte de las regalías del Fondo de Ciencia y Tecnología, arrojó como resultado que **3.8 billones de pesos que debían destinarse en investigación, terminaron en la financiación de proyectos como un asadero un spa, o en manos de contratistas y políticos** que destinaron el presupuesto en otras inversiones.

Así lo afirmó el Contralor General Edgardo Maya, tras descubrir que los dineros **“no se están invirtiendo totalmente en ese fin**” y agregó que la forma en la que se realiza el proceso de destinación de los presupuestos queda en manos de instituciones que no están reconocidas por Colciencias.

Maya indicó que de 277 proyectos aprobados entre el 2012 y el 2015, que tenían un costo de 2.2 billones de pesos, **2.1 billones fueron ejecutados por las gobernaciones y el resto por Universidades y Colciencias. **Le puede interesar: ["Ser Pilo Paga ha gastado más de 350 mil millones del presupuesto de Educación"](https://archivo.contagioradio.com/ser-pilo-paga-se-llevo-373-290-470-719-del-presupuesto-de-educacion/)

“Una tercera parte del valor de la contratación lo realizan entidades o instituciones no reconocidas por Colciencias, que ejecutan contratos por cerca de **\$600.000 millones, de un total contratado a la fecha de corte del informe de la Contraloría, de \$1,8 billones**" señaló el Contralor.

En Vichada, la Contraloría encontró que se aprobó un proyecto que pretendía construir un centro de investigación y formación en energías no renovables, por un monto de **32.917 millones de pesos**, lo preocupante es que la gobernación haya contratado al consorcio Ciner, integrado **por dos empresas de construcciones que no tienen conocimiento en esta área**, proyecto finalmente desemboco en la construcción de una empresa y perdió por completo su finalidad.

Otro caso se encuentra en Córdoba en donde se auditaron 10 proyectos en Ciencia y Tecnología y se encontraron pérdidas por **30.000 millones de pesos, debido a subcontrataciones del 70% de las obligaciones** y de empresas creadas recientemente. Le puede interesar: ["Reforma al Sistema de Educación se pasaría vía Fast Track"](https://archivo.contagioradio.com/reforma-al-sistema-de-educacion-se-pasaria-via-fast-track/)

La gobernación del Cauca, implicada en estas investigaciones de la Contraloría, respondió frente a los señalamientos del Contralor que incurrió en imprecisiones, ya que los proyectos empresariales apoyados con el rubro de Ciencia y Tecnología se hicieron en el **marco del proyecto de fortalecimiento de Fortalecimiento de las capacidades de las empresas de base tecnológica para competir en un mercado global**.

Estos malos manejos es otra de las afectaciones que aquejan al fondo de investigaciones de Ciencia y Tecnología, que funciona con el 10% de las regalías que surgen de la explotación de recursos no naturales. **Hace apenas un mes, el presidente Santos informó que recortaría un billón de su presupuesto para financiar el plan 51/50** para la construcción de vías en el país. Le puede interesar: ["Con presupuesto de Ciencias y Tecnologías no se resuelve problema de vías terciarias"](https://archivo.contagioradio.com/con-presupuesto-de-ciencia-y-tecnologia-no-se-resuelve-problema-de-vias-terciarias/)

###### Reciba toda la información de Contagio Radio en [[su correo]
