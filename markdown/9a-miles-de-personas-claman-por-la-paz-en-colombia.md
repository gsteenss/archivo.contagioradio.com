Title: 9A: Miles de personas claman por la paz en Colombia
Date: 2015-04-10 17:10
Author: CtgAdm
Category: Hablemos alguito
Tags: 9 Abril colombia se mueve por la paz, 9 Abril conmemoración muerte Jorge Eliecer Gaitán, 9 de Abril día de las víctimas Colombia, Colombia se moviliza por la paz, Marcha 9 de Abril
Slug: 9a-miles-de-personas-claman-por-la-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/Bogota-manifestacion-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Contagioradio.com 

###### **Hablemos Alguito:** 

<iframe src="http://www.ivoox.com/player_ek_4337246_2_1.html?data=lZigmZeYeo6ZmKialJqJd6KmmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRfYzYxpCuxNfNsIy30NHcz8fNpYznxpDa0dvNsMruwpDd0dePsMKf0cbnj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Con motivo de la gran movilización del **9 de Abril por la paz**, en homenaje a las **víctimas del conflicto armado** y en conmemoración de la muerte de **Jorge Eliécer Gaitán**, Contagioradio realizó un extenso cubrimiento de toda la jornada. Hablemos alguito dedica un especial para **dar voz a las miles de personas** que acudieron a la llamada por la paz.

**Esperanza, paciencia, fuerza de voluntad, disciplina, trabajo duro, amor y mucha alegría son algunas de las palabras que definen los sentimientos de las personas que acudieron**.

Entrevistando nos pudimos dar cuenta de que este proceso **ya no tiene una marcha atrás**, o al menos en el sentir de una gran mayoría en el país.

Pero también indignación porque la **paz** que viene posiblemente **no sea con justicia social**, ejemplos de ello son el Plan Nacional de Desarrollo o el estancamiento de las negociaciones con las demás insurgencias, es algo que la asistencia a la marcha remarcaba y de lo que era plenamente consciente.

Organizaciones sociales de todo el país, con fuerte presencia de Marcha Patriótica y del funcionariado de la Bogotá Humana, y más de 30 autobuses de las distintas regiones se dieron cita en la capital.

Esta vez no solo fueron los activistas de las distintas organizaciones sociales, sino que también **personas que nunca habían sido afectadas por el conflicto o que nunca habían luchado por la paz se encontraban cara a cara apoyando a quienes si lo habían hecho durante muchos años**.

Así hemos querido con esta edición de Hablemos Alguito dar un aliento de esperanza y de renovación a Colombia y un sentido homenaje a quién estuvo detrás de la gran movilización por la paz del 9 de Abril en Colombia.
