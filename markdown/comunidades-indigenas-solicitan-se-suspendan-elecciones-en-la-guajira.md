Title: Comunidades indígenas solicitan se suspendan elecciones en la Guajira
Date: 2016-11-03 12:34
Category: Nacional, Política
Tags: colombia, elecciones, Guajira, indígenas, indigenas wayuu, Juan Manuel Santos, La Guajira, Presidente Santos, Suspension Elecciones, Votaciones, Wayuu
Slug: comunidades-indigenas-solicitan-se-suspendan-elecciones-en-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/votacion-e1478194341725.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas] 

###### [4 Nov de 2016] 

Luego del anuncio realizado por  la Misión de Observación Electoral – MOE-  de no realizar veeduría el próximo domingo en las elecciones atípicas a realizarse en el departamento de la Guajira, diversas organizaciones sociales se han sumado para **pedir que no se realicen estos comicios, por no existir las garantías para realizarlas. **Le puede interesar: [No hay garantías para acompañar elecciones atípicas en la Guajira: Moe](https://archivo.contagioradio.com/no-hay-garantias-para-acompanar-elecciones-guajira-moe/)

**Según la Asociación Shipia Wayúu, en la Guajira no existe garantías “ni siquiera” en lo democrático** debido a la corrupción sistémica que existe en este territorio de Colombia que está acabando con todo, así lo dejó claro Javier Rojas integrante de esta organización.

En un comunicado la Asociación manifestó que le **solicitó al presidente Juan Manuel Santos suspender las elecciones previstas para el 6 de Noviembre** “no vemos que haya garantías para que este proceso sea transparente porque los candidatos que actualmente se postularon son los mismos zorros, vestidos de ovejas” advirtió Rojas.

Las luchas de poderes, las amenazas, la corrupción, el asistencialismo, las amenazas y los homicidios a líderes que protestan en el departamento de La Guajira son algunos de los temas que según Rojas afectan de manera grave esta región y que demuestran que se sigue gobernando por los mismo y de la misma manera “por eso desde hace 6 años no hemos podido tener un gobernante porque todos los que llegan ya vienen siendo investigados, señalados bien sea por homicidio, corrupción y por todas las situaciones que pasan” y argumenta que “estas elecciones están impregnadas de esa misma situación”.

La Asociación Shipia Wayúu aseguró que ha enviado esta carta al presidente para dejar una constancia de lo que está sucediendo en el departamento de La Guajira e hicieron un **llamado al Gobierno nacional a declarar el estado de excepción, nombre un gobierno interino y pueda dejar que la Fiscalía y los entes de control investiguen y castiguen los responsables de la corrupción. **Le puede interesar: [Las propuestas de los Wayúu para superar crisis humanitaria en la Guajira](https://archivo.contagioradio.com/las-propuestas-de-los-wayuu-para-superar-crisis-humanitaria-en-la-guajira/)

Por su parte, Jorge manifestó que **diversas organizaciones han manifestado que este domingo mostrarán su rechazo y sentarán su voz de protesta absteniéndose de asistir a las urnas** para elegir el próximo gobernador de la Guajira “así diga la Registraduría que va a movilizar a sus funcionarios siempre está presente el fraude electoral y se va a llevar a cabo” concluyó. Le puede interesar: [La crisis de la Guajira en voz de las comunidades](https://archivo.contagioradio.com/la-crisis-de-la-guajira-en-voz-de-las-comunidades/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
