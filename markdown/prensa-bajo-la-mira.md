Title: Prensa bajo la mira de actividades ilegales del ejército en Colombia
Date: 2020-05-03 13:17
Author: CtgAdm
Category: Nacional
Slug: prensa-bajo-la-mira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/prensa-chuzadas-ejercito.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Son varios los episodios en los que se ha puesto al descubierto que los organismos del defensa del Estado parecen ubicar a la prensa como enemiga o como una amenaza para la seguridad. El último de ellos en el que, gracias a una investigación periodística, cerca de 130 personas entre periodistas, políticos y defensores de derechos humanos fueron víctimas

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas revelaciones dan cuenta de “carpetas secretas” perfilamientos, ubicación personal, de sus familias y de posibles fuentes, seguimientos, toma de fotografías y actos de intimidación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otras de las actividades puestas al descubierto en los primeros meses de este años fue la definición de listados de "opositores" son algunos de los ejemplos de cómo se sigue haciendo persecución a la prensa en Colombia. [Le puede interesar: Así es hacer periodismo en las regiones](https://archivo.contagioradio.com/asi-es-hacer-periodismo-en-el-bajo-cauca-antioqueno/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Por qué las FFMM ubican a la prensa como enemiga

<!-- /wp:heading -->

<!-- wp:paragraph -->

Investigaciones sobre directrices que parecen haber revivido los mal llamados “falsos positivos”, que luego generan **persecución a las fuentes periodísticas e intentos de desprestigio**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Investigaciones sobre la financiación empresarial a las FFMM y a la Fiscalía que están ligadas con judicialización de líderes que se oponen a las **operaciones empresariales, entre otros varios episodios** [**que incluyen asesinatos de periodistas.**](http://%3Cbr%20/%3E%0A%3Cblockquote%20class=%22twitter-tweet%22%3E%3Cp%20lang=%22es%22%20dir=%22ltr%22%3EQuienes%20estén%20a%20cargo%20de%20la%20administración%20de%20la%20cuenta%20%3Ca%20href=%22https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw%22%3E@COL_EJERCITO%3C/a%3E%20deben%20explicar%20los%20criterios%20bajo%20los%20cuales%20integran%20cuentas%20de%20Twitter%20a%20la%20lista%20'oposición'%3Cbr%3E%3Cbr%3EEl%20art%20219%20de%20la%20Constitución%20les%20impone%20límites%20a%20la%20intervención%20en%20debates%20políticos.%3Cbr%3ECc%3Ca%20href=%22https://twitter.com/CarlosHolmesTru?ref_src=twsrc%5Etfw%22%3E@CarlosHolmesTru%3C/a%3E%20%3Ca%20href=%22https://t.co/Vx30nq3EAz%22%3Ehttps://t.co/Vx30nq3EAz%3C/a%3E%20%3Ca%20href=%22https://t.co/0lbODuyx55%22%3Epic.twitter.com/0lbODuyx55%3C/a%3E%3C/p%3E—%20Pedro%20Vaca%20V.%20(@PVacaV)%20%3Ca%20href=%22https://twitter.com/PVacaV/status/1237395664931155968?ref_src=twsrc%5Etfw%22%3EMarch%2010,%202020%3C/a%3E%3C/blockquote%3E%20%3Cscript%20async%20src=%22https://platform.twitter.com/widgets.js%22%20charset=%22utf-8%22%3E%3C/script%3E)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La cifras de las agresiones a la prensa dan cuenta de una realidad que no se puede ocultar

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la Fundación para la Libertad de presan FLIP, “durante los últimos tres años (2017-2019) fueron amenazados 583 periodistas en Colombia mientras que en el trienio anterior (2014-2016) esa cifra había sido de 257”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo se denunció que “**hubo 137 amenazas y cuatros secuestros entre los 515 ataques que registró a la prensa colombiana en 2019.** Mencionó, a su vez, que entre 2012 y 2019 hubo 15 proyectos de ley que buscaban limitar la expresión en internet y que los últimos cuatro años hubo 1.100 personas despedidas de medios de comunicación en el país.”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Debido a ello, este 3 de Mayo y después de revelarse el último informe sobre actividades ilegales contra periodistas, más de un centenar de ellos y ellas han enviado una comunicación abierta al público pero dirigida al gobierno nacional en la que exigen respuestas a preguntas que son claras y concretas y que podrían encender luces sobre lo que hay detrás de esta persecución. [Lea tambien: ¿Por qué nos persiguen?](https://flip.org.co/index.php/es/informacion/pronunciamientos/item/2505-por-que-nos-vigilan-preguntas-publicas-al-gobierno-del-presidente-ivan-duque-y-al-ejercito-de-colombia)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las preguntas claves de los periodistas

<!-- /wp:heading -->

<!-- wp:list {"ordered":true,"type":"1"} -->

1.  ¿Quién o quiénes dieron la orden de perfilamiento y vigilancia a periodistas y medios por parte de organismos de inteligencia militar, que ataca directamente las garantías para el libre ejercicio del periodismo en el país?
2.  Uno de los objetivos de la inteligencia militar es proteger los derechos humanos, y prevenir y combatir amenazas internas o externas que se relacionen con la seguridad nacional. ¿Somos los periodistas y medios vigilados por el Estado, una amenaza para la seguridad nacional? ¿Con qué criterio justifican activar contra la prensa labores que están dispuestas para combatir la criminalidad?
3.  ¿Quiénes eran los destinatarios y/o tuvieron acceso a las carpetas con los perfilamientos y espionajes a periodistas, políticos y defensores de derechos humanos?
4.  ¿Tiene el presidente Iván Duque, el Ministerio de Defensa o alguno de sus altos funcionarios conocimiento de estas actividades de espionaje ilegal? ¿Qué acciones tomarán para garantizar que los periodistas puedan ejercer la profesión sin ser blanco de perfilamientos, espionaje y estigmatización?

<!-- /wp:list -->

<!-- wp:paragraph -->

Al mismo tiempo exigen que “se le entregue toda la información captada de manera inconstitucional a cada uno de los perfilados.”

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Información sobre acciones ilegales en Colombia son gracias a la prensa no a la fiscalía

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el mismo sentido, unos 25 congresistas señalaron en otra comunicación que, siguen siendo las actividades investigativas de la prensa las que le están dando cuenta al país sobre los crímenes que se están cometiendo desde organismos del Estado y no la fiscalía, la que debería estar encargada de ello. Igualmente, [debería judicializar e imponer sanciones a los responsables.](http://%3Cbr%20/%3E%0A%3Cblockquote%20class=%22twitter-tweet%22%3E%3Cp%20lang=%22es%22%20dir=%22ltr%22%3E1.%20Carta%20de%2025%20congresistas%20de%20oposición%20al%20Presidente%20Duque:%20“De%20nuevo%20el%20país%20ha%20sido%20informado%20a%20través%20de%20un%20medio%20de%20comunicación,%20y%20no%20como%20resultado%20de%20investigaciones%20judiciales,%20sobre%20un%20gigantesco%20aparato%20criminal%20que%20ha%20funcionado%20desde%20el%20más%20alto%20nivel%20del%20Ejército”.%20%3Ca%20href=%22https://t.co/1llIEcFC5s%22%3Epic.twitter.com/1llIEcFC5s%3C/a%3E%3C/p%3E—%20Iván%20Cepeda%20Castro%20(@IvanCepedaCast)%20%3Ca%20href=%22https://twitter.com/IvanCepedaCast/status/1256958020455739395?ref_src=twsrc%5Etfw%22%3EMay%203,%202020%3C/a%3E%3C/blockquote%3E%20%3Cscript%20async%20src=%22https://platform.twitter.com/widgets.js%22%20charset=%22utf-8%22%3E%3C/script%3E)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente el Senador Iván Cepeda afirmó "Presidente, le informamos que ante la ausencia de respuesta a estos repetidos hechos acudiremos no solo a instancias judiciales nacionales, sino que enviaremos información detallada a instancias internacionales encargadas de la protección de DDHH y al Congreso de los EEUU."

<!-- /wp:paragraph -->
