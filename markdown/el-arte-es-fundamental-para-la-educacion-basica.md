Title: El arte es fundamental para la educación básica
Date: 2016-05-04 18:26
Category: Hablemos alguito
Tags: Encuentro Internacional Arte y educación, Producciones El MImo, Teatro la mama
Slug: el-arte-es-fundamental-para-la-educacion-basica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/bab5c7c4-9486-4099-afb9-517065a1dd62-e1462403774340.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Corporación Producciones El Mimo 

##### 4 May 2016 

Desde hoy y hasta el sábado 7 de mayo se realiza en Bogotá el IV Encuentro Internacional de Arte y Educación, una iniciativa que busca fortalecer la enseñanza en los colegios de la capital por medio de la apropiación de distintas prácticas artísticas.

La propuesta de llevar a las aulas el conocimiento y la sensibilidad propios del arte, surge en el seno de la Corporación de Teatro Producciones El mimo, creada y dirigida por Julio Ferro, maestro en la pantomima, quien por más de 50 años ha trabajado por retratar las realidades del país en cada uno de sus montajes.

"Aquí se confunde el ser mimo con ser un imitador" asegura el maestro Ferro, añadiendo que el mimo se prepara con disciplina y técnica antes de pararse sobre un escenario, y lo debe hacer por "respeto al público" considerando que en muchas oportunidades quienes pasan por artistas olvidan cual es el objeto de su trabajo.

Brasil, Ecuador, México y Perú, son los países llamados a participar en las 25 actividades programadas entre talleres, ponencias, obras de teatro y espacios de interlocución en los colegios, donde se hablará y analizará el rol de la educación artística en las nuevas generaciones.

El Encuentro busca además elaborar una propuesta de políticas públicas para el sector educativo que ayude a mejorar la calidad de la formación básica en nuestro país; y posicionar el ámbito artístico en un lugar importante para la construcción de una nación sólida, con más razón en tiempos que se habla de paz.

En Hablemos Alguito, hablamos con el maestro Ferro, sobre su trayectoria, motivaciones y detalles de la programación e invitados de este IV Encuentro Internacional de Arte y Educación.

<iframe src="http://co.ivoox.com/es/player_ej_11412991_2_1.html?data=kpahk5edfZKhhpywj5mbaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncavpzc7cjavJttPjjJKYstfTqNbXxM7c0MrXb6bgjLLWz9SRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
