Title: Coronel Jorge Armando Pérez será investigado por asesinato de excombatiente Dimar Torres
Date: 2019-05-17 12:41
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Dimar Torres
Slug: coronel-jorge-armando-perez-sera-investigado-por-asesinato-de-excombatiente-dimar-torres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Dimar-Torres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

A través de pruebas recolectadas, la Fiscalía determinó que  el coronel **Jorge Armando Pérez** tuvo conocimiento del asesinato del excombatiente Dimar Torres a manos del cabo **Daniel Eduardo Gómez Robledo** el pasado 22 de abril,  sin embargo lo negó a las autoridades, por tal motivo será vinculado a las investigaciones por encubrimiento. [Lea también: (Militar implicado señala que Ejército ordenó seguir a Dimar Torres)](https://archivo.contagioradio.com/militar-implicado-senala-que-ejercito-ordeno-seguir-a-dimar-torres/)

Esta información fue otorgada por fiscal encargado Fabio Espitia quien  informó que se pudo llegar a esta conclusión a través de las pruebas recolectadas, como grabaciones y testimonios en Convención, Norte de Santander, lugar donde sucedieron los hechos.

Según la Fiscalía, de los 107 homicidios cometidos contra excombatientes han sido resueltos 52 casos logrando un avance del 48,9& del esclarecimiento.  [(Le puede interesar: Señalan a Ejército Nacional de asesinar al excombatiente Dimar Torres) ](https://archivo.contagioradio.com/excombatiente-dimar-torres-habria-sido-asesinado-por-ejercito-en-el-catatumbo/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
