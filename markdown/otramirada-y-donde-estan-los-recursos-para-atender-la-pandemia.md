Title: OtraMirada: ¿Y dónde están los recursos para atender la pandemia?
Date: 2020-07-02 20:28
Author: PracticasCR
Category: Otra Mirada, Otra Mirada, Programas
Tags: dinero para pandemia, Iván Marulanda, recursos para sistema de salud, sistema de salud
Slug: otramirada-y-donde-estan-los-recursos-para-atender-la-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/hospital-de-tadó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Hospital de Tadó

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las familias se van quedando sin recursos, las pequeñas empresas quiebran viéndose obligadas a despedir personal, los contagiados y muertos van en aumento por la COVID-19, y el gobierno a pesar de recibir cerca de 29 billones destinados para hacer frente a la emergencia, solo ha ejecutado 3.6 billones y una gran parte ha ido a los bancos, según congresista Wilson Arias. **¿Por qué el gobierno no está usando esos recursos? ¿Dónde esta ese dinero?** (Le puede interesar: [Lecciones desde el territorio la importancia de ninos y ancianos en medio de la pandemia](https://archivo.contagioradio.com/lecciones-desde-el-territorio-la-importancia-de-ninos-y-ancianos-en-medio-de-la-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón y para conocer la situación en el sector salud y saber si los recursos para la pandemia están llegando a los hospitales y centros sanitarios y personal médico, tuvimos como invitados a Olga Lucía Zuluaga de la Asociación Colombiana de Hospitales Públicos, Carolina Corcho vicepresidenta de la Federación Médica Colombiana e Iván Marulanda, senador de la República de Colombia por el partido Alianza Verde. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las personas invitadas dieron a conocer cuál es la situación de los hospitales y del sistema de salud público en cuanto a inversión, deudas y sobre todo si la situación del sistema público de salud, que atiende a la mayoría de la población rural, está listo o no para atender un futuro pico de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otra crisis generada por el mal uso de los recursos ha sido la situación con el **personal de la salud que denuncia no haber recibido ni garantías, ni protección ni salarios** y quienes ahora deben lidiar con agresiones por parte de la comunidad. Por este motivo, nuestros invitados dan un panorama de cómo el personal está afrontando cada una de estas situaciones y comparten cómo se están identificando casos de contagio en los profesionales de la salud.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, recalcan la importancia del apoyo popular a esta lucha no solo de la pandemia sino también de presión hacia el gobierno e insisten en el deber de los colombianos a respetar las medidas para evitar que el número de contagios siga en aumento pues de esta manera, cualquier sistema de salud colapsa. (Si desea escuchar el programa del 30 de junio: [Restitución de derechos para niñas y mujeres indígenas](https://www.facebook.com/contagioradio/videos/587218102159968))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aún así, la pregunta sigue en pie: ¿dónde están los otros 26 millones?

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F2386236091680320%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
