Title: ¿Cómo afecta a la paz el recorte de presupuesto al Sistema Integral de Verdad Justicia Reparación y No Repetición?
Date: 2019-07-10 13:52
Author: CtgAdm
Category: infografia
Tags: comision de la verdad, JEP, Recorte de Presupuesto, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: como-afecta-a-la-paz-el-recorte-de-presupuesto-al-sistema-integral-de-verdad-justicia-reparacion-y-no-repeticion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/declaracion-sistema-integral-verdad.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/UNIDAD-DE-BÚSQUEDA-DE-PERSONAS-DADAS-POR-DESAPARECIDAS.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Tras conocerse la eventual reducción del presupuesto para el 2020 sobre los recursos de las entidades del sistema integral de justicia transicional: **Jurisdicción Especial para la Paz (JEP), la Comisión para el Esclarecimiento de la Verdad y la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD)**, estas han alertado sobre las dificultades que existirían para continuar su trabajo territorial.

Patricia Linares, presidenta de la JEP, Francisco de Roux, presidente de la Comisión de la Verdad y Luz Marina Monzón, directora de la UBPD,  han expresado cuáles serían algunas de las más grandes dificultades que presentarían de cara a este recorte. [(Lea también: Duque recortó 30% de presupuesto al Sistema Integral de Verdad Justicia Reparación y No Repetición)](https://archivo.contagioradio.com/duque-recorto-presupuesto-sistema-integral/)

**![Sistema Integral](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/UNIDAD-DE-BÚSQUEDA-DE-PERSONAS-DADAS-POR-DESAPARECIDAS.png){.aligncenter .wp-image-70164 .size-full width="810" height="450"}**

Síguenos en Facebook:

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
