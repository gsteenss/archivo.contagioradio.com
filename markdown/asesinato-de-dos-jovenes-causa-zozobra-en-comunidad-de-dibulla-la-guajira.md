Title: Asesinato de dos jóvenes causa zozobra en comunidad de Dibulla, La Guajira
Date: 2019-06-21 17:45
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Dibulla, La Guajira
Slug: asesinato-de-dos-jovenes-causa-zozobra-en-comunidad-de-dibulla-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Panfleto-Dibulla.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Contagio Radio] 

El pasado 20 de junio fueron asesinados los jóvenes **Albeiro De Ávila Álvarez, de 14 años y Ángel Enrique Pinzón Salas, de 21 años de edad** en la vereda Jamichera, corregimiento de La Punta de los Remedios, zona rural de Dibulla, La Guajira con arma de fuego por dos hombres desconocidos. En días pasados habían circulado panfletos que advertían de un paro armado en la población.

Según el testigo que llamó a las autoridades, dos hombres llegaron a la finca y cruzaron palabras con Ángel y Albeiro, para después disparar en numerosas ocasiones, se desconocen las palabras que intercambiaron, sin embargo se sabe que después de asesinar a los jóvenes  continuaron su camino con rumbo desconocido hacia zona montañosa del sector. [(Le puede interesar: José Ceballos, líder y docente Wayúu fue asesinado en Riohacha, La Guajira)](https://archivo.contagioradio.com/jose-ceballos-lider-y-docente-wayuu-fue-asesinado-en-riohacha-la-guajira/)

Los homicidios aún no han sido atribuidos a un grupo armado en particular, tampoco se conocen detalles sobre los dos jóvenes. Por ahora, las autoridades adelantan la investigación que permita dar con la identidad de los responsables y los motivos del asesinato.

> [\#URGENTE](https://twitter.com/hashtag/URGENTE?src=hash&ref_src=twsrc%5Etfw) En Dibulla la [\#Guajira](https://twitter.com/hashtag/Guajira?src=hash&ref_src=twsrc%5Etfw) doble homicidio. Mientras autoridades y Gob. [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) niegan el paro armado que anunciaron los grupos paramilitares, estos grupos de fortalecen en los territorios a la vista de la Fuerza Pública [@COL\_EJERCITO](https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw) [@mindefensa](https://twitter.com/mindefensa?ref_src=twsrc%5Etfw)[@FiscaliaCol](https://twitter.com/FiscaliaCol?ref_src=twsrc%5Etfw) [pic.twitter.com/X5hGeNWSSx](https://t.co/X5hGeNWSSx)
>
> — Camilo Fagua (@FaguaCamilo) [21 de junio de 2019](https://twitter.com/FaguaCamilo/status/1142116511848570880?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### Han circulado panfletos amenazantes en Dibulla 

Los asesinatos de estos dos jóvenes coinciden con la aparición de panfletos en los corregimientos de Palomino y Mingueo, en Dibulla, en los que se advertía de un paro armado de 72 horas por el operativo en el que los líderes de la estructura delincuencial de **Los Pachenca: Jesús María Aguirre alias 'Chucho Mercancía' y Mario Giraldo, alias 'Mario' fueron asesinados.**

\[caption id="attachment\_69478" align="aligncenter" width="498"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Panfleto-Dibulla.jpeg){.wp-image-69478 .size-full width="498" height="681"} Foto: Archivo\[/caption\]

Pese a que el Ejército anunció que los panfletos eran falsos e incrementó las unidades en la zona a modo de prevención, el crimen contra Albeiro y Ángel ha puesto en alerta a la comunidad que cerró las tiendas y el comercio, tal como se advierte en el panfleto, pues temen por su integridad.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
