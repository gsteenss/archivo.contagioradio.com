Title: Indígenas en Chocó prohíben extracción de madera y minería en sus territorios
Date: 2017-01-06 16:18
Category: Ambiente, Nacional
Tags: Extracción minera, indígenas, Jigua, Jiguamiandó, Mineria, Resguardo Indígena, Urada
Slug: indigenas-del-choco-prohiben-extraccion-de-madera-y-mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/MINERIA_ILEGAL.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Actualidad Étnica] 

###### [6 Enero 2017] 

Las Autoridades Indígenas del Resguardo Urada - Jiguamiandó en el departamento del Chocó, han dado a conocer un comunicado en el que aseguran **han prohibido realizar trabajos de minería con retroexcavadora, así como las actividades de extracción de madera de personas ajenas a la comunidad en sus territorios.**

En el texto aseguran que si los mineros incumplen las decisiones de la comunidad serán denunciados ante las Corporaciones y la Fuerza Pública y añaden **"ningún minero tiene licencia ambiental para ejercer esta actividad". **Le puede interesar: [Minería inconsulta afecta a comunidades y al río Apartadocito en Chocó](https://archivo.contagioradio.com/mineria-inconsulta-afecta-comunidades-al-rio-apartadocito-choco/)

Además del tema de la minería, las autoridades indígenas definieron que **los compradores y vendedores de madera no podrán realizar más esta actividad en el Resguardo**, a partir del próximo 30 de Enero. Le puede interesar: [Control paramilitar y empresarial impide restitución de tierras en Curvaradó y Jiguamiandó](https://archivo.contagioradio.com/control-paramilitar-y-empresarial-impide-restitucion-de-tierras-en-curvarado-y-jiguamiando/)

Las anteriores decisiones fueron tomada luego de haber realizado una Asamblea en la que estuvieron presentes las Autoridades Indígenas de 6 comunidades indígenas de la zona.

En la misiva también dan cuenta de **la situación de orden público que están viviendo en la que aseguran hay presencia de grupos paramilitares vestidos de civil controlando la zona** "desde Mutatá a Pavarandó grande en civil, organizando y controlando en el área de influencia de los grupos Paramilitares (sic)".

Concluyen además instando a las autoridades para que "cumplan con las funciones asignadas, acorde a la Constitución". Le puede interesar: [Tribunal confirma sentencia contra 16 empresarios y paramilitares por despojo de tierras](https://archivo.contagioradio.com/tribunal-confirma-sentencia-contra-16-empresarios-y-paramilitares-por-despojo-de-tierras/)

[Indígenas del Chocó prohíben extracción de madera y minería en su resguardo](https://www.scribd.com/document/335884595/Indigenas-del-Choco-prohiben-extraccion-de-madera-y-mineria-en-su-resguardo#from_embed "View Indígenas del Chocó prohíben extracción de madera y minería en su resguardo on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_26936" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/335884595/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-CpKqwcJ66t611Qcfi3UO&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7268331990330379"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
