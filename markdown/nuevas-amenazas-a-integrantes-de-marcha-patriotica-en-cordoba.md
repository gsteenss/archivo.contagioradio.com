Title: Nuevas amenazas a integrantes de Marcha Patriótica en Córdoba
Date: 2017-02-13 13:03
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, Líderes amenazados, marcha patriotica, Paramilitares en Córdoba
Slug: nuevas-amenazas-a-integrantes-de-marcha-patriotica-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/marcha-patriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [13 Feb 2017] 

El movimiento Marcha Patriótica en Córdoba, denunció que el pasado domingo sobre las 3:40pm fue hallado un sobre con un panfleto, en la sede Montería del movimiento, en el cual **“sentencian y declaran objetivo militar a los máximos colaboradores de las Farc, todos de marcha patriótica”** puntualizando los nombres de los integrantes Arnobis Zapata, Elena Mercado, Ivan Ojeda y Andrés Chica.

Hasta el momento la única información que se tiene es que la misiva, está firmada por las Autodefensas Gaitanistas, no se tiene conocimiento de cómo llegó el sobre al lugar y la red de **derechos humanos Francisco Isaías, afirma que aún las autoridades no inician las labores investigativas.**

Los integrantes de la Marcha Patriótica amenazados, manifestaron su preocupación ante la negligencia del Estado, teniendo en cuenta que en días pasados se denunció también el asesinato de Porfirio Jaramillo, reclamante de tierras en Turbo, Antioquia y el asesinato de cuatro líderes campesinos en Córdoba, de la Asociación Campesina del Sur de Córdoba, en la vereda de El Barro. ([Le puede interesar: Denuncian cuatro asesinatos simultáneos en el sur de Córdoba](https://archivo.contagioradio.com/atencion-denuncian-cuatro-asesinatos-simultaneos-en-el-sur-de-cordoba/))

### Noticia en desarrollo... 

###### Reciba toda la información de Contagio Radio en [[su correo]
