Title: Denuncian a presidente Senado por prevaricato en Circunscripciones de Paz
Date: 2017-12-15 14:34
Category: Nacional, Paz
Tags: Circunscripciones de paz, víctimas del conflicto armado
Slug: denuncian-a-presidente-senado-por-supuesto-prevaricato-en-circunscripciones-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/cepeda.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [15 Dic 2017] 

Senadores y representantes de la Cámara instauraron una denuncia contra el presidente del Senado Efraín Cepeda, por supuesto prevaricato en el trámite de las circunscripciones de paz debido a que, según el senador Iván Cepeda, el **presidente del Senado “de manera consciente”** omitió revisar acciones que en últimas terminan generando un gran daño a la implementación de los acuerdos de paz y a las víctimas del conflicto.

La denuncia, firmada por Alirio Uribe, Ángela María Robledo, Víctor Correa y Germán Navas Talero e Ivan Cepeda busca que se haga justicia, que se tomen las sanciones contra el funcionario en cuestión y dé las órdenes necesarias para que se garantice la participación de las víctimas en las circunscripciones de paz y en el menor tiempo posible.

Según Iván Cepeda “**No es un tema que permita dilaciones, estamos hablando de un debate que tiene en vilo la participación de muchas personas en las próximas elecciones** y como se sabe ya expiro una de las fechas de los términos en lo relacionado a las inscripciones a esas candidaturas” expresó Cepeda. (Le puede interesar:["Tres opciones para la aprobación las circuncripciones de paz"](https://archivo.contagioradio.com/aprobacion-circunscripciones-de-paz/))

Además, señaló que hay todos los argumentos y un concepto del Consejo de Estado para ratificar las circunscripciones de paz, de igual forma también está el respaldo de la Corte Constitucional que dictaminó que las autoridades del Estado, incluyendo a sus funcionarios, **están obligados a cumplir con buena voluntad y diligencia todos los procedimientos que tienen que ver con la implementación de los Acuerdos**.

Finalmente, el Senador aseveró que aspira que los mecanismos que se han levantado para defender las circunscripciones tengan efectos pronto y **favorezcan los intereses de las víctimas del conflicto armado en el país, **como lo es la tutela interpuesta por la Organización de los Pueblos Indígenas de la Amazonía en Colombia (OPIAC). (Le puede interesar: ["Revivieron las circunscripciones de Paz por cuenta de una tutela de la OPIAC"](https://archivo.contagioradio.com/tutela_opiac_revive_circunscripciones_de_paz/))

<iframe id="audio_22678251" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22678251_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
