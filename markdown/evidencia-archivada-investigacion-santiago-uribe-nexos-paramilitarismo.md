Title: Pese a evidencias, archivan investigación contra Santiago Uribe por financiación de paramilitares
Date: 2020-11-24 23:12
Author: AdminContagio
Category: Actualidad, Judicial
Tags: Fiscalía General de la Nación, Paramilitarismo, Santiago uribe
Slug: evidencia-archivada-investigacion-santiago-uribe-nexos-paramilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Santiago-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @sermeca / Santiago Uribe

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Fiscalía decidió archivar la investigación que se adelantaba en contra del ganadero Santiago Uribe Vélez por haber promocionado y financiado la creación del **Bloque Suroeste de las Autodefensas Unidas de Colombia (AUC).** Cabe señalar que el ganadero se encuentra a su vez en juicio por su presunta participación en la creación del grupo paramilitar de "Los 12 Apóstoles”, proceso que se encuentra en su etapa final en el Tribunal Superior de Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que desde marzo de 2017, la Sala de Conocimiento del Tribunal Superior de Medellín compulsó copias e inició un proceso de investigación al ganadero y su nexo con la financiación de grupos paramilitares, la [Fiscalía](https://www.fiscalia.gov.co/colombia/) sostiene que no existe suficiente material probatorio para vincular a Santiago Uribe a la investigación. [(Le recomendamos leer: Juicio sobre Los 12 Apóstoles revelaría la génesis del paramilitarismo en Colombia)](https://archivo.contagioradio.com/juicio-sobre-los-12-apostoles-revelaria-la-genesis-del-paramilitarismo-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque testimonios de paramilitares aseguran que este financió a dichos grupos en los municipios de **Amagá, Titiribí y Angelópolis al sur de Antioquia,** el fiscal 128 expresó que el ente acusador no logró establecer que Uribe Vélez se "hubiese reunido con integrantes de esa organización ilegal, ni que se hubiese concertado con estos para acordar una suma de dinero a manera de contribución".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, el abogado Daniel Prado, quien representa a las víctimas en el caso de los 12 Apóstoles señaló que "el fiscal que ordenó el archivo de la investigación contra Santiago Uribe, no inspeccionó el radicado 387 por la desaparición de Edgar Monsalve, ni el de los 12 apóstoles" donde, **afirma se encuentran las pruebas que probarían que hizo parte del grupo paramilitar del suroeste Antioqueño.** [(Lea también: Inicia audiencia final contra Santiago Uribe en caso los 12 Apóstoles)](https://archivo.contagioradio.com/inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Santiago Uribe Vélez portaba un radioteléfono asignado por Efraín Ochoa jefe paramilitar de la zona, se identificaba como "R 15". Mancuso también Utilizaba un radioteléfono en esta misma frecuencia", agregó el abogado quien ha señalado que las familias de las víctimas están esperando que se respeten sus derechos y llegue pronto y de manera efectiva la administración de justicia. [(Lea también: Mancuso: "Colombia no conoce la verdad, porque no hubo ni existe interés político")](https://archivo.contagioradio.com/mancuso-colombia-no-conoce-la-verdad-porque-no-hubo-ni-existe-interes-politico/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre el caso, la Fiscalía estableció que sí existían personas que contribuyeron financieramente al funcionamiento del grupo paramilitar a través de "gente del comercio”, contribuciones forzosas y aportes voluntarios de finqueros y ganaderos de dicha región.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Etapa final del caso Santiago Uribe Vélez y Los 12 Apóstoles

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A Santiago Uribe Vélez además se le acusa de participar en el homicidio de Camilo Barrientos, conductor de un bus escalera, a cargo del mencionado grupo que poseía una ‘lista negra’ de al menos 25 nombres incluido el de Barrientos. [(Le puede interesar: Aplazamiento del juicio de Santiago Uribe es una nueva agresión a las víctimas)](https://archivo.contagioradio.com/aplazamiento-del-juicio-de-santiago-uribe-es-una-nueva-agrasion-a-las-victimas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El abogado ha mencionado en ocasiones anteriores que han pasado 25 años desde que se denunciaron los hechos por los que está investigado Santiago Uribe y después de ese tiempo, **«es justo que el país sepa si esta familia sí tiene relación con la conformación de este grupo paramilitar**, y si tienen responsabilidad en cerca de 509 crímenes en esta región.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras el juicio sobre el caso de los 12 Apóstoles se encuentra en su etapa de alegatos finales, se espera que para enero de 2021, se presenten los argumentos finales de la defensa de Uribe y posteriormente el juez dé su veredicto. [(Le recomendamos leer: Una vez más suspenden audiencia contra Santiago Uribe)](https://archivo.contagioradio.com/una-vez-mas-suspenden-audiencia-contra-santiago-uribe/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
