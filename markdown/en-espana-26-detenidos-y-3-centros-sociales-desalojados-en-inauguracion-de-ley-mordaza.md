Title: En España 38 detenidos y 3 centros sociales desalojados en inauguración de "Ley Mordaza"
Date: 2015-03-30 21:59
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: #Operación Pandora, Anarquistas presos España Operación Pandora, Ley de seguridad ciudadana España, ley mordaza
Slug: en-espana-26-detenidos-y-3-centros-sociales-desalojados-en-inauguracion-de-ley-mordaza
Status: published

###### Foto:Taringa.net 

###### **Entrevista con [Daniel Amelang], abogado de los detenidos:** 

<iframe src="http://www.ivoox.com/player_ek_4290754_2_1.html?data=lZemkpyZeI6ZmKiakp2Jd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYpsbSrcbgjKbax9HFssifwsfcycbIs4zYxpDZ0diPqMboxtPWxtTXb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En la mañana de este 30 de Marzo han sido **detenidas 14 personas** acusadas de **terrorismo** y otras **24 acusadas de resistencia** cuando se han producido las capturas, todas pertenecen a movimientos sociales y a colectivos anarquistas. Las detenciones se han producido en los propios domicilios y en centros sociales de **Madrid, Barcelona, Palencia y Granada**.

En el marco de la **Operación Pandora**, en Diciembre pasado fueron detenidos 13 activistas que actualmente se encuentran en libertad pero en proceso de juicio. Para los distintos colectivos afectados, esta operación es un paso más de la represión del estado español, en **contra de la disidencia** ante las medidas de austeridad y ante la vía parlamentaria. El lema de la campaña que exige su liberación reza: **“Ni inocentes ni culpables”.**

También el transcurso de la operación represiva han sido registrados **17 domicilios** y se han desalojado dos centros sociales en Madrid, el **CSO Quimera, y el CSO 1314**, según las personas que habitaban junto con los detenidos todas las casas han quedado totalmente destrozadas por la policía española.

Según **Daniel Amelang**, 14 de los detenidos han sido acusados de terrorismo y se ha decretado el **secreto de sumario**, además han **sido incomunicados , por tanto los abogados aún no han podido comunicarse con ellos.**

Para el abogado las detenciones responden a un intento de **criminalización** por parte del estado del **movimiento anarquista** y las fuertes **protestas contra las medidas de austeridad** impuestas por el partido conservador en el gobierno, el FMI, el BM, y la Troika.

[![idMTY5OTExMzFlNmYyOTc3-31caa437c7298229f9b23e7f9dfbf0f1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/idMTY5OTExMzFlNmYyOTc3-31caa437c7298229f9b23e7f9dfbf0f1.jpg){.aligncenter .wp-image-6594 width="598" height="399"}](https://archivo.contagioradio.com/en-espana-26-detenidos-y-3-centros-sociales-desalojados-en-inauguracion-de-ley-mordaza/idmty5otexmzflnmyyotc3-31caa437c7298229f9b23e7f9dfbf0f1/)

###### Foto:Disopress.com 

Este no ha sido el único acto represivo desde la aprobación de la “ley mordaza”, el domingo en las inmediaciones del conocido, y con gran afluencia turística, **museo del Prado**, una persona de **avanzada edad portaba una pancarta en su bicicleta explicando a los turistas que realmente España no era una democracia**. Cuando ha sido **detenido** por una pareja de agentes de la policía nacional, al **pedirle que la retirara y negarse a hacerlo.**

\[caption id="attachment\_6595" align="aligncenter" width="604"\][![Twitter Olga Rodríguez](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/detenido-anciano-paseo-prado-1024x768.jpg){.wp-image-6595 .size-large width="604" height="453"}](https://archivo.contagioradio.com/en-espana-26-detenidos-y-3-centros-sociales-desalojados-en-inauguracion-de-ley-mordaza/detenido-anciano-paseo-prado/) Twitter Olga Rodríguez\[/caption\]

En el momento se encontraba la periodista **Olga Rodríguez,** conocida por cubrir las guerras en Oriente Medio, quién ha fotografiado el hecho y al preguntar a la policía ha sido respondida por parte de los agentes: **“Somos la autoridad y punto”.**

Calificada por organizaciones de **DDHH y movimientos sociales** como grave retroceso de los derechos de libertad y expresión. El viernes 28 de Marzo fue aprobada con los votos del conservador Partido Popular, actualmente en el gobierno.

No solo **criminaliza las fuertes protestas** que está viviendo el país a causa de los recortes sociales y económicos impuestos por la Troika, sino que también **persigue al inmigrante ilegal**, respondiendo así a las directrices de la UE, con el objetivo de blindar la frontera sur.

Cientos de organizaciones sociales de derechos humanos han criticado fuertemente la ley y han exhortado al gobierno a derogarla, porque en su opinión es anticonstitucional y además ya ha sido advertido por tribunales como el de **derechos humanos de Estrasburgo o la ONU que es incompatible con las propias libertades**.

\[caption id="attachment\_6596" align="aligncenter" width="604"\][![Eldiario.es](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/ley-seguridad-ciudadana-Manel-Fontdevila_EDIIMA20150327_0176_13-724x1024.jpg){.wp-image-6596 .size-large width="604" height="854"}](https://archivo.contagioradio.com/en-espana-26-detenidos-y-3-centros-sociales-desalojados-en-inauguracion-de-ley-mordaza/ley-seguridad-ciudadana-manel-fontdevila_ediima20150327_0176_13/) Eldiario.es\[/caption\]
