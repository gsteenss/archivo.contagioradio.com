Title: Las rutas de movilización para el 21E
Date: 2020-01-20 18:37
Author: CtgAdm
Category: Movilización
Tags: Bogotá, Medellin, Paro Nacional
Slug: las-rutas-de-movilizacion-para-el-21e
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/74802282_1249874088532329_945839746624520192_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Archivo Contagio Radio/Andrés Zea {#foto-archivo-contagio-radioandrés-zea .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El próximo martes 21 de enero se desarrollará una nueva jornada de movilización en el marco del Paro Nacional y aunque el Comité del Paro ha llamado a un cacerolazo en todas las plazas del país, las y los ciudadanos han decidido manifestarse de diversass maneras en Bogotá, y otras ciudades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En [Bogotá](https://www.justiciaypazcolombia.com/nuevo-protocolo-de-la-alcaldia-mayor-para-la-movilizacion-y-la-protesta-social/) las manifestaciones iniciarán desde tempranas horas de la mañana, con la convocatoria a plantones citados a las 5 a.m. en los Portales de Transmilenio de Suba Norte y Sur. Asimismo, la Universidad Distrital, sede tecnológica, convocó a realizar una marcha desde su campus hasta el Portal Américas desde las 7 a.m.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Más tarde, sobre las 10 de la mañana, se realizarán concentraciones de artistas en el Parque Nacional, Parque de los Hippies y Avenida Jiménez. Por su parte, la Universidad Nacional ha anunciado que saldrá de la Plaza Ché rumbo al norte de la ciudad. (Le puede interesar: ["Solidaridad: la fuerza que ha vencido la violencia en el Paro Nacional"](https://archivo.contagioradio.com/solidaridad-la-fuerza-que-ha-vencido-la-violencia-en-el-paro-nacional/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, los sindicatos que integran el Comité Nacional del Paro han convocado a una movilización desde el Parque Nacional hasta la Plaza de Bolívar, en la que se realizará un cacerolazo rechazando la reciente noticia de chuzadas ilegales, y el asesinato a líderes sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Medellín se establecieron tres puntos de encuentro: Parque de los Deseos a las 9 a.m., San Juan con la 65 a la misma hora y Parque de Las Luces a las 10 a.m. Desde estos puntos se llegará al Parque El Poblado; a las 5 p.m. también se convocó a cacerolazo en la capital antioqueña.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, en Cali se desarrollarán concentraciones desde las 5 de la mañana en la Universidad del Valle, la Portada al Mar y Puerto Rellena. En horas de la tarde, en sintonía con el resto del país, las concentraciones marcharán hacía el Parque de las Banderas donde se unirán al cacerolazo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo...

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
