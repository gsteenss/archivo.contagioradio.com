Title: Eran tres los mecanismos de desaparición forzada en la cárcel modelo
Date: 2016-02-20 08:00
Category: DDHH, Nacional
Tags: Bogotá, carcel la modelo, INPEC, Paramilitarismo
Slug: eran-tres-los-mecanismos-de-desaparicion-forzada-en-la-carcel-modelo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/TERROR-EN-LA-CARCEL-MODELO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: jessijr] 

###### 20 Feb 2016 

Según testimonios de líderes paramilitares,  los métodos de desaparición forzada en la cárcel modelo fueron variados y se intercalaban de acuerdo al momento y los descubrimientos que se iban realizando por parte de la opinión pública. Más de 100 personas habrían sido las víctimas, entre internos, visitantes y personas traídas de afuera del penal, cuando el control de los patios estaba en manos de paramilitares.

Uno de los métodos que se usaron para desaparecer personas en “La Modelo” fue el de descuartizar a las personas luego de asesinarlas con armas de fuego, corto punzantes, veneno y hasta choques eléctricos. Los relatos dan cuenta que los restos de las personas eran extraídos entre las sobras de la comida que se repartía al interior de la cárcel y que tenía como destino final una granja de cerdos a las afueras de Bogotá.

Según el relato del paramilitar, cuando se descubrió este método para desaparecer los cuerpos se resolvió cambiar la manera y muchos de los restos fueron tirados por los drenajes de la cárcel. Todo ello fue posible por la complicidad del INPEC, en algunos casos, y al que ni siquiera la guardia podía ingresar a hacer los conteos respectivos en los patios controlados.

Otra de los mecanismo usados era el de descuartizar, luego triturar los restos e incorporarlos a las paredes de las nuevas construcciones, como “la panadería” que se realizaron en la época en la cárcel modelo. Los restos triturados eran ubicados entre los ladrillos que sirvieron de paredes a la construcción.

En el testimonio también se establece que varias personas habrían sido descuartizadas y luego incineradas en el horno de “la panadería”, todo ello para evitar que algunos restos aparecieran en el sistema de alcantarillado o en otros sectores de la misma cárcel o de la ciudad. También se establece que los huesos eran despojados de la carne, triturados y mezclados con el cemento con el que se realizaban las construcciones.

Para algunas organizaciones de derechos humanos que conocieron los relatos de los paramilitares, si la Fiscalía tiene la intención real de investigar estos crímenes, hallar a los responsables y sobre todo identificar a las víctimas, el trabajo tendrá que ser arduo y debería conducir, incluso, a demoliciones controladas de las instalaciones de la cárcel modelo que fueron construidas desde 1999 en adelante.
