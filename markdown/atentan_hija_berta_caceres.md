Title: Atentan contra Bertita Zúñiga, hija Berta Cáceres
Date: 2017-07-09 16:51
Category: El mundo, Voces de la Tierra
Tags: asesinato Berta Cáceres, Berta Cáceres
Slug: atentan_hija_berta_caceres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/demo-berta-caceres-2-e1499636908311.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: promosaik 

###### [9 Jul 2017] 

Los bancos FMO y Finn Fund **anunciaron que se retiran oficialmente del proyecto Hidroeléctrico Agua Zarca,** gracias a la presión ejercida por los integrantes  del COPINH, quienes han denunciado las reiteradas  violaciones a los derechos humanos de la comunidad indígena Lenca de Honduras.

Sin embargo, denuncian que dichos bancos han ignorado las recomendaciones del COPINH y al contrario siguen promoviendo un proceso que contempla “la impunidad por los asesinatos y crímenes cometidos y la invisibilización de la responsabilidad de la empresa DESA y el estado de Honduras por la violencia en la zona”, dice el comunicado de la organización”

Asimismo el **COPINH asegura que en su declaración pública el FMO y Finn Fund no se consideran responsables del asesinato de Berta Cáceres** y demás actos victimizantes contra las comunidades.

### Atentan contra Bertita Zúñiga 

Por otra parte, el COPINH denuncia también que la hija de Berta Cáceres ya sufrió un intento de asesinato en la capital hondureña. Según la información de la organización, el pasado 30 de junio camino de vuelta de la comunidad de Cancire acompañada de los miembros de la coordinación general del COPINH, fueron **atacados por 4 hombres en dos ocasiones.**

En un primer momento **“tres hombres armados con machetes, apostados al lado de un carro atravesados en el camino de la comunidad** de Cancire a San Antonio, amenazaron a las personas que estaban dentro del carro del COPINH ondeando los machetes en posición de ataque. Gracias a la habilidad del conductor no pudieron detener ni machetear el carro, sin embargo, el cuarto hombre, conductor del carro Tacoma arrojó una piedra que golpeó la parte de arriba de la ventana del conductor de COPINH.”, dice la denuncia.

En una segunda ocasión la misma camioneta persiguió el carro de COPINH **“queriendo chocarlo y arrojarlo al abismo.** Luego de esto el carro negro se puso en frente del carro de COPINH, intentando frenarlo durante 10 minutos. Afortunadamente el conductor no pudo tomar un desvío, ir por otro camino y evitar mayores incidentes”.

Tras el atentado, en una entrevista realizada por Amy Goodman, directora de Democracy Now! Zuñiga expresó, “Sabemos que ataque tiene que ver también con la conflictividad instalada nuevamente por el tema del agua, las fuentes de agua, y también el papel de la agencia USAID, que está teniendo un papel importante en el conflicto suscitado en la comunidad y que dio lugar al ataque”, y agrego **un mensaje para quienes perpetraron el ataque que pudo haberle costado la vida: “Vamos a continuar en la lucha”**.

###### Reciba toda la información de Contagio Radio en [[su correo]
