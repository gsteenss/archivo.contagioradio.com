Title: Incumplimientos del Gobierno llevarían al cierre de la cárcel 'La Tramacua'
Date: 2016-01-18 17:47
Category: DDHH, Nacional
Tags: cárcel la tramacua, cárceles colombia, presos politicos
Slug: carcel-la-tramacua-cerraria-sus-puertas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Cárcel-La-Tramacua.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:doralnewsonline 

<iframe src="http://www.ivoox.com/player_ek_10116593_2_1.html?data=kpWek5uZfZShhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncarixNra0tHNscrZz9nc1ZDIqc2fqNTPy8rWstCfzdHS2MbWaaSnhqaxw9OPpc2fxM7S1NfJb8XZjNHOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Osneider Martínez, prisionero] 

<iframe src="http://www.ivoox.com/player_ek_10116611_2_1.html?data=kpWek5uadZKhhpywj5WYaZS1lpyah5yncZOhhpywj5WRaZi3jpWah5yncarixNra0tHNscrZz9nc1ZDIqc2fqNTPy8rWstCfzdHS2MbWaaSnhqaxw9OPpc2fxM7S1NfJb8XZjNHOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gloria Silva, Cómite Solidaridad con Presos Políticos] 

######  <iframe src="http://www.ivoox.com/player_ek_10116630_2_1.html?data=kpWek5uad5Ghhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncarixNra0tHNscrZz9nc1ZDIqc2fqNTPy8rWstCfzdHS2MbWaaSnhqaxw9OPpc2fxM7S1NfJb8XZjNHOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Jairo Ramírez, Defensor DDHH] 

###### [18 Enero 2016 ]

[**Tras 1 año del fallo de tutela T282** en el que la Corte Suprema de Justicia ordena al Estado colombiano y particularmente al INPEC implementar acciones eficaces para que cesen las violaciones de derechos humanos y mejoren las condiciones de reclusión al interior de la cárcel de alta y mediana seguridad de **‘La Tramacua’ en Valledupar**, prisioneros políticos denuncian que **persisten las prácticas de tortura, el desabastecimiento de agua y la precaria atención en salud, **por lo que el centro carcelario tendría que cerrarse.]

[Osneider Martínez, prisionero político, asegura que los **20 litros suministrados a diario para cada interno son insuficientes para el consumo**, que incluye el lavado ropa y platos en cada celda; una difícil situación que ha provocado en los reclusos serios problemas de salubridad manifiestos en **fuertes virosis y hongos en la piel**. Así mismo, “el proyecto de resocialización de los reclusos ha quedado en el discurso, en la práctica **‘La Tramacua’ se ha convertido en un depósito de seres humanos donde predomina el ocio**”. ]

[**Por cada celda de 1.20 mts. por 2.5 mts. hay 3 internos,** quienes deben soportar temperaturas superiores a los 40 grados centígrados, dada la negativa del INPEC frente al uso de ventiladores. Además del hacinamiento los reclusos denuncian **represamiento de tramites por parte de la oficina jurídica e inoperancias en el área de salud**, continúan represadas las cirugías pendientes y los medicamentos esenciales no son entregados, aseveran. ]

[Las condiciones de salud en los reclusos se han empeorado, debido también a la construcción de una cantera a escasos 300 metros de la cárcel, asegura Martínez y concluye afirmando que **las denuncias por torturas han quedado en el vacío y los victimarios continúan con sus labores habituales sin recibir la sanción pertinente**.]

[Jairo Ramírez, defensor de derechos humanos integrante de la ‘Unión Sindical Obrera’, asegura que "**es claro que hay una ley federal en los Estados Unidos que ha sido tomada para implementar en Colombia**" y en ese sentido 'La Tramacua' se inscribe "en un sistema perverso que impone unos elementos conformes a la tradición norteamericana que es degradante y denigrante" con el tratamiento de reclusos.  ]

[Las **cárceles en Colombia se han convertido en “verdaderos centros de tortura, de culto a la indignidad y al atropello**” por lo que algunas de ellas se deben cerrar, agrega Ramírez e insiste en que “el Gobierno nacional, en el contexto del proceso de paz, debe repensarse seriamente el tema carcelario, porque las cárceles se han convertido en centros de castigo a la inconformidad social”.]

[Más de 9.500 personas recluidas en Colombia son profesores, campesinos, estudiantes, defensores de DDHH o líderes sindicales que han sido **víctimas de montajes judiciales por parte de la inteligencia militar**, como retaliación a sus críticas al Estado, al modelo económico, al régimen político o al carácter represivo de la fuerza pública contra la población civil, asegura Ramírez.    ]

[Gloria Silva, abogada integrante del ‘Comité de Solidaridad con Presos Políticos’, indica que en las próximas horas el Tribunal Contencioso Administrativo, **realizará la inspección para determinar los niveles de incumplimiento en la cárcel 'La Tramacua' para determinar su cierre definitivo**, en un plazo de 3 meses, en el que se tendrá que establecer un plan de acción para la ordenada evacuación de los reclusos.]

Reciba toda la información de Contagio Radio en su correo <http://bit.ly/1nvAO4u> o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio.
