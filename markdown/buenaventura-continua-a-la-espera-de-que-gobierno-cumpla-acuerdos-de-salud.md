Title: Exigen declarar emergencia sanitaria en Buenaventura
Date: 2017-08-25 17:35
Category: Movilización, Nacional
Tags: buenaventura, Salud
Slug: buenaventura-continua-a-la-espera-de-que-gobierno-cumpla-acuerdos-de-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/abandono_buenaventura_-_03-09-2014-11_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Silla Vacia] 

###### [25 Ago 2017] 

Habitantes de Buenaventura se reunieron hoy con el viceministro de Salud para buscar que se declare la emergencia sanitaria de salud y se establezcan recursos que sean destinados a la construcción de una red hospitalaria que **garantice este derecho a los ciudadanos y que dé cumplimiento a los acuerdos que se pactaron en el paro del mes de mayo**.

De acuerdo con Humberto Hurtado, integrante de la mesa coordinadora de salud, la situación en Buenaventura es cada vez más crítica, muestra de ello es la inexistencia de camas hospitalarias y que solo exista una clínica de tercer nivel para atender todo tipo de urgencias, de carácter **privada y  que según los ciudadanos se encuentra colapsada**.

A estos hechos se suman las denuncias de que hace Hurtado **por la muerte de madres gestantes** debido a la falta de una atención apropiada, el cierre de centros de atención médica y el traslado de pacientes hasta seccionales en Cali. (Le puede interesar:["¿Qué ha pasado dos meses después del paro de Buenaventura?"](https://archivo.contagioradio.com/que-ha-pasado-a-dos-meses-del-paro-civico-en-buenaventura/))

En los acuerdos del paro, el gobierno se había comprometido a destinar **85.000 mil millones de pesos que fortalecerían la red hospitalaria** y avanzar en la garantía para los ciudadanos del distrito de Buenaventura de contar con atención integral, oportuna y de calidad en la atención de los servicios de baja, media y alta de la red pública.

Frente a esta situación, los bonaverenses están exigiendo que se declare la emergencia en salud y que se cumpla con los acuerdos pactados en el último paro, “para nosotros es de suma importancia la declaratoria sanitaria de salud**, para que el gobierno que se encuentra acá, tome las acciones inmediatas de tipo económica** y de fortalecimiento distrital para contar con el bienestar de la salud” afirmó Hurtado.

De igual forma, los habitantes están pidiéndole al gobierno una **inversión de 183 mil millones de pesos para construir una ciudadela hospitalaria**. Se espera que una vez finalizada la reunión se concrete, de parte del gobierno, de donde saldrán los recursos para la salud de Buenaventura y las fechas para el inicio de los estudios en la construcción de la red hospitalaria.  (Le puede interesar:["Así va el cumplimiento por parte del gobierno de los acuerdos de Buenaventura")](https://archivo.contagioradio.com/asi-va-el-cumplimiento-del-acuerdo-del-gobierno-con-buenaventura/)

<iframe id="audio_20525345" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20525345_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
