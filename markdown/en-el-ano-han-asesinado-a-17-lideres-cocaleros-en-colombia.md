Title: En el año han asesinado a 17 líderes cocaleros en Colombia
Date: 2017-10-24 15:07
Category: Movilización, Nacional, Otra Mirada
Tags: COCCAM, Fiscal General, sustitución de cultivos ilícitos
Slug: en-el-ano-han-asesinado-a-17-lideres-cocaleros-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/hoja-de-coca-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

###### [23 Oct 2017]

Campesinos de Yarumal denunciaron que integrantes de la Fuerza Pública están realizando erradicación de cultivos ilícitos desde esta mañana, pese a que ya tienen acuerdos de sustitución de cultivos de uso ilícitos, según César Jerez, vocero de la COCCAM sumada a esta situación se encuentra el asesinato de **17 líderes cocaleros durante lo corrido de este año que cobró como última víctima a Miguel Pérez, en Taraza**.

De igual forma el pasado domingo los campesinos en Guaviare, **denunciaron que tres campesinos resultaron heridos**, mientras se realizaba el proceso de erradicación en el municipio del Retorno, actualmente las personas se encuentran en el Hospital de San José del Guaviare. (Le puede interesar: ["Asesinan a Miguel Pérez impulsor de sustitución de cultivos en Taraza, Antioquia"](https://archivo.contagioradio.com/asesinan_miguel_perez_taraza_antioquia/))

De acuerdo con César Jerez, estos hechos demuestran que el gobierno nacional no tiene intenciones de respetar el acuerdo de paz, ni la implementación del punto 4 del acuerdo de paz “los hechos dramáticos continúan y el gobierno sigue insistiendo en las medidas de fuerza, **lo que ha llevado al inicio de una movilización que será la respuesta del campesinado organizado**”.

De igual forma, Jerez manifestó que las últimas afirmaciones por parte del Fiscal Néstor Martinez, en donde señala la importancia de retomar las fumigaciones con glifosato, solo evidencian las presiones internas que buscan el regreso de las aspersiones aéreas “es lamentable que el **Fiscal general este siendo el vocero oficial de esta postura y es totalmente absurdo que el gobierno tenga en sus manos un acuerdo** que evitaría todo esto”.

Además, Jerez señaló que actualmente hay más de tres mil campesinos cocaleros en las cárceles, acusados de narcotráfico, que tendrían que estar en libertad si el punto 4 del acuerdo de paz se hubiese puesto en marcha y que la impunidad sobre los líderes asesinados continúa siendo la constante, en ese sentido afirmó que es preocupante que pueda suceder lo mismo con los **7 campesinos asesinados en Tumaco y que se establezca la responsabilidad de estos hechos**.

<iframe id="audio_21660846" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21660846_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
