Title: En ViVo Foro Internacional Hacia una ley de medios
Date: 2015-07-25 09:51
Category: Otra Mirada
Slug: en-vivo-foro-internacional-hacia-una-ley-de-medios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/hacia-una-ley1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe style="border: 0; outline: 0;" src="http://cdn.livestream.com/embed/contagioradioytv?layout=4&amp;height=340&amp;width=560&amp;autoplay=false&amp;mute=false" width="560" height="340" frameborder="0" scrolling="no"></iframe>

<div style="font-size: 11px; padding-top: 10px; text-align: center; width: 560px;">

[contagioradioytv](http://original.livestream.com/contagioradioytv?utm_source=lsplayer&utm_medium=embed&utm_campaign=footerlinks "Watch contagioradioytv") on livestream.com. [Broadcast Live Free](http://original.livestream.com/?utm_source=lsplayer&utm_medium=embed&utm_campaign=footerlinks "Broadcast Live Free")

</div>
