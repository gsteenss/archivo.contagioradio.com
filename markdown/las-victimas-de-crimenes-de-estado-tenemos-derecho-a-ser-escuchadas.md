Title: ¡Las víctimas de crímenes de Estado tenemos derecho a ser escuchadas!
Date: 2017-04-06 09:04
Category: DDHH, Nacional
Tags: Comisión de Búsqueda y esclarecimiento de la verdad, Implementación de acuerdos de paz, MOVICE, víctimas de crímenes de Estado
Slug: las-victimas-de-crimenes-de-estado-tenemos-derecho-a-ser-escuchadas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/VÍCTIMAS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOVICE] 

###### [05 Abril 2017] 

El 5 de abril de 2017, en Bogotá- Colombia, el presidente Santos firmó los decretos que daban paso a la creación de la Comisión de Búsqueda de los Desaparecidos, a la Comisión de Esclarecimiento de la Verdad, y **el grupo de 5 personas que elegirán los Magistrados de las Jurisdicción Especial de Paz.**

En un acto solemne, que contó con la intervención de diferentes víctimas del conflicto armado en el Centro de Memoria Histórica, **era visible la exclusión de voces de las víctimas de crímenes de Estado.**

Pese a no ser parte del programa oficial de esa mañana, minutos antes de la intervención del presidente Santos, tres de ellas en silencio extendieron una pancarta:"**Paz sin Crímenes de Estado"** sobre un costado del recinto en medio de una reacción de agentes de seguridad que desconcertadamente y sin poder hacer mucho tuvieron que resignarse a verla extendida.

![WhatsApp Image 2017-04-06 at 8.34.50 AM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-06-at-8.34.50-AM.jpeg){.alignnone .size-full .wp-image-38872 width="1024" height="768"}

Minutos después las víctimas de Estado se pronunciaron con un estribillo gritando:**¡Las víctimas de crímenes de Estado tenemos derecho a ser escuchadas!**

Eran las voces de los hijos e hijas, de hombres y mujeres desaparecidos, torturados y asesinados por agentes Estatales, eran las voces de la generación que reclama justicia sobre la muerte de sus padres, tías, tíos y abuelos, y que ha tenido que esperar más de 30 años, para encontrar verdad. Y finalmente, **tras escuchar la intervención del presidente, lograron expresarse con el visto bueno de Santos**. La presentadora anunció a Alejandra Gaviria.

Sus palabras, precisas y contundentes “soy Alejandra Gaviria, **hija de Francisco Gaviria y María Josefa Serna, ambos líderes de la Unión Patriótica y del Partido Comunista,** mi padre fue detenido y desaparecido, torturado y asesinado el 10 de diciembre de 1987, día internacional de los derechos humanos, claro en un país en conflicto”. Ella se refiero a las víctimas de Crímenes de Estado una voz que concentró a todo el auditorio y que fue interrumpida en tres ocasiones con muchos aplausos.

Alejandra agradeció a Santos el haberles dado la palabra señalando la importancia de que víctimas de crímenes de Estado participaran en este tipo de escenarios. “Hoy con nuestra presencia, usted nos hace un compromiso, **usted se compromete, no con una parte de la verdad, sino con toda la verdad, con la verdad para todas las víctimas**”.

Luego, se dirigió a los **militares llamándoles amigos e invitándo a permitir la desclasificación de archivos de inteligencia** y contrainteligencia. Le puede interesar: ["Víctimas de Crímenes de Estado piden que se desclasifiquen archivos de inteligencia"](https://archivo.contagioradio.com/victimas-de-crimenes-de-estado-piden-que-se-desclasifiquen-archivos-militares/)

Con voz emocionada pero en representación de toda una generación fruto de la esperanza, Gaviria entregó una pequeña urna a Juan Manuel Santos, que en esta ocasión cargaba los pensamientos, **las frases y anhelos de un país que aspira a construir las vías de la paz, a través de la participación política de todos y todas.**

Alejandra abogó para que no se volviera a repetir otro capítulo como el genocidio de la Unión Patriótica, A Luchar y expresó su exigencia para que ninguno de los integrantes de las FARC-EP **puedan iniciar un camino de construcción política y sin que les cueste la vida.**

“En esta nueva Colombia que hoy vamos a empezar a construir, con todos, con las víctimas, incluyendo las víctimas de crímenes de Estado, **tiene que haber un lugar para todos los pensamientos, para todas las posiciones y en eso estaremos juntos todos y todas para trabajar**” afirmó Alejandra Gaviria.

Compartimos intervención de Alejandra Gaviria

<iframe src="https://www.youtube.com/embed/dKZPGWduyYg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
