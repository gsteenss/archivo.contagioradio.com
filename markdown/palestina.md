Title: Ejército Israelí  "recibe órdenes de disparar a matar" contra civiles Palestinos
Date: 2018-05-15 14:07
Category: El mundo
Tags: Estados Unidos, Israel, Palestina
Slug: palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/palestina-CPI-contagioradio.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Palestina.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/palestina.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/palestina-222.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/palestina-solidaridad-colombia.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/palestina-e1465594097366.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/palestina_nota.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Palestina.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Palestina1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/palestina.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Palestina-asentamientos.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/palestina-e1487788435663.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Palestina.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Palestina-presion.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Palestina.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [15 May 2018] 

Tras la apertura de la embajada de Estados Unidos en Jerusalén, aumentó la arremetida contra las manifestaciones de los palestinos. **60 civiles desarmados fueron asesinados** por el ejército israelí que "recibe órdenes de disparar a matar".

De acuerdo con el analista político Víctor de Currea, la difícil situación de Medio Oriente data desde 1948 cuando se fundó el Estado de Israel. Ante esto, los israelíes **“durante los 70 años ha demostrado su capacidad de sangre y fuego”**. Argumentó que la ocupación del territorio palestino desde 1967 ha violado los convenios de Ginebra y las Naciones Unidas al querer ocupar Jerusalén. (Le puede interesar: ["Fuertes protestas en Palestina contra decisión de Trump sobre Jerusalén"](https://archivo.contagioradio.com/palestina-trump-jerusalen/))

Adicionalmente, Israel ha cometido otros actos de opresión contra el pueblo palestino como es la construcción del muro en Ciszjordania y la **prohibición del retorno** de los palestinos a su tierra. Esto, ha generado que “los palestinos en Gaza estén atrapados” sin la posibilidad alguna de satisfacer sus derechos básicos.

### **Soldados israelíes reciben órdenes de disparar a matar** 

Con las demostraciones de violencia de los últimos meses, donde se ha visto los ataques reiterados de la fuerza pública israelí, el analista político manifestó que “los soldados mismos **han hecho público que reciben órdenes de disparar a matar** como lo han venido haciendo los francotiradores”.

Es decir que “las 60 personas asesinadas significan **la protesta por los 70 años** de la expulsión de cientos de miles de refugiados palestinos del territorio y significa el ejemplo de lo sanguinario y genocida que es el ejército israelí”.

### **Estados Unidos ha “alcahuetiado” la violación a los DDHH** 

Frente a la política de Estados Unidos en Isarael, De Currea indicó que “ha habido una alcahuetería histórica por parte de Estados Unidos que le ha generado a Israel una gran impunidad”. Afirmó que esto ha permitido que en el mundo **“no se pueda decir nada contra Israel”** y el planeta entero siga aplaudiendo a un genocida “que chantajea a la comunidad internacional por ser víctimas de un genocidio”.

Sin embargo y pese a la poca actuación de la comunidad internacional y de organizaciones como las Naciones Unidas, “los palestinos tienen **el arma demográfica a su favor** para poder sobrevivir”. De Currea explicó que “hoy en día es impensable que van a matar a los millones de palestinos que hay simplemente porque Israel así lo quiera”.

Por esto, recordó que los palestinos que viven en territorio israelí y en las zonas ocupadas, representan **un problema para Israel** quien se ha negado a aceptar un Estado laico “en el cual los palestinos puedan votar en igualdad de condiciones”. Por esto, el analista enfatizó que los israelíes “le tienen miedo a la democracia, es uno de sus mayores enemigos”.

<iframe id="audio_26011069" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26011069_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
