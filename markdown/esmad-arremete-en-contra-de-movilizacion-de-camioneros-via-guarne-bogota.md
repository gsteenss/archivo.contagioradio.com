Title: ESMAD arremete en contra de movilización de Camioneros vía Guarne - Bogotá
Date: 2016-07-07 14:49
Category: Movilización, Nacional
Tags: Paro Nacional Agrario
Slug: esmad-arremete-en-contra-de-movilizacion-de-camioneros-via-guarne-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/destinoseguro.net_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: viajeseguro.net] 

###### [7 jul 2016]

Camioneros que hacían parte de la movilización que se estaba desarrollando entre el tramo Bogotá - Gaurne en el marco del paro, denunciaron que fueron **atacados por disponibles del ESMAD** con gases lacrimógenos. Estos hechos se desarrollan en medio de las tensiones entre el Gobierno y los camioneros, debido a que aún no llegan a un acuerdo.

Durante lo corrido de esta movilización, también se han presentado [enfrentamientos en Boyacá, Buenaventura](https://archivo.contagioradio.com/tras-25-dias-de-paro-camionero-gobierno-continua-dilatando-la-negociacion/) y Medellín, de acuerdo con Jorge García, presidente de la Federación Nacional de Camioneros, "hay un parte de **90% de la flota transportadora detenida,** y hemos dicho que si nosotros perdemos este paro sabemos que no tendremos derecho el año entrante a que nos den carga o trabajo; **no hay nada a futuro que nos garantice la estabilidad del sector**".

[Hasta el momento la mesa de diálogos no ha logrado llegar a ningún acuerdo](https://archivo.contagioradio.com/tras-desplante-del-gobierno-continua-paro-camionero/), pues el gremio de los camioneros considera que las propuestas por parte del gobierno **no tienen garantías de cumplimiento** y no subsanan las problemáticas de fondo que presentan, como lo es la chatarrización, la regulación de parque automotor, y costos.

Frente al aumento de precios de los alimentos en el país, García aseguro que los carros transportadores de comida están funcionando a pérdida, pagando consumos caros, y en el último año se ha **elevado en un 40%** el valor de los repuestos de los automóviles. Por ende, continuarán con los vehículos detenidos hasta no tener una propuesta por parte del gobierno conforme a sus necesidades.

<iframe src="http://co.ivoox.com/es/player_ej_12153603_2_1.html?data=kpeel5iadJShhpywj5aWaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncavj08zSjazFtsSZpJiSo6nFaZO3jKvSxsrWpcTd0NOYsMbHrdDiwtGYxsqPh8LhytTbx9fTt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
