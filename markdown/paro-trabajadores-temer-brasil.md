Title: Trabajadores en paro contra medidas de Temer en Brasil
Date: 2016-11-11 15:26
Category: El mundo, Otra Mirada
Tags: Brasil, Dilma Rousseff, Paro Nacional, Temer
Slug: paro-trabajadores-temer-brasil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Cw_XLpLWgAAGbXk.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: CUT Brasil 

##### 11 Nov 2016 

Varias carreteras de Brasil amanecieron este viernes bloqueadas y ciudades sin servicio de transporte público  por cuenta del **paro nacional de trabajadores decretado por ocho centrales obreras del pais**, en rechazo a las medidas regresivas en materia laboral implementadas por el gobierno del Presidente (e) Michel Temer.

Entre las políticas de Temer, los trabajadores se oponen a la **Propuesta de** **Enmienda a la Constitución (PEC) 55/16**, por la cual **se establece un techo para los gastos públicos**, la reforma sobre la edad mínima para jubilarse, la flexibilización del combate al trabajo esclavo, y la alteración del tiempo mínimo de contribución para que trabajadores y trabajadoras puedan recibir beneficios tales como auxilio para enfermedades y maternidad pagada después de desvincularse de la Seguridad Social.

Sergio Nobre, secretario general de la Central Única de Trabajadores CUT, una de las organizaciones movilizadas, asegura que el “Gobierno golpista” de **Temer busca disminuir los salarios, privatizar empresas y servicios y entregar las riquezas del país a las multinacionales**, asi como mermar las inversiones en áreas públicas esenciales como la educación y salud.

El líder sindical afirma que las decisiones gubernamentales hacen parte de una política "**regresiva y autoritaria de ajuste fiscal**", que hace parte de los objetivos que se tenian al destituir a la presidenta Dilma Rousseff, y que perjudica a los trabajadores y a los sectores más pobres de la sociedad. Le puede interesar:[Rousseff disminuyó desigualdad, Temer traerá de vuelta medidas neoliberales](https://archivo.contagioradio.com/brasil-rousseff-disminuyo-desigualdad-temer-traera-de-vuelta-medidas-neoliberales/).

A las manifestaciones **se han sumado estudiantes de 170 universidades del pais** que se oponen a la PEC-55, propuesta que también afectaría a quienes cursan enseñanza media al poner fin a la obligatoriedad de cursar disciplinas como educación física, artística, filosofía y sociología, decisiones que además de no ser consultadas, a**fectan la calidad de la educación públic**a.

Las movilizaciones se dan un día después de del inicio de una campaña pro-democracia y en favor de Lula da Silva, los promotores del paro estiman realizar **nuevas manifestaciones el próximo 25 de noviembre**.
