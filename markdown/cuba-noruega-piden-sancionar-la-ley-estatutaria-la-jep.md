Title: Cuba y Noruega piden a Iván Duque sancionar la Ley Estatutaria de la JEP
Date: 2019-03-10 13:42
Author: ContagioRadio
Category: Paz, Política
Tags: acuerdo de paz, jurisdicción especial para la paz
Slug: cuba-noruega-piden-sancionar-la-ley-estatutaria-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Duque-y-sus-objeciones-a-la-JEP-e1552341356510-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [10 Mar 2019] 

En una carta abierta, delegados de Cuba y Noruega, los países garantes del proceso de paz, instaron al Presidente Iván Duque sancionar la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP) porque "constituiría un paso esencial en la preservación y continuación del Acuerdo Final alcanzado". Al contrario, la comunicación afirma que las consecuencias serían "imprescindibles" para las víctimas y los reincorporados.

Asimismo, los delegados Dag Nagoda, de Noruega, e Iván Mora Godoy, de Cuba, aseguran que el sistema de la justicia transicional es la "columna vertebre" sobre la cual se construyeron los Acuerdos de Paz y al objetar la Ley Estaturia quedaría "en vilo" la implementación de los aspectos más fundamentales de los acuerdos pactados. (Le puede interesar: "S[i Duque objeta la Ley Estatutaria vulneraría derechos de víctimas: organizaciones](https://archivo.contagioradio.com/razones-para-aprobar-la-ley-estatutaria/)")

La carta agrega que varios representantes de organismos internacionales como las Naciones Unidas y la Corte Penal Internacional ya han expresado sus preocupaciones sobre la posible objeción que se espera del mandataria de algunos artículos de la Ley Estaturia. Según los delegados, esta decisión significaría "una involución del Acuerdo Final de Paz", lo cual crearía un precedente negativo en otros procesos de paz en el mundo. (Le puede interesar: "[Corte Penal Internacional manifiesta su respaldo a la JEP](https://archivo.contagioradio.com/corte-penal-internacional-respalda-la-justicia-transicional-mediante-ataques/)")

El Presidente tiene plazo hasta el 11 de marzo para decidir si sancionar o no la Ley que brinde a la justicia transicional su marco normativo. Sin embargo, se ha reportado que el mandatario planea objetar la ley por inconveniencia este lunes ya que por inconstitucionalidad no es posible dado la revisión que completó la Corte Constitucional en el 2018. (Le puede interesar: "[Las falacias del Fiscal Néstor Humberto Martínez para objetar la JEP](https://archivo.contagioradio.com/falacias-para-objetar-jep/)")

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
