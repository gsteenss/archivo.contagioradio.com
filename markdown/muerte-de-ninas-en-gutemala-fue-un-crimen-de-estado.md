Title: Muerte de niñas en Guatemala "fue un crímen de Estado"
Date: 2017-03-10 16:52
Category: Mujer, Nacional
Tags: crímenes de estado, feminicidio, Niñas de Guatemala
Slug: muerte-de-ninas-en-gutemala-fue-un-crimen-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cdn4.uvnimg.com_.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Univisión] 

###### [10 Mar 2017] 

En el marco de la conmemoración del día de la mujer, en Guatemala, 35 niñas que protestaban buscando visibilizar los abusos sexuales que sufrían al interior del Hogar Virgen de la Asunción, murieron en medio de un incendio. Fuentes locales aseguraron que **hubo negligencia por parte del personal del refugio, pues no llamaron al personal de bomberos ni “hicieron algo por detener el fuego”**.

En el establecimiento con capacidad para 400 personas, residían 748 niños, niñas y adolescentes, en anteriores ocasiones algunas organizaciones defensoras de derechos de la niñez habían denunciado la situación de hacinamiento, l**as condiciones indignas a las que sometían a estos menores y los abusos sexuales por parte del personal.**

Según medios de prensa guatemalteca, el fuego fue provocado por las niñas quienes incendiaron colchones en forma de protesta. El defensor de la Procuraduría de Derechos Humanos del Estado guatemalteco, también confirmó la información. ([Le puede interesar: 92 feminicidios en América Latina han sido registrados en menos de 2 meses](https://archivo.contagioradio.com/92-feminicidios-en-america-latina-han-sido-registrados-en-menos-de-2-meses/))

Al respecto, Lorena Cabnal integrante de La Red de Sanadoras Ancestrales del Feminismo Comunitario de Guatemala, señaló que desde el 8 de Marzo, las mujeres guatemaltecas han realizado movilizaciones y plantones frente a las instituciones estatales, "exigimos la verdad, **exigimos que el Estado reconozca su responsabilidad, no las mató el fuego, las mató el Estado”.**

Organizaciones defensoras de derechos humanos a nivel internacional, también han repudiado el lamentable suceso y han aseverado que el Gobierno de Guatemala debe pronunciarse al respecto y hacer justicia con el caso, afirmaron que se trató de un crimen de Estado, debido no sólo a las negligencias de los funcionarios públicos del refugio, sino también **en general a las políticas públicas orientadas a la niñez, la juventud y las mujeres.**

La Red de Sanadoras, también denunció que el Gobierno no ha creado políticas que “resguarden la vida y los derechos de la niñez” no hay oportunidades de vida digna para la juventud y la niñez en Guatemala, puntualiza Cabnal, y asegura que estas niñas, niños y jóvenes se ven constantemente re victimizados, pues muchos de ellos y ellas **“antes de estar en los refugios, en sus casas ya eran víctimas de abusos, reclutamientos de maras y otros vejámenes”.**

Por último, Cabnal señaló que mujeres de todo el mundo se han unido solidariamente al clamor de justicia y **“han propuesto realizar plantones frente a las embajadas de Guatemala en todos los países del mundo**, para gritar a una sola voz y exigir que este caso no quede en la impunidad”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
