Title: 6 de marzo, Día por la Dignidad de las víctimas de Crímenes de Estado
Date: 2015-03-06 15:37
Author: CtgAdm
Category: DDHH, Nacional
Tags: crímenes de estado, Dignidad de las víctimas, MOVICE
Slug: 6-de-marzo-dia-por-la-dignidad-de-las-victimas-de-crimenes-de-estado
Status: published

###### Foto: MOVICE 

Este 6 de marzo, las víctimas de crímenes de Estado se reúnen en distintas ciudades del país conmemorando 10 años del nacimiento del MOVICE. Manifestaciones artísticas, culturales y galerías de la memoria con los rostros de las victimas van a tener lugar hoy en las distintas concentraciones contra la impunidad y por la solidaridad a realizarse en todo el país.

En el Valle del Cauca la concentración se llevará a cabo en la tradicional Plaza Caicedo de Cali desde las 9:00 de la mañana, protagonizada por la elevación de cometas como símbolo del movimiento y en memoria de los y las familiares de los presentes asesinados y asesinadas por el estado colombiano.

En Medellín, Antioquia, se realizará un plantón de 4:00 a 6:00 de la tarde en la Plaza Botero, celebrando los diez años del movimiento y recordando los años de lucha anteriores a la existencia del mismo.

El capítulo Santander realizará un plantón al que llegará la marcha de las mujeres sindicalistas del departamento en el parque Santander a las 9 de la mañana, frente a la Iglesia de la Sagrada Familia, donde además de posicionar la tradicional galería de la memoria, se alzaran unos pasacalles con los rostros de las mujeres que han sido víctimas del conflicto en conmemoración al próximo día internacional de la mujer.

En Bogotá se realizan tanto un plantón desde las 11:00 de la mañana en el parque Santander, como una rueda de prensa en la sede del Colectivo de Abogados José Alvear Restrepo, donde se presentará un balance de los diez años de trabajo del Movimiento.

Desde el 2005, Mas de más de 200 organizaciones volcadas a la defensa de los derechos humanos y los derechos de las víctimas de crímenes cometidos por el estado colombiano han juntando sus luchas y se han ido congregando en la plataforma del Movice, buscando visibilizar de esta manera que la violación sistemática a los derechos humanos es cometida por el Estado en todo el territorio nacional, y que además de la constante repetición de estas conductas por parte del establecimiento, se sigue castigando a las víctimas con la impunidad y la falta de garantías.
