Title: Ponencia de JEP se presentaría este fin de semana
Date: 2017-09-21 14:45
Category: Entrevistas
Tags: Fast Track, Jurisdicción Especial de Paz
Slug: ponencia-de-jep-se-presentaria-este-fin-de-semana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/CONGRESO-e1483381822139.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón] 

###### [21 Sept 2017] 

**La ponencia de la ley estatutaria de la Jurisdicción Especial de Paz se presentaría este fin de semana**, de acuerdo con el senador Horacio Serpa del Partido Liberal, ponente para este proyecto de ley. Sin embargo, aunque el proyecto presentado por el gobierno se está estudiando, también se han realizado consultas a los partidos para lograr una ponencia con amplio respaldo que sea aprobada en el marco del Fast Track.

Entre tanto las víctimas del conflicto armado ya han hecho reparos para este acto legislativo, manifestando las posibles modificaciones que se habrían hecho a la cadena de mando y la responsabilidad de terceros, **"hemos escuchado a todos lo sectores"** afirma Serpa, sin embargo no se puede referir al tema hasta que la ponencia sea presentada. (Le puede interesar: ["Víctimas se entierran para exigir búsqueda de desaparecidos"](https://archivo.contagioradio.com/victimas-se-enterraran-en-bogota-para-exigir-la-busqueda-de-los-desaparecidos/))

Según Serpa, se han escuchado diferentes representaciones de las víctimas “**queremos que todo corresponda a lo que necesita el país en materia de justicia transicional**, y ello desde luego incluye el tema de víctimas”, afirmó el senador y agregó que ya hubo una reforma constitucional que creo la Justicia Especial y que la labor que han asumido es desarrollar esos insumos.

Referente a las denuncias que han hecho otros congresistas como Iván Cepeda o Alirio Uribe, de las intenciones de Cambio Radical y el Centro Democrático, por frenar los debates del Fast Track, Serpa aseguró que se han tenido reuniones con integrantes de diferentes partidos. Sin embargo, **manifestó que sabe que será un debate intenso y que lo que finalmente se espera es la aprobación de la ley estatutaria**.

Sobre la presión del tiempo y que finalice el Fast Track, Serpa señaló que considera que este debate alcanzará a entrar dentro de los tiempos establecidos para la rápida implementación. (Le puede interesar: ["Acuerdos de paz podrían perder blindaje jurídico"](https://archivo.contagioradio.com/acuerdos-de-paz-podrian-perder-su-blindaje-juridico/))

**Los reparos a la Jurisdicción Especial de Paz**

Congresistas han expresado que el proyecto de ley sobre la JEP tendría varios **“micos” que disminuirían las sanciones a terceros por su responsabilidad** en el conflicto armado y la cadena de mando al interior de las Fuerzas Militares. (Le puede interesar: ["Modificaciones en la JEP violarían el Acuerdo de paz"](https://archivo.contagioradio.com/jep-debe-garantizar-derecho-a-la-justicia-para-las-victimas-ivan-cepeda/))

Iván Cepeda, senador por el Polo Democrático, ha manifestado en reiteradas ocasiones que “es imprescindible que la Jurisdicción Especial de Paz obedezca al espíritu con el que fue creada, y eso es una jurisdicción al servicio de las víctimas, al servicio de la verdad y al servicio de la justicia en Colombia”.

<iframe id="audio_21021425" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21021425_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
