Title: En operativos de erradicación en Putumayo deja dos campesinos muertos y tres heridos
Date: 2020-07-06 09:10
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Erradicación Forzada, Putumayo
Slug: en-operativos-de-erradicacion-en-putumayo-deja-dos-campesinos-muertos-y-tres-heridos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Coca-Bolivia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Erradicación

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La [Asociación Minga](https://twitter.com/asociacionminga)denunció el pasado 3 de julio un nuevo caso de exceso de la fuerza por parte de la Unidad Antinarcóticos de la Policía que en medio de procesos de erradicación de cultivos de uso ilícito en la vereda La Caucasia en el corregimiento de Teteyé en Puerto Asís acudió al uso de la violencia contra campesinos de la zona, causando la muerte de dos personas e hiriendo a tres más, a dos con armas de fuego.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la comunidad, la Unidad Antinarcóticos disparó contra la población que reclamaba el cumplimiento de los acuerdos del Plan Nacional de Sustitución de Cultivos de Uso Ilícito (PNIS) y el respeto al diálogo y acuerdos a los que se habían llegado previamente con las autoridades, en medio del suceso informa Minga fueron atacados los campesinos, dos de los cuales perdieron la vida a causa del accionar y otros más resultaron heridos. [(Le recomendamos leer: En Putumayo no cesa la violencia contra la comunidad)](https://archivo.contagioradio.com/en-putumayo-no-cesa-la-violencia-contra-la-comunidad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A pesar de no haber ingresado al Programa de Sustitución, tras la firma del Acuerdo de Paz las familias cocaleras de esta vereda se comprometieron a hacer procesos de sustitución concertada, cabe resaltar que **Caucasia, hace parte de la Mesa de Diálogo para la Sustitución Autónoma y Voluntaria de Cultivos de Coca** que fue establecida entre las comunidades y el Ministerio del Interior en agosto de 2019 y que en las últimas semanas viene dialogando con representantes del Gobierno y autoridades locales precisamente sobre las violaciones a lo pactado. [(Le recomendamos leer: Ejercito y narcotráfico: verdugos de la sustitución de la Coca)](https://archivo.contagioradio.com/ejercito-y-narcotrafico-verdugos-de-la-sustitucion-de-la-coca/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/asociacionminga/status/1279461436947742720","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/asociacionminga/status/1279461436947742720

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, Minga denunció que los operativos no cuentan con los protocolos de bioseguridad que se requieren a nivel nacional en medio de la pandemia de la Covid-19, poniendo en riesgo a los habitantes y en general a las comunidades del sector. [(Lea también: Ejército pone en riesgo a población de Piamonte, Cauca)](https://archivo.contagioradio.com/ejercito-pone-en-riesgo-a-poblacion-de-piamonte-cauca/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El Gobierno aún piensa que la erradicación forzada es la clave

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

De acuerdo con un adelanto del informe anual de la Oficina de las Naciones Unidas contra la Droga y el Delito (UNODC por sus siglas en inglés), el área total de cultivos de coca disminuyó 9% en 2019 a 154.000 hectáreas , a propósito de este dato, de las 101.000 hectáreas de coca erradicadas en 2019, en sólo unas 7.000 hubo sustitución voluntaria, pese a ello, la producción real de cocaína aumentó. [](https://archivo.contagioradio.com/ascamcat-denuncia-posible-ejecucion-extrajudicial-de-campesino-en-teorama/)[(Lea también: Pactos de sustitución deben cumplirse, glifosato no debe ser prevalente)](https://archivo.contagioradio.com/pactos-de-sustitucion-deben-cumplirse-glifosato-no-debe-ser-prevalente/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tal estadística, es un reflejo de la prioridad que ha dado el Gobierno a la erradicación forzada, en departamentos como Norte de Santander, Cauca, Nariño y Putumayo, dejando de lado lo pactado en el Acuerdo de Paz y el compromiso adquirido con **al menos 100.000 familias que se acogieron al PNIS** y que demostraron el éxito de la sustitución voluntaria pues sólo el 0,4% volvieron a plantar coca después de eliminar sus cultivos. Pese a ello el accionar de la erradicación forzada ha ido en aumento en medio de la pandemia. [(Lea también: Persecución y desplazamiento campesino: el resultado de incumplimiento del Gobierno en Guaviare)](https://archivo.contagioradio.com/persecucion-y-desplazamiento-campesino-el-resultado-de-incumplimiento-del-gobierno-en-guaviare/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
