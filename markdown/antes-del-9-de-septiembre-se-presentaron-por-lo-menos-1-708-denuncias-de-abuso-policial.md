Title: Antes del 9 de septiembre se presentaron por lo menos 1.708 denuncias de abuso policial
Date: 2020-09-16 17:57
Author: AdminContagio
Category: Actualidad, Judicial
Tags: Abuso policial, CAI, colombia, Javier Ordóñez, Medicina Legal
Slug: antes-del-9-de-septiembre-se-presentaron-por-lo-menos-1-708-denuncias-de-abuso-policial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/violencia-policial-ESMAD.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Carlos Zea/ Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En medio del debate de control político que se adelanta en la Cámara de Representantes en contra de los **abusos policía**, la representante a la Cámara por Bogotá, **Katerin Miranda denunció que ante el Instituto de Medicina Legal se han presentado por lo menos 1.708 denuncias de abusos por parte de policías** en contra de ciudadanos en Bogotá.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CamaraColombia/status/1306342856957755393","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CamaraColombia/status/1306342856957755393

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

De estas denuncias de abuso policial, **1.479 fueron cometidas contra personas entre los 15 y los 35 años,** lo que para la congresista representa, *"una agresión en contra de una generación"*. Y aseguró, que en un 80% de los casos, las heridas que les propinaron a las víctimas **fueron tan graves que Medicina Legal tuvo que decretar incapacidad médica**. Estas cifras contrastan con las entregadas por la Policía en las que solamente se han reportado 3[7 denuncias](https://archivo.contagioradio.com/plataformas-de-dd-hh-solicitan-a-la-onu-permanencia-de-mision-de-verificacion/) por abuso policial.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para la congresista, estas cifras indican que no son solamente unas *[“manzanas podridas”](https://www.justiciaypazcolombia.com/mucho-mas-que-manzanas-podridas-hay-que-reformar-la-inteligencia-militar-colombiana/)*, como se ha señalado; sino que obedecen a una situación estructural que tiene que ver con la formación que reciben las personas al ingresar a la institución, junto a la impunidad que reina, pues **por lo menos el 95% de las denuncias no prosperan y no provocan ningún tipo de reacción al interior de la policía.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los resultados de Medicina Legal y el testimonio de los amigos de Javier Ordóñez, evidencian que en el CAI hubo tortura

<!-- /wp:heading -->

<!-- wp:paragraph -->

Uno de los amigos de Javier Ordóñez, quien lo acompañó desde el momento de su captura hasta su muerte en el hospital, **relató los horrorosos hechos de tortura que tuvo que sufrir tanto en la calle como en el CAI del barrio [Villa Luz](https://archivo.contagioradio.com/en-dos-anos-del-gobierno-duque-se-ha-derrumbado-institucionalidad/).**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte en el informe de defunción, Medicina Legal indicó que **Javier murió porque se le propinaron 13 descargas de arma taser**, **40 golpes en diferentes partes del cuerpo**, que le destruyeron el hígado, **y 9 fracturas craneales** registradas al momento del ingresara al hospital; lo que indicaría que ***"hay evidencias suficientes para denunciar muerte por tortura"* asegura Katherin Miranda.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

La comunidad LGBTI, otra de las víctimas del abuso policial
-----------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por otra parte el representante Mauricio Toro aseguró que en los últimos **5 años se ha registrado que 515 personas LGBTI,** han sido víctimas de violencia policial, dentro de las cuales se han presentado 1**6 casos de torturas, 320 agresiones físicas y 3 asesinatos.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CamaraColombia/status/1306346182181040129","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CamaraColombia/status/1306346182181040129

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

*"3 de cada 100 bogotanos confía en la policía"*, aseguró el congresista acotando a un estudio de la Universidad de los Andes, agregando que cada vez es menor el nivel de legitimidad que tiene el accionar de la policía. ([La violencia policial es la nueva pandemia: Casos a nivel mundial](https://archivo.contagioradio.com/la-violencia-policial-es-la-nueva-pandemia-casos-a-nivel-mundial/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PizarroMariaJo/status/1306358884035166208","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PizarroMariaJo/status/1306358884035166208

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Al mismo tiempo la representante a la Cámara, María José Pizarro, evidencio las cifras de los costos promedio que dispone la nación en un agente del E**SMAD, el cual oscila en los 30 millones** de pesos al año , en comparación a un **estudiante de universidad pública**, en este caso de la Universidad Nacional, en donde los gastos no superan los **10 millones** de pesos. ***"Un agente de ESMAD cuesta 3 veces lo que cuesta un estudiante universitario"*,** enfatizó la representante.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este llamado desde el congreso indica que es importante que el gobierno responda por los avances en las investigaciones, **pero que también se impulsen las reformas estructurales que son necesarias para frenar la violencia policial.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
