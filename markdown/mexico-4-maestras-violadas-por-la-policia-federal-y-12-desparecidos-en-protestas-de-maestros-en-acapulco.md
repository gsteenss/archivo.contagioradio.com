Title: México: 4 maestras violadas por la policía federal y 12 desparecidos en protestas de maestros en Acapulco
Date: 2015-02-27 18:01
Author: CtgAdm
Category: El mundo, Entrevistas
Tags: 12 desaparecidos protestas Acapulco, 4 maestras violadas por policía federal México, mexico, Protesta magisterio México, Represión policial Acapulco
Slug: mexico-4-maestras-violadas-por-la-policia-federal-y-12-desparecidos-en-protestas-de-maestros-en-acapulco
Status: published

###### Foto: Twitter Radio Regeneración 

##### **Entrevista con [Gilberto Ramirez]:** 

<iframe src="http://www.ivoox.com/player_ek_4143799_2_1.html?data=lZahlZydfY6ZmKiak5yJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiReIzhwsrg1tfFt4zqytTZw8nFt4zZz5C6h6iXaaKt2c7Q0ZDUs9Of0dTZy8iJh5SZoqnOjcvJqMbmwtGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Después de la brutal represión contra la marcha de maestros en Acapulco, **4 maestras fueron violadas por la policía federal, así como otros 12 profesores se encuentran desparecidos.** En la **jornada de conmemoración de los 5 meses de desaparición forzada de los 43 estudiantes** de la Normal Rural de Ayotzinapa se ha vuelto a producir la **represión de la policía** contra las marchas dejando decenas de heridos y detenidos.

Según el representante de la Coordinadora Estatal de Trabajadores de la Educación de Guerrero (CETEG), las 4 maestras violadas tenían miedo de denunciar el suceso, una de ellas se encuentra hospitalizada de gravedad.

En total fueron **72 desaparecidos que fueron encontrados en distintos hospitales y en comisarías de policía, a dí de hoy 12 continúan desparecidos, a esto hay que sumarle el asesinato a golpes del profesor jubilado Claudio Castillo Peña, de 65 años. **De todo lo sucedido existen pruebas a modo de videos y fotos que pueden servir para judicializar a los responsables.

Todo lo sucedido ha transcurrido mientras la **ONU presentaba un informe que confirmaba que la tortura en México es sistemática y generalizada y que además se produce en un clima de total impunidad.**

El informe elaborado entre el 21 de Abril y el 2 de Mayo del año pasado concluye que las torturas se producen en las detenciones y suelen ir acompañadas de desaparición forzada. También explica que entre 2005 y 2013 solo ha habido cinco sentencias condenatorias por torturas.

Para poder comprender las causas de la represión estatal entrevistamos a Gilberto López y Rivas, analista político mexicano, quién nos relata que **el estado está defendiendo los intereses de los narcos, de la minería, de la privatización del agua y en general de las multinacionales en los distintos estados**. Guerrero es un estado codiciado por las mineras y de gran resistencia de grupos sociales, como los profesores o indígenas.

Actualmente, en el estado de Tamaulipas el 55% del territorio está concesionado a mineras y en todo el país el 33%, también está vendido a las empresas mineras. Lo que equivale a una **dinámica de despojo y de re-colonización por parte de intereses privados en manos de extranjeros** con consecuencias directas sobre la población.

Rivas explica que el Estado de **México utiliza a la delincuencia como sus propios agentes,** un Estado que responde a los intereses del capital, un ejemplo de ello es la medida que permite a los extranjeros utilizar armas en el territorio, a pesar de que la constitución no lo permite**.".. El estado está fuera de las leyes, de tal manera ya no representa al pueblo mexicano.."**. Por último nos explica el modelo paramilitar de represión  y sus conexiones con Colombia.
