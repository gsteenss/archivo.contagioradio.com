Title: El derecho a organizarnos
Date: 2015-06-25 07:43
Category: Opinion, superandianda
Tags: Antonio Navarro Wolff, Claudia López, Movilización para la paz, paz, superandianda
Slug: el-derechos-a-organizarnos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Captura-de-pantalla-2015-06-25-a-las-7.35.01.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

###### Foto: tomada de internet 

#### [Por **[Superandianda ](https://archivo.contagioradio.com/superandianda/) - [~~@~~Superandianda](https://twitter.com/Superandianda) 

###### [25 Jun 2015] 

Creo que la principal falta colombiana es la ignorancia de sus derechos, lo que los hace violables y manipulables al antojo del estado y los medios de comunicación.

La falta de organización de los colombianos como sociedad no ha permitido abrir el debate a tres puntos que considero fundamentales si queremos hacer la transición de la guerra a la paz:

**La paz como derecho**:  según el artículo 22 de la constitución nacional **“La paz es un derecho y un deber de obligatorio cumplimiento”** cuando el presidente Juan Manuel Santos insiste en el no cese bilateral del fuego está pasando por alto aquellas poblaciones que han sido azotadas por la violencia y que seguirán siéndolo –como en las últimas semanas- si se empeña en dialogar en medio del conflicto; el primer mandatario gobierna bajo el capricho de la extrema derecha pero no para la población rural: los campesinos que han sufrido y siguen sufriendo la amenaza constante del terror de la guerra. Es insultante cuando se propone desde el congreso una fecha límite para un acuerdo que se ha desarrollado en una mesa pero que aún no se ejecuta en las zonas de combate. La papeleta para la paz, que proponen los senadores Claudia López y Antonio Navarro Wolff, no solamente es absurda sino irresponsable, dado que pretende lanzar las negociaciones a la opinión pública sin un previo conocimiento de los puntos firmados; señalando que la opinión pública no es solo sino la voz  de los medios que día a día atacan las conversaciones. Es lógico un referendo pero bajo un acuerdo ya firmado y con el previo conocimiento de los pactos, de lo contrario todo el tiempo de la mesa de la Habana seria echado a la basura y al saboteo de aquellos interesados por acabarla. El  gran esfuerzo para alcanzar la paz del gobierno se contradice en el doble discurso que pone en duda su voluntad para lograrla.

**Desmovilización de los grupos armados**: al contrario de la desinformación que recibimos todos los días, los actores violentos deben tener las garantías para su desmovilización, acordar un proceso de paz supone cambiar armas por urnas, de igual manera, es fundamental el reconocimiento de los crímenes y las victimas de la guerrilla y el estado para lograr un nuevo escenario participativo.  Antes de un referendo los colombianos deben estar en la capacidad para entender que el postconflicto no solamente repara a las víctimas sino que debe dar el espacio para que los actores que decidieron dejar las armas participen de manera democrática y sin ningún tipo de discriminación politica.

**Movilización para la paz**: La sociedad colombiana debe defender el derecho a vivir en paz, no basta con la cátedra para la paz, es urgente la conformación de  mesas comunales donde no solo los estudiantes tengan acceso a contenidos históricos del conflicto armado en Colombia, sino que el ciudadano del común entre al contexto que nos ha llevado a mantener 50 años de guerra. Los congresistas preocupados por el proceso de paz no han propuesto acciones reales que involucren a aquellos sectores ignorantes sobre la violencia y el proceso: trabajadores y civiles deben ser informados y capacitados desde sus áreas de labor; es increíble que se hable de paz pero que la cátedra llegue solamente al área académica cuando tenemos un serio problema social de desconocimiento sobre nuestra historia, siendo la causa principal que nos impide llegar a la reconstrucción social.  El error está en esperar que los medios nos informen y no informarnos por cuenta propia, el error está en el estado que no involucra al sector urbano con la problemática que ha soportado el rural.

El derecho a organizarnos debe nacer desde la ciudadanía y para eso el estado debe brindar y promover escenarios de debate e información. 50 años nos han organizado culturalmente para la guerra y para ser unos completos ignorantes  de nuestra historia. La verdadera propuesta no es poner fecha limite a las negociaciones de la Habana, la verdadera propuesta es generar campos de acción que nos permita tener una ciudadania capacitada para opinar de manera responsable sobre un periodo importante para la historia de Colombia.

Yo le diria a Claudia López y Antonio Navarro Wolff que proponer un referendo en medio de la ignorancia y la manipulación de los medios es tan peligroso como mantener a un pueblo 50 años bajo el terror y pretender perpetuarlo desde la comodidad de sus sillas.
