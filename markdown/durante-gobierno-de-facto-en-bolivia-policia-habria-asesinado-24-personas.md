Title: Durante gobierno de facto en Bolivia policía habría asesinado 24 personas
Date: 2019-11-17 14:42
Author: CtgAdm
Category: DDHH, El mundo
Tags: Bolivia, Manifestaciones, muertes, policia
Slug: durante-gobierno-de-facto-en-bolivia-policia-habria-asesinado-24-personas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/arton143162.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:]AbadMiranda 

Desde la autoproclamación de Jeanine Áñez como presidenta interina de Bolivia el pasado 13 de noviembre, apoyada por sectores señalados como golpistas, **la Defensoría del Pueblo evidenció que son 24 las muertes registradas** desde que comenzó la crisis política en el país, 14 de ellas en medio de intervenciones conjuntas entre Policía Boliviana y Fuerzas Armadas.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/arton143162-1024x709.jpg){.aligncenter .size-large .wp-image-76653 width="1024" height="709"}

En este mismo informe registran que 2 víctimas presentan muerte cerebral, alrededor de 500 denuncian afectaciones a su integridad, entre ellas 12 menores de edad; evidenciando que además de la capital, la ciudad de Sacaba en del departamento de Cochabamba es una de las más afectadas; **según medios de Bolivia el número total de muertes en las jornadas de protesta en esta región es de 8, varias pertenecientes a integrantes del Movimiento Cocalero.** (Le puede interesar:[Futuro incierto en Bolivia tras la renuncia de Evo Morales](https://archivo.contagioradio.com/futuro-incierto-en-bolivia-tras-la-renuncia-de-evo-morales/))

> Ante la continua convocatoria a movilizaciones hacia [\#LaPaz](https://twitter.com/hashtag/LaPaz?src=hash&ref_src=twsrc%5Etfw), se demanda al Estado y a las fuerzas de seguridad conjuntas que están obligadas a no afectar la vida y la integridad de las personas cuyo derecho a la protesta está protegido por la Constitución. <https://t.co/Qn1RdPn3R4>
>
> — Defensoría Bolivia (@DPBoliviaOf) [November 17, 2019](https://twitter.com/DPBoliviaOf/status/1195906669856874496?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**Organizaciones defensoras de derechos en cabeza de la Comisión Interamericana de Derechos Humanos ha condenado el "uso desproporcionado" de la fuerza**, así mismo algunos medios de comunicación del país han reportado que las ciudades más golpeadas en Bolivia  por la nueva ola de violencia policial han sido La Paz (5 muertes) , Santa Cruz (5 muertes), Potosí (1) y El Alto con múltiples heridos, lugares que no tienen precedentes en la historia reciente por enfrentamientos y agresiones en Bolivia.

> Mientras el pueblo humilde clama por paz y diálogo, el régimen de facto de Camacho, Mesa y Añez, reprime con tanques y balas. Desde su autonombramiento, ya causaron 24 muertos; ayer, 12 en Sacaba. Todos hermanos indígenas. Esa es la verdadera dictadura que masacra sin clemencia [pic.twitter.com/q8GJh7IIcr](https://t.co/q8GJh7IIcr)
>
> — Evo Morales Ayma (@evoespueblo) [November 17, 2019](https://twitter.com/evoespueblo/status/1195873572033576960?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Con este tipo de actuaciones queda en entre dicho la consigna de la oposición de un supuesto restablecido de la democracia en el país, reflejo de ellos sos las miles de personas que se han sumado a las manifestaciones en todas las regiones, y que han **enfrentado agresiones y hostigamiento de una represión mucho mas fuerte por parte del estado**. (Le puede interesar: [La represión y el terror armas de control social de Gobiernos Latinoamericanos](https://archivo.contagioradio.com/la-represion-y-el-terror-armas-de-control-social-de-gobiernos-latinoamericanos/))

> La CIDH condena cualquier acto administrativo del gobierno de [\#Bolívia](https://twitter.com/hashtag/Bol%C3%ADvia?src=hash&ref_src=twsrc%5Etfw) que atente contra el derecho a la verdad, la justicia y al derecho internacional de los DDHH, particularmente en el contexto de actuaciones de Fuerzas Armadas en las protestas sociales. (3/3)
>
> — CIDH - Comisión Interamericana de Derechos Humanos (@CIDH) [November 16, 2019](https://twitter.com/CIDH/status/1195817637445545984?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
> Está clarísimo que la Sen. Añez vive en una dimensión absolutamente distinta a la realidad. Una donde puede twittear sobre entrevistas y llamadas y no lamentar el asesinato de 8 compatriotas por parte de fuerzas militares. Esa no es la Democracia que prometieron.
>
> — Adriana Salvatierra (@Adriana1989sa) [November 16, 2019](https://twitter.com/Adriana1989sa/status/1195763284265320448?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### "Añez ilegal asesina", familiares despiden a los 24 protestantes asesinados en Bolivia

Este 17 de noviembre cientos de personas realizaron las honras fúnebres de los 24 manifestantes muertos en todo el país sudamericano; muchos de estos [en su mayoría cocaleros del Chapare integrantes de la resistencia de Evo Morales. ]{.wc9LZ}

[**Los protestantes perdieron su vida reclamando el regreso de su líder indígena**;  al intentar  cruzar un puesto militar de control en la ciudad de Cochabamba donde opositores de Áñez  se enfrentaban con defensores de esta, **situación que se vio dispersa en medio de una operación**]{.wc9LZ}**conjunta entre policía y ejército.**

> Los autores del [\#GolpeDeEstadoEnBolivia](https://twitter.com/hashtag/GolpeDeEstadoEnBolivia?src=hash&ref_src=twsrc%5Etfw) gobiernan con decretos, sin en el Legislativo y apoyados en armas y bayonetas de Policía y FFAA. Promulgaron un DS que deja a la institución militar exenta de responsabilidad penal. Es una carta blanca de impunidad para masacrar al pueblo [pic.twitter.com/tfIFtutprv](https://t.co/tfIFtutprv)
>
> — Evo Morales Ayma (@evoespueblo) [November 16, 2019](https://twitter.com/evoespueblo/status/1195849846973968384?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Ante este acto que ahora se conoce como la "masacre de Sacaba"  la **Defensoría del Pueblo solicitó al  gobierno interino  investigar  si las acciones de las fuerzas de control se ejecutaron  en el marco de la constitución** y las normas  internacionales entorno a la defensa de los Derechos Humanos.

### "emplear toda la fuerza necesaria"

La presidenta interina, **Jeanine Áñez**, firmó este 16 de noviembre el Decreto Supremo 4078 que **autoriza que los integrantes de las Fuerzas Armadas queden "exentos de responsabilidades penales"** en medio de las acciones que entren en el cumplimiento de sus funciones, y añadió "Para el cumplimiento del presente decreto, **las Fuerzas Armadas emplearán todos los medios disponibles, y los que puedan ser adquiridos, de acuerdo a las necesidades a fin de garantizar, la seguridad y la protección del pueblo boliviano**".

### "Represión y abuso de fuerza puede llevar a Bolivia salirse de control"

Luego de las acciones registradas en Bolivia en los últimos últimos días muchas organizaciones internacionales defensoras de derechos se han pronunciado entre ellas, la Alta Comisionada de Naciones Unidas para los Derechos Humanos, Michelle Bachelet, quien en un comunicado de las últimas horas advirtió,  "uso innecesario o desproporcionado de la fuerza policial o militar, puede llevar a la situación en Bolivia a  salirse de control".

Y agregó, "El país está dividido y personas de los distintos sectores del espectro político se encuentran indignados. En una situación como esta, las acciones represivas de parte de las autoridades simplemente avivarán más esa ira, y pueden poner en peligro cualquier camino de diálogo posible".

### 702 militares bolivianos destituidos por protestar contra el racismo hacia soldados indigenas

La movilización de esta semana fue constituida por militares de rango bajo y medio, sus familiares y asociaciones bolivianas de etnia Aymara, quienes coreaban múltiples arengas en contra del racismo al interior de la organización, **“Son somos pocos, somos miles”**, exclamaron en defensa de la igualdad de los soldados indígenas.

Luego de esta marcha el Jefe de las Fuerzas Armadas, con respaldo de el Ejército de Tierra y la Fuerza Aérea, anunciaron el **retiro obligatorio de 702 suboficiales y sargentos, acusándolos de faltar a su trabajo**  **sedición, motín, realizar acción política,** atentando contra el honor y dignidad.

### Regreso inmediato de Evo morales a Bolivia

Por otro lado luego de que el parlamento de Bolivia declarara el posicionamiento de la senadora Adriana Salvatierra Arriaza representante de Juventudes del MAS-IPSP, como ilegal y le negaran el ingreso a la sede del congreso en La Paz , así mismo **el parlamento declaró que aceptaría  la renuncia de Evo Morales y se solicitó el inmediato regreso al país**; situación que ha sido omitida en algunos medios bolivianos e internacionales con lo que podría ser una estrategia para evitar que lo que esta sucediendo en Bolivia si se apegue a las normas constitucionales.

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
