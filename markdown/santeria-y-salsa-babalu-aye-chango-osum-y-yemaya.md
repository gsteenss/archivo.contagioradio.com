Title: Santería y Salsa: Babalú Ayé, Changó, Osum y Yemayá
Date: 2015-09-17 16:00
Category: Cultura, En clave de son
Tags: Influencia Orisha en la salsa, Mezcla cultural en latinoamérica, Orígenes de la Salsa, Santería en la salsa
Slug: santeria-y-salsa-babalu-aye-chango-osum-y-yemaya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/santeria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Ilustración Santería Orisha] 

<iframe src="http://www.ivoox.com/player_ek_8440390_2_1.html?data=mZmhkpiddI6ZmKiakpeJd6Kmkoqgo5qYcYarpJKfj4qbh46kjoqkpZKUcYarpJK0h6iXaaKtz8rgy9iPqMafzcaYtcbQt8KZk6iYz8rep83VjMjiztnZtsLgjN6Y1cbSuMbmhqigh6aopY%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### 11 Sep 2015

Para hablar de los orígenes de la salsa, es muy amplio el espectro, tanto musical como cultural, que debemos atravesar, para llegar hasta sus raíces. El largo camino a recorrer en el tiempo, nos remonta hasta el siglo XIX, cuando diásporas de la cultura africana, se expandián por el mundo, gracias en parte al alto tráfico de esclavos durante la época.

Cuba, por su localización geográfica, se convirtió en un puerto con alta afluencia de colonizadores y por ende sus esclavos, siendo estos últimos los directos responsables de incubar una mezcla cultural, que algunos historiadores de música latina catalogarían como afrohispanoindigenas.

Esta mixtura, no solo nutriría aspectos de la cultura general, como la gastronomia y constumbres, también permearía de forma importante la religión y la musica, por eso es este el preciso punto de quiebre en donde nos detendremos para este texto, por cimentar una base solida y que se prolongaría durante la evolución de la salsa.

Gracias a que la religion venida desde Europa, se concentraba en convertir solo a los latifundios o a sus iguales, pasando por alto a sus esclavos, es que la cultura Africana se mantendría con vida, ya que estas minorias, gestarón en donde tuvierón la posibilidad, sus costumbres, para no dejarlas extinguir.

Es esta la razón por la cual en la isla, se instauraría con fuerza "**La Santeria"**, un culto religioso, heredado de tradiciones culturales **Yoruba** y combinado con algo de catolicismo. Este tendría una importante influencia en la salsa, ya que sus ritos traicionales de evocación a deidades, se llevrían y se llevan a cabo, con ritmos musicales, combinados de fuertes percuciones y llamados vocales de quienes relizan el rito.

Grandes salseros, muchos años después del inicio del mestizaje cultural, rindieron homenajes, a las divinidades que se le cantaban siglos atrás, ejemplo de estos tributos son las siguientes interpretaciones.

**Babalú Ayé**, Asociado con San Lazaro, es el Orisha de las enfermedades venéreas, las pestes y la miseria.

<iframe src="https://www.youtube.com/embed/SvRSTbFHzcQ" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Changó**, Asociado con Santa Barbara, es el rey guerrero de la religión Yoruba.

<iframe src="https://www.youtube.com/embed/rS8aOJcPlJU" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Osum**, Asociado con San Juan Bautista, es otro guerrero Osha, y es el encargado de proteger y vigilar, y **Yemayá**, es la madre de todos los hijos en la tierra.

<iframe src="https://www.youtube.com/embed/p89YhYCaXG0" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>  
 
