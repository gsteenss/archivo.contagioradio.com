Title: Infiltración en marchas por parte de Policía y Ejército: una práctica que se está haciendo común
Date: 2020-01-22 13:00
Author: CtgAdm
Category: Movilización, Nacional
Tags: ejercito, Infilitrados
Slug: infiltracion-marchas-parte-policia-ejercito-practica-comun
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Cauca.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Cauca-Infiltrados.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además de los videos en los que se evidenció que policías de civil ingresaron encapuchados al campus de la Universidad Nacional de Colombia por uno de los costados del alma mater, también se presentaron hechos similares en la **Universidad del Cauca en Popayán**. Según la denuncia de los estudiantes de esa institución. Dos de las personas descubiertas se identificaron como integrantes del Ejército, dos como integrantes de la policía y un quinto que no se identificó.

<!-- /wp:paragraph -->

<!-- wp:image {"id":79537,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Cauca-Infiltrados-768x1024.jpg){.wp-image-79537}  

<figcaption>
Foto: UNEES

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Según la denuncia de los organizadores de la marcha, quienes participaban notaron un comportamiento extraño en algunos de los participantes, por ejemplo **“hacían registro audiovisual de los dinamizadores”.** Una vez se intentó corroborar el grupo al que pertenecían las personas, fueron abordados por integrantes de los grupos de DDHH a lo que respondieron que hacían parte del ejército. Luego fueron entregados a la policía que los liberó minutos después. [(Le puede interesar: Como falsos positivos judiciales, califican capturas de estudiantes en Pereira)](https://archivo.contagioradio.com/como-falsos-positivos-judiciales-califican-capturas-de-estudiantes-en-pereira/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Ya esta situación se presentó el pasado mes de noviembre cuando un grupo de encapuchados se infiltró en la facultad de Humanidades** y realizaron actos vandálicos de los cuales acusaron a los estudiantes. En ese caso se realizó la denuncia con las pruebas correspondientes, por lo cual hasta ahora no se conoce de ningún tipo de decisión por parte de las autoridades. “Nos dicen que van a abrir la investigación pero no pasa nada” asegura.

<!-- /wp:paragraph -->

<!-- wp:image {"id":79535,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Cauca-1024x768.jpg){.wp-image-79535}  

<figcaption>
Se identificaron dos personas de la Policía, dos del Ejército y alguien que no se acredito como parte de una institucionalidad Foto: UNEES

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Lo que más preocupa a los estudiantes y defensores de DD.HH. es que **este tipo de acciones siguen en la impunidad, los hechos vandálicos no han sido reparados por la Policía o el Ejército** y por último, los registros de video y fotográficos son los que luego se usan para estigmatizar y hasta ofrecer recompensa por los líderes sociales que participan en las movilizaciones. [(Lea también: Hay que salir masiva, enérgica y creativamente para contrarrestar el miedo)](https://archivo.contagioradio.com/hay-que-salir-masiva-energica-y-creativamente-para-contrarrestar-el-miedo/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El próximo 25 de Enero se realizará asamblea popular en Popayán**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Con el objetivo de seguir fortaleciendo la movilización, las organizaciones presentes en el departamento del Cauca realizarán una asamblea popular abierta el próximo 25 de Enero en la que se definirán las próximas movilizaciones y la agenda de participación e impulso de la Asamblea Popular nacional que se realizará a finales del mes de enero en Bogotá. [(Lea también: ¿Vuelve la persecución judicial contra el movimiento estudiantil?)](https://archivo.contagioradio.com/persecucion-judicial-movimiento-estudiantil/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

<!-- /wp:paragraph -->
