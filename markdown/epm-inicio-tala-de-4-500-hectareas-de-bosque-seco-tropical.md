Title: EPM inició tala de 4.500 hectáreas de Bosque Seco Tropical
Date: 2016-07-28 13:47
Category: Ambiente, Otra Mirada
Tags: Animales, Antioquia, bosque seco tropical, EPM, Hidroituango
Slug: epm-inicio-tala-de-4-500-hectareas-de-bosque-seco-tropical
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/tuparro-bosques-secos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Alexander von Humboldt 

###### [28 Jul 2016] 

Empresas Públicas de Medellín, EPM, inició la tala de 4.500 hectáreas de bosque seco tropical, en la zona donde se lleva a cabo el proyecto hidroeléctrico Hidroituango. La actividad ya deja **las primeras víctimas pues cientos de animales han perdido su hábitat** y muchos de ellos son especies en peligro de desaparecer.

De acuerdo con Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos de Antioquia, a prinicipios de 2016 la empresa inició una campaña en la que le ofreció a los campesinos de la zona talar árboles a cambio de un pago. Actualmente hay cerca de 200 personas en esta actividad, situación que ha generado confrontaciones entre quienes aceptaron la propuesta de EPM y quienes están preocupados por pervivencia de los animales y del bosque.

La empresa argumenta que requiere talar las 4.500 hectáreas para llenar el embalse del proyecto hidroeléctrico, desconociendo que el Bosque Seco Tropical es un ecosistema del cual solo queda el 8% de las 9 millones de hectáreas que existían en Colombia, según reportó ** el libro Bio Diversidad 2015 del Instituto Alexander Von Humboldt.**

"Teniendo en cuenta que el Bosque Seco Tropical constituye un porcentaje muy pobre de las áreas del Sistema Nacional de Áreas Protegidas (…) es imperante establecer estrategias integrales para su gestión", dice la publicación, que agrega que "la mayoría de sus áreas están expuestas", cuando **allí  viven aproximadamente 2.600 especies de plantas, al menos 230 de aves, 60 de mamíferos, de las cuales 119 especies animales son endémicas**.

Esta situación además afecta a las comunidades teniendo en cuenta que si no hay árboles, los pobladores se verían obligados a desplazarse pues en esa zona del país es imposible vivir sin sombra, y además el bosque es necesario para la actividad que realizan milenariamente las familias, según afirma Zuleta.

Desde el Movimiento Ríos Vivos, se señala que la situación es preocupante debido a que en Toledo se inició la tala y ya han empezado a migrar las especies hacia la parte alta de la montaña. Aunque la comunidad ha realizado las respectivas denuncias a las autoridades ambientales entre ellas, **Corantioquia, el Ministerio de Ambiente, la alcaldía y la Fiscalía General de la Nación,** estas no han actuado pues dicen que el plan ambiental de EPM es uno de los mejores respecto a este tipo de proyectos.

"Corantioquia ha dicho que el equipo de EPM es de alta calidad. Las instituciones están anonadadas con el plan ambiental pero lo que hay es una reparación del bosque en el papel", denuncia Zuleta quien explica que en la licencia ambiental la reparación del bosque debía haberse iniciado antes que las obras, así mismo, debían tener unos sitios especiales o viveros donde se recuperara las semillas de la zona y unos refugios para los animales.

**"Lo que hay es una carpa rudimentaria, una veterinaria que espera que los animales lleguen**, y además no existe un sitio óptimo para resguardar a las especies", indica la vocera del movimiento. A su vez, EPM ha dicho a las comunidades que solo se protegerá una pareja de cada especie que habite en ese ecosistema.

Para Isabel Cristina, el hecho de que la Alcaldía de Ituango tenga una valla de EPM en su fachada evidencia que "el poder corporativo es el que está posicionado en las instituciones", pues recuerda que hace unos meses hubo un incendio que afectó 500 hectáreas de bosque, provocado por unas personas que luego fueron identificadas por los pobladores, pero **aunque el Movimiento Ríos Vivos presentó la denuncia, la Fiscalía se negó a recibirla.**

Ante esta situación, desde la organización defensora del ambiente y los derechos humanos, se está elaborando una acción urgente por el bosque, de la mano de un listado de especies que conoce la población, pues **"Cuando se mata a un árbol se mata una persona"**, expresa Zuleta, quien agrega que próximamente habrán acciones de movilización para llamar la atención sobre este hecho.

\  
<iframe src="http://co.ivoox.com/es/player_ej_12368812_2_1.html?data=kpegmJ2cdZOhhpywj5abaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncarnwsfSzpC-uc3Z1caSlKiPloa3lIquptTXb7fd19Tgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
