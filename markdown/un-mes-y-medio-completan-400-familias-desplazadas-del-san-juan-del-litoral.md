Title: Un mes y medio completan 400 familias desplazadas del San Juan del Litoral
Date: 2017-05-05 12:16
Category: DDHH, Nacional
Tags: Bajo San Juan del Litoral, buenaventura, Desplazamiento
Slug: un-mes-y-medio-completan-400-familias-desplazadas-del-san-juan-del-litoral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/coliseo-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [05 May 2017] 

**Un mes y medio completan más de 400 familias que permanecen en el Coliseo de Buenaventura, desplazadas desde el San Juan del Litoral**, en precarias condiciones de salud, debido a los enfrentamientos que se ha reportado entre estructuras paramilitares y el ELN. Las familias han denunciado que hay brotes de gripa, problemas estomacales y una falta de atención por parte de las instituciones gubernamentales que no les han ofrecido garantías de retorno a sus tierras.

De igual forma, las **más de 3.000 personas que no han podido salir de sus comunidades, continúan confinadas en sus hogares** afrontando el desabastecimiento**,** debido a que por temor a los enfrentamientos, no han podido salir a realizar sus quehaceres ni las labores tradicionales de pesca y trabajo. Le puede interesar:  ["3.000 personas se encuentran confinadas en el Bajo San Juan Litoral"](https://archivo.contagioradio.com/3-000-personas-se-encuentran-confinadas-en-el-bajo-san-juan-litoral/)

En el coliseo de Buenaventura, las familias que se mantienen allí señalaron que, pese a que han recibido ayudas de organizaciones como ACNUR, la Cruz Roja Colombia y la Cruz Roja Internacional, la ayuda por parte de la Alcaldía ha sido nula, además expresaron que **los inconvenientes con el agua continúan y ahora les está generando daños estomacales**.

Frente a los brotes de gripa que ya habían empezado a surgir, Dagoberto Pretelt, representante legal de la comunidad de cabecera del consejo comunitario de Acadesan, indicó que solo los ha visitado una brigada de salud por parta de la Secretaría de Salud, **sin embargo, los casos de estos brotes persisten. **Le puede interesar: ["Confinadas comunidades indígenas y Afros en el Alto San Juan Litoral"](https://archivo.contagioradio.com/san-juan-choco-paramilitares/)

De acuerdo Pretelt, pese a que se han sostenido dos reuniones con entidades gubernamentales, una en Cali, con la gobernación departamental del Valle del Cauca y otra en Bogotá, con una comisión de alto nivel, no se han dado garantías ni para las familias que se encuentran en el Coliseo, **ni para las que se encuentran confinadas en el Bajo San Juan.**

“No hay una respuesta definida, se habló de mucho dinero que envían a los municipios, pero la verdad es que a las cabeceras de las comunidades, tanto indígenas como negras, ese dinero que dicen que han invertido no se ve reflejado. **Hoy lo que estamos exigiendo es de carácter urgente, que se tomen medidas ya**”, afirmó Pretelt.

<iframe id="audio_18523519" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18523519_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
