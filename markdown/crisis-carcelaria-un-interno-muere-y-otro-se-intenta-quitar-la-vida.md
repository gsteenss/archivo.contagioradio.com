Title: Por crisis carcelaria un interno muere y otro intenta quitarse la vida
Date: 2016-02-04 12:19
Category: DDHH, Nacional
Tags: carcel de cúcuta, crisis carcelaria, Palogordo
Slug: crisis-carcelaria-un-interno-muere-y-otro-se-intenta-quitar-la-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/crisis-carcelaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sicsemanal 

###### [4 Feb 2016]

Continúa intensificándose la crisis carcelaria en Colombia, esta vez, por falta de atención en salud eficaz y oportuna en la cárcel de Cúcuta murió el detenido **Renzo Alí Roa, y en la cárcel de Palogordo, el interno Elias Moñis intentó quitarse la vida** tras presentar intensa enfermedad digestiva sin ser atendido.

El Movimiento Nacional Carcelario, asegura por medio de un comunicado, que desde el sábado 30 de enero, el interno Renzo Alí empezó a sufrir un fuerte dolor estomacal, que solo fue atendido por la enfermería con una pastilla para el dolor, sin embargo, esta no fue suficiente y el estado de salud del interno se agravó, y fue demasiado tarde cuando lo trasladaron al hospital, pues el 1 de febrero falleció.

Por otro lado, se denuncia que el preso Elias Moñis, recluido en la Cárcel de Palogordo, **“presentó por más de diez días intensos vómitos y diarrea sin que fuera remitido a un centro hospitalario bajo el argumento que no existe convenio de atención en salud con el INPEC.** Ante el desespero que le produjo la situación, el 2 de febrero del presente año, a las 5:00 am, ELIAS intentó quitarse la vida produciéndose heridas en su cuello, originando de esta manera que fuera remitido al hospital público de Bucaramanga”, dice la denuncia.

Por esta situación y desde hace semana los internos de los patios tres y cuatro de la cárcel de Palogordo se encuentran en desobediencia pacífica para exigir una  solución a la crisis carcelaria que en este momento tiene en difíciles condiciones a **los más de 200 enfermos con órdenes judiciales para ser atendidas sin hallar respuesta positiva del INPEC.**

A esta situación se suma, la jornada desobediencia pacífica que iniciaron esta semana todos los internos del centro penitenciario, el ERON Picota, debido a los incumplimientos del gobierno frente a los acuerdos de la Habana, en los que se establecía jornadas de salud.
