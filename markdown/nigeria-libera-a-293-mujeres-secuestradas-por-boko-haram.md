Title: Nigeria libera a 293 mujeres secuestradas por Boko Haram
Date: 2015-04-29 16:01
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Boko Haram, Boko haram secuestra 293 niñas de escuela, Nigeria libera 293 niñas en poder de Boko Haram
Slug: nigeria-libera-a-293-mujeres-secuestradas-por-boko-haram
Status: published

###### Foto:Nbc-news.com 

**Nigeria ha liberado a 293 mujeres y niñas secuestradas por la milicia islámica Boko Haram** en el monte Sambisa, bastión territorial de la milicia.

En total son 200 mujeres y 93 niñas pero según ha asegurado el portavoz del ejército **ninguna pertenece a las niñas secuestradas en la escuela de Chibok hace un año** que conmocionó al mundo entero. La liberación responde a las **operaciones conjuntas con Camerún, Chad y Níger** contra el grupo islámico, que comenzaron con el cambio de gobierno hace un mes.

**Boko Haram es una milicia islamista radical,** cuyo principal objetivo es la imposición de la Sharia o ley islámica, que es considerada en el norte como justicia informal, pero rechazada frontalmente por los estados del sur de mayoría cristiana.

Aunque la **Sharia es considerada como la ley islámica,** es más un modo de vida interpretable, que es tomada como patrón de grupos extremistas para así, crear del código de conducta, un dogma sobre lo que se puede o no hacer en la vida.

La milicia se crea en 2002, tomando posiciones en el norte del país. Su acción mas sonada fue en abril del 2014 cuando tomaron una escuela secuestrando a mas de 200 niñas con el objetivo de denunciar la política educativa occidental que se aplica en dichas escuelas.

**Bring Back Our Girls (Devolvednos a nuestras chicas), es la organización social creada por las víctimas** y por solidarios de todo el mundo. La organización ha desplegado una gran campaña internacional con el objetivo de presionar al gobierno para que despliegue todos los medios y las encuentre.

Los familiares apenas han recibido información y la única respuesta del gobierno ha sido que mantengan la esperanza de que serán recuperadas.

Hay que recordar que el **anterior ejecutivo tardó 19 días en condenar el ataque** y además condenó primero los ataques contra la revista satírica Cahrlie Hebdo en Francia, por lo que fue fuertemente criticado.
