Title: MinInterior impide participación de 14 comunidades en elección del Representante Legal
Date: 2016-08-01 13:35
Category: Comunidad, Nacional
Tags: Consejos Comunitarios Bajo atrato, Curvarado, Desplazamiento, Restitución de tierras
Slug: mininterior-impide-participacion-de-14-comunidades-en-eleccion-del-representante-legal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Curvaradó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Justicia y Paz ] 

###### [1 Ago 2016] 

Los 14 Consejos Comunitarios que figuran en la titulación colectiva de los territorios en Curvaradó, Chocó, denuncian que el Ministerio del Interior impidió que participaran en la elección del representante legal, argumentando que los pobladores mestizos no tienen voz y voto, ni tienen derecho a elegir y ser elegidos, porque no son del territorio. Para las comunidades esta elección no fue legítima pues **la mayoría de votos fueron de quienes no viven en el territorio y quien fue elegido, representa intereses empresariales**.

Según afirma Ligia María Chaverra, candidata elegida por las comunidades, hubo plata de por medio para votar por quien fue electo. La noticia les cae como un balde de agua fría y les deja claro que "los empresarios están reclamando el territorio que compraron" y que el representante legal elegido, los favorecerá. Para las comunidades esta grave situación **responde a las intimidaciones y presiones de los empresarios ocupantes de mala fe** y al "silencio y la sordera de los responsables de buscar la paz, la armonía y el respeto a los derechos de toda la población".

Dos días duró la asamblea en la que las comunidades concertaron su candidatura y acordaron enviar una carta a las autoridades competentes denunciando que en el proceso de elección se desconocían el artículo 10 de la Ley 70 y el auto 299 de 2012; sin embargo, estos argumentos nunca fueron escuchados y según denuncian las comunidades, los dos funcionarios del Ministerio del Interior que lideraron las elecciones se negaron a recibir la comunicación, por lo que aseguran que **crearán un Consejo paralelo así el Gobierno no lo reconozca**.

[![Carta Curvaradó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Carta-Curvaradó.jpg){.aligncenter .size-full .wp-image-27081 width="500" height="803"}](https://archivo.contagioradio.com/ministerio-del-interior-impide-participacion-de-comunidades-en-eleccion-de-representantes-legales/carta-curvarado/) [![Carta Curvaradó\_](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Carta-Curvaradó_.jpg){.aligncenter .size-full .wp-image-27082 width="500" height="802"}](https://archivo.contagioradio.com/ministerio-del-interior-impide-participacion-de-comunidades-en-eleccion-de-representantes-legales/carta-curvarado_/) [![Carta Curvaradó\_\_](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Carta-Curvaradó__.jpg){.aligncenter .size-full .wp-image-27083 width="500" height="749"}](https://archivo.contagioradio.com/ministerio-del-interior-impide-participacion-de-comunidades-en-eleccion-de-representantes-legales/carta-curvarado__/) [![Carta Curvaradó\_\_\_](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Carta-Curvaradó___.jpg){.aligncenter .size-full .wp-image-27084 width="500" height="770"}](https://archivo.contagioradio.com/ministerio-del-interior-impide-participacion-de-comunidades-en-eleccion-de-representantes-legales/carta-curvarado___/)

<iframe src="http://co.ivoox.com/es/player_ej_12406356_2_1.html?data=kpehkpuXeZehhpywj5WdaZS1lpyah5yncZOhhpywj5WRaZi3jpWah5ynca3dyM7OjbLFtoa3lIqupsaPh8nV18rf1MaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
