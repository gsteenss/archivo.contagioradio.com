Title: Anulan inscripción de cédulas de familiares de excombatientes de FARC
Date: 2019-10-22 18:51
Author: CtgAdm
Category: DDHH
Tags: cesar, Elecciones2019, etcr, excombatientes, FARC, Tierragrata
Slug: familiares-farc-no-podran-votar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-22-at-11.06.50-AM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-22-at-11.06.50-AM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/IMG_9726.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@PachoTolozaF] 

El pasado 30 de septiembre excombatientes, familiares y candidatos por el Partido FARC en el ETCR de Tierra Grata, Cesar, recibieron un mensaje de texto de la Procuraduría General de Nación informando que había inconsistencias en la inscripción de sus cédulas, y por tal motivo se inhabilitaría su voto; luego de una reunión con el Comité de Seguimiento Electoral del Cesar, se logró habilitar el voto de varios de ellos, sin embargo la gran mayoría de familiares y amigos de los excombatientes aún están impedidos.

En el país fueron anuladas al menos 1.233 cédulas, en su mayoría de excombatientes de FARC y 75 de ellas solo en el municipio de Manaure donde se encuentra el Espacio Territorial de Tierra Grata, la razón de la Procuraduría fue sospecha de trashumancia electoral.  (Le puede interesar:[Tres elementos claves para garantizar unas elecciones transparentes](https://archivo.contagioradio.com/tres-elementos-claves-para-garantizar-unas-elecciones-transparentes/))

Según Fredy Escobar, integrante del ETCR en Tierra Grata “hay una especie de jugada para que quienes hacen parte del partido FARC en Cesar no puedan votar”; así mismo explicó que el hecho se dio tras la confusión de si los excombatientes tendrían que votar en La Paz o en Tierra Grata, “el Gobierno sabe dónde habitamos y en qué proceso estamos, por eso nos resulta ilógico imaginar que nos adjudiquen trashumancia”, agregó Escobar.

Así mismo Escobar agregó que se ha logrado solucionar la situación de muchos de los votantes, en su mayoría candidatos del corregimiento y excombatientes, pero asegura que “el daño ya está hecho, muchos de nuestros colegas y familiares ya no podrán votan, lo cual representa pérdida de votos, y  le resta credibilidad, y confianza al proceso, esto desanima y nos desequilibrio en plena campaña electoral”.

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
