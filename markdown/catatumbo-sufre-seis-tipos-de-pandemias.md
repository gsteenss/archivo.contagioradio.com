Title: Catatumbo sufre seis tipos de pandemias
Date: 2020-07-28 22:43
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Catatumbo, Desplazamiento forzado, Erradicación Forzada, Paramilitarismo
Slug: catatumbo-sufre-seis-tipos-de-pandemias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/desplazameinto-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Desplazamientos en Catatumbo/ Ascamcat

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La región del Catatumbo en Colombia, conformada por nueve municipios en la parte norte del departamento de Norte de Santander: **Convención, El Tarra, Hacarí, Teorama, San Calixto, La Playa, Sardinata, El Carmen** y **Tibú**, sufre hoy por hoy pero desde hace más de 30 años de seis problemas estructurales contra los que los diferentes gobiernos no han actuado y cuando lo han hecho ha sido de manera ineficaz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La militarización de los territorios por la vía de la lucha contra el narcotráfico que no ha servido, la persistencia del paramilitarismo, la presencia del ELN y el EPL así como de disidencias o nuevas guerrillas, la apropiación de tierras para la siembra de palma de aceite, el petróleo y ahora el COVID son algunos de los mayores problemas que enfrenta esta región, sin contar con el asesinato de líderes sociales y personas en proceso de reincorporación que ha cobrado la vida de mas de 800 personas desde la firma del acuerdo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tras la firma del Acuerdo de Paz, su implementación no ha llegado a la región del Catatumbo, grupos como el ELN, el EPL y herederos del paramilitarismo como Los Rastrojos se disputan el territorio generando masacres, asesinatos selectivos y desplazamientos forzados en contra de líderes sociales y población civil que queda en medio del fuego cruzado ante un Estado ausente y solo representado por la Fuerza Pública que pese a ser numerosa centra sus ataques a las comunidades.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Líderes sociales y comunidades corren peligro

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A lo largo del año, **en el Catatumbo han sido asesinados cinco líderes sociales,** dos de ellos en Convención y otros tres en el municipio de Tibú, mientras que se han conocido al menos 50 amenazas contra liderazgos en Norte de Santander. [(Le puede interesar: Fue asesinado el líder social Carmen Ángel en el Catatumbo)](https://archivo.contagioradio.com/fue-asesinado-el-lider-social-carmen-angel-en-el-catatumbo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante registros como los 973 líderes asesinados tras la firma del Acuerdo, **Olga Lucía Quintero, lideresa de la [Asociación de Campesinos del Catatumbo (ASCAMCAT)](https://twitter.com/AscamcatOficia)** señala que la situación de los líderes sociales en la región también es compleja, mientras la judicialización aqueja a algunos defensores de DD.HH. otro fenómeno que ha alarmado a las comunidades es el de la desaparición forzada. Un claro ejemplo es el caso de **Juan de Jesús Pineda, presidente de la Junta de Acción Comunal de la vereda Miraflores, desaparecido desde el 24 junio en Convención.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A ello se suman las cuatro ejecuciones extrajudiciales conocidas en el Catatumbo a manos del Ejército y la situación de desplazamiento suscitada tras el el asesinato de ocho personas en la zona rural de Cúcuta, en límites con Tibú, la población afectada continúa en los albergues con zozobra, aun más, tras el asesinato de Jhonny Alexander Ortiz a un kilómetro del albergue humanitario dónde se encuentran más de 400 personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con relación a los grupos armados, la lideresa de ASCAMCAT resaltó la importancia de que el Gobierno atienda la solicitud del ELN para retomar el diálogo y se establezca un cese al fuego y así avanzar en disminuir el conflicto en los territorios, señalando que es el sentir de las comunidades.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Repunte del paramilitarismo en medio de una región militarizada

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El paramilitarismo ha estado vivo en toda la línea fronteriza mientras no se realiza un esfuerzo por parte de las autoridades, mientras fenómenos como el sicariato, el desmembramiento de cuerpos y torturas que no son abordadas por las autoridades en particular en áreas como Totumito, Palmarito, Villa del Rosario y Cúcuta. [(Le puede interesar: Rastrojos asesinan a ocho personas en el Catatumbo)](https://archivo.contagioradio.com/masacre-catatumbo-rastrojos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para inicios de 2018, el Gobierno instauró en Tibú, nuevos Batallones de Operaciones Terrestres, conformados cada uno por cerca de 1.000  hombres, mientras para octubre del mismo año la administración Duque activó una fuerza de despliegue rápido de 5.000 militares más sumando cerca de 15.000 hombres en la región, pese a ello la violencia se ha recrudecido.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, **Quintero** relata que si se recorren las vías de la zona en particular las que llevan a **Cúcuta, El Zulia, Sardinata, Tibú y Ocaña,** se pueden divisar fuerzas militares por todos lados por lo que debería haber un control mayor en los territorios, lo que no obedece a la realidad. [(Lea también: Sobreviviente de la masacre de Tibú, fue asesinado)](https://archivo.contagioradio.com/sobreviviente-de-la-masacre-de-tibu-fue-asesinado/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"No es simplemente militarizar la zona, la gente necesita que se desarrolle el Programa Nacional Integral de Sustitución de cultivos de Uso Ilícito** (**PNIS), los Programas de Desarrollo con Enfoque Territorial y que haya inversión real en la región, lo que a la fecha no se ve por ningún lado"**, manifiesta Lina Amaya, directora de la Corporación Construyendo Poder Democracia Y Paz - (Poderpaz).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Todo gira alrededor de los cultivos"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Amaya señala que al problema se suman los monocultivos de la región con el cultivo de palma de aceite y en otras zonas con la hoja de coca, lo que ha llevado al paramilitarismo desde la década de los noventa a despojar y desplazar a las comunidades que habitan dichas tierras.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Sí, hay políticos detrás de las 28.000 hectáreas de cultivos de palma aceitera en el Catatumbo" afirma Olga Quintero cuestionando cómo "el Estado dice tener presencia en un territorio por medio de la bota militar o al imponer la palma aceitera, que puede que beneficie a algunas familias pero no a una región" al generar problemas ambientales y de consumo de agua. [(Lea también: Catatumbo necesita un acuerdo humanitario, no erradicación forzada)](https://archivo.contagioradio.com/catatumbo-sigue-clamando-por-un-acuerdo-humanitario-que-ponga-fin-a-la-guerra/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Durante el aislamiento preventivo se ha visto un incremento en las erradicaciones forzadas y afectando directamente a quienes se acogieron al acuerdo de sustitución voluntaria y a "la gente que no está dentro del acuerdo pero que anhela ser parte del PNIS, sin embargo el Gobierno envía los fusiles" denuncia Olga Quintero resaltando cómo el accionar del Ejército ha llevado al asesinato de agricultores que se oponen a la erradicación, por lo que pareciera que en **"el Catatumbo, el Putumayo y en Guaviare, el Ejército tiene el aval de disparar contra el campesinado que se opone a la erradicación forzada".** [(Lea también: Alejandro Carvajal, joven de 20 años asesinado por el Ejército en Catatumbo: ASCAMCAT)](https://archivo.contagioradio.com/alejando-carvajal-joven-de-20-anos-asesinado-por-el-ejercito-en-catatumbo-ascamcat/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El interés en la zona fronteriza

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Las organizaciones sociales también han advertido sobre las apuestas políticas del Gobierno y de EE.UU. en el Catatumbo por ser una región fronteriza con Venezuela, lo que ha sido usado como excusa de igual forma para aumentar la presencia de las fuerzas militares, además de tropas estadounidenses en el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Hassam Dodwell, director de Justice for Colombia,** organización que acompaña a las comunidades y organizaciones sociales en el país, afirma que en países como Gran Bretaña y Holanda, sindicatos y agrupaciones han denunciado la llegada de fuerzas militares extranjeras a suelo colombiano en respaldo a los congresistas que señalaron como inconstitucional el arribo de las tropas norteamericanas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Acuérdense que fueron Los Rastrojos quienes ayudaron a pasar a Juan Guaido el año pasado por la frontera, en la línea fronteriza hay todo un interés político, económico y estratégico" advierte Olga Quintero con relación a la connivencia que además existiría entre grupos armados leales e ilegales con el aval del Gobierno.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La comunidad internacional tiene su mirada sobre el Catatumbo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, Dodwell señala que "existe una situación de desigualdad social que se ha mantenido a través de la violencia y un apoyo institucional limitado" y que la mejor manera de responder a esta crisis humanitaria es implementar de forma completa el Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El director de **Justice for Peace** advierte, que aunque hay otras voces que "obedecen a los intereses de empresarios y maquinarias de guerra" y que buscan presionar al gobierno británico en sus relaciones con Colombia, hay un compromiso con el Acuerdo de Paz por parte de la comunidad, conscientes de los problemas en la implementación y de las denuncias que provienen de las zonas rurales del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Hay muchos parlamentarios que están comprometidos con este proceso y mantener esta presión, recordando la importancia de este proceso, haciendo preguntas al Parlamento Británico",** - explica -y aunque existe un gobierno conservador en Reino Unido, que puede ser afín a las políticas del gobierno Duque, la realidad de la situación de DD.HH. es tan extrema que es difícil ignorar la lenta implementación del Acuerdo de Paz".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
