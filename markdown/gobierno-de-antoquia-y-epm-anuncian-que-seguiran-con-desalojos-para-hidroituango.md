Title: Gobierno de Antoquía y EPM anuncian que seguirán con desalojos para HidroItuango
Date: 2015-08-31 15:01
Category: Movilización, Nacional
Tags: Antioquia, campesinos, Empresa Públicas de Medellín, EPM, Foro políticas minero energéticas, Hidroituango, Isabel Zuleta, ministerio de agriculura, Ministerio de Ambiente, Rios Vivos
Slug: gobierno-de-antoquia-y-epm-anuncian-que-seguiran-con-desalojos-para-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Hidroituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: agenciadenoticiasunal.edu.co] 

<iframe src="http://www.ivoox.com/player_ek_7718412_2_1.html?data=mJyempmVdo6ZmKiak5mJd6KklpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi9DWysrf0NSPqMafwtPh0dbZaaSnhp2xw5Ddb8bkzpDO0NrSp8rVz5De18qPt8bb1s7fh6iXaZmlz5DQ0dOPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [ Isabel Cristina Zuleta, miembro de Ríos Vivos] 

###### [31 Ago 2015] 

El debate sobre políticas minero energéticas dejó preocupantes alarmas frente a las constantes evasivas del Gobierno frente a Hidroituango y Empresas Públicas de Medellín (EPM) con la comunidad de 12 municipios impactados de manera directa y a más de 180.000 personas.

Isabel Cristina Zuleta, miembro de Ríos Vivos , habló sobre lo que dejó el foro “La Política Minero Energética al debate popular” se realizó con una asistencia de más de 700 afectados por Hidroituango, quienes dieron un balance negativo frente a lo adelantado con el gobierno debido a las negativas que dieron los delegados en temas como las víctimas y el desplazamiento.

Según el movimiento Ríos Vivos uno de los asuntos más preocupantes es que el secretario de gobierno del departamento de Antioquía respondió sobre los desalojos “*Que se iban a continuar haciendo los desalojos forzosos hasta que un Juez de la República lo impidiera*”

Las [denuncias](https://archivo.contagioradio.com/hay-cerca-de-180-mil-victimas-de-mega-proyecto-hidro-ituango/) por parte de la población pasan por los daños ambientales, la presencia paramilitar, los ataques contra Defensores de DH y el desplazamiento forzado que ha traído la construcción de Hidroituango y la participación de Empresas Públicas de Medellín (EPM), han sido cuantiosas.

Los representantes del gobierno se han mostrado intransigentes y evasivos frente a las soluciones, por ejemplo quitándole la protección a distintas personas que han denunciado amenazas en su contra , además que representantes del Ministerio de Medio Ambiente dijeron “que ellos no tenían ninguna competencia en el control, ni manejo de operaciones de las operaciones de EPM”. En el foro se presentaron respuestas similares por parte de otros estamentos del gobierno.

### **Movilizaciones:** 

En el marco de esta cumbre agraria se movilizarán en distintas regiones aproximadamente 700 personas principalmente entre Toledo e Ituango, por parte de la organización de Ríos Vivos.

Isabel Zuleta denuncia que pese a que las personas no han llegado al sitio de concentración, la policía ha iniciado algunos hostigamientos. “A las 8 am unas 30 personas, que estaban en el puente de comunicación entre los municipios de Ituango y Toledo estas personas son rodeadas por el ejército”

Sin embargo, la comunidad en su rechazo hacia estos proyectos minero energéticos han decidido no dar ingreso, ni ningún paso a ningún vehículo que sea de Hidroituango “La decisión es que si no se habla sobre afectados, estamos en toda la legitimidad para expresar este rechazo al mega proyecto”
