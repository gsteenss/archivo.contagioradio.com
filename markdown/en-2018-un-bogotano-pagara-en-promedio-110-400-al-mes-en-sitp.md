Title: En 2018 un bogotano pagará en promedio $110.400 al mes en SITP
Date: 2018-01-10 14:15
Category: Economía, Nacional
Tags: aumento pasaje transmilenio, Bogotá, Enrique Peñalosa, SITP, Transmilenio
Slug: en-2018-un-bogotano-pagara-en-promedio-110-400-al-mes-en-sitp
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/TransMilenio-Calle-100-RCN-9-copia-e1493944742569.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RCN ] 

###### [10 Ene 2018] 

El alcalde de Bogotá Enrique Peñalosa presentó el decreto por el cual **aumenta las tarifas para los pasajes** de Transmilenio y el Sistema Integrado de Transporte Público en la capital del país. El aumento se realizará bajo el argumento de que “los sistemas de transporte deben ser sostenibles” y que los precios se deben ajustar teniendo en cuenta el aumento de la inflación.

El decreto indica que el aumento será de \$100 pesos tanto para Transmilenio como para el SITP. Es decir que los usuarios de articulados pasarán de pagar **\$2.200 pesos a \$2.300** pesos y para hacer uso de los buses azules, los ciudadanos pasarán de pagar \$2.000 pesos a pagar \$2.100 pesos. (Le puede interesar: "[Salario mínimo para 2018 será de \$781.242](https://archivo.contagioradio.com/salario_minimo_2018_colombia/)")

Este borrador de decreto, que espera por la firma del alcalde, indica también que los transbordos del componente zonal hacia Transmilenio tendrán un costo de **\$200 pesos**. Además, reitera que los usuarios deben estar inscritos en la base de datos del sistema de transporte a través de la respectiva tarjeta para que se les aplique este costo, de lo contrario el usuario deberá pagar la tarifa máxima.

### **2017 fue el año de protestas contra Transmilenio** 

Durante el transcurso del año pasado los usuarios se movilizaron en varias ocasiones protestando por el **mal servicio de transporte** que tiene la ciudad. En marzo, por ejemplo, hubo bloqueos por varios días en diferentes troncales justamente por el aumento de los precios.

Los usuarios se han quejado también por la **demora en la frecuencia de las rutas**, la mala calidad en la que se encuentran los buses del SITP y Transmilenio, los derechos laborales de los conductores y las personas que trabajan en el sistema, la tercerización de los trabajadores, la renegociación de los contratos y los gastos que se hacen del patrimonio público. (Le puede interesar: "[Se aprueba cupo de endeudamiento para más Transmilenio en Bogotá](https://archivo.contagioradio.com/se-aprueba-cupo-de-endeudamiento-para-mas-trasmilenio-en-bogota/)")

### **Pese a descontento, Transmilenio por la séptima sigue en marcha** 

Adicional al incremento en los pasajes, Peñalosa continua firme con el desarrollo de la troncal de la carrera séptima a la cual **se ha opuesto la ciudadanía y algunos concejales**. En repetidas ocasiones, expertos le han manifestado al alcalde que ese sistema de transporte es desventajoso en la medida en que, por cada \$100 pesos que le entran al sistema \$90 llegan a manos de los dueños de los articulados, \$5 a los recaudadores y solo \$5 obtiene el Distrito.

El concejal Manuel Sarmiento, del Polo Democrático, ha denunciado que el proyecto de Transmilenio por la séptima **no cuenta con los estudios necesarios** y por el contrario ya ha generado una deuda 2.8 billones de pesos.

Sarmiento le ha dicho a Contagio Radio que algunos de los riesgos de permitir que se construya por allí este sistema de transporte tienen que ver con que esta carrera **es muy estrecha** para construir una troncal “generando un deterioro del entorno urbano de la séptima”. (Le puede interesar: ["Anuncian acciones legales y movilización contra Transmilenio por la séptima](https://archivo.contagioradio.com/ciudadanos-marcharan-contra-proyecto-de-transmilenio-por-la-septima/)")

Finalmente, el aumento al costo del pasaje se realizará una vez el alcalde firme el decreto. Sin embargo, los **descontentos aumentan** y los usuarios no perciben una mejora en la prestación del servicio.

[Proyecto de Decreto Tarifas Sitp 2018](https://www.scribd.com/document/368860159/Proyecto-de-Decreto-Tarifas-Sitp-2018#from_embed "View Proyecto de Decreto Tarifas Sitp 2018 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_23911" class="scribd_iframe_embed" title="Proyecto de Decreto Tarifas Sitp 2018" src="https://www.scribd.com/embeds/368860159/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-fpsga0Tw8Z5HgJvX523S&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

 
