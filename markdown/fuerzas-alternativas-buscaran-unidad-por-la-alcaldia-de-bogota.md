Title: Fuerzas alternativas buscarán unidad por la Alcaldía de Bogotá
Date: 2019-05-02 16:13
Author: CtgAdm
Category: Nacional, Política
Tags: Alcaldía de Bogotá, Angela Maria Robledo, Petro
Slug: fuerzas-alternativas-buscaran-unidad-por-la-alcaldia-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Angela-Robledo-y-Gustavo-Petro.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

Este jueves 2 de mayo, en medio de una rueda de prensa, **Gustavo Petro y Ángela María Robledo se refirieron a la pérdida de la curul para la Representante a la Cámara**, los pasos que seguirán para defender este escaño en el Congreso y el futuro político de Robledo. Ambos líderes acordaron que lo primero sería una prioridad para el movimiento político, mientras lo segundo se definiría en una asamblea distrital de la Colombia Humana.

Robledo y Petro indicaron que actuarían siguiendo dos caminos: la defensa del Estatuto de la Oposición, y de la curul obtenida por Robledo, como el resultado de la implementación del proceso de paz; y trabajando en lograr coaliciones de fuerzas alternativas para las elecciones regionales que permitan a la oposición convertirse en opción real de poder.

### **Quieren destruir la alternativa de poder**

Angela María Robledo explicó que la **Colombia Humana es un movimiento que piensa en un país con perspectiva de estar en paz, con justicia social** y tomando como elemento fundamental la defensa de lo público y el respeto por la Constitución; en ese proyecto político creyeron más de 8 millones de colombianos, y por eso, la Colombia Humana buscará defender la curul de Robledo.

Por su parte, Petro añadió que el caso de Robledo se presenta en el marco de una persecución al movimiento político que ambos integran; que se suma a una demanda interpuesta por Gloria Flórez, candidata al senado por la lista de Decentes, y quien pese a obtener los votos para ser parte de esta corporación, no recibió el aval del Consejo Nacional Electoral, y tuvo que recurrir a una demanda (que aún no se resuelve) para tratar de garantizar sus derechos.

De igual forma, el Senador expuso el peligro que se cierne sobre las curules de la circunscripción indígena, porque por un error, la Registraduría imprimió en el tarjeton la casilla del voto en blanco para Senado en la misma línea de los candidatos indígenas, y ahora se presenta una demanda alegando que los votos en blanco se produjeron pensando que se trataba de la casilla de voto en blanco específicamente para esta circunscripción.

### **Aún no descarta ir por la Alcaldía de Bogotá**

> Construir convergencias para juntar fuerzas independientes y ganar poder local...

Petro sostuvo que Colombia Humana no se constituye únicamente como oposición, sino como opción de poder, por esa razón tomaron la decisión de "construir convergencias para juntar fuerzas independientes y ganar poder local"; con esa idea en mente, la asamblea distrital del movimiento determinó buscar una sola candidatura de los sectores alternativos en Bogotá en torno a un acuerdo programático. Sin embargo, esa Asamblea no contempló que la opción para la Alcaldía fuera Ángela Robledo.

Tomando en cuenta que solo una asamblea del movimiento podría tomar esta decisión, ambos líderes bromearon al señalar que **están como Fajardo: ni sí ni no**, han determinado que Robledo sea la candidata de Colombia Humana para la Alcaldía de Bogotá. Pero aclararon que en caso de que así fuera, respetarían la idea de buscar una coalición con sectores independientes y con la idea en mente de una convergencia.

> Estamos absolutamente firmes en nuestra coalición ciudadana para ganar la Alcaldía de Bogotá unidos Verdes, ACTIVISTA, Polo y Colombia Humana.
>
> ¡Tendremos candidatura única para enfrentar a todos los candidatos de continuidad de Peñalosa! <https://t.co/WXzlP2xaX4>
>
> — Luis Ernesto Gómez? (@LuisErnestoGL) [2 de mayo de 2019](https://twitter.com/LuisErnestoGL/status/1123995298559340544?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
 

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
