Title: Premio Nacional a la defensa DD.HH. 2020: ganadores y ganadoras
Date: 2020-12-02 12:41
Author: AdminContagio
Category: Nacional
Slug: premio-nacional-a-la-defensa-dd-hh-2020-ganadores-y-ganadoras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/PREMIOS-DEFENSORES.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/ganadores-defensores.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Leyner-palacios-ganador.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/ganadores-defensores-pesos-politicos.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/ganadores-defensores-marino.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/ganadores-defensores-marino-1.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: **Archivo Contagio Radio**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este miércoles 2 de diciembre se realizó la [IX edición del Premio Nacional de Derechos Humanos en Colombia 2020](https://www.facebook.com/PremioDDHHColombia/videos/3041052639455185), los premios que galardonan y **reconocen el trabajo de defensores y defensoras de Derechos Humanos** **en Colombia**; entre ellos se encuentran; asociaciones, colectivos, ONG, líderes y lideresas, de distintas comunidades alrededor del país, que trabajan por los territorios, el crecimiento de las comunidades y en general  su compromiso por  los derechos humanos. (Le puede interesar: [Ganadores y ganadoras del Premio Nacional a la defensa DD.HH. 2019](https://archivo.contagioradio.com/ganadores-del-premio-nacional-a-la-defensa-de-derechos-humanos-2019/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Categoría **1: «Defensor o Defensora del Año»**
-----------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la categoría «Defensor del Año**»**, el ganador fue el comisionado **[Leyner Palacios](https://archivo.contagioradio.com/leyner-palacios-representara-el-legado-de-las-victimas-ante-la-comision-de-la-verdad/)**, sobreviviente de la tragedia en el municipio de Bojayá y defensor de la paz y de los derechos humanos en la región del Pacífico desde hace más de 20 años. Además, es impulsor de la implementación del acuerdo de paz con las Farc.

<!-- /wp:paragraph -->

<!-- wp:image {"id":93565,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Leyner-palacios-ganador.png){.wp-image-93565}  

<figcaption>
Foto: Premio Nacional a la Defensa de los Derechos Humanos en Colombia

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading -->

**Categoría 2A: «Experiencia o Proceso Colectivo del Año (Proceso Social Comunitario)»**
----------------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

**La Guardia Indígena Kiwe Thegnas del Plan de Vida Proyecto Nasa. Experiencia Asociación de Cabildos Indígenas de Toribío, Tacueyó y San Francisco** se llevó el reconocimiento a la Experiencia o Proceso Colectivo del Año. En la premiación, una de las integrantes de la guardia indígena expreso que **«no es fácil para nosotras las mujeres dadoras de vida tener que levantarnos día a día, salir y no saber si vamos a volver a nuestras casas o a volver a ver a nuestros hijos, sin embargo salimos para seguir construyendo los derechos humanos, de la vida y del territorio».**

<!-- /wp:paragraph -->

<!-- wp:image {"id":93564,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/ganadores-defensores.png){.wp-image-93564}  

<figcaption>
Foto: Premio Nacional a la Defensa de los Derechos Humanos en Colombia 

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

**Categoría 2B: «Experiencia o Proceso Colectivo del Año (Modalidad de ONG)»**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esta categoría, el galardón se lo llevó L**a Fundación Comité de Solidaridad con los Presos Políticos (FCSPP)**, organización creada con el objetivo de fomentar el respeto de los derechos de las personas privadas de la libertad por delitos políticos.

<!-- /wp:paragraph -->

<!-- wp:image {"id":93566,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/ganadores-defensores-pesos-politicos.png){.wp-image-93566}  

<figcaption>
Foto: Premio Nacional a la Defensa de los Derechos Humanos en Colombia

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading -->

**Categoría 3: **«**Reconocimiento a toda una vida»**
-----------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Marino Córdoba Berrío fue el galardonado en esta categoría, como un reconocimiento a su labor como defensor de los derechos del pueblo negro en Colombia y quien fundó la Asociación Nacional de Afrocolombianos Desplazados (afrodes) visibilizando la tragedia humanitaria de las víctimas de desplazamiento forzado.

<!-- /wp:paragraph -->

<!-- wp:image {"id":93574,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/ganadores-defensores-marino-1.png){.wp-image-93574}

</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->
