Title: Comunicación para la paz: un desafío aún pendiente
Date: 2017-08-04 12:23
Category: invitado, Opinion
Tags: comunicacion, Derechos, medios
Slug: comunicacion-paz-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/buenvantura-6-e1501865222525.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### Por: [[Tomás García Laviana].] 

###### 04 Ago 2017 

[En Colombia, a través de los años e intensos procesos, han surgido un sinnúmero de personajes que han configurado formas y caminos de aprehensión y representación del mundo, desde acciones de lectura de la realidad de forma crítica, propiciando y promoviendo espacios comunicativos alternativos.]

[Esos recorridos se han transitado teniendo en cuenta las necesidades de la sociedad y las coyunturas de un país dominado por la maquinaria comunicativa y el periodismo voraz que permea, cosifica y desestabiliza a los individuos, ofertando des-información, distractores, construcciones sociales de la realidad que no permiten tomar postura desde dinámicas de consciencia.]

[En ese sentido se reconoce y reivindica a todos los que se han comprometido con la tarea de informar y comunicar desde la realidad, desde la verdadera razón del periodismo: ¡la verdad!]

[Teniendo en cuenta esto, el próximo 13 de agosto se realizará la conmemoración de la siembra del abogado, filósofo, humorista, activista, periodista y ante todo mediador de paz Jaime Garzón Forero, quien fue asesinado por los enemigos de la paz, la justicia y la verdad. Un hombre que instauró una propuesta comunicativa desde el humor inteligente y  que supo ser un crítico de los medios hegemónicos aun desde sus propias pantallas.  En ese entendido se puede decir que su legado tiene algo imprescindible: la “reflexión”, pues se convierte en el ambiente propicio para que, como pueblo, reflexionemos sobre el papel que juegan los medios de comunicación y los emporios de la información, en la conformación de redes de la sociedad y, por ende, de la forma de relacionarse, sea esta pacífica o altamente conflictiva.]

[Así mismo, en un mundo que está totalmente codificado, el lenguaje como aspecto esencial de la comunicación, puede prestarse para manipular y polarizar la sociedad, afectando la cultura en tanto la mercantiliza y banaliza, igual que la democracia y los bienes comunes que la deben configurar.]

[Colombia no es ajena a esta dinámica y en su medio de descomposición, se construye, funciona y actúa a través de ese lenguaje con fines políticos. Todo ello se da en un escenario social caracterizado por la alta concentración del poder y la riqueza, así como por la fragmentación e insolidaridad, motivo por el cual los medios de comunicación son factores esenciales e incidentes en la solución de conflictos, en su prolongación y no resolución, pudiendo y debiendo actuar en otro sentido, pues es desde allí que se reconfiguran los mismos, donde se propende por la construcción de estrategias, propuestas y acciones que permitan avanzar o retroceder en la consecución de un verdadero desarrollo humano, lo más anhelado en el país; sin embargo, la realidad colombiana nos muestra unas empresas de comunicación al servicio de la guerra y del expolio, que generan y promueven conflictos nacionales e incluso internacionales.]

[A su vez, la manera cómo se maneja el lenguaje de la comunicación para la construcción de estos discursos de violencia, debe ser confrontado por esa amplia mayoría que está a favor de la paz y la verdad, a través de medios que respondan a las necesidades propias de los pueblos de Colombia, y en la coyuntura actual, de medios que sirvan a la verdad, a la construcción de memoria, de educación y de una solución pacífica a los conflictos, que no promueva la “]*[pacificación]*[” como acallamiento de las diferentes voces. El ejercicio consciente y responsable del periodismo debe subordinarse al bien común, estar al servicio de las mayorías sustituyendo la perversidad y la mezquindad del sistema económico individualista; cuya tarea sea la identidad, el humanismo social y la resignificación de las culturas que habitan Colombia.]

**Entender la comunicación como un bien común**

[La]***comunicación para la paz***[ debe ser entendida como un ejercicio de poder popular, que propicia la formación y construcción colectiva de saberes, propendiendo por la posibilidad y responsabilidad de ser sujetos políticos participantes de la consolidación de una paz con trasformaciones, es decir que sea una comunicación que garantice búsqueda permanente de soluciones a los grandes problemas que afectan al país y que sólo se legitima en la medida que los colombianos y colombianas vean los beneficios de una información desintoxicada del lenguaje de la guerra.]

[En este sentido es fundamental la concertación y un plan estratégico donde confluyan periodistas independientes, alternativos, populares, que junto a los sectores que se encuentran organizados, pugnen por un proyecto de nación, entendida ésta como la posibilidad de tejer redes sociales desde la colectividad y cada uno de sus actores, donde se trabaje por un cambio real y estructural para  Colombia, sin segregaciones, entendiendo y realizando el ejercicio del periodismo ético como una parte del gran proyecto de las diferentes agremiaciones que se dignifican desde ese posicionamiento resistente.]

[Es así que en Colombia más de tres millones de personas no tienen acceso a la internet o no cuentan con medios de comunicación regionales (FLIP, 2016); a la vez la concentración de medios es abrumadora: solo tres grupos empresariales concentran el 57% de la audiencia en los distintos soportes comunicativos: radio, internet y prensa. (MONITOREO DE MEDIOS, RSF 2016), por lo cual la fuente de información casi exclusiva y por ende excluyente con la que cuenta la sociedad es la que ofrecen los medios tradicionales al servicio de las elites, a través de los grandes medios que controlan económica y políticamente, los cuales resultan estando a favor de poderes económicos y políticos que por su naturaleza opresora merecen ser afrontados.]

[De esta forma es importante exponer a la par y como condición de posibilidad de ese genocidio político en curso, que en los últimos cuarenta años han sido asesinados más de 150 periodistas, que tienen un común denominador: “Las víctimas adelantaban investigaciones que evidenciaban corrupción en el poder público y militar” (CNMH,2015). Sin contar que en lo transcurrido del 2017 más de 200 periodistas han sido víctimas de violaciones a sus derechos bajo la forma de  amenazas, acoso judicial, detención ilegal, estigmatización, hurto de material periodístico, entre otras acciones que ponen en riesgo la labor periodística (FLIP,2017).]

**Paz también es democratizar los medios de comunicación**

[En efecto, el buen vivir no debe ser monopolio de quienes llevan las riendas del poder político y económico del Estado y la consolidación de un país con prosperidad e igualdad debe propender por el rescate y construcción de la memoria histórica, considerándose ésta como un valor y un beneficio colectivo que fomenten las estructuras de una organización social digna.]

[Para esto es necesario la recuperación de espacios que garanticen la asociación, en sentido colectivo de los medios para las comunidades, que permitan sus propias formas de desarrollar la comunicación a través de una legislación efectiva y de garantías que responda a la democratización y los recursos necesarios para la comunicación, así como la igualdad de oportunidades para asegurar a las comunidades el acceso al espectro electromagnético, permitiendo de esta manera desarrollar una comunicación proactiva que beneficie a las comunidades, y cree verdaderas condiciones de seguridad en el ejercicio del periodismo, así como el respeto a la libertad de prensa.]

[En ese caso es necesario y urgente reglamentar un nuevo marco legal para la comunicación dentro de la sociedad colombiana que, quizá, sea referente del presente de nuestro país.]

[En conclusión, es imperativo realizar un ejercicio constante y consciente de reivindicación en memoria del legado de Jaime Garzón y de muchos otros periodistas que han abonado con su sangre y su vida el ejercicio del periodismo ético, responsable e independiente; es momento de que en conjunto, un periodismo organizado, aporte las condiciones necesarias para que el ejercicio ético de la profesión desarrolle una]***pedagogía de paz***[ que aporte a la formación de los comunicadores en campos y ciudades, en los diferentes contextos y rincones del país, brindando información, material que les permita obtener insumos fundamentales en la toma de decisiones ecuánimes, honestas, transparentes y coherentes con la labor comunicativa, que apunten al buen vivir y posibiliten transitar a una nueva cultura política de emancipación, bienes comunes y cuidado de nuestro habitar en el planeta. Los retos entonces son definitivos e históricos.]

##### ![Laviana](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Laviana-e1501867069978.jpg){.alignnone .size-full .wp-image-44698 .alignleft width="75" height="100"}

##### [**Tomás García Laviana**]  
*Productor de imagen, realizador audiovisual, comunicador popular,*  
*Integrante de la Delegación para los Diálogos de paz del ELN*.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
