Title: Policía investigado por muerte del grafitero Diego Felipe Becerra podría recibir ascenso
Date: 2019-02-27 15:29
Author: ContagioRadio
Category: Judicial, Nacional
Tags: asesinato joven grafitero, Diego Felipe Becerra, Gustavo Trejos, John Harvey Peña, Padre Diego Felipe Becerra
Slug: policia-investigado-muerte-del-grafitero-diego-felipe-becerra-podria-recibir-ascenso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/diego-felipe-becerra1-contagio-radio-e1534672072851-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Tiempo] 

###### [27 Feb 2019] 

El Teniente Coronel John Harvey Peña podría ser ascendido al rango de Coronel el próximo mes a pesar de que la Fiscalía General de la Nación lo investiga **por manipular presuntamente la escena del crimen** donde el grafitero Diego Felipe Becerra fue asesinado, en el 2011.

En el 2014, la Fiscalía imputó cargos al Teniente Coronel Peña y a los subintendentes Fléiber Leandro Zarabanda y Nelson Giovanni Tovar, por **el favorecimiento al homicidio, tráfico y porte ilegal de armas, destrucción y alteración del material probatorio, falso testimonio y fraude procesal**, delitos por los cuales podría enfrentar una pena de hasta 15 años de prisión.

Trejos afirmó que **la Constitución establece que los oficiales que estén bajo investigación disciplinaria o estén acusados formalmente frente a un juez no podrán ser ascendidos**. Sin embargo, este oficial está bajo consideración para el ascenso, que sería su segundo ascenso en los últimos ocho años desde el crimen.

"Nosotros estamos esperando que decisión toma la Policia y de ahí tomaremos las medidas pertinentes para evitar el ascenso," afirmó Trejos. Unas de estas medidas podría ser una demanda en contra de la Policía en que acusarían a esta entidad de violar normas de la Constitución.

Según Gustavo Trejos, padre del joven, las pruebas ponen en evidencia que el Teniente Coronel estuvo presente en la escena del crimen y sabía que Becerra no había portado un arma cuando fue asesinado por el patrullero Wilmer Alarcón. El testimonio de un oficial indica que en** la versión original del informe policial declaró que no se encontró un arma en la escena del crimen.**

Después de pasar por las manos de Peña, el informe reporta un arma y un presunto robo de un bus cometido por Becerra. Por tal razón, la Fiscalía lo acusó **de elaborar un operativo de encubrimiento**, en el que se compró un arma para presentar al joven de 16 años como un delincuente. "Hizo esto para poder encubrir el falso positivo de Diego Felipe," Trejos dijo. (Le puede interesar: "[Nos defendemos con colores, un año más sin 'Trípido](https://archivo.contagioradio.com/7-anos-sin-tripido/)'")

Además, Peña es acusado de desaparecer un libro del CAI La Alhambra, donde se registraron todos los hechos en torno a la muerte de Becerra. Trejos afirma que para **este año, entre mayo y agosto, se espera el juicio del Teniente Coronel Peña** y los otros coroneles que estuvieron presentes en la escena del crimen.

<iframe id="audio_32899103" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32899103_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
