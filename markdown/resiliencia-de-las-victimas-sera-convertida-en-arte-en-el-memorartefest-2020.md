Title: Resiliencia de las víctimas será convertida en arte en el MemorArteFest 2020
Date: 2020-09-14 11:26
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Comisión de la Verdad, MemorArteFest, víctimas del conflicto armado
Slug: resiliencia-de-las-victimas-sera-convertida-en-arte-en-el-memorartefest-2020
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-13-at-2.59.27-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/49089974298_8f364b9c37_c.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-13-at-2.59.27-PM-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: MemorArteFest

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Durante 40 días y del 14 de septiembre al 31 de octubre 2020, el mundo podrá participar e interactuar con el Primer Festival de la Memoria y el Arte, un evento en homenaje a las víctimas del conflicto armado en Colombia a través de la creación y del arte basada en sus testimonios, una propuesta que surge desde el nodo Italia de la Comisión de la Verdad, como parte de su mandato.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según explica Gloria Mendiola, vocera del del MemorArteFest 2020, la resilencia será la espina dorsal del Festival, junto con las voces de las víctimas del conflicto armado, "queremos exaltar y dar un lugar preponderante a la capacidad humana de renacer, de recrear y transformar la propia realidad", expresa. [(Le puede interesar: Compromiso de la Comisión de la Verdad es poner a la luz la resistencia afrocolombiana)](https://archivo.contagioradio.com/compromiso-de-la-comision-de-la-verdad-es-poner-a-la-luz-la-resistencia-afrocolombiana/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La vocera señala que más 58 años de guerra vividos en Colombia no son una historia fácil de narrar, tanto dentro como fuera del país, y en países como Italia, lugar en el que nació el festival pero donde la "realidad, consecuencias y causas" del conflicto no son un tema cotidiano, por lo que el mayor desafío es el de relatar los sucesos de violencia que han marcado a la nación desde otras perspectivas y otras visiones.

<!-- /wp:paragraph -->

<!-- wp:image {"id":89835,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-13-at-2.59.27-PM-1.jpeg){.wp-image-89835}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

"Queremos dar voz a quienes han vivido en primera persona este conflicto y transformar sus testimonios, no en un monumento al dolor sino en contar con un gran respeto y humildad sus historias de resiliencia", explica [(Lea también: Así fue encuentro de diálogo entre víctimas y Salvatore Mancuso)](https://archivo.contagioradio.com/asi-fue-encuentro-de-dialogo-entre-victimas-y-salvatore-mancuso/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El evento, liderado entre otros por el nodo Italia de la [Comisión de la Verdad,](https://comisiondelaverdad.co/) apoyado por organizaciones como la **Associazione di Promozione sociale MIGRA**, **Europaz, Alas y el Instituto Catalán Internacional, también cuenta con la diversa participación de los 23 nodos que tiene la Comision en América Latina y la comunidad Europea**, asumiendo la tarea como un compromiso con la verdad histórica para que juntos realicen "una narración colectiva no solo del conflicto y su impacto en la sociedad sino de la capacidad de transformar las duras realidad que vive el país en otro tipo de relatos".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La programación del MemorArteFest contará con diversos seminarios web, conversatorios, conciertos, entrevistas, cineforos, homenajes, relatos, cine comunitario y talleres de formación con los que buscarán crear una reflexión y tejer memoria, "creemos que es un momento coyuntural importante y necesario para que estas acciones se sumen a todas las que se realizan en el exterior junto a la COmisión de la Verdad", concluye.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
