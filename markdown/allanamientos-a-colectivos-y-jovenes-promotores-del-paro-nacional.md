Title: Allanamientos a colectivos y jóvenes promotores del Paro Nacional
Date: 2019-11-19 10:08
Author: CtgAdm
Category: DDHH, Paro Nacional
Tags: Allanamientos, Bogotá, Cali, Medellin, Paro Nacional
Slug: allanamientos-a-colectivos-y-jovenes-promotores-del-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/allanamientos-en-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Congreso de los Pueblos ] 

A lo largo de la mañana se han desarrollado hasta el momento 17 operativos de allanamiento a defensores de derechos humanos, integrantes de colectivos artísticos y estudiantes en Bogotá, Cali y Medellín, alertan diferentes organizaciones que denuncian, se trata de un accionar por parte del Gobierno, previo a la movilización del 21 de noviembre.

**Sebastián Quiroga**, politólogo de Ciudad en Movimiento y Congreso de los Pueblos relata que cerca de las 6:00 am se reportaron siete allanamientos en Bogotá contra integrantes de Congreso de los Pueblos y el Grupo Estudiantil Anarquista en los que la Policía indicó que era parte de la investigación por la quema del edificio del Icetex el pasado 27 de septiembre, suceso que se comprobó mediante evidencia, fue responsabilidad de integrantes de la Fuerza Pública. Tras  la irrupción de la  Policía, no se halló nada en las casas de las personas que fueron allanadas. Ver: [Con tutelas, altos mandos militares quieren censurar mural sobre falsos positivos](https://archivo.contagioradio.com/con-demandas-altos-mandos-militares-quieren-censurar-mural-sobre-falsos-positivos/)

Además de los allanamientos en contra de miembros estas organizaciones, también se alertó sobre situaciones similares en las casa de varios de los artistas que trabajaron en la creación del mural de la Campaña por la Verdad que denuncia la responsabilidad de la Fuerza Pública en las más de 5763 casos de ejecuciones extrajudiciales en Colombia ocurridos entre 2000 y 2010.

El politólogo señala que se trata de **"ejercicios sistemáticos en el marco de la coordinación bélica y hostil del Gobierno contra el paro"** agregando que se ha creado un discurso alrededor de los riesgos del paro, "los que están incendiando el país antes que empiece el paro son ellos, lo que está haciendo es caldear los ánimos intencionalmete antes de esta jornada de movilización ".

> La fuerza pública persigue al movimiento social. En éste momento se realizan allanamientos en diferentes puntos de Bogotá contra activistas sociales y defensores de DDHH del Congreso de los Pueblos, quienes participarán en las movilizaciones del [\#21NParoNacional](https://twitter.com/hashtag/21NParoNacional?src=hash&ref_src=twsrc%5Etfw) [@DefensoriaCol](https://twitter.com/DefensoriaCol?ref_src=twsrc%5Etfw) [pic.twitter.com/Wg01ApPHZb](https://t.co/Wg01ApPHZb)
>
> — Congreso de los Pueblos (@C\_Pueblos) [November 19, 2019](https://twitter.com/C_Pueblos/status/1196762546147078145?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Con respecto a los allanamientos en otras ciudades como Cali y Medellín, la información que se conoce por ahora es que se habrían efectuado seis allanamientos contra estudiantes de la Universidad del Valle, sin embargo la Fiscalía de Cali  ha afirmado que no participó en los operativos efectuados en la ciudad.

> Allanan viviendas de integrantes del [@C\_Pueblos](https://twitter.com/C_Pueblos?ref_src=twsrc%5Etfw), estudiantes y artistas que realizaron mural dedicado a generales del ejército implicados en ‘falsos positivos’. La campaña alarmista del gobierno sobre el paro del 21 de noviembre sirve para justificar violaciones de DDHH
>
> — Iván Cepeda Castro (@IvanCepedaCast) [November 19, 2019](https://twitter.com/IvanCepedaCast/status/1196795072743952391?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
 

Noticia en desarrollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
