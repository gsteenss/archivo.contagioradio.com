Title: Sí hubo censura a Santiago Rivas y los Puros Criollos
Date: 2019-01-23 15:23
Author: AdminContagio
Category: DDHH, Política
Tags: Censura, Santiago Rivas los puros criollos
Slug: si-hubo-censura-a-santiago-rivas-puros-criollos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/censurado-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Dos y Punto] 

###### [23 Ene 2019] 

Aunque el  Gerente de RTVC, Juan Pablo Bieri, había afirmado que nunca se enteró de una posible censura al programa “Los Puros Criollos” ni en Señal Colombia ni en el Sistema de Medios Públicos, este 23 de enero se dio a conocer por parte de la FLIP un audio en el que cuatro personas acuerdan retirar el programa de la parrilla e impedir que **Santiago Rivas trabaje de nuevo en el Canal Público.**

Esta conversación se habría dado el mismo día en que la reconocida “La Pulla” publicó un video en el que se observa a Santiago Rivas criticar y rechazar las leyes que se estaban discutiendo en el congreso y afectaban la televisión pública en el país, proyecto que también fue criticado por amplios sectores de la sociedad, razón por la cual fue aplazado para una próxima legislatura.

Ante esta situación Medios de Comunicación, alternativos y populares manifestaron "El Proyecto de Ley No 234 de 2018 “Por medio de la cual se reconoce la profesión de Comunicación Social – Periodista y Organizacional, se crea el Consejo Profesional del Comunicador Social – Periodista y Organizacional y se dictan otras disposiciones”.

Cuya exigencia de profesionalización derivaría en una suerte de elitización del ejercicio periodístico, atentando contra la libertad de expresión y de prensa. Así mismo, la creación de dicho Consejo con asiento del gobierno para asignar o suspender “credenciales” llevaría al riesgo de censurar el ejercicio periodístico ciudadano..."

Por su parte la FLIP aseguró que el gerente de RTVC no cumple con los requisitos para seguir al frente del sistema de medios públicos "Con esta información sobre la mesa, en la FLIP no encontramos cómo sustentar que Juan Pablo Bieri cumpla con las credenciales necesarias para gerenciar el sistema de medios públicos de Colombia."

A continuación presentamos algunos apartes del audio en el que se escuchan las voces de cuatro personas entre ellas el Gerente de RTVC Juan Pablo Bieri.

***Diana Díaz: ****Pues nosotros tenemos ¿Qué?, una serie, una temporada nueva de Los Puros Criollos por estrenar.*

***Juan Pablo Bieri:**** ¿Con él?*

***Diana Díaz****: Sí.*

***Juan Pablo Bieri:**** No puede ser.*

***Diana Díaz:**** Pero la cosa es la siguiente. No podemos no emitir.*

***Juan Pablo Bieri:**** ¿No podemos qué?*

***Diana Díaz:**** No podemos no emitir. Porque eso hace parte de…*

***Juan Pablo Bieri:**** ¿Pero ya lo vamos a hacer?*

***Diana Díaz:**** Claro. Eso ya está entregado, ya está finalizado, ya está pagado, ya está todo.*

*-2:14*

***Juan Pablo Bieri:**** Es muy difícil de hacer eso. Le vamos a cambiar el horario. ¿No sé qué hacemos ahí? Oriéntame. Este señor está haciendo unas cosas…*

***Martín Pimiento: ****Es que, es que realmente, a ver. Uno se encuentra Diana en una disyuntiva con el personaje, no porque sea bueno no porque sea malo, sino porque siente uno que tiene el (¿?; 2:35) del canal, si él hace parte de una producción financiada con recursos del Estado, lo mínimo que él puede hacer es guardar silencio frente a las políticas públicas que expone ese Estado, ¿si? Si RTVC...*

***Juan Pablo Bieri:**** Le está Pagando.*

***Martín Pimiento:**** Si él y su equipo están convencidos de que el proyecto de ley para organizar las tic, un huevón que no ha estudiado ingeniería de sistemas, no puede salir a decir lo que dijo. ¿Ok? Entonces nos encontramos en una disyuntiva moral, qué hacer con el tipo que (¿?; 3:07) ¿Cómo hacer para que Los Puros Criollos no vuelvan tener algo así?*

***Diana Díaz:**** No, es que no. En este momento ya no hay producción de Los Puros Criollos, esa serie, esa temporada…*

***Martín Pimiento:**** ¿Cuánto demora esa temporada?*

***Diana Díaz:**** Esa temporada son 12 capítulos.*

***Martín Pimiento:**** Imagínese.*

***Juan Pablo Bieri:**** ¿12 capítulos al aire significa cuánto tiempo?*

***Diana Díaz:**** Son dos semanas, tres semanas. Tres semanas.*

***Juan Pablo Bieri:**** ¿Y empiezan a emitirse cuándo?*

***Diana Díaz:**** En enero.*

***Juan Pablo Bieri:**** ¿En enero? ¿En qué horario?*

***Diana Díaz:**** 7:30 de la noche.*

*-3:42*

***Juan Pablo Bieri:**** Lo cambiamos de horario. Matamos la producción. Lo ponemos tres de la mañana, no tengo ni idea, pero no puede ser, él no sabe, digamos no tiene ni idea de lo que está diciendo, y segundo se está burlando del Estado, se está burlando de la entidad, que le da de comer y le paga un sueldo, eso me parece una parte un poco (¿?; 4:07), y vamos a ver en qué horario metemos a Los Puros Criollos en la temporada.*

***Martín Pimiento:**** ¿No hay cómo evitar la participación?*

***Diana Díaz:**** No.*

***Alejandra Cendales:**** Es el presentador.*

***Diana Díaz:**** Sí todo está articulado. Y honestamente sería más contraproducente que Los*

*Puros Criollos salieran sin Santiago*

***Martín Pimiento:**** Porque es que la serie es muy buena (¿?; 4:28).*

*-4:29*

***Juan Pablo Bieri:**** No, la serie se acaba.*

***Diana Díaz:**** No, la serie no se... o sea la serie ya terminó la temporada hace rato, ellos entregaron capítulos hace rato.*

***Juan Pablo Bieri:**** Y si se vuelve hacer Los Puros Criollos, se hace con otra persona. No hay posibilidad de que él lo vuelva a ser. No hay posibilidad que en esta gerencia Santiago vuelva a trabajar con esta gerencia, con esta empresa, en ninguna coproducción o producción.*

***Martín Pimiento****: (¿?; 4:46)*

***Juan Pablo Bieri****: No, falta de seriedad.*

***Martín Pimiento:**** Porque uno acepta la crítica constructiva, eso es lo que permite, construye país, eso lo que permite construir medios públicos, pero la cosa es que la crítica negativa, la crítica mediocre, panfletaria, para mí en lo personal y para Juan Pablo es inconcebible.*

*-5:03*

***Juan Pablo Bieri:**** No además es que, ¿tú viste lo de La Pulla?*

***Diana Díaz:**** No. Vi que algo publicaron, ¿pero eso fue hoy no?*

***Juan Pablo Bieri:**** Sí.*

***Diana Díaz:**** No he tenido chance de verlo.*

***Juan Pablo Bieri:**** Lo de La Pulla es que...Ni, ósea, ni este señor que fue elegido políticamente por el pueblo por las grandes mayorías, ni esta empresa que está, venga yo no estoy dispuesto a aguantarme el sonsonete ni de La Pulla y ni de él mucho menos, de La Pulla que diga lo que quieran…*

***Diana Díaz****: ¿Pero es que habla de RTVC? Es que no lo he visto.*

***Juan Pablo Bieri:**** No habla de RTVC, pero habla del proyecto de ley, pero hace quedar, digamos si RTVC está a favor del proyecto de ley, y crítica el tweet donde RTVC respalda el proyecto de ley, como lo hacen los canales regionales también, no sólo nosotros.*

***Martín Pimiento:**** Hombre tenga la mínima decencia y renuncie a RTVC.*

***Juan Pablo Bieri:**** O*

*Alejandra Cendales Porque es un tweet que saca RTVC. Con la ley. Él dice: “En serio ¿ustedes? Yo sé que son una entidad estatal pero al menos un elegante silencio se les habría agradecido, nada de lo que dicen en ese tweet está garantizado en el articulado, mientras que la dependencia del gobierno y la desfinanciación son claras. Antes eras chévere”. Es del tweet de nosotros.*

***Juan Pablo Bieri:**** Claro.*

***Martín Pimiento:**** Claro pero con un (¿?; 7:03). Si él cree que esto antes era chévere, váyase.*

***Diana Díaz:**** Es que no está. A lo que voy es que…*

***Martín Pimiento:**** Renuncie a la producción que tiene contrato con nosotros.*

***Diana Díaz:**** ¿Cómo? Es que ya está cerrado, ósea ya no está vinculado. Eso ya está liquidado, eso ya está entregado.*

***Martín Pimiento****: Conclusión no lo vamos a ver más.*

***Diana Díaz:**** Exactamente.*

*-7:24*

***Juan Pablo Bieri: ****Acá no, pero al aire sí.*

***Diana Díaz:**** Al aire sí claro. Sí porque falta estrenar esa serie, esa temporada que son 12 capítulos, y lo que no podemos es no sacar al aire, porque hay una directriz de la ANTV, que tiene salir al aire antes del 31 de marzo del siguiente año.*

***Martín Pimiento:**** Por ficha.*

***Diana Díaz: ****Por ficha exactamente. Por normativa de la ANTV.*

***Juan Pablo Bieri: ****¿Antes del 31 de marzo del próximo año?*

***Diana Díaz:**** Sí.*

***Juan Pablo Bieri:**** Bueno, esperemos tres semanas antes del 31 de marzo del próximo año para sacarlo.*

***Diana Díaz: ****¿Para sacar el estreno de Los Puros Criollos?*

***Juan Pablo Bieri:**** Sí.*

***Diana Díaz: ****Ok.*

***Juan Pablo Bieri: ****Alargamos la serie hasta allá, y antes de que se venza la sacamos tres semanas antes y lo sacamos en un horario, puede ser…*

***Alejandra Cendales:**** Tres de la mañana.*

***Juan Pablo Bieri:**** No, puede ser dos de la mañana.*

***Martín Pimiento: ****Sí. La obligación es emitir.*

***Juan Pablo Bieri:**** La obligación es emitir.*

***Martín Pimiento:**** Listo*

*-9:03*

***Juan Pablo Bieri: ****Y espero que de aquí a marzo ya la ANTV no exista, pero si existe todavía, entonces justo lo que nos exige la ANTV se saca Los Puros Criollos en un horario en el que, el horario lo miramos, lo miramos, ¿cuándo estaba para salir esta serie, esta temporada?*

***Diana Díaz:**** Estaba para estrenar en enero.*

***Juan Pablo Bieri:**** ¿Al aire en qué temporada estamos?*

***Diana Díaz: ****Estamos con varias temporadas. Es ancla de las 7:30 de la noche, 9:30.*

***Juan Pablo Bieri:**** O sea, al aire…*

***Diana Díaz:**** Es que no tenemos tanto contenido… La situación es la siguiente no tenemos tanto contenido nacional ahí tenemos como sesenta y pico, setenta y pico de capítulos con las temporadas anteriores de Los Puros Criollos, y nos apalanca el prime que tiene una cuota nacional.*

***Juan Pablo Bieri: ****¿Qué es cuota nacional?*

***Diana Díaz: ****Que tenemos que acoplar. ¿Qué?*

***Juan Pablo Bieri:**** ¿Cómo así cuota nacional?*

***Diana Díaz:**** 70 de contenidos nacionales, 30...*

***Martín Pimiento:**** Nosotros para poder emitir tenemos (¿?; 10:00) un mínimo de programas nacionales, de productos realizados y producidos en Colombia.*

***Juan Pablo Bieri: ****Listo, entonces vamos a meter lo de promo.*

***Diana Díaz: ****¿Ah bueno cuándo llegue, cuándo esté listo?*

***Juan Pablo Bieri:**** ¿O empezamos a ver? Mejor dicho ¿la temporada de Puros Criollos, la primera ya salió?*

***Diana Díaz:**** Eso es lo que está al aire. Todo eso es lo que está al aire.*

***Juan Pablo Bieri:**** Por eso, ¿ya terminó el ciclo o estamos repitiendo?*

***Diana Díaz:**** Sí claro. No no no, no tenemos estrenos de Los Puros Criollos.*

*10:26*

***Juan Pablo Bieri:**** Por eso, entonces no repitamos más Los Puros Criollos. Sale de parrilla, lo que ya se emitió sale de parrilla, y búscate qué tenemos para cumplir la cuota.*

***Diana Díaz:**** OK.*

***Martín Pimiento:**** Mientras metemos…*

***Juan Pablo Bieri: ****Mientras metemos otra cosa.*

***Martín Pimiento: ****En ese horario, precioso horario además, queda perfecto lo de la (¿?;*

*10:45).*

***Juan Pablo Bieri:**** Ojala que lo más pronto lo tengamos.*

***Martín Pimiento: ****¿La de Troya puede meterla ahí?*

***Diana Díaz:**** La de Troya va en maratón el 25 de diciembre. Ya está programada. Pero son cinco capítulos.*

***Juan Pablo Bieri: ****Bueno, pero con eso vamos llenando. 48 horas para existir.*

***Diana Díaz:**** OK.*

***Juan Pablo Bieri: ****Vamos llenando, ¿si?*

***Diana Díaz: ****OK. Desconectados.*

***Alejandra Cendales:**** Ok ¿48 no es una serie web que dura como 9 minutos?*

***Diana Díaz: ****Toca mandar como 2 capítulos. Dos, tres capítulos.*

***Juan Pablo Bieri: ****Vamos llenando.*

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
