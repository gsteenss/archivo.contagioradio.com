Title: Dolce y Gabbana, diseñadores homosexuales en contra de la adopción igualitaria
Date: 2015-03-20 21:08
Author: CtgAdm
Category: Escritora, Opinion
Tags: adopcion gay, Dolce y Stefano Gabbana, Dolce&amp;Gabbana
Slug: dolce-y-gabbana-disenadores-homosexuales-en-contra-de-la-adopcion-igualitaria
Status: published

###### Foto: Domenico Dolce y Stefano Gabbana 

#### [**Por [Escritora Encubierta](https://archivo.contagioradio.com/escritora-encubierta/) - [[@]OneMoreHippie](https://twitter.com/OneMoreHippie)**] 

La controversia empezó cuando Domenico Dolce y Stefano Gabbana, fundadores de la marca de ropa Dolce&Gabanna, hablaron en la revista italiana 'Panorama' sobre temas como la adopción homosexual, la fertilización in vitro y el uso de vientres de alquiler. Para ellos, la única familia posible es la tradicional y lo dejaron muy claro en la entrevista. ''Nos oponemos a las adopciones gais. No a los hijos químicos y a los úteros alquilados. La vida tiene un flujo natural, hay cosas que no se deben cambiar'', señaló Domenico Dolce, quien sostuvo una relación homosexual con Stefano Gabbana.

El bloguero Perez Hilton indicó que los comentarios de los diseñadores podrían perjudicar sus relaciones con Hollywood, y no se equivocó puesto que rápidamente una gran lista de celebridades se manifestó en contra de ellos y una gran ola de críticas se propagó en las redes sociales.

Elton John fue el primero en pronunciarse públicamente al respecto. A través de su cuenta en Instagram expresó su desagrado: '' ¿Cómo se atreven a referirse a mis hermosos hijos como 'sintéticos'? Debería darles vergüenza juzgar a la Fecundación In Vitro –un milagro que ha permitido a legiones de gente amorosa, tanto heterosexuales como homosexuales, cumplir su sueño de tener hijos. Sus pensamientos arcaicos están fuera de sintonía con los tiempos, al igual que sus modas. Nunca más voy a usar Dolce&Gabbana. '' Sentenció el músico británico junto a una fotografía de los diseñadores usando como hashtag \#BoycottDolceGabbana.

Inmediatamente, varios famosos se unieron al boicot iniciado por Elton. La tenista estrella, Martina Navratilova publicó en Twitter: ''Mis camisas D&G van a la basura - no quiero que NADIE las use''. Courtney Love, la exesposa de Kurt Cobain, prometió quemar todas sus prendas. Incluso Ryan Murphy, el creador de la serie American Horror Story, dijo que no sólo él dejará de usar la marca, sino que también impedirá que los personajes de sus series la usen.

El cantante Ricky Martin, padre de gemelos nacidos a través de una madre de alquiler, utilizó su cuenta en Twitter para mencionar directamente a los diseñadores en un tweet: ''@dolcegabbana sus voces son demasiado poderosas para estar propagando tanto odio. Despierten, es 2015. Ámense a ustedes mismos''.

Madonna, la reina del pop, también se sumó a las críticas. ''Cada bebé tiene un alma independientemente de cómo venga al mundo y cómo sea su familia, no hay nada sintético en un alma… ¡Piensen antes de hablar!'' publicó como leyenda de una fotografía en Instagram.

Los empresarios Domenico Dolce y Stefano Gabbana se defendieron manifestando que sólo estaban ejerciendo su derecho a expresarse y que no podían ser juzgados por sus opiniones, sin embargo, como la libre expresión es para todos, todas las personas tienen derecho a[ ]{.Apple-converted-space}opinar sobre sus declaraciones. Lo que sí está claro es que tras la polémica cualquier celebridad pensará dos veces antes de usar alguna prenda Dolce&Gabbana.
