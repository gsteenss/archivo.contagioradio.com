Title: Cerrejón ha contribuido a la vulneración del derecho al ambiente sano en la Guajira: Indepaz
Date: 2018-05-22 13:08
Category: Ambiente, Nacional
Tags: Cerrejón, contaminación del agua, Guajira, minería del cerrejón, río Ranchería
Slug: cerrejon-ha-contribuido-a-la-vulneracion-del-derecho-al-ambiente-sano-en-la-guajira-indepaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/cerrejón-e1527011547700.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Indepaz] 

###### [22 May 2018] 

El más reciente informe **“Cuando el río suena, piedras y otras cosas lleva”**,** **realizado por el Instituto para el Desarrollo y la Paz, Indepaz, retrata la situación de riesgo que viven las comunidades en la Guajira debido a los efectos negativos de la minería del Cerrejón. El documento detalla los procesos de contaminación del agua y aboga por el respeto al derecho a un ambiente sano.

De acuerdo con Golda Fuentes, asesora académica de Indepaz, el trabajo de investigación se realizó con las comunidades que han habitado por **más de 30 años este territorio,** en conjunto con las actividades mineras que realiza el Cerrejón. Siendo esta una de las empresas más grandes de América Latina que se dedican a la extracción del carbón.

Debido a las preocupaciones de las comunidades en las afectaciones del medio ambiente, Indepaz realizó un monitoreo independiente, en conjunto con universidades de Colombia y Alemania, para identificar **las afectaciones al agua y al ambiente** que repercuten en las condiciones de salud de las comunidades.

### **Preocupaciones encontradas** 

Luego del trabajo de campo, el Instituto logró identificar la presencia de agentes contaminantes, producto de las actividades mineras en el **río Ranchería y sus arroyos,** que sirven de fuentes hídricas para el abastecimiento de las comunidades.  Fuentes indicó que, actividades como el vertimiento de aguas industriales en el río genera contaminación con plomo y otros metales pesados.

Afirmó que la expansión de la minería “ha generado un riesgo potencial en la **contaminación de los pozos de agua** y el río que es utilizado por las personas para bañarse y abastecerse del mismo”. Esta contaminación no sólo afecta la vida de las personas sino también pone en riesgo a las especies de fauna que se alimentan de los afluentes hídricos. (Le puede interesar:["Resguardo indígena gana pelea por la tierra al Cerrejón en la Guajira"](https://archivo.contagioradio.com/reguardo_nuevo_espinal_la_guajira_comunidades_el_cerrejon/))

### **Repercusiones en la salud de la contaminación del aire y el agua**

Fuentes indicó que los riesgos para las personas que entran en contacto con el agua contaminada son altos y están relacionados **con la toxicidad de metales como el plomo.** Esto puede generar afectaciones en el cerebro de personas vulnerables como los son los niños, los adultos mayores y las madres gestantes.

Adicionalmente, la exposición al Zinc y al Manganeso, que están presentes en la contaminación del río Ranchería pueden ocasionar afectaciones a la salud de las personas como **el desarrollo del cáncer.** Por esto es importante realizar estudios toxicológicos de las aguas y los ecosistemas que dependen de los afluentes en el departamento de la Guajira.

### **Recomendaciones de Indepaz** 

Frente a este panorama, el informe establece una serie de recomendaciones tanto para las instituciones encargadas de los temas ambientales como para la empresa Cerrejón que ha causado múltiples afectaciones. Indepaz **hará seguimiento** a la implementación de las recomendaciones toda vez que ya fue socializado el informe con las comunidades afectadas y las instituciones encargadas de propender por un ambiente sano.

Fuente manifestó que una de las principales recomendaciones se enfoca al cumplimiento de las **obligaciones ambientales** de la empresa Cerrejón. Afirmó que la empresa no está cumpliendo y “la ANLA y Copoguajira han manifestado irregularidades en el actuar de Cerrejón sin que haya sanciones correspondientes”.

Por esto, recordó que es necesario que se hagan **mediciones de contaminación del agua** de manera mensual para vigilar los niveles de contaminación y así poder tomar medidas correctivas “como por ejemplo restringir que los residuos de aguas industriales entren en el río Ranchería”.

Adicionalmente, recomendaron a las entidades públicas que se disponga de personal especializado para instalar **plantas de purificación del agua** en las comunidades afectadas para garantizar la supervivencia de estas personas. Desde Indepaz, consideraron necesario que todas las labores que se realicen tengan en cuenta las tradiciones culturales de las comunidades que han visto un deterioro en su salud y sus derechos al agua y un ambiente sano. (Le puede interesar:["Cerrejón deberá suspender actividad minera sobre arroyo Bruno"](https://archivo.contagioradio.com/cerrejon-debera-suspender-actividades-mineras-sobre-arroyo-bruno/))

###  

### **Informe involucra plataforma web interactiva** 

Como parte del desarrollo de la investigación, Indepaz realizó una [plataforma web](http://www.empresasyddhh.co/) para que las personas conozcan los resultados de la investigación desarrollada. Allí, se “dará a conocer en detalle los valores de las investigaciones que se realizó y los procesos de contaminación que están relacionados con la actividad minera”. (Le puede interesar:["13 razones para no desviar el arroyo Bruno"](https://archivo.contagioradio.com/13-razones-para-no-desviar-el-arroyo-bruno/))

Además, la plataforma web recoge las necesidades de las comunidades y pone de manifiesto las recomendaciones a las diferentes entidades. De acuerdo con Indepaz, esta plataforma ayuda a que la ciudadanía **haga un seguimiento** a la situación de violación de los derechos del ambiente de las comunidades de la Guajira para así, garantizar que las entidades públicas tomen correctivos frente a las labores de la empresa Cerrejón.

Finalmente, Fuentes recordó que el lanzamiento del informe será el día **23 de mayo de 2018** en la sede de la Fundación Henrich Boll de Bogotá a partir de las 8:30 de la mañana. Allí, contarán con la presencia de diferentes líderes sociales de la Guajira quienes detallarán cómo ha sido la convivencia con la empresa minera y qué desafíos han enfrentado a lo largo de más de 30 años.

<iframe id="audio_26115899" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26115899_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
