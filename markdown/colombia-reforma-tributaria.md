Title: Debate en El Ecléctico: Reforma Tributaria
Date: 2016-10-31 12:15
Category: El Eclectico
Tags: colombia, debate, Impuestos, Reforma tributaria
Slug: colombia-reforma-tributaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/reforma-tributaria-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Acuda.org.co 

##### 31 Oct 2016

Hablar de impuestos nunca es sencillo. En primer lugar porque los aspectos tributarios en su composición son desconocidos por la mayoría de la población y además porque un incremento de los mismos nunca es una medida popular, incluso si es necesaria. Le puede interesar:[Reforma tributaria afectará a trabajadores y pensionados](https://archivo.contagioradio.com/reforma-tributaria-afectara-a-trabajadores-y-pensionados/).

El debate de hoy cuenta con jóvenes que conocen y se interesan por los cambios en cuestión de impuestos que traerá la que ha sido llamada la “primera reforma tributaria estructural” y que abarca aspectos cotidianos en la vida de cualquier colombiano. Desde diferentes líneas políticas nuestros invitados nos cuentan paso a paso lo bueno y lo malo de los gravámenes que el Gobierno Nacional pretende imponer para mermar el déficit fiscal por el que atraviesa el país.

¿Sobre qué productos aumentará el IVA? ¿Qué es el impuesto verde? ¿Soluciona esta reforma el problema de evasión de impuestos? ¿Qué tan necesaria es una reforma tributaria en Colombia al día de hoy? Éstas y más preguntas se resuelven en nuestro debate de hoy, aquí en El Ecléctico.

<iframe id="audio_14936657" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14936657_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
