Title: Más de 2.500 víctimas acreditadas ante la JEP, exigen garantías de seguridad para Mancuso
Date: 2020-09-08 11:39
Author: AdminContagio
Category: Actualidad, Paz
Tags: carta, garantias de vida, Mancuso, víctimas
Slug: mas-de-2-500-victimas-acreditadas-ante-la-jep-exigen-garantias-de-vida-para-mancuso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/carta-de-victimas.-a-mancuso.pdf" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Ante la reciente reiteración del exjefe paramilitar Salvatore Mancuso en su [compromiso con la verdad](https://archivo.contagioradio.com/las-razones-que-explican-el-regreso-de-las-masacres-a-colombia/)y las víctimas en Colombia, **más de 2,500 víctimas acreditadas ante la JEP exigieron garantías de vida** para este y reiteraron su confianza en su palabra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por medio de una carta víctimas del conflicto armado en Colombia, acompañadas por la Comisión Intereclesial de Justicia y Paz, extendieron la invitación a la instancias judiciales, nacionales e internacionales para id**entificar los factores de riesgos derivados de las verdades que pueden ser entregadas por Mancuso,** señalando que *"esto lejos de ser una excusa, es la necesidad de garantías hacia la verdad y la no repetición".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Indicando que en el país, ***"muchos le temen a la verdad y prefieren repetir los errores del pasado con asesinatos y persecuciones"***, las organizaciones instaron, en qué es necesario que el ex jefe paramilitar, este vivo y que además haya garantías para que las verdades que el conozca junto con las que ya dijo y se deben profundizar, sean garantizadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"**Ya ocurrió un intento frustrado con uno de los responsables que acudió ante la JEP para contribuir con la verdad,** esa verdad quedó incompleta por absurda, silenciosa y negligencia ante su inminente fallecimiento como consecuencia de una enfermedad que parecía"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Señalamiento en el que recuerdan lo ocurrido con el sicario del criminal del Cartel de Medellín, Jhon Jairo Velásquez (Popeye), quien murió llevando con sigo muchas verdades, el pasado 6 de febrero de 2020, *"**no queremos que esta posibilidad de conocer la verdad no se arrebata una vez más"***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por lo tanto las víctimas, no solamente exigieron garantías para la vida de Mancuso, sino también reiteraron su compromiso en continuar *"una relación directa y dialógica, que les permita profundizar y detallar a los responsables"* de las verdades que aún esté tenga por decir, verdades que además de ser plenas requieren que sean [tomadas ante la JEP.](https://www.jep.gov.co/Sala-de-Prensa/Paginas/La-JEP-rechaza-sometimiento-de-Salvatore-Mancuso.aspx)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de la verificación de estás por medio de la conformación, de una Comisión de Observación Internacional, **en la que existan organizaciones de la Comisión Etica Internacional, para que haya una verificación de este proceso de diálogo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Destacando, que las verdades son actores de transformación ante la violencia que aún continúa en territorios como **el Bajo Atrato en el Chocó,** o en el Urabá cordobés y antioqueño; d**onde las comunidades continúan siendo víctimas de la violencia armada desencadenada en el desarrollo de las estrategias armadas de tipo paramilitar.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un pasado que aún sigue estando vigente a través de masacres, desapariciones forzadas, homicidios, tortura, desplazamientos forzados y despojo. ([Otra Mirada: Masacres, ¡De vuelta al terror!](https://archivo.contagioradio.com/otra-mirada-masacres-de-vuelta-al-terror/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Las víctimas, estamos con usted en el camino de la verdad"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Recordando el encuentro telefónico que tuvieron el 24 de agosto en compañía de la comisionada de la verdad, Patricia Tobón en la ciudad de Apartado, en dónde Salvatore Mancuso diálogo con varias de las víctimas de este territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Agradecieron el compromiso de este en la continuación de aportar al esclarecimiento de la verdad, la reparación de los derechos y *"el reconocimiento de responsabilidades para transitar a una democracia distinta"*. (**[Así fue encuentro de diálogo entre víctimas y Salvatore Mancuso](https://archivo.contagioradio.com/asi-fue-encuentro-de-dialogo-entre-victimas-y-salvatore-mancuso/)**)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y aclararon que así como las comunidades son conscientes de la situación que él está viviendo, porque ,*"decir la verdad en Colombia es un riesgo"*, señalaron que **como víctimas entienden ese miedo, porqué han tenido que sufrir en la búsqueda de la verdad, amenazas, torturas, desapariciones, asesinatos, desplazamientos**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente invitaron a Mancuso a que públicamente manifieste ante el país y el mundo su compromiso con la verdad, y agregaron que se encuentran a la espera de la apelación de este ante la Justicia Transicional, como signo de su compromiso con la verdad.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Contamos con usted, y estamos con usted en el camino de la verdad, **esperamos que se adopten decisiones que contribuyan a todas y todos a lograr los derechos de las víctimas y de la sociedad para tejer un país con democracia justa e incluyente"**

<!-- /wp:quote -->

<!-- wp:file {"id":89489,"href":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/carta-de-victimas.-a-mancuso.pdf"} -->

<div class="wp-block-file">

[  
  
**Conozca la carta completa de las víctimas a Mancuso**](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/carta-de-victimas.-a-mancuso.pdf)[Descarga](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/carta-de-victimas.-a-mancuso.pdf){.wp-block-file__button}

</div>

<!-- /wp:file -->

<!-- wp:block {"ref":78955} /-->
