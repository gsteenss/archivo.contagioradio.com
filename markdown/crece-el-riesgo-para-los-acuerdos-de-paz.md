Title: Crece el riesgo para los Acuerdos de Paz
Date: 2017-02-14 13:23
Category: Nacional, Paz
Tags: jurisdicción especial para la paz, Nestor Humberto Martínez
Slug: crece-el-riesgo-para-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/f852363e-918e-4c86-9de2-b70b2be986a5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Feb 2017] 

**La implementación de los acuerdos de paz está atravesando una situación que es “complicada”** debido a la falta de compromiso por parte del Estado y de las intenciones de actores políticos como el Fiscal Néstor Humberto Martínez que pretenden re negociar lo ya acordado, de acuerdo con el abogado Enrique Santiago, uno de los juristas asesores del proceso de paz.

Según el jurista, aunque algunos han insistido en que hay voluntad de paz de las partes, es **necesario que el Estado ponga el acelerador, puesto que puede haber funcionarios que no han hecho lo que deberían hacer,** en ese sentido el gobierno tiene la potestad de remover de sus cargos o hacer cambios en instituciones sobre las cuales recaiga la responsabilidad de las demoras.

### **Néstor Humberto Martínez quiere modificar el acuerdo como si no estuviera cerrado** 

Para Enrique Santiago la Fiscalía ha venido manteniendo dos actitudes hacia los acuerdos de paz de La Habana: la primera es **obviar que existe un Acuerdo firmado** y pretender re negociar “hasta la última palabra de lo que tiene que ver con justicia” como si este ente fuera la representación del Estado.

La segunda tiene que ver con las propuestas que ha hecho la Fiscalía para modificar la Justicia Especial para la Paz, que busca **limitar al máximo las competencias de esta jurisdicción especial** para llevar los casos de guerrilleros a la justicia ordinaria, la Fiscalía “no deberían estar buscando cualquier resquicio constantemente para atribuirse estas competencias, cuestión que no hacen frente a los agentes del Estado” afirmó Enrique Santiago

De otro lado, se encuentran las propuestas que hizo la Fiscalía respecto a los civiles que hicieron parte del conflicto armado, y que de acuerdo con el Acuerdo de Paz de La Habana, entran a ser parte de la Justicia Especial. Para Enrique Santiago las propuestas intentan bloquear la JEP y hacer que estas personas sean juzgadas por la Justicia ordinaria, **“parece que la Fiscalía no se da cuenta que estos civiles, financiadores y organizadores del paramilitarismo, son los sectores que más impunidad han disfrutado”. **Le puede interesar:["Organizaciones sociales exigen justicia para todos en implementación de JEP"](https://archivo.contagioradio.com/organizaciones-sociales-exigen-justicia-para-todos-en-implementacion-de-jep/)

### **Amnistía no se ha aplicado a pesar de haberse aprobado hace más de 45 días** 

Uno de los primero puntos de la implementación de los Acuerdos de Paz, tiene que ver con las amnistías que se debían conceder a guerrilleros para que se trasladaran a las zonas verdales, sin embardo según el abogado Enrique Santiago lo que **están haciendo los jueces es “desidia” hacia la aplicación de la Amnistía.**

Los argumentos por parte de los jueces recaen en decir que la **ley no es clara**, a lo que el abogado respondió que se han generado en la historia de Colombia otras 4 leyes de amnistía siendo esta la más clara y detallada. Le puede interesar: ["Hay más de 700 solicitudes a integrantes de las FARC sin respuesta"](https://archivo.contagioradio.com/700-solicitudes-de-amnistia-sin-respuesta/)

De otro lado, Enrique Santiago afirmó que  los **jueces no  están actuando conforme a la ley**, debido a que la amnistía también se puede otorgar de oficio, es decir los jueces y fiscales pueden concederlas, como ya algunos lo han hecho, mientras que otros han señalado no tener las facultades para hacerlo.

Otra de las situaciones que se ha presentado es la **ausencia de las listas de miembros de la fuerza pública**, que les corresponde optar al ejercicio de la libertad condicional para quedar a disposición de la Jurisdicción Especial, que tendría que haber generado el Ministerio de Defensa y que aún no se conocen. Estas problemáticas han retrasado la llegada de los guerrilleros a las zonas veredales, ya que han transcurrido **45 días desde la aprobación de la Amnistía sin que el procedimiento se haga efectivo.**

### **No se entiende que el Estado sea el que "ponga en riesgo los acuerdos"** 

Diferentes organizaciones como la Corte Penal Iternacional, ya han expresado su preocupación frente a la Justicia Especial para la Paz y la impunidad que podría generar. Para la comunidad internacional, de acuerdo con Enrique Santiago, no es entendible **“tanta improvisación en las zonas veredales transitorias de normalización, ni por qué con su comportamiento el Estado pone en riesgo los acuerdos”.**

De igual forma, el que instituciones que hacen parte del gobierno no estén cumpliendo con los acuerdos de paz, sino que por el contrario le pongan trabas al mismo, refleja para el abogado una **“falta de seriedad y compromiso”**. Le puede interesar: ["Modificaciones a  Jurisdicción Especial de Paz, abrirían la puerta a Corte Penal Internacional"](https://archivo.contagioradio.com/modificaciones-a-jurisdiccion-especial-de-paz-abririan-la-puerta-a-corte-penal-internacional/)

### **La sociedad y la paz** 

Pese a este panorama, Enrique Santiago considera que las soluciones provienen de muchos sectores, el primero tiene que ver con que las **Instituciones cumplan su papel, hagan su trabajo y asuman que el tiempo de negociar el acuerdo de paz ya pasó** y que se está en el tiempo de la implementación,  el segundo con que el gobierno adopte medidas de seguridad y active mecanismos de cumplimiento de lo que le corresponde.

Finalmente esta la sociedad, los jóvenes que se movilizaron en Colombia para presionar al gobierno y a las FACR-EP a continuar y finiquitar el proceso de paz, Enrique Santiago dice que ahora tienen la responsabilidad de **presionar al gobierno para que la implementación se de en las mejores formas y condiciones para el país.**

<iframe id="audio_17005938" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17005938_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
