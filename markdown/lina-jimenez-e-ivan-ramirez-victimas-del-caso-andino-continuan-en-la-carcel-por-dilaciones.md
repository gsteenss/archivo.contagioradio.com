Title: Lina Jiménez e Iván Ramírez, víctimas del caso Andino, continúan en la cárcel por dilaciones
Date: 2020-09-01 15:56
Author: CtgAdm
Category: Expreso Libertad
Tags: #Andino, #CasoAndino, #Fiscalía, #MontajeJudicial
Slug: lina-jimenez-e-ivan-ramirez-victimas-del-caso-andino-continuan-en-la-carcel-por-dilaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/lina-e-ivan.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Tras la salida de 7 personas de la cárcel, por vencimiento de términos en el proceso conocido como Caso Andino, Lina Jiménez e Iván Ramírez, quienes también denunciaron ser víctimas del montaje judicial, **continúan en la cárcel.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa del Expreso Libertad el abogado defensor de estás personas, Jason Paba, señaló que en la última audiencia [la jueza desconoció el tiempo establecido para el vencimiento de términos.](https://archivo.contagioradio.com/caso-andino-un-proceso-cargado-de-injusticias-y-dilaciones/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, manifestó que este aún se sigue señalando la responsabilidad de ambas personas en los hechos, cuando el delito por el cuál continúan en la cárcel está relacionado con otras acciones de las cuales sigue sin existir material probatorio.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_55952864" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_55952864_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

De igual forma, Laura Jiménez, hermana de Lina, aseguró que **el bienestar de ambas personas se encuentra en riesgo** debido a que Iván se encuentra recluido en la Cárel La Picota, la segunda con más contagios de Covid 19 en el país. Mientras que Lina se encuentra en la Cárcel de Coiba, sobre la que ya se manifestó un fallo de tutela por violación a derechos humanos en el marco de la pandemia.

<!-- /wp:paragraph -->
