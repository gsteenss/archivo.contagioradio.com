Title: Solicitan a reconocido chef peruano no dejarse utilizar por gobierno israelí
Date: 2017-10-19 17:01
Category: Onda Palestina
Tags: Apartheid, Israel, Palestina
Slug: chef-peruano-palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/chef.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: BDS Colombia 

###### 18 Oct 2017 

Activistas solidarias con el pueblo palestino han dirigido una carta al chef peruano Mitsuharu Tsumura pidiéndole desista de participar en el evento Roud Table TLV, el cual es auspiciado por el Ministerio de Relaciones Exteriores y el Ministerio de Turismo isralelí.

Los activistas señalan que por medio de estos eventos se posiciona la “Marca Israel”, que busca utilizar la cultura y las artes como distracción de las violaciones de Derechos Humanos y así crear una imagen positiva. Uno de los colaboradores del evento, la empresa vinícola Golan Heights, opera en una colonia israelí ilegal en los Altos del Golán sirio.

En la misiva se le informa al Chef que Israel se está apropiando de las especias y productos agrícolas nativos palestinos; que se han talado miles de bosques de olivos y que las tierras donde se cultivan varios productos agrícolas han sido parte de crueles procesos de despojo.

También se le informa, que la cadena Dan Hotels, donde él se iría a hospedar en caso de que aceptara definitivamente la invitación, ha construido varios hospedajes en territorio palestino robado. Finalmente se le informa que muchos otros chefs en el mundo han dejado de participar en este evento, al conocer estos graves hechos, como es el caso del Chef irlandés JP McMahon.

En esta emisión de Onda Palestina una de las activistas peruanas nos explica a fondo esta campaña, además estaremos hablándoles respecto a la salida de EEUU de la UNESCO por supuesta complicidad anti-israelí, el retroceso en la decisión del gobierno egipcio para reabrir la frontera entre la franja de Gaza y Egipto, el apoyo de un cineasta a nuestro movimiento, otra victoria del BDS sobre la empresa G4S y finalmente hablaremos sobre las delicias de la cultura palestina.

<iframe id="audio_21539554" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21539554_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
