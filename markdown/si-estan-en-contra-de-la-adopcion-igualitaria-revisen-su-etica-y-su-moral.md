Title: Si están en contra de la adopción igualitaria revisen su ética y su moral
Date: 2015-11-11 06:00
Category: Javier Ruiz, Opinion
Tags: Adopción igualitaria, comunidad gay en Colombia, Corte Constitucional, homosexuales
Slug: si-estan-en-contra-de-la-adopcion-igualitaria-revisen-su-etica-y-su-moral
Status: published

###### [Foto: Contagio Radio] 

#### **[[Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/)- [@ContraGodarria ](http://@ContraGodarria)]** 

Se podría decir que fue un gran triunfo de la comunidad gay en Colombia la decisión tomada por la Corte Constitucional (uno de los pocos reductos de democracia que existen en el país a mi parecer) en donde se protegen los derechos de los niños que necesitan y anhelan tener un hogar y en donde le dan la oportunidad a las familias conformadas por personas del mismo sexo de darle un futuro y una buena calidad de vida al niño que piensan adoptar.

[Las reacciones contrarias a esta decisión no se han hecho esperar. Los sectores ultraconservadores desaprueban esto porque consideran a los homosexuales como personas “pervertidas” y por no ser “normales” respecto a su orientación sexual argumentan que lo anterior sería un impedimento para que los niños sean adoptados por ellos ya que, según los ultraconservadores, podrían sufrir “traumas irreversibles” por no tener un papá y una mamá.]

[Los sectores ultraconservadores (iglesia católica y sectas cristianas) han hecho un llamado a una “cruzada moral” para defender a los niños de los homosexuales y están ideando trabas para que no puedan adoptar. En el congreso los senadores cristianos proponen un referendo para que el pueblo decida si los homosexuales puedan adoptar o no pero sería algo macabro porque buscan que a través de la “tiranía de las mayorías” aplastar a esa minoría que busca tener igualdad al momento de adoptar a un niño. Por otro lado, la iglesia católica pide a sus seguidores heterosexuales que vayan masivamente al ICBF a adoptar niños impidiendo que las parejas gay puedan adoptar.]

[Teniendo en cuenta lo anterior, es necesario decirle a estas personas que militan en contra de los homosexuales que revisen su moral y su ética porque hasta ahora si muestran signos de preocupación por los niños huérfanos o abandonados que esperan ser adoptados por una familia que les garantice un buen futuro. En realidad los cristianos y católicos (no generalizo) nunca han mostrado preocupación por los niños abandonados, prostituidos, niños que fueron reclutados a la fuerza por grupos ilegales para combatir en el terrible conflicto interno, niños que son utilizados para cometer delitos, etc.]

[Estas personas que ven terrible que una familia conformada por dos personas del mismo sexo quiera tener un hijo por medio de la adopción les parecen más ético y moral que los niños estén abandonados a su suerte aguantando todo tipo de abusos y de desgracias a que puedan saber por alguna vez en sus primeros años de vida que es el amor de familia, el saber que es tener un hogar y el saber que es tener a unas personas que le den amor y protección.]

[Les parecen más ético y moral a los sectores homofóbicos desinformar y engañar a la gente en general sobre el “peligro” de que los homosexuales puedan adoptar metiendo miedo y amenazando gravemente a quienes apoyan la adopción igualitaria que el niño desamparado que puede ser víctima de los peores vejámenes y que, por desgracia, los ha sufrido por las “familias normales” conformadas por un papá y una mamá.]

[A esos sectores homofóbicos que les puede más la ignorancia y el meter miedo deben replantearse sus posturas éticas y morales sobre la niñez desamparada que con la decisión de la Corte Constitucional se van a ver protegidos sus derechos y saber que es tener una familia que no hay una sino múltiples tipos de familia.]

[Pregunto a los que están en contra de la adopción igualitaria ¿tienen una ética y moral pervertida al respecto?.]
