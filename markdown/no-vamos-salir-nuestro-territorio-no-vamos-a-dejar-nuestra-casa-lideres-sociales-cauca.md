Title: No vamos a salir de nuestro territorio, no vamos a dejar nuestra casa: líderes sociales del Cauca
Date: 2019-07-15 18:54
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: amenazas contra líderes sociales, Francia Márquez, Norte del Cauca
Slug: no-vamos-salir-nuestro-territorio-no-vamos-a-dejar-nuestra-casa-lideres-sociales-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Líderes-Sociales-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

Durante la rueda de prensa convocada en Yolombó, en el municipio de Suarez, los líderes y lideresas sociales del Cauca, reiteraron que continuarán trabajando por la defensa de su territorio pese a las amenazas de las que han sido víctimas por parte de grupos como las Águilas Negras, quienes les dieron un plazo de 72 horas para dejar la zona.

Con la presencia de la Procuraduría, la Defensoría del Pueblo, la Personería Municipal y diferentes organizaciones sociales se dio inicio a la rueda de prensa en la que la defensora de derechos, Francia Márquez, se refirió a las amenazas que ella, junto a los líderes sociales han  recibido por denunciar la minería ilegal y otras problemáticas en su territorio a lo largo de casi una década.

**"Nosotros nos empezamos a preguntar ¿ahora qué hacemos? y ayer llegamos a la conclusión de que lo que podemos hacer es juntarnos y abrazarnos como familia, y como familia vamos a afrontar esta situación"** expresó.

### "Hoy el Cauca es uno de los departamentos que más agresiones está viviendo"

Francia Márquez expresó que continúan siendo declarados objetivo militar por exigir la restitución de los derechos étnicos de los habitantes del corregimiento de la Toma y por, según aquellos que los amenazan, "oponerse al desarrollo, pero ¿cuál desarrollo si no tenemos agua potable ni electricidad?", inquirió.

Previo a la rueda de prensa, la lideresa indicó que "el Gobierno nos ha asignado medidas de protección, pero la protección para nosotros está en términos de la garantía de los derechos, que se implementen las ordenes de la Corte Constitucional de suspender los títulos mineros y  garantizar los derechos a la consulta previa y los derechos constitucionales que se han establecido". [(Le puede interesar: "Los acuerdos son para cumplirlos" Comunidades negras del Cauca al Gobierno)](https://archivo.contagioradio.com/los-acuerdos-son-para-cumplirlos-comunidades-negras-del-cauca-al-gobierno/)

Asímismo señalo que estuvo en contacto con el comandante del cuerpo élite del CTI de la Fiscalía, institución que esperan realice las acciones necesarias para salvaguardar la vida de las personas, **"queremos respuestas porque la impunidad que se guarda ha sido un premio para que los victimarios crean que nos pueden seguir intimidando",** aseguró Márquez frente a la garantía de la vida y la necesidad de que la paz llegue a sus territorios.

### "Cuando nos amenazan a nosotros, están amenazando a nuestra comunidad" 

Víctor Hugo Moreno, líder afro, indicó que el respaldo de los habitantes de la región les da fuerza para seguir en su lucha, invitando a la realización de una movilización en el territorio. **"Estamos aquí para seguir fortaleciendo la dinámica del pueblo negro y la defensa del territorio, la vida y la dignidad"**, aseveró. [(Lea también: Atentan contra Francia Márquez y otros líderes de comunidades negras del Norte del Cauca)](https://archivo.contagioradio.com/atentan-contra-lideresa-francia-marquez/)

Durante la rueda de prensa también intervino la Defensoría del Pueblo que resaltó que en 2018 se emitieron 86 alertas tempranas y en lo corrido del 2019 se han emitido 27 alertas tempranas, pese a ello las amenazas ascienden a 983 y los homicidios de defensores a 479 líderes, **"una alerta temprana escuchada a tiempo evita la muerte de líderes y defensores, a todo panfleto que se mueve en este país hay que tenerle cuidado"**, enfatizó el defensor.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38462869" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38462869_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
