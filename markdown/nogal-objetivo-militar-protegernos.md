Title: El Nogal era un objetivo militar y el Estado tenía que protegernos: Víctima del atentado
Date: 2018-08-23 12:45
Category: Judicial, Nacional
Tags: Fallo contra el Estado, FARC, JEP, Nogal
Slug: nogal-objetivo-militar-protegernos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-23-a-las-12.22.31-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @asantosrubino] 

###### [23 Ago 2018] 

Tras la condena al Estado Colombiano por su omisión en la protección del **Club El Nogal de Bogotá,** lo que permitió el atentado perpetrado en **febrero de 2003** por parte de la entonces guerrilla de las FARC, **Bertha Lucía Fries,** líderesa y vocera de las víctimas, la decisión es histórica al reconocer responsabilidades que aporten a la verdad y la justicia que hace más de 15 años vienen reclamando.

En el fallo, el Consejo reconoce que hubo negligencia por parte de actores estatales quienes realizaban reuniones en el Club, al norte de la Capital, **pero no protegieron de forma adecuada a todas las personas que frecuentaban sus instalaciones.** Adicionalmente, porque no se prestó atención a un informante que había advertido sobre la posibilidad de que se cometiera el atentado. (Le puede interesar: ["El Estado sabía que se iba a perpetrar un atentado en el Club El Nogal"](https://archivo.contagioradio.com/estado-sabia-que-se-iba-a-perpetrar-el-atentado-del-club-el-nogal/))

### **"Colombia era un campo de batalla y el Estado tenía que protegernos"** 

Según la representante de las víctimas, el Estado tenía que proteger a quienes se encontraban en el Club el Nogal no por "tener corona", sino en razón de que allí se realizaban reuniones institucionales, al punto que se llegó a conocer al Club como "la segunda Casa de Nariño".  Para Fries este fallo prueba que** Colombia vivía una guerra, y era un campo de batalla en el que el Estado debía evitar que los civiles se vieran afectados.**

Aunque se reconoce que también el Club el Nogal tuvo responsabilidad por las fallas en su seguridad privada, en la determinación se sindica únicamente a los Ministerios de Justicia y Defensa, la Fiscalía y el desaparecido Departamento Administrativo de Seguridad (DAS) en calidad de representantes del Estado colombiano, como responsables de la omisión. A pesar de ello, el presidente Iván Duque calificó como "incongruente" la decisión de la Corte, y señaló que no hay equilibrio en la justicia si se condena al Estado mientras los autores del atentado no son severamente castigados.

Por su parte, los autores intelectuales y materiales del hecho, ahora partido político Fuerza Alternativa Revolucionaria del Común **(FARC), en febrero del presente año pidieron perdón a las víctimas, y recientemente reiteraron su compromiso con la verdad, la justicia y su reparación. ** (Le puede interesar: ["FARC contará la verdad de atentado al Club El Nogal y pedirá perdón"](https://archivo.contagioradio.com/38897/))

[![Victoria Sandino, Club el Nogal](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-23-a-las-12.27.40-p.m.-617x350.png){.size-medium .wp-image-55923 .aligncenter width="617" height="350"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-23-a-las-12.27.40-p.m..png)

###  **La Jurisdicción Especial de Paz es la que debe juzgar a FARC**

Para Fries, una cosa es la condena al Estado por no garantizar la vida de sus ciudadanos, que demuestra una ineficiencia y debe ser pagada; y otra cuestión es el nuevo contexto que vivimos, en el que hay un acuerdo de paz, una justicia restaurativa como es **la Jurisdicción Especial para la Paz (JEP),** y el compromiso con la verdad expresado y reiterado por los miembros del ahora partido político.

Precisamente, Fries recordó que en tres eventos sobre víctimas en las que asistieron miembros de la ex guerrilla, afirmaron que ellos efectivamente eran los responsables del atentado, y también, que el carro bomba fue puesto en el Nogal porque **tenían información sobre reuniones que allí se realizaban entre altos funcionarios del Estado y Paramilitares.**

### **Ahora llega una nueva jurisprudencia para el caso** 

Con este fallo, se abren nuevas puertas para quienes tienen procesos en la justicia contra la Nación colombiana por el atentado al Club el Nogal. Según Fries, desde hace 15 años se han presentado una serie de demandas por parte de empleados del Club contra el Estado para que se los indemnice por la desprotección de su vida, sin embargo, en diferentes sentencias se  les ha negado el reconocimiento de tal responsabilidad.

**Dichos procesos deberían ser revisados a la luz de este nuevo fallo,** e igualmente, el expediente tendría que ser trasladado a los que están en curso puesto que representa una prueba a tomar en cuenta para estos casos. En el Club El Nogal **36 personas murieron y cerca de 200 más resultaron heridas.** (Le puede interesar: ["Señores Justicia, hagan justicia para las víctimas del El Nogal: Bertha Fries"](https://archivo.contagioradio.com/senores-justicia-hagan-justicia-para-las-victimas-del-nogal-bertha-fries-victima/))

<iframe id="audio_28045961" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28045961_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU) 
