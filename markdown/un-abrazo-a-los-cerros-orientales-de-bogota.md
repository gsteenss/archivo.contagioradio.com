Title: Un abrazo a los cerros orientales de Bogotá
Date: 2015-03-12 20:55
Author: CtgAdm
Category: eventos
Tags: cerros de bogota, quebrada las delicias
Slug: un-abrazo-a-los-cerros-orientales-de-bogota
Status: published

La Asociación Amigos de la Montaña realizará el domingo 15 marzo el  Primer Abrazatón a los Cerros Orientales, una acción que tienen como propósito el cuidado y respeto por los cerros bogotanos. Entre las actividades de este día se realizará una cadena humana en símbolo de protección  por las  montañas de la ciudad y  la **quedrada Las Delicias** que desciende  por los cerros orientales en la localidad de Chapinero

[![Abrazaton por los cerros](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/11041466_10153150531079805_1031187146_n.jpg){.wp-image-5899 .alignleft width="282" height="395"}](https://archivo.contagioradio.com/eventos/un-abrazo-a-los-cerros-orientales-de-bogota/attachment/11041466_10153150531079805_1031187146_n/)

**Punto de encuentro**: Frente del café OMA de la calle 62 con carrera 3ª.

**Hora**: 9:00 a.m.

**Descripción**: Desde la calle 62 con carrera 1ª formaremos una cadena humana a lo largo de la primera parte de la Quebrada Las Delicias para continuar bordeando las comunidades y la Avenida Circunvalar hacia el norte.

**En redes sociales**: @amigosmontana
