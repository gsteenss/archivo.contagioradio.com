Title: Propuesta de ex presidente Gaviria confunde justicia transicional con leyes de “Punto Final” Rodolfo Arango
Date: 2015-02-23 23:39
Author: CtgAdm
Category: Entrevistas, Paz
Tags: conversaciones de paz, ELN, FARC, justicia transicional en colombia, Víctimas en Colombia
Slug: propuesta-de-ex-presidente-gaviria-confunde-justicia-transicional-con-leyes-de-punto-final-rodolfo-arango
Status: published

###### Foto: las2orillas.com 

<iframe src="http://www.ivoox.com/player_ek_4124289_2_1.html?data=lZaflpecfY6ZmKiak5iJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjLfcxtTQqtCfotfO0MzTb9Tjw9fSja%2FZt9XdxM7OjbnWpc%2FnysjW0dPFsI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Rodolfo Arango] 

Para Rodolfo Arango, doctor en filosofía y jurista experto, la **propuesta del Ex presidente Gaviria** ha sido muy bien recibida en varios sectores porque abarca varios problemas, el tema de la **fuerza pública**, el tema de los **empresarios** que apoyaron el paramilitarismo en Colombia y con esta propuesta lo que se haría sería **“taparnos todos con la misma cobija”.**

Sin embargo ese concepto riñe con lo jurídico que exige que se hagan las distinciones necesarias entre unos y otros, pero para Arango, lo que hace Gaviria es **usar el concepto de justicia transicional** que acaba siendo confundido con la idea de que los procesos que no se han abierto ya no se vayan a abrir y dejar la idea de que se le abre camino a la impunidad.

Frente a la responsabilidad de los empresarios, Arango señala que es necesario que se **distingan los** **varios tipos de responsabilidad.** La de tipo penal puesto que ni siquiera hay la capacidad de mandar a tantos miles y miles de personas a la cárcel, otra sería la responsabilidad política, la responsabilidad moral y otra la responsabilidad social dada cuando algunos actores admiten que estuvieron vinculados en el conflicto. Está la responsabilidad económica, en que tanto las demandas como las responsabilidades recaen en quienes se beneficiaron de la guerra, y no solamente se deben pagar las indemnizaciones sino restablecer las condiciones de vida que se destruyeron.

En cuanto al juzgamiento, Arango señala que no comparte la posibilidad de establecer tribunales mixtos, puesto que ya **existen mecanismos en la justicia actual y además hay mecanismos que le dan competencia a la Corte Penal Internacional**, pero Colombia debe intentar resolver sus problemas en casa, por ello la propuesta del ex presidente llega en un muy buen momento porque la sociedad colombiana debe entrar en el debate de lo que se está discutiendo en La Habana.

De acuerdo a las declaraciones de Iván Márquez, Arango señala que es necesario tener **altas dosis de generosidad por parte de todos los implicados** porque también hay una decisión política de la CPI de observar los países en los que interviene y Colombia no puede permitirse una situación así puesto que la legislación vigente es mucho más avanzada que en otros países del mundo, por ejemplo en lo que tiene que ver con genocidio y otros elementos.
