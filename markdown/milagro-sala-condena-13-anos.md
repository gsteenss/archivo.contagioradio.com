Title: Condenan a lideresa argentina Milagro Sala a 13 años de prisión
Date: 2019-01-15 10:20
Author: AdminContagio
Category: El mundo, Otra Mirada
Tags: Argentina, Milagro Sala, organización Túpac Amaru
Slug: milagro-sala-condena-13-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/juicio-y-sentencia-a-milagro___CIZGOLbvI_1200x0__1-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Juan Fernández 

###### 15 Ene 2018 

En una decisión tomada el lunes en la noche, las juezas María Alejandra Tolaba, Claudia Sadir y Ana Carolina Pérez Rojas, se declaró culpable por los delitos de asociación ilícita en calidad de jefa, fraude al estado y extorsión a la líderesa barrial e indígena Milagro Sala en Argentina, dictándole una condena de 13 años de reclusión.

Según la defensa, el tribunal oral en el que también fueron condenadas otras 8 personas a penas entre 3 y 8 años, tomo la decisión sin pruebas para condenarla. Por su parte, la dirigente de Tupa Amaru aseguró que “La justicia hoy está cooptada por Gerardo Morales” gobernador de Jujuy que a su juicio presionó a las juezas para que dictaran sentencia.

"Nos acusan por hacer cosas”, aseguró Sala, mientras reafirmó su inocencia y la de sus compañeros Pachila" Cabana, "Diablo" Altamirano, Miguel Sivila, denunciando la corrupción y el control sobre el poder judicial que según ella ejerce el gobierno de Morales: “Todos mis compañeros que están sentados acá son inocentes. Lo único que han hecho es buscar dignidad y vivir un poco mejor”.

La lideresa que completará mañana 3 años privada de la libertad, aseguró que "Acá han apretado a un montón de testigos, han amenazado a las familias de los testigos para que nos involucren y hablen mal de nosotras” y agregó que “De la salud, la educación y de viviendas dignas, nosotros nos hicimos cargo. Nos acusan por hacer cosas”.

Con Sala y sus compañeros, fueron condenados además** **dos funcionarios del exgobernador Eduardo Fellner, Lucio Agregú y Pablo Tolosa, quienes hacían parte del Instituto de Vivienda de Jujuy  recibieron tres y dos años. La defensa asegura que apelar el fallo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
