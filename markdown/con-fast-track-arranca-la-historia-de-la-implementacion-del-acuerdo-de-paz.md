Title: Con Fast Track arranca la historia de la implementación del acuerdo de Paz
Date: 2016-12-13 16:56
Category: Nacional, Paz
Tags: acuerdo de paz, Corte Constitucional, Fast Track, Implementación
Slug: con-fast-track-arranca-la-historia-de-la-implementacion-del-acuerdo-de-paz
Status: published

###### [Foto: el político] 

###### [13 Dic 2016] 

Contagio radio consultó a dos expertos ex magistrados de la Corte Constitucional para entender el detalle de la discusión y las implicaciones de la decisión de en torno a la implementación del acuerdo de paz a través del Fast Track. La Discusión al interior de la corte no fue solamente alrededor de la **legalidad del procedimiento abreviado, sino en la refrendación del acuerdo, ambos contemplados en el acto legislativo para la paz.**

### **El Fast Track es constitucional** 

Por tratarse de un procedimiento abreviado para realizar reformas constitucionales que está dispuesto en la ley colombiana, el Fast Track o la Vía Rápida son plenamente reconocidos y en este punto no habría mayor discusión. Además el tema de la demanda son los 4 primeros artículos del acto legislativo por la paz, sobre el cual **no pesa solamente una demanda sino varias que la Corte tendrá que resolver en orden cronológico.**

### **"Refrendación popular" dos palabras claves** 

Tanto Beltrán como Hernández plantean dos posturas diferentes en torno a la refrendación. Una de las discusiones se centra en si al referirse a **“refrendación popular” se está condicionando o no la aprobación por voto directo de los ciudadanos** o a través de sus representantes.

Asegura Beltrán , el congreso es el representante del pueblo que lo eligió con voto directo, es decir, la refrendación a través de los y las congresistas sería legítima porque, así sea de manera indirecta, los votos del legislativo representan la voluntad popular.

Mientra que para Hernández, el asunto va más allá, hacia la refrendación popular directa, es decir que el acuerdo debería someterse a un nuevo plebiscito u otro mecanismo de refrendación popular, como lo podrían ser los cabildos abiertos. Para Hernandez si lo refrenda el congreso no es refrendación popular.

### **Lo que viene** 

Aprobado el mecanismo de Fast Track y la refrendación por parte de la Corte Constitucional, en un primer fallo, se espera que el gobierno cite a sesiones extraordinarias para que se aprueben las medidas más urgentes que son casi condicionantes para el proceso de **dejación de armas y para la puesta en marcha de varios elementos del acuerdo**. (Le puede interesar [Tres medidas urgentes a implementar después del](https://archivo.contagioradio.com/las-tres-medidas-urgentes-para-implementar-el-acuerdo-de-paz-luego-del-fast-track/)Fast Track)

Se espera que una de las primeras reformas que proponga el gobierno para la implementación sean la ley de amnistía, el nombramiento de la comisión de seguimiento a los acuerdos y la puesta en marcha de la comisión de garantías para el ejercicio de la política. Esta última se entiende como crucial no solamente porque está en el acuerdo de paz sino **por la coyuntura de amenazas atentados y asesinatos contra líderes sociales.**

Además, quedan por delante dos demandas más contra el acto legislativo para la paz, en los que **se espera que la Corte mantenga la línea que ha trazado en la medida de aprobar el acto legislativo y mantener las facultades especiales la president**e para que se presenten como decretos presidenciales algunos asuntos de urgencia como la amnistía.

<iframe id="audio_14943400" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14943400_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_14943494" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14943494_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo]](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por][[Contagio Radio.]](http://bit.ly/1ICYhVU) 
