Title: Amenazas a periodistas evidencian fanatismo político en Colombia
Date: 2019-05-20 17:55
Author: CtgAdm
Category: DDHH, Política
Tags: Estigmatización en los medios, Libertad de Prensa, New York Times
Slug: amenazas-a-periodistas-evidencian-fanatismo-politico-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Periodismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Check/NYTimes] 

Los señalamientos por parte del Centro Democrático en contra del **periodista del New York Times, Nicholas Casey** y su posterior salida del país a raíz de la publicación del artículo que vincula al Ejército con cuestionables métodos para incrementar sus resultados, reabren un debate sobre la censura y estigmatización que sufren los periodistas en Colombia y el alcance que las declaraciones en redes sociales tienen en la realidad.

El periodista y cofundador de la Fundación para la **Libertad de Prensa (FLIP), Ignacio Gómez** expresó que sin importar de dónde vinieran las declaraciones o amenazas, la salida de Casey es un síntoma de la división política en Colombia, por lo que el Estado debería actuar al respecto pues tal "ambiente de polarización podría generar muerto, y de hecho ya los ha generado".  [(Lea también: Duplicar resultados a toda costa, la orden a militares en el gobierno Duque) ](https://archivo.contagioradio.com/duplicar-resultados-a-toda-costa-la-orden-a-militares-en-el-gobierno-duque/)

El periodista señaló que la situación en Colombia no debe ser un motivo de alarma para el New York Times pero sí expresó su preocupación frente "al grado de fanatismo" que existe en el país generando situaciones donde el periodista debe reflexionar antes de publicar "para que no lo vayan a confundir con un guerrillero o un paramilitar, es un factor de censura que priva a la sociedad", concluyó.

### Garantizar la vida de los periodistas no garantiza la libertad de prensa

Gómez hizo un llamado para proteger la libertad de prensa, a la que definió como **"el motor del pensamiento de un país, un país con limitaciones para expresar sus ideas",** agregando que con solo brindar seguridad a los periodistas no se resolverá el problema, "los escoltas no garantizan la libertad de expresión, garantizan la vida de los periodistas, pero ese no es el objetivo", afirmó,  agregando que lo que requiere Colombia es una mayor variedad en el periodismo y sus medios.

Acerca de los audios conocidos de mandos del Ejército increpando al general Diego Villegas por pedir perdón a la comunidad de Convención, Norte de Santander por el asesinato del excombatiente Dimar Torres, el cofundador de la FLIP indicó que las expresiones de las Fuerzas Militares están sujetas a las determinaciones de un Estado sobre la guerra y la paz, sin embargo expresó que estas permiten diagnosticar "una situación interna preocupante donde el reconocimiento de un error es interpretado como un ataque contra la institución".

<iframe id="audio_36108718" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36108718_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
