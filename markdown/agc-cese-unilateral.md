Title: Es posible que las AGC declaren cese unilateral antes de terminar esta semana
Date: 2017-12-13 15:14
Category: Nacional, Paz
Tags: AGC, ELN, Iglesia, paz
Slug: agc-cese-unilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/monsenor-dario-jesus-monsalve-cali-1200x600.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Publimetro 

###### 13 Dic 2017 

Con expectativa y confianza recibió **Darío de Jesús Monsalve**, arzobispo de Cali, la respuesta de las AGC ante el llamado a que ese grupo neoparamilitar y otros actores de la guerra evidencien acciones que aporten a la paz del país, "**Espero un compromiso unilateral de las AGC, y también del ELN**", y en esa vía, anhela que esta guerrilla tenga puesta la mirada en el beneficio de las víctimas, las comunidades y el pueblo, más que en el gobierno.

"Espero que la respuesta que me enviaron respondan hacia una paz completa, **espero que el gobierno sea recíproco y que la fiscalía, el gobierno y el Congreso se sintonicen para un buen marco jurídico que permita sumar colombianos en la causa de la paz**". expresa.

"**Las AGC van dar un paso. De ellos hay un interés en responder a ese espíritu de salvar la paz, la autogestión y ellos**". Sin embargo, indica que es necesario un marco legal, pues el Estado solo ha buscado "obtener trofeos policiales, esquivando el compromiso de desmontar esas estructuras de otra manera, poniendo bases sociales, jurídicas y penales reales, para que no se siga reproduciendo ese fenómeno"

No obstante, reconoce que **existen sectores de la institucionalidad de la fuerza pública están comprometidos en el proceso, pero los intereses pueden estar tirando en contra**, por eso afirma que debe fortalecerse la voluntad de paz de los sectores de la sociedad civil. "Que sea la paz el conector de todos los pueblos y las regiones", expresa.

**El papel del pueblo colombiano**

"El acuerdo de paz se ha sometido a una institucionalidad que no puede responder. A una democracia enferma y secuestrada por el clientelismo, corrupción, y unas fuerzas de violencia y narcotráfico", señala el Arzobispo, y agrega que eso "**ha sido fatal y se ha dejado al margen al interlocutor**".

Son las comunidades y el conjunto de la sociedad colombiana la que se autogestione para "organizarse a favor de la vida, la convivencia, la ocupación inteligente de los territorios, ya que la sociedad está siendo invadida no solo de producción, sino de consumo masivo de drogas por parte de los niños y jóvenes.

**Una voto por la paz**

De acuerdo con la percepción de Monsalve, "**la gente no confía en promesas oportunistas como las electorales**". Así pues considera que se debe fortalecer construcciones iniciadas y valores sembrados en la población y precisamente, frente a las elecciones del 2018 asegura que habría una gran oportunidad para el país y por eso es importante provocar una participación responsable en las elecciones, que sea antinarcotráfico, anticorrupción y proactivas en la búsqueda de la paz.

"Por eso me arriesgo a decirle a la gente que **ya el voto en blanco y la abstención no tienen sentido"**, pues no hay partidos que hagan efectiva la democracia, ya que estos se han vuelto "empresas electorales para y comprar y vender votos y firmas".

Finalmente, el arzobispo propone una **séptima papeleta por la paz en las elecciones del 2018**, en el que se evidencie el clamor y el respaldo del pueblo de cara a los acuerdos de paz.

<iframe id="audio_22637246" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22637246_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
