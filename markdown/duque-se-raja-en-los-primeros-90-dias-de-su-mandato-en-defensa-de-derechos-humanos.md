Title: Duque se raja en los primeros 90 días de su mandato en defensa de derechos humanos
Date: 2018-11-07 18:15
Author: AdminContagio
Category: DDHH, Nacional
Tags: acuerdos de paz, Gobierno, Iván Duque
Slug: duque-se-raja-en-los-primeros-90-dias-de-su-mandato-en-defensa-de-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/ivan-duque.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Iván Duque ] 

###### [07 Nov 2018] 

Se cumplen 90 días de Iván Duque en la presidencia del país y el balance en términos de derechos humanos, de acuerdo con Alberto Yepes, vocero de la Coordinación Colombia Europa Estados Unidos, es preocupante, debido a que se han profundizado los incumplimientos en los Acuerdos de Paz y **se han adelantado medidas para limitar los derechos fundamentales como el derecho a la protesta**.

### **Hay una regresión en garantizar la implementación de los Acuerdos de Paz** 

Para Yepes, este gobierno "no ha mostrado voluntad" por cumplir con los Acuerdos de Paz, ejemplo de ello sería la falta de interés por promover la participación de las regiones más afectadas por el conflicto armado, los diversos golpes que se le han intentado dar a la JEP para que no se garantice el derecho a la verdad, justicia y reparación y el incumplimiento al punto sobre reforma rural, **puntualmente en las modificaciones que se le hicieron a la Ley de Restitución de Tierra.**

Además, uno de los hechos más regresivos que se ha venido gestando en su mandato, según Yepes, tiene que ver con acabar los planes de sustitución de cultivos de uso ilícito, pactados en La Habana y el retorno a las fumigaciones con glifosato, en conjunto con erradicación forzada por parte del Ejército.

De igual forma expresó que aún **no existe ninguna medida frente al desmonte de las estructuras paramilitares,** que por el contrario, han aumentado su accionar en regiones del país como el Cauca y Antioquia. (Le puede interesar: ["Ejército nunca llegó a comunidad indígenas confinada por paramilitares"](https://archivo.contagioradio.com/ejercito-nunca-llego-comunidad/))

### **Las limitaciones a los derechos fundamentales** 

Yepes, resaltó que no se pueden olvidar afirmaciones como las del ministro de Defensa, Guillermo Botero, referente a señalar que las protestas eran financiadas por sectores ilegales, **deslegitimando este derecho fundamental y estigmatizando a quienes se movilizan.**

Asimismo, resaltó que hasta el momento no se ha gestado ningún espacio de conversaciones con el movimiento social o con las plataformas de Derechos Humanos, ni se ha impulsado compromisos o pactos que se habían firmado con sectores campesinos o indígenas en otros gobiernos. (Le puede interesar: ["Modificaciones a la JEP sería inconstitucionales"](https://archivo.contagioradio.com/modificacion-a-tribunales-de-la-jep-es-inconstitucional-gustavo-gallon/))

Finalmente, Yepes destacó que estos 90 días de mandato de Iván Duque han estado marcados por beneficios y leyes a sectores empresariales, como es el caso de la posible Reforma Tributaria y **"castigando" a los sectores populares**.

### **Los retos del movimiento social y las plataformas de DD.HH**

Frente a esta situación Yepes aseguró que el movimiento social, las diversas organizaciones de derechos humanos y de víctimas deben continuar presionando para que se de la implementación de los Acuerdos de paz, continuar con los diálogos con el ELN.

<iframe id="audio_29925559" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29925559_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
