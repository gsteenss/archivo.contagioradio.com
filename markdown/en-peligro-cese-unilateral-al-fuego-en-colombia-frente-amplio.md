Title: "En peligro cese unilateral al fuego en Colombia" Frente Amplio
Date: 2015-01-30 17:16
Author: CtgAdm
Category: Paz, Política
Tags: cese al fuego unilateral FARC-EP, conversaciones de paz, Frrente Amplio por la Paz
Slug: en-peligro-cese-unilateral-al-fuego-en-colombia-frente-amplio
Status: published

###### Foto: noticias.telemedellin.tv 

Los **bombardeos indiscrimandos, la continuidad de los desembarcos , el asesinato de Carlos Pedraza y las amenazas contra promotores de la paz**, son las razones por las cuales el cese de fuego unilateral de las FARC está en peligro, señala el comunicado número 009 del **Frente Amplio por la Paz**, organización de la sociedad civil, veedora del cese al fuego.

El Frente Amplio expresa también “**rotundo rechazo a la continuidad de la ofensiva militar de la fuerza pública, contra las estructura de las FARC-EP** que se encuentran en plena aplicación de la decisión de Cese Unilateral” e instan al presidente Juan Manuel Santos a que se tomen las medidas necesarias y se le dé un tratamiento adecuado a la decisión de esa guerrilla.

Por otra parte, el asesinato del defensor de DDHH **Carlos Pedraza** y las reiteradas amenazas en contra de personas comprometidas con la construcción de la paz  corroboran que hay “**planes sistemáticos para atacar a los trabajadores y trabajadoras de la paz**” desde estructuras militares que aún no se identifican y mucho menos se desarticulan.

El comunicado de 6 puntos también hace un llamado a la sociedad colombiana a integrarse al esfuerzo de la paz y convoca a que haya 1.000.000 de veedores de cese al fuego y promotores de la paz.

Anexamos el comunicado del Frente Amplio.  
<!--more-->

*FRENTE AMPLIO POR LA PAZ, LA DEMOCRACIA Y LA JUSTICIA SOCIAL*

*COMUNICADO DE PRENSA 009*

*“Los ojos de la sociedad civil colombiana velando por la paz para todos”*

*EN PELIGRO CESE UNILATERAL AL FUEGO EN COLOMBIA*

*Enero, 29 de 2014*

*En cumplimiento de nuestro papel como veedores del Cese Unilateral al Fuego decretado por las FARC-EP y como sujetos activos en el propósito de hacer realidad la terminación del conflicto armado, comunicamos.*

1.  *El frente amplio ha constatado un aumento de las amenazas contra sectores y líderes sociales, que trabajan por la paz y los derechos humanos, en un intento claro de generar un ambiente de terror e inmovilidad del movimiento a favor de una paz definitiva en Colombia.*
2.  *Estas amenazas se han concretado entre otras, en el vil asesinato en la ciudad de Bogotá del compañero defensor de Derechos Humanos Carlos Alberto Pedraza, quién era integrante del Congreso de los Pueblos y el MOVICE. Hoy una víctima más de la intolerancia,  cuyo asesinato corrobora la existencia de planes sistemáticos para atacar a los trabajadores y trabajadoras de la paz y  contradice  las afirmaciones de las autoridades de Gobierno en cabeza de Juan Manuel Santos, que pretenden desconocer la existencia de grupos paramilitares y otros grupos criminales, actitud que  los compromete frente a la responsabilidad de los acontecimientos por la falta de acciones contundentes contra estos grupos que desde fuera o dentro del Estado atacan al movimiento por la paz en Colombia.*
3.  *Expresamos nuestro rotundo rechazo a la continuidad de la ofensiva militar de la fuerza pública, contra las estructura de las FARC-EP que se encuentran en plena aplicación de la decisión de Cese Unilateral. Hacemos un llamado al Presidente para que tome las medidas necesarias y  dé  un tratamiento adecuado a la situación de cese al fuego unilateral de las FARC-EP  para que  se evite un levantamiento de esta decisión, que tantos beneficios ha producido para las comunidades en el tiempo de su vigencia.  Hacemos un llamado a los países garantes, a la comunidad internacional, a otros sectores de la sociedad colombiana, como partidos políticos e iglesias y a la sociedad en general, a que soliciten del Gobierno Nacional una actitud consecuente con el cumplimiento del cese unilateral y plena consideración de los efectos humanitarios que tendría  un levantamiento de la tregua.*
4.  *La actual situación de tensión que amenaza con la terminación del Cese Unilateral del Fuego y Hostilidades decretado por las FARC-EP, demuestra una vez más la necesidad de que lo más pronto posible las partes acuerden un Cese Bilateral al Fuego, para que se mantenga un  ambiente favorable hacia   el logro del acuerdo de paz.*
5.  *Resaltamos que hemos recibido información proveniente de dos organizaciones en región, en la que nos expresan su preocupación, por la continuidad de bombardeos y desembarco de tropas que ponen en riesgo a comunidades de por lo menos dos departamentos, Chocó y Meta.*
6.  *Convocamos a todas las organizaciones sociales y políticas miembros del Frente Amplio por la Paz en las regiones, y a las comunidades en general, para que juntos conformemos una red de por lo menos 1.000.000 DE VEEDORES DEL CESE UNILATERAL AL FUEGO EN COLOMBIA y de Promotores de la paz y la negociación del conflicto armado.*

*FRENTE AMPLIO POR LA PAZ, LA DEMOCRACIA Y LA JUSTICIA SOCIAL*

*“Los ojos de la sociedad civil colombiana velando por la paz para todos”*
