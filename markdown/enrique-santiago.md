Title: Ley de amnistía es la más completa que se ha aprobado en Colombia
Date: 2016-12-29 14:30
Category: Entrevistas
Tags: Amnistia, colombia, enrique santiago, FARC-EP, proceso de paz
Slug: enrique-santiago
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/enrique-santiago-entrevista.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/ENRIQUE-SANTIAGO-A-BLUERADIO.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Enrique-Santiago.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Archivo 

##### 29 Dic 2016 

Con la aprobación de la Ley de amnistía en Colombia en el Congreso de la República, se abrió en firme la puerta para iniciar la implementación de los acuerdos de paz, que **permitirá avanzar en la concentración e incorporación a la vida civil de aproximadamente 15 mil integrantes de las FARC EP**.

Surgen interrogantes de cómo será su desarrollo y cuáles sus alcances tanto para guerrilleros como para todas las personas involucrados en el conflicto.

El asesor jurídico del proceso de paz Enrique Santiago, aseguró que para p**oner en práctica la ley de amnistía y de tratamientos proporcionales para agentes del Estado es necesaria la aprobación de la Jurisdicción Especial para la Paz JEP**, que ya se encuentra radicada en el Congreso.

"Sin la aprobación de la JEP la ley de amnistía no puede aplicarse al cien por cien", por la necesidad de **acudir a las instancias jurídicas que en esta se contemplan para resolver el estado de quienes son beneficiarios** de la ley que hoy se encuentra para su firma en manos del presidente Juan Manuel Santos.

Para dar certeza a la sociedad en la transparencia del proceso de Amnistía, el abogado aseguró que desde que el tema fue puesto en la mesa de negociación tanto el **Gobierno como las FARC-EP vienen trabajando para que cada beneficiario lo sea realmente.** Preciso que el primer criterio se basa en decisiones de los jueces y sus providencias, se añadirán unos listados que harán las FARC cuando se adelante el proceso de dejación de armas.

El defensor manifestó que " en principio se van a beneficiar de la amnistía no solo los encarcelados, es decir todos los guerrilleros y guerrilleras de las FARC".  En cuanto al número de guerrilleros, milicianos y colaboradores de las FARC-EP que se encuentran en prisión p**odría rondar entre los 2.500 y los 3.ooo**. Sin embargo asegura que es complicado determinarlo por que **en muchos casos los procesados han sido condenados en tribunales por delitos distintos al de rebelión**, lo que a su juicio se abstrae "a la realidad de que en Colombia había un conflicto armado y una guerrilla en armas". En tales situaciones los procesados deberán esperar un poco más para ser amnistiados.

En caso que el gobierno encuentre problemas en la verificación de los listados, **las FARC deberán emitir unas acreditaciones que permitan dar cuenta de la participación de estas personas al interior de esa guerrilla**, estas serán tanto de índole judicial como por las declaraciones u otros elementos de prueba que hayan sido presentados en cada proceso "va a haber un examen caso a caso (...) , las FARC no tienen ningún interés en que haya una sola persona a la que se le aplique la amnistía de forma indebida por que eso sin duda iría en absoluto descrédito de todo el proceso" aseguró el abogado.

**Agentes del Estado**

En cuanto a los agentes del Estado procesados y condenados por ejecuciones extrajudiciales, Santiago hace claridad en que **la ley de amnistía "excluye de la aplicación las ejecuciones extrajudiciales y asesinatos en personas protegidas"**, y en caso de tratarse de homicidios cometidos en combate la Sala de Definición de situaciones Jurídicas tendrá que comprobarse caso por caso si se acoge en tales casos a las reglas de combate.

El abogado recuerda que existe una prohibición internacional para quienes estan involucrados en este tipo de delitos para recibir amnistía. Del mismo modo el mecanismo no operará ara quienes hallan cometido delitos de lesa humanidad, toma de rehenes u otra privación grave de la libertad, tortura, desaparición forzada, violencia sexual, desplazamiento forzado o reclutamiento de menores, entre otros.

En el caso de las fuerzas militares tampoco aplica para delitos contra el servicio, la disciplina, los intereses de la Fuerza Pública, y el honor y la seguridad de la misma.

**La situación de las zonas veredales**

El asesor jurídico manifestó que **las demoras en la construcción de infraestructura en las zonas veredales no afectarán el calendario de las FARC para la dejación de las armas.** "Los guerrilleros ya están en las puertas de las zonas veredales listos para entrar en el momento en que se esté construyendo esta infraestructura”.

A pesar de los retrasos en las zonas, que el defensor adjudica a la contratación de particulares, Santiago asegura que **las FARC son las más interesadas en que las zonas estén construidas con urgencia para iniciar el proceso de dejación de armas** que debe iniciar según el calendario en el día  D+180.

En cuanto a las declaraciones del Gobernador de Antioquia Luis Pérez, sobre la falta de control de los guerrilleros en zonas ubicadas en su jurisdicción y la acusación de delitos como abuso de menores e ingreso de prostitutas, Santiago se pregunta "**¿a quien se le ocurre que en zonas veredales puedan estar cometiendo tal tipo de atrocidades o en la zonas de pre concentración donde esta el ejército, la policía y el mecanismo monitoreo internacional?**" añadiendo que se trata de los mismos argumentos utilizados por la campaña y que casualmente se hace pública el mismo día en que debía aprobarse la ley de amnistía en el Congreso de la República.

Sobre las críticas a la Ley de amnistía.

En atención a las críticas surgidas en algunos sectores políticos y organizaciones de DDHH como Human Rights Watch, el abogado asegura que "e**l texto aprobado ha ido recogiendo recomendaciones, apostaciones y mejoras desde que quedo aprobado por primera vez en el mes de agosto**", sin embargo apunta que muchas de esas críticas están fuera del contexto y no se han relacionado con la Jurisdicción Especial de Paz que "califica las conductas criminales conforme al código penal colombiano como al derecho internacional con la finalidad que no  haya la posibilidad que ninguna conducta grave quede sin ser perseguida".

El abogado asegura que la presente "**es la ley de amnistía mas precisa y mas completa que se ha aprobado en la historia de Colombia y el que tenga dudas que coja los textos de la anteriores y compare**". Le puede interesar: [Ley de amnistía, un mecanismo para afrontar la impunidad del pasado](https://archivo.contagioradio.com/ley-amnistia-mecanismo-afrontar-la-impunidad-del-pasado/).
