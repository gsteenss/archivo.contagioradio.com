Title: Promover la paz en los territorios: el desafío de Defendamos la Paz para el 2020
Date: 2020-02-21 18:34
Author: AdminContagio
Category: Nacional
Slug: promover-la-paz-en-los-territorios-el-desafio-de-defendamos-la-paz-para-el-2020
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Defendamos-la-Paz.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto Defendamos La Paz: Alcaldía de Bogotá

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En el marco de la I Cumbre por la Paz Territorial se celebró el primer año de existencia de un[movimiento](https://twitter.com/defendamospaz?lang=es) que ha dedicado sus esfuerzos a mantener la paz como un tema de la agenda pública, promoviendo la implementación del Acuerdos de Paz, la vida de los liderazgos sociales y excombatientes de FARC y la defensa de las 16 circunscripciones especiales para la paz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Promover la implementación del Acuerdo en Bogotá

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Fue en medio de este escenario que contó con la presencia de diferentes figuras incluidos los exnegociadores de La Habana Humberto de la Calle y Rodrigo Londoño; el jefe de la Misión de Verificación de la ONU, Carlos Ruiz Massieu; el presidente de la Comisión de la Verdad, Francisco de Roux; y el consejero presidencial para la estabilización y la consolidación, Emilio Archila; además de cerca de 500 personas, incluidos alcaldes y gobernadores de municipios estratégicos para la construcción de la paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Alcaldía de Bogotá propuso constituir una réplica de los **Programas de Desarrollo con Enfoque Territorial (PDET) en Bogotá.** Dichos programas estarían situados en la localidad de Sumapaz, así como entre Bogotá y Soacha, zonas donde habitan la mayoría de las víctimas del conflicto que viven en la capital. Además, se acordó conformar una Red Nacional de Autoridades Locales por la Paz que asumirá compromisos para la implementación del Acuerdo en la ciudad.

<!-- /wp:paragraph -->

<!-- wp:image {"id":81049,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![Defendamos la Paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Defendamos-la-Paz-1024x682.jpeg){.wp-image-81049}  

<figcaption>
Foto: Alcaldía de Bogotá

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Cuatro logros de Defendamos la Paz en 2019

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

"A veces los colombianos no alcanzamos a dimensionar el impacto de lo que significó la firma del Acuerdo de Paz", manifestó Juan Fernando Cristo, exministro de Interior e integrante de esta plataforma. A su vez, agradeció a Emilio Archila por acudir al encuentro y a quien instó a que, como representante del Gobierno, se respalde a la Jurisdicción Especial para la Paz (JEP). [(Lea también: Gobierno Duque cumple un año obstaculizando la paz y la garantía de DDHH)](https://archivo.contagioradio.com/gobierno-duque-cumple-un-ano-obstaculizando-la-paz-y-la-garantia-de-ddhh/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cristo se refirió a cuatro logros claves a lo largo del primer año del movimiento resaltando en primer lugar el trabajo mancomunado realizado con un sector del Congreso y la Corte Constitucional para promover la Ley Estatutaria de la JEP, de cara a las objeciones del presidente Duque quien finalmente .

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En segundo, lugar hizo referencia a la visibilización de la "tragedia de los líderes y excombatientes de FARC asesinados en Colombia", un hecho que se vio reflejado en la marcha que se convocó el pasado 26 de julio y que contó con la numerosa participación de diversos sectores, a este hecho sumó la defensa del Acuerdo de paz internacionalmente, encomendando a las naciones que han apoyado el proceso que no dejen a Colombia sola en este desafío. [(Recomendamos leer: “Desmantelamiento de organizaciones criminales debe empezar ya” Defendamos La Paz)](https://archivo.contagioradio.com/desmantelamiento-de-organizaciones-criminales-debe-empezar-ya-defendamos-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Finalmente, el cuarto logro, es la defensa de las curules para la paz, que esperan en este año haya un avance positivo en la conciliación con el presidente del Senado, para que se puedan elegir a los 16 representantes de las víctimas del conflicto en el Congreso, un desafío que para este 2020, permitiría el fortalecimiento de la paz en los territorios de Colombia. [(Le puede interesar: Propuesta del Gobierno sobre Curules de Paz busca desconocer lo acordado en La Habana)](https://archivo.contagioradio.com/curules-gobierno-desconocer-acuerdo-habana/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
