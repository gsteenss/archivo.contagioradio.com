Title: Desconocidos incineran La Casa del Viento en San Cristóbal, Bogotá
Date: 2017-01-09 13:22
Category: Educación, Nacional
Tags: Bibliotecas, Bogotá, San Cristóbal
Slug: desconocidos-incineran-la-casa-del-viento-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/xxx-1038x576.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Casa del Viento] 

###### [9 Enero 2017] 

En horas de la mañana de este lunes, se dio a conocer  que **desconocidos incineraron *La Casa del Viento*, un proceso cultural de la Promotora Cultural Zuroriente** ubicado en la localidad de San Cristóbal, al sur oriente de Bogotá.

***La Casa del Viento* es un proceso de autoconstrucción física y social de la ampliación de la Biblioteca Pública comunitaria Simón el Bolívar**, que se desarrolló en el marco del 4to. Encuentro de Arquitectura Expandida.

La iniciativa partió de la Corporación Cultural Zuroriente, organización que gestiona la pequeña infraestructura que fue construida por los vecinos ante la falta de equipamientos sociales y comunitarios. Así mismo, fueron varios los colectivos que desde distintas perspectivas disciplinares acompañaron a la comunidad en este proceso. Le puede interesar:[ "Le Big Sur" Arte, música y camino.](https://archivo.contagioradio.com/le-big-sur-arte-musica-y-camino/)

Según la breve comunicación dada a conocer en redes sociales, **con este acto se ve afectado un proyecto que tiene 20 años de existencia y que incluye la Biblioteca Simón El Bolívar "hay dolor y tristeza en el campo cultural bogotano. Se requiere una acción solidaria para volver a construirla"** aseguran. Le puede interesar: ['Gasimba', 'bacán', 'camello' hacen parte del nuevo Diccionario de Colombianismos.](https://archivo.contagioradio.com/gasimba-bacan-camello-hacen-parte-del-nuevo-diccionario-de-colombianismos/)

Noticia en Desarrollo...

Ver más información de *La Casa del Viento a*quí: <http://bit.ly/2ib9NDE>

Ver Documental sobre La Casa del Viento

\[embed\]https://vimeo.com/39661202\[/embed\]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
