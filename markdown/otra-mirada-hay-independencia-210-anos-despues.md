Title: Otra Mirada: ¿Hay independencia 210 años después?
Date: 2020-07-22 09:39
Author: AdminContagio
Category: Nacional, Otra Mirada, Otra Mirada, Programas
Tags: 20 de julio, Independencia de Colombia
Slug: otra-mirada-hay-independencia-210-anos-despues
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/independencia-bandera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Agudelo41

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El 20 de Julio se celebrarán los 210 años desde que Colombia dio su grito de independencia, proceso que marco el final del período de dominio del Imperio español y que erroneamente se cree, permitió a los criollos, negros y mestizos construir su propio país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, la pregunta que surge es qué tan real fue esa independencia para los pueblos originarios, pero sobre qué tan independientes y libres somos después de 210 años teniendo en cuenta que quienes más sufrieron en esa época son pueblos que el día de hoy siguen siendo víctimas de violencia, asesinatos y olvido por parte del estado y de la sociedad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A estas preguntas, dieron respuestas Sonia López vocera del Movimiento Político de Masas social y Popular del Centro Oriente de Colombia, Ariel Palacios, integrante del Consejo Nacional de Paz Afrocolombiano y Jhoe Sauca, coordinador de derechos humanos del Consejo Regional Indígena del Cauca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como personas que siguen luchando por la verdadera independencia y libertad de sus comunidades, explican cuál es la perspectiva que tienen de este grito de independencia y qué se ha aprendido en el pasado para no repetirlo en estos momentos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También explican en qué momento dejó de existir ese sueño que se creyó lograr con el acuerdo de paz de tener una nación que incluyera a todas las comunidades y cómo las injusticias que se siguen presentando son un impulso para luchar desde movimientos como las diversas marchas que han surgido desde departamentos como el Cauca y el Norte de Santander. (Si desea saber más: [Marcha por la Dignidad arribará el 20 de julio a Bogotá buscando una «verdadera independencia»](https://archivo.contagioradio.com/marcha-por-la-dignidad-arribara-el-20-de-julio-a-bogota-buscando-una-verdadera-independencia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, explican que luchar en estos momentos es una responsabilidad histórica que tenemos con quienes antes y ahora han pensado diferente y se atreven a proponer proyectos alternativos a los que nos han estado imponiendo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Si desea escuchar el programa del 16 de julio: [La comisión de la verdad: La verdad del pueblo negro](https://www.facebook.com/contagioradio/videos/2654846704781926))

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/900877967075430","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/900877967075430

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
