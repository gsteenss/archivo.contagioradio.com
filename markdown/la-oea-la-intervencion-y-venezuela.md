Title: La OEA, la intervención y Venezuela
Date: 2016-06-04 20:18
Category: Cesar, Opinion
Tags: Luis Almagro, Nicolas Maduro, OEA Carta democrática
Slug: la-oea-la-intervencion-y-venezuela
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/unnamed64-e1465089385981.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Resúmen Latinoamericano 

#### Por [César Torres Del Río](https://archivo.contagioradio.com/cesar-torres-del-rio/)

##### [4 Junio 2016] 

La Organización de Estados Americanos (OEA) es sinónimo de confiscación de las soberanías, es decir, de intervención (ilegal) legalizada y legitimada. Desde su fundación en 1948 (con pre-existencia a finales del siglo XIX) ha sido un instrumento de coerción y de chantaje socio-político. Alberto Lleras Camargo - su primer secretario y expresidente de Colombia – supo diseñar el edificio jurídico-político; los demás secretarios han sido aprendices de brujo. Desde otro ángulo, la OEA ha sido, y es, instrumento de multilateralización del unilateralismo norteamericano; en pocas palabras, las definiciones gringas de política exterior - militares y políticas - se trasladan al organismo continental y pasan, y posan, como “multilaterales”. Así mismo, la OEA es un órgano de abyección política en tanto que los gobiernos, y Estados, allí representados aceptan de buen agrado su subordinación al hegemón mundial; es lo que el argentino Félix Peña denomina “dependencia consentida”.

La que ha hecho el señor Almagro, actual secretario, no es, pues, nuevo. Al apelar a la Carta Democrática de la OEA para tratar la situación interna de Venezuela, sin el consentimiento del gobierno de la República Bolivariana, ha procedido de modo intervencionista y con conocimiento de causa. Tanto él como su antiguo jefe, el expresidente uruguayo Mujica, han calculado sus actos políticos (este último tildando a Maduro como “más loco que una cabra”) en defensa de la decadente democracia continental; era de esperarse: la “democracia” se defiende interviniéndola, o sea, pasando por encima de las soberanías concertando la “seguridad colectiva”, señorial y “democrática”. Ese es el telos de la economía de mercado y del “progreso” social.

Pero el gobierno de Maduro se movió mejor diplomáticamente, y con justicia; detuvo la conjura mediática, real y política (de la OEA, López, Mujicas y Pastranas) contra el soberano pueblo de Venezuela mediante el apoyo de UNASUR, países del Caribe y el grupo ALBA. Buen punto en medio del partido …

Sin embargo, la situación interna es desesperante. La inflación pronto será de cuatro dígitos (está en 700%); la escasez de alimentos es evidente; la inseguridad ciudadana aterra; los precios de todo están por las nubes; el mercado negro impone sus leyes y los acaparadores están en su agosto; el contrabando asegura la supervivencia diaria; el salario mínimo es bien mínimo; la inquietud social comienza a desbordarse. Al mismo tiempo la derecha (o mejor, las derechas pues aún no alcanzan su punto de equilibrio) gana espacios interna y externamente.

Por el lado del chavismo, el programa político no acierta dentro de lo institucional y el gobierno no ofrece garantías de cambio ni se sustenta en la movilización popular y ciudadana (al contrario, la elude y la está limitando); los cuadros dirigentes del PSUV aparecen desorientados y no encuentran en la retórica de Maduro posibilidades de acción (el llamamiento a las armas, por ejemplo). De allí que, según fuentes serias, las Fuerzas Armadas se sientan perturbadas, con voces discordantes sobre el apoyo al gobierno y sobre el “proyecto” de la derecha; en otras palabras, como actores políticos que son, en los próximos días veremos sus pronunciamientos y sus definiciones.

Pero no hay nada predeterminado; el futuro inmediato no se ha decidido. Como siempre ha ocurrido, en los vencidos, en los subalternos, está la posibilidad, sólo eso, de apuntar a un redireccionamiento de la situación, de definirla; el camino está lleno de bifurcaciones, de contra-tiempos; hay que aguzar la vista y el oído y construir, con esfuerzo pero sin pausa, el terreno de la dirección política independiente, que incluya alianzas, claro, pero *independiente*. La retórica populista y las alianzas por arriba (con gobiernos, con multinacionales, con el capital) conducen al abismo. La apuesta ha sido hecha.
