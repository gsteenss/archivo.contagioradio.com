Title: Santrich es dado de alta de hospital mientras hay expectativa frente a su caso
Date: 2019-05-19 13:48
Author: CtgAdm
Category: Judicial, Nacional
Tags: Fiscalía, JEP, Jesús Santrich
Slug: santrich-es-dado-de-alta-de-hospital-mientras-hay-expectativa-frente-a-su-caso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Jesus-Santrich-6-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

Tras ingresar la madrugada del sábado al hospital Méderi,  luego de ser recapturado por la Fiscalía minutos después de salir de la cárcel La Picota,  **Jesús Santrich**,  quien llegó a la unidad de cuidados intensivos  se encuentra estable  y fue trasladado al bunker  de la Fiscalía pese a que no se conoce un informe médico oficial.

A lo largo del fin de semana, un juez de control de garantías legalizó la captura de Seuxis Paucias Hernández, mientras este lunes 20 se adelantarán las audiencias de imputación de cargos y medida de aseguramiento, pese a que la Procuraduría apeló a esta decisión,

Aunque la **Jurisdicción Especial para la Paz** ordenó no continuar con el trámite de extradición y solicitó la libertad del exnegociador de paz, Santrich volvió a ser detenido por la Fiscalía que manifestó contar con nuevas pruebas en su contra. [(Lea también: El Fiscal buscará la forma para que Santrich permanezca en la cárcel: Gustavo Gallardo)](https://archivo.contagioradio.com/fiscal-santrich-carcel/)

Por su parte, el dirigente del partido Farc, **Timoléon Jiménez** convocó a una reunión de emergencia junto a los excomandantes firmantes del acuerdo citados por  la JEP, señalando vía Twitter que "Santrich nunca estuvo libre, se le violaron todos sus derechos. Se montó espectáculo para humillarlo y golpear el proceso de paz".

Además, exhortó a los miles de hombres y mujeres que acogieron el acuerdo a "fortalecer su unidad y no vacilar un instante en la importancia de lo pactado". Asimismo el abogado de Santrich, Gustavo Gallardo señaló que estarán prestos para comparece y demostrar "la arbitrariedad que se está cometiendo".

> ?[\#Declaración](https://twitter.com/hashtag/Declaraci%C3%B3n?src=hash&ref_src=twsrc%5Etfw) ?[@PartidoFARC](https://twitter.com/PartidoFARC?ref_src=twsrc%5Etfw) frente a los ataques que buscan atizar el miedo, romper la confianza y afectar la seguridad jurídica de los exguerrilleros que firmaron la paz y a los hechos ocurridos nuestro compañero [@JSantrich\_FARC](https://twitter.com/JSantrich_FARC?ref_src=twsrc%5Etfw) [pic.twitter.com/74PbOyctbS](https://t.co/74PbOyctbS)
>
> — Rodrigo Londoño (@TimoFARC) [19 de mayo de 2019](https://twitter.com/TimoFARC/status/1129957572449058816?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]

.
