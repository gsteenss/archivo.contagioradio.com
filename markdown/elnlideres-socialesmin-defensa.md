Title: "Declaraciones de Min Defensa son Temerarias":ELN
Date: 2017-03-18 12:19
Category: Nacional, Paz
Tags: ELN, lideres sociales, Ministro de defensa, paz
Slug: elnlideres-socialesmin-defensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/ministro-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Min defensa 

###### 18 Mar 2017 

El Ejército de Liberación Nacional ELN, emitió este viernes un comunicado en relación con las declaraciones presentadas por el ministro de Defensa Luis Carlos Villegas, en las que desestima el informe publicado por la oficina del Alto Comisionado de las Organización de las Naciones Unidas en Colombia, sobre las amenazas y asesinatos a líderes sociales en Colombia y la[impunidad frente a crímenes por parte de Agentes Estatales](https://archivo.contagioradio.com/onu-llama-la-atencion-posible-impunidad-frente-crimenes-parte-agentes-estatales/).

En la misiva el ELN se refiere particularmente a las declaraciones de Ministro ante los medios, en las que asegura que un alto porcentaje en investigaciones relacionadas con los ataques a líderes sociales refiere a “quienes se opusieron a mantener cultivos ilícitos o minería ilegal”, que a su juicio demuestran vinculaciones con el ELN. Aseveraciones que esa guerrilla califica como "graves e infundadas", solicitando que contribuyan con una actividad investigativa de verdad, en aras de esclarecer "tan temerarias acusaciones", que a su parecer buscan debilitar los planteamientos planteados por ellos en la mesa de Quito.

En su respuesta insta además a la Fiscalía general de la Nación a dejar el silencio y poner en conocimiento de la opinión pública las investigaciones realizadas en el Departamento del Cauca, en las que se indica que la organización acusada del asesinato de varios dirigentes no tiene vínculo o ni relación con el ELN y en la mayoría de los casos se relacionaban con la oposición de las comunidades a las explotaciones minera.

Por último hace un llamado a las organizaciones sociales y de Derechos Humanos, a los medios de comunicación y a la ciudadanía en general, a no facilitar el encubrimiento de las responsabilidades que debe asumir el Estado ante el sistemático asesinato de líderes y a procurar el esclarecimiento de la verdad. Le puede interesar: [Ante ola de asesinatos de líderes organizaciones de DD.HH acuden a CIDH](https://archivo.contagioradio.com/situacion-de-lideres-sociales-en-colombia-ante-la-cidh/).

A continuación, la transcripción del comunicado completo.

Señor Todd Howland

Representante de la Oficina en Colombia del Alto Comisionado de las Naciones Unidas para los Derechos Humanos

Señor Christoph Harmisch

Jefe de la Delegación del Comité Internacional de la Cruz Roja en Colombia

Doctor Carlos Negret

Defensor del pueblo

El día de hoy, el Ministro de Defensa Luis Carlos Villegas hizo graves e infundadas denuncias contra el Ejército de Liberación Nacional, relacionándonos con el ataque a líderes sociales.

En aras de la verdad, a la que tiene derecho el país y la comunidad internacional, frente a los graves hechos que se vienen presentando, por el nuevo genocidio que está en curso, les solicitamos contribuir con una actividad investigativa y de verdad, frente a tan temerarias acusaciones. De nuestra parte estamos dispuestos a prestar todo nuestro concurso para que ello se esclarezca.

Con estas falsedades del Ministro de defensa, lanzadas en forma ligera a los medios de comunicación, el gobierno busca debilitar nuestros planteamientos en la mesa de conversaciones de Quito, crear confusión sobre el asesinato creciente de líderes sociales, encubrir a los verdaderos responsables y exculparse de su responsabilidad y la de las Fuerzas armadas estatales, frente a tales hechos.

Igualmente, le solicitamos a la Fiscalía que ponga en conocimiento de la opinión publica las investigaciones realizadas en el Departamento del Cauca por el Director seccional de la entidad, Raúl Humberto González y el Director seccional del CTI, en las que se indica que la banda acusada del asesinato de varios dirigentes -a causa de la oposición de las comunidades a la explotación minera-, no tiene vínculo, ni relación alguna con el ELN.

Cabe preguntar, ante el silencio del Fiscal, ¿La Fiscalía Cauca no informa o le miente al Fiscal General? ¿El gobierno sabe la verdad sobre estos hechos y miente deliberadamente?

Llamamos a los organismos defensores de los Derechos Humanos, a los medios de comunicación, a las organizaciones sociales y a todas las gentes de bien de Colombia, a no facilitar el encubrimiento que con estas declaraciones, pretende hacer el Ministro de defensa de sus propias responsabilidades en los crecientes asesinatos de líderes sociales y defensores de Derechos Humanos. Igualmente los invitamos a contribuir en el esclarecimiento de la verdad.

Delegación de Diálogo

Ejército de Liberación Nacional

<iframe src="https://www.youtube.com/embed/O6Mk7Iaq8q4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
