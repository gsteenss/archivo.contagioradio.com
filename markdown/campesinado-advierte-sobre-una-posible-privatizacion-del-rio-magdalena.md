Title: Campesinado advierte privatización del Río Magdalena
Date: 2015-04-01 18:20
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, ANLA, Autoridad Nacional de Licencias Ambientales, Corpomagdalena, EMGESA, Fauna, flora, HidroChina, Hidroeléctricas, Huila, megarproyectos, Quimbo, represas, Río Magdalena
Slug: campesinado-advierte-sobre-una-posible-privatizacion-del-rio-magdalena
Status: published

##### Foto: notiagen.wordpress.com 

<iframe src="http://www.ivoox.com/player_ek_4290603_2_1.html?data=lZemkpuUd46ZmKiak5iJd6Kll5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Lh0crgy9PFqNCfwsnjy8rWuMaf1NTP1MqPuc%2FVjNXc1c7GsMaf0dfW2MbYrdvVxM6SpZiJhpTijMmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Robinson Rojas, Comité en Defensa del Río Magdalena] 

Campesinos del Huila y de varios departamentos en la rivera del río Magdalena, denuncian que empresas como **Emgesa e HidroChina** estarían detrás de la licitación para la construcción de nueve represas al sur del país, en base al Plan Maestro "El río de la pátria"  para el aprovechamiento del Río Magdalena, que tiene previsto el gobierno.

Campesinos y campesinas que han realizado el seguimiento a esa situación, denuncian que la ANLA, Autoridad Nacional de Licencias Ambientales, no ha suministrado información sobre cómo se piensan llevar a cabo estos mega proyectos, que traerían graves **consecuencias, sociales, ambientales y económicas para ellos, ellas y todo el paí.** Todo ello** **pese a que Juan Manuel Santos ha prometido al principio de su gobierno que no permitiría construcciones de represas en el Río Magdalena.

Según denuncia Robinson Rojas, campesino e integrante del **Comité en Defensa del Río Magdalena**, **Corpomagdalena ha perdido la autonomía** en las decisiones sobre el río y ahora solo está encargada la ANLA que dá vía libre a estas obras. Para Rojas, se trata de “una organización hecha por los altos mandos del gobierno que orotgan** el permiso de construcción a toda costa**”, por lo que no se estaría teniendo en cuenta la visión de las comunidad que conoce los daños a la fauna y flora sobre los que se incurriría.

El estudio de Hydrochina menciona la posibilidad de construcción de los proyectos Guarapas y Chillurco (en Pitalito), Oporapa (en Oporapa), Pericongo (en Timaná), El Manso (norte del Huila), Veraguas (en Aipe), Bateas (en Villavieja), Basilias (en Natagaima), Carrasposo, Nariño (en Girardot), Lame (en Ambalema), Cambao (en Cambao), y Piedras Negras (en Honda). [(Leer nota relacionada) ](https://archivo.contagioradio.com/plan-maestro-de-aprovechamiento-del-rio-magdalena-cuando-se-hizo-y-por-quienes/)

Rojas, vive en Oporapa, Huila y allí por cuenta de la realización de la represa, se inundaría 9.300 hectáreas, sin tener en cuenta que ese terreno es propenso a deslizamientos, por lo que sería un peligro para la población. Así mismo, podrían aumentarán los sismos, se dañarán los cultivos, se  desalojará a las familias comprándoles sus predios a bajos costos o sino serán desplazados a la fuerza, (como ya sucedió en el Quimbo), y además, “no podremos volver a entrar al río porque va a ser privado”, asegura el posible afectado, quien añade que es muy probable que **esa energía ni siquiera sea para el país, sino para venderla a otras naciones.**

Para el campesino Robinson, este tipo de mega-proyectos solo significan la privatización del río Magdalena, para que finalmente, las multinacionales puedan extraer los recursos naturales de las comunidades, por eso es que **EMGESA ya está comprando tierras en partes altas de la región donde nacen los acueductos** construidos por la misma comunidad, advierte Rojas.
