Title: Proyecto de seguridad privada revive las 'convivir': Iván Cepeda
Date: 2015-03-26 21:28
Author: CtgAdm
Category: DDHH, Nacional
Tags: Alvaro Uribe, Convivir, Iván Cepeda, Paramilitarismo
Slug: proyecto-de-seguridad-privada-revive-las-convivir-ivan-cepeda
Status: published

###### Foto:coyuntura.blogspot.com 

**Iván Cepeda**, senador por el **Polo Democrático**, afirmó que el nuevo proyecto de ley que **regularía el sector de la seguridad privada** en el país es inconstitucional, ya que mediante la figura que establece sobre cooperativas de seguridad, podría **revivir el paramilitarismo** en Colombia.

En el capítulo IV del proyecto de ley, se permite la creación de cooperativas de vigilancia y seguridad privadas, estas podrían estar formadas por ciudadanos y se regularían mediante la normativa de las empresas de seguridad y vigilancia privadas.

También se la obligatoriedad de la colaboración de las cooperativas de seguridad con la fuerza pública.

Según el senador “…Es contraproducente que en el contexto de un acuerdo de paz y ante posibilidad de un posconflicto, sectores del Congreso presenten una iniciativa que revive ese **viejo peligro de paramilitarizar el país a** través de entregarles a **cooperativas de seguridad el manejo de asuntos de orden público.** La tenebrosa experiencia de las Convivir nos enseñó que no se puede seguir en la línea de otorgarles facultades del Estado a los privados…”.

Además en el artículo 2 del proyecto permitiría a las empresas de seguridad privadas **realizar investigaciones sin tener que rendir cuentas sobre los objetivos de la misma**. De tal forma podrían estar **violando la intimidad** de las personas o otros derechos humanos sin tener que justificar la investigación.

En el artículo 3  determina que las **empresas** pueden realizar **acciones defensivas contra cualquier acto delictivo** a través de sus empresas de seguridad privada, función que corresponde a la fuerza pública.

Para el senador del Polo, este proyecto de ley constituye una forma de volver a la antigua forma de seguridad privada conocida como las **"Convivir", que propició el paramilitarismo** y se saldó con la vida de cientos de personas.
