Title: Con huelga de hambre internos de Cárcel Palogordo reclaman su derecho a la salud
Date: 2019-05-08 12:29
Author: CtgAdm
Category: Expreso Libertad
Tags: crisis carcelaria, Huelga, Palogordo
Slug: huelga-hambre-internos-carcel-palogordo-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Palogordo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Brigada Eduardo Umaña 

Una semana completaron en **huelga de hambre los internos de la Cárcel Palogordo de Girón Santander, debido a las difíciles condiciones de salud que afrontan 27 de sus compañeros**. De acuerdo a la denuncia más reciente, dos prisioneros, **Eliecer Yaguara y Ángel Alfonso Briñas**, padecen terijios que al no ser atendidos cada vez les ha provocado una mayor disminución de la vista.

Sumado a esto, el abogado Uldarico Flórez, presidente del a Brigada Eduardo Umaña Mendoza, señaló en Expreso Libertad que los 27 internos **ya interpusieron tutelas en defensa de su derecho a la salud**, q**ue han sido falladas a su favor**, sin que haya ninguna medida por parte del INPEC y el centro carcelario para garantizar los tratamientos adecuados que necesitan. Los internos de está cárcel están exigiendo tratamientos inmediatos para los 27 prisioneros afectados y **que no se les condene a la muerte por falta de atención**.

<iframe id="audio_35561931" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35561931_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 

 
