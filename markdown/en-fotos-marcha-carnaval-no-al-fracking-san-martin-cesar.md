Title: En Fotos: Marcha Carnaval NO al Fracking, San Martín, Cesar
Date: 2016-09-27 12:54
Category: Galerias, Nacional
Tags: Ambiente, fracking
Slug: en-fotos-marcha-carnaval-no-al-fracking-san-martin-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/No-al-Fracking1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Fotos: Gert Steenssens / EsperanzaProxima.net** 

###### 27 Sep 2016 

Más de 7000 personas marcharon por las calles de San Martín, Cesar, para decir “no al fracking”. En la movilización participaron delegaciones de Huila, Meta, Santander. Caqueta y Catatumbo y algunos invitados internacionales que estuvieron presentes en el foro ‘**El Fracking en las Américas y Colombia: Impactos, luchas y resistencias**" realizado en este municipio de Colombia y que se da en el cierre de la II Jornada Nacional frente al Fracking

<p>
   
[![Marcha Carnaval NO al Fracking, San Martín, Cesar](https://c5.staticflickr.com/9/8261/29837804372_26fe53eea3_z.jpg){width="640" height="427"}](https://www.flickr.com/photos/esperanzaproxima/albums/72157671047205924 "Marcha Carnaval NO al Fracking, San Martín, Cesar")  

<script src="//embedr.flickr.com/assets/client-code.js" async charset="utf-8"></script>
</p>

