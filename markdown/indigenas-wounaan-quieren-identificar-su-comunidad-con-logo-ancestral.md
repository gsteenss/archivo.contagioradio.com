Title: Indígenas Wounaan quieren identificar su comunidad con logo ancestral
Date: 2016-08-12 13:03
Category: Comunidad
Tags: Comunidad, Cultura, indígenas
Slug: indigenas-wounaan-quieren-identificar-su-comunidad-con-logo-ancestral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/pichima.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Bernardino Dura] 

[La champita (kugwiu), sería el logo ancestral con la que identificaría las noticias o reportajes de la comunidad **Wounaan** .]

[Para los integrantes de Pichima del municipio de San Juan de Litoral, departamento del Chocó y Valle del Cauca, es importante resaltar su primer medio de comunicación empírico, ancestral, frente a los nuevos canales y medios tecnológicos, según Bernardino Dura de la comunidad, el logo estaría listo después de unos ajustes y se haría el lanzamiento a través en redes sociales.]

[Guimer Quiro de la red CONPAZ,  comunidad de Santa Rosa de Guayacán,  Valle del Cauca, afirmó estar de acuerdo con la iniciativa para visibilizar otras formas de comunicar desde su territorio, igualmente Saúl Chamarra, comunicador de la red CONPAZ y gestor del kiosco Vive Digital del resguardo humanitario biodiverso del municipio de Buenaventura, quiere visibilizar el arte del pueblo **Wounaan** a través de los medios digitales. ]

[A través de este logo inspirado en la formas ancestrales de comunicarse, la comunidad Wounaan quiere romper barreras  y llegar a todo el país y  al mundo a través de medios modernos y tecnológicos de comunicación actual identificados con su marca.]

**[![Víctor Hugo Baltazar](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Víctor-Hugo-Baltazar--200x150.jpg){.wp-image-27797 .size-post-thumbnail .alignleft width="200" height="150"}](https://archivo.contagioradio.com/indigenas-wounaan-quieren-identificar-su-comunidad-con-logo-ancestral/victor-hugo-baltazar/)Víctor Hugo Baltazar Conda - Comunicador de la red CONPAZ - Putumayo pueblo Nasa**

###### *Como parte del trabajo de formación de Contagio Radio con comunidades afros, indígenas, mestizas y campesinas en toda Colombia, un grupo de comunicadores realizó una serie de trabajos periodísticos recogiendo las apuestas de construcción de paz desde las comunidades y los territorios. Nos alegra compartirlas con nuestros y nuestras lectoras.* 
