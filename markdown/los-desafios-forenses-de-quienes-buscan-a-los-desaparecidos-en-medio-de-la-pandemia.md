Title: Los desafíos forenses de quienes buscan a los desaparecidos en medio de la pandemia
Date: 2020-12-04 11:11
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Cementerios, Ciencias Forenses, Desaparecidos en Colombia, Desaparición forzada
Slug: los-desafios-forenses-de-quienes-buscan-a-los-desaparecidos-en-medio-de-la-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Forense.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

*Foto: Equitas*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Encuentros como el reciente **Forensic Tech 2020,** que reunió a diferentes profesionales forenses de varias naciones, **han abierto la discusión sobre la investigación de derechos humanos, la búsqueda de personas dadas por desaparecidas y los retos** previos y posteriores a la pandemia que permitirían dimensionar los alcances y limitaciones y posibilidades reales de esta labor y su abordaje en Colombia y Latinoamérica.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Muertes producto del conflicto armado podrían hacerse pasar por muertes de Covid-19

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Ana Carolina Guatame, coordinadora de investigación científica del Equipo Colombiano Interdisciplinario de Trabajo Forense y Asistencia Psicosocial (EQUITAS)** señala que el manejo de cadáveres en el marco de la pandemia permitió identificar falencias estructurales y tensiones en dicho ámbito no solo en Colombia sino en diferentes países de América Latina.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Una situación crítica está asociada al mal manejo de los cuerpos que murieron producto de la pandemia y que sumados a los casos de víctimas del conflicto y de desaparición forzada, **podrían poner en riesgo su identificación, esto en medio de un contexto en el que los casos han subido a los 34.754 fallecimientos reportados lo que lleva a Colombia** a ocupar el lugar número 10 de la tasa de contagios y el lugar 12 en la tasa de mortandad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, **Ginna Camacho Cortés, coordinadora de Asesoría Técnico Científica de [Equitas](https://www.equitas.org.co/)** señala que el más reciente informe de Equitas sobre la administración de muerte en los casos de contagio, versus las personas no identificadas con corte al 15 de noviembre, **les han llevado a centrar su atención de un total de 426 cementerios del país donde se presumen que existe casi 30.000 personas entre no identificadas y no reclamadas** en los cementerios de los departamentos de Norte de Santander, Huila, Tolima, Meta, Antioquia y Atlántico; donde persisten problemas de infraestructura, censo y elementos que impiden hacer una documentación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En ese sentido, llamó la atención sobre la responsabilidad del Estado en la disponilbidad de espacios y la gestión que las Alcaldías responsables de dichos cementerios deberían tener en épocas de pandemia, incluyendo el aporte de recursos y equipos médicos y de bioseguridad necesarios para sepultureros, trabajadores de funerarias y forenses del Estado. [(Lea también: Manejo erróneo de cementerios por Covid-19 pondría en riesgo memoria histórica del conflicto)](https://archivo.contagioradio.com/manejo-erroneo-de-cementerios-por-covid-19-pondria-en-riesgo-memoria-historica-del-conflicto/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega también que los retos más grandes persisten en materia de identificación algo en lo que los expertos **han insistido desde 2005 cuando surgió la ley de Justicia y Paz que permitió la recuperación de casi 9.000 cuerpos de los cuales 15 años después, más del 50% permanecen sin identificar,** por lo que llama la atención, deben ser la Defensoría del Pueblo y la Procuraduría quienes estén alertas frente a los registros de muertes que pueden no corresponde a Covid-19 sino a víctimas del conflicto que aún se vive en los territorios.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Búsquedas en contextos acuáticos demandan mayor complejidad

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según investigaciones como el proyecto **"Ríos de vida y muerte", una investigación conjunta de Rutas del Conflicto y de Consejo de Redacción** señalan que la práctica de desaparecer cuerpos arrojándolos a los ríos de Colombia perpetrada por grupos armados legales e ilegales **podría tener más de 60 años, lo que implica un verdadero reto para los equipos forenses.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por ejemplo, según datos del Centro Nacional de Memoria Histórica, **grupos paramilitares serían responsables del 68% de los casos de desaparición forzada en los afluentes colombianos**, mientras que los grupos guerrilleros son presuntos responsables del 8%, de igual forma la Fuerza Publica no solo tendría a algunos de sus integrantes vinculados con el incremento de estos métodos de esconder la verdad en el agua, sino que habría acudido a los mismos paramilitares para efectuar dichas tarea.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, **Ricardo Borrero, arqueólogo y candidato a PHD del programa de Arqueología Náutica de la Universidad de Texas A&M,** explica que existen una serie de tecnologías que pueden contribuir al hallazgo de desaparecidos en contextos subacuáticos, algunas con acústica y otras de forma electromagnética sin embargo **las expectativas que se generen "*deben ser limitadas pues a pesar del uso de estas tecnologías, son altas las probabilidades de que el deterioro de los cuerpos impida hacer estos hallazgos*** *en territorios donde muchas veces son remotos o de difícil acceso".*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En términos más técnicos, el experto en búsqueda acuática, señala que se puede aprender mucho de los estudios tafonómicos, es decir los relacionados a las transformaciones que experimentan los cuerpos desde el momento que son depositados en contextos terrestres o acuáticos hasta el momento en que se realiza el hallazgo por parte del equipo forense.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Señala que el proceso de degradación será diferente según el contexto y puede variar según el tipo de fondo (arena, limo, arcilla) la profundidad en la que se encuentren los cuerpos e incluso otros factores como el rol de la flora y la fauna que también pueden intervienen en los procesos de descomposición al igual que los flujos de corrientes.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, **Ginna Camacho,** explica que si bien los cuerpos se pueden recuperar en dichos contextos acuáticos, también **existen otras probabilidades y no necesariamente el hecho que una persona haya sido arrojada al río significa que se encuentre aún en el agua,** y sus cuerpos pueden ser halladas en las riveras e incluso hallados en las orillas y llevados a los cementerios

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En el caso de las medidas cautelares que han solicitado organizaciones como el MOVICE en lugares como Ituango, Antioquia; donde si bien pueden haber cuerpos en el agua la coordinadora señala que también existe la posibilidad de que se encuentren en lugares en los que pescadores o barequeros hallaron los cuerpos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto agregan que es necesario dar un debate con relación a la protección del agua y los conflictos que surgen alrededor de ella, y otras nociones como la defensa del territorio o actividades extractivas y del cómo, cuando ocurren estos conflictos sociales así mismo se producen desapariciones, homicidios de líderes o desplazamientos forzados.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Familiares y forenses trabajan en medio de la guerra

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Ana Guatame señala que a diferencia de otros países en donde lo forense se realiza en el marco de la criminalidad común o después de un conflicto, **en Colombia se debe trabajar en medio de una guerra que está viva incluso tras la firma del Acuerdo de Paz y eso implica condiciones diferentes de seguridad tanto para forenses como para comunidades.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

*"Lo más importante es que se está tratando con humanos y en el marco del conflicto, con víctimas y sus familias es a lo que se debe dar prelación, no es lo mismo buscar un barco que tiene 150 años de hundido que enfrentarse a personas que perdieron a sus familiares recientemente, son situaciones que le agreguen sensibilidad al trabajo"*, agrega Ricardo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Coinciden en que el contexto latinoamericano ha llevado a que la desaparición forzada de personas sea recurrente en la región, lo que ha permitido que cada país desarrolle acciones forenses distintas para responder a esa situación y pese a ser realidades diferentes, todas comparten un desarrollo normativo en políticas públicas para la investigación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Es precisamente, el trabajo de familiares y organizaciones sociales el que ha llevado a que el Estado se mueva en la línea de búsqueda, no tanto así en la investigación, pues, señala Ginna Camacho que **en los países latinoamericanos existe una impunidad del 99% en la sanción de los responsables de desaparición forzada.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Para conocer más de este tema le invitamos a ver, **Otra mirada: Ejercicio forense en Colombia y sus aportes a los DD.HH.***

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://youtu.be/D0HkHaNHsxI","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://youtu.be/D0HkHaNHsxI

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:block {"ref":78955} /-->
