Title: Es hora de hablar en serio de agua y petróleo
Date: 2015-07-09 12:44
Category: CENSAT, Opinion
Tags: Barrancabermeja, Catatumbo, Censat agua Viva, contaminación de ríos, daño ambiental, petroleo, Putumayo, Tumaco
Slug: es-hora-de-hablar-en-serio-de-agua-y-petroleo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/petroleo-putumayo-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

#### [**Por [CENSAT](https://archivo.contagioradio.com/censat-agua-viva/) - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva) 

###### [9 Jul 2015] 

El pasado 21 de junio se produjo un derrame petrolero provocado por las Fuerzas Armadas Revolucionarias de Colombia (FARC). Más de 5 mil barriles de petróleo contaminaron ríos y manglares de Tumaco, los vertimientos alcanzaron la Bahía. Este hecho, como otros similares ocurridos en Putumayo y en el Catatumbo, han sido condenados masivamente. En las redes sociales el tema se volvió viral. Es entendible. La contaminación petrolera es grave, los vertimientos de hidrocarburos afectan las relaciones ecológicas y los ciclos hídricos y biogeoquímicos. En el agua, pequeñas concentraciones retardan la división celular y el crecimiento del plantón, en altas concentraciones produce la muerte de cantidad de organismos y especies fitoplantónicas, así como larvas y huevecillos de peces. Al depositarse sobre las branquias, impide el intercambio de oxígeno y estos mueren por asfixia. El crudo puede permanecer muchos años en los sedimentos afectando microorganismos y formas de vida, así que se necesitan varias décadas para que los ecosistemas se recuperen. Esto afecta a largo plazo los medios de vida de la población local.

Sin embargo, estos hechos condenables desde cualquier punto de vista, no son el principal problema ambiental en las regiones. Precisamente, lo que se ha ocultado es aún más grave, pues los derrames provenientes de las actividades petroleras en Tumaco y en otras regiones son frecuentes. En Nariño, la gente aún recuerda lo sucedido hace 19 años, el 26 de febrero de 1996, cuando en el puerto petrolero, específicamente en la Playa de Salahonda, se vertieron 15 mil barriles de crudo en el momento en que se cargaba el buque tanque Daedalus, de bandera griega. El crudo cubrió las arenas de la playa, entró a los manglares y se precipitó al fondo marino. Durante la investigación para dimensionar los daños, la Armada Nacional advirtió que la contaminación provocó la destrucción casi total del plantón y la muerte de los peces muestreados. El derrame afectó unas 150 familias de pescadores, a concheras, almejeras y chiquiteros, es decir, a aproximadamente 1.500 personas cuya vida depende de los bienes comunes marinos.

Tumaco enfrentó un accidente similar dos años más tarde, en el mes de julio, mientras otro vertimiento proveniente de Ecuador también afectó sus playas. En esa ocasión, se derramaron 18 mil barriles de crudo al Oceano Pacífico, tras un daño en el oleoducto Transecuatoriano, en el sector Santo Domingo-Esmeraldas.

Años después la marea negra se volvió a sufrir: el petróleo derramado se recogió en bultos y se enterró en las costas de las veredas Milagros, Terán, Bocana las Mercedes, Bocagrande y El Rompido, ubicadas entre Tumaco y Francisco Pizarro, pero las corrientes marinas y la inestabilidad de los suelos hicieron que los sacos salieran a flote, se abrieran y quedara al descubierto el petróleo mezclado con arena.

Hoy los derrames se han vuelto cada vez más frecuentes en el litoral Pacífico. Grupos criminales instalan llaves artesanales en el poliducto andino para robar gasolina, de manera que de cada 10 derrames, nueve los provoca el saqueo al oleoducto. Los demás ocurren por el estado de las tuberías, que están muy deterioradas y se rompen, o por atentados al oleoducto conducidos por las organizaciones insurgentes.

Estos hechos que suceden en Tumaco son también frecuentes en otras regiones del país. El debate abierto con lo sucedido más recientemente es una oportunidad para hablar, ahora sí en serio, acerca de las afectaciones al agua y al medio provocadas por la extracción de petróleo, y de la crítica situación de varias regiones de tradición petrolera: Yopal, Acacías, Puerto Gaitán, Barrancabermeja, Tibú… Más ahora que la nueva frontera extractiva se abre sobre los mares, en la cordillera, la extracción de crudos ultrapesados en la Amazonia y que se expande otra brecha inmensa con la introducción de nuevas tecnologías, para la extracción de crudos ultrapesados o como el *fracking,* que demandan mucha más agua y son más contaminantes.
