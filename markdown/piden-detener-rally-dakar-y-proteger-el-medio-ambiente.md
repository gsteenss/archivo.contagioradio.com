Title: Piden detener el Rally Dakar para proteger a animales y el ambiente
Date: 2018-01-15 13:22
Category: Ambiente, El mundo
Tags: medio ambiente, protección del medio ambiente, Rally Dakar, sudamérica
Slug: piden-detener-rally-dakar-y-proteger-el-medio-ambiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/rally-dakar-e1516039023256.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Petición "No queremos más Dakar"] 

###### [15 Ene 2018] 

Personas y diferentes organizaciones sociales han escrito una petición dirigida a los gobiernos de Argentina, Chile y Bolivia para que se detenga el Rally Dakar que se lleva a cabo entre el 6 y el 20 de enero por **décima vez en América del Sur**. La petición la hacen teniendo en cuenta los daños ambientales, la desprotección de las comunidades originarias y en especial por el maltrato animal.

Desde hace varios años organizaciones enfocadas hacia la conservación han manifestado que esta carrera, que agrupa a **142 motociclistas**, 50 conductores de cuadriciclos, 104 automóviles y 44 camiones, atenta contra el medio ambiente en la medida en que exalta un modelo de movilidad insostenible.

De acuerdo con el movimiento Ecologistas en Acción de España, el Rally Dakar **“produce un impacto ambiental severo que es indirecto e indirecto”.** Esto pues genera contaminación atmosférica y acústica además de la erosión del suelo por donde pasan los vehículos a gran velocidad”. Expresan que “cada mil km recorridos por uno de los vehículos suponen la erosión de una hectárea de suelo”. (Le puede interesar: ["Películas animadas sobre la conservación del medio ambiente"](https://archivo.contagioradio.com/peliculas-animacion-mensaje-ambiente/))

Adicionalmente, “promocionar este tipo de vehículos vinculando su imagen con la “aventura” y el “descubrimiento” de la naturaleza es de una **gran irresponsabilidad** y supone un desprecio hacia los principales problemas en los que se encuentra inmersa la humanidad, entre los que destaca el cambio climático y la escasez de combustibles fósiles”.

### **Animales y comunidades son quienes más sufren el impacto negativo del Rally Dakar** 

La petición que han realizado para este año se llama **“No queremos más Dakar, No queremos más asesinatos de animales!”** y se hizo por medio de la plataforma SOSVOX. Allí, indican que desde los siete años que se ha venido realizando el Rally en Sudamérica, se han registrado atropellos contra diferentes especies de animales y se ha desprotegido a las comunidades indígenas que viven en las zonas por donde pasan los vehículos. (Le puede interesar: ["La defensa del ambiente y los derechos humanos en Colombia"](https://archivo.contagioradio.com/la-defensa-del-ambiente-y-los-derechos-humanos-en-colombia/))

Ponen el ejemplo de 2016 donde en Bolivia fueron **envenenados cientos de perros** antes de iniciar la competencia “para que no molestaran a los organizadores, corredores y al público que iba a asistir al evento”. Por hechos como este es que han pedido a las personas que firmen la petición y así luchar contra el maltrato animal, la protección del medio ambiente y la protección de los pueblos indígenas.

Puede encontrar y firmar la petición acá: [http://bit.ly/2B1N8Qz](https://www.sosvox.org/es/petition/no-queremos-mas-dakar-no-queremos-mas-asesinatos-de-animales.html?utm_source=sharing&utm_medium=twitter&utm_campaign=signature.thanks)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

###### 

<div class="osd-sms-wrapper">

</div>
