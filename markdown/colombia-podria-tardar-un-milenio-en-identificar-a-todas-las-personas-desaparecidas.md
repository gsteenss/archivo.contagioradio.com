Title: Colombia podría tardar un milenio en identificar a todas las personas desaparecidas
Date: 2018-03-06 16:24
Category: DDHH, Entrevistas
Tags: desaparecidos, Luz Maarina Monzón, víctimas de crímenes de Estado
Slug: colombia-podria-tardar-un-milenio-en-identificar-a-todas-las-personas-desaparecidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/victimas-movice-en-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOVICE] 

###### [06 Mar 2018] 

Un conjunto de organizaciones sociales defensoras de derechos humanos en compañía del senador Iván Cepeda y el representante a la Cámara por Bogotá Alirio Uribe hicieron entrega a Luz Marina Monzón, directora de la Unidad de Búsqueda de Personas Desaparecidas, un informe que devela la difícil y urgente tarea por priorizar la búsqueda e identificación de las personas desaparecidas **en Colombia que tardaría al menos un milenio**.

Este informe llamado “Resultados de la implementación de las medidas inmediatas humanitarias y la situación actual de los cementerios municipales”, para César Santoyo, integrante del Colectivo Orlando Fals Borda, evidencia que por lo menos para lograr identificar a tan solo los cuerpos exhumados en **5 cementerios, se podría calcular un periodo de 140 años, lo que significa que identificar y encontrar a todos los desaparecidos de Colombia podría durar un milenio**.

De acuerdo con el informe, La Fiscalía General de la Nación reportó **2.292 personas inhumadas como no identificadas solamente en 5 cementerios** de los llanos orientales. De estos, entre 2010 y 2015, tanto el ente investigador como el Instituto Nacional de Medicina Legal y Ciencias Forenses, “entregaron 77 cuerpos de los cementerios de los llanos orientales” por lo que “en promedio se restituyeron 15 cuerpos por año, lo cual indica que, a este ritmo, hacen falta 144 años para entregar dignamente el total de los cuerpos inhumados tan sólo en estos cinco cementerios”.

### **"Es urgente acelerar el proceso de entrega digna de los cuerpos inhumados"** 

Con esto en mente, el informe establece que hay una necesidad “urgente” de acelerar el proceso de restitución y entrega digna de los cuerpos inhumados en cementerios municipales “dotándolo de recursos humanos, técnicos, financieros y, ante todo, de voluntad política por parte del gobierno a través las entidades competentes, no sólo para los Llanos Orientales, sino involucrando el resto del país”.

Por esto, es necesario que se adopten medidas de carácter humanitario teniendo en cuenta que en 2015 se publicó el comunicado conjunto no. 62 entre las partes de la mesa de conversaciones de la Habana. Allí, se acordó **“poner en marcha unas primeras medidas inmediatas humanitarias de búsqueda,** ubicación, identificación y entrega digna de restos de personas dadas por desaparecidas”. (Le puede interesar:["la verdad es el único camino a la reconciliación": Víctimas de Crímenes de Estado"](https://archivo.contagioradio.com/la-verdad-es-el-unico-camino-a-la-reconciliacion-victimas-de-crimenes-de-estado/))

### **Las cifras de los desaparecidos** 

Las preocupaciones radican en que, por ejemplo, hay una falta de coordinación entre las entidades encargadas de este proceso como lo son la Fiscalía, Medicina Legal y el Ministerio del Interior. Indica el informe que hay una “duplicidad de esfuerzos de las entidades, las cuales realizaron acciones similares generando desgaste en los equipos técnicos e insumos necesarios, sin una coordinación de planes y una distribución adecuada de la experticia de cada entidad”.

En lo que tiene que ver con la evaluación de las actividades de la Fiscalía General de la Nación y su Grupo de Búsqueda, Identificación y entrega de personas Desaparecidas, “Se han exhumado a la fecha **8.969 cuerpos tanto de sitios de disposición en campo abierto como de cementerios**”. Estos cuerpos se encuentran en etapa de documentación, exhumación y localización de los familiares.

Por su parte, Medicina Legal ha exhumado 461 cuerpos logrando identificar sólo 29. En coordinación con el Comité Internacional de la Cruz Roja lograron realizar el “abordaje forense integral de 22 cuerpos, 11 de ellos en la ciudad de Medellín, 4 en Bucaramanga, 3 en Florencia, 3 en Tumaco y 1 en Buenaventura. De **los 22 cuerpos, 11 ya fueron entregados a los familiares, 5 están identificados y en proceso de entrega y los 6 restantes están en proceso de identificación**”.

Ahora, el Ministerio del Interior, hizo una inversión de 9 mil millones de pesos para hacer un diagnóstico en 426 cementerios donde identificó **26.395 cuerpos inhumados sin identificar**. Sin embargo, el informe indica que el Ministerio reportó que los datos de personas inhumadas como no identificadas “están incompletos y las entidades competentes manejan datos parciales”.

### **Conclusiones del informe** 

Habiendo caracterizado los datos propuestos por las tres entidades, el informe concluyó que “cada entidad reportó indicadores y resultados diferentes frente a la implementación de las medidas inmediatas humanitarias, aun cuando estas se enmarcaban dentro de un plan de intervención interinstitucional, tal como lo mandataba el Comunicado No 62”. Además, afirma que “los escasos resultados reportados de personas identificadas y entregadas (..) evidencia la falta de una adecuada planeación e investigación”.

En esa vía el informe recomendó la necesidad de generar una matriz de coordinación entre todas las entidades competentes del nivel nacional para que en conjunto y bajo el liderazgo de Luz Marina Monzón, se puedan establecer mecanismos y planes de búsqueda contundentes que garanticen la dignidad de la entrega de los desaparecidos.

Esta coordinación debería generarse en el corto plazo junto con **políticas públicas que propendan por acabar el paramilitarismo y los crímenes de Estado en Colombia**. De igual forma, el documento asegura que debe mantenerse la entrega digna de los cuerpos de los familiares, lo que significa que tiene que haber una plena identificación de las personas para poderles dar un nombre y posteriormente ser entregadas a las familias.

<iframe id="audio_24289518" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24289518_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
