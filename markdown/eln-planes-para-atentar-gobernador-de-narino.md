Title: ELN desmiente planes de atentar en contra de Gobernador de Nariño
Date: 2019-03-10 19:03
Category: DDHH, Paz
Tags: acuerdos de paz, Ejercito de Liberación Nacional, Gobierno
Slug: eln-planes-para-atentar-gobernador-de-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/maxresdefault-1-e1454356698981.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [10 Mar 2019] 

En un comunicado, la Dirección Nacional del Ejército de Liberación Nacional (ELN) rechazó reportes mediáticos difundidos el pasado 4 de marzo que señalan al grupo armado de planear **el asesinato de Camilo Romero, el Gobernador de Nariño**, indicando que esta acusaciones constituyen en "calumnia".

"Es falso que el ELN tenga planificado o haya pensado atentar contra la vida del Gobernador de Nariño, tal como lo difundieron algunos medios de comunicación el pasado 4 de Marzo. Desmentimos públicamente esta falsa noticia, al igual que otras similares que recientemente circulan por la prensa, porque no es una práctica del ELN proferir este tipo de amenazas", afirma la comunicación. (Le puede interesar: "[Comunidades le piden a Duque retomar diálogos con ELN y llevar paz a los territorios](https://archivo.contagioradio.com/comunidades-le-piden-a-duque/)")

Los dirigentes también negaron otros reportes del pasado 5 de marzo que indican que **el ELN prepara atentados en Medellín y Cali**, presuntamente basados en fuentes de inteligencia de la Fiscalia. El ELN sostiene que **las agencias de inteligencia fabrican y difunden deliberadamente estas noticias** para "generar una imagen distorsionada y negativa sobre nuestra organización, nuestros ideales y la lucha revolucionaria".

### **Las negociaciones entre el ELN y el Gobierno** 

Actualmente, las diálogos de paz entre el ELN y el Gobierno colombiano siguen suspendidos y para el grupo armado, estas supuestas acciones del Gobierno del Presidente Iván Duque solo indican un desinterés en buscar una salida política al conflicto entre las dos partes. Sin embargo, el grupo armado le pide al Gobierno colombiano restablecer la mesa de negociaciones para encontrar las "salidas de paz". (Le puede interesar: "[Colombia no pude omitir el DIH ni violar los protocolos de la mesa con ELN](https://archivo.contagioradio.com/colombia-no-puede-omitir-el-d-i-h-ni-violar-los-protocolos-de-la-mesa-con-eln/)")

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
