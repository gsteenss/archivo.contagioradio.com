Title: Campesino herido en protestas contra erradicación forzada en el Meta
Date: 2017-07-17 17:43
Category: DDHH, Movilización, Nacional
Tags: Erradicación Forzada, Plan de Sustitución de Cultivos de Uso ilícito, Policía Antinarcóticos, puerto rico
Slug: protestas-erradicacion-forzada-puerto-rico-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/erradicacion-forzada-puerto-rico-meta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: prensarural] 

###### [17 Jul 2017]

Según información de las comunidades del municipio de Puerto Rico en el Meta, desde el pasado 15 de Julio se están realizando operaciones de **erradicación forzada** de cultivos de uso ilícito por parte de la Policía Antinarcóticos, a **pesar del acuerdo de los habitantes con el gobierno nacional**. La arremetida de la policía dejan un saldo de un campesino herido con disparos de fusil y dos campesinos de los que se desconoce su paradero.

Según la información de las organizaciones de DDHH presentes en la zona, el campesino herido acudía a una cita al caserío de la vereda “La Tigra”, invitación que habría sido realizada por los integrantes de policía presentes en las erradicaciones. Aunque no se precisa como se produjeron los hechos , el campesino **Arnulfo Sánchez tuvo que ser trasladado de emergencia al hospital de San José del Guaviare** con un impacto de fusil en su cuerpo. [Lea también: Plan de sustitución de cultivos de uso ilícito debe ser integral o no durará](https://archivo.contagioradio.com/sustitucion-cultivos-ilicitos-cocam/).

### **Policía no estaría respetando los acuerdos de sustitución a los que llegan campesinos y gobierno** 

Desde el mismo día de ingreso de los efectivos policiales más de 350 campesinos han rechazado la erradicación forzada con dos argumentos principales. Uno, **que ya hay un compromiso de sustitución entre la comunidad y el gobierno**, y dos, que hasta tanto no se implementen los planes de sustitución **no se pueden dejar sin sustento a las familias cocaleras** de la región, pues no tienen una fuente de ingresos diferente a la que han tenido por muchos años.

Según las organizaciones de DDHH, como la Corporación DHOC, que hace presencia en el sector, los campesinos informaron a las autoridades de policía sobre los acuerdos firmados que tienen como respaldo el decreto 986 de 2017. Sin embargo, **haciendo caso omiso de dicho acuerdo, las fuerzas de policía han procedido con la erradicación forzada**.

Por estas razones, las organizaciones de DDHH han emitido una alerta temprana en la que llaman la atención sobre la violencia con que la policía ha respondido a las exigencias de la comunidad y también porque luego del traslado al hospital del campesino herido con disparos de fusil, se han presentado **hombres encapuchados que amenazan la seguridad de las personas que de manera solidaria hicieron el traslado y realizarán la denuncia.**

### **En Putumayo y otras regiones tampoco se están cumpliendo acuerdos del Plan de Sustitución** 

Recientemente las comunidades campesinas del municipio de Puerto Asis en Putumayo, reiniciaron una jornada de protesta pacífica ante los incumplimientos por parte del gobierno nacional en torno a la puesta en marcha de planes de sustitución de cultivos de uso ilícito y la permisividad con la expansión petrolera. [Lea también: Dijimos no a la erradicación pero si a la sustitución](https://archivo.contagioradio.com/dijimos-no-a-la-erradicacion-pero-si-a-la-sustitucion-campesinos-de-argelia-cauca/)

Según los habitantes de ese sector del departamento, hay una dilación injustificada en el cumplimiento de los acuerdos, tal como sucede en varios escenarios similares como en el departamento de Nariño, en los alrededores de Tumaco donde se han presentado protestas ante la presencia de los erradicadores. [Le puede interesar: Se dilata plan de sustitución en Putumayo](https://archivo.contagioradio.com/en-putumayo-el-estado-ha-dilatado-el-plan-de-sustitucion-de-cultivos-de-uso-ilicito/).

### **Un plan de sustitución muy bueno en el papel pero que no es realidad para muchos** 

Según el gobierno el Plan Nacional de Sustitución de Cultivos de Uso Ilítico contempla subsidios para los campesinos que decidan erradicar voluntariamente los cultivos de coca, además el acuerdo con las FARC incluye también la puesta en marcha de planes de infraestructura básica para las regiones, que facilite el transporte y la comercialización de los productos en la región.

La meta es sustituir unas 50.000 hectáreas durante el primer año en más de 40 municipios y contempla dar 1.800.000 pesos solo una vez para la implementación de proyectos autosostenibles y seguridad alimentaria y 9.000.000 pesos solo una vez para adecuación y ejecución de proyectos de corto plazo como piscicultura y avicultura.

[Alerta Temprana Puerto Rico Meta Julio de 2017](https://www.scribd.com/document/354014634/Alerta-Temprana-Puerto-Rico-Meta-Julio-de-2017#from_embed "View Alerta Temprana Puerto Rico Meta Julio de 2017 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_72818" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/354014634/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Ovi6R50wjrCOTYoI6gZc&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6069986541049798"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
