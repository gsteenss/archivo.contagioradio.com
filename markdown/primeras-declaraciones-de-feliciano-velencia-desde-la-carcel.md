Title: Primeras declaraciones de Feliciano Valencia desde la cárcel
Date: 2015-09-23 12:37
Category: DDHH, Nacional
Tags: Cauca, constitución de 1991, Feliciano Valencia, Minga Indígena 2008, Persecución política en Colombia, presos politicos, proceso de liberación de la Madre Tierra
Slug: primeras-declaraciones-de-feliciano-velencia-desde-la-carcel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Feliciano-Valencia-habló-desde-la-cárcel.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Juan Pablo Gutiérrez 

**“Están juzgando la misma Constitución Política, están encarcelando la protesta social y la jurisdicción indígena",** son algunas de las palabras que pronunció el líder indígena, Feliciano Valencia, en sus primeras declaraciones desde la cárcel de máxima seguridad San Isidro, en Popayán tras haber sido capturado (Ver: [Sectores sociales e indígenas rechazan detención del líder indígena Feliciano Valencia](https://archivo.contagioradio.com/?p=14101))

Valencia, asegura que el soldado que fue capturado por la guardia indígena durante la Minga de 2008 en La María Piendamó, nunca fue secuestrado, **“actuamos en tiempo, modo y lugar en el marco de la justicia indígena y se dio todas las garantías al soldado”,** además añadió que el nunca participó directamente de la detención, que se hizo a todas luces de los medios de comunicación, “no pudo haber secuestro porque nunca se hizo a escondidas”, expresó.

**"Al soldado se le permitió llamar a los familiares y se informó que estaba detenido por la guardia indígena”,** dijo el líder indígena.

Así mismo, reiteró que en la Constitución Política de 1991 los pueblos indígenas fueron incluidos, y con el artículo 246 se permitió que las comunidades indígenas ejerzan su propio marco de jurisdicción.

Finalmente, Feliciano Valencia denuncia que toda esta acción **se trata de una persecución política,** pero confía en que las acciones jurídicas que se han instaurado en su defensa para que se le permitan regresar a sus territorios y seguir en su ejercicio de construcción de paz.

https://youtu.be/nzZecKayR0s
