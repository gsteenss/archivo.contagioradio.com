Title: Fecode rechaza violencia del Esmad contra profesores en Nariño
Date: 2017-06-15 12:36
Category: Educación, Nacional
Tags: abusos Fuerza Pública, fecode, Movilizaciones, Paro de maestros
Slug: fecode-rechaza-violencia-del-esmad-contra-profesores-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/PROFESNARINO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tomada de Twitter] 

###### [15 Jun 2017 ]

La Federación Colombiana de Educadores FECODE, **denunció las represiones violentas que ha tenido el Esmad en contra de los maestros y maestras en Nariño**. En la vía que conduce al aeropuerto de Chachagüi a 35 km de la ciudad de San Juan de Pasto, los docentes fueron atacados con gases lacrimógenos y agredidos con balas de goma, actuaciones que pusieron en riesgo a niños y niñas que acompañaban la marcha pacíficamente.

Para Over Dorado, miembro de la junta directiva de FECODE, “**estas acciones de violencia hacen parte de una agitación del Estado para frenar el paro**”. Ante estos hechos, FECODE le ha exigido al Gobierno Nacional que respete a los educadores y educadoras del país quienes están ejerciendo su derecho constitucional a la protesta. (Le puede interesar: "[Esmad ataca el paro de maestros en Bogotá](https://archivo.contagioradio.com/esmad-ataca-con-agua-y-gases-lacrimogenos-el-paro-de-maestros-en-bogota/)")

En un comunicado de prensa, la organización gremial denunció también “**las amenazas de enviar al Esmad contra el Magisterio en la Guajira durante las movilizaciones en Uribia**”. De igual manera, la Federación rechazó “las amenazas contra la Junta Directiva de la Asociación Directiva de Educadores ADE a través de panfletos intimidatorios”.

**Estado debe cumplir su papal de garante del derecho a la educación**

Ante las declaraciones del secretario de movilidad de Bogotá, Juan Pablo Bocarejo, de querer demandar a los profesores ante la Fiscalía por generar bloqueos en las vías, Over Dorado cuestionó tales actuaciones. “El secretario de movilidad de la capital debe pensar si lo que estamos haciendo en las calles va en contravía del orden legal”. Dorado recordó que los **maestros y maestras se están movilizando en función de proteger un derecho fundamental** por medio de una vía legal como lo es la protesta.

En cuanto a las negociaciones con el Gobierno Nacional, Dorado aseguró que van por buen camino. Según él “hemos avanzado mucho en los temas económicos y **esperamos que entre hoy y mañana lleguemos a un acuerdo** en lo que tiene que ver con el Sistema General de Participaciones”. Los docentes continúan afirmando que el paro se levantará cuando el gobierno tenga la voluntad de negociar, de lo contrario continuará. (Le puede interesar: "[Hay propuestas pero el gobierno no tiene voluntad para negociar](https://archivo.contagioradio.com/maestros-tiene-propuestas-pero-el-gobierno-no-tiene-voluntad-para-negociar/)")

**Hoy continúan las movilizaciones**

Dorado afirmó que para hoy las **seccionales del Magisterio en el país continuarán con las movilizaciones** en espera de que las negociaciones entre las partes avancen. Para Dorado, “entre hoy jueves y mañana viernes tomaremos las decisiones pertinentes para establecer si el paro se mantiene o no”.

<iframe id="audio_19286971" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19286971_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
