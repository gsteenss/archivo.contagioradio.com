Title: Habitantes de Mocoa denuncian que ayudas no estarían llegando a personas fuera de los albergues
Date: 2017-04-08 09:03
Category: DDHH
Tags: Ayudas para Mocoa, Mocoa
Slug: habitantes-de-mocoa-denuncian-que-ayudas-no-estarian-llegando-a-personas-fuera-de-los-albergues
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/58e06df58eb26.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [08 Abr 2017]

A 8 días de la tragedia de Mocoa, los habitantes siguen denunciando la falta de acciones urgentes por parte del gobierno que les permita subsanar la situación en la que están, las personas señalan que **no se están repartiendo ayudas a las personas que no estén en los albergues y que los precios de los productos en las tiendas se han elevado, impidiendo el acceso a los mismos.**

El defensor de derechos humanos Carlos Fernández, integrante de la Comisión de Justicia y Paz, señala que hay una problemática con la forma en la que la Unidad de Riesgos está distribuyendo las diferentes ayudas que llegan a Mocoa solamente en los albergues, a las personas que ya se han censado, mientras que aproximadamente el **40% de la población que sobrevivió a este desastre están por fuera de estos lugares**, re agrupándose en sus comunidades.

Motivo por el cual no están recibiendo ningún tipo de ayuda por parte del Estado. De igual forma las personas que se encuentran en los albergues denuncian que están **militarizados, que los agentes no dejan entrar ni salir a nadie,** impidiendo que vayan al baño o se acerquen a las comidas comunitarias para recibir alimentos. Le puede interesar: ["Peligro de epidemias debe ser contrarrestadas con urgencia en Mocoa"](https://archivo.contagioradio.com/peligro-de-epidemias-deben-ser-contrarrestados-con-urgencia-en-mocoa/)

El presidente Santos anunció que esta medida tenía como fin, evitar que personas inescrupulosas se aprovecharan de la situación, sin embargo, las personas que se encuentran en estos lugares **señalan que la medida es excesiva que, en vez de contribuir, viola el derecho a la libre movilidad de las personas**. Le puede interesar: ["Demoras en entrega de cuerpos otra tragedia en Mocoa"](https://archivo.contagioradio.com/tragedia-mocoa-cuerpos-ayudas/)

De otro lado, los **habitantes de Mocoa también se están viendo expuestos al aumento de los precios de los alimentos, prodructos de aseo y medicamentos**, por parte de los comerciantes, de acuerdo con Carlos Fernández, un plato de comida que antes costaba entre \$5.000 a \$8.000 pesos ahora está costando entre \$13.000 a \$18.000 pesos.

Los últimos datos recolectados por la Unidad de Riesgo es que la cifra de muertos ya aumenta a **312 víctimas, de las cuales 102 son menores de edad,** de igual forma la Unidad señaló que hasta el día de hoy se harían búsquedas y se finalizaría con el censo de los habitantes de Mocoa.

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
