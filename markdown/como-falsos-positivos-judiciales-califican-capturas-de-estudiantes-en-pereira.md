Title: Como falsos positivos judiciales, califican capturas de estudiantes en Pereira
Date: 2019-10-17 16:59
Author: CtgAdm
Category: Entrevistas, Estudiantes
Tags: estudiantes, Pereira, policia, Universidad Tecnológica de Pereira
Slug: como-falsos-positivos-judiciales-califican-capturas-de-estudiantes-en-pereira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Universidad-Tecnológica-de-Pereira-e1571349344572.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Youtube UTP  
] 

El comité de derechos humanos de la Unión Nacional de Estudiantes de Educación Superior (UNEES) denució que en las últimas horas se presentaron allanamientos ilegales por parte de la SIJIN a casas de estudiantes que participaron en recientes movilizaciones en Pereira, Risaralda. Según voceros de esta organizacicón **estos hechos harían parte de la creación de falsos positivos judiciales en contra del movimiento estudiantil**, que den respuesta a los actos vandálicos contra la vivienda del Comandante de Policía del municipio.

### **Nicolás Velásquez y Jey Londoño, estudiantes víctimas de procedimientos irregulares** 

Según denunció la UNEES, el pasado miércoles 16 de octubre sobre las 4 de la mañana, agentes de la SIJIN llegaron a las viviendas de los estudiantes de la Universidad Tecnológica de Pereira (UTP) Jaime Andrés Hernández Henao, Jey Alejandro Londoño Arias y Nicolás Velásquez Escudero. Andrés Gómez, líder estudiantil de la Universidad, explicó que el allanamiento se realizó en las casas de Velásquez y Londoño, porque se encontraban en el lugar, y contra ellos se adelantaron procedimientos irregulares.

En el caso de Velásquez, estudiante de octavo semestre de Administración Ambiental, **los agentes llegaron a su casa en un taxi y tumbaron su puerta.** Estando en el lugar, recaudaron lo que consideraron 'material probatorio', tratándose de botas de campo, batas de laboratorio, un gorro tipo pasamontañas y una imagen de la marcha del 8 de octubre contra Álvaro Uribe Vélez. (Le puede interesar: ["Estudiantes de la Universidad Distrital tienen razones de sobra para protestar"](https://archivo.contagioradio.com/estudiantes-de-la-universidad-distrital-tienen-razones-de-sobra-para-protestar/))

El estudiante fue llevado a la sede metropolitana de la Policía de Pereira, y de acuerdo a Gómez, se le imputaron los delitos de terrorismo y rebelión. En el caso de Jay Arias, estudiante de licenciatura en Comunicación y Tecnología, su familia manifestó que cuando se realizó el operativo, los agentes sacaron a la familia de la vivienda y se quedarón solos haciendo el registro, posteriormente, **apareció una maleta que tenía botellas con gasolina, pese a que la familia dijo que nunca sintió el olor que emite este líquido**, y que tampoco la maleta pertenecía a las personas que habitan la casa.

### **"No tenemos elementos para creer que los compañeros estaban en la manifestación violenta"** 

Gómez afirmó que creía que en el marco de la protesta en que ocurrieron los hechos vandálicos contra la casa del Comandante hubo infiltrados, porque el desarrollo de los hechos no corresponde con la tradición con que ha actuado el movimiento estudiantil en Pereira. Asimismo, manifestó su preocupación porque han recibido información sobre la existencia de otras 100 órdenes de allanamiento contra estudiantes, y se preguntan si lo acusaran falsamente de ser parte de grupos al margen de la Ley, "cuando en la Universidad nunca se han presentado pintas, ni panfleto que indicara la presencia de estos grupos".

La preocupación de los estudiantes estaría relacionada con lo ocurrido en el caso de Velásquez y Londoño, que tras vivir procesos de allanamiento con irregularidades, fueron capturados y trasladados en carros particulares hasta lugares de detención transitoria. Aunque la Defensoría intervino, y los jóvenes fueron dejados en libertad, Gómez resaltó que en sus casos, **no había "elementos para creer que los compañeros estaban en la manifestación violenta".** (Le puede interesar: ["Mateo Gutierrez León sería víctima de otro falso positivo judicial"](https://archivo.contagioradio.com/mateo-gutierrez-leon-seria-victima-de-falso-positivo-judicial/))

### **En contexto: Los actos vandálicos en Pereira** 

El pasado martes 15 de octubre se presentó en la UTP una protesta por parte de encapuchados que prendieron fuego a una moto, pero ante la nula reacción de la Policía, decidieron avanzar más allá de la Universidad hasta una glorieta en la que de acuerdo a Goméz, hace tiempo se han presentado robos contra los estudiantes. Mientras se adelantaba la protesta, dos ladrones intentaron atacar a un estudiante que estaba en moto, él pidió auxilio, lo que desató una persecución en la que se capturó a los dos hombres, pero luego intentaron lincharlos.

Los delegados de derechos humanos que estaban en la zona intervinieron para que no se les golpeara, y entonces la multitud decidió llevar a los ladrones a la casa del Comandante de la Policía que está cerca de la Universidad, como forma de pedir que se hiciera algo con ellos, debido a que ambos ladrones habían sido identificados por otros actos contra estudiantes. Posteriormente, la marcha siguió avanzando y lo que ocurrió después aún no está del todo claro.

Una versión de los hechos indica que la Policía iba a dejar en libertad a los ladrones, lo que generó la furia colectiva; otra versión señala que uno de los uniformados que estaban custodiando la casa del Comandante desenfundó su arma y disparó en dos ocasiones. Lo que sí está claro es que ese día se presentaron ataques a la casa con bombas incendiarias; un hecho que Gómez reiteró que era extraño, porque los estudiantes de la Universidad se han caracterizado "por realizar protestas de manera pacífica, de hecho, desde 2012 no se han presentado disturbios violentos".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
