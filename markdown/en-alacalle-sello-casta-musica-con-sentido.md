Title: "Sello Casta", música con sentido en "A la Calle"
Date: 2015-06-06 02:00
Category: Sonidos Urbanos
Slug: en-alacalle-sello-casta-musica-con-sentido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/SELLO-CASTA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_4600521_2_1.html?data=lZudkpqWdY6ZmKial5uJd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl6bArbSYpaa3mKKfhpelz4qnd4a2otjWxcaPp9DijNjS0NnNqNCZk56ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### ["Sello Casta" y su música, invitado en A la Calle] 

Jorge Eduardo Gonzales, "***Sello Casta***", es un artista radicado en Bogotá. Comenzó su carrera a los 13 años de edad y ha participado en diversos eventos en universidades y colegios de la ciudad. Cuenta con dos álbumes titulados "**Por amor a la calle**" y **"Gotas de tinta**". Expresa en sus letras una visión crítica frente a lo social y cultural, tomando la literatura de autores como Germán Castro Caicedo como referente en sus canciones.

\[caption id="attachment\_9797" align="aligncenter" width="4546"\]![\_DSC0002](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/DSC0002-e1433520858958.jpg){.wp-image-9797 .size-full width="4546" height="2537"} "Sello Casta" junto al equipo de A la Calle, Angel, Kevin y Leonardo.\[/caption\]
