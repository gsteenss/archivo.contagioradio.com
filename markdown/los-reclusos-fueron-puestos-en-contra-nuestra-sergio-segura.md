Title: "Los reclusos fueron puestos en contra nuestra" Sergio Segura
Date: 2015-09-22 18:25
Category: DDHH, Entrevistas
Tags: 13 jóvenes capturados, atentados Bogota, Colombia Informa, ELN, Sergio Segura
Slug: los-reclusos-fueron-puestos-en-contra-nuestra-sergio-segura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Entrevista-con-Sergio-Segura.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_8552560_2_1.html?data=mZqilJqadI6ZmKialJ2Jd6KolJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYtcrWq8rjjLjSydrWpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Sergio Segura] 

En Contagio Radio hablamos con Sergio Segura, uno de los jóvenes líderes sociales a quien se le devolvió la libertad, luego de permanecer más de 2 meses en prisión. Él habló su experiencia en la cárcel, los maltratos a los que fue sometido, y el peligro en el que están sus vidas, las de sus familias y el movimiento social, tras las acusaciones realizadas en los medios hegemónicos.

**Contagio Radio** ** ¿Quién es Sergio Segura?**

**Sergio:** Hasta antes de la captura yo me dedicaba a escribir artículos para la Agencia de Comunicación de los Pueblos Colombia Informa, era el editor encargado para la sección de Conflicto y Paz. Principalmente me dedicaba a cubrir este tipo de situaciones sobre todo lo que tiene que ver con los Diálogos de Paz.

 Antes  de la captura había sido invitado en la Universidad Nacional para hablar en una conferencia sobre todo lo que tiene que ver con medios, conflicto social, armado, y todo lo que tiene que ver con libertad de prensa y manipulación mediática de la información frente a la paz. También hacia otro tipo de cuestiones más académicas como investigación social sobre temas que tienen que ver con movimientos sociales, estaba a punto de terminar un posgrado de esta misma materia, pues también soy profesional en Comunicación Social, me dedicaba también a tocar algo de música entre otras cosas que siempre  han procurado ser una herramienta de comunicación y difusión de distintas problemáticas sociales que vive el país.

**CR ¿Qué estaba haciendo en el momento en que lo capturan?**

**Sergio:** En ese momento me encontraba durmiendo, pues eran las 6:00 de la mañana. Llegaron golpeando, todo era una cantidad de policías armados, había francotiradores al frente del peatonal, había personal de antiexplosivos, pero principalmente agentes de la SIJIN.

Yo salí  a la ventana y les dije que no rompieran la puerta que yo bajaba a abrirles, todo era cuestión de vestirme. No esperaron a eso y como me asomé por el balcón creyeron que me iba a volar, por lo que agilizaron todo el procedimiento, yo mismo abrí la puerta del apartamento de un segundo piso, ni siquiera sabían que iba a ser la persona que iban a capturar.

Me dijeron el nombre y yo les dije “si soy yo”, les dije donde era mi habitación, cuando se cercioraron que yo era la persona que estaban buscando me dijeron los cargos que era rebelión, concierto heterogéneo para delinquir y terrorismo, pero esos no son los cargos por los cuales me investigaron  y tampoco fueron los cargos que se nombraron durante todo el procedimiento. Me los cambiaron dos otras veces de cargos.

**CR ¿Que sintió con la forma como  usted fue allanado y además por esos cargos como terrorismo que suenan tan fuerte?**

**Sergio:** Primero fue una sorpresa, ni siquiera pensé que se dirigían a mi apartamento. Luego de eso lo que hice fue reflexionar un poco y pensar el por qué me estaban buscando, si era por mi activismo social y porque he vendido haciendo comunicación social. Cuando veo que esto fue una captura masiva, y nos damos cuenta que la mayoría de personas fueron capturadas a la misma hora y también eran pertenecientes a organizaciones del Congreso de los Pueblos, entonces ya todo empezó a tener un poco más de sentido y empezamos a conocer  todo este entramado falaz que se venía planeando, no solo de las fuerzas de seguridad del Estado, sino también por parte de los medios de comunicación.

**CR: ¿Cómo se sintió usted y cómo vio también que se sintieron los demás jóvenes capturados por la forma como se realizó el cubrimiento de los diversos medios empresariales tildándolos muchas veces de guerrilleros y que hacían parte de una célula urbana del ELN?**

**Sergio:** solo hasta la primera audiencia que fue casi a la media noche del día que nos capturaron pudimos darnos cuenta que no tenía nada que ver con lo que se estaba diciendo en los medios al momento de la captura. Darío Arizmendi, noticieros como CMI ,Cityty,  noticias RCN, Caracol, El Tiempo y Semana, sobretodo dijeron que habían capturado a los terroristas que habían atentado contra las sedes de Pensiones y Cesantías Porvenir, entonces es una gran sorpresa porque hemos visto que ninguna de las trece personas capturadas que ahora estamos en libertad tuvimos que ver en algo con esa situación, además en ninguna audiencia nos mencionaron ese caso, aunque si estaban buscando a esas personas.

Lo que sentimos es que estaban buscando una excusa o un chivo expiatorio para darle contentillo a la ultraderecha que estaban presionando al gobierno por resultados. Lo primero que hizo el General Palomino fue decir que habían capturado a estos “bandidos” que habían sacado de sus “madrigueras” pero como nos pudimos dar cuenta esas “madrigueras” quedaban en casas común y corriente  en localidades como Ciudad Bolívar, Suba, particularmente a mi me capturaron en Galerías, otras personas en otros sectores de Teusaquillo. Barrios común y corriente ningunos escondites y mucho menos “madrigueras” como lo dijo el General Palomino.

Todo el país estaba felicitando a la Policía y a la Fiscalía por estas capturas. En Twitter el Presidente Santos solo tardó unos minutos después de la captura para felicitar personalmente al General Palomino y a la Fiscalía por estas capturas, y dijo algo así muy descaradamente “pagarán por lo que hicieron”.

Entonces,  si fue un cubrimiento bastante problemático para nosotros, poniendo no solo en riesgo nuestra reputación sino la seguridad de las organizaciones  que nos rodean, de nuestras familias y principalmente nuestra labor que tiene que ver con Derechos Humanos, en otros casos derechos de las mujeres  y en mi caso la labor comunicativa.

Sentimos bastante preocupación y zozobra por todo lo que estaba sucediendo y todo lo que sigue sucediendo en los medios masivos. Esto no ha cambiado mucho diciendo, pues cuestionan la decisión del Juez de conocimiento que nos dejó en libertad, y siguen diciendo que tenemos que ver con los atentados en las sedes de Porvenir, pese a que quedó bastante claro que ninguna de las trece personas tiene que ver con esto.

**CR: ¿Cómo se viven estos meses en la prisión, mientas los medios de comunicación sigue reiterando que ustedes hacía parte de estructuras guerrilleras y ese tipo de cosas, que como usted lo dice atentan contra su vida, la de sus familias y en general ponen en peligro el movimiento social en Colombia?**

**Sergio:** Luego de la captura nos llevaron a Medicina Legal a hacer un chequeo que supongo, es protocolo después de hacer estas capturas. Luego fuimos a la URI, allí ninguna de las personas que estábamos sabíamos qué hacer, pues nunca habíamos estado en un lugar de estos.

En una situación de estas, lo primero que hicimos fue tener un poco de precaución, porque estaban pasando cosas bastante irregulares como que el todo el tiempo estaban entrando perros antiexplosivos, el ESMAD estaba alrededor de la URI. Siempre estuvimos bastante custodiados, salíamos todos los días en la prensa, incluso exponían ante la opinión pública situaciones que nada tenía que ver con esto, como por ejemplo llamadas en donde había cuestiones meramente personales, esto lo que hacía principalmente era dañar toda la investigación que estaban haciendo, quitándole la seriedad y generando que la sociedad en general empezara a darse cuenta que eso se trataba de un montaje, de una ataque del gobierno hacia el Movimiento social y persecución política, por lo que realizamos, lo que nos agravaba la situación y nos ponía en peligro en los lugares en donde estuviéramos.

Luego del 28 de Julio, cuando la Juez municipal 72 de garantías dicta la medida de aseguramiento nos llevaron  a la Cárcel Modelo, nos llevaron a escondidas y no le avisaron a nadie.

Fue un viernes casi a la media noche después de una audiencia, lo que sucedió el 31 de julio el día que nos llevaron fue bastante sorprendente, no esperábamos que nos llevara a  media noche porque lo que teníamos entendido es que a esas horas o los fines de semana no se llevan a las personas a la cárcel, pudimos  ver casi como una pelea entre el Sargento encargado de la URI Y El INPEC para que nos recibieran, ya que ellos decían prácticamente que querían salir de ese “chicharrón” y que no querían seguir haciendo custodia  de nosotros.

Algunas familias no se enteraron donde nos encontrábamos, sino hasta el tercer día de haber estado allá, nos dijeron que cuando iban a la URI  decían que no estábamos, otros decían que nuestras cosas si estaban per que nosotros no, para no decir que ya nos habían llevado a la cárcel pues estaban rompiendo con todo un procedimiento legal.

En la cárcel también vivimos situaciones difíciles. Primero por los supuestos hechos  por los que nos llevaban pues hacía que todo mundo en la cárcel supiera quiénes éramos, había personas por todo tipo de delito, temíamos por nuestra vida pues la dinámica de la cárcel  y todo en la dinámica del INPEC es una cuestión bastante complicada, insegura  y corrupta.

Desde ese momento ya hacíamos parte de los más de 9.500 presos políticos que hay en el país y se acrecentaban los peligros que podía haber en nuestra contra.

**CR: ¿Cómo se vivió y sintió el apoyo con los plantones que se realizaron afuera de Paloquemao, cuando estaban en las audiencias?**

**Sergio:** Para nosotros fue bastante importante  el apoyo porque nos dio mucha fortaleza, principalmente los primeros días o quizá todo el tiempo que estuvimos encerrados pues no dimensionábamos la magnitud de lo que estaba pasando afuera.

Teníamos una comunicación limitada, a veces ni siquiera permitían que nos dieran cartas u otro tipo de comunicación entonces lo que pasaba y sentíamos es que nos tenían al margen de los medios para que se siguiera reproduciendo lo que se decía en los medios masivos, entonces nunca pasaban lo que realmente estábamos viviendo allí, que nos llevaban al baño esposados, que muchos policías nos seguían hostigando, que a veces nos quitaran la comida al llegar a la URI, muchos de nosotros fuimos maltratados en los trayectos. No obstante, lo que nosotros pudimos observar es de qué están hechos estos aparatos estatales de seguridad, pudimos conocer la rigurosidad del conflicto, pudimos ver que hay varios policías que son nobles y buenas personas pero que les prohibían que hablaran con nosotros, pero siempre algunos oficiales eran los que les daban las ordenes y ellos no tenían más opción.

Algunos policías nos contaban que su vida no era lo que ellos quería, se sentían presos también mientras nos custodiaban, incluso una vez uno de los policías nos dijo que estaba pidiendo que lo retiraran porque ya llevaba 11 años en la SIJIN. Es bastante complicado ellos también se sentían de uno u otro modo presos, unos se ponían contentos  porque nos llevaban a audiencias porque salían del encierro de la cárcel.

Supongo que esta es la dinámica de estos aparatos que hacen parte de la seguridad del Estado, sin embargo escuchar las cosas de afuera nos llenaba de muchos motivos para salir adelante para no desfallecer para no caer en la tristeza y la preocupación, escuchábamos desde las audiencias los tambores, nuestros nombres afuera, nos llegaban fotos, cartas, entonces eso fue bastante importante. Si no hubiera sido por esta presión social y también por todo lo que hicieron los medios alternativos, quizá nunca se le hubiera puesto la atención que esto merecía para que se desmintiera los hechos de Porvenir principalmente, que era algo que nos causaba bastante preocupación porque fueron unos atentados muy graves, aunque cada persona tenía pruebas de que no tenía nada que ver con esos hechos. Sin embargo una cosa era lo que pasaba en las audiencias, otra era lo que decía la Fiscalía y otra cosa era lo que decían los medios masivos,  vivimos bastantes mentiras y acusaciones, pero el apoyo social logró controvertir esto y logró tanto que nacional e internacionalmente se brindara el apoyo que nosotros necesitábamos.

Este tipo de cosas suceden todo el tiempo, por esos días también habían detenido a personas de Marcha Patriótica, personas que habían participado en el paro agrario de manera que esto fue lo que develó la complejidad de lo que sucede  en la justicia colombiana, hay un índice bastante clave, como por ejemplo del 2009 al 2011 más de 8000 personas fueron detenidas por delitos como rebelión terrorismo y más del 75% salieron en libertad, de manera que las demandas que tiene el Estado por esto son infinitas.

Hay que ponerle mucha atención a esto, de todas maneras nunca va cambiar el sistema judicial ya sabemos a quién  sirve este sistema de justicia que no es para nada justo, nuestro caso llamó la atención porque fue en Bogotá, pero esto pasa todo el tiempo en la región a los campesinos, a los indígenas a muchos estudiantes, a muchas mujeres y a muchos jóvenes. Deben realizarse reformas y los ajustes para que no se sigan cometiendo este tipo de actos injustos, sobretodo en este momento que se está hablando de paz.

**CR: ¿Podría especificar más sobre esos maltratos a los que usted se refiere cuando estuvo en prisión?**

**Sergio:** Bueno, voy a poner un ejemplo. Por lo general no tuvimos complicaciones muy graves por lo que había tantos ojos encima, pero una de las cosas que sucedieron fue que le estaban diciendo a personas detenidas en la URI que el domingo no iban a ver visitas de los niños porque las personas que estaban “capturadas por la bombas” podrían coger a los niños de rehenes, lo que ponía a los todos los reclusos en contra nuestra, asegurando que nosotros representábamos un peligro para todas las personas que estaban allí.

A veces nos apretaban mas fuerte las esposas, los insultos eran todo el tiempo pero como les digo no es una cuestión tan general, hay matices, como les dije existen policías con lo que uno podía hablar de su vida.

**CR: ¿Qué viene para usted, que viene para su familia, también centrándonos en los tema de la seguridad?**

**Sergio:** Situaciones como los seguimientos, como los hostigamientos, las amenazas se han convertido casi que normales en el movimiento social, se denuncian internacionalmente y en ocasiones es muy poco lo que pasa entonces tenemos que cuidarnos bastante porque no es cualquier cosa que salgan nuestros rostros en televisión y la prensa diciendo que somos terroristas con todo lo que implica un calificativo de esta magnitud. Queremos garantías por lo que se viene, para poder asumir una postura correcta con esta investigación porque hasta el momento no hemos tenido garantía ni el proceso judicial, ni en la calle, antes de nuestra captura eran conocidas las amenazas, los hostigamientos ilegales bastantes cuestiones que ponen en riesgo nuestra seguridad.

Hacemos un llamado a la comunidad nacional e internacional para que sigamos pendientes de estos casos, esto no ha terminado y no terminará hasta que haya una transformación  radical de este tipo de imposiciones estatales.
