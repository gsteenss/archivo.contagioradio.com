Title: Con novenas y villancicos el paro estudiantil se mantiene en diciembre
Date: 2018-12-05 12:06
Author: AdminContagio
Category: Educación, Movilización
Tags: educacion, estudiantes, ley 30, Mesa de negociación, Paro Nacional
Slug: paro-estudiantil-diciembre
Status: published

###### [Foto: Contagio Radio] 

###### [5 Dic 2018] 

Luego de la realización del Encuentro Nacional de Emergencia de Estudiantes por la Educación Superior (ENEES), que se desarrolló en la sede Bogotá de la Universidad Nacional de Colombia, y del que participaron más de 2500 estudiantes en representación de 57 Instituciones de Educación Superior (IES), se decidió que **el paro continuará en diciembre, al igual que las dinámicas de movilización y resistencia**.

Para **Cristian Reyes, vocero nacional de la Unión Nacional de Estudiantes por la Educación Superior (UNEES)**, el balance del Encuentro es bueno, en tanto las IES participantes manifestaron su intención de mantener el paro y los bloqueos en los Campus universitarios, así como se mostraron positivos ante los objetivos trazados en el pliego nacional de esta organización estudiantil.

No obstante, Reyes sostuvo que los encuentros con el Gobierno Nacional no han sido efectivos, esto en razón de que la posición de la Ministra de Educación ha sido pedir que se retomen las clases, pero **sin entregar más recursos para la base presupuestas de las IES públicas**. (Le puede interesar: ["Nuestra estrategia será movilizarnos con con alegría: UNEES"](https://archivo.contagioradio.com/movilizarnos-unees/))

El estudiante reconoció que el Gobierno propuso entregar 30 mil millones para superar el déficit que enfrentan Universidades e Institutos tecnológicos, así como reorganizar la distribución del acuerdo pactado con los rectores; pero **la UNEES ha señalado que el hueco presupuestal para finalizar 2018 asciende a 4 billones de pesos**. (Le puede interesar: ["Llueva o truene, el paro se mantiene"](https://archivo.contagioradio.com/paro-se-mantiene/))

Adicionalmente, Reyes señaló que están a la espera de la aprobación de un decreto que mantenga la mesa de concertación en el tiempo y anunció que en enero de 2019 se realizaría un nuevo ENEES para evaluar la dinámica de movilización durante diciembre, así como para discutir la proyección política del movimiento estudiantil, que incluye la modificación de la Ley 30 de educación superior.

### **Aún hay alrededor de 40 campamentos de caminantes en Bogotá** 

Para el desarrollo del ENEES, así como de las movilizaciones que tuvieron lugar en la capital del país la semana pasada, cerca de 600 estudiantes de diferentes regiones llegaron caminando como forma de protesta, exponiendo a lo largo de su recorrido la grave crisis que atraviesa la educación superior. (Le puede interesar: ["Caravanas de estudiantes avanzan hacía gran movilización por la educación"](https://archivo.contagioradio.com/caravanas-de-estudiantes-avanzan-hacia-gran-movilizacion-por-la-educacion/))

Según Reyes, en este momento aún hay cerca de 40 campamentos en Bogotá, que han recibido por parte de la comunidad universitaria mercados, así como recursos para su manutención y regreso a sus regiones de origen, camino que emprenderán luego de participar en la **movilización nacional de este 6 de diciembre**. (Le puede interesar:["De Caquetá a Bogotá: 550 kilómetros caminando por la educación superior"](https://archivo.contagioradio.com/550-km-caminando-educacion-superior/))

### **Estudiantes se preparan para comer Natilla y Buñuelos en los campus universitarios** 

Reyes reconoció que es difícil sostener jornadas de protesta en diciembre, en tanto representa para muchos estudiantes el retorno a sus lugares de origen o trabajar en temporada para pagar los recursos del siguiente semestre, por lo que afirmó que desde la UNEES se está contemplando que **las jornadas de movilización se mantengan en conjunto con otros sectores como centrales obreras y gremios de profesores**.

La primera jornada de movilización de diciembre se llevará a cabo este jueves 6 en conmemoración por la masacre de las bananeras, actividad que** finalizará en la Plaza de Bolivar con un concierto en el que participarán 1280 almas y los Petit Fellas**. Adicionalmente, se está discutiendo la posibilidad de volver a las calles el 13 de diciembre para reclamar por la Ley de Financiación.

Para finalizar el estudiante resaltó los campamentos que se mantienen en los campus universitarios, entre ellos el de la Macarena en la Universidad Distrital, que lleva 55 días; y manifestó que en muchas IES estos espacios continuarán, **con novenas y villancicos, manteniendo su estadía en los territorios universitarios**. (Le puede interesar: ["Las 3 condiciones de los estudiantes para levantar el paro nacional"](https://archivo.contagioradio.com/condiciones-levantar-paro-nacional/))

> DECLARACIÓN POLÍTICA
>
> Desde la Unión Nacional de Estudiantes de Educación Superior invitamos a la comunidad a seguir fortaleciendo el movimiento estudiantil colombiano.  
> Gracias a las más de 55 IES por creer en la UNEES. [\#NosVemosEnLasCalles](https://twitter.com/hashtag/NosVemosEnLasCalles?src=hash&ref_src=twsrc%5Etfw) [\#VenYTeUNEES](https://twitter.com/hashtag/VenYTeUNEES?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/vUVNZRme4d](https://t.co/vUVNZRme4d)
>
> — UNEES (@UneesCol) [5 de diciembre de 2018](https://twitter.com/UneesCol/status/1070339261940011008?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
<iframe id="audio_30608917" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30608917_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
