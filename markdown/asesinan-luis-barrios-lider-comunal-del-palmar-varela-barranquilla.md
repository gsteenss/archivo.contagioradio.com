Title: Asesinan a Luis Barrios líder comunal del Palmar de Varela en Barranquilla
Date: 2018-07-03 17:56
Category: DDHH, Nacional
Tags: Barranquilla, Lider social, Luis Barrios
Slug: asesinan-luis-barrios-lider-comunal-del-palmar-varela-barranquilla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Herlando1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cuenta de Twitter Iván Cepeda] 

###### [03 Jul 2018]

El senador Iván Cepeda denunció en su cuenta de Twitter, el asesinato de Luis Barrios, líder comunal del Palmar de Varela. Los hechos ocurrieron durante la tarde del martes cuando el líder fue víctima de un atentado en su casa mientras veía el partido de fútbol entre la selecciones de Colombia e Inglaterra.

De acuerdo con versiones de testigos, dos hombres habrían ingresado a la vivienda de Barrios, entraron a la habitación en donde se encontraba y le dispararon en la cabeza, acabado con su humanidad.

Noticia en desarrollo ...
