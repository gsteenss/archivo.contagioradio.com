Title: "Detención de 4 estudiantes de la ACEU  es un montaje judicial":JUCO
Date: 2016-05-03 12:56
Category: Movilización, Nacional
Tags: estudiantes, Falsos Positivos Judiciales, Marchas, Movimiento estudiantil
Slug: detencion-de-4-estudiantes-de-la-aceu-podria-ser-un-montaje-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/juco-e1462297315541.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Juventud Comunista Bogotá] 

###### [3 May 2016] 

El pasado primero de mayo, luego de la movilización que se realizó en Medellín, 4 estudiantes pertenecientes a la Asociación Colombiana de Estudiantes Universitarios (ACEU) fueron capturados e imputados por el delito de terrorismo, **con pruebas insuficientes y una condena anticipada por parte del Físcal**.

Los hechos se presentaron después de la movilización, cuando un grupo de policías vestidos de civil se acercaron a los estudiantes, los requisaron y posteriormente trasladaron al CAI de La Candelaria, luego de esto, los estudiantes fueron llevados al Bunquer de la Fiscalía donde fueron imputados por terrorismo.** Las pruebas que se presentaron eran vídeos de muy baja calidad, donde aparecían personas encapuchada y testimonios de policías que afirmaron ver a los estudiantes pintando graffitis y entregando pegatinas en medio de la marcha alusivas a las FARC. **A su vez la detención de los estudiantes se llevo a cabo sin orden de captura, ni el debido proceso.

De acuerdo con Carlos Galeano, miembro del Comité Ejecutivo de la ACEU, los estudiantes **participaron de la movilización todo el tiempo junto con sus compañeros sin taparse el rostro,** y las pruebas presentadas por la Fiscalía ni siquiera pueden asegurar que las personas encapuchadas corresponden a los rostros de los estudiantes.

**Las 4 personas eran activistas del movimiento estudiantil y ambientalistas,** además participaron en el proceso de movilización de la Universidad de Antioquía del año pasado frente a la reforma al examen de admisión y tenían una apuesta por la salubridad de las aguas en la Comuna 8 de Medellín.

Galeano afirma que estas capturas hacen parte de un nuevo caso de falso positivo judicial y  que por ahora están en el proceso de la **recolección de alimentación e implementos de aseo**, debido a que los estudiantes no se encuentran en la cárcel, sino en la SIJIN, lugar en donde no les suministran alimentación. Esta semana se convocará una **Asamblea Estudiantil en la Universidad de Antioquia** para definir las acciones que se llevarán a cabo.

<iframe src="https://co.ivoox.com/es/player_ej_11396315_2_1.html?data=kpagm5uXdZahhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncYyZk5exx9nJssTdhqigh6eXsozYxpChjcrXuNbYysbb1srXb8XZjNHOjaanibafxtiY19OPsdDi1cbXx5CRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
