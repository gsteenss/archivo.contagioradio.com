Title: Guerrillero muerto en bombardeo era parte de delegación de paz y desarrollaba labores pedagógicas
Date: 2015-05-27 11:11
Category: Nacional, Paz
Tags: bmbardeo, delegacion de paz, FARC, Guapi, jairo martinez, Santos
Slug: guerrillero-muerto-en-bombardeo-estaba-haciendo-labores-de-pedagogia-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Captura-de-pantalla-2015-05-27-a-las-11.11.26.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colombiano 

###### [27 may 2015] 

Según lo expuesto por la guerrilla de las FARC en un comunicado, uno de los guerrilleros asesinados en el bombardeo realizado por el Ejercito en el municipio de Guapi, Cauca, era Jairo Martinez, ex integrante de la Delegación de Paz de esta insurgencia en Cuba, y que había regresado a Colombia para realizar pedagogía en las filas guerrilleras en torno al proceso de paz.

Según el comunicado, se habría constatado que los guerrilleros heridos en el bombardeo fueron rematados con tiros de gracia, tal como ocurriera con Alfonso Cano, máximo comandante de la guerrilla, en el año 2011. Por este motivo, la guerrilla de las FARC solicita a organismos nacionales e internacionales de medicina legal y forense, y al CICR, que se inspeccionen los cadáveres de los guerrilleros, para hacer pública la causa de sus decesos.

También se constató el asesinato del comandante Román Ruiz, en el bombardeo adelantado por el Ejercito el 25 de mayo en el departamento del Chocó.

“Queremos afirmar de manera enfática, que se equivoca el Gobierno de Santos si piensa que con los cuerpos destrozados y la sangre de nuestros compañeros, va a imponernos una justicia que no persigue la responsabilidad de los poderosos", aseguró Pastor Alape, en Cuba.

La guerrilla finalizó su intervención haciendo un nuevo llamado a que se avance en el desescalamiento del conflicto armado, y el acuerdo de un cese al fuego bilateral. “Tenemos que salir de esta turbulencia para entregarle al pueblo colombiano un horizonte diáfano que nos permita transitar, libre de enredaderas, el camino hacia el Acuerdo Final. No más pérdidas de valiosas vidas humanas", declaró Alape.
