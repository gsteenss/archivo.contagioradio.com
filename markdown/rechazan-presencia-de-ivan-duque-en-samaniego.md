Title: Con gritos de paz y justicia rechazan presencia de Iván Duque en Samaniego
Date: 2020-08-22 18:37
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Iván Duque, Samaniego
Slug: rechazan-presencia-de-ivan-duque-en-samaniego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/EgC5ZhzXoAI_54h.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Iván Duque/ [@jhonrojasca](https://twitter.com/jhonrojasca)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con gritos de justicia, pulgares abajo y arengas exigiendo la paz, la población de Samaniego, Nariño salió a las calles par rechazar la presencia del presidente Iván Duque, en lo que se interpreta como múltiples gestos de indignación por su ineficaz gestión frente a los grupos criminales y las masacres. [(Le recomendamos leer: Nueva masacre, en Nariño fueron asesinados 9 jóvenes)](https://archivo.contagioradio.com/nueva-masacre-en-narino-fueron-asesinados-9-jovenes/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La visita de Duque a Samaniego se da luego de que se conociera que esta semana el Alcalde de Samaniego, **Óscar Pantoja tuvo que viajar 19 horas en carro para llegar a la casa de Nariño a atender un llamado del [mandatario](https://twitter.com/infopresidencia)para que le contara lo sucedido en la masacre en la que fueron asesinados 9 jóvenes el pasado el pasado 11 de agosto**. [(Lea también: En Gobierno Duque han ocurrido 1.200 violaciones de DDHH contra el pueblo Awá en Nariño)](https://archivo.contagioradio.com/en-gobierno-duque-han-ocurrido-1-200-violaciones-de-ddhh-contra-el-pueblo-awa-en-narino/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, desde Bogotá, activistas políticos como María Fernanda Carrascal denunciaron que no se permitió el viaje de la Comisión de paz del senado en avión oficial, prevista para este fin de semana.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RodrigoDDHH/status/1297264321035206657","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RodrigoDDHH/status/1297264321035206657

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Los reclamos contra el Gobierno se han intensificado también por la nula respuesta ante las cinco masacres que han ocurrido este mes y en las que la mayoría de las víctimas han sido menores de edad. [(Lea también: Tres masacres en una semana, seis jóvenes fueron asesinados en Tumaco, Nariño)](https://archivo.contagioradio.com/tres-masacres-en-una-semana-seis-jovenes-fueron-asesinados-en-tumaco-narino/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Contagioradio1/status/1297301432153112581","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Contagioradio1/status/1297301432153112581

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/QSirleny/status/1297281068693946370","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/QSirleny/status/1297281068693946370

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
