Title: "Toma de Caracas" sería parte de un plan de desestabilización en Venezuela
Date: 2016-08-31 13:36
Category: El mundo, Política
Tags: Golpe de Estado, Venezuela
Slug: vicepresindete-de-venezuelaa-alerta-plan-desestabilizacion-en-marcha-toma-de-caracas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/imperiocrucial.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Imperiocrucial] 

###### [31 Ago 2016] 

**El primero de septiembre se llevará a cabo la marcha “Toma de Caracas”** en Venezuela convocada por los sectores de oposición, que de acuerdo con el vicepresidente Aristóbulo Isturiz obedece a un plan de desestabilización, además las autoridades anunciaron la **captura de dos personas con armamento pesado e implementos militares que participarían de la movilización**.

Uno de los detenidos es Yon Goicoechea miembro del Partido Opositor Voluntad Popular, quien **tenía en su poder cordones detonantes para explosivos**. Por su parte, el vicepresidente Isturiz insto a los ciudadanos a mantener la movilización permanente y salir a las calles el 1 de septiembre en defensa del presidente Maduro.

A su vez, la organización ALBA Movimientos, en un comunicado de prensa, **manifestó su preocupación frente al posible intento de golpe de Estado** y expresó su solidaridad con el pueblo de la República Bolivariana de Venezuela.

De acuerdo con Hernán Vargas quien hace parte de la secretaría operativa del ALBA y del Movimiento de Pobladores de Venezuela, **“esta marcha es otro intento por parte del sector oficial de traducir su porcentaje electoral en su porcentaje de movilización en la calle, que es algo que no ha logrado hacer”**.

Sobre las personas detenidas Vargas afirmó que “forman parte de un golpe de Estado, orquestado por algunos líderes de la derecha que han venido llevando este proceso, por eso desde el Chavismo[hemos estado en una guerra permanente y de distintas maneras](https://archivo.contagioradio.com/la-oea-la-intervencion-y-venezuela/); mañana también estaremos movilizándonos defendiendo las calles centrales de Caracas”.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU).] 
