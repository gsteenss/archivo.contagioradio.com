Title: Indígenas del Norte del Cauca iniciaran siembras si Min Interior no acude a diálogo
Date: 2015-03-03 21:04
Author: CtgAdm
Category: Movilización, Nacional
Tags: Corinto, Feliciano Valencia, Indigenas del Norte del Cauca, liberación de la madre tierra, Miranda, ONIC, Pueblo Nasa de Colombia, Santader de Qulichao
Slug: indigenas-del-norte-del-cauca-iniciaran-siembras-si-min-interior-no-acude-a-dialogo
Status: published

###### Foto: lauralruiz.wordpress.com 

<iframe src="http://www.ivoox.com/player_ek_4160892_2_1.html?data=lZajkp2ddo6ZmKiak5aJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjKvSzs7HrcLi0JDDw9HJssTdwpDg0cfWqYzgysfS1MbHrYa3lIqvldOPqMaf1c6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Entrevista a Feliciano Valencia] 

Desde el pasado viernes, parece haberse suspendido la orden de desalojo de las comunidades indígenas que están liberando la madre tierra en el norte del departamento del Cauca. Sin embargo las consecuencias de la represión siguen latentes puesto que hay **53 indígenas heridos, 4 de ellos con impactos de armas de fuego y 4 más están en proceso de judicialización**, afirma Feliciano Valencia, vocero de las movilizaciones.

Valencia afirma que le han pedido a la fuerza pública que se retire a una distancia prudente para evitar la represión y la reacción por parte de los indígenas que si son atacados responderán. **La orden de desalojo no se ha levantado lo que permite una tensa calma**. Según Valencia la mayoría de los heridos son por golpes contundentes o cortes con **machetes que usan los efectivos del ESMAD.**

El vocero indígena agrega que las comunidades se mantendrán en las fincas señaladas como terrenos para la reparación de la masacre del Nilo y que no se retirarán a pesar de las presiones del gobierno. Esperan que el próximo **miércoles 4 de Marzo haga presencia el Ministro del Interior** y que llegue con soluciones prontas y eficaces, de lo contrario las comunidades establecieron que **iniciarán ejercicios de propiedad como limpias y siembras en los predios que reclaman.**

Desde el pasado 23 de Febrero, comunidades indígenas del Norte del Cauca realizan lo que han llamado "proceso de liberación de la madre tierra" para hacer valer las promesas incumplidas del gobierno en cuanto a la entrega de tierras como parte de la reparación colectiva por la masacre del Nilo que contempla 30000 hectáreas.

Las tierras reclamadas por los indígenas en los municipios de Corinto, Miranda y Santander de Quilichao, asciende a 6500 hc que el gobierno tiene alquiladas a los ingenios del Cauca, sobre los cuales pesan los intereses del propio ministro de agricultura que no se ha pronunciado sobre la reclamación de los indígenas.
