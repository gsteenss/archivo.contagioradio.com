Title: EPM administra acueductos comunitarios a favor de los más ricos
Date: 2016-03-16 16:28
Category: Movilización, Nacional
Tags: comuna 8 medellin, Empresas Públicas de Medellín, Hidroituango, Movimiento Ríos Vivos
Slug: epm-administra-acueductos-comunitarios-a-favor-de-los-mas-ricos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Acueducto-Comunitario.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semillas ORG] 

<iframe src="http://co.ivoox.com/es/player_ek_10830692_2_1.html?data=kpWllZWafZOhhpywj5acaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncabErpDOxtLNssrn1dfOjcbHucbY1sjh0diPp9Dh1tPW1sbWrdDnjMaYyMbas9OfxcqYztTXb86hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Isabel Cristina Zuelta, 'Movimiento Ríos Vivos'] 

###### [16 Mar 2016 ]

[Este domingo con la participación de mil setecientas personas se llevó a cabo una consulta popular autónoma promovida por el 'Movimiento Ríos Vivos', la 'Mesa de Desplazados' y la 'Mesa de Vivienda y Proceso de Planeación Local de la Comuna 8 de Medellín', con la que se busca lograr una política pública de mejoramiento integral para los barrios de esta comuna, habitados en su mayoría por población desplazada que enfrenta racionamientos selectivos de agua, aún cuando viven en corregimientos dónde hay nacederos.  ]

[De acuerdo con Isabel Cristina Zuleta, integrante del 'Movimiento Ríos Vivos' el 98% de la población votó a favor de que la construcción y el diseño de las viviendas que el gobierno entrega a los habitantes de la Comuna 8 cuente con el aval de las comunidades, debido a las críticas que las familias desplazadas han presentado frente a los modelos de vivienda de 40 km2 que el Estado pretende imponerles sin tener en cuenta que proceden de zonas rurales en las que las viviendas son distintas. ]

A las precarias condiciones de vivienda que enfrentan estas familias se suman los constantes racionamientos de agua que se dan en corregimientos en los que los acueductos comunitarios fueron comprados por Empresas Públicas de Medellín, quien "hoy los administra a su antojo privilegiando las zonas más ricas de la ciudad", como afirma Zuleta.

Zuleta agrega que esta consulta popular no contó con la autorización de la alcaldía de Medellín, ni con el aval de la Gobernación de Antioquia quien además le incumplió con el transporte a las delegaciones que se trasladaron para la movilización del pasado lunes y que se vieron obligadas a [[alojarse en la Universidad de Antioquia](https://archivo.contagioradio.com/400-campesinos-desplazados-por-epm-estan-refugiados-en-la-u-de-antioquia/)] esperando garantías de retorno a los municipios afectados por la construcción de Hidroituango, en los que continúa propagándose un incendio que ha consumido 600 has de bosque seco tropical.

Tras esta movilización de los campesinos las autoridades de gobierno firmaron un decreto que será evaluado por las comunidades y con el que se pretende restituirlas por los daños causados con la implementación de Hidroituango y con los incendios que han provocado desconocidos en bosques en los que se ha buscado extender la construcción de esta represa, según refiere Zuleta quien agrega que se prepara una audiencia pública ambiental de seguimiento a esta problemática que debe ser autorizada por la ANLA.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
