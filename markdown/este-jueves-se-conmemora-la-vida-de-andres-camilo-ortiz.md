Title: Este jueves se conmemora la vida de Andrés Camilo Ortíz
Date: 2019-06-13 16:30
Author: CtgAdm
Category: Memoria
Tags: conmemoración, estudiante, memoria, Velatón
Slug: este-jueves-se-conmemora-la-vida-de-andres-camilo-ortiz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Andrés-Camilo-Ortíz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia de Noticias UN  
] 

Este jueves se llevará a cabo en la Universidad Nacional una conmemoración en honor a **Andrés Camilo Ortíz**, el joven estudiante de Contaduría que hace un año fue asesinado, presuntamente a manos de un integrante de la Policía.  (Le puede interesar: ["Universidad Nacional se moviliza contra asesinato de A. Camilo Ortíz"](https://archivo.contagioradio.com/universidad-nacional-se-moviliza-contra-asesinato-de-andres-camilo-ortiz/))

El evento iniciará con un panel, después de las 4 de la tarde, en el que participarán Jackeline Castillo, integrante de las Madres de Soacha; Erika Ortíz, hermana de Andrés Camilo Ortíz; Rosemberg Ariza, profesor vinculado a la Universidad Nacional; Cesar Santoyo, integrante del Colectivo Orlando Fals Borda; Frank Molano, vocero del MODEP y Gloria Alvarado, integrante del MOVICE.

La conmemoración se desarrollará en el **auditio principal del edificio 310 de ciencias económicas, y posteriormente se hará una velatón, afuera de la Facultad, frente al mural de Andrés Camilo.**

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
