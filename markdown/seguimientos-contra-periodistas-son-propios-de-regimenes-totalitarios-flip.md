Title: Seguimientos contra periodistas son propios de regímenes totalitarios: FLIP
Date: 2020-05-04 20:44
Author: CtgAdm
Category: Nacional
Tags: Ejército Nacional, FLIP, Libertad de Prensa
Slug: seguimientos-contra-periodistas-son-propios-de-regimenes-totalitarios-flip
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/LIBERTAD-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: periodistas/ Semméxico

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tras conocerse las pruebas de cómo el Ejército ejecutó un programa de seguimiento informático ilegal que incluyó al menos a 130 personas, muchos de ellos periodistas, se reabre el debate sobre qué tan seguro es ejercer la libertad de prensa en Colombia, un país en el que de 2017 a 2019 fueron amenazados 583 periodistas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicho seguimiento incluyó la recolección de teléfonos, direcciones de residencia y trabajo, correos electrónicos, identificación y perfilamiento de amigos, familiares, colegas, y contactos de dichos perfiles; actividades financiadas por una agencia de inteligencia extranjera que entregaba a los batallones de ciberinteligencia aproximadamente 400.000 dólares anuales para adquirir equipos y herramientas informáticas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para **Pedro Vaca, director de la Fundación para la Libertad de Prensa** se trata de una agresión que puede ser una de las más graves registradas en los últimos años, debido a su masividad y su extensa cobertura, pues no solo se trata de periodistas con un amplio recorrido, sino fotógrafos de zonas rurales, jóvenes que están comenzado su camino profesional e incluso corresponsales de medios internacionales. [(Le puede interesar: Prensa bajo la mira de actividades ilegales del ejército en Colombia)](https://archivo.contagioradio.com/prensa-bajo-la-mira/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Es muy grave que el Ejército esté haciendo un monitoreo del debate público, la Constitución plantea que la Fuerza Pública no es un cuerpo deliberante", afirma Vaca, quien advierte que cuando el Ejército tiene facultades de ponerles etiquetas, de perseguir sus fuentes y en general de imponer una vigilancia sobre ciertos sectores del periodismo con un criterio que aún se desconoce, **es una evidencia propia de regímenes totalitarios.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Seguimientos contra periodistas: una bitácora de promesas rotas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para el director de la [FLIP](https://twitter.com/FLIP_org) el problema viene desde mucho antes, pues pese a la desarticulación del Departamento Administrativo de Seguridad y la creación de nuevo sistema de inteligencia durante el gobierno Santos, los sucesos denunciados hoy, demuestran que no existen controles sobre los recursos humanos, infraestructura y recursos financieros que deberían ser usados para perseguir el crimen, pero son usados para perseguir actividades legítimas, **"esta es una responsabilidad que va más allá del Gobierno actual, es una responsabilidad de Estado".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Lo que da a entender este perfilamiento es que molesta la actividad y molesta lo que se dice"**, afirma Vaca quien concluye que aunque la violencia en las zonas rurales continúan siendo una de las principales problemáticas para ejercer la prensa en Colombia, ahora con las denuncias de seguimientos ilegales contra periodistas no existen garantías para quienes trabajan en los medios de comunicación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un escándalo en en Día Mundial de la Libertad de Prensa

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de lo ocurrido, y que coincidió con la conmemoración del Día Mundial de la Libertad de Prensa, las Naciones Unidas, pidió al Gobierno que tome medidas frente a los hechos revelados por la Revista Semana. [(Le puede interesar: Espionaje ilegal del Ejército de Colombia apunta a periodistas y Defensores de DD.HH)](https://archivo.contagioradio.com/espionaje-ilegal-del-ejercito-de-colombia-apunta-a-periodistas-y-defensores-de-ddhh/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, Alberto Brunori, representante de la alta comisionada de Naciones Unidas para los Derechos Humanos, recordó al Gobierno que debe existir una supervisión y control permanente sobre las actividades de inteligencia que adelanta el Ejército Nacional y que **estos deben respetar los derechos humanos, protegidos en pactos regionales e internacionales, como el de San José y la Declaración Universal de DD.HH. de Naciones Unidas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“El Estado debe de tomar medidas específicas para que los sistemas de inteligencia respeten los derechos humanos y estén sujetos a estrictos controles civiles y judiciales" manifestó el representante y resaltó la importancia de fortalecer la democracia y brindar garantías para "la defensa de los derechos humanos, de la libertad de prensa, de la libertad de opinión, de la libertad de asociación y de la libertad de participación política.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
