Title: 29 de Marzo jornada de movilización contra Transmilenio
Date: 2017-03-28 15:56
Category: Movilización
Tags: Bogotá, Movilidad, plantones, Transmilenio
Slug: 29-marzo-jornada-movilizacion-transmilenio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Transmilenio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [[Foto: Archivo Particular]] 

###### [28 Mar 2017] 

Una jornada de bloqueos en las vías de Transmilenio se llevó a cabo desde tempranas horas en Soacha y Bogotá, debido a los largos periodos de espera de los articularos y las alzas en el precio del pasaje que  pasará de \$2.000 a  \$2.200 pesos. Este miércoles 29 de marzo, se **realizará una movilización para exigir una mejora del servicio y el 31 de marzo, plantones y bloqueos en todos los portales de Bogotá**.

La marcha tiene como lugar de encuentro el SENA de la autopista sur con primero de mayo, desde las 7:00 am, esta movilización pretende entre otras cosas: reducir el precio del pasaje de Transmilenio, **mejorar la frecuencia de las rutas y la calidad de los articulados**, brindar garantías de derechos laborales y terminar con la tercerización de los trabajadores, la renegociación de los contratos y en la defensa del patrimonio público del que hace parte el sistema de transporte Transmilenio.

Durante la jornada de hoy, 20 estaciones de Transmilenio estuvieron sin servicio y los portales del Sur y de Suba, estuvieron bloqueados momentáneamente, **sin embargo, estos bloqueos esporádicos se han venido presentando desde las últimas dos semanas con mayor frecuencia.**

Para este 31 de marzo, diferentes páginas en redes sociales también han convocado plantones y bloqueos en todos los portales de la Capital, en diferentes horas del día. Le puede interesar: ["Aumento en transporte público en Bogotá el más alto en 8 años"](https://archivo.contagioradio.com/aumento-en-transporte-publico-en-bogota-el-mas-alto-en-8-anos/)

![Transmilenio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/transmi.jpg){.size-full .wp-image-38504 width="960" height="960"}

###### Reciba toda la información de Contagio Radio en [[su correo]
