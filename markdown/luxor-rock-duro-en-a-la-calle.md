Title: "Luxor, rock duro" en "A la Calle"
Date: 2015-06-19 13:11
Category: Sonidos Urbanos
Tags: A la Calle, Luxor Rock Duro
Slug: luxor-rock-duro-en-a-la-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/1798199_10153905301183018_1160026640057338525_n-e1434730952399.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### <iframe src="http://www.ivoox.com/player_ek_4663416_2_1.html?data=lZujlZmVeo6ZmKial56Jd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmrdrl0deJdqSfs9TQzZCoudPjhpefjcfFssXVjKfcydTYpc%2FVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

#### ["Luxor, Rock Duro" en A la Calle] 

[![10679586\_4846305092014\_7745635948295672260\_o](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/10679586_4846305092014_7745635948295672260_o.jpg){.aligncenter .size-full .wp-image-10289 width="2048" height="805"}](https://archivo.contagioradio.com/luxor-rock-duro-en-a-la-calle/10679586_4846305092014_7745635948295672260_o/)

"**Luxor Rock Duro**" es una banda que nace en el año de 2007 en la ciudad de Bogotá bajo la influencia de algunas propuestas musicales rock y glam rock de los años 80. El nombre "**LUXOR**" hace referencia a la homónima ciudad Egipcia. Han participado en diversos eventos que contribuyen a personas con pocos recursos, promoviendo el valor del respeto, dedicación y pasión en cada uno de sus proyectos. Desde **Hathor** (sala de ensayo) generan espacios para la formación y promoción de bandas que inician su camino en la música.
