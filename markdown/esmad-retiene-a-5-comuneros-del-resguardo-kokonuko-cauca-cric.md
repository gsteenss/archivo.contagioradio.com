Title: ESMAD retiene a 15 comuneros del Resguardo Kokonuko, Cauca: CRIC
Date: 2017-10-10 16:24
Category: DDHH, Nacional
Tags: Efigenia Vásquez, Resguardo de Kokonuko
Slug: esmad-retiene-a-5-comuneros-del-resguardo-kokonuko-cauca-cric
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-09-at-4.04.25-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

###### [10 Oct 2017] 

Indígenas del resguardo de Kokonuko continúan en permanente movilización en al predio Aguas Tibias. En horas de la tarde del día de ayer se presentaron enfrentamientos con el ESMAD que dejaron como resultado **2 indígenas heridos, durante la jornada de hoy el CRIC manifestó que habría 15 comuneros retenidos y un agente del ESMAD, en estos momentos se desarrollan conversaciones para las respectivas liberaciones.**

De acuerdo con Viviana Ipia, vice gobernadora del resguardo de Kokonuko, ayer hizo presencia en el territorio Horacio Guerrero, como delegado del Ministerio del Interior para establecer que ruta se podría seguir, sin embargo, la comunidad decidió no recibirlo, hasta que no haya acciones efectivas de la devolución del predio, “**se conversará siempre y cuando sea contundente el diálogo**, porque si es para generar nuevamente acuerdos, ya no estamos para esos tiempos”.

 En horas de la noche, los indígenas denunciaron que llegaron más efectivos del ESMAD al territorio, situación que para la vice gobernadora Ipia demuestra la falta de garantías para la comunidad y los ponen en alerta máxima. (Le puede interesar: ["Por ataque del ESMAD muere comunicadora indígena en Kokonuko, Cauca"](https://archivo.contagioradio.com/por-ataque-del-esmad-muere-comunicadora-indigena-en-kokonuko-cauca/))

Con respecto al avance de la investigación sobre el asesinato de la comunicadora indígena Efigenia Vásquez, la vicegobernadora expresó que la comunidad tiene miedo por la forma en la que se  ha realizado la recolección de información “entes policiales nos manifestan que los uniformados que estaban en el  área **fueron retirados el mismo día, para hacer lo de la revisión de las armas**, entonces no se hizo en el resguardo ni el sitio de los hechos” señaló Ipia.

De igual forma, Viviana señaló que han llegado muchas muestras de apoyo y solidariad a la familia de Efigenia, en respuesta a la labor que realizaba la comunicadora en torno a la defensa de los derechos de su comunidad.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
