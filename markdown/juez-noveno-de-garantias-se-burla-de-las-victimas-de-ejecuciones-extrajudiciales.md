Title: Juez noveno de garantías "se burla" de las víctimas de ejecuciones extrajudiciales
Date: 2017-03-30 13:54
Category: DDHH, Nacional
Tags: Ejecuciones Extrajudiciales, falsos positivos, madres de soacha, militares
Slug: juez-noveno-de-garantias-se-burla-de-las-victimas-de-ejecuciones-extrajudiciales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Soach.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASOMINGA] 

###### [30 Mar. 2017] 

**Luego de 9 años en los que las madres de Soacha han esperado la audiencia de imputación** contra varios militares acusados por haber cometido ejecuciones extrajudiciales – **“falsos positivos”**, este miércoles el **Juez Noveno de control de garantías de Bogotá declaró que la competencia para estos casos recae ahora sobre la** Jurisdicción Especial para la Paz – **JEP**-, situación que ha dejado a las madres y a los abogados de las víctimas en un limbo ante estos casos.

La determinación se da luego de una solicitud hecha por algunos de los **abogados de los militares en la que piden al juez se declarara con falta de competencias** para llevar el caso dado que estos casos debían ser adelantados bajo la JEP. Le puede interesar:[ Primer General del Ejército será llamado a juicio por "falsos positivos"](https://archivo.contagioradio.com/general-del-ejercito-llamado-a-juicio35257/)

**En el lugar, la Fiscalía, el Ministerio Público y los representantes de las víctimas se opusieron a la solicitud** hecha por la defensa de los militares, relató Pilar Castillo abogada de la Asociación Minga, puesto que **estos casos “no tienen ningún tipo de relación con el conflicto interno armado,** más aun si tenemos en cuenta que por ese mismo caso ya uno de los reclutadores aceptó su responsabilidad en los hechos, diciendo que él entregaba a los jóvenes a miembros del Ejército para que los asesinaran”.

Para la jurista con las pruebas recaudadas durante el proceso, es claro que estos jóvenes fueron engañados y asesinados en total estado de indefensión “**para nosotros esto no se puede catalogar como un delito cometido en el marco del conflicto interno armado”** puntualizó Castillo. Le puede interesar: [Por décima vez aplazan audiencia por "falsos positivos" de Soacha](https://archivo.contagioradio.com/por-decima-vez-aplazan-audiencia-por-falsos-positivos-de-soacha/)

Aún si en la actualidad existieran los tribunales de la JEP, de acuerdo a la reglamentación estos casos si quedarían excluidos por no haberse cometido con ocasión directa o indirecta con el conflicto armado interno.

Además, el Acuerdo Final de Paz, establece que **hasta tanto no entren en vigencia los tribunales la competencia total sigue siendo de la Fiscalía** y los jueces que tengan la atribución para estos casos de “falsos positivos”.

Luego de esta decisión, calificada como muy grave por la abogada, **el caso de las madres de Soacha queda en un limbo judicial**, puesto que “cuando un juez se declara con falta de competencia, lo que tiene que hacer es remitirlo al juez que considera es el competente, pero como en este caso no existen el juzgado lo que hace es remitirlo al Consejo Superior de la Judicatura, institución que solo interviene si hay conflictos entre jurisdicciones y en este momento no los hay” relató Castillo.

**A la salida del Juzgado las madres dejaron ver su descontento, dolor y asombro por lo sucedido** “salieron destrozadas, indignadas, angustiadas, después de 9 años esta era una expectativa muy amplia la que tenían de poder conocer al menos parte de qué fue lo qué pasó, porqué se llevaron a sus hijos. Hay desconcierto, sentimientos encontrados” relató Castillo. Le puede interesar: [Corte Penal Internacional investigaría "falsos positivos" en Colombia](https://archivo.contagioradio.com/corte-penal-internacional-investigaria-falsos-positivos-en-colombia/)

Los delitos que se le imputan a estos 12 militares, incluido el coronel Gabriel de Jesús Amado Rincón, son **homicidio agravado y desaparición forzada** “aquí se pretendía imputar el delito de desaparición forzada que es uno de los delitos más graves, catalogado como de lesa humanidad. Cuando se presenta este tipo de delitos no pueden ir a ningún tipo de Jurisdicción Especial sino a la Justicia Ordinaria” recordó Castillo.

[Comunicado Público MINGA](https://www.scribd.com/document/343545348/Comunicado-Publico-MINGA#from_embed "View Comunicado Público MINGA on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="audio_17867884" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17867884_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="doc_11364" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/343545348/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-6zQqb7CppWEdyleNDnPe&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
