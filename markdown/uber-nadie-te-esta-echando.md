Title: Uber: nadie te está echando
Date: 2020-01-15 13:52
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: aplicaciones, Bogotá, Uber
Slug: uber-nadie-te-esta-echando
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/uber_risk-1200x800-e1507148819593.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"dropCap":true} -->

Profundicemos las razones de un debate que supera nuestras meras sensaciones, hoy muchos se preguntan ¿cómo es posible que el Estado Colombiano saque a Uber? No. Yo creo que está mal planteada la pregunta, que está sesgada, orientada a configurar a Uber como una víctima y no es así. Del mismo modo tampoco se le está diciendo “váyase”. Se le está diciendo quédese, pero como lo que es: una empresa de transporte. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De otra parte, (y esta es una premisa importante) es que no existe conexión lógica directa en el hecho de que solicitarle a Uber que pague impuestos como toda empresa colombiana, signifique que uno esté apoyando un pésimo servicio de algunas empresas colombianas, o incluso, celebrando abusos de todo tipo por parte de algunas empresas colombianas. No. Quienes han propuesto el importante pero superficial debate sobre la calidad del servicio, por encima del modelo económico que marca las pautas de inserción de las multinacionales a nuestro país, son precisamente los portavoces de las multinacionales. En resumen, en primer lugar, salgamos del absurdo de creer que criticar a Uber, es por ejemplo apoyar el robo o el abuso de algunos conductores colombianos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, llamar a Uber “plataforma” o “tecnología” es una tergiversación. Uber es en primer lugar una empresa multinacional estadounidense que se dedica a transportar personas empleando la tecnología para tal fin. El fin de dicha multinacional es trasportar personas, el medio es la tecnología, particularmente sus plataformas o aplicaciones. Por tanto, su razón de ser se potencia o se disminuye debido al desarrollo tecnológico como ocurre en todo negocio o empresa de carácter doméstico o industrial. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entonces llamar a Uber “plataforma tecnológica” ha sido el primer error nominal desde que llegó al país, pues su objetivo no fue venderse como plataforma, sino generar la riqueza a partir del transporte de personas; las personas utilizan Uber para trasportarse, y para lograr ese fin, descargan una aplicación, que vendría siendo el desarrollo tecnológico de la empresa de trasporte. La gente no usa Uber para descargar la aplicación, sino que descarga la aplicación para usar Uber, es decir, para transportarse. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ahora bien, toda empresa que ofrezca cualquier tipo de servicio está sujeta a una franca lid con sus competidores. Si tu trasportas gente, pronto alguien competirá contigo. Hasta allí no veo inconveniente; si apelamos a lo que se denomina derechos del consumidor, uno elige lo que quiere consumir y punto. Por ello el servicio de transporte de Uber fue tan llamativo para los usuarios, pues era otra clase de servicio y el servicio es la punta del iceberg de todo lo que tiene que acontecer legalmente para prestarse. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si otras empresas de transporte prestan pésimos servicios, no van a los lugares de destino y no dan buen trato al cliente, pues sencillamente sin competencia se constituyen en monopolios absurdos, como es el caso de Transmilenio; una empresa privada que se dedica al transporte masivo de personas en Bogotá. Al ser un monopolio, no tenemos más que llamarle en ocasiones “mal necesario”. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero cuando Uber llegó, con un mejor servicio ¿acaso no era coherente que la gente lo prefiriera? ¿aún así costara menos o más? Tú pagas por un buen servicio así sea más costoso, el consumo de una cosa está orientado a un efecto placentero. Si no hay placer no lo consumimos. Si lo consumimos sin placer es porque es una obligación, es decir que se deriva de un monopolio económico.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Vista desde el marco del servicio, la propuesta y victoria de Uber es lícita. Presta un mejor servicio que otras empresas y punto. Pero ¿acaso esto no ocurre en otros gremios, no solo el del transporte sino por ejemplo el de los servicios de comida? Me explico con un ejemplo. Existen dos restaurantes: el primero es desaseado, te tratan mal, la comida la sirven mal, no te llega el plato que ordenaste. El segundo, aunque un poco más costoso, te ofrece un servicio impecable, te trae lo que quieres, sus platos son más elaborados, más modalidades de pago y la atmosfera es limpia, agradable. ¿qué restaurante eliges tu? Claramente, el segundo. El servicio está relacionado teóricamente con el placer que buscamos a la hora de consumir un bien o servicio. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero desde el marco de los requerimientos legales que debe tener una empresa para funcionar, la cosa se pone más compleja. En el ejemplo de los restaurantes, acaso por más de que presten servicios notablemente diferentes ¿ambos no tienen que pagar impuestos, cámara y comercio, retefuentes, IVA, ICA, certificaciones, arriendos, prediales etc.?  ¿entonces no tendría que competir Uber, pero con todas las de la ley? 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Uber no debe irse, aquí nadie lo está echando. Es necesario que se quede por que ha generado dinámicas económicas y de movilidad que jamás habíamos visto. Como empresa de transporte emplea la tecnología de una forma inteligente y eso a la gente le gusta porque le facilita la vida, es decir, le produce placer. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algo que no ha sido leído de esta manera, es que Uber con toda la millonada que cotiza en la bolsa de valores de los Estados Unidos, sea tan injusto y prefiriera no pagar unos impuestos al Estado Colombiano, prefiera de dejar sin trabajo a miles de colombianos, y aparte de todo, echar la culpa a la resta de privilegios que inequitativamente tiene, porque no podemos negar que (por especular un poco) hace 5 años, pagó con lobbies a muchos congresistas para que archivaran el ejercicio legal en su contra, eso lo hacen las multinacionales aquí en todas las partes del mundo.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte las empresas de transporte colombianas tienen que pagar bastantes millones de pesos en impuestos. Este es el punto clave de la discusión, ¿por qué una multinacional de transportes quiere competir transportando gente sin pagar los debidos impuestos, contra empresas colombianas que sí les toca pagar todos los debidos impuestos? Si una empresa presta un mejor servicio que otra no es el meollo del asunto, de hecho, si Uber presta mejor servicio ¿acaso no tiene garantizados a sus clientes? ¿Qué les cuesta a las arcas financieras de Uber, pagar impuestos y trabajar legalmente en Colombia? algunos millones menos, pero nada que le signifique pérdidas.  El problema estructural es que las multinacionales tienen un historial de privilegios no solo en Colombia sino en América Latina que muchos colombianos ignoran o lo ven como “necesario” debido a la ideologización con que se observa el modelo económico, que, aunque vigente, ha demostrado su fracaso en muchos aspectos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, estos son solo comentarios para que se clasifiquen las discusiones frente a multinacionales como Uber, pues incluso en esta columna, no he querido entrar en otro tema complejo; el de las condiciones laborales de los conductores de la empresa Uber, las garantías para sus vehículos ¿qué vinculación tienen? ¿qué garantías tienen? ¿es explotación o es subcontratación? Allí la cosa se pone más densa. Por eso, injusto no fue el juez que demandó a Uber, injusta es la multinacional de transportes Uber, que prefiere irse de Colombia, dejar sin trabajo a la gente en vez de, pudiendo hacerlo, pagar impuestos y trabajar con una clientela que repito, ya tiene asegurada pues evidentemente presta un mejor servicio y se apoya inteligentemente en las plataformas tecnológicas.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver columna[ de opinión de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
