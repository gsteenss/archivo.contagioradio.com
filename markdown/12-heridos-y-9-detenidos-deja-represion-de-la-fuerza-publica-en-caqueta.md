Title: 12 Heridos y 9 detenidos deja represión de la fuerza pública en Caquetá
Date: 2016-08-19 17:07
Category: Ambiente, Nacional
Tags: Caquetá, ESMAD, Esocpetrol, Movilización, petroleo
Slug: 12-heridos-y-9-detenidos-deja-represion-de-la-fuerza-publica-en-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/ESMAD-e1471644079239.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa Rural 

###### [19 Ago 2016] 

El ESMAD nuevamente arremetió este jueves contra la movilización que mantienen los campesinos rechazando actividades de exploración petrolera por parte de la empresa Petroseimic, para **Emerald Energy y Ecopetrol en varios puntos del departamento de Caquetá, donde ya se contabilizan [44 bloques petroleros.](https://archivo.contagioradio.com/43-bloques-petroleros-en-caqueta-amenazan-la-amazonia-colombiana/)**

De acuerdo con Mercedes Mejía, profesora Universidad de la Amazonia y coordinadora de la mesa para la defensa del agua y el territorio de Caquetá, la represión por parte de la fuerza pública ya deja 9 personas detenidas y 12 más heridas en **el municipio de Paujil,** donde los líderes campesinos mantienen una reunión desde tempranas horas de la mañana, con un consejo de seguridad y las autoridades departamentales para buscar soluciones a esta situación.

La fuerza pública atacó la movilización en la noche del jueves, con gases lacrimógenos cuando la población se encontraba durmiendo, lo que **afectó no solo a quienes se manifestaban, sino también a niños, niñas y ancianos.**

Desde el viernes pasado la fuerza pública acompañó a la empresa Petroseimic, para hacer sísmica en Betania. La preocupación aumenta, porque la población se ha venido dando cuenta que la **empresa está realizando la exploración sísmica con un sistema que en tan solo una semana termina la actividad** y por tanto, los daños ambientales aparecerán más rápido.

<iframe src="http://co.ivoox.com/es/player_ej_12601848_2_1.html?data=kpejkpaceJmhhpywj5aUaZS1lZ6ah5yncZOhhpywj5WRaZi3jpWah5ynca7Z08jSxsrXb67Zy87Oh5enb7Hm0MvS1dTWpYzJjKbaw9_Tsoa3lIqupsaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
