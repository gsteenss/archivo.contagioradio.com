Title: Estas serán las próximas actividades del paro de profesores
Date: 2017-06-09 13:43
Category: Educación, Nacional
Tags: fecode, Marchas, profesores, Sindicato de Maestros
Slug: estas-seran-las-proximas-actividades-del-paro-de-profesores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/marcha-de-profesores.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [09 Jun. 2017] 

Ni las inclemencias del clima detienen las movilizaciones que se realizan en Bogotá y en todo el país en el marco del Paro de Maestros que completa su día 29. En la capital, desde muy temprano se dan cita los docentes frente a la Secretaría de Educación, actividad que se llevará a cabo en varias ciudades.

Así mismo, **en diversos municipios se realizarán durante este viernes marchas y caravanas en las principales vías del país**, para mostrar que el paro continúa y el descontento ante la respuesta del Gobierno a las exigencias del Magisterio. Le puede interesar: [Con Jhoana Alarcón ya son 3 profesores asesinados en una semana en Colombia](http://bit.ly/2re5Qzo)

### **Cacerolas y antorchas a las calles** 

**Este 12 de junio sobre las 10 a.m se ha convocado a todos** los docentes, estudiantes y padres de familia a salir en una movilización por las calles de ciudades, municipios y principales capitales para mostrar con un cacerolazo su inconformidad. **Por su parte, el 13 de junio se llevará a cabo una marcha de antorchas.**

### **Todo el país seguirá marchando** 

Barranquilla, la vía Acacías en el departamento del Meta, Castilla La Nueva (Meta), Santa Marta y todo el Tolima son algunos de los lugares que se han movilizado a la par con Bogotá y que han manifestado que seguirán reuniéndose en las principales vías para bloquearlas y llamar la atención gubernamental. Le puede interesar: [Asesinan a profesor Washington Cedeño en Córdoba](https://archivo.contagioradio.com/asesinan-a-profesor-en-cordoba/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
