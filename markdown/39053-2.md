Title: Armados torturan campesina para amenazar a lideresa del Congreso de los Pueblos
Date: 2017-04-11 13:36
Category: DDHH, Entrevistas
Tags: congreso de los pueblos, Conversacioines de paz con el ELN, Cumbre Agraria, Marylen Serna
Slug: 39053-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/marylen-serna-congreso-de-los-pueblos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [11 Abr 2017]

El pasado 7 de Abril, fue agredida, torturada y abusada sexualmente una mujer, del círculo cercano de Marylen Serna, vocera del Congreso de los Pueblos. Según la denuncia publicada este martes, los hechos se produjeron en la ciudad de Popayán cuando la mujer llegaba a la casa de Serna y fue **raptada por 3 hombres armados que la subieron a un carro para agredirla.**

Según la denuncia, la mujer fue raptada y alejada al interior de un vehículo en el que la subieron a la fuerza y luego, para evitar ser reconocidos pusieron una bolsa negra en su cabeza. Durante el trayecto fue golpeada e insultada, **además le inyectaron una sustancia desconocida para luego abusar sexualmente de ella.** ([Lea también: Asesinada lideresa de ASOKINCHAS en Medellín](https://archivo.contagioradio.com/asesinada-lideresa-asokinchas-medellin/))

Luego de abandonarla los armados se comunicaron con Marylen Serna para ratificar que se trataba de una persona cercana al círculo familiar de la lideresa del Congreso de los Pueblos. Según lo manifestó la propia Marylen, esta situación se reafirma como una **amenaza directa relacionada con la labor social que desarrolla, tanto en el Congreso de los Pueblos como en el espacio de la Cumbre Agraria.**

El comunicado de la organización hace énfasis en que el hecho tiene relación directa con el trabajo organizativo de Serna “Este acto de violencia de género es muy deplorable, pues se ha agredido a una mujer **con el objetivo de amenazar e intimidar, hostigar, torturar psicológicamente y disuadir de su labor a otra mujer** que encarna una lucha por la paz y la defensa de los derechos humanos en Colombia”.

Marylen Serna explicó que aunque los hechos ocurrieron el 7 de abril, solamente se logró concertar un plan de acción con la familia de la mujer víctima hasta este lunes y por ello se tomó la decisión de hacer la denuncia pública. Sin embargo, hasta el momento **no hay medidas del Estado o de las fuerzas de seguridad que garanticen las condiciones mínimas** para que no se presente una nueva agresión contra la mujer o contra la lideresa.

### **Se agudiza persecución contra el Congreso de los Pueblos** 

Por otra parte el Congreso de los Pueblos señala que este hecho se enmarca dentro de una serie de agresiones que se han presentado en los últimos días, como la judicialización de varios líderes en el Sur de Bolívar y los asesinatos de varios de los integrantes en diversas regiones del país. ([Lea también: Capturados 3 integrantes del Congreso de los Pueblos)](https://archivo.contagioradio.com/capturas-congreso-de-los-pueblos/)

Por ello hacen un llamado al Estado a que estos crímenes no queden en la impunidad y cese la persecución judicial y paramilitar contra las personas que están comprometidas con la construcción de la paz en el país.

[En Grave Riesgo La Vida de Marylen Serna Salinas.pdf](https://www.scribd.com/document/344885472/En-Grave-Riesgo-La-Vida-de-Marylen-Serna-Salinas-pdf#from_embed "View En Grave Riesgo La Vida de Marylen Serna Salinas.pdf on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_70569" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/344885472/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-5A2XQFReYwLqQa8diZBQ&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe><iframe id="audio_18094891" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18094891_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU).]{.s1}
