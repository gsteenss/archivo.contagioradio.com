Title: Primer ministro israelí investigado por corrupción
Date: 2017-08-17 10:00
Category: Onda Palestina
Tags: BDS, Benjamín Netanyahu, Israel, Palestina
Slug: primer-ministro-investigado-corrupcion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Benjamin.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Emil Salman 

###### 17 Ago 2017

Un ex alto asesor del primer ministro Benjamín Netanyahu informará de **dos casos de corrupción en los que el premier israelí ha sido interrogado como sospechoso**, según aseguró una fuente del Ministerio de Justicia el viernes.

La decisión de **Ari Harow**, quien se desempeñó como jefe de gabinete de Netanyahu antes de renunciar en medio de acusaciones de improcedencia en 2015, **representa uno de los desarrollos más importantes en la investigación** contra el Primer Ministro, que lleva muchos meses en curso.

La policía confirmó que **Netanyahu es sospechoso de soborno, fraude y abuso de confianza en dos casos**, uno en el que se alegaba que el primer ministro y su familia recibieron donaciones de benefactores ricos y otro que involucra acusaciones de que trató de negociar un acuerdo que habría resultado cobertura favorable para el primer ministro en uno de los periódicos más grandes de Israel.

Netanyahu es despreciado por gran parte de la izquierda israelí ya que **con él la extrema derecha ha cobrado mayor fuerza en este país**, lo que ha permitido la implementación de políticas como la expansión de colonias ilegales en Cisjordania.

Es por esto que gran parte de la izquierda ha celebrado lo que **se perfila como una inminente caída del primer ministro**, sin embargo otros afirman que lo que se necesita realmente es una transformación de fondo de la sociedad israelí. Como escribe Gideon Levy para el periódico Hareetz, "Como populista sofisticado, Netanyahu no moldeó el espíritu del pueblo, lo reflejó".

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
