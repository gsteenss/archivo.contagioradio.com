Title: En 2015 Colombia compró 850 mil Euros en software de Hacking Team
Date: 2015-07-13 16:33
Author: AdminContagio
Category: Entrevistas, Tecnología
Tags: Andromeda, das, DIPOL, Dirección de Inteligencia Policial, Fundaicón Carisma, Hacking Team, Ley Lleras, Lista negra, Nice, operaciones de inteligencia, Robotech
Slug: en-2015-colombia-compro-850-mil-euros-en-software-de-hacking-team
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/hacking-team-el-negocio-del-espionaje-y-sus-conexiones-gubernamentales-al-descubierto-1436290705.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: vice.com 

<iframe src="http://www.ivoox.com/player_ek_4783732_2_1.html?data=lZyllZyXdo6ZmKialJqJd6KompKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjMLXzM7byZC4qcLhjMrbjajTsNDhw87Oj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Pilar Sáenz, Fundación Carisma] 

###### [13, Julio , 2015]

**El gobierno colombiano, ha adquirido servicios de software para espionaje de "Hacking Team",** compañía que en los últimos días ha sido seriamente cuestionada tras ser hackeada poniendo en evidencia que vendía sus servicios a países señalados de violar derechos humanos.

Desde 2011 Colombia tomó la decisión de hacer un cambio en su tecnología relacionada con ciberseguridad y defensa, por los problemas del DAS y la Ley Lleras. Fue así como en 2013 el **gobierno compró servicios de Hacking Team a través de la empresa "Robotec" por 335 mil euros,** y este año adquirió el mismo software pero a través de "**Nice", empresa israelí, por 850 mil euros.**

De acuerdo con Pilar Sáenz, quien hace parte de la Fundación Karisma, organización experta en nuevas tecnologías, derecho y sociedad, cuando se buscaron los recursos para hacer esa compra de software, ** no se plantearon controles para hacer un seguimiento a la utilización de los programas,** es decir, que hay pocas medidas que garanticen que los equipos se usan adecuadamente.

Según Sáenz, el único control que se tiene en el país es una **comisión del Congreso del la República, que solo se ha reunido una vez** por el escándalo de la operación Andromeda a inicios del 2014. La experta en tecnologías de la información, asegura que esta comisión **“no tiene dientes, no se reúnen y no supervisan”** y además no hay ningún tipo de informe de transparencia de las entidades que usan esta tecnología.

El problema entonces, radica en que no se sabe cómo se están realizando los procedimientos legales para que se avale la interceptación de computadores y celulares, que operen con **Windows, Mac, Linux, Android, Iphone y Black Berry.**

**La  Dirección de Inteligencia Policial, DIPOL, es una de las instituciones que usa este software**, algo que es completamente legal y que además le compete, sin embargo, si no sigue  el procedimiento necesario para empezar una labor de inteligencia, habría abuso por parte del sistema de vigilancia y se estaría violando el derecho a la privacidad.

Además, cabe recordar que a través del programa no solo se conoce la información que haya en el aparato, sino que también puede **activar la cámara o incluso implantar información en el dispositivo.**

Hacking Team es una compañía italiana que comercializa software para interceptar celulares o computadores. Aunque públicamente la empresa aseguraba que **trabajaba legalmente, se comprobó que tenían relación con 35 naciones,** entre las que había países que hacen parte de la lista negra, por ser acusados de graves violaciones a derechos humanos.
