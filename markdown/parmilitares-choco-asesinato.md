Title: Paramilitares asesinan a dos pobladores del Salaquí en el Bajo Atrato
Date: 2017-01-10 12:25
Category: DDHH, Nacional
Tags: Chocó, paramilitares, Salaquí
Slug: parmilitares-choco-asesinato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Caminata-por-la-paz-19.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Archivo Contagio Radio 

##### 10 Ene 2017 

El pasado 9 de Enero en horas de la tarde, la comunidad de Salaquí informó que **paramilitares identificados como de las Autodefensas Gaitanistas de Colombia asesinaron a Moises y Chogoló Mosquera, padre e hijo**. También denunciaron los habitantes de esta región del Bajo Atrato Chocoano que **la movilidad paramilitar es constante y permanente desde hace varios meses**, razón por la cual el temor se incrementa.

Según la denuncia, el hecho se produjo cuando los paramilitares que hacen presencia en cercanías a la comunidad El Tamboral **abordaron a uno de los integrantes del consejo comunitario y con la acusación de ser miliciano lo golpearon hasta la muerte**. Su padre, reclamó a los hombres armados por la acción y fue ultimado a disparos por los mismos integrantes de las AGC.

Tanto la comunidad de la cuenca del Río Salaquí, como las comunidades de la cuenca del Río Cacarica en el Bajo Atrato Chocoano, vienen denunciando el **crecimiento de la presencia por parte de paramilitares**, así como las **constantes amenazas de ingresar las Zonas Humanitarias** ubicadas en las comunidades de Nueva Vida y Nueva Esperanza en la cuenca del Cacarica. [Le puede interesar: 300 neoparamilitares transitan por territorios en Chocó sin presencia del Estado](https://archivo.contagioradio.com/300-neoparamilitares-transitan-por-territorios-en-choco-sin-respuesta-del-estado/)

Estas acciones han sido denunciadas desde 2015 **sin que las FFMM realicen algún tipo de operación de combate a esas estructuras o de protección de las comunidades** más vulberables y en mayor nivel de riesgo. Incluso, organizaciones como la Comisión de Justicia y Paz han denunciado que **las operaciones militares posibilitan la presencia y operación de los grupos paramilitares**.

Hasta el momento se desconoce si hay o no acciones por parte de las FFMM o de policía para establecer un plan de acción que permita la captura de los responsables y el combate efectivo a las estructuras paramilitares que operan en la región. Por su parte las comunidades han afirmado que **persistirán en las denuncias hasta ser escuchados** pues no quieren repetir una situación de desplazamiento forzado como el ocurrido en 1997 en desarrollo de la operación Génesis.
