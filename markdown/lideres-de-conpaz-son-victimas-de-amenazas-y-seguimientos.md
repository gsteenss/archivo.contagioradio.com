Title: Líderes de CONPAZ son víctimas de amenazas y seguimientos
Date: 2016-08-09 20:52
Category: DDHH, Nacional
Tags: Antioquia, Cauca, Conpaz, paz, Tubo, víctimas
Slug: lideres-de-conpaz-son-victimas-de-amenazas-y-seguimientos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/conpaz_4_contagioradio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [9 Ago 2016] 

Luz Marina Cuchumbe y Marco Velázquez integrantes de la Red de Comunidades Construyendo Paz en los Territorios vienen siendo víctimas de amenazas por parte de grupos paramilitares del departamento del **Cauca y Antioquia.**

De acuerdo con la denuncia de la Comisión de Justicia y Paz, por una parte**, el pasado 4 de agosto a las 7:30 de la mañana, Luz Marina,** una de las delegadas de las 60 víctimas a La Habana, fue objeto de seguimientos durante varias ocasiones por parte de hombres vestidos de civil, mientras ella realizaba sus actividades diarias en su ida a su parcela  de trabajo en San Vicente, municipio de Inzá Cauca y luego nuevamente fue seguida cuando salía de allí.

Una situación que se da, **en el marco del inicio de reclutamientos  por parte de la estructura paramilitar Los Gaitanistas,** quienes además, semanas realizaron llamadas amenazantes hacia a líderes comunitarios de la zona, donde se ha evidenciado la presencia de hombres desconocidos como lo ha notado la comunidad, desde que en abril al departamento del Huila y Cauca han llegado integrantes de grupos paramilitares.

Por su parte, el pasado jueves 7 de julio, sobre las 5 de la tarde Marco Velázquez, denunció que mientras se encontraba e**n el municipio de Turbo, en Antioquia, dos paramilitares que se movilizaban en una motocicleta de alto cilindraje, marca Suzuki**, con armas cortas en la cintura, abordaron al líder afrodescendiente, diciéndole: "ya no te acordás de mi?, ustedes nos humillaban en el asentamiento, hoy no te vamos a hacer nada, ahora que entremos allá, vamos a coger 3 o 4 líderes para que el pueblo los sienta".

Cabe recordar, que estas situaciones se suman a una serie de amenazas y  extorsiones contra dos líderes ambientalistas miembros de la Red CONPAZ, Ricardo Barragán y Nicodemus Sánchez.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
