Title: Asesinan a Luis Gabriel Gómez, rector de colegio en Puerto Asís, Putumayo
Date: 2018-07-25 11:44
Category: DDHH, Nacional
Tags: Docente, Luis Gabriel Gómez, Puerto Asís, Putumayo
Slug: asesinan-luis-gabriel-gomez-rector-de-colegio-en-puerto-asis-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/luis-gabriel-gomez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASEP] 

###### [25 Jul 2018] 

Docentes del departamento de Putumayo rechazaron el asesinato de **Luis Gabriel Gómez, rector de la institución educativa rural Bajo Lorenzo, en Puerto Asís**. Los hechos ocurrieron cuando el docente salía de Mocoa, en la madrugada hacia su puesto de trabajo, luego de haber realizado gestiones para mejorar el colegio.

La Asociación de Educadores de Putumayo y la Asociación de Directores y Rectores de este departamento, repudiaron las acciones violentas contra el docente y exigieron a la Policía, a la Fiscalía General de la Nación y al Gobierno, el esclarecimiento de este crimen y el castigo a los responsables. (Le puede interesar: ["Dos líderes sociales asesinados durante el 20 de julio"](https://archivo.contagioradio.com/lideres-asesinados-el-20-de-julio/))

De igual forma pidieron que se garantice la labor de los docentes en el departamento y manifestaron **la necesidad de acabar con los asesinatos a defensores de derechos humanos, líderes sociales y docentes que se han venido registrando este año**.

Noticia en desarrollo...
