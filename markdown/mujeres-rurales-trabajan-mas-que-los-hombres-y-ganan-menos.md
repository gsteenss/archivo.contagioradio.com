Title: Mujeres rurales trabajan más que los hombres y ganan menos
Date: 2017-01-24 15:53
Category: El mundo, Mujer
Tags: desigualdad, genero, mujeres, mujeres rurales
Slug: mujeres-rurales-trabajan-mas-que-los-hombres-y-ganan-menos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/trabajadora.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Aldea 42] 

###### [23 Ene. 2017]

**Los salarios insuficientes, las malas condiciones para ejercer su trabajo y la desigualdad en el acceso a la tierra son tan solo algunas de las realidades con las que viven cerca del 43% de las mujeres rurales,** así lo aseguró el Programa para las Naciones Unidas (PNUD) en su reporte para el desarrollo.

De manera adicional, manifiestan que las mujeres tienen en menor porcentaje tierras, es decir, existen menos mujeres dueñas de territorios en el mundo, dice el PNUD que **en 8 países del mundo “las mujeres casadas no poseen los mismos derechos que los hombres en el momento de acceder a la tierra”.**

En el caso de Colombia, según el Censo Nacional Agropecuario se continúa evidenciando la desigualdad entre géneros en el campo, teniendo en cuenta que la adjudicación de tierras desfavorece a las mujeres en el acceso a baldíos y a subsidios integrales de tierra, pues el 60% se asigna a los hombres y 40% a mujeres.

De igual manera, el PNUD contiúa haciendo énfasis en que **las mujeres ganan 24% menos que los hombres y tan solo el 25% de ellas logran mantener posiciones administrativas y gerenciales** en sus lugares de trabajo. Le puede interesar: [Solo el 24% de las mujeres rurales pueden tomar decisiones sobre sus tierras](https://archivo.contagioradio.com/solo-el-24-de-las-mujeres-rurales-pueden-tomar-decisiones-sobre-sus-tierras/)

Estas condiciones ya han sido denunciadas por organizaciones de mujeres y feministas quienes continúan trabajando por equidad e igualdad no solo en espacios como el laboral, sino también de acceso a cargos públicos y de representación.

La Corporación Viva la Ciudadanía y el Colectivo de Abogados José Alvear Restrepo presentaron en 2016 uno de los resultados de la Encuesta realizada por el Departamento Administrativo Nacional de Estadística (DANE) sobre los usos del tiempo, en el que se evidenció que** las mujeres dedican 3 veces más tiempo en labores del hogar, el cuidado de menores y de la tercera edad que los hombres, promedio que aumenta a 4 en las zonas rurales.  **

En el mundo, cerca de 500 millones de granjas trabajan a diario para producir el 80% de la comida. Le puede interesar: [El trabajo de las mujeres en Colombia](https://archivo.contagioradio.com/trabajo-las-mujeres-colombia/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}
