Title: Comunidades del litoral del Bajo San Juan retornan a su territorio tras desplazamiento forzado
Date: 2018-03-09 12:18
Category: DDHH, Nacional
Tags: Bajo San Juan, comunidades litoral bajo san juan, Desplazamiento, retorno de comunidades
Slug: comunidades-del-litoral-del-bajo-san-juan-retornan-a-su-territorio-tras-desplazamiento-forzado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/litoral-bajo-san-juan-e1520614657240.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [09 Mar 2018] 

Las comunidades del litoral del Bajo San Juan, en el Chocó, se encuentran realizando el retorno a su territorio, luego de haber sido desplazados forzosamente **por enfrentamientos entre grupos paramilitares y el ELN**. Ellos y ellas iban a realizar el retorno el 15 de febrero de 2017 sin embargo, fueron amenazados y debieron quedarse en condiciones deplorables en el coliseo de Buenaventura.

De acuerdo con Dagoberto Pretelt, líder social y defensor de derechos humanos de esa comunidad, **“hoy salimos desde las 6:00 de la mañana** rumbo a la vereda del Bajo Calima para llegar al corregimiento de cabecera del Bajo San Juan”. Las familias iniciaron el retorno en lanchas para llegar al territorio colectivo de Cabecera.

### **26 de 33 familias iniciaron el retorno** 

De la totalidad de las familias que fueron desplazadas, 26 de ellas tomaron la decisión de volver a su territorio. Este proceso pudo realizarse luego de que se produjera **la firma de un acta** con diferentes autoridades locales y nacionales quienes realizaron lo correspondiente para el retorno seguro de estas personas.

Pretel manifestó que hasta el momento **se ha cumplido con los protocolos** para garantizar el retorno y “la administración municipal realizó un compromiso para adecuar las viviendas donde va a retornar la población”. Indicó que se están mejorando las casas a la vez que se está implementando un plan de seguridad alimentaria.

Ante esto enfatizó que la Unidad de Víctimas “se comprometió en acompañar y apoyar a la comunidad **con planes de alimentación** hasta tanto los cultivos que está apoyando la administración estén dando sus frutos”. (Le puede interesar:["Líder social y comunitario del litoral del Bajo San Juan amenazado por retornar a su territorio"](https://archivo.contagioradio.com/lider-social-y-comunidad-del-litoral-bajo-san-juan-amenazadas-por-retornar-a-su-territorio/))

### **Comunidades** [**establecerán**]** zona humanitaria como medida de auto protección** 

Frente al tema de las garantías de seguridad que tiene la comunidad para no volver a sufrir un desplazamiento o hechos violentos en su contra, el defensor indicó que la Fuerza Pública se comprometió **a brindar un acompañamiento** para las personas “no directamente en el territorio sino realizando rondas perimetrales”.

Como medida de auto protección, las comunidades van a retomar la experiencia de otras comunidades para crear una **zona humanitaria** teniendo en cuenta que “esto ha servido como medida de protección para las comunidades”. Esto implica que ningún actor armado, incluida la Fuerza Pública, puede ingresar en este espacio y es una figura que “por ley se debe respetar”. (Le puede interesar:["3.000 personas se encuentran confinadas en el litoral del Bajo San Juan"](https://archivo.contagioradio.com/3-000-personas-se-encuentran-confinadas-en-el-bajo-san-juan-litoral/))

Finalmente, Pretelt manifestó que las comunidades **“están muy contentas con la ilusión de volver nuevamente a sus tierras”**. Indicó que hay una nueva posibilidad de vivir dignamente, para él “es un día simbólico” porque significa “volver a nacer nuevamente” en un territorio que esperan sea libre de conflicto armado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
