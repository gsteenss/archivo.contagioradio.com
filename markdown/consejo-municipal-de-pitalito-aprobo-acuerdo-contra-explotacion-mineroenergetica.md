Title: Pitalito aprobó acuerdo contra explotación mineroenergética
Date: 2016-11-30 13:36
Category: Ambiente, DDHH
Tags: efectos de la explotación de petróleo, Hidroeléctricas, Protección al Ambiente
Slug: consejo-municipal-de-pitalito-aprobo-acuerdo-contra-explotacion-mineroenergetica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FOTOS-DEL-MUNICIPIO-DE-PITALITO-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Guía turistica del Huila] 

###### [30 Nov 2016]

Consejo Municipal de Pitalito – Huila **aprobó el Proyecto de Acuerdo para la protección de los recursos naturales frente a la explotación mineroenergética**,  acción que prohíbe en ese territorio, la construcción de represas, exploración y explotación de petróleo y el desarrollo de proyectos de  gran minería. Esta decisión  hace parte de un conjunto de medidas para la protección de los recursos naturales que venían promoviendo organizaciones como Asoquimbo.

**En el fallo se prohíbe puntualmente:** la construcción de hidroeléctricas en la jurisdicción del municipio de Pitalito, la minería a cielo abierto, subterránea y por solución a gran escala y la exploración o explotación petrolera convencional y no convencional.

Además se en el texto se hacen salvaguardas como que **se permite la minería a pequeña y mediana escala** y establece que se impulse la vigilancia, control y formalización de las empresas dedicadas a las actividades que se mencionan anteriormente. Le puede interesar: ["Anglogold Ashanti entutelo consulta populr en Cajamarca"](https://archivo.contagioradio.com/anglogold-ashanti-entutelo-consulta-popular-en-cajamarca/)

Para hacer un poco más de pedagogía en términos de este nuevo fallo, organizaciones como Asoquimbo, en coordinación con el Movimiento Ciudadano por la Defensa del Territorio, ofrecerán el martes 6 de diciembre, en Pitalito, en el Centro Cultural, una **capacitación sobre competencias de los entes territoriales y ordenamiento territorial.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
