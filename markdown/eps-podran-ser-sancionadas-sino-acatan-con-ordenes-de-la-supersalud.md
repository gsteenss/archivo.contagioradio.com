Title: EPS podrán ser sancionadas si no acatan órdenes de la Supersalud
Date: 2016-07-18 19:56
Category: DDHH, Nacional
Tags: crisis de la salud, EPS, Juan Manuel Santos, POS, Salud
Slug: eps-podran-ser-sancionadas-sino-acatan-con-ordenes-de-la-supersalud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/crisis-salud-e1468889628523.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: primicidiario 

###### [18 Jul 2016] 

Ahora las colombianas y los colombianos cuentan con una nueva herramienta para exigir que las EPS cumplan con las decisiones tomadas desde la Superintendecia de Salud para garantizar este derecho. Se trata de  **Ley de Fortalecimiento Institucional** del sector salud o la **Ley 1797.**

La nueva Ley permite que, **con arresto a la persona o entidad  hasta por seis meses y multas de hasta de 20 salarios mínimos mensuales,** las EPS sean sancionadas por desacato a las órdenes de la Superintendencia de Salud. Es así como se busca terminar con los constantes incumplimientos de las EPS, que pese a que cuentan con una orden por parte de la Supersalud,  siguen dilatando y poniendo en riesgo la salud de los colombianos.

Esta Ley, firmada por el presidente Juan Manuel Santos el pasado 13 de julio, **consta de 28 artículos y tiene como objetivo establecer nuevas posibilidades para financiar servicios no incluidos en el plan de beneficios** (no POS), fortalecer el saneamiento contable y además busca generar mayor liquidez para la salud.

Otros de los puntos importantes de Ley son: el fortalecimiento de los canales de comunicación con la ciudadanía para recibir y resolver quejas y reclamos. Así mismo, habilita a departamentos y distritos para que utilicen sus excedentes de rentas cedidas para pagar deudas de servicios no **POS** en el régimen subsidiado, que hoy ascienden a 800.000 millones de pesos en todo el país.

De esta manera se espera mejorar la calidad del servicio a los pacientes que están esperando medicamentos, ya que permite que **el Ministerio de Salud gire recursos directamente a proveedores del régimen subsidiado,** pues se saltan varios escalones de la cadena  de pago tales como el Fosyga, EPS, IPS y los proveedores de medicamentos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
