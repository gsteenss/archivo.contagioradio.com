Title: Netanyahu busca acercamientos con Colombia tras boicot en Europa
Date: 2017-09-14 17:32
Category: El mundo, Política
Tags: Boicot a Israel, colombia, Israel, Palestina
Slug: netanyahu-busca-acercamientos-con-colombia-tras-boicot-en-europa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Santos-y-Netanyahu-e1505428077206.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Canal de youtube Israeli PM] 

###### [14 Sept 2017] 

Tras la visita a Colombia del primer ministro israelí Benjamin Netanyahu, la comunidad de Palestina en Bogotá indicó que ese país **ha buscado acercamientos con América Latina tras boicot comercial en Europa y **que la visita pudo estar encaminada al fortalecimiento de la venta de armamento en el continente.

De acuerdo con Ali Nofal, vocero de la Comunidad Palestina en Bogotá, **los países de Europa han venido realizando una campaña de boicot** contra la empresas y los productos israelíes para defender y reconocer a los palestinos como pueblo independiente. Ante esto, han tenido que buscar apoyo comercial en lugares como América Latina.

Dijo que esta es la razón por la cual la vista de Netanyahu fue tan rápida y **dirigida hacia el sector comercial y empresarial.** Recordó que Israel es uno de los mayores comercializadores de armas del mundo y que la venta de armamento es un tema que viene a concretar Netanyahu en los diferentes países de América Latina. (Le puede interesar: ["Cancelado encuentro entre países africanos e Israel por presiones pro palestinas"](https://archivo.contagioradio.com/))

De igual forma, fue enfático en establecer que Colombia se ha caracterizado por **negar e ignorar las resoluciones de las Naciones Unidas** e históricamente “los gobernantes de Colombia no han querido reconocer y demostrar su apoyo con nosotros.”

Dijo que a pesar de que Palestina ha fortalecido su relación con el país con la consolidación la misión diplomática, “Colombia es el patio trasero en América Latina de los intereses de Estados Unidos e Israel”.

**TLC con Israel**

En 2013, **Colombia e Israel firmaron el acuerdo comercial** que reduce los aranceles en productos de intercambio comercial y establece mecanismos de cooperación, inversión e intercambios técnicos. De igual manera, fomenta el intercambio de bienes y servicios en el sector tecnológico y especialmente en el sector agroindustrial. (Le puede interesar: ["Ronaldo, un cortometraje que plasma la realidad de la niñez en Palestina"](https://archivo.contagioradio.com/))

Esto ha sido fuertemente cuestionado por varios sectores que han manifestado que los TLC que ha impulsado Colombia con diferentes países, **perjudican la capacidad de producción** y comercialización de productos especialmente de los campesinos.

Finalmente, el vocero indicó que **es necesario que haya una presión interna y externa** para que Colombia reconozca el derecho que tiene este pueblo a ser independiente como Estado. Dijo que la actitud de Colombia es irresponsable más aun habiendo creado los acuerdos para una paz estable y duradera.

<iframe id="audio_20886682" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20886682_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
