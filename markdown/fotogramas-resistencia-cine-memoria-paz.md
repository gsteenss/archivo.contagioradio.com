Title: Fotogramas en resistencia, un festival de cine por la memoria y la paz
Date: 2018-10-25 20:15
Author: AdminContagio
Category: 24 Cuadros
Tags: Festival cine, Independencia Records, paz
Slug: fotogramas-resistencia-cine-memoria-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/descarga-11.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Independencia Records 

###### 25 Oct 2018 

Como reconocimiento de la creación audiovisual en el rescate de la memoria, la construcción de paz y de un nuevo país, la plataforma de gestión cultural **Independencia Récords, realizará el primer Festival de Cine Independencia: "Fotogramas en Resistencia".**

Fotogramas de Resistencia se configura como una muestra de **múltiples miradas acerca de memoria, la verdad y el futuro de nuestro país de cara a la construcción de paz con justicia social**, la implementación y defensa de los Acuerdos de La Habana.

Para participar en la convocatoria, **estudiantes, realizadores, excombatientes, victimas y gente del común, pueden enviar sus obras finalizadas** en una las siguientes categorías:

**Filminutos:** se recibirán obras terminadas de máximo 1 minuto de duración, grabada en cualquier formato y cuya temática debe ser la lucha por la Paz.

**Cortometrajes:** las obras en esta categoría no deben superar los 30 minutos de duración, pueden ser cortometrajes de ficción o documentales cuya temática sea la Lucha del pueblo Colombiano, la paz o la reconciliación.

**Animación:** Se recibirán obras en la categoría de animación sin restricciones en duración, obras de ficción, documental o video clip cuya técnica principal sea la animación (puede contener elementos fotográficos) y la temática sean los Acuerdos de la Habana y la lucha por la paz.

**Largometraje:** Se recibirán largometrajes para la muestra oficial del festival, de ficción o documentales cuya temática sea la construcción de paz en Colombia.

Todas las obras presentadas deben contar en alguna etapa de la producción y/o postproducción, **con la participación de al menos 1 excombatiente o 1 víctima del conflicto armado**, debidamente acreditada.

Las inscripciones serán recibidas a partir **del Jueves 25 de Octubre, hasta el Domingo 25 de Noviembre**, y se premiara al ganador con \$500.000 y un bono para compra de equipos de realización audiovisual, en el cierre del evento que tendrá lugar durante el Primer **Independenzia Fezt**.

**Sobre Independencia Récords**

Ante los escasos escenarios de participación y muestra de propuestas alternativas, críticas y que le apuesten a la paz, esta organización trabaja por la apertura a un espacio de autogestión, creatividad y resistencia. Incidiendo en la promoción, y difusión de obras artísticas, creativas y políticas que desde la imagen contribuyan a paz y la reconciliación.
