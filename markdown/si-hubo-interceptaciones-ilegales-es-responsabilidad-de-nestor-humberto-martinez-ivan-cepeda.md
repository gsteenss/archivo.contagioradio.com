Title: Si hubo interceptaciones ilegales es responsabilidad de Néstor Humberto Martínez: Iván Cepeda
Date: 2019-12-02 17:44
Author: CtgAdm
Category: Política
Tags: chuzadas, Fiscalía, interceptaciones ilegales, Iván Cepeda, Nestor Humberto Martínez
Slug: si-hubo-interceptaciones-ilegales-es-responsabilidad-de-nestor-humberto-martinez-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Iván-Cepeda-denuncia-a-Néstor-Humberto-Martínez-por-chuzadas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio.] 

[El senador Iván Cepeda decidió presentar una denuncia penal contra el exfiscal General de la Nación, Néstor Humberto Neira, tras enterarse de las declaraciones presentadas por los periodistas de la Revista Semana, María Jimena Duzán y Daniel Coronell, en las que denuncian que se habrían presentado interceptaciones o “chuzadas” ilegales a varias personas del equipo negociador del Acuerdo de Paz.]

[“Dadas las denuncias que han aparecido en la Revista Semana sobre lo que habrían sido acciones ilegales del ex Fiscal General de la Nación, Néstor Humberto Neira, he decidido presentar tanto en la Fiscalía como en la Comisión de Investigación y Acusación una denuncia penal en su contra, y la solicitud ante la Fiscalía General de ser reconocido en condición de víctima”. ]

[El congresista rechazó las declaraciones del Fiscal encargado, Fabio Espitia, quien se defendió explicando que, al revisar las bases de datos, no se encontraron los datos de contacto del senador Cepeda, dejando por hecho que nunca estuvieron allí.][(Le puede interesar: Las falacias del fiscal Néstor Humberto Martínez para objetar la JEP).](https://archivo.contagioradio.com/falacias-para-objetar-jep/)

[Ante ello, el parlamentario señaló que “manipular un listado es absolutamente posible. Ya lo hemos visto, se han manipulado las bases de datos en las salas de interceptación. Todos estos hechos serían muy graves de corroborar, porque implicarían una confabulación contra el Proceso de Paz y su implementación, siendo un ataque soterrado del señor Martínez”. ]

### **El senador Iván Cepeda ya había sido señalado en la Fiscalía de Néstor Humberto Martínez** 

[Como facilitador de los diálogos de Paz entre el Gobierno Nacional y las FARC-EP, cumpliendo el mismo papel con los diálogos con el  ELN y siendo facilitador para el sometimiento a la justicia del llamado “Clan del Golfo”, el senador Iván Cepeda fue cuestionado en su momento por el entonces fiscal, Néstor Humberto Neira, al insinuar que el congresista había utilizado la Ley Estatutaria de la JEP para introducir un “articulejo que favorecería a organizaciones del narcotráfico”.]

[Tras estos señalamientos, el 26 de abril de 2019 el congresista dirigió un derecho de petición al ex Fiscal preguntando si había algún proceso en su contra, encontrando una respuesta negativa por parte del ex Fiscal General y calificando aquella acusación como “falaz” y parte de una “campaña difamatoria” en su contra. ]

### **La situación es de “extrema gravedad”** 

[Fabio Augusto Martínez Lugo y Luis Carlos Gómez Góngora, dos ex funcionarios de la Fiscalía, recibían órdenes del ex Fiscal General “en términos de realizar seguimientos ilegales e interceptaciones contra algunas personas involucradas en el Proceso de Paz. Estos exfuncionarios dan datos precisos, hablan de circunstancias, modo, tiempo y lugar de cómo fueron ordenadas esas interceptaciones y están prestos para colaborar con la justicia” afirmó el senador". ]

[Las interceptaciones ilegales se “habrían llevado a cabo sin orden judicial” y corresponden a los delitos de “violación ilícita de comunicaciones, interceptación de datos informáticos, violación de datos personales, concierto para delinquir, y abuso de autoridad por acto arbitrario e injusto”. ]

[Lo que preocupa al senador es la escasa credibilidad de las instituciones del Estado, ya que, como él mismo menciona:  ¿en dónde queda la fiabilidad de las instituciones? Si es el organismo que investiga el que está realizando acciones ilegales, ¿quién va investigar a ese organismo? Esa es una circunstancia de extrema gravedad y que conduciría a una serie de conclusiones muy inquietantes”. "Una investigación sobre las chuzadas podría conducir a conclusiones muy inquietantes: Iván Cepeda]

[Mientras se esperan declaraciones oficiales de la Fiscalía, el congresista resaltó la necesidad de llevar a cabo una investigación seria que incluya a todos los interceptados en calidad de víctimas. “Esa investigación debe ser rigurosa y exenta de cualquier sombra de parcialidad. Si al cabo de un tiempo se verifica que no hay ningún tipo de investigación, habrá que proceder a elevar esto a organismos internacionales”.]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45280650" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45280650_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]

</iframe>
