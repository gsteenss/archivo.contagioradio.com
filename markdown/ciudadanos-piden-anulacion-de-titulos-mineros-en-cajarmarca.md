Title: Ciudadanos piden anulación de títulos mineros otorgados en Cajamarca
Date: 2019-06-06 17:52
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Anglogold Ashanti
Slug: ciudadanos-piden-anulacion-de-titulos-mineros-en-cajarmarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/cajamarca-defensores.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

La Corporación Cajamarca Dispensa Hídrica y Agrícola **presentó una demanda al Tribunal Administrativo de Cundinamarca** en contra de la multinacional AngloGold Ashanti y la Agencia Nacional de Minería (ANM), con el objetivo de **anular tres títulos mineros otorgados a esta empresa para la exploración y explotación de oro** en este municipio del Tolima.

Según el comunicado, el grupo ambiental argumenta que dichos contratos de concesión **"violan el mandato superior" que los ciudadanos del municipio aprobaron el 26 de marzo de 2017 a través de una consulta popular**, en contra de a realización de actividades mineras en el territorio. (Le puede interesar: "[Amenazan promotores de consulta popular en Cajamarca, Tolima](https://archivo.contagioradio.com/amenazan-promotores-de-consulta-popular-en-cajamarca-tolima/)")

[Robinson Mejía, integrante de Colectivo Socio-ambiental Juvenil de Cajamarca, afirmó que **tres de los 16 títulos mineros otorgados en el 2007 a la multinacional se mantienen vigentes.** Sin embargo, tales títulos comprometen **el 30% del territorio municipal, incluso zonas de páramo**, lo que según los grupos ambientales, serían suficientes para construir la mina de gran escala La Colosa.]

A pesar de los llamados de los cajamarcunos, la empresa ha expresado que no desistirá de los títulos hasta que se defina la validez de la consulta popular y los territorios para decidir sobre el uso del subsuelo. Según Mejía, está declaración es un revés dado que la compañía previamente manifestó que respetaría los resultados del mecanismo ciudadano.

Pero para el líder ambiental, "lo más grave es que está desconociendo la Constitución, que nos permiten hacer consultas populares, y dos leyes estatutarias que nos permiten hacer que estos resultados sean implementados obligatoriamente".

Mejía agregó que Cajamarca es el primer municipio que presenta una demanda de nulidad de títulos en contra de una empresa minera y espera que los resultados de la acción legal generen un precedente favorable para las comunidades que prohibieron la minería a través de la consulta popular y ahora buscan implementarla.

<iframe id="audio_36784408" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36784408_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la informaciónde Contagio Radio en][[su correo][[Contagio Radio]
