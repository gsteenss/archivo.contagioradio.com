Title: "Militares no pueden seguir ocultando sus crímenes en el Palacio de Justicia"
Date: 2015-02-18 17:47
Author: CtgAdm
Category: Judicial, Otra Mirada
Tags: justicia transicional en colombia, Palacio de Justicia, víctimas
Slug: militares-no-pueden-seguir-ocultando-sus-crimenes-en-el-palacio-de-justica
Status: published

###### Foto: caracol.com 

<iframe src="http://www.ivoox.com/player_ek_4101660_2_1.html?data=lZadk5uadI6ZmKiakp6Jd6KompKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc3Zy8bbxtfFb7PjxdeSpZiJhaXb1srnj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejandra Rodríguez, hija de Cesar Rodríguez] 

En videos revelados en la tarde de este martes 17 de Febrero, se observan claramente las nuevas evidencias conocidas en el marco de las investigaciones por los crímenes cometidos por las FFMM durante la retoma del **Palacio de Justicia en 1985**. En el material fílmico se observa claramente a Carlos Rodríguez, administrador de la cafetería, **William Almonacid Rodríguez, integrante del M19, y Orlando Arrechea, torturado.**

<iframe src="http://www.ivoox.com/player_ek_4101730_2_1.html?data=lZadk5yXdI6ZmKiak5uJd6KplJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbixpC018bWaaSnhqax0IqWh4zawtLWzs7FtozYxpDZw9iPuoa3lIqupsjYrc7V1JDRx9GPlMLgwsjW0ZDIqYyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [René Guarín, Hermano de Cristina del Pilar Guarín] 

Alejandra Rodríguez, hija de **Carlos Rodríguez, administrador de la cafetería del Palacio** y una de las personas que se observa saliendo vivo de las instalaciones afirma que aunque no es la primera vez que observa los últimos minutos de vida de su padre, se mueven muchas fibras que creía que estaban muertas.

Ante las críticas de estos, casi, 30 años, Alejandra afirma que reconocen que el M19 tuvo responsabilidad en la toma del Palacio a sus familiares los desaparecieron las FFMM y ellos son los que tienen la responsabilidad por esos crímenes. **"Mi papá y sus compañeros de cafetería eran inocentes frente al conflicto" puntualiza Alejandra.**

**René Guarín,** hermano de María del Pilar Guarín, afirma que son muchas las razones que tienen los familiares para ser escépticos frente a las acciones de la justicia y del propio Estado. Aunque hay 50 minutos de este video  y contiene mucho material, se cuestiona el hecho de la tardanza para anexar esta información al material probatorio que ya existe.

Guarín también afirma que después de varios meses de que la **Corte Interamericana de Derechos Humanos** dictó sentencia, todavía no hay contactos satisfactorios por parte del gobierno nacional para el cumplimiento de las órdenes de la Corte, y aunque espera un reconocimiento de responsabilidad por parte del Estado seguramente estará acompañado de nuevas afrentas en contra de los familiares y de los propios fallos de la justicia.
