Title: Organizaciones de periodistas exigen liberación de Salud Hernández
Date: 2016-05-24 14:47
Category: DDHH, Nacional
Tags: FLIP, Periodista Salud Hernández, Salud Hernández desaparecida
Slug: organizaciones-de-periodistas-se-pronuncian-y-exigen-liberacion-de-salud-hernandez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Salud-Hernández.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País ] 

###### [24 Mayo 2016 ] 

Tanto la Fundación para la Libertad de Prensa, FLIP, como la Asociación de Prensa Internacional de Colombia se han pronunciado por el regreso sana y salva de la periodista **Salud Hernández Mora de quien no se tienen noticias desde el pasado sábado 21 de Mayo**, cuando se encontraba en la región del Catatumbo desarrollando labores periodísticas.

Además la FLIP ha denunciado que no solamente se trataría de la periodista Salud Hernández sino que también **habrían sido retenidos por desconocidos otros periodistas y camarógrafos de RCN y de Caracol**. En el relato de la denuncia pública afirman que "el lunes 23 de mayo a mediodía el periodista Diego D'Pablos y su camarógrafo, Carlos Melo, partieron desde Tibú hacia el corregimiento de Filogringo, municipio de El Tarra. Al lugar también acudió Diego Veloza, corresponsal de Caracol Tv junto con su camarógrafo, William Mora. Y un tercer periodista".

Aunque la FLIP confirmó ([Ver comunicado](https://es.scribd.com/doc/313720169/Comunicados-Flip-y-Apic)s) que los periodistas fueron retenidos y sus equipos averiados, no se tiene certeza de los que estarían en poder de algún grupo armado, puesto que las autoridades de policía no han confirmado esa información y e**n la zona no hay cobertura de señal de celular o telefonía fija**.

La [FLIP](https://archivo.contagioradio.com/?s=flip) y la APIC instaron a quienes tengan en su poder a los periodistas para que los liberen de inmediato y recordaron que las personas que desarrollan labores periodísticas en el marco del conflicto armado son civiles con protección especial e hicieron un llamado a "los organismos internacionales de carácter humanitario para que **inicien la exploración de instancias de mediación con el fin de facilitar la liberación y el retorno de los periodistas**".

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
