Title: Hay más $56 mil millones asignados para asuntos electorales como consultas populares
Date: 2017-11-03 19:29
Category: Ambiente, Nacional
Tags: consultas populares, La Macarena, Registraduaría Nacional
Slug: hay-mas-56-mil-millones-asignados-para-asuntos-electorales-como-consultas-populares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-voto-popular.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: revistaccesos] 

###### [3 Nov 2017]

[A la suspensión de la consulta popular en Granada, se suma la cancelación de los mecanismos de participación popular en El Castillo y La Macarena, en el departamento del Meta. ¿La razón? la falta de recursos que argumenta el gobierno nacional para costear las urnas y las papeletas requeridas para la votación.]

De acuerdo con Delio Franco, presidente de la Asociación de Juntas de La Macarena, la comunidad ha decidido consultar con diferentes instancias los costos que implicarían la realización de una consulta popular, para una población de un poco más de 8 mil personas. **El precio, según Franco, no superaría los 50 millones de pesos.**

### **¿Qué pasa con el presupuesto para eventos electorales?** 

[Precisamente un análisis que hace el [constitucionalista Luis Arturo Ramírez Roa, señala que s]egún el presupuesto para el 2017, la Registraduría Nacional cuenta con **un total de \$44'769.000.000 para financiación de Asuntos electorales y procesos democráticos.** Además, de acuerdo con la Ley de participación ciudadana, la 1757 de 2015, hay \$10.189.000.000 en el Fondo para la Participación Ciudadana, y \$1.252.000.000 en el ítem de Participación Ciudadana.]

Es decir que hay más de [\$56.000.000.000 destinados para asuntos como las consultas populares, de manera tal, que si la consulta popular en La Macarena tiene un costo cercano a los 50 millones de pesos, habría dinero de sobra para esta, y otras consultas en los diferentes municipios del país. ]

"Estamos indignados por lo que el gobierno le dijo al registrador que no tenía recursos para desarrollar consultas populares en el país", expresa el líder de la comunidad de La Macarena, quien señala que lo que están haciendo las comunidades no es otra cosa que defender sus territorios, bajo el amparo de la Constitución.

Y así parece ser. De acuerdo con el análisis del [constitucionalista Ramírez Roa, en la Constitución Política de 1991, reza en el artículo 334, que “**bajo ninguna circunstancia, autoridad alguna de naturaleza administrativa, legislativa o judicial, podrá invocar la sostenibilidad fiscal para menoscabar los derechos fundamentales**, restringir su alcance o negar su protección efectiva”. Para Ramírez, esto no significa otra cosa que no se puede negar el derecho a la participación, al tratarse de un derecho fundamental.]

Así las cuentas, por dar un ejemplo, teniendo en cuenta que la Asociación Colombiana de Petróleos, tiene un registro de 23 consultas populares en curso con el objetivo de impedir la **extracción y explotación de de hidrocarburos, si cada consulta costara lo mismo que la del municipio de La Macarena, el gobierno no gastaría más \$1'150.000.000.**

### **La respuesta de las comunidades** 

Teniendo en cuenta que lo que se procura es proteger de actividades petroleras, toda la región del Ariari, los municipios han decidido unirse para empezar a analizar soluciones jurisprudenciales, como la tutela, y acciones de movilización social para exigir el cumplimiento de la Constitución y por tanto, su derecho a la consulta popular. Asimismo, **las comunidades han decidido empezar a realizar bazares y colectas ciudadanas para recaudar dinero para poder realizar las consultas.**

En La Macarena, toda la comunidad está en busca de salidas para la situación en la que los ha puesto el gobierno nacional. Comerciantes, ganaderos, profesores,  estudiantes, agricultores, etc, buscan la manera de blindar su territorio de las petroleras, teniendo en cuenta en el, hacen parte esencial de las actividades agrícolas y ecoturísticas del Meta.

<iframe id="audio_21870282" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21870282_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

 
