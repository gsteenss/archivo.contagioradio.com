Title: "Guatemala ya despertó" pero vienen grandes retos
Date: 2015-09-02 14:20
Category: El mundo, Movilización, Política
Tags: constitucion, Guatemala, inmunidad, Movilización social, Otto Pérez Molina, Reformas
Slug: guatemala-ya-desperto-pero-vienen-grandes-retos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/85e5c39f3aeed12d93ca5beecb873aca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elconfidencial.com 

<iframe src="http://www.ivoox.com/player_ek_7762635_2_1.html?data=mJyjlJuXeY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRaZS6hqifh6elq9bV1craw9HFb9rVjMnS1dXJttWZpJiSm5iJd6eZpJeSm6mPtMbm0JDjy8rSqc%2BfyNfO0MnJt4zmxtnc1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Andrea Ixchiu, Prensa comunitaria] 

###### [2 Sep 2015] 

Otto Pérez Molina perdió la inmunidad con 132 votos de los diputados, un hecho histórico para la democracia guatemalteca y que permite a la justicia de este país investigarlo por los escándalos de corrupción conocidos como “La línea”.

Tan pronto Pérez Molina se decretó la pérdida de inmunidad el juez Miguel Ángel Gálvez otorgó medida de arraigo solicitada por el Ministerio Público (MP) para evitar su salida del país, con lo que queda abierta la posibilidad de que se le dicte una orden de arresto.

“Esta noticia se recibe con alegría, es un gesto con voluntad política”, indica Andrea Ixchiu, periodista de prensa comunitaria en Guatemala, en donde los diputados han escuchado al pueblo que se ha estado movilizando desde hace más de dos meses.

Desde que Guatemala firmó la paz en 1996, este es otro de los momentos históricos en el país que incluso hace pensar a la población en una reforma a la constitución que priorice los intereses del pueblo “En esto momentos lo guatemaltecos tenemos la posibilidad de pensar y diseñar una nueva constitución”, indica Ixchiu.
