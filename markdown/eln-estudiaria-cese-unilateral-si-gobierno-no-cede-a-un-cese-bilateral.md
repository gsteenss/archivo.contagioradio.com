Title: ELN estudiaría cese unilateral si gobierno no cede a un cese bilateral
Date: 2017-08-19 12:20
Category: Nacional, Paz
Tags: ecuador, ELN, Pablo Beltrán, proceso de paz eln
Slug: eln-estudiaria-cese-unilateral-si-gobierno-no-cede-a-un-cese-bilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/cese-bilateral-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 19 Ago 2017 

Desde Quito, Ecuador, sede de las conversaciones de paz entre el gobierno y el ELN, delegados de esta guerrilla reiteraron la falta de agilidad para acordar un cese bilateral, la concreción de los alivios humanitarios, y la realización de las audiencias territoriales y sectoriales sobre participación.

El jefe del equipo de paz de esa guerrilla, Pablo Beltrán, aseguró que siguen trabajando en el acuerdo de un Cese Bilateral y que están dispuestos a ceder en varios puntos para llevar un mensaje irrefutable a la sociedad colombiana.

"*Nosotros estamos trabajando para que haya cese al fuego bilateral temporal y somos optimistas y pensamos que va haber*" **Pablo Beltrán, ELN**

Integrantes de la guerrilla del ELN dejaron entrever ante la solicitud de comunidades y organizaciones que estudiarían la posibilidad de un cese unilateral que discutirían en este cierre de ciclo en el que el gobierno se obstina en mantener posturas inflexibles.

El ELN en rueda de prensa con diversos medios insistió en la preocupación que les asiste por la continuidad del asesinato de líderes sociales y defensores de Derechos Humanos en el país.

Agregó Beltrán que han insistido al gobierno su respondabilidad en garantizar las condiciones de combate al paramilitarismo.

Organizaciones de base y derechos humanos y de Paz vienen reiterando a las partes de llegar a un Acuerdo Bilteral de cese de fuego y hostilidades.

Observadores y participantes en las conversaciones de Quito indicaron hoy que constatan sensatez y disposición del ELN para escuchar y disponerse a un gesto humanitario de ceses de fuego y hostilidades temporal en razón de la visita del Papa Francisco a Colombia, más que el propio gobierno en cabeza del Nobel de Paz, Juan Manuel Santos.

Respecto a los derechos de las víctimas valoran el momento histórico y sostuvieron que la verdad toda y verdad de todos es el principio de su organización.

Un eventual cese de fuegos sea bilateral o unilateral será un salto cualitativo en estos siete meses de esta mesa que se desarrolla entre el gobierno Juan Manuel Santos y el ELN.
