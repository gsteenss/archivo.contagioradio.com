Title: Los animales en situación de calle también merecen nuestro cuidado
Date: 2020-03-25 11:00
Author: CtgAdm
Category: Actualidad, Ambiente
Tags: Animales, Bogotá, Perros, protección animal
Slug: los-animales-en-situacion-de-calle-tambien-merecen-nuestro-cuidado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/csm_AgenciaDeNoticias-20180109-05_2a22f6b01e.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Ante la obligación de protección por el [Covid-19](https://archivo.contagioradio.com/y-que-garantias-ofrece-el-gobierno-a-los-excombatientes-frente-al-covid-19/) miles de personas se refugian en sus casas para evitar contagios, sin embargo mientras el Gobierno nacional y local generan decretos que de alguna manera protejan a sus pobladores, olvidan sectores vulnerables como los animales en situación de calle.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto hace algunos días 22 concejales de Bogotá presentaron una carta al presidente Iván Duque donde solicitan cobijar en los decretos denominados \#Cuarentenaporlavida a cientos de animales en el país podrían fallecer o sufrir a causa de la situación que también los afecta, y no por el riesgo al contagio, sino por el olvido.  

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/andreanimalidad/status/1242420634300407808","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/andreanimalidad/status/1242420634300407808

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Concejales animalistas como Andrea Padilla, [Susana Muhamad](https://archivo.contagioradio.com/cambio-climatico-extincion-humanos/), María Fernanda Rojas, Diego Cancino, Julián Rodríguez entre otros, presentan en el documento 7 puntos fundamentales que para ellos representan el cuidado animal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Inicialmente se refieren a la atención de *"alimentación e hidratación de animales en condición de calle o comunitario por parte de las personas que identifiquen las autoridades territoriales"*, así como el cuidado de animales que se encuentren en centros de comercio como plazas de mercado y tiendas de mascotas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado piden que dentro de las excepciones de los decretos se contemplen las emergencias veterinarias y la atención a aquellos animales que se encuentre en tratamiento médico, al igual que aquellas personas a cargo de refugios o albergues.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma hicieron referencia a los perros de seguridad, atención de desastres, asistencia, o de compañía para que sus cuidadores tenga la posibilidad de brindar atención a sus necesidades fisiológicas fuera de los espacios donde habiten.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y finalmente destacaron el trato que se debe dar a los animales que se encuentran en cuarentena previa al sacrificio en mataderos, para que estos no queden atrapados días o horas en los camiones de transporte, sino que se pueda brindar albergues provisorios que no afecten su estado de salud mientras esperan a la activación de los centros de sacrificio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El compromiso con los animales es de todos y todas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Natalia Parra, subdirectora de cultura ciudadana y gestión de conocimiento del[Instituto de Protección y Bienestar Animal](http://www.proteccionanimalbogota.gov.co/)afirmó que ante la situación que vive el mundo producto de la pandemia se hizo más evidente la dependencia que el ser humano ha creado con los animales, *"a falta de personas en las calles muchos animales, gatos, perros e incluso aves han quedado desamparados y hoy sufren de hambre y de sed".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por eso señaló que ante esta emergencia el Instituto, junto con la Subdirección de Cultura de Bogotá, han desarrollando una serie de tareas, entre ellas un mapa de las zonas donde están los animales concentrándose, y así coordinar brigadas de atención, *"estamos gestionando donaciones y transporte dentro de lo permitido en los nuevos decretos para poder alimentar periódicamente a estos animales"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Trabajo que según Parra ha incluido un proceso de participación ciudadana, en función de solucionar dudas pero también construir un proyecto de atención animal, *"la gente va poder inscribirse en una lista donde de acuerdo a la zona donde viva y la edad que tenga acatando las restricciones de autocuidado, podrán hacer rondas en sus cuadras para poder alimentar a los animales que allí habiten"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Acción que será compuesta por un curso virtual donde las personas, podrán aprender a coordinar con empresas y organizaciones donantes la entrega de alimentos, el cuidado a la hora de salir, el paseo a sus mascotas y hasta la elaboración de comedores para los animales en situación de calle.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente recalcó que esta acción de intervención humana que a afectado a cientos de especies, hoy debe ser atendida y asumida por los bogotanos y bogotanas, *"es importante que hagamos un trabajo conjunto responsable que evite a futuro desplazamiento masivo de animales domésticos en situación de calle, que por necesidad se vean obligados a recurrir a otros territorios donde pueden afectar la vida silvestre".*

<!-- /wp:paragraph -->
