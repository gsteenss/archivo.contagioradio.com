Title: Somos Defensores documentó 51 asesinatos de líderes de DDHH en 2017
Date: 2017-08-09 13:34
Category: DDHH, Entrevistas
Tags: aguzate, Asesinatos a líderes comunitarios, defensores de derechos humanos, Somos defensores
Slug: somos-defensores-documento-51-asesinatos-de-defensores-de-derechos-humanos-en-2017
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/defensore-de-derechos-humanos-asesinados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Archivo ] 

###### [09 Ago 2017] 

La organización Somos Defensores lanzará la próxima semana su informe semestral sobre la situación de las y los defensores de Derechos Humanos. Sin embargo, han denunciado que, en 2017, **han documentado 51 asesinatos de líderes sociales**. Igualmente, en redes sociales lanzaron la campaña “Aguzate” para decirle a los defensores, que ante la inoperancia del Estado, es necesario que se protejan entre ellos.

Según  Somos Defensores, **hubo un incremento de 30% en los asesinatos de los defensores con relación al mismo tiempo del año anterior**. Para Carlos Guevara, miembro de esa organización , esto se debe a factores como el vacío de poder que existe en los territorios del país. “Cuando las FARC salieron de los territorios se creó un vacío de poder que fue tomado por diferentes grupos que presionan a los líderes sociales”.

Guevara manifestó que esta situación ya se sabía que iba a ocurrir y fue desatendida por el Gobierno Nacional. **“El gobierno opta por enviar fuerzas militares a los territorios y allí no se sabe quién manda** y es ahí cuando resultan afectados los líderes”. A esto se suma que “ha habido un incremento en la aparición de movimientos armados entre paramilitares, bandas criminales y guerrilla que se están disputando los territorios”. (Le puede interesar: ["En 2017 han sido asesinados 15 líderes sociales en el Cauca"](https://archivo.contagioradio.com/en-2017-han-sido-asesinados-15-lideres-sociales-en-el-cauca/))

### **Independientemente de quién sea defensor, Fiscalía debe investigar** 

Somos Defensores ha hecho énfasis en que, el debate de quién es o no defensor no debe ser el centro de la discusión en la medida que **“todos los casos que se denuncien deben ser investigados por la Fiscalía General de la Nación”.** Guevara recalcó que “la Fiscalía y el Gobierno Nacional tienen una posición reduccionista que se centra en ver quiénes son defensores y no hacen su labor de investigar y proteger”.

Igualmente, manifestó que los reportes de asesinatos o amenazas que se hacen desde las organizaciones **no pueden ser estigmatizadas** y el Gobierno tiene que priorizar la investigación de estos casos”. Recalcó además que “la Fiscalía tiene una responsabilidad muy grande para investigar todos los casos sin debatir si son o no defensores”.

### **Debería haber una institución que lidere situación de defensores de derechos humanos** 

Una de las grandes preocupaciones de la organización Somos Defensores es que **"no hay en el país una institución que lidere y asuma la problemática que viven los defensores** de derechos humanos en todo el país". “Todos se han lavado las manos y nadie soluciona el problema, la Defensoría del Pueblo solo tiene capacidad para advertir, la Procuraduría General no investiga a los funcionarios que no evitan los asesinatos y el Gobierno hace cuanta mesa existe para dialogar, pero no protege”. (Le puede interesar: ["Informe de Fiscalía sobre asesinatos de defensores de DDHH es otro "falso positivo"](https://archivo.contagioradio.com/informe-de-fiscalia-sobre-asesinatos-a-defensores-de-dd-hh-es-otro-falso-positivo/))

Ante este panorama, Guevara fue enfático en manifestar que **“los asesinatos y las matanzas van a continuar mientras no haya acciones integrales** que protejan desde lo local a los defensores, que entre otras cosas están matando a los que están en el campo, no en las grandes ciudades”. Igualmente, dijo que hay una actitud de negar y condenar las acciones violentas contra los defensores, pero “nadie asume una responsabilidad completa ni un liderazgo desde alguna institución”.

### **Solamente con 5 investigaciones concluidas no se puede decir si asesinatos son o no sistemáticos** 

Carlos Guevara indicó que **“hace más de 4 meses el Fiscal dijo que los homicidios de los defensores no tienen ninguna relación entre si”**. Sin embargo, para él es incomprensible que la Fiscalía haga estas aseveraciones cuando “sólo hay 5 condenas en más de 80 casos, si no hay investigaciones y condenas no se puede saber si hay sistematicidad o no”.

Según él, **“hay una actitud facilista de la Fiscalía** y las organizaciones no están diciendo que haya sistematicidad sino que le piden a la Fiscalía que muestre los argumentos que indican que no hay una sistematización en los homicidios y esto sólo se logra haciendo las investigaciones y condenando a los responsables”. (Le puede interesar: ["En 2017 han sido asesinados 41 líderes sociales"](https://archivo.contagioradio.com/en-el-transcurso-del-2017-han-sido-asesinados-41-lideres-sociales/))

Finalmente, Somos Defensores enfatizó en que el **informe que presentarán está dirigido a los defensores de derechos humanos** para que entre ellos mismos se protejan. “Tenemos que dejar a un lado las diferencias entre nosotros y protegernos de la mejor manera que es comunicándonos, estando pendiente del otro y haciendo las denuncias respectivas”.

<iframe id="audio_20255905" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20255905_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

######  
