Title: Denuncian asesinato de campesino a manos del Ejército en Córdoba
Date: 2018-12-26 13:25
Author: AdminContagio
Category: DDHH, Nacional
Tags: Agresiones fuerza pública, Asesinatos de campesinos, cordoba
Slug: denuncian-asesinato-de-campesino-por-miembros-del-ejercito-en-cordoba
Status: published

###### Foto:  @CesarJerezM 

###### 26 Dic 2018 

A través de un comunicado, la **Asociación de Campesinos del Sur de Córdoba (ASCSUCOR)** denunció el asesinato del campesino Luis Eduardo Garay Manchego, presuntamente por integrantes de la unidad militar conocida como Batallón Rifles en Puerto Libertador,  Córdoba.

[Como respuesta a la incursión de grupos armados al margen de la ley y el incremento de amenazas y asesinatos contra líderes sociales en las zonas rurales y municipios a lo largo del país, el Gobierno ha optado por intervenir militarmente dichos territorios, una medida que han reiterado las comunidades, no resulta efectiva y que por el contrario limita el movimiento de la población. [ (Le puede interesar Recuperar su territorio le está costando la vida a indígenas en Cauca)](https://archivo.contagioradio.com/nos-estan-asesinando-llamado-de-comunidades-indigenas-del-cauca/)]

[En el marco de dicha militarización, el pasado 24 de diciembre cerca de las 4:30 pm,  el campesino **Luis Eduardo Garay Manchego** quien se desplazaba en moto desde el corregimiento **La Rica**, al corregimiento **Juan José del municipio Puerto Libertador en Córdoba**, fue abordado en un retén por integrantes del **Batallón Rifles**, responsables de la seguridad en aquella zona.]

[Según el comunicado difundido por ASCSUCOR,  se presume que en confusos hechos el campesino habría muerto a manos de los integrantes de la fuerza pública después de recibir un disparo por la espalda. Mientras los soldados afirman que Garay no acató la orden de pare y dispararon, la comunidad denuncia que abrieron fuego sin advertirle. ]

[El cuerpo de Luis Eduardo Garay, de 47 años fue encontrado a varios metros de la vía "sin portar ningún tipo de prendas de uso privativo de las fuerzas militares" o algún artefacto  u objeto que lo pudiera relacionar con algún delito. ]

[La Asociación de Campesinos del Sur de Córdoba responsabiliza de los hechos al presidente Iván Duque y al coronel **Carlos Eduardo Luque Ochoa,** a cargo del Batallón Rifles, el cual a su vez pertenece a la **Brigada Once**, supervisada por el coronel **Gabriel Fernando Marín Peñalosa,** y por el** mayor general Nicacio Martínez Espinel, d**e igual forma exigen se esclarezcan los hechos por parte de las fuerzas militares para que se tomen los debido correctivos contra los perpetradores.]

###### Reciba toda la información de Contagio Radio en [[su correo]
