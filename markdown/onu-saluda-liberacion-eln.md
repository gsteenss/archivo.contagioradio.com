Title: ONU saluda liberación de las personas retenidas por el ELN
Date: 2018-09-14 17:21
Author: AdminContagio
Category: DDHH, Paz
Tags: Chocó, ELN, Liberación, ONU
Slug: onu-saluda-liberacion-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/7e176aeb3830d5a8d6c8251eaf5189a0_XL-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Proceso Digital] 

###### [14 Sep 2018] 

Mediante un comunicado, la **Oficina del Alto Comisionado para los Derechos Humanos** de la **ONU** saludó la liberación de las 6 personas retenidas por el Ejército de Liberación Nacional (**ELN**), el pasado 2 de Agosto en Chocó. En el documentó el organismo internacional expresó que esperan una pronta reanudación de los diálogos, entre este grupo armado y el Gobierno colombiano.

En la comunicación Alberto Brunori, representante en Colombia del Alto Comisionado de la ONU, expresó que esperan “también **que en algún momento de los diálogos -ojalá cercano- se llegue a un acuerdo sobre un cese del fuego**”, que junto a la liberación de las personas retenidas por el frente de guerra occidental Omar Gómez, permitiría que los colombianos y colombianas vivan en un ambiente propicio para la paz.

Sin embargo, la organización internacional le recordó al ELN que el **Derecho Internacional Humanitario prohíbe la toma de rehenes**, y sostuvo que "tomar rehenes causa profundo dolor y provoca consecuencias difíciles de sanar tanto en las personas privadas de libertad como en sus familias, y en la sociedad". (Le puede interesar: ["Por ahora, Duque no quiere la negociación sino la rendición del ELN: Victor de Currea"](https://archivo.contagioradio.com/duque-quiere-rendicion-eln-currea/))

Por último, la Oficina reiteró que seguirá apoyando la búsqueda de la Paz y el respeto por los derechos humanos; de otra parte, la campaña **"Paz Completa"** sostuvo que es vital continuar con los diálogos de paz y advirtió que no continuar la mesa de negociaciones **"sería nefasto para los territorios y para las comunidades que aún siguen padeciendo el conflicto armado"**.

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
