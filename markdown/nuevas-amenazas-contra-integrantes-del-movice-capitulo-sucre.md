Title: Nuevas amenazas contra integrantes del MOVICE capítulo Sucre
Date: 2015-03-26 21:36
Author: CtgAdm
Category: DDHH, Nacional
Tags: Amenazas, Los Urabeños, MOVICE, Movimiento de víctmas de crímenes de Estado, paramilitares, vícitmas
Slug: nuevas-amenazas-contra-integrantes-del-movice-capitulo-sucre
Status: published

##### Foto: [notascect.wordpress.com]

<iframe src="http://www.ivoox.com/player_ek_4269451_2_1.html?data=lZejm5mZdY6ZmKiakpqJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkbDKqqiyjdjZp9PZjMnS0NrSp8rVjMbax9PFvsLnjMjc0NnWpYzn1tiYy9PYqcjmwtPhx9iPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan David Díaz, vocero MOVICE Sucre] 

El Movimiento Nacional de Víctimas de Crímenes de Estado MOVICE, capítulo Sucre denuncia que **Malena Mariet Martínez y Juan David Díaz,** integrantes del movimiento, fueron amenazados por el **bloque paramilitar ‘los Urabeños**’,

La amenaza fue enviada **el pasado 20 de marzo del 2015,** a la esposa de Díaz, Malena Mariet Martínez, quien también es defensora de derechos humanos. Ella recibió un mensaje de texto en su celular, donde también se hacía referencia a que la vida de su esposo estaba en peligro.

De acuerdo a Juan David Díaz, las amenazas provienen de los ‘Los Urableños’, debido a que en este momento el MOVICE viene acompañando a varios campesinos en los procesos de **restitución de tierras de Sucre**, específicamente en el caso de la **finca La Europa**.

Otra de las razones de las amenazas, según indica el vocero del MOVICE, es la presión que viene ejerciendo Juan David, para que se conozca la verdad sobre el caso de su padre el  ex **Alcalde de El Roble, Eudaldo Diaz,** asesinado por paramilitares hace 12 años. Precisamente, desde ese momento, indica Díaz, fue amenazado por primera vez, y desde ese entonces las amenazas no han cesado.

Los integrantes de la organización, afirman que estas amenazas se vienen registrando desde inicio del presente año, sin embargo, pese a que las víctimas han logrado obtener los números desde donde se envían los mensajes de texto y se reconocen las placas de los carros que persiguen a los integrantes del MOVICE, los defensores de derechos humanos **no han conocido ningún resultado de las investigaciones que adelantan las autoridades competentes.**

Por su parte, Juan David Díaz, asegura que, pese a las múltiples amenazas, el MOVICE capítulo Sucre, seguirá al tanto de los casos de restitución de tierras, **reclamando el derecho a la verdad y la reparación de las víctimas.**
