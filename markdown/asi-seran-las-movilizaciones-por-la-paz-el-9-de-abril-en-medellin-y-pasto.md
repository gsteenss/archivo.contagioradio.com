Title: Así serán las Movilizaciones por la paz el 9 de Abril en Medellín y Pasto
Date: 2015-04-06 18:07
Author: CtgAdm
Category: Nacional, Paz
Tags: 9, 9 de abril, abril, Antioquia, carlos lugo, carlos medina gallego, Corporación Jurídica Libertad, marcha patriotica, maria alejandra rojas, Medellin, MOVICE, nariño, pasto, paz
Slug: asi-seran-las-movilizaciones-por-la-paz-el-9-de-abril-en-medellin-y-pasto
Status: published

###### Foto: Canal Capital 

<iframe src="http://www.ivoox.com/player_ek_4313555_2_1.html?data=lZielZqZeY6ZmKiakpmJd6KplZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdDqytHW3MbHrdDixtiY0tTWb83VjNXO3JDJsIytjMnSjcbGtsrgjMrbjabSuMrj0trWw5CRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Adriana Arboleda, Movice Antioquia] 

<iframe src="http://www.ivoox.com/player_ek_4313539_2_1.html?data=lZielZqXfY6ZmKiakpyJd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdDqytHW3MbHrdDixtiY0tTWb83VjNXO3JDJsIytjMnSjcbGtsrgjMrbjbPFtsqZpJiSpJbTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Wilka Rodríguez, Marcha Patriótica Nariño] 

En el marco de la conmemoración del 9 de abril en Colombia, organizaciones sociales y populares se han dado cita en diferentes lugares del país para realizar movilizaciones, actos culturales y políticos en apoyo al proceso de paz.

La ciudad de Medellín será el punto de encuentro de al rededor de 50 mil personas del eje cafetero, Córdoba, Chocó y la misma Antioquia, que se movilizarán el 9 de abril a partir de las 9 de la mañana en una marcha-carnaval desde la Universidad de Antioquia hasta el Parque de las Luces, frente al Centro Administrativo La Alpujarra, donde se realizará un concierto por la paz.

Adriana Arboleda, del MOVICE Antioquia, señala que las consignas giran en torno al apoyo al proceso de paz, el cese al fuego bilateral, los derechos ambientales y la democracia.

Por su parte, en el departamento de Nariño se realizará un foro "Por la paz y el cese al fuego bilateral" el miércoles 8 de abril a partir de las 10 am en el Auditorio Luis Santander Benavides de la Universidad de Nariño, que contará con ponente como Carlos Medina Gallego, profesor universitario, Maria Alejandra Rojas de la Federación de Estudiantes Universitarios y Carlos Lugo, artista ex-prisionero político. La secretaría de educación del departamento dio permiso a los colegios para que los y las estudiantes de secundaria puedan asistir.

<div>

El 9 de abril se sumarán a la iniciativa nacional por una movilización, que partirá a las 9 de la mañana de 3 puntos (la Universidad de Nariño, el Estadio Libertad y el Sindicato del magisterio) hacia la Plaza de Nariño, donde se leerá un manifiesto unitario por parte de la ONIC, Progresistas, Marcha Patriótica, Congreso de los Pueblos, Polo y Partido Verde, y se finalizará con un concierto.

</div>
