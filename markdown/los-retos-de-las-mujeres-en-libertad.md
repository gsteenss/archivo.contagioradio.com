Title: Los retos de las mujeres en Libertad
Date: 2019-07-03 11:14
Author: CtgAdm
Category: Expreso Libertad
Tags: carceles, Derechos Humanos, Expreso Libertad, mujeres libres, programa radio
Slug: los-retos-de-las-mujeres-en-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/presa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Denis Oliveira 

<div>

Angela y Esperanza son dos mujeres que, tras salir de la cárcel, se juntaron al colectivo Mujeres Libres, una iniciativa que pretende denunciar las consecuencias de una falta de resocialización en las cárceles del país, que pone en riesgo a las mujeres que han pasado por estos centros penitenciarios.

</div>

<div>

</div>

<div>

Desde la falta de acompañamiento sicosocial, pasando por la negación de derechos fundamentales como a la salud, la mujeres deben continuar afrontando obstáculos en una sociedad y contra un Estado que las estigmatiza y castiga, sin brindarles oportunidades.

</div>

<div>

\[embed\]https://www.facebook.com/contagioradio/videos/429388924278455/\[/embed\]

</div>

<!-- Load Facebook SDK for JavaScript -->

<!-- Your embedded video player code -->

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
