Title: "Cese bilateral al fuego sería el mejor regalo para los colombianos"
Date: 2015-10-29 17:55
Category: Nacional, Paz
Tags: Camilo González Poso, carlos medina gallego, Centro de Pensamiento y Seguimiento al Proceso de Paz de la Universidad Nacional, Cese bilateral al fuego, Constituyente por la paz, Diálogos de La Habana, dialogos de paz, FARC, INDEPAZ, Negociaciones de paz Colombia
Slug: cese-bilateral-al-fuego-seria-el-mejor-regalo-para-los-colombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/12.3-Foto-AFP-Luis-Robayo.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_9210617_2_1.html?data=mpeekpuVe46ZmKiak5uJd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8bnxpDPy9HFuMbmwtGYw9GPqtbZyNSYh5eWpYzYhqigh6aopdSfxcqY19OPpcTpxtfR0ZDKrc%2FVzYqflJKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Medina Gallego] 

<iframe src="http://www.ivoox.com/player_ek_9210627_2_1.html?data=mpeekpuWe46ZmKiak5eJd6KmkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8bnxpDPy9HFuMbmwtGYw9GPqtbZyNSYh5eWpYzYhqigh6aopdSfxcqY19OPpcTpxtfR0ZDKrc%2FVzYqflJCWcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Camilo González] 

###### [29 Oct 2015] 

[El presidente Juan Manuel Santos anunció la posibilidad de un **cese bilateral al fuego a partir del 1 de enero de 2016**, siempre y cuando el punto sobre el fin del conflicto se firme en diciembre. De ser así, el **acuerdo final** se lograría en un plazo no mayor a 145 días, es decir, al terminar el mes de **marzo de 2016**.   ]

[Carlos Medina Gallego, integrante del Centro de Pensamiento y Seguimiento al Proceso de Paz de la Universidad Nacional, asegura que existe la **necesidad de acordar un cese bilateral al fuego**, dados los avances y gestos que las dos partes en negociación han mostrado durante el proceso, y que sobre todo, que evidencian la **voluntad política de las FARC quienes **han cumplido con no operar militarmente en los territorios en los que hacen presencia pese a los **hostigamientos de la fuerza pública**.]

[Por su parte, el director de Indepaz, Camilo González Pozo, afirma que el mensaje sobre el cese bilateral "es positivo" y añadió  indica que **“estamos a días de un acuerdo final de paz”,** y agrega que sería un gran regalo de navidad para todos los colombianos .]

[Lo que sigue ahora es revisar las condiciones y los requerimientos para garantizar su aplicación, lo que exigiría el **acompañamiento la comunidad internacional**, con el fin de asegurar que este cese de hostilidades incluya al movimiento social que ha sido encarcelado, judicializado y criminalizado durante los últimos meses, dice Medina.]

[Por otra parte, frente a posibilidad de una **Constituyente por la paz, para el profesor Carlos Medina, esta **debe ser un **pacto entre las fuerzas políticas hegemónicas actuales, **teniendo en cuenta que los acuerdos a los que se puedan llegar deben incluir tanto a las guerrillas como a los sectores sociales y empresariales dominantes como una **“suma de todos los conflictos y todas las propuestas”** para lograr la inclusión. ]

[Frente a la posibilidad de que los **gremios empresariales** estén presentes en los Diálogos, el director de Indepaz, asegura que es pertinente dada su activa participación en el conflicto, demostrada en los cerca de **13 mil casos en contra de empresarios** en la compulsa de Justicia y Paz, por lo que se hace necesario sincronizar un pacto político entre todos los sectores para que los acuerdos y “reformas no queden en el aire”, de manera que "la propuesta de una **constituyente** **debe ser concertada con todos los sectores, teniendo en cuenta que es un pacto político”.**]

[Finalmente, Medina, expresó que de acuerdo a los resultados de las elecciones regionales del pasado 25 de octubre, el vicepresidente Germán Vargas Lleras, es el más opcionado para la presidencia de Colombia en el próximo periodo, sin embargo, "**Vargas Lleras tiene que apersonarse del proceso de paz, es hora de que se ponga la palomita de la paz",** dice el profesor, teniendo en cuenta que el vicepresidente ha estado muy alejado de las negociaciones en la Habana. ]
