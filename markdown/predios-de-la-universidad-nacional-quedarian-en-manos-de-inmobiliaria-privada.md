Title: Predios de la Universidad Nacional quedarían en manos de inmobiliaria privada
Date: 2016-06-14 14:08
Category: Educación, Nacional
Tags: Ampliación CAN, Concejo de Bogotá, Plan de desarrollo Peñalosa, Universidad Nacional sede Bogotá
Slug: predios-de-la-universidad-nacional-quedarian-en-manos-de-inmobiliaria-privada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/UNal-Uriel-Gutierrez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia Noticias UNAL ] 

###### [14 Junio 2016]

De acuerdo con el más reciente comunicado emitido por el Consejo de la Sede Bogotá de la Universidad Nacional, el actual Plan de Renovación Urbana del CAN, aprobado en el plan de desarrollo distrital, deja **en manos de la 'Agencia Nacional Inmobiliaria Virgilio Barco Vargas' terrenos de la institución** universitaria en los que están los edificios Uriel Gutierrez, Camilo Torres, el campus en el que ya se ha implementado la primera fase del Hospital Nacional Universitario, junto con los predios en los que se planea construir la segunda etapa del proyecto y el Centro de Atención en salud estudiantil.

La decisión aprobada por el Concejo de Bogotá que prioriza la renovación del CAN, ha sido rechazada por las directivas de la Universidad, quienes insisten en que los terrenos de la institución tienen un uso de suelo dotacional, que implica que **sólo puedan ser usados para fines educativos**. Por lo que aseguran que llevarán a cabo todas las acciones necesarias para que el claustro siga siendo el propietario de estos predios.

Noticia en desarrollo...

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]]
