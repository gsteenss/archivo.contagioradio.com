Title: Católicos pidieron perdón por crímenes de la Iglesia
Date: 2017-04-09 21:03
Category: Nacional, Paz
Tags: iglesia católica, Víctimas de colombia
Slug: catolicos-pidieron-perdon-por-crimenes-de-la-iglesia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/IMG_0021.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [09 Abr 2017] 

Aproximadamente 600 integrantes de la Iglesia Católica reunieron sus firmas para pedirle perdón a las víctimas del conflicto armado, **por los hechos de violencia en los que esta iglesia se ha visto involucrada. Se espera que a mediados de Junio durante, este mismo acto se haga públicamente y más integrantes se unan.**

Entre el grupo de víctimas que recibió este perdón se encontraban el senador **Iván Cepeda y su hermana, María Cepeda, la vocera del movimiento político Marcha Patriótica, Piedad Córdoba, Hilda Quiroga**, esposa del desaparecido líder Jacinto Quiroga, entre otros, que fueron afectados directamente por acciones de la iglesia Católica. Le puede interesar: ["Católicos piden perdón por implicación de la Iglesia en el conflicto armado colombiano"](https://archivo.contagioradio.com/35728/)

Para Iván Cepeda, este acto de perdón, también significa un llamado a la iglesia Católica para que asuma su responsabilidad “aquí habrá que examinar desde el nivel de lo estrictamente penal, hasta el nivel de lo espiritual, porque parte de los cambios profundo que tendremos que afrontar los colombianos será con la cultura”, y agregó que espera que **aquellos miembros de la iglesia que cometieron actos de violencia contra la sociedad no esperen a ser llamados para decir la verdad, sino que vayan por voluntad propia.**

Por su parte, Piedad Córdoba expresó que ojala el acto público alcance y convoque a muchas personas para “tocarle el corazón al país”, “lo más importante es dejar atrás toda esta historia de muerte, de desaparición, de desplazamiento para poder entrar a una sociedad del amor” **además, señaló que los altos representantes de esta iglesia se unan al pedido de perdón.**

Entre las personas que firman la carta hay un grupo de 200 integrantes, que se encuentran en el exterior, mientras que en Colombia desde teólogos hasta campesinos creyentes en la fe Católica se sumaron a esta petición de perdón. Le puede interesar: ["Las víctimas de crímenes de Estado tenemos derecho a ser escuchadas"](https://archivo.contagioradio.com/35728/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
