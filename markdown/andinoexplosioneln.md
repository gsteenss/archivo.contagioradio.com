Title: Rechazan politización de explosión en Centro Comercial Andino
Date: 2017-06-19 09:55
Category: Nacional
Tags: Bogotá, ELN, paz
Slug: andinoexplosioneln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/centro-comercial-andino-tres-personas-muertas-y-diez-heridos-deja-explosion-556459.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colombia.com 

###### 17 jun 2017 

Tras la explosión ocurrida en un baño de mujeres del Centro Comercial Andino de Bogotá, que dejó como saldo **3 personas muertas y 8 heridas**,  varias voces expresaron su solidaridad con las víctimas y rechazaron la politización que han pretendido hacer algunos sectores políticos y periodistas sobre los hechos.

Algunas de las primeras reacciones, aseguraron que la responsabilidad de la explosión recaía en el ELN, guerrilla que expresó a través de su cuenta en twitter su solidaridad con las víctimas y rechazó las malintencionadas afirmaciones.

> 1.[@ELN\_Paz](https://twitter.com/ELN_Paz) repudia ataque en Centro Comercial Andino contra civiles. Compartimos el dolor y nos solidarizamos con las víctimas. (sigue)
>
> — ELN-PAZ (@ELN\_Paz) [18 de junio de 2017](https://twitter.com/ELN_Paz/status/876248001739706368)

En uno de los 4 tweets, el grupo guerrillero que actualmente se encuentra en diálogos con el Gobierno nacional, pidió "**seriedad a quienes hacen acusaciones infundadas y temerarias**" con los que pretenden generar respuestas por parte de la opinión pública, asegurando que "hay quienes pretenden así hacer trizas los procesos de paz". Le puede interesar: [ELN está listo para firmar cese bilateral](https://archivo.contagioradio.com/42291/).

> 2\. [@ELN\_Paz](https://twitter.com/ELN_Paz) pide seriedad a quienes hacen acusaciones infundadas y temerarias; hay quienes pretenden así hacer trizas los procesos de Paz
>
> — ELN-PAZ (@ELN\_Paz) [18 de junio de 2017](https://twitter.com/ELN_Paz/status/876249470010355716)

<p style="text-align: justify;">
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
En sus comunicaciones, el ELN condena el hecho y pide al Estado **realizar a fondo las investigaciones pertinentes para identificar a los responsables**, asegurando que jamás realizarían acciones "cuyo objetivo sea afectar a la población civil". Por su parte, Rodrigo Londoño Echeverry, comandante máximo de las FARC EP, envió un mensaje de solidaridad con las víctimas de tales actos.

</p>
> Solidaridad con las víctimas de hoy en Bogotá. Dicho acto sólo puede venir de quienes quieren cerrar los caminos de la paz y reconciliación. — Rodrigo Londoño (@TimoFARC) [18 de junio de 2017](https://twitter.com/TimoFARC/status/876263671533580288)

Varias opiniones concuerdan en que las afirmaciones proferidas respecto a los autores y móviles del hecho, tienen propósitos políticos y la clara intención de **desestabilizar los procesos que se adelantan con ambas guerrillas**, tal como lo manifestó  el  gestor de paz Carlos Velandia.

> Acto terrorista en CC Andino, tiene propósito calculado de hacer fracasar esfuerzos de paz. [@RestrepoJCamilo](https://twitter.com/RestrepoJCamilo)[@Giarias](https://twitter.com/Giarias)[@ELN\_Paz](https://twitter.com/ELN_Paz)[@FARC\_EPueblo](https://twitter.com/FARC_EPueblo) — Carlos A Velandia J (@carlosvelandiaj) [18 de junio de 2017](https://twitter.com/carlosvelandiaj/status/876231430149459968)

<p>
Otra de las reacciones en ese sentido proviene del Senador Ivan Cepeda quién aseguro que el acto terrorista fue " meticulosamente calculado para causar el mayor pánico posible: elección de víctimas día y lugar". El senador coincide en que el acto busca "daño al proceso de paz, sembrar zozobra y sentimiento de inseguridad" asegurando que sus autores son "enemigos de la paz". El representante a la Cámara Alirio Uribe, manifestó en su cuenta de Twitter el rechazo al atentando en el Centro Comercial Andino con un mensaje de solidaridad por las mujeres fallecidas y levantó una exigencia para que se realice una investigación con verdad total. Adicionalmente compartió el comunicado interno de la Policía en que se alerta de a[cciones por parte del "Clan del golfo" en Medellín y Bogotá](https://archivo.contagioradio.com/paramilitaresmedellinbogota/?platform=hootsuite), amenazas que algunos especulan podrían estar relacionadas con el atentado del sábado. Por su parte, el presidente Juan Manuel Santos, aseguró que "los que quieren aguar la fiesta de la paz no lo van a conseguir", envió sus  condolencias a las familias de las víctimas y aseguro que "este acto vil y cobarde no quedará impune" impartiendo una orden precisa a la Policía sobre cómo proceder hasta capturar a los responsables. El domingo anunció una recompensa de 100 millones de pesos por información que de con los responsables del hecho.

<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
  

<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>

