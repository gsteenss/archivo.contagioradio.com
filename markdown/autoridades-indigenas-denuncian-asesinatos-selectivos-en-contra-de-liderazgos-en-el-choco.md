Title: Autoridades indígenas denuncian asesinatos selectivos de liderazgos en  Chocó
Date: 2020-11-21 10:26
Author: AdminContagio
Category: Nacional
Tags: Asesinatos selectivos, autoridades indígenas, Autoridades Tradicionales Indígenas de Colombia Gobierno Mayor, indígenas
Slug: autoridades-indigenas-denuncian-asesinatos-selectivos-en-contra-de-liderazgos-en-el-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Autoridades-Indigenas.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Autoridades-Indigenas-1.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este jueves la Organización Nacional de Autoridades Tradicionales Indígenas de Colombia [Gobierno Mayor](https://www.facebook.com/gobiernomayor1/) denunció a través de una rueda de prensa las múltiples violaciones a los Derechos Humanos y al Derecho Internacional Humanitario de las que vienen siendo víctimas las comunidades indígenas en todo el país y especialmente en el Chocó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre las varias violaciones, las Autoridades indígenas resaltaron los asesinatos y masacres selectivos, los desplazamientos forzados, hostigamientos, amedrentamientos a las comunidades y el confinamiento al que se han visto sometidos los indígenas por cuenta de la acción violenta de grupos armados legales e ilegales en sus territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las Autoridades indígenas señalaron que su defensa a la vida, el territorio oponiéndose a las concesiones mineras, a la explotación masiva e irresponsable de los recursos naturales, a la siembre de cultivos para uso ilícito y a la oposición frente al proceder de los grupos ilegales han implicado para ellos(as) una sentencia de muerte ante la inacción total del Estado. (Le puede interesar: [Asesinan a Erlin Undagama, docente embera del Chocó](https://archivo.contagioradio.com/asesinan-a-erlin-undagama-docente-embera-del-choco/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ello, en su comunicado denunciaron los múltiples asesinatos selectivos de los que han sido víctimas varios indígenas que ejercían diversos tipos de liderazgos al interior de sus comunidades como Rubilio Papelito Limón y Javier Urágama Chamorro asesinados en el mes de julio; José Nelson Tapic asesinado en el mes de septiembre; Erlin Forastero Undagama asesinado en el mes de octubre y Genaro Isabare Forastero asesinado en el mes de noviembre. (Le puede interesar: [En el Bajo Baudó fue asesinado el profesor Rubilio Papelito](https://archivo.contagioradio.com/en-el-bajo-baudo-fue-asesinado-el-profesor-rubilio-papelito/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, las Autoridades indígenas señalaron que eran más las víctimas producto de la violencia, como el caso de la niña indígena de tan solo nueve años que murió en Alto Baudó, Chocó; luego de que la alcanzaran balas del fuego cruzado entre los grupos armados ilegales del Clan del Golfo y el ELN. (Lea también: [Menor Embera de 9 años muere en medio de enfrentamiento armado](https://archivo.contagioradio.com/menor-embera-de-9-anos-muere-en-medio-de-enfrentamiento-armado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, rechazaron de manera contundente la estigmatización que el Gobierno y la Fuerza Pública han ejercido en contra de los pueblos indígenas vinculándolos con grupos al margen de la ley o señalándolos como narcotraficantes.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Queremos decirle al pueblo colombiano que nosotros no tenemos cultivos ilícitos, nosotros hemos usado las plantas para la medicina tradicional y el Buen Vivir de nuestros pueblos; nosotros no comercializamos nuestras plantas así como tampoco comercializamos nuestros saberes. Pedimos respeto hacia las comunidades y pueblos indígenas”
>
> <cite>Autoridad indígena Miryam Chamorro Caldera</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Finalmente, las Autoridades indígenas denunciaron la indolencia del Gobierno y expresaron estar cansados de generar alertas, llamados, cartas, solicitudes y  comunicados de prensa, denunciando estas situaciones sin obtener ningún tipo de respuesta satisfactoria por parte de las autoridades, instituciones y entes de control del Estado, por ello insistieron en hacer llamados a la Comunidad Internacional para que a través de su intermediación pudiera hacer que el Estado colombiano escuche sus clamores.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/gobiernomayor1/videos/129547738661065","type":"rich","providerNameSlug":"embed-handler","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-rich is-provider-embed-handler">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/gobiernomayor1/videos/129547738661065

</div>

<figcaption>
Rueda de Prensa Autoridades Tradicionales Indígenas de Colombia [Gobierno Mayor](https://www.facebook.com/gobiernomayor1/)

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
