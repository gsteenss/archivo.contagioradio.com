Title: BOLETÍN INFORMATIVO ABRIL 23
Date: 2015-04-23 16:15
Author: CtgAdm
Category: datos
Slug: boletin-informativo-abril-23
Status: published

Noticias del Día:  
<iframe src="http://www.ivoox.com/player_ek_4399015_2_1.html?data=lZimm5WVeY6ZmKiakpuJd6KmmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjabGtsrgjJegj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Senadores y representantes de diferentes partidos políticos, asistieron el pasado 22 de abril a un desayuno organizado  por movimientos animalistas en el Congreso de la República. El objetivo fue retomar la bancada animalista, para impulsar los compromisos por la defensa de los animales en Colombia, habla Catalina Tenjo, de la Plataforma ALTO.

-En el Plan Nacional de Desarrollo “2014-2018”, existe un decreto que impediría frenar la actividad minera, que tiene unos 347 títulos en 26 zonas de páramo del país. Habla el senador del Polo Democrático Alternativo, Iván Cepeda.

-Este 24 de abril la ciudadanía se tomará las calles de Bogotá y Bucaramanga para protestar en contra de la minería y los megaproyectos que atentan contra el ambiente, Erik Jerena, del Colectivo Agenda Animal habla sobre la movilización en Bogotá.

-En el marco de la jornada del Movimiento Nacional Carcelario, cuatro internos de la carcel la "Picota" iniciaron el pasado lunes una huelga de hambre como forma de presión al Gobierno para buscar salidas urgentes a la actual crisis carcelaria en el país. Habla Gloria Silva, abogada de la Fundación Comité de Solidaridad con Presos Políticos.

-El procurador Alejandro Ordóñez, envió una carta perentoria al ministro de Salud, para que detenga el proceso administrativo que le daba vía libre a la muerte digna en Colombia, Ferney Rodríguez de la Asociación de Ateos de Bogotá habla sobre este tema

-El Frente Amplio por la Paz entregó su cuarto informe de seguimiento al cese unilateral de las farc, en que lamentaron la muertes de militares en el departamento del Cauca y resaltaron la importancia de decretar un cese bilateral de fuego. Alirio Uribe representante a la cámara por el Polo Democrático.
