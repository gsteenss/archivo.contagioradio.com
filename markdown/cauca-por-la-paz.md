Title: El sur del Cauca marchó por la paz
Date: 2016-10-27 08:55
Category: yoreporto
Tags: Cauca, marcha
Slug: cauca-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/cauca-marcha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

####  Agencia de Comunicaciones Prensa Alternativa Cauca - Yo Reporto 

[26 Oct 2016]

[**Más de 5 mil campesinas y campesinos recorrieron la carretera internacional en Cauca** con el objetivo de exigir la implementación de los acuerdos logrados en La Habana entre la insurgencia de las FARC-EP y el Gobierno Nacional.]

[Desde tempranas horas de la mañana, **la Gran Marcha por la Paz del sur del Cauca**, convocada por la Asociación Campesina de Trabajadores de Argelia –ASCAMTA-, la Escuela Cultural Campesina de Argelia, el Comité Cocalero de Argelia, la Asociación Agropecuaria del Patía –AGROPATIA-, la Asociación Campesina de Trabajadores de Balboa, la Federación Sindical Unitaria Agropecuaria –FENSUAGRO-, la Red de Derechos Humanos Francisco Isaías Cifuentes, el Proceso de Unidad Popular del Suroccidente Colombiano –PUPSOC- y el Movimiento Político y Social Marcha Patriótica, recorrió la vía panamericana mostrando una fuerte convocatoria a favor de la paz en Colombia.]

[Dentro de la programación de la marcha por la paz, estuvo presente un acto en conmemoración de las personas heridas y asesinadas en el marco del Paro Nacional Agrario y Popular del 2013, luchando por justas reivindicaciones para las comunidades pobres.]

[De la misma manera y en un acto de reconciliación, las comunidades movilizadas realizaron una entrega floral al personal del Ejército Nacional ubicado en la zona de El Estrecho -Patía- lugar donde culminó la movilización.]

[“Hoy continuamos en movilización pacífica como un acto de paz verdadera. Nuestra lucha siempre ha sido por la paz, por vida digna y por el buen vivir del pueblo colombiano. Esta movilización, además de exigir la implementación de los acuerdos, tiene un componente de conmemoración al compañero caído y a aquellos que fueron heridos en el 2013 y de reconciliación con la fuerza pública. Hoy les decimos que siempre hemos estado dispuestos a construir una verdadera patria entre las grandes mayorías de nuestras comunidades”, expreso Gloria Burbano, dirigente campesina de Mojarras]

### **Algo de historia** 

[El 5 de septiembre, cuando más de 5 mil personas apostadas en la vía panamericana exigían condiciones mínimas para sus comunidades, la fuerza pública –policía, ESMAD, Ejército Nacional y Fuerza Aérea- arremetió contra los movilizados en un]*[“intenso ataque indiscriminado en el que dispararon gases lacrimógenos y granadas incendiarias (por aire desde helicópteros y en tierra), granadas de fragmentación, granadas de mortero, granadas de fusil, granadas MGL, granadas aturdidoras, granadas recalzadas, lanza llamas, perdigones, balas de goma, ráfagas de proyectiles de armas de fuego, proyectiles recalzados y "papas" bomba sobre la humanidad de los concentrados y bienes civiles,”]*[ dejando un saldo de un muerto, más de 30 heridos y mutilados –entre los que se encontraban defensores de derechos humanos- y violentada la propiedad particular de decenas de campesinos.]
