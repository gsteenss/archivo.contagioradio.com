Title: En Guamal, Meta sí se extrae petroleo pero no hay gas para sus habitantes
Date: 2019-01-18 13:09
Author: AdminContagio
Category: Ambiente, Comunidad
Tags: comunidades, Defensa de los servicios públicos, Meta
Slug: en-guamal-meta-si-se-extrae-petroleo-pero-no-hay-gas-para-sus-habitantes
Status: published

###### Foto: Archivo 

###### 18 Ene 2019 

[El pasado 16 de enero 150 habitantes de la comunidad de Guamal, Meta se desplazaron hasta la sede del Ministerio de Minas y Energía en la ciudad de Bogotá  para exigir que sea instalado el gas domiciliario en el municipio, tal como se acordó en un convenio con Ecopetrol desde noviembre de 2017, un proyecto que no ha iniciado y que hoy  **afecta**] **a más** [**1390 familias**.  
  
Aunque **en noviembre de 2017 la Alcaldía y Ecopetrol aprobaron la financiación del  proyecto por un valor de \$13.121 millones** de los cuales la empresa aportará la suma de \$11.821 millones y el municipio el valor restante, según Miguel Ángel Sánchez, líder de la vereda de Santa Bárbara, más de un año después el servicio de gas no ha sido instalado en sus hogares, afectando a 17 veredas.  
  
Por su parte Ecopetrol,  afirma que la ejecución del convenio está a cargo de la Alcaldía de Guamal y que la compañía no determina la entidad que deba cumplir ese rol, una problemática que se agudiza con la ausencia de injerencia de **la Comisión de Regulación de Energía y Gas (CREG)** que según Mejía, tampoco ha brindado el apoyo necesario para la instalación del gas.]

[Ante tal circunstancia, cuatro buses con 150 habitantes del municipio se desplazaron hasta la capital del país para manifestarse frente a la sede del Ministerio de Minas, después de varias horas de plantón, se acordó realizar una mesa de trabajo junto a Ecopetrol la CREG, la Superintendencia, el alcalde Cristobal Lozano y la comunidad del Municipio.]

[Durante las reuniones del día 16 y 17 de enero, se llegó a un acuerdo mediante el cual se realizó una modificación al convenio entre la Alcaldía y **Ecopetrol** el cual permitiría aprobar una licitación pública para que cualquier empresa de gas del país pueda postularse y obtener el proyecto de gasificación rural.]

[El líder comunitario explica que esperarán hasta el 30 de enero para conocer el dictamen oficial del convenio y acto seguido se dirigirán a la CREG para comenzar la licitación y resolver qué precio se aplicará a las tarifas que serán cobradas por el servicio, sin embargo advierte que de no existir una solución para ese entonces, la comunidad entrará en paro.]

###### Reciba toda la información de Contagio Radio en [[su correo]
