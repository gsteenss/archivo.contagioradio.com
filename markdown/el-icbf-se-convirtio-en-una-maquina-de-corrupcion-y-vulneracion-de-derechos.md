Title: El ICBF se convirtió en una máquina de corrupción y vulneración de derechos 
Date: 2016-02-03 14:30
Category: DDHH, Nacional
Tags: corrupción, Elvira del Pilar Forero, ICBF
Slug: el-icbf-se-convirtio-en-una-maquina-de-corrupcion-y-vulneracion-de-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/ICBF-e1454527754226.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Radio Nacional 

<iframe src="http://www.ivoox.com/player_ek_10304827_2_1.html?data=kpWgkpmcdpihhpywj5WaaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5yncabgjK6wpKuPt8afxNTb2M7WuMqZpJiSpJiPqc%2Bf1tPOjdKJh5SZopbe187SpYzYxpDQ0dfWudHXyoqwlYqmd8%2Bf2pCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Angela María Robledo, Alianza Verde] 

###### [3 Feb 2016 ]

Tras conocerse el detrimento patrimonial que la dirección de Elvira del Pilar Forero provocó al ‘Instituto Colombiano de Bienestar Familiar’ ICBF, por presuntas irregularidades en las contrataciones, la Procuraduría decidió **inhabilitarla para el ejercicio de cargos públicos durante 11 años**, medida con la que también sancionó a Rosa María Navarro exsecretaria general de la institución.

De acuerdo con el ente de control esta sanción es producto de las múltiples inconsistencias halladas en el contrato que en 2011 suscribió el ICBF con la ‘Red de Universidades Públicas del Eje Cafetero para el Desarrollo Regional Alma Mater', entre ellas los**altos sobrecostos que representó la subcontratación de la sociedad ‘Fiscolombia’** por parte de Alma Mater, al declararse que no contaba con capacidades jurídicas ni operativas para ejecutar el contrato pactado.

La Representante Ángela María Robledo, asevera que pese a la importancia de esta decisión, **quedan pendientes otras denuncias que se han venido realizando** en relación con el ‘carrusel de contratos’ que esta entidad ha suscrito para prestar el servicio de alimentación de la población infantil en distintas regiones del país. “De fondo el problema del ICBF no se ha resuelto” pues mientras la entidad “siga siendo **una empresa de contratación,** seguirá siendo **una máquina de corrupción y un espacio de violación a los derechos de los niños y las niñas en Colombia**”.

El ICBF requiere una profunda transformación porque como viene funcionando **no es la entidad garante de los derechos de los menores en el país**, afirma la Representante, e insiste en que la institución debe funcionar descentralizadamente como venía funcionando antes de la administración de Elvira del Pilar Forero, designada en el cargo por el entonces presidente Álvaro Uribe, “en una época en la que lo público se vio afectado y reducido” y que convirtió al ICBF “en una máquina de contratación **con muy poca capacidad de operar en los territorios y garantizar la vida digna de los niños en Colombia**”.

Robledo concluye asegurando que **en esta etapa de alistamiento para el posconflicto compete al Gobierno nacional promover una transformación institucional** tanto en lo local como en lo departamental, para evitar que se presenten casos de corrupción como el del ICBF y hacer que las **instituciones sean garantes de los derechos de toda la población colombiana**.
