Title: Hay una intención de exterminar los procesos que impulsan los líderes sociales: Procuraduría
Date: 2018-04-16 13:14
Category: DDHH, Política
Tags: asesinato de líderes sociales, defensores de derchos humanos, lideres sociales, procuraduria
Slug: hay-una-intencion-de-exterminar-los-procesos-sociales-que-impulsan-los-lideres-sociales-procuraduria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/avance-minero-energético.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo COA] 

###### [16 Abr 2018] 

De acuerdo con el portal de información [Verdad Abierta,](https://verdadabierta.com/violencia-defensores-derechos-territoriales-sistematica-generalizada-procuraduria/) la Procuraduría General de la Nación emitió un informe titulado "Violencia Sistémica contra Defensores de derechos terrritoriales en Colombia" donde retrata **la sistematicidad en los asesinatos de defensores** de derechos humanos y líderes sociales. Teniendo en cuenta que los hechos se han presentado con la intención de “exterminar los procesos sociales que los líderes impulsan en sus comunidades”.

### **Disparidad en cifras de asesinatos evidencia un desorden institucional** 

Según el Ministerio Público, hay una disparidad entre las cifras que presentan los diferentes mecanismos de control y organizaciones sociales en lo relacionado con los **asesinatos de líderes sociales**. Esta disparidad, para la Procuraduría es preocupante en la medida en que reflejan las pocas garantías de seguridad que hay para los defensores y esto supone una continuidad en la comisión de delitos contra estas personas.

El trabajo de la unificación de las cifras que se manejan desde y fuera del Gobierno implica que las Autoridades “superen los **modelos reduccionistas** de atención a este fenómeno de violencia”.  Para la Procuraduría la atención a líderes sólo se ha centrado en estrategias de reacción frente al riesgo más no al problema de abandono estatal como causa de esta situación.

Para ejemplificar la disparidad en las cifras, el portal recoge las cifras de la Defensoría de Pueblo, que indican **282 homicidios entre 2016 y 2017**, frente a las cifras de la organización Somos Defensores que indican que sólo en lo que va de 2018 han sido asesinados 48 defensores del territorio. (Le puede interesar:["Efrén Zuñiga, docente y líder de ASOINCA fue torturado y asesinado en Piendamó, Cauca"](https://archivo.contagioradio.com/efren-zuniga-docente-y-lider-de-asoinca-fue-torturado-y-asesinado-en-piendamo-cauca/)) modificar

### **UNP está fallando en proporcionar mecanismos de protección** 

En lo que tiene con ver con la protección dirigida a los defensores del territorio, el informe de la Procuraduría cuestiona la labor que realiza la Unidad Nacional de Protección. Esto teniendo en cuenta que la **UNP no relaciona las causas de defensa del territorio** con sus estudios de riesgo por lo que no se puede hacer un seguimiento específico de las solicitudes de protección que llegan.

Adicionalmente, el Ministerio Público hace énfasis en que **“hay vacíos”** en las medidas de protección adoptadas teniendo en cuenta que no corresponden con el nivel de riesgo que tienen los defensores en cada uno de sus territorios. Esto ha derivado en que algunos de  los defensores que cuentan con mecanismos de protección son igualmente asesinados.

### **Procuraduría hizo llamados de atención a la entidades estatales** 

Frente a esta situación, que retrata un “desarreglo institucional”, la Procuraduría advirtió que las entidades Estatales **deben proteger la vida de los derechos humanos**. En primer lugar, le advirtió al Gobierno Nacional sobre la necesidad de crear “un protocolo unificado de atención y reacción ante las solicitudes de protección y las denuncias elevadas por los líderes”. Esto con el fin de brindar una respuesta inmediata ante los problemas de seguridad y que se activen las rutas institucionales para las mismas.

En segundo lugar, le advirtió al Ministerio del Interior que debe activar los **comités territoriales de prevención y protección** para garantizar la vida de los defensores de los derechos humanos y el territorio. Además, de manera especial le solicitó a la UNP que implemente las medidas de protección de manera eficaz para evitar hechos de violencia. (Le puede interesar:["No hay freno a los asesinatos de líderes sociales"](https://archivo.contagioradio.com/continuan-los-asesinatos-de-lideres-sociales-en-el-pais/))

Finalmente, hizo una advertencia sobre la necesidad de que se cumpla lo pactado en el Acuerdo de Paz en lo referente con el fortalecimiento de los sistemas de seguridad que son propios de los pueblos indígenas como lo son **la Guardia Cimarrona y la Guardia indígena**. Esto teniendo en cuenta que en departamentos como el Cauca, en donde habitan poblaciones étnicas, tan sólo en 2018 han sido asesinados 48 defensores de los derechos humanos.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
