Title: Trabajadores de Indupalma denuncian posible desalojo por parte de Fuerza Pública
Date: 2018-02-09 15:39
Category: Movilización, Nacional
Tags: Insupalma, Trabajadores
Slug: trabajadores-de-indupalma-denuncian-posible-desalojo-por-parte-de-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Indupalma-4jpeg.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Partido Socialista de los trabajadores] 

###### [09 Feb 2018] 

Trabajadores de Indupalma que están en huelga desde hace dos semanas, d**enunciaron una posible arremetida por parte del ESMAD y la Fuerza Pública, con la autorización de los directivos de la empresa**. Esta acción tendría como finalidad acabar el paro pacífico que se adelantan los trabajadores exigiendo garantías laborales.

De acuerdo con Andrey Piñeres, trabajador de Indupalma, los empleados se habrían enterado de que las directivas de la empresa autorizarían el ingreso del ESMAD y la Fuerza Pública la próxima semana, “una fuente muy seria nos dijo que **ya había un escuadrón y un operativo listo para el municipio de San Alberto, que sacaría a los trabajadores que se encuentran en huelga**”. (Le puede interesar: ["Avanza la huelga de los trabajadores de Indupalma"](https://archivo.contagioradio.com/avanza-la-huelga-1200-trabajadores-indupalma/))

En el último encuentro que hubo entre trabajadores y directivas, estas señalaron que se levantarían de la mesa si el cese de actividades continuaba, **a lo que los empleados respondieron que no hay las suficientes garantías para retomar la jornada laboral**, ni de que continúe el diálogo.

Piñeres recalcó que las dos semanas que llevan de huelga, las acciones han sido pacíficas y sin bloqueos, y que hasta el momento ningún juez **ha determino como ilegal la protesta**, “en la jornada la persona de la cocina se levanta a las 4 de la mañana, desayunamos, a las 8 de la mañana entramos a capacitación sobre derechos laborales y en la hora de la tarde son procesos lúdicos”.

Los trabajadores señalaron que seguirán en la huelga hasta que las directivas de la empresa demuestren garantías concretas para avanzar en la negociación sobre el cambio del modelo de contratación y que será hasta ese momento que se levante la manifestación. (Le pueda: ["Trabajadores denuncian abuso laboral por parte de la empresa Indupalma"](https://archivo.contagioradio.com/trabajadores-de-indupalma-tienen-que-pagar-hasta-su-dotacion/))

<iframe id="audio_23663819" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23663819_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
