Title: Ágora “Contemos cuentos hagamos la PAZ”
Date: 2016-08-13 16:10
Category: eventos
Tags: Cuenteros Bogotá, Cultura, Teatro Jorge Eliécer Gaitán
Slug: agora-contemos-cuentos-hagamos-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/63196b23-f299-4be7-a53c-7a227a8ca093.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Mauricio Grande 

##### [14 Agos 2016]

Más de 20 cuenteros locales y nacionales se encuentran reunidos en Bogotá en el marco de 'Ágora', el **Festival de Cuentos y cuenteros**, que en su décimo-segunda edición propone un espacio para compartir con los asistentes las mejores obras de narración oral creadas en lo que va corrido de 2016.

El tema central para esta edición de Ágora, es Bogotá como escenario de la guerra y la paz en Colombia, considerando a la narración oral como pilar importante en la comunicación y la memoria colectiva; racionamiento a partir del cual se desarrollarán las actividades que irán hasta el próximo 10 de septiembre.

[![Agora festival](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/c96e2564-81d0-4e4c-88bb-68228c9f9bcb.png){.aligncenter .size-full .wp-image-27826 width="539" height="754"}](https://archivo.contagioradio.com/?attachment_id=27826)

El Festival tiene como invitado especial desde Pie de cuesta Santander a **Marco Durán “El Garlotero”**, el comediante **Gonzalo Valderrama**, y los narradores **Hanna Cuenca**, **Juan Carlos Grisales, Lina María Orozco, Diego Beltrán, Nelly Pardo, Alejandro Campos, Mauricio Grande, el Grupo Luna Nueva** y los estudiantes **Sergio Muñoz**, **Katerine Paredes **y **Sergio Londoño**, ganadores del Festival Universitario.

Con la propuesta de escuchar y contar los cuentos que habitan en todos los rincones de la ciudad (el Ágora), se inaugura para la capital una nueva franja de programación denominada “**Vespertinas del Municipal**”, donde el público podrá disfrutar de un plan familiar en las tardes de los domingos en el Teatro Jorge Eliécer Gaitán (Cra. 7 \#22-47, Bogotá) y el Teatro Hilos Mágicos (Calle 71 \# 12 – 20, Bogotá).
