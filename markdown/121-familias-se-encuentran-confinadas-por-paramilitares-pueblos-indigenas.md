Title: 121 familias se encuentran confinadas por paramilitares: Pueblos Indígenas
Date: 2018-07-10 12:21
Category: DDHH
Tags: CDDHHPI, Derechos Humanos, paramilitares, pueblos indígenas
Slug: 121-familias-se-encuentran-confinadas-por-paramilitares-pueblos-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/indigenas-exterminio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 Jul 2018] 

De acuerdo con la denuncia hecha por la Comisión de Derechos Humanos de los Pueblos Indígenas(CDDHHPI), **121 familias que habitan en los resguardos Rio Domingodó y Mamey Dipurdu en Carmen del Darien, Chocó**, están en situación de confinamiento por cuenta del control territorial y asentamiento que está realizando un grupo paramilitar desde abril de este año.

Aunque en la denuncia no se precisa la identificación del actor armado, la Comisión indica que el 10 de abril hicieron presencia 300 hombres amenazando a miembros de la guardia indígena así como a un líder de la comunidad, acusándolo de ser colaborador del ELN. Conforme con el documento, el 20 de junio "**nuevamente ingresaron al territorio aproximadamente 400 paramilitares,** realizaron una reunión con todos los miembros  de la comunidad  indicándoles que varios de sus compañeros tenían problemas con este grupo"**.**

### **Los paramilitares controlan y se asentaron en las comunidades** 

Según lo relata el comunicado, desde el 20 de junio el grupo paramilitar se estableció en las comunidades Sokerre, Unión Chogorodo y Mamey Dipudu de ambos resguardos manteniendo el control sobre las comunicaciones telefónicas como vía internet, e igualmente controlando el desplazamiento de las 121 familias que habitan el territorio. (Le puede interesar: ["66% de los pueblos indígenas está a punto de desaparecer: ONIC"](https://archivo.contagioradio.com/pueblos-indigenas-a-punto-de-desaparecer-onic/))

Adicionalmente, controlan la cantidad de comida a la cual acceden los miembros de ambos resguardos indígenas mediante la verificación de facturas de compra que no pueden superar el valor de 100 mil pesos. Como lo señala la denuncia, **"en caso de no presentar la factura les decomisan los alimentos, (y) si sobrepasan el monto establecido se infiere que están colaborando con el ELN".**

Por estas razones la CDDHHPI pide a la Unidad Nacional de Protección que se inicie el tramite para proteger al líder así como a los miembros de la guardia indígena amenazados por el grupo paramilitar; a la Fiscalía para que se investiguen los hechos y a la Defensoría del Pueblo para que se priorice la atención de las **836** personas confinadas en las 3 comunidades indígenas. **Sin embargo, resalta que las acciones de carácter humanitario deben ser realizadas por actores no militares. **

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
