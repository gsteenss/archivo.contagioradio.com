Title: Animalistas proponen consulta antitaurina por una Bogotá SinToreo   
Date: 2015-02-05 17:48
Author: CtgAdm
Category: Animales, Política
Tags: #Bogotásintoreo, Consulta antinaurina, corridas de toros, derechos de los animales, Natalia Parra Osorio, Onda Animalista
Slug: consulta-antitaurina-proponen-animalistas-por-una-bogota-sintoreo
Status: published

##### Foto: contagioradio 

<iframe src="http://www.ivoox.com/player_ek_4043674_2_1.html?data=lZWhlZubeI6ZmKiak5eJd6KmmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc%2FdzsbZy9jYpdSf0dfc0tTSqc%2BfhpegpdTSt9bg1cau0NnNuMLp087bw5DUs9Of1tPOjYqWd6PjyNSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Natalia Parra] 

**Organizaciones animalistas  proponen una consulta popular,** con el objetivo de revocar la decisión de **Corte Constitucional**, que con una votación de cinco votos a favor, permitió revivir las corridas de toros en la Plaza de Todos la Santamaría, que el Alcalde Gustavo Petro había destinado para otro tipo de actividades culturales totalmente alejadas de la "fiesta brava".

Natalia Parra, directora de la **Plataforma ALTO** e integrante de la mesa de trabajo del programa Onda Animalista de Contagio Radio, afirmó que  los animalistas desde hace tiempo contemplan de posibilidad de que se realice una **consulta antitaurina**, "**queremos que seamos los habitantes de Bogotá los que decidamos si un festejo cruel como las corridas de toros deben volver** a la Plaza La Santa María", señala.

El estatuto taurino es un reglamento interno que existe en todos los países donde aun se realiza la “fiesta brava”, el problema, según resalta la animalista, es que “**en Colombia lo volvieron una ley que es prácticamente intocable,** dice expresamente que el toro debe morir en la plaza”.

Los defensores de la vida de los animales proponen que en el congreso se apruebe un proyecto de ley donde se “eliminen  **excepciones absurdas que hay en la ley 84  de 1989**, que permiten la tortura”, lo que hace que las corridas se vean como actividades de tradición cultural, según indica la directora de la Plataforma ALTO.

Precisamente, uno de congresistas que promueve un proyecto de ley sobre las actividades taurinas, es el senador **Armando Benedetti**, sin embargo, Natalia Parra, afirma que para los animalistas es preocupante que esta iniciativa de Benedetti  más bien sea para regular los eventos taurinos y no para terminar definitivamente con las corridas de toros, “**no queremos que se trate de un proyecto meramente regulacionista, y que se sigan presentando este tipo de espectáculos que maltratan a los animales,** eso sería fatal”, expresó la activista.

Finalmente, Natalia Parra, concluyó que “**un país que está buscando salir de la guerra no puede permitir el maltrato a los animales**”, ese será el mensaje de las organizaciones animalistas, que se manifestarán por los derechos de los animales el próximo **domingo 8 de febrero a las 10 de la mañana frente a la Plaza de Todos.**
