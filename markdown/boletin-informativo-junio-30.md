Title: boletín informativo Junio 30
Date: 2015-06-30 17:31
Category: datos
Tags: Adelfo Rodríguez, Amenazas a opositores de Hidroituango, Cesar Jeréz ANZORC, Destrucción por árbol tumba Jaime Pardo Leal, Jornada de movilizaciones de la cumbre agraria, Policia y habitantes de El Mango llegan a un acuerdo, Retornan familias a la hacienda "Bella Cruz"
Slug: boletin-informativo-junio-30
Status: published

[*Noticias del Día:*]

<iframe src="http://www.ivoox.com/player_ek_4706923_2_1.html?data=lZydmJ6Wd46ZmKiakpyJd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZiUcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-**300 familias** que habian sido desplazadas en 1996 por grupos al margen de la ley, retornaron a los terrenos valdíos de su propiedad en la **hacienda "Bellacruz"**, ubicada en el Departamento del Cesar. Los habitantes denuncian que empresas a las que le fueron adjudicados los terrenos, en particular aquellas dedicadas al **monocultivo de palma aceitera**, mantienen a los habitantes sumidos en una crisis socio-económica, humanitaria y ambiental por la afectación que representa la explotación. Habla **Adelfo Rodríguez** en representación de los pobladores.

-La semana anterior **la cumbre agraria** se reunió con el presidente Juan Manuel Santos, exigiendo respuestas del **Gobierno nacional** sobre sus incumplimientos. Sin embargo, **no hubo respuestas concretas** por lo que las organizaciones ya preparan todo para una próxima jornada de movilizaciones. Habla **César Jeréz**, miembro de la **Asociación Nacional de Zonas de reserva campesina**.

-La tensión aumentó durante el fin de semana en el corregimiento **"El Mango"** en Argelia Cauca, debido a la presencia de la policía y el ESMAD que llegaron al pueblo a **tomarse** las casas para “**usarlas como trincheras**”, sin embargo, con la **mediación** de la Defensoría del Pueblo se acordó que los agentes de la policía permanecerían **15 días** en un lote mientras se define un sitio permanente para instalar una nueva estación, a las afueras del corregimiento. Habla **Paola**, vocera de la comunidad,

-Continúan llegando **amenazas** a barequeros y **opositores de Hidroituango**, esta vez de parte del grupo autodenominado **NPLM**, que a través de un mensaje de texto afirma tener identificados los líderes que se oponen al mega proyecto, les da **72 horas** para que salgan de la playa **"la Arenera"**, donde se construye la hidroeléctrica de Empresas Públicas de Medellín, **Juan Pablo Soler**, integrante del **Movimiento Ríos Vivos**.

-A pesar que fue rectificada la información en relación con la **destrucción de la tumba** del ex candidato presidencial y miembro de la Unión Patriótica **Jaime Pardo Leal**, ocasionada por la caída de un árbol y no por razones políticas, la presidenta de la UP **Aida Avella** afirma que el cementerio central de Bogotá, **patrimonio cultural de la nación**, se encuentra en ruinas y requiere la intervención de las autoridades competentes.
