Title: Denuncian asesinato de integrante de las FARC en Ituango
Date: 2017-07-14 15:39
Category: Nacional, Paz
Tags: Amnistia, FARC, Guerrilleros, Ituango, Zona Veredal
Slug: denuncian-asesinato-de-exguerrillero-de-las-farc-en-ituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/las-preocupaciones-de-las-farc-luego-de-la-entrega-de-armas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [14 Jul. 2017]

Según información entregada por la Zona Veredal Santa Lucia, ubicada en el municipio de Ituango, departamento de Antioquia, **el 12 de julio fue asesinado otro integrante de las FARC beneficiado con indulto** y que había salido de la cárcel de Bellavista en Medellín el 12 de diciembre de 2016. Juan Fernando Amaya Valencia residía y trabajaba en el casco urbano de Ituango, lugar en el que también convivía con su familia.

Los hechos se dieron hacia las 3 de la tarde, mientras Juan Fernando se dirigía hacia la Vereda La Granja en una moto con otra persona “**en el trayecto fue interceptado por dos sujetos desconocidos, que dicen pertenecer al grupo paramiltar AGC**, quienes lo retuvieron y le pidieron a su acompañante proseguir su camino” asegura el comunicado.

Cerca de 4 horas después, sobre las 7 de la noche, una persona se comunicó con la mamá de Juan Fernando para manifestarle que **habían** **asesinado a su hijo y que el cuerpo sin vida se encontraba a las orillas de la vía La Granja** “desde dicho momento sus padres y allegados desplegaron la búsqueda y solo siendo más o menos las 09:30 de la noche fue encontrado el cuerpo” relata la misiva.

La guerrilla de las FARC ha rechazado este hecho y aseguran que se suma a los dos recientes asesinatos que han sido cometidos en el Municipio de Ituango en los últimos 12 días. Le puede interesar: [Asesinan en Putumayo a familiares de guerrillero de las FARC](https://archivo.contagioradio.com/asesinan-familia-de-guerrillero/)

**"Sentamos nuestra voz de protesta frente a tan infames hechos y dejamos constancia de nuestra preocupación** frente a la falta de garantías, seguridad y la indiferencia por parte del Estado frente a tan atroces hechos” recalcan. Le puede interesar: [José Yatacúe, integrante de las FARC-EP fue asesinado en Toribio, Cauca](https://archivo.contagioradio.com/jose-huber-yatacue/)

Por último, aseveran que están en zozobra dado que en el mes de agosto será el fin de las ZVTN por lo que hacen “un llamado a toda la sociedad colombiana, a tomar conciencia del tan importante momento que vive nuestro país, y apoderarse de los acuerdos para construir la PAZ, y no permitir que nos roben la tranquilidad”.

Con este asesinato, **se completan 6 integrantes de las FARC asesinados luego de la firma de los Acuerdos de Paz,** así como de las amnistías otorgadas. Le puede interesar: [Asesinan a Rulber Santana guerrillero Indultado de las FARC-EP](https://archivo.contagioradio.com/asesinan-rulber-santana-guerrillero-indultado-de-las-farc-ep/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
