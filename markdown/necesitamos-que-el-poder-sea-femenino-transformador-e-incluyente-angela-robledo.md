Title: Necesitamos que el poder sea femenino, transformador e incluyente: Ángela Robledo
Date: 2019-10-28 13:53
Author: CtgAdm
Category: Entrevistas, Nacional
Tags: Bogotá, Claudia López
Slug: necesitamos-que-el-poder-sea-femenino-transformador-e-incluyente-angela-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Angela-Robledo-y-Claudia-López.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @angelamrobledo  
] 

Este domingo 27 de octubre Bogotá hizo historia al elegir a una mujer, la primera en su historia, como alcaldesa con la mayor asistencia en las urnas y la más grande cantidad de votos a favor que se han registrado en la capital. Este hecho marca un precedente de cara a lo que puede significar esta elección frente a los comicios presidenciales de 2022, y cómo se configura el poder en torno a candidaturas con una agenda 'de género'. (Le puede interesar: ["Sin desmonte del paramilitarismo no habrá Paz con legalidad: Ángela María R."](https://archivo.contagioradio.com/politica-paz-con-legalidad-huele-a-impunidad-angela-maria-robledo/))

**¿Qué significa tener a una mujer como Claudia frente a la Alcaldía?**

Para Ángela María Robledo, una de las líderes que se sumó a la campaña de López, lograr la alcaldía "es verdaderamente un salto histórico para las mujeres", que llegan al segundo cargo de elección popular más importante del país, detrás del presidente. Robledo dijo que era una oportunidad para volver a asumir la educación como el camino fundamental, y un avance en el anhelo materializado de ver a una mujer diversa, comprometida con tener un gabinete idóneo, transparente y que cumplirá los acuerdos establecidos con sectores ambientalistas, de jóvenes y mujeres.

Robledo señaló que López asume una ciudad con enormes retos y mucho malestar ciudadano, pero anunció que también las mujeres que se sumaron a su campaña estarán ahí para acompañarla, y asegurarse que se cumpla con la agenda propuesta. Asimismo, resaltó la elección al Concejo, que en general renovó a muchos de los cabildantes y dió lugar a un espacio en el que hay más mujeres, personas jóvenes y sectores diversos.

### **El rechazo a la guerra, el apoyo al Acuerdo de Paz** 

Robledo resaltó que los sectores alternativos también triunfaron en Medellín, Cali, Villavicencio y Manizales; en estos casos, afirmó que "es un mensaje para las y los señores de la guerra, de que este país está cansado de vivir la confrontación", al tiempo que es una invitación para que el presidente Duque se comprometa con implementar el Acuerdo de paz y apoyar su consolidación en los territorios.

En el caso de Bogotá, expresó que llegará a la Alcaldía un mujer con una resolución firme para que Bogotá sea un espacio de paz. La líder también destacó el hecho de que si las dos propuestas alternativas para Bogotá se hubiesen unido (Morris y López), el caudal electoral habría alcanzado el millón y medio de votos, lo que "está dando una señal a Colombia de cara a las elecciones de 2022. (Le puede interesar: ["«Tejeremos la bancada de oposición» Ángela María R."](https://archivo.contagioradio.com/tejeremos-la-bancada-de-oposicion-angela-maria-robledo/))

### **Necesitamos que el poder en Colombia sea femenino, transformador e incluyente: Robledo  
** 

Al tiempo que Robledo enfatizó que la elección de López es un salto histórico en Bogotá para las mujeres, también es un recordatorio frente a que no es suficiente tener cuerpo de mujer para representar sus intereses. En consecuencia, aseguró que se necesitan mujeres que en su agenda no solo recojan a otras sino también a los inmigrantes, víctimas y en general, excluidos,  porque "hay mucha gente que dice que donde hay una mujer con agenda de mujer, hay agenda para todos".

De cara a los próximos comicios presidenciales de 2022, Gustavo Petro y Sergio Fajardo ya han puesto sus nombres a consideración como candidatos. Pero Robledo llamó a que las candidaturas se resuelvan mediante una consulta, y con procesos democráticos al interior de los movimientos políticos y partidos, en los que también se tomen en cuenta las voces de mujeres, porque concluyó que "necesitamos que el poder en Colombia sea femenino, transformador e incluyente".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44016123" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_44016123_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
