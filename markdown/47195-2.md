Title: Colombia entre la guerra y la paz: Timoleón Jiménez
Date: 2017-09-26 17:14
Category: Nacional, Paz
Tags: acuerdo cese al fuego farc y gobierno, FARC, Incumplimientos acuerdos
Slug: 47195-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/timoleon-jimenez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [26 Spet 2017] 

En una carta, el máximo jefe del partido Fuerza Alternativa Revolucionaria del Común, Timoleón Jiménez, le expresó al presidente Juan Manuel Santos su preocupación por la falta de implementación de los acuerdos de paz que **está poniendo en riesgo la vida de los ex combatientes y ahora militantes de las FARC** y que hacen parte de incumplimientos que vienen dándose desde el inicio, por parte del gobierno.

En el texto Jiménez señaló los acuerdos que se pactaron como la salida de prisión de los presos políticos, la suspensión en todo el territorio de las órdenes de captura de los integrantes de las FARC-EP que hayan dejado las armas, las reformas constitucionales para garantizar el funcionamiento de la Unidad Especial de Investigación, **entre otras, hasta el momento no se han tramitado y se transforman en incumplimientos a la paz**. (Le puede interesar:["Implementación de los acuerdos de paz no va por buen camino"](https://archivo.contagioradio.com/hay-crisis-en-la-implementacion-del-acuerdo-de-paz/))

“Igual que en todo, las FARC-EP cumplimos sagradamente con la dejación de armas. ¿Qué explica entonces la desidia oficial para honrar a su vez la palabra empeñada?”, es la pregunta que el máximo jefe de las FARC le hace a Juan Manuel Santos y manifestó que hasta el momento tampoco se ha aprobado alguno de los proyectos colectivos, **ni se ha brindado el subsidio de 8 millones para los ex combatientes.**

### **El Congreso de la República y las trabas a los Acuerdos de Paz** 

Sobre la lentitud con la que se ha llevado acabo el mecanismo de Fast Track, en el Congreso, Jiménez afirmó “El Congreso de la República se enreda en la expedición de las normas sobre participación política y jurisdicción especial de paz, mientras que el señor Fiscal General de la Nación dirige una campaña de difamación contra las FARC, e **ignora sospechosamente los más de quince mil expedientes que por paramilitarismo se apiñan en su dependencia**”.

Estas situaciones han provocado inconformidad e indignación, de acuerdo con Timochenco, en la familia Fariana. De igual forma, **señaló que Colombia sigue presenciando un momento histórico en donde se puede** “enrumbar por los caminos de la paz, la democracia y la justicia social trazados por los Acuerdos de La Habana, o se hunde en el piélago de la violencia como consecuencia de su violación e incumplimiento”.

Finalmente hizo un llamado a la sociedad colombiana para que salga a la defensa de los acuerdos y con ello de la paz, **para Colombia y se de la posibilidad de cerrar un ciclo de violencia** que permita a las nuevas generaciones construir un país diferente." (Le puede interesar:["Hay crisis en la implementación de los Acuerdos de Paz"](https://archivo.contagioradio.com/hay-crisis-en-la-implementacion-del-acuerdo-de-paz/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
