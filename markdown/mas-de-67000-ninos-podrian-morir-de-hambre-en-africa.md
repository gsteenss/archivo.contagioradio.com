Title: Más de 67,000 niños podrían morir de hambre en África.
Date: 2020-09-03 19:28
Author: AdminContagio
Category: El mundo
Tags: África subsahariana, Desnutrición Infaltil, Save The Children
Slug: mas-de-67000-ninos-podrian-morir-de-hambre-en-africa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/ninos-con-desnutricion-en-Africa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Unicef West and Central Africa

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La organización [Save The Children](https://www.savethechildren.net/news/sub-saharan-africa-426-children-day-risk-death-hunger-following-impact-covid-19) advirtió que cerca de 67,000 niños están en riesgo de morir de hambre extrema en el África subsahariana antes de que finalice este año 2020. Además, se estimada que para el 2030, alrededor de 433 millones de personas estarán en condición de desnutrición en África. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con datos tomados de la revista británica, The Lancet, **un promedio de 426 niños estarían en riesgo de morir diariamente a menos que se tomen acciones urgentes e inmediatas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Covid-19 ha hecho más difícil poder acceder a los servicios de salud, pero también ha reforzado la crisis económica y en consecuencia, muchos padres y madres no cuentan con los recursos para alimentar a sus hijos. (Le puede interesar: [«Reforma laboral por decreto» denuncian gremios y sectores políticos](https://archivo.contagioradio.com/reforma-laboral-por-decreto-denuncian-gremios-y-sectores-politicos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aún así, además del coronavirus, otros factores que se han presentado este año como como inundaciones, desplazamientos y un alza en los precios han hecho que se agrave la inseguridad alimentaria. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ubah, madre de 6 niños en Somalia, comentó a Save The Children **“la vida es dura para mi y mi familia, yo solía trabajar duro y podíamos sobrevivir. El coronavirus ha causado que nuestra situación empeore. Antes de recibir ayudas y apoyo, solo comíamos por la mañana. Yo he tenido que ver a mis hijos irse a la cama con hambre. El peor sentimiento para una madre es cuando no puedes alimentar a tus hijos”.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, Ian Vale, director regional de Save the Children en África Oriental y Meridional, advierte que “ya se están viendo los devastadores impactos de este virus \[...\] muchos padres ya no pueden poner comida en las mesas para sus hijos”, agregando que cada día son más los casos que reciben en los centros médicos.

<!-- /wp:paragraph -->

<!-- wp:heading -->

No solo África se encuentra en riesgo
-------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ante esta situación, Save the Children ya está proporcionando alimentos y dinero a las familias más vulnerables, garantizando el acceso a agua potable y manteniendo los programas de salud y nutrición. Además, ha pedido a gobiernos y donantes ayudar a la infancia más pobre y vulnerable del mundo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, es una situación que no es nueva y que no solo está viviendo el África subsahariana; en el estudio publicado por The Lancet, la desnutrición se está viendo agravada en niños de países de ingresos bajos y medios. Una de cada diez muertes entre niños menores de 5 años se atribuye a la emaciación (peso inferior al que corresponde a la estatura) pues estos tienen mayor riesgo a presentar enfermedades infecciosas. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y antes de la pandemia se estimaba que 47 millones de menores de 5 años tenían emaciación moderada o grave, la mayoría en África subsahariana y el sur de Asia. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con el Programa Mundial de Alimentos, el número de personas de estos países que enfrentarán situaciones de crisis alimentaria casi se duplicará a 265 millones al final del 2020.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ahora con la pandemia y todas las problemáticas que esta representa se espera que la situación se exacerbe tanto en África como en los países de ingresos bajos y medios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**“En 2017, el Banco Mundial estimó que se necesitan \$7 mil millones por año durante 10 años para alcanzar las metas mundiales de nutrición de los Objetivos de Desarrollo Sostenible. Estas estimaciones deben revisarse para superar los contratiempos relacionados con el COVID-19”.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

Los niños wayúu en Colombia también están en riesgo de desnutrición
-------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según un[informe realizado por Human Rights Watch y el Centro de Salud Humanitaria de Johns Hopkins](https://www.hrw.org/es/news/2020/08/13/colombia-ninos-indigenas-en-riesgo-de-desnutricion-y-muerte) los niños y niñas wayuu de Colombia también están sufriendo de desnutrición y la situación se podría estar agudizando con la pandemia.(Le puede interesar: [Pandemia profundiza riesgo de desnutrición en indígenas wayús](https://archivo.contagioradio.com/pandemia-profundiza-riesgo-de-desnutricion-en-indigenas-wayus/)).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, de acuerdo con estadísticas oficiales solo 4% de los wayuu que viven en zonas rurales de la región de la Guajira, tienen acceso a agua limpia y los que residen en zonas urbanas reciben un servicio irregular. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El estudio indicó que la desnutrición está siendo causada por la inseguridad alimentaria e hídrica, los obstáculos para el acceso a la atención médica, la corrupción y a las empresas de minería que adelantan actividades en el departamento.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
