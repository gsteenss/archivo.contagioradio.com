Title: Campesinos de Jericó le cierran la puerta a Anglogold Ashanti
Date: 2017-04-12 17:47
Category: Animales, Nacional, Voces de la Tierra
Tags: Anglogold Ashanti, Jericó Antioquía
Slug: campesinos-de-jerico-antioquia-se-movilizan-contra-extractivismo-en-su-territorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/1200px-PanorámicaJardínAntioquia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [12 Abr 2017] 

Campesinos del municipio de Jericó, en Antioquia, impidieron la movilización de dos camionetas con 10 miembros de la multinacional Anglogold Ashanti para **impedir que se continúe con la explotación que ya ha afectado fuentes hídricas en el territorio**.

Desde el año 2008 la multinacional hace presencia en el territorio con plataformas de exploración y desde entonces los habitantes de Jericó han expresado los daños ambientales que genera. Además, señalan que la Anglogold Ashanti t**iene un proceso sancionatorio abierto, ante Corantioquia, por ubicar una plataforma a 15 metros de una zona de protección** según el esquema de ordenamiento territorial del municipio.

De acuerdo con Fernando Jaramillo “algunas fuentes de agua subterránea fueron afectadas mermando los flujos que abastecen los acueductos del corregimiento de PaloCabildo, **algunas lagunas terminaron perdiendo sus aguas y 4 fuentes hídricas que abastecen otros acueductos han disminuido sus caudales**”. Le puede interesar. ["El Cerrejón buscaría desalojar a 27 familias Wayúu para desviar el Arroyo Bruno"](https://archivo.contagioradio.com/cerrejon-buscaria-desalojar-27-familias-wayuu-desviar-arroyo-bruno/)

Los habitantes también denuncian que en algunos lugares donde hay perforación ha salido agua de manera permanente desde hace 5 años, **por lo que consideran que el taladro perforó un acuífero subterraneo** y esta situación estaría afectando las dinámicas hidrológicas. Le puede interesar: ["La polémica detrás del proyecto de Ecopetrol en Guamal, Meta"](https://archivo.contagioradio.com/la-polemica-detras-del-proyecto-ecopetrol-guamal-meta/)

Además, Jaramillo denunció que “**la empresa desde que llegó empezó un proceso de cooptación de las comunidades a través de regalos**, de injerencia en las instituciones educativas en donde ofrecen cuadernos, bolsos, incluso en algunas son los profesores ambientales” lo que ha roto un proceso social consolidado desde la fundación del municipio.

“Jericó tiene unas características de unidad de identidad profunda con su territorio, cultura y tradiciones, somos familias **que vienen desde la fundación de los pueblos  y la empresa llega rompe el tejido social**”. Le puede interesar: ["Gobierno miente, decisión de consulta popular debe cumplirse"](https://archivo.contagioradio.com/gobierno-miente-decision-de-consulta-popular-debe-cumplirse/)

El próximo martes en una de las canchas de la vereda, se dará una reunión entre los campesinos, integrantes de la multinacional y el Alcalde para hablar de las afectaciones de la exploración en el ambiente. Sin embargo, los campesinos señalaron que están contemplando la posibilidad de emprender un proceso de **consulta popular y modificar los esquemas o planes de ordenamiento territorial en los que se reglamenta el uso del suelo**.

<iframe id="audio_18120169" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18120169_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
