Title: Comunidades denuncian judicialización injusta contra lideresa Jani Silva
Date: 2016-08-16 12:03
Category: DDHH, Movilización, Nacional
Tags: Amerisur en Putumayo, judicialización Jani Silva, manifestación contra Amerisur, Zona de Reserva Campesina Perla Amazónica
Slug: comunidades-denuncian-judicializacion-injusta-contra-lideresa-jani-silva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Zona-de-Reserva-Campesina-Perla-Amazónica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [16 Ago 2016 ] 

De acuerdo con la Comisión Intereclesial de Justicia y Paz, el pasado sábado, varios de los habitantes de los caseríos de La Alea y La Rosa, de la Zona de Reserva Campesina Perla Amazónica, quienes se movilizan contra la explotación petrolera en la región, fueron notificados de una citación judicial de la Fiscalía 12 Especializada de la Dirección Nacional Contra el Terrorismo, por el **delito de obstrucción de la vía y afectación del orden público**. Entre los citados están la lideresa Jani Silva, quien es la representante legal de ADISPA e integrante de la dirección de CONPAZ.

<div style="text-align: justify;">

Según los pobladores, Jani Silva se ha destacado en la defensa de los derechos humanos y ambientales, en el marco de las violaciones e impactos negativos que ha causado la [[acción de la empresa petrolera Amerisur](https://archivo.contagioradio.com/?s=amerisur+)] en la Zona de Reserva Campesina, por lo que temen que la judicialicen injustamente; teniendo en cuenta que el pasado domingo 7 de agosto fue **retenida por militares de la Brigada 27 cuando transitaba por el caserío Ancurá**, lugar de concentración de las comunidades.  
Este lunes, miembros del Ejército se dirigieron a las comunidades del corregimiento de Piñuña Blanco, para asegurarles que "los carros de las empresas petroleras van a pasar por encima de quien sea" si continúan con la movilización; así mismo tomaron registros fotográficos de varios de los pobladores, quienes insisten en seguir **manifestándose contra el accionar de la petrolera Amerisur en la región** y en exigencia del cumplimiento de los acuerdos que fueron pactados con el Gobierno el pasado 22 de mayo.  
Las comunidades denuncian el despliegue de más de 300 efectivos militares en los predios de la Zona de Reserva Campesina, pese a que en la Alea, la Viceministra Sandra Devia Ruiz se comprometió a garantizar el respeto de los derechos en el marco de la protesta pacífica que adelantan los pobladores, a quienes les preocupa la **persecución judicial de varios de los líderes porque se estaría criminalizando la protesta legal** y legitima contra los daños ambientales y sociales que ha generado Amerisur.
</p>
###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]

</div>
