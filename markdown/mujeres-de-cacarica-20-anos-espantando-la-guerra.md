Title: Mujeres de Cacarica: 20 años espantando la guerra
Date: 2016-08-31 11:58
Category: Reportajes
Tags: cacarica, Chocó, colombia, mujeres, paz
Slug: mujeres-de-cacarica-20-anos-espantando-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Caminata-por-la-paz-201.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo- Contagio Radio 

#### **Por. Silvia Arjona Martín** 

Ana del Carmen Martínez lleva 19 años luchando por la paz con justicia social en su territorio. Por obligación, tuvo que armarse de valor ante el desplazamiento forzado que su familia y sucomunidad sufrieron en febrero de 1997, cuando la Operación Génesis tomaba forma ante la incomprensión de las personas afectadas.

El Ejército, junto con grupos paramilitares, se hicieron con el control del Chocó por los muchos intereses que esta zona tiene para el capital: monocultivos, proyectos extractivistas, ampliación de la carretera Panamericana, instalación de bases militares, etc. La fórmula para ejercer dicho control fueron las amenazas, la humillación y el asesinato a quien se pusiera delante con tal de conseguir los fines planificados. Marino López fue decapitado y su cabeza utilizada como balón de fútbol por unos militares deshumanizados y corrompidos por la codicia y el mal.

Hasta hoy todavía no se ha logrado la reparación integral de las víctimas ni la devolución de los territorios asaltados impunemente, a pesar de que la Corte Interamericana de Derechos Humanos sentenció en noviembre de 2013 al Estado colombiano responsable de dicha operación militar.

Es por ello que unas 80 personas venidas de distintos puntos de Colombia y del mundo peregrinaron durante cinco días a finales de febrero por el Urabá chocoano para conmemorar los 19 años de esta masacre. Gentes de distintas Zonas Humanitarios del Departamento del Chocó, también de las Comunidades Construyendo Paz en los Territorios (CONPAZ), así como de otros países como Chile, España, Alemania, Canadá y Estados Unidos caminaron por la selva hasta la base militar panameña-colombiana situada en Cerro Mocho, ya en la frontera entre los do países. El objetivo, recordar la historia y reclamar justicia social y ambiental tras los desplazamientos forzados y asesinatos que marcaron un antes y un después en esta zona campesina del país.

Para Ana del Carmen es un momento especial. Camina con energía durante todo el recorrido. Transporta en su cabeza lo que necesita para cinco días intensos en medio de la selva, mantiene una pose erguida mientras sus ojos no quitan la vista del suelo con el objetivo de evitar tropiezos ante tanta vegetación silvestre. Lleva botas de goma, como buena campesina, a pesar de las altas temperaturas del lugar. Un trapo que hace las veces de espantamoscas, secador de sudor y ventilador, se convierte en su inseparable compañero de viaje. Lo va moviendo de izquierda a derecha a la altura de sus hombros y cabeza, removiendo así el aire denso que se concentra en el interior de la selva para refrescarse por momentos. Pareciera también que con él logra espantar todo “lo feo”, como llama a las cosas malas de su historia y, en parte, de Colombia.

Se la siente muy autóctona y sabe que no necesita muchas cosas materiales en la naturaleza porque ésta le reporta todo. Transita la última de toda la peregrinación de manera tranquila pero constante, aunque al final siempre acaba llegando la primera en cada final de etapa. Es la encargada de hacer el almuerzo y la cena en cada parada que hace la peregrinación y una se pregunta de dónde saca las fuerzas. Mientras cuece el agua para el arroz en ollas comunitarias, va preparando las cebollas que acompañarán el guiso y de seguido le da tiempo a fumarse un cigarrillo. Expulsa el humo con gusto y, en su pose, se la siente muy empoderada.

Como ella, muchas han sido las mujeres que han acompañado la resistencia de las comunidades campesinas, indígenas y afro que han sufrido (y sufren) la violencia del conflicto social y armado de Colombia. “No sé qué me he perdido de estos 19 años”, explica asumiendo que ha estado en todas las batallas cuando empezó aquél 23 de febrero de 1997, cuando tuvo que salir con sus siete “pelaos” con lo puesto si no quería morir entre una lluvia de balas.

Este reportaje sirve para visibilizar su fuerza, su historia y conmemorar su lucha y resistencia por conseguir la justicia social robada desde hace años y que su presente vuelva a tener el sentido que un día tuvo su pasado. Ese pasado en el que, mientras recogía frijoles con sus hijos en el campo, podía mirar el cielo sin temor a ser bombardeados.

Por ella y por todas las mujeres del Chocó que siguen padeciendo los pesares del desplazamiento forzado y luchan por volver a sus territorio, va este reportaje en formato radiofónico…

<iframe src="http://co.ivoox.com/es/player_ej_12720112_2_1.html?data=kpeklJWVdZOhhpywj5aaaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5ynca3V1JDa18_JtsbnjNfS1c7XuMbi1crgjcnJsIy3ydTQh6iXaaOnjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
