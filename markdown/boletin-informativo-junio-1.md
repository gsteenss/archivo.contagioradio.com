Title: Boletín Informativo Junio 1
Date: 2015-06-01 05:30
Category: datos
Tags: Ejército Nacional inició enfrentamientos con integrantes de la Columna Móvil Daniel Aldana de las FARC–EP., Jeferson Tunjano, Paramilitares amenazan a estudiantes de la Universidad Colegio Mayor de Cundinamarca
Slug: boletin-informativo-junio-1
Status: published

[*Noticias del Día:*]

<iframe src="http://www.ivoox.com/player_ek_4580715_2_1.html?data=lZqlkpyVeY6ZmKiakpmJd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-**Paramilitares amenazan a estudiantes de la Universidad Colegio Mayor de Cundinamarca**, quienes desde hace varios semestres han estado al frente del proceso de denuncia por los **malos manejos administrativos y la exigencia de la libre movilización y asociación** al interior de la universidad. **Jeferson Tunjano**, estudiante amenazado.

- Desde las 6 de la mañana del pasado 29 de mayo en el municipio de **Tumaco Nariño**, el **Ejército Nacional inició enfrentamientos con integrantes de la Columna Móvil Daniel Aldana de las FARC–EP**. En medio del hostigamiento **resultaron gravemente heridos dos civiles, entre ellos una menor de 16 años en estado de embarazo, y un campesino de 36 años de edad**. **Alejandro Pantoja**, integrante de la **Red de Derechos Humanos Francisco Isaías Cifuentes**, explica el hecho.

-Un informe del **Washington Post** alarma a la población estadounidense al demostrar que **las cifras de muertes en el país han aumentado considerablemente**. Se asegura que **las fuerzas policiales matan a dos personas o más al día** y que este tipo de hechos se inclinan más hacia las **minorías**.

- Organizaciones sociales y ambientales solicitaron la **revocatoria inmediata del Plan de Manejo Ambiental del Programa de Erradicación de Cultivos Ilícitos con el herbicida Glifosato**, además desaconsejan el uso de otros herbicidas. Habla la abogada **Yamile Salinas**.
