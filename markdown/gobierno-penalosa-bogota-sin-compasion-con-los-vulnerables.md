Title: Gobierno Peñalosa: Bogotá sin compasión con los vulnerables
Date: 2016-02-08 09:24
Category: Opinion
Tags: Bogotá, ESMAD, Peñalosa, protestas
Slug: gobierno-penalosa-bogota-sin-compasion-con-los-vulnerables
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/penalosa-vendedores.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### [**Diana Marcela Otavo**] 

###### [7 Feb 2016] 

[En el mes largo que lleva de mandato, Enrique Peñalosa demuestra que no estaba listo para liderar una ciudad en el siglo XXI, gobierna  más intentando acabar con todas las banderas relacionadas con Bogotá Humana y Gustavo Petro,  que demostrando la conveniencia de su modelo de ciudad para la capital del país.]

[Empezamos esta lista desafortunada de políticas y acciones excluyentes del distrito, con su eslogan “Bogotá Mejor Para Todos”. Este mensaje pasa por [encima del acuerdo 381 de 2009](http://www.alcaldiabogota.gov.co/sisjur/normas/Norma1.jsp?i=36561)que ordena a las entidades distritales incluir en todas sus piezas y medios, palabras que incluyan al género femenino. ]

[Ante esta situación y el silencio de la nueva Secretaria de la Mujer, el colectivo “Somos la generación de la paz” en cabeza de Mariana Hernández y David Racero, radico el pasado 18 de enero  un [derecho de petición](http://www.las2orillas.co/wp-content/uploads/2016/01/Derecho-Peticion-Penalosa1.pdf), que exige la inclusión de lenguaje de género en la comunicación de la alcaldía. Hasta el momento en el que se escribió esta columna  la petición ha sido desviada por varias dependencias sin emitir respuesta oficial por parte de la administración. ]

[A esto le sumamos varios eventos donde el uso desproporcionado de la fuerza ha causado indignación, parece que quienes llegaron al gobierno dejaron en sus casas la humanidad, el respeto por los derechos humanos y el sentido común,  describimos a continuación varios ejemplos:]

[Caso Rubiela Chivará: Llego primero el ESMAD que la fiscalía, disiparon a la fuerza la protesta de un hijo desgarrado por el dolor, cuya única petición era que levantaran el cadáver de su madre del espacio público. Peñalosa afirmo no tener responsabilidad en la falta de atención y advirtió que su [administración no permitiría bloqueos en Transmilenio](http://www.lafm.com.co/bogot%C3%A1-y-cundinamarca/noticias/pe%C3%B1alosa-permitiremos-que-se-#ixzz3zVmgtYfT).]

[Pregunta: ¿Respondería igual si la fallecida hubiese sido su madre, o la madre de uno de sus amigos empresarios? ]

[Caso mujeres en ejercicio de prostitución: [Fueron golpeadas en la plaza de la Mariposa](http://www.elespectador.com/noticias/bogota/controversia-detencion-de-prostitutas-plaza-de-mariposa-articulo-611837) por miembros de la Policía  y conducidas a la UPJ de Puente Aranda cosa que no pasaba desde el 2013.  Junto a ellas, vendedores informales han sido víctimas de batidas por parte también de la policía sin contemplar ningún mecanismo de dialogo previo, como se lo recordaron a Peñalosa el día 4 de febrero cuando una [vendedora de tinto llorando](https://www.youtube.com/watch?v=H8jiaPjMwbM&feature=youtu.be) reclamaba al alcalde por sus maltratos. [Ver video](https://www.youtube.com/watch?v=H8jiaPjMwbM&feature=youtu.be) (Peñalosa es el responsable de la policía de Bogotá)]

[Lo anterior sucedió violando la sentencia [T629 de 2013](http://corteconstitucional.gov.co/relatoria/2013/T-629-13.htm)  de la Corte Constitucional que exige cumplir con el debido proceso y respeto de los derechos fundamentales en los procesos de recuperación del espacio público. ]

[[Caso madres, ICBF, ESMAD,](http://www.eldiariobogotano.com/protestas-por-cierre-de-jardin-infantil-distrital/) hospitales: No le tembló la mano para enviar el ESMAD a las madres que protestaban ante el cierre de un jardín infantil del ICBF y tampoco para congelar el 40% del presupuesto de los hospitales  públicos, ordenarles un ahorro adicional del 20% en sus gastos de operación y anunciar la finalización del modelo actual del programa territorios saludables . ¿Quiénes son los usuarios de los hospitales públicos y de los jardines del ICBF? Los humildes.]

[Sumando lo anterior al desmantelamiento de Canal Capital,  el alza de las tarifas de Transmilenio, negarle la sede nueva a la Filarmónica de Bogotá, no tener en cuenta los diseños del metro subterráneo, proponer llenar de pavimento la reserva ambiental del norte, todos temas que afectan a la población vulnerable, se demuestra que esta administración está muy lejos de ser incluyente y respetuosa de la constitución del 91.]

[Siempre será fácil justificar en los desayunos con los periodistas y empresarios de Bogotá, acciones en contra  de quienes ocupan los lugares más vulnerables de nuestra sociedad, pero sería bueno que Peñalosa considerara que existe el estado social de derecho y que esa facilidad no la encontrara en los juzgados. Somos más quienes pertenecemos a la gente común, trabajadora y de paz que puede exigir en las calles y ante los organismos de justicia que cesen sus desmanes desde el gobierno.]
