Title: Salgar, otro desastre nacional
Date: 2015-05-19 18:56
Author: CtgAdm
Category: Abilio, Opinion
Tags: alvaro uribe velez, La Orquidea, Plan de Desarrollo, Salgar
Slug: salgar-otro-desastre-nacional
Status: published

<div id=":158" class="ii gt m14d6e8873f64964c adP adO">

<div id=":lm" class="a3s">

<div dir="ltr">

###### Foto: Colprensa 

[Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena){.twitter-atreply .pretty-link}

El  lunes 18 de mayo  en horas de la mañana, los medios de información daban cuenta de la avalancha que sepultó a mas de 78 habitantes del corregimiento Las Margaritas del municipio de Salgar y  que produjo, a la fecha de elaboración de esta columna,  un número de 150 desaparecidos. Según cuentan en sus reportes, la quebrada la Liboriana se salió de su cauce por efecto de los torrenciales aguaceros produciendo una avalancha que arrastró todo lo que encontró  a su paso.

Luego de reportar el dolor de la tragedia, ha trascendido también que se sabía desde hace varios años, que esto podía pasar y que nada, como en casi todos los desastres naturales de Colombia,  se hizo para evitarlo. La advertencia está claramente consignada en el Plan de Desarrollo del Municipio, que paradójicamente tienen el lema “Salgar... con responsabilidad”:  “ las riberas de la quebrada La Liboriana y de los ríos Barrosora y San Juan donde hay asentamientos humanos configuran, igualmente, zonas de riesgo por movimientos en masa (deslizamientos, caida -sic- de rocas, desplomes, socavación lateral y avenidas torrenciales) que afectan de manera adversa el bienestar de estas poblaciones” (p.80).

Este plan debió orientar la gestión de gobierno de la alcaldesa, quien además tendría que estar  respaldada por  la gobernación de su departamento  Antioquia y  por el  gobierno nacional durante el período 2012-2015, so pena de responsabilidades disciplinarias.

Un segundo  tema  ha salido a la luz pública por cuenta del twitter del  alcalde de Bogotá Gustavo Petro  es el de la posible vinculación de la violencia con las causas del desastre:  “Arrinconaron al campesinado sobre el agua a punta de fusil y masacres y el agua recupera su territorio”. Las críticas a esta afirmación fueron severas, al menos, en Blu Radio donde se  afirmó que el alcalde busca  culpables donde no los había,  pues  no todo desastre natural  se deben a  causas humanas y menos  tienen que ver  con la violencia.

La categórica afirmación de Petro, requeriría, como es obvio, de una contratación fáctica que responda a las preguntas  sobre el origen de los habitantes del corregimiento Las Margaritas, por los hechos victimizantes que se   han presentado en lugares de origen, como también sobre los hechos de violencia ocurridos en el municipio y sus responsables.

El propio Plan de Desarrollo de Salgar (2012-2015)  presenta 2.266 casos de desplazamiento forzado  en el municipio entre 1997 y 2010 y 408 casos de desplazamiento forzado  de los que Salgar fue receptor  durante este mismo período de tiempo. Efectivamente en marzo de 1997 llegaron a Salgar los paramilitares de las Autodefensas Campesinas  de Córdoba y Urabá a combatir, según dijeron, a todo a aquel que estuviera vinculado con las guerrillas de las Farc y del Eln. Desde ese año hasta 2006, de acuerdo a registros del Banco de Datos del Cinep, asesinaron a 25 personas y  desaparecieron a 6.

Estos datos no hacen  tan descabellada  la arriesgada afirmación de Petro, dado que algún lugar  debieron buscar  refugio las  al menos 408 personas  que llegaron al municipio, provenientes de otros lugares de donde fueron expulsados por la violencia. Una verificación en terreno podría confirmar, quizás, que se albergaron en las riveras de los ríos.

Los datos del desastre anunciado en el Plan de Desarrollo municipal y la hipótesis de la violencia como causa  adicional de la catastrofe “natural”,  dan cuenta del desastre nacional del que no salimos. Las autoridades se auto advirtieron de la amenaza,  ni mas ni  menos que en el plan que orienta el mandato de la alcaldesa actual. No obstante como todo plan en Colombia,  éste es  rico en el diseño de estrategias modernizadoras, pero  la burocracia clientelista que ve el  poder de gobernar solo como un medio para enriquecerse o acrecentar la riqueza, no permite su implementación.

Ante una sociedad civil debilitada  por el terror de la violencia estatal, las veedurías ciudadanas a las que los gobernantes deberían rendir cuentas de la ejecución de los planes de desarrollo, no son más que  buenos propósitos. Este gravísimo hecho es una prueba de ello.    También da cuenta de la  irresponsabilidad rampante de gobernantes como Juan Manuel Santos y exgobernantes como Alvaro Uribe Vélez que solo aparecen  en las regiones amenazadas por los riesgos “naturales”, como en este caso,  cuando  se convierten en lugares de desastre sin  haber hecho lo que les correspondía para  salvar  la vida humana y natural, teniendo todo el poder y los recursos para hacerlo. En este caso, los dos apostaron carrera  para ver quien llegaba primero a mostrarse como el legítimo doliente de esta tragedia, que había podido  evitarse.

</div>

</div>

</div>
