Title: Corte Constitucional reglamentó sentencia en pro de mujeres víctimas del conflicito armado
Date: 2015-02-02 22:42
Author: CtgAdm
Category: Mujer, Otra Mirada
Tags: conflicto armado, Derechos, mujeres, violencia sexual
Slug: corte-constitucional-reglamento-sentencia-en-pro-de-mujeres-victimas-del-conflicito-armado
Status: published

##### Foto: [tulua.gov.co]

El pasado 27 de enero, la Corte Constitucional reglamentó la **sentencia T-025 DE 2004, a favor mujeres víctimas del conflicto armado.**

Por medio de esta sentencia, se hizo el seguimiento a la orden segunda y tercera del auto 092 de 2008, que se refiere al traslado de casos de violencia sexual a la Fiscalía General de la Nación, y a la creación e implementación de un programa de prevención del impacto de género mediante la Prevención de los Riesgos Extraordinarios de Género en el marco del Conflicto Armado y El Programa de Prevención de la Violencia Sexual contra la Mujer Desplazada y de Atención Integral a sus Víctimas.

Debido a la insuficiencia en la respuesta de las entidades responsables de atender y proteger a la población en situación de desplazamiento, la Corte Constitucional se vio obligada a declarar en el 2004 un estado de cosas inconstitucional en materia de desplazamiento forzado por medio de la sentencia T-025. **En esta se evalúa la respuesta por parte de las autoridades responsables de atender a las víctimas** y se dictan nuevas órdenes con el objetivo de superar las falencias existentes en el sistema.

**La Corte se pronunció sobre 108 demandas interpuestas por 1.150 familias** y decidió mantener su atención al seguimiento de esta ley debido a que se necesitan esfuerzos presupuestales, administrativos e institucionales  para llevar a cabo estrategias que restablezcan los derechos a las personas en situación de desplazamiento, y específicamente a **las mujeres, niñas, niños y adolescentes, que se vuelven más vulnerables al ser víctimas del conflicto armado,  y por lo tanto son objeto de constantes violaciones a sus derechos. **
