Title: Macron y Le Pen llegarían al ballotage en elecciones Francesas
Date: 2017-04-20 13:07
Category: El mundo, Otra Mirada
Tags: ballotage, elecciones, francia, Le Pen, Macron
Slug: francia-le-pen-macron
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Untitled-4.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

##### 20 Abr 2017 

El 23 de abril tendrá lugar la primera vuelta presidencial en Francia y los candidatos **Emmanuel Macron** de Centro y **Marine Le Pen** de Ultra derecha han empezado a tomar ventaja en las encuestas **para llegar al ballotage que se realizaría el 7 de mayo**.

Hasta hace pocos días los sondeos de opinión reflejaban un cuádruple empate técnico entre los ahora punteros y los candidatos François Fillon del Partido Conservador y el izquierdista Jean-Luc Mélenchon, **distancias que han variado a tres días de realizarse los comicios**.

Macron, candidato más joven de la contienda con 39 años, ha sumado un punto porcentual durante la última semana llegando a **un 25% de intención de voto sobre el 22% alcanzado por Le Pen** de acuerdo con un estudio publicado este jueves por el Instituto demoscópico Harris Interactive.

Sin embargo, las especulaciones crecen ante el **alto número de indecisos** que según las cifras corresponden a **un tercio de la población** apta para votar, y los temores de los empresarios ante las posiciones radicales de Le Pen, quien en un escenario de victoria **rompería el vínculo con la Unión Europea y Francia abandonaría el euro**, panorama ante el cual las empresas vienen construyendo planes de choque.

El estudio también preve, ante un posible escenario de ballotage, **una vitoria de Macron con 30 puntos sobre la candidata del Frente Nacional**. Resultado que el candidato François Fillon reconoce al declarar que "Le Pen no ganará las elecciones" (ya que pierde en todas las proyecciones de segunda vuelta) y por eso votar por ella "es votar a Macron", a quien considera su principal adversario para llegar al Elíseo.

Con ese discurso **Fillon,  busca recuperar el favor de los electores que ha venido perdiendo dese el inicio de la carrera electoral** por una serie de escándalos de contratos paralamentarios por miles de dólares pagados a su esposa e hijos por labores que no habrían realizado. Le puede interesar: [Trump no es el problema](https://archivo.contagioradio.com/trump-no-es-el-problema/)...

Distante de la postura mesurada y serena que ha pretendido construir Macron, el discurso que analistas han catalogado como populista y euroséptico de **Jean-Luc Mélenchon ha logrado acortar distancias con Fillon, igualandolo con un 19%** de la intención de voto. Un trabajo potencializado por el uso de hologramas  que le han permitido estar en varios lugares a la vez.

**Candidatura de Le Pen en riesgo**

La líder de la extrema derecha francesa t**iene tres juicios pendientes**, de manera personal o en su calidad de presidenta del Frente Nacional. De avanzar estos procesos, **Le Pen podría verse obligada a renunciar a cualquier aspiración electoral** y a ejercer cargos públicos por varios años.

Los procesos, uno por el pago de **342.000 euros** a dos personas que contrato como ayudantes para sus labores como eurodiputada, **dinero desembolsado del presupuesto de Bruselas**. Según las investigaciones, los dos asistentes nunca cumplieron las funciones para las que se les había contratado y por eso desde la capital belga se reclama a Le Pen devuelva el dinero.

En el segundo proceso un Tribunal francés investiga si el Frente Nacional adelantó un operativo para remunerar gente que se hacia pasar como ayudantes de eurodiputados, a los que la candidata aseguró **responderá al finalizar la contienda electoral**, y **por la  presunta financiación ilegal de su partido** en todas las campañas en las que ha participado desde su llegada a la presidencia de la colectividad en 2011.

El último de los procesos tiene que ver con l**a declaración patrimonial de la candidata y de su padre Jean-Marie Le Pen**, en el que tasaron varias propiedades familiares por un valor más bajo del real, que de comprobarse podría acarrearle una acusación por **fraude fiscal agravado**, con una sanción de hasta tres años de cárcel, 45.000 euros de multa y la inhabilidad electoral para cargos públicos durante diez años.
