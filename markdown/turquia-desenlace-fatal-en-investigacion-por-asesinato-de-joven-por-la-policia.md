Title: Turquía: Desenlace fatal en investigación por asesinato de joven por la policía
Date: 2015-03-31 19:26
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: #Gezi, Berkin Elvan, DHKP/C, DHKP/C secuestra fiscal, Protestas Gezi
Slug: turquia-desenlace-fatal-en-investigacion-por-asesinato-de-joven-por-la-policia
Status: published

###### Foto:WorldRiots24h 

###### **Entrevista con [Mikail Diye Biri], politólogo y activista turco:** 

<iframe src="http://www.ivoox.com/player_ek_4294997_2_1.html?data=lZemlp6de46ZmKiakp6Jd6Kol5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8ro1sbQy4qnd4a2lNOYxsqPp8Lj1JDS0JC4udPl1oqwlYqliMKZk6iYxsrXtNaZpJiSo57Xb8XZjMbdw8yJh5SZo5jbjcnJb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El fiscal que investiga la **muerte del joven por una granada lacrimógena disparada por la policía** en las protestas del parque **Gezi en 2013**, fue **secuestrado** y tomado como rehén en el **Palacio de Justicia** de Turquía por el grupo de izquierdas al margen de la ley conocido como el Partido de la liberación o **Frente Popular Revolucionario** ( **[DHKP/C]** ).

El objetivo del secuestro era que la **policía turca reconociera que fue ella quién asesinó** al joven en el contexto de las protestas en 2013, **de no ser así asesinarían al fiscal**.

La **policía entró, al asalto, matando a los secuestradores y liberando al fiscal, que resultó gravemente herido, y que, dos horas más tarde falleció.** En el palacio de justicia se concentraron cientos de personas para exigir una negociación antes del desenlace y la policía cargó fuertemente contra los manifestantes.

**Berkin Elvan, joven turco de 15 años**, fue **asesinado** por una **bomba de gas lacrimógeno** mientras salía de su casa con su familia para comprar pan, en el marco de las protestas contra el gobierno turco de 2013. Tras 269 días en coma fallecía el 11 de marzo de 2014.

Hasta la fecha han declarado 4 policías ante la fiscalía, quienes han declarado su inocencia. El primer ministro, Recep Tayyin **Erdogan declaró** que el joven era militante de una organización de izquierdas **terrorista** al tener un pañuelo en el rostro, debido a la abundancia de gases lacrimógenos en el lugar donde se encontraba.

Después de su muerte y de las declaraciones del primer ministro el **país vivió una dura jornada de protestas** tanto a nivel nacional como en otras ciudades en el extranjero.

Hasta ahora el grupo de izquierdas al margen de la ley [DHKP/C**,**] ha practicado actos terroristas como el **atentado contra la embajada estadounidense en 2013**, y ha sido calificado por la UE, Turquía y EEUU como terrorista.

En el momento en el que Erdogan ha sabido la noticia ha decidió **cancelar** todas las **emisiones televisivas y radiofónicas sobre la toma de rehenes**.

Por último el **padre del joven** asesinado en 2013 pidió la **liberación del fiscal y el procesamiento legal de los policías por la muerte de su hijo.**

Según **Mikail Diye Biri, analista político** y activista turco, **debido al apagón de luz** en todo el país durante las 24 horas del martes, **no se ha podido acceder a la información sobre lo que sucedió**. Además el **primer ministro ha prohibido hablar en los medios de comunicación de masas sobre el secuestro y el fatal asalto de la policía**, que acabo con la muerte del fiscal y de los secuestradores.
