Title: 6.000 inmigrantes rescatados en mares de Italia este fin de semana
Date: 2015-05-04 15:27
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Emergencia inmigrantes en Sicilia, Italia rescata a 6000 imigrantes este fin de semana, MSF y Guardia Costiera rescatan 6000 inmigrantes
Slug: 6-000-inmigrantes-rescatados-en-mares-de-italia-este-fin-de-semana
Status: published

###### Foto:Arrobaradio.pe 

Este fin de semana la **Guardia Costiera italiana** y el equipo de rescate de **Médicos sin Fronteras** rescataron un total de **6.639 inmigrantes, sin emabrgo se presentaros 8 desaparecidos y 2 ahogados** mientras se desarrollaba el operativo.

En total el **domingo fueron rescatados 2.152**, siendo el día en el que más barcazas ilegales llegaron a costas de **Sicilia y en concreto a la isla de Lampedusa**. La gran mayoría fueron rescatados en aguas cercanas a Libia, siendo interceptados mediante los radares de los equipos de socorro italianos.

Conforme se acerca el verano el flujo va en aumento debido al buen tiempo, pero después de la **muerte de más de 1.200 inmigrante**s en las últimas semanas, Francia, España y sobretodo Italia ha reforzado la búsqueda de los inmigrantes con 5 buques de guerra más y 6 botes con preparación para el salvamento.

Todo apunta a que la situación de **caos y postconflcito que se vive en Libia** donde hay un fuerte enfrentamiento entre dos gobiernos, milicias islamistas y mafias que se reparten los flujos migratorios del África subsahariano.

La Comisión Europea ha destinado en la última semana **18 millones de euros para operaciones de salvamento que anteriormente contaban con 3 millones de euros.**

La emergencia humanitaria continua sin ningún tipo de respuesta política por parte de la UE, ni por parte de los países afectados, que en un intento de ocultar la situación están invirtiendo en el rescate y en internamiento forzado y la expulsión de los inmigrantes.
