Title: Base inconsulta en Cerro Mocho, Cacarica
Date: 2016-08-17 10:30
Category: Comunidad, Nacional
Tags: cacarica, comunidades
Slug: base-inconsulta-en-cerro-mocho-cacarica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Caminata-por-la-paz-141.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 14 Ago 2016 

En el año 2014, el gobierno de Juan Manuel Santos instaló una base bilateral en Cerro Mocho,  frontera de Colombia y Panamá,  en territorio colectivo de comunidades negras, indigenas y mestizas del Cacarica, este hecho tiene a las comunidades indignadas por lo que no hubo consulta previa,  lo cual viola la soberanía de un territorio colectivo.

Las comunidades dicen que han visto helicópteros pasar de la base militar con maquinaria muy pesadas que lo cual se cree que lo que llevan ahí  es para extraer minerales del Cerro Mocho.

Hasta este lugar se han realizado dos caminatas, las cuales han tenido paso restringido por los militares de las dos naciones. El objetivo de estas caminatas ha sido el de verificar  que pasa en este base militar.

El 27 de febrero del año 2016 se realizó una de las caminata por la paz hacia Cerro Mocho en la que participaron personas de 23 comunidades y dos zonas humanitarias, acompañadas  por la Comisión de Justicia y Paz y ciudadanos de algunos países como España, Estados Unidos, Argentina, Ecuador, Alemania, Brazil, Honduras.

Por la construcción de esta base y la violación de la libre movilidad en el territorio las comunidades se han sentido muy indignadas  y le da miedo salir de casería porque ese era uno de los sitios donde mas especie de animales habitaban  y al estar la base militar ubicada en ese lugar ya no pueden ir allí.

Las comunidades esperan volver a Cerro Mocho y tener acceso a la base y corroborar si están o no extrayendo minerales en este territorio que pertenece a las comunidades del Cacarica.

[![FERNEY](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/FERNEY-200x150.jpg){.wp-image-27917 .size-post-thumbnail .alignleft width="200" height="150"}](https://archivo.contagioradio.com/base-inconsulta-en-cerro-mocho-cacarica/ferney/)**Farney Murillo Martinez - Comunicador popular de la red CONPAZ - Cacarica**

------------------------------------------------------------------------

###### *Como parte del trabajo de formación de Contagio Radio con comunidades afros, indígenas, mestizas y campesinas en toda Colombia, un grupo de comunicadores realizó una serie de trabajos periodísticos recogiendo las apuestas de construcción de paz desde las comunidades y los territorios. Nos alegra compartirlas con nuestros y nuestras lectoras.* 
