Title: Alerta por fuertes operaciones Neoparamilitares en territorios de Curvaradó y Jiguamiandó
Date: 2019-02-17 11:46
Author: AdminContagio
Category: Comunidad, DDHH
Tags: Comisión de Justicia y Paz, Curvarado, Jiguamiandó, paramilitares
Slug: operaciones-neoparamilitares-curvarado-jiguamiando
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/ç.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 17 Feb 2019 

Según la denuncia de la Comisión de Justicia y Paz, este sábado 16 de Febrero se evidenció la presencia de **por lo menos 20 paramilitares de las llamadas Autodefensas Gaitanistas en el caserío "El tesoro"** en horas de la noche. De igual forma otro grupo de paramilitares hizo presencia en los caseríos de **Llano Rico y Pipón** ubicados a pocos metros de una base militar.

La denuncia de la organización de Derechos Humanos también incluye información sobre **un plan de asesinato en contra de dos líderes** del territorio del Curvaradó y Jiguamiandó, asi como a** defensores que hacen parte de la Comisión de Justicia y Paz** como Danilo Rueda y otras personas que hacen presencia en la región.

Uno de los asuntos que la organización de DDHH resalta como preocupante es que se estaría repitiendo una **operación de "Sitiamiento"** de las comunidades similar a la que se presentó entre 1999 y 2000 **cuando se produjeron varios desplazamientos masivos** que dieron lugar a la entrada de los empresarios que ocuparon el territorio con Palma de Aceite.

Adicionalmente, esta serie de operaciones paramilitares habría surgido como respuesta al reciente **paro armado desarrollado por estructuras de la guerrilla del ELN** que también hacen presencia en la región. Según las versiones de los habitantes del sector los paramilitares los señalan como colaboradores de esa guerrilla.

**Ocupación empresarial que persiste y restitución que no se da**

Desde el desplazamiento forzado y la ocupación ilegal de los territorios colectivos por parte de empresarios palmeros, las comunidades vienen exigiendo sus derechos a la restitución de tierras, sin embargo esto no se ha dado de manera efectiva, entre otras razones, porque las **estructuras paramilitares responsables del desplazamiento siguen ejerciendo el control territorial** **en medio de la presencia de la fuerza pública**.

Uno de los factores agravantes es que recientemente, algunos **funcionarios de instituciones del Estado habrían sido destituidos en medio de acciones efectivas para la restitución de las tierras a sus legítimos propietarios. (**Le puede interesar[: [Actores del conflicto listos para decir la verdad desde las cárceles](https://archivo.contagioradio.com/la-verdad-esta-lista-para-salir-de-la-carcel/))]
