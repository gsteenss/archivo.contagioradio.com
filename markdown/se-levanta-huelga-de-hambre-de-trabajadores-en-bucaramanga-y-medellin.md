Title: Se levanta huelga de hambre de trabajadores en Bucaramanga y Medellín
Date: 2016-11-08 21:56
Category: Movilización, Nacional
Tags: Actividad sindical en Colombia, Huelga de hambre
Slug: se-levanta-huelga-de-hambre-de-trabajadores-en-bucaramanga-y-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/sinaltrainan.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sinaltrainan] 

###### [8 de Nov de 2016] 

**Empresas prestadoras de servicios públicos en Bucaramanga llegaron a un acuerdo con el gobierno de este departamento para levantar la huelga de hambre** que ya cumplía seis días, entre los puntos a los que llegaron las partes, se encuentran respetar y reconocer el derecho a la asociación y libertad sindical.

La huelga se levantó el día sábado sobre las cinco de la tarde, después de dos días de arduas jornadas de negociación, otros de los puntos acordados fueron **generar una planta transitoria de empleados públicos para los trabajadores oficiales que habían sido recalificados por parte del Alcalde y que no habían sido remunerados.**

Además el Alcalde se comprometió a respetar y cumplir todas las normas establecidas en la Constitución y leyes para los trabajadores de Sinaltrainal, estos derechos de asociación sindical serán respaldados por las empresas a partir de la próxima reunión el 10 de noviembre. Para este mismo día se hará un encuentro con el Alcalde para sindicalizar al sector de transporte público de Bucaramanga. **El 29 de noviembre se generará otra reunión sobre temas sociales de los diferentes sectores que hacen parte de la Mesa Laboral y Social.**

Frente a los trabajadores de Coca Cola, el 4 de noviembre l**a Ministra de Trabajo Clara López se comprometió a establecer una mesa de trabajo que converse las problemáticas notificadas por los empleados de esta multinacional**. La huelga de los trabajadores en Medellín se levantó el pasado sábado y esperan al cumplimiento de este escenario de interlocución con el gobierno. Le puede interesar[: "Trabajadores de Coca Cola completan cuatro días en huelga de hambre"](https://archivo.contagioradio.com/trabajadores-coca-cola-completan-4-dias-huelga-hambre/)

<iframe id="audio_13679569" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13679569_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
