Title: Transportadores de carga pesada se suman a la Minga Nacional
Date: 2016-06-07 14:18
Category: Movilización, Nacional
Tags: Minga Nacional, paro camionero 2016, paro transportadores colombia
Slug: transportadores-de-carga-pesada-se-suman-a-la-minga-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Paro-Camioneros-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto: Andres Rojo ] 

###### [7 Junio 2016 ] 

"Los conductores están aguantando hambre, nuestros camioneros están en la quiebra, las empresas de transporte hacen lo que quieren con ellos, y aparte de eso dejaron entrar a la transnacional Impala que vino a acabar con el gremio camionero pagando fletes irrisorios, vetando por cualquier cosa y sacándolos", asegura María Antonia Cuervo, presidenta de la Asociación Colombiana de Camioneros seccional Cundinamarca, quien agrega que **desde este martes los transportadores de carga pesada se declaran en paro pacífico e indefinido**.

Según afirma Cuervo, tras el paro de 2015 se firmó un acuerdo cuyo garante fue el ministro del Interior Juan Fernando Cristo, de este pacto no se ha cumplido ninguno de los puntos. **No se ha establecido el precio del flete de acuerdo con los gastos de la canasta camionera y las tarifas son cada vez más inferiores**; tampoco se ha regulado el cargue de los camiones turbos, que habitualmente circulan en las carreteras con sobrepeso; desde diciembre no han cancelado el dinero de la chatarrización a propietarios de camiones y no se han puesto en marcha el programa de vivienda, ni de regulación del parque automotor.

"Los camioneros están trabajando a pérdida, están en la quiebra, ganan más con los carros guardados en los parqueaderos que transportando (...) **la carga es la misma que hace diez u once años, con el agravante de que el parque automotor se creció y no hay regulación**", denuncia la presidenta, e insiste en que la entrada de la multinacional Impala, que ha tenido que salir de África y Chile por irregularidades en su funcionamiento, ha agravado la [[crítica situación del gremio de transportes](https://archivo.contagioradio.com/camioneros-de-colombia-se-unen-a-minga-nacional/)] en Colombia.

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Maria-Antonia-Cuervo.mp3"\]\[/audio\]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

 
