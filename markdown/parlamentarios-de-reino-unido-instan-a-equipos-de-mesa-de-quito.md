Title: Parlamentarios de Reino Unido instan a Gobierno y ELN a retomar diálogos
Date: 2018-01-26 12:48
Category: Nacional, Paz
Tags: ELN, Gustavo Bell, Mesa de Quito
Slug: parlamentarios-de-reino-unido-instan-a-equipos-de-mesa-de-quito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Eln-gobierno-cese.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 En 2018] 

Trece Parlamentarios del Reino Unido, enviaron una carta al jefe del equipo negociador del gobierno Gustavo Bell y a Nicolas Ródriguez, máximo jefe del ELN, en la que les piden “poner su máximo esfuerzo para lograr un acuerdo de paz”, expresan su preocupación por la **no renovación del cese bilateral y alentaron a los equipos para que continúen en la construcción de este proceso de paz**.

En el comunicado de prensa, manifestaron que unen su voz a la de los diferentes sectores nacionales e internacionales que hacen un llamado a ambas partes de la mesa de Quito, para que retornen a las conversaciones, y afirmaron que es importante que en el **diálogo se construyan criterios para reanudar el cese bilateral que sea cumplido por ambas partes**. (Le puede interesar:["Convencer a  la sociedad, el desafío de la Mesa ELN-Gobierno"](https://archivo.contagioradio.com/eln-y-gobierno-exploran-posibilidad-de-retornar-a-la-mesa-de-conversaciones/))

Los parlamentarios señalaron que el cese al fuego bilateral, que duró 4 meses, **“facilitó la entrega de ayuda humanitaria esencial a las comunidades rurales** y vio un descenso en los niveles de violencia más afectadas por el conflicto”, de igual forma, exhortaron a ambas partes, a que apliquen sin excepciones el principio del derecho Internacional Humanitario sobre la distinción de la población civil.

En ese sentido aseguraron que “se debe poner fin a cualquier acto de violencia o de otra índole, como la siembra de minas antipersonales, que puedan afectar negativamente a la población civil y a su capacidad de acceder a sus cultivos”. Referente a las acciones violentas que se han dado en el departamento de Chocó, instaron a las partes a integrar plenamente el Frente de Guerra Occidental del ELN en el proceso de negociación.

[Carta Para Los Negociadores en Quito (1)](https://www.scribd.com/document/370061207/Carta-Para-Los-Negociadores-en-Quito-1#from_embed "View Carta Para Los Negociadores en Quito (1) on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_93373" class="scribd_iframe_embed" title="Carta Para Los Negociadores en Quito (1)" src="https://www.scribd.com/embeds/370061207/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-QAxg4YytzL1kZsSknmwa&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7055599060297573"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
