Title: Aguas ultra-profundas: la nueva frontera del extractivismo petrolero en Colombia
Date: 2020-11-05 16:01
Author: AdminContagio
Category: CENSAT, Opinion
Tags: Agua, Ambiente, Ambiente y Sociedad, CENSAT, Censat agua Viva, extractivismo
Slug: aguas-ultra-profundas-la-nueva-frontera-del-extractivismo-petrolero-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/foto-ambiente.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: doctor Óscar García / Florida State University. {#foto-por-doctor-óscar-garcía-florida-state-university. .has-text-align-right .has-cyan-bluish-gray-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la Plataforma Intergubernamental de Biodiversidad y Servicios Ecosistémicos (IPBES, por sus siglas en inglés), se ha perdido aproximadamente la mitad de la cubierta viva en los arrecifes de coral desde 1870, y se aceleró la pérdida en los últimos decenios, debido la crisis climática; la razón principal: tanto el uso de combustibles fósiles como la ampliación de los límites de su búsqueda, que incluyen la inmensidad marina. De acuerdo al mismo reporte, *la explotación de petróleo y gas costa afuera se ha ampliado desde 1981 a cerca de 6.500 instalaciones en 53 países (60 % en el Golfo de México en 2003) y probablemente se ampliará a las regiones ártica y antártica a medida que se vaya derritiendo el hielo*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta destructiva saga de explotación se materializó en el Caribe colombiano en 1972, cuando la Texas Petroleum Company (hoy Chevron Texaco), descubre el campo Chuchupa a 26 kilómetros de la costa de Riohacha, que sería el puntal de la ampliación de la frontera extractiva que, en los últimos años, ha avanzado de manera vertiginosa. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los gobiernos de Uribe y Santos promovieron las reformas necesarias para expandir aún más esta frontera, entre otras, con la declaratoria de “Zonas Francas Permanentes Costa Afuera”, que entregan beneficios como los de las zonas continentales, que [según la ANH](http://www.anh.gov.co/Sala-de-Prensa/Lists/Anuncios/Noticias.aspx?ID=254&ContentTypeId=0x01040072853B5EA34F2842806117375623237C), generan una “disminución sobre el impuesto de renta y beneficios de aproximadamente 19%”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, para hidrocarburos extraídos a mil metros o más por debajo de la placa de agua, se pagará solamente el [60% de la tarifa de regalías](https://crudotransparente.com/2016/12/13/investigacion-mensual-octubre-noviembre-2016-crudo-transparente/). Muy pronto, los resultados de estas “dádivas” comenzaron a notarse: los kilómetros de exploración sísmica crecieron exponencialmente, para luego convertirse en descubrimientos que atrajeron el interés de las grandes trasnacionales: desde el pozo Mapalé perforado en 2012 (a 12 km. de las costas del departamento de Bolívar), hasta la configuración de una nueva “provincia gasífera” en frente de los departamentos de Córdoba y Sucre, que sugieren los pozos Kronos-1, Purple Angel-1 y Gorgon-1, y que Ecopetrol anunció en 2017 como el mayor descubrimiento de gas en los últimos 28 años en el país, que “daría tranquilidad en el suministro hasta 2027 (…) de un combustible económico y amigable con el medio ambiente”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Este nuevo descubrimiento inaugura en el Caribe colombiano las posibilidades de explotación de las llamadas “aguas ultra-profundas”, pozos que, como en el caso de los mencionados anteriormente, atraviesan entre 1500 m. y 2300 m. de agua, solo para alcanzar el lecho marino.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los hidrocarburos extraídos de dichas profundidades, en términos del investigador Michael Klare, son energías extremas que “ponen a prueba las posibilidades técnicas en entornos geológica y geográficamente prohibitivos”. Para extraer este gas, se debe perforar a profundidades, temperaturas y presiones extremas, lo que implica una posibilidad más alta de accidentes de todo tipo, con consecuencias tan peligrosas como el gigantesco desastre de la transnacional BP en el Golfo de México en 2010 que derramó más de 900 millones de litros de petróleo y afectó más de 25.000 kilómetros de costas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la actualidad, la participación de los hidrocarburos oceánicos en la tasa de extracción mundial corresponde a cerca del 30% del total, con impactos sobre los ecosistemas que, contrario al caso mencionado, pasan inadvertidos en la gran mayoría de ocasiones debido a la dificultad de verificación por lo inaccesible de sus instalaciones.  Con las posibilidades del desarrollo de este descubrimiento, la reaparición con más fuerza de Shell no es buen augurio para la protección del patrimonio de nuestro Caribe: una corporación con serias denuncias por [contaminación](https://www.amnesty.org/es/latest/news/2015/11/shell-false-claims-about-oil-pollution-exposed/), [abusos laborales](http://www.industriall-union.org/es/los-abusos-de-los-trabajadores-de-shell-abordados-en-el-consejo-de-derechos-humanos-de-la-onu) y [violación de derechos humanos](https://www.rtve.es/noticias/20090506/juzgan-shell-ser-complice-crimenes-contra-derechos-humanos/275346.shtml).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La holandesa adquirió el 50% de la participación en la llamada nueva provincia gasífera, y asumirá el control de la operación. La explotación de Yacimientos No Convencionales (fracking) y la de aguas ultra-profundas, son las principales vías de intensificación del modelo petrolero, que ahora con su disfraz verde de “gas para la transición”, extrae combustibles fósiles que consumen más energía fósil en su proceso extractivo, dejando una menor energía neta disponible para la sociedad, y que, por tanto, serán mayores contribuyentes a la crisis climática en términos absolutos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tan solo en hidrocarburos en el Caribe, la ANH estima un potencial de [9.000 millones de barriles](https://www.portafolio.co/economia/ofreceran-45-bloques-de-exploracion-offshore-500457), más de 3.000 millones de toneladas de dióxido de carbono equivalente: una verdadera bomba de calor en medio de la crisis climática: más combustible para incendios eternos, cuando es imperativo actuar con urgencia, dejar el petróleo enterrado donde debe estar: en el subsuelo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Nuestros Columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
