Title: ¿Qué va a hacer "El Paisa"  en la Habana?
Date: 2016-04-25 13:12
Category: Paz
Tags: FARC-EP, habana, proceso de paz
Slug: que-va-a-hacer-el-paisa-en-la-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/farc-e1461604407528.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Question Digital 

###### 25 Abril 2016 

Durante la última semana arribaron a Cuba diferentes miembros de las FARC-EP que conformarán el equipo dinamizador para debatir los últimos puntos, entre ellos se encuentra Hernán Darío Velásques alias "El Paisa", jefe de la columna móvil Teófilo Forero y uno de los jefes más importnate militares, este hecho  desmiente la división de las FARC.

De acuerdo con Camilo González Posso, ex comisionado de paz y director Indepaz, el papel que podría llegar a cumplir alias "El Paisa" en la mesa de diálogos tiene que ver [con la pedagogía de paz hacía las bases](https://archivo.contagioradio.com/la-pedagogia-para-la-paz-debe-ser-multilateral/)[,](https://archivo.contagioradio.com/la-pedagogia-para-la-paz-debe-ser-multilateral/) y cohesión de las milicias, además dar las últimas puntadas al proceso de organización de las FARC para la dejación de armas y la concentración en zonas especiales , "deben hacerse las reuniones en los campamentos y es necesario consultar a la gente, llevando mensajes de tranquilidad tanto al gobierno como a las milicias.  Las cabezas más duras significan compromiso y generan confianza, esto en términos del ejercicio que se viene que es el cese definitivo y la implementación de los acuerdos, nunca antes vividos en el país".

Por otro lado, González, afirmó que las voces indignadas que han salido a relucir con la participación de alias "El Paisa", no provienen de las víctimas del conflicto armado en el país, considera que esto podría ser un momento de oportunismo que no debe perjudicar al Proceso de Paz.

<iframe src="http://co.ivoox.com/es/player_ej_11299267_2_1.html?data=kpafm56Wepihhpywj5WcaZS1lZWah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Z0ZCrs8%2Fuhqigh6aVsMbnjLXc1djTb46fqrOxp7Wlno6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
