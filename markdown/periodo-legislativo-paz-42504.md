Title: Próximo periodo legislativo será fundamental para la paz
Date: 2017-06-21 16:08
Category: Nacional, Política
Tags: Congreso, Fas Track, Iván Cepeda, Legislación de Paz
Slug: periodo-legislativo-paz-42504
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/debate-congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [21 Jun. 2017] 

A propósito de la última sesión de labores del Senado de la República en este periodo legislativo, el senador Iván Cepeda dijo que hay **buenas y malas noticias**. Se logró aprobar un proyecto que beneficia a los pensionados de Colombia y se avanzó en la aprobación de recursos para la Universidad Nacional, pero **faltó avanzar en la implementación del acuerdo de paz vía ‘Fast Track’.**

Dentro de los logros que resalta el senador en cuanto a leyes por fuera del Fast Track señala dos, “se logró aprobar un proyecto de ley que reduce las cotizaciones en salud para todos los pensionados del 12 al 4%. También le otorgamos unos recursos de 500 mil millones de pesos para gastos a la Universidad Nacional."

En el balance del último día del periodo que se cerró este 20 de Junio Cepeda señala que ** "Vía ‘Fast Track’ solamente se pudo votar el informe de la ponencia de las circunscripciones especiales de paz**, con modificaciones”. Le puede interesar: [Congreso archivó posibilidad de que víctimas estén en centro de Acuerdo Paz](https://archivo.contagioradio.com/congreso-archivo-posibilidad-de-que-victimas-sean-centro-de-acuerdo-paz/)

### **Dificultades del Fast Track y los retos para el próximo periodo** 

Según Cepeda **se siguen encontrando dificultades para avanzar en materia de legislación para la paz** y que seguramente el periodo que viene puede ser mucho más difícil “también hay que decirlo que ya estamos llegando a un punto en el cual comienza a verse el final, por lo menos de la parte sustancial de las leyes de paz”.

En trámite se encuentran 6 iniciativas que podrían ser  aprobadas o no durante las primeras sesiones del siguiente periodo legislativo, dejando aún pendientes 10 normas que “podrían ser aprobadas de una manera acelerada. **Cuando el Congreso quiere puede avanzar rápidamente en normas.** Casi que en 24 horas el Congreso aprobó 50 leyes. Se requiere de voluntad política” recordó. Le puede interesar: [Del Fast Track al Slow Track en la implemetación de Acuerdos de Paz](https://archivo.contagioradio.com/del-fast-track-al-slow-track-en-la-implemetacion-de-acuerdos-de-paz/)

### **Proyectos de Ley en Trámite** 

Para trámite están el proyecto de ley 008 de innovación agropecuaria y la ley de adecuación de tierras, el Acto Legislativo 0012 de reforma política electoral, proyecto de Acto Legisltivo 004 el cual eleva a rango constitucional la prohibición del Paramilitarismo, proyecto de Acto Legislativo 05 circunscripciones especiales de paz, proyecto de ley estatutaria de la JEP y el proyecto de Acto Legislativo de reforma de regalías para la financiación de la implementación del Acuerdo Final.

### **"Si no se estuviera avanzando no habría una reacción virulenta por parte del Centro Democrático** 

Dice el parlamentario que no hay tampoco que creer que el Congreso es "un mundo rosa", pero que no hay que tener posiciones extremistas, haciendo referencia a la oposición que ha encontrado el Acuerdo de Paz en el legislativo. Le puede interesar: ["Crece el riesgo para los Acuerdos de Paz"](https://archivo.contagioradio.com/crece-el-riesgo-para-los-acuerdos-de-paz/)

“Ha habido muchos intentos por menoscabar el acuerdo de paz, pero tampoco es cierto que las cosas estén quietas y no se esté avanzando. **Si no se estuviera avanzando no habría una reacción tan virulenta del Centro Democrático,** lo que les quita el sueño es que paso a paso se viene avanzando” manifestó el Congresista. Le puede interesar: ["Dejar de explicar los acuerdos", la estrategia del Centro Democrático](https://archivo.contagioradio.com/dejar-de-explicar-los-acuerdos-la-estrategia-del-centro-democratico/)

### **Objeciones del Gobierno a las leyes aprobadas en el Congreso** 

Ante las declaraciones del Ministerio de Hacienda, en las que ha asegurado que no hay dinero para poder dar cabida a las leyes aprobadas en el Congreso como la disminución del aporte para los pensionados o de las horas extras nocturnas, Cepeda asegura que está casi seguro que será rechazada esa posición de manera unánime por los pensionados y los sectores sociales.

**“Esta ley tiene una marca social. Si se producen objeciones el Congreso tendrá que ratificarse** y sino la vía es como siempre la movilización y el imponer por la vía de la actuación política de los sectores sociales el respeto a los derechos” afirmó el legislador. Le puede interesar: [Implementación de los Acuerdos requiere movilización social: Iván Cepeda](https://archivo.contagioradio.com/implementacion-de-los-acuerdos-requiere-movilizacion-social-ivan-cepeda/)

<iframe id="audio_19400268" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19400268_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
