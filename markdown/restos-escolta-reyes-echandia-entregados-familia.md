Title: Restos óseos de escolta de Reyes Echandía son entregados a su familia
Date: 2018-09-10 17:07
Author: AdminContagio
Category: Sin Olvido
Tags: desaparecidos, Palacio de Justicia, restos
Slug: restos-escolta-reyes-echandia-entregados-familia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/IMG_0715-e1536614495509-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión de Justicia y Paz 

###### 10 Sep 2018 

El 7 de septiembre a las 10:00 a.m. se realizó en el Centro Religioso de la Policía Nacional una ceremonia religiosa de entrega de los restos óseos de Jaime Benítez Prieto, quien fungía como escolta del magistrado Alfonso Reyes Echandía durante la toma y retoma del Palacio de Justicia ocurrida el 6 y 7 de noviembre de 1985.

Gracias a la exhumación realizada y a un cotejo de ADN, se logró identificar plenamente los restos de Benítez, que fueron hallados en la fosa común del Cementerio del sur. El Instituto Nacional de Medicina legal y la Fiscalía indicaron a la familia que hace 32 años habían enterrado partes de otras personas y no de su ser querido.

La ceremonia estuvo enmarcada por un protocolo de la Policía Nacional y un acto de memoria acompañado por el Sin Olvido que ha caracterizado los casos relacionados con el Palacio de Justicia, las palabras del comisionado por la Fiscalía para el caso, así como la intervención de familiares de distintas generaciones que conservan su recuerdo.

Su hija Sandra Elizabeth Benítez, se pronunció frente a las inconsistencias de la institucionalidad por los hechos ocurridos en 1985: “me parece muy triste y lamentable ya que fue reconocido en su momento y nosotros teníamos la certeza que mi padre era quien visitábamos (…), es un hecho lamentable para nuestro país que después de tantos años ese sea el proceso más largo que pueda existir dentro de un hecho judicial y que esta verdad que tanto estamos buscando no las estén dando poco a poco nuestros familiares y que las personas que planearon, organizaron, atentaron,ocultaron y silenciaron no hablen, cada gobierno de turno no le da importancia para buscar la verdad y la justicia que es este lamente suceso del palacio de justicia”.

Sus palabras evidenciaron el dolor, la indignación que representa no solo la falta de compromiso y las irregularidades estatales, y también la fuerza que surge en la búsqueda de la verdad, la justicia y del amor por su padre al que recuerda como “…una persona trabajadora, muy humana y muy dada a las personas y su familia”.  Luego de sus palabras, la hija y la nieta de Sandra le homenajearon interpretando la canción de la alegría.

La tumba que la familia había preparado al cuerpo de Jaime es la evidencia de las irregularidades por parte de los funcionarios que hicieron el levantamiento después de la toma y retoma del Palacio, el fiscal especializado de la Corte Suprema de Justicia encargado de la entrega indico que: “los errores que se cometieron en 1985 son producto de la deficiencia de criminalística (…), esa deficiencia llevo a que en 1985 se entregaran cuerpos que no correspondían con la verdadera victima”.

\
