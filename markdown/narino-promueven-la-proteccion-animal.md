Title: Nariño se une a los departamentos que promueven la protección animal
Date: 2019-09-05 17:22
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Animales, nariño, política ambiental, protección animal
Slug: narino-promueven-la-proteccion-animal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/EDZLaoTX4AEV2qM.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ContagoRadio] 

La Asamblea de Nariño, aprobó el proyecto de ordenanza presentado por la Gobernación,  que busca una **política de protección y cuidado animal**, dentro de la que se reconoce la importancia de las abejas, elementales para el equilibrio del ecosistema mundial, y las cuales se han visto altamente afectadas por procesos de explotación forestal y consumo de miel, que la convierte en el primer departamento de Colombia en implementar esta medida.

Natalia Parra, directora de la Plataforma Colombiana por los Animales ¡ALTO!, afirma que **con Nariño, ya son cuatro los departamentos que se suman esta política después de Antioquia, Bolívar y Boyacá,** "en Bolívar tuvimos que ser más constantes, en la costa siempre hay resistencia con estos temas por los gremios de producción de carne, que lideran esta zona, sin embargo, se logró también instaurar  un centro de protección animal.(Le puede interesar:[Así está Colombia en materia de protección animal](https://archivo.contagioradio.com/asi-esta-colombia-en-materia-de-derechos-de-los-animales/))

Este logro, pretende **beneficiar la gran diversidad de fauna que incluye no solo animales de compañía como perros y gatos, sino también aquellos que habitan de manera salvaje**, "Nariño conecta con una parte amazónica y además tiene costa, en tema de fauna es, completa, zorros, erizos, ardillas, tigrillos, venados y serpientes” señala Parra.

La política de protección animal, fue liderada por Camilo Romero, gobernador de Nariño e incluyó temas como esterilización para evitar el aumento de la población de algunas especies, apicultura amable con la vida animal y natural, conservación de fauna silvestre, mantenimiento adecuado de animales de consumo y el sostenimiento  responsable de animales de compañía.

Tras conocerse el impacto que generaría la extinción de una especie como la abeja, **Colombia generó un proyecto de ley para la defensa de los polinizadores, fomento de cría de abejas y desarrollo de la apicultura.** La iniciativa pretende proteger a estos insectos, teniendo en cuenta que en los últimos tres años han muerto el 34% del total de abejas debido a envenenamiento masivo con agrotóxicos, según la organización Abejas Vivas.

Con respecto a temas controversiales como l**a tauromaquia y las peleas de gallos, la política busca cambiar  conductas de maltrato.** “En Nariño no hay audiencia tan fuerte para las corridas, sin embargo, las peleas de gallos se realizan y este es un punto donde se busca que la misma comunidad deje atrás esa práctica” señaló Parra. (Le puede interesar: [Protección animal es incluida en el PND](https://archivo.contagioradio.com/proteccion-animal-es-incluida-en-el-pnd-2014-2018/))

**La norma ya está aprobada  y dependerá de la próxima administración** que llegue después de las elecciones de octubre, instaurar y preservar su aplicación, “esperamos que los nuevos gobernantes incentiven la creación de políticas de protección animal, basta con bajar a gastos un poco suntuosos y así poder incluir en los presupuestos a los animales”, afirmó Parra, quien invitó a aquellos departamentos que aún no tienen políticas similares a a instaurarlas en sus planes de gobierno.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
