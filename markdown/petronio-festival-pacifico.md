Title: Con un canto a la mujer del Pacífico vuelve el Petronio Álvarez
Date: 2017-08-17 16:34
Category: eventos
Tags: Cultura, Festival, pacífico, petronio
Slug: petronio-festival-pacifico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Petronio-alvarez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Cali.gov.co 

###### 17 Ago 2017

Del 16 al 21 de Agosto se vive en Cali la XXI edición del Festival Petronio Álvarez 2017, una fiesta incluyente en donde se celebra la vida y cultura Colombiana, que para esta ocasión presenta un homenaje a la mujer con ‘Un canto a la mujer del Pacífico’.

Para este año, la agenda del festival es muy variada, en principio se le rendirá homenaje a cuatro grandes artistas del pacifico: Julia Estrada, de Buenaventura, Zully Murillo de Choco, Eva pastora Riascos de Nariño y Inés Granja de Cauca, mujeres que desde el canto demuestran sus raíces ancestrales y tradiciones culturales.

Como invitados especiales para la presente edición, el Festival contará con la presentación de las agrupaciones Herencia de Timbiquí y Chocquibtown, el sábado  19 y domingo 20 de Agosto respectivamente.

De igual modo, para el día 17 de agosto El Quilombo, un espacio pedagógico abrirá sus puertas con danzas, cuentearía y exposición fotográfica, que se presentara en el Coliseo El Pueblo, en el cual se espera el ingreso de 2.000 personas.

El Festival Petronio Álvarez trae varias sorpresas, entre ellas está la presentación ‘Pario la Luna’, el primer currulao del Pacifico en ser escuchado en la Unidad Deportiva Alberto Galindo Herrera, y la muestra de ‘Mano e Currulao’ (bailar mucho currulao), composición poética del escritor y periodista de Buenaventura Medardo Arias Arizábal, algunas de las muchas actividades que son imperdibles.

Para más información puede consultar la [página oficial](http://www.cali.gov.co/petronio/programacion)de Festival Petronio Álvarez 2017 y conocer los horarios y demás actividades que trae este gran evento.
