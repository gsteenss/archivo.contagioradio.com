Title: GrilloZambo & La Zamblues, vodevil bogotano en "A la Calle"
Date: 2015-08-25 09:00
Category: Sonidos Urbanos
Tags: "Grillozambo y la Zamblues", Blues bogotano, uan Guillermo Rojas (GrilloZambo)
Slug: grillozambo-la-zamblues-vodevil-bogotano-en-a-la-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/grilloza.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

<iframe src="http://www.ivoox.com/player_ek_7513662_2_1.html?data=mJqelZuado6ZmKial5uJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi9PdzdHcvMbRptCfhpejjbHFb7vVzsfZ18rXb8bijIqfmaaPsMKfpMbZzsqJdpihhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### ["Grillozambo y la Zamblues" en "A la Calle"] 

###### [24, ago, 2015]

"***Grillozambo y la Zamblues***" es una agrupación bogotana, que experimenta con sonidos mestizos: blues, swing, vodevil rock, rockabilly, funk, R&B, soul, reggae y algunos elementos del[ folkclore colombiano, describiendose como "Artesanos cacofónicos, diablos del vodevil, guitarra de madera sorda".  
]{.text_exposed_show}

\[caption id="attachment\_12648" align="aligncenter" width="2048"\][![grillozambo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/grillozambo-e1440437825685.jpg){.wp-image-12648 .size-full width="2048" height="1003"}](https://archivo.contagioradio.com/?attachment_id=12648) Carlos Tapia (Nesta) - Fotografía y Prensa\[/caption\]

El orígen de la banda se remonta al [proyecto solista del cantautor **Juan Guillermo Rojas** (GrilloZambo), quien buscaba fusionar músicas del mundo con los sonidos del blues, el soul y el funk. En el 2015 se le unirían **Daniel Felipe Parra** (teclados, Melódica, y coros), **Juan Germán Moreno** (batería y percusión menor), **Nicolás Guevara** (babybass-bajo, coros) **Segio "Gafas" Laverde** (flauta traversa - Melódica y voz), buscando performances en vivo más contundentes a través de la interpretación instrumental y los arreglos en formato rockabilly. La más reciente incorporación de la banda es **Luz Cortés**, en el rol de corista.]{.text_exposed_show}

\[caption id="attachment\_12665" align="alignright" width="363"\]![grillozambo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/IMG-20150824-WA0011-e1440437699692.jpg){.wp-image-12665 width="363" height="228"} "Grillozambo y la Zamblues" con Angel de "A la Calle"\[/caption\]

Con influencias que van de Jimmy Hendrix a Robi Draco Rosa, pasando por Chris [Cornell, Eddie Vedder, Eric Clapton, Led Zeppelin, Janis Joplin, y Gustavo Cerati, entre muchos otros, ***"Grillozambo y la Zamblues"***, nos cuentan parte de su historia, recorrido y proyección dentro de la escena blues bogotana.]{.text_exposed_show}

Facebook:[ facebook.com/grillozambo](https://www.facebook.com/grillozambo/timeline?ref=page_internal)

Soundlist: [reverbnation.com/grillozambo](https://www.reverbnation.com/grillozambo)
