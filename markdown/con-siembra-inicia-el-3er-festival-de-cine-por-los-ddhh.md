Title: Con 'Siembra´ inicia el 3er Festival de Cine por los DDHH
Date: 2016-05-23 15:04
Author: AdminContagio
Category: 24 Cuadros
Tags: 3er Festival Internacional de Cine por los DDHH, Diana Arias, Impulsos Films, Pelicula Siembra
Slug: con-siembra-inicia-el-3er-festival-de-cine-por-los-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/p4gacetajul26-15n1photo04.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Fotograma 'Siembra' 

##### [23 May 2016] 

Con presencia de Angela Osorio y Santiago Lozano, directores de la película colombiana 'Siembra', inicia el 3er Festival Internacional de cine por los Derechos Humanos de Bogotá, que para esta edición [propone abrazar](https://archivo.contagioradio.com/asi-luce-la-3ra-edicion-del-festival-internacional-de-cine-por-los-ddhh/) la esperanza, el respeto, la defensa y promoción de la vida en todas sus manifestaciones.

Las luces del Teatro Nacional la Castellana se encenderán a partir de las 7 de la noche de este martes 24, para recibir de manera libre a todos los asistentes que harán parte del evento inaugural en el que se darán cita además Diego Balanta, protagonista del film y la productora Gerylee Polanco.

<iframe src="https://player.vimeo.com/video/133082521" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

'Siembra' será la primera de las [81 películas provenientes de 27 países](https://archivo.contagioradio.com/estas-son-las-81peliculas-del-3er-festival-de-cine-por-los-ddhh/) que hacen parte de la selección oficial, compuesta por largometrajes, documentales nacionales e internacionales, cortometrajes nacionales e internacionales y Animación, 35 de las cuales tendrán su estreno durante el Festival.

Gracias al programa "Cine para todos" las personas en condición de discapacidad visual  y auditiva podrán disfrutar de la función inaugural por medio de la audiodescripción, iniciativa de inclusión que resulta de la alianza con la Fundación Saldarriaga Concha y el Ministerio de Tecnologías de la Información y las Comunicaciones.

Diana Arias, directora del Festival, explicó en entrevista con 24 cuadros, la dinámica del evento de lanzamiento y algunos detalles de la programación, en salas y académica de esta 3ra edición.

<iframe src="http://co.ivoox.com/es/player_ej_11635685_2_1.html?data=kpajlZqafJahhpywj5aXaZS1k5aah5yncZOhhpywj5WRaZi3jpWah5yncaXdwtPOjabWrcLnjJKYqMrXuMrqwtGYxsqPh8rixpDd0dePsNDnjKmxqq2RaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#####  Consulte aquí la programación completa del 3er Festival Internacional de Cine por los Derechos Humanos de Bogotá.

https://www.scribd.com/doc/313571575/Programacion-3er-Festival-Internacional-de-cine-por-los-DDHH
