Title: Comunidades votaron No a la minería en Pijao y Arbeláez
Date: 2017-07-09 17:01
Category: Nacional, Voces de la Tierra
Tags: Arbeláez, consulta popular, Mineria, Pijao
Slug: arbelaez-piajo-consulta-minera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/DEUux0tXUAEmjoV.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Periferia 

###### 9 Jul 2017 

Con una asistencia fluída los habitantes del municipio de [Arbeláez](https://archivo.contagioradio.com/arbelaez_consulta_popular_contra_petroleo/) en Cundinamarca y [Pijao](https://archivo.contagioradio.com/consulta-popular-pijao/) en Quindío votaron en Consulta popular su decisión de no permitir los procesos de exploración y extracción minera en sus territorios.

En Pijao con un potencial de sufragantes de 6073, los habitantes alcanzaron el umbral con un total de 2.673 votos en las 9 mesas instaladas para la jornada, con un porcentaje de 97.76% correspondientes a 2.613 votos por el no y un 0, 97% de 26 votos por el sí. Votos nulos 8 y no marcados 26.

![pijao](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/regs.jpg){.alignnone .size-full .wp-image-43354 .aligncenter width="1226" height="707"}

Por su parte en Arbeláez de los 8872 electores posibles, 4.376 se presentaron y ejercieron su derecho al voto según informaron las 10 mesas instaladas, con un 98.54% correspondientes a 4.312 votos por el no y un 0.87% para 38 votos por el sí. Votos nulos 9 y no marcados 17.

![Arbeláez](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/DEUzEc8XgAAUpgC.jpg){.alignnone .size-full .wp-image-43355 .aligncenter width="588" height="321"}

En desarrollo...
