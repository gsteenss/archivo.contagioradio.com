Title: Cabalgando sobre la guerra
Date: 2017-04-27 09:58
Category: Camilo, Opinion
Tags: acuerdo farc y gobierno, pax neoliberal, Proceso de paz con el ELN
Slug: cabalgando-sobre-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/cabalgando.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Por[Camilo de Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)**

###### 27 Abr 2017 

Pastrana y el otro ex, navegan sobre el discurso de una "justicia" sin justicia, la de siempre, aquella que condena a la mayoría de empobrecidos, luchadores sociales y ambientales, a la auténtica oposición política civil y armada,  a los lugares donde se pretende aleccionar la mente, el alma y el cuerpo en hacinados barrotes.

Para ellos, los ex y Santos, esa justicia representa sus privilegios protegidos sin barrotes de frío, la venganza, el resort, las casas fiscales, los campos deportivos y demás.

Navegan los ex sobre la impopularidad de un presidente que nunca traicionó a su clase social ni a sus intereses como lo quiso sostener. Santos traicionó el método militar y concedió una posibilidad de aparato de justicia independiente. En el fondo con el diálogo político y el Acuerdo con las FARC EP, Santos despejó el camino de la seguridad para la inversión, lo que los ex buscaban.

Menos costoso un Acuerdo que una guerra prolongada por otros 20 años, objetivo estratégico también con el ELN y con el EPL, así no se le reconozca. Pero de otro lado, para nosotros terminar el conflicto armado con los disidentes es fundamental para romper muchos de los espejismos que se construyeron en estos 70  últimos años de violencia socio política para crear una cultura política.

Los ex reaccionan por vanidad, dejaron la tarea inconclusa ni por el diálogo ni por la aniquilación física lograron la condecoración de la seguridad inversionista y por eso piden la bendición de Trump.

Santos dio un salto cualificado en el acuerdo con las FARC EP para lograr su Pax Neoliberal, con la vanidad propia de su ego estimulado con un indecente nobel de paz excluyente. Ellos, todos ellos, son la expresión del mismo modelo económico que desean el país con más o menos reformas. Casi seguro es que la "bondad" de Francisco, que vendrá a Colombia los sentará con un gran facilitador, el arzobispo castrense y encargado de la visita, para que sigan hablando sobre sus vanidades.

Demeritar lo que han logrado en el Acuerdo las FARC EP es ciego, reconociendo sus alcances y limitaciones. En todos los puntos del Acuerdo están algunas de las apuestas de las FARC EP, y también, las apuestas de muchas voces del sector variopinto de la sociedad colombiana.

Pasar de una Pax Neoliberal de sometimiento a una Pax Neoliberal con mecanismos potenciales de reconstrucción de las verdades de la guerra y sus causas para enfrentar la impunidad social.

Se ha logrado un intento de independencia judicial para sancionar ante unos sectores del aparato institucional de la llamada justicia permeada por el clientelismo, la exclusión clasista,  la compra y venta de fallos.

La Jurisdicción de Paz, novedosa, que está siendo saboteada por los ex y el propio Santos es una ganancia, y un espacio para enfrentar la impunidad de Estado en su criminalidad. Santos, sectores militares, empresariales y religiosos temen a la verdad, como los ex. Hay que ver históricamente. Alguien puede olvidar al Fiscal Osorio de la huestes de Pastrana o la expansión del paramilitarismo durante su gobierno.  Quedarse en Uribe hace perder la visión.

Proteger lo logrado por encima de sus limites un reto, lograr en la mesa de Quito, otro modelo de justicia criticando emocionalmente el Sistema Integral, ahonda absurdas y equívocos fraccionamientos de lo llamado alternativo.

Un encuentro fraterno y con sinceridad se conoce de la reunión en Quito entre las delegaciones, importante pero insustancial, si esa fraternidad desconoce la importancia de las verdades, de las responsabilidades y de la superación de los juegos tácticos para y por cada uno.

Si el ELN percibe el país nacional, más allá de sus propias fuentes, de sus propios grupos de masas, y ven lo que realmente son y representan, como lo ha ido comprendiendo las FARC EP, se avanzará en la profundidad de lo nuevo.

El pueblo del que se habla está en el espejismo de los ex y Santos; son  los abstencionistas, los vargalleristas, ordeñistas, santistas, son el 20 o 30 por ciento del país que vota y decide.

Pero están el 70 por ciento restante a la espera en su subconciente de lo nuevo.  
Hay que ver lo que ellos representan identificando un sentimiento, falso, pero sentimiento que  mueve a los ciudadanos. Cabalgar sobre la guerra es un acierto parcial de los ex, hacerles el juego una inviabilidad a lo nuevo.

#### [**Leer más columnas de opinión de  Camilo de las Casas**](https://archivo.contagioradio.com/camilo-de-las-casas/)

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
