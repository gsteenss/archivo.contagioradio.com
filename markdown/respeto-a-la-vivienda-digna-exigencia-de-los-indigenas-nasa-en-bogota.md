Title: Respeto a la vivienda digna, exigencia de los indígenas Nasa en Bogotá
Date: 2018-05-16 13:17
Category: DDHH, Nacional
Tags: comunidades indígenas de Bogotá, derechos de los indígenas, indígenas, indígenas Bogotá
Slug: respeto-a-la-vivienda-digna-exigencia-de-los-indigenas-nasa-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/indigenas-bogota-e1526491530767.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Indígenas Nasa Bogotá] 

###### [16 May 2018] 

Luego de que los indígenas Nasa que habitan en Bogotá se retiraran de un predio en Usme, en el cual permanecían como forma de **protesta pacífica** por la **vulneración** de sus derechos, se inició la mesa de diálogo con las autoridades. Les hicieron saber que hay una **vulneración** sistemática de los derechos de los 17 pueblos indígenas víctimas del conflicto armado que hay en la capital, toda vez que no se sienten incluidos en la política pública que rige la capital.

### **Indígenas instalaron mesa de diálogo con las autoridades** 

Como parte de estas exigencias, durante el fin de semana estuvieron reunidos en una finca de la localidad de Usme y expresaron que “en el marco de los usos, costumbres y gobierno propio, avalados por la ley 89/1890, nos **retiraremos del predio** en posesión como segunda muestra de voluntad de diálogo de manera respetuosa”.

En ese sentido, pidieron que se instalara la mesa de **diálogo** con las instituciones pertinentes y así [**hablar**] sobre las problemáticas de vivienda y otras situaciones relacionadas con su condición de **víctimas del conflicto armado**. Según el gobernador, “estamos en asamblea permanente para negociar cuatro puntos importantes para los indígenas”. (Le puede interesar:"[Artesanas indígenas en Ciudad Bolívar salvaguardan la cultura ancestral](https://archivo.contagioradio.com/mujeres-indigenas-tejen/)")

Esos cuatro puntos son el tema de vivienda, debido a que el alcalde Enrique Peñalosa derogó el decreto 166 del año 2014 que le daba garantías a los indígenas para acceder de una manera **más fácil** a proyectos de vivienda. Razón por la cual están pidiendo que se reactive el decreto. Piden también que los cabildos indígenas que están en Bogotá sean registrados por parte del Ministerio del Interior y con esto sean tenidos en cuenta en los proyectos que se realicen.

Además, han exigido que se adelante el proceso de **declaración de víctimas colectivas** a los cabildos indígenas, argumentando que ellos son sujetos de reparación colectiva debido a las múltiples violencias que se han cometido contra ellos. También pidieron que se agilicen las salvaguardas para proteger a los cabildos indígenas en las actividades que se desarrollan en la ciudad.

### **Para poder acceder a proyectos deben ser registrados los cabildos y las víctimas** 

Ante esto, denunciaron que los procesos en los que se adelanta el tema de reparación y víctimas “han tenido **muchos retrasos** y ha habido muchas dificultades al momento de acceder a proyectos”. Pechele afirmó que “una de las barreras es que se deben presentar registros de víctimas individuales y nosotros hemos sufrido hechos victimizantes de manera colectiva”.

Manifestó que, al no haber un registro de víctimas de manera colectiva, no pueden acceder a proyectos de **reparación colectiva** que les permita adelantar actividades para su desarrollo social. Para esto, también expresaron que es necesario que se reconozca a los cabildos indígenas y sean comprendidos como tal ante las autoridades que realizan las diferentes políticas públicas. (Le puede interesar:"[13 años desplazado, la vida de un taita Wounaan en Bogotá](https://archivo.contagioradio.com/el-taita-sercelinito-piraza-burgara/)")

Finalmente, los indígenas manifestaron que se **mantendrán en asamblea permanente** para que sean escuchados y sus necesidades sean atendidas. Esperan que los obstáculos que están teniendo los indígenas para acceder a beneficios de vivienda sean discutidos y así salvaguardar la supervivencia de los pueblos indígenas.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
