Title: Propuesta del Gobierno sobre Curules de Paz busca desconocer lo acordado en La Habana
Date: 2019-12-17 18:25
Author: CtgAdm
Category: Paz, Política
Tags: circunscripciones especiales de paz, Congreso de la República, Víctimas del Conflicto
Slug: curules-gobierno-desconocer-acuerdo-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Congreso-curules.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: SenadoGovCo] 

Aunque aún está pendiente la decisión del Consejo de Estado que busca definir si en el Congreso fueron aprobadas las Circunscripciones Especiales para la Paz, en contravía al Acuerdo de Paz, el Gobierno ha propuesto que el proyecto no brinde nuevas plazas para las víctimas del conflicto sino que que dichos escaños sean cedidos por los actuales partidos políticos quienes podrán dar prioridad a víctimas de las FARC y víctimas militares y de Policía, **una propuesta que, advierten sectores, es inconstitucional y no reconoce los derechos de los afectados por la guerra.**

 

En 2017 la plenaria del Senado votó la conciliación del proyecto, fijando la mayoría para avalar el proyecto en 51 votos, sin embargo, algunas impedimentos de congresistas y el rechazo de partidos políticos como el Centro Democrático o el Partido Conservador limitó los votos a 50 dando lugar a una controversia jurídica que ahora está en manos del Consejo de Estado, que debe decidir si se alcanzó la mayoría para aprobar la reforma con los 50 votos. . [(Lea también: No fue correcto archivar acto legislativo de las curules de paz: Alfredo Beltrán)](https://archivo.contagioradio.com/tribunal_superior_circunscripciones_paz/)

La propuesta original indicaba que quienes fueran los candidatos a las curules - cuya existencia abarcaría el periodo legislativo entre 2018 -2022 y 2022 y 2026 - **debían ser postulados por organizaciones de víctimas legalmente reconocidas, asociaciones campesinas u organizaciones sujeto de reparación colectiva** que estuvieran conformadas antes del 1 de diciembre de 2016 y que pudiesen demostrar el trabajo con víctimas o a favor de ellas.

Además, las 16 circunscripciones estaban distribuidas en Bolívar, Cesar, Córdoba, Arauca, Antioquia, Cauca, Caquetá, Chocó, Meta, Nariño Norte de Santander, Putumayo y Tolima, departamentos del país que más fueron golpeados por el conflicto armado. [(Lea también: Plataforma Defendamos la Paz recogerá firmas para curules de víctimas)](https://archivo.contagioradio.com/defendamos-la-paz-2/)

Al respecto, el exministro de Interior y actual integrante de la plataforma Defendamos la Paz, Juan Fernando Cristo **manifestó que las curules fueron aprobadas por la plenaria del Senado en diciembre de 2017 y luego avaladas por un fallo de la Corte Constitucional que en la actualidad ** estudia una tutela interpuesta por el senador Roy Barreras que solicita se protejan **"los derechos a la igualdad, al debido proceso, y al participación política, vulnerados por la Mesa Directiva del Senado de la República”.**

### Las curules que propone el gobierno Duque

Ahora, desde el Gobierno, el presidente Duque ha propuesto que la iniciativa busca avanzar  "sin generar más espacio, más costo presupuestal  y que haya generosidad y grandeza de todas las fuerzas políticas", es decir, que estos espacios se brindarían sin crear 16 curules nuevas para las víctimas sino generando una recomposición al interior de cada una de las bancadas de la Cámara de Representantes.

A esta propuesta se suman voces como las del senador Ernesto Macías quien habla de "verdaderas víctimas" refiriendose a aquellas que deben estar certificadas y ser consideras prioridad al ser victimas de Farc o víctimas militares y de Policía, afirmación que discrimina a aquellas personas que han vivido el conflicto. [(Le puede interesar: Si no es de otro modo, que se hundan las 16 curules de víctimas)](https://archivo.contagioradio.com/49799-2/)

> Presidente [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw): respete a las víctimas. No es usted ni su partido quienes deciden quién es o no es víctima en Colombia. Como Jefe de Estado es su obligación no discriminar a ninguna persona o grupo que haya sufrido las consecuencias del conflicto armado y la violencia.
>
> — Iván Cepeda Castro (@IvanCepedaCast) [December 15, 2019](https://twitter.com/IvanCepedaCast/status/1206218081204350976?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Ante los cambios propuestos, senadores como Roy Barreras han advertido que este borrador, **"restringe derechos establecidos en el Acuerdo de Paz y desconoce concepto de reparación integral del territorios"**, enfantizando que el Congreso ya aprobó y que tanto la Corte Constitucional como la Corte Suprema de Justicia les están dando la razón, "lo que es necesario es promulgarlas".

"El Gobierno parece estar dando de palos de ciego en estas materias y no está bien informado de lo que sucede alrededor de la implementación del Acuerdo de paz, las circunscripciones transitorias de la paz, fueron aprobadas en acto legislativo desde hace dos años" concluye Cristo.

Por su parte, representantes de las víctimas como Martha Aguirre, presidenta de la fundación Sonrisas de Colores y representante de los familiares de víctimas de la masacre de concejales de Rivera en Huila señaló que es lo mínimo que el Gobierno y el Congreso pueden dar a las víctimas, «no podemos seguir hablando del Acuerdo de Paz y del conflicto sin tener en cuenta nuestra participación y sobre todo sin una representación política». [(Le recomendamos leer: No podemos hablar de paz sin nuestra participación política: víctimas del conflicto) ](https://archivo.contagioradio.com/no-podemos-hablar-de-paz-sin-nuestra-participacion-politica-victimas-del-conflicto/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45644959" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45644959_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
