Title: Abolir los fueros para que la verdad y la justicia haga posible la paz
Date: 2015-11-24 15:43
Category: Nacional, Paz
Tags: Conpaz, delegacion de paz, ELN, FARC, habana, JEP, jurisdicción especial para la paz, proceso de paz, Timochenko, Timoleon Jimenez
Slug: abolir-los-fueros-para-que-la-verdad-y-la-justicia-haga-posible-la-paz
Status: published

###### Foto: Contagio Radio 

###### [23 Nov 2015] {#nov-2015 dir="ltr"}

La Jurisdicción Especial de Paz para enfrentar la impunidad durante y con ocasión del conflicto armado, debe partir de la igualdad ante la ley, afirman la red de comunidades de víctimas, Comunidades Construyendo Paz, CONPAZ.

A través de una carta dirigida **al presidente Juan Manuel Santos, al Comandante Timoleón Jiménez de las FARC EP y al Comandante Nicolás Rodríguez del ELN, CONPAZ**, a los tres años de inicio de las conversaciones realizan propuestas para desempantanar el tema de víctimas.

Ningún tipo de fueros, sean eclesiásticos, políticos, económicos o para magistrados, deben impedir el derecho a la verdad y la justicia a las victimas y la sociedad, por lo que CONPAZ propone su abolición para dar paso a la JEP.

La JEP que forma parte del Sistema Integral de Verdad, Justicia y Reparación, fue acordada el pasado 23 de septiembre con la presencia de Santos y de Timoleón Jiménez en La Habana, fue **objeto de un saboteo por parte del exmandatario Uribe y de los propios sectores del gobierno.**

Para CONPAZ y otras organizaciones de víctimas, los planificadores, los instigadores y beneficiarios de la violencia, temen a la verdad, pues los militares y paramilitares, fueron ejecutores, recibieron órdenes y son los que en ocasiones se ponen como los chivos, mientras los factores de poder gozan de impunidad.

En la misiva las organizaciones de víctimas solicitan a los exmandatarios y a la clase dirigente, asumir con generosidad la igualdad ante la ley para asumir el reto de la verdad y el derecho reparativo.

Como se sabe una delas razones para que el acuerdo sobre justicia no se haya dado a conocer es que sobre las 27 páginas de la JEP, la delegación del gobierno objeta el que este sistema para dar fin a la impunidad, pueda juzgar causas que se abran contra exmandatarios y el propio presidente Santos.

En la comunicación pública proponen que  el gobierno en la mesa de exploración con el ELN, de a conocer el acuerdo de  justicia, para que se vayan armonizando los dos mesas de conversaciones.

Por otra parte, mientras esperan la respuesta de las partes, las comunidades proponen que en la mesa de La Habana, el gobierno y las FARC EP, **constituyan una  comisión de trabajo con comunidades negras e indígenas,** para que los acuerdos incluyan con claridad, la perspectiva étnica.
