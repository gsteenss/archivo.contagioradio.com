Title: Seguridad Democrática y el delito político
Date: 2018-09-21 08:43
Author: AdminContagio
Category: Expreso Libertad
Tags: delito político, Seguridad Democrática
Slug: seguridad-democratica-delito-politico
Status: published

###### Foto: La F.M. 

###### 12 Sep 2018 

Durante la presidencia de Álvaro Uribe Vélez se abrió paso a la política de la Seguridad Democrática, una herramienta que a través de diversos mecanismos, inicio una persecución a líderes sociales, defensores de derechos humanos y aquellas personas que fuesen consideradas como peligrosas para el Estado.

Diego Pinto relató en los micrófonos del Expreso Libertad las tácticas y estrategias que utilizó el movimiento social para sobrevivir a las judicializaciones, las amenazas y hostigamientos y hacerse mucho más fuerte.

<iframe id="audio_28756419" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28756419_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 

 
