Title: Herir a nuestra niñez y condenarnos a la infamia
Date: 2015-02-24 14:45
Author: CtgAdm
Category: Carolina, Opinion
Tags: Derechos de los niños y las niñas
Slug: herir-a-nuestra-ninez-y-condenarnos-a-la-infamia
Status: published

###### Foto: Gabriel Galindo 

#### [Por [**Carolina Garzón Díaz**](https://archivo.contagioradio.com/carolina-garzon-diaz/)] 

En Colombia nos han resultado impresionantes las noticias de los últimos días que hablan sobre las atrocidades que sufren los niños y niñas de nuestro país: cruentos asesinatos, violencia sexual, reclutamiento forzado, trabajo infantil, discriminación, falta de acceso a la salud y a la educación; es tanta la violencia en todos los niveles, que hasta la Corte Constitucional, al no permitir la adopción igualitaria, le negó a 80.000 niños colombianos que esperan ser adoptados su derecho a tener una familia.

Estos casos son aún más dolorosos porque, aparezcan o no en los medios de comunicación, se cometen a diario, a lo largo y ancho del país, y a cada minuto. Todos los días un adulto comete un crimen contra un niño o niña, y los victimarios somos todos, desde los padres hasta la sociedad en general que no se inmuta ante las heridas que le provocamos a la niñez:

Cada nueve horas un menor de edad es asesinado en Colombia. Según Medicina Legal, el ICBF y la Agencia Pandi, cada 30 minutos un niño o niña acude a Medicina Legal tras ser víctima de agresión sexual, 10 menores son reclutados cada mes por grupos armados ilegales (entre el 2013 y el 2014, los grupos paramilitares reclutaron al menos 65 niños en varias regiones del país), la mitad de las víctimas de desplazamiento tienen menos de 18 años; cada año se reportan unos 9.000 casos de violencia intrafamiliar y 1.078 fueron abandonados en el año 2014.

Creer que "los niños son el futuro de una sociedad" o considerarlos como "pequeños adultos", son ideas que nos han dañado la cabeza en la medida en que asumimos a los niños como personas incompletas cuyos derechos plenos están en el futuro, y no como los sujetos de derecho que realmente son y cuyos derechos fundamentales deben ser protegidos. Las heridas que día tras día le causamos a nuestra niñez son parte del presente, del hoy de sus vidas y de las bases de nuestro futuro; esas mismas heridas son las que nos condenan a la infamia en el futuro.

Ahora que buscamos la paz y anhelamos la verdad, la justicia, la no repetición de los hechos violentos que hemos sufrido a lo largo de estos años de conflicto, vale la pena pensarnos sobre la sociedad que deseamos ser. Pero el verdadero cese de la violencia en Colombia pasa por los hogares, las veredas, los colegios, los parques y todos los espacios en los que vive la niñez. Nuestras acciones contra los niños y las niñas son el caldo de cultivo de los odios, los resentimientos, la desdicha y la misma violencia del presente y del futuro.

Es responsabilidad del Estado en su totalidad proteger y blindar a la niñez ante las atrocidades que viven en el presente. Además, brindarles las oportunidades para que los niños y niñas puedan desarrollar su vida lejos de la violencia y el crimen. También es obligación del Estado garantizarles su derecho a una familia, a atención pronta e integral en salud, a educación de calidad, a vivienda, a alimentación, a recreación y a la protección de todos sus derechos.

Pero la sociedad civil, los medios, el sector privado y las organizaciones no gubernamentales, también tenemos la obligación de documentar su situación, exigir sus derechos, denunciar las atrocidades cometidas contra la niñez y ser ejemplo de un trato digno acorde a las necesidades y condiciones propias de los niños y las niñas. No podemos seguir ahondando las heridas de una sociedad tan lastimada, ni indignándonos en público mientras victimizamos en privado.

##### Carolina Garzón Díaz ([@E\_vinna](https://twitter.com/E_Vinna)) 
