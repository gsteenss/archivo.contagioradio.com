Title: Universidad de Antioquia estaría evitando respuesta de fondo a Ríos Vivos Antioquia
Date: 2020-09-22 19:22
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Hidroituango, Ríos Vivos, Universidad de Antioquia
Slug: universidad-de-antioquia-estaria-evitando-respuesta-de-fondo-a-rios-vivos-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Rios-Vivos-cita-a-debate-etico-a-Universidad-de-Antioquia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**El [Movimiento Ríos Vivos](https://riosvivoscolombia.org/) que agrupa a las víctimas y afectados por el proyecto Hidroituango, citó a un debate de ética a la Universidad de Antioquia**, y cuestionó su participación en los procesos de exhumación y traslado de cuerpos para la construcción de la represa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La citación se da a raíz de **la exhumación y traslado de cuerpos**, que según el Movimiento Ríos Vivos, **realizó la Universidad de Antioquia «*sin los avales necesarios y sin los más mínimos criterios técnicos y éticos para llevar a cabo estos procedimientos*».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Ríos Vivos estas labores violaron los derechos a la verdad, justicia, reparación  y no repetición de las víctimas, ya que, la falta de rigurosidad en su ejecución «*ha imposibilitado esclarecer posibles casos de asesinatos, ejecuciones extrajudiciales y desapariciones forzadas*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La Jurisdicción Especial de Paz -[JEP](https://www.jep.gov.co/Paginas/Inicio.aspx)-**, en una visita realizada al Laboratorio de Osteología y Antropología forense de la Universidad, sitio al que fueron trasladados algunos de los cuerpos removidos de los cementerios de Orobajo, Barbacoas y La Fortuna, **constató que en al menos tres casos había evidencia de «*presuntas alteraciones perimortem compatibles con mecanismo por proyectil de arma de fuego*».** (Le puede interesar: [JEP abre incidente contra gerente de EPM](https://archivo.contagioradio.com/jep-abre-incidente-contra-gerente-de-epm/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Adicionalmente,  Ríos Vivos cuestionó que la Universidad de Antioquia hubiese priorizado los intereses económicos, por encima de su compromiso con la sociedad y las comunidades** como institución educativa; acordando convenios «*con actores  que ejercen un poder arbitrario*», refiriéndose al consorcio responsable del proyecto hidroeléctrico. (Le puede interesar: [Continúa la violencia y los desalojos contra comunidades afectadas por Hidroituango en Antioquia](https://archivo.contagioradio.com/continua-la-violencia-y-los-desalojos-contra-comunidades-afectadas-por-hidroituango-en-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por todo lo anterior, las víctimas y afectados, convocaron a los órganos de administración de la Universidad de Antioquia, para que se generen escenarios de verdad y perdón sobre las situaciones expuestas; citándolos públicamente a un debate en el que se ventile la ética en el actuar institucional de la Universidad.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RiosVivosColom/status/1308234349834383360","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RiosVivosColom/status/1308234349834383360

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### La respuesta de la Universidad de Antioquia

<!-- /wp:heading -->

<!-- wp:paragraph -->

A través de un comunicado la Universidad de Antoquia se pronunció a la comunicación remitida por Ríos Vivos; sin embargo, solo se refirió de fondo frente al **contrato a través del cual se comprometió a realizar la exhumación y traslado de los cuerpos señalando que había sido suscrito con la empresa Integral S.A.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a los señalamientos por la inexistencia de avales y la ejecución de los procedimientos sin los criterios técnicos y éticos mínimos realizados por Ríos Vivos, no emitió pronunciamiento concreto; como tampoco lo hizo frente a la solicitud de realizar un debate sobre la ética institucional*.*

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FCSH_UdeA/status/1308222209845010433","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FCSH\_UdeA/status/1308222209845010433

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
