Title: Atentan contra Ricardo Silva, líder comunal en Mapiripán
Date: 2018-08-24 12:00
Category: DDHH, Nacional
Tags: Atentado, lideres sociales, mapiripan
Slug: atentan-contra-lider-comunal-en-mapiripan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/mapiripan-ricardo-silva.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 24 Ago 2018 

Se trata de **Ricardo Silva, presidente de la Junta de acción Comunal de la vereda San Antonio del municipio de Mapiripán**, Meta, quien fue atacado por desconocidos aproximadamente a las 11:30 de la noche del miércoles 22 de agosto. **En el atentado resulto muerto su hijo Richard**.

Silva, quién ejerce actividades de liderazgo en materia de restitución de tierras, f**ue atacado en su vivienda ubicada en la Finca "El Rosario" recibiendo disparos que le dejaron herido a la altura de la rodilla**. El líder habría logrado comunicarse con la fuerza pública para pedir ayuda.

En diálogo con la emisora regional Marandua Esteréo, el Coronel Miguel Ángel Botía, comandante de Policía Guaviare, aseguró que "**8 sujetos dispararon indiscriminadamente contra su residencia**" donde se encontraba el líder con su familia. Según el reporte oficial a la zona llegaron uniformados del ejército para resguardar el lugar.

Adicionalmente, el Coronel Botía reportó que la policía arribó en horas de la madrugada, logrando evacuar al líder para ser atendido en el hospital San José, de Puerto Concordial, mientras se realizaron actividades de policía judicial para recaudar evidencia e inspección técnica al cadáver de Richard Silva, que aporten a esclarecer los móviles del atentado.

De acuerdo con el uniformado, están buscando establecer con la SIJIN del Meta, si existe algún tipo de relación con la [desaparición y posterior asesinato de Jefferson Arévalo](https://archivo.contagioradio.com/es-hallado-sin-vida-jefferson-arevalo-sobreviviente-de-la-up-en-el-meta/), en el municipio de Puerto Rico, reportado también el pasado 23 de agosto. Lo que asegura el oficial **es que existen grupos delincuenciales en el sector, interesados en apoderarse de tierras que están en proceso de restitución**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
