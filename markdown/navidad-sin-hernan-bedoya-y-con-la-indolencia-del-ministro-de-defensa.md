Title: Navidad sin Hernán Bedoya y con la indolencia del Ministro de Defensa
Date: 2017-12-19 06:00
Category: Abilio, Opinion
Tags: Hernan Bedoya, lideres sociales, Ministro de defensa
Slug: navidad-sin-hernan-bedoya-y-con-la-indolencia-del-ministro-de-defensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/hernan-bedoya-curvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo/Contagio Radio 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 19 Dic 2017

[Diez días pasaron entre la muerte de Mario Castaño y la de Hernán Bedoya. Mario era del Consejo de la Larga Tumaradó y Hernán resistía, muy cerca,  en Bijao Onofre, dentro de una pequeña isla de tierra que preservaba del despojo, tras el acaparamiento empresarial de extensas áreas de ese consejo comunitario, facilitado por la corrupción del representante legal, destruyendo bosques, haciendo canales de drenaje, sembrando  palma aceitera y  plátano de exportación.]

[Hernán no sabía leer ni escribir y la fuerza de su convicción lo convirtió en un líder natural, capaz de animar otros despojados en la reclamación de la herencia que dejaron sus padres y que quería dejar a sus hijos, hasta que los victimarios vieron la eficacia de su resistencia y acabaron con su vida. La razón de su vida y de su muerte quedó claramente expresada en un [corto y bello documental](https://www.youtube.com/watch?v=bK1imP4nlAg) grabado en 2016: “nos sacaron los paramilitares dos veces, nos compraron lo que quedaba a cualquier precio, luego volvimos, nos intentaron sacar de nuevo y no pudieron por la presencia de Justicia y Paz. Quieren sembrar mil hectáreas de palma, para hacerlo, tendrán que sacarnos del territorio primero”. Toda una premonición de su desenlace.]

[La posesión de su tierra, luego del crimen, aún no se consuma, pero se ambienta con la desidia gubernamental, la usencia de actuaciones efectivas frente a los paramilitares que, de acuerdo con pobladores de la región, transitan a sus anchas por las mismas vías en que patrulla el Batallón 54  de Selva, construido, dicho sea de paso, en tierras usurpadas, también, a las comunidades en Llano Rico, sin que el gobierno haga nada.]

[Y ahora el Ministro de Defensa   dirige el coro gubernamental de las justificaciones. Con machismo, suma indolencia, y desconsideración con lxs dolientes, afirmó en Noticias Uno que cada crimen: “ha sido fruto de un tema de linderos, de un tema de faldas, de un tema de peleas por rentas ilícitas”.]

[Antes, a los líderes y pobres les quitaban la vida señalándolos de terroristas y  en los llamados falsos positivos,  hoy siguen justificando su muerte con la  misma lógica del enemigo interno. El Ministro del nobel de paz, que participó en la mesa de diálogos en la Habana, que habló de la centralidad de las víctimas, demuestra que para el gobierno ellas no son más que un recurso acomodaticio para lograr ciertos consensos, más en el fondo está la protección de los intereses empresariales sin importar si están  o no vinculados con la criminalidad.]

[En esta Navidad sin Hernán, sin Mario, sin tantxs asesinadxs, bastaría que el ministro se fijara en   en su testimonio, en el brillo de sus ojos, en la  bondad de sus palabras  y en la  dignidad de sus gestos. Pero si no es Hernán la inspiración para el cumplimiento de su deber de frenar este desangre, debería mirar los informes de la Comisión de Justicia y Paz, de organizaciones internacionales sobre la criminalidad empresarial de la palma aceitera, de la ganadería extensiva, del plátano de exportación en el Urabá y Bajo Atrato que ha cobrado tantas víctimas.]

[Podría examinar los propios convenios de la Agencia Nacional de Tierras de su gobierno para constatar si existen o no vínculos con la empresa Agromar S.A y la empresa Recife S.A; pero que sus asesores se tomen el trabajo de pedir el certificado de existencia y representación legal de esas dos compañías, examinar el nombre de los socios, y los pusiera en el buscador  de Google.]

[Encontrarán que, en el caso de Agromar, uno de sus socios está implicado en  proceso por tráfico de drogas ilícitas en Panamá iniciado por su captura en flagrancia por parte del Servicio de Guarda Costas de los  Estados Unidos, y en el caso de Recife S.A, que  uno de sus socios estuvo detenido por participar en el desplazamiento paramilitar en el Jiguamiandó y  en una finca del otro socio,  hallaron  un gigantesco  cargamento de cocaína apunto de ser exportado.]

[Quizás en esta navidad con su familia pueda dedicar el ministro un minuto de sus pensamientos, mientras se toma algún trago de aguardiente, a las víctimas que como Hernán entregaron su vida para preservar la tierra de su herencia, esos familiares que este año pasarán la primera navidad sin él. Quizás por la memoria de Hernán, Mario, tantos y tantas, tenga el ministro  el valor de rectificar sus ofensivas palabras.]

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio
