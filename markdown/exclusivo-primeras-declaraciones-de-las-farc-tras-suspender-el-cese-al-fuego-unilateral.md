Title: EXCLUSIVO: Primeras declaraciones de las FARC tras suspender el cese al fuego unilateral
Date: 2015-05-23 10:11
Category: Entrevistas, Paz
Tags: Cese al fuego, dialogos de paz, FARC, Gobierno, guerrilla, pastor alape, Santos
Slug: exclusivo-primeras-declaraciones-de-las-farc-tras-suspender-el-cese-al-fuego-unilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/17359732111_5a27061e12_k.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Delegación de paz, FARC 

<iframe src="http://www.ivoox.com/player_ek_4538416_2_1.html?data=lZqgmpmVeo6ZmKiak5yJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibm3rbrAq7uzaZS1jLXfy9LJtsLnjMnSxdHFtsLXytTbx9iPqMafzcbgjaullqSf1dfO1ZDXudTkjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Pastor Alape, FARC-EP] 

###### 

###### [23 may 2015] 

El comandante de la guerrilla de las FARC-EP, Pastor Alape, miembro de la delegación de paz de esta insurgencia, concedió una entrevista a Contagio Radio para hablar sobre la situación en que se encuentran el proceso de paz que adelantan con el gobierno nacional.

En ella, Alape afirma que si bien el ciclo 37 de conversaciones no se llevó a cabo el sábado 23 de mayo, por encontrarse la guerrilla en un momento de duelo y reflexión, el ciclo se retomaría el lunes 25 de mayo. "La posición de la organización \[FARC\] es completamente diferente a las incoherencias del gobierno, que por una parte dice que el debate de la paz debe darse en medio de la confrontación, pero cuando se da un hecho en que las estructuras del Estado no salen exitosas, forma pataleta. Nosotros vamos a ir el lunes a la mesa como está establecido y el debate vamos a darlo allí", afirmó.

Frente a la suspención del cese unilateral al fuego, que cumplieron durante 5 meses desde el 20 de diciembre del año 2014 hasta el 22 de mayo pasado, Pastor Alape asegura que mal hizo el gobierno en pensar que se trataba de un gesto de debilidad que debía aprovechar para fortalecerse militarmente y arrinconar a la insurgencia. "El cese unilateral que decretamos no fue una muestra de debilidad, fue una muestra de fortaleza por la paz, de sueño esperanzador". También se equivocaría Santos al afirmar que la guerrilla de las FARC prepararía nuevas retaliaciones. Según afirma el comandante guerrillero, "no estamos planteando retaliaciones: simplemente decimos que no podemos mantener el cese al fuego unilateral, porque si no hay cese bilateral van a presentarse operativos y enfrentamientos y van a ser presentados como que somos nosotros los que estamos violando una decisión".

Finalmente, frente al papel de los medios de comunicación en los proceso de paz en Colombia, Alape insiste en que "es el pueblo, es la gente, es la movilización, las manifestaciones en apoyo a la paz que no tienen acceso a los grandes de comunicación" los que permitirían romper las matrices de opinión que juegan en contra de los diálogos de paz. "Esto lo salva la gente que se moviliza en barriadas, municipios y veredas, que están sintiendo el rigor del conflicto".

Y concluyó con que "Hemos venido convocando a algunos directores de medios a mostrar que estamos interesados en que se construya la paz, y que para eso se requiere el compromiso de los medios, no para censurarlos o para que modifiquen la información, sino para que sea una información sana y con base en la realidad, y que la objetividad periodística sea la base de la información, y no la manipulación y la mentira."

La delegación de paz de las FARC insistiría en la mesa de conversaciones en la necesidad de avanzar en la concreción de un cese bilateral para "evitar la muerte de mas colombianos".
