Title: "Antioquia le habla a la Habana" sobre el proceso de paz
Date: 2016-03-10 12:34
Category: Nacional, Paz
Tags: Antioquia le Habla a La Habana, Consejo de Medellín, proceso de paz
Slug: antioquia-le-habla-a-la-habana-sobre-el-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Antioquia-le-Habla-a-La-Habana.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Víctor Correa 

<iframe src="http://co.ivoox.com/es/player_ek_10751507_2_1.html?data=kpWkl5aZdJihhpywj5WXaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncYamk6bb1s7TtdbdwpDZx5DMpcPgwpDOjdHFb6nVw8bbw4qWdozn0Mffx5DJsIzk09TQx9jTb8XZjNXOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gerardo Vega, Fundación Forjando Futuros] 

###### [10 Mar 2016] 

Desde las 8 de la mañana en el Concejo de Medellín se lleva a cabo el conversatorio **“Antioquía le Habla a La Habana”**, un espacio de diálogo y participación que busca acercar a la ciudadanía con  el proceso de Paz, en términos del conocimiento de los puntos ya pactados, los alcances de los acuerdos y los mecanismos de refrendación del mismo.

El escenario tiene como **finalidad generar un encuentro entre la ciudadanía con las las delegaciones de Paz del gobierno y las FARC, que se encuentran en la Habana.** Así mismo participan diversas organizaciones sociales y de Derechos Humanos para abrir el debate  frente a lo que se ha pactado, las expectativas y dudas que hay en la sociedad colombiana.

De acuerdo con Gerardo Vega, director de la Fundación Forjando Futuros y uno de los organizadores del evento, este conversatorio es fundamental ya que a través de estas actividades  se legitima el proceso de paz, “concebimos la paz territorial con los líderes sociales, con las juntas de comunidad, con las JAL, con las asambleas departamentales, con la participación de todas y todos. No puede ser solamente un acuerdo que se celebra desde Bogotá y la Habana. La ciudadanía en esta metodología de pedagogías por la paz, debe participar en cada una de las regiones. **El día de hoy tiene como propósito central que se pueda conversar sobre los puntos ya pactados sobre desarrollo rural, víctimas, y la refrendación de los acuerdos”.**

Esta iniciativa organizada por la Fundación Forjando Futuros, la Comisión Intereclesial de Justicia y Paz,  la Corporación Jurídica Libertad  y el Instituto Popular  de Capacitar, cuenta con la participación de diferentes expertos en el tema y representantes del Movimiento Social como lo son Piedad Córdoba, el Representante a la Cámara Víctor Correa, la defensora de Derechos Humanos Adriana Arboleda, el analista político León Valencia, el gerente de Paz para Antioquía Luís Guillermo Pardo, entre otros.

La agenda del día tiene en primera instancia la intervención de la delegación de Paz de las FARC, posteriormente la intervención de los diferentes panelistas, la intervención de la delegación de Paz del Gobierno, para concluir con una ronda de intervenciones de la ciudadanía.

Cabe resaltar, que **mientras se realiza el foro en el Consejo de Medellín, un grupo de uribistas se encuentra a las afueras del recinto con carteles en contra del proceso de paz.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
