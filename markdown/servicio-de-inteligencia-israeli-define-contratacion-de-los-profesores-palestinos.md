Title: Servicio de inteligencia israelí define contratación de los profesores palestinos
Date: 2017-10-11 16:25
Category: Onda Palestina
Tags: Apartheid, BDS, Israel, Palestina
Slug: servicio-de-inteligencia-israeli-define-contratacion-de-los-profesores-palestinos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Escuela-palestina2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:www.middleeastmonitor.com 

###### 11 Oct 2017 

La agencia de seguridad interna Israelí , conocida como Shin Bet, está llevando a cabo un control excesivo sobre el personal docente que atiende escuelas Palestinas. Según el mismo ministro de educación de Israel, Emmanuel Koplovich, la agencia de seguridad ha actuado rechazando a nuevos docentes y despidiendo a otros más, en cumplimiento a una política que en el fondo busca depurar de las escuelas palestinas a los docentes que tienen ideas críticas frente al apartheid, e incluso justificando los despidos cuando se descubre que los docentes o sus familiares son activistas políticos.

El director del centro Adalah para los derechos de palestinos dentro de Israel, Hassan Jabareen, afirmó que la participación del Shin Bet en el reclutamiento o rechazo de los profesores palestinos ha sido reconocida desde hace mucho tiempo dentro de la comunidad Palestina. Según el medio de comunicación Quds Press, estos actos reiteran la hostilidad del Ministerio de Educación Israelí contra la población palestina.

En esta emisión de Onda Palestina hablaremos sobre el problema del agua en Palestina, para lo cual tendremos una entrevista con un profesor santandereano especializado en ello, además hablaremos sobre los nuevos esfuerzos israelíes para promocionar los productos agrícolas que produce en la Palestina ocupada. Noticias, cultura y la mejor música palestina en este programa de solidaridad desde Colombia con Palestina.

<iframe id="audio_21401757" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21401757_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
