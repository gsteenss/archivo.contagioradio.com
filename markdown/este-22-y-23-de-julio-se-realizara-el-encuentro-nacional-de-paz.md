Title: Avanza el Encuentro Nacional de Paz
Date: 2015-07-21 18:38
Category: Movilización, Paz
Tags: alirio uribe, Angela Maria Robledo, Centro Memoria Histórica, Concejo Nacional de Paz, Congreso de la República, Encuentro nacional de paz, Frente Amplio por la PAz, Iván Cepeda, proceso de paz, Víctor Correa
Slug: este-22-y-23-de-julio-se-realizara-el-encuentro-nacional-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [tag-buzz.com]

Este 22 y 23 de julio se realizao el Encuentro Nacional de Paz, en el Congreso de República donde **diversas organizaciones discuten sobre propuestas de paz que impulsen el proceso de conversaciones** que se lleva a cabo en la Habana entre el Gobierno Nacional y las FARC-EP.

El Comité de Impulso del Consejo Nacional de Paz, Parlamentarios de las Comisiones de Paz de Senado y Cámara, plataformas de paz de la sociedad civil, organizaciones indígenas, afrodescendientes, sindicatos, víctimas, confluencias y expresiones de fe y organizaciones defensoras de derechos humanos harán parte de este encuentro donde **a partir de diversas mesas temáticas se desarrollarán estrategias en torno a la paz.**

“**El Encuentro Nacional de paz lo comenzamos a preparar hace mes y medio en pleno recrudecimiento de la guerra**”, dice la representante a la cámara e integrante del Frente Amplio por la Paz, Ángela María Robledo, quien agrega que se trata de una actividad impulsada desde los movimientos sociales y la necesidad insistir en una salida política al conflicto armado.

El miércoles 22 de julio se desarrollará la primera parte del Encuentro, en el Auditorio Luis Guillermo Vélez desde las 3:00 de la tarde. El jueves 23 de julio, el evento se realizará en el Centro de Memoria, Paz y Reconciliación desde las 8:00 de la mañana y cerrará con una actividad de reflexión en el que se encenderá la **llama por la paz.**

Las personas y organizaciones interesadas podrán asistir con previa inscripción a través de [encuentronacionaldepaz.com ](http://www.encuentronacionaldepaz.com%C2%A0) o en los sitios Web de los congresistas Iván Cepeda, Alirio Uribe, Víctor Correa, Ángela María Robledo entre otros.
