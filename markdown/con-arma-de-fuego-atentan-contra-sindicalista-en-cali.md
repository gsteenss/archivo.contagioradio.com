Title: Con arma de fuego atentan contra sindicalista en Cali
Date: 2016-11-30 15:53
Category: DDHH, Nacional
Tags: arma de fuego, Cali, Sindicalistas
Slug: con-arma-de-fuego-atentan-contra-sindicalista-en-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/sindicalista.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter ] 

###### [30 Nov. 2016] 

El pasado martes **un integrante de un sindicato que pertenece al sindicato de las empresas públicas de Cali fue víctima de un atentado con arma de fuego.**

Según Jorge Vélez  presidente de Sintraemcali, **el sindicalista fue abordado por unos sujetos que impactaron el vehículo** “gracias a Dios y a la rapidez con la que actuaron los escoltas del dirigente no pasó a mayores. Estas personas que pretendían atentar contra el líder dejaron una moto abandonada” precisó.

Según Vélez en los últimos días los integrantes de este y **otros sindicatos han estado recibiendo amenazas directas por parte de grupos, según él, al margen de la ley, a través de llamadas telefónicas y panfletos** “estas amenazas plantean que se dan directamente por las actividades sociales que adelantamos en la ciudad, en las calles, en la defensa de otras organizaciones sociales y populares que presentan problemas. Somos objetivo militar”.

En la actualidad, **según cifras otorgadas por Sintraemcali son cerca de 10 personas, todos integrantes de la junta directiva**, las que han recibido amenazas en los últimos días a través de sufragios que han llegado a la sede del sindicato.

Aunque Sintraemcali ya ha instaurado las respectivas denuncias, hasta el momento no han recibido respuesta alguna por parte de las autoridades en lo que respecta a lo adelantado en las investigaciones “queremos que se obre con rapidez para adelantar las pesquisas de los autores materiales e intelectuales del caso” concluyó Vélez.  Le puede interesar: [‘Voces que no Callan’ un informe sobre el panorama del sindicalismo en Colombia](https://archivo.contagioradio.com/voces-que-no-callan-un-informe-sobre-el-panorama-del-sindicalismo-en-colombia/)

<iframe id="audio_14325553" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14325553_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
