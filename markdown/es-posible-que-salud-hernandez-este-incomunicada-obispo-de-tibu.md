Title: No se puede asegurar que Salud Hernández esté secuestrada: Obispo de Tibú
Date: 2016-05-25 14:37
Category: DDHH, Nacional
Tags: Catatumbo, Desaparición Diego D'Pablos y Carlos Melo, Desaparición Salud Hernández, Salud Hernández
Slug: es-posible-que-salud-hernandez-este-incomunicada-obispo-de-tibu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Salud-Hernandez_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CMI ] 

###### [25 Mayo 2016]

Cuatro días lleva desaparecida la periodista Salud Hernández y dos, los periodistas Diego D'Pablos y Carlos Melo, un caso atípico en los últimos años, porque **en situaciones normalizadas del conflicto en Colombia se sabría qué grupo armado los tiene en su poder**; sin embargo, ninguno se ha pronunciado y hasta el momento lo que hay son sólo hipótesis arriesgadas que aún no han sido confirmadas, según afirma el abogado Pedro Vaca, director de la Fundación para la Libertad de Prensa.

Por su parte, Monseñor Omar Sánchez, Obispo de Tibú, asegura que lo último que se sabe es que Salud Hernández durmió la noche del viernes en un hogar religioso, abordó una moto el sábado y una hermana fue la última persona con la que tuvo contacto. Por lo que no puede decirse que ella fue tomada por la fuerza, **cabe la probabilidad de que ella haya aceptado ir a las veredas en las que puede que no haya señal**, y se espera que regrese una vez haya terminado su tarea investigativa.

Monseñor insiste en que a un territorio complejo como el Catatumbo los periodistas sólo pueden entrar de tres formas, la primera, que algún grupo armado les haya invitado y por tanto los blinde, o porque la fuerza pública los invitó a observar una operación, o porque la comunidad fue quien los invitó; por lo que hace un llamado a la prudencia y a tener en cuenta que **las operaciones militares pueden complicar la integridad y el regreso de los periodistas**.

Pedro Vaca asevera que estas desapariciones reflejan que hay regiones en el país en las que no hay garantías para el ejercicio periodístico; zonas como el Catatumbo y municipios como El Tarra, en los que hay debilidad institucional, rentas ilegales, conflicto armado, narcotráfico, cultivos de uso ilícito y que necesitan una **mayor presencia de las instituciones del Estado**, las que en este caso deben brindar mensajes más claros, para que haya menos espacio para la especulación

La cosa es simple, en este momento hay una apuesta de paz territorial, que incluye temas agrarios, de desarrollo integral, participación política, y sustitución de cultivos ilícitos, y sí se firman los acuerdos la tarea no es la guerra, la tarea es apostarle al desarrollo, asunto que va a necesitar de tiempo y de la concertación con otros actores, concluye Monseñor.

<iframe src="http://co.ivoox.com/es/player_ej_11664804_2_1.html?data=kpajmJmcdJWhhpywj5aZaZS1lZyah5yncZOhhpywj5WRaZi3jpWah5ynca7jz9jSh6iXaaOl0NeYsdLFtozHhqigh6aVssTcxt%2BYj5Czpsrn0dSYts7GuY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
