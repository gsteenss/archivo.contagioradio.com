Title: Refugio Humanitario del Alto Sinú denuncia la militarización de su territorio
Date: 2015-03-18 17:43
Author: CtgAdm
Category: DDHH, Nacional
Tags: Aida Avella, Alto del Sinú, ASODECAS, cordoba, DDHH, Derechos Humanos, Hidroelectrica, ministerio de defensa, Ministerio de Minas y Energía, Refugio Humantitario, Unión Patriótica, Urrá
Slug: refugio-humanitario-del-alto-sinu-denuncia-la-militarizacion-de-su-territorio
Status: published

##### Foto: [parentesiscali.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_4232202_2_1.html?data=lZeglJeUdo6ZmKiak5WJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Lh0crgy9PFqNCfxt3WycqPpYy70MfS1NPHrYa3lIqvldOPqMafpIqwlYqmd9PY0MfOjdjJtozZ1Mjixc3FcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Cogollo, Asociación Campesina para el Desarrollo del Alto Sinú (ASODECAS)] 

Este 17 de marzo campesinos, campesinas e indígenas del Campamento Refugio Humanitario del Alto Sinú, denominado Territorio de Paz, realizaron un plantón frente  a la Gobernación de Córdoba, con el objetivo de pedirle al alcalde del municipio y al gobernador  Alejandro Lyons Muskus que **sean escuchadas sus exigencias frente a la grave situación de derechos humanos que vive actualmente la comunidad.**

De acuerdo a Luis Cogollo, profesor de derechos humanos de la Asociación Campesina para el Desarrollo del Alto Sinú, ASODECAS, la comunidad ha decido declarar su territorio como  **Campamento de Refugio Humanitario, como mecanismo de autoprotección popular,** debido a la preocupante situación humanitaria que los habitantes viven hace un año, debido a la presencia del ejército y la policía, que ha dejado niños y campesinos heridos.

Además, de acuerdo al comunicado de ASODECAS, los pobladores le reclaman al gobierno “las frecuentes violaciones a los DDHH, la falta de garantías para trabajar la tierra dignamente, el abandono del estado en materia de salud, educación, saneamiento básico y vías han llevado a las comunidades a una etapa de crisis”.

Pese a que se ha denunciado la situación, la asociación afirma que **no ha habido respuesta de por parte de las instituciones del estado y la fuerza pública,** en cambio, cada vez se agrava la situación, afirma el profesor.

Así mismo, **se ha denunciado la muerte de un joven de 23 años quien fue víctima de las dispersiones aéreas de glifosato**, que tienen como objetivo acabar con los cultivos ilícitos, es por eso que “los campesinos han llamado a establecer una mesa de negociaciones con el gobierno pero solo se incumple y se deja plantada a la comunidad”.

Por otro lado, se denuncia el incumplimiento de la licencia ambiental por parte de la Hidroeléctrica Urrá, debido a que “no se ven las regalías de la Urrá”, además,  hay bastante presencia de militares y aunque la represa produce energía “no hay una casa del territorio que tenga energía eléctrica”, asegura el defensor de DDHH, quien agrega que “las personas que fueron desplazadas viven en condiciones infrahumanas, y no se les cumplió a los campesinos con sus tierras”.

Frente a la militarización de la represa, la hidroeléctrica argumenta que “no le pueden pedir retirar las tropas militares porque el Ministerio de Minas y Energía tiene unos convenios con el Ministerio de Defensa”, sin embargo, “**no se ha logrado ninguna respuesta de los ministerios”**, añade el miembro de ASODECAS.

El jueves **19 de marzo,** el campesinado empezará las gestiones pertinentes sobre su pliego de peticiones, **allí contarán con la presencia de la presidenta de la Unión Patriótica, Aida Avella,** quien ha demostrado su apoyo al campamento humanitario, que a pesar de ser un territorio de paz, se ha visto hostigado por las Fuerzas Militares.
