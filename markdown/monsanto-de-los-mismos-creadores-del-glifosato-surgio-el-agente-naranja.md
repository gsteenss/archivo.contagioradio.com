Title: Monsanto: De los mismos creadores del glifosato surgió el "Agente Naranja"
Date: 2015-05-12 12:27
Author: CtgAdm
Category: Ambiente, Otra Mirada
Tags: aspersión, colombia, EEUU, Glifosato, Monsanto, paz, proceso, Vietnam
Slug: monsanto-de-los-mismos-creadores-del-glifosato-surgio-el-agente-naranja
Status: published

###### Foto: Los Agrotóxicos Matan 

 

##### [12 May 2015 ] 

#####  

Durante la guerra de Vietnam, entre 1961 y 1971, los Estados Unidos utilizaron 16 herbicidas para fumigar las selvas y campos de Vietnam con el objetivo, aseguran, de acabar con la vegetación en la cual podían esconderse las guerrillas del Viet Cong.

Entre los herbicidas se encontraban el Agente Púrpura, el Agente Verde, el Agente Rosa, el Agente Blanco, el Agente Azul, y el Agente naranja, llamados así por los colores de las franjas que identificaban los barriles que guardaban los tóxicos. Este último, el Agente Naranja, es el más reconocido por los impactos que ocasionó durante la guerra, y las secuelas que persisten en las 3 generaciones de vietnamitas siguientes.

La aspersión de más de 12 millones de galones del Agente Naranja sobre el país asiático no sólo acabó con la vegetación: las 3.181 villas que fueron rociadas quedaron inutilizables para la siembra de alimentos, y fue tan tóxico que no solo afectó a los guerrilleros sobre los cuales se realizó la fumigación aérea, sino también a los soldados estadounidenses y a las cerca de 5 millones de personas que vivían en la región. La Cruz Roja estima que actualmente cerca 1 millón de personas han sido mutiladas, tienen discapacidades o problemas de salud como consecuencia de su contacto con el territorio infectado por el tóxico, incluyendo al rededor de 150 mil niños.

Entre las enfermedades más comunes producidas por el Agente Naranja se encuentran diversos tipos de cáncer, espina bífida, anormalidades reproductivas y defectos de nacimiento.

En la década de los años 80, estadounidenses veteranos de la Guerra de Vietnam instalaron demandas penales contra Monsanto y Dow Chemical, fabricantes de los agentes tóxicos, incluyendo el Naranja, por problemas de salud generados por el contacto con el herbicida durante la guerra. Sin embargo, las empresas persuadieron a las familias de los millones de veteranos de EEUU para llegar a un acuerdo por 180 millones de dólares, y evitar el juicio que se adelantaría en su contra.

Los procesos judiciales que intentaron emprenderse años después en contra de Monsanto quedaron inhabilitados por la Corte Suprema de los EEUU, argumentando que estas empresas eran contratistas del gobierno, y a pesar de haber producido el herbicida, no fue su responsabilidad el uso que el gobierno estadounidense hiciera de él.

Contrarresta este con el argumento presentado por Monsanto en su página web, en que asegura que "los militares de EEUU utilizaron el Agente Naranja de 1961 a 1971 para salvar las vidas de soldados de EE.UU. y sus soldados aliados deshojando la densa vegetación en las selvas de Vietnam y por lo tanto reduciendo las posibilidades de una emboscada.", cuando las vidas presuntamente salvadas terminarían sufriendo posteriorente los efectos y la muerte a manos del Agente Naranja.

Actualmente, en Colombia se discute la suspensión del uso de glifosato tanto en los cultivos de uso ilícito como en la producción de alimentos. La Organización Mundial de la Salud y las comunidades rurales en Colombia afectadas por el tóxico, cuentan con que el gobierno no espere 50 años, como en Vietnam, para reconocer los efectos altamente negativos de los herbicidas de Monstanto sobre la vida ambiental, animal y humana del planeta.

##### Fotografías de Brian Driscoll 

[![AgenteNaranja12](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/AgenteNaranja12.jpg){.alignleft .size-full .wp-image-8502 width="640" height="284"}](https://archivo.contagioradio.com/monsanto-de-los-mismos-creadores-del-glifosato-surgio-el-agente-naranja/agentenaranja12/) [![AgenteNaranja06](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/AgenteNaranja06.jpg){.alignleft .size-full .wp-image-8503 width="640" height="426"}](https://archivo.contagioradio.com/monsanto-de-los-mismos-creadores-del-glifosato-surgio-el-agente-naranja/agentenaranja06/) [![AgenteNaranja04](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/AgenteNaranja04.jpg){.alignleft .size-full .wp-image-8504 width="640" height="424"}](https://archivo.contagioradio.com/monsanto-de-los-mismos-creadores-del-glifosato-surgio-el-agente-naranja/agentenaranja04/) [![AgenteNaranja02 (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/AgenteNaranja02-1.jpg){.alignleft .size-full .wp-image-8505 width="640" height="285"}](https://archivo.contagioradio.com/monsanto-de-los-mismos-creadores-del-glifosato-surgio-el-agente-naranja/agentenaranja02-1/) [![AgenteNaranja09](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/AgenteNaranja09.jpg){.alignleft .size-full .wp-image-8506 width="640" height="425"}](https://archivo.contagioradio.com/monsanto-de-los-mismos-creadores-del-glifosato-surgio-el-agente-naranja/agentenaranja09/) [![AgenteNaranja](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/AgenteNaranja.jpg){.alignleft .size-full .wp-image-8507 width="640" height="422"}](https://archivo.contagioradio.com/monsanto-de-los-mismos-creadores-del-glifosato-surgio-el-agente-naranja/agentenaranja/)
