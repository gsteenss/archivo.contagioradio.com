Title: Estudiantes colombianos se movilizan contra el Acuerdo 2034
Date: 2014-12-16 15:47
Author: CtgAdm
Category: Video
Slug: estudiantes-colombianos-se-movilizan-contra-el-acuerdo-2034
Status: published

\[embed\]https://www.youtube.com/watch?v=JXnbs4Vk3i4\[/embed\]

### El jueves 16 de octubre se llevaron a cabo movilizaciones en las principales ciudades del país, para rechazar el Acuerdo 2034, exigir el pago de 12,5 billones de pesos que adeuda el Estado a las universidades públicas, y garantías a la autonomía y democracia universitaria. 
