Title: Legalización de sustancias psicoactivas podría aportar en reducción de la violencia
Date: 2020-08-05 13:55
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Amapola y Marihuana, Legalización de la marihuana
Slug: legalizacion-de-sustancias-psicoactivas-podria-aportar-en-reduccion-de-la-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/marihuana.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Legalización/ Pixabay

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La legalización del consumo de sustancias en Colombia, siempre ha sido un motivo de debate en Colombia, mientras el 6 de julio de 2016 entró en vigencia la ley que permite el uso medicado de la marihuana, **en julio de 2020 el Consejo de Estado tumbó el decreto 1884 de 2018 que ordenaba decomisar dosis personales a quienes las portarán en espacios públicos** reduciendo su accionar a decomisarla cuando se verifique que el portador la va a comercializar o distribuir. Ante esta novedad se busca llevar al Congreso la legalización de la marihuana con fines recreativos de forma regulada como alternativa a guerra contra el narcotráfico.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con la discusión de nuevo sobre la mesa, **Gloria Miranda, magister en Construcción de paz y asesora del senador Gustavo Bolívar** se refirió a las posibilidades de que el proyecto de ley sea aprobado en el Senado, destacando que este entra por la Comisión I del Senado, instancia en la existe una alianza interpartidista de varios sectores y donde ya se cuentan con 11 votos de 12 que serían necesarios para que sea aprobado. [(Lea también: Decreto reglamenta el uso medicinal de la Marihuana en Colombia)](https://archivo.contagioradio.com/decreto-reglamenta-el-uso-medicinal-de-la-marihuana-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hay una bancada muy amplia que se ha dado cuenta que la lucha contra el narcotráfico de la forma clásica es realmente fracasada por lo que es necesario apostarle a otros paradigmas de la lucha contra las drogas que apunten a la regulación y que tiene aspectos positivos en otros países"

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el discurso de sectores conservadores que señalan que este tipo de propuestas es promover el narcotráfico, la evidencia ha demostrado que en la mayoría de países que ha optado por esta opción, ha reducido la incidencia del narcotráfico, algo que implica un proyecto de control y prevención para su consumo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto se señala que existirían cuatro mecanismos de acceso que incluyen dispensarios de cannabis, membresías sin ánimo de lucro que incluyan a personas en condición de calle sin generar un consumo problemático, sitios web emulando servicios de países como Canadá y un último elemento que quedaría a disposición de los entes territoriales.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Acceso a sustancias ha cambiado con la pandemia del Covid-19

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mientras la Policía impusó cerca de 350.000 comparendos y decomisó (234 kilogramos de marihuana hasta la fecha en que el Consejo de Estado tomó una nueva decisión sobre su consumo en los espacios públicos, Juan Camilo Carvajal, investigador en educación y consumo de sustancias psicoactivas del Colectivo [Échele Cabeza](https://twitter.com/echelecabeza)agrega que el aislamiento para mitigar el efecto del virus ha generado un cambio en el consumo de sustancias y en la reducción de espacios y reuniones que facilitaban su distribución.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según registros del Observatorio de Drogas de Colombia y encuestas realizadas por el Colectivo Échele Cabeza, las sustancias sintéticas como anfetaminas o el éxtasis dejaron de consumirse mientras que las estimulantes como la cafeína y el cannabis se dispararon, al igual que el aumento de cocaína".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Cuando hablamos de una regularización estamos hablando de un acuerdo social que no está abierto a todos pero lo hace de una manera informada y permite tomar decisiones de forma más acertada"**, señala el experto sobre la necesidad de hablar de la educación sobre las drogas. [(Le puede interesar: Desmitificando la Marihuana en Colombia)](https://archivo.contagioradio.com/desmitificando-la-marihuana-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Paradigma de regulación y legalización está dejando resultados en otras naciones

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde 2013, el Senado uruguayo sanciono la ley definitiva que legalizó la venta y producción de marihuana, así como la producción de cáñamo industrial y el autocultivo de hasta seis plantas para uso personal convirtiendo a Uruguay en en el primer país en legalizar la producción y venta de marihuana.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En este país, el mercado promedio de los primeros años fue de 35.200 personas habilitadas para su consumo de esta población el 55% accedía al mercado regulado mientras el mercado ilegal de recibir una cifra cercana a los 20 millones de euros mientras el mercado ilícito desde la frontera con Paraguay descendió.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Otros casos como el de Colorado, estado que se convirtió en el primer mercado público de marihuana de Estados Unidos, revela que los delitos relacionados con marihuana disminuyeron desde que se reguló el mercado, al igual que los casos de posesión que vinculaban a adolescentes y población afroamericana; una de las más estigmatizadas por la Fuerza Pública en el país norteamericana.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Cuando se pierde lo sagrado, se pierde el respeto"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En contraste, Camilo Niño, coordinador de la omisión de Territorios Indígenas señala que **las iniciativas de erradicación de forma manual o con el uso de glifosato no están dando efecto por lo que debe considerarse la legalización como una alternativa para eliminar las causa de la violencia** originadas en los territorios por el consumo de coca y que ha golpeado en particular a las comunidades indígenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"La planta de hoja de coca es sagrada y para usted usarla necesita un permiso, cuando se hace un uso indebido de esta, el padre espiritual cobra, ahí es cuando el narcotráfico se manifiesta, la violencia se da porque ese uso sagrado se ha perdido". Agrega que cuando se realizan las jornadas de erradicación no se tiene en cuenta el uso que se le va a hacer a la hoja de coca, por lo que los pueblos indígenas están dispuestos a mostrar su utilidad en otros usos que se han tergiversado.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Están poniendo en problema la soberanía alimentaria de las comunidades indígenas y la contaminación del agua, es necesario buscar nuevas medias para mitigar la violencia en los territorios
>
> <cite>Camilo NIño</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Gloria Miranda señala al respecto que desde el marco jurídico del proyecto de ley que busca la legalización, generó un capítulo para las comunidades indígenas, se estableció que el 50% de las licencias que se otorgarían para la venta del cannabis como producto sería dirigido a personas que habitan en lugares afectados por el conflicto armado, personas víctimas de la guerra y mujeres cabezas de familia, mientras que para el tratamiento de semillas y el autocultivo no sería necesario acudir a las compañías que han patentado las semillas para su siembra.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
