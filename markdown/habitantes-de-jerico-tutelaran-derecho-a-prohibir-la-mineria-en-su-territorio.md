Title: Habitantes de Jericó tutelarán derecho a prohibir la minería en su territorio
Date: 2017-12-19 13:24
Category: Ambiente, Nacional
Tags: Anglo Gold Ashanti, Jericó, Jericó Antioquía, Mineria, Tribunal Administrativo de Antioquia
Slug: habitantes-de-jerico-tutelaran-derecho-a-prohibir-la-mineria-en-su-territorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/jericó-minería-e1550514587907.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jericó Alerta] 

###### [19 Dic 2017] 

Luego de que en junio del presente año el Consejo Municipal de Jericó, en Antioquia, tomara la decisión de prohibir la minería en ese territorio, incluyendo el proyecto minero Quebradona de la Anglo Gold Ashanti, el Tribunal Superior de Antioquia **dejó sin efecto esta declaratoria**. Los defensores del ambiente y la población de Jericó manifestaron su rechazo ante esta decisión.

Fernando Jaramillo, coordinador de la Mesa Ambiental de Jericó, manifestó que “el Tribunal Administrativo de Antioquia **ha venido desconociendo la pretensión justa y legitima** de los habitantes de esta zona del departamento desde hace varios años cuando se han tomado iniciativas para proteger el territorio y la vocación agrícola”.

Además, enfatizó en que el territorio está amenazado por las empresas mineras como Anglo Gold Ashanti que, en particular en Jericó, ha pretendido **explotar un yacimiento de cobre, oro y plata**. Sin embargo, no solo ha sido Jericó el único municipio que se ha opuesto “al modelo económico de desarrollo del país”. Jaramillo indicó que son 11 municipios de la región del sur este antioqueño “que hemos aprobado acuerdos para prohibir la minería”.

### **Comunidad de Jericó instalará acción de tutela** 

Ante esta decisión, las comunidades **van a presentar una acción de tutela** ante el Consejo de Estado, donde demostrarán como “el fallo del Tribunal Administrativo de Antioquia ha violado el debido proceso”. Esto lo explican teniendo en cuenta que ha habido varias intervenciones al proceso que se ha adelantado en el Tribunal.

Esto quiere decir que el proceso de revisión “solamente tuvo en cuenta las intervenciones que están **a favor de la invalidez del acuerdo**”. Además, las intervenciones que hicieron los concejales de Jericó y la ciudadanía, “fueron casi en su totalidad desconocidas”. (Le puede interesar: ["Concejo de Jericó también le dice no a la minería"](https://archivo.contagioradio.com/jerico_no_mineria/))

También, argumentarán que es improcedente que el Tribunal Administrativo de Antioquia tome como sustento de sus argumentaciones el artículo 37 del código de minas “que fue declarado **inexequible el año pasado** por la Corte Constitucional”.

Los defensores del ambiente creen que es inadmisible que el mismo Tribunal **“no tome en consideración** la totalidad de la sentencia de la Corte, la 445, donde se dice que los municipios están facultados para prohibir la minería en sus territorios. Por esto, el acuerdo de Jericó tomó las medidas necesarias para defender el patrimonio ecológico y cultural del municipio, atendiendo lo estipulado en la Constitución.

Finalmente, Jaramillo enfatizó en que, tanto el Tribunal Superior de Antioquia como la Gobernación del mismo departamento **“pretenden desviar la atención** de la ciudadanía haciendo creer que nos estamos extra limitando y a través de ese acuerdo, estuviéramos ordenando el territorio, lo cual no es el caso”.

<iframe id="audio_22739537" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22739537_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
