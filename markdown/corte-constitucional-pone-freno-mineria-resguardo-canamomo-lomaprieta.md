Title: Corte Constitucional pone freno a minería en Resguardo Cañamomo y Lomaprieta
Date: 2017-01-31 09:00
Category: Ambiente, Nacional
Tags: Corte Constitucional, licencias ambientales, Mineria
Slug: corte-constitucional-pone-freno-mineria-resguardo-canamomo-lomaprieta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/corte_constitucional-Fuero_Penal_Militar_contagioradio-e1485870742527.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CM& 

###### [31 Ene 2017] 

[Tras años denunciando la presencia de empresas mineras afectando los derechos de las comunidades indígenas del Resguardo]**Cañamomo y Lomaprieta donde se encuentran asentadas las comunidades Embera-Chamí en Caldas, la Corte Constitucional** [le advirtió a la Agencia Nacional de Minería (ANM) que tiene plazo de un año para suspender los procesos de contratación, formalización e inscripción de los títulos mineros que han sido otorgados sobre esos territorios indígenas.]

[La decisión se toma con el objetivo de proteger los derechos fundamentales de las comunidades indígenas ordenando] a las **Alcaldías de los municipios de Riosucio y Supía**[ que realicen todos los “procedimientos policivos tendientes a clausurar aquellas minas que no se encuentran en proceso de formalización y titulación o que no hayan sido autorizadas por las autoridades indígenas”.]

[De esta manera, y aceptando los argumentos presentados por el representante de las comunidades indígenas, la Corte le indicó al Ministerio del Interior que deberá expedir una circular para que los concesionarios mineros adelanten actividades de]**socialización de las actividades de exploración que se busca llevar a cabo en esas regiones, exponiendo los impactos ambientales** que implican las actividad minera en territorios ancestrales.

[En Ríosucio y Supía, lugar donde se encuentra ubicado el Resguardo Indígena Cañamomo-Lomaprieta, las comunidades denuncian que las empresas mineras canadienses como]**Anglogold Ashanti**[ **y Antioquia Gold** han intentado adelantar actividades engañando a la población.]

[“Nosotros desde nuestro punto de vista de resistencia y defensa territorial, le decimos que no a esas empresas, y al Gobierno que no nos hable de consulta previa, porque el territorio es nuestro y en lo nuestro no negociamos nada”, expresa Fabio de Jesús Moreno Herrera, vocero de la Asociación de Mineros Artesanales del Resguardo, quien agrega que no permitirán que se extraiga oro de lugares sagrados para la comunidad.]

### Corte Constitucional, nuevamente a favor de las comunidades 

[No es la primera vez que la Corte falla a favor del ambiente y los pueblos étnicos. “En estos momentos **la mejor agencia ambiental es la Corte Constitucional, que es una corte progresista en términos ambientales y de derechos**”, asegura  el profesor Mario Alejandro Pérez-Rincón, quien lideró la investigación de un reciente informe elaborado por el I]nstituto Cinara[ de la Universidad del Valle.]

[De acuerdo con Pérez-Rincón, aunque ha hecho falta seguridad jurídica para garantizar los derechos de las comunidades en contraposición a los megaproyectos extractivistas, **el Alto Tribunal ha suspendido cerca de 24 de los 115 proyectos minero-energéticos**, nombrados en el informe de Universidad del Valle.]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
