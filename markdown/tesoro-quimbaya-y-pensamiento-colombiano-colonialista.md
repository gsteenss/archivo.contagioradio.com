Title: Tesoro Quimbaya y pensamiento colombiano colonialista
Date: 2016-02-02 08:20
Category: Cesar, Opinion
Tags: historia, tesoro quimbaya
Slug: tesoro-quimbaya-y-pensamiento-colombiano-colonialista
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/tesoro-quimbaya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: You Tube 

#### **[Cesar Torres del Río  ](https://archivo.contagioradio.com/cesar-torres-del-rio/)** 

###### 2 Feb 2016

No nos bastaron los saqueadores de los siglos XVI al XX (calendario cristiano) monárquicos españoles e ingleses además de los “republicanos” norteamericanos (Panamá). En el XXI y en nuestro país pululan el pensamiento colonialista y la reverencia abyecta: burócratas y mandarines coinciden en el escenario de la reificación alienatoria con los intelectuales (filósofos, profesores, literatos, historiadores …) a la hora de los balances históricos relativos al acto colonialista y a sus múltiples consecuencias. Tales especies dinosáuricas son inmanentes a la supervivencia del colonialismo; hacen parte de la corte del Príncipe.

Parcialmente esta caracterización  nos sirve para analizar la postura gubernamental, específicamente de la Cancillería de Colombia, frente a la demanda interpuesta por un ciudadano para que a nuestro país le sea devuelto su patrimonio, “Tesoro” Quimbaya le dicen, que fue regalado a la monarquía española en 1893.

Después de varios años y de ires y venires en los estrados, el ciudadano Felipe Rincón logró por fin que mediante tutela la Corte Constitucional estudiara el caso. La audiencia pública  se llevó a cabo el jueves 28 de 2016 y se espera una decisión en los próximos meses. Lo que se está pidiendo es simple: que la Corte exija a la Cancillería adelantar las acciones necesarias para que España nos devuelva el patrimonio colombiano.

Anotemos, y no al margen, que el azar está presente en esta coyuntura. Resulta que el tío bisabuelo de la actual canciller María Ángela Holguín fue el presidente Carlos Holguín, quien tuvo la “brillante” idea de regalarle a la monarquía de España nuestro patrimonio en razón al papel que ésta había desempeñado en el contencioso por límites con Venezuela y que nos había beneficiado.

Pues bien, en la audiencia resaltó el pensamiento colonialista de la Cancillería; la vicecanciller Patty Londoño (quien siempre ofició como académica) anotó sin perplejidad que no hay mecanismos (jurídicos) para proceder a una acción como la que se exigiría; y agregó que la decisión del presidente Holguín había sido legítima y legal (?). Defensa de bolsillo, es decir, reverencial.

Mecanismos hay, por supuesto. Quienes saben del asunto afirman que la vía está señalada en el artículo 15 de la Convención de París de 1970. Esperamos que la Corte lo considere, junto con otras herramientas jurídico-políticas, y tome una decisión en favor de Colombia.

Sobre el hecho bochornoso protagonizado por la Vicecanciller - con el visto bueno de la sobrina bisnieta -  hay que recordar otro no menos escandalizante, colonialista y execrable: el que se presentó cuando intelectuales colombianos (Gabriel García Márquez, Fernando Botero, Álvaro Mútis, Fernando Vallejo, William Ospina, Darío Jaramillo Agudelo y Héctor Abad Faciolince) enviaron una carta al primer ministro José María Aznar, publicada en *El País* (Madrid) el 18 de marzo de 2001:

Aunque las guerras de independencia hayan cortado el cordón umbilical que nos unía políticamente a la Península, los colombianos no hemos dejado de sentir … que nuestra imaginación, nuestra lengua mayoritaria, nuestros referentes culturales más importantes provienen de España. Aquí nos mezclamos con otros riquísimos aportes de la humanidad, en especial con el indígena y el negro, pero nunca hemos renegado, ni podríamos hacerlo, de nuestro pasado español. Nuestros clásicos son los clásicos de España, nuestros nombres y apellidos se originaron allí casi todos, nuestros sueños de justicia, y hasta algunas de nuestras furias de sangres y fanatismos, por no hablar de nuestros anticuados pundonores de hidalgo, son una herencia española (Tomado de Alexander Betancourt, **Historia y nación**, Medellín, La carreta histórica, 2007, p. 228).
