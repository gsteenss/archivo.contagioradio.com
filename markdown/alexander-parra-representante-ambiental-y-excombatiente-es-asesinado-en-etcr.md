Title: Alexander Parra, representante ambiental y excombatiente es asesinado en ETCR
Date: 2019-10-25 11:31
Author: CtgAdm
Category: DDHH, Nacional
Tags: Alexander Parra, asesinato de excombatientes, etcr, Mesetas, Meta
Slug: alexander-parra-representante-ambiental-y-excombatiente-es-asesinado-en-etcr
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Alexander-Parra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CPDHColombia] 

El pasado 25 de octubre fue asesinado el excombatiente **Alexander Parra** al interior del Espacio Territorial de Capacitación y Reincorporación (ETCR) Mariana Páez en Meta por hombres encapuchados quienes atentaron contra la humanidad del **representante ambiental de su comunidad. Con el asesinato de Alexander ya son 168 los excombatientes asesinados desde la firma del Acuerdo y  88 durante el Gobierno Duque. **

Según información preliminar, alrededor de las 9:30 PM Alexander fue atacado por encapuchados que ingresaron al ETCR y se desplazaron hasta la casa del excombatiente  disparándole en numerosas ocasiones para luego huir del lugar.  [(Lea también: Asesinan en Patía, Cauca al excombatiente Dago Galíndez)](https://archivo.contagioradio.com/asesinan-en-patia-cauca-al-excombatiente-dago-galindez/)

Pese a que estos espacios cuentan con anillos de seguridad de Policía y Ejército, Pastor Alape, delegado al Consejo Nacional de Reincorporación, señaló que **"la seguridad no puede darse con un enfoque militarista, hay que desarrollar un enfoque en derechos humanos y reconciliación**, de nada sirve un soldado o un policía en el territorio cuando desde la institucionalidad se promueve la polarización".

En ese sentido, desde el Partido se realizó un "llamado de atención para hacer una investigación donde el Estado asuma esa responsabilidad", priorizando la necesidad del hallazgo de los autores intelectuales por encima de los materiales.

### Alexander Parra ahora hacía la paz con la naturaleza 

El excombatiente quien hizo parte de las FARC por más de 30 años, era reconocido por su liderazgo y su trabajo por la reconciliación,  además era delegado al Consejo Territorial de Reincorporación del departamento del Meta. Alexander también fue esposo de la candidata al concejo por el Partido FARC, Luz Marina Giraldo, juntos habían solicitado esquemas de protección a la Unidad Nacional de Protección.

En una de sus últimas declaraciones, Alexander, quien era delegado de ambiente del ETCR Mariana Páez, manifestó que podía notar un mayor interés de la comunidad en términos de participación, "pienso que la paz nos ha llevado a ese despertar, ya no está la zozobra de los tiros o de las bombas, ya hay un reencuentro de la familia colombiana". [(Con homicidio de Servio Cuasaluzán, son 134 excombatientes asesinados)](https://archivo.contagioradio.com/con-homicidio-de-servio-cuasaluzan-son-134-excombatientes-asesinados/)

> Duele y mucho, otro reincorporado de Farc asesinado. Con Alexander Parra van 168 personas desde que firmaron el acuerdo. Su esposa es candidata al concejo por el Partido Farc en Mesetas. Hasta cuándo capuchas y bala? <https://t.co/YvyNs5Zdrs> vía [@elespectador](https://twitter.com/elespectador?ref_src=twsrc%5Etfw) [pic.twitter.com/XmrnumzYRh](https://t.co/XmrnumzYRh)
>
> — JesusAbadColorado (@AbadColorado) [October 25, 2019](https://twitter.com/AbadColorado/status/1187598260594233344?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
El senador Carlos Lozada se refirió al hecho y agregó que "a la rabia y el dolor que produce se suma la impotencia al ver que desde el Gobierno no se toman medidas eficaces para garantizar la vida de los exguerrilleros que firmamos la paz".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
