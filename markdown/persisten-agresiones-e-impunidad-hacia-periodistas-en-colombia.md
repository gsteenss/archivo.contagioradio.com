Title: Periodistas fueron agredidos 147 veces este año en Colombia
Date: 2016-09-08 12:14
Category: DDHH, Otra Mirada
Tags: agresiones a periodistas, colombia, impunidad
Slug: persisten-agresiones-e-impunidad-hacia-periodistas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/la-prensa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Laprensa] 

###### [08 de Sept de 2016] 

El 8 de septiembre se conmemora el día internacional del periodista, motivo por el cual Reporteros Sin Fronteras ha presentado un informe sobre la situación actual del periodismo en el mundo. En el documento se expone una clasificación de los países con mayores dificultades para el ejercicio de la libertad de prensa, **Colombia ocupa el puesto 138 en la lista, ya que prevalecen los casos de agresiones y violaciones contra periodistas en el país.**

Hasta el momento, [durante el 2016 han sido registradas un total de 147 agresiones](https://archivo.contagioradio.com/en-2016-se-han-registrado-70-agresiones-contra-periodistas-en-colombia/)directas y 232 víctimas, según el informe emitido por la FLIP (Fundación para la Libertad de Prensa). Algunas de las principales violaciones corresponden a desplazamientos **(2)**, agresiones **(25)**, amenazas **(52)**, estigmatización **(11)**, daños contra la infraestructura **(11)**, detención ilegal **(1)**, obstrucción al trabajo periodístico **(29)**, secuestro **(3) y** tratos inhumanos y degradantes **(10).**

Para el 2015, la FLIP **reportó un total de 159 casos de asesinatos a periodistas**, desde el año 1977, de los cuales, 112 de ellos trabajaban para medios pequeños. Las regiones en las que se presentó mayor número de casos fueron Antioquia, Valle del Cauca, Santander, Caquetá y Bogotá.

De igual manera, el informe presentado este año por el Centro Nacional de Memoria Histórica, titulado “*La palabra y el silencio”*, señala que, de un total de **140 periodistas asesinados por ejercer su oficio, entre los años 1977 al 2012**, solo 4 casos han obtenido condena para el autor intelectual del homicidio, sin olvidar, el 47% de casos prescritos para el 2015.

De acuerdo con la organización Reporteros sin fronteras, l[a convivencia que existe entre algunos Estados y los intereses privados en contra de la libertad e independencia del periodismo](https://archivo.contagioradio.com/libertad-de-prensa-en-colombia-2015/), resultan ser uno de los móviles más frecuentes en los casos de agresión y asesinatos en contra periodistas.

En el marco de este día, se presentará un breve panorama del ejercicio periodístico libre e independiente en Colombia y se expondrá la **aguda problemática en materia de agresiones, violaciones, e índices de impunidad que prevalecen en el país.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
