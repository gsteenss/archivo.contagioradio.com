Title: ¿Existe presión y vulneración a la autonomía judicial en el caso Andino?
Date: 2018-09-04 15:49
Author: AdminContagio
Category: Judicial, Nacional
Tags: Centro comercial andino, ELN, Fiscalía, MRP
Slug: presion-vulneracion-judicial-caso-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/jovenes-detenidos-andino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [4 Sept 2018] 

Por cuenta del nuevo proceso en que se ha involucrado a los jóvenes implicados en el caso del **centro comercial Andino,** el viernes 31 de agosto les fue dictada medida de aseguramiento intramural, decisión frente a la cual, la bancada de la defensa se mostró escéptica, alegando falta de garantías al debido proceso.

**Gloria Silva,** abogada de la defensa, aseguró que la juez de control de garantías dictaminó la medida por hechos ocurridos en Medellín, añadiendo que en este nuevo proceso se acusa a los jóvenes de cometer el **delito de rebelión,** y con ello se buscaría probar  su relación con el **Movimiento Revolucionario del Pueblo (MRP) y el ELN.**

https://twitter.com/FiscaliaCol/status/1035661153618325504

Silva anunció que estarán atentos a la decisión de **segunda instancia, **y espera que tenga una interpretación del derecho que brinde garantías a sus defendidos. Sin embargo, se mostró escéptica sobre esa posibilidad, porque observa una presión y vulneración de la autonomía judicial; ejemplo de ello, es la situación de **la "juez que con razones constitucionales y legales dio la orden de libertad para los jóvenes hace 2 semanas, actualmente está siendo investigada penal y disciplinariamente".**

### **"Aquí se persigue el derecho a la libertad y a quien la concede"** 

La abogada señaló que con la investigación a la juez, y la Ley con la que se busca prorrogar la medida de aseguramiento para personas que pertenecen presuntamente a estructuras criminales, **se observa una persecución al "derecho a la libertad y a quien la concede"**. Por eso, Silva sostuvo que es probable que se extiendan los términos en la investigación de los jóvenes, y añadió que "no podemos esperar del Estado colombiano justicia e imparcialidad". (Le puede interesar: ["La Fiscalía está montando pruebas: Defensa de jóvenes implicados en caso Andino"](https://archivo.contagioradio.com/fiscalia-esta-montando-pruebas-andino/))

<iframe id="audio_28316981" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28316981_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU) 
