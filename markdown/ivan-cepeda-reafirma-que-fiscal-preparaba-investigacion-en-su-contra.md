Title: Iván Cepeda reafirma que Fiscal preparaba investigación en su contra
Date: 2019-04-29 16:21
Author: CtgAdm
Category: Judicial, Política
Tags: Fiscalía General de la Nación, Iván Cepeda, jurisdicción especial para la paz, Nestor Humberto Martínez
Slug: ivan-cepeda-reafirma-que-fiscal-preparaba-investigacion-en-su-contra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-79.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Iván Cepeda] 

La Fiscalía General de la Nación respondió a un derecho de petición que el senador Iván Cepeda había radicado el pasado 26 de abril, por medio del cual, le solicitó al ente investigador sobre información relacionado a un presunto caso en su contra.

El senador había indicado en una rueda de prensa que la Fiscalía lo tenía bajo la mira por servir  presuntamente como un intermediario entre el Clan del Golfo y la introducción de un artículo a la ley estatutaria de la Jurisdicción Especial para la Paz, que el Fiscal Nestor Humberto Martínez señaló como una garantía a la no extradición para narcotraficantes.

Sin embargo, la Fiscal aclaró, a través de una carta, que no existe alguna investigación en su contra y que no hay más información para entregar relacionado con el caso dado que no existe. (Le puede interesar: "[Iván Cepeda denuncia presunta investigación de Fiscalía en su contra](https://archivo.contagioradio.com/ivan-cepeda-denuncia-presunta-investigacion-de-fiscalia-en-su-contra/)")

Por su parte, el Senador Cepeda afirmó nuevamente que el Fiscal había estado recogiendo presuntos elementos para lanzar una investigación en su contra, que le hubiera servido para el voto en el Senado que se dará hoy sobre las objeciones a la ley estatutaria, y por tal razón, había decidido presentar de manera pública el derecho de petición ante la Fiscalía y "salirle al paso".

Al respecto a la declaración de la Fiscalía, concluyó que "ha quedado saldado este asunto pero con el Fiscal hay que andar muy, pero muy atentos porque contra sus contradictores políticos y judiciales el suele efectivamente desarrollar proceso penales que regularmente no tienen ninguna clase de sustento".

> Respuesta del Fiscal General de la Nación, Néstor Humberto Martínez Neira, a la petición del Senador Iván Cepeda [pic.twitter.com/QNdOAITlG2](https://t.co/QNdOAITlG2)
>
> — Fiscalía Colombia (@FiscaliaCol) [28 de abril de 2019](https://twitter.com/FiscaliaCol/status/1122518810647572480?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_35103109" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35103109_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
