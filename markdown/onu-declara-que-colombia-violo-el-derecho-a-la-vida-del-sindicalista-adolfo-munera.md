Title: ONU declara que Colombia violó el derecho a la vida del sindicalista Adolfo Múnera
Date: 2020-06-03 15:43
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Adolfo Munera, Alberto Brunori, cspp, justicia, ONU
Slug: onu-declara-que-colombia-violo-el-derecho-a-la-vida-del-sindicalista-adolfo-munera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Adolfo-Munera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: CSPP*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la lucha de más 18 años de la familia de **sindicalista Adolfo Munera López**, por la justicia y la verdad de su asesinato; el pasado 19 de mayo el **Comité de Derechos Humanos de la ONU, falló en contra del Estado colombiano por no investigar a los responsables de este homicidio**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El caso llegó a la comunidad internacional, gracias al respaldo del Comité de Solidaridad con los Presos Políticos (CSPP), **quienes se encargaron de presionar por el esclareminento del crímen de Múnera, ante la inoperancia de la justicia interna.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Alberto Brunori, director de la Oficina la Alta Comisionada de la ONU en Colombia para los Derechos Humanos, reconoció la lucha de casi dos décadas de la familia de Adolfo, *"en la búsqueda de Justicia verdad y reparación".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

He indicó que el Estado tiene la responsabilidad de responder a puntos como; *"llevar a cabo una investigación pronta exhaustiva, eficaz, imparcial, dependiente y transparente sobre las circunstancias del homicidio con el objetivo del establecimiento de la verdad".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado deben proporcionar los autores a los familiares información detallada sobre los resultados de estas investigaciones, también una *"indemnización adecuada a los familiares, incluyendo los gastos legales y razonablemente".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y enfatizó en que el la Oficina reconoce que, *"**la [Fiscalía](https://archivo.contagioradio.com/inpec-niega-libertad-de-cesar-barrera-y-cristian-sandoval-victimas-del-caso-andino/)hizo investigaciones ante este hecho, pero que a pesar de ello la investigación aún no es completa,** especialmente al punto de la investigación y sanción de los presuntos autores intelectuales"*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La lucha de Adolfo Munera en contra de las transnacionales

<!-- /wp:heading -->

<!-- wp:paragraph -->

Adolfo Múnera, hacía parte de la junta directiva del **Sindicato Nacional de Trabajadores del Sistema Agroalimentario** (Sinaltrainal), y durante años defendió los derechos de los trabajadores de Coca-Cola; a pesar de ello durante 1997 fue señalado, perseguido e investigado de pertenecer al [ELN](https://archivo.contagioradio.com/politica-exterior-de-colombia-obedece-a-intereses-de-eeuu-congresistas/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Situación que según su esposa **Gladys Rincón de Munera, lo llevó a salir de la ciudad varias veces y vivir con miedo cada día,** *"no entiendo porque lo asesinaron, mi esposo no era guerrillero, lo asesinaron solo por ser un sindicalista, y por eso pedimos justicia".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Adolfo Munera, fue asesinado a la salida de la casa de su madre el 31 de agosto de 2002 en Barranquilla;* ante ello el Estado colombiano indicó que se trato de un delito de intereses políticos y cerró la investigación condenando a 17 años de prisión al autor material.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dejando de lado las investigaciones de los autores intelectuales y *"**la eventual responsabilidad de la multinacional Coca-Cola que había sido denunciada públicamente por vínculos con grupos paramilitares**”*, indicó Franklin Castañeda, presidente del [CSPP](http://www.comitedesolidaridad.com/es/content/comit%C3%A9-de-ddhh-de-la-onu-declara-al-estado-responsable-por-violaci%C3%B3n-del-derecho-la-vida-y).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, **Javier Correa, secretario general de Sinaltrainal** señaló, que el asesinato de Adolfo Munera, *"se da cuando en el país estaba gobernando un partido de ultraderecha, donde se generó una persecución sistemática e integral contra todo el movimiento sindical y social*", destacando que **es un patrón que hoy nuevamente se está repitiendo en el país.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Hoy el país reclama justicia social y defiende sus anhelos de paz , pero lo único que recibe es represión, criminalización y exterminio por parte del Gobierno"*
>
> <cite>Javier Correa | secretario general de Sinaltrainal</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Asímismo destacó que Sinatrainal **ha sido víctima de 32 asesinatos, 11 de trabajadores de la trasnacional Coca- Cola,** *"somo una organización que ha sido víctima de encarcelamiento en más de 48 ocasiones una organización que tiene compañeros exiliados, y una constante política de persecución".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"El asesinato de Adolfo lo entendemos como estrategia de silenciamiento, nos sentimos muy agradecidos porque en este momento esta decisión del comité de Derechos Humanos mantiene viva la esperanza de llegar a los autores intelectuales de los crímenes que se cometen en Colombia, y romper de alguna manera la cadena de impunidad, y seguir nuestra lucha por la verdad la justicia y la reparación"*, concluyó Correa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente **Raffaele Morgantini integrante del Centro Europa-Tercer Mundo (CETIM)**, organización internacional que apoyó el proceso junto a la CSPP y Sinaltrainal para que se llevara a la ONU, señaló,*"que como organización comprometida con la promoción de los Derechos Humanos, la justicia social y la participación popular felicitamos esta decisión histórica"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y ratificó que lo que rompió Colombia a, *"fue un pacto internacional que va más allá de las leyes nacionales, y en donde se debe comprometer a cumplirlo de manera inmediata"*, y relató **que este paso es un ejemplo hacia las luchas que se dan en todo el mundo** por la defensa de la vida y los DDHH.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"small"} -->

*Le puede interesar ver: Conferencia de prensa con CETIM | CSPP | SINALTRAINAL| ONU Human Rights | Gladys Rincón de Munera*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F598045757513187%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
