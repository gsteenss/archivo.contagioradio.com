Title: Mujeres critican patriarcado en congreso de abogados javerianos
Date: 2016-10-27 13:16
Category: Mujer, Nacional
Tags: congreso de abogados, Javeriana, javerianos, mujeres, paz
Slug: mujeres-critican-patriarcado-en-congreso-de-abogados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Congreso-Javeriana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Change Org] 

###### [27 Oct de 2016] 

**Varias mujeres han manifestado su descontento frente a la nula participación de ellas en el XVII Congreso de Abogados Javerianos "Desarrollo y Paz", convocado en Cali.** Así lo dejaron saber en una carta abierta enviada a la Decana de la Facultad de Humanidades y Ciencias Sociales y al Decano de la Facultad de Ciencias Jurídicas de la Universidad Javeriana de Bogotá.

Andree Viana, abogada egresada de este claustro, aseguró que hay que analizar y ver que en el evento están 29 ponentes, 4 moderadores, es decir de 33 personas y solo hay 1 mujer “**es increíble que en estos temas tan relevantes solamente 1 de esas personas es mujer. Esta realidad contrasta con la de la universidad donde la población estudiantil activa de la facultad de derecho es de 53%”** aseveró. Le puede interesar: [Las mujeres: gestoras en las políticas e implementación de la paz](https://archivo.contagioradio.com/las-mujeres-gestoras-en-las-politicas-e-implementacion-de-la-paz/).

En la misiva, las mujeres también aseguran que "Convocar a un Congreso sobre Desarrollo y Paz, sin darle a la mujer la participación a que tiene derecho y el protagonismo que merece, y negándole el carácter de interlocutora legítima en un diálogo igualitario, contrasta con la realidad del país y está lejos de lo que se puede esperar de una Universidad que ha participado en la construcción de sociedad y en su formación como motor de desarrollo y cambio social".

A lo que Viana agrega “las mujeres han sido la que con mayor notoriedad han asumido el salto importante de su condición de ser víctimas a ser ciudadanas activas y no esto no se puede desconoce”.

Andree aseguró que lo que  han pretendido con la carta es hacer un llamado para que la realidad de la universidad comulgue con la realidad del país y que “**si la universidad está comprometida a ayudar a superar el conflicto armado debe comprometerse también con políticas que permitan derribar los obstáculos que hemos tenido en la igualdad** y superar modelos de producción patriarcal del conocimiento, porque a partir de la producción plural del conocimiento, pluraliza también la discusión política”

Además, otro de los objetivos de esta manifestación simbólica es dejar constancia de lo sucedido “**de izquierda a derecha somos muchísimas mujeres las que nos encontramos en coincidencia con la opinión de que la universidad tiene que ser plural y tiene que tener prácticas que en realidad integren la perspectiva de género, que derrumben los modelos patriarcales**” puntualizó la abogada.

Ante la respuesta informal en la que se ha manifestado a este grupo de mujeres que lo que ha sucedido en este Congreso de abogados javerianos, no ha sido planeado ni ejecutado con ninguna mala intención, Viana agrega “ese hecho preocupa mucho, porque **estamos ante una normalización de la exclusión y es preocupante porque cuando a la gente todo le parece normal y solamente es maligno aquello que expreso, entonces todos los modelos patriarcales pasan desapercibidos**”

Estas mujeres también han manifestando que la universidad debe luchar contra ese tipo de prácticas “ya estamos en el año 2016 es hora de vacunarnos contra esta pre modernidad que castiga a toda nuestra sociedad colombiana, no solo a las mujeres” conlcuye Viana. Le puede interesar: [América Latina retrocede en la garantía de derechos para las mujeres](https://archivo.contagioradio.com/america-latina-retrocede-en-la-garantia-de-derechos-para-las-mujeres/).

<iframe id="audio_13505949" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13505949_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
