Title: Van más de 400 familias desalojadas forzosamente por Hidroituango
Date: 2015-01-27 16:43
Author: CtgAdm
Category: Ambiente, Movilización
Tags: Ambiente, Hidroituango, Juan Manuel Santos, Movimiento Ríos Vivos
Slug: van-mas-de-400-familias-desalojadas-forzosamente-por-hidroituango
Status: published

##### [Foto: polinizaciones.blogspot.com]

##### [Isabel Cristina Zuleta] 

<iframe src="http://www.ivoox.com/player_ek_4005466_2_1.html?data=lZWdl5maeo6ZmKiakp2Jd6Knl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjdTVw8rZjajWrdToytPOjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos, denunció a través de Contagio Radio, los **desalojos forzados que viene realizando Empresas Públicas de Medellín, EPM,** debido a la realización del proyecto hidroeléctrico Hidroituango en Antioquia.

Hasta el momento la tensión en la playa la Arenera Cañón del Río Cauca,  en Toledo, se ha agravado por los **hostigamientos y amenazas de la vigilancia privada de la obra de EPM hacia miembros del Movimiento Ríos Vivos.**

Ya van más de **400 familias desplazadas forzadamente de las playas del Río Cauca,** razón por la cual, el movimiento inició una campaña para que en Colombia no se realizasen más desalojos forzosos por proyectos mineros e hidroeléctricos.

Se trata de la campaña **“Ni un desalojo más por represas en Colombia”, por medio de esta iniciativa, se enviará una carta al Presidente Juan Manuel Santos** con firmas recogidas a través de internet, con el objetivo de exigirle al  mandatario que se cumplan los protocolos internacionales para los  desalojos, se dialogue y se respeten los derechos humanos.

“Vamos a hacer énfasis en la revictimización que generan los mega proyectos en el país, los desalojos son una forma de revictimizar a la población ya que la mayoría de personas son desplazadas por el conflicto armado”, señala Isabel Cristina.

Al movimiento le preocupa que mientras sigan incrementando este tipo de obras del gobierno, de la misma manera aumentarán las poblaciones afectadas.
