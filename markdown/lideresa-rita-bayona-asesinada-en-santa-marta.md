Title: Lideresa Rita Bayona asesinada en Santa Marta
Date: 2020-08-27 20:10
Author: AdminContagio
Category: Actualidad, DDHH, Nacional
Tags: lideresa social, Santa Marta
Slug: lideresa-rita-bayona-asesinada-en-santa-marta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/lideresa-asesinada-Rita-Bayona.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Canal Tropical

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sobre las 7 de la noche de este martes, fue asesinada la lideresa social de 45 años y **vicepresidenta de la Junta de Acción Comunal del barrio Once de Noviembre, en Santa Marta, Rita Rubiela Bayona Alfonso.** 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según información recopilada, la lideresa se encontraba en el parqueadero donde trabajaba, en el barrio Monterrey, cuando dos hombres armados en una moto la abordaron y le dispararon repetidas veces. (Le puede interesar: [Edis Care, líder comunitario del Chocó es asesinado](https://archivo.contagioradio.com/edis-care-lider-comunitario-del-choco-es-asesinado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La alcaldesa de Santa Marta, Virna Lizi Johnson Salcedo, anunció que las razones no han sido oficialmente determinadas, pero el caso podría estar relacionado con un problema de extinción de dominio.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/VirnaJohnson/status/1298471573830107139","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/VirnaJohnson/status/1298471573830107139

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según la alcaldesa, se trataba del predio en que la lideresa trabajaba y se encontraba en extinción de dominio desde 2012. En este se presentaron conflictos y disputas con alrededor de unas 40 personas, quienes repetidas veces intentaron tomar posesión del mismo. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, de acuerdo con Óscar Olarte, comandante de la Policía Metropolitana de Santa Marta, **Bayona ya había denunciado ante la Fiscalía y la Unidad Nacional de Protección (UNP) que estaba recibiendo amenazas por una persona que reclamaba el predio .**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el momento, hay una recompensa de \$5 millones de pesos para quien entregue información sobre los autores materiales del homicidio. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según, [el Instituto de Estudios para el Desarrollo y la Paz (Indepaz)](http://www.indepaz.org.co/1-000-lideres-y-defensores-de-ddhh/), desde la firma del acuerdo de paz hasta agosto 21 de este año 1000 líderes y lideresas sociales y defensores de derechos humanos han sido asesinados. (También le puede interesar: [Tres masacres en una semana, seis jóvenes fueron asesinados en Tumaco, Nariño](https://archivo.contagioradio.com/tres-masacres-en-una-semana-seis-jovenes-fueron-asesinados-en-tumaco-narino/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
