Title: El abandono estructural a las comunidades indígenas en Putumayo
Date: 2020-04-28 18:51
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Ayudas, Covid-19, indígenas, Putumayo
Slug: el-abandono-estructural-a-las-comunidades-indigenas-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Pueblos-indígenas-en-Putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @OPIAC\_Amazonia {#foto-opiac_amazonia .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

En un departamento como Putumayo en el que la violencia se ha acrecentado desde este principio de año, las comunidades indígenas denuncian que deben enfrentar sin el adecuado apoyo estatal la pandemia del Covid-19 autogestionando sus propios elementos de seguridad, y tratando de mitigar el virus con un aislamiento sin garantías.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luis Ulcue, exgobernador indígena del Pueblo Nasa en Putumayo afirma que las comunidades están viviendo las restricciones por la pandemia en un territorio en el que hay amenazas, homicidios y desplazamientos. Pero que dado el contexto, los obliga a adoptar medidas de autoprotección. (Le puede interesar:["Casi 6.000 familias indígenas en riesgo por COVID en sus territorios"](https://archivo.contagioradio.com/casi-6-000-familias-indigenas-en-riesgo-de-crisis-humanitaria/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, reclama que "se sigue viendo el abandono estatal a las comunidades indígenas en la región, desde lo local, lo regional y lo nacional", porque aunque se señala que hay canales de comunicación abiertos con el Gobierno para solicitar atención y ayudas, los mismos no funcionan.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, las comunidades han tenido que asumir el costo de los elementos de bioseguridad por sus propios medios, y ha sido gracias a organizaciones defensoras de DD.HH. como la [Comisión Intereclesial de Justicia y Paz](https://www.justiciaypazcolombia.com/joven-asesinado-en-puerto-asis/) que han podido poner de manifiesto este tipo de situaciones. (Le puede interesar: ["Se agudizan amenazas contra defensores de DD.HH. en Putumayo durante cuarentena"](https://archivo.contagioradio.com/se-agudizan-amenazas-contra-defensores-de-dd-hh-en-putumayo-durante-cuarentena/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Así ha continuado la vida en Putumayo para los pueblos indígenas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Respecto a la coordinación con la Comisión, Ulcue manifiesta que las comunicaciones se han hecho por canales digitales y telefónicos, dejando de lado las reuniones personales. Sin embargo, señala que cambiar el canal de comunicación no ha significado dejar de estar en contacto para atender todos los temas que manejan.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por otra parte, respecto a la atención estatal, Ulcue advierte que la misma solo llega a los cascos urbanos, es decir, mientras los 160 mil pesos prometidos para personas de escasos recursos o los giros adicionales de Familias en Acción llegan a las cabeceras municipales, no logran llegar a las veredas que tampoco reciben apoyo alimentario.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a su denuncia, las autoridades municipales y departamentales han señalado que las ayudas están por llegar. Situación que ocurre mientras algunos elementos comienzan a escasear en las casas. Respecto al tema de la salud, Ulcue sostuvo que el sistema de ambulancias para urgencia ha funcionado pero no se ha realizado un seguimiento a los casos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, afirma que este año tampoco se han desarrollado brigadas médicas, hecho que es relevante porque en medio de la Pandemia se requeriría haber identificado las enfermedades preexistentes y los posibles riesgos en la población para mitigarlos. (Le puede interesar: ["Erradicación forzada provoca múltiples violaciones de DD.HH. en Teteyé, Putumayo"](https://archivo.contagioradio.com/erradicacion-forzada-violaciones-ddhh-teteye/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
