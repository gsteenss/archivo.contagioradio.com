Title: Comunidad internacional está cargando, en solitario, la implementación del acuerdo de paz
Date: 2018-10-29 14:01
Author: AdminContagio
Category: Nacional
Tags: acuerdos de paz, fondo multidonante, ONU
Slug: comunidad-internacional-acuerdo-paz
Status: published

###### Foto:  Contagio Radio 

##### 29 oct 2018 

El asesor jurídico de la mesa de conversaciones en el acuerdo con las FARC, **Enrique Santiago**, aseguró que para la comunidad internacional hay dos claridades en torno a las fallas en el proceso de implementación. Por un lado que **no hay un compromiso por parte del gobierno actual en el cumplimiento del acuerdo, y por otro que la Fiscalía colombiana es uno de los principales obstáculos**.

El jurista señaló que **este gobierno no está asumiendo el acuerdo como un compromiso de Estado** e indicó que **de no ser por la comunidad internacional y su apoyo irrestricto, no se podría hablar en este momento de ningún avance, **resaltando el papel financiador del Fondo Multidonante de la ONU que **está cargando “en solitario” el peso de la financiación del acuerdo** alcanzado entre las FARC y el gobierno de Juan Manual Santos.

**El papel de la Fiscalía**

El abogado aseguró que para la comunidad internacional es claro que **uno de los principales obstáculos en la implementación del acuerdo y la puesta en marcha de la Jurisdicción Especial de Paz es la Fiscalía**, que se ha inmiscuido en asuntos de competencia estricta de la JEP.

Una de las intervenciones más sonadas, fue **el allanamiento e incautación de material probatorio en el caso 001 relacionado con las personas secuestradas por las FARC**, hecho que desató un "choque de trenes" resuelta por la Corte Constitucional a favor de la JEP. Otro de los ejemplos es que **la JEP ha tenido que frenar su investigación en torno a lo bienes entregados por la exguerrilla** dado que el listado verificado de las posesiones no ha sido entregado por la Fiscalía.

También está la situación de **Jesus Santrich**, solicitado en extradición por EEUU, de quien la Fiscalía aseguró tener pruebas irrefutables de su responsabilidad en el tráfico de drogas. Para resolverlo **la JEP ha solicitado en repetidas ocasiones que se allegue a su despacho el material probatorio** sin que, hasta el momento, se aporten evidencias contundentes de la responsabilidad del líder político en el ilícito.

En ese sentido el jurista señala que, aunque la comunidad internacional es clara en la importancia de seguir aportando en la implementación, también **se necesita encontrar gestos concretos por parte del gobierno actual para recuperar la confianza perdida** en el proceso por parte de varios integrantes de las FARC que se han visto en la obligación de aislarse del proceso. (Le puede interesar: [Implementación del Acuerdo de paz no se esta asumiendo como un compromiso de Estado](https://archivo.contagioradio.com/implementacion-del-acuerdo-de-paz-no-se-esta-asumiendo-como-un-compromiso-de-estado/))

<iframe id="audio_29681969" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29681969_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
