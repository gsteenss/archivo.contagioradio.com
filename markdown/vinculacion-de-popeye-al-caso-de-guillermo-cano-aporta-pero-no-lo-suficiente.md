Title: Vinculación de Popeye al caso de Guillermo Cano aporta, pero no lo suficiente
Date: 2019-05-28 14:44
Author: CtgAdm
Category: Judicial, Memoria
Tags: Alias 'Popeye', Guillermo Cano, Narcotráfico en Colombia
Slug: vinculacion-de-popeye-al-caso-de-guillermo-cano-aporta-pero-no-lo-suficiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/CANO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

A raíz de nueva evidencia y testimonios, el pasado 27 de mayo se dictó orden de encarcelamiento contra Jhon Jairo Velásquez, alias Popeye y Gustavo Adolfo Gutiérrez, alias Maxwell por, al parecer tener conocimiento e incidir en la planeación del asesinato del periodista, **Guillermo Cano** el 17 de diciembre de 1986 y que casi 32 años después,  continúa  en la impunidad.

María Jimena Duzán, periodista que acompañó durante varios años al entonces director de El Espectador, señaló que la captura de 'Popeye' fue tardía, **"no es suficiente para mí, conozco y sé de nombres precisos, que han tenido que ver con la investigación, la muerte y el pago de los sicarios que acabaron con la vida de Guillermo Cano y no les ha pasado nada, siguen tranquilos viviendo en sus fincas en Antioquia**", afirmó.

<iframe src="https://co.ivoox.com/es/player_ek_36424038_2_1.html?data=lJuhlJmUd5mhhpywj5WXaZS1lpaah5yncZOhhpywj5WRaZi3jpWah5ynca7V04qwlYqliMKfq87ax9PFb6Xp24qwlYqldc-Zk6iY1dTGtsaf187bxdrQpcTdhqigh6eXsozYxpC90dXJvcafwpDQw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La columnista se mostró escéptica ante los avances que podrían darse en el caso tras la captura de los dos acusados. Para ella, las posibilidades de obtener o esclarecer quienes fueron los autores son cada vez más difíciles, "o mueren de muerte natural o  han muerto por la propia dinámica de la violencia en Colombia", haciendo alusión a Pablo Escobar, quien dio la orden de cometer el crimen.

"Es muy triste y lamentable la situación de la justicia en Colombia" apuntó Duzán, y enfatizó que esta captura se produjo 33 años después del magnicidio,"**se acuerdan que 'Popeye' pudo haber tenido algo que ver con Cano, es importante, pero no suficiente", concluyó.**

### El engranaje que se teje detrás del asesinato de Guillermo Cano 

<iframe src="https://co.ivoox.com/es/player_ek_36424110_2_1.html?data=lJuhlJmVdZGhhpywj5aUaZS1lpyah5yncZOhhpywj5WRaZi3jpWah5yncavpzc6SpZiJhZLijLLO1NmJh5SZoqnbx9-JdqSf2pDZw9iPusbmxcbRx9iPtdbZjMvOztnFsozZz5DQw9jTb6iijoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Julián Martínez, periodista** que en 2016 confrontó al senador Álvaro Uribe sobre sus conexiones con la empresa Comfirmesa , donde el Cartel de Medellín lavaba su dinero para Pablo Escobar y a su vez donde Luis Carlos Molina, testaferro del Cartel, sacó los fondos para cometer el asesinato del director de El Espectador en 1986, también se manifestó al respecto.  [(Lea también: "Álvaro Uribe siempre va a estigmatizar a la persona que revele cosas que él quiere ocultar")](https://archivo.contagioradio.com/alvaro-uribe-estigmatiza-a-periodismo-de-noticias-uno/)

"**Esto no es solo Pablo Escobar, Popeye cuenta que la familia Ochoa fue determinante en el asesinato de Cano** y precisamente los Ochoa, testaferros del Cartel de Medellín, eran las conexiones que tenían Álvaro, Santiago y Alberto Uribe, estos móviles nunca se han investigado, pero la justicia siempre ha tenido los expedientes", aclaró Martínez, que agregó que si el senador no quiere responder también es legítimo, sin embargo "le debe una explicación al país".

> Hoy capturaron a Popeye. La familia de Guillermo Cano ha pedido a la Fiscalía interrogar a Uribe sobre los negocios que tuvo con Luis Carlos Molina. Desde las cuentas de Molina y Uribe en Confirmesa salió el dinero para matar al periodista en 1986 [pic.twitter.com/OlE1gasTpJ](https://t.co/OlE1gasTpJ)
>
> — Julián F. Martínez (@JulianFMartinez) [28 de mayo de 2019](https://twitter.com/JulianFMartinez/status/1133163436354019328?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
El periodista señaló que se van a cumplir 33 años de este crimen y "la justicia está dejando morir a los testigos vivos que podrían determinar hechos que se desconocen, uno de estos testigos es  'Popeye' y el otro es Álvaro Uribe".

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
