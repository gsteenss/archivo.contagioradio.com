Title: La salud ¿el primer incumplimiento del gobierno a los maestros?
Date: 2017-07-31 18:57
Category: Educación, Nacional
Tags: ade, Educadores, fecode
Slug: la-salud-el-primer-incumplimiento-del-gobierno-a-los-maestros
Status: published

###### [Foto: Archivo Particular] 

###### [31 Jul 2017]

Maestros denuncian incumplimientos por parte del gobierno en los acuerdos pactados tras el último paro, de acuerdo con William Agudelo, presidente de la Asociación Distrital de Educadores, **el gobierno no habría cumplido con el punto de salud, sometiendo a más de 320 mil docentes a un mal servicio y poco eficiente.**

Agudelo manifiesta que en este punto **el pacto que se había realizado era que los decentes serían atendidos por un prestador del servicio de salud**, que hubiese hecho una propuesta por licitación y luego de que se realizarán los estudios correspondientes se escogería. (Le puede interesar:["La plata del Estado no se puede invertir solamente en carreteras"](https://archivo.contagioradio.com/la-plata-del-estado-no-se-puede-invertir-solamente-en-carreteras-profesores/))

Sin embargo, la queja de los docentes es que este mecanismo habría cambiado y el gobierno e**staría intentado realizar una contratación directa sin que se evalúe si el prestador de servicio es apto o no para bridar salud**.

“Con el gobierno ya se habían firmado dos acuerdos, uno en el 2015 y otro en el paro pasado que tiene que ver con el tema de la salud de los educadores y de la contratación de los servicios de salud, y **el gobierno Nacional viene incumpliendo en la medida de que están imponiendo la forma de contratación de manera directa, es decir una contratación a dedo”** afirmó Agudelo.

Debido a esta situación la Federación de Docentes expidió un comunicado en el que alertan a los docentes sobre el incumplimiento, el próximo miércoles se llevará a cabo una reunión entre las directivas de los sindicatos para coordinar y decidir si podría retornar a un paro. (Le puede interesar:["Padres, madres y estudiantes respaldan el paro de docentes"](https://archivo.contagioradio.com/con-pedagogia-maestros-incluyen-a-padres-de-familia-y-estudiantes-en-las-movilizaciones/))

<iframe id="audio_20104595" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20104595_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
