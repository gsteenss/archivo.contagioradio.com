Title: Cambio Radical y Centro Democrático se unen "para hacer trizas los acuerdos" FARC
Date: 2017-10-17 13:38
Category: Entrevistas, Paz
Tags: acuerdos de paz, FARC, Vargas Lleras
Slug: cambio-radical-y-centro-democratico-se-unen-para-hacer-trizas-los-acuerdos-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/CONGRESO-e1483381822139.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón] 

###### [17 Oct 2017] 

**"Lo que hace Vargas Lleras es una campaña para hacer trizas los acuerdos"**

Andrés París, integrante del Secretariado de las FARC, desmintió todas las afirmaciones  hechas por Vargas Lleras sobre la creación de grupos de retaguardia de las FARC, **los supuestos pagos por "colados en las listas" y otras aseveraciones que expresan que hay una campaña constante contra los acuerdos y contra el partido de las FARC**.

Según el vocero, esas afirmaciones evidencian que esta campaña **reunirá a los “sectores más retardatarios”** del país, que mediante muchas acciones en diferentes instancias están intentando desmontar los acuerdos de La Habana, como lo han sido los frenos en los debates a la JEP y los incumplimientos por parte del gobierno.

De igual forma señaló que con Vargas Lleras se **evidencia un avance de un bloque de derecha electoral que trabaja por el retorno del uribismo a la presidencia**. Razón por la cual, manifestó que el resto de sectores, que han afirmado su apoyo no solo a los acuerdos de paz, sino a la construcción de la misma, deben desarrollar una estrategia pronta que frene este avance.

“Creemos que es el momento del lanzamiento de un candidato único, de una convergencia, que permita que en el centro este la defensa de los acuerdos de La Habana, como un elemento de reconciliación” señaló París. (Le puede interesar: ["Solo se ha cumplido el 18% de los acuerdos de La Habana"](https://archivo.contagioradio.com/solo-se-ha-cumplido-el-18-de-los-acuerdos-de-la-habana/))

### **“No hay retaguardias de las FARC” Andrés París** 

Sobre los señalamientos que hace Vargas Lleras de la conformación de retaguardias de las FARC, que estarían operando en puntos de control del narcotráfico, París expresó que estas declaraciones pusieron de manifiesto el caballo de batalla de la campaña de Lleras que será la guerra contra el narcotráfico, sin importar los pactado en La Habana y los programas de sustitución de cultivos, **“Vargas Lleras se anuncia como un hombre que va a pregonar la violencia contra los cultivadores**, que la guerra llevo a los campos colombianos”.

Frente a Gentil Duarte, ex integrante de las FARC-EP, que ahora hace parte de las disidencias y que según Lleras sería quien está conformando las retaguardias, París señaló que son especulaciones que buscan **“torpedear el proceso de paz e incriminar a los dirigentes del nuevo partido”**.

### **El Fiscal general desconoce el Acuerdo de Paz** 

La semana pasada se desarrolló una reunión entre integrantes de las FARC y el Fiscal General, Néstor Humberto Martínez, en la que se precisaron los alcances de la JEP, y en la que se denunció los reiterados intentos por parte del Fiscal para desplazar los **instrumentos de la JEP por la Justicia Ordinaría, que desconocen de igual forma el Acuerdo de Paz**. (Le puede interesar: ["Intromisión del Fiscal ha sido funesta para la JEP: Imelda Daza"](https://archivo.contagioradio.com/47601/))

A esta situación se suman los más de 1.400 prisioneros políticos que continúan en las cárceles del país a la espera de que se realice su proceso de amnistía y que de acuerdo con París, se convierte en uno más de los incumplimientos por parte del gobierno “que hacen trizas los acuerdos en la práctica”. Razón por la cual se realizará un plantón, a las 5:00pm en la Plaza de Bolívar, en Bogotá para exigir el cumplimiento y la defensa de los Acuerdos de Paz.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
