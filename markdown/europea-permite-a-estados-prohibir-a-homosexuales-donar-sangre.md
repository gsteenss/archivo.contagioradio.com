Title: Estados de la UE podrían prohibir a homosexuales donar sangre
Date: 2015-04-29 16:07
Author: CtgAdm
Category: closet, El mundo
Tags: Europa podrá prohibir a homosexuales donar sangre, Justicia Europea Homosexuales no puedan donar sangre, Justicia europea permite que estados prohiban a homosexuales donar sangre
Slug: europea-permite-a-estados-prohibir-a-homosexuales-donar-sangre
Status: published

###### Foto:Estoybailando.com 

Este miércoles 29 de Abril el Tribunal de Justicia de Europa ha avalado que los **estados miembros puedan prohibir donar sangre permanentemente a hombres que hayan tenido relaciones con otros hombres**, por el supuesto alto riesgo de contraer enfermedades infecciosas.

El tribunal permite a la **justicia de cada país** determinar la proporcionalidad del riesgo y si existe un **informe científico** que avale dicha medida.

Todo comenzó en **Francia**, cuando un **medico prohibió** a Geoffrey Léger, realizar la **donación de sangre por ser homosexual** basándose en una ley ministerial francesa. Fue entonces cuando Geoffrey recurrió la decisión llegando hasta la más alta instancia, es decir el Tribunal Europeo de Justicia.

La decisión vincula a todos los tribunales europeos pero permite la independencia de cada justicia ha dictaminar la proporcionalidad de la misma.

Según el pronunciamiento de la Justicia Europea "...Una contraindicación permanente para la donación de sangre aplicable a la totalidad del grupo constituido por los hombres que han tenido relaciones sexuales con otros hombres solo resulta proporcionada si no existen métodos menos coercitivos para garantizar un alto nivel de protección de la salud de los receptores...".

Aún ha si el tribunal también ha dicho que **“...puede entrañar una discriminación por razón de orientación sexual...”**. A pesar de ello el gobierno francés mantiene la moratoria por el riesgo del contagio de VIH.

En Francia entre el 2003 y el 2008, el 90% de los **contagios por VIH** tienen que ver con relaciones sexuales, de las cuales solo el **48% pertenecen a homosexuales**, el restante provienen de personas que mantienen relaciones heterosexuales.

Actualmente más de **100 países prohíben dar sangre a homosexuales**, entre ellos Colombia.

Organizaciones sociales de defensa de los derechos de los homosexuales han tildado la **medida de discriminatoria, acusando a la justicia de la UE de avalarla**, además han criticado la medida ya que la sangre donada es analizada minuciosamente para impedir cualquier contagio de enfermedades infecciosas para el receptor.
