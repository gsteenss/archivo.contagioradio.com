Title: Señor presidente electo, usted dijo que iba a respetar los derechos de las víctimas: CONPAZ
Date: 2018-06-28 10:10
Category: Nacional, Paz
Tags: acuerdos de paz, Centro Democrático, Conpaz, Iván Duque, JEP
Slug: respeten-los-derechos-de-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/conpaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 Jun 2018] 

En un comunicado, la red Comunidades Construyendo Paz en los Territorios (CONPAZ), pidió al saliente presidente Juan Manuel Santos como al recién electo, Iván Duque **que no se modifiquen los acuerdos de ninguna forma y que se pongan en marcha los mecanismos creados por el Acuerdo de Paz**, que garantizan un Sistema Integral de Verdad, Justicia y Reparación. (Le puede interesar: ["Cambio Radical y Centro Democrático se unen para 'Hacer trizas los acuerdos': FARC"](https://archivo.contagioradio.com/cambio-radical-y-centro-democratico-se-unen-para-hacer-trizas-los-acuerdos-farc/))

Adicionalmente, se pronunciaron ante la posibilidad de que se modifiquen los acuerdos mediante un referendo: **"La razón de mayorías urbanas votantes, que no han vivido la violencia, no es la razón verdadera"**. Por lo tanto esperan que se respete el acuerdo, y por sobre todo, que se respeten los derechos de las víctimas.

[Comunicado de CONPAZ](https://www.scribd.com/document/382795036/Comunicado-de-CONPAZ#from_embed "View Comunicado de CONPAZ on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_87099" class="scribd_iframe_embed" title="Comunicado de CONPAZ" src="https://www.scribd.com/embeds/382795036/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-gBMRY6tAo0DAEUTe7SNq&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
 

###### [Reciba toda la información de Contagio Radio en [[su correo] 
