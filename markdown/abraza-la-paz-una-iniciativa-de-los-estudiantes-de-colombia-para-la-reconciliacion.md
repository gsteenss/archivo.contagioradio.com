Title: Abraza la paz, una iniciativa de los estudiantes de Colombia para la reconciliación
Date: 2017-03-29 16:44
Category: Nacional, Paz
Tags: aceu, estudiantes, Zonas Veredales Transitorias de Normalización
Slug: abraza-la-paz-una-iniciativa-de-los-estudiantes-de-colombia-para-la-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/vigilia-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Mar 2017] 

Con la finalidad de ayudar en la construcción de paz y de gestar reconciliación en los territorios, **la Asociación Colombiana de Estudiantes Universitarios, ACEU, lanza su iniciativa “Abraza a la paz**” una apuesta que visitará tanto las zonas veredales, como territorios que han vivido el conflicto armado, con brigadas conformadas por estudiantes de universidades públicas y privadas.

Esta iniciativa funcionará a través de la conformación de 8 grupos de trabajo: **la misión médica, la misión jurídica, la misión cultural, artística y deportiva, la misión internacional de acompañamiento a la implementación de los Acuerdos de Paz**, la misión de alfabetización, la misión de visitas a las zonas veredales y la misión de actos de reconciliación.

Para hacer parte de estos equipos de trabajo los requisitos son: s**er mayor de 18 años, ser estudiante activo de alguna universidad**, estar afiliado a una EPS y tener disponibilidad de tiempo para viajar a diferentes regiones del país. Le puede interesar: ["Familia de líder guerrillero de los años 50 ofrece sus tierras en la construcción de paz"](https://archivo.contagioradio.com/familia-aljure-dona-tierras-para-proyectos-productivos-de-las-farc/)

En este servicio estudiantil, también podrán inscribirse personas que estén fuera del país, esto con el interés de que **organizaciones estudiantiles de otros países también se vinculen en la construcción de paz de Colombia**. Le puede interesar: ["La paz en el Chocó: Entre sombras y luces"](https://archivo.contagioradio.com/pazchocoestadoguerrilaparamilitares/)

De acuerdo con Yuliana Montez, integrante de la ACEU “esta iniciativa busca colocar los conocimientos adquiridos en el proceso de formación universitaria, **al servicio de las necesidades de los territorios y así fortalecer el vínculo universidad-sociedad**”.

[Encuentre aquí en link de inscripción](https://docs.google.com/a/contagioradio.com/forms/d/16GEJSXiyT-GzFgzxUzutLcHbtdXmObJfh86rkWTod90/viewform?edit_requested=true#responses)

<iframe id="audio_17849994" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17849994_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
