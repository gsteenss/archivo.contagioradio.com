Title: Con el fin de Noticias Uno se da un golpe al periodismo libre e independiente
Date: 2019-09-02 18:44
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Canal Uno, Censura, Libertad de Prensa, Medios Corporativos, Noticias Uno, periodismo
Slug: con-el-fin-de-noticias-uno-se-da-un-golpe-al-periodismo-libre-e-independiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Noticias-Uno.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Youtube/ Noticias Uno] 

[Con el anuncio del cierre de Noticias Uno hecho por el gerente de la productora NTC, Jorge Acosta, se abre el debate sobre lo que sería un caso de censura contra uno de los noticieros insignia de la investigación en Colombia. Para **Julián Martínez**, quien fue integrante del equipo periodístico del medio por cerca de siete años se trata de un golpe al periodismo independiente.]

[El periodista descarta que se trata de un debate por el rating e indica que aunque la salida del aire de Noticias Uno se esté presentando como una decisión empresarial en el marco de una crisis mediática, **se trata de una decisión política** relacionada a los contenidos y denuncias que ha realizado el noticiero en su trayectoria.  
]

[Martínez afirma que El Estado ha ejercido una persecución contra uno de los medios que a principios de los 2000, fue víctima de las chuzadas del DAS con el objetivo de identificar las fuentes de los periodistas, **"quienes ven a Noticias Uno como un enemigo están en el Gobierno"** sentencia el periodista, quien destaca que cuando un medio es cerrado, también se envía un mensaje al resto. ]

### **"Sin Noticias Uno, perdemos una voz absolutamente necesaria"** 

Pese al anuncio dado por Jorge Acosta en el que indica que la fecha de salida del aire sería en el 2020 , Martínez señala que Noticias Uno podría desaparecer [el 1 de octubre, un hecho que considera extraño por ser una época en que vienen importantes sucesos para el país, como es el caso de las elecciones locales, la indagatoria del **expresidente Álvaro Uribe Véle**z ante la Corte Suprema de Justicia y nuevos avances en el caso de Odebrecht.]

"Esto indiscutiblemente va más allá de todo lo que conocemos, detrás se oculta un gigante como lo es Odebrecht y tras el Luis Carlos Sarmiento Angulo". ([Lea también: La importancia de la ética en medios públicos, una lección de Javier Darío Restrepo)](https://archivo.contagioradio.com/la-importancia-de-la-etica-en-los-medios-publicos-una-leccion-de-javier-dario-restrepo/)

[Resalta a su vez la independencia que existía en las decisiones editoriales del medio, caracterizado por un periodismo de **"contrapoder, crítico y en el que las pasiones políticas no t**]**enían cabida"**, algo que hoy ad portas de ser cerrada es una muestra de su independencia y de que en Colombia "al poder no le conviene el pensamiento crítico". [(Le puede interesar: Sí hubo censura a Santiago Rivas y los Puros Criollos)](https://archivo.contagioradio.com/si-hubo-censura-a-santiago-rivas-puros-criollos/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_40884987" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_40884987_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
