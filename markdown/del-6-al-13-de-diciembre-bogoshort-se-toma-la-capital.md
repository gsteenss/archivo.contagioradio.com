Title: Del 6 al 13 de diciembre Bogoshort se toma la capital
Date: 2016-12-06 15:30
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: 24 c, Bogoshort, cortos
Slug: del-6-al-13-de-diciembre-bogoshort-se-toma-la-capital
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/bogoshorts-14-bogota-short-film-festival-cortos-bogota-2016-facebook.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Bogoshort] 

###### [6 Dic 2016] 

**Regresa Bogoshort a la capital del 6 al 13 de diciembre**,  uno de los más importantes festivales de cortos  de cine que en esta ocasión se prepara con sorpresas y novedades para todos los asistentes, con actividades como los chikishorts y el world tour, esta decimo cuarta versión del Festival promete ser un escenario para que tanto grandes como chicos interactúen con el mundo del séptimo arte.

**Chikishorts** será un espacio dispuesto para los más pequeños y será una sección en la que habrá una programación de tres cortometrajes especialmente  escogidos para este público. **World Tour** tiene el objetivo de **traer los mejores cortos del extranjero con muestras de Suiza, Polonia, entre otros países y presentarlos en el Festival.**

De igual forma, el festival continuará con sus clásicos espacios de aprendizaje como lo son el **Bogoshort al Tablero**, sección que de acuerdo con María Alejandra Santamaría “busca ofrecer a la gente a la gente una amplio espectro de actividades  con las que puedan nutrisr sus conocimientos no solo relacionados al cine sino de este **relacionado a la realidad y vida cotidiana”.**

Motivo por el cual seguirán programándose los “encuentros cercanos”, las actividades y conversatorios relacionados con los 200 años de Frankenstein y foros referente a la reconciliación y como el cine puede aportar a estos procesos de paz al interior de la sociedad, además se tendrá un escenario primicia sobre la experiencia del **“SENA al cine”,** una iniciativa que pretendió que estudiantes del SENA en el área del cine intercambiaron con directores colombianos.

Bogoshort lleva 14 ediciones y a lo largo de su historia se ha logrado posicionar como uno de los festivales más importantes de cortometrajes en el mundo.  En la convocatoria para el 2016 participaron aproximadamente **3.730 cortometrajes de 109 países, que tendrá una muestra final de  480 cortometrajes en 18 espacios diferentes en toda la capital.**

<iframe id="audio_14671981" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_14671981_4_1.html?c1=ff6600"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
