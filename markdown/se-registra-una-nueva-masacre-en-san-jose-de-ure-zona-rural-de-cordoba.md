Title: En San José de Uré, Córdoba, se registró una nueva masacre
Date: 2020-09-23 12:48
Author: CtgAdm
Category: Actualidad, Nacional
Tags: colombia, Córdoba, nueva masacre, san josé de Uré
Slug: se-registra-una-nueva-masacre-en-san-jose-de-ure-zona-rural-de-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/image.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En horas de la tarde del **22 de septiembre se conoció de una nueva masacre en el departamento de Córdoba**, exactamente en San José de Uré en dónde fueron asesinadas cuatro personas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/SandinoVictoria/status/1308790056216408064","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/SandinoVictoria/status/1308790056216408064

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Dos de las víctimas hasta el momento ha sido identificadas como **Eduardo Olea, campesino** de la zona y **Santos Baltazar, miembro de la guardia indígena del pueblo Zenú**, las otras víctimas aún están por identificar. ([Duque expresó «el desprecio que siente por la ciudadanía», Ángela Robledo](https://archivo.contagioradio.com/duque-expreso-el-desprecio-que-siente-por-la-ciudadania-angela-robledo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunas versiones indican que **hombres armados ingresaron a la vereda Batatal, en el corregimiento de Versalles**, ubicado a 30 kilómetros de la cabecera municipal, San José de Uré, y atacaron con arma de fuego a las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Debido a que el hecho donde se perpetró la masacre **esta alejado del corregimiento de Versalles, esto ha dificultado las labores de identificación** por parte de las autoridades, así como el abordaje de las investigaciones del caso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al igual que en otros municipios del país donde se registran masacres, la comunidad de San José de Uré ha recibido **desde el pasado mes de julio al menos 279 campesinos que se han tenido que desplazados hacia el casco urbano**, debido a los asesinatos cometidos por los[grupos armados](https://archivo.contagioradio.com/casos-homicidios-contra-defensoras-de-dd-hh-se-encuentran-en-un-91-de-impunidad/)que operan en esta zona; algunos de ellos son los Caparrós y el Clan del Golfo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dentro de la **61 masacres que se han registrado en Colombia durante el 2020, según[Indepaz](http://www.indepaz.org.co/informe-de-masacres-en-colombia-durante-el-2020/)**en su informe más reciente presentado el domingo pasado; han perdido la vida 246 personas en el marco de estos hechos violentos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El más reciente registrado en Córdoba fue el pasado 11 de septiembre en el que fue atacado con arma de fuego en las calles de Montelievano el presidente de la **Junta de Acción Comunal del Barrio Villa Clemen, y miembro de Cordupaz, Cristóbal José Ramos.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
