Title: ¿Qué significa la concesión por 20 años más a la carbonera Drummond?
Date: 2019-02-13 12:00
Author: AdminContagio
Category: Voces de la Tierra
Tags: carbón minería, cesar, Drummond
Slug: 20-anos-concesion-drummond-carbon-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Mineria-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Pilón 

###### 7 Feb 2019 

La extensión de 20 años en la concesión a la empresa **Drummond** en el departamento de Cesar por parte del gobierno colombiano, representa **una serie de consecuencias en materia ambiental,** decisión tomada bajo el argumento que en contraprestación se realizarán inversiones sociales, generación de empleo e incremento en las regalías.

En Voces de la tierra, nos acompañaron  **Isabel Cavelier Adarve**, directora de Transforma y **Luis Álvaro Pardo**, economista y especialista en derecho minero, para analizar si las compensaciones económicas son equitativas con las ganancias que recibirá la compañía y si son suficientes para reparar las afectaciones al ambiente y la salud de las comunidades.

<iframe id="audio_32393799" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32393799_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información ambiental en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
