Title: Jorge Eliecer Gaitán, su vida y luchas en la historia de Colombia
Date: 2018-04-09 05:29
Category: Otra Mirada, Política
Tags: Jorge Eliécer Gaitán, memoria
Slug: jorge-eliecer-gaitan-68-anos-de-una-vida-en-la-historia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/jorge-eliecer-gaitán.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: lachachara] 

Un recorrido interactivo por la vida y el legado de Jorge Eliecer Gaitán a través de sus discursos y los momentos icónicos de su vida política. Una herramienta de reconstrucción histórica y de memoria. 9 de Abril de 1948. 70 años de una vida en la historia.

<iframe src="https://s3.amazonaws.com/uploads.knightlab.com/storymapjs/1cec37ac266de75a5f942e69b571952c/gaitan/draft.html" width="100%" height="800" frameborder="0"></iframe>
