Title: "No pueden seguir paralizadas las declaraciones de Zonas de Reserva Campesina"
Date: 2015-02-20 19:44
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Seminario Internacional de Zonas de Reserva Campesina, zonas de reserva campesina
Slug: no-pueden-seguir-paralizadas-las-declaraciones-de-zonas-de-reserva-campesina-2
Status: published

##### Foto: Prensa Rural 

<iframe src="http://www.ivoox.com/player_ek_4112572_2_1.html?data=lZaelJqbdo6ZmKiakpyJd6KolZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8bhytPO1M7Tb6ri1crf0MbHrdDiwtGYxsqPntDiwtiYxsqPlsbnxtfjw5Cnpc7kxtjW0MaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Martha Gladis, ANZORC] 

Este 20 y 21 de febrero, se lleva a cabo el Seminario Internacional de Zonas de Reserva Campesina, que tiene como objetivo generar un debate en torno a **la defensa de los derechos del campesinado, la soberanía alimentaria y a la protección y defensa de los derechos del medio ambiente.**

El evento, "recuerda las luchas de los campesinos y las campesinas en Colombia, quienes han demandado  al Estado  para que se reconozca su lugar como sujetos políticos y tengan los mismos  derechos que tiene el común de la ciudadanía", asegura Martha Gladis, una de las organizadoras del seminario.

Por medio de la constitución de  las Zonas de Reserva Campesina, se exige al gobierno nacional **el reconocimiento del sujeto político del campo más allá del trabajador agrario**, teniendo en cuenta que en el Plan Nacional de Desarrollo 2014-2018, no se nombra en ningún momento la palabra campesino.

La congresista Ángela María Robledo, el sociólogo Alfredo Molano, el congresista Alirio Uribe, representantes de movimientos populares de Centro y Sur América, así como campesinos y campesinas de regiones, académicos y el sector rural, harán parte de la primera jornada del Seminario Internacional de Zonas de Reserva Campesina.

Son más de 200 campesinos y campesinas los que han llegado a Bogotá, para participar en el seminario que se realiza en **Centro de memoria paz y reconciliación,** donde se tocarán temas puntuales como:

-   Territorialidades campesinas y movimientos sociales campesinos, espacios de vida en la producción y reproducción de los sectores rurales y urbanos
-   Soberanía alimentaria, seguridad y derecho a la alimentación.
-   Derechos campesinos: apuestas y desafíos del movimiento campesino en Colombia.
-   Economía campesina: Retos, desafíos y perspectivas para el campo y la ciudad en Colombia.
-   Género: apuestas de enfoques diferenciales de género en el sector rural colombiano y en las ciudades del país.
-   Proceso de paz: apuestas y desafíos de pobladores rurales y urbanos.

Durante el encuentro, el experto en temas rurales, Darío Fajardo, presentará el libro donde se muestran  las experiencias de constitución de las zonas de reserva campesina, trabajado desde las luchas agrarias de la Asociación Campesina del Valle del Río Cimitarra. **Los interesados en participar en el evento se pueden inscribir al correo: zrc@gmail.com.**
