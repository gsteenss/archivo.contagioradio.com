Title: Campo muerto, la resistencia a la violencia hecha danza
Date: 2017-09-07 12:45
Category: Cultura
Tags: Bogotá, danza, eventos, memoria
Slug: campo-muerto-obra-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Campo-Muerto_Danza-Común-3-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Danza Común 

###### 7 Sept 2017 

Para celebrar sus 25 años de labores artísticas, l**a Compañía Danza Común, presenta en Bogotá del 7 al 16 de septiembre  su montaje de danza contemporánea "Campo Muerto"**, una pieza que toma elementos de las **artes plásticas, el teatro y el performance**, para hablar del dolor, pero también de la fuerza de un pueblo que ha sabido resistir una historia absurdamente cruel.

La obra estrena en 2008 como respuesta a la larga historia de violencia colombiana, llega al escenario de **Factoría L’explose**, en esta oportunidad en una adaptación de la maestra Bellaluz Gutiérrez, enmarcada en el **proceso de reflexión que se viene dando en torno a la paz y la reconciliación**, sin abandonar la intención de visibilizar los efectos que la guerra trae para los campesinos del país.

Durante seis funciones programadas, los bailarines **Margarita Roa, Andrés Lagos, Rodrigo Estrada, Juliana Rodríguez, Ricardo Villota, Dalia Velandia, Daniela Trujillo y Jenny Angélica Angulo**, serán los encargados de transmitir a los espectadores la sensibilidad artística a través del movimiento de sus cuerpos interactuando en un mismo escenario. (Le puede interesar: [La Sangre Rota, un homenaje teatral a García Lorca](https://archivo.contagioradio.com/sangre-rota-homenaje-garcia-lorca/))

<iframe src="https://www.youtube.com/embed/j9Yl94Rj1bQ" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**El montaje.**

Campo Muerto **presenta un testimonio de nuestra trágica historia reciente**, resultado de una manera de percibir la guerra que ha afectado, sobre todo, a la población del campo. A través del manejo impecable y vertiginoso de objetos en escena y cuerpos amontonados que atraviesan anónimamente el peligro y el desasosiego, **la pieza refleja la fragilidad y la fortaleza de la naturaleza humana cuando se expone a la violencia**, al señalamiento, al desplazamiento, al silenciamiento forzado y al asesinato selectivo, símbolos todos estos de una tragedia colectiva inmersa en un abismo de indiferencia y olvido.

**Sobre la Compañía Danza Común.**

Surge en 1992 en la Universidad Nacional de Colombia gracias a la reunión de artistas de danza contemporánea con estudios de antropología, literatura, sociología y artes plásticas. En el año 2001 la compañía se establece en el centro de Bogotá, en donde abre su propio estudio. Allí realizan producciones propias, procesos de creación con artistas invitados, talleres y programas de formación.

Es reconocida por piezas como: Tumpacte, Transparente, La Pura Verdad, Historias Comunes Historias Bailadas, Ni Manzanas, Ni Peras, Ni Caimitos, Campo Muerto y Aquí Lo Aparente, las cuales han participado en festivales y eventos en Colombia, México, Estados Unidos, España, Francia, Brasil, República Dominicana, Chile, Alemania, Venezuela y Suiza, entre otros.

Funciones del 7 al 16 de septiembre, De jueves a sábado a las 8:00 p.m.  
Bono de apoyo: \$30.000 General \$25.00 estudiantes y tercera edad.  
Lugar: Factoría L’EXPLOSE Carrera 25 \# 50-34, Informes y reservas: 2496492
