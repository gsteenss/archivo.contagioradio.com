Title: 75 años después, Auschwitz y las nuevas caras del fascismo
Date: 2020-01-27 18:10
Author: CtgAdm
Category: El mundo, Memoria
Tags: Holocausto Nazi, Negacionismo, Segunda Guerra Mundial, Verdad y no repetición
Slug: 75-anos-despues-auschwitz-y-las-nuevas-caras-del-fascismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Pixaba.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Auschwitz.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Auschwitz Pixabay

<!-- /wp:paragraph -->

<!-- wp:audio -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/sergio-zubiria_md_47010527_wp_1.mp3&quot;" preload="auto">
</audio>
  

<figcaption>
Entrevista Sergio de Zubiría

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph {"align":"justify"} -->

Este 27 de enero se conmemoran 75 años de la liberación de los prisioneros del campo de concentración de [Auschwitz](https://twitter.com/AuschwitzMuseum) en Alemania donde habrían sido asesinadas más de 1.300.000 personas. En su homenaje, las Naciones Unidas establecieron el Día Internacional de Conmemoración en Memoria de las Víctimas del Holocausto, y que en épocas en la que algunos sectores buscan desconocer los sucesos que han marcado la historia, es necesario destacar la importancia de la no repetición y de la verdad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tras la invasión alemana de Polonia en 1939 y el ascenso nazi, se comenzó a deportar a judios desde Alemania y Austria hasta Polonia, donde crearon guetos para separarlos del resto de la población, para 1941 habían asesinado a 500.000 personas, y en 1945 habían asesinado a unos dos millones de los qu**e 1,3 millones eran judíos incluidos 75.000 polacos, 15.000 prisioneros de guerra soviéticos, 2.000 gitanos, así como homosexuales y prisioneros políticos.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Existen nuevas caras de fascismo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Para el profesor y filósofo, Sergio de Zubiría, esta fecha resulta fundamental** pues es un momento de importante reflexión para que, más de medio siglo después, un escenario de tal magnitud no se repita. Citando al sociólogo Boaventura de Sousa Santos, el profesor De Zubiría se refiere a nuevas **prácticas como el fascismo social,** que aunque no sea el mismo que caracterizó la solución final durante la Segunda Guerra Mundial es necesario traer a colación por sus manifestaciones y tipos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El académico evidencia el más claro de estos ejemplos en gobiernos derecha como el de Jair Bolsonaro en Brasil quien ha sostenido diversas declaraciones en contra de minorías, la última de estas con relación a las comunidades indígenas, afirmando que “cada vez más, el indígena es un ser humano como nosotros”. Asimismo identifica otro claro ejemplo de esta conducta en los bombardeos contra naciones como Irak o Irán por parte de Estados Unidos, "usando razones culturales y religiosas para iniciar una carrera armamentística". [(Lea también: Respondiendo las preguntas de Trump sobre movilidad humana)](https://archivo.contagioradio.com/respondiendo-preguntas-sobre-movilidad-humana/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Son afirmaciones que rayan en el racismo del siglo XXI resignificadas y que llevan a la pérdida de dignidad y al fortalecimiento de proyectos totalitarios" alerta el profesor sobre las consecuencias que podrían tener este tipo de acciones y amenazar el presente del continente y en particular de América Latina. [(Le recomendamos leer: María Tila Uribe, reflexiones sobre la masacre de las bananeras)](https://archivo.contagioradio.com/%f0%9f%8e%a5-maria-tila-uribe-reflexiones-sobre-la-masacre-de-las-bananeras/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "El imperativo moral de nuestra época es impedir y evitar que se repitan los campos de concentración de Treblinka, en ningún momento ha desaparecido la amenaza del fascismo" Sergio de Zubiría

<!-- /wp:quote -->

<!-- wp:image {"id":79877,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![Auschwitz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Pixaba.jpg){.wp-image-79877}  

<figcaption>
Foto: Pixabay

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Las claves para no olvidar Auschwitz

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para el profesor existen tres ejes esenciales para garantizar la no repetición de hechos como los ocurridos en Auschwitz: la educación de carácter reflexivo y auto crítico, la importancia de los medios de comunicación que buscan combatir la verdad hegemónica y oficial de otros medios que contribuyen a la violencia y finalmente el fortalecimiento de prácticas culturales y artísticas para rememorar que hechos como el holocausto nazi o sucesos más cercanos como la conquista y el exterminio en América, sucedieron y no pueden quedar en el olvido.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"El Holocausto no solo es un tema judío, se trata de ver a cualquier otro ser humano como un ser que no tiene dignidad ni racionalidad sino es visto casi como un ser que debe ser desechado"** explica el profesor comparando esta situación con contextos actuales como la xenofobia contra la población venezolana y los brotes de tipo fascista ante refugiados, desplazados y migrantes, "es un fenómeno que no ha desparecido de la faz de la tierra". [(Le puede interesar: A 91 años de la masacre de las bananeras la historia se repite y el negacionismo pretende imponerse)](https://archivo.contagioradio.com/a-91-anos-de-la-masacre-de-las-bananeras-la-historia-se-repite-y-el-negacionismo-pretende-imponerse/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Es por tal razón que explica que aunque algunos gobiernos, como el de Bolsonaro buscan "estrangular el interés de la sociedad por las ciencias sociales y en particular por la historia", es necesario fortalecer la educación y la formación de profesores, "la historia debe ser transversal para salir de esta crisis de la humanidad y cultivar cultivar el diálogo intergeneracional para de **este modo cultivar el "deber ético de la memoria".** [Postura de la Fiscalía sobre asesinato de líderes distorsiona la realidad de los territorios](https://archivo.contagioradio.com/fiscalia-asesinato-lideres-distorsiona-realidad-territorios/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El siglo XXI debe ser el siglo de la verdad y la justicia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El profesor de Zubiría señala que durante el siglo XX se registraron cerca de 187 millones de víctimas por acción u omisión, incluyendo sucesos históricos como los campos de concentración Auschwitz, la I y la II Guerra Mundial, ante ello enfatiza que con un nuevo siglo la justicia y la verdad deben ser temas centrales tanto en el contexto europeo como en el colombiano, "si no hay verdad, no habrá reparación, reconciliación, ni una historia en otra dirección".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Lo mas grave para una cultura es el negacionismo, es negativo que existan sectores sociales y políticos que quieren negar hechos históricos como la masacre de las bananeras, el genocidio de partidos políticos, defensores de DD.HH o periodistas" afirma el profesor relacionándolo a la realidad nacional en una época en la que se busca reducir las consecuencias de los asesinatos líderes sociales, y el genocidio de excombatientes donde se requiere, a 75 años del fin de Auschiwtz asumir la historia con franqueza y de forma directa. [(Lea también: ¡Sí existen desaparecidos! familiares de las víctimas del Palacio de Justicia)](https://archivo.contagioradio.com/existen-desaparecidos-l-palacio-de-justicia/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
