Title: 'Celeste' la plataforma que busca saldar la deuda histórica con la memoria  LGBTI en Colombia
Date: 2020-02-19 10:21
Author: CtgAdm
Category: Entrevistas, LGBTI
Tags: Asesinatos contra población LGBT, LGBT, memoria
Slug: celeste-plataforma-busca-saldar-deuda-historica-memoria-lgbti-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/plataforma-celeste.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: LGBTI

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al menos 1.140 personas LGBT han sido asesinadas en los último 10 años en Colombia, sus historias podrán ser conmemoradas a través de Celeste, una nueva plataforma online que fue lanzada para dignificar la memoria de las 1.237 personas LGBTI que tan solo entre 2007 y 2018 fueron asesinadas en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Detrás de este trabajo encuentra el trabajo de un equipo de investigación de Colombia Diversa que documentó los asesinatos de personas LGBTI en el país desde el 2005, ante la ausencia de datos oficiales producidos por el Estado. [(Lea también: Por primera vez, informes de población LGBTI llegan a una Comisión de la Verdad)](https://archivo.contagioradio.com/por-primera-vez-informes-de-poblacion-lgbti-llegan-a-una-comision-de-la-verdad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Gustavo Pérez, coordinador de DD.HH. de [Colombia Diversa](https://twitter.com/ColombiaDiversa)** señala que hay una obligación del Estado en generar información estadística y en promover la memoria histórica sobre lo que ha ocurrido contra la población LGBTI tanto en el conflicto como fuera de él, **"parte de la deuda le compite también a la sociedad, no se trata solo del Estado que ignora esta violencia, a veces incluso la justifica apelando a prejuicios".**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La responsabilidad de lo prejuicios en los crímenes contra la comunidad LGBTI

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Dicha recopilación se materializó en un informe anual de derechos humanos que relató cuántas personas de esta población fueron asesinadas año tras año, sin embargo fue evidente la existencia de una deuda histórica en la que se ha pretendido borrar a las personas LGBT, sus vidas y sus relatos, así como justificar sus muertes, **como en el 32% de los casos a través de prejuicios y estereotipos hacia la orientación sexual o la identidad de género de las víctimas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La información recopilada en Celeste permite dar cuenta de una serie de patrones contra la población que se ha repetido a lo largo del tiempo, incluidos los asesinatos de **hombres gay en sus viviendas, el asesinato de mujeres trans en zonas laborales y los homicidios de mujeres lesbianas en pareja en espacios públicos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El coordinador afirma que es necesario saber cuál ha sido el papel de los prejuicios en la ocurrencia y la repetición crónica de la violencia contra otros tipos de orientación sexual a lo largo del país, **"solo al poder dimensionarlo podemos empezar a pensar en cómo resolver este tipo de violencia y discriminación".**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Pasamos de las cifras a mostrar el rostro humano detrás de estos crímenes"  

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Las historias compiladas en Celeste se encuentran compiladas en **“un universo digital”** divididas en tres niveles: el primero recopila 20 perfiles detallados de personas LGBT, elaborados a partir de una rigurosa investigación que incluye audios de sus familiares y en ocasiones de las mismas víctimas. El segundo nivel recolecta 50 historias, construidas a partir de la información que proviene de la base de datos de Colombia Diversa,mientras el tercer nivel contiene un texto en homenaje a 1,064 personas LGBT asesinadas de quienes no existe la suficiente información.  [(Le puede interesar: Asesinato de Ariel López, es el octavo contra la comunidad LGBTI de Barranquilla entre 2018 y 2019)](https://archivo.contagioradio.com/asesinato-de-ariel-lopez-es-el-octavo-contra-la-comunidad-lgbti-de-barranquilla-entre-2018-y-2019/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
