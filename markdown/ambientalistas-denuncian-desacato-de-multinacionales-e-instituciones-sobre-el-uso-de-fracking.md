Title: Ambientalistas denuncian desacato de multinacionales e instituciones sobre el uso de Fracking
Date: 2019-09-11 16:18
Author: CtgAdm
Category: Ambiente, Judicial
Tags: Alianza Colombia Libre de Fracking, Consejo de Estado, Fracking en Colombia
Slug: ambientalistas-denuncian-desacato-de-multinacionales-e-instituciones-sobre-el-uso-de-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/fracking_ronnie_chua_sstock_0.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Ronnie Chua] 

El pasado 9 de Septiembre la Corporación Podion junto con la Alianza Colombia Libre de Fracking radicaron, a través de una audiencia ante el Consejo de Estado, un incidente de desacato contra la empresa multinacional de exportación y procesamiento de carbón Drummond. Las organizaciones buscan comprobar una falta de cumplimiento por parte del Gobierno en el marco de la medida cautelar, emitida por el Alto Tribunal en noviembre 2018, que ordenó suspender proyectos piloto de extracción mediante estimulación hidráulica como el fracking en Colombia y de ser comprobado, que se impongan las correspondientes sanciones.

### **La situación actual** 

Dicho desacato se originó por la explotación actual de 15 pozos de gas de carbón que provienen de un yacimiento no convencional llamado Caporo Norte del bloque La Loma, ubicado en los municipios de Chiriguaná y La Jagua. Esta acción, de acuerdo con las organizaciones habría sido avalada por el Ministerios de Minas y Energía y la Agencia Nacional de Hidrocarburos, pasando por alto el marco normativo necesario para continuar con dicha labor. (Le puede interesar: [Ciudades del mundo se unieron a la marcha contra el fracking](https://archivo.contagioradio.com/ciudades-del-mundo-se-unieron-a-la-marcha-contra-el-fracking/)).

Según explicó el abogado David Uribe de la Corporación Podion, la resolución 9341 y el decreto 3004 se encuentran suspendidos a la luz del principio de precaución, por los posibles riesgos asociados a la practica de estimulación hidráulica por medio de fracturamiento de rocas generadoras (rocas donde hace 60 mi millones de años se formaron los hidrocarburos y que gracias a  técnicas alrededor del fracking se pueden sacar).

«Lo que nosotros estamos denunciando es que Drummond dijo que no estaría haciendo fracking» porque los pozos que actualmente explotan no son profundos, debido a que no tocan la roca madre, afirmó David Uribe. Sin embargo, el lugar en donde se realiza la extracción de hidrocarburos hace parte de un yacimiento cobijado por la orden del Consejo de Estado.

### **Drummond conoce la legislatura ** 

David Uribe expresó que la multinacional tiene conocimiento sobre la violación que estaría cometiendo "porque en la licencia ambiental que les consiguió el Estado para hacer esta explotación, se enmarca la resolución 9341 que establece cuáles son los criterios técnicos para hacer estimulación hidráulica en yacimientos no convencionales”. Es decir que sabrían que esa misma licencia ambiental se encuentra dentro de los proyectos que el Consejo ordenó suspender.

«Lo que queremos es que el magistrado compruebe estos hechos y que se impongan sanciones a la empresa y a los directores de las entidades competentes que no hicieron bien su trabajo» declaró el abogado.

De igual forma, el abogado manifestó que Drummond reconoce haber hecho estimulación hidráulica en un yacimiento no convencional y afirma públicamente que esta produciendo en estos pozos de gas asociados a mantos de carbón, sin embargo para ellos no representa un incumplimiento a la ley. (Le puede interesar: [Debe suspenderse todo tipo de actividad relacionada al desarrollo de fracking en el país, no solo en Santander](https://archivo.contagioradio.com/debe-suspenderse-todo-tipo-de-actividad-relacionada-al-desarrollo-de-fracking-en-el-pais-no-solo-en-santander/))

Teniendo en cuenta **la reciente decisión del Consejo de Estado sobre mantener la medida cautelar sobre la explotación de petroleo para  mantener el principio de precaución**, se espera que en las próximas semanas se continúe con la verificación de los hechos denunciados y que el Alto Tribunal tome una decisión frente a si hubo o no un desacato.

<iframe id="audio_41570046" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_41570046_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>**Síguenos en Facebook:**  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
