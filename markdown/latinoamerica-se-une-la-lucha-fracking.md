Title: Latinoamérica se une en la lucha contra el fracking
Date: 2016-12-07 14:47
Category: Ambiente, Voces de la Tierra
Tags: Argentina, fracking, petroleo
Slug: latinoamerica-se-une-la-lucha-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/fracking.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 350.org 

###### [7 Dic 2016]

Legisladores, representantes de la iglesia, voceros de la sociedad civil, ambientalistas, científicos, líderes indígenas y comunitarios de países latinoamericanos como Argentina, Brasil, Uruguay, Ecuador y Colombia hicieron un e**nérgico llamado para emprender las acciones necesarias y detener el fracking.**

En el marco de la Segunda Jornada Internacional 'Cambio Climático y Crisis Ambiental: los peligros del fracking y las Alternativas para América Latina' que se realizó el primero de diciembre en la Cámara de Diputados de Argentina, diversas figuras debatieron sobre los riesgos y peligros que conlleva la fracturación hidráulica.

**“El fracking es un verdadero atentado y una burla contra el cambio climático, el agua y la naturaleza.** La extracción, consiste en inyectar 25 millones de litros de agua en cada pozo”, dijo el senador argentino Fernando Solana, alertando sobre los riesgos que genera este tipo de práctica para extraer petróleo.

Solana además denunció que en Argentina se hacen pruebas piloto de fracking, puntualmente en Vaca Muerta. “Argentina está siendo utilizada como proyecto piloto para la explotación de gas no convencional, con graves consecuencias para el agua y el clima. Es una batalla, una guerra larga la que tenemos delante, pero es una que debemos pelear”.

Por su parte, Magdalena Odarda, senadora del Frente Amplio Progresista se refirió a las consecuencias del fracking en su región, la provincia de Río Negro, **donde los productores de peras y manzanas hoy deben competir con las multinacionales extractivas por el acceso al agua.** "Hay gente en las zonas cercanas a los yacimientos que abre la canilla en el campo y el agua le sale negra. Nadie les da explicaciones", afirmó.

En el evento se concluyó que con la llegada de un presidente de los Estados Unidos como Donald Trump, que desde siempre ha negado la crisis climática que actualmente atraviesa el planeta, es necesario la generación **energías renovables e implementar medidas que aporten a la lucha contra el calentamiento global.**

Frente a ese panorama, Nicole Oliveira, directora de 350.org para Brasil y Latinoamérica, denunció que los **recientes acuerdos internacionales para enfrentar el cambio climático, "no son vinculantes",** ni representan sanciones para las naciones que no cumplan las normativas. Así mismo, resaltó la importancia de que no solo los gobiernos hagan parte de esa lucha, sino que también la sociedad civil participe activamente en este proceso.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
