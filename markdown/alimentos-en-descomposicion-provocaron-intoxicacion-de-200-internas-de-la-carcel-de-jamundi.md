Title: Cerca de 400 internas resultaron intoxicadas en cárcel de Jamundí
Date: 2016-03-04 13:57
Category: Judicial, Nacional
Tags: crisis carcelaria, Jamundi, Movimiento Nacional Carcelario
Slug: alimentos-en-descomposicion-provocaron-intoxicacion-de-200-internas-de-la-carcel-de-jamundi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Crisis-carcelaria-e1482755978940.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [04 Mar 2016] 

Según la denuncia de la Corporación Suyana, el pasado 2 de Marzo una de las internas de la cárcel de Jamundí se comunicó para denunciar que se proporcionaron alimentos en descomposición en el desayuno de ese mismo día. Las afectadas serían **más de 200 personas y hasta el momento**, según la Corporación, no han recibido atención médica por parte del INPEC o la empresa de salud que debe atender a la población reclusa.

La interna Mariela Ramírez denunció que este **[4 de Marzo más de 400 personas presentan síntomas de intoxicación y además el INPEC](https://archivo.contagioradio.com/crisis-carcelaria-un-interno-muere-y-otro-se-intenta-quitar-la-vida/)** no ha cambiado el menú propicio para enfrentar la situación que afronta el penal en este momento. El desayuno proporcionado fue avena en leche que al parecer estaba descompuesta.

Según la denuncia tampoco se han proporcionado medicamentos, incluso se ha pedido la solidaridad de las internas para reunir medicamentos como acetaminofén y atender la emergencia. Por ello exigen al director del Penal Carlos Alberto Murillo que se atienda la **emergencia de manera inmediata y que se prevengan nuevas situaciones dado que el suministro de alimentos en descomposición es reiterado** y no se ha presentado solamente en la cárcel de Jamundí.

Este hecho, sumando a la crisis de hacinamiento y de [salud en las cárceles](https://archivo.contagioradio.com/page/2/?s=carcel) de Colombia vuelve a prender las alarmas en torno a la política de seguridad que privilegia el arresto como mecanismo de re socialización. Al mismo tiempo se avanza en la revisión del estado de salud de los cerca de 200 internos en grave estado de salud en varias cárceles del país.
