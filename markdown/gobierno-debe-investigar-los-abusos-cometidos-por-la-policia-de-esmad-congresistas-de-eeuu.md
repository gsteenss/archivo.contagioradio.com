Title: “Gobierno debe investigar abusos cometidos por el ESMAD” Congresistas de EEUU
Date: 2017-06-08 10:04
Category: DDHH, Nacional
Tags: buenaventura, DDHH, ESMAD, racismo, USA
Slug: gobierno-debe-investigar-los-abusos-cometidos-por-la-policia-de-esmad-congresistas-de-eeuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/ESMAD-e1495823544560.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [08 jun 2017]

En una carta dirigida a la Alta Consejera de la Presidencia para las Víctimas, Paula Gaviria, un grupo de 10 congresistas de Estados Unidos **llamaron la atención por las denuncias de abusos por parte del ESMAD en el paro de Buenaventura**, así como las exigencias del Paro Cívico en Chocó que dan cuenta de la falta de efectividad en el combate al paramilitarismo en ese departamento.

Los congresistas señalaron que durante la respuesta del ESMAD contra los manifestantes en Buenaventura se utilizó “**una fuerza desproporcionada fue utilizada para dispersar a los manifestantes,** incluyendo gas lacrimógeno, helicópteros, bombas aturdidoras, tanques y armas de fuego”. También afirmaron que “ hay informes de que el saqueo y otras conductas desordenadas se produjeron a raíz de las tácticas violentas utilizadas por ESMAD”. (Le puede interesar: ["Esta es el acuerdo entre comité cívico de buenaventura y Gobierno"](https://archivo.contagioradio.com/acuerdan-plan-de-desarrollo-social-a-10-anos-en-buenaventura/))

Por otra parte reconocieron que en el Chocó, una de las exigencias de la movilización tenían que ver con las garantías de seguridad para los habitantes del departamento, esto frente a la arremetida de organizaciones paramilitares en varias regiones. De ello daría cuenta el desplazamiento de cerca de **5000 personas que tuvieron que alojarse en el casco urbano de Riosucio, localidad sobre la parte baja del Río Atrato.**

Frente a esas dos situaciones los congresistas instaron a investigar las denuncias contra la fuerza pública para que se garantice el derecho a la protesta “**creemos que el gobierno debe investigar los abusos cometidos por la policía de ESMAD**, sancionar a las personas declaradas culpables de actos ilícitos y tomar todas las medidas necesarias para garantizar la seguridad de los futuros manifestantes contra cualquier amenaza o ataque de grupos paramilitares.” (Le puede interesar: ["ESMAD impide labores de defensores de DDHH y periodistas en Buenaventura"](https://archivo.contagioradio.com/41556/))

Además los legisladores señalaron que podría existir el **racismo en la distribución de la riqueza,** dado que lo que se produce en el departamento del Chocó y en Buenaventura no llega ni a indígenas ni a comunidades negras. Para ellos, “el Chocó es un departamento rico en recursos y la discriminación racial parece desempeñar un papel en la distribución desigual de la riqueza en el Pacífico”.

Por ultimo resaltan que van a estar acompañando la implementación de los acuerdos a los que se llegó entre los bonaverenses y el Gobierno Nacional. A manera de recomendación señalan que, una forma de atender las exigencias de la población es implementar integralmente el capítulo étnico presente en el acuerdo de fin del conflicto firmado con las FARC-EP.

[Letter to Paula Gaviria\_6.7.2017](https://www.scribd.com/document/350741410/Letter-to-Paula-Gaviria-6-7-2017#from_embed "View Letter to Paula Gaviria_6.7.2017 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_94382" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/350741410/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-wgJxHdAxe4taUDMYgJqU&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7727272727272727"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
