Title: Gremio de profesores se suma a la movilización social en Argentina
Date: 2016-04-04 15:22
Category: Educación, El mundo
Tags: Argentina, Docentes Argentina, Grupo el Clarin, Mauricio Macri, Paro Nacional
Slug: gremio-de-profesores-se-suma-a-la-movilizacion-social-en-argentina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/docentes-argentina-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: losandes] 

###### [4 Abril 2016] 

Docentes de diferentes organizaciones sociales y gremiales en Argentina, se movilizan hoy hacia el Ministerio de Educación y Deporte para dar inicio al [PARO Nacional](https://archivo.contagioradio.com/?s=argentina) de 24 horas, con el fin de exigir al gobierno un **reajuste salarial y avances en la investigación del asesinato del docente Carlos Fuentealba.**

Como primera exigencia los docentes hacen un llamado al gobierno del presidente Macrí para que dé cumplimiento al acuerdo de **aumento salarial de 45% que se pactó con el Ministro de Educación Bullrick** y que hasta el momento no se ha llevado a cabo. Por otro lado, se exige la aplicación del convenio colectivo de trabajo y que no se den de baja **programas socioeducativos que agrupan a una cantidad de estudiantes de bajos recursos.**

De acuerdo con Martín Ogando, miembro de la Asociación Gremial Docente de la Universidad de Buenos Aires, los docentes están pidiendo el porcentaje que a lo sumo alcanza para apalear la **inflación que atraviesa Argentina en estos momentos**, no están exigiendo un aumento salarial en términos reales, sino por el contrario, tratar de alcanzar un poco la **[pérdida del poder adquisitivo de los salarios que se ha producido por la inflación.](https://archivo.contagioradio.com/el-verdadero-poder-en-argentina-lo-tiene-el-grupo-clarin-macri-es-una-mascara-v-h-morales/)**

Referente al asesinato del docente Carlos Fuentealba, los docentes exigen que se den avances en los resultados de la investigación y salga de la impunidad. Fuentealba fue un docente de la provincia de Neuquén asesinado hace nueve años en una movilización que impedía el corte de rutas de carretera, por este hecho solo se ha reportado una captura.

**Esta movilización se suma a las cientos de marcha y jornadas de huelga** por el derecho a salarios dignos de trabajadores de las empresas estatales y privadas, las movilizaciones a favor de la [libertad de prensa,](https://archivo.contagioradio.com/los-ataques-de-macri-contra-la-libertad-de-prensa-en-argenina/) y el rechazo continuo a las políticas económicas y de impunidad, entr eotras, impulsadas desde el inicio del gobierno de Mauricio Macri.

<iframe src="http://co.ivoox.com/es/player_ej_11042273_2_1.html?data=kpadlpeWe5Shhpywj5WbaZS1kZiah5yncZOhhpywj5WRaZi3jpWah5ynca7V09mSpZiJhaXijLTUw9PIs4ampJDay8rRptPjjMnSjdHFb6Ln0MjWw8jNaaSnhqeg0JCrtsbhysbZjanTp8bijoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
