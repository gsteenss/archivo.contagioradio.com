Title: Militares vinculados con falsos positivos podrían ascender a generales
Date: 2016-01-14 15:15
Category: DDHH, Nacional
Tags: Ascensos en las FFMM, Falsos positivos Colombia, FFMM
Slug: militares-vinculados-con-falsos-positivos-podrian-ascender-a-generales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/falsos-positivos-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Ene 2016] 

**Siete oficiales de las fuerzas militares relacionados con hechos de ejecuciones extrajudiciales y otras violaciones de DDHH cometidas entre los años 2002 y 2008 han sido nominados al rango de generales**. Los uniformados que enfrentan investigaciones por estos hechos esperan la aprobación del Congreso de la República para seguir su entrenamiento al Centro de Altos Estudios Militares (CAEM), pre-requisito para ser ascendidos, según la denuncia de la Oficina en Washington para Asuntos Latinoamericanos (WOLA)

Los nombres de los militares nominados a ascenso son el coronel del Ejército Nelson Velásquez Parrado, el coronel del Ejército Marcos Evangelista Pinto Lizarazo, coronel del ejército Edgar Alberto Rodríguez Sánchez, el coronel del ejército Adolfo León Hernández Martínez, general del ejército Marcolino Tamayo Tamayo, general del ejército Mauricio Ricardo Zúñiga Campo, y el coronel de la Fuerza Aérea Sergio Andrés Garzón Vélez.

A través de una carta, WOLA, objetó el posible ascenso de los militares relacionados con violaciones a los derechos humanos,  **“Colombia está finalmente llegando al fin de su conflicto armado, y está haciendo rendir cuentas más y más a abusadores de derechos humanos, pero las promoción de estos funcionarios sin permitir que el sistema de justicia penal haga su trabajo representaría un retroceso",** dijo Adam Isacson, Coordinador Principal del Programa de Políticas de Seguridad Regional. **"Todos esperábamos que las fuerzas armadas de Colombia hubiesen dejado este tipo de cosas en el pasado. Es por eso que estas nominaciones son tan desconcertantes”.**

La organización demandó al Ministro de Defensa Luis Carlos Villegas, reconsiderar la promoción de hombres que se encuentran en investigaciones por violación de los derechos humanos.

[Carta a Ministro de Defensa Juan Carlos Villegas](https://es.scribd.com/doc/295489527/Carta-a-Ministro-de-Defensa-Juan-Carlos-Villegas "View Carta a Ministro de Defensa Juan Carlos Villegas on Scribd")

<iframe id="doc_2038" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/295489527/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>  
 
