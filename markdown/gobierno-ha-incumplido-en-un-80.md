Title: Hay un 80% de incumplimientos por parte de Gobierno en Zonas veredales: FARC
Date: 2017-03-06 15:35
Category: Nacional, Paz
Tags: Zonas Veredales Transitorias de Normalización
Slug: gobierno-ha-incumplido-en-un-80
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [06 Mar 2017] 

Hay un **80% de incumplimientos por parte del gobierno en la construcción de las estructuras en las zonas veredales**, no es cierto que los guerilleros tengan peticiones "suntuosas", ni que sea la guerrilla la culpable de los retrasos, así lo expresa el informe expuesto el día de hoy por la comisión de las FARC-EP que visitó las 26 zonas veredales del país, como parte del seguimiento a la implementación de los acuerdos de paz.

El primer punto que aclara la delegación de las FARC-EP, es que no es cierto que las zonas veredales sean espacios para dejar las armas, sino que son lugares que servirán para hacer el proceso de reincorporación de la guerrilla a la sociedad, por lo tanto, la construcción de los espacios comunes, la dotación de los servicios públicos como agua, **luz y saneamiento básico, son necesarias para llevar a cabo esa pedagogía, que podría durar más de 6 meses**. Le puede interesar: ["Inicio proceso de dejación de armas y es "irreversible": FARC"](https://archivo.contagioradio.com/ya-comenzo-el-proceso-de-dejacion-de-armas-y-es-irreversible/)

De acuerdo con el informe de las FARC, la instalación de servicios públicos no se ha culminado en su totalidad en ninguna de las 26 zonas, mientras que hay 11 que no cuentan con ningún servicio, es decir solo hay un cumplimiento del **23% de la adecuación**. De igual modo **tampoco se han finalizado las construcciones de las zonas comunes y hay 2 en las que ni siquiera se han iniciado con la construcción.**

![servicios farc](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/servicios-farc.png){.alignnone .size-full .wp-image-37285 width="1124" height="595"}

Referente a los materiales y tipos de instalación, Pastor Alápe señaló que lo mencionando por el director de las ZVTN, Carlos Córdoba, sobre a las **“peticiones suntuosas”** que hacen los guerrilleros para estar en los puntos, es completamente falsa y no reconoce la realidad de estos lugares. Le puede interesar: ["Nueva ola de solidaridad con las zonas veredales transitorias"](https://archivo.contagioradio.com/nueva-ola-de-solidaridad-con-las-zonas-veredales-transitorias/)

Otro de los incumplimientos que "afana" a la guerrilla de las FARC-EP es la alimentación que se está distribuyendo a las zonas veredales y que no llega en las cantidades suficientes. Carlos Lozada, integrante de las FARC, expresa que tienen informes donde existirían irregularidades en los costos de los productos, **“adquiríamos una libra de arroz por mil pesos, ahora la están facturando por 15 mil”.**

![alimentaciòn farc](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/alimentaciòn-farc.png){.alignnone .size-full .wp-image-37287 width="1135" height="590"}

Las dotaciones de kit de aseo, calzado y ropa, **se ha incumplido en todas las zonas veredales en un 80%**, situación que se agrava debido a que en esta **dotación va incluida la de las madres gestantes que no ha llegado a ninguna zona** y que pone en riesgo la salud tanto de las madres como de los bebes en estos lugares. Le puede interesar: ["Continúan los incumplimientos del gobierno en las Zonas veredales"](https://archivo.contagioradio.com/continuan-incumplimientos-del-gobierno-en-zonas-veredales/)

![Informe de las FARC](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/dotaciòn-farc.png){.size-full .wp-image-37288 width="1115" height="589"}

Finalmente está el compromiso que había hecho el gobierno para construir los campamentos de las y los guerrilleros amnistiados, frente a este punto **solo existe una zona veredal, la de Buena Vista en Mesetas, que tiene el inicio de las construcciones**, en las demás no hay ni materiales ni señalización para estos campamentos.

![Informe FARC](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Obras-excarcelados.png){.size-full .wp-image-37289 width="1104" height="605"}

De acuerdo con Carlos Lozada, integrante de las FARC-EP, la continuación de estas fallas y las polémicas que se han creado **“demuestran la incapacidad estatal y gubernamental para implementar los Acuerdos”** y agrega que si bien es cierto que el gobierno ha incumplido ellos continuaran con el proceso de dejación de armas para cumplir con lo pactado.

###### Reciba toda la información de Contagio Radio en [[su correo]
