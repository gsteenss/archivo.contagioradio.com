Title: ¿Quién los Mató? Homenaje a jóvenes y niños víctimas de masacres y ejecuciones extrajudiciales
Date: 2020-09-11 14:54
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Canción, Fuerzas militares, Masacres, quién los mató
Slug: quien-los-mato-cancion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Quien-los-mato-cancion.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego de la masacre de los cinco niños en el barrio Llano Verde en Cali, la de Samaniego en Nariño, en las que la mayoría de las víctimas son menores de edad, un grupo de cantantes, compositores y arreglistas ha impulsado la canción ¿Quién los mató? que también esta dedicada a las madres de las víctimas e invita a identificarse con su dolor.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se trata de Cristhian Salgado el productor, Hendrix Hinestroza, Cristhian Salgado, Alexis Play, Junior Jein, Nidia Góngora quienes son los compositores. La canción grabada en Bombo Records Estudios, en Cali - Colombia ha tenido una gran acogida, pues justamente apela a la solidaridad con el dolor de las familias de las víctimas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Esta es la letra de ¿Quién los mató?:

<!-- /wp:heading -->

<!-- wp:paragraph -->

*Madre,  
No llegaré a la hora de la cena  
Aparecí en un lugar  
Que no era mi hogar  
Me duele estar tan lejos  
Oigo me están llamando*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*No llegaré a la hora de la cena  
Aparecí en un lugar  
Que no era mi hogar  
Dicen que ven mi cuerpo  
Oigo me están llorando*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Volvió el monstruo que acecha  
El que despoja las tierras  
Y el que pudre las cosechas  
Tiene la mirada fria y carece de empatía  
Su apetito es insaciable, tiene la panza vacía  
No cree en edades, ni dogmas, ni formas, ni normas  
Destruye lo que vé y no se conforma  
Solo obedece intereses economicos  
Infunde el miedo y entierra a soldados anónimos  
Hermanos de otras madres que salieron de sus casas  
Se fueron hace un día y hace años que no abrazan  
Ese monstruo llegó al cañaduzal  
Quiso azúcar de la vida y dejó peste con Cal  
¿Por qué ser otro desaparecido?  
¿Por qué darlo todo por perdido?  
¿Por qué cambiar mi nombre y apellido?  
¿O me quieren pasar por otro falso positivo?*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Madre,  
No llegaré a la hora de la cena …*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Hay sangre en la arena y esta vez no es del torero  
Son cinco chicos que salieron pero nunca volvieron  
Uno de ellos resistió de una manera inexplicable  
Para señalar el camino y que lo pudiera encontrar su madre  
En medio de una escena con respuestas en potencia  
Y unos cuantos que no se entendía que hacían allí  
El dolor de familiares impulsados por el miedo  
Queriendo llevar sus hijos sin saber si podrían salir  
Con vida a contarle al mundo lo ya sucedido  
Si esta madre no se atreve todo estaría perdido  
Y estaría en archivo y otra historia pa contar  
Del país con la clase obrera que se muere en la impunidad*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Sangre,  
Hay sangre en unas manos ajenas  
Si me convierto en canción  
Solo recuérdame feliz  
Aquí no pasa el tiempo  
No hay pena o sufrimiento*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Ahora soy yo quien va a escandalizarse  
Con la fuerza de los gritos de Ruby Cortes en los cañaduzales  
Le exijo a la justicia que este caso se aclare  
Y que no quede impune como casi siempre hacen  
Nada, la vida de los negros no importa nada  
Lo primero que dicen es: "Andaban en cosas raras"  
Como Jean Paul, Jair, Léyder, Álvaro y Fernando  
Somos víctimas del sistema y el abandono del estado  
Pero el pueblo no se rinde Carajo!*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*¿Quién los Mató?  
¿Quién interrumpió sus sueños?, eso no era justo, no  
¿Quién los Mató?  
Eran universitarios los de Samaniego, acabaron con sus vidas y con sus sueños*  
*¿Quién los Mató?  
Oi e ie ie oi ¿Quién los mató?  
¿Quién los Mató?  
No hay propuestas, ni protesta, nadie sale  
La indiferencia social mata a líderes sociales  
¿Quién los Mató?  
Pido justicia  
O ie Quien los mató  
¿Quién los Mató?  
Las masacres en el Urabá,  
Por esos crímenes atroces atroces ¿quién va a pagar?  
¿Quién los Mató?  
Pregón  
Quedaron madres solas, padres solos  
Y hermanos también  
¿Quién los Mató?  
El miedo acorrala,  
El llanto de una madre hace más eco que una bala  
No mas farsas ni fachas  
No se olvida el dolor de las madres de Soacha  
¿Quién los Mató?  
Hasta cuando esta guerra que cobra vidas inocentes*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Madre,  
No llegaré a la hora de la cena …*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Madre,  
¿te acuerdas que te hablé de las estrellas?  
Hoy ellas están aquí  
Hay muchas otras junto a mi  
Y todas van volando  
Se van surcando en lo alto*

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=i7vBVvvHBYY\u0026feature=youtu.be","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=i7vBVvvHBYY&feature=youtu.be

</div>

</figure>
<!-- /wp:core-embed/youtube -->
