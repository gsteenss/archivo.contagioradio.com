Title: Frente Amplio por la Paz pide que se agilice cese al fuego bilateral
Date: 2015-11-09 16:16
Category: Entrevistas, Paz
Tags: Cese al fuego bilateral, cese al fuego unilateral, FARC, Frente Amplio por la PAz, gobierno colombiano, Iván Cepeda, ministerio de defensa, Polo Democrático Alternativo, proceso de paz, teléfono rojo
Slug: frente-amplio-por-la-paz-pide-que-se-agilice-cese-al-fuego-bilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Frente-Amplio-por-la-Paz-e1454017570870.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [laportadacanada.com]

<iframe src="http://www.ivoox.com/player_ek_9330141_2_1.html?data=mpigkpaYdY6ZmKiakp6Jd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRitPZz9nSjabRtM3d0JDd0dePsMKfscbnjdXNqMaf0trSjdjJb8LbytHWxcqPp8bnxpDOzpDKuY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Iván Cepeda, senador] 

###### 9 Nov 2015

El Frente Amplio por la Paz, emitió un comunicado en el que se pide al gobierno Nacional acelerar y promover las condiciones necesarias para que se agilice el cese al fuego bilateral.

De acuerdo con el senador del Polo Democrático Alternativo e integrante del Frente Amplio por la Paz, Iván Cepeda, “**llego el momento de avanzar en un cese de hostilidades, para lo que se requiere que las fuerzas militares dejen de copar territorios**” y ejecutar operativos, mientras que la guerrilla mantiene el cese al fuego unilateral.

Cepeda, asegura que además de las constantes operaciones militares que se vienen desarrollando en el país, el crecimiento y proliferación de las bandas paramilitares, tienen en riesgo el mantenimiento del cese al fuego de las FARC, que este año ya fue suspendido una vez por cuenta de las actuaciones de la fuerza pública.

“El  fenómeno del crecimiento y proliferación de grupos paramilitares podría generar que se fracase en el proceso de paz, como ha sucedió en otros países”, aseguró el senador.

Mediante el comunicado, el Frente Amplio propone que “se trabaje en la formulación de nuevos mecanismo, que permitan asegurar el camino hacia el cese bilateral y definitivo (…) incluyendo un sistema de monitoreo y verificación con participación internacional y la participación de las comunidades y movimientos sociales”.

Desde el Frente Amplio se propone, entre otras medidas, que se desarrollen propuestas que permitan que los guerrilleros y guerrilleras se inserten en la vida  civil y dejen las armas, pero **dejando de lado las campañas de desmovilización y en cambio se generen escuelas de formación política, así mismo se propone un “teléfono rojo”** como mecanismo de comunicación directa con el Ministerio de Defensa.

Finalmente, las organizaciones que hacen parte del Frente Amplio, se comprometen a integrar una comisión de apoyo para la distención del conflicto que trabajaría en los territorios donde podría estar en riesgo el cese al fuego.

[  
](https://archivo.contagioradio.com/frente-amplio-por-la-paz-pide-que-se-agilice-cese-al-fuego-bilateral/comunicado-frente-amplio-2/) [![Comunicado Frente Amplio 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Comunicado-Frente-Amplio-1.png){.aligncenter .size-full .wp-image-16909 width="584" height="736"}![Comunicado Frente Amplio 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Comunicado-Frente-Amplio-2.png){.aligncenter .size-full .wp-image-16908 width="559" height="741"}](https://archivo.contagioradio.com/frente-amplio-por-la-paz-pide-que-se-agilice-cese-al-fuego-bilateral/comunicado-frente-amplio-1/)
