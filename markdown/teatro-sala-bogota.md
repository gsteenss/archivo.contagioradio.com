Title: La rebeldía y el absurdo de la guerra en dos obras de teatro
Date: 2018-02-14 15:22
Category: eventos
Tags: Bogotá, Cultura, teatro
Slug: teatro-sala-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/MG_0108-e1518639192146.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Días de campo 

###### 14 Feb 2018 

Dos producciones cargadas de alto voltaje dramático y humor negro llegan en corta temporada del 15 de febrero al 8 de marzo a la Sala- Fábrica de Hechos Culturales de Bogotá: **Atravesado y Días de Campo**, producidas por las compañías Quinta Picota y Algarabía Teatro respectivamente.

**Atravesado** es espectáculo unipersonal, bajo la dirección de Alexis Rojas e Iván Carvajal, adaptado por la dramaturga Viviana Zuluaga, quien transforma el cuento de **Andrés Caicedo**, rebelde y revolucionario que dejo su huella en la historia de la literatura colombiana, a una versión que combina música, narración y teatro, con referencias del cine, que respeta el espíritu del célebre escritor caleño.

[![atravesado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Felipe_Atravesado17-800x533.jpg){.alignnone .size-medium .wp-image-51529 width="800" height="533"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Felipe_Atravesado17-e1518639679147.jpg)

Su protagonista Julián Mora,  da vida a Andrés Iván Zuluaga Rojas, un hombre dispuesto a darse duro con quien sea, sin miedo (o disfrazándolo de gallardía), sin titubeos, tal como aprendió andando en la calle, con su tropa "**Presume cicatrices que parecen el mapa del camino recorrido, interrumpido por la partida de cada amigo ido, de cada amigo muerto**"

Posteriormente los viernes y sábados, el público podrá disfrutar de otro relato que alude a nuestra realidad, en **Días de Campo,** versión libre de la obra Pic Nic, de Fernando Arrabal, que denuncia el absurdo de la guerra a través de personajes inocentes y sencillos inmersos en una situación de violencia, pero con una alta dosis de ironía.

Bajo la dirección de Alexis Rojas y Natalia Ramirez, con las actuaciones de Diana Silva, Nataly Ruiz, Jhon Hernández, Sergio Cadavid y Angélica Parra, la obra refleja **la ilusión de un final diferente para la guerra**, donde un soldado, recibe una visita inesperada que desencadena una serie de acontecimientos sorpresivos. ¿Quiénes son los malos? ¿Quiénes son los buenos? ¿Por qué somos enemigos?

La Sala- Fabrica de Hechos Culturales, abierta al público en junio del 2017, bajo la dirección de reconocido actor y mimo Julio Ferro, con una programación cultural ininterrumpida donde se destaca su proyecto de teatro para animales de compañía.

Del 15 de febrero al 8 de marzo a las 7:30 p.m.  
**Atravesado:** jueves 15 y 22 de febrero – 1 y 8 de marzo  
**Días de Campo:** viernes 16 y 23 de febrero – 2 y 3 de marzo  
Sábados: 17 y 24 de febrero - 3 de marzo  
Bono de apoyo: 25.000 General y \$20.00 estudiantes y tercera edad.  
**Lugar:** La Sala- Fábrica de Hechos Culturales – Carrera 22 \# 41-28

 
