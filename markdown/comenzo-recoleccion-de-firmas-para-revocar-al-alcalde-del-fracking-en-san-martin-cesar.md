Title: Comenzó recolección de firmas para revocar al alcalde del Fracking en San Martín, César
Date: 2017-02-10 14:49
Category: Movilización, Nacional
Tags: cesar, fracking, san martin
Slug: comenzo-recoleccion-de-firmas-para-revocar-al-alcalde-del-fracking-en-san-martin-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/sanMartin.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Samuel Arregoces ] 

###### [10 Feb 2017] 

**Arrancó la revocatoria del alcalde del Fracking, así llaman en San Martín, César, a Eduardo Celis** quien está en este cargo desde el 2015 y que de acuerdo con los habitantes, ha hecho muy poco en respaldar a la comunidad que ha expresado su rechazo en contra de esta práctica, se necesitan reunir **1.086 firmas** y desde esta semana comenzó la recolección que espera recoger por lo menos el doble de la cifra exigida.

De acuerdo con Orlando Reina, uno de los impulsores de esta iniciativa, las respuestas violentas que ha dado Celis a las **protestas de los habitantes con el uso del ESMAD, evidencia su poco interés por dialogar y representar los verdaderos intereses de la comunidad.** Le puede interesar:["5 Heridos deja agresión del ESMAD a comunidad de Cuatro Bocas en San Martín"](https://archivo.contagioradio.com/5-heridos-deja-agresion-del-esmad-a-comunidad-de-cuatro-bocas-en-san-martin/)

“Estamos insatisfechos con la política del Alcalde, hay ausencia de diálogo y concertación, las problemáticas por las que protestan las comunidades, él siempre las está solucionando con la implementación de la fuerza. **Es el único alcalde que ha usado de manera permanente al ESMAD”.** Le puede interesar: ["Gases y aturdidoras del ESMAD contra pobladores de San Martín"](https://archivo.contagioradio.com/gases-aturdidoras-del-esmad-pobladores-san-martin/)

Reina afirmó que  es necesario que haya un cambio de alcaldía para poder realizar una consulta popular minera que sí este respaldada por el alcalde y que le permita a los ciudadanos de San Martín, decidir sobre su territorio **“comprendemos que es el gobierno el que quiere a como de lugar sacar petróleo,** pero la constitución le da la herramienta al alcalde de hacer la consulta popular”. Le puede interesar: ["El mapa del Fracking en Colombia"](https://archivo.contagioradio.com/mapa-del-fracking-colombia/)

Una vez, el Consejo Nacional Electoral apruebe las firmas recolectadas, se consultará a toda la población, a través de una votación, si está de acuerdo con la revocatoria, llegado a ser este el caso **se procederá con nuevas elecciones a la Alcaldía**. Reina también aclaró que detrás de la iniciativa no hay ningún tinte político y que es respaldada por la ciudadanía.

<iframe id="audio_16945926" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16945926_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
