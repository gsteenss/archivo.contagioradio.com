Title: Con documental, Argentina celebra 89 años de Fidel Castro
Date: 2015-08-13 12:48
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: "Rebeldes, Carlos Javier Rodríguez periodísta, Cumpleaños 89 Fidel Castro, Frank Fernández, Omara Portuondo, Santiago Feliú, Silvio Rodríguez, y así seguimos" documental
Slug: con-documental-argentina-celebra-89-anos-de-fidel-castro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Fidel-castro-e1439487589530.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [13 Ago 2015 ] 

"*Rebeldes, y así seguimos*", es el título de la producción argentina que se estrena hoy en Buenos Aires, coincidiendo con el aniversario número 89 del nacimiento del líder de la Revolución cubana Fidel Castro.

La producción, dirigida por el periodísta cubano radicado en Argentina, Carlos Javier Rodríguez (*Che en todas partes*, 2007) con la producción de "Craneo" y participación de Rafael de la Torre, busca evidenciar la actualidad cubana, desde la perspectiva de ciudadanos comunes, profesionales y artistas, y cómo el proceso revolucionario ha incidido en sus vidas.

Un acercamiento a la figura de Fidel, a través de valoraciones de cubanos y otras personalidades de renombre como el periodista y escritor Ignacio Ramonet, el Premio Nobel de Paz Adolfo Pérez Esquivel y el conductor televisivo Víctor Hugo Morales. De igual manera, aborda las implicaciones del restablecimiento de relaciones con Estados Unidos, el proceso de actualización de la economía en la isla y las expectativas que genera.

La música resulta relevante en el documental, no sólo por la incorporación de temas compuestos por Silvio Rodríguez , también por la entrevista realizada al pianista y compositor Frank Fernández, a la diva Omara Portuondo y el tributo especial al desaparecido trovador cubano Santiago Feliú, quén interpretó una canción inédita para la cinta.

<iframe src="https://www.youtube.com/embed/HTOehVH7JU8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El cine Gaumont del Instituto Nacional de Cine y Artes Audiovisuales es el lugar escogido para el estreno del audiovisual, y tendrá una segunda proyección el 10 de septiembre entro Cultural de la Cooperación de Buenos Aires. Luego de una pequeña gira por ciudades argentinas, los realizadores buscarán incluir el documental en la próxima edición del Festival Internacional del Nuevo Cine Latinoamericano de La Habana.

(Con información de Prensa Latina)

 

 

 

 

 
