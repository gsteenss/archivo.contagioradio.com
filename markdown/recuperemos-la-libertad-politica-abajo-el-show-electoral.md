Title: Recuperemos la libertad política: abajo el show electoral
Date: 2018-05-11 06:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: democracia, Elecciones presidenciales, libertad política, politica
Slug: recuperemos-la-libertad-politica-abajo-el-show-electoral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/debate2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Teleantioquia 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 11 May 2018 

[El simple comparativo de los ambientes de opinión entre las elecciones parlamentarias y presidenciales, en donde unas elecciones parecen “más interesantes” o más “candentes” que otras, deja en evidencia la crisis del régimen democrático representativo que existe en el país. Un régimen que impulsa la individualización tanto de los problemas como de las soluciones políticas. Que impulsa la cultura de la imagen por sobre el contenido ideológico de los discursos, un régimen que ha precarizado las funciones intelectuales de la sociedad colombiana a tal punto de delegar a los medios corporativos de información, la tarea de funcionar como el único marco para entender la realidad. Algo muy grave, si se comprende que esos medios masivos, son masivos no siempre por la calidad de la información que ofrecen, sino por su capacidad económica, es decir, su posición dominante en el mercado que consecuentemente les da amplitud e influencia.  Lo anterior, por definición, bipolariza los términos “sociedad de mercado” y “sociedad democrática”.]

[¡Sí! bipolariza. ¡toca! pues no tanto en la polarización como sí en la bipolarización y ojalá en la multipolarización es donde se halla el verdadero respeto a las diferencias, la autenticidad política y por tanto la raíz liberadora del sujeto. Basta ya, de discursos trasnochados neoliberales sobre la necesidad de hacer desaparecer las contradicciones políticas y sociales, ya muchos saben que a pesar de lo lindo que suena la frase “no polaricemos”, en la teoría y en la práctica eso solo conviene a quien domina el mercado.]

[Una sociedad no es democrática porque tenga derecho a votar. Que se pueda votar es importante, pero para hablar de democracia, los de abajo, la gente común y corriente tiene que tener poder, tiene que gozar de derechos y no solo tener derecho a comprar lo que dejó de ser un derecho.]

[No tener acceso a la salud pública de calidad, educación pública, pensión, carretas libres de peajes, sistemas de transporte públicos (no privatizados), con votaciones o sin votaciones, es sin duda una situación antidemocrática. Muchos personajes de clases poderosas y sus cajas repetidoras, es decir, mamarrachos de clase media bastante adiestrados, quieren imponer los valores de una sociedad de mercado por sobre los de una sociedad democrática cacareando que las mayorías “quieren todo gratis y así no se puede”. Acaso no podríamos responder quienes nos oponemos a esa tergiversación de la democracia “pero es que ya hemos pagado por todo, peso sobre peso en todos y cada uno de los impuestos lo hemos pagado”. Por tanto, que se acabe el show de la democracia que hay por estos días en Colombia, ya que, si la soberanía no está en la gente sino en los mercados, así exista un sistema electoral, lo que hay es una dictadura.]

[Los medios masivos informativos son agentes dominantes en el mercado y al estar limitados por su carácter informativo, someten a las masas a vivir del “ahora” del “yo creo” del “yo opino” de la efímera “tendencia”, de la instantánea inconexa que niega la posibilidad comprensiva y acepta el chapuceo de los temas, chapuceo que se convierte en una terrible y violenta bandera que levantan los individuos a su antojo, porque se funda sobre una supuesta libertad de expresión que al final, no es libre, pues es construida sobre la base de las tendencias creadas por quienes dominan el mercado, y por tanto protege intereses muy particulares.]

[De allí que emerja la baja identificación de las clases populares y las clases medias urbanas con un proyecto de cambio político estructural, de allí que, en los campos, donde la gente consume mil veces menos basura, la tienen que matar para que “entienda”.]

[La crisis del régimen democrático representativo se manifiesta en los shows en que se han convertido los supuestos debates políticos. Es una equivocación muy grande llevar debates que se deben tener en escenarios políticos, al escenario de los medios de comunicación masiva. Allí pierden el carácter democrático y se convierten en meros productos que la gente elige consumir, mientras sin darse cuenta, al “consumir política” le reducen a la gente su capacidad política a la más mínima expresión: el voto.]

[Hay que debatir donde debemos debatir. No es en Facebook. No es en Twitter. No es en los shows mediáticos. Es en los sindicatos empresariales y agrarios, en los consejos populares y comunitarios, en los colegios y universidades, en los cabildos, en medios democráticos de información, en las sedes de partidos, en las sedes de movimientos políticos. No podemos permitir que el debate político se dé donde los poderosos quieren (es decir en los medios masivos y corporativos de comunicación) sino que somos nosotras y nosotros los que debemos otorgar sentido político a nuestras vidas, debemos recuperar nuestra libertad política. Es un deber ético enriquecer a la sociedad democrática y resistir a la sociedad de mercado cuando se trate de política.  ]

[En este marco, es esperable que emerjan las presiones, si decidimos enfrentarnos a este régimen democrático representativo en decadencia, pues el régimen actuará y las reglas serán inequitativas, a veces injustas; no obstante, con orgullo y convicción, la lucha debe continuar. Por la gente que hoy muere asesinada en Colombia, y es condenada al olvido por esa sociedad de mercado, nosotros, desde la sociedad democrática dejaremos la piel e incluso la salud, porque sentiremos como la carne misma, las ideas sobre una nueva Colombia que continúan floreciendo, tan coloridas, tan vivas y tan resistentes a las balas, a la infamia, al olvido que jamás seremos.]

<iframe id="audio_25922598" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25922598_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
