Title: Paramilitares de las Águilas Negras amenazan a organizaciones que impulsaron 'Carnaval por el agua'
Date: 2016-06-09 16:43
Category: DDHH, Nacional
Tags: Aguilas Negras, consulta popular, FARC, Ibagué, Mineria
Slug: paramilitares-de-las-aguilas-negras-amenazan-a-organizaciones-que-impulsaron-carnaval-por-el-agua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Ibague.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carnaval por el agua 

###### [9 Jun 2016] 

A través de un panfleto el grupo paramilitar ‘las Águilas Negras’ amenazó a las organizaciones y colectivos que hicieron parte del **‘Carnaval por el agua’ en Ibagué’, el pasado viernes, en donde participaron cerca de 120 mil personas,** que en la marcha expresaron su NO rotundo al proyecto minero La Colosa de Anglo Gold Ashanti.

De acuerdo con Renzo Alexander García, integrante del Comité Ambiental en Defensa de la Vida, las amenazas han circulado vía mensajes de texto y correos electrónicos, de los integrantes y líderes del Comité, el Congreso de los Pueblos, Marcha Patriótica, el Consejo Regional Indígena del Tolima CRIT, Organización Nacional Indígena de Colombia ONIC, la Cumbre Agraria, Campesina, Étnica y Popular, las organizaciones defensoras de los derechos humanos y el alcalde de Ibagué, Guillermo Alfonso Jaramillo.

El panfleto los señala como colaboradores de la guerrilla de las FARC, y también son amenazados por **“desinformar a la gente solo para poner tropiezo a las buenas ideologías y en los proyectos de desarrollo en el Tolima y en nuestra patria”**, dice el comunicado de las Águilas Negras.

En ese sentido, se les ordena que cesen sus actividade teniendo en cuenta que la comunidad de Ibagué se encuentra organizada para lograr una consulta popular, con la que esperan detener los proyectos mineros en esta zona del país. Para ello, se adelantan diferentes acciones de movilización y de pedagogía con la población para explicar las afectaciones de la minería en esos territorios.

Pese a que este grupo paramilitar declaró a los ambientalistas como objetivo militar, los defensores del ambiente seguirán movilizándose a la espera del concepto de constitucionalidad que deberá emitir **en los próximos días el Tribunal Administrativo del Tolima sobre la Consulta popular, que tiene un umbral de 130 mil votantes.**

Las organizaciones y colectivos amenazados le exigen al gobierno nacional que se implementen las medidas necesarias para garantizar la seguridad de los ambientalistas quienes ya han sido amenazados antes por su lucha en la defensa del agua y el territorio. Por el momento se denuncia que **las investigaciones no avanzan y que entre el 2013 y 2014 grupos paramilitares asesinaron a tres personas.**

[![Amenaza-contra-comite-ambiental-original-574x600](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Amenaza-contra-comite-ambiental-original-574x600.gif){.aligncenter .wp-image-25225 width="512" height="535"}](https://archivo.contagioradio.com/paramilitares-de-las-aguilas-negras-amenazan-a-organizaciones-que-impulsaron-carnaval-por-el-agua/amenaza-contra-comite-ambiental-original-574x600/)

<iframe src="http://co.ivoox.com/es/player_ej_11845341_2_1.html?data=kpallpqXeJKhhpywj5aYaZS1lJiah5yncZOhhpywj5WRaZi3jpWah5yncbPZz9_cjazFtsSZpJiSo6nFb46fpNTay9mJh5SZop6Yo9LGrcbi1cbZjcrSb6XZx8rb1caPqMafzcaYuM7IpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
