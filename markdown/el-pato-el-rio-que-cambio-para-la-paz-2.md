Title: El Pato, el río que cambió para la paz
Date: 2019-11-15 17:31
Author: CtgAdm
Category: DDHH, Especiales Contagio Radio
Tags: acuerdo de paz, Caquetá, Deporte, etcr, excombatientes, Miravalle, Proyectos Productivos, Rafting
Slug: el-pato-el-rio-que-cambio-para-la-paz-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/IMG_0466.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**EL PATO, EL RÍO QUE CAMBIÓ PARA LA PAZ **

#### El Río Pato en Caquetá,  fue la sede del Campeonato Nacional de Rafting Remando por la Paz, un escenario de reconciliación con  la participación de deportistas, comunidad campesina, excombatientes y Fuerza Pública 

16 de noviembre de 2019

<figure>
![Cristian](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Background-copy.png){width="437" height="447" sizes="(max-width: 437px) 100vw, 437px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Background-copy.png 437w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Background-copy-293x300.png 293w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Background-copy-370x378.png 370w"}

</figure>
###### Cristian Mora J

Periodista/Contagio Radio

*"El barco en el cual volvieron Teseo y los  
jóvenes de Atenas tenía treinta remos, y los atenienses lo conservaban desde la  
época de Demetrio de Falero, ya que retiraban las tablas estropeadas y las reemplazaban  
por unas nuevas y más resistentes, de modo que este barco se había convertido  
en un ejemplo entre los filósofos sobre la identidad de las cosas que crecen."*  
-  Plutarco

![Río Pato Remando por la Paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/IMG_0466.jpg "Río Pato Remando por la Paz")

Existe una doctrina en la filosofía atribuida al griego Heráclito quien afirmaba que “en los mismos ríos entramos y no entramos, pues somos y no somos los mismos.”, tanto porque las aguas son cambiantes como porque el hombre nunca es el mismo, una reflexión que parece adaptarse a la historia del río Pato en Caquetá.

San Vicente del Caguán es el lugar donde se inaugura el lanzamiento del Campeonato Nacional de Rafting Remando por la Paz, un evento que por vez primera congrega a un total de 20 equipos y 95 deportistas provenientes de Santander, Caquetá, Meta, Cundinamarca y Huila, además de dos invitados especiales de Costa Rica.

<figure>
![Remando por la Paz Rafting El Pato](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0180-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0180-1024x683.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0180-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0180-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0180-370x247.jpg 370w"}  

<figcaption>
Foto: Contagio Radio

</figcaption>
</figure>
<figure>
![Remando por la Paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Colombia-1024x668.jpg){width="1024" height="668" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Colombia-1024x668.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Colombia-300x196.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Colombia-768x501.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Colombia-370x241.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Colombia.jpg 1280w"}  

<figcaption>
Foto: @Remandoporlapaz

</figcaption>
</figure>
> ### "Con el silencio de las bombas y los fusiles pudimos ver y apreciar la naturaleza con otros ojos"

La idea del campeonato surgió cuando el equipo de Remando por la Paz, conformado por excombatientes de las FARC – EP y campesinos de la región de El Pato, regresaron de Australia después de representar a Colombia en el mundial de Rafting 2019, sin embargo, todo comenzó a materializarse tan solo unos meses antes de la competencia.

Después de la inauguración del evento, que contó con la participación de diversas organizaciones; los deportistas, periodistas y habitantes de las zonas aledañas se acomodan como pueden a bordo de tres chivas que parten del municipio y se dirigen al Espacio Territorial de Capacitación y Reincorporación (ETCR) de Miravalle donde se desarrollará el torneo.

<figure>
![ETCR Miravalle](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/IMG_0543.jpg "ETCR Miravalle")  

<figcaption>
Fotografía: Contagio Radio  
Murales de Manuel Marulanda y Jorge Briceño Suárez en la parte superior del ETCR

</figcaption>
</figure>
Tras dos horas de carretera, las chivas se internan en la montaña y ascienden a través de vertiginosos senderos, el ascenso dura cerca de 20 minutos hasta que finalmente y tras cruzar los anillos de seguridad establecidos por la Policía, el Ejército y la Guardia Campesina, el ETCR de Miravalle está ante nosotros.

Descendemos de las chivas, hace frío y la tierra es fangosa bajo nuestros pies. En medio de la noche, las viviendas parecen más lejanas de lo que en realidad lo son y los bombillos que iluminan las casas titilan a la distancia como si se tratase de una  constelación.

Aunque existen algunos campamentos en la parte de abajo donde se van a hospedar los deportistas, nuestro camino hasta las casas está en dirección cuesta arriba, mitad pavimentado, mitad destapado  y que a pie tomaría cerca de quince o veinte minutos recorrer, lo realizamos a bordo de una camioneta, reduciendo el ascenso a cinco minutos. En la parte de arriba nos recibe, Adriana, una de las líderesas del ETCR, es una mujer robusta, de voz firme, de cabello largo y encendido, quien nos presenta a otro excombatiente al que llaman ‘Amistad’, un apodo que ganó durante su tiempo en la guerrilla.

      - Amistades, ¿cómo están? Les voy a mostrar el lugar, lo que necesiten cuenten conmigo, sigan por aquí, aquí hay cinco camas, cada una con su respectiva cobija, ahora si me siguen les mostraré los baños…

 ‘Amistad’ va a todos lados seguido de un pastor alemán, pese a su gran tamaño es dócil, pero se mantiene alerta; es su compañera afirma el excombatiente mientras se rasca su cabellera oculta bajo una gorra. Nuestro anfitrión nos da un corto recorrido por el espacio y nos lleva de regreso a nuestra habitación, "siéntanse como en su casa", dice antes de despedirse.

El trayecto fue largo y el cuerpo debe descansar, pronto las luces se apagan y solo se escucha el incesante ruido de los insectos de la noche y el chasquido de las lagartijas que aún permanecen despiertas.

Al día siguiente la jornada comienza a las 6:00 am, al salir de la habitación, el paisaje revela una imponente postal de las montañas del Caquetá que comienzan a dibujarse. A lo lejos pueden escucharse bandadas de pájaros a medida que se disipa la niebla; por lo que nos contaron más tarde, en aquel mismo lugar, al atardecer, puede verse a tucanes y guacamayas emprenden su camino, de regreso a sus hogares.

<figure>
![Miravalle, Caquetá](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0246.jpg){width="2976" height="1984" sizes="(max-width: 2976px) 100vw, 2976px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0246.jpg 2976w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0246-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0246-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0246-1024x683.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0246-370x247.jpg 370w"}  

<figcaption>
</figcaption>
</figure>
![Miravalle ETCR](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0306-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0306-1024x683.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0306-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0306-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0306-370x247.jpg 370w"}  
![Miravalle ETCR](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0318-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0318-1024x683.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0318-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0318-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0318-370x247.jpg 370w"}

Son 13 las puertas de los baños que nos fueron asignados, algunas son duchas, otros son inodoros e incluso una esconde una lavadora y aunque el acueducto llega hasta arriba, son pocos los lavamanos que funcionan o las cisternas que descargan. Finalmente en uno de los cubículos, el chorro  de la regadera cae con fuerza mientras el agua fría despeja cualquier sensación de sueño que aún quede en el cuerpo.

El torneo debe dar inicio a las 8:00 am por lo que una camioneta aguarda metros abajo junto a la biblioteca para llevarnos al lugar donde los deportistas se preparan. Al igual que en muchos de los espacios territoriales, en Miravalle existen varios murales que decoran las casas y hacen alusión a fundadores y altos mandos de las FARC, sin embargo en este lugar en particular, orgullosos de convertir el rafting en su actividad insignia, hay varias fotografías que hacen referencia al deporte y a Caguán Expeditions, pegadas en la pared de lo que parecer ser un comedor comunal.

Caguán Expeditions es uno de los frutos de la reincorporación, una sociedad que nació en 2017 de la unión de excombatientes y emprendedores que funciona como un operador turístico en San Vicente del Caguán promoviendo la región por medio del rafting.

<figure>
![Miravalle Rafting](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0337.jpg "Miravalle Rafting")  

<figcaption>
Fotografía: Contagio Radio

</figcaption>
</figure>
En la parte de abajo ya hay gran actividad, los participantes del torneo están congregados en la cooperativa, departiendo y comiendo el desayuno; hay huevos, fruta y avena para los atletas que en pocos minutos comenzarán a navegar por las aguas de El Pato.

Los equipos emocionados suben los botes al techo de las camionetas, los amarran con fuerza y uno a uno se cuelgan de la parte trasera del vehículo que los llevará al punto de salida de la primera prueba.

La primera competencia es una categoría denominada Sprint, una bajada corta y rápida de menos de 700 metros de distancia efectuada en menos de tres minutos la cual mide el tiempo individual de cada equipo. Allá van, trajes de neopreno, chalecos salvavidas, remos al hombro y casco, uno a uno los equipos se suben a las balsas neumáticas y  descienden mientras periodistas, habitantes de Miravalle y fotógrafos se deleitan con la fuerza del río.

La idea del rafting en El Pato siempre le ha sido atribuida a Hernán Darío Velásquez, alias 'El Paisa' quien pese a volver a las armas, dejó un legado muy importante en el ETCR en el que habitaba al impulsar lo que con el tiempo se convertiría en la actividad por excelencia de este espacio territorial. Según relatan un amigo del jefe guerrillero llegó desde Florencia y juntos comenzar a bajar por el río en bote hasta que poco a poco un grupo fue conformándose alrededor del deporte.

Uno de aquellos pioneros es Duberney Moreno quien se encuentra alistándose para las pruebas, el excombatiente es originario del Caquetá y llegó a las filas de la columna Teófilo Forero de las  FARC en el 2004, sí, Duberney vivió los días de la guerra, sin embargo hoy cobra más importancia narrar su vida después del Acuerdo.

![Duberney Rafting](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0357.jpg "Duberney Rafting")

<figure>
[![Campeonato de Rafting](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/IMG_0610.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/IMG_0610.jpg)

</figure>
<figure>
[![Remando por la Paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/IMG_0460.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/IMG_0460.jpg)

</figure>
<figure>
[![Remando por la Paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0363.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/MG_0363.jpg)

</figure>
Duberney fue uno de los primeros en subirse a la balsa y a comenzar a practicar el rafting en El Pato, al principio únicamente tomaba fotos y grababa videos de la experiencia, más tarde vino el aprendizaje que hoy, además de permitirle conocer el cauce, el caudal, el curso y los caprichos del río, lo ha convertido en el capitán de Remando por la Paz.

Es fácil percibir su emoción cuando se le ve subirse al bote y manejarlo con destreza junto a sus compañeros. Al lado de Duberney también se encuentra Hermides Linares, un excombatiente de 43 años quien también hizo parte casi 30 años de las filas de la entonces guerrilla y que ahora está mucho más inmerso en la cuestión logística del ETCR y del campeonato pues fue el encargado de viajar a San Gil, Bogotá, Florencia y a Medellín, efectuando reunión tras reunión, para poder materializar el campeonato

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Río-Pato-1-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Río-Pato-1-1024x683.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Río-Pato-1-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Río-Pato-1-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Río-Pato-1-370x247.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Río-Pato-1.jpg 1191w"}  
![Río Pato](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Río-Pato-2-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Río-Pato-2-1024x683.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Río-Pato-2-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Río-Pato-2-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Río-Pato-2-370x247.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Río-Pato-2.jpg 1042w"}

El Pato, es un afluente que históricamente simbolizó la resistencia guerrillera en la región, pues además de haber sido la barrera entre el Ejército y las FARC, fue uno de los lugares en los que entre 1960 y 1964, campesinos resistentes a la violencia partidista, como Pedro Antonio Marín, mejor conocido como Manuel Marulanda Vélez, alias ‘Tirofijo’ se refugiaron y crearon sus organizaciones de autodefensa campesina al no acogerse a las amnistías que les ofreció el gobierno militar de Rojas Pinilla.

El Pato, el mismo que se alimenta de las corrientes del río Balsillas y la quebrada Malabrigo formando un cañón de más de 3 km de longitud y que fue testigo de la historia del conflicto en Colombia hoy es el protagonista de un nuevo capítulo de la reconciliación.

Según me explicó Frellin, uno de los protagonistas de este relato y a quien conoceremos más adelante, el Pato es un río complejo, perfecto para el rafting pero a su vez complicado pues a medida que la lluvia cae y el agua sube, el riesgo de que por sus aguas se deslicen palizada y piedras, aumenta; y aunque en la actualidad no cuentan con un medidor de nivel para conocer la profundidad del afluente cuando llueve, una marca en la orilla del río les permite saber a los remadores qué tan apropiado es navegar.

Para atravesar a alguno de los dos costados del río, es necesario cruzar un puente que tan solo permite tener tres personas sobre su base. Tanto los costados como la parte de abajo del río, ahora son custodiadas por integrantes del Ejército Nacional quienes brindan seguridad al espacio territorial y han visto la forma en que el deporte se ha convertido en el día de a día de quienes navegan en las aguas de El Pato, así lo revela el mayor Martínez, del Batallón de Operaciones Terrestres N.° 70, uno de los uniformados presentes en medio de las pruebas, quien señala que ha aprendido de los excombatientes lo que le ha cambiado su forma de pensar hacia ellos.

> ***“La paz debe estar por encima de todos los proyectos que tenga Colombia”*** 
> ------------------------------------------------------------------------------

<figure>
![ETCR Rodolfo Rodríguez](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/ETCR-Rodolfo-Rodríguez-1024x542.jpg){width="1024" height="542" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/ETCR-Rodolfo-Rodríguez-1024x542.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/ETCR-Rodolfo-Rodríguez-300x159.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/ETCR-Rodolfo-Rodríguez-768x406.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/ETCR-Rodolfo-Rodríguez-370x196.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/ETCR-Rodolfo-Rodríguez.jpg 1295w"}  

<figcaption>
Rodolfo Rodríguez, líder del ETCR de Miravalle  
Foto: Contagio Radio

</figcaption>
</figure>
Una de esas historias de reconciliación es precisamente la que llega con el equipo Unidos por la Paz, conformado por patrulleros de la Policía Nacional y excombatientes, así lo destaca Rodolfo Rodríguez, parte de las directivas del Espacio Territorial de Miravalle y quien ha asumido el liderazgo tras la salida de ‘El Paisa’ de la región.

“El pueblo ve que nosotros ya nos dimos la mano, duramos 52 años en guerra, pero siempre el policía que moría era el hijo del campesino, el soldado que moría era hijo del campesino y el guerrillero que moría era hijo del campesino, queremos seguir unidos, pero luchando contra los de arriba”, afirma.

Rodolfo es un hombre que ronda los 50 años, de piel trigueña, figura robusta y caminar decidido, un fino bigote cruza las comisuras de su boca y sus ojos achinados miran de frente al hablar. Cuando no está en su casa se le ve caminando con un radio portátil que le permiten estar al tanto de todo lo que sucede en Miravalle.

<iframe id="audio_45381373" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45381373_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###  ***“Que la paz unilateral no nos cueste la vida a nosotros y si nos matan por hacer esto que nos digan qué debemos hacer y lo hacemos”.*** 

![Proyectos Productivos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Proyectos-Productivos-3-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Proyectos-Productivos-3-1024x683.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Proyectos-Productivos-3-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Proyectos-Productivos-3-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Proyectos-Productivos-3-370x247.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Proyectos-Productivos-3.jpg 1488w"}

> ### "A mí a ratos me da alegría, pero de la misma alegría me da tristeza, porque dicen que Miravalle es el espacio que va más adelante y digo, si nosotros somos los que vamos adelante, cómo será los que van atrás..." 

Rodríguez se emociona al hablar del campeonato y afirma que es una sensación indescriptible pues no solo se trata de Miravalle, con el campeonato adquiere visibilidad San Vicente del Caguán, la región del Caquetá y por supuesto están destacando el compromiso de los excombatientes.

“Oíga, es que nosotros apenas comenzando a caminar en esta reincorporación y lo que ha sido este deporte ha traspasado fronteras, es supremamente importante”, exclama el líder de la comunidad quien aclara que, pese a este logro, para poder hablar de reintegración debe existir un sostenimiento político, social y económico en la comunidad, garantías que no han visto del todo en la realidad.

Según el informe 'La reincorporación económica de los excombatientes de las FARC' de la Fundación Ideas para la Paz publicado en julio de 2019, para 2017 habían sido aprobados 24 proyectos productivos colectivos y cerca de 160 individuales, mientras que 1.242  han podido acceder a un proyecto. En adición, “según la Agencia Colombiana para la Reintegración, para abril de este año, 701 excombatientes se encontraban trabajando en la Unidad Nacional de Protección (UNP) y 29 más en la misma ARN”.

Otros datos aportados por Ubaldo Zúñiga, presidente de Economías Sociales del Común – ECÓMUN, cooperativa nacional establecida por la Fuerza Alternativa Revolucionaria del Común, indican que los excombatientes han presentado 13.000 proyectos productivos de los que solo 500 han sido aprobados.

“A mí a ratos me da alegría, pero de la misma alegría me da tristeza, porque dicen que Miravalle es el espacio que va más adelante y digo, si nosotros somos los que vamos adelante, cómo será los que van atrás”, expresa Rodríguez al explicar que, aunque existen varias iniciativas en el espacio territorial, estas no han podido despegar en su totalidad.

Rodolfo destaca además que ya no se trata únicamente del porvenir de los reincorporados, “estamos hablamos de familias, aquí hay un promedio de 111 habitantes ya con familias de reincorporados”, una cifra que contrasta con la de quienes viven en otros ETCR como en Tierra Grata en Manaure, La Guajira donde viven más de 160 personas.

Casi al finalizar la entrevista un niño de cerca de dos años se acerca a don Rodolfo, “él nació aquí, él es uno de los hijos de esta paz y por ellos trabajamos y si alguien me mata, que sepa que hay Rodolfo para rato” - dice alzando a su pequeño - “que la paz unilateral no nos cueste la vida a nosotros y si nos matan por hacer esto que nos digan qué debemos hacer y lo hacemos”.

![Remando por la Paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Remando-550x385.jpg){width="550" height="385" sizes="(max-width: 550px) 100vw, 550px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Remando-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Remando-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Remando-770x540.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Remando-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Remando-216x152.jpg 216w"}  
![Aventura Total](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Aventura-Total-550x385.jpg){width="550" height="385" sizes="(max-width: 550px) 100vw, 550px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Aventura-Total-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Aventura-Total-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Aventura-Total-770x540.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Aventura-Total-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Aventura-Total-216x152.jpg 216w"}

Como bien señala Rodolfo, al interior del ETCR existen varias iniciativas, Adriana Villa, también responsable de temas como salud, mujer y memoria histórica se refiere a cómo han sido respaldados por diferentes organizaciones entre ellas, el Fondo Europeo para la Paz de la Unión Europea con el proyecto Amazonía Joven, implementado junto a organizaciones como Corpomanigua, Acción Cultural Popular - ACPO, y la Diócesis de San Vicente del Caguán.

Tal proyecto busca impulsar un alojamiento bioclimático con el fin de aprovechar los recursos y demostrar cómo se puede vivir con la naturaleza y estar en armonía con el territorio a partir de negocios verdes sostenibles que permitan la conservación de la Amazonía y frenen la deforestación y la expansión agrícola y ganadera.

Además, se busca trabajar con nuevas generaciones a través de la cultura, la historia y la apropiación del territorio. “es fundamental porque la mayoría de personas y muchachos que ingresan a la guerra es porque no tienen algo por lo qué luchar, es importante tener un futuro que recoja una esperanza y algo por lo  qué trabajar para ilustrarme junto a mi familia”, apunta Adriana.

> ### ***"Aquí debe haber una política que incline a todos hacia el deporte"*** 

Es irónico que el rafting haya sido el deporte que ha unido a diferentes sectores de la población, aunque sus orígenes datan de 1842 en el río Platte en Estados Unidos, solo fue hasta la década de los años 50 en medio de la Guerra Fría cuando el estudiante Jib Ellison, como parte de la tesis de su carrera de ciencias políticas, montó en un mismo bote a rusos y estadounidenses para que remaran juntos pese a sus diferencias ideológicas.

De este hecho histórico surgió el proyecto RAFT: Russians and Americans for Teamwork por sus siglas en inglés y traducido como Rusos y Estadounidenses por el Trabajo en Equipo y que dio nacimiento a la Federación Internacional de Rafting (IRF), que hoy congrega a 62 naciones.

Camila Labbat, estadounidense de nacimiento y quien viene junto a uno de los equipos de Costa Rica destaca que lo más grande de este evento es la habilidad de la gente de unirse a pesar de venir de diferentes contextos, refiriéndose a policías, excombatientes y militares que ahora comparten tiempo juntos, algo sorprendente de ver.

“Esta es una gran oportunidad para conocer un lugar en el que no había estado antes, es muy diferente, creo que el hecho de que estas personas hayan estado en guerra por más de 50 años, conocerlos, llegar a hablar con ellos y escuchar sus historias te abre los ojos, con suerte van a dejar esa tristeza en el pasado y será el inicio para el turismo del futuro”, afirma.

Por su parte, Humberto ‘Beto’ López, en representación del equipo Team Río Ponce Rafting proveniente de Santander, afirma que el deporte es una de las mejores estrategias para alejar de la violencia, “uno tiene la mente y el cuerpo ocupado y no da cabida a otras cosas diferentes, quienes ingresaron a los grupos armados es quizá porque no tenían una oportunidad, teniendo el deporte sería diferente".

Esta tesis también la respalda don Rodolfo que asegura que “el deporte es algo que debe ayudar a la juventud, no tanto a nosotros los viejos, gran parte de lo que engruesa las filas de la delincuencia es la juventud, pero si se apoya el deporte en los municipios la juventud va a encontrar las formas”.

![San Gil](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/San-Gil-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/San-Gil-1024x683.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/San-Gil-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/San-Gil-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/San-Gil-370x247.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/San-Gil.jpg 1488w"}

> ### ***"Allá donde lleguemos y haya un río, habrá una unión y seremos todos"*** 

San Gil, lugar de donde proviene ‘Beto’ quien ha practicado por más de 20 años el rafting, es considerado por muchos como la capital nacional del turismo de aventura y el deporte extremo, bien sea por su diversidad paisajística o su riqueza hídrica y natural como la del río Fonce.

Cabe resaltar que la experiencia adquirida en las corrientes del río Fonce fue lo que permitió que Master Team San Gil y Colombia Rafting de Santander fueran los ganadores del torneo en sus respectivas categorías y que el equipo de ‘Beto’ se ubicara en el tercer lugar de la competencia.

La llegada del rafting a Miravalle no solo abre las puertas al deporte, también lo hace para otras expresiones culturales como el cine. Aquellas dos noches, las comunidades aledañas a Miravalle pudieron disfrutar, algunos por primera vez, de ver una función de cine, un privilegio del que carece el 95% del país y con el que la firma del Acuerdo poco a poco puede abrirse camino. Estos frutos de la paz, los resalta Ximena Narváez, excombatiente quien estuvo en las FARC durante más de 30 años y que es presidenta de la Junta de Acción Comunal del ETCR de Agua Bonita en La Montañita.

“Nuestro compromiso es el de trabajar junto a las comunidades, seguir luchando por la implementación de acuerdo que no es solo para los excombatientes, sino para los colombianos, nos la hemos jugado a seguir apostándole a hacer contactos con instituciones y apostarle a la paz”, afirma la lideresa.

![Aventura Total](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Aventura-Total-2-1024x588.jpg){width="1024" height="588" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Aventura-Total-2-1024x588.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Aventura-Total-2-300x172.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Aventura-Total-2-768x441.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Aventura-Total-2-370x213.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Aventura-Total-2.jpg 1184w"}

La humedad de la cuenca amazónica permite que en el Caquetá exista un clima lluvioso a lo largo de todo el año, en medio de la segunda prueba denominada head to head - en la que a través de una distancia corta, dos equipos se enfrentan en un carrera - una fuerte lluvia que emula los fuertes monzones del Vietnam de 'Apocalypse Now', de Francis Ford Coppola comienza a caer sobre la montaña, obligando a los deportistas y asistentes del evento a correr buscando refugio en la cooperativa ubicada a quizá un kilómetro de distancia del lugar de la competencia.

Aquella tarde, Aventura Total, el único equipo femenino del torneo se ubico en una privilegiada posición, y aseguró su pase al mundial de China, es un equipo conformado por cinco mujeres de Caquetá y Santander: Diana Rojas, Camila Álvarez, Leidi Lorena Leyva, Jenny Galvis y Jenny Bayona.

“Somos el ejemplo de inclusión de género, no hay límite, no hay género solo empoderamiento”, afirma Diana. Por su parte, Leidi agrega que “el deporte nunca lo habíamos visto en la región y participar en este es un gran orgullo para mí y para mi comunidad”.

Con Leidi pasa algo en particular y es que las entrevistas, según me cuenta, son su dolor de cabeza, a pesar que tiene numerosas ideas por expresar, es difícil ponerlas en palabras, así que se siente mucho más cómoda cuando los micrófonos y cámaras están apagados.

Aunque en la competencia Leidi compite con Aventura Total, ella hace parte del equipo de Remando por la Paz y fue una de las campesinas que se unió a los excombatientes de Miravalle para explorar el mundo del rafting. Leidi es lo que coloquialmente en Colombia se llamaría una mujer berraca.

Leidi tiene 32 años y es madre soltera de tres mujeres, es de estatura baja, algo que acentúa su cabellera negra que a menudo lleva recogida en una larga trenza, sus cachetes se sonrojan con facilidad, pero a su vez se acentúan al sonreír, lo que sucede a menudo mientras que de su cuello cuelgan una camándula y una especie de tótem fabricado en madera con forma de balsa.

Natal de El Pato, la deportista vive en su finca ubicada en la vereda Los Andes, se levanta antes del amanecer, ordeña a la vaca, alista a sus hijas y viaja en moto durante 45 minutos hasta llegar a Miravalle donde continúa puliendo su técnica y donde pasó de ser una aprendiz a convertirse  en una guía de este deporte, tanto tiempo paso entre los caudales del río a orillas del ETCR que incluso se le otorgó un lugar para quedarse cuando sea necesario.

![Leidi Remando por la Paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Leidi-1-1024x545.jpg){width="1024" height="545" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Leidi-1-1024x545.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Leidi-1-300x160.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Leidi-1-768x409.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Leidi-1-370x197.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Leidi-1.jpg 1348w"}

> ### **Ha sido lo mejor que me hubiera podido pasar en la vida, si yo no estuviera aquí en el rafting estuviera trabajando en mi finca y de ahí no hubiera salido...** 

Leidi rememora la experiencia que vivió junto a sus compañeros durante el mundial de Australia, “fue una experiencia muy bonita, nunca pensé salir de mi país, para mí es un gran orgullo el pasar de ser campesina a ser guía de rafting y que me digan ‘Lore usted ya nos puede entrenar’; no es fácil, pero he aprendido mucho y me siento capacitada” afirma orgullosa.

Para la remadora, navegar por el río implica una serie de sensaciones que involucran desde la adrenalina que se siente el bajar los rápidos hasta la alegría de compartir junto a sus compañeros la oportunidad de sentir la naturaleza al practicar este deporte.

“**Ha sido lo mejor que me hubiera podido pasar en la vida, si yo no estuviera aquí en el rafting estuviera trabajando en mi finca y de ahí no hubiera salido**, p**ero en esto uno se distrae y comparte”** admite Leidi quien, con su testimonio, reafirma ser uno de los ejemplos de paz territorial al convertirse en un puente entre la población rural y los hombres y mujeres que alguna vez portaron un rifle y que hoy le apuestan todo a la paz.

Uno de esos excombatientes y también parte del equipo de ‘Remando por la Paz’ es Frellin Alberto Noreña, más conocido por la comunidad como ‘Pato’, un apodo que obtuvo en Guacamayas, Caquetá, lugar en el que nació y se crió. Después de la muerte prematura de su mamá y fruto de esa pérdida, Frellin se acercó mucho a sus primos a quienes llamaban ‘Los Patos’, tanto, que pasó a ser uno más de ellos. Al formarse el ETCR, uno de sus compañeros, proveniente de la misma región lo reconoció, “¡Hola patico ¿Cómo estás!”, desde entonces, su apodo prevaleció.

Dirían las mamás que más que ‘Pato’ a Frellin le podrían decir *Ringlete*, siempre se le ve de un lado a otro, conversando con uno, charlando con el otro, una capacidad de empatía que él mismo reconoce y que lo distingue dentro de su equipo.

<figure>
![Pato Río Pato](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Pato-Río-Pato-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Pato-Río-Pato-1024x683.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Pato-Río-Pato-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Pato-Río-Pato-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Pato-Río-Pato-370x247.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Pato-Río-Pato.jpg 1191w"}  

<figcaption>
Foto: Contagio Radio

</figcaption>
</figure>
“Tengo un sentido muy humanista, a veces hablo mucho dentro del bote, pero tiendo a mediar cuando hay dificultades, el capitán es Duberney mientras el ‘Negro’, haciendo referencia a su compañero Deiver Buitrago - tiene fuerza como un diablo, yo más bien estoy muy pendiente de las técnicas, de cómo estudiar, así manejamos el equipo, es muy difícil ponerse de acuerdo todos de alguna manera, pero vamos aprendiendo".

Además de su rol como deportista, ‘Pato’ tiene otros pasatiempos y habilidades entre ellos, tallar madera, tocar, hacer guitarras y cuando alcanza el tiempo, cantar, incluso sin saberlo, podría apostar que el tótem que lleva Leidi en su cuello fue una creación de Frellin.

Relata que aprendió a tallar y a tocar música durante el tiempo que fue prisionero por los rigores de la guerra, “estando allí trabajando en un taller había un hombre ya muy mayor que fabricaba guitarras y me enseñó un poco a  tocar y hacerlas,  allá fabriqué mi propia guitarra” relata el deportista quien hace la salvedad que se le da mejor hacerlas que interpretar música, sin embargo, allí mismo en prisión y con la tutoría de un profesor de la Universidad de Antioquia, aprendió al respecto y lo sumó a su conocimiento.

Para ‘Pato’ tanto la música como la libertad tienen un valor incalculable, aún más después de haber pasado ocho años privado de la libertad, “es algo que no tiene precio, no hay dinero ni riqueza que valga el precio de la libertad”, asegura el excombatiente quien recuerda cómo de niño pasaba tanto tiempo junto a sus primos en el río y ahora, como deportista, esta cercanía a la naturaleza cobra un valor agregado.

“Estamos iniciando en el mundo del turismo, comenzamos un sueño, construyendo paz y empresa desde el deporte” afirma ‘Pato’, quien bien sabe que el río fluye y cambia, mientras la vida, ese cauce que continúa, debe seguir un movimiento permanente, un caudal que los ha traído hasta aquí y del que no piensan desistir, y en el que hay que seguir remando

Lo único seguro es que El río Pato y quienes se bañan en él en tiempos de paz, jamás volverán a ser los mismos.

Entradas recientes:
-------------------

[![“Están atacando es para matarnos”: integrante de la Primera Línea sobre el ESMAD](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/79535730_10157155146552832_4608684003791208448_o-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/estan-atacando-es-para-matarnos-integrante-de-la-primera-linea-sobre-el-esmad/)

#### [“Están atacando es para matarnos”: integrante de la Primera Línea sobre el ESMAD](https://archivo.contagioradio.com/estan-atacando-es-para-matarnos-integrante-de-la-primera-linea-sobre-el-esmad/)

Integrantes de la Primera Línea fueron atacados por agentes del ESMAD en medio de manifestaciones pacíficas en la…[Leer más](https://archivo.contagioradio.com/estan-atacando-es-para-matarnos-integrante-de-la-primera-linea-sobre-el-esmad/)  
[![Informe revela la violencia estatal-paramilitar en Viotá, Cundinamarca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Informe-Viotá-370x260.jpeg){width="370" height="260"}](https://archivo.contagioradio.com/informe-revela-la-violencia-estatal-paramilitar-en-viota-cundinamarca/)

#### [Informe revela la violencia estatal-paramilitar en Viotá, Cundinamarca](https://archivo.contagioradio.com/informe-revela-la-violencia-estatal-paramilitar-en-viota-cundinamarca/)

El informe que será entregado a la Comisión de la Verdad recoge información ypatrones de macrocriminalidad sucedidos en…[Leer más](https://archivo.contagioradio.com/informe-revela-la-violencia-estatal-paramilitar-en-viota-cundinamarca/)  
[![Juez ordena desalojar a 36 familias indígenas Inga damnificados por avalancha en Mocoa](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/EK4KBPqXsAAaTUI-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/juez-ordena-desalojar-a-36-familias-indigenas-inga-damnificados-por-avalancha-en-mocoa/)

#### [Juez ordena desalojar a 36 familias indígenas Inga damnificados por avalancha en Mocoa](https://archivo.contagioradio.com/juez-ordena-desalojar-a-36-familias-indigenas-inga-damnificados-por-avalancha-en-mocoa/)

Foto:Marino Peña Este lunes 9 de diciembre finalizan los 5 días de plazo, para el desalojo de 36…[Leer más](https://archivo.contagioradio.com/juez-ordena-desalojar-a-36-familias-indigenas-inga-damnificados-por-avalancha-en-mocoa/)  
[![Corte Penal Internacional tiene información sobre 2.047civiles y empresarios vinculados al paramilitarismo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Cour-Penale-Internacionale-e1575589878337-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/mas-de-2-mil-casos-de-falsos-positivos-bajo-la-lupa-de-la-corte-penal-internacional/)

#### [Corte Penal Internacional tiene información sobre 2.047civiles y empresarios vinculados al paramilitarismo](https://archivo.contagioradio.com/mas-de-2-mil-casos-de-falsos-positivos-bajo-la-lupa-de-la-corte-penal-internacional/)

En su preinforme, la Corte Penal Internacional documentó la información que maneja sobre más de 10 mil personas…[Leer más](https://archivo.contagioradio.com/mas-de-2-mil-casos-de-falsos-positivos-bajo-la-lupa-de-la-corte-penal-internacional/)
