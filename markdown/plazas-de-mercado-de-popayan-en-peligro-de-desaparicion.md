Title: Plazas de mercado de Popayán en peligro de desaparición
Date: 2015-05-22 21:54
Category: Economía, Nacional
Tags: Asociación de Trabajadores y Comerciantes, Barrio Bolívar, Comerciantes, Martín Chicangana, Plazas de Mercado, Popayán
Slug: plazas-de-mercado-de-popayan-en-peligro-de-desaparicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: periferiapopayan.wordpress.com 

###### <iframe src="http://www.ivoox.com/player_ek_4536346_2_1.html?data=lZqgmJiYeo6ZmKiak5WJd6KnmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLm1c7bjajMrcTVz8zO0MaPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Entrevista Martín Chicangana] 

##### [22 May 2015]

Los comerciantes de la **plaza de mercado del barrio Bolivar de Popayán** están enfrentando una problemática con el gobierno local que propone que las plazas de mercado sean llevadas fuera de la periferia.

De acuerdo a Martín Chicangana, presidente de la Asociación de trabajadores y comerciantes de la plaza de mercado, la necesidad de los trabajadores es que se construya una plaza aproximadamente para 500 comerciantes en la que esté el mercado campesino y una central mayorista que requiere la ciudad de Popayán.

Martín asegura que los comerciantes de las plazas de mercado le aportan a Popayán de 300 a 400 millones mensuales que no son invertidos en las plazas,  por lo que se exige una reinversión social para que se construya una plaza que sea un lugar digno de trabajo.

La Asociación de Trabajadores y Comerciantes ha acudido a la acción de tutela para evitar la reubicación que les exige el gobierno, adicional a esta problemática, está la externa, con las empresas multinacionales que se ubican en los alrededores y dejan sin trabajo a los campesinos y comerciantes que los surten.

Las plazas de mercado en Popayán fueron creadas hacia 1968 aproximadamente, el comercio se evidenciaba en varios escenarios, la principal plaza de mercado fue la del barrio Bolívar que se ubicó frente a la estación urbana de Popayán, en la plaza central, y finalmente se repartieron en 5 plazas.
