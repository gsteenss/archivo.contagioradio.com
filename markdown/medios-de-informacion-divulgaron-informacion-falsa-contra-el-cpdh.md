Title: Medios masivos divulgaron información falsa contra el CPDH
Date: 2017-06-22 13:07
Category: DDHH, Nacional
Tags: cpdh, Derechos Humanos, medios de comunicación
Slug: medios-de-informacion-divulgaron-informacion-falsa-contra-el-cpdh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/CPDH-MENTIRAS-MEDIOS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [22 Jun 2017]

El Comité Permanente por la Defensa de los Derechos Humanos CPDH **manifestó su rechazo ante las acusaciones que dicen que, se encontró un cargamento de cocaína en un vehículo asignado al Comité** por la Unidad Nacional de Protección. Según el Comité “estas afirmaciones nos han puesto en riesgo y la información es falsa”.

En días pasados, la Policía incautó 73 kilos de base de cocaína que se encontraban en las llantas de un vehículo adscrito a la Unidad Nacional de Protección. Ante esto, algunos medios de información como **Noticias Caracol, El Espectador, RCN, Blu Radio y Somos Caquetá.com** establecieron que “el vehículo estaba asignado a Arlex Gómez Lopez, miembro del comité de Derechos Humanos de Caquetá”.

Para Gelacio Cardona, miembro directivo del Comité para la Defensa de los Derechos Humanos CPDH, **“el vehículo está adjudicado a una persona particular que no tiene nada que ver con el Comité”.** Según Cardona, “la vinculación con el Comité fue arbitraria y vamos a pedir la rectificación a los medios de información que dieron la información falsa.” (Le puede interesar: ["Defensores de DDHH fueron hostigados y perseguidos en Bogotá"](https://archivo.contagioradio.com/defensores-ddhh-asedio-bogota/))

En un comunicado, solicitaron que “se haga la respectiva retractación de la información divulgada y suministrada por los medios de comunicación el pasado 19 de junio, que hace **referencia a un vehículo de la UNP incautado en carretera del departamento del Huila** y que, se afirmó, habría sido asignado al Comité Permanente por la Defensa de los Derechos Humanos”.

Además, manifestaron su rechazo a **“los juicios paralelos que desde los medios de comunicación se emiten, sin tener el mínimo respeto al rigor periodístico**”. Según el CPDH, estas afirmaciones “ayudan a incrementar la estigmatización a la labor y transparencia de los defensores y defensoras que a diario ponen en riesgo sus vidas en defensa de los Colombianos”.

Para el Comité **es infortunado que se hagan este tipo de afirmaciones que perjudican el actuar de la CPDH.** “Nosotros tenemos una ética profunda y se las exigimos a nuestros funcionarios. Sabemos que la UNP sabe que ese vehículo no nos fue asignado y nos reuniremos con ellos”.

<iframe id="audio_19417699" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19417699_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
