Title: Jóvenes aportan a propuesta de servicio social para la paz
Date: 2015-09-24 11:46
Category: DDHH, Nacional
Tags: audiencia pública, Cámara de Representantes, Comisión de Paz, Congreso de la República, construcción de paz
Slug: jovenes-aportan-a-propuesta-de-servicio-social-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/batidas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  Foto: diarioadn.co 

###### [24 sept 2015 ]

[Este jueves desde las 8 de la mañana y hasta la 1 de la tarde, en el Centro de Memoria Paz y Reconciliación, la Comisión de Paz del Congreso de la República y la Cámara de Representantes adelantan una **audiencia pública sobre el proyecto de ley que pretende la creación del servicio social para la paz**. ]

[La iniciativa consta de dos artículos que consolidan en un solo marco jurídico, la propuesta de **un servicio social que les permita a los jóvenes optar por alternativas distintas al servicio militar** para "defender la independencia nacional y las instituciones públicas" cómo lo determina el **artículo 216 de la Constitución Política, que este proyecto de ley busca modificar**.   ]

[De constituirse en Ley esta inciativa, los **jóvenes podrán ser partícipes en la construcción de paz por espacio de 12 meses**, prestando servicio social en entidades gubernamentales civiles, organizaciones sociales y comunitarias, del orden nacional y regional, en coordinación con entidades estatales.]

[Este evento que también es promovido por el Proceso Distrital de Objeción de Conciencia y diversas Organizaciones Juveniles, tiene como propósito que la Comisión de Paz escuche las opiniones de los jóvenes sobre la propuesta, para **reunir información que sirva como fuente en el  trámite legislativo en el Congreso**.]
