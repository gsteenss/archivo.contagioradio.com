Title: Lucille Dupin  en Sonidos Urbanos
Date: 2016-07-01 14:55
Category: Sonidos Urbanos
Tags: Cultura, Música, nuevas bandas
Slug: lucille-dupin-en-sonidos-urbanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/lucille-dupin-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Lucille Dupin 

Con sonidos vibrantes y envolventes, **Lucille Dupin,** **cantante y compositora bogotana, **nos acompañó en los estudios de Contagio Radio.** **Su calma y sutileza, expresadas con la conjugación de frases y melodías de amor, libertad, y melancolía, caracterizaron esta cuarta sesión de Sonidos Urbanos.

Su voz y Chopin, su Ukelele compañero fiel de viajes, aventuras, y tránsitos hacia la resignificación del amor, hacia el convencimiento de amar en libertad, son los dos elementos para entonar cantos inspirados en episodios de su vida, acompañados de su fuerza ejercida desde la ternura y la serenidad. Sus pasiones, amores, desamores, encuentros y desencuentros, hallados en su acercamiento a la música, han sido los motores para afianzar su carrera como solista.

Además de deleitarnos con canciones en vivo como Jeux D’ Enfants, la primicia de 227, y el cover de Nirvana *Smells Like Teen Spirit,* la joven artista nos compartió historias de calle, su postura frente a la escena musical en Colombia, y la proveniencia de su seudónimo *Lucille Dupin*; un símbolo de liberación a la mujer y reivindicación a sus derechos, que en los años 1800 sería también el nombre artístico la escritora francesa e icono de la emancipación, George Sand.

**En Redes sociales:**

Facebook: [Lucille Dupin](https://www.facebook.com/LucilleDupinMusic)

Twitter: [@[LucilleDupin]{.u-linkComplex-target}](https://twitter.com/LucilleDupin){.ProfileHeaderCard-screennameLink .u-linkComplex .js-nav}

<iframe id="audio_12093474" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12093474_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
