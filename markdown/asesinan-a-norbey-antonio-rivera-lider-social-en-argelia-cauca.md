Title: Asesinan a Norbey Antonio Rivera, líder social en Argelia Cauca
Date: 2020-12-30 16:22
Author: CtgAdm
Category: Actualidad, Nacional
Slug: asesinan-a-norbey-antonio-rivera-lider-social-en-argelia-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-1-3.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Un nuevo asesinato enluta al país. **Norbey Antonio Rivera, líder social en Argelia, Cauca, fue asesinado en Popayán** en el sector conocido como la Ladera. Rivera hizo parte de la Asociación Campesina en Argelia y además era el compañero sentimental de Nancy Santacruz, Concejal amenazada en este municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un recrudecimiento del conflicto armado, se ha venido suscitando en Argelia, Cauca, donde el pasado mes de noviembre se presentaron dos masacres. La presencia de actores armados como la guerrilla del ELN, los grupos que se hacen llamar disidencias de las FARC, grupos que no se acogieron al acuerdo de paz y diversas expresiones armadas ligadas al narcotráfico hacen del departamento del Cauca el más convulso en materia de violencias contra la población civil.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En días pasados, la Comisión para el esclarecimiento de la Verdad, señaló

<!-- /wp:paragraph -->

<!-- wp:quote -->

> &lt;&lt;***Rechazamos la violencia generalizada que se presenta en la zona norte del departamento del Cauca que cada vez se agudiza más, afectando directamente a las comunidades indígenas, campesinas y afrocolombianas de esa zona del país.&gt;&gt;***
>
> <cite>Comisión para el esclarecimiento de la Verdad</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Este dado que, según la comisión, el 5 y 6 de diciembre, en este territorio se presentó una masacre, donde hubo 4 asesinatos, dos homicidios de comuneros, entre ellos, el hijo de una lideresa indígena, una amenaza individual a coordinador de la Guardia Indígena y una amenaza colectiva a la Asociación de Cabildos del Norte del Cauca ACIN

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar leer: [Se registran dos masacres en menos de 12 horas en Cauca y Antioquia](https://archivo.contagioradio.com/se-registran-dos-masacres-en-menos-de-12-horas-en-cauca-y-antioquia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La muerte de Norbey se suma a los 375 asesinatos a líderes sociales suscitados en lo que va corrido del año, según [Indepaz](https://twitter.com/Indepaz). La violencia no cesa y los defensores de DD.HH. hacen un llamado urgente al gobierno del presidente Iván Duque, para que se pronuncié al respecto, haga cumplir los acuerdos de paz y les brinde garantías de seguridad a los líderes y lideresas sociales.

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/JuventudAntioq1/status/1344040514040033282","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JuventudAntioq1/status/1344040514040033282

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

Le puede interesar leer: [Crece a tres la cifra de personas asesinadas en ataque armado en Argelia, Cauca](https://archivo.contagioradio.com/dos-personas-asesinadas-y-tres-heridas-tras-ataque-armado-en-argelia-cauca/)

<!-- /wp:paragraph -->
