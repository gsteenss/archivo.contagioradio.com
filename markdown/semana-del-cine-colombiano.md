Title: Más de 60 películas componen la Semana del Cine Colombiano
Date: 2018-07-26 18:22
Author: AdminContagio
Category: 24 Cuadros
Slug: semana-del-cine-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/semana-cine.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El cine que somos 

###### 26 Jul 2018 

Del 26 de julio hasta el 1 de Agosto, inicia la Semana del cine colombiano, creada hace 14 años por el Ministerio de cultura con el objetivo de llevar algunas de las películas que se producen en el país, de diferentes géneros cinematográficos, a todos los rincones de la geografía nacional.

Serán 215 municipios de los 32 Departamentos, donde se presentarán más de 50 producciones estrenadas durante los dos últimos años, incluyendo 20 filmes que serán proyectados en 20 centros carcelarios del país y en las 117 sedes con las que cuenta el Servicio Nacional de Aprendizaje SENA.

La cinta encargada de abrir la Semana del Cine Colombiano es "Jericó, el infinito vuelo de los días", estrenada en el 2016, bajo la dirección de Catalina Mesa, y producida por Savia Dulce. Durante la semana, el público podrá escoger su película favorita que se exhibirá el día de la clausura el próximo primero de agosto.

Adicionalmente, en esta séptima edición los espectadores de disfrutar 7 producciones nacionales a través de la plataforma digital Retina Latina: Ciudad sin sombra de Bernardo Francisco Cañizares, Eso que llaman amor de Carlos César Arbeláez, Destinos de Alexander Giraldo, Jericó, el infinito vuelo de los días de Catalina Mesa, No todo es vigilia de Hermes Paralluelo, Sin mover los labios de Carlos Fernando Osuna y Una mujer de Daniel Paeres y Camilo Medina.

El evento contará con 129 aliados regionales, entre universidades, consejos departamentales y distritales de cinematografía; alcaldías, gobernaciones, casas de la cultura, bibliotecas, institutos culturales, colectivos, teatros, festivales de cine, y cajas de compensación familiar.

Para conocer la programación completa y las salas en su ciudad ingrese [Aquí](http://elcinequesomos.com/)

###### Encuentre más información sobre Cine en 24 Cuadros y los sábados a partir de las 11 a.m. en Contagio Radio 
