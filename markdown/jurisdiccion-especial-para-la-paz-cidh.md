Title: ¿Por qué la Jurisdicción Especial para la Paz irá a la CIDH?
Date: 2019-05-06 16:03
Author: CtgAdm
Category: Paz, Política
Tags: audiencia, CIDH, JEP, paz
Slug: jurisdiccion-especial-para-la-paz-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Diseño-sin-título-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El próximo jueves 9 de mayo se adelantará una audiencia privada en la Comisión Interamericana de Derechos Humanos (CIDH), en la que representantes de la Jurisdicción Epecial para la Paz (JEP) hablarán sobre los retos de este tribunal, así como la implementación de los acuerdos de paz. Para Maria Camila Moreno, directora del Centro Internacional para la Justicia Transicional, ICTJ, el encuentro es una oportunidad para que el Estado colombiano ratifique su apoyo a la Jurisdicción y su compromiso con las víctimas.

Moreno afirmó que el encuentro con la CIDH "es muy importante", tomando en cuenta que hay un convenio entre la Comisión y la JEP; y uno de los mandatos de este organismo internacional es apoyar a los Estados para el cumplimiento de sus obligaciones en materia de derechos humanos. En ese sentido, **la audiencia funciona como una forma de hacer seguimiento al proceso de implementación de la paz** en Colombia. (Le puede interesar: ["Denuncian fuerte arremetida de Gobierno para que senadores aprueben objeciones a la JEP"](https://archivo.contagioradio.com/denuncian-gobierno-objeciones-jep/))

Adicionalmente, la directora del ICTJ recordó que la Comisión ha expresado su interés en hacer seguimiento del avance de los procesos sobre hechos ocurridos en el marco del conflicto y que estén en la Jurisdicción; por lo tanto, resulta trascendente el encuentro, tomando en cuenta las más recientes decisiones que ha tomado el Congreso sobre la distribuciòn de los recursos que tiene la JEP y las objeciones presidenciales a su Ley Estatutaria.

### **El Plan Nacional de Desarrollo y los recursos de la Jurisdicción**

A las múltiples denuncias sobre la aprobación del Plan Nacional de Desarrolló se añadió una sobre **los recursos que entrega a la JEP**; se trata del artículo 148, incluido por el representante a la cámara Édgar Gómez, con el que se dividen en dos los recursos que entran para la Jurisdicción: **Un presupuesto que será manejado por la secretaría ejecutiva del Organismo, y otro por la Unidad de Investigación y Acusación (UIA) del mismo.**

**Según Moreno, esta división de presupuestos tiene consecuencias graves, pues la Jurisdicción "es un sistema en sí mismo, y debe garantizar una coherencia interna** y una administración de los recursos que sea coordinada y coherente"; es decir, que el órgano de Gobierno de esta institución debería estar encargado de distribuir los recursos para toda la organización. Esto se garantizaba con un presupuesto unificado que se distribuyera de acuerdo a las necesidades de las diferentes instancias.

En su lugar, ahora la Jurisdicción tendrá dos presupuestos manejados de forma diferente; al tiempo que el director de la UIA (quien hizo el trámite para que se incluyera este artículo en el PND) tendrá la potestad para nombrar o remover personal en cada área de trabajo, y variar los integrantes de cada dependencia según lo considere. (Le puede interesar: ["Así fue aprobado el Plan Nacional de Desarrollo en Senado"](https://archivo.contagioradio.com/aprobado-plan-nacional-de-desarrollo-senado/))

### **Objeciones de Duque a la JEP ya no están en un limbo jurídico** 

La semana pasada el Senado discutió sobre las objeciones presidenciales a la Ley Estaturia de la JEP, en el primer debate se contaron 47 votos en favor de rechazar las objeciones; posteriormente se intentó realizar una segunda votación porque el presidente de esta corporación, Ernesto Macias, afirmó que no se había alcanzado la mayoría. Esta situación dejó varias dudas sobre el estado jurídico en el que está este importante Proyecto para la Jurisdicción.

Sin embargo, la Directora del ICTJ expuso que **no hay un limbo jurídico en el caso, porque la Corte Constitucional había determinado que este Proyecto regresaría a su despacho tras hacer su tránsito por el Congreso**, y sería el Alto Tribunal el encargado de dar la última palabra sobre el asunto. Tomando en cuenta que la Corte en su jurisprudencia ya aprobó la Ley, se esperaría que la JEP contara con Ley Estatutaria sin modificaciones antes del 21 de julio de este año.

[English Version](https://archivo.contagioradio.com/the-reason-why-the-special-jurisdiction-for-peace-will-be-in-the-inter-american-commission-on-human-rights-focus/)

> ?|| Carta de la presidenta de la [@JEP\_Colombia](https://twitter.com/JEP_Colombia?ref_src=twsrc%5Etfw), Patricia Linares, al canciller Carlos Holmes Trujillo sobre audiencia en la [@CIDH](https://twitter.com/CIDH?ref_src=twsrc%5Etfw). [pic.twitter.com/yWIJL0riue](https://t.co/yWIJL0riue)
>
> — Jurisdicción Especial para la Paz (@JEP\_Colombia) [5 de mayo de 2019](https://twitter.com/JEP_Colombia/status/1125109062708936706?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_35439613" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35439613_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
