Title: 77 Familias Emberá Karambá serían desplazadas por la Anglo Gold Ashanti
Date: 2017-03-06 15:49
Category: Entrevistas, Nacional
Tags: Anglo Gold Ashanti, Emberá Karambá, La Colosa
Slug: 77-familias-embera-karamba-serian-desplazadas-por-la-anglo-gold-ashanti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Emberás.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Histórico] 

###### [6 Mar 2017] 

A través de un comunicado, la comunidad indígena Emberá Karambá y comunidades campesinas del municipio de Quinchía en Risaralda, denunciaron que la multinacional Anglo Gold Ashanti **ha puesto en "inminente riesgo de desplazamiento" a más de 77 familias que habitan el territorio ancestral.**

Según la denuncia, directivos de la multinacional han anunciado que iniciarán el proceso de exploración para explotación de oro a cielo abierto 'La Colosa Regional', en un área de 4.000 hectáreas y **advirtieron que si la comunidad continúa oponiendose, procederá a bloquear la única vía de acceso a la zona, destruir el centro de salud, el polideportivo o sus lugares habituales de trabajo artesanal.  
**

Edit Taborda, Gobernadora de la Parcialidad Indígena de Quinchía señaló que desde el año 2016, estas "estrategias  de  **presión  e intimidación  orquestadas  entre  las  empresas  interesadas en dicho proyecto y las instituciones gubernamentales"**, han generando  temor y zozobra en las comunidades frente  al futuro  y  permanencia  en  sus  territorios. ([Le puede interesar: 12 días de desplazamiento de la Comunidad indígena Wounnan Nonam](https://archivo.contagioradio.com/36695/))

###  

También manifestó que respecto a la negativa de la comunidad Emberá Karambá, de vender sus terrenos para la explotación minera, la Anglo Gold y la operadora Minera Miraflores, han amenazado con destruir la única vía de acceso al asentamiento, lo que activo las alarmas, pues de ser así, **más de 77  familias de la zona se verían confinadas.**

La Gobernadora también denunció que la Compañía Minera Miraflores "ha venido adelantando actividades de explotación minera de oro sin Licencia Ambiental, ni Plan de Manejo Arqueológico  y  violando  el  derechos  fundamentales  como  la  Consulta  Previa". Explica que existe una "autorización ilegal del Ministerio del Interior", pues cerró **de  manera  irregular  el  proceso  de  garantía  del  Derecho  Fundamental  a  la  Consulta  y  el Consentimiento Previo, Libre e Informado el 22 de septiembre de 2015.**

###  

Reveló que en 2015, la Anglo Gold realizó un censo ilegal y forzado para "obligar a la comunidad a participar en indemnizaciones falsas, traslados forzados y la manipulación de la información", **lo que ha generado fuertes rupturas "en  el  tejido  social  y  en  los  procesos organizativos propios".** ([Le puede interesar: 800 indígenas en riesgo de desplazamiento por presuntos paramilitares](https://archivo.contagioradio.com/800-indigenas-en-riesgo-de-desplazamiento-por-presuntos-paramilitares/))

Taborda aseguró que ya se estan adelantando acciones legales para la exigencia de derechos y nulidad para la autorización otorgada por Mininterior, con la cual la multinacional ha logrado avanzar en el proyecto inconsulto de explotación minera.

Afirmó que las comunidades de Quinchía y municipios aledaños, desean entablar un diálogo con las instituciones y de no ser posible **"seguiremos nuestra lucha, movilizandonos y denunciando estos atropellos".**

Por último, la Gobernadora hizo un llamado a organizaciones ambientalistas, defensoras de derechos humanos y medio de comunicación, para que presten atención a lo que sucede en la región y apoyen las denuncias de los Emberá Karambá y resaltó que "este es un aviso preliminar y **si no se resuelve, el Estado es el responsable de estos nuevos desplazamientos".**

###### Reciba toda la información de Contagio Radio en [[su correo]
