Title: De 47 estudiantes solo se gradúan 7 por altos costos de matrículas en UniCauca
Date: 2017-04-19 12:35
Category: Educación, Nacional
Tags: Movimiento estudiantil, Universidad del Cauca
Slug: de-45-estudiantes-que-inician-carrera-en-la-unicauca-solo-finalizan-6-por-altos-costos-de-matriculas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/unicauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UniCauca] 

###### [19 Abr 2017] 

Estudiantes de la Universidad del Cauca se mantienen en paro de actividades, desde hace 2 semanas, **debido a los altos costos de las matrículas y a la falta de estructuras** como laboratorios en cada una de las sedes de la institución, que obliga a los a tener que movilizarse hacia otras ciudades para tener clases.

Los alumnos señalan que desde noviembre de 2015, le han expresado a las directivas que el aumento en los precios de las matriculas son el motivo principal para la alta tasa de deserción que tiene la Universidad, **carreras como las Ingenierías o Lenguas que inician con grupos de 40 personas, terminan con 7 0 14 estudiantes que se gradúan**.

Eliana Collaguaso, estudiante de Lenguas Modernas denuncia que entró a la Universidad **pagando \$1.200.000 pesos y actualmente paga un promedio de \$1.500.000 pesos**, a pesar de ser una institución de carácter público.

Frente a esta situación, las directivas de la Universidad han expresado que el aumento en los precios de las matrículas se debe a que las sedes de regionalización son autofinanciadas, debido a que el **gobierno solo da aportes para el funcionamiento de la sede principal de la institución**. Le puede interesar: ["Recursos de Ciencia y Tecnología no se están invirtiendo en investigación"](https://archivo.contagioradio.com/fondo-de-ciencia-y-tecnologia/)

Otra de las dificultades que afrontan los estudiantes es la falta de infraestructura en las sedes de la Universidad (Santander de Quilichao, Popayán y Miranda) que de acuerdo con los estudiantes no cuenta cada una con los laboratorios suficientes para cada carrera, lo que obliga a los a **tener que movilizarse entre las sedes para poder cumplir con sus prácticas.**

Collaguaso, indica que un ejemplo de esta situación, es que los estudiantes de la sede de Santander de Quilichao deban compartir campus con los estudiantes de la Universidad del Valle, por falta de un espacio propio o la falta de libros en la Biblioteca **“que tiene sus estantes vacíos”**. Le puede interesar:["U de Pamplona otra víctima de la desfinanciación de la Educación Pública"](https://archivo.contagioradio.com/universidad-de-pamplona-en-asamblea-permanente/)

Los estudiantes afirmaron que continuarán en cese de actividades hasta que el rector de la Universidad entable conversaciones con ellos y se establezcan acuerdos. **“Estamos estudiando en una universidad solamente pública de nombre**, porque de resto es privada y eso es lo que estamos alegando los estudiantes” afirmó Eliana.

<iframe id="audio_18232335" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18232335_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
