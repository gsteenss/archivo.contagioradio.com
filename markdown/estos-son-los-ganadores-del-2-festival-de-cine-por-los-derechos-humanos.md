Title: Estos son los ganadores del 2° Festival de Cine por los Derechos humanos
Date: 2015-04-14 17:34
Author: CtgAdm
Category: 24 Cuadros, Fotografia, Galerias
Tags: Cine Migrante, Cinemateca Distrital, Documentales Nacionales, Festival de Cine de Bogotá, Festival de Cine y Derechos Humanos
Slug: estos-son-los-ganadores-del-2-festival-de-cine-por-los-derechos-humanos
Status: published

Con gran participación finalizo el 2° Festival de Cine por los Derechos humanos de Bogotá, un espacio de diálogo y reflexión a través del audiovisual entorno a la construcción de una posición crítica frente al tema de Derechos Humanos en Colombia y el mundo. (Ver nota Diana Arias)

En diez escenarios de la ciudad y de manera gratuita, los capitalinos se dieron cita con la selección de 80 cintas provenientes de 23 países que hicieron parte de la muestra audiovisual y la competencia oficial.

La difícil tarea de elegir las mejores producciones quedó en manos del jurado calificador, conformado por Erwin Goggel, Juan Pablo Morris, Helkin René Díaz, Felipe Moreno, Karla Monge, Camilo Velásquez, Ricardo Coral, Alejandra Islas, Carlos Mogollón, Jorge Forero y Carlos Ortiz, quienes por calidad y pertinencia, escogieron las siguientes producciones audiovisuales como ganadoras.

-   **Mejor Cortometraje Nacional**

Control Z, Dirección Eri Pedrozo y Jehisel Ramos, Producción: Jaquelín Hernández López, País: Colombia

-   **Mejor Cortometraje Internacional**

El camino de Raffael, Dirección: Alessandro Falco, País: Italia, País coproductor: España

-   **Mejor Documental Nacional**

Putas o peluqueras, Dirección: Mónica Moya, País: Colombia.

-   **Mejor Documental Internacional**

Fresia, Dirección: Corrado Punzi, País: Italia, País Coproductor: Chile

-   **Mejor Largometraje**

El regreso, Dirección: Patricia Ortega, Producción: Sergio Gómez, País: Venezuela

-   **Menciones Especiales**

** Mención Especial Cortometraje Internacional**

Cuerdas: Pedro Solís García, Producción: Nicolas Matji, País: España

**Mención Especial Documental Internacional **

Resistiremos (Sizobalwa), Dirección y producción: Pablo Pinedo Bóveda, País: Sudáfrica

-   **Video Nuevos Realizadores**

Por decisión de la Fundación Konrad Adenauer  patrocinadora  del concurso "Video de Nuevos Realizadores", el "Premio del público" es para los dos videos que puntearon la votación  en el canal de YouTube y obtuvieron un empate: "Top Model" de Horacio Ramírez Lara “Currículum vítae”  de Harvy Manuel Muñoz

Los otros dos ganadores premiados por la Fundación son:

"Los derechos son de todos" y "Hay alguien más"
