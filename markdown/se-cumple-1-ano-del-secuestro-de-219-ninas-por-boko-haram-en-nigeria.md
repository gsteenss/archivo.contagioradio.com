Title: Se cumple 1 año del secuestro de 219 niñas por Boko Haram en Nigeria
Date: 2015-04-15 15:04
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Boko Haram, Secuestro 219 niñas en Nigeria, un año de secuestro 300 niñas en Nigeria
Slug: se-cumple-1-ano-del-secuestro-de-219-ninas-por-boko-haram-en-nigeria
Status: published

###### Fotos:voces.huffingpost.com 

A un año del **secuestro de las 219 niñas** de una escuela de Nigeria por parte del la milicia islámica **Boko Haram**, los **padres no han recibido ningún apoyo del gobierno**, y el ejecutivo a reconocido no tener medios suficientes para encontrarlas.

"**No tenemos comunicación directa con el Gobierno** y nadie parece pensar que nos merecemos una explicación de cómo está el caso. Cada día nos consolamos como podemos, pero también **estamos un poco más cerca de la desesperación total", afirmó Samuel Yanga, padre de una de las chicas.**

**Bring Back Our Girls (Devolvednos a nuestras chicas), es la organización social creada por las víctimas** y por solidarios de todo el mundo. La organización ha desplegado una gran campaña internacional con el objetivo de presionar al gobierno para que despliegue todos los medios y las encuentre.

Los familiares a penas han recibido información y la única respuesta del gobierno ha sido que mantengan la esperanza de que serán recuperadas.

Hay que recordar que el **anterior ejecutivo tardó 19 días en condenar el ataque** y además condenó primero los ataques contra la revista satírica Cahrlie Hebdo en Francia, por lo que fue fuertemente criticado.

Una coalición de países africanos **logró liberar las ciudades del noroeste Nigeriano de Gwoza, Damboa y Bama de la milicia islámica Boko Haram**, abriendo las puertas a la esperanza de que fueran encontradas.

Pero todos estos esfuerzos han sido inútiles, obligando al gobierno ha reconocer que no **tiene los suficientes recursos para enfrentar los bastiones de la milicia** y para finalizar la bus queda de las escolares.

El país actualmente enfrenta distintos **conflictos armados**, el de mayor importancia es el de la milicia islamista de carácter extremista **Boko Haram**, que en los últimos años ha tomado fuertes posiciones en el noreste, y la **amenaza de los rebeldes en el delta del Níger**.

El **presidente entrante** ha declarado que en unos meses **va a acabar con la amenaza de Boko Haram y a pacificar el país**. También ha felicitado a los observadores internacionales por su labor en la verificación de las elecciones, que según los diplomáticos han sido totalmente transparentes.
