Title: "No hay signo de tortura contra los 12 soldados asesinados por el ELN"
Date: 2015-10-29 12:21
Category: Nacional, Paz
Tags: 12 soldados asesinados, ASO' U'WA, Asociación de Autoridades Tradicionales y Cabildos U’wa, Bachira, Boyacá, Carlos Valdez, Director Medicina Legal, ELN, Instituto de Medicina Legal y Ciencias Forenses
Slug: no-hay-signo-de-tortura-contra-los-12-soldados-asesinados-por-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/soldados-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### www.cablenoticias.tv 

###### [29 oct 2015]

El director del Instituto de Medicina Legal y Ciencias Forenses, Carlos Valdez descartó que los 12 militares muertos en una emboscada realizada por el ELN  hubiesen recibido tiros de gracia, “en ninguno de los cuerpos se encontraron signos de disparos a contacto, las distancias de disparo corresponden a larga distancia”, señaló Valdez.

Así mismo, mencionó que **“las trayectorias intracorporales dan cuenta de que la mayor cantidad de los impactos fueron recibidos en la cabeza, tórax y miembros”**, de manera que se concluye que “no hay signo de tortura ni tratos crueles e inhumanos”.

El resultado de las necropsias practicadas a los soldados determina que la causa de la muerte se dio por **“politraumatismo múltiple por el paso de proyectil de arma de fuego de alta velocidad tipo fusil”**.

Desde la Asociación de Autoridades Tradicionales y Cabildos U’wa, de Boyacá  se asegura que **360 indígenas del resguardo Bachira**  se habrían visto afectados por el ataque del ELN, sin embargo, Vladimir Moreno Torres, presidente ASO' U'WA, afirma que la comunidad no ha podido verificar la situación.
