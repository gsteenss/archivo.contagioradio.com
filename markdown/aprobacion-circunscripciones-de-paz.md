Title: Tres opciones para la aprobación de las Circunscripciones de Paz
Date: 2017-12-11 14:57
Category: Política
Tags: Cámara, Circunscripciones de paz, Senado
Slug: aprobacion-circunscripciones-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/elecciones-tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: hbs noticias] 

###### [11 Dic 2017] 

Dentro de las alternativas por las cuales podrían aprobarse las Circunscripciones de Paz está la posibilidad de que el presidente del Senado pase a firma el acto legislativo, dando cumplimiento al concepto del Consejo de Estado, otra que haya **concepto positivo a las acciones de tutela que presentaron las víctimas y una tercera es que se tramite de nuevo el proyecto como ley ordinaria** y con mensaje de urgencia.

Cualquiera de las tres posibilidades incluye la necesidad de que la Registraduría avale el proceso de inscripción que tendría que ser extemporáneo y especial. Además tendrían que abrirse un proceso de elecciones atípicas en los 16 territorios lo que implica un gasto extra por cuenta de la demora de Efraín Cepeda, afirmó el congresista del Polo Democrático

**Circunscripciones de paz para las víctimas son parte del acuerdo y deben implementarse**

Según el Senador Iván Cepeda, las circunscripciones especiales de paz están en el acuerdo y deben implementarse. El congresista señala que la palabra del Consejo de Estado ordena que el presidente del Senado debe enviar el acto legislativo a la presidencia para que sean aprobadas esas curules para las víctimas y por ello se van a promulgar.

A pesar de que existe la posibilidad de que se radique el acto en presidencia, también está latente la posibilidad de que Efraín Cepeda se niegue a hacerlo. Sin embargo la acción de cumplimiento interpuesta por el gobierno y las tutelas que han presentado las organizaciones de víctimas y de DDHH son dos de las probabilidades de que se aprueben. Además la registraduría podría convocar a elecciones atípicas para que se definan los nombres de quienes ocuparán esas curules.

### **Las curules de paz son una deuda con las víctimas** 

Por otra parte, frente a la posibilidad de que las víctimas obtengan representación por la vía ordinaria, el senador Iván Cepeda señala que uno de los criterios a la hora de elegir debe ser la defensa de la paz y la lucha contra la corrupción, también es cierto que las curules para las víctimas tienen que implementarse como parte de pago de la deuda histórica que tiene el país con las personas que han sufrido la guerra y no han tenido representación en el legislativo.

<iframe id="audio_22587333" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22587333_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
