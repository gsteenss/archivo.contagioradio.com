Title: Así avanza el paro nacional en la Región Amazónica
Date: 2016-05-30 00:05
Category: Paro Nacional
Tags: Paro Nacional
Slug: asi-avanza-el-paro-nacional-en-la-region-amazonica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/REGION-AMAZONICA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASEP] 

### Amazonas 

**2 Junio**

En Leticia las guardias de quince comunidades indígenas acompañan la movilización, en la que lograron acordar con el Alcalde y el Coronel de la Policía que se garantizaría el derecho a la protesta y que se haría uso de la fuerza. Los líderes además de sumarse al pliego de peticiones de la Cumbre, insisten en que llevan seis años con un acueducto sin funcionar, enfrentan pésimas condiciones de atención en salud y las consecuencias de la escasa inversión educativa. Sin embargo; permanecen en el punto de concentración motivados, participando de las actividades.

Indígenas del pueblo Muruy continúan movilizándose en el corregimiento del Encanto.

### Caquetá 

**2 Junio**

El pueblo Coreguaje continúa movilizándose.

### Putumayo 

**30 Mayo **

Se tiene estimado que cerca de 3 mil personas se unan al paro. En Puerto Asís, el alcalde Omar Guevara, aseguró que se garantizaría el derecho a la protesta pacífica. Las exigencias de esta parte del país tienen que ver específicamente con los incumplimientos del Gobierno Nacional con el campesinado, el rechazo a la explotación petrolera, las fumigaciones con glifosato y la 'Ley Obama', que da la posibilidad de que campesinos productores de coca sean extraditados.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
