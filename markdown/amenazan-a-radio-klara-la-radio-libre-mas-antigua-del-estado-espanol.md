Title: Amenazan a Radio klara, la radio libre más antigua del estado español.
Date: 2015-01-15 18:07
Author: CtgAdm
Category: El mundo
Slug: amenazan-a-radio-klara-la-radio-libre-mas-antigua-del-estado-espanol
Status: published

###### Foto: Radio Klara 

**Radio Klara ha recibido cartas anónimas amenazando a sus locutores. Estos hechos han sido denunciados ante la justicia ordinaria.**

Por otro lado el viernes 9 de enero la alcaldesa de Valencia recibió una carta anónima con una bala y una carta amenazante firmada falsamente en nombre de la radio libre, lo que se interpreta como un intento de criminalización.

Todo esto se enmarca en un clima de represión que coincide con la recién aprobada ley de seguridad, conocida como ley mordaza, que limita derechos fundamentales de protesta criminalizando así movimientos sociales, medios de comunicación libres y  los movimientos libertario e independentista en el estado español.

Contagioradio.com rechaza todo ataque contra medios de comunicación libres o comunitarios, aquí en Colombia, allí en el estado español o en donde se produzca.

**Reproducimos  el comunicado de Radio Klara:**

Las amenazas recibidas por la Sra. Alcaldesa de Valencia, Dª Rita Barberá, en forma de sobre con una bala en su interior y, aparentemente, remitido desde Ràdio Klara, nos llevan a manifestar lo siguiente:

1.  Negamos rotundamente la autoría de dicho envío, cuyo objetivo creemos que no es otro que el de perjudicar a Ràdio Klara.
2.  Este hecho se produce unas semanas después de que se hayan recibido en el buzón de Ràdio Klara anónimos amenazantes y que fueron denunciados en el juzgado de guardia el pasado 2 de diciembre de 2014. Los textos recibidos tienen la misma caligrafía y ortografía que el recibido por la Sra. Alcaldesa de Valencia, según observamos en las fotos publicadas.
3.  Repudiamos cualquier forma de violencia que se ejerza contra cualquier ciudadano o ciudadana, la ejerza quién la ejerza. Rechazamos, pues, las amenazas recibidas por la Sra. Alcaldesa por doble motivo: Por principios éticos y por el hecho de que se haga suplantando el nombre de Ràdio Klara.
4.  Ràdio Klara tiene como función ser altavoz de aquellas personas y colectivos sociales comprometidos con una sociedad más justa y más libre, tarea ésta en la que Ràdio Klara lleva más de 30 años y las amenazas o los intentos de criminalización no nos harán desistir de ella.

