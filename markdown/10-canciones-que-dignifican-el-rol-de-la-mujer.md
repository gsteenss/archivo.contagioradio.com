Title: 10 canciones que dignifican el rol de la mujer
Date: 2015-11-28 11:36
Category: Cultura, Otra Mirada
Tags: Amor, Amparo Ochoa, Ana Tijoux, Canciones, Lila Downs, Marta Gómez, mujeres, Música, Orishas, Silvio Rodríguez, Ska-P
Slug: 10-canciones-que-dignifican-el-rol-de-la-mujer
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/ujer-musi.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

###### [25 nov 2015] 

Son muchas las canciones que hablan de una o varias mujeres. Algunas de ellas, banalizan este tema al estereotipar el concepto de belleza. Es por eso, que en el marco del Día Internacional de la Eliminación de la Violencia contra las Mujeres, compartimos un listado con 10 melodías que hacen homenaje al significado de la palabra ‘mujer’.

**Amparo Ochoa – Mujer.**

https://www.youtube.com/watch?v=57neSrYUJpw

La canción compuesta por la compositora y cantante venezolana Gloria Martín, es interpretada por la mexicana amparo Ochoa, perteneciente a la generación de cantantes de la década del 60 y fue una de las primeras figuras del movimiento musical de la Nueva Canción, que daba importancia a composiciones con amplio contenido social.

**Ana Tijoux – Antipatriarca.**

https://www.youtube.com/watch?v=fSqOdoldsUc

Ana Tijoux nació en Francia, debido a que sus padres se trasladaron a ese país tras el Golpe de Estado en Chile en 1973. Después del regreso de la Democracia al país latinoamericano, se instala e inicia su carrera Musical. Integró la agrupación de rap Makiza y luego formó su carrera en solitario en donde ha tenido éxito notable.

**Marta Gómez – La Esperanza.**

https://www.youtube.com/watch?v=bTw55Dwf5d8

Una estrofa vale más que mil palabras: “De mañana doña Juana se Levanta y va inventándose la vida como Dios se la dejó. Y aunque sueña no es con duendes ni con hadas, Doña Juana tiene un sueño que no cambia de color”. Es una de las frases de la cantautora colombiana, en donde se le canta a la mujer que vive el día y busca una esperanza.

**Lila Downs – Dignificada.**

https://www.youtube.com/watch?v=NyO429r34-Y  
Esta cantante, productora, actriz y antropóloga mexicana le da un toque diferencial a sus canciones: las canta en inglés, en español y en idiomas nativos, tales como el mixteco, el zapoteco, el maya, el Purépecha y el Nahuatl. Dignificada es en muchos casos como lo dice en la canción, “la voz del silencio”.

**Orishas – Mujer.**

https://www.youtube.com/watch?v=bg7ATBSP8aM

Son conocidos como precursores y pioneros del hip hop cubano, además por cantar composiciones con crítica social. La canción es dedicada a todas las mujeres que luchan día a día por hacer valer sus derechos y por alcanzar un mejor futuro.

**One Woman – ONU**

https://www.youtube.com/watch?v=-m6v4U7zKuI

En 2013, 25 cantantes y músicos de más de 20 países, fueron reunidos por la ONU Mujeres, para hacer esta melodía en alusión al Día de la Mujer que se celebra el 8 de marzo.

**Reincidentes – Amor Revolucionario.  **

https://www.youtube.com/watch?v=4-CCdG3aSxo

Esta agrupación de hip hop bogotano busca reivindicar el mensaje social del rap. Amor Revolucionario es una canción de unión y fraternidad hacia el sentimiento del amor que viene permeado de realidades propias que no por eso quitan la pureza de este sentimiento.

**Silvio Rodríguez – Mujer.**

https://www.youtube.com/watch?v=m5bnEVBeeOI

Es uno de los artistas latinoamericanos con mayor trascendencia en los últimos años. Silvio Rodríguez habla de las mujeres que lo estremecen; “las mujeres de fuego, de nieve, de frío…” pero que en sus propias palabras, lo que lo hacen perder casi el sentido son los ojos de su hija a los que implícitamente dedica esta canción.

**Ska-P – Violencia Machista.**

https://www.youtube.com/watch?v=uRxVkn-hyBA

Para este grupo español de Ska-Punk, el maltrato contra la mujer es un tipo de “violencia fascista” que no merece ningún tipo de consentimiento. Es un llamado a las mujeres para que denuncien y ellas mismas desaten las cadenas que les impiden ser libres.
