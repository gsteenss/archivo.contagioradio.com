Title: Por lo menos 2 niños heridos por ESMAD en marcha de transportadores
Date: 2016-07-21 14:37
Category: Movilización, Nacional
Tags: negociaciones entre camioneros y gobierno, Paro camionero, paro transportadores colombia
Slug: por-lo-menos-2-ninos-heridos-por-esmad-en-marcha-de-transportadores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Paro-camiones-061-e1466110579707.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [21 Julio 2016 ]

De acuerdo con Jorge García, presidente de la Federación Nacional de Camioneros, la marcha de la que hacían parte los transportadores y algunos integrantes de sus familias, fue violentamente reprimida por miembros del ESMAD quienes los provocaron cuando iban a la altura de la calle 13 con avenida Boyacá, les lanzaron gases y balas de goma, **hiriendo a por lo menos 2 de los niños que participaban de la movilización**.

"Nosotros hemos recibido de buena manera todas las expresiones de los Senadores y Representantes pero eso no quiere decir que estemos con unos o con otros, estamos con el Congreso en pleno", afirma García, en relación con los supuestos vínculos que existen entre el gremio y el Centro Democrático y agrega que "la protesta de ayer que se hizo con camisas negras y algunas banderas negras, fue iniciativa de los mismos camioneros **en duelo por la persona que murió en Boyacá**, queriendo demostrar que la vida de un ser humano vale mucho y que no puede pasar inadvertida menos en una protesta".

Los transportadores reprochan los ataques del ESMAD y **critican que el presidente Santos tras 45 días de paro, no haya hablado con ellos** y que por el contrario haya aprobado la cancelación de 150 licencias de conducción y sanciones por el orden de \$487 millones a quienes no presten el servicio. Son 330 mil camioneros de 2 mil 900 empresas quienes denuncian el "amedrantamiento injustificado" por parte de la presidencia que, aseguran, les está "midiendo el aceite" porque sabe que en sus casas no tienen mercado.

Según García el punto álgido de las negociaciones es el de la chatarrización, pues mientras que MinTransporte propone 18 meses, los transportadores mantienen su exigencia de que el tiempo se prolongue hasta que hayan recursos y que se haga bajo el método uno a uno, como la única forma que tienen para blindarse pues "**no podemos hablar de fletes ni de otros asuntos cuando mi camión no va a ser el que va a prestar el servicio**, la columna vertebral es la regulación del parque automotor".

<iframe src="http://co.ivoox.com/es/player_ej_12297123_2_1.html?data=kpefm5yVdpShhpywj5abaZS1kZyah5yncZOhhpywj5WRaZi3jpWah5yncavj08zSjazFtsSZpJiSo6nFaZO3jKvSxsrWpcTdhqigh6eXsozCwtGYxsqPh8LhytTbx9fTt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

######  

 
