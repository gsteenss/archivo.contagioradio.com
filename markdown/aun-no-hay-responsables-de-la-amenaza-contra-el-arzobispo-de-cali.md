Title: Arzobispo de Cali es amenazado a través de un panfleto
Date: 2016-12-19 14:27
Category: DDHH, Nacional
Tags: amenazas contra líderes sociales, Darío de Jesús Monsalve, Implementación de Acuerdos
Slug: aun-no-hay-responsables-de-la-amenaza-contra-el-arzobispo-de-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Arzobispo-de_Cali.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País] 

###### [19 Dic 2016] 

El Arzobispo de Cali, Monseñor Darío de Jesús Monsalve, denunció que el pasado 18 de Diciembre fue dejado en la casa de Arzobispal de Cali un panfleto intimidatorio, y aún no se conocen a los responsables. **"Muerte a las Farc, a Santos y a los clérigos Comunistas", es el mensaje del panfleto que además contiene imágenes de una soga y una calavera.**

Frente a los hechos, el Arzobispo manifestó que "se trata de un eco al discurso político que están ejerciendo algunos en el país de señalamientos y de **una fijación política en el anticomunismo totalmente anacrónica y obsoleta, pero peligrosa**... En este país se vive el anacronismo".

Además, personajes como Iván Cepeda, Ángela Robledo y Juan Camilo Restrepo han denunciado a través de sus redes sociales que las amenazas que recibió el prelado pueden tener una estrecha relación con **su papel activo en los diálogos de paz entre el Gobierno Nacional y las FARC y su labor en los acercamientos con el ELN. **

Por último, a pesar de las amenazas, monseñor Darío de Jesús Monsalve aseguró que se encuentra tranquilo y que **puso en conocimiento de las autoridades la situación para que se investigue el origen de las intimidaciones.**

###### Reciba toda la información de Contagio Radio en [[su correo]
