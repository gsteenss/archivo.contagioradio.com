Title: Líder social del Catatumbo, Celiar Martínez completa cinco días desaparecido
Date: 2019-09-13 17:20
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Catatumbo, Celiar Martínez
Slug: lider-social-del-catatumbo-celiar-martinez-completa-cinco-dias-desaparecido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Celiar-Martínez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ascamcat] 

Desde el pasado 8 de septiembre sobre las tres de la tarde en San Pablo, Teorama, municipio de  Norte de Santander, Celiar Martínez, presidente de la Junta de Acción Comunal de la vereda Puente Azul fue abordado por hombres armados quienes se lo llevaron con rumbo desconocido, sin embargo días después de su desaparición, aún no hay noticias de su paradero.

Juan Carlos Quintero, vicepresidente de ASCAMCAT relata que **Celiar, quien a su vez es vicepresidente de la Asociación de Juntas de Acción Comunal (ASOJUNTAS) del corregimiento de San Pablo** también ha trabajado junto a la organización desde hace 10 años y que su labor en el territorio como líder comunal y social, está orientada a mantener el trabajo por veredas de la zona. [(Le puede interesar: Asesinan a Ezequiel Rangel líder de ASCAMCAT en Norte de Santander)](https://archivo.contagioradio.com/asesinan-a-ezequiel-rangel-lider-de-ascamcat-en-norte-de-santander/)

Aunque no se conocen amenazas contra Celiar, según la información aportada por la comunidad, señala como presuntos responsables a integrantes del ELN. Su desaparición sin conocerse su paradero ni los motivos por los cuáles habría sido raptado, es motivo de preocupación para  su familia y a la comunidad de San Pablo.

Desde ASCAMCAT se ha solicitado a las autoridades que activen una ruta humanitaria que permita esclarecer el paradero de Celiar, cuya desaparición se suma a una serie de acciones contra la organización campesina, pues en 2018 fueron asesinados cinco líderes afiliados y en lo que va corrido del 2019, han sido once de sus integrantes quienes han sido obligados a salir del Catatumbo debido a constantes amenazas.

<iframe id="audio_41571041" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_41571041_4_1.html?c1=ff6600"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
