Title: Medidas de austeridad violan los DDHH: Parlamento Europeo
Date: 2015-04-06 20:11
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Banco Central Europeo, Banco Mundial, españa, Fondo Monetario Internacional, Gracia, Italia, Medidas de Austeridad, Parlamento Europeo, Troika, Violaciones a los DDHH
Slug: medidas-de-austeridad-violan-los-ddhh-parlamento-europeo
Status: published

###### Foto:Vozpópuli 

###### **Entrevista con [Jordi Sebastiá], eurodiputado por el grupo mixto Primavera Europea:** 

<iframe src="http://www.ivoox.com/player_ek_4313719_2_1.html?data=lZielZyVfY6ZmKiakp6Jd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMLmzcbax9PYs4zZ1tfc0srTb8bhytnSjc7SqtDmzsqY1dTGtsaf187czsbHrYa3lIqvldOPqMafpamah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Una comisión del parlamento europeo emitió el domingo 5 de Abril un **informe sobre el impacto de las medidas de austeridad** impuestas por la Troika, el BM y el BCE en países como **España**, con el objetivo de poder medir el impacto sobre los DDHH.

El informe mantiene que dichas medidas han fomentado la **violación de derechos básicos de la ciudadanía en España**, así como arroja cifras alarmantes sobre la pobreza infantil, la desprotección en derechos como la salud, la vivienda digna, el trabajo y a última hora sobre la libertad de manifestación y de expresión.

Las medidas de austeridad que se han ido aplicando en países del sur de Europa como España, Grecia, Italia o Portugal, comenzaron a aplicarse con el inicio de la crisis económica hasta el día de hoy. Todas las medidas se centran en **recortes sobre los derechos del trabajador, de la asistencia médica, la educación, la desprotección de derechos básicos como el del acceso a una vivienda diga y los derechos referentes a la protesta social**.

**Jordi Sebastiá, eurodiputado** español por la coalición **Primavera Europea**, afirma que la izquierda europea antes de emitirse este informe y de la aplicación de los recortes sociales ya había tildado dichas medidas como **“Austericidio”**, por sus graves consecuencias sociales a futuro.

Para elparlamentario todo comenzó cuando el país decidió **pagar la deuda privada de los bancos por su mala gestión**. Para ello tuvo que recortar el presupuesto social. Como ejemplo nos indica que **Islandia no lo hizo**, sus bancos quebraron, actualmente hay otros nuevos y el país está creciendo económicamente **sin consecuencias** de ningún tipo **sobre lo social** al no haber tenido que aplicar ningún recorte.

El eurodiputado nos brinda de  ejemplos de las consecuencias, como el decreto 16/2012 que ha dejado **a 800.000 personas sin sanidad**,  o como en la educación el gasto familiar se ha incrementado desde 2006 en un 30,3%, quizás el más grave es el de la **pobreza infantil que según UNICEF aumentó un 28% entre 2008 y 2012**.

El derecho al trabajo digno es otro de los más afectados, actualmente el **23,7% de la población está en paro**, siendo los **jóvenes** los más afectado con un **51,8% de desempleo**.

También es importante, según el informe, la protección del derecho a la vivienda digna. Para el eurodiputado está ha quedado totalmente desprotegida y los créditos hipotecarios a merced de los bancos. Él nos recuerda como, antes de su trabajo en la UE, el era alcalde de una localidad de Valencia y tuvo que contemplar dos suicidios por no poder pagar la deuda contraída con el sector inmobiliario.

Por último el informe, igual que la ONU advierte al estado de la grave **violación de DDHH** , en concreto los referidos a la libertad de expresión y de protesta, en qué consiste la **ley de seguridad** ciudadana conocida como **“Ley Mordaza”.**
