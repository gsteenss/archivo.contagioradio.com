Title: Asesinan a familiares del excombatiente Nencer Barrera en Huila
Date: 2020-07-17 14:45
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Algeciras, Excombatientes de FARC, Nencer Barrera
Slug: asesinan-a-familiares-del-excombatiente-nencer-barrera-en-huila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Excombatientes-Indepaz.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Familiares-de-Nencer-Barrera-asesinados.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

  
*Foto: Joaquín Sarmiento AF*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 16 de julio sobre las 8:30 de la noche, desconocidos armados ingresaron a la vereda el Quebradón Sur, municipio de Algeciras, Huila y **asesinaron a cuatro personas, entre ellas dos familiares del excombatiente y firmante de paz Nencer Barrera Bustos.** **Al menos otras dos personas resultaron heridas en el hecho, entre ellos un menor de 8 años.** 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las víctimas fueron reconocidas como, Juan David de 25 años, Luis Eduardo Gómez Marulanda de 22, Luz Stella Burgos de 34 años y el adolescente Edison Sebastián Moya 16 años. (Le puede interesar: [Siguen los asesinatos contra líderes y excombatientes en medio de la pandemia](https://archivo.contagioradio.com/siguen-los-asesinatos-contra-lideres-y-excombatientes-en-medio-de-la-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según informaciones preliminares, los padres y hermanos de **Nencer Barrera habrían desaparecido**, pero según Camilo Fagua, abogado defensor de DD.HH. del Partido FARC, se trató de un hecho temporal mientras la familia huía de los hombres armados para salvaguardar su vida.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Sergio_FARC/status/1284013343477563392","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Sergio\_FARC/status/1284013343477563392

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Ante el hecho, Nencer Barrera pidió auxilio a las **autoridades que se encuentran a 20 minutos de la vereda donde se presentó el hecho, pero estos no llegaron sino varias horas después.** «*Al momento de los hechos ni la Policía hizo presencia y los efectivos del Ejército llegaron hasta pasada la medianoche. Cuatro horas después de lo ocurrido*», afirmó Camilo Fagua.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Algericas opera la Novena Brigada del Ejército y pese a tener alta presencia militar, sus habitantes denuncian que la incursión de grupos armados es constante ante la inacción de la Fuerza Pública que se encuentra allí desplegada. (Lea también: [«Crímenes contra excombatientes de FARC son producto de la acción y omisión del Estado»](https://archivo.contagioradio.com/crimenes-contra-excombatientes-de-farc-son-producto-de-la-accion-y-omision-del-estado/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "***Lo que sucedió ayer es absolutamente reprochable: que un grupo armado tenga la capacidad de reacción de efectivos de la Fuerza Pública y que ellos se nieguen a hacer presencia***"
>
> <cite>Camilo Fagua, abogado defensor de DD.HH del Partido FARC.</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Familiares de Nencer Barrera víctimas del «exterminio sistemático» contra los excombatientes y sus familias

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Partido FARC ha sido enfático en señalar que están siendo víctimas de un **«exterminio sistemático»** y ha denunciado el **asesinato de al menos 219 excombatientes después de la firma del Acuerdo de Paz**; señalando como responsable directo al Gobierno de Iván Duque.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1283904037767847937","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1283904037767847937

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Cabe señalar, que la violencia no solo se ejerce en contra de los y las excombatientes, sino que, como en el presente caso, muchas veces afecta a sus familiares. Según el más reciente informe del Instituto de Estudios para el Desarrollo y la Paz -[Indepaz](http://www.indepaz.org.co/wp-content/uploads/2020/07/Informe-Especial-Asesinato-lideres-sociales-Nov2016-Jul2020-Indepaz.pdf)- **al menos 20 familiares de excombatientes han sido asesinados y 7 más han sido víctimas de atentados contra su vida desde que Iván Duque asumió la Presidencia.**

<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":86934,"sizeSlug":"large"} -->

<div class="wp-block-image">

<figure class="aligncenter size-large">
![Nencer Barrera](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Excombatientes-Indepaz.png){.wp-image-86934}
</figure>

</div>

<!-- /wp:image -->

<!-- wp:paragraph {"align":"center","fontSize":"small"} -->

Fuente: Informe "Paz Para Liderar" de Indepaz

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
