Title: Organizaciones de DDHH denuncian vigilancia y hostigamiento con drones
Date: 2020-01-31 10:34
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: drones, Inteligencia Estatal, Vigilancia
Slug: organizaciones-de-ddhh-denuncian-vigilancia-y-hostigamiento-con-drones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Untitled-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Drones

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/gustavo-gallon-sobre-denuncia-vigilancias-hostigamientos_md_47283933_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Gustavo Gallón | Director Comisión Colombiana de Juristas

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

La[Comisión Colombiana de Juristas (CCJ)](https://twitter.com/Coljuristas)denunció que en menos de una semana ha sido víctima en dos ocasiones de la vigilancia de drones que ha merodeado en sus instalaciones. La situación ha generado preocupación en medio de las revelaciones que dan cuenta de cómo diversas instituciones, incluidas organizaciones defensora de derechos humanos han sido víctimas de chuzadas y seguimientos por agentes estatales desde el año pasado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Su director, Gustavo Gallón, relata que los hechos se dieron el pasado lunes 20 de enero mientras integrantes de diferentes organizaciones de derechos humanos se encontraban reunidos en la sede de la oficina de la CCJ cuando se percataron de la presencia del dron. [(Le puede interesar: Las peores prácticas de inteligencia militar nunca se fueron)](https://archivo.contagioradio.com/las-peores-practicas-de-inteligencia-militar-nunca-se-fueron/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras el dron permanecía suspendido en el aire, las personas presentes observaron la cámara del dispositivo que registró la escena durante unos segundos antes de ascender y salir de lugar. "Para nosotros esto implica la presión de agentes estatales, maxime cuando hemos sabido que hay personas que han estado realizando actuaciones irregulares" con un programa de inteligencia conocida como el hombre invisible".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los hechos fueron denunciados por el director de la CCJ ante el comisionado de Paz del Gobierno, Miguel Ceballos con quien sostuvo una reunión al día siguiente, el funcionario pidió a un coronel presente que hiciera las investigaciones pertinentes. Aunque el director indica que no ha recibido información al respecto, por el contrario, cerca de las 5:00 pm del viernes 24, apareció nuevamente un dron en el patio trasero de la CCJ, el objeto sobrevoló por más de 10 minutos realizando movimientos intermitentes al interior de la propiedad, hecho que varias personas documentaron en videos y fotografías.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No se trata de un hecho aislado

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Esta situación se da tan solo semanas después que se diera a conocer cómo el Ejército utilizó un sofisticado sistema de interceptaciones  a través de dos de las oficinas de inteligencia y contra inteligencia, durante el segundo semestre de 2019 para espiar de manera ilegal a magistrados de la Corte Suprema de Justicia (CSJ), congresistas, gobernadores, periodistas y defensores de DD.HH. [Ejército realizó "chuzadas" a congresistas, jueces, periodistas y defensores de DD.HH.](https://archivo.contagioradio.com/ejercito-realizo-chuzadas-a-congresistas-jueces-periodistas-y-defensores-de-dd-hh/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A este episodio se suma la denuncia de la Comisión de Justicia y Paz que alertó el pasado 21 de noviembre de la presencia de un dron de la Policía Nacional que sobrevoló por 15 minutos la sede de la organización y el episodio denunciado a través de redes sociales por el exnegociador de La Habana, Humberto de la Calle quien vivió un evento similar cuando un dron sobrevoló fuera de su vivienda y permaneció durante un tiempo frente a su ventana, algo que sucede por tercera vez, según relata.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, el 30 de enero, en la vivienda del abogado y defensor de DD.HH Daniel Prado, se conoció la aparición de otro dron hallado en el patio de su residencia, en la ciudad de Bogotá. Cabe mencionar que **el abogado representa a las víctimas en el caso de los 12 Apóstoles en un proceso que vincula al ganadero Santiago Uribe Vélez** y ha sido víctima de diversos seguimientos en el desarrollo de este caso

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DeLaCalleHum/status/1221099165041729536","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DeLaCalleHum/status/1221099165041729536

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Mientras el Gobierno no se manifieste, será responsable de lo que pase

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Gustavo Gallón señala que por ahora, las denuncias sobre los drones se han hecho directamente al Gobierno, pues considera que de hacerse directamente a la Fiscalía no tendría resultados, tal como las denuncias que se han hecho desde hace más de 10 años entorno a amenazas que continúan archivadas. [(Lea también: Infiltración en marchas por parte de Policía y Ejército: una práctica que se está haciendo común)](https://archivo.contagioradio.com/infiltracion-marchas-parte-policia-ejercito-practica-comun/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Aquí debe haber una acción directa por parte del Gobierno, el Estado debe tener control del espacio aéreo", afirma Gallón quien es enfático en rechazar este tipo de acciones que pareciera, frente a las más recientes denuncias de diversos sectores, estarse naturalizando, "no es posible que vivamos en una sociedad en la que estamos siendo seguidos o supervisados por drones circulando impunemente, debe haber un reclamo ciudadano".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
