Title: 311 trabajadores serán despedidos de la Secretaría de Hábitat
Date: 2016-06-30 15:32
Category: DDHH, Economía
Tags: Masacre Laboral, Secretaría Habitad
Slug: 311-trabajadores-seran-despedidos-de-la-secretaria-de-habitat
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/WhatsApp-Image-20160630.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: 

##### [30 Jun 2016]

EL día de hoy se vence la resolución de nombramiento que ampara en términos económicos presupuestales a **311 empleados de la Secretaría de **[**Hábitat**]. Los trabajadores serán despedidos debido a que no se renovará esta resolución bajo el argumento de la Alcaldía de [Peñalosa de finalizar con los empleos por meritocracia](https://archivo.contagioradio.com/3-trabajadores-despedidos-de-etb-permanecen-en-huelga-de-hambre-y-encadenados/) y de planta temporal que pertenecían a la alcaldía de la Bogotá Humana.

Los trabajadores han argumentado que muchos de estas vinculaciones no se dieron bajo la alcaldía de Gustavo Petro, sino que vienen de tiempo atrás. Por ende señalan que los despidos son injustificados y preparan mecanismos de acción como [la tutela, los derechos de petición](https://archivo.contagioradio.com/trabajadores-despedidas-de-la-etb-se-encadenan-a-sus-puestos-de-trabajo/)  y las quejas de orden administrativos para exigir la información suficiente y necesaria sobre por qué se llevo a cabo el despido masivo y **condenar el silencio que han mantenido diferentes funcionarios.**

Por otro lado, los trabajadores han denunciado que durante esta última semana se han presentado efectivos de la vigilancia privada **con listas en mano** obstaculizando el libre tránsito al interior de la Secretaría y disponibles de la Policía sobre a las afueras de la misma.

De acuerdo con Cesar Santoyo, profesional especializado de la Secretaría de Habitad, estas actividades tienen como fin generar **zozobra entre los trabajadores y como un mecanismo de presión y represión frente a la movilización.**

Estos despidos afectan directamente a la ciudad debido a que diferentes proyectos van a cesar, al igual que la protección de políticas públicas anteriores como lo es el **subsidio de vivienda que se podría eliminar**.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
