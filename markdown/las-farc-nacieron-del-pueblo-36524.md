Title: "Las FARC nacieron del pueblo y van a ser pueblo"
Date: 2017-02-19 16:25
Category: Entrevistas, Paz
Tags: Aldinever, FARC, paz, Zonas Veredales
Slug: las-farc-nacieron-del-pueblo-36524
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_2024-e1487538210314.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [19 Feb. 2017] 

En medio de las difíciles condiciones en las que se encuentran los 600 guerrilleros y guerrilleras de las FARC en la Zona Veredal de Buena Vista en Mesetas, Meta, **Aldinever Morantes, del Bloque Oriental, habló con Contagio Radio** sobre la situación del país, el retorno a la vida civil de cientos de integrantes de esta guerrilla, la realidad del paramilitarismo en Colombia y pidió perdón a las víctimas por los daños ocasionados en más de 50 años de guerra.

Aldinever se define como un guerrillero más, “común y corriente”, pese a ser uno de los guerrilleros más importantes en la actualidad y que **hace parte del Estado Mayor del Bloque Oriental.**

Proveniente de una familia cundiboyacense, humilde que fue afectada por el desplazamiento, razón por la que **a los 14 años y esperanzado en los fundamentos de las FARC, ingresó hace algo más de 27 años, a finales de los años 90.**

Es un hombre que le agradece a las FARC y a la “universidad de la vida” lo que sabe, pues solamente cursó hasta primero de primaria. Y **tanto tiempo después, cuenta que sigue con la ilusión ver una Colombia en paz.**

#### [**¿Cómo se sintió Aldinever al saber del inicio de los diálogos de paz?¿Cómo fue ese momento para usted?**] 

Pues es un momento que todos los guerrilleros hemos esperado porque las banderas de las solución política nacen del mismo pueblo y de la insurgencia, no de la contraparte. Desde que se fundó las FARC en 1964 hicimos una propuesta de transformación política y social del país y a partir de ahí las FARC siempre ha planteado la solución política al conflicto. Entonces **al principio nadie pensaba que iba a haber un acuerdo, afortunadamente paulatinamente se fueron dando las cosas.**

#### [**¿Con Juan Manuel Santos tuvieron dudas en hacer este proceso de paz?**] 

No dudamos porque con cualquiera se tenía que firmar un proceso de paz, porque ¿con quién se firma la paz? **La paz no se firma entre amigos, la paz se firma entre enemigos** y este Gobierno ha sido uno de los contendores más acérrimos que ha tenido las FARC en su historia de existencia.

#### [**¿Qué sentimientos hay en la actualidad en las FARC?**] 

Nosotros no tenemos miedo, de pronto algo de preocupación. Miedo no porque precisamente nos volvimos guerrilleros y las FARC surgió pensando en un proyecto político y hemos estado preparados toda la vida, para llegar el momento de implementar lo que es el acuerdo político como al que llegamos en este caso. Por su parte **el temor existe porque en este país hay muchas historias, como la de la Unión Patriótica en el que hubo muertos, violencia.** Entonces uno dice está vez no se puede repetir lo que pasó en los años 80, porque fracasar un proceso de paz tan importante como este y volver a atrás a una etapa de violencia yo creo que el país no está en esas condiciones.

#### [**¿Qué piensan del apoyo al proceso de paz?**] 

Todo el mundo quiere este proceso, todo el mundo quiere sacar adelante este proceso. Todos los colombianos y el mundo, por eso encontramos países, la comunidad internacional, la iglesia **apoyando para que este proceso de paz florezca y salga adelante.** Entonces vemos con buenos ojos la posibilidad de desarrollar el acuerdo y que Colombia como país salga adelante.

#### [**¿Cómo hacer con el sector que sigue diciendo que no le apuesta a la paz y a la propuesta que se ha hecho en La Habana?**] 

Tampoco se puede negar que en Colombia el sector mayoritario es el que quiere la paz y si entre todos trabajamos en esa dirección **no habrá nadie que se pueda oponer y que pueda hacer fracasar este proceso.**

#### [**¿Cómo se ha visto las FARC en un futuro?**] 

**Las FARC nació producto del pueblo y las FARC son pueblo,** entonces dentro de 1 año, 2 años o 5 o 10 años, las FARC van a ser pueblo. Es que las FARC está en la conciencia de cada colombiano pobre que quiere la transformación de un país, ahí van a estar las FARC transformadas en un movimiento político, luchando pero además solucionando los problemas estructurales y raizales de la gente. Le puede interesar: [En fotos | Las nuevas vidas en la Zona Veredal de Buenavista en Mesetas, Meta](https://archivo.contagioradio.com/en-fotos-la-vida-en-la-zona-veredal-en-buena-vista-mesetas/)

#### [**Detrás de micrófonos nos hablaba de un proyecto de memoria que las FARC tienen, cuéntenos un poco de ese tema.**] 

Nosotros tenemos la responsabilidad moral de reconstruir la memoria de lo que ha sido propiamente la vivencia de las FARC. A lo largo de estos 52 años han sido varios los compañeros nuestros y del sector popular que han muerto, y por eso tomamos la decisión de reconstruir la memoria de cada uno de nuestros líderes, hombres y mujeres que han jugado un papel importante en la construcción de otro país.

#### [**Las víctimas de este conflicto también han resaltado la importancia de trabajar la memoria ¿Por qué es importante hablar de memoria?**] 

Pensamos que hacer memoria nos permitiría primero **no olvidarnos que Colombia sufrio una confrontación de  más de 50 años,** pero también estaríamos todos comprometidos a que no se vuelva a repetir.

#### [**En diversas entrevistas varios integrantes de las FARC nos han manifestado la palabra “sacrificio” y aseguran que van a seguir parados, cumpliendo los compromisos pese a las situaciones adversas encontradas en la Zonas Veredales ¿cómo hacen para persistir?¿Por qué vale la pena resistir, seguir ahí al frente?**] 

Por la solución de un conflicto como el colombiano vale la pena sacrificarse y no solamente nosotros, es que todos los colombianos tenemos que sacrificarnos. Yo supongo que ustedes están haciendo un sacrificio para estar aquí y llevar esta historia, no es fácil, en un verano como estos, debajo de un camping, eso es un sacrificio y ustedes lo están haciendo supongo por la construcción de un nuevo país, pues nosotros también hacemos lo mismo (…) **el sacrificio que sea necesario nosotros lo vamos a seguir haciendo por la construcción de un nuevo país** y por el cumplimiento de los acuerdos.

#### [**Hay muchos retrasos en la implementación de los Acuerdos de Paz, las Zonas Veredales no están adecuadas, el Congreso está legislando de manera lenta, la sustitución de cultivos de coca no ha comenzado ¿Qué va a hacer ahí las FARC para que la implementación se haga según lo acordado en La Habana?**] 

Es un proceso complejo y pues nosotros como contraparte y como uno de los elementos debemos exigir que la contraparte cumpla. Pero ahí está lo grueso del asunto, y es que los medios de comunicación, el pueblo, los sectores políticos y sociales de este país ayuden a presionar para que **la contraparte ponga de una vez por todas sobre la mesa la implementación de los acuerdos.** Ya no le podemos hacer cambios a los acuerdos y confiamos que por la vía ‘Fast Track’ el Congreso pueda legislar para darle salida a cada uno  de los puntos del acuerdo. Seguiremos sacrificándonos por los acuerdos y aspiramos a que la contraparte cumpla de una buena vez. Le puede interesar: [Inicia campaña de donaciones para guerrilleros de las Zonas Veredales](https://archivo.contagioradio.com/inicia-campana-de-donaciones-para-guerrilleros-de-las-zonas-veredales/)

#### [**¿Cómo hacer para que desde los medios de comunicación seamos aportantes a la paz?**] 

Los medios de comunicación han jugado un papel en la guerra, para un lado o para el otro, entonces hoy en día deben ser conscientes que deben comenzar a generar las condiciones propicias para que se dé entre todos una buena actividad en función de materializar este acuerdo de paz. Porque si los medios de comunicación se ponen solo a favor de una parte por intereses mezquinos, pues este proceso de paz no sale adelante.

#### [**Hemos conocido que en diversas zonas del país se está dando un rearme paramilitar ¿Qué analiza las FARC frente a esta difícil situación?**] 

Lo más importante para nosotros es que usted lo está diciendo y usted es un medio de comunicación. Yo lo que sugiero es que eso hay que decírselo al país y al mundo, hay un acuerdo sobre seguridad que no es solamente la de las FARC sino la global, en el país, que tiene que ir más allá de la seguridad de los dirigentes que nazcan de las FARC, sino del conjunto de la sociedad. **El Estado debe eliminar toda actividad criminal, paramilitar, mafiosa, delincuente** que ha habido en este país. Le puede interesar: [Zonas Veredales bajo la sombra paramilitar](https://archivo.contagioradio.com/zonas-veredales-bajo-la-sombra-paramilitar/)

#### [**¿Cuál sería el mensaje para la sociedad?**] 

El mensaje para el pueblo es que el proceso de paz y el acuerdo como tal hay que continuar desarrollándolo y hay que apoyarlo. Yo sé que ningún colombiano quiere volver a la guerra. Hay un sector muy pequeño que habla no estar de acuerdo con el proceso, bueno ellos sabrán porque lo dirán, pero yo sé que **la mayoría de la población se la juega por el proceso y a esa mayoría yo la invito a seguírsela jugando** si queremos un país mejor para las presentes y futuras generaciones de colombianos.

#### [**¿Un mensaje final para las víctimas?**] 

Para la gente que de una u otra manera afectamos dentro de la actividad propia de la guerra, **todas nuestras disculpas y que no se preocupen que muy pronto vamos a tener** en el desarrollo de la Justicia Especial para la Paz **toda la verdad** y donde tengamos responsabilidad **nosotros de corazón vamos a ofrecer nuestras disculpas y vamos a hacer la reparación.** Disculpándonos, poniéndoles la cara a las víctimas que fueron afectadas por nuestra responsabilidad. Y para todas esas comunidades, nuestras felicitaciones por haber resistido estos años de guerra. Le puede interesar: [Listos los procedimientos para amnistía iure para FARC-EP](https://archivo.contagioradio.com/listos-los-procedimientos-para-amnistia-iure-para-farc-ep/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
