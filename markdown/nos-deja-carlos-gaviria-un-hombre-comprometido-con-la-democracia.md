Title: Nos deja Carlos Gaviría, un hombre comprometido con la democracia
Date: 2015-04-01 04:44
Author: CtgAdm
Category: Nacional
Tags: abogado, carlos gaviria, murio, Polo Democrático
Slug: nos-deja-carlos-gaviria-un-hombre-comprometido-con-la-democracia
Status: published

"*Me place mucho saludarlos*", era la frase con la que el ex magistrado Carlos Gaviria atendía cada llamada que desde hace más de cuatro años hacíamos desde Contagio Radio para consultar su opinión.

Hoy a los 77 años de edad murió el pedagogo, el abogado de grandes cualidad humanas y éticas, Carlos Gaviria Díaz, hombre que desde su profesión como abogado defendió las causas de los menos favorecidos en Colombia, profesor universitario, magistrado y político colombiano que desempeño varios cargos importantes a lo largo de su carrera,  entre ellos la presidencia del Polo Democrático Alternativo, Senador de la República y Magistrado de la Corte Constitucional.

Abogado que desde su profesión defendió la Constitución del 91, abordó y aplicó desde su trabajo el respeto  por la democracia. Así mismo un político que trabajo por una salida negociada al conflicto armado y que desde sus análisis dejo puesta su posición frente a la crisis en la Corte Constitucional, su oposición a la reelección presidencial, su preocupación frente al marco jurídico para la paz, su defensa por los derechos de las víctimas y de las minorías.

Hoy desde el equipo de Contagio Radio lamentamos la partida de Carlos Gaviria y recordamos con admiración a este defensor de la vida y de los derechos al que decimos "fue un placer enorme saludarlo".

[**Las memorables frases que nos deja Carlos Gaviria:**]

<ul>
-   “A mí me gusta es la educación y la libertad y a él (el presidente Uribe) le gusta la cárcel”.
-   "En un Estado de derecho a nadie se le puede privar de su libertad porque se fume un pucho de marihuana".
-   "Si la democracia es el gobierno de las mayorías, ¿cómo es posible que las mayorías estén desprotegidas y se encuentren en la pobreza o en la miseria?"
-   10 de abril de 2006, entrevista con la revista Semana.
-   "Uno de las grandes obstáculos que ha encontrado la izquierda es la guerrilla, porque cuando uno dice que es de izquierda, lo vinculan con la lucha armada. Es muy importante que la gente se saque de la cabeza que toda propuesta de izquierda tiene que ver con el uso de las armas. Lo he dicho en todos los foros. Ni la ética que yo profeso ni las convicciones filosóficas que tengo son compatibles con la lucha armada".10 de abril de 2006, entrevista con la revista Semana.
-   "Que nos investiguen: a mí por nexos con la guerrilla, a Uribe por nexos con ‘paras', a ver quién sale mejor librado" 30 de octubre de 2007, en respuesta a las palabras del Presidente Uribe en medio de la campaña para elecciones regionales de 2007.
-   "Soy un defensor a ultranza de la autonomía universitaria"
-   Audiencia Publica "En Defensa de la Calidad de la Educación Superior", Senado de la República.
-   "Yo no digo lo que digo por estar en la oposición, sino que estoy en la oposición porque creo en lo que digo" 9 de mayo de 2006, Medellín.
-   "Soy un liberal en el sentido más puro de la palabra"

</ul>

