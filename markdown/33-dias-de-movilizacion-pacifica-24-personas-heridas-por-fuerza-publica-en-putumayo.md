Title: A un mes de movilización 24 personas heridas por la fuerza pública en Putumayo
Date: 2016-08-22 12:40
Category: DDHH, Nacional
Tags: Amerisur, ESMAD, Movilización, Petroleras, Putumayo
Slug: 33-dias-de-movilizacion-pacifica-24-personas-heridas-por-fuerza-publica-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Putumayo-movilización.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [22 Ago 2016] 

Tras 33 días de movilización pacífica en cinco municipios de Putumayo contra el accionar de la petrolera Amerisur y de la insistencia del Gobierno de continuar con la erradicación forzada de cultivos de uso ilícito, las comunidades denuncian **agresiones y vulneraciones de derechos humanos y del Derecho Internacional Humanitario** por parte de la fuerza pública, con un saldo de 24 personas heridas.

En el municipio de Puerto Asís, puntualmente en el Corredor Puerto Vega-Teteye, el pasado viernes se desplegó un contingente de por lo menos 1.300 miembros del ESMAD y de la fuerza pública, quienes **agredieron a la población con gases lacrimógenos y artefactos letales, como canicas, recalzadas, balas de goma y esquirlas de balas no convencionales**, hiriendo a 6 pobladores, 2 de ellos por lo menos de gravedad.

Lola Camayo comunera indígena, denuncia que Herney Mestizo y Wilmer Lago fueron heridos de gravedad y permanecen hospitalizados, a causa de los **disparos que impactaron sus extremidades inferiores**, mientras impedían el paso de una de las tractomulas de la petrolera que estaban siendo custodiadas por miembros de la fuerza pública. Los líderes aseguran que no suspenderán la movilización hasta que el Gobierno cumpla.

En el municipio de San Miguel, en la vereda Puerto Colon, el pasado martes efectivos del ESMAD lanzaron **gases lacrimógenos e hirieron a cinco pobladores**, por su parte la Policía retuvo por 24 horas a otros 4. Este sábado mientras se realizaba un sepelio en el cementerio del municipio, miembros del ESMAD agredieron a la población.

En el corregimiento San Pedro, del municipio de Puerto Caicedo, el pasado martes los campesinos que se movilizaban hacia Puerto Asís, sin impedir el paso de trasporte público, fueron agredidos por el ESMAD que durante 4 horas lanzó gases lacrimógenos y bombas aturdidoras, **entró a la fuerza al casco urbano y desde algunas casas arrojó armas no convencionales**.

Agresiones similares se presentaron el jueves contra las comunidades que se movilizaban en la vía que conduce de Puerto Asís a Mocoa, con un saldo de 12 personas heridas, entre ellas 2 menores de edad con afectaciones respiratorias, dos campesinos de la tercera edad y ocho personas más, quienes fueron **afectadas en diferentes partes del cuerpo con golpes de bolas de goma y de cristal**.

En la vereda La Alea, del municipio de Puerto Asís, el pasado miércoles en la noche por lo menos **300 miembros de la Policía Nacional intimidaron a la población con armas de largo alcance** y junto al ESMAD se instalaron en la cancha de fútbol. Así mismo, sobrevolaron la zona en helicópteros a menos de 4 metros de altura. El pasado sábado, en el Municipio de Orito, en el punto de concentración del Yarumo, el ESMAD **atacó a los manifestantes con gases lacrimógenos e ingresó al Resguardo La Cristalina con armas de largo alcance**, amedrentando a la población.

Las comunidades hacen un llamado a las personerías y alcaldías municipales, así como a los órganos de control departamental y nacional para garantizar que la **fuerza pública respete el Derecho Internacional Humanitario** y la protesta social, pues aseguran que [[continuarán en las carreteras](https://archivo.contagioradio.com/esmad-reprime-violentamente-movilizacion-en-putumayo/)] hasta que se logren cumplimientos por parte del Gobierno.

<iframe src="http://co.ivoox.com/es/player_ej_12626288_2_1.html?data=kpejlJuWfJmhhpywj5WdaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5ynca3jzcaYpcbRpdrjhpewjcjTsdbixtfOjbXZuNbhwt7cj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

https://youtu.be/G7zNSoYPTfo  
 
