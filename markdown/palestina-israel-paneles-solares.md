Title: Gobierno Israelí confisca paneles solares donados por Holanda a Palestina
Date: 2017-07-07 14:39
Category: Onda Palestina
Tags: Apartheid, Israel, ocupación, Palestina
Slug: palestina-israel-paneles-solares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Palestine-Solar-Panels-to-generate-electricity1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:BDS 

###### 07 Jul 2017 

El gobierno de los Países Bajos reaccionó con indignación ante la **confiscación por parte de autoridades israelíes de docenas de paneles solares**, que habían sido donados por ellos al pueblo de Jubbet al-Dhib, al este de Belén, en Cisjordania.

La incautación se dio con el argumento que fueron construidos sin los permisos requeridos por Israel, quien mantiene más de la mitad del territorio de Cisjordania bajo control militar completo. Los palestinos señalan que **los permisos de construcción para los nuevos hogares palestinos y la infraestructura son casi imposibles de obtener**.

El alcalde del pueblo dijo que los paneles fueron destruidos, aunque Comet-ME, la organización de ayuda que instaló los paneles, dijo que entre **60 y 90 fueron retirados intactos y otros equipos en el sitio destruidos y dejados por Fuerzas israelíes**.

**El Ministerio de Relaciones Exteriores de Holanda pidió que los equipos sean devueltos a Jubbet al-Dhib** y está considerando qué "próximos pasos se pueden tomar", según un informe publicado en el diario israelí Haaretz el sábado.

Para conocer sobre más noticias de Palestina les invitamos a escuchar esta emisión de [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/) en la cual se discuten además noticias del movimiento de Boicot Desinversiones y Sanciones a Israel, así como entrevistas, cultura y mucha música.

<iframe id="audio_19643463" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19643463_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
