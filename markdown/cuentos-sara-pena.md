Title: Cuentos  - Sara Peña
Date: 2015-01-23 23:35
Author: CtgAdm
Category: Viaje Literario
Tags: literatura, poesia, Sara peña
Slug: cuentos-sara-pena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/descarga-9.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Imagen: Sara Peña 

<iframe src="http://www.ivoox.com/player_ej_3992018_2_1.html?data=lJ6mlJWVfI6ZmKiakpuJd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmsrVy8qYzs7YqdPV087cjZKPl8LmwpC9x4qnd4a2ksaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
"Escritora natural, su sensibilidad por la humanidad con apuestas por construir un mundo desde lo bello, lo simple, desde la ternura de niña expresada en cuento  hacen de esta joven y sus historias un escape para el alma".
