Title: Asesinato de docentes en el norte del Cauca no se detiene
Date: 2018-11-02 17:30
Author: AdminContagio
Category: DDHH, Nacional
Tags: amenazas a líderes sociales, audiencia minera en el cauca, Docentes
Slug: continuan-asesinatos-contra-docentes-al-norte-del-cauca
Status: published

###### Foto:  @RigoMarulanda 

###### 2 Nov 2018 

El pasado 1 de noviembre, mientras se desplazaba del municipio de Suárez, a donde había sido trasladado por su seguridad, hacia su lugar de residencia en Morales, Javier Ancizar Fernández recibió un impacto de bala que acabó con su vida, sumándose a la creciente lista de docentes asesinados en el departamento del Cauca.

Ancizar, reconocido por haber sido directivo de ASOINCA y un destacado líder sindical de la región, había aparecido en dos panfletos amenazantes meses atrás. En el más reciente, su nombre aparecía junto al de otras personas incluido el alcalde de Morales y otros docentes. Por tal motivo la Secretaría de Educación optó por trasladarlo pero a un sector cercano, dejándolo parcialmente en la misma zona de peligro.

Víctor Hugo Jiménez, colega de Ancizar e integrante de la Asociación de Institutores y Trabajadores de la Educación del Cauca (ASOINCA) afirma que “la Unidad Nacional de Protección, entidad que dictamina el nivel de riesgo, ya le había otorgado un nivel de riesgo extraordinario y que la Secretaria de Educación debió trasladarlo a una zona más apartada donde tuviera mayores garantías”.

**Negligencia desde las entidades**

A pesar que existen diálogos con la Policía y la Fiscalía, estas no le han aportado a los docentes alguna respuesta efectiva frente a la coerción que ejercen los denominados Grupos Armados Organizados (Gaos),  justificando una negligencia en las instituciones del Estado antes las repetitivas denuncias que Asoinca ha realizado.

“Asesinan a un compañero docente y a los cinco minutos sale una declaración del Gobierno o la Policía diciendo que fue por robarlo, pero los artículos aparecen al lado de los cadáveres lo que desvirtúa las investigaciones serias y los pronunciamientos del Gobierno” cuestiona Jiménez.

**Sistematicidad en los ataques**

La muerte de un docente indígena en la misma carretera donde fue asesinado Javier Ancízar una semana después, sumadas a los constantes amedrentamientos que padecen, prueban que se trata de un ataque sistemático contra quienes ejercen la labor de la docencia y que “por su accionar no puede terminarse como están terminando los líderes: con un tiro en su cuerpo” señala Jiménez.

Según los registros de ASOINCA en la actualidad son un total de 25 docentes amenazados y otro gran grupo que aguarda a que la Secretaría de Educación los traslade de forma efectiva a un lugar donde se les brinde garantías de seguridad, lejos de la situación que viven  en un departamento como el Cauca donde el conflicto armado sigue vigente.

<iframe id="audio_29802082" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29802082_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
