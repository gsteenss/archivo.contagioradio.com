Title: Aislamiento inteligente debe estar basado en la verdad y no en sub registros
Date: 2020-04-06 16:40
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Aislamiento Inteligente, Covid-19
Slug: aislamiento-inteligente-debe-estar-basado-en-la-verdad-y-no-en-sub-registros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Aislamiento-inteligente-o-preventivo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Víctor de Currea/Presidencia de la República {#foto-víctor-de-curreapresidencia-de-la-república .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Académicos, expertos y ciudadanos respondieron a las declaraciones del presidente Iván Duque sobre flexibilizar las medidas de aislamiento preventivo y pasar a un 'aislamiento inteligente', en el marco de la lucha contra el Covid-19. Para el médico y profesor de la Universidad Nacional Víctor de Currea Lugo, hay 5 elementos que debería tomar en cuenta el Presidente, antes de decidir sobre la situación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Aislamiento inteligente no está basado en estudios serios sobre el impacto del COVID en Colombia**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En primer lugar, de Currea sostiene que no se entiende qué es aislamiento inteligente: ¿Por zonas de las ciudades o para permitir la operatividad de más sectores productivos? En la misma medida, cuestiona si el aislamiento preventivo que está vigente no es ya inteligente, en cuyo caso, el problema es que no está clara la definición del término.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En segundo lugar, señala que en este momento hay personas que no han podido entrar en aislamiento porque ello significa dejar de comer o ser sacados de sus lugares de vivienda como ha sido el caso de vendedores informales en sectores de Bogotá como la localidad de Santafé, Los Mártires o Suba. Pero igualmente, en departamentos como Chocó o Nariño que no ha podido cubrir con asistencia a sus pobladores, lo que los obliga a seguir buscando diariamente su alimentación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En tercer lugar, el experto cuestiona que la prioridad para tomar tal decisión (flexibilizar el aislamiento) podría obedecer a que la prioridad es la economía: "hay gente que plantea que sin economía no hay personas, yo digo que sin personas no hay economía". Asimismo, refuta que con el aislamiento se pueda cumplir con que las personas estén distanciadas un metro una de otra, o que todos cuenten con elementos de bioseguridad necesarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El quinto, "y más grave" elemento según De Currea es que "no tenemos datos reales de lo que pasa". La afirmación se sustenta en que los datos sobre contagio y muertes son una fotografía del pasado, y varios mandatarios locales han expresado demoras de hasta 8 días en los resultados de exámenes de Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si a ello se suma que hay un subregistro tanto de casos confirmados como de posibles muertes por el virus, debido a la falta de capacidad que se tienen para tomar muestras, el escenario se hace más difícil para tomar decisiones sobre un supuesto cambio en la curva de crecimiento de infectados. (Le puede interesar: ["La crisis económica no se resuelve con limosnas: Alirio Uribe"](https://archivo.contagioradio.com/la-crisis-economica-no-se-resuelve-con-limosnas-alirio-uribe-munoz/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Lo que se debe hacer: Mantener la cuarentena y la renta básica**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para de Currea, la cuarentena debe mantenerse y en consonancia con organizaciones sociales y defensoras de DD.HH., asegura que debería brindarse un ingreso mínimo o renta básica "que le permita vivir a la gente". Adicionalmente, afirma que se deben hacer más exámenes porque "hasta que no sepamos la verdad de lo que está pasando no podemos tomar una decisión".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "La arrogancia del Presidente es tan grande que le ofrecen dos máquinas y las rechaza"

<!-- /wp:quote -->

<!-- wp:paragraph -->

Currea también critica la arrogancia del Gobierno, al que Venezuela le ofreció dos de las máquinas que importó para practicar pruebas de Covid-19, pero las rechazó, y ahora mandatarios como el gobernador de Magdalena Carlos Caicedo, o el alcalde de Cali Jorge Iván Ospina intentan aceptar la oferta para aumentar la cantidad de pruebas que se practican en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A renglón seguido, Currea enfatiza que "Semana está haciendo publicidad y periodismo", refiriéndose a una [entrada](https://www.semana.com/confidenciales/articulo/la-cuarentena-esta-funcionando-en-colombia/661500)en el portal en la que se afirma que el aislamiento preventivo ya tuvo efecto porque redujo la curva de infectados. Sin embargo, el médico recuerda que "no hay elementos de juicio" para llegar a esa conclusión.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La realidad de lo que puede significar el Covid-19 en Colombia**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Con el ánimo de ser realistas y hablar sobre hechos concretos, no para generar pánico, de Currea aplicó un ejercicio de matematica para asumir qué tanto podría afectar el Covid-19 en términos de vidas humanas a Colombia tomando en cuenta que "no hay evidencia científica que permita pensar que el virus se porta diferente en América Latina".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así, si se aplica una tasa de mortalidad del 4%, "que es muy baja porque en Italia es del 11%", y se calcula que según datos del Instituto Nacional de Salud (INS) en el país tendremos 3,9 millones de infectados, entonces la tasa de mortalidad significaría que cerca de 160 mil personas morirían. (Le puede interesar: ["En cuarentena mujeres temen ser víctimas de violación por parte de la Policía"](https://archivo.contagioradio.com/en-medio-de-cuarentena-mujeres-temen-ser-victimas-de-violacion-por-parte-de-la-policia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si se reemplaza la tasa de mortalidad del 4%, por la de Alemania que tiene un sistema de salud garantista, quedaría en 1,2% lo que según datos del INS igualmente se traduciría en cerca de 40 mil muertes. Por esta razón, de Currea reitera la necesidad de estadísticas reales para tomar decisiones, porque estos datos significan vidas y no se pueden manejar como en el pasado se han manejado los datos de pobreza o empleo.

<!-- /wp:paragraph -->

<!-- wp:html -->

> Reducción del uso de transmilenio vs Estrato Social... saquen conclusiones. [pic.twitter.com/BQVrb3emcx](https://t.co/BQVrb3emcx)
>
> — STREAM FUTURE NOSTALGIA (@JuanxAcevedo) [April 5, 2020](https://twitter.com/JuanxAcevedo/status/1246809215370514432?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:paragraph -->

En redes sociales también aparecieron críticas sobre lo que sería la decisión de Duque por quienes la padecerían, porque son los sectores populares los obligados a ir a trabajar (y por ende más posibildiades de contagiarse), mientras las capas con mejores ingresos de la sociedad tienen posibilidades de teletrabajo entre otras opciones. Asimismo, se cuestionó la eficacia del aislamiento inteligente que se aplicó en el Reino de los Países Bajos, mediante hilos en twitter como el que puede revisar en este [enlace](https://twitter.com/ConKdeKenz/status/1247192122534625280).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
