Title: “Para mí será un orgullo dar la vida por mi pueblo mapuche" Celestino Córdova
Date: 2020-08-14 20:02
Author: AdminContagio
Category: El mundo, Nacional
Tags: Chile, Pueblo Mapuche
Slug: para-mi-sera-un-orgullo-dar-la-vida-por-mi-pueblo-mapuche-celestino-cordova
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/mapuche.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: [@TrawuncheMadrid](https://twitter.com/TrawuncheMadrid)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El líder espiritual mapuche Celestino Córdova que lleva cerca de 100 días en huelga de hambre, emitió este 12 de agosto un último mensaje. El mensaje va dirigido a todos los pueblos indígenas que continúan su lucha por el territorio, por su libertad y agrega que “para mí será un orgullo dar la vida por mi pueblo mapuche, por nuestra creencia espiritual que es sagrada por sobre todas las cosas”. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El líder mapuche fue condenado a 18 años de prisión por la muerte en 2013 de un empresario agrícola y su esposa, ahora, lo que pide Celestino Córdoba es ser trasladado para cumplir parte de su condena en su comunidad para ir a renovar su *rewe*, (lugar sagrado, en que el líder espiritual practica un rito anual que las autoridades espirituales mapuches deben cumplir).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

«El Estado chileno, a través de su institución policial, me despojó de mi *rewe*, de mi familia, de mi comunidad, de mi territorio, de todos mis pacientes, que les brindaba sabiduría, les daba vida y salud, siendo una autoridad espiritual mapuche», expresó el Machi en su audio.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/capucha_informa/status/1293033230162157569?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1293033230162157569%7Ctwgr%5E\u0026amp;ref_url=https%3A%2F%2Fwp.telesurtv.net%2Fnews%2Fagravamiento-estado-salud-lider-mapuche-celestino-cordova-20200811-0005.html","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/capucha\_informa/status/1293033230162157569?ref\_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1293033230162157569%7Ctwgr%5E&ref\_url=https%3A%2F%2Fwp.telesurtv.net%2Fnews%2Fagravamiento-estado-salud-lider-mapuche-celestino-cordova-20200811-0005.html

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El líder Córdova finaliza diciendo “solo espero que al Estado de Chile le sigan exigiendo de todas las formas de devolver nuestro territorio ancestral mapuche y toda deuda histórica con todos los pueblos originarios” y es que en las últimas semanas se vienen presentando diversas protestas por parte del pueblo originario en apoyo al líder mapuche y a los otros 26 presos que mantienen la huelga de hambre. La última protesta terminó en la detención de cuatro participantes y en enfrentamientos entre carabineros (policía de Chile) y mapuches.   

<!-- /wp:paragraph -->

<!-- wp:heading -->

La larga lucha por la tierra mapuche
------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El conflicto entre el pueblo mapuche, el Estado chileno y empresas que se encuentran en las tierras de este pueblo es de décadas e incluso siglos, y es que desde que se formó el Estado de Chile, los indígenas no recuperaron todos los terrenos de los que los españoles se habían apropiado y poco a poco fueron perdiendo más de sus tierras. (Le puede interesar: [La dictadura persiste contra el pueblo mapuche en Chile](https://archivo.contagioradio.com/la-dictadura-persiste-contra-el-pueblo-mapuche-en-chile/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Especialmente en la región de Araucanía, se realizan muchas actividades de explotación de recursos en la que los afectados son los pueblos originarios. Sin embargo, lo que buscan los indígenas es que haya un equilibrio entre la producción económica y los recursos naturales, explica Fernando Pairican, postdoctorante del Centro de Estudios Interculturales Indígenas de la Universidad Católica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otro problema que se suma es que siendo la población indígena chilena de alrededor de 12,7%, es decir más de 2 millones de personas, según censo del 2017, no se reconoce la existencia de los pueblos indígenas u originarios, por lo que no hay una inclusión adecuada y mucho menos “hay voluntad de cambiar las estructuras", agrega Pairican.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Racismo contra el pueblo mapuche
--------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Además, en las protestas que se vienen presentando, los enfrentamientos entre carabineros manifestantes y civiles son cada vez más violentos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque en una encuesta presentada por [Cadem-Plaza Pública](https://www.cadem.cl/encuestas/plaza-publica-no-343-10-de-agosto/) a principios de agosto, 93% de los encuestados está a favor de que a los mapuche se les reconozca en la Constitución y “76% está de acuerdo con que el pueblo Mapuche es discriminado”, la noche del 1 de agosto civiles con diversas armas y convocados por organizaciones de agricultores, buscaban desalojar a los indígenas aclamando **“el que no salta es mapuche”.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los civiles quemaron vehículos y aún con la presencia de carabineros, violentaron y golpearon a familias mapuche. Después de conocerse este suceso, en redes sociales se empezó a hablar del creciente racismo contra los mapuche tanto por parte de civiles como de carabineros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la luz de esta situación, el representante para Sudamérica de la máxima oficina de Derechos Humanos de la ONU llamó a investigar el uso excesivo de la fuerza. Igualmente alarmo sobre la discriminación y expresiones de odio contra el pueblo originario, afirmando que el único camino frente a estas tensiones es un diálogo “participativo y de buena fe”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El próximo 25 de octubre se realizará el plebiscito que tiene como fin evaluar la re elaboración de la Constitución. Uno de los puntos a tratar serán darle reconocimiento a los pueblos originarios y la creación del Consejo de Pueblos Indígenas.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
