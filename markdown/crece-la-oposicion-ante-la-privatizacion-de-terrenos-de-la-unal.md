Title: Terrenos de la UNal podrían quedar en manos de empresa holandesa
Date: 2016-06-16 18:16
Category: Educación, Nacional
Tags: plan de renovación CAN, pot bogotá, Universidad Nacional
Slug: crece-la-oposicion-ante-la-privatizacion-de-terrenos-de-la-unal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Unal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia Noticias UNal ] 

###### [16 Junio 2016 ] 

De aprobarse el proyecto de ley de renovación urbana del CAN y el plan de ordenamiento territorial de la actual administración capitalina, los terrenos de la Universidad Nacional en los que están los edificios Uriel Gutierrez, Camilo Torres, la primera fase del Hospital Nacional Universitario, y la segunda etapa del proyecto del Centro de Atención en salud estudiantil, quedarían en manos de la empresa 'Agencia Nacional Inmobiliaria Virgilio Barco Vargas'[. ]

Una medida que de acuerdo a la docente Beatriz Martínez, representante ante el Consejo Superior, **dejará en manos de empresas privadas los terrenos del Estado, afectando el fortalecimiento de la infraestructura de la Universidad, la cobertura y la calidad del alma mater.**

Según la docente podría existir una propuesta a las directivas de una alianza público-privada con una multinacional holandesa, que  tiene injerencia en la sede de Tumaco y que podría ofrecer prebendas a cambio de poder usar los terrenos de la sede de Bogotá.

La decisión, por una parte, depende del Congreso, y por otra, de la aprobación del plan de desarrollo en el que está interviniendo el vicerrector de la sede Bogotá, y según afirma Martínez, la comunidad estudiantil espera que el directivo "esté a la altura de la universidad, que pueda defender esos terrenos y **no se convierta en un mecanismo para que los especuladores inmobiliarios saquen sus proyectos adelante**. Para él va a ser un reto; hay muchas dudas en la comunidad, pero queremos confiar".

"La Universidad no se puede someter, ni puede dejar que este tipo de negocios, entre comillas, extorsiones, se den (...) **no podemos permitir que hayan negocios ni extorsiones con los presupuestos de las universidades públicas**, debemos exigir que la educación pública esté al alcance de todos los ciudadanos; que las universidades públicas puedan ofertar un servicio de calidad y que no sigamos con un modelo en el que se privilegie la demanda y no la oferta", concluye la docente, e insiste en que el destino de los terrenos dependerá de la movilización ciudadana en defensa de la educación pública.

<iframe src="http://co.ivoox.com/es/player_ej_11930551_2_1.html?data=kpamlZWZeZKhhpywj5aYaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5yncaPZwtnfy9-PkcLm1YqwlYqliM_Z25CajajTstTZy9SYtdrUqdPd0NeYt7ORaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
