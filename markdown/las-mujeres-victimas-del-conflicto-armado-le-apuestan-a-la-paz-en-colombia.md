Title: Mujeres víctimas del conflicto armado le apuestan a la paz en Colombia
Date: 2018-04-10 16:35
Category: Mujer, Nacional
Tags: acuerdo de paz, mujeres, Ruta Pacífica de Mujeres
Slug: las-mujeres-victimas-del-conflicto-armado-le-apuestan-a-la-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/mujeres-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 Abr 2018] 

El conflicto armado colombiano ha dejado por lo menos a 4.125.202 mujeres afectadas, tanto por violencias físicas y sicológicas, como por la ausencia de un Estado que permite la revictimización de las mujeres a la hora de buscar verdad, justicia y reparación. Sin embargo, han sido las mismas mujeres las que han logrado fortalecer y estructurar el movimiento de víctimas en Colombia y **continúan apostándoles porque finalmente sean estas las que estén en el centro de la implementación de los Acuerdos de Paz.**

### **Las mujeres la semilla de la esperanza** 

De acuerdo con Sandra Luna, integrante de la Ruta Pacífica de Mujeres, el camino hacia la búsqueda de la verdad ha sido arduo, “**no es fácil tener 8 millones de víctimas y que de ellas el 51% correspondan a ser mujeres”**. En ese sentido afirmó que el Estado se ha quedado corto a la hora de poder cumplir con la satisfacción de los derechos que han sido vulnerados.

Al mismo tiempo, desmintió que no es cierto que la reparación administrativa sea la única que buscan las víctimas, “hay un imaginario de que las víctimas solo buscan la plata y no es cierto”. Ejemplo de ello es que mujeres han reconocido que como parte de la reparación necesitan medidas de satisfacción y reconocimiento público de los actos cometidos durante el conflicto armado, **escuchar la verdad detrás de los crímenes de los que son víctimas y obtener justicia**.

“Las mujeres no solo se han quedado en el papel de víctimas, sino que han logrado sobrepasar este hecho y convertirse en mujeres constructoras de paz, han tejido gran parte de las estrategias de sostenibilidad de la sociedad”, afirmó Sandra Luna y agregó que son quienes se han vuelto actoras políticas de la construcción de paz. (Le podría interesar: ["Mujeres víctimas de la guerra interponen tutela en Medellín ante revictimazación"](https://archivo.contagioradio.com/mujeres-victimas-guerra/))

### **Las mujeres lucharán por la centralidad de las víctimas** 

Frente a los Acuerdos de paz de La Habana, Sandra manifestó que las mujeres han depositado una gran esperanza en la Comisión de la verdad, para que se aporte a una verdad, no solo para las víctimas sino para todo el país, **“necesitamos saber qué pasó, quiénes fueron los grandes autores, dónde están los desaparecidos**, qué pasó con las mujeres en el marco del conflicto, con el objetivo fundamental de la no repetición”.

En esa medida uno de los grandes retos para el movimiento de víctimas mujeres en Colombia es lograr que las instancias que se crean en el marco de la implementación de los Acuerdos, cuenten con la participación de mujeres y lograr que las mujeres sean beneficiarias directas del mismo.

<iframe id="audio_25248344" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25248344_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
