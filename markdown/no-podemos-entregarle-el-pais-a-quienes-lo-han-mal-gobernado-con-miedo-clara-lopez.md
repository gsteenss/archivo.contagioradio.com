Title: "No podemos entregarle el país a quienes lo han mal gobernado con miedo": Clara López
Date: 2018-01-17 12:51
Category: Nacional, Política
Tags: Clara López, elecciones, Elecciones presidenciales, Gustavo Petro, Humberto de la Calle, progresistas
Slug: no-podemos-entregarle-el-pais-a-quienes-lo-han-mal-gobernado-con-miedo-clara-lopez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/DTuKGcyWsAAnls0-e1516209046155.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Clara López] 

###### [17 Ene 2018] 

Tras haber definido el mecanismo de consulta popular para elegir el candidato único de la alianza entre Gustavo Petro, Clara López y Carlos Caicedo para llegar a la presidencia, la coalición **reiteró el llamado a Humberto de la Calle**, candidato del partido Liberal, para que se sume a la coalición progresista.

### **Progresistas buscarán una renovación en Colombia** 

Una de las grandes apuestas de esta coalición es el trabajo por el cumplimiento y la **implementación de los Acuerdos de Paz** alcanzados entre el Gobierno Nacional y las FARC. López manifestó que “las fuerzas que estamos buscando que la paz se pueda consolidar, no en una paz de unos años sino como una paz duradera, se tiene que cimentar en lo que le da paz al hogar y la familia”. (Le puede interesar: ["Primera coalición de la izquierda lanza lista unitaria de la decencia"](https://archivo.contagioradio.com/primera-coalicion-de-la-izquierda-lanza-lista-de-la-decencia/))

Por esto, reiteró que los sectores alternativos han propuesto que **debe ser el Estado el que cree criterios** de inversión social donde se ponga los social primero y se incentiven oportunidades de educación, salud y el fundamento del crecimiento económico para superar la desigualdad.

Además afirma que hay un sector de la política que **ha gobernado tradicionalmente el país con "miedo y violencia"**. Reiteró que "no ha habido ninguna edad de oro que la derecha pueda reclamar como etapa en la que el pueblo colombiano se haya sentido gobernado en función de sus intereses". Por esto hizo énfasis en que esos sectores están ofreciendo el mito del pasado mejor cuando se debe construir un futuro mejor.

### **Coalición invitó a Humberto de la Calle a que se una al proceso** 

Para la alianza de 3 partes, hay una cercanía programática con el candidato del partido Liberal por lo que en repetidas ocasiones **lo han invitado a que haga parte de la consulta** de la mano de ellos. Tanto Humberto de la Calle como la alianza progresista han coincidido en que es necesaria una gran coalición para “garantizar que al gobierno de Colombia llegue una opción progresista”.

### **Hay puntos en lo que no hay acuerdo con la alianza del Polo, los Verdes y Sergio Fajardo** 

Si bien los progresistas han manifestado que el Estado social de derecho es una apuesta de ambas coaliciones, López insistió en que hay una diferencia entre los dos al momento de crear políticas concretas. Dijo que no están de acuerdo, por ejemplo, **con la política pensional** que propone Fajardo “como una reforma cuyo eje central es el aumento de la edad de la pensión.” (Le puede interesar: ["El riesgo de los Acuerdos de Paz en la elecciones de 2018"](https://archivo.contagioradio.com/el-riesgo-de-los-acuerdos-de-paz-en-elecciones-presidenciales-2018/))

Tanto López como Petro y Caicedo concuerdan con la necesidad de una reforma pensional pero **sin aumentar la edad para que las personas se pensionen**. Además, manifiestan que debe haber una responsabilidad fiscal en el Gobierno “sin buscar recursos vendiendo el 25% de Ecopetrol como lo ha anunciado Fajardo”.

Finalmente, Clara López envió un mensaje a los colombianos en donde les dice que **“no se dejen llevar por los intereses”** teniendo en cuenta los resultados de las encuestas que califica como “firmas que son negocios de propiedad privada”. Por esto, desde el progresismo van a tratar de “abrirle los ojos a la gente” pues por medio de mentiras y miedo “han tenido sometido al pueblo colombiano”.

### **CNE le quitó el nombre al partido de Clara López** 

La candidata informó que el Consejo Nacional Electoral **le quitó la denominación al movimiento** de “Todos Somos Colombia” por lo que tendrán que cambiarlo. Dijo que “el CNE le entregó el nombre a un partido que se llamaba Fundación Evano con lo que me han obligado a cambiar de nombre en esta etapa de campaña”.

Por esto, la candidata **se identificará con el partido Alianza Social Independiente**. Ella calificó esta acción como una mala pasada por parte del Consejo Nacional Electoral en una etapa donde es difícil cambiar de nombre y cuando ya está la campaña electoral diseñada. “En qué queda la buena fe cuando uno inscribe ante la Registraduría un nombre y le dicen que todo está bien y 15 días antes de que se termine la inscripción de listas le cuentan que se lo van a dar a otro movimiento político”, argumentó.

<iframe id="audio_23208207" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23208207_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
