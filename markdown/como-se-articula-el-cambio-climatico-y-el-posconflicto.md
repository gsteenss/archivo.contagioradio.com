Title: ¿Qué tiene que ver el cambio climático y con el posconflicto?
Date: 2016-09-12 13:47
Category: Ambiente, eventos
Tags: Ambiente, derechos étnicos, posconflicito, Universidad Javeriana
Slug: como-se-articula-el-cambio-climatico-y-el-posconflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/ambiente-y-comunidades-e1473705635492.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Etnoterritorios 

###### [12 Sep 2016] 

El próximo 20 de septiembre se llevará a cabo el **Encuentro Nacional: derechos territoriales, transición hacia el posconflicto, compromisos frente al cambio climátic**o. Un espacio de discusión entre autoridades étnicas, la academia, el Estado y expertos internacionales, que busca articular el acuerdo de paz con los compromisos ambientales y los derechos colectivos.

La profesora Johana Herrera, investigadora y coordinadora del Encuentro, explica que se **está viendo de manera separada los acuerdos de paz y la problemática del cambio climático,** cuando realmente se trata de una oportunidad para generar las medidas necesarias frente a las demandas ambientales en concordancia con los derechos de las comunidades, siempre y cuando se tenga especial atención en los riesgos que también pueden existir.

“Es fundamental la participación de las comunidades, pues las áreas protegidas muchas veces coinciden con los asentamientos de estos pobladores. Lo que proponemos es que sea algo articulado, pues las Reservas forestales podrían estar en riesgo por lo que han llamado el Fondo Nacional de Tierras”, según la docente, las áreas de reservas naturales no se pueden tomar como áreas baldías pues ya allí ya existen “compromisos de conservación, y ya hay unos derechos y unos usos específicos del suelo por parte de las comunidades indígenas, afro y campesinas”, por tal motivo, **no pueden ser tierras para ampliación de frontera agrícola o adjudicación a terceros,** pues se estaría desconociendo que ya hay unos compromisos de adaptación y mitigación al cambio climático”.

Es así como este Encuentro Nacional planteará propuestas orientadas a una mayor inclusión de los pueblos afrocolombianos e indígenas en los Acuerdos de Paz y a que se garanticen los derechos territoriales de estas poblaciones. Igualmente, se establecerán acuerdos para que se cumpla **el compromiso que el Gobierno pactó para la reducción a un 20% de los gases de efecto invernadero para el 2030.**

El evento está dirigido a entidades gubernamentales, de la sociedad civil y del sector privado y es organizado por la Fundación Rights and Resource Initiative (RRI), la Embajada de Noruega, el Proceso de Comunidades Negras, la Pontificia Universidad Javeriana y la Asociación Ambiente y Sociedad.

<iframe id="audio_12875905" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12875905_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<iframe id="doc_29246" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/323759875/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
