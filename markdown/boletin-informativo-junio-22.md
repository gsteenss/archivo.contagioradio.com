Title: Boletín informativo Junio 22
Date: 2015-06-22 17:49
Category: datos
Tags: ACA presenta ley del Actor Colombiano, Alonso Osorio Dignidad Cafetera, Dignidad Cafetera se pronuncia crisis por monopolios, exiliados colombianos, Germán Rincón Perfetti., País Vasco, Plantón Hidrosogamoso desalojado forzosamente, Procurado General a declarar por persecución a comunidad LGTBI, Victimas exiliadas necesitan ser caracterizadas
Slug: boletin-informativo-junio-22
Status: published

*[Noticias del Día: ]*

<iframe src="http://www.ivoox.com/player_ek_4674661_2_1.html?data=lZuklpuadY6ZmKiakpuJd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZeWcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [22 jun 2015]

-Corte Constitucional solicita declaración del Procurador General de la Nación, por denuncias de persecución a parejas del mismo sexo al **solicitar información adicional** al momento de llevar a cabo el **proceso de unión solemne** por medio de notarías, **violando el derecho** a proteger la intimidad establecido en la sentencia **T-444** de **2014**. Habla el abogado defensor de DDHH de la comunidad LGBTI **Germán Rincón Perfetti**.

-Producir un kilo de café cuesta más de **5.300** pesos y se vende a **5.000** pesos. Las deudas no se pagaron y no hay control de los **5** **monopolios** importadores. Los precios de los fertilizantes no se han controlado y **tampoco hay control** sobre los 6 o **7 monopolios** de importación de insumos, además hay crisis en la producción de azúcar y panela agravada por los **TLC** y acuerdos con empresas que realiza el gobierno afirma **Alonso Osorio**, integrante de la junta directiva de **Dignidad Cafetera**.

-El pasado 20 de junio se llevó a cabo el **II Foro Internacional de Víctimas en el País Vasco**, donde se convocó aproximadamente a 50 personas, entre ellos colombianos y colombianas exiliados y autoridades locales, donde se llegó a la conclusión de la **necesidad de caracterizar a la población colombiana** que se encuentra en ese país, y así mismo, **implementar propuestas de reparación** de las víctimas del conflicto colombiano que se han visto obligadas a salir del país, **Mario Calixto**, habla sobre la persecución que viven los exiliados por cuenta de las embajadas.

-La Asociación Colombiana de actores y actrices **ACA**, organización que trabaja en defensa de los derechos de más del mil agremiados en el país, **socializará este martes 23 de Junio** el proyecto legislativo por medio del cual se crearía **la** **Ley del Actor Colombiano**, figura que establece un **marco jurídico** para el ejercicio de la profesión en el territorio nacional.

-Tras **3 meses** de pedir la presencia del gobernador de Santander para que interfiera en el proyecto de la **hidroeléctrica Hidrosogamoso**, la comunidad decidió realizar un **plantón de forma pacífica** frente a la puerta de la gobernación para que se les prestara atención, sin embargo, la respuesta de las autoridades fue el **desalojo forzoso**. **Nubia Anaya**, líder comunitaria relata cómo fue el desalojo.
