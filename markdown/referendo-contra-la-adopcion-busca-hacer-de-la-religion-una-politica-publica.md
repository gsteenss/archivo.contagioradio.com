Title: Referendo contra la adopción busca hacer de la religión una política pública
Date: 2016-12-13 17:42
Category: Entrevistas, LGBTI
Tags: Adopcion, LGBTI, referendo
Slug: referendo-contra-la-adopcion-busca-hacer-de-la-religion-una-politica-publica
Status: published

###### [Foto: Colombia Gay Parade] 

###### [13 Dic. 2016] 

El Referendo contra la adopción gay ya va por su segundo debate y ha suscitado posiciones a favor y en contra de este tema dado que tal y como se ha planteado, **dicho proyecto de ley convocaría a los colombianos a un referendo que buscaría prohibir la adopción de menores de edad a parejas del mismo sexo así como también a las personas que son solteras.**

**Para Mauricio Albarracín, abogado y activista LGBTI esta propuesta busca prohibir la adopción por parte además de los homosexuales a los separados y viudos** "el Referendo pretende privar a las familias colombianas de las mismas oportunidades de los heterosexuales" dijo.

**Así mismo, para Albarracín este referendo es “discriminatorio” puesto que viola los derechos de los niños y las niñas** “este es un viola no solo los derechos de los niños sino que también los priva de más posibilidades para ser adoptados (…) además rompe con una tradición colombiana y es que todas las familias tienen la misma habilidad, respeto, la misma protección por parte del Estado”.

De igual modo, no solo Albarracín sino diversos sectores aseguran que este referendo impone un modelo de familia **“realmente es un referendo que no beneficia a nadie**, la única persona beneficiada es la senadora Viviane Morales que tiene un proyecto político evangélico”.

Según Albarracín **“la senadora Viviane Morales usa este mecanismo de participación para promover los prejuicios y la discriminación y así beneficiarse electoralmente”.**

Para este abogado **someter el tema de la adopción a un referendo como estos no le aportaría “absolutamente nada” a la sociedad colombiana** “someter este asunto de este grado de polémica involucrando una minoría, asustando a la gente con prejuicios contra los homosexuales o tratando de usar a los niños como una herramienta política no le aporta nada al país” aseguró Albarracín.

Y concluye diciendo que **el promotor del referendo ya ha dejado ver “claramente” que es lo que se pretende con este mecanismo** “lo dijo Carlos Alonso Lucio, el propósito de este referendo es hacer lo cristianamente correcto, y eso es una confesión. Ellos lo que quieren hacer es que una religión particular sea la fuente de una política pública y eso es contrario a un estado social de derecho laico”. Le puede interesar: [Si están en contra de la adopción igualitaria revisen su ética y su moral](https://archivo.contagioradio.com/si-estan-en-contra-de-la-adopcion-igualitaria-revisen-su-etica-y-su-moral/)

<iframe id="audio_14944751" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14944751_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)

 
