Title: Asesinan a Hamilton Gasca Ortega y sus dos hijos en Piamonte, Cauca
Date: 2020-04-04 13:53
Author: CtgAdm
Category: Nacional
Slug: asesinan-a-lider-hamilton-gasca-ortega-y-sus-tres-hijos-en-piamonte-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Hamilton-Gasca.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Hamilton-Gasca-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Red de DD.HH. de Putumayo {#foto-red-de-dd.hh.-de-putumayo .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la denuncia de la Red de Derechos Humanos del Putumayo, los hechos ocurrieron en la madrugada de este 4 de Abril en la vereda La Consolata en el municipio de Piamonte, departamento del Cauca. Hamilton era integrante de la Asociación Sindical de Trabajadores Campesinos de Piamonte, filial de[FENSUAGRO](https://twitter.com/Fensuagro1976).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las primeras versiones del hecho señalan que por lo menos cuatro personas, fuertemente armadas llegaron hasta la vivienda de Hamilton y lo asesinaron con impactos de arma de fuego, también fueron asesinados dos de sus hijos menores de edad, un tercero alcanzó a ser auxiliado. Su esposa Maria José Arroyo sobrevivió al ataque y pese a que inicialmente se desconocía su paradero, hay información en el sentido que se encuentra viva.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Campesinos de ASINTRACAMPIC ya habían sido amenazados

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Comisión Intereclesial de Justicia y Paz, denunció que el pasado 05 de marzo, los integrantes de esta asociación campesina fueron amenazados de muerte por grupos armados presentes en la zona.  Y resaltaron que en la zona hace presencia el grupo armado conocido como “La Mafia”, responsable de los últimos asesinatos en el bajo Putumayo, así como integrantes de la disidencia del Frente Primero de las Farc. Vea también. [Masacre en Piamonte](https://www.justiciaypazcolombia.com/masacre-en-piamonte-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante estos hechos la asociación que defiende a los campesinos exigió a las “Agencias Internacionales y entidades de Estado defensoras de DDHH que corresponda, ubicar con precisión el paradero de MARÍA JOSÉ ARROYO y realizar las acciones necesarias tendientes a garantizar su vida e integridad física, toda vez que según información preliminar, sigue en la zona.”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además resaltaron que “urge que el Estado colombiano tome las medidas necesarias, para garantizar la vida e integridad física y psicológica de María José, de ASINTRACAMPIC, así como de todos los pobladores del municipio de Piamonte Cauca.” (Le puede interesar:["Hallan restos de Elder Daza, desaparecido por paramilitares en 2008 en Argelia, Cauca"](https://archivo.contagioradio.com/hallan-restos-de-elder-daza-desaparecido-por-paramilitares-en-2008-en-argelia-cauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar:[Asesinan al líder sindical Alexis Vergara en Cauca](https://archivo.contagioradio.com/asesinan-al-lider-sindical-alexis-vergara-en-cauca/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
