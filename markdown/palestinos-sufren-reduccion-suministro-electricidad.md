Title: Palestinos sufren por reducción en suministro de electricidad
Date: 2017-08-03 15:36
Category: Onda Palestina
Tags: Apartheid, BDS Colombia, Israel, Palestina
Slug: palestinos-sufren-reduccion-suministro-electricidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/crisis-en-GAza.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: eldiario.es 

###### 3 Ago 2017 

Van tres meses en que **Gaza sufre una reducción a su ya miserable suministro de electricidad**. En Gaza viven más de dos millones de personas por lo que se estima que **necesitan aproximadamente 450 megavatios** de electricidad diarios para tener energía las 24 horas.

Sin embargo, en la última década, **gracias al bloqueo de Gaza impuesto por el Estado de Israel, han tenido alrededor de 200 megavatios**, lo que resulta en frecuentes apagones. En los últimos meses, esa realidad ha empeorado. Según la organización de derechos humanos israelí Gisha, en estos meses **Gaza ha tenido solo entre 140 y 70 megavatios diarios**.

La actual agudización de la crisis tiene su origen primero en la disputa entre los dos grandes partidos políticos palestinos: **Fatah y Hamas**. Sin embargo, el problema también está en que l**a única planta de energía de Gaza ha sufrido muchos daños** gracias a los bombardeos por parte de Israel en el 2006 y el 2014. Esto significa que no produce toda la electricidad que podría producir.

Esto resulta en que la mayoría de la población **sufre apagones de 12 a 16 horas,** lo que afecta no solo a las familias en sus hogares sino también a los hospitales, lugares de trabajo y la infraestructura para el tratamiento de aguas residuales.

La crisis de electricidad se suma a la **tasa de desempleo entre jóvenes de 15 y 29 años** más alta del mundo, y a otros factores negativos como la gran escasez de agua potable, por lo que la calidad de vida gazatí no se puede calificar como muy alta. No sorprende por lo tanto que el mes de julio en **Gaza estuvo marcado por persistentes protestas en las fronteras del territorio asediado**.

<iframe id="audio_20273381" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20273381_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
