Title: Entre vigilias y Campamentos se hace llamado a la paz
Date: 2016-10-31 15:34
Category: Nacional, Paz
Tags: Campamento por la Paz, Vigilias por la paz
Slug: entre-vigilias-y-campamentos-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/vigilia-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [31 de Oct 2016] 

A pocas días de cumplirse un mes de la decisión sobre el referendo por la paz en Colombia, continúan sumándose iniciativas y acciones que persisten en que finalice el conflicto armado en el país. Una de estas acciones son las **vigilias** que se están realizando desde el 27 de octubre y que pretenden que personas sumen sus voluntades en escenarios más espirituales por la paz. **Hoy se realizarán vigilias en los llanos del Yarí y Bogotá**.

Estas vigilias además han sido convocadas por diferentes **sectores como las FARC-EP, comunidades eclesiásticas y la sociedad civil.** Una de las primeras que se llevó acabo, fue la vigilia en El Castillo – Meta, zona que vivió el conflicto armado y a donde llegaron aproximadamente 200 personas. Hoy en la Plaza de Bolívar se realizará “la Gran Vigilia por la Paz” desde las 6:00pm y se se espera que cientos de faroles y velas llamen a la paz.

En los llanos del Yarí e Ituango  la guerrilla de las FARC-EP realizará sus vigilias, diferentes miembros de organizaciones sociales y colectivas ya hacen presencia en estos lugares desde el 29 de octubre. En otras **zonas como el Catatumbo, Buenos Aires y Miranda – Cauca,  Remedios – Atioquia, entre otros ya se inician las acciones para sumarse a la “Gran Vigilia por la Paz”.**

Otra de las iniciativas que sigue tomándose el país son los Campamentos por la paz que ya suman 7 ciudades: Cali, Montería, Manizales, Armenia, Arauca, Cartagena y Bogotá. El 28 de octubre, el presidente Santos visitó el Campamento por la Paz, en Bogotá y le aseguró a los campistas que **“Diciembre estará en paz”**, mensaje que Catherin Miranda, ciudadana que se encuentra en el campamento, recibe como alentador y aviso de vivir la primera navidad en paz. Le puede interesar:["Presidente atiende llamado de Campamento por la Paz"](https://archivo.contagioradio.com/santos-campamento-por-la-paz/)

De igual forma, Catherine afirmó que estas iniciativas han tenido efecto por las maneras diferentes de hacer política **“La política en Colombia tiene que volverse a inventar y dignificarse y debe trascender de lo electoral.** Lo que estamos haciendo son acciones políticas que inciden y salen de lo tradicional"

A suvez, el equipo de conversaciones de las FARC-EP invitó a un grupo de delegados de iniciativas ciudadanas que se reunirán durante el día de hoy. **Esta semana irán diferentes artistas al campamento y se realizarán clases o cátedras por iniciativa de docentes en estos lugares.** Le puede interesar: ["Amenazados integrantes del Campamento por la Paz"](https://archivo.contagioradio.com/amenazados-integrantes-del-campamento/)

<iframe id="audio_13557892" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13557892_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU)
