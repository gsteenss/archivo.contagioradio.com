Title: Más de 300 grupos indígenas de Brasil podrían desaparecer por el Covid-19
Date: 2020-07-22 22:44
Author: AdminContagio
Category: Actualidad
Tags: Brasil, jair bolsonaro, Pueblos indígenas en Brasil
Slug: mas-de-300-grupos-indigenas-de-brasil-podrian-desaparecer-por-el-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Brasil.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: [@IWGIA](https://twitter.com/IWGIA)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Brasil es actualmente el segundo país más afectado por el coronavirus, superando los dos millones de contagios y cerca de 80.000 muertos. Sin embargo, de esta cifra también hacen parte las comunidades indígenas del país, pues según la Articulación de los Pueblos Indígenas de Brasil (APIB) se han registrado 16,803 casos confirmados por COVID- 19 y 544 muertes entre 137 pueblos indígenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Grupo Internacional de Trabajo sobre Asuntos Indígenas (IWGIA) la población indígena en Brasil es de 896,900 personas, distribuidos entre 305 grupos étnicos y dado que algunos de los grupos están integrados por menos de 1.000 personas, si no se toman medidas urgentes, la pandemia amenazaría con exterminar muchos de estos grupos por completo. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aún cuando los indígenas han mostrado responsabilidad al mantener el distanciamiento social y aplicar las medidas pertinentes para evitar los contagios. La tasa de mortalidad es casi el doble de la tasa nacional (12,6% versus 6,4%), como reveló APIB.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esto, indígenas y defensores de derechos humanos acusan al gobierno por no adoptar medidas eficientes para combatir la propagación de la enfermedad en los territorios y explican que mineros ilegales que invaden los territorios y los trabajadores de la salud que se han enviado para monitorear a las comunidades, están siendo los causante de que la cifra de contagios aumente. (Le puede interesar: [Asesinan a José Oliver Maya, niño indígena Awá en operativos de erradicación forzada](https://archivo.contagioradio.com/asesinan-a-jose-oliver-maya-nino-indigena-awa-en-operativos-de-erradicacion-forzada/))

<!-- /wp:paragraph -->

<!-- wp:heading {"customTextColor":"#1b79a1"} -->

Bolsonaro vetó leyes que favorecían a grupos indígenas del Brasil contra el COVID 
---------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Defensores de derechos han alarmado repetidas veces al gobierno de Jair Bolsonaro, no obstante, el 8 de Julio, **Bolsonaro vetó leyes aprobadas anteriormente por el Congreso que obligarían al gobierno a proveer a la población indígena con acceso a fuentes de agua seguras, camas de cuidados intensivos reservadas para casos de Covid-19 y distribución gratuita de las necesidades básicas.  **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente rechazó los fondos para los gobiernos estatales y locales que proveían ayuda de emergencia para los pueblos nativos. Bolsonaro justificó su decisión afirmando que las disposiciones eran "contra el interés público" e "inconstitucionales", ya que creaban gastos excesivos para el gobierno federal. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Diversos institutos y ONGs han calificado estas decisiones como un acto “criminal” que solo evidencia la poca importancia que representan las comunidades indígenas para el presidente.

<!-- /wp:paragraph -->

<!-- wp:heading {"customTextColor":"#1b79a1"} -->

El problema no termina ahí 
--------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Como si no fuera suficiente con la posibilidad de que el virus acabe con muchos de los pueblos indígenas, un nuevo estudio alerta que en la Amazonía brasileña han sido deforestados 3.069,61 km2, entre enero y junio, y que ha habido un crecimiento del 54% en los últimos diez meses más que en el periodo anterior, según el** **Instituto Nacional de Investigaciones Espaciales (INPE).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe recordar que Brasil ha reconocido 690 territorios para los indígenas, de los cuales casi toda esta reserva territorial, el 98,5% se ubica en la Amazonia. Pero con estas decisiones de deforestación, están quitándoles lo que les pertenece y amenazando la existencia y preservación de los más de 70 grupos indios aislados que se encuentran en el territorio.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
