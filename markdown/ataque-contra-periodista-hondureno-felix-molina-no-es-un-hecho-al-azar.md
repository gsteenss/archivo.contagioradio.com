Title: Ataque contra periodista hondureño Félix Molina "no es un hecho al azar"
Date: 2016-05-03 16:07
Category: El mundo
Tags: Feliz Molina, honduras, Libertad de Prensa, Reporteros Sin Fronteras
Slug: ataque-contra-periodista-hondureno-felix-molina-no-es-un-hecho-al-azar
Status: published

###### Foto: Timepo Honduras 

###### 3 May 2016

Honduras es uno de los países de América Latina y el mundo entero donde se violan los derechos a la libertad de prensa. Así se evidenció este lunes cuando el periodista **Felix Antonio Molina fue atacado este lunes, a manos de hombres que le dispararon en repetidas ocasiones.**

Según información de medios locales hondureños, Félix Antonio Molina, director del espacio comunicacional Alter Eco,  fue baleado cuando se transportaba en una unidad de taxi. A esta hora el periodista hondureño se recupera en el Hospital Escuela Universitario de Tegucigalpa, luego de ser intervenido  la anoche del lunes.

Bertha Oliva,  coordinadora del Comité de Familiares de Detenidos Desaparecidos de Honduras, aseguró además que en la noche del lunes  Molina fue víctima  de dos hechos  violentos, uno a mediodía y el segundo en la tarde.

**“Sabemos que lo que le ha pasado a Félix Molina hoy no es un hecho al azar, es parte de una programación y lo que significa que vendrán cosas más difíciles en este país, yo creo que pueden pasar distintas hechos a las voces opositores**. Ya la gente que está consciente de la realidad que vive el país, tanto a nivel de Honduras como a nivel internacional, están muy preocupadas por lo que sucede en Honduras”, sostuvo la defensora de derechos humanos.

Cabe recordar que según el más reciente informe de Reporteros Sin Fronteras, entre México, Brasil, Honduras y Guatemala hubo 21 periodistas asesinados en 2015, "**a los que se añaden numerosos ataques violentos y amenazas de todo tipo contra la libertad informativa",** dice el informe.

[![Muro-Felix-Molina](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Muro-Felix-Molina.jpg){.aligncenter .size-full .wp-image-23547 width="600" height="589"}](https://archivo.contagioradio.com/ataque-contra-periodista-hondureno-felix-molina-no-es-un-hecho-al-azar/muro-felix-molina/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
