Title: Camargo, una obra para repensar la violencia
Date: 2016-11-29 16:44
Category: Cultura, Nacional
Tags: Daniel Camargo, Teatro Reflexivo, Violencia contra las mujeres en Colombia
Slug: camargo-una-obra-para-repensar-la-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/WhatsApp-Image-2016-11-24-at-12.43.48-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Nov 2016 ] 

En el marco del III Festival Ni con el Pétalo de Una Rosa, organizado por Casa Ensamble, una de las obras más emblemáticas fue Camargo, que **lleva de la risa al llanto y del odio a la comprensión.**

Camargo despierta emociones opuestas entre la risa y la indignación y conclusiones azarosas entre la condena inmediata y el cuestionamiento. Esta obra escrita y protagonizada por Johan Velandia reconstruye la historia de “el monstruo de los manglares”, Daniel Camargo, **el asesino en serie que cobró más de 150 víctimas, entre mujeres y niñas vírgenes a quienes violó y estranguló en diferentes ciudades de Colombia y Ecuador.**

Velandia investigó la vida de este asesino y comenta que lo que le conectó con el personaje “fue descubrir que ambos estudiamos en el mismo colegio, eso me llevó a **querer explorar en los motivos que pueden determinar que un ser humano se convierta en un asesino serial,** y lo logré convirtiéndome yo mismo en ‘el monstruo de los manglares’ cada función.

### **¿Somos violentos por naturaleza?** 

La situación entre Daniel o Danielita, hijo de un padre alcohólico, maltratado por su madrastra Alcira quien sólo tenía gritos para comunicarse con él, lo vestía como una niña para llevarlo al colegio, le enterraba alfileres en las manos, entre otros episodios tormentosos, **evoca la reflexión persistente de los ciclos de violencia que se repiten en las familias al momento de la crianza.**

Ana María Sánchez quien interpreta a Alcira señala que uno de los objetivos más importantes de la obra es llevar al público a pensar sobre las relaciones entre padres e hijos, sobre la violencia latente en nuestro país **“porque no sabemos dar, no sabemos amar y terminan unos seres llenos de odio, locos, criando a otros locos”.**

Si al inicio se siente un repudio profundo por Camargo, durante el desarrollo de la obra el sentimiento se hace más agudo pero lleva a pensar que todos esos episodios atormentados de su vida que pasan ante los ojos, se conjugan para retratar a un sujeto que **“no es un ser irreal e infernal, sino un ser cuya humanidad ha sido dañada por su pasado”** resalta Ana María.

Esta pieza artística conformada también por Nelson Camayo y Diana Belmonte, busca que el público se adentre por unos minutos en la psique de un asesino en serie como Camargo, que encuentre **preguntas y respuestas sobre como la infancia de una persona influye radicalmente en lo que va a ser en sus años futuros** y las formas de pensar, actuar y sentir que se van construyendo dentro de sí.

También inevitablemente genera indignación y mucha conmoción puesto que los rostros de las mujeres víctimas de Camargo acompañan el escenario, **genera mucha rabia y dolor ver rostros de niñas desde los 8 a los 20 años que por una u otra circunstancia cayeron en las manos de este violador quien calló para siempre sus voces,** así que la obra también invita a la memoria, a no olvidar a aquellas mujeres y hombres que han sido víctimas de actos despiadados que quedan por años en la impunidad.

Por último la pertinencia de Camargo está en el espectro de rechazo a todas las formas de violencia contra las mujeres pero dentro de ello, invita a que sea tarea cotidiana el repensarnos como sujetos, el repensar como en nuestras relaciones cotidianas en casa, en el trabajo, en la calle, con las personas cercanas y nuestras parejas **legitimamos la violencia a través de nuestros actos y reproducimos sentimientos de rencor, desconfianza y odio sólo con la palabra dicha.**

###### Reciba toda la información de Contagio Radio en [[su correo]
