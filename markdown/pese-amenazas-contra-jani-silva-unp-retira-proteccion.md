Title: Pese a amenazas contra la lideresa Jani Silva, UNP retira una de sus medidas de protección
Date: 2020-12-10 17:10
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Amenazas contra defensoras de derechos, Jani Silva, Putumayo
Slug: pese-amenazas-contra-jani-silva-unp-retira-proteccion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-03-at-1.11.12-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Jani Silva / Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de las **amenazas que se han conocido en contra de la Asociación de Desarrollo Integral Sostenible Perla Amazónica (ADISPA),** conformada por las comunidades de la  Zona de Reserva Campesina en Putumayo y los hostigamientos e intentos de **atentados contra la lideresa Jani Silva**, la Unidad Nacional de Protección (UNP) anunció a la defensora de DD.HH. el retiro de una de sus medidas de protección.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La [Comisión Intereclesial de Justicia y Paz](https://www.justiciaypazcolombia.com/) organización que ha alertado sobre reiteradas amenazas de muerte, presiones, hostigamientos contra la vida e integridad de Jani y su familia denuncia que el pasado 25 de noviembre por medio de un correo electrónico, el Grupo de Desmonte de Medidas de la UNP, notificó a la lideresa **retiro de una de las medidas materiales de protección como es un chaleco. **

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La organización defensora de DD.HH afirma que la difícil situación de seguridad de la lideresa requiere implementar **medidas materiales de protección idóneas **de manera urgente y no el retiro de las mismas. [(Lea también: Denuncian nuevo plan para atentar contra lideresa Jani Silva en el Putumayo)](https://archivo.contagioradio.com/denuncian-plan-en-contra-de-la-vida-de-la-lideresa-jani-silva-en-el-putumayo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el mes de marzo y hasta inicios del mes de julio, se han realizado denuncias sobre un nuevo plan de atentado por parte de la estructura criminal identificada como La Mafia, en contra la vida de la lideresa promotora de la sustitución voluntaria de cultivos de uso ilícito y quien está protegida por medidas cautelares de la CIDH, institución que reconoce la situación de riesgo que viven los liderazgos en el Putumayo. ** ** [(Le puede interesar: Lideresa Jani Silva en riesgo tras descubrirse plan para atentar contra su vida)](https://archivo.contagioradio.com/lideresa-jani-silva-en-riesgo-tras-descubrirse-plan-para-atentar-contra-su-vida/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esta es la** segunda ocasión en que, pese a las evidencias de peligro, las instituciones actúan en contravía de la realidad que vive la lideresa.** En agosto de este año, la Fiscalía **cerró la investigación que se adelantaba por desplazamiento forzado **de Jani y su familia ocurrido en diciembre de 2017 concluyendo que en el caso se dio una "inexistencia de desplazamiento forzado". [(Lea también: Desplazamiento forzado de lideresa Jani Silva es negado por la Fiscalía)](https://archivo.contagioradio.com/desplazamiento-forzado-de-lideresa-jani-silva-es-negado-por-la-fiscalia/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Lideresas como Jani Silva están en particular riesgo en Putumayo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El más reciente informe entregado por el **Secretario General de las Naciones Unidas al Consejo de Seguridad**, menciona la preocupación en torno a la situación de los liderazgos sociales en el Putumayo, e**n particular los riegos específicos para mujeres defensoras y vinculadas a las iniciativas de sustitución de cultivos de uso ilícito.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según defensores de DD.HH. de la región, durante el aislamiento preventivo las agresiones contra defensores y defensoras fueron en aumento, incluyendo **9 asesinatos contra liderazgos en el primer semestre del 2020** en un departamento donde se han identificado actores armados como el Frente Carolina Ramírez, parte de las disidencias que no se acogieron al Acuerdo de Paz. [(Le puede interesar: Se agudizan amenazas contra defensores de DD.HH. en Putumayo durante cuarentena)](https://archivo.contagioradio.com/se-agudizan-amenazas-contra-defensores-de-dd-hh-en-putumayo-durante-cuarentena/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma hacen presencia en la región otros grupos como los Comandos de La Frontera conformado por exintegrantes de las Autodefensas Unidas de Colombia (AUC) y las FARC. De la misma manera esta estructura armada ha manifestado ser respaldada por sectores de la economía y de la Fuerza Pública del departamento.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ha sido este mismo grupo el que en recientes días se ha adjudicado un panfleto amenazante contra ADISPA y lideresas como Sandra Lagos, presidenta de la comunidad de Puerto Playa y delegada del Programa Nacional de Sustitución (PNIS) y PDET.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
