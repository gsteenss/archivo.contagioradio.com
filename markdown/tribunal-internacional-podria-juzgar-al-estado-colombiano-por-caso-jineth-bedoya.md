Title: Tribunal internacional podría juzgar al Estado colombiano por caso Jineth Bedoya
Date: 2019-01-29 16:47
Author: AdminContagio
Category: DDHH, Mujer
Tags: CIDH, Corte IDH, Gobierno, impunidad, Jineht Bedoya
Slug: tribunal-internacional-podria-juzgar-al-estado-colombiano-por-caso-jineth-bedoya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/JINETH-BEDOYA-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Audiencia CIDH ] 

###### [29 Ene 2019] 

La **Comisión Interamericana de Derechos Humanos** notificó la aprobación de su informe de fondo en el caso que se adelanta por las violaciones a los derechos de la periodista Jineth Bedoya. Hecho que marca un precedente según la Fundación para la Libertad de Prensa, **porque permite que se abra la puerta para que el Estado Colombiano sea juzgado por la Corte IDH** y se exponga la situación de las mujeres periodistas en el marco del conflicto armado.

**Jineth Bedoya** fue víctima de secuestro, tortura y violencia sexual el 25 de mayo del 2000, mientras adelantaba una investigación con delitos que involucraban funcionarios y miembros de las AUC. Sin embargo, pasados más de 18 años los adelantos en materia de justicia sobre los responsables intelectuales de estos hechos son nulos, razón **por la cual la periodista** [**acudió**]** a instancias internacionales en su persistencia por establecer justicia en el caso. ** (Le puede interesar:["Jineth Bedoya devuelve indemnización al Estado Colombiano"](https://archivo.contagioradio.com/jineth-bedoya-devuelve-indemnizacion-al-estado/))

Esta notificación según la **FLIP** y el Centro por la Justicia y el Derecho Internacional **CEJIL**, representa un paso muy importante en la incansable lucha que la periodista ha librado por la justicia y la verdad en su caso". Asimismo, las organizaciones aseguraron que esta también podría ser un primer paso para poner **freno a las dinámicas de impunidad generalizada en casos de violencias contra la mujer y de restricciones de hecho a la libertad de prensa.**

Finalmente, tanto la Fundación para Libertad de Prensa como el CEJIL, expresaron que si bien estos hecho se cometieron bajo otros gobiernos, esperan que el actual "cumpla con lo decidido por la CIDH a fin de garantizar los derechos de Jineth Bedoya y evitar la repetición de hechos de violencia contra las mujeres y su impunidad".

> Buscar justicia es una de las tareas más difíciles que se puedan asumir en la vida, porque cuesta la vida misma. Hoy hemos dado otro paso significativo en esa búsqueda, y confío en que el Estado responda con altura. [\#NoEsHoraDeCallar](https://twitter.com/hashtag/NoEsHoraDeCallar?src=hash&ref_src=twsrc%5Etfw) [@FLIP\_org](https://twitter.com/FLIP_org?ref_src=twsrc%5Etfw) [@fcarrilloflorez](https://twitter.com/fcarrilloflorez?ref_src=twsrc%5Etfw) [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) [pic.twitter.com/Vx1GgA2YxN](https://t.co/Vx1GgA2YxN)
>
> — Jineth Bedoya Lima (@jbedoyalima) [29 de enero de 2019](https://twitter.com/jbedoyalima/status/1090228447132205056?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
