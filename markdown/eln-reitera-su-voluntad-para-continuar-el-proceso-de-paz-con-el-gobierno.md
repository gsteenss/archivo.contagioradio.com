Title: ELN reitera su voluntad para continuar el proceso de paz con el gobierno
Date: 2017-01-04 09:22
Category: Nacional, Paz
Tags: Mesa de diálogo con el ELN, proceso de paz eln
Slug: eln-reitera-su-voluntad-para-continuar-el-proceso-de-paz-con-el-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/entrevista-eln.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ELN] 

###### [4 Enero 2017] 

En un comunicado de prensa el ELN, dio a conocer su balance de lo que fue el proceso de paz que se adelantaba con el gobierno y que por ahora se encuentra suspendido. En el texto el ELN afirma que “**Es evidente que al gobierno no le conviene abrir la Mesa Pública con el ELN, porque eso significa darle la palabra a la sociedad”**. Sin embargo señala que mantiene la voluntad para continua en este proceso

A su vez en el documento, la guerrilla hace referencia las trabas que se han generado para la continuación con la instalación de la mesa de conversaciones en Quito, en ella aclara que los dos indultos que habían solicitado fueron negados por el gobierno “para concretar los indultos, nuestra delegación postuló, inicialmente, dos compañeros condenados por retenciones, los cuales fueron rechazados por el gobierno, argumentando las limitaciones que impone la Ley 418 de 1997”. **Punto que para el ELN se constituye como un incumplimiento.**

Entre tanto Juan Camilo Restrepo reitero que se hace necesaria la liberación de Sánchez de Oca para continuar con este proceso. Frente a este hecho la guerrilla replico que las liberaciones humanitarias pactadas desde el pasado 6 de octubre se realizaron y que esta actitud por parte del gobierno es un intento por “dilatar” las conversaciones.

Sin embargo, en el comunicado el ELN expresó que realiza una invitación a la sociedad en general a pronunciarse y participar de esta fase del proceso de paz “También, extendemos el llamado a todas las colombianas y todos los colombianos, para que hagan suya esta Fase Pública de conversaciones, asumiendo su propio protagonismo y abriendo un nuevo escenario de participación política, para que podamos transitar hacia otro país, donde la paz se traduzca en transformaciones palpables de vida digna, justicia, soberanía y alegría para todas y todos”. Le puede interesar: ["ELN ratifica su compromiso con la paz"](https://archivo.contagioradio.com/eln-ratifica-su-compromiso-con-la-paz/)

En los últimos días, también se conoció una prueba de supervivencia de Sánchez de Oca en donde pide que se reanuden estas conversaciones lo más pronto posible. E**ntre tanto ambas partes han expuesto que están en la disposición de continuar con el proceso que podría tener un  re inicio el próximo 10 de enero**. Le puede interesar: ["Aspiro a que gobierno nacional resuelva este impase: Odín Sánchez"](https://archivo.contagioradio.com/aspiro-a-que-gobierno-nacional-resuelva-este-impasse-odin-sanchez-de-oca/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
