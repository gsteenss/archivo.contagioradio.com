Title: Aumenta la preocupación por la seguridad en campaña de Gustavo Petro
Date: 2018-03-05 16:36
Category: DDHH, Nacional
Tags: Cucuta, elecciones 2018, Gustavo Petro
Slug: aumenta-la-preocupacion-por-la-seguridad-en-campana-de-gustavo-petro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/gustavo_petro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [05 Mar 2018] 

Tras las denuncias que hizo el candidato a la presidencia Gustavo Petro, de ser víctima de un intento de asesinato cuando se encontraba en la ciudad de Cúcuta en un acto de campaña, Jorge Rojas, integrante de su comité de campaña, aseguró que hay un conjunto de preguntas frente a la actuación **por parte de la Fuerza Pública, la Alcaldía y las personas que se encontraban en el lugar.**

Rojas manifestó que están a la espera de los resultados de la investigación de la Fiscalía, sin embargo, aseguró que no consideran que los impactos encontrados en las ventanas del vehículo en el que se movilizaba Petro, fueran producto de piedras lanzadas, sino por el contrario de balas.

### **El accionar de la Fuerza Pública** 

Petro ha afirmado que el esquema de la Policía que estuvo a cargo de su seguridad lo llevó por una ruta que finalmente lo deja expuesto a mayores riesgos, evidenciados en los impactos de las ventas del carro en el que iba. (Le puede interesar: ["Candidatos y Partidos deben pronunciarse contra violencia en campañas: MOE"](https://archivo.contagioradio.com/candidatos-y-partidos-politicos-deben-pronunciarse-contra-violencia-en-campanas-moe/))

Rojas aseguró que hay una preocupación hacia esa institución que habrían permitido el ataque, su función “no era de proteger a los manifestantes y que más bien, permitieron que saboteadores ingresaran al Parque Santander” para generar caos, esto debido a que no habría guiado el vehículo lejos de la turba y por otra entrada al parque, sino por el lugar en donde se estaban presentando las agresiones.

Así mismo, responsabilizó al Alcalde de Cúcuta, César Rojas, que **“con su actitud no ayudó a garantizar la seguridad del candidato”** y que además, se pasaron por alto las normas para garantizar los derechos a la libre expresión y la reunión. En ese sentido Rojas denunció que el día previo al evento, la Policía no dejo instalar ni las vallas de seguridad, ni el sonido en la tarima, aumentando la situación de riesgo.

**Los buses contratados por Ramiro Suarez**

Otra de las denuncias hecha por Petro es que había buses con personas contratadas por el ex alcalde de Cútuca, Ramiro Suarez Corzo, para sabotear el acto de campaña, hecho que está en investigación por parte de la Procuraduría y de la Fiscalía, según Jorge Rojas, los grupos desaparecieron después de los actos de provocación.

Así mismo, Rojas aseguró que hay un temor al interior de quienes siguen a Gustavo Petro, debido a la historia de Colombia en donde han sido asesinados diferentes candidatos durante su campaña electoral, razón por la cual han pedido a la **CIDH que se planteé ampliar el tiempo de las medidas cautelares para Petro, debido al alto riesgo que afronta.**

<iframe id="audio_24247148" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24247148_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
