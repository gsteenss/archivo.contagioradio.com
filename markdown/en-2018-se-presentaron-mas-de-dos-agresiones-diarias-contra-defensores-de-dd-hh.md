Title: Agresiones contra defensores de DD.HH. aumentaron más de 40% en 2018
Date: 2019-04-23 08:15
Author: CtgAdm
Category: DDHH, Nacional
Tags: AGC, Agresiones contra defensores de DDHH, Economía Naranja, La Naranja Mecánica, Somos defensores
Slug: en-2018-se-presentaron-mas-de-dos-agresiones-diarias-contra-defensores-de-dd-hh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Colombia.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Líderess.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-02-25-at-9.02.58-AM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/SomosDefensores.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

Con su más reciente informe, titulado ['**La Naranja Mecánica**'](https://somosdefensores.org/wp-content/uploads/2019/04/informe-somos-defensores-2019-espanol-web.pdf), **Somos Defensores** revela que las fallas en la implementación de los acuerdos y la ausencia de medidas de seguridad  han sido unas de las razones que han desencadenado un escalamiento del conflicto armado y en la aparición de más grupos armados que buscan seguir acumulando territorios despojando a las poblaciones que los habitan, **presentándose un incremento en las agresiones contra defensores en un 43.75% en relación con 2017**.

El informe, que está dividido en cuatro ejes; analiza el cambio de Gobierno y lo que esto ha significado en materia de de paz y seguridad, las disputas territoriales derivadas del vacío que dejó la desmovilizada guerrilla de las FARC y la baja capacidad del Estado para responder a estas amenazas, finalmente el informe incluye un análisis de datos del Sistema de Información sobre Agresiones contra los defensores de DD.HH. en Colombia.

### Un nuevo Gobierno con diferentes prioridades

A pesar del contexto de transición en el Gobierno, que requería de una alta exigencia para implementar los acuerdos firmados con las antiguas FARC,  se evidenció un recrudecimiento de la violencia  en mayo cuando se realizó la primera vuelta y la segunda vuelta en junio, hasta el punto en que que **para el 7 de agosto de 2018, 97  personas defensoras de DD.HH. habían sido asesinadas y 591 habían sido agredidas.**

La situación se recrudeció con la llegada de la administración Duque, que definió como prioridades la economía y el emprendimiento dejando de lado el blindar la paz y garantizar  el cumplimiento de los acuerdos pactados, mostrando una predilección por las empresas privadas, de ahí que el informe se titule La naranja mecánica, pues como evidencia el informe, el  presidente Iván Duque ha demostrado que  **"está gobernando de frente a la economía y de espaldas a la paz**", además expone con preocupación que discursos como la negación de la existencia del conflicto armado, el desconocimiento de la sistemicidad contra personas defensoras se han convertido en un "juego retórico y mediático".

\[caption id="attachment\_65226" align="aligncenter" width="539"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Colombia.png){.wp-image-65226 .size-full width="539" height="583"} Foto: Somos Defensores\[/caption\]

### Lucha por los territorios

El informe también revela que "la defensa de la tierra y del territorio" es una de las causas que más tratan de ser silenciadas por diferentes actores, dichas agresiones, **presentes en 27 de los 32 departamentos del país están relacionadas directamente con la presencia de cultivos de uso ilícito**, actividades de extracción, procesos sociales fuertes, y cercanía a las antiguas Zonas Transitorias de Normalización o actuales Espacios Territoriales de Capacitación y Reincorporación (ETCR).

Este panorama se evidencia con mucha más fuerza en Cauca, seguida por Antioquia, Valle del Cauca, Bogotá, Cesar, Norte de Santander y Nariño, donde han aparecido estructuras criminales como las Autodefensas Gaitanistas de Colombia, Los Puntilleros, La Cordillera, La Constru, Los Pachenca, Los Caparrapos, Los Pachely, Los Rastrojos, el  Clan Isaza, LasFuerzas Unidas del Pacífico, el Frente Oliver Sinesterra o el Comando Especial Antiparamilitarismo Frente 36.

\[caption id="attachment\_65227" align="alignleft" width="240"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Líderess-240x300.png){.size-medium .wp-image-65227 width="240" height="300"} Somos Defensores\[/caption\]

**Incrementa la presencia paramilitar**

Según las cifras registradas por el  Sistema de Información sobre Agresiones contra Defensores y Defensoras de Derechos Humanos (SIADDHH), nunca antes se habían registrado cifras tan altas de agresiones como en el 2018, registrándose 805 casos de violencia entre los que aparecen 155 asesinatos.

También destaca que de estos 155 homicidios, 19 es decir, el 12.2%. estaban relacionados con  zonas de sustitución de cultivos, es importante señalar que la persecución a líderes que promueven esta actividad se ha recrudecido por lo que hoy, actores armados continúan persiguiendo a líderes comunales, defensores y representantes de las comunidades  quienes han sido visibilizados dentro de los territorios gracias al PNIS.

Con relación a las agresiones en Colombia, Somos Defensores revela que durante el 2018 en promedio fueron agredidas 2,2 personas por día. presentándose julio con 119 hechos, mayo con 112, agosto con 109 y junio con 77 como los meses con más casos presentados, además resalta que de los 805 casos de agresiones el 29% fueron contra mujeres y el 71% restante contra hombres revelando un aumento en los ataques contras las mujeres defensoras en un 64%.

> Las agresiones contra los defensores de derechos humanos aumentaron en 2018 ¿En 2019 esta cifra podría aumentar de cara a las elecciones regionales? Diana Sánchez, directora [@SomosDef](https://twitter.com/SomosDef?ref_src=twsrc%5Etfw) [\#LaNaranjaMecanica](https://twitter.com/hashtag/LaNaranjaMecanica?src=hash&ref_src=twsrc%5Etfw) <https://t.co/sbUMsDIrbC> [pic.twitter.com/APlOB6x0rw](https://t.co/APlOB6x0rw)
>
> — Contagio Radio (@Contagioradio1) [23 de abril de 2019](https://twitter.com/Contagioradio1/status/1120706255931285504?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Finalmente, señala que de estas **805 agresiones registradas en el 2018, se presume que el 55,5% fueron cometidas por grupos o estructuras paramilitares, 33,5% por desconocidos, 5% por grupos disidentes de las FARC, 4,2% por la Fuerza Pública y el 1,8% por el ELN**. Lo que evidencia una mayor responsabilidad de los grupos armados paramilitares en los ataques registrados hasta la fecha.

\[caption id="attachment\_65300" align="aligncenter" width="640"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/SomosDefensores.jpeg){.wp-image-65300 .size-full width="640" height="1600"} Foto: Contagio Radio\[/caption\]

[English Version](https://archivo.contagioradio.com/assaults-against-human-rights-defenders-increased-more-than-40-in-2018/)  
[Puede descargar el informe completo aquí ](https://somosdefensores.org/wp-content/uploads/2019/04/informe-somos-defensores-2019-espanol-web.pdf)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
