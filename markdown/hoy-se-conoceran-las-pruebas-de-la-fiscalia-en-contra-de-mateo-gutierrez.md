Title: Hoy se conocerán las pruebas de la Fiscalía en contra de Mateo Gutiérrez
Date: 2017-06-22 11:57
Category: DDHH
Tags: estudiante, falso positivo judicial, Mateo Gutierrez
Slug: hoy-se-conoceran-las-pruebas-de-la-fiscalia-en-contra-de-mateo-gutierrez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/mateo-gutierrez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mateo Gutiérrez] 

###### [22 Jun 2017] 

El día de hoy se lleva a cabo la audiencia de acusación contra el estudiante de 20 años Mateo Gutiérrez. Su familia es enfática en señalar que **“la fiscalía no tiene pruebas que presentar contra él, confiamos en que se juzgue en derecho”.** Aracely León, madre de Mateo, reiteró la preocupación de que los estudiantes de la universidad pública sigan siendo estigmatizados.

Mateo Gutiérrez está detenido en la cárcel Modelo de Bogotá desde febrero del año en curso. **El joven fue acusado de participar en la creación y detonación de “bombas panfletarias”** en Bogotá en septiembre de 2015. Ante esto, fue acusado de los cargos de terrorismo, concierto para delinquir, hurto calificado, tráfico y porte de armas de fuego. (Le puede interesar: ["De Mateo a Mateo: Carta a un preso político"](https://archivo.contagioradio.com/de-mateo-a-mateo-carta-a-un-preso-politico/))

Sin embargo, la defensa, su familia, sectores de la sociedad y el mismo Mateo han establecido en repetidas ocasiones que **él no tiene nada que ver con los hechos que lo sindican** y aseguran que es víctima de un “falso positivo judicial”.

Para Aracely León, “la Fiscalía no tiene pruebas sino indicios de prueba y lo que han hecho es desacomodar su caso para inculparlo.” Mateo había escrito desde la prisión hace 3 meses que su encarcelamiento **“es totalmente injusto y arbitrario y responde a motivaciones políticas** de un gobierno que busca dar respuesta a la inseguridad de las ciudades”.

**“No hemos contemplado una condena para Mateo”**

A pesar de que existe la posibilidad de que Mateo Gutiérrez sea condenado a 15 años de prisión, **su familia y amigos no han contemplado esa posibilidad.** Según su madre, “sabemos que él puede estar expuesto a un juicio largo y a estar detenido por las decisiones injustas de los jueces, pero sabemos que no hay pruebas para condenarlo”. (Le puede interesar: ["Mateo Gutiérrez León sería víctima de otro falso positivo judicial"](https://archivo.contagioradio.com/mateo-gutierrez-leon-seria-victima-de-falso-positivo-judicial/))

De igual manera manifestaron que **“a Mateo se le han negado todas posibilidades de un juicio justo y en libertad”**. Su juicio ha estado enmarcado por las irregularidades; desde la testificación de un testigo que cambió la versión sobre el retrato hablado de la persona que cometió el delito en esa fecha y la identificación de Mateo en fotografías que la familia aún no conoce. Aracely León afirmó que “confiamos plenamente en la seriedad de los jueces especializados que toman el caso para que juzguen en derecho”.

La madre de Mateo aseguró que ha podido ver a su hijo los días de visita y ella manifiesta que el joven se encuentra bien, **“él está fuerte y ha asumido el caso con la certeza de que pronto estará en libertad”**. De igual forma, Aracely León agradeció la solidaridad de las personas en Colombia y en países como México, España y Canadá para con Mateo. “La solidaridad de las personas que indagan y buscan información de la injusticia que se comete con Mateo nos da fortaleza y nos sentimos acompañados”. (Le puede interesar: ["Se conforma red de solidaridad en defensa de Mateo Gutiérrez"](https://archivo.contagioradio.com/red-solidaridad-defensa-mateo-gutierrez/))

El día de hoy se realiza un plantón frente al Juzgado Penal Especializado de Circuito de Cundinamarca en la calle 31 con carrera 6, para pedir por la libertad de Mateo. Su familia ha extendido la invitación a la ciudadanía y a la comunidad educativa. Aracely León manifestó que “vamos a hacer este plantón hasta que se termine la audiencia. **Hoy es mi hijo, mañana puede ser el conocido de alguien más”.**

<iframe id="audio_19414919" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19414919_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
