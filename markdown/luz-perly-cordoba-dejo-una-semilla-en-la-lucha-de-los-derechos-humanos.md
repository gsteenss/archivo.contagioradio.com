Title: Luz Perly Córdoba dejó una semilla en la lucha de los derechos humanos
Date: 2020-02-04 18:17
Author: CtgAdm
Category: Nacional
Tags: Luz Perly Córdoba
Slug: luz-perly-cordoba-dejo-una-semilla-en-la-lucha-de-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Luz-Perly-Córdoba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto:

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify","textColor":"very-dark-gray","fontSize":"normal"} -->

**Luz Perly Córdoba, lideresa social** falleció el pasado 2 de febrero cuando se movilizaba en su esquema de seguridad desde Arauquita hasta Fortul. Durante más de 30 años trabajó junto al movimiento campesino de Arauca, a su vez, fue integrante de la comisión política de la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana (COCCAM) y vocera del movimiento Cumbre Agraria.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los sucesos, relatan medios locales ocurrieron aparentemente por esquivar un hato de ganado que se encontraba en la vía. Debido al impacto también fallecieron Javier Barón Bautista y Ómar Julían Cárdenas Silva, dos de los cuatro escoltas de la Unidad Nacional de Protección (UNP) que le acompañaban, las otras dos personas fueron remitidos a un hospital de la zona. [(Le puede interesar: Tras una semana en cautiverio, fue liberada Andrea Ardila, lideresa de Saravena, Arauca)](https://archivo.contagioradio.com/andrea-ardila-lideresa-de-saravena-arauca-completa-una-semana-desaparecida/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La lideresa nacida en Arauquita, Arauca dedicó parte de su vida a defender los derechos de las comunidades campesinas en los departamentos de **Meta, Guaviare y Arauca,** llegando a ser intimidada y amenazada en múltiples ocasiones por organizaciones como las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y las Autodefensas Unidas del Casanare debido a su trabajo junto a la la Asociación Campesina de Arauca,

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Los campesinos concebimos la paz desde el ámbito del derecho al acceso tierra con garantías - Luz Perly Córdoba

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

[Luz Perly Córdoba](https://www.justiciaypazcolombia.com/luz-perly-cordoba/) también fue víctima de persecución y de un montaje judicial en 2004 por el que estuvo encarcelada durante un año y medio hasta que fue absuelta, de igual forma tuvo que permanecer en exilio durante un largo periodo en Suiza antes de poder volver al país en 2011 y continuar con su defensa por la vida. Se reintegró a la lucha campesina. Apoyó el fortalecimiento y nacimiento de Marcha Patriótica y junto a COCCAM, organización con la que trabajó en el desarrollo territorial contemplado en el Acuerdo de Paz.[(Lea también: Denuncian nuevo falso positivo judicial contra líder José Vicente Murillo, en Arauca)](https://archivo.contagioradio.com/denuncian-nuevo-falso-positivo-judicial-contra-lider-jose-vicente-murillo-en-arauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tras conocerse la noticia de su fallecimiento, diferentes personas y miembros de la movilización social lamentaron su partida, destacando la labor que ejercía en el territorio, promoviendo el acceso digno de tierra para los campesinos y manifestándose en contra de las afectaciones al ambiente, la explotación de los recursos naturales, los monocultivos y los cultivos de uso ilícito. [(Le recomendamos leer: Ante la JEP buscan justicia para casos de ejecuciones extrajudiciales en Arauca)](https://archivo.contagioradio.com/ante-la-jep-buscan-justicia-para-casos-de-ejecuciones-extrajudiciales-en-arauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dentro de los mensajes en su honor, Marcha Patriótica expresó que "los movimientos sociales de América Latina y el Caribe sienten la pérdida de una compañera fundamental para la construcción de unidad y el protagonismo del campesinado". [(Lea también: Más de 200 integrantes de Marcha Patriótica han sido asesinados entre 2011 y 2020)](https://archivo.contagioradio.com/asesinatos-marcha-patriotica-2011-2020/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ANZORC_OFICIAL/status/1224665541375336448","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ANZORC\_OFICIAL/status/1224665541375336448

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
