Title: Amenazas paramilitares en Zona Campamentaria de las FARC
Date: 2016-10-26 15:06
Category: Nacional, Paz
Tags: amenazas paramilitares, Cauca, zonas campamentarias
Slug: amenazas-paramilitares-en-zona-campamentaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/paramilitarismo-en-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Red de DDHH Francisco Isaías] 

###### [26 Oct 2016] 

En cercanías a la **Zona Campamentaria de las FARC** en Calandaima, municipio de Miranda Cauca, la comunidad recibió **amenazas paramilitares** a través de **panfletos y grafitis con las siglas de las AUC y las AUGC,** pintados en la carretera, postes, alcantarillas y casas abandonadas.

Cristian Delgado, integrante de la Red de Derechos Humanos Francisco Isaías Cifuentes del movimiento Marcha Patriótica, señaló que con base en el informe elaborado por la red, sobre la verificación del cese bilateral en este territorio, **habitantes han reportado la presencia de personas a altas horas de la noche, en motos de alto cilindraje y vestidos de negro. **Delgado, asegura que el pasado lunes se hizo una reunión con los militares de la brigada móvil 17, policía, defensoría y alcaldía para tratar de esclarecer lo ocurrido**. **Le puede interesar: [Paramilitares amenazan a organizaciones de derechos humanos del Valle del Cauca.](https://archivo.contagioradio.com/organizaciones-ddhh-del-valle-del-cauca-amenazadas-por-paramilitares/)

Por otra parte miembros de la Guardia Indígena y Campesina de Miranda, estan actuando para evitar problemas de seguridad en la pre-concentración de integrantes de las FARC, afirmando que en la parte alta de la montaña no hay presencia de grupos armados, sin embargo, han visto gente rondando por la zona y el mismo sábado 22 de Octubre **“en las zonas de los grafitis, se vio gente de camuflado”** enunció Delgado.

El defensor de Derechos Humanos, asegura que los mandos militares “ordenaron que **nadie de la base saliera del perímetro de la zona de agrupamiento de las FARC”** lo que pudo facilitar las amenazas. Le puede interesar: [Asesinan a Yimer Chávez, líder campesino del Cauca.](https://archivo.contagioradio.com/asesinan-a-yimer-chavez-lider-campesino-del-cauca/)

Los habitantes de Miranda han adelantado varias reuniones comunitarias, para hacer una articulación de medidas de protección propia del territorio a cargo de las guardias indígenas y campesinas. En dichas reuniones, **la comunidad también ha aprobado que se hagan Zonas Campamentarias en las veredas de Calandaima, El Desbaratado y Monteredondo.**

De igual forma, la comunidad y los integrantes de la Red, resaltan la importancia de coordinar las formas de seguridad de las guardias y las de las fuerzas estatales. Delgado, concluye que se hace urgente “iniciar las investigaciones que señalen a los **posibles enemigos del proceso de paz, que están generando acciones con tendencias paramilitares o herederas de paramilitarismo”.**

[Informe Red de Derechos Humanos Francisco Isaías Cifuentes](https://www.scribd.com/document/329001374/Informe-Red-de-Derechos-Humanos-Francisco-Isaias-Cifuentes#from_embed "View Informe Red de Derechos Humanos Francisco Isaías Cifuentes on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_49994" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/329001374/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-JuDR7mCCEbTriLrWb8Jo&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"><iframe id="audio_13489692" frameborder="0" allowfullscreen="allowfullscreen" scrolling="no" height="200" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13489692_4_1.html?c1=ff6600"></iframe></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
