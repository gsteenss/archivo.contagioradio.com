Title: Estado pide perdón por el asesinato del sociólogo Alfredo Correa de Andréis
Date: 2019-09-17 18:28
Author: CtgAdm
Category: DDHH, Memoria, Sin Olvido
Tags: Actividades ilegales del DAS, Alfredo Correa Andreis, AUC, das, FARC
Slug: estado-pide-perdon-por-el-asesinato-del-sociologo-alfredo-correa-de-andreis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/EEsOfBYWsAA17fq.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @constanzavieira ] 

El 17 de septiembre de 2004 fue asesinado Alfredo Correa de Andréis, pensador y académico. Quince años más tarde, el Estado colombiano pide perdón por el crimen que acabó con la vida del profesor y que estuvo organizado por el Bloque Norte de las Autodefensas Unidas de Colombia (AUC) y el antiguo Departamento Administrativo de Seguridad (DAS).

Alfredo Correa, nacido en Ciénaga, Magdalena, trabajaba por la defensa de los derechos humanos, enfocándose durante sus últimos años en investigar sobre el fenómeno del desplazamiento forzado en la región. Debido a su vinculación con las comunidades desplazadas, fue detenido y acusado de pertenecer a las Fuerzas Armadas Revolucionarias de Colombia (FARC), pero posteriormente fue liberado. Dos meses después, era asesinado a manos de los paramilitares.

#### Un perdón que llega quince años después 

El Consejo de Estado condenó este año a la Nación por la detención injustificada del sociólogo y profesor universitario. Ahora, tras 15 años de su asesinato, el Gobierno pide públicamente disculpas en un acto en el que se instaló una placa en su memoria. El acto contó con la presencia del director de la Agencia Nacional de Defensa Jurídica del Estado, Camilo Gómez Alzate, y autoridades del orden nacional, departamental y local.

Ante este hecho, **Magda Correa, hermana de Alfredo Correa**, manifiesta que "las disculpas del gobierno nunca se debieron dar porque nunca debieron acusar injustamente a Alfredo, ni mucho menos asesinarlo. Las disculpas no llegan tarde ni temprano porque nunca debió suceder lo que pasó".

#### Actos conmemorativos 

En el transcurso del día se realizaron actos en las dos universidades en las que Correa dio clases en Barranquilla para rendirle homenaje: la Universidad Simón Bolívar y la Universidad del Norte. Fue en ésta última donde el Estado colocó la placa conmemorativa.

Paralelamente, la organización Colectivo Sentipensante en Ciénaga, Magdalena, realizó un taller que le rindió homenaje: “Sentipensando los territorios desde el pensamiento de Alfredo Correa de Andreis”, Asimismo, Magda, su hermana, espera que este episodio  sea "una lección aprendida, por el bien y el futuro de Colombia. Que esto no siga sucediendo y que otras familias no tengan que pasar por lo que ha pasado la mía".

> Al conmemorarse 15 años del crimen de Alfredo Correa De Andréis, el Estado pedirá perdón e instalará una placa en memoria del sociólogo. [\#SinOlvido](https://twitter.com/hashtag/SinOlvido?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/cKEF1R3Joq](https://t.co/cKEF1R3Joq)
>
> — Sin Olvido (@SINOLVIDO) [September 17, 2019](https://twitter.com/SINOLVIDO/status/1173955513379373057?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
\#Sinolvido

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
