Title: Una contradicción entre el “cristianismo” y la realidad de las mujeres
Date: 2020-06-09 15:00
Author: CtgAdm
Category: A quien corresponda, Opinion
Slug: mujeres-cristianismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Con la medida que midan a los otros, los medirán a ustedes** (Mateo 7,2)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"right"} -->

*Hay cristianos que no reconocemos los pecados personales, sociales y ambientales, mientras exigimos a los demás que no pequen, o los criticamos por pecadores. *

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estimado

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Hermano en la fe**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cristianos, cristianas, personas interesadas

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este mayo de cuarentena, repensando la relación entre el “cristianismo” y las mujeres, recordé, que entre los temas imposibles de abordar con la corriente religiosa a la que perteneces, está el de los derechos de las mujeres. Cualquier alusión a sus derechos es descalificada y rechazada como “ideología de género”, *incompatible con la fe auténticamente cristiana”,* porque “*es la culpable de la desintegración de la familia, del libertinaje y de todos los vicios de la modernidad*”, “*desde que las mujeres salieron con el cuento de la liberación y de los derechos, toda va de mal en peor”,* *“la ideología quiere acabar con las relaciones entre hombres y mujeres queridas por Dios*”. Y muchas otras afirmaciones parecidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este mayo, celebramos en Colombia el día de las madres, de forma atípica porque la pandemia confinó madres e hijos. Esta celebración se caracteriza, entre otros hechos, por restaurantes llenos, abundante venta licor, comercios atiborrados y por ser unos de los días más violentos del año, según Medicina Legal. Igualmente, en mayo, tradiciones cristianas celebran a María de Nazaret, la madre de Jesús, la Madre de Dios o la “virgen María” en sus diversas “devociones”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Podemos decir que América Latina es un continente que le canta y declama a las mujeres, en especial a las madres; es un continente “mariano”, con muchos y hermosos santuarios. Y la vez es un continente violento y machista. Las cifras de violencia física, sexual, psicológica y asesinatos de mujeres son escandalosos y “aterradores”, para ellas, para la generación actual y las futuras. A no ser que como sociedad hagamos transformaciones profundas. [ Lea tambien: En cuarentena mujeres temen ser víctimas de violación por parte de la Policía](https://archivo.contagioradio.com/en-medio-de-cuarentena-mujeres-temen-ser-victimas-de-violacion-por-parte-de-la-policia/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Las cifras de violencia física, sexual, psicológica y asesinatos de mujeres son escandalosos y “aterradores”, para ellas, para la generación actual y las futuras. A no ser que como sociedad hagamos transformaciones profundas.

<!-- /wp:quote -->

<!-- wp:paragraph -->

Esta realidad genera profundos cuestionamientos por las contradicciones entre el mensaje religioso, especialmente de los sectores tradicionalistas de las iglesias que andan preocupados y ocupados detectando y persiguiendo la “ideología de género”, presente donde se habla de derechos, especialmente de las mujeres, y el mensaje de Jesús de Nazaret: el reino de Dios, reino de paz, justicia, amor, respeto y armonía entre los seres humanos y la creación. Entre esos cuestionamientos están:  

<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true,"type":"1"} -->

1.  ¿Cómo entender, para transformar, que el día de las madres sea uno de los más violentos del año? Algo anda mal en el inconsciente colectivo de una sociedad como la colombiana en la que coexistan el amor a las madres y las violencias que llevan incluso a la muerte de sus hijos. Se puede intentar explicar con las violencias vividas en la familia de las madres, por las madres y contra las madres; con los sentimientos que despierta el “día más importante” que, por supuesto “hay que celebrar como se lo merece”; con los tragos que desinhiben violencias reprimidas y generan estragos; con las depresiones que se agudizan la ausencia de la madre; con la culpa de haber sido malos hijos/as y que llevan a buscar evasiones a conflictos profundos que puede generar violencias. Son necesarios estudios profundos e interdisciplinares que ayuden a comprender esta realidad y construir propuestas colectivas para superarla. Ojalá las iglesias analicen el impacto negativo de predicaciones culpabilizantes, generadoras de miedos y presiones psico-espirituales que, en vez de ayudar a superar estos problemas, los agudizan.

<!-- /wp:list -->

<!-- wp:list -->

-   ¿Cómo compaginar el mensaje de Jesucristo: amor, justicia, respeto, paz y buen vivir, con prácticas y predicaciones “cristianas” desentendidas de las injusticias, violencias, marginaciones, empobrecimiento, discriminaciones…  que vive el país y el continente y que degrada la vida de la mayoría de mujeres y de sus hijos e hijas?   Esta contradicción es muy preocupante por la repercusión en la vida de las mujeres y en toda la sociedad, y porque “hace quedar muy mal a Dios”. Las consecuencias de esta contradicción, por estar dentro de los creyentes, hace difícil identificarlas y dimensionar las repercusiones negativas para las iglesias y la sociedad.  

<!-- /wp:list -->

<!-- wp:list -->

-   ¿Cómo creer, a quienes mucho hablan y predican en defensa de la familia “establecida por Dios”, de los valores tradicionales, de la “buena conducta” sexual, de las “buenas costumbres” que, según dicen se están perdiendo por causa del “modernismo”, el “libertinaje” o la ideología de género; sin reconocer la situación de las familias reales (poco existen familias ideales, incluso en quienes tanto hablan de ella), la realidad del abuso sexual y violencia contra niños y niñas, el hambre, las injusticias humanas y ambientales, con todas su consecuencias y comprometerse en su transformación? Estas realidades, causan graves daños socio-ambientales, están profundamente arraigadas en la mentalidad y cultura de la sociedad, construidas con aportes de lo religioso. Se necesitan estudios serios e interdisciplinares, más allá de predicaciones y condenas religiosas, pare responder a “la voluntad de Dios” y aportar al cambio cultural necesario para construir una sociedad mejor.   

<!-- /wp:list -->

<!-- wp:paragraph -->

Un ejemplo de estas realidades, es la violencia, el abuso y la agresión sexual que genera daños irreparables, especialmente en niños y niñas por los traumas para toda la vida. Sus dimensiones son escandalosa: entre el 2015 y el 2019[\[1\]](#_ftn1), fueron denunciados 91.982 casos violencia sexual contra niños/as en Colombia (la mayor parte de los hechos no son denunciados). Los abusadores fueron: familiares un 46%, conocidos 22%, otros 14%, amigos 11% y la pareja o ex - pareja el 7%; es decir, que entre 65 o 70 de cada 100 casos los responsables fueron del círculo de confianza, del que esperaría protección y respeto. Las más afectadas fueron niñas entre los 10 y los 14 años ( 44 de cada cien casos). En ese mismo tiempo y en esas mismas edades, se documentaron 23.923 madres, imaginemos las repercusiones en estas madres-niñas, en sus hijos/as y en la sociedad. En estas violencias, las niñas “representan el 74,4 por ciento de las denuncias de 2018. En 2016 y 2017, la tendencia fue prácticamente la misma: 85,4 por ciento y 73,9 por ciento, respectivamente”[\[2\]](#_ftn2). Esta realidad, debe llevar a las iglesias a analizar las causas, asumir responsabilidades y aportar a una cultura que facilite el diálogo abierto en las familias, las escuelas y las iglesias para reconocer sus dimensiones y aportar a la solución real, más de allá de condenas y buenas intenciones. [Le puede interesar: Tres propuestas universitarias para erradicar la violencia de género](http://Tres%20propuestas%20de%20universitarias%20para%20erradicar%20la%20violencia%20de%20géner)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otro ejemplo, es la agresión y/o abuso sexaul de caracter incestuoso[\[3\]](#_ftn3), que podemos catalogar de “pendemia” porque *“se calcula que el 65% de las mujeres han sido víctimas, alguna vez en su infancia, de un abuso sexual incestuoso*”, con graves repercusiones para sus vidas, que se agudizan por el silencio forzoso impuesto y por el efecto “*desestabilizador del orden afectivo y sexual tanto para la victima, como para el agresor y el entorno familiar”*. El mayo número de abuso sexual incestuoso es *“de padre-hija, entre hermanos, abuelo-nieta, entre primos, madre-hijo”*,  abusos que se viven en la casa paterna y aumentan con factores como “*el hacinamiento y la pobreza extrema, la precocidad sexual de los niños, los problemas psicopatológicos de la familia o la sexualización de la vida social”.* Muchas predicaciones agudizan las afectaciones psico-espirituales de las víctimas, poco aportan al recocimiento de su existencia y a reducir las afectaciones y repetición.

<!-- /wp:paragraph -->

<!-- wp:list -->

-   ¿Cuántos responsables de las violencias contra las mujeres son personas tradicionalistas de doble moral, la mayoría hombres, “creyentes”, machistas, “mujeriegos” y profundamente celosos con sus hijas, hermanas y familiares, porque como dice el dicho popular “*el que las usa, se las imagina”?*  Es urgente, como sociedad y como iglesias, reconocer estas realidades descritas y otras relacionadas, en su complejidad y consecuencias, para aportar en la superación; ver la realidad y la historia personal de mujeres y hombres agredidos y agresores; conocer la “realidad de los hechos” para diferenciar el abuso con poder, fuerza, chantaje, amenaza con violencia, miedo o presión, de “las relaciones” en momentos de pasión o euforia; de los hechos planeados, buscados o propiciados con ventajas para quien tienen el poder en cualquiera de sus expresiones. Solo un análisis complejo, interdisciplinar y con participación de implicados puede ayudar a comprender el problema con realismo y dar luces para abordarlo, socialmente con asertividad y cristianamente con responsabilidad.   

<!-- /wp:list -->

<!-- wp:paragraph -->

Paradójicamente, mucho líderes/as sociales, políticos, religiosos o de opinión, actúan, conceptúan y deciden movidos por intereses “políticos-económicos”, religiosos o por impulsos internos, conscientes o inconscientes, por estar implicados de diversas formas en estas realidades o por tendencias sin identificar o reconocer; por esta razón muchas “respuestas” son más impulsivas y reactivas que asertivas y eficaces realmente. Es importante considerar el estudio de Teodoro

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lea más C[olumnas de nuestro columnista aquí](https://archivo.contagioradio.com/author/a-quien-corresponde/)

<!-- /wp:paragraph -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:paragraph -->

[\[1\]](#_ftnref1) Cf. <https://www.alianzaporlaninez.org.co/entre-2015-y-junio-de-2019-se-han-presentado-91-982-casos-de-violencia-sexual-en-contra-de-la-ninez-colombiana/>, consultado del 25 de mayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[\[2\]](#_ftnref2) <https://www.eltiempo.com/justicia/delitos/registro-de-abuso-sexual-en-colombia-contra-menores-de-edad-311738>, consultado del 25 de mayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[\[3\]](#_ftnref3) Asumo ideas y datos del P. Rafael Prada Ramírez, CSsR, doctor en Psicología, Conferencia presentada en el  XXV Capítulo General de los Misioneros Redentoristas, en octubre del 2009 en Roma, con el título: “*Abuso sexual infantil por parte de clérigos y religiosos católicos”.*

<!-- /wp:paragraph -->
