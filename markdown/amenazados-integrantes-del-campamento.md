Title: Amenazados integrantes del Campamento por la Paz
Date: 2016-10-21 15:24
Category: Nacional, Paz
Tags: #AcuerdosYA, amenazas a líderes, Campamento por la Paz
Slug: amenazados-integrantes-del-campamento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/IMG-20161007-WA0009-01-e1476216153492.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Oct 2016] 

En días pasados, una de las integrantes del Campamento por la Paz, Catherine Miranda, recibió amenazas a través de su cuenta de Facebook por un grupo que se hace llamar Fuerzas Especiales Anticomunistas. Catherine comenta que “el mensaje dice que por mi seguridad, **es mejor que me quede quieta y deje de hacer movilizaciones, plantones y reuniones”**.

Sin embargo, ella no es la única integrante del campamento que ha recibido amenazas, afirma que **otras 4 personas han sido amenazadas por vía telefónica y también por redes sociales.** “Ya se dio aviso a las autoridades, y estas afirman que han iniciado la investigación respectiva” señala Catherine. Le puede interesar: [Impunidad, persecución y amenazas fue lo que encontró caravana internacional de juristas.](https://archivo.contagioradio.com/impunidad-persecucion-y-amenazas-fue-lo-que-encontro-caravana-internacional-de-juristas/)

Por otra parte, la integrante del campamento, manifiesta que la policía metropolitana ha dispuesto 7 efectivos sólo para el campamento, y que desde lo ocurrido “somos más **cuidadosos con el uso de las redes sociales, y hemos hecho seguimiento a personas que dejan comentarios malintencionados y amenazantes”**.

Catherine resalta que muchos ciudadanos les han expresado su apoyo y que **“las amenazas han fortalecido la unión del campamento”**. Extiende la invitación para que “Colombia pierda el miedo a movilizarse y se haga **una acción a nivel nacional de todos los campamentos que se han instalado** para exigir los acuerdos ya”. Le puede interesar: [Montería instala campamento por la paz.](https://archivo.contagioradio.com/monteria-instala-campamento-por-la-paz/)

<iframe id="audio_13422208" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13422208_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
