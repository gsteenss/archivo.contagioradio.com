Title: Líder reclamante de tierras en Mapiripán es blanco de amenazas por parte de paramilitares
Date: 2015-09-10 12:38
Category: DDHH, Nacional
Tags: Comisión de Justicia y Paz, Entre el Agua y el Aceite de Palma, mapiripan, Meta, Palma aceitera, paramilitar Edgar Pérez, Poligrow, William Aljure
Slug: lider-reclamante-de-tierras-en-mapiripan-es-blanco-de-amenazas-por-parte-de-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/William-Aljure.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Entre el Agua y el Aceite de Palma 

El pasado 8 de Septiembre, William Aljure, líder campesino y reclamante de tierras en Mapiripán, Meta, **volvió a ser blanco de intimidaciones en Villavicencio** por parte de grupos Paramilitares.

Esta vez una mujer entre 30 y 35 años de edad fue observada tomando fotografías de la vivienda de Aljure. La mujer permaneció en el lugar  por más de 30 minutos, se acercó a una tienda y preguntó acerca de las personas que habitan en la casa con el pretexto que ese lugar era la sede de una empresa.

En los últimos días, la vida del líder campesino ha estado en grave peligro, tras la publicación del documental “[Entre el Agua y el Aceite de Palma”](https://archivo.contagioradio.com/documental-evidencia-despojo-paramilitarismo-y-accion-empresarial-en-mapiripan/), donde se denuncia  los efectos de las actividades con palma aceitera de la empresa Poligrow en Mapiripán, por ese mismo hecho, el pasado **15 de agosto el paramilitar Edgar Pérez alias “Tomate”**, aseguró que **“era necesario matar a William porque estaba generando mala imagen”**, según denunció la Comisión de Justicia y Paz.
