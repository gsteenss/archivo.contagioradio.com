Title: Fuerza Pública arremete contra comunidad en Popayán
Date: 2018-03-28 20:55
Category: DDHH
Tags: Comunidad, policia, Popayán
Slug: fuerza-publica-arremete-contra-comunidad-en-popayan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/La-Fuerza.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mesa de Derechos Humanos del Cauca] 

###### [28 Mar 2018] 

A través de un comunicado de prensa, la Mesa de derechos humanos por la vida y el territorio en el Cauca, denunció que durante el mes de marzo, la comunidad del asentamiento La Fortaleza fue víctima de múltiples **agresiones por parte de la Fuerza Pública, sin que existiera alguna respuesta por parte de las instituciones**.

Este asentamiento está ubicado en la comuna 7 de Popayán y de acuerdo con la denuncia, las personas que viven allí afrontan condiciones infrahumanas y de hacinamiento, razón por la cual, los habitantes decidieron hacer una reubicación de los lotes, el día 27 de marzo a las **4:00 pm, cuando realizaban esta acción, la Policía llegó al lugar agrediendo a la comunidad**.

A su vez, tumbaron las construcciones que habían levantado las familias, golpearon a personas que se encontraban en el lugar, entre los que había mujeres, niños y adultos mayores y según la denuncia, habría **usado un tipo de polvo blanco que generó asfixia, vómito y desmayo a quienes lo inhalaron**.

Previo a este hecho, los días 12 y 17 del mismo mes, también se registraron agresiones de la Fuerza Pública que dejaron como resultado una persona judicializada y más personas agredidas. Todos estos actos de violencia **se han realizado sin que la Policía tenga una orden judicial por parte de alguna autoridad**.

De igual forma, la Mesa de derechos humanos, señaló que la Alcaldía de Popayán no ha hecho nada frente a esta situación e hicieron un llamado a las diferentes instituciones para que se generen sanciones a los respectivos responsables de las agresiones a la comunidad. (Le puede interesar:["Operativo de la Fuerza Pública en Caloto, Cauca, dejo un muerto y 6 heridos](https://archivo.contagioradio.com/operativo-de-la-fuerza-publica-en-caloto-cauca-dejo-un-indigena-muerto-y-seis-heridos/)")

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
