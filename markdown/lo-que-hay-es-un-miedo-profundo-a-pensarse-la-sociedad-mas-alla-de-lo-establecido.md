Title: "Lo que hay es un miedo profundo a pensarse la sociedad más allá de lo establecido"
Date: 2016-08-12 16:14
Category: Educación, LGBTI
Tags: discriminación sexual, gina parody, LGBTI, Ministerio de Educación
Slug: lo-que-hay-es-un-miedo-profundo-a-pensarse-la-sociedad-mas-alla-de-lo-establecido
Status: published

###### [Foto: Contagio Radio ] 

###### [12 Ago 2016] 

En las escuelas y en la sociedad es urgente un debate alrededor del género, no en términos ideológicos como lo han querido plantear quienes están en contra de las disposiciones de la Corte y de la intención del Ministerio, sino desde el dejar de pensar que los estudiantes son hombres y mujeres desde una sola manera de ser, porque **hay muchas maneras de ser hombres y diversas de ser mujer**, y ello no tiene que implicar ningún tipo de violencia, asegura Jhon Vargas Rojas, magister en educación y estudios sociales.

De acuerdo con el licenciado, **las escuelas no solamente educan en contenidos, también lo hacen en prácticas sociales**;** **además de enseñar matemáticas o sociales, la escuela forma ciudadanos con posibilidad de conocer que el mundo es diverso y que pueden tomar decisiones. Para el profesor el debate de la sexualidad debe darse en las escuelas porque hace parte de la realidad, y agrega que es incorrecto pensar las escuelas por fuera de la sociedad, pues fue un debate que se superó en la década del setenta.

El licenciado asevera que una de las preguntas fundamentales que habría que hacerle al Gobierno es desde que mecanismo va a cumplir con la sentencia de la Corte Constitucional frente a la revisión de los manuales, teniendo en cuenta su negativa frente al uso de la cartilla en los colegios, pues "el tema del matoneo, no es un distractor, es una realidad" y reducir el debate a la imposición de la 'ideología de género' es **legitimar la idea de que los colegios pueden tener autoridad para violentar a los estudiantes que no responden al modelo heterosexual hegemónico**.

Los manuales de los colegios deben dejar de ser instrumentos de violencia para quienes no responden a la heterosexualidad, afirma el licenciado y agrega que **lo que hay es un miedo profundo a pensarse la sociedad más allá de los parámetros establecidos** y sí bien la familia da pautas para la constitución de los seres humanos, es en la escuela dónde los niños, las niñas y los jóvenes aprender a ser ciudadanos y a tramitar debates como el de la diversidad con mucha más tolerancia que las personas adultas que les rodean.

<iframe src="http://co.ivoox.com/es/player_ej_12528637_2_1.html?data=kpeilJ2ad5ihhpywj5eXaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5yncavjydOYuMbWq8LnhpewjbLFq8rn1crfjcrSb6bY1sjOxc6Jh5SZo5jbj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
