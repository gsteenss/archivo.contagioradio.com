Title: Fallas en búsqueda de desaparecidos de Comuna 13 se presentan ante la CIDH
Date: 2016-04-04 17:25
Category: DDHH, Nacional
Tags: CIDH, Comuna 13, desaparecidos, La escombrera, Medellin
Slug: fallas-en-busqueda-de-desaparecidos-de-comuna-13-se-presentan-ante-la-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/escombrera-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [4 Abr 2016]

La búsqueda de desaparecidos y excavaciones en la Escombrera de Medellín será uno de los temas que tiene audiencia prevista para este 5 de abril ante la Comisión Interamericana de Derechos Humanos, con el objetivo de que la CIDH y el Estado colombiano, escuchen las **recomendaciones de diferentes organizaciones de Derechos Humanos para haya una búsqueda eficaz de los desaparecidos de la Comuna 13 en Medellín.**

Organizaciones como la Corporación Jurídica Libertad, el Grupo Interdisciplinario por los Derechos Humanos, EQUITAS, Mujeres Caminando por la Verdad, Obra Social Madre Laura y el Movimiento Nacional de Crímenes de Estado aseguran que **son muy pocos los avances que se han logrado en el marco de la búsqueda en sector conocido como La Escombrera.**

De acuerdo con Adriana Arboleda, abogada de la Corporación Jurídica Libertad, asegura que en este proceso de búsqueda hay muchas fallas metodológicas de todo tipo al no existir un plan de búsqueda acertado, esto generado “por afanes de tipo políticos”, expresa la abogada.

Las recomendaciones de las organizaciones de derechos humanos y de víctimas se centran en que el proceso de búsqueda se concentre en La Fiscalía, y no siga liderado por la Unidad de Justicia y Paz pues allí solo se tiene en cuenta las declaraciones de los exjefes paramilitares sin ningún tipo de contraste con otras investigaciones.

Así mismo, los **familiares de los desaparecidos exigen que se busque en otros sitios y no solo en los polígonos establecidos en el sector de La Escombrera,** y para ello, según Adriana Arboleda, debe existir una metodología como la que propusieron las organizaciones de víctimas al inicio del proceso, de tal manera que haya equipos independientes que apoyen la definición de la metodología y la excavación, pero ese plan alterno no ha sido tenido en cuenta por las autoridades.

**“Tiene que haber planes de búsqueda que obedezcan a un proceso continuo respetando la dignidad de los familiares, que tienen un dolor permanente al no saber que ha pasado con sus seres queridos”,** dice Arboleda, quien concluye que el mecanismo de búsqueda “no pueden seguir así y debe haber correcciones de fondo y permitirse una participación de víctimas y organizaciones a otro nivel”.

<iframe src="http://co.ivoox.com/es/player_ej_11043364_2_1.html?data=kpadlpiXepWhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaLY087O0MaPhdPW0NHSxsaRb6Tj09Xc1MbHrYa3lIqvldOPjtbmhqigh6aoqMrXwpC5y8fJttXVxZKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Adriana Arboleda, Corporación Jurídica Libertad ] 

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
