Title: Los efectos del asesinato del médico Cristian Camilo Julio Arteaga en Antioquia
Date: 2019-05-12 09:08
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Antioquia, bajo cauca antioqueño, El Bagre, Médico
Slug: efectos-asesinato-medico-cristian-camilo-julio-arteaga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Cristian-Camilo-Julio-Arteaga-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RCN Radio] 

El pasado viernes la Universidad Nacional emitió un comunicado en el que lamentaba el asesinato del médico Cristian Camilo Julio Arteaga de 24 años, quien ejercía su profesión en El Bagre, Antioquia. Según las primeras informaciones, Julio se dirigía a Caucasia para encontrarse con su mamá, cuando fue asesinado por hombres armados. (Le puede interesar: ["Asesinan a Giovany Murillas, excombatiente de las FARC que se la jugó por la paz"](https://archivo.contagioradio.com/asesinado-giovany-murilas-excombatiente-farc/))

El joven médico se encontraba cumpliendo su servicio social obligatorio en el municipio; y al comunicado de la Nacional se sumó el de la Asociación Colombiana de Facultades de Medicina y el Colegio Médico Colombiano, quienes lamentaron el trágico hecho, solicitaron su investigación, y pidieron que se respete la vida de aquellos que tienen por labor la protección de la salud de los otros.

El hecho, además de ser lamentable por la pérdida de Julio, significó un retroceso para la población de El Bagre; pues como lo relataron medios de comunicación comerciales, seis médicos compañeros del joven decidieron salir del Municipio por considerar que no había garantías para el ejercicio de su labor en el lugar, situación que obligó al hospital público Nuestra Señora del Carmen a suspender sus servicios.

En desarrollo...

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
