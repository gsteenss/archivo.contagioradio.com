Title: Así se vivió el Quinto Festival de las Memorias #SomosGénesis
Date: 2020-03-03 20:10
Author: CtgAdm
Category: Nacional
Slug: asi-se-vivio-el-quinto-festival-de-las-memorias-somosgenesis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.32-PM-1.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.15-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.47-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.44-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.08-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.00-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.40-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.40-PM-1-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.38-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.33.34-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.14-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.36-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.03-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.48-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Del 28 de febrero al primero de marzo se realizó el quinto Festival de las Memorias, que conmemoró a las víctimas de la Operación Génesis, luego de 23 años de la arremetida efectuada por la Brigada 17 y estructuras Paramilitares en la Cuenca del Cacarica. Además puso de manifiesto desarrollo en materia de justicia restaurativa que en este tiempo han desarrollado las comunidades a pesar de seguir viviendo la guerra y el control de tipo paramilitar ejercido por las AGC. (Le puede interesar: ["En medio del asedio paramilitar víctimas conmemoran \#SomosGénesis"](https://archivo.contagioradio.com/en-medio-del-asedio-paramilitar-victimas-conmemoran-somosgenesis/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para esta oportunidad, el Festival se desarrolló con un recorrido de la memoria que inició en el Coliseo de Turbo, Antioquia y finalizó en la zona humanitaria de Nueva Vida, en la Cuenca del Cacarica, en donde el eje central del diálogo fueron las garantías de no repetición.

<!-- /wp:paragraph -->

<!-- wp:jetpack/slideshow {"ids":["81529","81530","81531",81598,81600,"81532","81533",81601,"81534","81535","81536",81602,"81539","81540"],"sizeSlug":"full"} -->

<div class="wp-block-jetpack-slideshow aligncenter" data-effect="slide">

<div class="wp-block-jetpack-slideshow_container swiper-container">

-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.32-PM-1-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81529}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.15-PM-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81530}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.47-PM-1-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81531}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.14-PM-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81598}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.36-PM-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81600}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.44-PM-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81532}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.08-PM-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81533}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.03-PM-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81601}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.00-PM-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81534}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.40-PM-1-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81535}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.40-PM-1-1-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81536}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.48-PM-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81602}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.32.38-PM-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81539}
    </figure>
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-02-at-6.33.34-PM-1024x576.jpeg){.wp-block-jetpack-slideshow_image .wp-image-81540}
    </figure>

[]{.wp-block-jetpack-slideshow_button-prev .swiper-button-prev .swiper-button-white}[]{.wp-block-jetpack-slideshow_button-next .swiper-button-next .swiper-button-white}[]{.wp-block-jetpack-slideshow_button-pause}

<div class="wp-block-jetpack-slideshow_pagination swiper-pagination swiper-pagination-white">

</div>

</div>

</div>

<!-- /wp:jetpack/slideshow -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
