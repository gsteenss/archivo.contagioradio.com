Title: “El cambio” la realidad hecha película de los defensores de DDHH durante 2015
Date: 2016-03-03 13:14
Author: AdminContagio
Category: 24 Cuadros, DDHH
Tags: Carlos Guevara Somos Defensores, El cambio informe Defensores 2016, Programa Somos Defensores
Slug: el-cambio-la-realidad-hecha-pelicula-de-los-defensores-de-ddhh-durante-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/el-cambio-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  Foto: Somos Defensores 

<iframe src="http://co.ivoox.com/es/player_ek_10661046_2_1.html?data=kpWjmJaUeJehhpywj5aXaZS1k5Wah5yncZOhhpywj5WRaZi3jpWah5yncYa5k4qlkoqdh6bgjMjOz8fNs4a5k4qlkoqdiIzgwpDfx8bQrcXVxZDVx8jMpYzkxtGSpZiJhaXX1tHOjcnJb83j1JDRx8vJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Guevara, Somos Defensores] 

###### [3 Mar 2016] 

Con el objetivo de contar a otros públicos las realidades de estigmatización, amenazas, desapariciones y asesinatos que enfrentan lideresas y líderes sociales en Colombia, el Programa Somos Defensores presentó su más reciente informe titulado [‘El Cambio’](https://archivo.contagioradio.com/2444-defensores-de-ddhh-han-sido-agredidos-en-5-anos-de-gobierno-santos/), convertido en la 'película de los defensores durante 2015'.

De acuerdo con Carlos Guevara, Coordinador del Programa no gubernamental, la construcción de la propuesta pretende enviar el mensaje a la sociedad que “**en Colombia la realidad de estos líderes y defensores supera la ficción, que puede ser aún más cruel de lo que ven en pantalla y se puede poner peor en el postconflicto**” por la radicalización de la violencia que se vive en el actual escenario de negociación.

La intención es **volver visibles** ante el grueso de la población las consecuencias que el conflicto trae para aquellos que luchan por los derechos propios y de sus comunidades, y hacerlo de una manera “distinta y comprensible”, que **no se limite únicamente a los datos, cifras y contextos políticos** que dificultan muchas veces la recepción, interpretación y reflexión por parte de quien lee los informes y en consecuencia cualquier manifestación de solidaridad con las víctimas.

“El problema es que la gente no entiende que esto es importante, porque no conoce las realidades” asegura Guevara, por lo que el sentido de la propuesta es “**tratar de aproximar esas realidades al ciudadano de a pie, e incluso a los mismos funcionarios públicos**”, adoptando el discurso audiovisual para “contar de otra manera una realidad a la que difícilmente se aproximan”, distante de la forma comercial, en que la televisión ha presentado temas sensibles para el país como el narcotráfico.

<iframe src="https://www.youtube.com/embed/2GoKLuyLutc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Además de su trabajo al frente del Programa, Carlos Guevara, es el director y guionista de “El Cambio”, tarea que califica como “un viaje”, desde su concepción y producción para no “venderlo como una novela mexicana”, una propuesta mucho más cercana al cine y que demandó de un **fuerte trabajo de investigación y preparación para los actores** y sus roles, alejados de los clichés,  que son determinantes para la comprensión de la propuesta.

Teniendo en cuenta que el país transita por el primer trimestre del año, el Coordinador de Somos Defensores, asegura que temas sensibles como el cambio de Fiscal, la política del Estado de sacar los Acuerdos en tiempo record, lo que representa la firma y el tiempo que tome implementación de los mismos, son escenarios en que “la violencia contra los defensores se va a transformar”, como ocurrió en países como Nicaragua, Burundi y el Salvador, donde **se invisibilizan públicamente las agresiones tras la firma de la paz**, al considerar el conflicto como única causa de las vulneraciones.

Con la presentación del informe, Somos Defensores, logró establecer relaciones directas con el Gobierno para establecer reuniones para discutir los temas de fondo, que no se relacionan directamente con acciones bélicas, en el marco de la defensa de los derechos humanos, y sobre la protección que se le va a ofrecer a quienes se desmovilicen y pretendan hacer parte vida política nacional, tarea para la que el Estado no está preparado y necesita del apoyo de las organizaciones civiles “de manera preponderante”.
