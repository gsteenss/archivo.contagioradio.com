Title: 13 militares y un paramilitar fueron Condenados por ejecuciones extrajudiciales
Date: 2016-09-29 14:36
Category: DDHH, Nacional
Tags: Ejecuciones Extrajudiciales, Fuerzas Militares de Colombia, Paramilitarismo
Slug: 13-militares-y-un-paramilitar-fueron-condenados-por-ejecuciones-extrajudiciales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/nota_ejecuciones1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ElPilón 

###### 29 de Sept de 2016 

Tras 10 años de investigación, fueron **condenados 13 militares y un paramilitar por el delito de secuestro** **simple y asesinato** de dos campesinos el 22 de marzo de 2006 en la vereda Caño Cafre, municipio de Puerto Concordia, Meta.

Ese día, en horas de la tarde irrumpieron en la casa de la señora Alicia Peña Montoya un grupo de hombres del batallón Joaquín París del ejercito. Con insultos y vejámenes, registran la vivienda sin autorización, supuestamente en busca de armas y, al no encontrar lo que buscaban, **resuelven llevarse a dos campesinos que estaban en ese momento laborando en su finca**, entre ellos a su hijo Arcadio Torres Peña.

Hombres del ejército en compañía de un paramilitar de la zona, se los llevan con vida para luego ser asesinados en el batallón, y reportados como guerrilleros dados de baja en combate. En medio de la confusión la señora **Alicia decide ir hacia el lugar de los hechos y sola, decide enfrentar a paramilitares y militares exigiendo le den respuesta del paradero de su hijo**, a cambio recibe la advertencia de uno de los soldados quien le manifiesta que es mejor se vaya del lugar si no quiere ser también asesinada.

A pesar de la amenaza que recibe, Alicia se llena de coraje y valentía y decide denunciar el caso ante el Juzgado 60 de Instrucción Penal Militar y la personería del Municipio de Puerto Concordia, pasan los meses y dicha institución no da solución alguna, así que decide contactar al Colectivo de Abogados Orlando Fals Borda quienes en adelante serán los apoderados de su caso.

La primera acción del Colectivo es solicitar que la Justicia Penal Militar entregue el caso a la Fiscalía. **Cuando el ente investigador solicita la entrega del caso, responden que el expediente se ha extraviado**. Frente a ello, el abogado del colectivo y defensor de victimas de Estado, Rodrigo Orjuela, afirma que se trata de [maniobras utilizadas por estas instancias para entorpecer una correcta investigación.](https://archivo.contagioradio.com/tapen-tapen-continua-la-impunidad-en-caso-de-falsos-positivos/)

Orjuela añade “sabemos que el cien porciento de los casos que asume la Justicia Penal Militar quedan en total impunidad, (…) lo que se traduce en una prueba mas para decir que esto **fue un crimen de Estado, un crimen que fue cometido con el conocimiento de altos mandos del ejercito,** (…) y la fiscalía debe abrir investigación contra estos altos mandos, coroneles, que contribuyeron al crimen”.

Gracias a la valentía de la señora Alicia y los testimonios de vecinos que presenciaron los hechos, se obtienen pruebas [que incriminan a los soldados](https://archivo.contagioradio.com/militares-vinculados-con-falsos-positivos-podrian-ascender-a-generales/)y se demuestra que se trató de una **operación conjunta entre el ejército y paramilitares, pues Alicia al acercarse al batallón el día de los hechos logra identificar a alias *pescuezo***, paramilitar también reconocido por otra gente de la zona.

Las conclusiones iniciales emitidas por el penal militar, que señalaban a Arcadio y Rosendo como guerrilleros que se habían enfrentado a los soldados con revólveres, pierden credibilidad cuando se da a conocer que el batallón Joaquín París contaba con cerca de 80 hombres totalmente armados, un helicóptero, fusilería y fuerzas especializadas contraguerrilleras.

Durante el proceso aparece un nuevo testigo, se trata de un hombre que servía de guía a las tropas en ese entonces, quien aporta unas declaraciones contundentes, confirmando que se los llevaron vivos y después los dieron como muertos en combate. **Tiempo después asesinan a este testigo y hasta el momento no se ha podido hallar al culpable**.

Orjuela comenta que Alicia no se encuentra del todo satisfecha, pues la condena no le devuelve a su hijo, pero de cierta forma se siente tranquila al saber que **su muerte no quedó en la impunidad y se restablece su buen nombre, se demostró que no era un terrorista sino un campesino honrado** que justo ese día, 22 de marzo de 2006 cumpliría 33 años.

<iframe id="audio_13114753" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13114753_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}
