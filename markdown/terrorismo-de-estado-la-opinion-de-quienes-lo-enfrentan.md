Title: Terrorismo de Estado, la opinión de quienes lo enfrentan
Date: 2020-01-24 17:59
Author: CtgAdm
Category: Hablemos alguito
Tags: estado, Terrorismo, terrorismo de Estado
Slug: terrorismo-de-estado-la-opinion-de-quienes-lo-enfrentan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/WhatsApp-Image-2020-01-24-at-13.55.18.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este viernes 24 de enero regresó \#HablemosAlguito, programa en el que proponemos temas que tienen relevancia en la actualidad nacional. En esta ocasión, hablamos sobre el Terrorismo de Estado, lo que significa y el efecto en la vida de las personas que de una u otra forma lo enfrentan.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el programa hablamos con Gustavo Pedraza, hermano del líder asesinado Carlos Pedraza; Rene Guarín, hermano de Cristina Guarín (desaparecida en la toma y retoma del Palacio de Justicia); y Sebastián Escobar, abogado defensor de derechos humanos e integrante del Colectivo de Abogados José Alvear Restrepo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante la conversación, los invitados señalaron que el terrorismo es una serie de acciones llevadas a cabo contra una población específica para generar miedo, o terror. En ese sentido, el terrorismo de Estado haría referencia a los actos de terror cometidos por los Estados-nación con algún propósito.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los invitados también hablaron sobre la [Cumbre Mundial Contra el Terrorismo](https://archivo.contagioradio.com/la-cumbre-que-no-fue-mundial-ni-contra-el-terrorismo/) que se desarrolló en Colombia, y la impunidad. Respecto a esta, señalaron que la impunidad es uno de los grandes problemas que enfrenta el país, pues el Estado es juez y parte en los delitos que sus mismos representantes cometen.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F889621024801994%2F&amp;show_text=0&amp;width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:html -->  
**Síganos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Siga conectado con la conversación los viernes desde la 1 p.m. en Contagio Radio]</a>[.]

<!-- /wp:html -->
