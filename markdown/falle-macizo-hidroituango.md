Title: “Es alta la posibilidad de que falle el macizo rocoso en Hidroituango”: Experto en Geología
Date: 2019-02-06 13:48
Author: AdminContagio
Category: Ambiente, Entrevistas
Tags: Compuertas, EPM, Ituango, Río Cauca
Slug: falle-macizo-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Macizo-rocoso-hidroituango-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Siata.gov] 

###### [6 Feb 2019] 

Sobre Hidroituango se han escuchado dos versiones desde el mismo inicio de la construcción del Proyecto: Una sobre la trascendencia que tiene para la economía de EPM y Antioquia, y **otra sobre los riesgos, y efectos nocivos para el ambiente (incluidos los humanos)** que viven cerca a la obra. Durante mucho tiempo se ha privilegiado la primera versión, pero con **el riesgo inminente de falla en el macizo rocoso que sostiene la presa, y la serie de fallos que presenta, se debe prestar más atención a la segunda visión** sobre el Proyecto.

Una de las voces que apoya esa versión es la de **Modesto Portilla, Doctor en Ingeniería del Terreno y profesor de la Universidad Nacional**, quien ha señalado en repetidas ocasiones el riesgo para las comunidades que significa el proyecto Hidroeléctrico de Ituango. Para el profesor, hay que hablar sobre lo inmediato, lo urgente y lo importante para tener una visión clara sobre lo que ocurre con la Obra.

### **Lo Inmediato: El nivel del Río Cauca** 

El primer gran problema que enfrentó la construcción misma hace un año estuvo relacionada con un derrumbe al interior del macizo rocoso (la montaña) en el que se encontraban los túneles de desviación y la casa de máquinas, y su posterior reapertura producto de la fuerza del agua. Con los caudales normales del Río Cauca y en medio de una época de lluvias, se dijo que la prioridad era terminar el muro de la represa para poder abrir las compuertas del vertedero y así evacuar el liquido de la represa.

> Hoy en [@ConcejoMedellin](https://twitter.com/ConcejoMedellin?ref_src=twsrc%5Etfw) debatimos sobre [\#HidroItuango](https://twitter.com/hashtag/HidroItuango?src=hash&ref_src=twsrc%5Etfw) Puedes seguir el debate ((EnVivo)) por [@Telemedellin](https://twitter.com/Telemedellin?ref_src=twsrc%5Etfw) [pic.twitter.com/aHzRIfJwcm](https://t.co/aHzRIfJwcm)
>
> — Daniela Maturana (@dany\_matu) [22 de marzo de 2017](https://twitter.com/dany_matu/status/844538989218594816?ref_src=twsrc%5Etfw)

Según EPM, la maniobra les permitiría bajar la presión al interior del macizo rocoso inundado sin control, posteriormente, cerrar los túneles temporales de salida de agua, evacuar el liquido de la casa de máquinas y los túneles de descarga. **Para clausurar los túneles de desviación se deben instalar unas compuertas, y eso es lo que hizo la empresa hace unos días con el cierre de las compuertas 1 y 2.**

Sin embargo, el cierre de las compuertas en medio del verano, y con el efecto de la misma presa, **ha significado que el agua deje de fluir a través del vertedero y quede estancada en la Hidroléctrica**. Situación que se ha visto reflejada a lo largo del Río Cauca, desde Ituango hasta la desembocadura en el Río Magdalena. (Le puede interesar: ["El Río Cauca en emergencia tras operación de Hidroeléctrica de Ituango"](https://archivo.contagioradio.com/el-rio-cauca-en-emergencia-ambiental-por-sequias/))

Tal como lo lo explica el Doctor Portilla, al tomar la decisión de cerrar las compuertas sin tomar en cuenta el nivel del embalse, se debió evaluar las condiciones mismas del Río, porque el nivel del agua adelante de la Hidroeléctrica bajará hasta niveles históricos, situación que **durará "5 días, hasta que el agua de la Represa La Salvajina llegue hasta la zona y vuelva a caer por el vertedero"**.

### **Lo Urgente: El macizo rocoso anuncia que puede fallar** 

Como lo recuerda el Experto, **la decisión para cerrar las compuertas se debe a un temor de EPM por perder el control de lo que ocurre en los túneles al interior de la montaña;** y dichos túneles están en alguna medida destruidos, pues como lo explica Portilla, se sabe que el agua ha pasado de un túnel a otro sin que exista una conexión ideada para que esto ocurra. (Le puede interesar: ["Sequía y mortandad de peces alcanzan niveles críticos por operación de Hidroeléctrica de Ituango"](https://archivo.contagioradio.com/sequia-mortandad-peces-alcanzan-niveles-criticos-operacion-hidroituango/))

En ese sentido, el profesor sostiene que el revestimiento al interior del macizo rocoso debe estar derruido, y no será fácil repararlo. Pero lo relevante de esta situación es que **mientras sube el nivel del embalse para que el agua salga por el vertedero, habrá una presión lateral contra la montaña**, y cuando suba el nivel del agua, dicha presión se aumentará, al tiempo que hará fuerza al muro mismo de la represa.

Aunque el profesos sostiene que es poco probable que ceda el muro, la falla de la construcción misma puede estar en el vertedero: Porque **la hidroeléctrica estaba diseñada para tener 5 compuertas y 3 salidas de agua, pero en su lugar hay 4 compuertas y solo 2 salidas de agua**; y en segundo lugar, porque un vertedero "en ninguna represa" se diseña para operar por largos periodos de tiempo, por el contrario, se usa en casos excepcionales y por cortos periodos de tiempo.

### **Lo importante: Los efectos ambientales de estas decisiones** 

El profesor es claro en afirmar que **las decisiones que está tomando EPM, desde el principio de la obra misma responden a intereses empresariales y no al cuidado y respeto por las comunidades que habitan el cañón del Río Cauca**, y es contundente al decir que "es un cinismo total decir que la decisión de cerrar las compuertas es para proteger las comunidades". (Le puede interesar: ["Hay riesgo de avalancha en municipios aledaños a Hidroeléctrica de Ituango"](https://archivo.contagioradio.com/informe-revela-riesgos-de-un-desastre-mayor-en-hidroituango/))

Adicionalmente, Portilla es rotundo al señalar que **es imposible la generación de energía en Hidroituango en los próximos años**, porque los túneles de la obra, la casa de máquinas y las turbinas generadoras de energía no están listas para trabajar, y toma mucho tiempo para que puedan entrar en funcionamiento. (Le puede interesar: ["Lo que EPM no ha dicho sobre apertura del vertedero"](https://archivo.contagioradio.com/apertura-vertedero-hidroituango/))

Teniendo en cuenta ambas situaciones, y que en su opinión, hay una alta probabilidad de que falle el macizo rocoso, generándose un canal de acceso que permita al Río Cauca retomar su cauce normal (generando una pared de agua y lodo que afectaría todo el cañón del Río, y de forma secundaría a los municipios cercanos a la desembocadura con el Magdalena), **lo importante ahora es pensar en el ambiente: Flora, fauna y comunidades que dependen del Río Cauca, y se ven afectados con cada decisión errada que toma EPM.**

> ¿Están [@ANLA\_Col](https://twitter.com/ANLA_Col?ref_src=twsrc%5Etfw), [@PGN\_COL](https://twitter.com/PGN_COL?ref_src=twsrc%5Etfw) y demás autoridades evaluando el daño ambiental producido por el corte del agua en [@hidroituango](https://twitter.com/hidroituango?ref_src=twsrc%5Etfw)? Es que [@EPMestamosahi](https://twitter.com/EPMestamosahi?ref_src=twsrc%5Etfw) dijo ayer que "revertirá" este daño ambiental, que también es un daño a la comunidad. [pic.twitter.com/6SvCCqmSaq](https://t.co/6SvCCqmSaq)
>
> — Luis Alfonso Yepes B. (@luisyepesb) [6 de febrero de 2019](https://twitter.com/luisyepesb/status/1093139050721087488?ref_src=twsrc%5Etfw)

###### <iframe src="https://www.youtube.com/embed/ySQpA67sbg4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_32332596" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32332596_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
