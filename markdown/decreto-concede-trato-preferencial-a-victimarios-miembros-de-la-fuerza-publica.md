Title: 10 razones de víctimas contra decreto que suspende ordenes de captura a la Fuerza Pública
Date: 2017-05-09 18:15
Category: DDHH, Nacional
Tags: Crimenes de Esstado, MOVICE
Slug: decreto-concede-trato-preferencial-a-victimarios-miembros-de-la-fuerza-publica
Status: published

###### [Foto: Archivo Particular] 

###### [09 May 2017] 

Más de 50 organizaciones de víctimas y defensoras de DD.HH, expresaron que la decisión del presidente Juan Manuel Santos de suspender todas las órdenes de captura contra miembros de la Fuerza Pública procesados, y que incluso hayan cometido graves violaciones a los derechos humanos, **les da un trato “preferencial” y un “beneficio arbitrario” y niega el derecho a la no repetición de las víctimas**.

1\. El decreto, desconoce la posición de garantes de los derechos que ocupan los miembros de la Fuerza Pública, que implica que sí cometen un delito la sanción debería ser más drástica, hecho que hace parte de estándares internacionales y constitucionales que tienen como finalidad diferenciar el grado de las responsabilidades de quienes **“teniendo la función de proteger los derechos de la población, atentan contra ella"**.

2\. Las  suspensiones **se podrían considerar como “beneficios arbitrarios” a quienes nunca comparecieron a un proceso penal o decidieron ser prófugos de la justicia**.

3\. La medida vulnera el principio de igualdad entre los mismos integrantes de la Fuerza Pública que se encuentran privados de la libertad y los que decidioeron ser prófugos. Le puede interesar:["Decreto dejaría en libertad a agentes estatales que han violado derechos humanos"](https://archivo.contagioradio.com/gobierno_decreto_agentes_estatales/)

4\. Ni el decreto, ni la ley 1820 de 2016, **establecen, frente a las suspensiones de las órdenes, el derecho a la información y la posibilidad de participación e intervención de las víctimas** o sus representantes, en estos procesos, ni siquiera tratándose de crímenes que no harían parte de la JEP como las ejecuciones extrajudiciales, la desaparición forzada, la tortura, la violencia sexual o el genocidio. Le puede interesar: ["Falsos positivos no pueden pasar a la JEP"](https://archivo.contagioradio.com/38886/)

5.  El Fiscal General de la Nación, Néstor Humberto Martínez, no tiene en cuenta que su aplicación termina profundizando un trato preferencial, puesto que **los guerrilleros permanecen en las zonas veredales, mientras que los agentes estarán en libertad**.

6\. La libertad de los agentes estatales somete a las víctimas de crímenes de Estado **a encontrarse cara a cara con su victimario antes de cualquier proceso judicial.**

7\. La medida desestimula el trabajo de fiscales que investigan a integrantes de la Fuerza Pública, dado que se podría entender que dichas investigaciones pierden sentido y utilidad cuando pasarían a ser asumidas por la JEP.

8\. La aplicación, en conjunto, de las reformas y las especificidades legales a los puntos del acuerdo se configurarían en una "autoamnistía" que implicaría la entrada en vigencia del estatuto de Roma y por consiguiente la intervención de la Corte Penal Internacional.

9\. Estas y otras disposiciones legales parecerían estar orientadas a la desfiguración del SIVJRNR como un mecanismo más de impunidad para agentes del Estado.

10\. Esta y otras medidas como la inclusión del Min Defensa en la junta directiva del CNMH demostrarían **"las prioridades del Estado en la construcción de narrativas que desvirtúen la existencia de la criminalidad estatal"**. Le puede interesar: ["Inclusión del Min Defensa afectaría autonomía del Centro Nacional de Memoria"](https://archivo.contagioradio.com/mindefensa-afecta-centro-nacional-memoria/)

[Víctimas rechazan suspensión de condenas de miembros de la Fuerza Pública](https://www.scribd.com/document/347872251/Victimas-rechazan-suspension-de-condenas-de-miembros-de-la-Fuerza-Publica#from_embed "View Víctimas rechazan suspensión de condenas de miembros de la Fuerza Pública on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe class="scribd_iframe_embed" src="https://www.scribd.com/embeds/347872251/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-MCCLTgwSspJN44N3iorZ&amp;show_recommendations=true" data-auto-height="false" data-aspect-ratio="0.7729220222793488" scrolling="no" id="doc_96806" width="100%" height="600" frameborder="0"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
