Title: Cine comunitario por la descolonización audiovisual y territorial
Date: 2016-06-02 14:17
Author: AdminContagio
Category: 24 Cuadros
Tags: Decolonización territorial y audiovisual, Diplomado Cine comunitario, La casita popular Tocancipa
Slug: cine-comunitario-por-la-descolonizacion-audiovisual-y-territorial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/13076551_1611608192491208_8670507402019486587_n-e1464977811685.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: La casita popular 

##### 3 Junio 2016 

Con el antecedente de las experiencias vividas en México y Argentina, se desarrolló en Colombia el 1er Diplomado de Cine Comunitario, dirigido a jóvenes rurales del municipio de Tocancipa, Cundinamarca. Una iniciativa promovida en nuestro país por Praxis Camino Films con el apoyo de organizaciones de Chile y Perú participantes en la Semana por la soberanía audiovisual realizada en Bogotá.

Desde el 20 de mayo y durante 1o días de trabajo, los participantes se integraron a la propuesta trasladada al municipio de la sabana bogotana, con el objetivo de descolonizar el audiovisual y aterrizar los conceptos del cine comunitario, en zonas particularmente afectadas por fuertes problemáticas territoriales por cuenta de la presencia de canteras y multinacionales que poco a poco han mermado la vida rural.

Laura Cadena, vocera e integrante del equipo base del diplomado, explica que el diplomado se dividió en dos espacios en las dos semanas en las que se realizó, en la primera se vincularon jóvenes como voluntarios en su mayoría provenientes de diferentes regiones del país, que con su colaboración al proyecto sustentaron su cupo en la semana académica que tuvo lugar desde el 26 de mayo.

Algunos de los talleristas vinculados a las jornadas, pertenecen a organizaciones audiovisuales y también barriales como  la Casa Airuguay de Ciudad Bolívar y su grupo musical andino Ambientes del Sur, el Colectivo escuela audiovisual Belen de los Andaquíes y su experiencia de enfrentar el conflicto usando la cámara como herramienta de defensa, el Colectivo Revelados y su taller de fotografía estenopeica, entre otros.

Uno de los talleres más destacados fue el de sonido, realizado en la Quebrada onda, territorio ancestral afectado por las canteras, donde los participantes tuvieron la oportunidad de capturar los sonidos del ecosistema y los animales con la asesoría de Alejandro Molano sonidista del cortometraje "Paciente".

El cierre y entrega de certificados a los participantes tendrá lugar este viernes 3 de junio en la Redada Miscelanea cultural ubicada en la Calle 17 \#2-51 desde la 1 p.m., en un evento que contará con agrupaciones en vivo, radio abierta, serigrafía, proyecciones y conversatorios de entrada de libre.

<iframe src="http://co.ivoox.com/es/player_ej_11772833_2_1.html?data=kpakmZecd5Shhpywj5abaZS1k5eah5yncZOhhpywj5WRaZi3jpWah5ynca3V1tfOjajFqMbiwpCajZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
