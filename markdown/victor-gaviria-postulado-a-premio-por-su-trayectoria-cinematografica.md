Title: Víctor Gaviria postulado a premio por su trayectoria cinematográfica
Date: 2015-10-10 10:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Academia colombiana de artes y ciencias cinematográficas, Carta a una sombra, el abrazo de la serpiente, La tierra y la sombra, Premio Fenix Cine Iberoamericano, Víctor Gaviria cineasta colombiano
Slug: victor-gaviria-postulado-a-premio-por-su-trayectoria-cinematografica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/1b66e81ab98484f23880b50dd5fcf707.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:cromos.com.co 

###### [8 oct 2015]

El cineasta antioqueño Víctor Gaviria, fué seleccionado por la Academia colombiana de Artes y Ciencias Cinematográficas (ACACC) para competir por el Premio Fénix Iberoamericano a la trayectoría; durante la gala que tendrá lugar en Ciudad de México el próximo 25 de noviembre.

Con películas como Rodrigo "D" no futuro (1990), La vendedora de Rosas (1998), Sumas y restas (2005) entre otras, Víctor Gaviria a logrado alcanzar varios reconocimientos y galardones a nivel nacional e internacional por "hacer de sus producciones un reflejo latente de la realidad del país", deacuerdo con lo expuesto por la ACACC al informar de su elección.

La del presente año será la segunda edición del Premio Fénix, competencia que cuenta con 13 categorías y 4 reconocimientos especiales, entre los que se encuentra al que optará el director colombiano, concedido por las diferentes academias y asociaciones de cine de los diferentes países de la región.

Gaviria, competirá con directores de la talla de Guillermo del Toro (España), Fina Torres (Venezuela), Fernando Pérez (Cuba), Patricio Guzmán (Chile), Jorge Sanjinés (Bolivia), António da Cunha (Portugal), Francisco Lombardi (Perú), Ángel Muñiz (República Dominicana), Juan Carlos Maneglia (Paraguay), Óscar Castillo (Costa Rica), Guillermo Escalón (El salvador) y José Sacristán (España).

Colombia participará además con "El abrazo de la serpiente" de Ciro Guerra, nomiada en siete categorías, el documental "Carta a una sombra" de Daniela Abad (2015) nominado en la categoría de Mejor Fotografía Documental y "La tierra y la sombra" de Cesar Acevedo (2015) apor Mejor Fotografía en ficción.
