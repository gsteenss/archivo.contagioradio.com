Title: Defensor de Ambiente es judicializado por EMGESA
Date: 2016-06-29 13:09
Category: Ambiente, Nacional
Tags: Persecución Judicial, Quimbo
Slug: defensor-de-ambiente-es-judicializado-por-emgesa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/quimbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: defensaterritorios.wordpress.com] 

###### [29 Junio 2016 ]

El defensor de derechos humanos Miller Dussan, que ha hecho **oposición a los efectos nocivos del la represa del Quimbo,** está siendo víctima de una persecución judicial por parte de la multinacional Emgesa que inició un proceso judicial en su contra.

De acuerdo con Miller Dussan, líder de de Asoquimbo e integrante del Movimiento Ríos Vivos a quién la empresa Emgesa demando, en días pasados le llego una citación del juzgado promiscuo municipal del municipio de Gigante, con la finalidad de asistir a una audiencia de formulación e imputación de cargos, bajo el delito **de obstrucción de vías que afectan el servicio público**. Este hecho se presenta luego de que Emgesa solicitará actas de Asoquimbo y del intento del organismo del CTI de ingresar a la casa del defensor de derechos humanos bajo la premisa de que esa era la sede de la organización Asoquimbo y que ahí se encontraban las actas.

Por otro lado Dussan argumenta que hasta el momento no tiene conocimiento sobre desde hace cuanto se encuentra la demanda interpuesta, pese a que ha acudido a las entidades correspondientes para obtener respuestas "es una situación paradójica en la medida en que sectores de la justicia **judicializan a quienes somos defensores de la justicia** al servicio de las personas que han sido vulneradas en sus derechos económicos,  sociales y ambientales por parte de las empresas multinacionales".

Organizaciones internacionales se han pronunciado frente a esta persecución judicial como es el caso de la Comisión Internacional de Juristas, que hace poco entregaron el informe **"El Quimbo megaproyecto derechos económicos, sociales,culturales y protesta social en Colombia"** que se presentará en la próxima audiencia pública ambiental el primero de septiembre de este año. En el informe se establece que [varios activistas de la comunidad han sido objeto de denuncias penales por parte de funcionarios de Emgesa](https://archivo.contagioradio.com/activistas-de-asoquimbo-serian-judicializados-este-jueves/) y que esas denuncias pueden ser consideradas como tentativas de criminalizar el ejercicio legitimo de la protesta social y el trabajo de los defensores de derechos humanos.

Otra de las organizaciones que se ha expresado es el Observatorio para la Protección de los Defensores de Derechos Humanos, que en un comunicado **condenó y expresó su preocupación frente al continuo hostigamiento** judicial en contra del señor Miller Dussan, que parece estar vinculado a su legitima [labor en denfensa de los derecho humanos y el medio ambiente en oposición al proyecto Hidroelectrico del Quimbo.](https://archivo.contagioradio.com/uribe-santos-y-anla-los-responsables-del-ecocidio-en-el-quimbo/)

"Mi actuación ha sido defender a la víctimas del desarrollo, **defender a quienes se les ha vulnerado todos sus derechos constitucionales** y por lo visto es la empresa la que ahora pretende o me interpone una demanda precisamente por defender el derecho a que a la gente se le respeta la vida digna" afirmo Dussan.

<iframe src="http://co.ivoox.com/es/player_ej_12068479_2_1.html?data=kpedmJ2Ye5qhhpywj5acaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5ynca7dzdHS1JCoudTnwtOYj5Cll7DFtq66pLSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
