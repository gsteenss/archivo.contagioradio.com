Title: Denuncian que helicóptero del Ejército atentó por error contra grupo de indígenas
Date: 2018-09-21 16:13
Author: AdminContagio
Category: DDHH, Nacional
Tags: Derechos Humanos, ejercito, indígenas, Resguardos
Slug: helicoptero-atento-error-indigenas
Status: published

###### [Foto: Contagio Radio] 

###### [21 Sep 2018] 

La **Organización Indígena de Antioquia (OIA)** denunció que en enfrentamientos entre el Ejército Nacional y grupos armados al margen de la Ley, una de las aeronaves que apoyaban el operativo de las Fuerzas Militares **atentó por error contra un grupo de indígenas** que se encontraban en el territorio sin embargo, gracias a su conocimiento de la zona y agilidad, **lograron salir ilesos.**

En un comunicado, la OIA detalló que los enfrentamientos iniciaron cerca de las 12 m y se extendieron por más de hora y media. Los hechos ocurrieron luego de que el tropas del Ejército decidieran avanzar desde el sector conocido como **'Gedega', en el Urabá antioqueño hasta la comunidad Isla, al interior del Resguardo Río Murindó** en el que habitan 74 familias indígenas del pueblo Embera.

Durante el enfrentamiento, las familias quedaron en medio del fuego cruzado, y un grupo de indígenas, **entre ellos una mujer en estado de embarazo,** fueron atacados por error con ráfagas de fusil por 1 de los 2 helicópteros que apoyaban la avanzada del Ejército en la zona. Sin embargo, **todos los miembros del resguardo lograron salir ilesos de la situación. **(Le puede interesar: ["Cuenten con nosotros para la paz, nunca para la guerra: ONIC"](https://archivo.contagioradio.com/cuenten-con-nosotros-para-la-paz-onic/))

La Organización rechazó las acciones armadas en su comunidad, y exigió que se respeten los territorios indígenas. Finalmente, declaró que son hombres y mujeres de paz que desde sus espacios buscan transformar el país, e **instaron al presidente Iván Duque "a buscar de manera urgente una salida negociada al conflicto con los distintos actores armados"** porque con sus acciones ponen en riesgo la vida de los habitantes del país. (Le puede interesar: ["32 líderes indígenas han sido asesinados en el primer semestre de 2018"](https://archivo.contagioradio.com/32-lideres-indigenas-han-sido-asesinados-en-el-primer-semestre-del-2018/))

###### [Reciba toda la información de Contagio Radio en ][[su correo] [[Contagio Radio](http://bit.ly/1ICYhVU)]
