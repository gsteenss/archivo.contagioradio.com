Title: Atentan contra la vida de Cecilia Lozano, líder y víctima de la masacre de Mapiripán, Meta
Date: 2018-01-30 11:48
Category: DDHH, Nacional
Tags: ASOMINUMA, Cecilia Lozano, Víctimas de Mapiripan
Slug: atentan-contra-la-vida-de-cecilia-lozano-lider-y-victima-de-la-masacre-de-mapiripan-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/Mapiripan_grandeinterno_revista_hekatombe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Hekatombe] 

###### [30 Ene 2018] 

María Cecilia Lozano, víctima de la masacre de Mapiripán y líder social del departamento del Meta, en los procesos de reparación colectiva y restitución de tierras, fue víctima de un atentado, el pasado 27 de enero, cuando un **hombre intentó acabar con su vida propinándole dos heridas con un arma blanca, una en su rostro y la otra en su espalda.**

De acuerdo con el comunicado de prensa realizado por las organizaciones Humanidad Vigente y ASOMUDEM, María Cecilia se estaba alistado para dirigirse a su hogar, después de departir con unos amigos, posteriormente observó que dos motocicletas pasaron en diferentes ocasiones por donde ella se encontraba y cuando se dirigía a tomar un taxi, miró que una mujer y un hombre descendieron de una de las motos. El hombre salió corriendo hacia ella y Cecilia reaccionó entregándole sus pertenencias, sin embargo, el hombre las ignoró y **lo propinó dos heridas con arma blanca, una en su espalda y la otra en su rostro**.

La líder social se encontraba acompañada por un amigo que también resulto herido al intentar defenderla. Así mismo, el 28 de enero, una defensora de derechos humanos llamó al celular de Cecilia y contestó un hombre que le aseguró que la líderesa estaba **“chupando gladiolo y la vamos a partir el culo”**. (Le pude interesar: ["Crímenes contra defensores de DD.HH aumentaron 17% en 2017 con respecto al 2016"](https://archivo.contagioradio.com/crimenes-contra-defensores-aumentaron-17-en-2017-con-respecto-a-2016/))

María Cecilia Lozano cuenta con medidas de protección y un esquema de la Unidad Nacional de Protección, no obstante, los **escoltas no se encontraban con ella porque le habrían manifestado que el esquema solo es diurno**, mientras que el atentado ocurrió en la noche.

### **El trabajo de María Cecilia por la defensa de las vítimas** 

Olga Silva, integrante de Humanidad Vigente, organización que ha acompañado el trabajo de las víctimas en Mapiripan, aseguro que María Cecilia es una mujer que ha trabajado inagotablemente por la defensa de los derechos de las víctimas y de las mujeres y ha estado **abanderando la exigencia del proceso de reparación individual y colectiva y de restitución de tierras en Mapiripan**.

Cecilia es la actual coordinadora departamental de mujeres, hace parte de la mesa departamental de víctimas y es la representante legal de Asociación de mujeres desplazadas del Meta, ASOMUDEM).

[DENUNCIA-PÚBLICA-ASOMUDEM](https://www.scribd.com/document/370348051/DENUNCIA-PUBLICA-ASOMUDEM#from_embed "View DENUNCIA-PÚBLICA-ASOMUDEM on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="audio_23447199" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23447199_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="doc_4013" class="scribd_iframe_embed" title="DENUNCIA-PÚBLICA-ASOMUDEM" src="https://www.scribd.com/embeds/370348051/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-RaKJjTwVi0Wh2ql6a3p4&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7066666666666667"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
