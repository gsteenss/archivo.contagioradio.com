Title: La tercera parte de las especies animales se extinguirán por el cambio climático
Date: 2016-03-09 16:20
Category: Animales, Nacional
Tags: animales en vías de extinción, biodiversidad, cambio climatico, WWF
Slug: la-tercera-parte-de-las-especies-animales-en-el-mundo-se-extinguiran-por-el-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Oso-perezoso-e1457557436326.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Campesino 

###### [9 Mar 2016] 

De acuerdo con la organización World Wildlife Fund for Nature, WWF por sus siglas en inglés, **una tercera parte de las especies animales del planeta podrían extinguirse** sino se actúa desde ya para frenar el cambio climático **impidiendo que la temperatura aumente entre 1,5 y 2 grados.**

Según la WWF, uno de los impáctos más importantes tiene que ver con el deshielo del Ártico, lo que genera, por ejemplo, que las crías de las focas estén en riesgo de morir debido a que el hielo se rompe y mueren ahogados, también los osos polares tendrán cada vez más complicaciones para conseguir su alimento.

Durante el evento “**Los años más cálidos de la historia. ¿Por qué se calienta nuestro planeta?”, realizado en Madrid**, en el que participó la WWF, Carlos Velasco, impulsor de la web “MeteoMadrid”, afirmó que ha habido un descenso de las nevadas en los últimos años, además asegura que en los próximos años va a ser cada vez más difícil predecir la nieve debido al continuo cambio de comportamiento de los frentes atmosféricos.

En el caso colombiano el cambio climático también representa una grave amenaza, siendo este el país con el segundo lugar en biodiversidad del planeta. Susana Vélez, Especialista en Política Forestal y Cambio Climático de WWF explica que “Todos los rincones de Colombia son vulnerables y están expuestos al cambio, pero tenemos algunos mucho más vulnerables que son muchas veces las regiones más pobres, las que tiene menos capacidad de respuesta ante los fenómenos extremos. **El Chocó, la Amazonía, algunas partes de los Andes están más expuestas que otras a estos eventos extremos y tiene menos capacidad de respuesta para atender la emergencias y prepararse a la ocurrencia de estos fenómenos”,** lugares en los que se encuentran la gran mayoría de especies, muchas de las cuales solo se encuentran en Colombia.

En el país los animales más amenazados son el armadillo, la guacamaya bandera, el manatí del Caribe, el oso perezoso, la rana dorada, el mono tití del Caquetá, el mono tití cabeza blanca, la rana cornuda del amazonas, el oso hormiguero, el oso de anteojos, entre otros, para un total son **359 especies silvestres que están en peligro de extinción, que lo dejan entre los ocho países responsables de la mitad del deterioro del planeta,** según un informe de la Unión Internacional para la Conservación de la Naturaleza, Uicn y la ONU.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
