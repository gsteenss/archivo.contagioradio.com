Title: Crímenes de las FARC y el Estado deberán ser esclarecidos con Comisión de la verdad
Date: 2015-06-05 14:42
Category: Entrevistas, Paz
Tags: Alberto Yepes, Camilo Villa, comision de la verdad, Coordinación Colombia- Europa-Estados Unidos, crímenes de estado, ELN, FARC, fuero penal militar, habana, justicia transicional, paramilitares, paz, proceso de paz, víctimas del conflicto armado
Slug: crimenes-de-las-farc-y-el-estado-deberan-ser-esclarecido-con-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/dialogos_de_paz_-_afp.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.pulzo.com]

<iframe src="http://www.ivoox.com/player_ek_4600794_2_1.html?data=lZudkpydeI6ZmKiak5mJd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYy3wtLWztSPmsrgzcaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Camilo Villa, MOVICE] 

<iframe src="http://www.ivoox.com/player_ek_4600779_2_1.html?data=lZudkpybfY6ZmKiak5WJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9DhytjWh6iXaaOnz5DRx5DQpYzqxtfRw8mPqMbWxpDW1JDFp9Dh0caSpZiJhpLVxcaYxsqPrtbn1c7Qy8aPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alberto Yepes, Coordinación Colombia- Europa-Estados Unidos] 

###### [5 de Junio 2015]

Este jueves se anunció desde la Habana que se acordó la [creación de una Comisión de la Verdad](https://archivo.contagioradio.com/farc-y-gobierno-anuncian-la-creacion-de-una-comision-de-la-verdad/) cuando se hayan firmado los acuerdos de paz entre el gobierno colombiano y la guerrilla de las FARC. Los analistas y las víctimas, aseguran que esta comisión deberá estar acompañada **paralelamente de la justicia transicional** para que se pueda esclarecer la verdad que reclaman las víctimas.

“Es un anuncio muy importante, es una propuesta que no está terminada pero hay elementos que demuestran la dimensión de este acuerdo”, expresa Camilo Villa, quien asegura que, cuando se avanza en los procesos de víctimas, **se recobra confianza social en el proceso de paz**, el cual actualmente se encuentra pasando por un momento difícil en medio de la suspensión del cese al fuego bilateral de las FARC y el escalamiento de la guerra. Así como el MOVICE, la Coordinación Colombia- Europa-Estados Unidos, aplaude este acuerdo al que se ha llegado, teniendo en cuenta el apoyo de la comunidad internacional.

A través de esta comisión, se espera que sea posible no solo esclarecer hechos victimizantes por parte de las FARC, sino también la responsabilidad del Estado, teniendo en cuenta que por parte de las las guerrillas toda la comandancia tiene cargos en su contra, mientras que la gran mayoría de los crímenes de estado no están siendo investigados, y según Yepes,  **no hay ningún alto responsable por parte del estado.**

Para Yepes y Villa, con esta comisión de la verdad se debe adelantar tareas retrasadas con respecto a la responsabilidad estatal; eso dependerá de la autonomía y la interrelación con la justicia. Por lo tanto, como lo asegura el integrante del MOVICE, debe **haber beneficios o incentivos que hagan que los victimarios quieran hablar.**

Según Camilo Villa, "hay una gran cantidad de información que los paramilitares no han dado y que no ha podido ingresar a los procesos; han señalado fosas comunes, sitios donde torturaban y esa información no ha migrado". En este sentido, como lo dice el vocero de las víctimas, aunque la comisión de la verdad no tiene un afecto directo, sí se podrá descubrir **información relacionada con hechos victimizantes, que tendría que migrar a los procesos para que se aplique a la realidad.**

Así mismo, es importante que esta comisión tenga oficinas en todas las partes del país, teniendo en cuenta que muchas regiones de Colombia son invisibilizadas. Además,  acuerdo al  MOVICE, se debería tener acceso a la información secreta del Estado, pues según Villa, el gobierno colombiano ha sido **“negacionista, porque niega su responsabilidad y su relación con los grupos paramilitares”**, teniendo en cuenta que según los comisionados de la Comisión de Memoria Histórica del Conflicto, no hay voluntad de parte del gobierno para que se haga un proceso pedagógico respecto a las conclusiones de estos informes, donde el Estado aparece como uno de los grandes responsables de los más de 50 años del conflicto.

Por otra parte, una vez se establezca la comisión de la verdad, deberá haber una serie de modificaciones cuando ELN entre a hacer parte del proceso, ya que "no puede haber más comisiones, esta debe ser la definitiva”, señala el integrante del MOVICE.

También deberá existir reformas frente a temas como la Ley de Justicia y Paz o el Fuero Penal Militar, entendiendo que esas reformas deberán se integrales así como lo debe ser la reparación a las víctimas.

Finalmente, la comisión tendría que centrarse en responsabilidades colectivas; sino, los 3 años que planean se desgastaría, ya que no sería suficiente esclarecer alrededor de 7 millones  de hechos victimizantes registrados por el Estado, dice Camilo.

**Fortalecimiento del fuero penal militar mientras se habla de comisión de la verdad **

De acuerdo a Alberto Yepes, el refuerzo del fuero penal militar es una de las grandes contradicciones del proceso de paz. Él indica que con la propuesta del fuero penal militar, propuesto por el Ministerio de Defensa, se implementará un **Derecho Internacional Humanitario “totalmente distorsionado según el cual muchos crímenes no serían declarados como delitos y terminarían exculpados”.** Teniendo en cuenta que este DIH, que difunde y ha usado el Ministerio de defensa, permite un uso desproporcionado de la fuerza “según el cual la prioridad es producir muertos y no preservar la vida; muchos delitos se convertirían en errores militares”.

Esto terminaría creando un panorama de **“inseguridad jurídica para los 47 millones** de colombianos que no van a poder invocar los derechos humanos cuando se les viole el derecho a la vida, y solo podrán hacer uso de un Derecho Internacional Humanitario amañado”.

Cabe recordar que este proyecto de acto legislativo, está a un debate en el Congreso de la República para ser aprobado, pues de ocho debates en total que necesita para ser aprobado, ha pasado por siete.
