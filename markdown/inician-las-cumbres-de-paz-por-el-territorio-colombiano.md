Title: Llegan las "Cumbres de Paz" por el territorio colombiano
Date: 2016-01-27 13:26
Category: DDHH, eventos
Tags: Cumbre Agraria Campesina Étnica y Popular, Cumbres para la paz, proceso de paz
Slug: inician-las-cumbres-de-paz-por-el-territorio-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/cumbres-de-paz-poster.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [27 Ene 2016] 

Con el objetivo de construír de manera colectiva una propuesta de agenda política y metodológica para alcanzar la paz sostenible y con justicia social, la Cumbre Agraria Campesina, Étnica y Popular llegará a los territorios con las **Cumbres de Paz, "Sembramos esperanza, cosechamos país"**, en el marco de las negociaciones que se adelantan con las FARC EP en la Habana y los acercamientos en el mismo sentido con el ELN.

Desde la iniciativa se busca trabajar en varios de los temas que no son abordados en los diálogos entre el gobierno y la insurgencia, y que son **indispensables para la paz al estar en la órbita de las realidades y problemáticas sociales presentes en los territorios**, particularmente en aquellos que han recibido con mayor intensidad los impactos del conflicto armado, donde no se ha garantizado la protección de los derechos humanos.

Los **8 ejes temáticos** propuestos para abrir los escenarios de trabajo son: Tierras, territorios colectivos y ordenamiento territorial, economía propia contra el modelo de despojo, minería, energía y ruralidad, Cultivos de Coca, marihuana y amapola, Derechos políticos, grantías, víctimas y justicia, derechos sociales, relación campo - ciudad, Paz, justicia social y solución política.

La convocatoria espera reunir al conjunto de las organizaciones sociales en las diferentes cumbres regionales y nacional, en **un gran diálogo nacional que sirva para articular y fortalecer las iniciativas** que han surgido en el seno de cada una de ellas, incrementando de manera unificada su capacidad política, de participación, incidencia y movilización en el presente año.

El punto de partida de las Cumbres por la Paz, será la sede de **Coorpotolima** en la ciudad de Ibague el próximo **29 y 30 de enero**, para luego desplazarse a Bogotá, Antioquia, Cauca, Caldas,  Arauca, Chocó, Sucre, Guajira, Ibague Cesar, Huila entre otros.

<iframe src="https://www.youtube.com/embed/I9kNvBPlkis" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ] 
