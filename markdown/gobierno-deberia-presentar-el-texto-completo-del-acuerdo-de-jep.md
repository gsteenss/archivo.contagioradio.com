Title: Gobierno debería presentar el texto completo del acuerdo de JEP
Date: 2015-09-29 15:27
Category: Nacional, Paz
Tags: Acuerdo de Justicia, acuerdo de justicia firmado en la habana, Comisión de Paz, Conversaciones de paz de la habana, diálogos en la Habana, enrique santiago, FARC, Heriberto de la calle, Juan Manuel Santos, proceso de paz, Timoleon Jimenez
Slug: gobierno-deberia-presentar-el-texto-completo-del-acuerdo-de-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Humberto_de_la_calle_y_Sergio_jaramillo_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: abc.py 

<iframe src="http://www.ivoox.com/player_ek_8689527_2_1.html?data=mZulm5qWe46ZmKiak5qJd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi9DWysrf0NSPqMbWxteSpZiJhaXVjNXfx9jJstXV05DSzpDYqdno0JDQ0dLUsMbo0JDRx9GPpcTpxteah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Enrique Santiago, Abogado] 

###### [29 sep 2015] 

Tras la entrevista de Sergio Jaramillo y Humberto de la Calle, integrantes del equipo de paz del gobierno de Colombia, el abogado Enrique Santiago, asesor de la mesa de conversaciones, afirmó que lejos de contribuir a despejar dudas sobre la JEP, lo que se hace es generar más confusión a presentar interpretaciones diversas y poco precisas.

El jurista indicó que al estar el acuerdo firmado entre las dos partes "lo que el **gobierno tiene que hacer** es presentar **el texto completo, lo cual evitaría dobles y triples interpretaciones**”, como se ha venido haciendo hasta ahora, dando paso a la especulación y recordó que es el gobierno nacional el que no ha querido publicarlo en su totalidad.

Frente a las afirmaciones en torno a que no se podrían juzgar expresidentes, Enrique Santiago afirma que este sistema de justicia es para todas las personas que hayan tenido que ver con la guerra y que incluso **la Comisión de Acusaciones de la Cámara tendrá que entregar los expedientes al tribunal que defina la mesa de conversaciones**.

Los motivos por los cuales no se ha publicado el documento son inciertos, sin embargo, lo que está claro y reafirma el jurista español es que lo que se firmó “*un sistema que ayuda a la verdad y a combatir la impunidad, además que procederá contra todos los responsables*”, en definitiva “e*s un sistema que está diseñado para estimular la verdad integra*”.

Santiago afirma sobre el documento  que esta jurisdicción estaría controlada por la unidad de investigación y acusación que se defina en la Habana, para aplicar penas restaurativas y de prisión, para esto basará su **proceder en las acusaciones e investigaciones** que presenten órganos tanto estatales, como organizaciones civiles y de derechos humanos.

Indica que “*decir que hay cláusula que condena expresamente el juzgamiento de algún presidente es falso de toda falsedad*” y que para que se diera un caso así sería mediante pruebas presentadas por los órganos del Estado y civiles y que tengan el debido proceso, igual que con todos los actores del conflicto.

Para las investigaciones, la imputación de penas, tanto restaurativas, como privativas de la libertad, **existirán entes internacionales que se encargarán de realizar veedurías a estos procesos**, los cuáles deben cumplirse en el marco de lo que dicte esa comisión de investigación y acusación.

Ver también:  
[Acuerdo sobre justicia FARC y gobierno](https://archivo.contagioradio.com/acuerdo-sobre-justicia-farc-y-gobierno-de-colombia/)  
[Claves del acuerdo entre FARC y gobierno](https://archivo.contagioradio.com/sistema-integral-de-verdad-justicia-reparacion-y-no-repeticion/)
