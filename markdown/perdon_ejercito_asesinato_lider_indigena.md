Title: Así fue el acto de perdón del Ejército por asesinato del líder indígena Eleazar Tequia
Date: 2018-02-02 18:06
Category: DDHH, Nacional
Tags: asesinatos de líderes sociales, Chocó, ejercito
Slug: perdon_ejercito_asesinato_lider_indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/perdon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Jesús O. Durán Tellez, Colombia Plural] 

###### [2 Feb 2018] 

“**Si el perdón sirve para construir un Chocó mejor, pido perdón**”, fueron las palabras con las que el Brigadier **General Mauricio Moreno Rodríguez**, comandante de la **Fuerza de Tarea Conjunta Titán**, quien pidió perdón por el asesinato de **Eleazar Tequia, Guardia Mayor Indígena del pueblo Embera  por parte de militares. **

El acto se llevó a cabo en la carretera Medellín-Chocó, gracias a la mediación de la Diócesis de Quibdó y de la Oficina del Alto Comisionado de la ONU para los Derechos Humanos. Fue así como la comunidad logró escuchar de boca del General pero también del gobernador de Chocó palabras que, si bien no les devolverán a a Eleazar, significan una medida de reparación tras el daño causado a la comunidad.

En presencia  el General, el coronel de la Policía, el gobernador del Chocó, el alcalde de El Carmen de Atrato, delegados de la Fiscalía, la Procuraduría, la Defensoría del Pueblo,además de la Cruz Roja Internacional y la Comisión Vida, Justicia y Paz de la Diócesis de Quibdó; junto a otras organizaciones sociales regionales, como el Foro Interétnico Solidaridad Chocó, se llevó a cabo el acto de perdón en memoria de Tequia.

En medio de las exigencias y el llanto de las mujeres que lamentaban la pérdida del líder indígena e general **Moreno se vio obligado a pedir perdón por el asesinato de este indígena de 41 años que entregó su vida a la protección y defensa de su pueblo**, en el marco de las protestas de la comunidad exigiendo una mejor calidad en la educación.

### **Los hechos** 

De acuerdo con la ONIC, el crimen  fue cometido por integrantes de la Fuerza Pública, cuando los indígenas, después de una movilización, se disponían a retornar a sus territorios.

Los hechos se presentaron en el kilómetro 18 de la vía que comunica a Quibdó con Medellín. Allí miembros del Ejército Nacional** **llegaron a la zona debido a que camioneros denunciaron que estaban siendo extorsionados. A su vez, en ese lugar se encontraba Tequia con otro compañero de la Guardia, ambos indígenas fueron llamados por miembros del Ejército para realizar una requisa y posteriormente fue encontrado el cuerpo de Tequia, sin vida.

Aunque el Ejército Nacional aseguró que el hecho se produjo cuando un integrante de la Fuerza Pública estaba siendo desarmado por indígenas que evitaban que se le retirara el arma, Cilsa Arias, de la ONIC, señaló que que el cuerpo del indígena fue hallado con dos impactos de bala y que **los indígenas solo estaban armados con su bastón de mando.**

### **Exigen que el Ejército reconozca a la guardia indígena** 

Moreno  **se comprometió esclarecer el asesinato del líder con la investigación y a compensar a la esposa de Tequia y a sus cinco hijos menores de edad.** Sin embargo, la comunidad no sintió que se tratara de un verdadero acto de arrepentimiento ya que por otro lado, el general Alberto Sepúlveda, comandante de la Séptima División, aseguró que no tenía que pedir perdón por nada.

"Es una reunión pero nadie le está pidiendo disculpas a nadie, **nosotros no tenemos ninguna culpa aquí.** Hay una reunión para concertar que retiren el bloque sobre la vía", dijo Sepúlveda.

Ante esta situación, el acto de pedido de perdón del general Moreno, apenas es un destello de reconocimiento de los militares frente a los errores cometidos contra la comunidad y la guardia indígena. Tras este hecho ,la ONIC,  exige que **la Fuerza Pública reconozca a la Guardia Indígena como órgano de control social** y legítima dentro de los pueblos indígenas y que se encuentren los responsables de la muerte del indígena.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
