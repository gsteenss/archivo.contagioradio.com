Title: Luis Darío Rodríguez, segundo líder asesinado en Córdoba en menos de una semana
Date: 2020-01-19 10:59
Author: CtgAdm
Category: DDHH, Nacional
Tags: cordoba, Cordoberxia, lideres sociales, Luis Darío Rodríguez
Slug: luis-dario-rodriguez-segundo-lider-asesinado-en-menos-de-una-semana-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/WhatsApp-Image-2020-01-18-at-15.27.37-e1579409507378.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Fundación Social Cordoberxia {#foto-fundación-social-cordoberxia .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

La **Fundación Social Cordoberxia** denunció que el pasado viernes 17 de enero en horas de la tarde, hombres armados que se desplazaban en motocicleta asesinaron al líder de tierras **Luis Dario Rodríguez Narváez** en la vereda Nueva Unión de Tierralta, Córdoba.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Organización señaló que **Dario Rodríguez** era agricultor y pescaba para asegurar la alimentación de su familia, compuesta por tres hijos y esposa. El líder tenía su lugar de residencia en Tierralta, y adelantaba procesos de "requisición de tierras para familias reasentadas en el marco de la inundación de las tierras del Alto Sinú por la hidroeléctrica URRA 1".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El líder hacía parte de la Asociación Unión de Familias Desplazadas y Vulnerables de Tierralta (UFADESVUL) e integraba la Red de Derechos Humanos del Sur de Córdoba. Su homicidio se suma al del líder Jorge Luis Betancourt, asesinado el pasado 13 de enero en Montelíbano, Córdoba. (L puede interesar: ["Dos nuevos casos de asesinato a líderes sociales enlutan al país"](https://archivo.contagioradio.com/dos-nuevos-casos-de-asesinato-a-lideres-sociales-enlutan-al-pais/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Sobre Dario Rodríguez, y la violencia en el sur de Córdoba

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El líder había terminado su jornada de pesca cuando dos hombres armados, que se desplazaban en motocicleta lo atacaron, causándole la muerte. En el caso de Betancourt, los armados también lo atacaron en la tarde, pero en su casa, en la que también estaban su esposa y tres hijos. (Le puede interesar: ["Se libra una guerra por tomar el control del Nudo de Paramillo"](https://archivo.contagioradio.com/guerra-control-nudo-paramillo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De acuerdo a las denuncias de Cordoberxia, el sur de Córdoba es un territorio en disputa por parte de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y los Caparrapos, en unión con el Nuevo Frente 18. Según defensores de derechos humanos, **la pugna se da por el valor estratégico de la zona (en el Nudo del Paramillo)**,y se agrava ante la ausencia de acciones eficaces por parte del Estado en su conjunto para garantizar los derechos de la población.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De allí se explica la violencia sufrida por los habitantes del sur de Córdoba, así como por los del Bajo Cauca Antioqueño, que esta semana padecieron el asesinato de cinco personas en el corregimiento El Guaimaro de Tarazá, Antioquia. (Le puede interesar: ["En el Bajo Cauca persiste el reclutamiento forzado de niños"](https://archivo.contagioradio.com/en-bajo-cauca-persiste-el-reclutamiento-forzado-de-ninos/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Defensoría del Pueblo había alertado por riesgos a la población

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Recientemente, la Defensoría del Pueblo emitió la Alerta Temprana N° 054-19 en la que expone que los **26.123 civiles que habitan las zonas rurales cercanas al Nudo del Paramillo están en alto riesgo** por presencia de actores armados ilegales. (Le puede interesar: ["En audiencia se denunció la grave situación de DD.HH. que vive Córdoba"](https://archivo.contagioradio.com/en-audiencia-se-denuncio-la-grave-situacion-de-dh-que-vive-cordoba/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque en el territorio se ha reforzado la presencia de la Fuerza de Tarea Aquiles, en una alerta pasada la Defensoría señaló que el aumento del pie de fuerza en el territorio no ha sido solución al conflicto porque, como ocurre en otras zonas del país, integrantes de las AGC se instalan en comunidades indígenas, lo que ha llevado a las comunidades a ser víctimas de estigmatización y persecución por parte de los uniformados, que los señalan de ser colaboradores de los ilegales.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "El territorio no vive de la hoja de Coca"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el director de la Fundación Social Cordoberxia Andrés Chica, no se entiende cómo se siguen presentando asesinatos de líderes en el territorio cuando allí funciona el Plan de Acción Oportuna (PAO), hay establecidas las llamadas Zonas Futuro y operan tres mil hombres con la Fuerza de Tarea Aquiles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Chica añadió que mientras el Estado en conjunto (Fuerza Pública e instituciones civiles) señala que en el territorio no pasa nada y "no asume que la arremetida tiene una lógica paramilitar", los llamados[puntos](https://archivo.contagioradio.com/persisten-incursiones-paramilitares-en-el-bajo-atrato/)de las AGC establecen cuando las jóvenes deben entregar sus cuerpos a los hombres armados y cuando los jóvenes están listos para tomar un fusil.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por último, el director de la Fundación sostuvo que entre los intereses económicos de los armados ilegales no está como principal el narcotráfico, porque "el territorio no vive de la hoja de Coca". En su lugar, **las dinámicas económicas estarían ligadas a la extracción minero energética**, mientras la violencia contra la población buscaría desalojar los territorios. 

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:html -->  
<iframe id="audio_46794032" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46794032_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
