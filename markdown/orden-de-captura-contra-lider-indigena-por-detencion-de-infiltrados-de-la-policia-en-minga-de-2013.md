Title: Ordenan captura de líder indígena por detención de infiltrados de la policía en Minga de 2013
Date: 2015-10-07 16:26
Category: Uncategorized
Tags: Consejo Regional Indígena de Risaralda, CRIC, CRIR, Feliciano Valencia, Minga Indígena, ONIC, Recuperación de la Madre Tierra, Ruben Darío Guagarabe
Slug: orden-de-captura-contra-lider-indigena-por-detencion-de-infiltrados-de-la-policia-en-minga-de-2013
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/CRIR-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: colombiasoberanda 

###### [7 Oct 2015]

Ruben Darío Guagarabe, profesor indígena del resguardo Suratena en el municipio de Marsella, participó en la Minga Indígena de 2013 durante la cual fueron retenidos varios integrantes de la policía que se habrían infiltrado en la movilización. Los agentes fueron entregados a la defensoría del pueblo y se denunció en su momento que este hecho era una provocación. **La orden de captura tendría que ver con esos hechos y podría ser acusado de secuestro simple.**

En la denuncia también se asegura que el movimiento social en general y el movimiento indígena hay alerta máxima tras la detención de Feliciano Valencia en el departamento del Cauca y el anuncio de nuevas posibles órdenes de captura en contra de otros comuneros del Consejo Regional Indígena de Risaralda.

El CRIR exigió a los jueces que revoquen la orden de detención en contra Ruben Dario Guabarabe y pidieron la intervención de los entes de control y organismos internacionales, así mismo solicitaron el acompañamiento de las organizaciones defensoras de los derechos humanos nacionales e intencionales.

El pasado mes de Septiembre fue capturado por unidades del CTI el líder indígena Feliciano Valencia, acusado de secuestro simple y condenado a 16 años de prisión por el Tribunal Superior de Cauca. Los hechos con los que se relaciona su detención tienen que ver con el **ritual de castigo que, bajo la jurisdicción especial indígena, fue aplicado al cabo Jairo Danilo Chaparral  infiltrado en la minga indígena de La María, Cauca, en 2008**.

Vea también [Organizaciones sociales e indígenas rechazan judicialización y captura de Feliciano Valencia](https://archivo.contagioradio.com/sectores-sociales-e-indigenas-rechazan-detencion-del-lider-indigena-feliciano-valencia/)
