Title: Comisión de la Verdad sigue firme en su respaldo a la JEP
Date: 2019-03-14 14:23
Author: CtgAdm
Category: Nacional, Paz
Tags: comision de la verdad, JEP, sistema
Slug: comision-de-la-verdad-deja-en-firme-su-respaldo-a-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/D1k0PZeXQAAjnZW.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 14 Mar 2019 

La Comisión para el Esclarecimiento de la Verdad, presidida por el padre Francisco de Roux, se pronunció respecto a la coyuntura que vive Colombia e invitó a la sociedad a proteger el **Sistema Integral de Verdad, Justicia, Reparación y No Repetición**; reiterando su apoyo a la JEP, la cual afirmó "asegura una justicia transicional para que no haya impunidad en este proceso".

Durante la intervención también estuvieron presentes las comisionadas **Ángela Salazar, Marta Ruiz y el comisionado Saul Franco** quien fue enfático en que a pesar de no ser una institución judicial sino estatal, "esto no quiere decir que no veamos la gravedad en que se ha puesto la esperanza de paz”,  de cara al  difícil momento que atraviesa la implementación del Acuerdo de Paz.

El presidente de la Comisión también hizo un llamado para frenar la polarización que vive el país, destacando la importancia que tienen las víctimas como parte central del Acuerdo de Paz. [(Lea también: Comisión de la Verdad se convierte en una esperanza para organizaciones del Caribe)](https://archivo.contagioradio.com/comision-de-la-verdad-se-convierte-en-una-esperanza-para-organizaciones-del-caribe/)

<iframe src="https://www.youtube.com/embed/ZR_WW4_RKjM" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

"Creo que como ciudadanos debemos darle paso a la palabra, silenciar los fusiles y solucionar los conflictos ciudadanos, a pesar de las embestidas que recibe el Acuerdo de Paz, uno va a los territorios y encuentras las ganas de decir y saber la verdad " concluye la comisionada Ángela Salazar. [(Lea también: 10 pautas para presentar un caso ante la Comisión para el Esclarecimiento de la Verdad) ](https://archivo.contagioradio.com/10-pautas-para-presentar-un-caso-consolidado-ant)

Al respecto de las dificultades que ha enfrentado la Comisión, la cual vio su presupuesto recortado en un 40% afectando su funcionamiento, el comisionado Saul Castro explica que el mecanismo continúa su trabajo, destacando el apoyo de la comunidad internacional y "estamos buscando otros recursos y financiamiento, vamos a seguir en esa cruzada" afirma.

<iframe id="audio_33381397" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33381397_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
