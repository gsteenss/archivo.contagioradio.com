Title: Asesinan a Nidio Dávila, líder de Marcha Patriótica, en Nariño
Date: 2017-08-08 12:21
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, nariño
Slug: asesinan-a-nidio-davila-lider-de-marcha-patriotica-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Foto-Lider-asesinado-marcha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [08 Ago 2017] 

**El pasado 6 de agosto fue asesinado en el corregimiento La Esmeralda, en Nariño, el líder y defensor de derechos humanos Nidio Dávila**, en frente de los habitantes, luego de que fuese sacado de su vivienda por un grupo de hombres que de acuerdo con la comunidad eran integrantes de las Autodefensas Gaitanistas de Colombia. A este hecho se suman las denuncias que diferentes organizaciones habían hecho previamente en donde alertaban de la presencia de hombres armados.

De acuerdo con los pobladores, los hechos ocurrieron cuando 20 hombres que vestían prendas del uso privativo de las Fuerzas Militares y portando armas de largo alcance, llegaron hasta la vivienda de Nidio Dávila y le exigieron el pago de un “impuesto”, ante la negativa del defensor de derechos humanos, los hombres sacan a la fuerza a Nidio de su hogar y le manifiestan a toda la comunidad que **“van a matar a todos los que vienen a hablar de sustitución** y a todos los campesinos que estén de acuerdo con eso de la sustitución”.

Minutos después Dávila es trasladado al sector conocido como “el cable” y frente a todas las personas que allí se encontraban, **es asesinado de un disparo y luego lo arrojan al río Verde**. Según testimonios de los habitantes detrás de este hecho estarían miembros de las autodenominadas Autodefensas Gaitanistas de Colombia del frente Conquistador del Sur o integrantes del frente “Los del Morocho”. (Le puede interesar: ["Atentan contra dos integrantes de las FARC, en Cauca"](https://archivo.contagioradio.com/44695/))

Nidio Dávila, pertenecía a la Asociación de Trabajadores Campesinos de Nariño, a la Coordinadora Nacional de Cultivadores de hoja de Coca, Amapola y Marihuana (COCCAM), al Proceso de Unidad Popular del Suroccidente Colombiano (PUPSOC) y a la plataforma política Marcha Patriótica. (Le puede interesar: ["Amenazan de muerte a integrantes de la Juventud Rebelde y Marcha Patriótica"](https://archivo.contagioradio.com/44543/))

### **Nariño bajo el control paramilitar** 

De acuerdo con un comunicado de prensa de la Red de derechos humanos Francisco Isaías Cifuentes, días previos al asesinato de Dávila, **ya se había tenido presencia de integrantes de las autodenominadas Autodefensas Gaitanistas de Colombia** y se había puesto en conocimiento a las autoridades, sin que estas tomaran medidas.

El 5 de julio, la organización denunció que un grupo de **20 hombres autodenominados como integrantes de las AGC**, estuvieron en la vereda del El Rosario y convocaron a una reunión a los integrantes de la comunidad. (Le puede interesar:["Presidente de CREDHOS es amenazado de muerte"](https://archivo.contagioradio.com/44445/))

Posteriormente el 9 de julio, la Red de derechos Francisco Isaías Cifuentes emitió una denuncia pública en donde expresó que El Rosario “es una zona de amplia presencia militar y policial” **en donde se distribuyen panfletos firmados por las AGC del Bloque Conquistador del Sur, sin que haya alguna acción por parte de la Fuerza Pública.**

El 21 de julio habitantes de los municipios de Leiva y el Rosario, señalaron que habían visto un grupo de 60 hombres vistiendo camuflados y portando armas de largo alcance. (Le puede interesar:["Parmilitares amenazan a líder por reunirse con organismos internacionales"](https://archivo.contagioradio.com/paramilitares-amenazan-a-lider/))

Frente a estos hechos la Red de Derechos Francisco Isaías Cifuentes y la plataforma Marcha Patriótica le exigen al presidente Juan Manuel Santos, a la Gobernación de Nariño y las autoridades de la Fuerza Pública, que **se adelantes acciones y medidas necesarias frente a las capturas de los responsables del asesinato de Nidio Dávila** y la presencia de grupos armados en la región.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
