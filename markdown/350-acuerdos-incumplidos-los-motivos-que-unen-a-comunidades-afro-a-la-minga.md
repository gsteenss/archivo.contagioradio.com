Title: 350 razones de la comunidad afro para unirse a la Minga en Cauca
Date: 2019-04-02 17:51
Category: Comunidad, Nacional
Tags: comunidades negras del cauca, Minga Nacional, Plan Nacional de Desarrollo
Slug: 350-acuerdos-incumplidos-los-motivos-que-unen-a-comunidades-afro-a-la-minga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/IMG_0903.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Imagen de referencia:  
Procesos de comunidades negras] 

###### [2 Abr 2019] 

43 consejos comunitarios afro provenientes de **Suárez, Buenos Aires, Puerto Tejada, Guachené, Caloto, Santander, Miranda, Corinto y Villa Rica en Cauca** han decidido sumarse a la Minga indígena, exigiendo que sean cumplidos los compromisos adquiridos por el Gobierno con los grupos étnicos del país.

**Alexis Mina Ramos, consejero de la Asociación de Consejos Comunitarios del Norte del Cauca (ACONC)**, explica que el Consejo Regional Indígena del Cauca (CRIC) les había invitado con tiempo de antelación a unirse a la Minga durante las reuniones del Consejo Territorial Interétnico, pero la decisión había sido sometido a discusión pues se habían planteado unirse a la gran movilización nacional del mes de abril. [(Lea también: Pueblos indígenas de ocho departamentos se suman a la Minga)](https://archivo.contagioradio.com/jose-silva-nacion-wayuu/)

Mina afirma que decidieron unirse a la Minga a propósito de la coyuntura que existe sobre la aprobación del **Plan Nacional de Desarrollo (PND)** , donde se definirá el tema de la  inversión plurianual para los grupos étnicos del país, "si esperamos mucho tiempo será improbable que los acuerdos que hay con nosotros, con los indígenas y con los campesinos se puedan cumplir", explica.

**¿Qué exigen las comunidades afro?**

En particular y por una línea muy similar a la de los pueblos indígenas, las comunidades afro han exigido que la cifra que les fue asignada por el Gobierno en el PND, que oscila entre los 19 billones de pesos, tenga indicadores y metas concretas y no sean invertidos en otros proyectos de desarrollo o infraestructura "que si bien son de ayuda no suplen las necesidades reales" de las personas. [(Le puede interesar: Estamos en capacidad de resistir, la Minga se revitaliza cada día)](https://archivo.contagioradio.com/estamos-en-capacidad-de-resistir-la-minga-se-revitaliza-cada-dia/)

También se han manifestado sobre el tema de territorios, argumentando que **después de varios desplazamientos en particular los ocurridos durante 1986 con la construcción del embalse  Salvajina, los habitantes del lugar fueron desplazados y nunca fueron reubicados**, por lo que el Estado se comprometió a otorgarles cerca de 4.000 hectáreas, promesa que hasta la fecha no se ha cumplido.

"Tenemos compromisos incumplidos desde 1986, relativos a las consultas previas, el desarrollo vial del Cauca y el Valle y la cumbre agraria,  **en total tenemos una matriz condensada de 350 acuerdos**" expresa Mina quien agrega que también se manifiestan en apoyo a la JEP y al cumplimiento de los acuerdos en particular del Programa Nacional Integral de Sustitución (PNIS), del que espera que ante estas exigencias exista voluntad política por parte del Gobierno.

Frente a estos temas, Mina indica que la sustitución de cultivos de uso ilícito no ha avanzando en el departamento y que existen otras problemáticas como la minería ilegal y el narcotráfico son los que más tensiones provocan en el Cauca, poniendo en peligro a los líderes sociales quienes para el coordinador requieren "mecanismos de protección en concordancia con un enfoque diferencial".

<iframe id="audio_34096684" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34096684_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
