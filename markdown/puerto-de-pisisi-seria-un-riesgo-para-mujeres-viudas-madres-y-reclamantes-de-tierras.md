Title: Puerto de Pisisí sería un riesgo para mujeres viudas, madres y reclamantes de tierras
Date: 2020-11-24 23:04
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Antioquia, Puerto de Pisisí, reclamantes de tierras, Turbo, víctimas del conflicto armado
Slug: puerto-de-pisisi-seria-un-riesgo-para-mujeres-viudas-madres-y-reclamantes-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Puerto-de-Pisisi.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Guillermo Ossa / EL TIEMPO

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El director de[La Fundación Forjando Futuros](https://www.youtube.com/watch?v=nQT8NbkTAO4&feature=youtu.be) en representación de las víctimas de despojo de tierras, expresó la preocupación de mujeres viudas, madres de reclamantes de tierras por la construcción del puerto. Según ellas el lugar donde se construirá el Puerto de Pisisí en Turbo, Antioquia es un lugar muy importante para el esclarecimiento de varios crímenes de fuerzas del Estrado y de paramilitares.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/forjandofuturos/status/1331044181679206401","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/forjandofuturos/status/1331044181679206401

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**De acuerdo con Gerardo Vega Medina, en esta zona todavía se encuentran víctimas del conflicto armado, tales como concejales asesinados en la época de los 90.** «Estos asesinatos habrían sido reconocidos por miembros de las autodefensas y excomandantes de los grupos paramilitares en los procesos de justicia y paz».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, señala que así como hay cuerpos de víctimas, **en varias zonas y fincas cercanas al lugar donde se construirá el Puerto de Pisisí hay fosas comunes.** (Le puede interesar: [Comunidades siguen clamando por un acuerdo humanitario](https://archivo.contagioradio.com/comunidades-reiteran-necesidad-de-un-acuerdo-humanitario/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La fundación representará judicialmente a las victimas y trabajará para que las mismas sean reparadas. Además indica el defensor de DDHH que respecto a la construcción de Puerto Pisisí y las obras de infraestructura que se construyen en la región de Urabá, «nadie se opone a ellas, ni las víctimas ni ninguna organización social **se oponen al desarrollo de la región de Urabá. Pero ese desarrollo tiene que servir para todos, no puede ser simplemente para que enriquezca a unas cuantas familias, ese desarrollo tiene que ser para sacar de la pobreza a la gente más pobre en Turbo y Urabá y para reparar a las víctimas del conflicto armado»**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así lo señala María de Los Ángeles Cubides, quien hoy en día se encuentra reclamando las tierras que tenían en Puerto Pisisí y por las que asesinaron a su padre. «Yo pienso que el puerto es un avance muy importante para Urabá \[...\] **Siendo nosotros reclamantes de tierras lo que queremos es que nos de vuelvan la tierra si es posible, pero si no es posible que nos den una compensación, nos ubiquen en una parte. Soluciones»**. (Le puede interesar: [“La política de guerra del Gobierno reactivó todas formas de violencia”](https://archivo.contagioradio.com/la-politica-de-guerra-del-gobierno-reactivo-todas-formas-de-violencia/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/forjandofuturos/status/1328707880472735746","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/forjandofuturos/status/1328707880472735746

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
