Title: La terquedad de Peñalosa no pudo acabar con conectividad de la Van Der hammen
Date: 2018-02-20 22:13
Category: Ambiente, Nacional
Tags: Bogotá, Enrique Peñalosa, Lagos de Torca, Reserva Thomas Van der Hammen
Slug: la-terquedad-de-penalosa-no-pudo-acabar-con-conectividad-de-la-van-der-hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/reserva-thomas-van-der-hammen.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Uniandes] 

###### [20 Feb 2018] 

Tras la orden del Juzgado 42 Administrativo del Circuito de Bogotá, el alcalde de la capital, Enrique Peñalosa, tiene seis meses para **reevaluar la propuesta para ampliar la ciudad hacia el norte** con el proyecto denominado Lagos de Torca. Esto luego de que un grupo de ambientalistas interpusiera una acción de cumplimiento argumentando que el proyecto afecta la Reserva Thomas Van der Hammen por romper la conectividad.

De acuerdo con María Mercedes Maldonado, experta en ordenamiento territorial, esta noticia apoya la tesis de las organizaciones ciudadanas que han trabajado por la defensa de la Reserva. Indicó que “el plan zonal Lagos de Torca, que rompe la conectividad entre los cerros orientales y la Thomas Van der Hammen, **incumple una norma del Ministerio de Ambiente** del año 2000”.

### **Proyecto debe ser reformulado ** 

Enfatizó en que hay una posibilidad de conciliar entre la necesidad del Distrito para hacer vivienda y la protección ambiental. Esto en la medida en que, en las 1.800 hectáreas que compone el plan zonal Lagos de Torca “hay un espacio de **86 hectáreas** que completa la conectividad con la Reserva”.

Dijo que **“era un capricho del alcalde”** porque no tuvo en cuenta que se rompía la conectividad de los ecosistemas por lo que le reiteraron el llamado a no apelar la decisión del juez y “ajuster el plan para vincular las áreas verdes y pasar la edificabilidad a otras zonas”. (Le puede interesar:["Peñalosa continúa firme en sus proyectos depredadores: María Mercedes Maldonado"](https://archivo.contagioradio.com/reserva_thomas_van_der_hammen_enrique_penalosa_cerros_orientales/))

En repetidas ocasiones, los ambientalistas han reprochado las actuaciones del alcalde quien “le quiere poner construcciones industriales **justo encima de las zonas aledañas al humedal**”. Además, recordaron que el Ministerio de Ambiente debe estar en la capacidad de proteger el medio ambiente teniendo como premisa que es su responsabilidad.

### **Bogotá no tiene un crecimiento desbordado** 

Maldonado manifestó que el crecimiento de la ciudad no se ha dado de una manera desbordada sino que, por el contrario, “se ha ido reduciendo y **cada vez es menor** la tasa de crecimiento de la población”. Por esto, indicó que “el crecimiento de la población no indica que debe haber un crecimiento físico de la ciudad”. Argumentó que Bogotá todavía tiene sin construir las zonas de expansión urbana.

Afirmó que “no se necesitan incluir nuevas zonas de expansión teniendo en cuenta que todavía hay algunas zonas en Bogotá sin desarrollar y **algunas que se pueden densificar**”. Por esto manifestó que las cifras de crecimiento que ha presentado el alcalde “están infladas y “él tiene que hacer un ejercicio de planificación; para eso son los Planes de Ordenamiento Territorial para saber cuánto va a crecer la población y cuánto se necesita de suelo”. (Le puede interesar:["Ya está listo el primer proyecto de urbanización que amenaza a la Van der Hammen"](https://archivo.contagioradio.com/proyecto-lagos-de-torca_amenaza_van_der_hammen/))

Finalmente, recordó que la expansión de Bogotá hacia la sabana, **no va a solucionar los problemas de movilidad** y menos “urbanizando la zona de la Reserva Thomas Van der Hammen”. Indicó que la ampliación de la autopista y la carrera séptima tiene problemas ambientales pero que van a generar un alivio en la movilidad. Sin embargo, “los problemas de movilidad no se solucionan con vías sino con la construcción de un sistema de metro”.

<iframe id="audio_23953276" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23953276_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [[su correo]
