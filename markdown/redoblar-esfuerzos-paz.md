Title: Es momento de redoblar los esfuerzos por la paz: Iván Cepeda
Date: 2019-08-29 16:41
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Iván Cepeda, marquez, Movimientos sociales, paz
Slug: redoblar-esfuerzos-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

En medio de diferentes interpretaciones sobre el anuncio de Iván Márquez de crear un nuevo grupo guerrillero, la voz del senador Iván Cepeda llamó a la reflexión para entender las causas que motivaron a Márquez y sus compañeros, así como para idear soluciones que permitan una paz completa en el país. El Senador sopesó los argumentos dados para volver a las armas, pero indicó que este era un camino equivocado que debería ser corregido prontamente.

### **"Nos debe llamar a la reflexión sobre lo que ha pasado en el proceso de paz"** 

Cepeda señaló que el anuncio era algo que se estaba preparando y "que se veía venir", pero ahora que tuvo lugar, **"nos debe llamar a la reflexión sobre lo que ha pasado en el proceso de paz, cuáles son las causas de esta situación y cuáles deben ser los caminos para resolverla"**. En ese sentido, el senador valoró los argumentos presentados en el video de Márquez para volver a las armas. (Le puede interesar: ["Gobierno también es responsable del regreso a las armas de Iván Márquez"](https://archivo.contagioradio.com/gobierno-tambien-es-responsable-del-regreso-a-las-armas-de-ivan-marquez/))

El dirigente político manifestó que no se puede 'echar por la borda' lo que se ha alcanzado  con el proceso de paz, "que le ha traído muchas cosas buenas al país, a pesar de los incumplimientos, las equivocaciones, los cambios al Acuerdo y el asesinato de líderes sociales". En consecuencia, el defensor de derechos humanos afirmó que es en esos últimos temas donde se debe poner énfasis ahora: En la exigencia de implementar el Acuerdo, llegar a los territorios olvidados por el Estado, y "resolver los problemas sociales donde ha tenido lugar la violencia para poder superar las raíces del conflicto y avanzar hacia la paz total y definitiva".

> El proceso de paz continúa | Declaración de [\#DefendamosLaPaz](https://twitter.com/hashtag/DefendamosLaPaz?src=hash&ref_src=twsrc%5Etfw)  
> Ante el comunicado dado a conocer hoy por Iván Márquez, en otra hora negociador de los Acuerdos de La Habana, [\#DefendamosLaPaz](https://twitter.com/hashtag/DefendamosLaPaz?src=hash&ref_src=twsrc%5Etfw) expresa su posición :  
> ?<https://t.co/1KpMMURkf4>[\#SeguimosPorLaPaz](https://twitter.com/hashtag/SeguimosPorLaPaz?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/jZ2AgAcEeA](https://t.co/jZ2AgAcEeA)
>
> — DefendamosLaPazColombia (@DefendamosPaz) [August 29, 2019](https://twitter.com/DefendamosPaz/status/1167123622185644037?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **"Creemos que la solución está en el diálogo para que quienes tomaron esta decisión, retomen el proceso"** 

Una de las grandes avances que consideró el senador es que la ciudadanía cada vez se involucra más en la implementación de la paz, muestra de ellos son las diferentes expresiones en apoyo a este proceso como Defendamos la Paz. Para quienes integran este, y otros movimientos, Cepeda hizo un llamado a no flaquear en el empeño de la construcción de la paz, y en su lugar, **ver en este momento "una oportunidad para redoblar los esfuerzos y conquistar la consolidación del proceso".**

El líder político concluyó que este no es momento para generar nuevas dinámicas de violencia y confrontación, sino para buscar una salida global y simultánea a la guerra mediante el diálogo; siguiendo ese camino, respecto a Márquez y las disidencias de las FARC, "nosotros creemos que la solución está en el diálogo, en el acercamiento, para que quienes tomaron esta decisión retomen el proceso de paz". (Le puede interesar:["El Acuerdo es mucho más grande que unas personas que se apartan"](https://archivo.contagioradio.com/acuerdo-grande-personas-apartan/))

**Síguenos en Facebook:**  
<iframe id="audio_40608960" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40608960_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_40608960" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_40608960_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
