Title: Tribunal ético por los derechos de los trabajadores/as de la agroindustria colombiana
Date: 2014-12-17 16:54
Author: CtgAdm
Category: Hablemos alguito
Slug: tribunal-etico-por-los-derechos-de-los-trabajadoresas-de-la-agroindustria-colombiana
Status: published

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fs784had.mp3)  
Los días 11 y 12 de Septiembre en el Salón Fundadores de la Universidad Autónoma de Colombia, ubicada en la Calle 12 B N 4-31, Bogotá. Se realizará el Tribunal ético por los derechos de los trabajadores/as de la agroindustria colombiana.  
   
El objetivo del Tribunal Ético es sensibilizar a la opinión pública sobre la violación a los derechos de las y los trabajadores de la agroindustria, emitir sentencias condenatorias contra las empresas de este sector e incidir en las decisiones políticas a favor de las y los trabajadoras de la agroindustria.  
Además de visibilizar y denunciar públicamente la violación permanente de los derechos laborales de trabajadoras y trabajadores de la agroindustria, se pretende fortalecer las alianzas y la solidaridad entre organizaciones sindicales y organizaciones campesinas en la región, así como construir una agenda regional de incidencia para promover la igualdad de género y el empoderamiento de la mujer.  
Escuchamos a Arturo Portillas abogado de la Asociación de Abogados Laboralistas, él nos explica el porqué del propio tribunal ético y profundiza sobre las principales violaciones de DDHH que se cometen en el sector, en Colombia y en otros países de Suramérica.  
En el programa dialogamos sobre la contratación mediada por sindicatos y empresas de trabajo temporal, siguiendo por las condiciones infrahumanas en el propio trabajo, las inexistentes medidas de seguridad, las jornadas extensivas de carácter ilegal, y la no afiliación a la seguridad social. Es importante remarcar el perfil que busca la propia agroindustria, es decir mujeres con 4 o más hijos, o sea mujeres con una necesidad urgente que se ven obligadas a aguantar todo tipo de violaciones de los derechos laborales. De tal forma se agrava la situación de la mujer trabajadora por el mero hecho de ser mujer y tener hijos.  
Por otra parte profundizamos sobre la destrucción del medio ambiente a través de monocultivos como la palma, el banano, los cultivos de flores que están secando actualmente los acuíferos y los páramos. Conversamos también sobre el entramado empresarial que permite a dichas empresas mutar con el objetivo de poder eludir los impuestos del propio estado. También es importante remarcar como los tratados de libre comercio han beneficiado a todas estas empresas y sus respectivos negocios.
