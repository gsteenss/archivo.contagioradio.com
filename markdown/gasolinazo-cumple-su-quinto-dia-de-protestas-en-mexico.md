Title: Gasolinazo cumple su quinto día de protestas en  México
Date: 2017-01-05 10:18
Category: Uncategorized
Tags: gasolinazo, mexico
Slug: gasolinazo-cumple-su-quinto-dia-de-protestas-en-mexico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/pf-3108120314_PROTESTA_GASOLINA_HC02-1-d.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Porceso.mx] 

###### [5 Enero 2017] 

Una grave situación se está presentando en México debido al **ajuste del precio de la gasolina que aumentó hasta en un 20%, medida que ha afectado a la población y que ha generado protestas en diferentes ciudades del país bajo la consigna "no más Gasolinazo"**, argumentando que una gran mayoría de la ciudadanía no podrá tanquear sus vehículos por los altos costos que se están presentando y un temor en el alza de los demás productos que dependen del combustible.

De acuerdo con la Secretaría de Hacienda los cambios obedecen al aumento del precio de la gasolina internacionalmente, ya que México tiene que importar la gasolina **debido a la crisis que afrontan las refinerías de este país, algunas de ellas llevan más de 10 años** cerradas. Situación a la que se le suma el desabastecimiento en diferentes estados, que según Pemex, compañía que actualmente provee la gasolina, se ha generado por el aumento en la demanda.

Sin embargo, una de las críticas más importantes que se le ha hecho a la medida del aumento del precio de la gasolina del presidente Peña Nieto, es que no es igual en todos los estados de México, sino que el país se **dividió en 90 regiones produciendo que en algunos lugares el aumento fuera del 14% mientras que en otros fue del 20%.**

Hoy inicia el quinto día de protestas que hasta el momento ha dejado un saldo de **250 personas detenidas y 150 estaciones de gasolinas bloqueadas**, hechos a los que el gobierno de Peña Nieto ha respondido que esta medida era necesaria y que no dará marcha atrás “apelo a que la sociedad escuche las razones del porqué de esta decisión, que de no haberse tomado, debo decir, serían más dolorosos los efectos y las consecuencias”.

Y es que la inconformidad de la población se ha hecho sentir desde el primero de enero, día en que entro a regir la medida, cuando **cientos de mexicanos salieron a tomarse algunas calles de las ciudades**, bloquear las vías y hacer tomas simbólicas de edificios y estaciones de gasolina. De otro lado también se ha reportado actos de vandalismo como robos de combustible y asaltos a las tiendas.

Frente a estos hechos algunos empresarios dueños de estaciones de gasolina, como el grupo Octanfuel, informaron que de seguir con estos actos **cerraran por lo menos 750 estaciones que manejan en Veracruz.** Acción que podría replicarse en otras ciudades del país.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
