Title: Tras asesinato de indígena, Pueblo Awá exige garantías en medio de las balas y el Covid-19
Date: 2020-04-01 19:34
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Barbacoas
Slug: tras-asesinato-de-indigena-pueblo-awa-exige-garantias-en-medio-de-las-balas-y-el-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/DtloyO9WoAEPZca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Referencia Awá: mesa permanente de concertación de los pueblos indígenas

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El día 26 de marzo del 2020 cerca de las 9:00 am en Barbacoas, Nariño fue asesinado Wilder García, indígena Awá perteneciente al Resguardo de Tortugaña Telembí, la comunidad afirma que mientras hacen lo posible por cumplir con la medida de aislamiento, grupos armados han puesto en la mira a líderes y lideresas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los sucesos ocurrieron en la vivienda de Wilder, en cercanías al río Pipalta en el municipio de Barbacoas, Nariño cuando los atacantes irrumpieron en su hogar y lo asesinaron frente de su familia compuesta por su esposa y dos menores. La comunidad resalta que debido a la actual situación de aislamiento y las amenazas de los grupos armados, resultó complejo avanzar en el levantamiento del cuerpo sin vida de su compañero. [(Le puede interesar: Asesinatos contra los pueblos indígenas son selectivos: Aida Quilcué)](https://archivo.contagioradio.com/asesinatos-contra-los-pueblos-indigenas-son-selectivos-aida-quilcue/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Organización Nacional Indígena de Colombia, (ONIC) ha alertado que el pueblo indígena habita en medio de un conflicto en el cual participan “quince grupos al margen de la ley”, incluidas las disidencias de las FARC, bandas delincuenciales y el ELN, grupos que se disputan el territorio y el control sobre los cultivos de uso ilícito en Nariño.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A esta situación se suma la ocurrida el pasado 24 de marzo cuando hombres armados intimidaron a quienes pasaban frente al centro educativo “Los Telembies” en el corregimiento de Buena Vista, realizando disparos en la zona, donde se encuentran comunidades y familias Awá en situación de desplazamiento. [(Lea también: Lilia García, secretaria del Cabildo Awá fue asesinada en Nariño)](https://archivo.contagioradio.com/lilia-garcia-secretaria-del-cabildo-awa-fue-asesinada-en-narino/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Pueblo Awá pide no desviar la atención de la crisis humanitaria

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

"Sabemos que la preocupación por el Coronavirus ha hecho que la atención de todos, se vuelque a la pandemia y con justa razón, pero en los territorios se sigue atentando contra los derechos humanos de quienes buscamos pervivir en paz " expresó la UNIPA. [(Le recomendamos leer: Asesinan a Leonardo Nastacuas, líder de la comunidad Awá en Nariño)](https://archivo.contagioradio.com/asesinan-a-leonardo-nastacuas-lider-de-la-comunidad-awa-en-narino/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"El pueblo Awá es un pueblo indígena que se encuentra en vía de extinción física y cultural por que ha tenido que sobrevivir en medio de la guerra y en plena cuarentena siguen asesinando a jóvenes en el territorio"**, manifestó Carlos Nastacuas, integrante de la Unidad Indígena del Pueblo Awá [(UNIPA).](https://twitter.com/awaunipa)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Vemos una afectación muy grave y más cuando el Gobierno ha dicho que cada grupo étnico atenderá la pandemia según sus costumbres, pero mirando la situación del pueblo Awá es muy difícil por el olvido histórico del Gobierno", explica a medios locales Rider Pai, Consejero Mayor asegurando que el conflicto armado está aprovechando esta pandemia para atacar a los pueblos ancestrales cuando están en casa salvaguardándose. [(Lea también: Con Oneida Epiayú y Constantino Ramírez son más de 115 indígenas asesinados en el último año)](https://archivo.contagioradio.com/con-oneida-epiayu-y-constantino-ramirez-son-mas-de-115-indigenas-asesinados-en-el-ultimo-ano/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante estos sucesos, la comunidad Awá ha exigido a los diversos grupos armados legales e ilegales que respeten la vida de sus habitantes, reiterando que se abstengan de entrar a sus territorios indígenas no solo por la actual pandemia sino en respeto de su autonomía.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
