Title: Inicia campaña por el NO a tortura animal en la Plaza la Santamaría
Date: 2015-09-01 17:37
Category: Animales, Voces de la Tierra
Tags: 25 de octubre, Bogotá Sin Toreo, Consulta Antitaurina, corridas de toros, crueldad animal, Elecciones 2015, Maltrato animal, Plaza La Santamaría
Slug: inicio-campana-por-el-no-a-las-corridas-de-toros-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/11948020_10153430662040020_187948499_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [1 Sept 2015]

Desde el colectivo Bogotá Sin Toreo se realizó el lanzamiento de la campaña por el NO a las corridas de toros en Bogotá. Allí se hizo un llamado a la ciudadanía para que se apropien y empoderen de esta campaña teniendo en cuenta que se trata de **“un momento histórico” que puede producir un verdadero cambio en la ciudad.**

Durante la rueda de prensa,  se explicó que el costo de 40 mil millones de pesos que se ha dicho en diversos  medios empresariales es “una falacia”, teniendo en cuenta la respuesta de la Registraduría a un derecho de petición de “Bogotá Sin Toreo”, donde se explica que si la consulta se realiza el 25 de octubre costaría 11 mil millones de pesos, donde **se incluye costos de la biometría que la Registraduría debe asumir haya o no consulta popular.**

Natalia Parra, integrante de Bogotá Sin Toreo, aseguró que es vital que la consulta antitaurina se lleve a cabo con las elecciones del próximo 25 de octubre, de manera que no se genere un gasto fiscal adicional, teniendo en cuenta que ya se tiene preparada toda una logística, lo que implicaría que **la consulta tendría un costo de 1600 millones de pesos y si el distrito imprime los tarjetones saldría por 65 millones de pesos.**

“Estamos aquí reclamando nuestros derecho a la participación luego de haber superado todos los requisitos legales… De aquí en adelante empieza una gesta ciudadana para buscar el NO a la crueldad en la Plaza la Santamaría”, expresó la integrante de Bogotá Sin Toreo.

Por su parte otros voceros del Bogotá Sin Toreo, invitaron a la ciudadanía a hacer parte de la campaña por el NO a las corridas de toros hasta el 25 de octubre, ya que este NO implicaría además, decir “**No a actos violentos cuando se habla de paz, No a una sociedad que promueva la violencia, No a la tradición por encima de la razón, No a la tortura de una animal, no a la crueldad animal”**.
