Title: Medellín alista los "corotos" para el AltavozFest 2015
Date: 2015-10-29 10:00
Category: Cultura, eventos
Tags: Aeroparque de Medellín, AltavozFest 2015, Festival Altavoz, Gogol Bordello, Música
Slug: medellin-alista-los-corotos-para-el-altavozfest-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Gogol.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gogol Bordello 

##### [29  0ct 2015]

Como viene acostumbrando a seguidores paisas y de otras latitudes desde hace más de una década, la edición 12 del "Altavoz Fest", llega con un cartel de lujo encabezado por la agrupación Neoyorkina "Gogol Bordello", que con su sonido gitano pondrá a vibrar a todos los asistentes.

9 agrupaciones internacionales; 10 nacionales, 3 departamentales; y cinco locales, subirán a tarima los días 31 de Octubre, 1 y 2 de noviembre, estrenando en esta oportunidad un nuevo escenario: el aeroparque Juan Pablo II, dividido en dos escenarios: Fest y Norte.

En total se presentarán 51 bandas incluyendo algunas agrupaciones participantes otros festivales como el Rock al parque de Bogotá, Rock al Río, Hip Hop al Parque, Altavoz Antioquia y el Festival Local Invazion,

Compartimos la [Programación](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Altavoz.png) completa del Festival, o si prefiere puede descargar la app  AltavozFest con toda la información, horarios y reseñas de los artistas del cartel. para que se programe con buenos sonidos, durante uno de los últimos puentes de este año.

[**Agrupaciones internacionales**]

**Gogol Bordello** (EU) / Asphyx (HOL) / Steel Pulse (ING) /Van fan culo (EC) / Miguel Botafogo (ARG) / El último Ke Zierre (ESP) / Da Culkin Clan (EC) / Catfish (FR) / **Sfdk** (ESP)

**Gogol Bordello:**

https://www.youtube.com/watch?v=GKkHMgCYMpM  
**SFDK:**  
https://www.youtube.com/watch?v=Bzf4aQIK6zI  
   
[**Agrupaciones nacionales, regionales y locales**]  
**Coffeeling Prole** (Hip Hop al Parque) / Colombian Blues Society / Inwaves (Invazion) / Iv Tiempos / La Minitk del Miedo / La Séptima (Rock al Río) / **Los petitfellas** / Mc K-no / Medellín Hardcore / Mula / Narcopsychotic (Rock al Parque) / Pedrina y Río /Profanía (Altavoz Antioquia) / Threat (Altavoz Antioquia) / Triple x

**Coffeling Prole:**

https://www.youtube.com/watch?v=lEGPhtnyHUo

[ **LosPetitFellas:**]

https://www.youtube.com/watch?v=IM-7NTa5HSw

[**Agrupaciones Ciudad Altavoz clasificadas para Altavoz Fest:**]  
Aorta / Braille / Calibre 38 / Colapso / Comandante Cobra / Eternal / Federico Goes / Futuro Simple / Ghetto Warriors / Golpe de Estado / Holocausto / Jaibanakus / La Fidel / La Tifa / Last Black Revolver / Mar Adentro / Memoria Insuficiente / Merchan Mch / Motin / Nix / No Señal / Perros de Reserva / Raven / Rosita y los Nefastos / Shapu Henzo / Signo Vital / Sonicals / Tucuprá

**Braile: **

https://www.youtube.com/watch?v=X9lAsHtunJs  
   
**La Tifa:**  
https://www.youtube.com/watch?v=OjTzG-bHPtg

[ ]
