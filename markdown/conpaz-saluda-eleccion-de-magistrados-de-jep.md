Title: JEP abre esperanza de una justicia que restaure los daños de la guerra
Date: 2017-09-28 14:37
Category: Nacional, Paz
Tags: Conpaz, JEP, Magistrados JEP, víctimas
Slug: conpaz-saluda-eleccion-de-magistrados-de-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/marcha-de-las-flores101316_142-e1506622054337.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CONPAZ] 

###### [28 Sept 2017] 

La red de víctimas Comunidades Construyendo Paz en los Territorios -CONPAZ-, **saludó la selección de los 51 magistrados que harán parte de la Jurisdicción Especial para la Paz -JEP-** e indicó que se abre una esperanza en el país "para cerrar la impunidad".

De acuerdo con ellos, **"por primera vez en Colombia se ha diseñado un sistema de selección democrática** de los integrantes de un Tribunal, un mecanismo inédito en el mundo y que puede indicar el camino a seguir frente a la crisis del aparato judicial en Colombia".

Igualmente, **destacaron la representación de las mujeres, las comunidades afrodecencientes y los indígenas** en los magistrados que harán parte de dicho tribunal. Con esto en mente, manifestaron que hay confianza en que los integrantes de la JEP estarán a la altura para enfrentar la impunidad, asegurar el respeto de los derechos de las víctimas". (Le puede interesar: ["Víctimas de Rito Alejo del Río esperan toda la verdad"](https://archivo.contagioradio.com/victimas-rito-alejo-del-rio/))

De acuerdo con Rodrigo Castillo, miembro de CONPAZ, la JEP le va a dar a las víctimas, por primera vez, **la posibilidad de conocer la verdad** de los hechos que sufrieron. Dijo que van a poder conocer que pasó con sus familiares, sus territorios y todo lo que alguna vez perdieron por culpa del conflicto armado.

Frente a las críticas que han hecho varios sectores sobre la composición de los magistrados y la JEP misma, Castillo recordó que **"la JEP es una iniciativa que sólo favorece a las víctimas**, seguramente los victimarios o quienes tengan algo que ver con estos hechos que la jurisdicción busca esclarecer, van a ser  los primeros en poner tropiezos para que el mecanismo no avance".

Finalmente, CONPAZ reiteró que las víctimas, "más que la cárcel, están **reclamando de manera incesante que haya verdad** frente a hechos que le han vulnerado sus derechos". (Le puede interesar: ["CONPAZ pide a Gobierno y ELN respetar propuestas humanitarias de cara a un cese bilateral"](https://archivo.contagioradio.com/tres-propuestas-de-las-comunidades-mientras-se-acuerda-cese-al-fuego-en-la-mesa-de-quito/))

Igualmente, invitaron al Fiscal General de la Nación, a los partidos políticos, "en particular a Cambio Radical y Centro Democrático, **a cesar sus ataques** sustentados en equivocas interpretaciones en la discusión de la Ley Estatutaria de la Jurisdicción Especial de Paz".

<iframe id="audio_21158023" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21158023_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
