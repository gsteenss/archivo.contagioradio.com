Title: La Cadena perpetua no reduce la criminalidad ni repara los daños a las víctimas
Date: 2020-06-23 20:48
Author: CtgAdm
Category: Nacional
Slug: la-cadena-perpetua-no-reduce-la-criminalidad-ni-repara-los-danos-a-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/cárcel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

A pesar de los diferentes debates generados en torno a la condena de candena perpetua para abusadores de menores en Colombia, **el pasado 18 de junio, el Congreso dio luz verde a esta medida celebrada por muchos.** Sin embargo hay varios aspectos que se analizan en algunos sectores de la sociedad y que cuestionan la eficacia de este tipo de medidas para combatir estos crímenes.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanDuque/status/1273793230929367040","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanDuque/status/1273793230929367040

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Pese a esta aprobación del primer mandatario, aún hace falta una revisión de control obligatorio por parte de la Corte Constitucional para que esta reforma sea un hecho; a este análisis se suman las voces de diferentes [analistas](https://archivo.contagioradio.com/entre-la-pandemia-las-fantasias-mortales-de-duque/) quienes con argumentos y estudios cuestionan la eficiencia de esta medida.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El costo de la cadena perpetua

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una comisión de expertos académicos y abogados en política criminal, presentaron un informe ante el Ministerio de Justicia señalando que esta medida violaba los tratados internacionales, e**ra desmedida, innecesaria, no garantizaba los derechos de las victimas y aumentaría el hacinamiento en las [cárceles](https://www.justiciaypazcolombia.com/pdte-duque-garantice-la-vida-de-las-y-los-prisioneros-en-colombia-medidas-efectivas-ya/).**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregaron que, el costo de manutención anual de una persona privada de la libertad asciende a más de 18 millones de pesos, lo que significaría un costo de mas de **900 millones de pesos en el caso de una persona recluida por 50 años**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dinero que según la comisión podría, *"costear el estudio anual de aproximadamente 9,54 niños de primaria (1’924.081 pesos), de 8,4 jóvenes de secundaria (2’164.591 pesos) y de 8 jóvenes de décimo"*.  (Le puede interesar:[Sin luz ni internet no puede haber educación virtual de calidad](https://archivo.contagioradio.com/a-colombia-llego-la-educacion-virtual-mientras-el-20-de-los-estudiantes-no-tenia-ni-luz/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La cadena perpetua es una medida para no analizar lo que pasa realmente en la sociedad"

<!-- /wp:heading -->

<!-- wp:paragraph -->

El ONG, **Penal Reform International**, aseguró que la cadena perpetua formal existe en** 183 países**, de los cuales 65 se imponen de por vida sin posibilidades de libertad condicional, revelando que **en el periodo de 2000 a 2014 la cifra pasó de 261.000 a 479.000** sentencias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Colombia la pena más alta contemplada en la Constitución era de 60 años, **una normativa que generalmente se ligaba a procesos que no eran investigados, ni juzgados, y en el caso de las violencias sexuales, ni siquiera denunciados**, por ello para algunos expertos este reforma es solo el reflejo de la necesidad de venganza ante la falta de atención a estos procesos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Geroid Loingsigh, periodista irlandés, investigador** y testigo del funcionamiento de estas condenas eternas en los centros de reclusión en paises como Estados Unidos, Italia e incluso Irlanda señaló, que esta *"****es una medida que se adopta para dejar de lado lo que pasa realmente en la sociedad"*.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el periodista, cifras oficiales de esos países indican un aumento de la cadena perpetua, pero no una reducción necesariamente en la criminalidad, y señaló que esta es solo la punta del iceberg, *"[**se inicia con adjudicar esta pena a los casos que mas se repudian, como las agresiones a los niños, pero con esto se abre la posibilidad de condenar a morir en la**]{}**cárcel a cientos de personas".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y señaló que actualmente el número de personas en **Estados Unidos con cadena perpetua es de 206.000, numero equivalente a la totalidad de la población carcelaria en 1970,** *"este tipo de medidas favorecen a los mismos que la votaron, y promovieron, y no las verdaderas víctimas".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Muchas cárceles están privatizadas, **entonces es un negocio mantener a más y más personas en las cárceles**; por ejemplo en Irlanda un guardián gana más que un profesor universitario, aclarandando que este es un país donde los profesores universitarios ganan bastante bien".*
>
> <cite>**Geroid Loingsigh**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

**Loingsigh**, señaló que el tratamiento de presos y víctimas debe estar acompañado de apoyo psicológico y social, *"los victimarios deben pasar por un tratamiento mental, **que permitan una transformación y no al contrario un recrudecimiento de la violencia, lo que suele ocurrir al interior de una cárcel,** en donde ya no importa cuanta violencia genere, ya que no hay esperanza de libertad".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por el lado de las víctimas presentó el caso de [Yuliana Sambon](https://archivo.contagioradio.com/nos-siguen-matando-aunque-ya-no-sea-noticia/)i, cuya familia sigue a la espera de una condena justa, lejos de un acompañamiento estatal, que les permita sobrellevar lo vivido, ***"viven en la pobreza absoluta, con miedo, enojo y sin ninguno tipo de indemnización material o emocional".***

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Una medida que no garantiza la protección a los niños y niñas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según **Luz Granada**, Directora de Incidencia Politica y Comunicaciones de la **Save the Children** Colombia, según el indice de niñez de los últimos dos años, **Colombia es el segundo país con más homicidios de menores de 18 años en el mundo**. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Agregando que Medicina Legal en su estudio forense del 2019 planteó que, ***"de 22.824 casos ocurridos en agresiones sexuales contra niños y niñas, la mayoría fueron niñas y el 70% se presentaron al interior del hogar"***.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Caso que según la Directora, **solo un 94% llegan a ser juzgados, y en el mejor de los casos suelen durar hasta 7 años en ser judicializados los agresores,** y enfatizó que para generar condenas reales y efectivas, es necesario el conocimiento de las leyes represadas durante años en la Constitución.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"En Colombia, nos estamos enfrentando a la falta de aplicación de leyes, no a su ausencia"***
>
> <cite>**Luz Granada**| Directora de Incidencia Política y Comunicaciones de la **Save the Children** Colombia</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y señaló que en país no hay inversión suficiente para trabajar los temas de salud mental, lo que en algunos casos hace que **la víctima se convierta en victimario , producto del odio, miedo, confusión y la misma venganza que les produce la agresión vivida**.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Vamos a tener lamentablemente como pueblo simplemente una sanción, que no resuelve el asunto de raíz , pero que al contrario va a poner en riesgo nuestra Constitución"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Finalmente señaló que, **la cadena perpetua es la solución fácil a un inversión formativa necesaria y que representa solo 4% de PIB**, *"**esa doble moral nos está matando, y pensar que con meter a una persona a la cárcel y olvidarnos de ella va a ser suficiente**; cuando lo único que hace es maquillar y agudizar un sin número de problemáticas que se pueden solucionar con solo estudiar y aplicar lo que ya tenemos".*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F773458143394748%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
