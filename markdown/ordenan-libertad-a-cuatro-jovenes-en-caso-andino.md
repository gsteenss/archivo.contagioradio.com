Title: Juez ordena libertad a cuatro jóvenes en caso Andino
Date: 2020-06-05 17:24
Author: AdminContagio
Category: Nacional
Slug: ordenan-libertad-a-cuatro-jovenes-en-caso-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/jovenes-detenidos-andino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Tras la audiencia por vencimiento de términos, el juez de control de garantías ordenó la libertad para otras cuatro personas víctimas del montaje judicial conocido como el caso "Andino". Las personas que recobrarían la libertad son Boris Rojas, Lina Jiménez, Andrés Bohorquez y Juan Camilo Pulido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En desarrollo...

<!-- /wp:paragraph -->
