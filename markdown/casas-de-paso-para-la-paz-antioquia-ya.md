Title: Antioquia necesita casas de paso para la paz. ¡Ya!
Date: 2016-02-12 08:40
Category: Fernando Q, Opinion
Tags: Antioquia, FARC, Medellin, Paramilitarismo, proceso de paz
Slug: casas-de-paso-para-la-paz-antioquia-ya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/casa-de-paz-medellin.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Joel Silva] 

#### [**[Luis Fernando Quijano](https://archivo.contagioradio.com/fernando-quijano/) - [@FdoQuijano](https://twitter.com/FdoQuijano)**] 

###### 12 Feb 2016

[Hablar de paz es muy fácil si no se entiende lo que hay que hacer para llegar a ella, y lo sacrificios que significa sostenerla en el tiempo.]

[Para muchos -no pocos- la paz es el desarme de las guerrillas, su sometimiento y su entrega para que purguen de por vida penas en la cárcel. Sin embargo, para otros -que tampoco son pocos-, entre los que me encuentro, es un acuerdo de paz entre dos ejércitos con dirección política que están buscando finalizar más de 60 años de guerra fratricida.]

[Por eso es tan lamentable que en este afán de buscar la reconciliación nacional se presenten dos hechos que, a mi parecer, son lunares negros –tal vez cancerígenos- en el ambiente que se pretende construir: abrir caminos de esperanza y reconciliación nacional.]

[Estos dos hechos –ocurridos en medio de un proceso de paz en La Habana, entre las Farc y el gobierno colombiano- son: la muerte de un integrante de las Farc que estaba enfermo y detenido en una cárcel de Pereira y las amenazas de muerte que recibió, por parte de la estructura paramafiosa Oficina del Valle de Aburrá, -instalada en el municipio de Envigado desde hace décadas y fachada del cartel de Medellín-, otro miembro de las Farc que fue indultado por el gobierno nacional, y valga la aclaración: un indultado no es un desmovilizado: es un ex prisionero de guerra.]

[El 20 de enero de este año, el presidente Juan Manuel Santos realizó un gesto humanitario unilateral que, a pesar de tener demoras, mostró cierta voluntad del gobierno nacional y obviamente del Estado colombiano. Enfermos y encerrados cumpliendo sus condenas, 28 integrantes de las Farc fueron indultados y obtuvieron su nuevamente su libertad, dejando atrás las condiciones infrahumanas en las que viven la mayoría de los presos de las cárceles de Colombia.]

[Lamentable, Jhon Jairo Moreno Hernández no fue uno de los indultados. Murió el 5 de febrero en un hospital de Pereira debido a una enfermedad hepática que lo agobiaba desde el año 2013. Rodrigo Granda, negociador de las Farc, dijo que hubo negligencia del Inpec a la hora de tratar esa dolencia. Razón que tiene Granda. El Inpec es una institución corrupta, represiva y negligente a la hora de atender a los reclusos cuando están enfermos, muchos han muerto y otros seguirán su camino mientras no se restructure y depure ese organismo estatal.]

[Jhon Jairo hacía parte de la lista de la lista de 71 prisioneros de guerra que organizaciones nacionales e internacionales solicitaron fueran liberados e indultados por las enfermedades que padecían: la falta de voluntad y el cálculo político mezquino no lo permitió. Wilson Antonio López, el indultado de la Farc, fue el amenazado y víctima inmediata del desplazamiento forzado. También se encuentra muy enfermo.]

[En medio de unas negociaciones que apuntan a la finalización del conflicto armado con las Farc esto no puede ocurrir. Estos dos hechos lamentables no pueden ser una constante en el antes, el durante y el después de la negociación política. Porque no se estaría resolviendo nada.]

[Teniendo en cuenta estos hechos, propongo al presidente Juan Manuel Santos que, en un gesto unilateral cargado de voluntad política y demostrando que en Colombia manda el Estado Social de Derecho, ordene de forma inmediata una operación de índole político, social y militar contra la Oficina del Valle de Aburrá y sus sucursales en Bello, Itagüí, Envigado, Sabaneta y Medellín. Que la obligue, irremediablemente, a dar el paso hacia unos diálogos urbanos que apunten a su sometimiento a la justicia colombiana. También le propongo que para afianzar la decisión de desmantelar el paramilitarismo y la mafia -dos cosas que encarnan La Oficina- autorice la creación de cinco casas de paso para la paz en estos municipios.]

[Las casas de paso para la paz tendrían objetivos concretos: la atención a prisioneros de guerra enfermos y la recepción inmediata de exprisioneros –indultados- que no tengan garantías de seguridad. Así mismo, y con posterioridad, la idea es que se conviertan en espacios propicios para construir desde el debate, socializando e instruyendo  más a fondo sobre los alcances del proceso de paz, de forma que logren aportarle a proyecto político desde la palabra y en sanas condiciones.]

[Sólo la voluntad política de las partes y la aceptación de todos los sectores de la sociedad lograrán que las fallas de la historia no se repitan; de otra forma, esto sólo será un genocidio de la Unión Patriótica, recargado.]
