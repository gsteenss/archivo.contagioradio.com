Title: Los ojos del mundo entero están puestos sobre la Paz de Colombia
Date: 2016-09-26 14:00
Category: Paz
Tags: Apoyo al proceso de paz, proceso de paz
Slug: los-ojos-del-mundo-entero-estan-sobre-la-paz-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/proceso-de-paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Tiempo] 

###### [26 de Sep 2016] 

Dentro de pocas horas se llevará a cabo la **ceremonia de la firma final de los acuerdos de paz entre el gobierno de Colombia y la guerrilla de las FARC-EP**, en Cartagena, y el mundo entero tendrá los ojos puestos sobre el país.

Entre la lista de invitados, que supera las 2.500 personas, se encuentra el ex presidente de Uruguay **Pepe Mujica,** quién siempre ha estado muy cercano al proceso de paz desde sus inicios y que afirmó, en el pasado encuentro “Sí a la Paz”, coordinado por la Unión Sindical Obrera, que **“la guerra debe ser en contra de la pobreza y la miseria”**, a su vez criticó las altas inversiones en guerra que hacen los países y la necesidad de **generar en Colombia posibilidades de empleo para que los guerrilleros no vuelvan a tomar las armas y los campesinos puedan sustituir los cultivos ilícitos.**

Otro de los invitados que ya arribó al país, es el **Secretario General de la ONU Ban Ki-moon**, que recordó su visita a Colombia en el año 2011, “la paz en ese entonces, aún era una posibilidad remota **y ahora es una victoria para el país y para la humanidad”**, reafirmando que la ONU estará acompañando y respaldando el proceso de implementación de los acuerdos en cada paso que dé, además el Secretario general de la ONU será testigo de honor y dará un discurso durante la ceremonia.

De igual forma, **Raúl Castro**, presidente de Cuba, país que ofició como sede de las negociaciones entre el gobierno de Colombia y las FARC-EP durante los cuatro años del proceso, llegó el pasado domingo a Cartagena; en intervenciones anteriores expresó que **"La firma de la paz en Colombia será la proclamación de nuestros pueblos a favor de la solución pacífica de las diferencias.** El logro de la paz en Colombia será también esperanza para millones de personas en el planeta".

El primer mandatario del Salvador, **Salvador Sánchez Seren**, declaró ante los medios en su llegada al aeropuerto de Cartagena, que celebra el logro del gobierno de haber alcanzado un acuerdo con la guerrilla de las FARC-EP e indicó que este hecho **“genera una esperanza para el mundo y abre una nueva historia para Latinoamérica”.**

Jhon Kerry Secretario de Estado, de los Estados Unidos, afirmó esta mañana que su **país podría reevaluar sacar a las FARC-EP de la lista de grupos terroristas**, [esto debido a la forma en la que se ha venido dando el proceso de paz en Colombia](https://archivo.contagioradio.com/51-congresistas-de-estados-unidos-respaldan-proceso-de-paz-con-el-eln/). Kerry asistirá a la ceremonia como delegado de su país.

El presidente de Ecuador Rafael Correa, aseguró, antes de abordar su vuelo a Colombia, que considera **disminuir en un 80% el pie de fuerza en la frontera con Colombia** de darse la paz y calificó a este acontecimiento como histórico para Latinoamerica, de igual forma, ratificó el apoyo incondicional de su país al proceso de paz y al camino que se deba construir a partir de este momento.

A su vez, mandatarios como Evo Morales, Nícolas Maduro, Michelle Bachelet, Enrique Peña Nieto, Jimmy Morales, Michel Temer, Horacio Cartes, Danilo Medina, Mauricio Macri, y Juan Carlos Varela llegarán en las próximas horas para asistir al evento.

El evento contará con la participación en total de **27 cancilleres**, **16 mandatarios**, **9 presidentes y directores de organismos multilaterales** y **3 expresidentes**, que como parte del evento recibirán el balígrafo, similar al original con el que se firmó el cese bilateral y definitivo de hostilidades. Se estima que la programación inicie al medio día con una eucaristía.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
