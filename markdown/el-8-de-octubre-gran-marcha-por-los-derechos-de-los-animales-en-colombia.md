Title: El 8 de octubre será la gran marcha por los derechos de los animales en Colombia
Date: 2017-09-25 16:19
Category: Animales, Nacional
Tags: marcha por los derechos de los animales, Plataforma Alto
Slug: el-8-de-octubre-gran-marcha-por-los-derechos-de-los-animales-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/marcha_imagen-e1506372819337.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: seloexplicoconplastilina.org] 

###### [25 Sep 2017] 

El próximo 8 de octubre se realizará la **Décima Marcha Mundial por los Derechos de los Animales,** con cual se busca continuar exigiendo que en la agenda pública se tengan en cuenta políticas en favor de la vida animal, y que en general las personas respeten a los demás seres vivos que habitan el planeta.

**"Ponte la 10 por los animales" es el eslogan que acompaña este año la marcha que se viene realizando desde hace 10 años.** De acuerdo con Natalia Parra, directora de la Plataforma ALTO uno de los colectivos que organiza la movilización, la idea es que este año la ciudadanía "se ponga la 10", para exigir, en primer lugar, que la consulta antitaurina en Bogotá se realice en marzo con las elecciones al Congreso de la República.

Otra de las motivaciones es impulsar a que los candidatos a la presidencia y al congreso para las elecciones de 2018, tengan en cuenta la defensa del ambiente y los animales en sus programas políticos de manera más preponderánte y además para que la Fiscalía cumpla los mandatos respecto a las sanciones frente al maltrato animal.

En esa medida, una de las actividades de la marcha de este año será una "huellatón", con la que los animales de compañía firmarán con su huella para apoyar un proyecto de Ley que se está tramitando en el congreso y con el que se busca reforzar la protección de animales de compañía.

Asimismo se planea que los participantes escriban cartas a los políticos solicitando algún tema puntual respecto a la protección animal y ambiental. De hecho, este año la movilización también se refiere a la defensa del planeta, por lo que cuenta con el apoyo de diversas organizaciones ambientalistas de todo el país, que entre otras cosas, **reivindicarán la moratoria contra el fracking, y en general las luchas contra proyectos minero-energéticos que afectan el ambiente.**

### **El recorrido** 

La movilización partirá en la Plaza La Santamaría, y seguirá por la carrera quinta hasta llegar a la Plaza de Bolívar. La recomendación que se da desde la Plataforma ALTO es que las personas pueden llevar perros siempre y cuando estén en las condiciones de realizar el recorrido, de igual forma se recomienda bozal, collar, comida y agua para los animales de compañía. Además están bienvenidos los disfraces, actividades culturales y carteles.

La marcha también se llevará a cabo en diferentes municipios de departamentos como **Norte de Santander, Caldas, Tolima, Cauca, Nariño, Santander, Cesar, Cundinamarca, Tolima,** entre otros.

Cabe resaltar, que en línea de las movilizaciones en contra del maltrato animal, ocho días antes de la Décima Marcha por los Derechos de los animales, el Colectivo Colombia sin Toreo, también llevará a cabo otra movilización exigiendo el fin de las corridas de toros. Esta será una marcha que partirá a las 9 de la mañana desde el Parque Nacional hasta la Plaza Santamaría.

<iframe id="audio_21083954" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21083954_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
