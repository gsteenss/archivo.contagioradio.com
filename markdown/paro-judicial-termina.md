Title: Un sindicato de trabajadores de la rama judicial continúa en paro
Date: 2016-02-16 20:40
Category: Judicial, Nacional
Tags: Asonal Judicial, Ministerio de Justicia, paro rama judicial
Slug: paro-judicial-termina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Asonal-Judicial-e1496772441737.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El País 

<iframe src="http://www.ivoox.com/player_ek_10469491_2_1.html?data=kpWhmJ6YfZKhhpywj5WYaZS1kZaah5yncZOhhpywj5WRaZi3jpWah5yncbbijNjW0MnNp8Lo0JDRx5DYtsLWws%2FOxtTWqdSfxcqYzsaPtsLhwpDX18nNp8rVzZDQ0dPYrc%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Luis Fernando Otálvaro, Asonal Judicial] 

###### [16 Feb 2016] 

Tras 10 días de paro de la rama judicial, Asonal Judicial logró un preacuerdo el pasado 22 de enero, pese a que un sindicato aún continúa en paro al no estar de acuerdo con lo acordado con el Consejo Superior de la Judicatura.

De acuerdo con Luis Fernando Otálvaro, integrante de Asonal Judicial, el conflicto laboral se superó tras revisarse el texto del pre-acuerdo a que se llegó entre las partes y su aprobación por parte del Consejo Superior de la Judicatura, quien añade que lo que solicitan es **“que no se desmantelen los despachos judiciales, es decir que reste personal a los juzgado, pues que no dan abasto”.**

Cabe recordar que el paro se originó en una decisión de la Sala Administrativa del Consejo Superior de la Judicatura sobre la estructura y funcionamiento de los centros de servicios que apoyarán a los juzgados civiles y de familia.

Los trabajadores de la rama judicial esperan que la mesa de concertación se establezca el próximo **29 de febrero**, aunque según Otálvaro, es posible que se aplace.

El sindicato que continúa en cese de actividades se denomina **“El vocero judicial”**, y afirman estar de acuerdo con exigir la revocatoria del Acuerdo de la Judicatura que creó los nuevos Centros de Servicios Judiciales, pero **no con el " aplazamiento" al que llegaron las partes.**
