Title: Con el foco sobre la violencia institucional inicial el Festival de cine y DDHH de Uruguay
Date: 2018-06-07 12:00
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Cine, DDHH, Festival, Uruguay
Slug: cine-ddhh-uruguay
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/Banner-horizontal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa Festival 

###### 07 Jun 2018 

Desde hoy y hasta el próximo 14 de junio, se vive la 7ma edición del Festival Internacional de Cine y Derechos Humanos de Uruguay, que para este año presenta como temática central la violencia institucional, enfocada en las estructuras que sostienen sistemáticamente las vulneraciones dirigidas a sectores concretos de las sociedades.

Desde el Festival se promueve la reflexión y el debate en torno a los derechos humanos a través de  una [programación](https://www.scribd.com/document/381197705/Program-a-Tenemos-Que-ver) totalmente gratuita, compuesta por una selección de largometrajes y cortometrajes, de ficción, documentales y animaciones, que retratan diferentes temáticas relacionadas con los derechos humanos.

Son en total 25 largometrajes y 23 cortometrajes provenientes de 22 países: Alemania, Argentina, Bélgica, Brasil, Canadá, Chile, Dinamarca, EEUU, Eslovenia, España, Francia, Irán, Italia, Líbano, México, Qatar, Rusia, Senegal, Serbia, Sudáfrica, Suecia, Uruguay.

Las producciones están distribuidas en  6 secciones: La competencia de largometrajes de ficción y la competencia de largometrajes documentales,  realizados entre 2016 y 2018 que serán estrenados por primera vez en Uruguay, la competencia de cortometrajes, donde se exhibirán cortometrajes que apelan a la reflexión sobre los Derechos Humanos y que compiten por el Premio Jurado Joven a Mejor Cortometraje.

También la muestra de largometrajes y cortometrajes que abordan cuestiones acerca de los Derechos humanos desde diferentes perspectivas, muestra de cine para niñas, niños y adolescentes, y algunas proyecciones especiales que incluyen dos largometrajes uruguayos recientemente estrenados, que aportan y mucho a reflexionar acerca del empoderamiento de personas que se encuentran en situaciones y contextos de vulnerabilidad.

El evento que tiene lugar cada año desde 2011, nació como un espacio de diálogo y construcción de ciudadanía, que busca potenciar el cine como herramienta de comunicación y sensibilización sobre temas socialmente relevantes, generando un espacio participativo de reflexión y debate.

El Festival Internacional de Cine y Derechos Humanos de Uruguay, es organizado por Tenemos Que Ver y Cotidiano Mujer.
