Title: Aprobada consulta popular en El Castillo Meta
Date: 2017-10-25 08:33
Category: Ambiente, Nacional
Tags: Castillo Meta, consulta popular, petroleo
Slug: aprobada-consulta-popular-en-el-castillo-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-24-at-2.53.40-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Asojuntas ] 

###### [24 Oct 2017] 

Fue aprobada la consulta popular en el municipio del Castillo, Meta, luego de que el Tribunal Superior aceptara la pregunta, ahora solo falta que la alcaldía fije la fecha. El umbral para esta votación es de **1.247 votos y de acuerdo con los habitantes, desde la alcaldía municipal se están buscando alternativas para blindar al mecanismo**.

De acuerdo con Yaneth Vega, integrante del Colectivo Ariari, la aprobación de la consulta popular es el producto de un trabajo de 4 años, “nosotros somos una región netamente de agricultores, eso no lo ha visto el gobierno, **por eso no queremos el tema de petróleos acá**”. (Le puede interesar: ["Gobierno intenta frenar el trámite de consultas populares con argumento de falta de presupuesto"](https://archivo.contagioradio.com/gobierno-intenta-frenar-el-tramite-de-las-consultas-populares-con-el-argumento-del-presupuesto/))

Además, Vega manifestó que esta consulta popular se junta a las iniciativas de otros municipios como la de Granada, La Macarena, Acacias, entre otros, que buscan que la región del Ariari, no haya exploración ni explotación minera, por tal razón Vega rechazó la suspensión de la consulta popular en Granada y manifestó que esto “el gobierno como tal no debe colocar tantos obstáculos, **necesitamos colaboración gubernamental, tenemos derecho a decidir**”.

Esta consulta popular se realiza en el marco de las acciones que adelantan las comunidades de la Región del Ariari con el objetivo de defender sus territorios del proyecto de PERFOTEC y HOCOL, que estiman la extracción de bituminosas que podrían acarrear graves afectaciones en los lechos de 4 ríos en Arauca, **además está el bloque petrolero CPO 16 que también pertenece a HOCOL**.

En esa medida, los comités promotores de la consulta popular señalaron que se están reuniendo con el alcalde del municipio para que el presupuesto no sea un impedimento a la implementación de este mecanismo **“la gran mayoría de la comunidad del Castillo está de acuerdo en que no quiere petróleo”** afirmó Vega.

Al igual que los otros municipios que han convocado el mecanismo de la consulta popular, Vega señaló que la opción a la extracción y la explotación de petróleo es potenciar la productividad agrícola de estos territorios que hacen parte de la despensa del país. (Le puede interesar:["La Consulta sigue viva: Habitantes de Granda, Meta"](https://archivo.contagioradio.com/consulta_popular_granada_meta_sigue_viva/))

<iframe id="audio_21662547" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21662547_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
