Title: Campamento por la Paz en Montería se levanta por amenazas
Date: 2016-11-08 15:54
Category: Nacional, Paz
Tags: campamento, Campamento por la Paz, marcha patriotica, Montería, paz
Slug: campamento-por-la-paz-en-monteria-se-levanta-por-amenazas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/campamento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón] 

###### [08 Nov. de 2016] 

**Como forma de respaldo y exigencia a la firma del acuerdo de paz de La Habana, diversas iniciativas de carácter social se han ido desarrollando en todo el país.** Una de ellas han sido los Campamentos por la Paz, que han permitido que diversidad de posturas se unan con un solo objetivo, la paz.

Sin embargo, en días pasados se conoció que **Elena María Mercado Rodríguez**, Miembro Directivo de la Fundación Social Cordoberxia, integrante del movimiento político y social Marcha Patriótica Córdoba y vocera del Campamento por la Paz que se instaló en la ciudad de Montería** recibió amenazas**. **Lo que obligó a que este se levantara por temas de seguridad.**

La amenaza fue recibida por Elena a su teléfono. Dicha comunicación que no sobrepaso 40 segundos, provenía de un número privado, según relata el comunicado entregado por el Campamento por la Paz y se dirigieron a ella con palabras soeces y fuertes.

**Como medida de seguridad para Elena y para los acampantes,** dentro de los cuales se encuentran más activistas sociales de organizaciones filiales a Marcha Patriótica, **los voceros del campamento por la paz de Montería decidieron en días pasados levantar el escenario de movilización** permanente que pretendía continuar hasta que el presidente de la República anunciara la implementación de los acuerdos de paz.

Elena, se había convertido en una imagen visible pues había sido la persona delegada para mantener interlocución con las instituciones gubernamentales, con las organizaciones sociales y con los medios de comunicación.

Así mismo, los integrantes del Campamento por la Paz en Montería aseguraron que faltó interés y atención de instituciones como la Defensoría del Pueblo y la Personería Municipal.

Pese a que las personas del Campamento por la Paz se vieron en la obligación de levantar el campamento, para salvaguardar sus vidas, aseguraron en su comunicado que continuarán en la “tarea constante y decisiva de defender y aportar a la construcción de la paz estable y duradera en nuestro país”. Le puede interesar: [Se cumple un mes de la instalación del Campamento por la Paz](https://archivo.contagioradio.com/se-cumple-un-mes-de-la-instalacion-del-campamento-por-la-paz/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]

<div class="ssba ssba-wrap">

</div>
