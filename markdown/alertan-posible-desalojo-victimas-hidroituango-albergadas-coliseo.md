Title: Alertan posible desalojo de víctimas de HidroItuango albergadas en coliseo
Date: 2018-09-26 08:20
Author: AdminContagio
Category: DDHH, Nacional
Tags: EPM, Ituango, Rios Vivos
Slug: alertan-posible-desalojo-victimas-hidroituango-albergadas-coliseo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/damnificados-hidroituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Rios Vivos] 

###### [24 Sep 2018]

Durante una audiencia citada por el gobierno del Municipio de Ituango se habría hecho un anuncio en torno al posible desalojo de un grupo de víctimas de HidroItuango que se encuentran alojadas en el coliseo de ese municipio y que todavía no tienen un lugar digno para vivir. Paradójicamente **el mismo secretario de gobierno fue el que indicó que ese era el sitio de alojamiento para el grupo de 22 familias.**

Según Isabel Cristina Zuleta, la única manera de evitar el desalojo es que las comunidades prueben que el Coliseo les pertenece y ello es imposible porque esa estructura es del municipio, así que lo más seguro “es que mañana mismo se dé la orden de desalojo”.

El gran riesgo es que estas personas no tienen cómo sobrevivir en otro sitio dado que la zona en la que habitaban no ha sido reparada y no se les permite a ellos y ellas seguir trabajando en el río que era el único medio de subsistencia de las familias. (Lea también: [Así sobrevivien las vícitmas de HodroItuango luego de la inundación](https://archivo.contagioradio.com/alerta-naranja-no-reduce-el-riesgo-de-una-avalancha-en-hidroituango-rios-vivos/))

### **Familias en coliseo de Ituango no tienen cómo sobrevivir** 

Este grupo de 22 familias, fueron alojadas en el Coliseo de Ituango, **luego de la inundación provocada por las fallas estructurales de la represa HidroItuango el pasado mes de abril**, en que resultaron afectados por lo menos 10 municiopios de la región y cerca de diez mil personas habitantes de la rivera del Río Cauca en Antoquia.

A esta hora las familias alojadas en el coliseo esperan que durante la audiencia citada por la secretaría de gobierno para este 25 de Septiemnbre se logre demostrar que no tienen un lugar a dónde ir en caso de ser desalojados, y que tampoco cuentan con mecanismos de subsistencia dada la falta de atención eficaz por parte de las autoridades y la falta de medidas de reparación sobre los daños causados por las fallas en el mega proyecto.

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio](http://bit.ly/1ICYhVU)]
