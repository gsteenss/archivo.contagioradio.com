Title: Cabildo Indígena Muisca asegura que Van Der Hammen es territorio sagrado
Date: 2018-09-20 08:22
Author: AdminContagio
Category: Ambiente, Entrevistas
Slug: van-der-hammen-terreno-indigena
Status: published

###### [Foto por: @Ricardoladino] 

###### [19 Sept 2018] 

[El Gobernador del Cabildo Indígena de Suba, Iván Mendoza señaló que esta comunidad si utiliza la reserva Thomas Van Der Hammen para ceremonias tradicionales de su pueblo, contrariando **la certificación emitida por Ministerio del Interior que señala que no existen grupos étnicos o indígenas en el terreno**.]

Mendoza aclaró que no están residiendo al interior del territorio, puesto que es un lugar sagrado para ellos, pero que aún así, la reserva sigue siendo parte fundamental de sus tradiciones, “**en lo sagrado nadie vive, hasta el cura vive al lado de la iglesia,** pero no dentro, por eso no se construyó en la Van Der Hammen, porque nuestros abuelos respetaron los caminos del agua”. (Le puede interesar: [Peñalosa no puede intervenir la Van Der Hammen](https://archivo.contagioradio.com/penalosa-intervencion-van-der-hammen/))

[De igual forma, aseguró que ya tenían previamente conocimiento de cuál sería la respuesta del Ministerio de Interior, puesto que, según él, es claro que los indígenas han sido un problema para el negocio de las tierras en Colombia, y **que el Ministerio no reconoce la presencia de sus comunidades en lugares de interés para la expansión urbana.**]

### **Los Muiscas están en una zona gris** 

El Gobernador Muísca manifestó que lastimosamente hay una relación compleja de la ciudadanía con el territorio, que ha generado una falta de apropiación ancestral y que se evidencia en el hecho de que actualmente las comunidades indígenas viven de forma dispersa,  **"nosotros nunca fuimos a la ciudad, la ciudad vino a nosotros".**

[Finalmente, afirmó que el cabildo iniciará un proceso de demanda para reclamar sus derechos como comunidad y evitar la intervención de la reserva por parte de la Alcaldía, “únicamente estábamos esperando que saliera la comunicación oficial para iniciar el proceso, no sabemos qué pueda pasar si s**e interviene el territorio o qué tipo de afectaciones ambientales pueda llegar a tener a largo plazo** la destrucción de una zona, que aunque es pequeña, es muy importante” afirmó. ]

<iframe id="audio_28733071" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28733071_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo] 
