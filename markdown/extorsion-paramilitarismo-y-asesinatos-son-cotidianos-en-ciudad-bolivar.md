Title: Extorsión, paramilitarismo y asesinatos son cotidianos en Ciudad Bolívar
Date: 2015-09-22 18:55
Category: DDHH, Nacional
Tags: Aguilas Negras, Bloque Capital D.C, Derechos Humanos, Limpieza social, Milicias Urbanas, paramilitares, policia, Radio derechos Humanos
Slug: extorsion-paramilitarismo-y-asesinatos-son-cotidianos-en-ciudad-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/ojo-al-sancocho.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ojoalsancocho.com 

<iframe src="http://www.ivoox.com/player_ek_8553211_2_1.html?data=mZqilZeVdY6ZmKiak5uJd6KlkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidno0Nfgy4qnd4a2lNOSlKiPtMLmwtLWzs7YpdPd1NLcjd6PpdTZ1M7bw9nTt4zn0NOYxdTYrcXdwtPc1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Víctor Hugo Olaya, defensor DDHH] 

###### [22 sep 2015] 

Según el Centro de Estudio y Análisis en Convivencia y Seguridad Ciudadana (Ceacsc), en el primer semestre del 2015 se presentaron 125 homicidios en Ciudad Bolívar. De esos homicidios, 14 en el barrio Arborizadora Alta; 15 en Lucero, 14 en Paraíso, 17 en San Francisco, 11 en Vista Hermosa, 11 en Santo Domingo, 11, en Perdomo y 10 en Candelaria.

Otros de los barrios afectados en Ciudad Bolívar por la muerte violenta de jóvenes entre los 14 y los 26 años son Arabia, La Joya, El Recuerdo, Potosí y Cazucá. Aunque muchos de estos hechos son registrados en el Instituto Colombiano de Medicina Legal bajo diversas causas, **organizaciones de DDHH afirma que el control de los grupos paramilitares y el control del microtráfico son las principales causas.**

Adicionalmente la extorsión o el pago de "vacunas" es común en muchos de los establecimientos comerciales de los barrios altos de la localidad. Según el nivel del establecimiento se pagan extorsiones desde 20.000 pesos mensuales. Quienes las cobran camuflan la actividad como "aportes voluntarios para la seguridad" señalan defensores de DDHH.

Víctor Hugo Olaya, defensor de derechos humanos de la localidad, agrega que la **complicidad de los agentes de seguridad es evidente**. Según el defensor de DDHH los paramilitares "pueden hacer lo que ellos (policía) no hacen" como entrar a las casas y sacar de allí a los jóvenes para luego asesinarlos.

Olaya explica también que se han identificado casos en que los paramilitares controlan el micro tráfico en la localidad a través de jóvenes controlados por ellos. Cuando los jóvenes están "*cogiendo poder*" los asesinan para mantener la conducción total del negocio. Además las **empresas mineras de la localidad están reclutando jóvenes y entregando armas para se vele por sus intereses.**

Sin embargo, a pesar de la inoperancia de las autoridades ante estas situaciones, **hay varias organizaciones barriales y comunitaria que se han dado a la tarea de generar un clima de vida y de esperanza** como el festival de cine "Ojo al Sancocho" o la inciativa ambiental y territorial "No le saquen la piedra a la montaña".
