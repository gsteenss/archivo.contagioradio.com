Title: "Cuerpo - territorio", eje central del encuentro de mujeres frente al extractivismo
Date: 2015-08-24 12:25
Category: Mujer, Nacional
Tags: Bolivia, capitalismo, Censat agua Viva, cuerpo de la mujeres, ecuador, extractivismo, Guatemala, honduras, modelo de desarrollo, Movilización Social en Guatemala, Mujeres y territorio, racismo
Slug: cuerpo-territorio-eje-central-del-encuentro-de-mujeres-frente-al-extractivismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/ni-la-tierra-ni-las-mujeres-somos-territorio-de-conquista.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [fincacieloazul.wordpress.com]

##### <iframe src="http://www.ivoox.com/player_ek_7514196_2_1.html?data=mJqelpadeo6ZmKiakpqJd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOrpNrS1NXTb46f1crf1M7Ys9Pd0IqfmZDJrsafxMrb1tfFsIzYxtGYx9PHucbi1dfcjcnJb87py8qah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [**Bertha Cáceres, Co-Fundadora y vocera del COPINH**] 

###### [24 Ago 2015]

El pasado 21 de agosto en Bogotá concluyó el **Encuentro Nacional de Mujeres frente al extractivismo,** con el que se logró fortalecer las luchas de las mujeres que lideran propuestas de acción frente al modelo extractivista en América Latina.

En el encuentro se contó con la **participación de más de 50 mujeres mujeres diversas** entre las que se encuentran indígenas, campesinas, afrodescendientes y pobladoras urbanas, además, hubo invitadas internacionales de países como, **Guatemala, Bolivia, Honduras y Ecuador,** donde el cuerpo de las mujeres también termina siendo víctima de la política extractivista.

Durante este evento se tuvo al posibilidad de  intercambiar acciones y experiencias de defensa territorial adelantadas por mujeres en distintas regiones del país, así mismo se pudo escuchar y construir alternativas de solución frente a los conflictos sociales y ambientales, y finalmente se logró concertar un marco común para la acción política en defensa de los territorios y la vida de las mujeres afectadas por el modelo de desarrollo.

De acuerdo con **Bertha Cáceres, Co-Fundadora y vocera del COPINH**- Consejo Cívico de Organizaciones Populares e Indígenas de Honduras,  una de las conclusiones más importantes durante el encuentro, fue el énfasis que se hizo sobre las formas de resistencia y las similitudes que hay en las luchas y contextos de los procesos de resistencia en Colombia con otros países latinoamericanos.

Mediante el encuentro, se reafirmó la lucha en contra del patriarcado, modelo capitalista, y el racismo ya que estos impactan  la vida emocional, sexual y comunitaria de las mujeres. Se tuvo como propósito **llamar la atención sobre la vulneración a los derechos humanos y la ausencia de mecanismos efectivos de participación para las mujeres.**
