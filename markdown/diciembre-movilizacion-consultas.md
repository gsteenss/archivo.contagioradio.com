Title: Diciembre 7: Gran movilización nacional en favor de las Consultas Populares
Date: 2018-10-25 22:11
Author: AdminContagio
Category: Ambiente, Movilización
Tags: Ambiente, consulta popular, Consulta Previa, locomotora minera, Movimientos sociales
Slug: diciembre-movilizacion-consultas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/consulta-ibague.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las Dos Orillas] 

###### [25 Oct 2018] 

En días recientes, movimientos ambientalistas, organizaciones sociales y comités promotores de consultas populares en todo el país se reunieron para definir una agenda común de **movilización a realizarse en todo el país el próximo 7 de diciembre**, buscando respaldar los mecanismos de participación popular frente a las limitaciones que la Corte Constitucional viene imponiendo a estas instancias de decisión.

### **Toda Latinoamérica se enfrenta a retos similares** 

<iframe src="https://co.ivoox.com/es/player_ek_29636846_2_1.html?data=k56jlZuceJehhpywj5WcaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncaLiwpC5x97apYampJDOxNTLpcXVjNXS1NrFssKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En el marco del encuentro de la Red Latinoamericana sobre las Industrias Extractivas (**RLIE**), la abogada peruana experta en extractivismo y participación ciudadana, **Ana Leyva**, afirmó que existen 4 grandes retos que deben enfrentar las sociedades que trabajan en favor de temas ambientales: La diversificación económica, la fragmentación de las luchas ambientales, la gobernanza del agua y la participación ciudadana.

Leyva sostuvo que **uno de los grandes retos en latinoamérica es la diversificación productiva** puesto que la dependencia del extractivismo, lo que en Colombia se denominó como locomotora minera, deja las economías a expensas de los precios del mercado; además, porque encontrar nuevas fuentes de ingresos nacionales permitiría lograr un desarrollo ambientalmente sostenible a largo plazo.

La abogada aseguró que para combatir la ausencia de agendas comunes que tienen los movimientos ambientales y sociales en general, es el desarrollo de una idea común sobre lo que se quiere para el país, de tal forma que las organizaciones sociales puedan establecer alianzas y propender por un objetivo común. (Le puede interesar: ["¿Por qué las consultas populares están en riesgo?"](https://archivo.contagioradio.com/consultas-populares-riesgo/))

El principal reto que señaló la experta es la **defensa de la participación ciudadana para determinar lo que pasa efectivamente en los territorios**, sosteniendo que actualmente hay un temor muy fuerte frente a esta por parte de los gobiernos y "no lo ven como una posibilidad de encuentro y de mayor gobernanza". Por eso suele pasar que los mecanismos de participación y consulta se reduzcan a mecanismos formales, y detalló que en Perú existe la Consulta Previa, pero se ha venido debilitando recientemente.

Adicionalmente, es necesario **revisar la gobernanza que tienen las comunidades sobre el agua, y ecosistemas frágiles como páramos y bosques**, de los que dependen en gran medida el ciclo del líquido vital. (Le puede interesar: ["Gobierno quiere meterle mico a consultas populares en el Presupuesto General de la Nación"](https://archivo.contagioradio.com/gobierno-quiere-hacerle-mico-a-las-consultas-populares-con-el-presupuesto-general-de-la-nacion/))

### **Movimientos en Colombia se unen en agenda común de movilizaciones** 

Respondiendo a la necesidad de lograr una articulación en el movimiento ambiental del país, recientemente organizaciones ambientales, sociales y comités promotores de distintas consultas populares en el país se reunieron en Bogotá para analizar una agenda común de movilización nacional en defensa de los mecanismos de participación ciudadana.

<iframe src="https://co.ivoox.com/es/player_ek_29636864_2_1.html?data=k56jlZucepWhhpywj5WYaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5yncbPj1MaYpMbQsMbn1crf0diJdqSfxNTay9mJh5SZop6YxdTSt9bg1caY0tTUuc3V05DS0JCqudTVyMbg18yRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Rosa Ballesteros, integrante del comité que promovió la consulta popular en Fusagasuga,** afirmó que "hay  una rotunda violencia contra la consultas populares y los mecanismos de participación ciudadana", por eso es necesario impulsar una movilización nacional que articule los diferentes territorios, para defender de la autonomía territorial.

Adicionalmente, Ballesteros señaló que para este propósito, es necesario lograr una **articulación entre procesos territoriales contra la minería, los hidrocarburos e hidroeléctricas**, de forma tal que se sumen a una agenda de carácter nacional. (Le puede interesar:["14 municipios de Boyacá ganaron la pelea contra el fracking"](https://archivo.contagioradio.com/boyaca-contra-fracking-sismica/))

En la reunión las y los líderes enviaron un mensaje de alerta sobre sobre el impulso de una serie de reformas, proyectos de ley y actos legislativos que pretenden imponer medidas regresivas para frenar mecanismos de participación, desvirtuar procesos de reparación y restitución de tierras.

Por estas razones, **acordaron una gran movilización para el próximo 7 de diciembre,** así como el desarrollo de una serie de audiencias públicas, encuentros nacionales de envergadura nacional, y acciones de carácter jurídico. (Le puede interesar: ["Radicado Proyecto de Ley que prohibiría el fracking en Colombia"](https://archivo.contagioradio.com/listo-proyecto-de-ley-que-prohibiria-el-fracking-en-colombia/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
