Title: "Con la Comisión de la Verdad, más de uno del gobierno se va a ir para la cárcel"
Date: 2015-05-29 16:36
Category: Entrevistas, Paz
Tags: Alejandro Ordoñez, alianza, colombianos y colombianas, comision de la verdad, frente amplio, habana, paz, piedad cordoba, Procurador
Slug: con-la-comision-de-la-verdad-se-va-a-ir-mas-de-uno-del-gobierno-para-la-carcel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/16113545443_0de3d1eb9e_k.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ministerio de Cultura Argentina 

<iframe src="http://www.ivoox.com/player_ek_4571078_2_1.html?data=lZqkk5WbfI6ZmKiakpmJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmpNTbjdHFb6Tjzs7gy4qnd4a2lNOYxsqPsMKft8rfxsbIb9TZjNvOjcaPrdOfzoqwlYqlddSfxcqY19PTb8Whhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Piedad Córdoba, Defensora de Derechos Humanos] 

###### [29 may 2015]

La defensora de Derechos Humanos, Piedad Córdoba, señaló que el proceso de paz se encuentra en un momento muy delicado, pero que "independientemente de que haya contradicciones, de que sea muy difícil, de que quisiéramos que las cosas fueran distintas, uno tiene que reconocer que no puede haber marcha atrás; y hay que buscar que en los puntos más difíciles que tienen que ver con justicia y dejación de armas, se avance rápidamente en ellos, para que antes de final de año se firme un acuerdo de paz".

Y es precisamente en este tema de justicia transicional en el que la Mesa de Diálogos de La Habana se ha mantenido durante los últimos ciclos de negociación sin aparente avance. Mientras el gobierno insiste en que los máximos jefes de la guerrilla vayan a la cárcel; la Delegación de Paz de las FARC se mantiene en la necesidad imperativa de establecer una Comisión de la Verdad que evalúe las responsabilidades en el conflicto colombiano.

Según señala la defensora de Derechos Humanos, "son 50 años de guerra; la guerrilla de las FARC es la guerrilla mas antigua del mundo, y la gente no puede esperar que estos señores vayan a ir a la mesa de La Habana a decir '¿qué nos van a dar para irnos rápidamente?'. Allá hay un debate sobre el país que se quiere tener".

Indica, además, que si el gobierno no cede en la propuesta hecha por la guerrilla de establecer una Comisión de la Verdad, es porque saben que "con la Comisión de la Verdad, más de uno se va a ir para la cárcel". Para la ex-senadora, el paramilitarismo no fue ni es una estrategia que surge espontáneamente, pues como indican los más de 14 mil procesos contra empresarios en la Fiscalía, importantes dirigentes económicos y políticos en Colombia han tenido nexos con estos grupos contra-insurgentes.

Señaló además, que en Colombia la guerra militar y paramilitar no se ha desatado únicamente contra los ejércitos irregulares, como las FARC y el ELN, sino contra líderes y [[lideresas](https://www.youtube.com/watch?v=YI6P81rsSHY)] sociales. Como le señaló el Defensor del Pueblo en días pasados, alrededor de 180 municipios de Colombia han sido tomados por el paramilitarismo. "Si no hay una decisión del gobierno de terminar el paramilitarismo, aquí no va a haber paz", enfatizó Córdoba.

Finalmente, frente a la insistencia de analistas y sectores sociales sobre la necesidad de establecer un cese bilateral de hostilidades, la ex-senadora afirmó que se está en mora de declarar el cese al fuego bilateral. Para Córdoba, la crisis que está viviendo el proceso sería consecuencia de la decisión de negociar en medio del conflicto. "Quienes tienen la desvergüenza de impedir el cese bilateral, como el Procurador, viven en el norte, se han paseado por las autopistas de la burocracia durante toda la vida, no han tenido otra posibilidad distinta a degustar las mieles del poder: No tienen nada que ver con la pobreza, no tienen nada que ver con las muertes de las gentes en las regiones mas apartadas de este país, pero ademas no tienen a condición ética de saber que por encima de cualquier circunstancia, lo mas importante es la vida de un ser humano".
