Title: Informe probaría responsabilidad de General (r) Torres Escalante en "falsos positivos"
Date: 2018-08-16 18:59
Category: DDHH, Nacional
Tags: Ejecuciones Extrajudiciales, falsos positivos, Henry Torres Escalante, JEP
Slug: el-antagonista-del-informe-ni-delincuentes-ni-combatientes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/FOTO-3-12.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Nación] 

###### [16 Ago 2018] 

En el informe **"Ni delincuentes ni combatientes"**, presentado a la Jurisdicción Especial para la Paz (JEP) por organizaciones sociales, aparece el **General (r) Henry Torres Escalante** como uno de los responsables de los hechos cometidos por la Brigada 16 entre 2005 y 2008. En el documento se detalla la responsabilidad de Torres Escalante en al menos dos casos de ejecuciones extra judiciales. (Le puede interesar: ["12 casos de ejecuciones extrajudiciales en Casanare serán revisados por la JEP"](https://archivo.contagioradio.com/jep-revisara-12-casos-de-ejecuciones-extrajudiciales-en-casanare/))

Fernando Rodríguez, abogado de la Fundación Comité de Solidaridad con los Presos Políticos (FCSPP) y quien lleva el caso contra el General, afirmó que Torres Escalante tiene una hoja de vida importante y buena parte de su trayectoria la hizo en el Casanare desde 2004. Incluso, entre 2005 y 2007 fue comandante de la **Brigada Nº16**, que opera en ese departamento.

El abogado afirmó, que según cifras del GAULA militar, durante el periodo de un año en el que Torres Escalante estuvo en la Brigada se **presentaron más de 90 víctimas,** mientras que, en el periodo del siguiente comandante no se registró ninguna víctima y hubo un alza en el número de personas detenidas y desmovilizadas.

Torres Escalante está en etapa de juicio por la muerte de **Daniel Torres (padre) y Roque Julio Torres (hijo),** campesinos que fueron asesinados por integrantes de la Brigada Nº16 en Aguazul, Casanare, y presentados como miembros del ELN abatidos en combate. (Le puede interesar: ["General (r) Henry Torres llamado a juicio por ejecuciones extrajudiciales"](https://archivo.contagioradio.com/general-r-henry-torres-llamado-a-juicio-por-ejecuciones-extrajudiciales/))

Según Rodríguez, Torres Escalante también podría estar vinculado con las muertes de Clodomiro Coba, Ignacio Pérez y Yolman Barbosa, quienes fueron asesinados por el grupo Delta 4, con la presunta participación del entonces director del DAS, **Orlando Rivas Tovar**. Sobre estos crímenes se han generado sentencias contra autores materiales, sin embargo, aún no se conocen los autores intelectuales.

### **Las víctimas piden que se sepa la verdad** 

El caso de Torres Escalante ya hace parte de la JEP, puesto que fue el primer General citado a audiencia por el Tribunal de paz, sin embargo, su comparecencia no fue satisfactoria para las víctimas, ya que aunque pidió perdón por los hechos, **no aceptó los cargos por los que se lo acusa.** (Le puede interesar: ["Citaciones y Audiencias: El nuevo capítulo de la JEP"](https://archivo.contagioradio.com/citaciones-y-audiencias-nuevo-capitulo-de-jep/))

Motivo por el cual, los afectados por las ejecuciones extrajudiciales pidieron que se supiera la verdad plena de los hechos y se brinden garantías de no repetición, porque para ellos, es claro que **no es válida la tesis de las 'manzanas podridas'** del Ejército, sino que hubo grandes cabezas actuando para que estas ejecuciones ocurrieran.

<iframe id="audio_27890228" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27890228_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<div class="stream-item-footer">

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 

</div>
