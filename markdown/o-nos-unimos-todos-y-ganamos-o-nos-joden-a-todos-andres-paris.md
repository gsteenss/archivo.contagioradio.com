Title: “Nos unimos todos y ganamos o nos joden a todos” Andrés Paris
Date: 2017-06-24 12:05
Category: Entrevistas, Paz
Tags: Acuerdos de paz con las FARC, Amnistia, unidad, Unión Patriótica
Slug: o-nos-unimos-todos-y-ganamos-o-nos-joden-a-todos-andres-paris
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Andres-paris.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: eltiempo] 

###### [24 Jun 2017]

Luego del anuncio de la alianza entre Alvaro Uribe y Andrés Pastrana de cara a las elecciones generales en 2018, Andrés Paris, integrante de las FARC-EP, quienes están próximos completar el 100% de la dejación de armas, afirmó durante el sexto congreso de la Unión Patriótica que el lema **“la esperanza es un nuevo tiempo” es un lema que vaticina que con las banderas de la paz se van a ganar las elecciones** y se va a derrotar al “candidato de la muerte”.

Para Andrés Paris, hay una sensación generalizada de que si no hay unidad el panorama será muy complicado, “o nos unimos todos y nos salvamos o nos joden a todos manteniéndonos desunidos” y señala que la alianza de Patrana y Uribe obliga una reacción rápida para “forjar la unidad” antes de que inicie una nueva ronda de exterminio.

El vocero de las FARC afirmó que hay plataformas y bases políticas fundadas en el miedo y el terror, que hay que afrontar en unidad. Sin embargo, a mediano plazo habría que esperar que los retos que se tienen en estos momentos tengan un debate ágil en el trámite de la **reforma política que está pendiente en el congreso de la república** en el marco de la implementación del acuerdo de paz.

### **El candidato del presidente Santos será Germán Vargas Lleras** 

Además, el integrante de las FARC confirmó que el presidente Santos acompañará la candidatura de Germán Vargas Lleras y que Alejandro Ordóñez será el candidato de las iglesias. A ese respecto París afirmó que convocar al espiritu cristiano para que las sectas religiosas no ganen en las campañas de desinformación en donde son tan activos.

Por otro lado, reiteró que se debe asegurar la implementación de los acuerdos de paz ceñidos al espíritu de la verdad y los derechos de las víctimas que deben seguir estando en el centro de los acuerdos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
