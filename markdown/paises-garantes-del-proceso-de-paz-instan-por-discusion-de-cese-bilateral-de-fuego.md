Title: Países garantes del proceso de paz instan por discusión de cese bilateral de fuego
Date: 2015-05-28 08:13
Category: Nacional, Paz
Tags: Conversaciones de paz de la habana, Cuba, Juan Manuel Santos, Noruega, paz, Proceso de conversaciones de paz de la Habana
Slug: paises-garantes-del-proceso-de-paz-instan-por-discusion-de-cese-bilateral-de-fuego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/paises-garantes-paz-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: pazfarc-ep.org 

##### [28May2015]

Ante la actual situación en el proceso de conversaciones de paz, Cuba y Noruega, países garantes del proceso de conversaciones entre el gobierno de Colombia y la guerrilla de las FARC-EP, han emitido un comunicado en el que demuestran su preocupación por el “actual escalamiento del conflicto” y “lamentan las pérdidas de vidas humanas”. Además hacen un llamado a la “adopción de un acuerdo para el cese bilateral definitivo”.

Durante las conversaciones de paz ha sido poco común que los países garantes decidan intervenir de manera directa, por lo que, para algunos analistas, este comunicado evidencia el difícil momento de las conversaciones de paz. En el comunicado de Cuba y Noruega se afirma que los hechos recientes ponen en riesgo la confianza de las partes y las acciones prácticas de des escalamiento.

A continuación el comunicado completo…

*La Habana, 27 de Mayo de 2015*

*Los Gobiernos de Cuba y Noruega, Garantes de la Mesa de Conversaciones entre el Gobierno de Colombia y las Fuerzas Armadas Revolucionarias de Colombia -Ejército del Pueblo- (FARC-EP), expresan su profunda preocupación por el actual escalamiento del conflicto armado en Colombia. Lamentan las pedidas de vidas humanas que se han producido.*

*Los avances logrados en la Mesa, con importantes acuerdos sobre tres de los puntos de la Agenda, deben ser preservados. Nunca antes desde el inicio del conflicto armado los colombianos habían estado tan cerca de lograr la paz.*

*El escalamiento de las acciones violentas también pone en riesgo las acciones prácticas que se han estado implementando para el desescalamiento del conflicto y el incremento de la confianza, como de las referidas a la limpieza y descontaminación del territorio de minas antipersonal, artefactos explosivos sin explotar y restos explosivos de guerra.*

*Hacemos un llamado a las partes a que continúen sus esfuerzos para seguir avanzando en la discusión de las cuestiones pendientes, incluyendo la adopción de un acuerdo para el cese bilateral definitivo del fuego y las hostilidades.*

*Cuba y Noruega reiteran su plena disposición a continuar contribuyendo en todo lo posible al avance de las conversaciones y la adopción de un acuerdo final para la terminación del conflicto y la construcción de una paz estable y duradera en Colombia.*
