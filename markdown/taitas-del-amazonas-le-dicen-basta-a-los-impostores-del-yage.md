Title: Taitas del Amazonas le dicen basta a los impostores del Yagé
Date: 2020-01-29 17:23
Author: CtgAdm
Category: Comunidad, Nacional
Tags: amazonas, colombia, indígenas, Yagé
Slug: taitas-del-amazonas-le-dicen-basta-a-los-impostores-del-yage
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/amazonian-psychedelic-tea-ayahuasca-yage.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/amazonian-psychedelic-tea-ayahuasca-yage-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/ayahuasca-yage-01.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/el_yage_-_florencia.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/20180915_Recordamos-a-Henry-Miller-Es-beneficioso-o-perjudicial-consumir-yagé.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/sergio-puentes-sobre-impostores-del-yage_md_47010536_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Ricardo Puentes | Experto en medicinas tradicionales

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

El 26 de enero diez chamanes y sabedores de Colombia y Ecuador hicieron un llamado a organismos nacionales e internacionales para que **se garantice la protección de los conocimientos tradicionales y se regule el uso de medicinas ancestrales como el Yagé**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El comunicado se hace en vocería de organizaciones de pueblos indígenas, autoridades políticas y espirituales del pueblo **Siona, Inga, Coreguaje, Kamentsá-Biya y Cofán, quienes denunciaron *"la apropiación, el abuso y la comercialización indebida de la planta sagrada del yagé,*** *y de tradiciones, prácticas y saberes que ponen en riesgo los derechos fundamentales de los pueblos indígenas".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:image {"id":80030,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/20180915_Recordamos-a-Henry-Miller-Es-beneficioso-o-perjudicial-consumir-yagé.jpg){.wp-image-80030}  

<figcaption>
Foto: <https://latinamericanpost.com/>

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Ademas señalaron, que empresas como la **Escuela Ayahuasquera Europea, Inner Mastery a cargo de Alberto Varela, y Verein Sol Jaguar de Antonio Valverde, *"siguen comercializando la medicina de yagé*** *y difundiendo malas-prácticas, poniendo en riesgo la salud de pacientes*". (Le puede interesar: <https://archivo.contagioradio.com/amerisur-pone-riesgo-la-pervivencia-del-pueblo-ziobain/>)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Una lucha que se mantiene a través de los años

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por medio del comunicado afirmaron que esta expropiación de su conocimiento viene desde hace más de 500 años, *"hoy día seguimos colonizados e invadidos, grupos armados, narcotraficantes, acaparadores de tierras, multinacionales mineras, entre otros, siguen amenazando la supervivencia de nuestros pueblos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Ricardo Puentes,** experto en medicinas tradicionales, señaló que la patente del yagé la tenía Loren Miller, un **estadounidense desde 1.986,** hasta que en 1.999, el abuelo mayor y cacique **Cofan Querubín Queta**, fue a la oficina de patentes en Estados Unidos y **reclamos sus derechos como guardián de esta planta sagrada**, *"gracias a sus argumentos y leyes que los cobijan como pueblo indígena, los pueblos pudieron recuperar la libertad de esta medicina"*, sañaló Puentes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que este comunicado no hace referencia a una reclamación por parte de los pueblos para apropiarse de este medicamento, *"el Yagé no es propiedad intelectual ni material de nadie, los pueblos indígenas reclaman la libertad de esta medicina ancestral, ellos son guardianes y defensores, no dueños".*

<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":80024,"width":348,"height":298,"sizeSlug":"medium"} -->

<div class="wp-block-image">

<figure class="aligncenter size-medium is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/el_yage_-_florencia-300x257.jpg){.wp-image-80024 width="348" height="298"}  
<figcaption>
Foto: caqueta.travel
</figcaption>
</figure>

</div>

<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### La adicción al Yage

<!-- /wp:heading -->

<!-- wp:paragraph -->

Puentes aclaró a la luz de las afirmaciones de los chamanes que un fenómeno mercantilista ha generado una apropiación de los conocimientos e imagen de los sabedores ancestrales, *"El objetivo de estas personas es buscar ganancias a toda costa, poniendo en riesgo la salud espiritual e incluso, hasta la vida de sus propios pacientes"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En los últimos años en Colombia diferentes personas se han acercado al yagé desde la curiosidad y el desconocimiento que termina en algunos casos en la muerte de quienes realizan esta practica, *"hay muchos que no poseen sabiduría, no conocen la planta pero realizan ceremonias asumiendo indebidamente el rol de médicos tradicionales*", señaló el comunicado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ejemplo de esta popularidad, así como el mal uso de este medicamento, según Puentes es *"la creación de una clínica de reposo hace casi 8 años en Francia, especializada en el tratamiento de personas con adicción al yagé ayahuasca*", y agregó que se ha evidenciado casos donde personas externas a la comunicad indígena ingieren este medicamento hasta dos veces al día durante la semana.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Medicina sagrada

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el comunicado los sabedores afirmaron que gracias a esta medicina han logrado sanar *"enfermedades de comuneros y comuneras y proteger su territorio y la vida de sus líderes y lideresas".* (Le puede interesar: <https://archivo.contagioradio.com/la-costumbre-de-hacer-criticas-sin-fundamento-el-caso-de-la-coca/>)

<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":80023,"width":251,"height":334,"sizeSlug":"medium"} -->

<div class="wp-block-image">

<figure class="aligncenter size-medium is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/ayahuasca-yage-01-225x300.jpg){.wp-image-80023 width="251" height="334"}  
<figcaption>
Ayahuasca /Foto: ayahuascapintayage.com
</figcaption>
</figure>

</div>

<!-- /wp:image -->

<!-- wp:quote -->

> *"El yagé no es un alucinógeno y no es una planta psicodélica. El yagé es una planta que posee un espíritu vivo y que nos enseña cómo vivir en paz y en armonía con la Pacha Mama"*
>
> <cite>Según comunicado </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por otro lado Puentes resaltó que así como cualquier comunidad científica o de expertos, *"los médicos indígena tienen que cumplir normas rigurosas y trabajar en conjunto con normativas espirituales reflejadas en varios documentos ancestrales"* , y añadió que el proceso para adquirir este conocimiento implica años, incluso toda la vida, *"por eso los múltiples errores se han visto han sido en manos de jóvenes inexpertos".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La responsabilidad del uso del Yagé es de todos

<!-- /wp:heading -->

<!-- wp:paragraph -->

El comunicado también agregó que "*aquellos extranjeros que consumen el yagé acceden a estos servicios sin darse cuenta que la apropiación y comercialización de nuestras tradiciones genera malas prácticas que dañan delicados equilibrios comunitarios y espírituales".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además según el experto es una responsabilidad que también recae en sobre las entidades del Estado, *"este es una caso que debe hacerle seguimiento el Ministerio de Salud, quien debe garantizar y regular el uso de medicamentos populares en la comunidad Colombiana"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último los voceros indígenas hacen un llamado a todas las personas interesadas en este ritual ***"a no poner en riesgo su salud participando de estas actividades comerciales y a respetar los procesos culturales** y sociales de resistencia de los pueblos indígenas".*

<!-- /wp:paragraph -->
