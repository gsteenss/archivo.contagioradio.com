Title: Comité de Impulso presentó informe sobre Resolución 1325
Date: 2020-10-31 22:08
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Consejo de Seguridad de la ONU, Mujeres y Paz, Resolución 1325
Slug: comite-de-impulso-presento-informe-sobre-resolucion-1325
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-en-la-celebracion-de-la-resolucion-1325-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este jueves 29 de octubre en la celebración del **20° Aniversario de la Resolución 1325 del Consejo de Seguridad de la ONU**, el Comité de Impulso hizo la presentación de,[informe: **A 20 años de la Resolución 1325. Las Organizaciones de Mujeres Revisan su implementación en Colombia durante el 2019.**](https://www.facebook.com/contagioradio/videos/662270007820580)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Resolución 1325 reconoce la participación de las mujeres en la resolución de conflictos, creando un camino hacia la paz y la seguridad internacional. Además, hace énfasis en el impacto desproporcionado de los conflictos armados en las mujeres.

<!-- /wp:paragraph -->

<!-- wp:image {"id":92151,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/informe-en-la-celebracion-de-la-resolucion-1325-1.jpg){.wp-image-92151}  

<figcaption>
Encuentro el 29 de octubre. /Foto: [@RNMColombia](https://twitter.com/RNMColombia)

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Beatriz Quintero, de la Red Nacional de Mujeres señala que el informe es una radiografía de lo que falta por implementar en el informe en cuanto a participación, prevención de violencia y reincorporación. (Le puede interesar: [Juana Perea, líder ambientalista es asesinada en Chocó](https://archivo.contagioradio.com/juana-perea-lider-ambientalista-es-asesinada-en-choco/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, Diana Salcedo,  Directora de la Liga Internacional de Mujeres por la paz y la Libertad -LIMPAL- comparte que **es una oportunidad para hablar desde las diversidades de las mujeres, siendo estas quienes «sostienen la paz, una paz reconocedora, creativa transgresora y transformadora de la opresión».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente agrega que cuando ni siquiera se hablaba de la Resolución 1325 ni mucho menos de las mujeres como actoras políticas, «nosotras ya estábamos construyendo paz». «Las mujeres estamos aquí para recordar que no es posible construir la paz y la democracia sin nosotras».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el evento y se presentó el documental **Palpitando la resistencia: mujeres en la paz de Colombia**, en el que se cuenta lo que las mujeres colombianas han aportado a la construcción de paz y al fin de la guerra mientras se escribía el acuerdo de paz.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Inclusión de mujeres LGBTI en la Resolución 1325
------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Lucía Vaca, coordinadora del  área de Paz de Colombia Diversa indica que la impunidad en materia de violencia sexual contra mujeres LGBTI **«es desolador». La fiscalía solo ha reportado 10 procesos penales abiertos por violencia sexual a mujeres LGBTI entre 2015 y 2019»,** por lo que explica cómo se incluyó a las mujeres de la comunidad LGBTI en el informe, ante las grandes brechas para implementar la resolución en esta comunidad. (Le puede interesar: [Feminicidios son sólo la punta del iceberg: Limpal](https://archivo.contagioradio.com/feminicidios-son-solo-la-punta-del-iceberg-limpal/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
