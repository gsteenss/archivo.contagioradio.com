Title: Radionovela "Sobrevivimos"
Date: 2019-09-24 16:09
Author: CtgAdm
Category: Expreso Libertad
Tags: actos de tortura en las cárceles, carceles, Cárceles colombianas, Expreso Libertad, radionovela
Slug: radionovela-sobrevivimos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-24-at-09.33.56.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/exxxx.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/ghjh.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/ñopoip.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/3908530_n_vir3.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/sovrevivimos-radionovela.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

*Sobrevivimos* relata la historia de Luis Alberto, un preso político que al ingresar a la Cárcel La Modelo, debe vivir los intentos por parte del paramilitarismo para acabar con el movimiento carcelario. Él junto a Camilo y Francisco, intentaran sobrevivir a las distintas masacres perpetradas por paramilitares en convivencia con la Fuerza Pública y alentar al resto de prisioneros políticos para continuar en la defensa de sus derechos humanos.

 

<iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/697341007&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true&amp;visual=true" width="100%" height="300" frameborder="no" scrolling="no"></iframe>

 

Le puede interesar ([Sin Olvido: Masacres en la Cárcel La Modelo](https://archivo.contagioradio.com/sin-olvido-masacres-en-la-carcel-la-modelo/))
