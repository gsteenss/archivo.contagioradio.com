Title: Yo reporto
Date: 2014-12-09 16:22
Author: AdminContagio
Slug: reporte-de-noticias
Status: published

Contagio Radio le invita a participar en nuestra web enviándonos sus notas, reportajes, noticias y fotografías de temas relacionados con derechos humanos en Colombia y el mundo. Gracias por acompañarnos en \#YoReporto  
   
\[contact-form-7 id="378" title="Sin título"\]  
[](https://archivo.contagioradio.com/acogiendo-la-vida-una-apuesta-para-la-proteccion-de-lideres-sociales-en-colombia/)  

###### [“Acogiendo la vida” una apuesta para la protección de líderes sociales en Colombia](https://archivo.contagioradio.com/acogiendo-la-vida-una-apuesta-para-la-proteccion-de-lideres-sociales-en-colombia/)

[<time datetime="2018-10-04T16:23:03+00:00" title="2018-10-04T16:23:03+00:00">octubre 4, 2018</time>](https://archivo.contagioradio.com/2018/10/04/)La Corporación Claretiana Normán Pérez Bello lanzó su campaña “Acogiendo la vida”, una apuesta de protección a los líderes sociales en Colombia[LEER MÁS](https://archivo.contagioradio.com/acogiendo-la-vida-una-apuesta-para-la-proteccion-de-lideres-sociales-en-colombia/)  
[](https://archivo.contagioradio.com/por-unanimidad-fue-reelegida-presidenta-del-polo-democratico-alternativo-clara-lopez-obregon-yoreporto/)  

###### [Por unanimidad fue reelegida presidenta Clara López como presidenta del Polo Democrático](https://archivo.contagioradio.com/por-unanimidad-fue-reelegida-presidenta-del-polo-democratico-alternativo-clara-lopez-obregon-yoreporto/)

[<time datetime="2015-05-16T20:48:13+00:00" title="2015-05-16T20:48:13+00:00">mayo 16, 2015</time>](https://archivo.contagioradio.com/2015/05/16/)Foto: Polo Democrático Alternativo 16 de May 2015 Clara López, presidenta del Polo Democrático Alternativo En el marco del segundo día del IV Congreso Nacional del Polo Democrático Alternativo, Clara López Obregón fue nuevamente elegida[LEER MÁS](https://archivo.contagioradio.com/por-unanimidad-fue-reelegida-presidenta-del-polo-democratico-alternativo-clara-lopez-obregon-yoreporto/)  
[](https://archivo.contagioradio.com/se-acerca-el-ii-foro-por-la-paz-de-colombia-en-uruguay-yoreporto/)  

###### [Se acerca el II Foro por la paz de Colombia en Uruguay \#YoReporto](https://archivo.contagioradio.com/se-acerca-el-ii-foro-por-la-paz-de-colombia-en-uruguay-yoreporto/)

[<time datetime="2015-05-13T15:59:09+00:00" title="2015-05-13T15:59:09+00:00">mayo 13, 2015</time>](https://archivo.contagioradio.com/2015/05/13/)13 May 2015 Con la experiencia y el saldo positivo que dejó el I Foro por la paz de Colombia realizado en Porto Alegre – Brasil en mayo de 2013, y con la firme y[LEER MÁS](https://archivo.contagioradio.com/se-acerca-el-ii-foro-por-la-paz-de-colombia-en-uruguay-yoreporto/)  
[](https://archivo.contagioradio.com/a-ti-eduardo-galeano-la-carta-de-despedida-de-una-nina-yoreporto/)  

###### [A ti, Eduardo Galeano… La carta de despedida \#YoReporto](https://archivo.contagioradio.com/a-ti-eduardo-galeano-la-carta-de-despedida-de-una-nina-yoreporto/)

[<time datetime="2015-04-26T12:06:29+00:00" title="2015-04-26T12:06:29+00:00">abril 26, 2015</time>](https://archivo.contagioradio.com/2015/04/26/)Foto: drugstoremag.es A ti, Eduardo Galeano, que te hiciste eterno, inmortal... “When he shall die, Take him and cut him out in little stars, And he will make the face of heaven so fine That all[LEER MÁS](https://archivo.contagioradio.com/a-ti-eduardo-galeano-la-carta-de-despedida-de-una-nina-yoreporto/)
