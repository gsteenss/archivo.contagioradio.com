Title: Campesinos Defensores de DDHH necesitan protección diferencial
Date: 2015-09-28 12:50
Category: DDHH, Nacional
Tags: cpdh, defensores de derechos humanos, Defensores Rurales, Encuentro Nacional de defensores y defensoras de Derechos Humanos rurales
Slug: campesinos-defensores-de-ddhh-necesitan-proteccion-diferencial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/afiche-encuentro-rural-01-1-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Imagen evento 

<iframe src="http://www.ivoox.com/player_ek_8666262_2_1.html?data=mZujmJeado6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRh8Lh0crgy9PTt4y4xsvS0NjTtsbnjMnSjamojKmfz8rQx9jNuMLijNXf0dnJp8Tdhqigh6eXsozYysuah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Pavel Santodomingo, Coordinador Encuentro] 

###### [28 Sep 2015] 

El **Encuentro Nacional de defensores y defensoras de Derechos Humanos rurales** organizado por el Comité Permanente de la Defensa de los Derechos Humanos (CPDH), presentó un balance en cuanto a las amenazas que reciben los DD.HH y las **medidas que debe tomar el  Estado** para la protección de quienes trabajan en contextos rurales y se encuentran en riesgo.

Pavel Santodomingo, coordinador del proyecto, recuerda que este año "*[1500 defensores han sido amenazados](https://archivo.contagioradio.com/ataques-contra-defensores-de-derechos-humanos-se-han-duplicado-en-2015/), muchos de ellos miembros de organizaciones rurales*", indica que una de las principales amenazas en las regiones donde trabajan los DD.HH es **el paramilitarismo el cual tiene nuevos focos** que “*ya no están organizados en estructuras que respondan a un solo mando*”, sino que son reductos de bloques anteriores.

A este problema se le suman las **actividades extractivistas** por parte de empresas y multinacionales en los territorios, ya que muchas de estas generan “ *alianza y coexistencia con grupos paramilitares*” presentando una amenaza permanente para los defensores.

El mismo Estado también se presenta como una amenaza al **estigmatizar a los líderes y defensores comunitarios** “*a los defensores los están judicializando sin pruebas, con un alto nivel de criminalización y de señalamiento*”, agregó el coordinador del proyecto y se refirió al caso de Feliciano Valencia

Durante esta investigación participaron defensores y defensoras de 16  departamentos  como Magdalena, Sucre, Norte de Santander, Antioquia, Arauca, Meta, Caquetá, Putumayo, Huila, Nariño, Cauca, Valle,Caldas y Boyacá.

### Medidas preventivas para DD.HH Rurales: 

-El Estado debe asumir el desmonte del paramilitarismo

-Un cese a la estigmatización de líderes y defensores en las comunidades.

-Más vías de comunicación entre las organizaciones y el Estado en las regiones.

- Un mecanismo efectivo de alertas tempranas por parte de la Defensoria del Pueblo.

-Que el Estado reconozca y dignifique la labor de DD.HH.

-Que los medios de comunicación manejen un lenguaje adecuado a la hora de informar sobre los defensores y líderes en las regiones.
