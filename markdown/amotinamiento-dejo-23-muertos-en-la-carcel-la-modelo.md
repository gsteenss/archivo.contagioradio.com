Title: Amotinamiento dejó 23 muertos en cárcel La Modelo
Date: 2020-03-22 14:21
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Cárcel modelo, cárceles colombia, INPEC
Slug: amotinamiento-dejo-23-muertos-en-la-carcel-la-modelo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/LA-MODELO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego de más de 10 horas la Ministra de Justicia Margarita Cabello, informó sobre los hechos sucedidos en la cárcel La Modelo que dejaron un saldo de 23 muertos y 83 heridos, cifra que según organizaciones podría ser superior. La situación fue calificada como una fuga masiva, sin embargo, la protesta se había convocado días antes por los detenidos tras las ausencia de medidas que los proteja de contraer el virus Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En las declaraciones, la Ministra aclaró que no hubo ninguna fuga y que 32 de los heridos se encuentran en hospitales y centro de atención, igualmente informó de siete funcionarios del Inpec heridos, dos de alta gravedad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con el movimiento carcelario, tanto los asesinatos como las personas heridas, fueron producto del acción violento y desmedido por parte de la guardia del INPEC. Asimismo aseguran que mientras se generaron los amotinamientos, si existió un intento de fuga, pero resaltan que no fue el motivo principal de las protesta como lo pretende asegurar la ministra de Justicia.

<!-- /wp:paragraph -->

<!-- wp:html -->

> Declaraciones de la Ministra de Justicia Margarita Cabello y el Director del [@INPEC\_Colombia](https://twitter.com/INPEC_Colombia?ref_src=twsrc%5Etfw), General Norberto Mujica con respecto a situación ocurrida en la cárcel nacional La Modelo en Bogotá. [pic.twitter.com/mp0AOHD4gH](https://t.co/mp0AOHD4gH)
>
> — Presidencia Colombia (@infopresidencia) [March 22, 2020](https://twitter.com/infopresidencia/status/1241773687495372800?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:heading -->

¿Por qué protestaba la población Reclusa?
-----------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El 18 de marzo del 2020 el “*Movimiento Nacional Carcelario*” convocó a todos los centros penitenciarios a un cacerolazo que se desarrollo en la noche de ayer, 21 de marzo, con la actividad se solicitaban medidas urgentes que prevengan la entrada del coronavirus a las cárceles y así mismo exigir que se declarará la emergencia carcelaria, al llamado se sumaron 14 cárceles del país.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En el año 2017 la Contraloría General de Nación informó de un hacinamiento de un 45.6% en 29 cárceles de Colombia.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esto sumado a la constante vulneración al derecho a la salud, en donde el 72% de los reclusos afirman no tener esta garantía, estos fueron las principales motivos del cacerolazo nacional en exigencia a medidas concretas por parte del gobierno de Iván Duque.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y es que según los reclusos de cárceles como Picaleña en Ibague, Cómbita en Boyacá, entre otras, si bien el pasado 14 de marzo el presidente Iván Duque anunció que se restringirían las visitas a las cárceles para evitar contagios, no sucedió lo mismo con los sectores administrativos o la guardia del INPEC, que continuaron entrando y saliendo de los centros penitenciarios, aumentando el riesgo de contagio.

<!-- /wp:paragraph -->

<!-- wp:heading -->

El Covid 19 una bomba de tiempo en las cárceles de Colombia
-----------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Comité Internacional de la Cruz Roja ha señalado que el virus que causa COVID-19 parece propagarse fácilmente, afirmando que "la tasa de transmisión dentro de los lugares de detención será mayor que fuera de un entorno de detención, debido a las condiciones que suelen caracterizarse por tener ventilación inadecuada, hacinamiento y sistemas de salud más deficientes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta propagación en un sistema penitenciario que desde 1998 fue declarado con el estado de cosas inconstitucionales, ordenado por la Corte Constitucional tras una masiva violación a los derechos fundamentales, podría ser el caldo de cultivo perfecto para que haya un riesgo más alto de propagación del virus.

<!-- /wp:paragraph -->
