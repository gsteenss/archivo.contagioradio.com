Title: Resistencias
Date: 2014-11-25 15:43
Author: AdminContagio
Slug: resistencias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/aborto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

RESISTENCIAS
------------

[](https://archivo.contagioradio.com/campesinos-piden-ser-censados-en-el-2018/)  

###### [Campesinos piden reconocimiento de su labor en el censo poblacional](https://archivo.contagioradio.com/campesinos-piden-ser-censados-en-el-2018/)

[<time datetime="2017-11-23T16:43:39+00:00" title="2017-11-23T16:43:39+00:00">noviembre 23, 2017</time>](https://archivo.contagioradio.com/2017/11/23/)Diferentes organizaciones campesinas interpusieron una tutela que busca proteger los derechos fundamentales del campesinado a través de un censo que los cuente como población[Leer más](https://archivo.contagioradio.com/campesinos-piden-ser-censados-en-el-2018/)  
[](https://archivo.contagioradio.com/buenaventura-vuelve-a-marchar/)  

###### [Buenaventura volverá a marchar ante incumplimientos del gobierno](https://archivo.contagioradio.com/buenaventura-vuelve-a-marchar/)

[<time datetime="2017-10-20T17:49:24+00:00" title="2017-10-20T17:49:24+00:00">octubre 20, 2017</time>](https://archivo.contagioradio.com/2017/10/20/)Si bien se ha avanzado en la mesa ambiental, según asegura Javier Torres, líder de Buenaventura, el resto de las mesas temáticas no han avanzado[Leer más](https://archivo.contagioradio.com/buenaventura-vuelve-a-marchar/)  
[](https://archivo.contagioradio.com/militarizan_sur_bogota_relleno_dona_juana/)  

###### [Ante anuncio de Paro cívico distrito militariza el sur de Bogotá](https://archivo.contagioradio.com/militarizan_sur_bogota_relleno_dona_juana/)

[<time datetime="2017-09-20T17:21:21+00:00" title="2017-09-20T17:21:21+00:00">septiembre 20, 2017</time>](https://archivo.contagioradio.com/2017/09/20/)Foto: Habitantes 20 Sep 2017 Este miércoles amanecieron militarizadas dos veredas en Ciudad Bolívar y más de 100 barrios de ese sector. Además la empresa operadora del Relleno sanitario Doña Juana en compañía de militares y policía habrían instalado cámaras al interior de la zona con el[Leer más](https://archivo.contagioradio.com/militarizan_sur_bogota_relleno_dona_juana/)  
[](https://archivo.contagioradio.com/presos_politicos_farc_huelga_carcelaria/)  

###### [Presos políticos de las FARC en Florencia inician huelga de hambre](https://archivo.contagioradio.com/presos_politicos_farc_huelga_carcelaria/)

[<time datetime="2017-06-24T04:32:56+00:00" title="2017-06-24T04:32:56+00:00">junio 24, 2017</time>](https://archivo.contagioradio.com/2017/06/24/)Empezarán una huelga de hambre y desobediencia civil pacífica a partir de las 00:00 horas del lunes 26 de junio.[Leer más](https://archivo.contagioradio.com/presos_politicos_farc_huelga_carcelaria/)
