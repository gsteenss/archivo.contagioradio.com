Title: Militares hostigan a guerrilleros de las FARC-EP en el Meta
Date: 2016-12-22 12:19
Category: DDHH, Nacional
Slug: militares-hostigan-a-guerilleros-de-las-farc-ep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FARC-EP] 

###### [22 Dic 2016] 

En un comunicado de prensa las Farc-Ep, el Comando de esta guerrilla, ubicado en la zona de preagrupamiento del municipio de Vista Hermosa, en Meta, denunció que las Unidades militares del Ejército Nacional que operan en este lugar **“han venido violando el acuerdo del cese al fuego bilateral y definitivo con claras intenciones crear un ambiente de desconfianza”.**

Esto debido a que según el texto, el pasado 20 de diciembre  9 miembros del **Ejército que se movilizaban en moto traspasaron los límites del punto de la zona de preagrupamiento**, con la finalidad de llevarse a un guerrillero que se encontraba en este lugar. Tensionando el cese al fuego, en un momento álgido como es la implementación de los acuerdos de Paz.

De igual modo, las FARC-EP también señala que estos hechos hacen parte de una campaña premeditada en su contra, en donde al parecer Fuerzas Militares estarían convenciendo a miembros de la guerrilla para que se salgan de la misma antes de que se de la implementación de los acuerdos de paz “denunciamos las constantes invitaciones a unidades nuestras, para que desistan del proceso de paz por medio de llamadas a guerrilleros, familiares y amigos, con el fin de **desmotivarlos y llevarlos a la deserción”.**

Además agregaron que el pasado 22 de noviembre, una patrulla de la **Brigada XII, se desplazó en  este mismo punto de Vista Hermosa, con personas encapuchadas que estuvieron rondando la zona de pre agrupamiento** y generando zozobra entre la población. Le puede interesar:["27 de diciembre será la prueba de fuego para la ley de amnistía"](https://archivo.contagioradio.com/27-de-diciembre-prueba-de-fuego-ley-de-amnistia-33926/)

En el documento las FARC –EP señalaron que no se entiende “que en esta etapa final en la que se busca poner fin a una confrontación de más de 52 años, aún persistan sectores militares poniendo trabas a la construcción de paz que tanto anhela el pueblo colombiano”. De igual forma, ratificaron su voluntad para seguir en la construcción de una paz para el país y de **cumplir con todos los protocolos y acuerdos ya firmados por ambas partes.**

######  Reciba toda la información de Contagio Radio en [[su correo]
