Title: Empresarios hondureños tendrían vínculos con paramilitares colombianos
Date: 2016-03-04 16:45
Category: El mundo, Entrevistas
Tags: Berta Cáceres, Paramilitares Honduras
Slug: empresarios-hondurenos-tendrian-vinculos-con-paramilitares-colombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Paramilitares-Honduras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Universal ] 

<iframe src="http://co.ivoox.com/es/player_ek_10678299_2_1.html?data=kpWjmZ2WfZqhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncabh0dfS1cbWrdDnjM3c0MnZtsaZpJiSpJbTt4zoxtPR1Iqnd4a1pcbbjduJh5SZoqnbxdrQs9SfxNTbjdXFtsLhytGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [David Romero, Radio Globo] 

###### [4 Mar 2016 ]

De acuerdo con David Romero, director Radio Globo, el asesinato de Berta Cáceres se da en “un ambiente tenso y de zozobra” que pone **en riesgo la vida e integridad de los líderes indígenas, defensores de los derechos humanos y periodistas en Honduras**, un país que tras el golpe de Estado de 2009 se convirtió en una nación, “militarizada, con política de seguridad nacional, con el Plan Colombia, con paramilitares, con falsos positivos, con persecución, tortura y desaparición”.

“El Gobierno ha lanzado su primer mentira al aire encaminada a hacernos creer que se trata de una muerte pasional” afirma Romero refiriendo el crimen contra Berta Cáceres, e insiste en que **detrás de este asesinato están las transnacionales generadoras de energía, como Agua Zarca, y los dirigentes del partido nacional**, alcaldes de los municipios en los que las comunidades se han movilizado contra los proyectos energéticos. Lo cual puede ser constatado con el testimonio que fue dado a un sacerdote por un hombre que vio a los asesinos de Cáceres en la escena del crimen.

Romero asevera que existe una “relación directa” entre empresarios y aparatos criminales colombianos con compañías y brazos armados hondureños, bajo concepto de acumulación de capital y ganancia, a través de la concesión total de recursos naturales. “Tras el golpe de Estado hubo una **migración de colombianos, especialmente de paramilitares que llegaron a asesorar a terratenientes del Bajo Aguán**” para propiciar la muerte, desaparición y tortura de campesinos que luchaban por defender sus tierras, ríos y bienes naturales, agrega.

En el asesinato de esta líder resultó herido el dirigente indígena mexicano Gustavo Castro, a quien las autoridades involucran en la muerte de Cáceres, cuyo ex esposo fue capturado por su supuesta responsabilidad en el crimen, acciones que demuestran, según afirma Romero que **las autoridades quieren ocultar la verdad de los hechos**. Distintos sectores se movilizan, no sólo en Honduras, exigiendo justicia en este atroz caso. Hasta el momento el funeral de la líder ha sido multitudinario. Este sábado en horas de la tarde será el sepelio.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

 
