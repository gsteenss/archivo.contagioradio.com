Title: INPEC continúa incumpliendo acuerdos establecidos en la Tramacua
Date: 2016-06-03 19:01
Category: DDHH, Nacional
Tags: Crisis carcelaria en Colombia, INPEC, La Tramacua
Slug: inpec-continua-incumpliendo-acuerdos-establecidos-en-la-tramacua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/La-Tramacua-II.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Turbión] 

###### [3 Jun 2016] 

[En la Tramacua, la cárcel de Valledupar, proliferan insectos y enfermedades, persisten los problemas sanitarios y de atención en salud, al igual que el hacinamiento, denuncia uno de sus internos e insiste en que **el INPEC no ha cumplido con la modificación del reglamento interno de la penitenciaria** que permitiría mejorar la situación.  ]

[Los internos aseguran que esta modificación del reglamento obligaría al INPEC **permitir el ingreso de ventiladores, un abanico por cada recluso, instalar toldillos, posibilitar el acceso a un radio transmisor**, reglamentar las visitas conforme a la ley 1709 de 2014 y permitir el uso de ropa civil, ya que los uniformes proporcionados a los presos, no son adecuados para las altas temperaturas.]

[De igual forma denuncian que **el suministro de agua es insuficiente, tan sólo les prestan el servicio durante 15 minutos una a dos veces al día**, lo que hace que muchos presos lancen sus desechos fisiológicos por las ventanas, situación que genera no solo la proliferación de zancudos, cucarachas y hormigas, sino también de enfermedades cutáneas y digestivas.]

[Los internos también tienen que **soportar las altas temperaturas que oscilan entre los 30 y 40 grados centígrados**, pues no cuentan con aire acondicionado ni ventiladores en las celdas en las que pese a que sean aptas para dos personas, conviven mínimo tres. Además de los incumplientos del INPEC, la falta de suministros para la atención de enfermos, la inadecuada prestación de servicios básicos como el agua, y el hacinamiento, los visitantes "reciben tratos crueles, inhumanos y degradantes".  ]

[Los internos esperan que el INPEC cumpla de forma eficaz los puntos establecidos para la modificación del reglamento interno de la Tramacua, al igual que el mejoramiento de las condiciones vitales al interior de la prisión. ]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
