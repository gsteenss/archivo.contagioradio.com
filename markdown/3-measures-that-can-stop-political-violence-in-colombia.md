Title: 3 Measures That Can Stop Political Violence in Colombia
Date: 2019-09-04 17:41
Author: CtgAdm
Category: English
Tags: MOE, political violence
Slug: 3-measures-that-can-stop-political-violence-in-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/MOE-Accidente.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Photo: MOE] 

[In the first 35 days of the electoral season in Colombia, the Electoral Observatory Mission (MOE) has registered one homicide attempt, one kidnapping and 10 death threats against political candidates. The most recent assassinations of Karina García, a mayoral candidate in Suarez, Cauca, and Yeison Obando, a City Council candidate of the same municipality, are symptoms of a surge in violence that is affecting regions across the country, the electoral monitor group warned.]

[Alejandro Barrios, the director of the MOE, stated that the risks candidates face varied according to the region where they live and the armed actors that control the area. “For example, in Arauca, it’s not the same for a candidate that has a background in social activism as it is for one that is tied to the traditional parties. This depends on the regions and their security depends on this,” Barrios said.]

[Despite the regional differences, the director assured that the illicit economies and the presence of illegal armed groups are the root cause of the increase in violence.]

### **The Activation of the Immediate Electoral Reaction Group**

[In July, President Ivan Duque presented a new plan to fight criminal activity during the electoral season known as the Immediate Electoral Reaction Group (GRIE). This group focuses on the most critical areas in the country and includes various state departments.]

[The government also created a special committee for electoral security measures and an integrated center of information for electoral intelligence, which includes the police and the MOE.]

### **“It’s necessary to identify the characteristics of the regions”**

[The MOE also stated that applying the same security measures to all the regions despite their differences would be ineffective. “Plans that only contemplate national elections don’t contemplate nor understand the realities and the threats that may present themselves at a local level,” Barrios said.]

[An approximate 117,000 citizens are running for office this year, according to Barrios, and not all of them can be protected with bulletproof cars or bodyguards. The MOE director pointed out that Karina García had been afforded these security measures but was ultimately assassinated. For Barrios, this proves the necessity of designing security measures that address the characteristics of the region.]

### **Dismantling verbal violence**

[Similarly, Barrios said that misinformation on the elections and the candidates can spread through social media and in-person conversations. He characterized this as symbolic violence and assured that it can also lead to real violence against political candidates.” Before Karina García’s death, she warned in a self-recorded video that the spread of false information could have dangerous consequences for her.]

The MOE director advised that political parties should be careful of their candidates' messages. "Yes to debate and to strong debates, but we can't incentivize the assassination of those who are competing for public office in this country with messages of hate."

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
