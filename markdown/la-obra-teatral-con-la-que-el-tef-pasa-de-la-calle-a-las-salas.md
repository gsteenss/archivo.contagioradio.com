Title: La obra teatral con la que el 'TEF' pasa de la calle a las Salas
Date: 2016-03-02 11:54
Category: Cultura, eventos
Tags: Muerte, Raúl Cortés Dramaturgo español, Resurrección y muerte obra teatral, Teatro experimental de Fontibón
Slug: la-obra-teatral-con-la-que-el-tef-pasa-de-la-calle-a-las-salas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/tefobra-e1457111988931.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: TEF 

###### [03 Marz 2016] 

Tras 36 años convirtiendo las calles en un escenario, el Teatro Experimental Fontibón (TEF) arranca una nueva etapa con el estreno, el próximo 4 y 5 de marzo de 2016, de la obra “Muerte, Resurrección y Muerte”, de Raúl Cortés, dramaturgo y director español (Morón de la Frontera, 1979).

Con este nuevo montaje, el TEF desafía su propia historia y se adentra en la aventura de un nuevo lenguaje: el teatro de sala. Y es que hace poco más de un año este grupo, dirigido por Emilio y Ernesto Ramírez, que atesora el mérito de ser uno de los más longevos del teatro regional, inauguró su propia sede, la sala TEF, homenaje a Augusto Boal.

Con “Muerte, Resurrección y Muerte”, además, el TEF pondrá sobre la escena un asunto de capital importancia en el contexto político actual del país: la justicia. En palabras de Raúl Cortés, autor y director de la obra, la propuesta “es una reflexión sobre la justicia y todo el aparato que el hombre ha ideado en su afán de preservar la ley y el orden, de alcanzar la paz… porque sin justicia no puede haber paz. Una mirada crítica, el trazo grueso de una caricatura a veces divertida y otras veces aterradora”.

Tras varios meses de trabajo intensivo, el TEF y Raúl Cortés (fundador de la compañía española Trasto Teatro, que ha sido distinguido con varios galardones internacionales) estrenarán próximamente un trabajo arriesgado, poético, sísmico… preñado de esas preguntas que cortan el aliento, que siembran de temblor el silencio.

Una nueva oportunidad, pues, para contemplar el trabajo de uno de los grupos más comprometidos e inconformistas de la escena bogotana, cuya voz es ya perfectamente reconocible. Una voz que retumba habitualmente en el Festival Iberoamericano de Teatro de Bogotá (este año, con su obra Canovaccio, dirigida por el director Peruano Alex Ticona) un espectáculo de calle sobre la comedia del arte Italiana). Tal vez, estas sean las razones por las que la Universidad de Estadual de Santa Catarina (Brasil) prepara un proyecto de investigación sobre la historia y trayectoria del TEF, siglas que cada vez son más internacionales y respetadas en el panorama teatral.

**Lugar:** Fontibón, en la Calle 24 D Bis No. 99-28

**Hora:** 7:00 p.m.

**Entrada libre** (con aporte voluntario)
