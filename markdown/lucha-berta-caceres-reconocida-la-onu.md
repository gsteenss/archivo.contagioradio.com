Title: Lucha de Berta Cáceres es reconocida por la ONU
Date: 2016-12-05 12:17
Category: Ambiente, El mundo
Tags: Ambiente, Berta Cáceres, ONU
Slug: lucha-berta-caceres-reconocida-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Justicia-Berta-Cáceres_AFP-e1466625702189.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### [5 Dic 2016] 

Pese a su asesinato la líder indígena hondureña, Berta Cáceres continúa siendo reconocida por su labor en la defensa de la naturaleza. Esta vez, la Organización de las Naciones Unidas, ONU, le concedió este sábado **el premio Campeón de la Tierra en la categoría de Inspiración y Acción,** en el marco de la 13 Conferencia de las Partes (COP13) del Convenio sobre Diversidad Biológica (CDB) de las Naciones Unidas.

Se trata del galardón más importante que otorga las Naciones Unidas a los defensores ambientales. Un premio que se otorga a agentes sociales cuyas acciones tuvieron un impacto positivo para el ambiente y a su vez a los derechos de las comunidades. En el caso de Berta, se resaltó su lucha contra la construcción de una planta hidroeléctrica en tierras de la etnia indígena Lenca.

“**Berta Cáceres se rehusó a permitir que poderosos intereses violaran los derechos de los pobres y marginados y destruyeran los ecosistemas de los que dependen**", dijo Erik Solheim, director ejecutivo de la ONU para el Ambiente.

Cáceres fue una de las fundadoras en 1993 del Consejo de Pueblos Indígenas de Honduras, Copinh. A su mando, esa organización alertó sobre la expropiación de territorios lencas y también protestó por las carencias en los sistemas de salud y agrícola de su pueblo. Además rechazó la creación de bases militares estadounidenses en ese territorio. Antes de su[asesinato el pasado 3 de marzo](https://archivo.contagioradio.com/desa-asesinato-de-berta-caceres-honduras/), denunció el asesinato de cuatro líderes indígenas de la comunidad Lenca.

El hermano de Berta, Roberto Cáceres, fue quien recibió el premio y en su discurso dijo que su **familia espera que ese premio “ayude a asegurar que la maravillosa vida de Berta, así como la lucha del pueblo Lenca,** no se olvide y a inspirar a todos los que luchan por los derechos ambientales en el mundo".

Finalmente, Solheim expresó que aunque la lucha de Berta era local “su causa y su sacrificio resuenan en todo el mundo. Ella es una gran inspiración, y una gran pérdida, para cualquier persona que luche por los derechos ambientales”.

### Los otros premios 

En la misma categoría de Berta, Afroz Shah fue premiado por su liderazgo e iniciativa para movilizar apoyo público a gran escala con el fin de eliminar 3.000 toneladas de basura de la playa de Versova en Mumbai, India.

En la categoría Liderazgo político, Paul Kagame, presidente de Ruanda, fue destacado por su lucha contra el cambio climático y en la acción ambiental nacional.

Leyla Acaroglu, fundadora de Disrupt Design, Eco Innovators, y UnSchool, fue premiada por promover un cambio positivo a través del diseño, la innovación, la comunicación y la conexión humana, en la categoría Ciencia e Innovación.

###### Reciba toda la información de Contagio Radio en [[su correo]
