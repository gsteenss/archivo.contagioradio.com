Title: Listo escenario para la participación social en conversaciones de Quito
Date: 2017-10-14 17:26
Category: Nacional, Paz
Tags: ELN, Proceso de paz en Quito
Slug: listo-escenario-para-la-partcipacion-social-en-conversaciones-de-quito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Oct 2017]

A través de un comunicado de prensa el Ejercito de Liberación Nacional y el Gobierno informaron que desde el 20 de octubre hasta el 13 de noviembre se desarrollarán las audiencias preparatorias, escenarios que ayudaran a **consolidar el primer punto de la agenda de diálogos "Participación de la sociedad en la construcción de Paz"**. Las formas de participación se darán algunas vías internet, otra desde los diferentes territorios del país, mientras otras serán presenciales.

Se espera que la metodología de estos espacios para construir esta ruta de trabajo, consistirá en reunir diversos sectores de la sociedad, organizaciones sociales y colectivos del movimiento social junto con las delegaciones del proceso de paz, para conocer las **propuestas e iniciativas que se han creado como mecanismo para que la sociedad participe en la mesa de Quito**. Las organizaciones serán escogidas de forma tal que la mitad las haya seleccionado el gobierno y la otra mitad el ELN.

Los escenarios tendrán dos preguntas guías para dar el debate, la primera es ¿Según sus experiencias y conocimientos, cuáles considera usted que deberían ser los mecanismos y formas de participación de la sociedad en el proceso de conversación, y en particular la participación de su sector?

La segunda es, sí su organización o sector tiene una experiencia que usted considere importante para los mecanismos y formas de participación de la sociedad, por favor expongalos. (Le puede interesar: ["Listo mecanismo de monitoreo y verificación del Cese bilateral entre el ELN-Gobierno"](https://archivo.contagioradio.com/cese-al-fuego-entre-el-eln-y-el-gobierno-ha-reducido-intensidad-del-conflicto/))

Con esta metodología se pretende hacer una síntesis mucho más fácil que permita construir el mecanismo y la ruta de participación que tendrá la sociedad en las conversaciones de paz en Quito y que desde un inició, **cuando se estableció la agenda de diálogos, el ELN manifestó como centro del proceso de paz. **

###### Reciba toda la información de Contagio Radio en [[su correo]
