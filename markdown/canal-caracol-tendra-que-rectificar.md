Title: Tutela ordena a Canal Caracol rectificar información falsa sobre ONIC
Date: 2016-11-04 18:12
Category: Judicial, Nacional
Tags: Canal Caracol, CRIC, Manuel Teodoro, ONIC, Rectificacion, Séptimo Día.
Slug: canal-caracol-tendra-que-rectificar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/caracol-y-onic.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**La Corte Constitucional emitió un fallo de segunda instancia que favorece el derecho al buen nombre y la honra de los integrantes de la Organización Indígena de Colombia -ONIC-  y el Consejo Regional Indígena del Cauca -CRIC -** luego de que el Canal Caracol emitiera una serie de dos programas en los que estigmatizó a las comunidades indígenas, y omitió la información aportada por los voceros de esas organizaciones en la que aclaraban las acusaciones que se les imputaban.

La sentencia de la Corte Constitucional se refiere a 3 emisiones del programa Séptimo Día, en que se acusó a las comunidades indígenas del Cauca de malversar fondos, supuestamente asignados por el Estado como reparación por la masacre del Nilo, entre otras. **Los periodistas de dicho programa omitieron la información en la que autoridades indígenas aclaraban que los fondos entregados no fueron los publicados en el programa y tampoco fueron manejados directamente por los indígenas. **Le puede interesar: [Comunidades campesinas del Cauca desmienten a periodista de Caracol](https://archivo.contagioradio.com/comunidades-campesinas-del-cauca-desmienten-a-periodista-de-caracol/)

En ese sentido, **la Corte ordenó que Canal Caracol, Séptimo Día y el periodista Manuel Teodoro deben adoptar un manual de ética** “que incluya unas reglas mínimas para abordar temas relacionados con grupos étnicos, minorías sexuales y demás sujetos tradicionalmente estigmatizados dentro de nuestro contexto social” además, afirma la Corte, que deberán aplicar dicho manual de manera constante y como mecanismo de evaluación de todos sus contenidos.

La sentencia T 500 de 2016 también le ordena al Canal Caracol “**dedicar en un lapso no superior a seis (6) meses un episodio completo de dicho programa, en su horario habitual, para permitirle a la organización demandante defenderse frente a las acusaciones hechas”. **

En su momento, Luis Fernando Arias, Consejero Mayor de la ONIC había denunciado que *"**Séptimo Día se propuso a desprestigiar la lucha indígena que está marcada de sangre, muerte, desplazamiento, violación**"* además responsabilizó de cualquier acción en contra de las comunidades indígenas a Manuel Teodoro y al programa Séptimo Día. Hasta el momento se desconoce un comunicado oficial por parte del canal o de sus directivas. Leer aquí contexto: [Onic demandará penalmente a Séptimo Día](https://archivo.contagioradio.com/onic-demandara-penalmente-a-septimo-dia/)
