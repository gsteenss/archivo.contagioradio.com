Title: Líderes caucanos huyen de su tierra amenazados y perseguidos
Date: 2020-04-10 19:29
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Amenazas, Cauca, covid19, Desplazamiento, Lideres
Slug: se-agudiza-conflicto-armado-en-el-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Grupos-armados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Red por la vida y los Derechos Humanos del Cauca denuncia que se agudiza el conflicto armado en este departamento, en medio de la pandemia del Covid 19. Hecho que ha provocado el desplazamiento de **cinco líderes y lideresas sociales**, y el aumento de amenazas y hostigamiento contra los mismos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el comunicado, en este periodo de tiempo se han profundizado las vulneraciones a los derechos humanos para la población caucana, "pero, especialmente para las comunidades menos favorecidas, especialmente las rurales". (Le pude interesar: "[Guerra y Covid-19, comunidades indígenas y afro bajo doble confinamiento en Valle del Cauca](https://archivo.contagioradio.com/guerra-y-covid-19-comunidades-indigenas-y-afro-bajo-doble-confinamiento-en-valle-del-cauca/)")

<!-- /wp:paragraph -->

<!-- wp:heading -->

En máximo riesgo líderes del Cauca
----------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con la red, la última semana de marzo se produjo el desplazamiento de 5 líderes del Consejo Comunitario AFRORENACER de Micay, en el corregimiento de San Juan de Micay, municipio del Tambo, junto a otras **15 personas de las veredas de Betania, Honduras y San Juan.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sumada a ello, el 8 de abril, hombres llegaron a la vivienda del líder Henry Agudelo, integrante de la organización campesina ACAAMI, en el corregimiento de Huisito, municipio del Tambo. Posteriormente **lo amenazaron de muerte a él y su familia, obligándolos a entregarles su hogar y el celular personal del lider.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estos hechos habrían sido producto de la disidencia de las FARC, autodenominada como "Columna Carlos Patiño", al mando de alias Ricardo. Actualmente las personas se encuentran en la ciudad de Popayán, a la espera de una respuesta por parte de los gobiernos territoriales y locales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Red por la vida y los Derechos Humanos del Cauca hace un llamado a las autoridades departamentales y nacionales **para tomar medidas frente al desplazamiento forzado de estas familias**. Asimismo piden a estas autoridades estar prestos a responder a las solicitudes y necesidades que se han planteado en la Mesa Territorial de Garantías. (Le puede interesar: ["Tercer niño indígena que muere en una semana"](https://www.justiciaypazcolombia.com/tercer-nino-indigena-que-muere-en-una-semana/))

<!-- /wp:paragraph -->
