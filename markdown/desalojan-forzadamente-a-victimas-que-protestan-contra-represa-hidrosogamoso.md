Title: Desalojan forzadamente a víctimas que protestaban contra Hidrosogamoso
Date: 2015-06-19 13:06
Author: AdminContagio
Category: Ambiente, Resistencias
Tags: hidrosogamoso, ISAGEN, Policía Nacional, represa, Rio Sogamoso, Rios Vivos, Santander
Slug: desalojan-forzadamente-a-victimas-que-protestan-contra-represa-hidrosogamoso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/descarga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto [veredasogamoso.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_4673985_2_1.html?data=lZuklZ6ceY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRiMbnwtHczMbSb8fj09%2FOxsbRqc%2FoxpDOjduJh5SZoqnQ1s7RpdSf0trSjdXWs9XZ1NnO0JDHs8%2Fo08aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Blanca Nubia Anaya, líder de la comunitaria ] 

###### [ 19 Jun 2015]

Tras 3 meses de pedir la presencia del gobernador de Santander para que interfiera en el proyecto de la hidroeléctrica Hidrosogamoso, la comunidad decidió realizar un plantón de forma pacífica frente a la puerta de la gobernación para que se les prestara atención, y realizaron una protesta en la que se amarraron con cadenas de manera simbólica, permitiendo el paso de las personas y funcionarios de la gobernación, sin embargo, la respuesta de las autoridades fue el desalojo forzoso por la fuerza.

Desde el día martes 16 de junio, víctimas de la represa, entre ellos adultos mayores, se encadenaron entre sí frente a las puertas de la Gobernación de Santander con el fin de exigir respuestas ante las graves afectaciones ambientales y sociales que ha dejado la construcción de la represa construida por ISAGEN, en el río Sogamoso. Vea también: [HIDROSOGAMOSO “HUELE A PODRIDO”](https://archivo.contagioradio.com/hidrosogamoso-huele-a-podrido/) .

La respuesta que recibieron los manifestantes, se dio por medio de golpes y empujones de policías exigiendo que se fueran del lugar. De acuerdo a Blanca Nubia Anaya, líder de la comunidad, al menos cinco personas habrían sido víctimas de los abusos por parte de los agentes quienes había arrastrado a varias personas, entre ellas, adultos mayores enfermos.

![Policía agrede a adulto mayor al protestar contra Hidroituango](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/image-e1434738376487.png){.aligncenter .size-full .wp-image-10294 width="385" height="324"}

"Afortunadamente llegó un carro de un medio masivo de televisión, y cuando los policías se dieron cuenta se detuvieron y pidieron disculpas por el ataque", relata Anaya, quien añade que la comunidad fue agredida cuando en ningún momento las personas que protestaban usaron la violencia.

De acuerdo a cifras de ISAGEN, **son 2000 familias, es decir más de 2 mil personas, las que vivían del río Sogamoso** y que ahora se encuentran afectadas por el proyecto hidroeléctrico. Así mismo, Blanca denuncia que la comunidad se ha quedado sin trabajo y la empresa solo ha brindado trabajo a unos pocos, que terminan manipulados por la compañía, ya que si llegan a denunciar o testificar en contra de la empresa, los campesinos serían sacados del trabajo que les dio ISAGEN.

Aunque la comunidad ha denunciado los impactos que han sufrido tras la realización de este proyecto, las autoridades ambientales, siempre han dado como respuesta que la empresa tiene todo en regla,  cuando **se ha evidenciado  por parte de expertos, universidades e investigadores, las consecuencias ambientales, sociales y económicas** que ahora cargan los pobladores tras la construcción de la represa.

Blanca Anaya, asegura que hay formas de negociar con el gobierno y con la empresa, "solo pedimos tierra para trabajar ya que nos mataron el río, solo le pedimos al gobierno que cuide las demás fuentes hídricas", pues el río Sogamoso fue contaminado por los químicos y ácidos que utiliza la hidroeléctrica. Vea también: [ISAGÉN TIENE 6 MESES PARA LIMPIAR EL RÍO SOGAMOSO](https://archivo.contagioradio.com/hidrosogamoso-huele-a-podrido/)

<iframe src="https://www.youtube.com/embed/KP70vKJnneU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
