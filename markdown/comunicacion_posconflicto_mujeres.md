Title: Comunicación para narrar el posconflicto, una mirada desde las mujeres
Date: 2017-04-28 14:22
Category: Mujer, Paz
Tags: conflicto armado, mujeres, paz, periodismo
Slug: comunicacion_posconflicto_mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/marcha01.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [28 Abr. 2017] 

Este 2 y 3 de mayo en Bogotá se llevará a cabo el **Seminario Internacional: Las mujeres y los medios en los procesos de paz**, convocado a propósito de los 10 años de la Red Colombiana de Periodistas con Visión de Género. Evento que analizará, entre otros temas, el ejercicio periodístico con miras a buscar la igualdad de derechos de las mujeres y su aporte en la construcción de la paz en Colombia.

Este evento contará con la presencia de invitadas internacionales, quienes han tenido un amplio recorrido en el trabajo periodístico con enfoque de género, en el marco de un proceso de paz. Tal es el caso de Rosalinda Hernández Alarcón, de Guatemala, quien es fundadora de *La Cuerda,* una publicación feminista que surge luego de la firma de los Acuerdos de Paz en ese país. Le puede interesar: [Acuerdo de Paz puede ser inicio para reparar mujeres en Colombia](https://archivo.contagioradio.com/acuerdo-de-paz-inicio-para-reparar-mujeres/)

Para **Fabiola Calvo Ocampo, directora de la Red Colombiana de Periodistas con Visión de Género** en este espacio se “reflexionará sobre experiencias de otros países en torno al tema de género, y en ese orden de ideas es importante saber también cómo se comportaron los medios en la transición de la guerra a la paz, en el tema de género”. Le puede interesar: [La participación de las mujeres: una clave para la construcción de paz con justicia social y ambiental](https://archivo.contagioradio.com/la-participacion-de-las-mujeres-una-clave-para-la-construccion-de-paz-con-justicia-social-y-ambiental/)

Según cifras entregadas por el Proyecto de Monitoreo Global de Medios de 2015, **las mujeres solamente aparecen en noticias el 29% de las veces** y del total de las noticias rara vez se pretende desafiar estereotipos de género.

Razón por la cual “algunos de los retos que afronta el periodismo en tiempos de paz, es lograr **asumir el enfoque de género y el papel de las mujeres en la construcción de un nuevo país”** manifestó Calvo. Le puede interesar: [Mujeres serán veedoras de la implementación de los Acuerdos](https://archivo.contagioradio.com/mujeres-seran-veedoras-de-la-implementacion-de-los-acuerdos/)

Asegura Calvo que no solo esperarán la asistencia de medios de comunicación, sino de estudiantes de periodismo, instituciones universitarias y público en general, para poder debatir temas que son importantes en la coyuntura de diálogo actual.

“Este evento entregará una serie de recomendaciones para un mejor diálogo entre el decir y el hacer de la comunicación, reflexión sobre sinergias necesarias entre el Gobierno, los medios y las facultades de comunicación **para la cualificación del quehacer periodístico en pro de la paz y los derechos de las mujeres”** indicó Calvo.

Para la asistencia a este Seminario, es necesario que las personas interesadas se inscriban en la página web la Red Colombiana Periodista de Periodistas con Visión de Género.

![unnamed](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/unnamed.jpg){.alignnone .size-full .wp-image-39824 .aligncenter width="1078" height="694"}

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}

<div class="ssba ssba-wrap">

</div>
