Title: Mural en homenaje a la matriarca Ligia María Chaverra del Curvaradó
Date: 2016-08-08 09:43
Category: Comunidad, Nacional
Tags: comunidades, Curvarado, educacion
Slug: mural-a-la-matriarca-ligia-maria-chaverra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/mural-ligia-maria-chaverra6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Ago 5 2016 

En la **Zona Humanitaria Camelias es Tesoro,** se realizó una jornada cultural durante la semana de clases del Colegio de las Familias de los Consejos comunitarios de la cuenca de Curvaradó, Jiguamiandó, Pedeguita y Mansilla, La larga Tumaradó y Vigia del Curvaradó (AFLICOC), donde se juntaron las manos para hacer un homenaje a las enseñanzas, sabiduría, lucha y resistencia de la **Matriarca Ligia María  Chaverra,** mujer que ha entregado su vida a la defensa del  territorio, exigiendo igualdad de derechos para todas las personas sin distinción de color, raza o religión. Enseñando a niños y niñas, jóvenes, mujeres y hombres la importancia del territorio en la vida.

[![mural ligia maria chaverra2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/mural-ligia-maria-chaverra2.jpg){.aligncenter .size-full .wp-image-27510 width="808" height="449"}](https://archivo.contagioradio.com/mural-a-la-matriarca-ligia-maria-chaverra/mural-ligia-maria-chaverra2/)

En medio de las injusticias que se están viviendo en el territorio del Curvaradó, hacer una pausa para la alegría, la imaginación, el deporte, el compartir, la comida, la pintura resulta un fundamental para a vida digna. En esta experiencia se unieron las manos para seguir construyendo el futuro desde la resistencia, construyendo la paz desde y en los territorios.

#### **Por Comunicadores CONPAZ y profesores AFLICOC** 

\
