Title: Las exigencias al Icetex por parte del movimiento estudiantil
Date: 2019-12-12 14:32
Author: CtgAdm
Category: Educación, Movilización
Tags: estudiantes, ICETEX, Iván Duque, Movimiento estudiantil, UNEES
Slug: las-exigencias-al-icetex-por-parte-del-movimiento-estudiantil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/ICETEX-CREDITOS-e1473862095534.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

[[En Colombia, para muchos estudiantes que se gradúan de secundaria, **la educación superior es un lujo al que sólo pueden acceder gracias al Instituto Colombiano de Crédito Educativo y Estudios Técnicos en el Exterior (Icetex).** Es por esto que el movimiento estudiantil, a partir de varios representantes, busca mejorar las condiciones, tanto de la educación pública como de los estudiantes que acceden a la educación privada a través del Icetex.]]

[[Sin voluntad política por parte del Gobierno para mejorar las condiciones de los créditos educativos, y reformas a gran escala que podrían agravar aún más la situación —como la propuesta de entregar los dineros públicos del Estado a privados, a través del Holding Financiero propuesto por el Presidente Duque—, el movimiento estudiantil exige ciertas condiciones dentro de la mesa de diálogos con el Gobierno.]][[(Lea también: Holding financiero; la jugadita de Duque privatizando los dineros públicos)](https://archivo.contagioradio.com/holding-financiero-la-jugadita-de-duque-privatizando-los-dineros-publicos/)]

### [**La educación universitaria es un lujo en Colombia**] 

[Para Cristian Reyes, integrante de la Unión Nacional de Estudiantes de la Educación Superior (UNEES) y vocero de la mesa de negociación con el Gobierno, las propuestas desde el ejecutivo “no recogen los elementos exigidos en la discusión del Icetex, lo que demuestra que **existe una clara desconexión entre lo que estamos proponiendo y lo que el gobierno, en cabeza del Presidente, quiere hacer”.**][[(Lea también: Menos del 10% de quienes presentan el ICFES se gradúan de la Universidad)](https://archivo.contagioradio.com/menos-icfes-graduan-universidad/)]

[[Según cifras del Sistema Nacional de Información de la Educación Superior (SNIES), la cobertura en educación pasó de 27% a 51% entre 2013 y 2017. Sin embargo, la poca financiación hacia el sector público, los altos costos de las universidades privadas y la deserción estudiantil en aumento han cambiado el panorama los últimos dos años.]]

[[El principal problema que afronta el Icetex es su falta de independencia financiera. Como explica Reyes, “el Icetex funciona a partir de un préstamo con el Banco Mundial, entonces el que solicite un préstamo está directamente endeudado con el Banco mundial”. Para frenar esto, se propone “que el Estado, a través del Presupuesto General de la Nación, financie totalmente la institución”.]]

[En el 2005 se implementó una reforma al Icetex con la Ley 1002, donde la institución dejaba de recibir fondos directamente de la nación. “A partir de aquí su financiamiento se unió al del Banco Mundial. Este le presta bajo una serie de condiciones, como la rentabilidad que le tiene que devolver”. [[(Lea también: Créditos del ICETEX, deudas impagables para jóvenes del país)](https://archivo.contagioradio.com/creditos-del-icetex-deudas-impagables-para-jovenes-del-pais/) ]]

[Para el vocero de la UNEES, eso es preocupante, ya que "por ejemplo, en][**este momento en que el dólar está tan alto, sube la rentabilidad con el banco, y el préstamo termina siendo pagado tres veces más del valor que se solicitó al principio”. **]

[“Las universidades públicas no cuentan con la infraestructura para recibir el conjunto de la demanda estudiantil. Por ende, la tasa de intereses tiene que ser igual a cero, pagando lo que se prestó en igual medida” afirmó Reyes. ]

### [**Las exigencias de la mesa de diálogo al gobierno de Iván Duque**] 

[[Si bien la lucha es por la educación superior pública, digna y gratuita, los representantes en la mesa de diálogo son conscientes de que la cobertura que pueden brindar las instituciones públicas se queda corta frente a la demanda de estudiantes que existen hoy en el país. “Hasta que llegue ese momento —de una educación pública para todos—,  queremos brindar un alivio para una gran parte de la población que solicita estos préstamos”.]]

[Las exigencias ante el gobierno de Iván Duque se centran en tres ejes: **la tasa de cero (0) interés para los estudiantes, la entrada en vigencia de la “financiación contingente al ingreso” y una reestructuración de la junta directiva de la institución.** [[(Le puede interesar: "Pedí al ICETEX \$29 millones, ya he pagado 30 pero aún debo 29")](https://archivo.contagioradio.com/pedi-al-icetex-29-millones-ya-he-pagado-30-pero-aun-debo-29/)]]

### **Tasa de interés del cero por ciento**

[Respecto al primer eje, “existe un alto grado de cobranza que termina perjudicando al pueblo colombiano y esto no aparece en la agenda del gobierno”. Este tiene un precedente, la ley 1547  de 2002, que garantiza la tasa cero de interés para los jóvenes afiliados al Sisben de estratos 1,2 y 3. De esta forma, los estudiantes cobijados por esta ley pagarían al terminar sus estudios únicamente el monto del préstamo, más el Índice de Precios al Consumidor (IPC) anual. ]

[El Gobierno de Ivan Duque busca derogar esta ley echando para atrás las pocas garantías que tienen algunos de los estudiantes con menos recursos en el país, motivo por el que desde la UNEES y la mesa de diálogo se busca extender el beneficio del cero interés a la totalidad de estudiantes que soliciten créditos educativos y que pertenezcan a los estratos 1,2 y 3.]

### [Una financiación que no termine por aniquilar a los estudiantes] 

[Por otro lado, la llamada **“financiación contingente al ingreso”** es un modelo de financiación que afecta al individuo y libera de responsabilidad al Estado. Consiste en lo siguiente; una persona de escasos recursos busca financiar su educación y toma un crédito.  Para financiarlo, contrae una deuda con el Estado que será pagada, a manera de impuesto, cuando el estudiante se integre a la vida laboral. De esta forma se vuelve auto-sostenible, opera sin subsidios públicos y libera al Estado de responsabilidad presupuestal. ]

[Como explica Reyes, “una persona accede a un préstamo para su educación pero empieza a pagar cuando comience a trabajar con un sobrecosto. El problema es el alto grado de desempleo en los jóvenes, tardando 30, 40 años en pagar el total de la deuda”. ]

### [**Una junta directiva que tenga en cuenta a la academia **] 

[En última medida está la **reestructuración de la junta directiva del Icetex** como una medida de transparencia ante las múltiples amenazas de corrupción que se han presentado en los últimos años dentro de la institución. Al no existir una mirada académica dentro de la institución que rige los destinos de estudiantes en la educación superior, se termina por aplicar una mentalidad corporativa enfocada en el beneficio del capital, no en el beneficio social. ]

[“Nosotros queremos que existan estudiantes y profesores al interior de la junta directiva, ya que es la que toma las decisiones que nos afectan a todos. Esto permitiría que se solucionaran injusticias dentro de los procesos que se levantan, como problemáticas en los préstamos, cobros mal tramitados, etc” señaló Reyes.  ]

### [**Los préstamos con el Icetex sí podrían ser condonados**] 

[Aunque existen sectores que consideran una medida populista o imposible la condonación de las deudas existentes, para Reyes esta no es una opción descabellada. “Cuando el Icetex da un préstamo, lo hace sobre la base de una condonación específica que ya tiene. Por ejemplo, cuando el Gobierno sacó el programa Ser Pilo Paga mantuvo una parte del presupuesto para pagar el conjunto de las carreras de todas las personas que accedieron a ese préstamo aún cuando desertaran de las universidades”.  ]

[No obstante, el Gobierno **“no quiere consensuar con los grandes sectores que se movilizaron el año pasado,** **incluidos** **los estudiantes”.** Para el vocero, lo importante es llegar a un escenario de verdadera voluntad política, donde se presten otras formas de pago “al servicio de la ciudadanía”. ]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45516233" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_45516233_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
