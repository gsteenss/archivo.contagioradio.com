Title: 379 solicitudes de objeción de conciencia en el primer año de Ley de Reclutamiento
Date: 2018-11-09 15:27
Author: AdminContagio
Category: DDHH, Nacional
Tags: Foro, libreta militar, objeción de conciencia, reclutamiento, servicio militar
Slug: foro-objecion-de-conciencia
Status: published

###### [Foto: Foro la Objeción de Conciencia ] 

###### [8 Nov 2018] 

Luego de un año de aprobarse la ley que permitió a los jóvenes declarar objeción de conciencia para evitar ser reclutados por las Fuerzas Armadas de Colombia, organizaciones sociales convocan al foro: **"La objeción de Conciencia en el primer año de la Ley de Reclutamiento"**, en el que presentarán un informe sobre los avances y dificultades que ha tenido su implementación y se discutirá sobre la normativa.

En la **Ley 1861** "por la cual se reglamenta el servicio de reclutamiento, control de reservas y la movilización" expedida en 2017, se regulan los procesos para definir la situación militar de los colombianos, así como **las causales de exoneración para prestar el servicio obligatorio, entre ellas, la objeción de conciencia**. (Le puede interesar: ["Los cambios en el sistema de reclutamiento militar en Colombia"](https://archivo.contagioradio.com/militar-reclutamiento-colombia/))

Un año después de la aprobación de la legislación, la Acción Colectiva de Objetores y Objetoras de Conciencia (ACOOC), JUSTAPAZ, La Tulpa y la Articulación Antimilitarista, evidenciarán las dificultades de acceso a este derecho, entre ellas la dilación del proceso de objeción, detenciones arbitrarias y vulneración al reconocimiento como objetor. (Le puede interesar: ["1.294 jóvenes murieron prestando el servicio militar en los últimos 23 años"](https://archivo.contagioradio.com/1-294-jovenes-murieron-presentando-el-servicio-militar-en-los-ultimos-23-anos/))

Entre las cifras que se destacan en el informe, están las publicadas por el Comando de Reclutamiento según el cual, **hasta el 29 de octubre de 2018, 371 jóvenes han presentado su solicitud de objeción de conciencia**; siendo Cali y Bogotá las ciudades en las que se registran mayor cantidad de solicitudes y Montería y Barranquilla, los lugares en los que no se presenta ninguna petición.

El Foro se desarrollará el **22 de noviembre, en el auditorio Darío Ávila (Cra. 20\#63-62) de Bogotá a las 4:30 p.m.**, y contará con la participación de delegados de la Defensoría del Pueblo, la Personería de Bogotá, el Congreso de la República, el Ministerio de Defensa, organizaciones sociales y objetores de conciencia. (Le puede interesar: ["Así es el camino para graduarse sin libreta militar"](https://archivo.contagioradio.com/se-gradua-en-colombia-el-primer-objetor-de-conciencia-sin-libreta-militar/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
