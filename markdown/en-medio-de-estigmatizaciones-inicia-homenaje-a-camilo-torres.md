Title: En medio de estigmatizaciones y amenazas inicia homenaje a Camilo Torres
Date: 2016-02-12 13:54
Category: DDHH
Tags: camilo torres, Conmemoración 50 años Camilo Torres
Slug: en-medio-de-estigmatizaciones-inicia-homenaje-a-camilo-torres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Camilo-Vive.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter ] 

<iframe src="http://www.ivoox.com/player_ek_10415889_2_1.html?data=kpWhk5qcfJqhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncabijNLSxs7Tb8XZjMrg1s7LscLoyt%2FOxc7TssbnjM7by8jNpYzc0NLS0MbOqYzVjKjOz87Qs4yhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Leonardo Jaimes] 

###### [12 Feb 2016 ]

En el marco de las [[actividades programadas para la conmemoración](https://archivo.contagioradio.com/camilo-torres-ejemplo-de-lucha-para-latinoamerica/)] de los 50 años de la muerte del sacerdote, sociólogo y líder Camilo Torres, **organizaciones defensoras de derechos humanos han denunciado señalamientos y estigmatizaciones** contra quienes desde hace varios meses están al frente de estos actos conmemorativos.

Leonardo Jaimes, integrante del Equipo Jurídico Pueblos, asegura que pese a que las autoridades departamentales y municipales aprobaron la realización de estas actividades, desde el pasado domingo **efectivos militares y grupos paramilitares con presencia en la región han aseverado que la peregrinación a Patio Cemento es impulsada por el ELN**, lo que ha llevado a que se presione para que la jornada conmemorativa no se lleve a cabo.

Los señalamientos no son aislados, asegura Jaimes, quien agrega que deben situarse en el contexto de las declaraciones del Ministro de Defensa quien afirmó que **“no respondía por lo que podía suceder” en el marco de las actividades**, y de lo ocurrido en el reciente Consejo municipal del Carmen de Chucuri, en el que participaron cerca de 200 personas entre militares, autoridades y civiles armados y no armados, y en el que un **reconocido paramilitar aseveró que no iban a permitir estos actos de proselitismo guerrillero. **

Pese a las estigmatizaciones y el anuncio por parte del ELN de iniciar este domingo y hasta el próximo miércoles un paro armado, Leonardo señala que las organizaciones que llevan meses trabajando en la agenda de esta jornada, insisten en que **no van a renunciar a "hacer historia, a buscar la verdad y a rendir homenaje** a quienes han querido aportar a las transformaciones del país", como lo buscó el sacerdote Camilo Torres.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
