Title: Hay un plan coordinado para impedir nuestra participación política: FARC
Date: 2018-02-09 13:09
Category: Nacional, Política
Tags: campaña política, carrera electoral, FARC, partido politico
Slug: hay-un-plan-coordinado-para-impedir-participacion-politica-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/farc-2-e1518197319974.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [09 Feb 2018] 

El nuevo partido político, Fuerza Alternativa Revolucionaria del Común, manifestó en rueda de prensa su decisión de **suspender temporalmente la campaña política** en esta carrera electoral tras los ataques violentos que han sufrido sus candidatos en los departamentos de Caquetá, Quindío y Valle del Cauca.

De acuerdo con el comunicado que hicieron público, los hechos violentos han dejado **dos menores de edad heridos** y ha habido atentados contra las sedes del partido en diferentes lugares. Pablo Catatumbo, integrante del partido, indicó que esto “pone de manifiesto un plan coordinado dirigido a impedir la participación política de un partido legalmente constituido luego del Acuerdo de Paz”, sin embargo señalaron que no rechazan las protestas pero si la violencia de la que han sido objeto los candidatos del partido.

### **Hechos recuerdan pasado violento de Colombia** 

Catatumbo argumentó que estos hechos de violencia contra los integrantes del partido político **recuerdan las causas del conflicto** armado como lo fueron “la intolerancia y la exclusión política mezclada con la violencia partidista”.

A lo sucedido, “se agregan amenazas, señalamientos y toma de fotos a las casas de militantes de la FARC a la vez que abundan mensajes de textos en las redes sociales **incitando a la violencia** e imágenes en las que se puede identificar claramente a los responsables de estos hechos delictivos”. (Le puede interesar:["La respuesta de la FARC ante las agresiones en Pereira"](https://archivo.contagioradio.com/la-respuesta-de-la-farc-ante-las-agresiones-en-pereira/))

### **FARC pide garantías para el ejercicio político** 

<iframe src="https://co.ivoox.com/es/player_ek_23661152_2_1.html?data=k5ijmJaVeZOhhpywj5aUaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZClcozA0NjOxsaJdqSfqtPhx8zWpc_oxpDdw9fYrcXjjNXczoqnd4a1pdnWxdSPiqLGpJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con Carlos Antonio Losada, integrante de ese partido político, debe haber unas condiciones de seguridad **previstas por el Estado** que protejan la participación política “de todos los movimientos que hay en el país”. Por esto, les han pedido a las instituciones que se garanticen la actividad política a la vez que le hicieron un llamado a los diferentes sectores políticos para “lograr entendimientos en el ejercicio de la campaña electoral”.

Ante esto, han planteado que el escenario político debe ser uno donde haya espacio para “las propuestas y las ideas donde **se deje de lado las formas de agresión** al adversario”. Losada enfatizó en que el Estado y la Fiscalía deben tomar las medidas correspondientes para garantizar que el partido político pueda llevar a cabo su campaña sin ser víctima de hechos violentos.

### **FARC interpondrá demandas a agresores** 

Teniendo en cuenta las agresiones, los integrantes de las FARC aseguran tener distintas pruebas para entablar demandas penales contra quienes llevaron a cabo los hechos violentos de los días pasados. Manifestaron que, “los responsables de instigar al odio y a la segregación por razones políticas **deben acudir ante la justicia**”. (Le puede interesar:["FARC denuncia asesinato de 3 de sus integrantes en Nariño"](https://archivo.contagioradio.com/farc-afirma-posible-asesinato-de-3-integrantes-por-estructura-del-eln/))

<iframe src="https://co.ivoox.com/es/player_ek_23661089_2_1.html?data=k5ijmJaUfJqhhpywj5WWaZS1kZmah5yncZOhhpywj5WRaZi3jpWah5yncbfdxNnc1M7Fb7TVz8nW0NSJdqSfytPhx8zWpc_oxpDRx9GPtMLm1c7R0ZCqhbO3joqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Por su parte, Victoria Sandino, integrante del partido FARC, aseguró que las manifestaciones sociales son válidas en la medida en que se realicen de manera pacífica y que el partido político **no puede exponer a sus integrantes** a que “los maltraten y los atropellen y que ocurra algo terrible en este país”. Por esto, enfatizó en que la ciudadanía debe permitir una jornada electoral y de debate “en armonía, con respeto y con civilización para lograr un entendimiento en medio de la diferencia”.

### **La FARC y su camino a la reconciliación** 

Losada indicó que las víctimas han tenido una actitud generosa “que han entendido que estamos en un **proceso histórico** que demanda mucha grandeza por parte de los colombianos”. Por esto, argumentó que en los procesos donde la FARC ha hecho un reconocimiento de su responsabilidad, “nos hemos encontrado con la bendición de las víctimas y el ánimo que nos impulsa a seguir adelante”.

Dijo que en la coyuntura política que vive el país “se necesita tender puentes antes que profundizar la diferencias ”. Por ejemplo, con los hechos sucedidos en el Club El Nogal en 2003, manifestó que se han comprometido **con contar toda la verdad** a las víctimas. Además, pidieron que los demás sectores “sociales, políticos, económico y eclesiales que participaron del conflicto contribuyan también con la verdad”.

Sin embargo,  Losada hizo énfasis en que “los mismos sectores que hoy en día, desde posiciones intolerantes y tomándose la vocería de las víctimas, son aquellos que, en el Congreso de la República, **impidieron que las víctimas tuvieran representación** a través de las Circunscripciones Especiales de Paz”. Para la FARC, “son ellos los que han negado los derechos a las víctimas y en su nombre quieren convocar a la intolerancia”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
