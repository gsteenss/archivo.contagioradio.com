Title: Dos comuneros indígenas fueron asesinados en Cauca
Date: 2020-10-20 22:04
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Asesinato de indígenas, Cauca., Municipio de Caldono
Slug: dos-comuneros-indigenas-fueron-asesinados-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/indigenas-extermnio-onic.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Archivo Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este lunes, mientras la Minga se movilizaba exigiendo el cese de la violencia contra sus comunidades y sus territorios, fueron asesinados dos comuneros indígenas en la vereda Guaico Alizal del municipio de Caldono, Cauca.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/leonardonzalez/status/1318585571317587968","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/leonardonzalez/status/1318585571317587968

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Información suministrada por el Instituto de Estudios para el Desarrollo y la Paz  indica que** los comuneros, identificados como Avelino Ipia de 60 años y Héctor David Marín fueron atacados con arma de fuego**. (Le puede interesar: [Asesinan al líder Jhon Jairo Guzmán en Tarazá, Antioquia](https://archivo.contagioradio.com/asesinan-al-lider-jhon-jairo-guzman-en-taraza-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además el alcalde de Caldono, José Vicente Otero señaló, «estamos de luto por el vil asesinato de dos comuneros, habitantes de nuestro municipio. Ellos transitaban por el lugar, y fueron sorprendidos por criminales que los asesinaron». **«No puede ser posible que las familias caldoneñas sigan llorando sus muertos en estos tiempo de una supuesta paz y armonía territorial. Mi solidaridad con estas familias y rechazo total a estos actos de violencia que enlutan nuestra población».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante lo ocurrido, organismos judiciales se desplazaron al sitio para efectuar las labores de levantamiento de los cadáveres. En medio de las investigaciones, las Fuerzas Militares del territorio, indicaron que en el área hay presencia de disidencias de las Farc.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Horas después, las autoridades anunciaron que tropas del Ejército Nacional capturaron al sospechoso del asesinato en la vereda Las Delicias. El detenido fue identificado como Darío Yatacué Campo quien, según las autoridades, coincide con las características descritas por los testigos del asesinato. (Le puede interesar: [Mujer indígena Angelina Collo fue asesinada en Cauca](https://archivo.contagioradio.com/angelina-collo/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
