Title: Estatuto de Oposición primer paso en largo camino para la participación
Date: 2017-04-17 13:22
Category: Nacional, Paz
Tags: Estatuto de oposición, Jurisdicción Especial de Paz, Unión Patriótica
Slug: estatuto-de-oposicion-un-primer-paso-en-largo-camino-para-la-participacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/movilizacio-paz-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [17 Abr 2017] 

Durante esta semana se espera que se apruebe el Estatuto de Oposición en el Congreso, para la Unión Patriótica, partido exterminado durante la década de los 80 por ser de izquierda, si bien este es un paso hacía la búsqueda de una política diferentes, es importante **“que se den cambios más estructurales al interior de las instituciones”**.

Para Jaime Caycedo, integrante de la Unión Patriótica, una de las características más importantes de este proyecto es la **Procuraduría que se encargaría de velar de que se cumplan los derechos de las organizaciones que se declaren en oposición**, por lo tanto, esperan que en los debates, sobretodo en senado, el órgano no desaparezca. Le puede interesar: ["Piden reabrir el Registro Único de Víctimas"](https://archivo.contagioradio.com/piden-reabrir-el-registro-unico-de-victimas/)

“Esperamos que el Estatuto de la Oposición pueda ser uno de los instrumentos para reivindicar el espacio político que se le ha quitado, histórica y progresivamente, a los sectores alternativos, **pero no basta, necesitamos reformas políticas más profundas**” afirmó Caycedo.

Además, agrego que el Estado Colombiano está en deuda con las organizaciones de izquierda y el movimiento popular, **razón por la cual debe garantizarse la ampliación de un espacio político que permita una real participación democrática**. Le puede interesar: ["Amenazan de muerte a Huber Ballesteros líder de Marcha Patriótica"](https://archivo.contagioradio.com/amenazan-de-muerte-a-huber-ballesteros-lider-de-marcha-patriotica/)

### **La Unión Patriótica y su lucha contra la impunidad** 

El Consejo de Estado determinó que las investigaciones de los crímenes cometidos contra militantes de la Unión Patriótica **no tendrán fecha de vencimiento, debido a que hacen parte de un plan de exterminio, caracterizado por asesinatos y desapariciones**. Le puede interesar: ["Las víctimas dimos ejemplo de respeto y dignidad: Alejandra Gaviria" ](https://archivo.contagioradio.com/las-victimas-dimos-ejemplo-de-respeto-y-dignidad-alejandra-gaviria/)

El Alto Tribunal señaló que “**las víctimas podrán demandar a la Nación por sus acciones u omisiones, sin que exista una fecha límite para hacerlo**”, Jaime Caycedo, sobreviviente a este exterminio indicó que, juntos con esta herramienta que se abre para la búsqueda de la verdad, es importante que también se agilicen los procesos de la Jurisdicción Especial de Paz.

“**La impunidad ha sido un signo permanente, por eso la Jurisdicción Especial para la paz, también es un escenario que permitirá defender y acusar a los victimarios** que no solo hacen parte del Estado, sino del gran empresariado latifundista y paramilitar” recalcó Caycedo. Le puede interesar:["Gobierno debe garantizar seguridad para líderes y defensores de DDHH"](https://archivo.contagioradio.com/gobierno-debe-garantizar-seguridad-para-lideres-y-defensores-de-ddhh/)

<iframe id="audio_18182330" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18182330_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
