Title: La muerte de Elicerio Mendoza es la quinta en menos de 48 horas en San Vicente del Caguán
Date: 2019-12-04 17:07
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: asesinato, Caquetá, Defensores DDHH, Lider social, lideres sociales, San Vicente del Caguán
Slug: la-muerte-de-elicerio-mendoza-es-la-quinta-en-menos-de-48-horas-en-san-vicente-del-caguan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Líder-social-asesinado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter ] 

[La muerte de Elicerio Mendoza se suma a la de **cinco personas más que han sido asesinadas en San Vicente del Caguán** en las ultimas 48 horas, entre ellas la de un maestro de construcción y dos mecánicos. Comerciantes y juntas comunales rechazan las acciones de violencia que se están gestando en la parte rural y el casco urbano del municipio del departamento del Caquetá.]

[La noche del martes 2 de diciembre **fue asesinado el líder social y comunitario, Elicerio Mendoza**, en San Vicente del Caguán, Caquetá. El líder social, que se movilizaba en motocicleta junto a su esposa en el sector de Puente Arenoso, fue atacado por desconocidos que le dispararon en varias ocasiones, causándole la muerte. [(Lea también: Lider de San Vicente del Caguán denunció amenazas y exigió la protección que nunca llegó)](https://archivo.contagioradio.com/lider-de-san-vicente-del-caguan-denuncio-amenazas-y-exigio-la-proteccion-que-nunca-llego/)]

[Uno de los líderes de la organización «Víctimas, tierra y vida», quien prefiere permanecer en el anonimato, explica que "después de las elecciones, se ha encrudecido el conflicto. Todos —los habitantes de San Vicente del Caguán— nos preguntamos lo mismo, ¿por qué está pasando esto?".]

[Elicerio —en su labor como tesorero— era el presidente de la Junta de Acción Comunal  de la vereda Casas Grandes y tesorero del comité de la carretera Troncal-Guacamayo. Algunas personas señalan que esta podría ser la razón por la que se le habría asesinado, al intentar apoderarse del dinero que manejaba en el peaje de esta vía del país.]

### [**La reactivación de la violencia en San Vicente del Caguán**] 

[En el último semestre del año, la zona de San Vicente ha vivido una reactivación de la violencia y tienen presencia activa grupos al margen de la ley y el frente 40 de las disidencias de las FARC-EP. Según el líder de la organización, este tipo de violencia no se veía "hace siete u ocho meses".]

[Después del asesinato de Elicerio, la preocupación se apodera del municipio de San Vicente del Caguán y la fuerza publica no tiene respuestas. "Solamente se escucha que se están haciendo consejos de seguridad, que no dan un parte de seguridad ni de información. El pueblo sanvicentuno, tanto del área rural como del casco urbano, quiere escuchar de mano de las autoridades qué es lo que está pasando" afirmo el líder.]

[**Síguenos en Facebook:**]

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
