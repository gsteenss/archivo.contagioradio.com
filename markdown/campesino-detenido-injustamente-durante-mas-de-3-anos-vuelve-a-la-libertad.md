Title: Campesino detenido injustamente durante más de 3 años vuelve a la libertad
Date: 2016-08-04 16:10
Category: Comunidad, Nacional
Tags: Chocó, Falsos Positivos Judiciales, Fiscalía, Río Sucio
Slug: campesino-detenido-injustamente-durante-mas-de-3-anos-vuelve-a-la-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Preso-Politico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Macondo] 

###### [4 Ago 2016] 

En 2013 Jorge Córdoba Romaña, campesino afrodescendiente del Bajo Atrato chocoano, fue detenido y acusado de pertenecer a un grupo guerrillero en el marco de un proceso que estuvo lleno de irregularidades, falsos e incoherentes testimonios, constantes dilaciones y suspensiones de audiencias, según el abogado Manuel Garzón integrante de la Comisión de Justicia y Paz.

En principio, a Córdoba **lo obligaron a aceptar el delito de rebelión bajo la amenaza de permanecer hasta 9 años tras las rejas**, luego lo acusaron de haber cometido un homicidio, por lo que tuvo que enfrentar cerca de 40 meses de reclusión en la cárcel de Carepa, Antioquía.

De acuerdo con Manuel Garzón, el caso de Jorge Córdoba es **uno entre los cientos de falsos positivos judiciales que ocurren en diversas regiones de Colombia**, como una práctica sistemática que se da tanto en el campo como en la ciudad y en la que se amañan los procesos para condenar a quienes son inocentes, con el fin de que la Fiscalía pueda mostrar resultados.

Sin embargo, pese a los intentos de buscar que este hombre continúe en la cárcel por un crimen que nunca cometió, este martes, el Tribunal de Riosucio, Chocó, emitió la sentencia absolutoria en favor de la libertad inmediata del campesino y pese a que es probable que la Fiscalía apele la decisión, hay confianza porque para el abogado es evidente que el proceso estaba montado, como lo probaron los testimonios de **varios guerrilleros presos que aseguraron nunca haber tenido algún tipo de relación con el campesino**.

<iframe src="http://co.ivoox.com/es/player_ej_12442432_2_1.html?data=kpehlpeYd5Ohhpywj5WcaZS1lZWah5yncZOhhpywj5WRaZi3jpWah5ynca7Vz9rSzpCrpdPuhqigh6eXsoampJCw0dLNt8qZpJiSpJjSb8XZjK_i1dnNp8rVjN6YssbecYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
