Title: Hidroituango: Hay que "liberar el río" para evitar una catástrofe
Date: 2018-05-18 17:15
Category: Ambiente, DDHH
Tags: crisis en Hidroituango, emergencia hidroituango, Hidroituango, Movimiento Ríos Vivos
Slug: hidroituango-hay-que-liberar-el-rio-para-evitar-una-catastrofe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Vícrtimas-de-Hidroituango-e1463081947920.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [18 May 2018] 

La emergencia por el represamiento del Rio Cauca en Hidroituango continúa para las comunidades que habitan en la **zona baja y alta de la presa construida**. Frente a esa situación los integrantes de Ríos Vivos afirman que sería necesario que se libere el Río Cauca y que se cuente con las organizaciones de base para atender la emergencia que ya está tomando magnitudes catastróficas.

Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos en Antioquia, quienes constantemente han denunciado las irregularidades del proyecto Hidroituango, afirmó que aunque se pueda alcanzar la **meta que tiene EPM** para controlar el nivel del Río, ya hay dos problemas estructurales que afectarían el funcionamiento de la hidroeléctrica.

### **Hidroituango es una tragedia anunciada** 

Por una parte habló sobre la sedimientación generada por diversos derrumbes en las montañas que soportan toda la presión del río. Zuleta señaló que uno de los problemas es que la sedimentación se da en este tipo de proyectos después de varios años. Para ella y los campesinos que habitan la zona esto ya pasó sin que comenzaran a funcionar las máquinas y no van a poder funcionar como se había presupuestado.

Por otra parte, los derrumbes del pasado, en montañas que se encuentran conectadas con las que fueron afectadas para la construcción de la presa, pueden llegar a convertirse en **una amenaza** teniendo en cuenta que ya se han producido grietas en las vías que están en los municipios aguas arriba y que se están distantes del caudal del río.

Adicionalmente, los movimientos sociales denunciaron que **no se ha realizado un censo** de los damnificados, no hay ayuda humanitaria ni psicosocial y no se ha tenido en cuenta a las organizaciones para hacer labores de organización. Recordaron que se trata de una tragedia anunciada y pidieron que se deje que el río recupere su caudal natural.

Desde el Ríos Vivos, denunciaron que la magnitud de la tragedia se extiende **mucho más allá de lo presentado** por los medios de información y argumentan que no ha habido claridad por parte de las autoridades y Empresas Públicas de Medellín sobre el manejo de la emergencia y el futuro del proyecto. Además, indicaron que los saberes ancestrales de las comunidades “están siendo pisoteados” generando así más problemas de los que ya hay.

### **Esta es la situación aguas arriba de la presa** 

Isable Cristina Zuleta, integrante del Movimiento Ríos Vivos, aseguró que el peligro no es solamente para las comunidades aguas abajo que han sido evacuadas ante la amenaza de una nueva avalancha. Afirmó que quienes habitan en la parte superior de la presa han evidenciado **inundaciones por el represamiento del agua** y temen que sucedan nuevos derrumbes de las montañas que están interconectadas unas con otras.

En el municipio de Sabanalarga, que es el municipio donde se ubica la represa, aún no se tiene el dato de **cuántas personas han sido afectadas**. Sin embargo, más de 300 familias “se cansaron de dormir en el parque municipal por lo que solicitaron ayuda en las oficinas de EPM y en este momento están pernotando allí”. Afirmó que no han recibido ayuda humanitaria y las autoridades “han dicho que allí no hay ninguna población damnificada, sino que eran personas que estaban protestando por la obra”.

En Sabanalarga, “el agua ya llegó y el represamiento inundó las riberas del río”. Esto ha hecho que el puente que comunica al municipio con Peque y Buriticá **esté bajo el agua** y “dejó aisladas a las comunidades que se abastecían con el puente”. Esta emergencia “no ha sido atendida por las autoridades y han dicho que la gente no tenía que estar allí cuando esas tierras no son de EPM, están inundando tierras ajenas”. (Le puede interesar:["Video: Hidroituango, una bomba de tiempo"](https://archivo.contagioradio.com/video-hidroituango-una-bomba-de-tiempo/))

Esta misma situación se presenta en el municipio de Ituango, donde el alcalde “sale a los medios de comunicación a decir que allí no pasa nada y son más de 100 familias que ya completan 20 días sin ningún tipo de atención humanitaria de emergencia”. Afirmó que la vía que le permite la comunicación a Ituango con otros municipios **se está agrietando** debido a que la presión del río y la casa de máquinas fue construida sobre la montaña de ese municipio y de Briceño.

Esto ha generado una preocupación en las comunidades que temen quedar aislados y “se desabastezca el municipio”. Recordó que el **puente Pescadero,** que comunica a esos municipios con Medellín, “está inundado por el represamiento ilegal e irresponsable de EPM y porque la zona de la obra está en completa incertidumbre”.

Como estos municipios se encuentran más cerca del muro, **“el agua llegó mucho más rápido de los que llegó a Sabanalarga”**. Desde Ríos Vivos, calculan que los daminificados en la parte alta de la presa son más de 700 y “no todas están en los albergues”. Sin embargo, aseguraron que “es muy difícil hacer un cálculo de las personas afectadas porque sólo se pueden contar a quienes están organizados”.

### **Esta es la situación aguas abajo de la presa** 

Más complicada aún está la situación humanitaria para los municipios que se encuentran en la parte baja de la presa, donde ya se presentó una **avalancha el 12 de abril de 2018.** Según Zuleta, “hay más de 3 mil personas concentradas en el municipio de Valdivia y hay un caos total”. Denunció que los afectados están desinformados sobre lo que está ocurriendo y dijo que “hay desorganización por parte de los organismos de socorro”.

Adicionalmente afirmó que no se le ha permitido al Movimiento Ríos Vivos mantenerse organizado y “el mismo proceso social de base **puede ayudar a contener la angustia** colectiva”. Dijo que en Tarazá, que está al nivel del río, la evacuación es complicada debido a que no tiene una parte alta como Valdivia. Esto mismo sucede en Caucasia, donde el alcalde “ha hecho un llamado a la ciudadanía, pero de manera irresponsable le dice que se retiren de los sitios cercanos al río pero no les dice a dónde ir”.

Esta situación se agrava en la medida en que **“sólo caracterizó a la población de Valdivia”** pero más abajo no se tiene un reporte de los afectados como en el caso de Tarazá. En caso de que se presente una nueva avalancha, los daminificados de esta zona podrían superar los 3 mil y su evacuación es complicada.

La situación de avalancha se presenta en esta parte de la presa, teniendo en cuenta que aguas arriba **“el agua se devuelve”.** Sin embargo y teniendo en cuenta los saberes ancestrales de las comunidades, las avalanchas se agravan cuando agua arriba se presentan derrumbes de las montañas que siguen ocurriendo. (Le puede interesar: ["ANLA y EPM responsables por la emergencia de Hidroituango: Ríos Vivos"](https://archivo.contagioradio.com/anla-y-epm-son-responsables-por-la-emergencia-de-hidroituango-rios-vivos/))

Estos derrumbes se presentan no sólo en el sitio de la presa sino en todo el Cañón del Río Cauca donde las **montañas se están “desmoronando”**. Esto hace que el agua que sale hacia abajo “lleve lodo y sea muy densa”. Esto si bien disminuye la velocidad del agua, aumenta la peligrosidad de la avalancha que lleva material vegetal.

Con la cantidad de personas evacuando a un mismo lugar, “en Valdivia se han presentado robos, peleas entre los mismos daminificados producto de un **caos en la organización**”. Por esto recalcó la importancia de la participación de las organizaciones sociales que pueden contribuir con la organización de las personas.

Zuleta recordó que las personas “están muy asustadas porque en la salida de sus casas y no se entiende por parte de los mecanismos de Gestión del Riesgo, que una cosa es evacuar a una población urbana y **otra muy distinta es evacuar a una población rural**”. Esto ha generado una situación de angustia en las personas que dejaron atrás sus fincas y los animales que tenían.

### **EPM nunca escuchó a las comunidades** 

Las comunidades, en conjunto con el Movimiento Ríos Vivos, han venido denunciando que, en 10 años no han sido escuchados y **tampoco lo están escuchando en este momento** que se vive la tragedia. Zuleta indicó que “la gente tiene muchas cosas importantes que decir” pues fueron ellas las que alertaron sobre las consecuencias que tendría el derrumbe del pasado 12 de abril.

Afirmó que con el derrumbe se alertó de nuevos desprendimientos de las montañas que están conectadas entre sí. Es decir que los desprendimientos de las montañas en las zonas de la presa “tienen implicaciones en la **desestabilización** en la parte alta de la montaña”. Dijo que se ha asumido equivocadamente que “la montaña se mueve abajo, se va el agua y no pasa nada como si no estuvieran conectadas las montañas”.

Zuleta enfatizó en que ya ha habido **emergencias en el pasado** cuando se empezó a construir la hidroeléctrica. Afirmó que “cuando se desestabilizó la base de las montañas para construir las vías, las veredas como el Alto de Chirí sufrieron grietas y las casas quedaron dañadas”. Esto nunca lo creyó la empresa “hasta que una casa se partió por completo ahí si le creyeron a la gente y estamos hablando de una vereda en la parte alta del río porque las montañas están conectadas”.

Además, las comunidades han alertado que el río, cuando se presentan movimientos en las montañas o crecientes producto del clima, **empieza a retomar su cauce natural** en todo el Cañón. Ahora, para las comunidades “resulta indignante que digan que el desastre se produjo por la naturaleza cuando no se respetaron ni conocieron las dinámicas del Cañón y está ocasionando este caos”. (Le puede interesar: ["Hidroituango: una bomba de tiempo"](https://archivo.contagioradio.com/hidroituango-una-bomba-de-tiempo/))

### **Propuestas de Ríos Vivos para atender la emergencia** 

Debido a la situación que afrontan las comunidades, desde Ríos Vivos expresaron que es necesario que se tenga en cuenta a las **organizaciones de base** para atender la emergencia “porque tiene sus propias reglas y normas”. Han dicho que “en este momento de rabia con el Estado colombiano, la gente no le copia”. Por esto, “para que hay un mínimo orden es que por las mismas estructuras internas de pescadores, barequeros y mujeres se establezca la organización”.

Además, han pedido que las autoridades **se sienten a hablar con las organizaciones** para que puedan participar del puesto de mando unificado. Con esto buscan evitar que las decisiones sigan siendo tomadas “de una manera arbitraria por unas personas desde la ciudad sin tener en cuenta la realidad de las comunidades”.

Esto ayudaría a atender problemas como el psicosocial donde “han mandado psicólogos de EPM para atender a las personas desesperadas”. Zuleta recordó que **“la gente está molesta con la empresa** y no los quieren ver, por más psicólogos que sean, la gente se enoja”. Por esto, han pedido que haya albergues para las personas de las organizaciones de base para que puedan ayudar con sus propios saberes.

### **“Autoridades no pueden decir que no conocían la situación”** 

Desde hace 10 años, las organizaciones sociales han realizado movilizaciones y denuncias para dar a conocer los **riesgos que supone la construcción** de una obra de esta magnitud. Por esto, afirman que “las autoridades no pueden decir que no sabían lo que podía pasar”. Zuleta indicó que “pueden decir que EPM los engañó igual que a nosotros”.

Ríos Vivos pidió por medio de un decreto, acordado con la Gobernación de Antioquia, “un plan para la atención de la situación”. Sin embargo, el decreto duró más de un año en instalarse y “llevamos rogando que se **verifique la zona y los daños** y no han querido”. En repetidas ocasiones pidieron que no se represara el río y desde Ríos Vivos dicen tener las pruebas para demostrar las advertencias que hicieron.

Nuevamente hicieron la alerta de que las montañas **se están derrumbando** por lo que corre riego la vida de las personas que viven aguas abajo pero también hay riesgo para las comunidades aguas arriba. Esto, teniendo en cuenta que “el agua llegó a la falla en donde hubo un deslizamiento y el río está retornando a su curso”.

Ante esto, han pedido que “dejen **libre al río** porque no va a haber manera de contener el agua”. Para esto, Zuleta afirmó que es necesario que se “desmantelen el horror que han hecho y hoy la única salida ética es que dejen correr el río libre”. Esto implicaría que se desmonte el muro para que el río retorne a su cauce para “salvar vidas humanas”.

### **Víctimas continúan pidieron que se siga con la búsqueda de los desaparecidos** 

A pesar de la tragedia que viven las comunidades, han pedido que se realice la búsqueda de sus seres queridos que **fueron asesinados y lanzados al río** durante el conflicto armado. Zuleta afirmó que, “con represa o sin represa las familias tenemos derechos a continuar con la búsqueda”. (Le puede interesar:["Memoria y resistencia en el Cañón del Río Cauca"](https://archivo.contagioradio.com/memoria-y-resistencia-en-el-canon-del-rio-cauca/))

Recordó que ese derecho **no se lo pueden quitar** a pesar de que los sitios de enterramiento que habían encontrado estén hoy bajo el agua. Por esto van a seguir con las labores de búsqueda para exigir que se exhumen los cuerpos “con la responsabilidad del Estado que tiene abandonadas a las víctimas y nos dejó en esta revictimización tan profunda”.

<iframe id="audio_26057258" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26057258_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
