Title: ¿Qué hacer y qué decir como cristiano frente al paro?
Date: 2019-12-17 16:32
Author: A quien corresponde
Category: A quien corresponda, Opinion
Tags: cristianas, Cristianos, Paro Nacional, Religión
Slug: que-hacer-y-que-decir-como-cristiano-frente-al-paro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

No hagas a nadie lo que no quieres que te hagan.  
(Tobías 4, 15)

Bogotá, 8 diciembre 2019

*El amor no se impone por la fuerza, la autoridad o el poder.*  
*El mensaje de Jesucristo es armonía, verdad, justicia, y amor.*

 

Estimado

Hermano en la fe

##### Cristianas, cristianos, personas interesadas 

Cordial saludo,

Por estos días, abundan informaciones, insultos, descalificaciones y mensajes contradictorios por redes sociales en torno al paro (“un cristiano no puede participar en paro”, “las cacerolas son satánicas”, “el paro busca acabar con el país” o “el paro es de una minoría de vándalos contra la gente de bien”) que generan incertidumbres, dudas de fe, malos entendidos en personas buenas y bien intencionadas y las llevan a discusiones sin argumentos con familiares, amigos y miembros de Iglesias, por esto hago contigo esta reflexión creyente en torno a esta realidad y lo hago pasados unos días, para reflexionar mejor y aprender de la historia, sin pelearnos.

##### La convocatoria al paro generó, como es lógico, posiciones encontradas, a favor y en contra. 

Esto es normal y no debe generar descalificaciones, miedo o extrañeza; **tampoco sería un problema ser cristiano y estar a favor o en contra del paro,** lo que sí sería un problema, para cristianos y cristianas, es tomar decisiones o posiciones sin tener las razones de fondo para decir sí o para decir no. El proceso “normal” que una persona cristiana debería seguir, en este y en todos los casos, debería ser:

Analizar las razones a favor y en contra y luego comparar este análisis con mensajes centrales de la Palabra de Dios, del Evangelio como *la verdad, la justicia, los pobres y el amor, incluyendo el amor a los enemigos\**  o el rechazo a toda injusticia y luego decidir qué pensar, qué decir y qué hacer.

El paro del 21 fue convocado, según el comité de paro, como respuesta a un “malestar generalizado” con el gobierno por hechos y decisiones que afectan a la mayoría de colombianas y colombianos y que el gobierno se ha negado, a escuchar, a reconocer y actuar como presidente de todo el país, aunque haya sido el candidato de un partido.

#### **Entre las razones del “malestar” están:** 

-   El asesinatos de líderes sociales sin una reacción adecuada por el gobierno
-   El bombardeo de los niños en el Caquetá y el engaño al país sobre lo ocurrido
-   La reforma laboral presentada al congreso por el partido de gobierno reduciendo el salario de jóvenes y la contratación por horas, hecho negado reiteradamente a pesar de estar radicada en el Congreso
-   La reforma tributaria que rebaja impuestos a las grandes empresas y aumenta a la clase media y trabajadora
-   El incumpliendo de los compromisos asumidos en la campaña a la presidencia
-   El holding financiero, la reforma pensional y muchos otros hechos con profundas repercusiones negativas para la vida digna de las mayorías.

Según el gobierno (el presidente, sus ministros y directores de instituciones del estado) no había razones para el paro, se basaba en mentiras, era promovido por Venezuela, el Foro de San Pablo, el comunismo internacional y la extrema izquierda, desconociendo la inconformidad social. Por diversos medios descalificó el paro, desvirtuó sus razones, tomó medidas represivas contra los promotores del paro, que llamó “enemigos del gobierno”, infundió miedo asegurando que era violento.

##### El partido de gobierno generó una campaña “sucia” en su contra, difundió la idea que el país se hundiría en el caos y la violencia e invalidó totalmente el paro. 

En el debate público, a favor y en contra, por diversos medios y redes sociales se fueron aclarando las razones reales del paro y evidenciando las contradicciones del gobierno. Esto generó indignación y malestar en la gente afectada por las políticas del gobierno y conocedora de la realidad del país. Diversos medios recordaron la legitimación del derecho a manifestarse contra las políticas del gobierno, realizada por el actual gobierno y su partido en la promoción del paro del **1 abril del 2017**, con mensaje como los siguientes:

*“La marcha del 1 de abril es una marchad el pueblo, para el pueblo y por el pueblo”*, *“en la marcha debemos participar quienes creemos que el gobierno está conduciendo mal a Colombia”* (Iván Duque, hoy presidente); *“Voy a marchar para expresar que nuestro país no le pertenece ni al gobierno ni a los congresistas”*, *“Nuestros ministros de Hacienda se sienten orgullosos de crecimiento PIB del 4%. Yo les exigiría el 6% y baja el desempleo al 5%. Menos Benevolencia”* (Marta Lucía Ramírez, hoy vicepresidente); *“Felicitaciones a organizadores de la marcha por la dignidad de Colombia. Definitivamente, la calle es el verdadero escenario de la democracia”*, *“el gobierno incumple y luego descalifica la protesta”* (Carlos Holmes Trujillo, hoy ministro de defensa), *“La marcha es un coscorrón en el ejercicio de la democracia. Es una estación en el recorrido que corrija el rumbo de Colombia cuya democracia ha sido traicionada por un gobierno que debería renunciar”* (Senador Álvaro Uribe Vélez).

###### Le puede interesar: [Su candidatura pudo ser contradictoria con su fe](https://archivo.contagioradio.com/sucandidaturapudosercontradictoriaconsufe/) 

##### **¿Podría decir con autoridad alguien del gobierno o del Centro Democrático que las marchas no son legítimas después estos argumentas en favor de las marchas?** 

Estas contradicciones y doble moral indignaron a mucha gente, especialmente a los pobres. Como sabes, hay un sector político y religioso que solo ve y oye los mismos medios, por eso sigue repitiendo las mismas cosas. Esto no quiere decir que deben estar a favor del paro, pero sí que discutan los verdaderos problemas que lo originaron y lo están alimentando, además, como creyentes deben tener en cuenta las palabras de Jesús: **“Traten a los demás como quieren que ellos los traten a ustedes”** (Lucas 6,31).

Como sabes, las calles de todas las ciudades se llenaron de gente muy diversa, marchando pacíficamente y con múltiples reclamos, banderas, mensajes, expresiones artísticas. Al final de la tarde un grupo de “vándalos”, unos “encapuchados”, el **ESMAD** y la policía generaron desmanes en algunas ciudades. En la noche del 21, hubo algo inesperado y sorprendente, el cacerolazo generalizado en todos los sectores sociales, que puede interpretarse como expresión del malestar general que siente la sociedad con el actual gobierno. Y aunque circularon mensajes diciendo que era una “cosa satánica”, que son “imbéciles” e “ignorantes espirituales” a quienes participaron y apoyaron, que los cristianos no podían participar en  los cacerolazos, siguieron y a pesar de la represión el paro siguió.

##### Entre la gente que participa y apoya, de diversas maneras el paro, conozco personas con mucha sabiduría espiritual y humana 

Por eso creo que esas palabras reflejan bien a quién las pronunció, que no representa ni a todos los creyentes, ni a toda la oficialidad de las iglesias. Me imagino que te llegaron mensajes por la redes diciendo que habían un plan para que los “vándalos” entraran a conjuntos residenciales de estratos medios y altos (conjuntos muy bien protegidos, de difícil acceso) que generaron pánico, el 21 en Cali y el 22 en Bogotá. En los videos se vio gente organizada para defenderse de los vándalos que no llegaron, camiones de los que bajaban grupos pequeños de personas que corrían hacia las puertas los conjuntos pero no entraron (¿cómo explicar que en medio del toque de queda los camiones se movieran tranquilos?).

##### En pocos conjuntos de barrios populares se vieron “tropeles”. 

La noche del 22 estuve en un barrio de estrato 5 y fui testigo de los gritos, del miedo, las sirenas, unos disparos, un helicóptero volando bajo, desde las ventanas decían: “están por el caño”, “van por allá”, “pasaron por ese lado” pero nada en concreto. Lo que pasó lo entendí días después cuando repasé unos textos que hablaban de la guerra psicológica, si quieres te hablo de ello en otra carta. Por las mismas redes la gente fue desmontando esta estrategia de pánico, al tiempo que se multiplicaban los carcelazos en todos los estratos sociales, se desobedecía el toque de queda, se multiplicaba el descontento y aumentaba el número de protestas de diversas formas.

He estado en las marchas, he visto las diversas expresiones en ellas, he hablado con personas de diversas corrientes políticas y diversas iglesias, he leído escritos a favor y en contra, he escuchado  
las razones de la mayoría de los marchantes y no he visto nada anticristiano, nada que vaya en contra de los valores fundamentales del Evangelio, más aún, escuchando los reclamos de los  
marchantes, no he podido dejar de pensar en las palabras de Nuestro Señor Jesucristo:

*“Vengan ustedes, a quienes mi Padre ha bendecido; reciban su herencia, el reino preparado para ustedes desde la creación del mundo. Porque tuve hambre, y ustedes me dieron de comer; tuve sed, y me dieron de beber; fui forastero, y me dieron alojamiento; necesité ropa, y me vistieron; estuve enfermo, y me atendieron; estuve en la cárcel, y me visitaron. Y le contestarán los justos: “Señor, ¿cuándo te vimos hambriento y te alimentamos, o sediento y te dimos de beber? ¿Cuándo te vimos como forastero y te dimos alojamiento, o necesitado de ropa y te vestimos? ¿Cuándo te vimos enfermo o en la cárcel y te visitamos?” El Rey les responderá: “Les aseguro que todo lo que hicieron por uno de mis hermanos, aun por el más pequeño, lo hicieron por mí” (Mateo 25,34-40).*

Más aún, me atrevo a decir, que en muchos momentos de la historia, han sido instrumentos de Dios (sin que sus protagonistas crean en Él o lo sepan), aunque hayan generado problemas y conmociones sociales, porque  gracias a paros, marchas, protestas, desobediencias civiles y acciones no violentas activas se han liberado países como la India de Gandhi o se han conquistado los derechos humanos, la educación para las mayorías, el acceso de la mujer a la educación superior, jornadas laborales justas, salarios más dignos, la libertada de los esclavos, el voto de la mujer, la igualdad de derechos de los pobres, el respeto a los pueblos indígenas o ancestrales, la eliminación del apartheid (la separación de negros y blancos), la igualdad de la mujeres ante la ley y muchas otras conquistas como la libertad de expresión para que los defensores de derechos humanos, sociales, económicos, culturales, ambientales, de género y los opositores a los paros y protestas puedan expresarse sin ser desaparecidos, asesinados, censurados o despedidos como tantas veces ha ocurrido a través de la historia.

##### **¿No te parece que deberíamos pensar mejor las cosas, independientemente que estemos a favor o en contra del paro?** 

Fraternalmente, su hermano en la fe,  
P. Alberto Franco, CSsR, J&P  
francoalberto9@gmail.com

###### \*Las siguientes son algunas de las referencias bíblicas para los criterios nombrados: **verdad:** Juan 8,31-32; 1Juan 1,6;  Efesios 4,15. **Justicia:** Mateo 5, 6; 6,33; 1Pedro 3,4; Santiago 3,18. **Pobres:** Mateo 5,3; 19,21; 25,45; 1Juan 3,17. Amor: Juan 15,12; 1Juan 4, 16.20; Romanos 12,9; 1Corintios 16,14. **Amor a los enemigos:** Mateo 5,43-47. **Injusticia:** Mateo 23,25; Romanos 1,18; 1Juan 5,17; Sofonías 3,13. 
