Title: Departamento de Estado pide celeridad para aprobar ley estatutaria de la JEP
Date: 2019-04-10 19:55
Author: CtgAdm
Category: Nacional, Paz
Tags: Departamento de estado, JEP
Slug: departamento-de-estado-pide-celeridad-para-aprobar-ley-estatutaria-de-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/DEPARTAMENTO-DE-ESTADO.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/departamento-de-estado.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/departamento-de-estado-jep-e1554925957630.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/NuevoDocumento-2019-04-10.pdf" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/departamento-de-estado-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

A través de un [comunicado fechado el 15 de Marzo](https://www.state.gov/r/pa/prs/ps/2019/03/290425.htm) de 2019 que fue dado a conocer recientemente, el **Departamento de Estado de Estados Unidos** afirmó que respalda todos los esfuerzos del Estado colombiano para implementar el acuerdo de paz, incluyendo la Jurisdicción Especial de Paz, y reafirma que en materia de extradición la JEP tiene autonomía desde 2016 y hacen un llamado a que se ponga en marcha la ley estatutaria de esa jurisdicción.

Según el comunicado, es necesario que la Jurisdicción Especial de Paz se implemente de manera pronta para que los derechos de las víctimas y el derecho a la paz sea respetado y promovido por todos. Agregan que para que esto sea una realidad todos los actores del conflicto, incluyendo los actores del Estado que sean responsables de violaciones de Derechos Humanos tengan el mismo tratamiento.

\[caption id="attachment\_64678" align="alignnone" width="500"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/departamento-de-estado-1.jpg){.wp-image-64678 width="500" height="434"} Comunicado oficial\[/caption\]

**Traducción no oficial del comunicado: **

ESTADOS UNIDOS APOYAN EL CAMINO DE PAZ EN COLOMBIA 
--------------------------------------------------

Los Estados Unidos agradecen los esfuerzos del Estado Colombiano para asegurar la implementación de la JEP en conformidad con los Acuerdos de Paz del 2016 y la Constitución del País. Los Estados Unidos agradecen además los esfuerzos para que las personas que han cometidos estos crímenes antes del 2016, en violación del objetivo de promover la no-repetición, sean responsables por el lleno alcance de la ley y están sujetos a extradición según los acuerdos del 2016.  
Los Estados Unidos afirma nuevamente la importancia de que Colombia apruebe rápidamente la ley estatutaria de la JEP para asegurar un marco normativo que opere de manera efectiva e independiente.

Por mas de tres décadas, la cooperación entre nuestros gobiernos ha demostrado de ser uno de los mecanismos mas potente y efectivo para garantizar la ley, perseguir la justicia, proteger las victimas, promover la paz y la prosperidad y salvaguardar la democracia en Colombia. Un componente significativo de esta cooperación fue la extradición, que ha permitido que cientos y mas criminales enfrenten la justicia.

Los Estados Unidos han apoyado de manera fuerte el camino que está haciendo Colombia hacia la paz. Ellos agradecen además los esfuerzos para fortalecer la rendición de cuentas por los crímenes de guerra y las violaciones de derechos humanos y asegurar que los responsables reciben sentencias adecuadas de manera proporcional que sean FARC, paramilitares, agentes del estado incluyendo los militares en Colombia. Los Estados Unidos miran a la JEP como un importante mecanismo para la paz y la justicia en Colombia.
