Title: Crimen de Mario Calderón y Elsa Alvarado podría ser declarado de Lesa Humanidad
Date: 2016-05-19 13:11
Category: Judicial, Nacional
Tags: Elsa Alvarado, Mario Calderon
Slug: crimen-de-mario-calderon-y-elsa-alvarado-podria-ser-declarado-de-lesa-humanidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/els-ay-mario-calderon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Semana 

###### [19 May 2016] 

Hace 19 años se cometió  el asesinato de  Mario Calderón y Elsa Alvarado, defensores de derechos humanos e investigadores del CINEP, asesinados por un escuadrón paramilitar en su apartamento en Bogotá. Hasta el momento solo se tiene una captura y pese al dolor y el daño que se cometió en contra de los investigadores, el crimen **se encuentra en la impunidad.**

Sin embargo, el homicidio de los investigadores podría declararse como crimen de lesa humanidad, debido a que haría parte de **un modus operandi para atentar en contra de varías personas en Bogotá y **generar una política de exterminio frente a ciertos sectores vinculados a la defensa de los derechos humanos o asuntos humanitarios. En este plan se encontrarían crimenes como el de Jaime Garzón.

Según declaraciones de alias "Don Berna" el asesinato de Mario Calderon y Elsa Alvarado se habría ejecutado bajo la orientación del entonces coronel Plazas Acevedo, que en la actualidad se encuentra purgando una condena de 40 años, y [que además es investigado por ser el presunto autor intelectual del asesinato de Jaime Garzón.](https://archivo.contagioradio.com/los-generales-y-militares-involucrados-en-asesinato-de-jaime-garzon/)

La persona condenada por ser el autor material  del asesinato de los investigadores, es Juan Carlos Jaramillo alias "El Colorado", paramilitar que se vinculaba a acciones en Medellín, los otro dos presuntos autores materiales quedaron en libertad por duda probatoria. Sin embargo 19 años después, **aún no se conocen  los autores intelectuales de este crimen.**

De acuerdo con Sebastian Escobar, abogado de la Comisión Colombiana de Juristas que representa a las víctimas del homicidio, es importante que en el marco de jurisdicción especial para la paz que se gestó como parte de los acuerdos de la Habana y en donde podrían participarán las Fuerzas Militares, se exponga este caso emblemático y se sepa la verdad **sobre la implicación de militares en este hecho.**"Para nosotros es fundamental que se comprenda que detrás de estos crímenes hay toda una mano oscura que vincula a las Fuerzas militares" asevera el abogado.

<iframe src="http://co.ivoox.com/es/player_ej_11590625_2_1.html?data=kpaim5Wadpahhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncbTZw8bg1s6Jh5SZopbbjarXp9DWwteYj5Cnh6uhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
