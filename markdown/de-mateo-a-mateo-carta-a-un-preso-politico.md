Title: De Mateo a Mateo: Carta a un preso político
Date: 2017-03-27 06:00
Category: Opinion
Tags: Mateo Gutierrez, presos político, sociologia
Slug: de-mateo-a-mateo-carta-a-un-preso-politico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/mateo-cordoba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mateo Gutiérrez 

#### [**Por: Mateo Córdoba Cárdenas**] 

###### 27 Mar 2017 

No te voy a mentir. A veces se hace difícil mantener el ritmo y el vigor de las arengas, no porque falten motivos, sino porque suelen sobrarnos los espantos. Nos quieren lejos, eso está claro, pero de aquí solo nos vamos contigo libre. Pronto, seguro.

Te escribo de sociólogo a sociólogo, de hijo a hijo, de Mateo a Mateo. Te escribo porque desde afuera me angustia la idea de que creas que estamos perdiendo el talante o que nos estamos acostumbrando a tu ausencia. Eso han esperado siempre, que hagamos del terrorismo estatal y de la persecución una experiencia reguladora de nuestras militancias. Nos quieren asustadas mientras invocan la paz y hacen la guerra. Pero puedes estar tranquilo al preguntarte sobre el alcance y el brío de las movilizaciones que se están dando en tu nombre. Dejarte solo no es una opción, renunciar a tu liberación no es una opción.

Supongo que en el presidio el ritmo de los días se hace insoportable. Tocayo, aquí afuera la lucha no cesa, hoy más que nunca despreciamos la molicie de la academia y bramamos por tu inocencia. Eso sí, teniendo claro que, como hombre libre que eres, las rejas se quedan cortas si lo que el Estado espera es desmoralizarte. No deja de ser evidente lo endebles que se vuelven las paredes de los juzgados cuando te nombramos junto a la palabra libertad. Ellos, los perros del Estado, quienes te tienen allí adentro, saben que aquí afuera hay dignidad, cariño y rabia en una sola consigna, esa que recuerda que no estamos todas, que faltas tú.

Sabemos que los móviles de tu captura son políticos. Esa necedad de vivir sin tener precio, como diría el gran Silvio, es la que nos pone bajo sospecha y la que hoy te tiene tras las rejas. El montaje judicial es el arma que usan esos próceres que se niegan a aceptar que van de salida, que temen ver que las luchas del pueblo están pariendo una nueva Colombia. He allí la elocuencia de tu encarcelamiento, es el Estado que hizo la guerra dando sus últimos berrinches ante el inapelable advenimiento de la paz que hemos luchado.

Muchas, sin conocerte, hemos entendido los motivos de tu aplomo y determinación para acompañar las resistencias que se forjan en nuestro país por una democracia que viva por y para el pueblo. Esos motivos, sin duda, han de ser doña Aracely y don Omar. Ellos jornada a jornada nos dan lecciones de firmeza y ternura. El rostro de tus padres es el fiel testimonio de este camino que empezó el día de tu detención. Se mantienen fuertes, serenos, expectantes a que el Estado les devuelva a su adoración. Su fortaleza no se envilece por las lágrimas que han dejado escapar. Al contrario, nos recuerdan que porque sentimos es que luchamos.

Sé fuerte y contagia de vitalidad a quienes llevan años encerradas por pensar distinto. Diles que no las olvidamos, que no habrá descanso hasta que sus voces se encuentren de este lado de los barrotes. Su lucha no ha sido en vano, no claudicamos. ¡Arriba las que luchan!

A ti, Mateo, te regalo un verso de una de mis canciones favoritas:

**“No te rindas, por favor no cedas, porque mañana se abrirán las grandes alamedas”**
