Title: En libertad 8 estudiantes de la UPTC detenidos el pasado miercoles
Date: 2015-09-25 17:25
Category: Educación, Nacional
Tags: Capturas de jóvenes, Protestas en la UPTC, Tunja, UPTC
Slug: en-libertad-8-estudiantes-de-la-uptc-detenidos-esta-semana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/uptc_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eltiempo 

###### <iframe src="http://www.ivoox.com/player_ek_8616514_2_1.html?data=mZuemJqVeI6ZmKiakpqJd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Bfzc7Px9fYpcWfmZDS1dnZqMrVz9nS1ZDIqYzgwpDCsrmnb8XZ1crby8nTt4zZ1NnOjdjJsY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>[Javier Rojas, Comité Permanente de DDHH] 

###### [25 Sep 2015] 

Los estudiantes de la **Universidad Pedagógica y Tecnológica de Colombia, UPTC**, con sede en Tunja fueron dejados en libertad la tarde de este viernes. En la audiencia el juzgado de control de garantías definió que los estudiantes no representan un peligro para la sociedad por lo tanto **la petición de medida de aseguramiento de la fiscalía no procedió en este caso.**

Javier Rojas, director del **Comité Permanente de DDHH** de esa ciudad, manifestó que aunque la libertad representa una buena noticia para los estudiantes y sus familias, el proceso judicial continúa y es necesario que se demuestre la inocencia de los jóvenes de los cargos que se les imputan.

Rojas recuerda también que es preocupante que se sigan presentando este tipo de judicializaciones, que no solamente **afectan el buen nombre de las personas implicadas, sino que pone en evidencia que hay una persecución judicial contra el movimiento social.** Hace unos días fue la detención de los jóvenes en Bogotá, luego la de Feliciano Valencia en el Cauca y ahora los estudiantes de la UPTC, señala.

El pasado miércoles 8 estudiantes de la Universidad Pedagógica y Tecnológica de Tunja en Boyacá, fueron capturados por civiles acompañados de agentes policiales, quienes les indicaron que los acusaban por fabricación, tráfico y porte de armas de uso privativo de las Fuerzas Armadas, fabricación de explosivos y concierto para delinquir.

Vea también [detención de 8 estudiantes de la UPTC podrías ser un montaje judicial](https://archivo.contagioradio.com/detencion-de-8-estudiantes-de-la-uptc-podria-ser-montaje-judicial-cpdh/).
