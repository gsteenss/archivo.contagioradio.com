Title: Sindicatos en Colombia votarán Si en el plebiscito por la paz
Date: 2016-08-17 09:27
Category: Nacional, Paz
Tags: CUT, plebiscito por la paz, SIndicatos, USO
Slug: sindicatos-en-colombia-votaran-si-en-el-plebiscito-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Captura-de-pantalla-2016-03-17-a-las-11.10.22-a.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: cedins] 

###### [17 Ago 2016] 

La Unión Sindical Obrera, uno de los principales sindicatos de la industria del petróleo en Colombia reafirmó que están comprometidos por la paz de Colombia y por ello anunciaron que votarán por el Si en el plebiscito por la paz aprobado por la Corte Constitucional y además reiteraron su llamado a que se abra una mesa pública de **conversaciones con el ELN e insistieron en la necesidad de abrir un escenario de conversaciones con la guerrilla del EPL.**

En un comunicado público la USO reiteró que es necesario que se cierren más de 52 años de guerra en Colombia y convoca a los ciudadanos a acudir a las urnas para refrendar los acuerdos de la Habana, como **fruto de 3 años de negociaciones entre el gobierno y las FARC**. El sindicato afirma que  *“votaremos por el [SÍ en el plebiscito](https://archivo.contagioradio.com/la-mejor-herramienta-para-votar-si-al-plebiscito-es-la-alegria-angela-maria-robledo/) para que se le ponga fin al conflicto armado e iniciemos la construcción de paz con justicia social.”*

Como uno de los argumentos de la decisión la USO afirma que [luego de la firma de los acuerdos habrán otras condiciones de exigibilidad de derechos](https://archivo.contagioradio.com/cuatro-millones-quinientos-mil-deberan-votar-si-en-plebiscito-para-la-paz/) en las que la ausencia del conflicto armado dará espacio a esas otras reivindicaciones, “*En el post acuerdo habrán mejores condiciones para continuar en la lucha popular y social por alcanzar las transformaciones que tanto anhelamos y conquistar la justicia social que tanto demandamos*”

También agregaron que con el voto por el SI en el plebiscito no renunciarán a otras exigencias como la **no privatización de ECOPETROL y la construcción de la Ley Orgánica de Hidrocarburos** en la que se garantice que la explotación de los recursos naturales sea responsable con el ambiente y con las necesidades de inversión social en Colombia.

Por su parte la Central Unitaria de Trabajadores, CUT, desde su congreso realizado el mes pasado, reiteró su respaldo al proceso de conversaciones de paz y anunció que este 17 de Agosto se realizará una jornada de pedagogía por la paz que incluirá manifestaciones públicas y mítines en los centros de trabajo, anunciaron **también la publicación en medios de información de su intención de apoyar el plebiscito** y el lanzamiento de una jornada de foros y talleres en las principales ciudades.

<iframe src="http://co.ivoox.com/es/player_ej_12575778_2_1.html?data=kpeimZqbe5mhhpywj5aUaZS1k5yah5yncZOhhpywj5WRaZi3jpWah5yncafVw87cjabWrcLnhpewjai5mIyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
