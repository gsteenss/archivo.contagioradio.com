Title: Diálogo por la Verdad: un alto a la violencia en Córdoba
Date: 2019-09-19 18:29
Author: CtgAdm
Category: Paz
Tags: asesinato de líderes sociales, comision de la verdad, cordoba, Diálogos para la No Repetición
Slug: dialogo-por-la-verdad-un-alto-a-la-violencia-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/No-Repetición.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de la Verdad] 

Este 18 de septiembre, la Comisión por el Esclarecimiento de la Verdad llevó a cabo el Tercer Diálogo para la No Repetición en Montería, Córdoba, un escenario de encuentro en el que diferentes actores sociales e institucionales del departamento plantearon alternativas hacia la no repetición de la violencia, el asesinato de líderes y lideresas en la región y una adecuada implementación del Acuerdo de paz.

### ¿Por qué la Comisión de la Verdad llega hasta Montería?

**Desde 1986 hasta el 2018 se han documentado 223 casos de asesinatos contra líderes y lideresas sociales** en este departamento, situación a la que se suma un evidente incremento en el número de defensores y defensoras asesinados tras la firma del Acuerdo, documentándose seis asesinatos a lo largo de 2019. [(Lea también: Diálogo por la verdad de líderes sociales asesinados llega a Arauca)](https://archivo.contagioradio.com/dialogo-por-la-verdad-de-lideres-sociales-asesinados-llega-a-arauca/)

Dichos asesinatos están asociados al carácter estratégico de la corrupción, a los enfrentamientos entre grupos armados, a los riesgos que implica la implementación del Acuerdo, a la ausencia de la institucionalidad en la región, por lo que la Comisión de la Verdad decidió profundizar en las raíces de esta incremento en la violencia.  [(Le puede interesar: Con asesinato de María del Pilar Hurtado, paramilitares siguen cumpliendo amenazas en Córdoba](https://archivo.contagioradio.com/con-asesinato-de-maria-del-pilar-hurtado-paramilitares-siguen-cumpliendo-amenazas-en-cordoba/)

El evento contó con la participación de la comisionada Marta Ruíz y el presidente de la Comisión, el padre Francisco de Roux, los congresistas Iván Cepeda, Aída Avella, Victoria Sandino y Juanita Goebertus, el defensor de derechos Andrés Chica, Jeferson Domico, miembro del cabildo mayor del Alto Sinú, Luis Trejos investigador de la Universidad del Norte, Wilson Castañeda, director de Caribe Afirmativo y Victor Negrete, docente de la Universidad del Sinú, Rogeres Higuita campesino del Alto Sinú,  y Laura Ardila periodista de la Silla Vacía Caribe.

### ¿Qué sucede al Sur de Córdoba?

A lo largo del diálogo se pudo establecer que en el sur del Córdoba nunca el Estado ha negociado con la totalidad de los actores armados presentes en el territorio, lo que causa que los demás actores permanezcan activos en el territorio y que promueven **"una competencia armada por el territorio",** así lo señaló el investigador Trejos.

Tal idea fue complementada por la intervención de Andrés Chica quien afirma que con la salida de las FARC, llegó al territorio una "andanada de paramilitares" que hoy conforman cerca de cuatro grupos armados irregulares y que entre estos, según el defensor han asesinado a 32 personas entre 2016 y 2019. [(Lea también: Con Diálogos para la No Repetición, Comisión de la Verdad promoverá defensa de líderes sociales)](https://archivo.contagioradio.com/con-dialogos-para-la-no-repeticion-comision-de-la-verdad-promovera-defensa-de-lideres-sociales/)

También se señaló que pese a la implementación de Plan de Acción Oportuna de Prevención y Protección (PAO), el Estado no es consecuente con la política pública de desmantelamiento de grupos neoparamilitares,y que además de ello, "en Córdoba no basta con desarmar aparatos militares, la paz, implica la escucha y la comprensión", tal como lo señaló la comisionada Marta Ruíz.

### ¿Qué propuestas surgen del diálogo?

Durante este diálogo, se invitó a los panelistas a promover propuestas que tengan como objetivo proteger a los líderes sociales, concluyendo que es necesaria la defensa del Acuerdo de paz e implementarlo con la movilización y la participación de los ciudadanos y las comunidades, "no podemos seguir haciendo la paz por partes", afirmó el senador Iván Cepeda.

Por su parte, la periodista Laura Ardila señaló que es necesario "persistir en hacer historias que más que indignar complejicen y que busquen que la gente logre entender las historias que han ocurrido y hacer énfasis en el periodismo regional", esto con relación al rol de los medios en medio de la implementación del Acuerdo.

Finalmanente el padre de Roux pidió que se trabaje junto a la Comisión de la Verdad con el fin de comprender cómo se debe transformar la sociedad, pues si no hay reconocimiento y verdad de todas las partes que hicieron parte de las afectaciones a los diversos sectores del país, es muy difícil avanzar, **"quisiera invitarlos a que se sientan parte de la Comisión, no somos nadie para decirle una verdad al país, busquemos juntos una verdad, pongámosle un nombre a las cosas"**, concluyó Francisco de Roux.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
