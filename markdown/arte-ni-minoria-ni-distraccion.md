Title: Arte: Ni minoría ni distracción
Date: 2015-03-04 12:35
Author: CtgAdm
Category: Opinion, superandianda
Tags: arte, arte en colombia, educacion
Slug: arte-ni-minoria-ni-distraccion
Status: published

###### Foto: [chirretegolden.tk](http://chirretegolden.tk/) 

Por [**[Superandianda](https://archivo.contagioradio.com/superandianda/) - [~~@~~Superandianda](https://twitter.com/Superandianda)

La educación diseñada para los grandes mercados del sistema capital ha puesto a la enseñanza artística como un simple "hobby" que no deja rentabilidad en una sociedad que necesita crecer comercialmente con más labores técnicas que producciones creativas. El diseño pedagógico que se aplica  implementa técnicas que no generan ningún tipo de criterio político ni social, lo que obstaculiza avanzar hacia la construcción de la crítica, el análisis y la libertad de opinión. Debería ser preocupante en un país subdesarrollado, corrupto y violento como Colombia ( y cuando estamos a puertas de un post-conflicto armado) que las asignaturas artísticas ocupen el pobre espacio que se les da, dado que si hablamos de paz y progreso es la educación el arma principal para reestructurar a una sociedad.

¿Por qué los artistas, intelectuales y activistas sociales se han encasillado en minorías? contentarse con los pequeños espacios que se ganan le quita al arte el lugar que le corresponde en la academia, la falta de respeto por el autor artístico en su profesión y la carencia de profesionales que alfabeticen  y abran caminos a nuevos talentos que no se desarrollan en las escuelas son algunos de los muchos puntos que necesitaríamos trabajar si en realidad estuviéramos dispuestos a generar un cambio.

El pobre concepto de educación en Colombia se desarrolla en un sistema de competencias que no despiertan capacidades creadoras, de ahí que se disparen las carreras técnicas, tecnológicas y de la mano universidades de garaje. El arte  va para un espacio privilegiado mientras que estudiantes de bajos recursos prefieren la mano de obra empresarial.  El diseño aparte de ser obsoleto promueve la distinción de clases sociales, es decir, lo que llaman competencias no es más que la estratificación en el sistema educativo.

En una autentica democracia el artista no puede ser la distracción para mantener el sistema capital, al contrario, debe ser el arma para promover nuevos líderes, llamar a la creación, al liderazgo, forjar pensamientos politicos. Somos diseñados para ser empleados y no para liderar.  La revolución educativa que nos han prometido los gobiernos no es más que llenarnos de universidades privadas haciendo de la educación, como de la salud, un auténtico negocio que deja muy buenos dividendos y grandes afectados: enfermos e ignorantes.

Los grandes cambios hacia un verdadero desarrollo en los países han nacido a través del arte, las grandes palabras de paz han sido dichas con arte, la revolución del ser humano nace a partir de la libertad de expresión del autor. No se podría hablar de humanidad sin nombrar a los artistas porque son cuna de diferentes pensamientos y es su obra la manera para compartirlos e invitar a muchos otros a realizarlos.

Se nos hace urgente la alfabetización artística, la defensa de la universidad pública, respetar el oficio como autentica profesión, despertar a la creación, organizar brigadas con profesionales que quieran construir una nueva sociedad. Es urgente que se nos haga urgente no esperar ser llamados a repetir sino a construir.
