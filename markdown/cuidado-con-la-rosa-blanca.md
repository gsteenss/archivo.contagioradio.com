Title: Cuidado con la rosa blanca
Date: 2016-09-14 05:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: campañas de paz, paz, politica
Slug: cuidado-con-la-rosa-blanca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/esmad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Vanguardia 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

Definitivamente creo que la paz, como ese gran concepto teológico, filosófico, funciona ofreciendo los mismos elementos que ofrece la utopía; como ya diría Eduardo Galeano, “la utopía sirve para caminar” la paz sirve para eso, para perseguirla, para caminar en medio de tanto debate y lograr extraer el mejor elemento con tal de no matarnos, con tal de no hacernos daño, con tal de creer que la unanimidad es una cosa y la democracia es otra, con tal de ver que en ocasiones nuestra realidad puede ser una hermosa rosa blanca.

Por estos días, la esperanza, de una forma generalizada y masiva ronda nuestra sociedad, es un espíritu que ha otorgado de energía política a los colombianos y colombianas, que, si somos sinceros, hace mucho tiempo no andábamos tan politizados. La gente incluso está agotada de los posteos, de los trinos, de los memes, de los artículos, de los foros, de las columnas que hablan y hablan sobre la paz, sobre la esperanza, sobre el sí, sobre el no, en definitiva: sobre política coyuntural.

Parece que lo único que hacía falta era que nos dieran un motivo para que habláramos de política, pues hoy todos quieren o sienten deseos de opinar, de debatir, y por supuesto, un habitual sector de la sociedad está “mamado” de tanta charla sobre el proceso de paz, sobre el plebiscito etc. probablemente, ese mismo sector que le “mama” todo y que no ve más allá de la punta de la nariz.

En este marco, en ocasiones la propaganda ha superado la comprensión y hoy tenemos una bandada de loros, repitiendo cuanta idea propagandística resuena aquí o allá. ¿qué responder ante las falacias propagandísticas? ¿qué pensar ante la persistencia en la ceguera como catalizador del discurso político?.

Cuando unos atacan diciendo que “el país fue entregado a las Farc” vale la pena recordar apellidos tales como, Sarmiento, Lleras, Ardila Lülle, Santo Domingo entre otros, y al final, por qué no terminar concluyendo, que el país ya fue arrebatado o se encuentra en unas manos muy particulares. Además, si la fórmula de los sapos la aplicamos inversamente, es probable que a las Farc le cueste tremendamente comprender (tragarse) la locura que cometieron aceptando curules en un sistema con tantas falencias y tan sofisticado en su actividad corrupta.

Cuando atacan diciendo que “la reforma tributaria es para pagar sueldos a los terroristas” se me viene a la cabeza una pregunta elemental ¿acaso la guerra es gratis?, del mismo modo, podrían surgir preguntas por el escándalo de interbolsa, la salud, o el descaro absoluto de Reficar ¿acaso ese hueco, tronera, agujero negro, desdicha de la vida fiscal y prueba fehaciente del fracaso de nuestro modelo económico lo va a pagar el sector privado, el ministro, el presidente o el expresidente? Por supuesto que no. Peso sobre peso, lo pagaremos nosotros con la reforma tributaria, para eso nos van a llenar de billetes nuevos, para que paguemos con más estilo.

Cuando atacan diciendo que “los acuerdos no favorecerán a los colombianos” tiene un tinte tan egoísta dicha afirmación, que la pregunta que emerge es si por “colombianos” solo están considerando a los uribistas, o quizá a los habitantes de la hipotética mitad del departamento del Cauca que defiende Paloma Valencia. Porque el ser tan deterministas, suena a pura propaganda. Es válido preguntarse realmente a quiénes no les interesa que se den los acuerdos, que, si bien son tan colombianos como otros, no deberían utilizar el viejo discurso del nacionalismo o salir con camisetas de la selección a hacer política.

Cuando atacan diciendo que “nos volveremos una Cuba, una Venezuela” es un cuentico tan antiguo, sobre todo el tema de Cuba, que, aunque desespere un poco la cuestión por lo anacrónico del comentario, se podría concluir que Colombia tiene problemas peores, iguales y diferentes a los que tienen estas dos naciones o cualquier nación del mundo. Esa maña de comparar y siempre coger de cocheche los intentos de socialismo son típicos. Pues es mejor siempre señalar al otro, antes que afirmar con total tranquilidad, que, si allá llueve, por acá no va escampar, y que, si allá fracasaron proyectos políticos de décadas, pues aquí fracasa cada cuatro años desde hace 200 años el pésimo intento de una democracia. Todos los países tenemos problemas, así que, si salen con el temita de Cuba o Venezuela, llamemos al orden de la charla, concentrémonos en Colombia.

¿y la rosa blanca? si pensamos aún con mayor sentido crítico, creo que es totalmente justo que la emoción y la esperanza invadan la cotidianidad política de muchos colombianos, no obstante, la emoción, no debería ser la fórmula para asumir la realidad colombiana y terminar viendo en todo a una gran rosa blanca… es decir, comparto la emoción por la paz, pero esto no es un partido de fútbol, los movimientos sociales están contra la espada y la pared ante el incremento de la violencia estatal (Esmad) y paramilitar. Asumir este momento como una gran rosa blanca, podría constituir un paso en falso para esos anhelos de paz.

Los que confiamos en el sí, pero asumimos la postura de un viejito irritable y cautelosos con la lógica histórica y social del país, seguimos alertando para que se evalúe seriamente el significado de las condecoraciones que le dan al Esmad por matar manifestantes, la amenaza y muerte de líderes de base, las babas que se le están escurriendo a las multinacionales bajo el argumento que dice: “se fueron las Farc, ahora sí el territorio es nuestro”, seguimos alertando para comprender que las acciones de los paramilitares, con su inagotable violencia, rompen hasta la última fibra el tejido político del país, seguimos alertando para que comprendamos qué significado tiene reprimir una macha pro medio ambiente y contra el extractivismo en el putumayo con 1300 efectivos del Esmad, eso no significa mantener el orden, eso es un mensaje mucho más directo.

Para que la rosa blanca no nos deje ciegos, es importante lograr comprender que los gozosos están lejos; seguimos alertando porque se comprenda que, el mayor productor de violencia nunca ha sido ni fue la guerrilla, por el contrario, siempre lo ha sido, lo es y lo seguirá siendo la manera errada en que se ha implementado el modelo político, económico y social, un modelo que de ninguna manera fue negociado en La Habana y sigue establecido en Colombia. Cuidado con la rosa blanca.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
