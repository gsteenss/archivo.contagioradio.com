Title: Cese al fuego bilateral y definitivo avanza con paso firme en la Habana
Date: 2016-04-14 18:02
Category: Nacional, Paz
Tags: cese bilateral, Conversaciones de paz de la habana, ELN, FARC, Humberto de la Calle
Slug: cese-al-fuego-bilateral-y-definitivo-avanza-con-paso-firme-en-la-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/farc-paz-gobierno-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elpais.cr] 

###### [14 Abril 2016]

El comunicado conjunto 67 emitido este 14 de Abril y firmado por las delegaciones de paz del gobierno nacional y de las FARC trae un mensaje alentador puesto que afirma que las partes han logrado  “**aproximar las visiones sobre los términos del [cese al fuego](https://archivo.contagioradio.com/los-verdaderos-nudos-de-las-conversaciones-de-paz/)** y de hostilidades bilateral y definitivo, proceso de dejación de armas y garantías de seguridad.” Y anuncia que las conversaciones se [reanudarán el próximo 21 de abril.](https://archivo.contagioradio.com/las-conversaciones-de-paz-necesitan-decisiones-bilaterales-ivan-cepeda/)

Por su parte la delegación de paz de las FARC, emitió otro comunicado en que se da cuenta  ha afirmado que si dejarán las armas y que uno de sus objetivos es hacer política sin ellas y hacen énfasis en que **esas armas no deben ser usadas contra ellos o contra los ciudadanos** “Nuestro compromiso es hacer política sin armas, esperando que las que la sociedad ha confiado a los agentes del Estado no se vuelvan a utilizar contra su propio pueblo. **Solo pedimos un Nunca más**.”

Agregan las FARC que es necesario que se acabe el paramilitarismo y criticaron la postura del ministro de defensa que [se refiere a este fenómeno como un fantasma](https://archivo.contagioradio.com/es-fantasioso-asegurar-que-el-paramilitarismo-no-sigue-vigente/) “**¿quiénes están matando a los defensores de derechos humanos, a reclamantes de tierras y a líderes opositores?”**. Además  según el comunicado muchos de los actos atribuibles al paramilitarismo y que el gobierno endosa al “Clan Úsuga” “se han cometido “fuera de los alcances” de ese grupo.

La delegación de paz de las FARC agrega que debe darse un juego limpio en Colombia y se refieren concretamente a los “Panamá Papers” y a la pretensión de explotar petróleo en la sierra de la macarena por parte de la empresa HUPECOL. El comunicado finaliza con la afirmación de que ellos y ellas quieren dejar las armas pero no sus vidas “**queremos garantizarle al pueblo colombiano, que queremos dejar las armas, pero no nuestras vidas”**
