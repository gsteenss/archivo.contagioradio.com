Title: Enrique Petro, líder social de Curbaradó es amenazado por paramilitares de las AGC
Date: 2020-12-18 11:32
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: Amenazas, Curbaradó, Enrique Petro, lideres sociales
Slug: enrique-petro-lider-social-de-de-curbarado-es-amenazado-por-paramiliatres-de-las-agc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Enrique-Petro-lider-750x330-1-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Comisión de Justicia y Paz*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este **17 de diciembre la Comisión de Justicia y Paz, denunció el riesgo sobre la vida del líder social Enrique Petro, vocero de las comunidades de Curbaradó en Chocó**, y quien fue recientemente amenazado por Paramilitares de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC).

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/Justiciaypazcol/status/1339608067248418816","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Justiciaypazcol/status/1339608067248418816

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

Según la Comisión, este 16 de diciembre** tres personas identificadas como paramilitares de las AGC, ingresaron a la Zona de Biodiversidad El Paraíso**, **propiedad del líder Enrique Petro, **y en donde se constituye el Consejo local de Andalucía, Territorio Colectivo de Curbaradó. ([AGC en Curbaradó asesinan a hijo de líder social](https://archivo.contagioradio.com/agc-en-curbarado-asesinan-a-hijo-de-lider-social/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los hombres señalaron que se llevarían dos cabezas de ganado en una acción que según la ONG, conforma la acciones de control territorial de los grupos paramilitares de las AGC, mediante las cuales someten a la población por medio de amenazas, abusos sexuales, entre otras vulneraciones a los Derechos Humanos, ***"en medio, con la complicidad, tolerancia e inacción de la fuerza pública"***.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La organización defensora de DD.HH. indica que los objetivos estratégicos de esta acciones de control y sometimiento de la población **dan cabida a las operaciones extractivas mineras a gran escala en predios colectivos.** ([Asesinan al líder ambientalista Jaime Monge](https://archivo.contagioradio.com/asesinan-al-lider-ambientalista-jaime-monge/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta no es primera vez en la que el líder social **Enrique Petro es amenazado de muerte,** en su accionar de defensa de la vida, durante años Petro ha sido obligado a guardar silencio y a salir de su territorio de manera forzada para proteger su vida y al de sus seres queridos. ([Paramiliatres de las AGC presionan y amenazan a líder Enrique Petro](https://www.justiciaypazcolombia.com/paramiliatres-de-las-agc-presionan-y-amenazan-a-lider-enrique-petro/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Nada absolutamente, nada se ha hecho de manera estratégica y estructural a una situación que se inició hace 26 años con operaciones de desplazamiento/despojo entre otros de la familia Petro".*
>
> <cite>*Comisión de Justicia y Paz*</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->
