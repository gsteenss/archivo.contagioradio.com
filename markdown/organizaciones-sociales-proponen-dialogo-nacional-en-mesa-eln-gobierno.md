Title: Organizaciones sociales proponen diálogo nacional en mesa ELN-Gobierno
Date: 2016-10-11 15:11
Category: Nacional, Paz
Tags: proceso de paz eln
Slug: organizaciones-sociales-proponen-dialogo-nacional-en-mesa-eln-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/congresoELN.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [11 de Oct 2016]

La **Campaña Por una Paz Completa, el Congreso de los Pueblos** y el comité de impulso de la Mesa Social para la paz anunciaron su respaldo al inicio de la fase pública de diálogos entre el ELN y el gobierno Nacional, y presentaron formalmente **su propuesta para establecer el dialogo Nacional como mecanismo de participación ciudadana en las conversaciones.**

La propuesta se presentó al ELN y el gobierno, como parte del primer punto en la agenda, y **tendrá como propósito reunir todas las inquietudes e iniciativas de la ciudadanía**, desde la inclusión de diversas voces y posturas políticas, para llevarlas a la mesa que se desarrollará en Quito, esto con el fin de **colocar a la sociedad en una posición protagónica, decisoria, transformadora y vinculante, durante el proceso de paz.  **Lea también: ["Conozca el acuerdo de diálogo firmado entre el ELN y el Gobierno"](https://archivo.contagioradio.com/conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln/)

Sin embargo, aún no se han establecido cuáles serán las herramientas o escenarios metodológicos que tendrá este espacio para deliberar, según Diana Sánchez, vocera de la organización MINGA, primero las delegaciones de ambas partes, deberán  acordar de qué forma se dará esa participación, para luego construir los mecanismos en donde **“se conversé frente a una agenda que parta de los acumulados e inconformismos de la sociedad”.**

De acuerdo con el senador por el Polo Democrático, Alberto Castilla esta mesa social posibilita que no se imponga un pacto de élites y que sea **“la movilización social la que reclame y exija la construcción de paz desde todos los actores”**, además afirma que esta es una oportunidad para suplir los vacíos de la democracia, a través de escuchar e incluir a los sectores marginados que no se ven representados ni en el Estado, las organizaciones sociales o las insurgencias. Lea También: ["Comisión Étnica se ofrece como mediadora para reanudar diálogos con el ELN"](https://archivo.contagioradio.com/comision-etnica-se-ofrece-como-mediadora-para-reanudar-negociacion-gobierno-eln/)

Frente a la propuesta que se había rumorado de generar una mesa con dos procesos, Castilla afirmó que cada guerrilla, tanto las FARC-EP como el ELN, tienen ciertas características diferentes, sin embargo **ambos procesos podrían complementarse** debido a que “es la sociedad la que debe entender que oportunidades surgen para posibilitar cambios reales en el país”.

Las plataformas impulsadoras del gran diálogo Nacional y de una mesa social para la paz, afirmaron que la iniciativas es abierta, motivo por el cual le hacen un llamado a las agremiaciones, organizaciones sociales y la ciudadanía en general, **para que se sumen a este espacio.**

<iframe id="audio_13289108" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13289108_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
