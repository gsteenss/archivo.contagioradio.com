Title: No permitiremos una intervención militar en Venezuela:Bancada Alternativa
Date: 2019-02-25 15:13
Author: ContagioRadio
Category: Paz, Política
Tags: Donald Trump, guerra, Iván Duque, Venezuela
Slug: no-permitiremos-una-intervencion-militar-en-venezuelabancada-alternativa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Bancada-Alternativa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [25 Feb 20219] 

A través de una carta, diferentes congresistas del país le manifestaron a Michael Pence, vicepresidente de Estados Unidos, que no están dispuestos a permitir "**que Colombia sirva de plataforma o base para la intervención de las Fuerzas Militares Norteamericanas** en operaciones bélicas contra Venezuela" y pidieron una solución dialogada empujada desde el Grupo de Contacto Internacional.

Los senadores, integrantes de la Comisión segunda Constitucional del Senado, que tienen entre sus temas las relaciones exteriores del país, le mencionaron a Pence que previo al encuentro del presidente Duque con Trump en Estados Unidos, ya le habían enviado una carta que contó con el respaldo de cientos de organizaciones sociales, en las que pedían no hacer parte de ninguna intervención. No obstante el presidente Duque no atendió al llamado.

Además, recordaron que ese tipo de decisiones no pasan únicamente por la determinación presidencial, sino que necesitan ser avaladas por el Senado de la República, tal y como lo ordena el artículo 172 de la Constitución. (Le puede interesar: ["Ni en Colombia, ni en Venezuela, Bancada Alternativa dice no a la guerra"](https://archivo.contagioradio.com/ni-en-colombia-ni-en-venezuela-bancada-de-la-alternativa-dice-no-a-la-guerra/))

### **El Grupo de Lima** 

Luego de que se escuchara la voz de los cancilleres como el de Colombia, Holmes Trujillo, y Perú, Hugo de Zela,  la reunión del Grupo de Lima acordó no usar una intervención bélica en el Venezuela, pero si el aumento de presiones que logrén la salida del presidente Nicolás Maduro del poder.

Este hecho de acuerdo con el senador Sanguino, del Partido Verde, haría parte del cerco diplomático y la actitud hostil en contra de Venezuela. Sin embargo, **señaló que existe otra iniciativa que surge del Grupo de Contacto Internacional, liderado por Uruguay y México,** que buscan una transición pactada entre el gobierno y la oposición de ese país, que culmine en la realización de un nuevo escrutinio, vigilado internacionalmente.

<iframe id="audio_32860536" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32860536_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
