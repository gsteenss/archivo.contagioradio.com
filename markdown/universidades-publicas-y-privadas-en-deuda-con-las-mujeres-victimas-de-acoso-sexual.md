Title: Universidades públicas y privadas en deuda con las mujeres víctimas de acoso sexual
Date: 2018-04-27 16:54
Category: Movilización, Mujer
Tags: acoso sexual, Lizeth Sanabria, Universidad Nacional
Slug: universidades-publicas-y-privadas-en-deuda-con-las-mujeres-victimas-de-acoso-sexual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/22528473_342398839520045_8554622844251818332_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: No más acoso en la UN] 

###### [27 Abr 2018] 

Tras la denuncia de Lizeth Sanabria, sobre el acoso sexual del que fue víctima por parte del profesor Freddy Alberto Monroy, el debate sobre el papel de las universidades a la hora de prevenir y activar a tiempo los mecanismos **contra el acoso sexual vuelve a ser una prioridad en la protección de los derechos de las mujeres**.

Isabel Rincón, integrante del colectivo Género y Seguridad, que realiza acciones conjuntas con otras universidades para construir mecanismos contra el acoso sexual y prevenir estos hechos, afirmó que tanto **las universidades públicas como privadas tienen una deuda con las mujeres a la hora de protegerlas y denunciar este tipo de hechos**.

“Ya debería existir un protocolo para hacer denuncias sobre acoso sexual en todas las universidades” manifestó Rincón y expresó que en el caso específico de la Universidad Nacional ya existen protocolos para denunciar este tipo de hecho. Sin embargo, en este caso habría que analizar qué salió mal con este mecanismo para que llevara a Lizeth a tener que denunciar a través de un vídeo, la violencia de la que era víctima.

### **Lizeth un aliento para las víctimas de acoso sexual en las Universidades** 

Lizeth Sanabria es estudiante de maestría en Enseñanza de las Ciencias Exactas y Naturales de la Universidad Nacional denunció acoso sexual por parte del profesor Freddy Alberto Monroy, a través de un **vídeo que grabó cuando era víctima del docente. Este hecho ha generado tanto críticas como respaldo hacía la estudiante**.

Para Isabel Rincón, en una situación como estas lo primero que debe hacerse es apoyar a la persona que está haciendo la denuncia y no caer en una revictimización, como en la que actualmente se está cayendo con este caso, no solo de parte de quienes juzgan a la estudiante por su forma de reacción ante el acoso, sino también por el manejo que los medios de información le han dado. (Le puede interesar: ["En Colombia el acoso sexual es una conducta naturalizada e invisibilizada"](https://archivo.contagioradio.com/jorge-armando-otalora-no-representa-la-defensa-de-los-derechos-humanos/))

Frente al profesor Monroy, Rincón señaló que el acosador sexual no tiene un estereotipo con el cual pueda identificársele, "de las cosas que más me sorprenden cuando atiendo un caso es que la gente me dice que ese tipo no tiene cara de acosador" aseguró y agregó además que hay un prejuicio al creer que todos los acosadores son personas que físicamente no son atractiva de acuerdo a los cánones de belleza impuestos por la sociedad, **hacen parte de estrato bajos o no tienen niveles altos de educación**.

“Hay falsedades en reacciones a denuncias por acoso sexual, la gente dice que si el tipo es bonito no es acoso sino coqueteo, no es acoso si el tipo tiene dinero" y afirmaciones como estas **hacen que las denuncias no sean tomadas en cuenta, se deslegitime a la víctima y se caiga en una revictimización**.

### **¿Cómo construir un mecanismo seguro para la protección de las mujeres contra el acoso sexual?** 

En primera instancia Isabel Rincón señaló que el mecanismo debe basarse en formas de protección y atención inmediata para la víctima una vez encienda la alarma. Sin embargo, a la hora de la prevención se deben detectar riesgos y violencias a las que puedan estar expuestas las mujeres. (Le puede interesar: ["El acoso contra las mujeres en universidades, un flagelo que no cesa"](https://archivo.contagioradio.com/46181/))

En esa misma vía se deben generar acciones de difusión y sensibilización hacia los qué es y los tipos de acoso sexual que existen y de los que las mujeres podrían estar siendo víctimas. Así mismo deben existir mecanismo de reparación para la víctima de violencia sexual y garantías para la no repetición.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
