Title: 710 mil trabajadores domésticos tendrán derecho a prima
Date: 2016-06-17 14:14
Category: Mujer, Nacional
Tags: Angela Maria Robledo, Congreso de la República, trabajadores domésticos
Slug: 710-mil-trabajadores-domesticos-tendran-derecho-a-prima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/WhatsApp-Image-20160616-640x360.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Angela María Robledo 

###### [17 Jun 2016] 

Cerca de **710 mil trabajadores y trabajadoras del servicio doméstico, cuyo 95% son mujeres, serán los beneficiarios de la aprobación del proyecto de Ley** presentado por la representante Ángela María Robledo, a través del cual se buscaba que estos empleados informales tengan derecho a una prima de servicios.

De la mano del trabajo de la congresista junto a la Unión de Trabajadoras Afrodescendientes del Servicio Doméstic (UTRASD), la Escuela Nacional Sindical, la Fundación Bien Humano, la Mesa de Economía del Cuidado y Fescol, entre otras organizaciones, **se logró que por Ley se empiece a dar pasos importantes para lograr la formalización del trabajo,** junto a iniciativas como las que adelanta el gobierno en torno a el acceso a seguridad social.

Esta Ley contiene cinco artículos que reconocen “**el acceso en condiciones de universalidad al derecho prestacional de pago de prima de servicios para las trabajadoras y los trabajadores domésticos”**, además establece que "El empleador está obligado a pagar a su empleado o empleados, la prestación social denominada prima de servicios que corresponderá a 30 días de salario por año, el cual se reconocerá en dos pagos, así: la mitad máximo el 30 de junio y la otra mitad a más tardar los primeros 20 días de diciembre. Su reconocimiento se hará por todo el semestre trabajado o proporcionalmente al tiempo laborado", también, "se incluyen en esta prestación económica a los trabajadores del servicio doméstico, choferes de servicios familiar, trabajadores por días, o trabajadores de fincas y en general, a los trabajadores contemplados en el Título III del presente Código o a quienes cumplan con las condiciones de empleado dependiente”.

Es decir, que según la explicación de la representante Robledo, **el empleador deberá ahorrar diariamente alrededor de \$ 1.900 pesos diarios durante el año para reconocer el pago de la prima de su trabajador para cumplir con la Ley** que además crea una mesa de seguimiento a la implementación del Convenio 189 de la OIT con la finalidad de promover de manera concertada una política pública para el derecho al trabajo digno de ese sector doméstico. Así mismo, la Ley estipula medidas de pedagogía para los empleadores y los trabajadores, como los y las que se dedican a los hogares, el cuidado de niños, ancianos, enfermos.

“**Este sueño se ha hecho realidad y por eso damos las gracias. Esto es un salto para los derechos de las mujeres que se desempeñan en este trabajo**. Esperamos que el gobierno inspeccione esta ley y sancione a quienes no la cumplan”, expresó María Roa, vocera del Sindicato de las Mujeres Afro del servicio doméstico, quien aplaudió la noticia.

De acuerdo con el Departamento Nacional de Estadística, **DANE el valor agregado del trabajo doméstico significaría el 20.8% del PIB,** de manera que al reconocer ese tipo de trabajo se estaría aportando a la economía del país.

Una vez la iniciativa de sea firmada por el presidente Juan Manuel Santos, pasará a ser Ley de la República y, por lo tanto, deberá empezar a aplicarse in mediatamente.

<iframe src="http://co.ivoox.com/es/player_ej_11941368_2_1.html?data=kpamlpaXepmhhpywj5aZaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5yncaLiyMrZw5CxpY-fs9TPzsrIs4yhjLfS0tfJt8bi1cbb1sqPpYzgwpCwh6iXaaKlzsbfw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
