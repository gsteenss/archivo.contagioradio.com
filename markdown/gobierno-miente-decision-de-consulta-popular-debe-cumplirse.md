Title: Gobierno miente, decisión de Consulta Popular debe cumplirse
Date: 2017-03-28 15:42
Category: Ambiente, Nacional
Tags: Cajamarca, consulta popular, Manuel Rodríguez, ministro de Ambiente, Ministro de Minas, Rodrigo Negrete
Slug: gobierno-miente-decision-de-consulta-popular-debe-cumplirse
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RDS Colombi] 

###### [28 Mar. 2017] 

<iframe src="https://co.ivoox.com/es/player_ek_17823175_2_1.html?data=kpyllJiVe5ahhpywj5aaaZS1k5yah5yncZOhhpywj5WRaZi3jpWah5ynca7Vz9rSzpC2s8Xmhqigh6aoq9bZ25Cvx8jJttPVhpewjcrcb67dz87g1tfTb8XZjKbaxM7JstXZjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Como nada sorpresivas, pero **como una actitud arrogante y mentirosa calificó el exministro de Ambiente, Manuel Rodríguez Becerra, las declaraciones del actual ministro de Minas**, Germán Arce, en las que aseguraba que los resultados de la Consulta Popular de Cajamarca no tenían la capacidad de cambiar la ley.

“Antes de que el Gobierno hiciera cualquier intervención, hice un par de trinos diciendo que el Gobierno en alianza con el sector privado desconocerá el resultado de Cajamarca y **no me extraña porque este Gobierno con su ministro de Minas y del Medio Ambiente solo han desconocido hasta la fecha la voz de las comunidades**, para que no tenga ningún valor en el proceso de definir el destino de sus territorios” agregó Rodríguez.

Para Rodríguez, esa ha sido la historia que ha vivido Colombia a través de los años “se supone que todo va a cambiar con el Acuerdo de Paz. **El Gobierno en su retórica sobre el Acuerdo de Paz vive diciendo que la paz desde los territorios** y una forma es escuchar a las comunidades y dejar que ellos definan el destino de sus tierras”.

Agrega el exministro que desconocer la decisión de Cajamarca, en la que 6165 cajamarcunos le dijeron NO a la minería en sus territorios no es gratis “**el Gobierno se está echando la soga al cuello en el mediano plazo, porque en Colombia no se puede seguir desconociendo la voluntad popular** de las gentes (…) si el Gobierno insiste en eso, obviamente la clase política será remplazada”. Le puede interesar: [¡Sí se pudo!: Cajamarca le dijo NO a la minería en su territorio](https://archivo.contagioradio.com/si-se-pudo-pueblo-de-cajamarca/)

Además, dijo Rodríguez, que las declaraciones dadas por el Ministro Arce, son inexactas dado que **decir “que se puede hacer minería a gran escala en forma ambientalmente sostenible es una mentira enorme,** no es posible, porque lo primero que se hace es tumbar montañas enteras”.

Razón por la que el exministro recomienda al Gobierno Nacional y a sus ministros a sincerarse con la gente en Colombia **“deben decirles sí, ese tipo de minería tiene un enorme daño ambiental,** lo que pasa es que nos parece que esos daños ambientales se justifican porque el país va a recibir unos ingresos económicos”.

Por último, Rodríguez aclara que no es que no se deba hacer minería en el país, sino que se haga en lugares donde no afecte al ambiente ni a la población **“se debe ordenar adecuadamente el territorio de Colombia para hacer minería o petróleo.** La alternativa entonces, para el caso de Cajamarca es continuar lo que vienen haciendo hace años, ser una despensa alimenticia de Colombia”.

### **Cajamarca derrotó al Gobierno** 

Así lo aseveró el exministro Rodríguez ante el resultado de la consulta popular este domingo en Cajamarca **“yo creo que la derrota que le hizo la ciudadanía de Cajamarca al Gobierno y a la Anglo Gold** y a todos los sectores poderosísimos de Colombia que están de acuerdo con ese proyecto, **es fenomenal**”.

### **Decisión en la Consulta Popular es vinculante** 

Dentro del marco de la Constitución y la ley y en cumplimiento de la Jurisprudencia de las Altas Cortes, el pueblo de Cajamarca se pronunció este domingo y dijo NO a la minería en sus territorios, razón por la cual, **las aseveraciones del Ministro de Minas estarían por fuera del marco legal.**

<iframe src="https://co.ivoox.com/es/player_ek_17823459_2_1.html?data=kpyllJiYeZqhhpywj5adaZS1kZmah5yncZOhhpywj5WRaZi3jpWah5yncbPjxdfWydSPksbb08rhx4qWh4y4xsvS0NjTtoztjK_i1M7XuMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para el defensor del ambiente y jurista **Rodrigo Negrete** lo que busca el ministerio es “confundir y generar un poco de caos frente a una decisión que ya está tomada.** Los cajamarcunos deben estar tranquilos pues ya tomaron la decisión más importante**”. [** **Le puede interesar: ["Consultas populares son constitucionales y vinculantes": Dejusticia](https://archivo.contagioradio.com/38431/)]

Con el título minero, según las disposiciones, no se pueden adelantar acciones de exploración, ni de explotación sino se consiguen después los permisos ambientales y posteriormente la licencia ambiental.

“Si no hubiese habido la Consulta Popular, para que La Colosa se materializara como un proyecto viable tendrían que terminar las etapas de exploración (…) presentar estudios. **Entonces de todas maneras tener el titulo minero por sí solo no permite explotar, no te consolida un derecho.** No debemos olvidar que los permisos ambientales son policivos y son revocables porque hay intereses superiores” recalcó Negrete.

Por estas razones, **los resultados de la Consulta en Camajarca convierten al municipio en una zona de exclusión de la minería** y producto de esto Cajamarca queda excluida de la minería de manera total y los títulos mineros no podrán consolidarse.

Por último, el jurista aseguró que esta decisión que a través de las urnas tomó Cajamarca son obligatorias “**El Ministro de Minas no puede seguir diciendo que no aplica a los títulos** mineros, claro que aplica, porque ahí no hay una situación jurídica consolidada (…) y por cumplir la ley La Colosa tiene que irse de Cajamarca, no va”. Le puede interesar: [Consulta Popular en Cajamarca pondría fin a La Colosa](https://archivo.contagioradio.com/consulta-popular-en-cajamarca-pondria-fin-colosa/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [[bit.ly/1ICYhVU](http://bit.ly/1ICYhVU)]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-bit.ly1nvao4u-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio-bit.ly1icyhvu .p1}
