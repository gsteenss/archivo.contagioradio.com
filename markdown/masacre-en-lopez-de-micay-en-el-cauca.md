Title: Seis personas asesinadas y dos heridas en López de Micay, Cauca
Date: 2018-10-30 12:35
Author: AdminContagio
Category: DDHH, Nacional
Tags: Cauca, López de Micay, masacre
Slug: masacre-en-lopez-de-micay-en-el-cauca
Status: published

###### Foto: López de Micay 

###### 30 oct 2018 

Por medio de un comunicado la organización Cococauca reportó el **asesinato de seis personas, dos heridas y dos más desaparecidas en el caserío de San Antonio de Gurumendi** del Consejo Comunitario SANJOC, municipio de López de Micay, Cauca.

Según la información conocida, **el lunes 29 de octubre  a las 10 pm**, las víctimas son **tres hombres y tres mujeres** pertenecientes a la comunidad, quienes habrían sido asesinados con armas largas. Hasta el reporte se habían identificado 4 de ellos como: Erika Riascos Suarez, Ester Jovani Valencia, Junior Rentería Rentería y  Paola Hermann.

En los hechos resultaron heridos además, un hombre y una mujer identificados como **Neifer Samuel Riascos** quien recibió un disparo en la espalda y L**aura Riascos Suarez** herida en uno de sus brazos; mientras que dos personas más fueron reportadas como desaparecidas.

Según la información, el primer homicidio ocurrió cerca de las 6 la tarde, de quien dicen en la zona era integrante de uno de los grupos armados que se encuentran en tensión con el Ejército de la Liberación Nacional (ELN). **Horas después fueron asesinadas las otras personas cuando se encontraban en un billar del sector**.

Desde la organización Cococauca, rechazaron lo ocurrido e hicieron un llamado a la Fiscalía Nacional que se investiguen estos asesinatos y que se tomen las medidas jurídicas correspondientes, así como a las organizaciones nacionales e internacionales, defensoras de derechos humanos monitorear, visibilizar y denunciar los asesinatos para que el Gobierno tome medidas eficientes que protejan la vida en los territorios.

Información en desarrollo...
