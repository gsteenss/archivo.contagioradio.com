Title: En la Guajira viven con menos de un litro de agua al día
Date: 2017-06-27 16:28
Category: Ambiente, Nacional
Tags: Agua, Defensa del agua, Guajira
Slug: 42837-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Sequia-en-La-Guajira-e1498596197917.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Otra Cara] 

###### [27 Jun 2017]

[El 28 de marzo del presente año, organizaciones sociales y miembros de la comunidad de la Guajira,  le  hicieron  llegar a la primera dama, María Clemencia Rodríguez de Santos, una  carta  con el fin de que se abrieran las compuertas de la Represa de El Cercado del Río Ranchería. **Ellos y ellas solicitaron que se garantice el derecho al agua a la comunidad Wayuu y a la Guajira en general** .]

La Carta fue respondida el 15 de mayo por la primera dama quien remitió el caso al Viceministro de ambiente y al gobernador de la Guajira. Según ella, estos serían los encargados de solicitar que se abra la represa para hacer llegar el agua a la población. (Le puede interesar: "[Al menos 1 niño muere a diario por desnutrición y deshidratación en la Guajira](https://archivo.contagioradio.com/al-menos-1-nino-muere-a-diario-por-desnutricion-y-deshidratacion-en-la-guajira/)")

Tras no recibir una respuesta concreta, decidieron redactar un derecho de petición  exponiendo que “**desde el 2010 se creó esa represa con el objetivo de llevarle agua a nueve municipios de la Guajira**”. Sin embargo, según Olga Mendoza, miembro de la organización Kanuliaa Jieru, “esta represa se encuentra al servicio de los intereses ** **de la Multinacional de Carbón El Cerrejón y a los campos de terratenientes de la región”.

[Mendoza afirmó además que  “estas empresas gastan una cantidad enorme de agua diaria para realizar sus actividades. Si esta agua fuera llevada a las comunidades, podría abastecer a dos millones de personas en el departamento”.]

### **Comunidades de la Guajira viven en condiciones inhumanas** 

Actualmente y según la Organización Kanuliaa Jieru, **las comunidades de este departamento viven con menos de un litro de agua al día.** Según Olga Mendoza “esta situación es totalmente inhumana porque según la Organización Mundial de la Salud, un ser humano por lo menos necesita 50 litros de agua al día para vivir”. Este fenómeno afecta en este momento a cerca de 400  mil  personas en la Guajira, llevando a la desnutrición y muerte de más de 5 mil personas durante los últimos años,  provocando una crisis social, ambiental, económica y cultural. (Le puede interesar: "[Piden abrir compuertas de represa "El Cercado" en la Guajira](https://archivo.contagioradio.com/represa-el-cercado-en-la-guajira/)")

[Los habitantes de la Guajira con participación de organizaciones sociales han hecho una serie de denuncias y han solicitado medidas cautelares para los responsables **para evitar que la población se quede sin este recurso vital.** Además, han desarrollado manifestaciones en busca de que más personas se unan al movimiento. La Organización Kanuliaa Jieru manifestó que las personas pueden participar de este movimiento ingresando a la página  ]*[Devuélvannos el agua, Devuélvannos la vida.]*

<iframe id="audio_19522991" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19522991_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

######  
