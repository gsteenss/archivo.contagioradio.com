Title: "Muchos nos ven como una piedra en el zapato por defender el territorio" ACONC
Date: 2019-05-06 16:03
Author: CtgAdm
Category: Comunidad, Nacional
Tags: ACONC, amenazas contra líderes sociales, Cauca, Francia Márquez, Vícto Hugo Moreno
Slug: muchos-nos-ven-como-una-piedra-en-el-zapato-aconc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Líderes-aconc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @VictorMorenoMin/ Cauca Extremo] 

Después del atentado ocurrido el pasado 4 de mayo en Santander de Quilichao cuando varios hombres armados lanzaron una granada contra el lugar en el que se encontraban líderes y lideresas de la **Asociación de Consejos Comunitarios del Norte del Cauca  (ACONC)**, aún se desconocen los autores y las motivaciones para atentar contra quienes desde sus regiones continúan exigiendo el cumplimiento del capítulo étnico del Acuerdo de Paz, el  cumplimiento de los programas de desarrollo con enfoque territorial y por acabar con la minería ilegal.  
**  
Víctor Hugo Moreno, consejero mayor de la ACONC y uno de los líderes que estaba presente en el lugar,** señala que desde el viernes pasado venían trabajando en la finca La Trinidad -  un lugar simbólico para la comunidad por ser uno de los primeros espacios en ser  recuperados por las comunidades -  en la creación de rutas de un informe a la JEP y otro para  la Comisión de la Verdad sobre los acontecimientos que han ocasionado afectaciones a las poblaciones étnicas. [[(Lea también: Atentan contra Francia Má]rquez y otros líderes de comunidades negras del Norte del Cauca)](https://archivo.contagioradio.com/atentan-contra-lideresa-francia-marquez/)

Relata que a pesar de no tener en aquel instante una amenaza directa, las más recientes habían sido dirigidas a las organizaciones que participaron en la Minga del suroccidente, entre las que se encuentra **ACONC**, tema que hacía parte de la agenda del 4 de mayo previo a la reunión que sostendrán con el Gobierno el 8 de mayo, "no queremos señalar a nadie, queremos que se avance en las investigaciones y que se aclare rápidamente" señalando que los 36 consejos comunitarios que hacen parte de este proceso seguirán trabajando en la preparación del informe y en el trabajo concerniente a la Minga

**"Hay mucha gente en territorio que no está dispuesta a que la verdad se aclare"**

Víctor declaró que la comunidad seguirá fortaleciendo sus sistemas de protección colectiva y comunitaria a través de la guardia cimarrona, sin embargo aclaró que el "Gobierno no se puede bajar de la responsabilidad de las garantías de seguridad y de protección" agregando que no se trata de solo establecer medidas materiales sino de atacar directamente las causas de lo que está generando riesgos en el territorio.

El consejero mayor señaló que su apuesta, junto a la de líderes como **Francia Márquez, Carlos Rosero, Clemencia Carabali y Sofía Garzón**  siempre ha sido la de defender la vida y el cuidado de los ríos, "muchos nos verán como una piedra en el zapato pero lo que hacemos es proteger la vida en el territorio, lo hacemos por el país y para sostener a los ecosistemas, tenemos que seguir en pie de lucha", concluyó.

<iframe id="audio_35439551" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35439551_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
