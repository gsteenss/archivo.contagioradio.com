Title: Militares habrían iniciando incendio en vereda de Puerto Asís, Putumayo según los campesinos
Date: 2018-02-27 14:47
Category: DDHH, Nacional
Tags: campesinos, Fuerzas militares, Incendio
Slug: militares-habrian-iniciando-incendio-en-puerto-asis-putumayo-segun-los-campesinos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Zona-de-Reserva-Campesina-Perla-Amazónica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión Intereclesial de Justicia y Paz] 

###### [27 Feb 2018] 

Campesinos denuncian que militares habrían comenzado un incendio en una finca en la comunidad de Sevilla, en Puerto Asís, Putumayo, que ya dejaría hasta el momento 20 hectáreas quemadas, **una bodega en donde había elementos para realizar siembra y cortar árboles y daños en la vivienda del predio del campesino afectado**.

De acuerdo con Gregorio Rosales, presidente de la Junta de Acción Comunal de la vereda la Rosa, el incendio inició el pasado jueves 22 de febrero hacia las 12 del mediodía, luego de que soldados pertenecientes a la Brigada 27 de Selva, **ubicados en las plataformas petroleras de la empresa británica Amerisur**, ingresaran al predio de un campesino en la comunidad de Sevilla y quemaron una pequeña bodega, hecho que provocó que el fuego creciera sin que se pudiese controlar.

Este incendio ha afectado fuentes hídricas, la fauna y flora del territorio y el potrero que estaba en la finca contigua a donde inicio el incendio, sumada a esta situación se encuentran los demás los incendios que se han registrado en la Amazonía colombiana desde la semana pasada. (Le puede interesar: [" Casanare y 32 departamentos más en alerta roja por incendios forestales"](https://archivo.contagioradio.com/casanare-y-otros-32-departamentos-estan-en-alerta-roja-por-incendios/))

“Aquí lo único que se sospecha es que el causante de esto fueron los militares que pasaron por aquí, entonces esas **20 hectáreas consumidas entre potrero y rastrojo son las que fueron consumidas por el fuego**” manifestó Rosales y agregó que no conocen los posibles intereses que tengan esos integrantes del Ejército por iniciar el fuego.

De igual forma Gregorio Rosales señaló que la familia dueña de las hectáreas se encuentra bastante preocupada y triste por la pérdida de las tierras para cultivar y así mismo se encuentra la familia que perdió la bodega en donde comenzó el incendio.

<iframe id="audio_24112362" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24112362_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
