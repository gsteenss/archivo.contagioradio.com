Title: 4 mil personas en riesgo de desplazamiento por enfrentamientos en El Bagre, Antioquia
Date: 2016-01-18 13:08
Category: DDHH, Nacional
Tags: Antioquia, FARC, Paramilitarismo
Slug: 4-mil-personas-en-riesgo-de-desplazamiento-por-enfrentamientos-en-el-bagre-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/El-Bagre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.elbagre-antioquia.gov.co]

<iframe src="http://www.ivoox.com/player_ek_10113671_2_1.html?data=kpWek5iae5Khhpywj5aXaZS1lp6ah5yncZOhhpywj5WRaZi3jpWah5yncZWfzs7ZjdXJttTjz8bgjcrSb9PdxtjU0ZDIqYzYxtjdzsbepc7dxtPh0ZDUs9OfxtPT1MrSuMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [18 Enero 2015]

Más de 4.000 personas del corregimiento Puerto Claver, en Antioquia se encuentran en riesgo de desplazamiento debido a los enfrentamientos que se han presentado desde diciembre entre guerrillas, militares y paramilitares, generando el desplazamiento forzado de 580 personas que en su mayoría son niños y niñas, y otras 4 personas desaparecidas que ya cumplen 10 días, uno de ellos conodido como Jair de Jesús Suárez de aproximadamente 23 años.

“Hay una zozobra muy grande, porque no se entiende cómo estando la fuerza  pública a la entrada del corregimiento, se mete un grupo armado y no pasa nada… hay mucha tensión y miedo”, dice uno de los habitantes a Contagio Radio, quien prefiere no revelar su identidad por seguridad, pues asegura que quien hable podría ser asesinado.

Según reporta el poblador, en la noche del domingo un grupo de paramilitares incursionaron en el lugar, y el casco urbano del municipio se encuentra totalmente militarizado y paramilitarizado, por lo que la población del corregimiento está pensado en evacuar por completo la zona.

“Las comunidades no han podido retornar, se empieza a sentir la falta de alimentos, hace  falta  la presencia de la administración municipal, la secretaria de gobierno, personería y otras autoridades para que se recupere el control en la zona”, dice el habitante.

Las familias exigen garantías de seguridad para regresar a su territorio, así mismo solicitan ayuda  humanitaria urgente de parte del gobierno nacional, pues están necesitando alimentos, elementos de aseo y colchonetas.

En las 8 veredas de El Bagre, operan el ELN, las FARC, grupos paramilitares y el Batallón energético Numero 5 del Ejército Nacional. Los combates se agudizaron desde el pasado 8 de enero, día en que empezó el desplazamiento de las familias rurales que ahora se encuentran en una crisis humanitaria que se ha acrecentado con el pasar de los días y las pocas ayudas humanitarias que han recibido por parte del gobierno nacional.

**En contexto**

Cabe recodar, que desde hace varios años,  la empresa Mineros S.A, viene realizando actividades de extracción oro  en la mayor parte del departamento.
