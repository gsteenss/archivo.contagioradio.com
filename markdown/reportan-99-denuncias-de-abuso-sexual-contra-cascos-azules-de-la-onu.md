Title: Funcionarios de la ONU acusados de 99 denuncias de abuso sexual
Date: 2016-03-04 17:04
Category: El mundo, Nacional
Tags: Abuso sexual, cascos azules, ONU
Slug: reportan-99-denuncias-de-abuso-sexual-contra-cascos-azules-de-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Cascos-azules-e1457128961466.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Taringa 

###### [4 Mar 2016] 

De acuerdo con el informe anual del secretario general de la ONU, Ban Ki-moon, militares de las Fuerzas de Paz de ese organismo, cometieron un total de **69 abusos sexuales y otros 30 los habrían cometido otro tipo de funcionarios de la organización, para un total de 99 denuncias. De esa cifra, más de 20 habrían sido hacia menores de edad.**

El informe evidencia 10 casos más que los registrados en 2014. Alemania, Burundi, Ghana, Senegal, Eslovaquia, Madagascar, Ruanda, República Democrática del Congo, Burkina Faso, Camerún, Tanzania, Níger, Moldova, Togo, Sudáfrica, Benín, Nigeria y Gabón, son los países de donde provienen las denuncias y aunque también hubo denuncias contra funcionarios de varios países europeos y Canadá.

La **República Centroafricana y la República Democrática del Congo,** serían los principales lugares en los que los soldados conocidos como ‘cascos azules’,  habrían cometido estos crímenes, donde **la ONU envió 12.000 militares en 2014.**

"La niña de siete años nos dijo que hizo sexo oral con soldados franceses, a cambio de una botella de agua y un paquete de galletitas", reportó la Comisión de Derechos Humanos de la ONU.

El informe de Ban Ki – moon, llama la atención de los países para que los países acusen y juzguen a los soldados y funcionarios de la ONU culpables de abuso sexual, así mismo, recomendó que se cree un banco de muestras de ADN de militares, para facilitar los procesos.

Sin embargo, ese tipo de acciones para enfrentar esta problemática, fueron criticadas por la Code Blue, una campaña de la ONG Aids Free World, que señaló que el comité de expertos de la ONU encargado de recibir las denuncias y actuar frente a ellas estaría integrado por **las mismas personas que ocupaban altos cargos de responsabilidad cuando se revelaron las violaciones.**
