Title: El Tango se une esta noche por Mocoa
Date: 2017-04-08 13:19
Category: eventos
Tags: Ayudas para Mocoa, Mocoa
Slug: el-tango-se-une-esta-noche-por-mocoa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Tango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Hoy Digital] 

###### [08 Abr 2017] 

Las mejores parejas de Tango de la capital, se unen esta noche para presentarse en tarima y recolectar ayudas particularmente para el pueblo Nasa, afectados por la avalancha. El aporte mínimo para el ingreso es de **\$20.000 mil pesos, además se recibirán donaciones de alimentos no perecederos, medicamentos, ropa, colchones y frazadas.**

Las ayudas se trasladarán a Mocoa por equipo de defensores de  derechos humanos de la Comisión de Justicia y paz que las entregaran al pueblo Nasa que se encuentra en esta región del país y que **al no estar reunida en los albergues, no ha recibido ningún tipo de ayuda por parte del gobierno. **Le puede interesar: ["Habitantes de Mocoa denuncian que ayudas no estarían llegando a personas fuera de los albergues"](https://archivo.contagioradio.com/habitantes-de-mocoa-denuncian-que-ayudas-no-estarian-llegando-a-personas-fuera-de-los-albergues/)

De acuerdo con Javier Sánchez, uno de los impulsadores de esta iniciativa, la finalidad de este evento es juntar las voluntades de muchas personas “mucha gente en Colombia piensa que consignar algo individual es muy poco y terminan no haciéndolo, **cualquier cosa que podamos aportar suma, nosotros juntamos el medio del tango"**.

De igual forma, afirmó que **dentro de 20 días podría estarse realizando otro evento similar a este**, ya que consideran que las ayudas que se den a los damnisficados de Mocoa se acabarán muy rápido y esta debe ser una labor constante hasta que los habitantes logren estabilizarse. Le puede interesar: ["Peligro de epidemias deben ser contrarrestadas con urgencia en Mocoa"](https://archivo.contagioradio.com/peligro-de-epidemias-deben-ser-contrarrestados-con-urgencia-en-mocoa/)

El evento se realizará en la dirección **carrera 27 \#51-10 y los asistentes podrán llegar desde las 8:00 pm**, entre las escuelas de baile que se encargaron de organizar este evento están: Alma de Tango, La Milonga del Sueñño Tango Esencias, El Desbande, Tupungatina, Barra de Tango Universidad del Rosario, Milonga Tinta Roja y Textiles Moda.

![tango1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/tango1.jpg){.alignnone .size-full .wp-image-38960 width="1200" height="799"}

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
