Title: Canción para dueto: las ausencias, el cuerpo y la violencia en Colombia
Date: 2017-09-18 16:16
Category: eventos
Tags: Bogotá, Cultura, teatro
Slug: teatro-bogota-cancion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/image003.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Omutismo 

###### 19 Sep 2017

La compañía Omutismo presenta “Canción para Dueto” segunda parte de la icónica obra “A la sombra del sol”, que parte de una desgarradora investigación sobre las ausencias, el cuerpo y la violencia en Colombia. Esta pieza hace parte del ciclo Cuerpo y Posconflicto de La Factoría L’explose y estará en temporada de estreno del 21 al 30 de septiembre.

“Canción para Dueto” es una coproducción entre Colombia y EEUU, una pieza de teatro físico dirigida e interpretada por Jimmy Rangel, recordado por dirigir a Flora Martínez en “Frida” y a Alejandra Borrero en “Histeria”. Ahora estará junto a una de las bailarinas más exitosas de Nueva York: Mariah Halkett, además la pieza cuenta con la asesoría coreográfica de Yenzer Pinilla y la música de Justin Vahala.

En “Canción para Dueto” un escenario poéticamente construido es utilizado para un velorio eterno, una vieja casa en medio de un árido paraje es trasformado sutilmente en un mar furibundo, en una habitación donde todo cobra vida, donde sucederá una desbordada fiesta, donde hay una lluvia constante, donde una mujer se transforma en pájaro, un demente juego de apariciones y desapariciones.

Desde su estreno en el 2008 "A la Sombra del Sol" ha sido una de las piezas de teatro físico que ha pretendido darle voz al profundo silencio de la guerra, para el 2017 la Compañía estrena su nueva pieza “Canción para Dueto”, un espectáculo que juega con la ausencia, el recuerdo, la muerte misma, una mezcla perfecta de silencio, música, humor surrealista y un cuerpo puesto al borde.

La obra se presenta de jueves a sábado a las 8:00 p.m. El costo de ingreso es de \$30.000 General \$25.00 estudiantes y tercera edad, en la Factoría L’EXPLOSE Carrera 25 \# 50-34 Informes y reservas: 2496492.
