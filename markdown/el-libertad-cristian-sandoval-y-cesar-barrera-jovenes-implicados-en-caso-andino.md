Title: ¡En libertad!  Cristian Sandoval y César Barrera, víctimas de montaje judicial en caso Andino
Date: 2020-06-05 23:33
Author: AdminContagio
Category: Nacional
Slug: el-libertad-cristian-sandoval-y-cesar-barrera-jovenes-implicados-en-caso-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Cristian-Sandoval-y-César-Barrera.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En la noche del viernes 5 de junio, **Cristian Sandoval y César Barrera**, dos de los jóvenes víctimas de montaje judicial en el caso del Centro Comercial Andino, quedaron en libertad tras no existir ninguna prueba que los inculpara de los hechos sucedidos el  17 de junio del 2017 en la ciudad de Bogotá.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los jóvenes que fueron recibidos por sus familias a la salida de la cárcel La Picota, quedaron en libertad luego de la orden del juez ante el vencimiento de términos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cerca de tres años estuvieron en la cárcel sindicados de pertenecer al Movimiento Revolucionario del Pueblo, acusación que la defensa ha manifestado como parte de un montaje judicial en un proceso que ha estado cargado de irregularidades desde el inicio. Ver: [La versión de los jóvenes capturados por el atentado en el Centro Comercial Andino](https://archivo.contagioradio.com/entrevista-a-boris-rojas-y-lizeth-rodriguez-capturados-por-atentado-a-centro-comercial-andino/)

<!-- /wp:paragraph -->

<!-- wp:html -->

> En libertad quedaron Cristian Sandoval y César Barrera, víctimas del montaje judicial conocido como "Caso Andino", tras fallo de juez. [pic.twitter.com/spH5LuOeES](https://t.co/spH5LuOeES)
>
> — Contagio Radio (@Contagioradio1) [June 6, 2020](https://twitter.com/Contagioradio1/status/1269117469903458305?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:paragraph -->

En desarrollo

<!-- /wp:paragraph -->
