Title: La agenda que debería adoptar el gobierno para facilitar conversaciones de paz con ELN
Date: 2018-01-12 10:49
Category: Nacional, Paz
Tags: ELN, Gobierno Nacional, Mesa de diálogo en Quito, paz, proceso de paz
Slug: la-agenda-que-deberia-adoptar-el-gobierno-para-facilitar-conversaciones-de-paz-con-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/pazpazpaz-e1512150798177.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Informa] 

###### [12 Ene 2018] 

Donka Atanasova, integrante y vocera de la Mesa Social para la Paz, expresó que se debe superar el estado de pánico en la mesa de Quito, a partir del diálogo, dé un respaldo desde la ciudadanía para recalcar la importancia de este proceso y de una estrategia concreta desde el gobierno que incluya **la seguridad para los líderes sociales y el desmonte del paramilitarismo.**

“Hay un momento que atraviesa el país que hace que sea complicado pactar la continuidad del proceso con expectativas certeras, y eso como sociedad tenemos que superarlo” afirmó Atanasova y agregó que a su vez, el gobierno debería tener una propuesta frente a qué hacer con los grupos armados ligados al narcotráfico, **que están generando una dinámica de guerra que no contribuye a establecer el cese bilateral**.

En esa misma vía, la vocera de la Mesa Social para la Paz, señaló que se debe ir avanzando en los puntos de conversación de la mesa en Quito, como la participación social y de transformaciones para la paz, para que de esta manera se genere una confianza entre todas las partes que dé como resultado un nuevo cese al fuego. (Le puede interesar: ["4 propuestas para destrabar la mesa ELN-Gobierno y reanudar el cese bilateral"](https://archivo.contagioradio.com/4-propuestas-para-destrabar-la-mesa-eln-gobierno-y-reanudar-cese-bilateral/))

### **"Hay voluntad para superar la crisis"** 

Los países garantes del proceso de paz entre el ELN y el gobierno de Juan Manuel Santos, anunciaron a través de un comunicado de prensa que ambas partes, en la mesa, ya expresaron su voluntad por **“superar este momento y continuar las conversaciones que conduzcan a la paz”**, de igual forma solicitaron que se inicie lo más pronto posible el V ciclo de conversaciones en donde el tema prioritario debe ser el cese bilateral.

Los países garantes son Venezuela, Noruega, Cuba, Brasil, Chile y Ecuador, que, además en la misiva también exhortaron, tanto al ELN como al gobierno Nacional, a evitar el escalamiento que ponga en peligro los avances logrados en el proceso de negociación y en el anterior cese bilateral. (Le puede interesar: ["En el limbo mesa de conversaciones entre gobierno y ELN"](https://archivo.contagioradio.com/cuidados_intensivos_mesa_quito_eln_gobierno/))

###### Reciba toda la información de Contagio Radio en [[su correo]
