Title: Caravana Humanitaria acompañará a comunidad del Cañón del Micay
Date: 2020-09-26 11:03
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Cañón del Micay, Caravana Humanitaria, Cauca, víctimas del conflicto armado
Slug: caravana-humanitaria-acompanara-a-comunidad-del-canon-del-micay
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Caravana-Humanitaria-al-Canon-del-Micay-scaled-2-e1601067045233.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Caravana-Humanitaria-al-Canon-del-Micay-scaled-3.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: alcaldía

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Más de 30 organizaciones sociales en el Cauca harán parte de la **Primera Caravana Humanitaria al Cañón del Micay**, que se dará del 22 al 26 de octubre. **La caravana tiene como objetivo acompañar a las comunidades víctimas del conflicto armado y visibilizar las problemáticas a las que hoy en día, se enfrentan.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las organizaciones, entre ellas la [Red por la Vida y los Derechos Humanos del Cauca](https://twitter.com/RedVidaDHCauca)y el [Coordinador Nacional Agrario –CNA-](https://twitter.com/CNA_Colombia) recorrerán los municipios de Argelia y El Tambo para acompañar a las comunidades campesinas, afrocolombianas e indígenas.

<!-- /wp:paragraph -->

<!-- wp:image {"id":90474,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Caravana-Humanitaria-al-Canon-del-Micay-scaled-3.jpg){.wp-image-90474}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

La principal razón por la que la Caravana Humanitaria se ha centrado en el Cañón del Micay, es porque durante este año, ha sido uno de los territorios del Cauca en que se ha agudizado la violencia contra la comunidad y contra líderes, lideresas y defensores de DD.HH. (Le puede interesar: [167.559 familias afectadas por conflictos societerritoriales en la Panamazonía](https://archivo.contagioradio.com/167-559-familias-afectadas-por-conflictos-societerritoriales-en-la-panamazonia/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

La violencia se sigue agudizando en el Cauca
--------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En lo corrido del año **los actos de violencia por la presencia de grupos legales e ilegales y los asesinatos a líderes sociales y defensores de derechos humanos han aumentado** de manera preocupante, sobre todo en departamentos como el Cauca, Antioquia y Nariño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual manera, siguen aumentando las masacres en el Cauca. Según el Instituto de estudios para el desarrollo y la paz [- Indepaz-](http://www.indepaz.org.co/masacres-los-tapabocas-mas-usados/) en el Cauca se han perpetrado 9 masacres hasta el 20 de septiembre. (Le puede interesar: [Nueva masacre en el Cauca, 7 jóvenes asesinados](https://archivo.contagioradio.com/nueva-masacre-en-el-cauca-7-jovenes-asesinados/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo a organizaciones defensoras de DD.HH., López de Micay, Argelia y El Tambo son el centro de una disputa territorial entre actores armados por tomar el Cañón del Micay, espacio en el que persisten los cultivos de uso ilícito.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar de que cerca de 7.600 hombres de las fuerzas militares hacen presencia en el Cuaca, las organizaciones que participan en la caravana señalan que "su accionar está lejos de recuperar la paz y la tranquilidad de sus habitantes. Su accionar se enfoca en reprimir y criminalizar los tejidos organizativos de las poblaciones campesinas, afrocolombianas e indígenas que hacen presencia en la región”. (Le puede interesar: [En riesgo líderes y comunidad del Espacio Humanitario de Puente Nayero](https://archivo.contagioradio.com/en-riesgo-lideres-y-comunidad-del-espacio-humanitario-de-puente-nayero/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
