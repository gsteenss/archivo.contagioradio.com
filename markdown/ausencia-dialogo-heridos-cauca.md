Title: Ausencia de diálogo ha dejado seis heridos y un muerto en Cauca
Date: 2019-03-20 15:25
Category: Comunidad, Movilización
Tags: Cauca, indígenas, Minga, Panamericana
Slug: ausencia-dialogo-heridos-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-2019-03-20-a-las-1.33.51-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @AndresEliasGil] 

###### [20 Mar 2019] 

La Minga indígena completa 10 días y tras la ausencia del presidente Duque en la zona, se han producido enfrentamientos con el Escuadrón Móvil Antidisturbios (ESMAD) y el Ejército, situación que alcanzó el pasado martes un pico de violencia alto al producirse disparos por parte de la fuerza pública, resultando 6 mingueros heridos.

Como explica Felipe Castiblanco, profesor del resguardo Cerro Tijeras en Suárez, Cauca, esta comunidad viene apoyando desde el lunes la Minga en dos puntos: Suárez y en La Agustina sobre la Vía Panamericana. Justamente en este último se produjo la confrontación, cuando dos batallones del Ejército que operan en la zona hicieron presencia para hacer remoción de los materiales con los que se estaba bloqueando la vía.

En medio de esta operación, cerca del medio día se escucharon ráfagas de fusil que dejaron a 6 comuneros indígenas heridos, uno de ellos del resguardo Cerro Tijeras, quien tuvo que ser trasladado a Cali por la gravedad de sus heridas, pero que afortunadamente ya está fuera de peligro. Adicionalmente se conoció en medios de comunicación el fallecimiento de un integrante del ESMAD. (Le puede interesar:["La Minga indígena que reclama a Duque cumplir los acuerdos"](https://archivo.contagioradio.com/minga-indigena-duque/))

### **El Ejército con balas, los indígenas con bastón de mando** 

Según la versión del Ejército sobre los videos en los que se aprecia a uniformados disparando al aire, se argumenta que desde las montañas se realizaron disparos en su contra, no obstante Castiblanco recuerda que el Consejo Regional Indígena del Cauca (CRIC) ha tomado las precauciones necesarias para evitar infiltraciones en la minga, tanto en el bloqueo a las vías como en los campamentos.

A ello se suma lo mencionado en otras entrevistas con líderes de la Minga, quienes han manifestado su rechazo rotundo al uso de las armas; así como resaltan el control del territorio que hace la guardia indígena, llegando a capturar incluso a integrantes de las disidencias mientras intentan cometer delitos. (Le puede interesar: ["Mientras policía afirma que disidencias infiltran la Minga, comunidades indígenas las capturan"](https://archivo.contagioradio.com/mientras-policia-afirma-que-disidencias-infiltran-la-minga-comunidades-indigenas-las-capturan/))

Respecto al caso del integrante del ESMAD que falleció, el Profesor sostiene que las informaciones que han circulado en medios de comunicación son por lo menos incompletas, puesto que en principio se dijo que había muerto por disparos de arma de fuego, mientras otras informaciones aseguran que se trató de un error en la manipulación de pólvora cuando el uniformado estaba ensamblando una 'recalzada'. A ello se suma la información según la cual un helicóptero habría recogido al uniformado en el lugar de los hechos, situación que Castiblanco desmiente.

### **Es momento que el Presidente asista al Cauca para dialogar** 

La primera invitación que hicieron los pueblos indígenas del Cauca al Presidente para que asistiera a la región tuvo lugar en agosto de 2018, 20 días antes de iniciar la Minga reiteraron la invitación y el 11 de marzo insistieron en su mensaje; sin embargo, Duque envío a la Ministra de Interior y afirmó que no tenía espacio en su agenda para llegar al Cauca. (Le puede interesar: ["Presidente Duque seguimos esperando su llegada: Minga indígena"](https://archivo.contagioradio.com/presidente-duque-seguimos-en-minga-esperando-su-llegada-comunidades-indigenas-del-cauca/))

Posteriormente, el Presidente dijo estar dispuesto a hablar, pero anunció que el ESMAD estaba listo para actuar si se tomaban vías de hecho. De acuerdo a Castiblanco, hace dos días Duque manifestó que asistiría una comisión de ministros y otros altos funcionarios al Cauca para dialogar, escenario que sí mostraría una intención de contribuir a la mesa de diálogos, evitando más heridos en nuevas confrontaciones.

<iframe id="audio_33554659" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33554659_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
