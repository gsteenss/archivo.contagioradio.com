Title: Aún quedan 12 licencias ambientales más que amenazan La Macarena
Date: 2016-04-20 14:53
Category: Ambiente, Nacional
Tags: ANLA, extracción petrolera, La Macarena
Slug: aun-hay-12-licencias-ambientales-mas-que-amenazan-la-macarena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/La-Macarena1-e1461181026531.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Econoticias 

###### [20 Abr 2016] 

Hoy en Villavicencio, miles de personas se movilizan en rechazo a la [exploración y explotación petrolera cercana a Caño Cristales.](https://archivo.contagioradio.com/habitantes-de-la-macarena-tambien-se-oponen-a-explotacion-petrolera/)Aunque gracias a presión nacional en internacional, la Agencia Nacional de Licencias Ambientales, **ANLA, se vio obligada revocar la licencia ambiental**, continúa la amenaza debido a que en esa zona **aún hay 12 licencias ambientales más para extraer petróleo y realizar actividades mineras.**

La decisión de la ANLA se debe a que **esta** **zona contaba con un plan de manejo integral**. Se trata de la resolución 2585 del 30 de diciembre del 2015 de Cormacarena, por la cual se creó el Plan Integral de Manejo del Sector Río Losada-Caño Perdido, ubicado en la zona de Recuperación para la Producción Sur del Distrito de Manejo Integrado Ariari-Guayabero, lo cual  excluía ese sector de la actividad petrolera.

Sin embargo, apenas se revocó una licencia de un bloque petrolero, cuando aún existen cuatro bloques más que pertenecerían a las empresas **Hupecol, PetroNova, Hocol y Petrominerales, además existen 7 títulos mineros para Arenas Bituminosas y otros dos títulos mineros para extraer oro**, que se encontrarían en zona de amortiguamiento de las reservas naturales de esa parte del país, donde se encuentra uno de los lugares más emblemáticos que enmarca un patrimonio ambiental del planeta, [Caño Cristales.](https://archivo.contagioradio.com/no-es-posible-hacer-explotacion-petrolera-sin-danar-los-ecosistemas/)

Por su parte, la empresa Hupecol ha asegurado que la ANLA no les ha notificado la revocatoria de la licencia ambiental. La petrolera afirmó que en rueda de presa que estarían pensado en demandar al Estado. Así mismo, la Asociación Colombiana de Petróleo (ACP), señaló que fue e**l gobierno quien invitó a la empresa a realizar explotación de petróleo en la región de La Macarena.**

Por lo anterior, la comunidad no solo se movilizará en Villavicencio sino en otros municipios del departamento del Meta como Vista Hermosa y Granada, donde el ambiente y los habitantes se verían afectados por este tipo de explotación de los recursos naturales.

<iframe src="http://co.ivoox.com/es/player_ej_11242344_2_1.html?data=kpaflpeXeJWhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncabYytnVjabLucXZzdSYj5CxqdTVjKbf1s7Huc3VxM6SpZiJhpTijMjiztnZtsKft87ZzsbarcTZz8jWj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
