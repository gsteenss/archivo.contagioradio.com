Title: Diálogo en la comunidad estudiantil: la respuesta al anuncio de Peñalosa
Date: 2018-03-09 12:37
Category: Educación, Nacional
Tags: Enrique Peñalosa, ESMAD, universidad pedagogica
Slug: fuerza_publica_universidad_pedagogica_rector_penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/esmad-en-plaza-de-toros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Desmonte del Esmad] 

###### [9 Mar 2018] 

"Por principio académico y convicción intelectual la universidad no debe recibir el ingreso de la fuerza pública", expresa el rector de la Universidad Pedagógica, Adolfo Atehortúa, frente al anuncio del alcalde de Bogotá, Enrique Peñalosa en el que señaló que la fuerza pública debería ingresar a los campus cuando hay protestas en las universidades.

[“Las armas que están utilizando son letales, si es el caso vamos a intervenir y entraremos a la universidad, nosotros no vamos a seguir permitiendo este tipo de situaciones como la fabricación de armas letales, **no nos podemos quedar por fuera como si fuera una República independiente“,** manifestó el alcalde, tras los enfrentamientos que se presentaron en la universidad en pasado miércoles.]

### **Los estudiantes han sido algunas de las principales víctimas de agresiones del ESMAD** 

De acuerdo con el rector, la universidad es de carácter nacional, y "lo que hacemos es trabajar por la formación, el saber y el conocimiento". Aunque reconoce que las dinámicas actuales del país y de la universidad pública son complejas, ante todo debe  prevalecer que se trata de un espacio educativo que no puede prestarse para la violencia. "**Rechazamos que el campus se use para la violencia porque su esencia está comprometida con la vida",** manifiesta.

Cabe mencionar, que de acuerdo con los debates de control político que han hecho congresistas como Alirio Uribe, Ángela María Robledo e Iván Cepeda, se ha denunciado que precisamente uno de los lugares más afectados por los abusos por parte del ESMAD es es Bogotá. El sector estudiantil, junto a los indígenas y quienes se han manifestado en los barrios fueron las principales víctimas del accionar de la fuerza pública en todo el país durante 2016.

Incluso, en muchos de los casos de personas que han perdido de la vida por actuaciones del Escuadrón Antidisturbios, se evidencia que las muertes se han producido en el momento en el que **el ESMAD realiza la intervención y arremete con disparos de arma de fuego, o con proyectiles de armas "no letales",** pues en distintas ocasiones se ha denunciado que las armas que son usadas para dispersar a la ciudadanía son utilizadas para agredir a las personas apuntando dichas armas a pocos metros del individuo, apuntando incluso a la cara.[(Le puede interesar: El prontuario del ESMAD)](https://archivo.contagioradio.com/el-prontuario-del-esmad/)

### **La reacción de la comunidad estudiantil ** 

Los estudiantes de la universidad han manifestado su rechazo a las acciones violentas por parte de los encapuchados, aunque también afirman estar de acuerdo con las exigencias del movimiento estudiantil referente la calidad de la educación. Además, señalan como preocupante las medidas que ha anunciado Peñalosa ya que no están van de acuerdo con la dinámica al interior de los centros educativos.

"Sé que el estudiantado de las universidades del país tiene razones para expresar sus inquietudes, sobre todo con respecto la financiación de la universidad pública y para exigir mayor compromiso del Estado con respecto a la calidad de la educación", dice  Adolfo Atehortúa.

Ante tal situación, este viernes los estudiantes han convocado a **"una jornada de sensibilización, reflexión y solidaridad para que nunca más se bañe en sangre, temor y dolor la universidad".** El plantón que tendrá entre otras actividades un compartir de alimentos se realizará a las 6 pm en la entrada de la 72.

Por su parte, el rector ha indicado que el lunes se retomarán las clases y al medio día se desarrollará una reunión con todos los estamentos para dialogar sobre los hechos que han acontecido y el compromiso de la universidad con la construcción de paz.

<iframe id="audio_24311890" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24311890_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
