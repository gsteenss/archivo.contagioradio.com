Title: Lo que pedería Ecuador y Latinoamérica si pierde pierde Alianza País
Date: 2017-02-24 15:55
Category: El mundo
Tags: ecuador, Elecciones segunda vuelta
Slug: lo-que-pierde-ecuador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Blog-post-on-Quito.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [ Foto: Power to the people] 

###### [23 Feb 2017] 

Ecuador se enfrentará el próximo dos de abril a la segunda vuelta electoral que definirá si el próximo presidente será Lennin Moreno, candidato de Alianza País o Guillermo Lasso, miembro del Movimiento Creando Oportunidades. **Panorama que podría finalizar con 10 años de políticas progresistas en Ecuador, en caso de ganar la oposición.**

Guillermo Lasso, quien obtuvo el 28,38% en el escrutinio, ha prometido una apertura económica y un cambio de gobierno. Además sectores de la oposición que apoyan esta transformación en las políticas del país, como Cynthía Viterim han expresado su apoyo en la segunda vuelta, **aumentando las posibilidades que Lasso se quede en el Palacio de Carondelet.** Le puede interesar:["Elecciones presidenciales a segunda vuelta en Ecuador"](https://archivo.contagioradio.com/elecciones-presidenciales-a-segunda-vuelta-en-ecuador/)

Sin embargo, qué significaría este posible cambio de gobierno en Ecuador, de acuerdo con Federico Larssen el país podría verse afectado concretamente en **las políticas económicas fiscales, el proceso de paz para Colombia o Latinoamérica.**

### **Sistema Financiero Alternativo** 

Ecuador es uno de los pocos países latinoamericanos que ha sido afectado en menor escala por la crisis económica mundial que ha generado recesión de un **4.5 % comparada a la de otros países como Venezuela que es de un 8% o Colombia con un 7.3%**. Para Larsso este hecho se fundamenta en la búsqueda que ha tenido este país, bajo el gobierno de Correa de un  sistema de desarrollo económico desligado del sistema financiero Internacional.

### **Ecuador y la paz** 

Actualmente Ecuador es la sede de la mesa de conversaciones que se desarrolla entre la guerrilla del Ejército de Liberación Nacional y el gobierno de Colombia, acto que ha demostrado el interés de este país por que se consolide una paz latinoamericana. Larsso señala que **“hay un Ecuador que se ha creado una imagen ligada al respeto de los derechos humanos”**. Le puede interesar: ["Los restos de las conversaciones de Paz ELN-Gobierno"](https://archivo.contagioradio.com/los-retos-de-las-conversaciones-de-paz-gobierno-eln/)

### **América Latina** 

Larsso señala que en este ámbito, Ecuador ha venido desempeñando un lugar muy importante gestando la organización de estructuras que agrupan países de Latinoamérica como el ALBA **“el trabajo que han hecho los ecuatorianos es de trabajo para que se dieran una serie de acuerdos latinoamericanos importantes”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
