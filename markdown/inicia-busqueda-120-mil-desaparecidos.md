Title: Inicia búsqueda de los 120 mil desaparecidos en los territorios
Date: 2019-05-29 17:52
Author: CtgAdm
Category: Nacional, Paz
Tags: Deaparecidos, semana del detenido desaparecido, territorio, Unidad de Búsqueda de Personas Desaparecidas
Slug: inicia-busqueda-120-mil-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/familiares-desaparecidos-Es-1132x670.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: larazon.co  
] 

En el marco de la semana internacional del detenido-desaparecido, la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD) inició su despliegue territorial en 9 departamentos. **La tarea de la Unidad es encontrar a más de 120 mil desaparecidos**; trabajando de la mano de organizaciones sociales y víctimas, con las que espera sortear todos los problemas a los que se enfrenta.

**Luz Marina Monzón, directora de la Unidad**, explicó que el universo de víctimas dadas por desaparecidas en el contexto y con ocasión del conflicto asciende a 120 mil; sin embargo, reconoció un "subregistro importante", lo que implicaría que podrían ser más. Para dar respuesta a las familias de estas personas, desde el lunes el mecanismo emprendió su presencia en **Barranquilla, Sincelejo, Apartadó, Rionegro, Cali, Puerto Asís, Cúcuta, Barrancabermeja, Villavicencio y San José del Guaviare**.

De esta forma la Unidad espera emprender procesos junto con las víctimas y organizaciones que permitan cumplir "con la labor de construcción de paz mediante la búsqueda de los desaparecidos". (Le puede interesar: ["Víctimas plantean los retos en la búsqueda de personas desaparecidas"](https://archivo.contagioradio.com/victimas-plantean-los-retos-en-la-busqueda-de-desaparecidos/))

> [\#SemanaDesaparecido](https://twitter.com/hashtag/SemanaDesaparecido?src=hash&ref_src=twsrc%5Etfw) | ¿Sabe cómo trabaja la UBPD? No se pierda nuestro ABC, en el que semanalmente explicaremos lo que necesita saber para entender cómo funciona la Unidad de Búsqueda.[\#LaBúsquedaEsContigo](https://twitter.com/hashtag/LaB%C3%BAsquedaEsContigo?src=hash&ref_src=twsrc%5Etfw) [\#UBPDpedagogia](https://twitter.com/hashtag/UBPDpedagogia?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/eOUsHEovPu](https://t.co/eOUsHEovPu)
>
> — Unidad de Búsqueda UBPD (@UBPDBusqueda) [27 de mayo de 2019](https://twitter.com/UBPDBusqueda/status/1133109702961913856?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Los retos de la búsqueda de desaparecidos en los territorios  
** 

Una de las críticas al Plan Nacional de Desarrollo era el financiamiento para la implementación del Acuerdo de Paz, específicamente con la Unidad, la denuncia sostenía que contaba con un 40% del presupuesto necesario para cumplir con su labor. No obstante, la Directora de la entidad afirmó que han sorteado las dificultades con recursos de cooperación internacional; y disminuyendo el personal de planta que estará en los territorios. (Le puede interesar: ["Pese a recortes presupuestales UBPD no se detiene"](https://archivo.contagioradio.com/pese-a-recortes-presupuestales-la-unidad-de-busqueda-de-desaparecidos-no-se-detiene/))

Efecto de dichas decisiones se evidencia en el número de integrantes que estarán en cada región, pues serán equipos de 4 personas, vinculadas como contratistas y personal fijo de la entidad. Teniendo en cuenta que los equipo son reducidos para la labor que deben realizar, Monzón manifestó que espera poder resolver el tema de la financiación con el Ministerio de Hacienda, para tener los recursos necesarios que posibiliten el desarrollo de su labor.

Otro desafío será la coordinación institucional de la Unidad con la Fiscalía General de la Nación y Medicina Legal, entidades encargadas hasta el momento de la búsqueda e identificación de desaparecidos; sobre ese aspecto, Monzón sostuvo que se deben "transformar prácticas que no han sido de mucha información y cooperación", añadiendo que la Unidad ha buscado apostarle a las buenas prácticas para el fortalecimiento de las relaciones institucionales.

### **Víctimas tienen esperanza y confían en la Unidad**

Para realizar el despliegue en los territorios, Monzón recordó que fueron necesarias sesiones de trabajo con diferentes instituciones, organizaciones y víctimas; y cuando el diálogo se realizó, recibieron de su parte un mensaje de esperanza. En ese sentido, aseguró que esperan **mantener esa esperanza, fortalecer la confianza en la institución que ella preside y responder a la necesidad de involucrar a todas las personas en el proceso de búsqueda.**

<iframe id="audio_36485001" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36485001_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
