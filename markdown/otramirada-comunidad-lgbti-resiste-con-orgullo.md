Title: OtraMirada:  Comunidad LGBTI resiste con orgullo
Date: 2020-06-28 13:52
Author: PracticasCR
Category: Actualidad, Otra Mirada, Otra Mirada, Programas
Tags: Comunidad LGBTI, Comunidad Trans, Día Internacional del Orgullo Gay, Población LGBTI
Slug: otramirada-comunidad-lgbti-resiste-con-orgullo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/lgbti_flag-e1478191764857.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Organizaciones LGBTI

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todos los años el 28 de junio se celebra el Día Internacional del Orgullo LGBT en el que miles de personas se toman las calles alrededor de todo el mundo para festejar la diversidad sexual. Sin embargo, no es solo un día de felicidad pues, dado que esta comunidad constantemente es víctima de discriminación, agresiones, amenazas y asesinatos también es un espacio para homenajear a todas aquellas personas que, por su orientación sexual, han sido víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, la batalla es aún más complicada desde los territorios, por esta razón y para conocer los procesos que articulan desde las comunidades invitamos a **María Victoria Palacios, directora de la Fundación Latidos Chocó, Marcela Sánchez Buitrago, directora de Colombia Diversa, Gina Colmenares, de la Red Comunitaria Trans (B. Santa Fé) y representante de trabajadoras sexuales y por último Juan Carlos Salas director de la Fundación Sucre Diversa.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante este espacio pudimos conocer experiencias, tanto personales como desde las organizaciones. Además, **nos permitieron entender su lucha diaria para reclamar los derechos de toda su comunidad y comprender cuáles han sido esas formas de resistir** a pesar de la discriminación y la violencia. (Le puede interesar: [feminicidios son solo la-punta del iceberg limpal](https://archivo.contagioradio.com/feminicidios-son-solo-la-punta-del-iceberg-limpal/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, muchos de los territorios desde los que nos hablan nuestros invitados e invitadas han estado marcados por conflictos socio-económicos, por grupos armados y por autoridades que parecen no conocer las leyes. Ellas y ellos nos comentan cuál es el impacto que esto genera y cómo logran desarrollar procesos para sacar adelante a toda la población LGBT en medio de conflictos y de leyes olvidadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De esta manera, **pudimos ver realidades que no son muy conocidas y nos demuestran la resistencia de la comunidad LGBTI.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “No hay que ser de la causa para apoyar la causa”.
>
> -Marcela Sánchez Buitrago

<!-- /wp:quote -->

<!-- wp:paragraph -->

(Si desea escuchar el programa del 25 de junio: <https://bit.ly/38b5yAh>)

<!-- /wp:paragraph -->

<!-- wp:html /-->

<!-- wp:block {"ref":78955} /-->
