Title: 6to Festival del Cine por los derechos humanos abre convocatorias
Date: 2019-02-13 14:47
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine Colombiano, convocatorias, Festival cine por los derechos humanos
Slug: festival-cine-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Cine-y-ddhh-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Festival de Cine por los DDHH 

###### 13 Feb 2019 

Atención realizadores, productores directores, ya **están abiertas las convocatorias para la sexta versión del Festival Internacional de Cine por los Derechos Humanos,** un espacio que busca producciones capaces de romper esquemas, que promuevan y defiendan los DDHH en Colombia y el mundo.

Los trabajos audiovisuales estarán divididos en **siete categorías** y podrán ser presentados hasta el próximo **20 de marzo**, con la condición que hayan sido realizados después del **1ro de enero de 2018,** mientras que **las producciones en lengua extranjera deberán contar con los respectivos subtítulos** en castellano para ser tenidas en cuenta.

Por otra parte, las obras audiovisuales que **no cuenten con la respectiva pertinencia al tema de derechos humanos serán descalificadas** automáticamente, y los trabajos **que ya han sido enviados a ediciones previas del evento no serán aceptados**, aun cuando se trate de una nueva versión del filme.

Las categorías en competencia son: **largometraje nacional, largometraje internacional, cortometraje internacional, documental nacional** (corto documental y largo documental), **documental internacional** (corto documental y largo documental), **animación, infantil y cortometraje nacional.**

Para inscribir su proyecto audiovisual puede consultar las bases de participación [aquí](http://cineporlosderechoshumanos.co/wp-content/uploads/2019/01/CONVOCATORIA-EL-CINE-NOS-UNE-2019.pdf)y si desea conocer más sobre este festival cinematográfico que se realizará en el mes de agosto en las ciudades de Bogotá, Barranquilla, Cartagena, Medellín y Pereira puede consultar la página oficial en [www.cineporlosderechoshumanos.com](http://cineporlosderechoshumanos.co/)

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
