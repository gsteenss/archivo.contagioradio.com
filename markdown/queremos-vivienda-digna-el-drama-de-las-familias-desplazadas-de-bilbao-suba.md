Title: "Queremos vivienda digna" el drama de las familias desplazadas de Bilbao, Suba
Date: 2017-04-29 19:59
Category: DDHH, Entrevistas
Tags: Bilbao-Suba, ESMAD, Esperanza Villa del Río, Peñalosa
Slug: queremos-vivienda-digna-el-drama-de-las-familias-desplazadas-de-bilbao-suba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/IMG_0257-e1493511811971.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Abr 2017] 

Esperanza Villas del Río, era el nombre que los habitantes le habían dado al lugar donde convivían más de 300 familias víctimas del conflicto armado del país, **el desplazamiento, el miedo o de la exclusión de las ciudades, la falta de oportunidades laborales** y de empleos dignos, que les permitieran sostener a sus familias.

300 familias que volvieron a perderlo todo tras el desalojo del pasado 24 abril, ordenado por la Alcaldía Mayor de Bogotá y efectuado por el Escuadrón Móvil Anti Disturbio (ESMAD), y en donde **el fuego, que aún se desconoce quién inicio, destruyó las casas de cartón con las esperezas que allí vivían**.

### **El día del Desalojo** 

Desde las 4:00 am los habitantes alertaron a la comunidad sobre la presencia de agentes del ESMAD, sin compañía de Defensoría del Pueblo, y con una orden de desalojo que se haría efectiva. Deidi Ortíz, lideresa del barrio, apenas supo la información, **sacó a su hija María Victoria del lugar y la dejó en la casa de unos vecinos, cuadras más arriba. **Le puede interesar: ["Un kit y 150 mil pesos, solución de la Alcaldía para desalojos en Bilbao, Suba"](https://archivo.contagioradio.com/un-kit-y-150-mil-pesos-solucion-de-la-alcaldia-para-desalojados-en-bilbao/)

Regresó a su casa para sacar algunas de sus pertenencias, sin embargo sobre las 10:00am, el fuego ya había comenzado y **solo logró rescatar su estufa, sus documentos y algunas prendas de vestir**. “Estábamos pidiendo un diálogo, que nos colaboraran, nadie escuchó, se nos mandaron, fue muy fuerte, y como no dejaron sacar nada, cuando se prendió un rancho, se prendieron todas las casas y ya nos tocó dejar que todo eso se quemará, porque qué más”relató Deidi.

Yaselis tiene nueve años, llegó hace un año desde Cartagena a Esperanza Villas del Río, durante el incendio estaba encerrada con Duván, su primo, en una casa cercana, mientras sus papás intentaban rescatar algo. Ese día su mascota, una perrita llamada “Negra”  se quemó, al igual que sus uniformes para ir al colegio, sus cuadernos y juguetes. “**Estábamos mirando todo por una ventana, veíamos fuego, como quemaban las casas, a Negra se la llevaron porque se estaba quemando**”.

![IMG\_0285](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/IMG_0285-e1493512283721.jpg){.alignnone .size-full .wp-image-39873 width="752" height="564"}

Jeimmy Parada, otra de las líderes de la comunidad, se encontraba en su casa y relató que el ESMAD quemó las viviendas: “empezaron a llegar a las 4 de la mañana. A las 9 de la mañana nos empezaron a rodear y a eso de las 10:30 u 11:00, fue cuando empezó el desalojo, **ellos mismos le prendieron candela a los ranchitos y nos quemaron absolutamente todo**”.

### **Los desalojados, víctimas de la indolencia Estatal** 

“**Habían más de 1.160 personas, que componían más de 360 núcleos familiares, la mayoría de ellos eran desplazados**, víctimas del conflicto armado interno, recicladores y madres cabeza de familia” expresó Karen Poveda, integrante del proceso Enrólate con Suba, de la plataforma de Congreso de los Pueblos, que ha estado acompañando a la comunidad.

De acuerdo con los censos, hechos por los líderes del barrio, **había 150 niños y niñas, 50 madres gestantes, de ellas la mayoría menores de edad,** en condiciones extremas de vulnerabilidad y con una negación por parte del Estado de todos sus derechos. Le puede interesar: ["Desalojo violento de 1200 habitantes del barrio Bilbao en la localidad de Suba"](https://archivo.contagioradio.com/bilbaosubadesalojo/)

Jeimmy y Deidi llegaron al barrio de invasión porque no tenían como pagar un arriendo, debido a la falta de un trabajo y a que no fueron tenidas en cuenta por ningún programa o política que se encargue de asegurarles mínimos vitales. **Con sus familias a cuestas, construyeron sus “ranchitos” un lugar donde meter la cabeza**, sin pagar arriendo y con la espera, día tras día, de trabajar en el rebusque.

![IMG\_0265](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/IMG_0265-e1493512481196.jpg){.alignnone .size-full .wp-image-39874 width="752" height="564"}

“A mí que me dieran un subsidio, yo pago mi vivienda, llevó 10 años de desplazamiento y si me han dado 5 ayudas por parte del Estado es mucho, sí me dan un mercado o plata para arriendo eso se pasa, **pero uno teniendo una vivienda sabe que tiene en donde meter la cabeza, es un derecho**” afirmó Deidi.

### **Los errantes de Colombia** 

De las 1.601 personas que habían en este lugar solo quedan 50, poco a poco las familias se han desplazado, seguramente a otras periferias de la capital, a otros barrios de invasión, en donde ni la Alcaldía ni el Estado hacen presencia. **Las familias que quedan son las que no tienen ningún otro lugar hacia donde desplazarse por cuarta o quinta vez, en sus vidas**.

La tierra quemada aún expulsa el calor del fuego que arrasó con los recuerdos, los muebles, los juguetes calcinados hacen parte de la vida a la que miles de personas están sujetas en el país, son las historias de la exclusión de siempre, oculta en los rincones en los que no mandata la constitución, en donde **recicladores y madres cabeza de hogar se abren paso entre latas, rechazo y la impertinencia que significa para los gobernadores que existan.**

Con voluntad, con valentía, desafiando a los sueños y a todos los no de los que han sido víctimas, continuarán luchando, caminado, serán errantes por este país que ha preferido omitirlos antes que hacerlos poseedores de derechos, **seguirán buscando la tierra prometida, para sembrar semillas que crezcan sin miedo al desarraigo, a la exclusión y la segregación.**

\

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
