Title: En Colombia la población desplazada "es más pobre que los pobres"
Date: 2015-02-17 23:00
Author: CtgAdm
Category: Economía, infografia, Otra Mirada
Tags: DANE, Desplazamiento, pobreza, víctimas
Slug: en-colombia-la-poblacion-desplazada-es-mas-pobre-que-los-pobres
Status: published

###### Foto: theprisma.uk 

Esta semana, se presentó ante la Corte Constitucional los resultados dos encuestas que se refieren a la situación de pobreza de las familias que viven en condición de desplazamiento. Se trata de informes presentados la Contraloría General y  la Unidad para las Víctimas, ambas, aunque con cifras un poco diferentes, **concluyen que las personas desplazadas son las más pobres del total de la población colombiana.**

Respecto al dato de la pobreza extrema, la encuesta de la Contraloría, que consultó a  10.761 víctimas de desplazamiento, **concluyó que el 35,5% de los desplazados viven en la indigencia, mientras que el 80 % vive por debajo de la línea de pobreza.** Así mismo determinó que, las brechas creadas por el desplazamiento generan 11,9% de desigualdad en el de la población.

[![infografia-pobreza-contagio-radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/02/infografia-pobreza.png){.aligncenter .wp-image-4963 width="501" height="668"}](https://archivo.contagioradio.com/actualidad/en-colombia-la-poblacion-desplazada-es-mas-pobre-que-los-pobres/attachment/infografia-pobreza/)

Por otro lado, la Unidad para las Víctimas, no difiere mucho de la conclusión de la Contraloría. Esta institución encuestó a aproximadamente 28 mil hogares,  y allí se encontró que **el 33% de la población desplazada vive por debajo de la línea de pobreza, y el 63.8% vive por debajo de la línea de pobreza.** Además, en este informe, se evidencia la situación de tierras, por lo que una de esas conclusiones fue que de 536.000 grupos familiares que perdieron sus tierras, el 46% de estos corresponde a hogares desplazados.

<iframe id="doc_46782" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/256076501/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-TI0z1nDSylP4s73WRX2G&amp;show_recommendations=true" width="50%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="1.3323485967503692"></iframe>
