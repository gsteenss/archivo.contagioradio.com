Title: 'Quebrantos': un clamor desde el arte por la vida de los líderes sociales
Date: 2019-06-10 17:56
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: comision de la verdad, Doris Salcedo, lideres sociales, Quebrantos
Slug: quebrantos-un-clamor-desde-el-arte-por-la-vida-de-los-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/MG_8693.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/MG_8663.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/MG_8672.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

**'Quebrantos'**,  una obra que surge de la mente de la artista **Doris Salcedo**, congregó a líderes, líderesas, voluntarios  y a la ciudadanía en general alrededor de una obra que honra **la memoria de 165 de los más de 600 líderes sociales** asesinados después de la firma del Acuerdo de Paz y que a través de vidrio fragmentado, sintetiza y denuncia cómo se quiebra el tejido social de una comunidad con cada muerte de un defensor de derechos humanos.

La obra, previa al primer **Diálogo para la No Repetición**, es parte del trabajo que viene realizando la Comisión de la Verdad en defensa de los líderes sociales y que según su autora permite recordar a los líderes y garantizar que un "nombre sobrevive a la persona, y permite mantenerlos vivos en la memoria".

Desde las 9:00 am y hasta las 5:00 pm, cerca de 80 líderes provenientes de diferentes regiones del país, acompañados de cerca de 300  voluntarios, acudieron a la cita en la Plaza de Bolívar para rememorar a aquellos que fueron silenciados por defender los derechos de sus territorios. Bajo la lluvia y a lo largo del día, su esfuerzo se vio materializado en la elaboración de esta obra que fue escrita en 35 renglones a lo largo del recinto.

Al trabajo también se unieron los comisionados **Saul Franco y Alejandra Miller** quienes se refirieron a las amenazas contra los líderes, "tenemos una enorme preocupación, a pesar del enorme esfuerzo del Acuerdo de Paz, estamos viendo que se repite la historia y esa preocupación es parte de lo que queremos mostrarle a la sociedad", puntualizó la comisionada. ([Le puede interesar: Comisión de la Verdad expresó su respaldo a líderes sociales del Cauca)](https://archivo.contagioradio.com/comision-de-la-verdad-expreso-su-respaldo-a-lideres-sociales-del-cauca/)

> Proteger nuestros hombres y mujeres líderes es deber de todos en Colombia. La obra “Quebrantos” señor presidente [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) de Doris Salcedo es un homenaje y llanto por los ausentes. La vida es sagrada! [@DanielSamperO](https://twitter.com/DanielSamperO?ref_src=twsrc%5Etfw) [@RSilvaRomero](https://twitter.com/RSilvaRomero?ref_src=twsrc%5Etfw) [@PaulaJllo](https://twitter.com/PaulaJllo?ref_src=twsrc%5Etfw) [@fdbedout](https://twitter.com/fdbedout?ref_src=twsrc%5Etfw) [pic.twitter.com/RUPQrIhivJ](https://t.co/RUPQrIhivJ)
>
> — JesusAbadColorado (@AbadColorado) [10 de junio de 2019](https://twitter.com/AbadColorado/status/1138212482277957632?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### Hablan los protagonistas de 'Quebrantos' 

"Son muchos los que han perdido la vida y no se puede naturalizar lo que está pasando en Colombia" afirmó **Julisa Mosquera, sobreviviente del conflicto armado y consejera Distrital de Comunidades Negras** quien se unió a la elaboración de la obra, refiriéndose a las expectativas que tienen frente a la Comisión de la Verdad, "necesitamos saber quiénes fueron las personas que nos desplazaron, pero no solo los que lo cometieron sino quiénes fueron los que intelectualmente diseñaron la forma de exterminio de nuestra comunidad".

\

Por su parte**, Yeison Mosquera, líder proveniente de Riosucio** se refirió a este espacio con el que evocan a los líderes que han sido asesinados, "queremos que sepan que día a día luchamos por nuestro territorio y que porque maten a uno no nos van a callar, seguiremos de pie".[(Le puede interesar: Con Diálogos para la No Repetición, Comisión de la Verdad promoverá defensa de líderes sociales)](https://archivo.contagioradio.com/con-dialogos-para-la-no-repeticion-comision-de-la-verdad-promovera-defensa-de-lideres-sociales/)

El líder social también destacó el intercambio de experiencias con otros líderes y lideresas que llegaron desde Catatumbo, Antioquia, Putumayo  y Chocó y quienes han sido amenazados pero que hoy con 'Quebrantos' se unen a este clamor.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
