Title: Fotos: Así se desarrolla manifestación por la crisis de la salud en Colombia
Date: 2015-08-19 14:43
Category: Movilización, Nacional
Tags: crisis de la salud, ESMAD, Medellin, Mesa Nacional por el Derecho a la Salud, Ministerio de Salud, protestas, Salud
Slug: fotos-asi-se-desarrolla-manifestacion-por-la-crisis-de-la-salud-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/salud-7.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: CUT Colombia. 

El sector salud, los y las pacientes iniciaron hoy una nueva jornada de movilizaciones en todas las ciudades del país por el derecho a la salud y para exigir el pago de la deuda** que tienen las EPS´s con los hospitales. **[(Ver infografía sobre situación de la salud en Colombia)](https://archivo.contagioradio.com/en-colombia-la-salud-se-encuentra-en-cuidados-intensivos/)

Pese a que se trató de una protesta pacífica, la Alcaldía de Medellín y el ESMAD agredieron a varias personas que se encontraban realizando la manifestación en el Parque de los Deseos en **Medellín.**

Uno de los heridos se trata de** Juan Edgar Marín, gerente IPS Universitaria,** quien fue  golpeado por el ESMAD en medio del plantón.

Sin embargo, la manifestación pacífica continúa. "Estamos en las calles para llamar la atención del derecho fundamental y constitucional de la salud", dice Clemencia Mayorga, vocera de la Mesa Nacional por el Derecho a la Salud, quien resalta que **“pacientes somos todos”.**
