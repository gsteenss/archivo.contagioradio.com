Title: Dos palestinos presos podrían morir luego de 82 días de huelga de hambre
Date: 2016-12-15 11:41
Category: DDHH, El mundo
Tags: Huelga de hambre, Israel, Palestina, presos politicos
Slug: dos-palestinos-podrian-morir-33770
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/62828-e1481819813837.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Palestina Libre] 

###### [15 Dic. 2016] 

**Luego de más de 82 días de huelga de hambre Anas Shadid y Ahmad Abu Farah podrían morir**, así lo dio a conocer el Comité para Asuntos de los Presos Palestinos quienes además agregaron que **los reclusos “dejaron de beber agua el pasado miércoles y por ello su situación ha empeorado”.**

**Los dos palestinos han sido detenidos, según dicha organización internacional, sin presentar hasta el momento cargos por parte de las autoridades israelíes.** Anas y Ahmad están sometidos a una figura legal llamada “detención administrativa” que se traduce en mantener detenidos a sospechosos por un tiempo de seis meses sin presentar cargos.

Lo más grave de esta situación es que los periodos en la cárcel, dentro de esta figura legal pueden ser renovados de manera indefinida por las autoridades israelíes, aún sin que existan cargos para enjuiciarlos y encarcelarlos.

Según el Comité para Asuntos de los Presos Palestinos **Anas Shadid tiene 19 años y al haber entrado en huelga de hambre su cuerpo está mostrando los resultados** “su cuerpo apenas se puede controlar, no tiene fuerza y ha perdido más de 15 kilos” reza el dictamen médico.

Por su parte **Ahmad Abu tiene 29 años y en la actualidad resultado de los 85 días de huelga de hambre tiene problemas con la visión y el habla, así como dolor en sus extremidades.**

El informe médico al que tuvo acceso El Comité para Asuntos de los Presos Palestinos también dice que **no acceder a alimentos puede “causar daño cerebral permanente y en el corazón, así como afectar la visión, hígado, riñones y capacidad para hablar”.**

Todos los síntomas anteriormente mencionados, en la actualidad los presentan los dos presos palestinos, quienes han anunciado que ahora dejarán de beber agua que era lo único que estaban ingiriendo.

Sin embargo y pese a todos los intentos de poder conseguir su libertad o un juicio justo, **hasta el momento la instancia judicial lo único que ha manifestado es que podría congelar la detención, pero en ningún momento cancelar.**

Según cifras recientes de la Asociación de Defensa de los Prisioneros Palestinos –ADAMIR-, **Israel ha apresado cerca de 7 mil palestinos. De los cuales 720 tenían una orden de arresto administrativo. Lo más grave es que de dichas cifras 400 personas son menores de edad. **Le puede interesar: [438 niñas y niños palestinos son prisioneros del régimen israelí](https://archivo.contagioradio.com/ninos-ninas-presos-regimen-israel/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
