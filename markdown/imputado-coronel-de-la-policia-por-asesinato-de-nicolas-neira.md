Title: Imputado Coronel de la Policía por asesinato de Nicolas Neira
Date: 2018-02-09 10:16
Category: DDHH, Nacional
Tags: nicolas neira
Slug: imputado-coronel-de-la-policia-por-asesinato-de-nicolas-neira
Status: published

###### [Foto: Archivo Partícular] 

###### [09 Feb 2018] 

Fue imputado como presunto determinador **Fabián Mauricio Infante Pinzón, Coronel de la Policía Nacional, en el asesinato del menor de edad Nicolas Neira**, en los hechos ocurridos el primero de mayo de 2005, cuando el joven se encontraba participando de la marcha de los trabajadores.

Esta decisión llega después de 12 años de inicio del proceso. Por este mismo hecho, el año pasado, el juez 71 de garantías de Bogotá ordenó **medida de aseguramiento bajo detención domiciliaria en contra del agente de la Policía, Néstor Julio Rodríguez Rúa. **(Le puede interesar: ["Medida de aseguramiento contra el agente Rodríguez Rúa por asesinato de Nicolás Neira"](https://archivo.contagioradio.com/esmad_nicolas_neira_rodriguez_rua/))

Según testigos, Rúa fue quien disparó el “lanzagases” que golpeó fuertemente la cabeza de Nicolás, y luego de estar dos días hospitalizado falleció. **Sin embargo, durante la audiencia este agente no aceptó los cargos imputados.**

Otra de las personas implicadas en este caso es Julio César Torrijos Debia a quien el año pasado, el Juez 14 del circuito Penal de Bogotá le dio una pena de 4 años y medio. **Torrijos aceptó que cuando fue jefe del ESMAD encubrió a sus subalternos** que golpearon con puños y patadas al joven y a Néstor Julio Rodríguez al disparar el gas.

Noticia en desarrollo ...
