Title: Sí hay paramilitares en el Catatumbo: Comisión de verificación
Date: 2017-02-20 13:03
Category: DDHH, Nacional
Tags: Catatumbo, ejercito, paramilitares, paz
Slug: comision-de-verificacion-en-catatumbo-verifico-denuncias-sobre-presencia-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/paramilitares_gaitanistas_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Universal] 

###### [20 Feb. 2017] 

A través de un comunicado, los integrantes de la Comisión aseguraron haber documentado la **amenaza que grupos paramilitares han realizado contra los pobladores de la zona**, en la que aseguran que “si no trabajan con ellos por las buenas, morirán”. Situación que mantiene en continua zozobra a los campesinos del Catatumbo.

Sin embargo continúa el recorrido de la Comisión de Verificación “solidaridad con el Catatumbo”, de la que hacen parte organizaciones sociales y medios de comunicación, que ya se han reunido con campesinos de la zona y **han constatado a través de sus testimonios la presencia de paramilitares en su territorio**, además del abuso de fuerza por parte de militares puesto que se evidenció la presencia de militares a pocos metros del sitio de la reunión.

Estas situaciones han obligado a los pobladores a cambiar sus dinámicas diarias, incluso optando por **no enviar a sus hijos a las escuelas como  mecanismo para salvaguardar sus vidas**. Le puede interesar: [ASCAMCAT denuncia aumento de amenazas y hostigamientos a sus dirigentes](https://archivo.contagioradio.com/ascamcat-denuncia-aumento-de-amenazas-y-hostigamientos-a-sus-dirigentes/)

Así mismo, la Comisión logró verificar a través del acceso al informe final realizado por el Frente Fronterizo por la Paz, organización de derechos humanos en Venezuela, que **cerca de 402 personas han optado por buscar refugio en el vecino país. **De igual modo, la Agencia de la ONU para los refugiados (ACNUR) manifestó que desde el pasado 11 de febrero un grupo de personas cruzó la frontera en busca de protección desde el corregimiento de La Gabarra, Norte de Santander, Colombia, hacia el Sector “El Cruce”, estado Zulia, Venezuela. [Comunicado ACNUR](http://www.acnur.org/noticias/noticia/respuesta-humanitaria-ante-llegada-de-personas-en-necesidad-de-proteccion-internacional-provenientes-del-catatumbo-colombiano-al-estado-zulia/)

De otro lado, la Comisión fue testigo de la fuerte presencia de miembros del Ejército Nacional como respuesta a la situación que se vive en Catatumbo. Le puede interesar: [Paramilitares amenazan de tomar el control en zona de La Gabarra, Norte de Santander](https://archivo.contagioradio.com/paramilitares-amenazan-de-tomar-el-control-en-zona-de-la-gabarra-norte-de-santander/)

Sin embargo, los pobladores fueron insistentes “en la **desconfianza que les genera la Fuerza Pública, debido a los antecedentes de violaciones a los derechos humanos** y la comprobada relación con el paramilitarismo que incursionó en la región desde el año de 1999” dice el comunicado.

Ante estas graves situaciones que deben enfrentar los pobladores del Catatumbo, **la Comisión ha expresado la falta de respuesta estatal**, encontrando como única solución la excesiva militarización de la zona, lo que se ha traducido en el aumento de casos de abuso por parte de la Fuerza Pública, tal como se ha denunciado por las comunidades.

La Comisión aseguró que continuará recibiendo las denuncias de los pobladores y próximamente dará a conocer un informe preliminar  detallado en el que se dé cuenta de la situación en el Catatumbo. Le puede interesar: [Presencia Paramilitar impide llegada de FARC-EP a zona veredal](https://archivo.contagioradio.com/presencia-paramilitar-impide-llegada-de-farc-ep-a-zona-veredal/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
