Title: Jóvenes, ¡a participar!: la paz está en sus manos
Date: 2019-10-25 13:39
Author: Foro Opina
Category: Opinion
Tags: democracia colombiana, Jovenes de Colombia, Votaciones, votar
Slug: jovenes-a-participar-la-paz-esta-en-sus-manos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/59e2a9c416e47.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

##### [Foto por: Jaime Moreno] 

\*Por la Fundación Foro Nacional por Colombia

**  **

Se acercan las elecciones regionales y las maquinarias tradicionales ya tienen listas sus cartas en cada uno de los departamentos, municipios y localidades para continuar con su juego, en el que por muchos años han sometido a Colombia.

#### Es este el panorama que reclama una alternativa. 

De acuerdo con el Observatorio de la Democracia de la Universidad de Los Andes, los jóvenes son los que menos votan en Colombia:

-   seis de cada diez dicen no confiar en las instituciones democráticas y siete de cada diez piensan que a los gobernantes no les interesan sus opiniones; ello ha propiciado que sólo cuatro de cada diez jóvenes en el país salgan a votar.

Este fenómeno se evidencia en diversas partes del mundo. En Reino Unido, quienes tomaron la decisión de retirarse de la Unión Europea fueron, por un lado, la clase tradicional conservadora que culpa al continente de sus problemáticas internas, y, por otro, los adultos mayores que se quedaron siglos atrás y continúan con la idea de una Inglaterra sin migrantes.

Posteriormente, cuando se analizaron las consecuencias del **Brexit** y los jóvenes se enteraron de que no podrían salir del país tan fácilmente, las oportunidades educativas y laborales se disminuirían en gran medida y la economía del país se vería afectada, se percataron del gran error de no participar en las decisiones públicas.

#### En Colombia, el plebiscito sobre los Acuerdos de Paz de 2016 demostró el mismo resultado. 

La clase conservadora tradicional promovió a través de un discurso de odio la continuación de la guerra, y claramente logró su objetivo conquistando el *50,21%* de votos a favor del NO. Días después se escuchaba por las calles y redes sociales el sinsabor, especialmente de jóvenes, que por pereza, desconocimiento, confianza o simple desinterés no salieron a las urnas a marcar un nuevo rumbo para Colombia. Dicho arrepentimiento se conoció como *“plebitusa”* y logró que las movilizaciones realizadas días después fueran masivas y con gran presencia juvenil que reclamaba, y sigue reclamando, la paz para nuestro país.

Y es que al parecer la juventud no ha entendido la importancia de su participación en la construcción de la democracia.

***La voz de los jóvenes representa innovación, nuevos ideales para la resolución de conflictos, la construcción de paz y las problemáticas que la clase tradicional no ha podido resolver.*** Ellos conocen la realidad desigual e injusta del país porque deben vivirla diariamente: la falta de oportunidades académicas y laborales, la baja calidad en la educación, la inseguridad en las ciudades, el precario reconocimiento a la diversidad y a la libertad de expresión, el escaso apoyo al desarrollo deportivo y cultural, y, sobre todo, la falta de incentivos para alcanzar sus sueños.

(le puede interesar:[La paz en Colombia es paz con participación o no es paz](https://archivo.contagioradio.com/la-paz-en-colombia-es-paz-con-participacion-o-no-es-paz/))

Por ello, es fundamental la participación de los jóvenes en las decisiones que determinan el rumbo de Colombia, lo cual no se logra únicamente por medio de la afiliación a un partido político, sino de una serie de dispositivos donde se legítima la capacidad de gestionar y contribuir a la transformación social por medio de nuevas narrativas como lo son: *el arte, la música, la vinculación y activismo en los movimientos sociales, las manifestaciones pacíficas físicas y virtuales, etc.*, todo lo anterior visto como acciones que irrumpen esos espacios de discusión cerrados e históricamente manipulados por las más antiguas fuerzas del poder político, para lograr cambios estructurales.

### Es hora de cambiar la historia. 

Es el momento que los jóvenes sientan su país así como lo sienten cuando juega la selección. Estamos con la tinta en las manos para empezar a escribir una nueva historia, una historia que no esté basada en odios, diferencias, sangre, balas, una historia que incluya todos los sectores de la sociedad, sin importar apellidos, origen, costumbres ni orientaciones.

Por lo anterior, el llamado es a informarse. Que las redes sociales no solo cumplan una función de entretenimiento sino también de pedagogía y de generar opinión pública entre los jóvenes, para que la decisión que se tomará el 27 de octubre desmienta las voces de ciertos adultos al decir que esta juventud de hoy en día no sirve para nada. Es fundamental que los jóvenes contribuyan a darle un giro a la política colombiana tradicional y dejen atrás la indiferencia frente a las decisiones públicas que a corto y largo plazo los afecta igualmente. Así que jóvenes, **¡a participar!**, la paz en Colombia está en sus manos.

###### \*La Fundación Foro Nacional por Colombia es una organización civil no gubernamental sin ánimo de lucro, creada en 1982 por iniciativa de intelectuales colombianos comprometidos con el fortalecimiento de la democracia y la promoción de la justicia social, la paz y la convivencia. El propósito de nuestro trabajo es crear las condiciones para el ejercicio de una ciudadanía activa con capacidad de incidencia en los asuntos públicos. El pluralismo, la participación ciudadana, la concertación democrática, la corresponsabilidad y la solidaridad son la base para el desarrollo de nuestra misión, con un enfoque diferencial (de género, generación y etnia). Desde sus inicios, Foro rechazó la violencia como forma de acción política. Por ello cobijó la propuesta de una salida negociada al conflicto armado y del fomento de una cultura democrática. Luego de la firma del Acuerdo entre el Gobierno y las FARC, Foro ha orientado su quehacer hacia el objetivo estratégico de contribuir a la construcción de la paz y la convivencia en Colombia. Foro es una entidad descentralizada con sede en Bogotá y con tres capítulos regionales en Bogotá (Foro Región Central), Barranquilla (Foro Costa Atlántica) y Cali (Foro Suroccidente).  
Contáctenos: 316 697 8026 –   (1) 282 2550 
