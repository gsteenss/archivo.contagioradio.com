Title: 60 mil firmas recolectadas para cabildo abierto contra venta de ETB
Date: 2016-09-19 15:14
Category: Movilización
Tags: ETB, privatizacion etb, Sintrateléfonos, Venta de la ETB, Venta ETB
Slug: 60-mil-firmas-recolectadas-para-cabildo-abierto-contra-venta-de-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/cacerolazo-etb.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo - Contagio Radio ] 

###### [19 Sept 2016] 

Este martes desde las 7 de la mañana, se darán cita organizaciones sindicales, estudiantes y ciudadanos en la Plazoleta Eduardo Umaña Mendoza para marchar hacia el edificio de la Registraduría Distrital, en donde **serán radicadas las 60 mil firmas que Sintratelefonos recolectó**, con el fin de convocar un cabildo abierto en defensa de la Empresa de Teléfonos de Bogotá, ETB.

Luis Casallas, Secretario General de Sintratelefonos, asegura la recolección de firmas inició **el pasado mes de mayo cuando el Concejo de Bogotá aprobó la venta de la empresa**. La Registraduría avaló la iniciativa y estableció que para lograr la convocatoria del cabildo abierto, debían ser recolectadas por lo menos 27 mil firmas. La organización logró recolectar 33 mil firmas más de las que les habían exigido.

Según Casallas éste es un hecho histórico para las organizaciones sindicales del país, pues es la primera vez que logran convocar una **reunión pública del Concejo distrital en la que la ciudadanía podrá participar de forma directa**, y discutir asuntos de interés como lo es la venta de la Empresa de Teléfonos de Bogotá.

De acuerdo con el líder sindical, [[la ETB no debe ser vendida](https://archivo.contagioradio.com/venta-de-etb-sera-demandada-por-sindicatos/)] pues es el único operador público que queda en Colombia, y esto es primordial si se tiene en cuenta que **la seguridad de los Estados depende de su control de las telecomunicaciones**. Para los sindicatos y organizaciones ciudadanas la empresa representa un icono histórico de 132 años.

Desde que fue aprobada la venta, Sintratelefonos interpuso dos [[acciones judiciales contra la medida](https://archivo.contagioradio.com/presidente-de-la-etb-sera-demandado-por-dar-informacion-erronea-y-parcializada/)], una nulidad y un acto administrativo, que se espera sean resueltas en favor de los demandantes. Durante los siguientes veinte días, la Registraduría revisará si las firmas recolectadas cumplen con los parámetros para que el **Concejo de Bogotá convoque a la ciudadanía a una plenaria**.

Organizaciones sindicales como Sintratelefonos manifiestan estar a favor de los acuerdos de paz y apoyar el sí en el plebiscito; pero insisten en que las privatizaciones dificultan la construcción de una paz estable y duradera, teniendo en cuenta que han propuesto la consolidación de un convenio interinstitucional para la implementación de **laboratorios de informática a través de los que sea posible poner en marcha procesos educativos para quienes dejen las armas**.

<iframe id="audio_12970571" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12970571_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
