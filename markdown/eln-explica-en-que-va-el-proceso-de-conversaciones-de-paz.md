Title: ELN explica en qué va el proceso de conversaciones de paz
Date: 2016-05-27 16:48
Category: Nacional, Paz
Tags: colombia, ELN, ELN Nicolas Rodríguez Bautista, Proceso de conversaciones de paz
Slug: eln-explica-en-que-va-el-proceso-de-conversaciones-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/ELN-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: video Contagio Radio] 

###### [27 May 2016]

En una nueva entrevista, el comandante de la guerrilla del ELN, Nicolás Rodríguez explica las demoras y el **momento actual del proceso de conversaciones de paz** que estaba previsto para tener sus primeras sesiones desde el mes de Mayo. Además expresa la opinión de esa guerrilla frente al fallido proceso de ubicación de los restos de Camilo Torres, tras conocerse el veredicto de Medicina Legal quien informo que los restos no correspondían al sacerdote.

Rodríguez Bautista, opina sobre las recientes declaraciones del presidente Santos, en el sentido de que las conversaciones no se iniciarán hasta que esa organización guerrillera desista de la **práctica del secuestro, en palabras de Bautista ese punto no hace parte del acuerdo inicial y se abordará en el punto 5 de la agenda.**

En el video también se hace referencia a los **aspectos que han demorado el proceso**, afirmando que en un primer momento el gobierno extendió los tiempos “de manera caprichosa” y en un segundo momento se esperaba que la agenda fuera más general que la que quedó, que incluye, de manera determinante a la sociedad civil. En último momento explica Gabino que se dio una discusión en torno a los países que serían la sede de las conversaciones.

Según el comandante del ELN, la presencia de **Cuba, Venezuela, Brasil, Ecuador y Chile** ha sido fundamental en el proceso de acercamientos y por ello tanto el gobierno como esa guerrilla confían en los buenos oficios de esos países, “se lo merecen, lo han hecho bien y creemos que son garantía” afirma Bautista.

Así mismo resaltó que las voces que afirman que el gobierno está entregando el país a la insurgencia son las voces que quieren que las condiciones sociales en Colombia sigan como están y por ello manifiestan su desacuerdo con los procesos de paz. ***“La población (…) ha dicho que la paz son cambios y no cualquier tipo de cambios”*** además “solamente si se superan las causas que generaron el alzamiento en armas podremos hablar de una paz auténtica en Colombia”

Rodríguez Bautista finaliza recordando que el proceso de búsqueda de los restos del sacerdote **Camilo Torres** ha sido “una farsa” del General Tovar porque se han realizado diferentes afirmaciones sobre el paradero de los restos. Señaló que lo que se ha conocido ha sido una **irresponsabilidad del Estado con “un asunto tan delicado”.**

\[embed\]https://www.youtube.com/watch?v=PLUoQDp7Z7I\[/embed\]
