Title: Duro reclamo de víctimas tras objeciones de gobierno Duque contra la JEP
Date: 2019-03-13 20:26
Author: CtgAdm
Category: Judicial, Paz
Tags: comunidades, Duque, JEP, paz
Slug: dura-carta-victimas-tras-objeciones-gobierno-duque-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/mujeres-rurales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [Mar 13 2019]

Cerca de 100 comunidades, familias, resguardos indígenas, zonas humanitaria y zonas de Biodiversidad que agrupan a personas que han sufrido los rigores del conflicto armado que se vive desde hace décadas en el país aseguran que las objeciones presentadas por el gobierno y su intención de plantear una reforma constitucional al Acto Legislativo 001 de 2017 son una burla al derecho a la paz.

Sin embargo manifestaron su apoyo a víctimas, ex paramilitares, militares, empresarios y todas las personas que han manifestado su interés de respaldar la construcción de la paz a través del acceso a la JEP y a los mecanismos del Sistema Integral de Verdad, Justicia, Reparación y No Repetición previstos en el acuerdo de paz.

En una comunicación las organizaciones y comunidades hacen un llamado a los “Humanos del Mundo” pues consideran que las objeciones y la reforma constitucional planteadas afectan gravemente su derecho a la verdad y promueven un enfrentamiento en los poderes del Estado Social de derecho que también vulnera el derecho a la verdad.

Según la comunicación objetar la ley estatutaria de la JEP “es grave pues valiéndose de su facultad de hacer objeciones desconoció la independencia de poderes, en articular de la corte constitucional y pretende mostrar como inconveniente lo que en realidad es una interpretación para los intereses de quienes se benefician del conflicto armado y temen a las verdades”. Le puede interesar: [Seis argumentos que desvirtúan las objeciones a la JEP](https://archivo.contagioradio.com/desmienten-objeciones-duque-jep/)

### **En la Fiscalía reposan miles de expedientes con denuncias contra el Estado que no prosperan** 

Pero las comunidades fueron más allá al manifestar que “evitar, dilatar y entorpecer” las verdades y el derecho restaurador son los verdaderos propósitos detrás de las acciones que ha emprendido el gobierno en contra del Sistema Integral de Verdad, Justicia, Reparación y No Repetición, y que además desconoció el pensar de ellos y ellas que respaldan la aplicación de la justicia restaurativa.

En la comunicación afirmaron también que no es cierto que la fiscalía no pueda seguir operando pue sigue conociendo de todos los delitos pero recordaron que en esa institución reposan expedientes de crímenes de Estado como el desplazamiento forzado, la violencia sexual y otros que no tienen ningún avance en materia de investigación o justicia.

### **Todos los incumplimientos del acuerdo de paz recrudecen la situación de violencia en los territorios** 

Los firmantes de la comunicación también aseguran que el incumplimiento en los PNIS, los PDTS y los demás puntos del acuerdo de paz sobre los que no hay avance son el detonante de nuevas olas de violencia, “paramilitarismo de Estado” y los preparan para una nueva ola de guerra, zozobra, confinamiento y graves violaciones a los Derechos Humanos.

Por último hicieron un llamado al congreso para que legisle en favor de los intereses de las víctimas en los territorios. Tambi{en invitaron al ELN a que a través de acciones territoriales positivas abran el camino para el fin de la violencia. De igual forma se dirigieron a empresarios, militares y todos aquellos que pueden contribuir con la verdad a la construcción de la paz.

###### Reciba toda la información de Contagio Radio en [[su correo]
