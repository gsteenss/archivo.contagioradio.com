Title: Bogotá mejor para todos -menos para los árboles-
Date: 2018-09-11 11:26
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Árboles, Enrique Peñalosa, Secretaría de Ambiente, tala árboles
Slug: bogota-menos-arboles
Status: published

###### [Foto: @JavierAArango] 

###### [10 Sept 2018] 

La tala de árboles de forma masiva ha sido una constante de la administración de **Enrique Peñalosa.** Lo ocurrido en Bosque Bavaria, el Parque de la 93 y en la **carrera 9ª** dan cuenta de ello. Sin embargo y a pesar de que es necesaria la intervención para derribar árboles enfermos, la manera en que se está haciendo afecta a los pájaros, los habitantes y el ambiente de Bogotá.

**Pedro Camargo, miembro de la Asociación Bogotana de Ornitología (ABO)** advierte que "está **tala tan inmediata y certera en muchos lugares puede afectar a varias especies de aves migratorias que están llegando a la ciudad por estos días"**. Muchas de esas especies son parte del ciclo de captura de carbono, y si se afecta los lugares en los que viven, se afecta el ambiente de la segunda capital más contaminada de Colombia. (Le puede interesar: ["ANLA aprueba licencias ambientales en el único bosque de niebla del Tolima"](https://archivo.contagioradio.com/anla-licencia-bosque-niebla/))

El experto afirma que las especies migratorias que están llegando en este momento a Bogotá son importantes para la ciudad porque consumen insectos y polinizan flores. Pero debido a las talas, se están perdiendo hábitats que son esencialmente grandes, y se los reemplaza por árboles pequeños que no pueden recibir las mismas especies. (Le puede interesar: ["Más agua y menos carbón, la pelea de la comunidad de Cucaita con la minería"](https://archivo.contagioradio.com/56242/))

Camargo afirma que **las talas deben realizarse, pero de forma controlada y programada** con tiempo para que se pueda dar la renovación necesaria, de tal forma que no se afecte el hábitat de los animales. Por otra parte, el integrante de la ABO sostiene que es importante sembrar especies nativas, "porque **el distrito siembra arboles por temas de diseño y de arquitectura, pero que no son los más ideales para la biodiversidad".**

### **La tala de árboles en propiedad privada también es un problema ** 

En días recientes, **la Fiscalía anunció una investigación contra una presunta red que agilizaba los permisos necesarios ante la Secretaría de Ambiente** para talar árboles ubicados en propiedades privadas. Los miembros de esta red estafaban a los ciudadanos asegurándoles que tenían contactos en el interior de la Secretaría, para agilizar estos trámites que podrían demorar varias semanas.

<iframe id="audio_28492223" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28492223_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
