Title: Vender Isagén: El peor negocio del mundo (II)
Date: 2015-05-21 10:59
Author: CtgAdm
Category: Opinion, Ricardo
Tags: bolsa de valores medellin, ISA, ISAGEN
Slug: vender-isagen-el-peor-negocio-del-mundo-ii
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/ISA-1993.pdf" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Ley_143_1994.pdf" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [Ricardo Ferrer Espinosa ](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/) 

Vender ISAGEN y los bienes estratégicos del Estado, no es un invento de los últimos gobiernos [(1)](http://justiciatributaria.co/el-peor-negocio-de-la-historia-nacional/).

El proceso inició en la década de los 80´s, cuando los gobernantes de Colombia se subordinaron al Consenso de Washington. En 1993 se anunció la reestructuración del sector eléctrico colombiano y un año después se publicó la Ley 143 de 1994 ([2](http://www.desdeabajo.info/ediciones/item/25918-la-venta-de-isagen-una-practica-para-privatizar-el-estado.html)) que abrió la puerta a la privatización de los servicios públicos. Sumisos, Pastrana, Uribe y Santos siguieron rematando bienes estratégicos, como política de Estado.

El 27 de marzo de 1994, como corresponsal de Colprensa, publiqué la nota económica del balance anual, presentada por el gerente general de ISA, Javier Gutierrez Pemberthy (3. [ISA-1993](https://archivo.contagioradio.com/vender-isagen-el-peor-negocio-del-mundo-ii/isa-1993/))  Entonces ISA presentaba utilidades por 171.971 millones de pesos. Luego de mostrar excelentes resultados, el gerente de ISA hizo los anuncios cuyas consecuencias se evidencian hoy: “La empresa desarrolló un proceso de transformación que busca materializar los objetivos de la **política de reestructuración del sector eléctrico colombiano: aceptar la competencia, darle participación al sector privado** e incrementar la eficiencia” (4. [Ley\_143\_1994](https://archivo.contagioradio.com/vender-isagen-el-peor-negocio-del-mundo-ii/ley_143_1994/))

El gerente soltó otro aviso: ISA separa sus actividades. “Somos tan fuertes que damos para la creación de dos empresas. **ISA es como una madre que se sacrifica para la creación de dos grandes hijos.** El uno, dedicado a la Generación  y el otro, dedicado a la Transmisión y despacho de energía”.  Conmovedor.

Para dividir la empresa, asignaron el 75% de los activos a la Empresa de Generación \[hoy ISAGEN\] y el 25% a la Empresa de Transmisión y Despacho de Energía”.

Recordemos que ISA había sido recapitalizada por el gobierno entre 1989 y 1993. Se saneó la cartera, se normalizaron relaciones con los clientes y se puso al día el pago a proveedores, contratistas y pagos a la nación. Y justo cuando la empresa está recapitalizada con dinero público, sin deudas y muy lucrativa, es cuando se le abre la puerta al capital privado. Por esta vía se socializaron los gastos y se privatizó una parte de las ganancias (5).

Javier Gutiérrez anunciaba la admisión de capital privado: “El proceso se hará en condiciones democráticas, a través de la Bolsa de Valores de Medellín. Aquí no se está pensando en venta de activos, sino de vinculación por vía de las acciones. Ello permitirá aportes nacionales y extranjeros, públicos y privados, regionales y nacionales”.

Observemos un hecho notable: esta rueda de prensa, en ISA, se realiza en marzo de 1994. Pocos meses después, el 11 de julio se aprueba la Ley 143, cuyo texto coincide, palabra por palabra, con los anuncios del gerente Javier Gutiérrez.  En 1995 se creó el Comité de Participación Privada por parte del Consejo Nacional de Política Económica y social...

Luego de iniciar la privatización de ISAGEN, Gutiérrez pasó a ECOPETROL (2007), donde siguió con las mismas mañas (6).

#### [Vender ISAGEN: El peor negocio del mundo (1er parte)](https://archivo.contagioradio.com/vender-isagen-el-peor-negocio-del-mundo-1er-parte/ "Vender ISAGEN: El peor negocio del mundo (1er parte)") 

###### **[l](http://www.desdeabajo.info/ediciones/item/25918-la-venta-de-isagen-una-practica-para-privatizar-el-estado.html) La venta de ISAGEN, una práctica para privatizar el Estado** 

###### [(2) Ley 143 de 1994 (Julio 11), Por la cual se establece el régimen para la generación, interconexión, transmisión, distribución y comercialización de electricidad en el territorio nacional– establece el régimen de las actividades del sector eléctrico colombiano.   ] 

###### [(3) 1993: ISA CONSOLIDÓ SU GENERACIÓN DE ENERGÍA. El Colombiano, 27 de marzo de 1994. \[recuadro\]] 

###### [(4) Colombia: análisis del sector eléctrico. CAF. Año 4, número 3, Septiembre de 2006. PDF.] 

###### [(5) La privatización de la energía eléctrica en la costa Atlántica generó mal servicio y tarifas  extremadamente altas. En Chile, luego de privatizar el sistema, se paga la energía más alta de todo el continente.] 

###### [[(6) “ISA tiene una destacada posición en el mercado de capitales, principalmente por la exitosa democratización del 28.19% de su]propiedad accionaria”.  En: web de Wharton Bogotá. Biografía del conferenciante.][[http://www.whartonbogota09.com/bio-jg-s.htm]l](http://www.whartonbogota09.com/bio-jg-s.html) 
