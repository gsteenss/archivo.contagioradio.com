Title: Las 6 respuestas a las víctimas sobre la Jurisdicción Especial de Paz
Date: 2015-12-29 12:15
Category: Entrevistas, Paz
Tags: Conversaciones de paz de la habana, ELN, FARC, Juan Manuel Santos, jurisdicción especial para la paz
Slug: victimas-y-jurisdiccion-especial-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/fo1-mundo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto: laportadacanada 

<iframe src="http://www.ivoox.com/player_ek_9916477_2_1.html?data=mp6emJmbe46ZmKialJuJd6KpkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMLnjJuY1MrXtNbZ1NnO1ZDFb83V1JDjh6iXaaK4xNnWz8bXb8bijNHOja%2FZtsrnxc7Qxc6Jh5SZo5jbjarXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Enrique Santiago, Abogado] 

###### [29 Dic 2015] 

Contagio Radio transmitió las preocupaciones que han expresado diversas víctimas y organizaciones de defensa de los derechos humanos en torno al [acuerdo firmado](https://archivo.contagioradio.com/?s=jurisdicci%C3%B3n+especial+de+paz) entre el gobierno colombiano y la guerrilla de las FARC. El abogado Enrique Santiago, uno de los gestores de la jurisdicción aclara algunas de estas dudas.

### **Sobre la cadena de mando**

Una de las principales críticas de las víctimas y de las organizaciones que las representan tiene que ver con la duda sobre la posibilidad de que los altos responsables de crímenes contra los DDHH queden en la impunidad o no atiendan su responsabilidad ante la justicia. Frente a ello el abogado, gestor del acuerdo de la Jurisdicción Especial para la Paz, afirma que en casos como las ejecuciones extrajudiciales los altos mandos van a tener que asumir su responsabilidad, y el acuerdo prácticamente coincide con el estatuto de la Corte Penal Internacional.

Solamente podría haber exención de responsabilidad si el alto mando logra demostrar que no tenía forma de conocer lo que estaba sucediendo. Además los altos oficiales deberían acreditar esa posibilidad, que se considera casi imposible en el marco de la organización de las Fuerzas Militares. “Se partiría de la premisa de que efectivamente esa responsabilidad existe por cadena de mando y sería la persona acusada por cadena de mando la que tendría que acreditar que por cualquier causal la cadena de mando no funcionó correctamente”

### **Human Rights Watch ¿está confundiendo la impunidad con penas de prisión?** 

Respecto de lo afirmado por José Miguel Vivanco, Enrique Santiago afirma que las críticas de HRW vienen siendo recurrentes desde que se anunció el acuerdo y en desconocimiento del texto. “el señor Vivanco salió de una forma imprudente” y la situación que se está dando es de “sostenerla y no enmendarla” además señala que algunos portavoces confunden impunidad con penas de cárcel.

El acuerdo incluye que se van a imponer sanciones en caso de que se reconozca la verdad, pero en caso de que no se reconozca habría penas de prisión de hasta 20 años, “no es tiempo de estar confundiendo la impunidad con la ausencia de penas de prisión, en cualquier Estado democrático moderno se contemplan penas restaurativas y retributivas, y agrega Santiago que no habrá impunidad por esa vía.

Lo que podría hacer la organización internacional HRW es decir que las penas contempladas en la JEP “no son de su gusto” y deberían revisar los últimos tratados de paz alcanzados en el mundo y comparar con el acuerdo alcanzado en la Habana, “todos los acuerdos de paz que se habidos hasta la fecha se han saldado con amplias amnistías, este no es el caso” según afirma Enrique Santiago.

### **Todas las acciones de una fuerza legal ¿se presumen legales?** 

Esta es otra de las preocupaciones que señalan algunas víctimas acerca de lo afirmado por el presidente Juan Manuel Santos. La respuesta del jurista redactor del acuerdo es que lo que dice el presidente es ajustado a la realidad pero eso no significa que “se parte de la premisa de que los funcionarios del Estado no cometen delitos” y el acuerdo contempla este punto de acuerdo al estatuto de la misma Corte Penal Internacional.

### **Los delitos graves y representativos ¿cómo se definen?** 

El abogado señala que hay un error en la apreciación que tiene que ver con el direccionamiento de la acción penal en la JEP. El abogado señala que en la JEP no hay una acción penal dirigida especialmente a los crímenes más graves y a los máximos responsables sino a todas las personas que sean responsables de los delitos que no sean objeto de amnistía.

Según el abogado en ninguno de los casos se renuncia a la persecución penal por parte del tribunal, lo que si contempla es que los tribunales podrán establecer un orden de prioridad a algunos casos pero eso no significa que se deja de perseguir a personas que cometieron crímenes de no objeto de amnistía. Los tribunales podrán empezar por los crímenes más graves pero no sucederá que los demás delitos y los demás responsables no van a ser perseguidos.

### **Para los militares ¿sólo aplican infracciones al DIH?** 

Para integrantes de la guerrilla aplicarían los llamados delitos políticos o conexos puesto que están por fuera del marco legal, lo que no sucede para los integrantes de las FFMM porque son una fuerza legal. Lo que si es necesario resaltar es que los militares tendrán un trato diferenciado pero no igual a los insurgentes, no es una amnistía, sino que se entiende que los crímenes son diferentes a los contemplados en el derecho internacional.

### **¿Habrá algún tribunal en segunda instancia?** 

Frente a esta preocupación Enrique Santiago señala que una de las ventajas del Tribunal Especial para la Paz es que no tiene un plazo de caducidad, es decir que la temporalidad es indefinida y podrá esclarecer todas las denuncias que se le presenten, además el estatuto de la Corte Penal Internacional contempla que si no hay justicia, aún después del paso por el Tribunal Especial, se podrá acudir a instancias internacionales como la propia Corte Penal Internacional.

Según el abogado y a manera de conclusión, habrá tiempo para seguir estudiando el acuerdo y si persisten las preocupaciones en torno a la posibilidad de impunidad se tendrán que argumentar y poner de manifiesto para intentar corregir los problemas que pueda tener cuando se defina el funcionamiento propio del tribunal.
