Title: Estudiantes logran histórico acuerdo en U.Distrital
Date: 2020-02-01 18:56
Author: CtgAdm
Category: Estudiantes, Movilización
Tags: asamblea, Movimiento estudiantil, Universidad Distrital
Slug: estudiantes-logran-historico-acuerdo-en-u-distrital
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Universidad-Distrital-en-Protesta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @jerojasrodrigue {#foto-jerojasrodrigue .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tras cuatro meses en paro, la noche del pasado viernes 31 de enero, el Consejo Superior Universitario (CSU) de la Universidad Distrital aprobó la creación de una asamblea para garantizar la participación del estudiantado en la toma decisiones de la institución.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este hecho se da luego de que, desde el pasado 30 de octubre, el movimiento estudiantil de la Universidad se declarará en asamblea permanente y exigiera como máxima condición para levantar el paro, la creación de un mecanismo de participación que contara con ellos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta asamblea, de acuerdo con la secretaria de Educación Edna Bonilla, será un órgano de participación y deliberativo de la comunidad educativa, cuya finalidad será tratar asuntos entorno a las políticas y reformas a los estatutos de la Universidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, la asamblea estará conformada por 100 miembros: 40 representantes de docentes, 40 representantes de estudiantes de pregrado y posgrado, 10 representantes de servidores públicos (no docentes), empleados administrativos y trabajadores oficiales, y 10 representantes de los egresados.  (Le puede interesar: ["En manos de Claudia López está la posibilidad de levantar el paro en la Universidad Distrital"](https://archivo.contagioradio.com/en-manos-de-claudia-lopez-esta-la-posibilidad-de-levantar-el-paro-en-la-distrital/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El encargado de convocar el escenario será el rector, que además, tendrá que hacerlo cada dos años por un periodo máximo de tres meses. Con esta aprobación se crea también el primer mecanismo de participación educativa de esta índole en las universidades públicas de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, el regreso a clases para la comunidad académica de la Universidad Ditrital se pactó para el próximo 10 de febrero, a su vez se espera que este año se logre cursar los periodos educativos de 2019-3, 2020-1 y 2020-3.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
