Title: 5 Ríos de Colombia gravemente afectados por la minería
Date: 2017-01-28 16:51
Category: Ambiente, Voces de la Tierra
Tags: Mineria, ríos
Slug: 5-rios-colombia-gravemente-afectados-la-mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Rio-sambingo-e1485623965155.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Tercera Divisiión del Ejército 

###### [28 Ene 2017] 

[Cuando se realiza minería las fuentes hídricas se ven seriamente afectadas debido a los residuos sólidos que son vertidos durante la actividad minera. Los resultados de esto se traducen en un mayor contenido de sedimentos, lo que causa que los ríos sean desviados de sus cauces naturales generando inundaciones y cambios en el paisaje, así como afectaciones a las comunidades, quienes algunas veces se ven expuestas a perder sus cultivos de pancoger, y además por el nivel de contaminación no pueden consumir agua de estos afluentes. Aquí les mostramos algunos ejemplos, de cómo la minería está afectado los ríos del país.]

### [Río  Ranchería] 

**La Serranía de Macuira y el río Ranchería**[, han sido afectados tanto por el cambio climático, como por los 30 años de extracción carbonífera por parte de El Cerrejón, esta última, las comunidades indígenas y afro la señalan como la principal causa. Según denuncias,]la empresa minera usa las aguas del río y luego vierte en su cuenca las aguas contaminadas resultantes.[ Afluentes de las cuales depende el abastecimiento de agua y alimentos de miles de familias.]

[Sumado a lo anterior, tas la desviación y puesta en marcha del proyecto multipropósito del río Ranchería en el año 2010, el panorama que se visibiliza hoy en La Guajira es desolador. De acuerdo con un informe de la Procuraduría actualmente la represa no es utilizada para los propósitos para los que fue edificada, pues no está suministrando el líquido vital a las comunidades ya que no se permite el paso del agua, y en cambio,]únicamente está al servicio de los grandes arroceros y de la multinacional Carbones de El Cerrejón.

### [Río Sambingo] 

[El río Sambingo, ubicado en el departamento del Cauca, se encuentra totalmente seco, y aunque este tipo de hechos el gobierno nacional los asocia con las altas temperaturas generadas por el fenómeno de El Niño, las comunidades que viven del río aseguran que más allá de eso,]**el río ha perdido el agua por de la actividad minera legal e ilegal** que se realiza en la zona.

[L][a población ha hecho las denuncias correspondientes al Ministerio de Ambiente, Interior y Minas y Energía, pero aseguran que no se ha hecho nada para impedir la actividad ilegal. Para los pobladores, el Estado es quien tiene la mayor parte de la responsabilidad frente a la sequía que atraviesa el río Sambingo, que afecta a aproximadamente a]80 mil personas que viven del caudal del afluente. [La comunidad afirma que hay dos batallones en el lugar, pero la fuerza pública no ha hecho nada para detener la actividad minera.]

### [Río Caquetá] 

Un estudio realizado por Corpoamazonia[ en alianza con la]Universidad de Cartagena[ evidenció, la actividad minera criminal tiene contaminadas las aguas del Río Caquetá. El mercurio, que se usa para separar las partículas de oro de lodo y tierra, termina en las fuentes hídricas, esto genera **altos niveles de contaminación en** **los peces**, los animales que beben agua del río, y por supuesto las comunidades que viven gracias al afluente.]

### [Río Muña] 

[En Sibate, Cundinamarca, la comunidad denuncia que la empresa italiana Comind S.A lleva más de 5 años de explotación minera]causando graves impactos ambientales, [sin que las autoridades tomen acción frente a esto. Debido a su cercanía con el río Muña, las]fuentes hídricas se están viendo seriamente afectadas **“no tenemos el agua suficiente que teníamos hace cinco seis años, ha disminuido muchísimo*”***[**,** además “del polvo que hay en el aire”, que afecta a los pobladores por la extracción de la mina, indica el líder campesino, Giovanni castillo, quien agrega que no solamente la población de Sibaté es la afectada sino que también Fusagasugá y otros municipios aledaños.]

### [Río San Jorge] 

[Pescadores y familias habitantes del territorio colectivo del Pilón en Galindez, Cauca, se están quedando sin alimentos, sin casa y sin Río. Según denuncian los afrodescendientes, la empresa Ingeniería Vías S.A está acabando con el Río San Jorge que ya perdió su acostumbrado caudal y su curso habitual por la extracción minera inconsulta que se realiza desde hace más de 10 años.]En el área de explotación, **80 familias resultaron perjudicadas por la desviación del  mismo río,**[ ya que se dañaron los cultivos de pancoger de las comunidades.]

[Según la comunidad, la explotación que realiza la empresa es directa, introduciendo máquinas retroexcavadoras que abren huecos de aproximadamente 2 metros de profundidad. el río también se ve perjudicado por las volquetas que cruzan el afluente aproximadamente] 200 veces al día.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
