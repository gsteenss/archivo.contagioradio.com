Title: Estadísticas, Violencia Vs Daniela Murcia
Date: 2016-01-07 10:52
Category: Mar Candela, Opinion
Tags: feminismo, mujeres
Slug: feminicidio-estadisticas-violencia-vs-daniela-murcia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/feminicidios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **[Mar Candela](https://archivo.contagioradio.com/reflexiones-diversas-para-las-colombianas/)** 

###### [7 Nov 2015] 

[La ciudadanía  se ha acostumbrado tanto a ver las violencias e infamias  sociales  en cifras que  la mirada de resignación  es solo proporcional  a la costumbre . No es diferente a  lo que pasa con  los feminicidios  que siempre son expresados  en números, y  hay que decirlo:]

[ Números  que  al final el frío de la estadística convierten en común  lo escandaloso.  Ya es habito que  todo lo que se diga sobre  una realidad debe estar soportado por números]**nadie  hace denuncia mediática   salvo que vengan acompañadas de alguna cifra.**[ Y sucede con la violencia que produce las FARC o la delincuencia “común”,  sucede con la violencia  de género o la exclusión social, racismo matoneo, violencia infantil  y todas las demás.]

[ Las estadísticas son alarmantes e  históricas pero parece que   fueran invisibles, el infierno arde y,  la gente prefiere ducharse para aliviarlo antes que buscar la puerta de salida. No cuestionan, no protestan solo miran perplejos los noticieros y diarios  y se limitan a lamentar pasito. Lo peor  es que se enojan con quienes resistimos porque  consideran que  les quitamos tiempo o que no vale la pena.]

**Tenemos una pandemia de violencia en Colombia**

[ Cantidades de violencias  y todas se pueden tipificar porque ninguna violencia se parece a otra, porque cada violencia tiene una raíz específica  y hay que conocer  las raíces para entender con qué vamos  a fumigar para buscar recuperar lo que se pueda recuperar de la vida.]

[Sé que estamos en campaña por la paz,  y anhelamos  que al fin podamos vivir en paz pero no será posible si vivimos con miedo, con injusticia  y sobre todo  con ceguera voluntaria. . Necesitamos creer en la paz pero no podemos  decidir ser ciegos voluntarios.]

Existe una estadística que no se conoce y de la que  nadie habla no sé si existen ya cifras...

[ Es la estadística de la madre de todas las violencias  hablo de la violencia estatal,  Si señores y señoras: Hay violencia estatal están  matando de hambre y sed a familias enteras incluyendo infantes  y ancianos. Esto es  en la Guajira y no solo en la Guajira,  si miramos chocó, Y me atrevo a decir sin tarareo, si vamos a todos los departamentos encontraremos muerte por hambre. El Estado en el caso de la Guajira  ha asesinado  a centenas quitándoles el río Ranchería -  En Chocó  y las otras regiones  lo hace permitiendo  que los poderosos de siempre  se adueñen de todos los recursos  de las regiones. El presidente Juan Manuel Santos ha despilfarrado seiscientos millones de pesos en cortinas  y no solo eso  en plena sequía y racionamiento han lavado la fachada del Congreso.]

[ No  puedo callar frente a esta violencia  presidencial y estata,  de la violencia estatal no se salva nadie, las mujeres ganamos mucho menos sueldo que los hombres  en las mismas profesiones u oficios. Nos están asesinando a cuatro diariamente pero el recurso para combatir la violencia de genero  ha sido risible, El IVA sigue siendo  la puñalada máxima del estado a toda la ciudadanía ya que  el IVA no distingue poder adquisitivo .]

[El aumento salarial es  de las balas más letales que recibimos desde el estado. Lo chocante es que esto no causa tanta indignación ni fuerza mediática como el hecho de que una mujer este saliendo con un hombre casado. Les parece escandaloso  un asunto de la vida personal de dos mujeres  y un hombre y no es escandalo el estado violento y asesino.]

[Mi asco por la sociedad  en que vivo es  orgánico porque  pienso que es asquerosa  una sociedad  católica, donde  habitan quienes presenciaron escarnio  al que era sometida Daniela ,  los mismos que pedían que  fuera golpeada  algunos incluso pedían apedrearla]

[Sociedad de gente  de bien, sociedad que postula la decencia  y la moral,  esa misma sociedad que  olvida la historia bíblica de la mujer adúltera  donde Jesucristo  sabiamente cuando le preguntaron  si debían  apedrearla   respondió; “él que esté libre de pecado tire la primera piedra”  tanto como olvida que vive  sometida a políticas de violencia estatal para que nunca pueda vivir y su destino sea simplemente sobrevivir.]

[ No necesito ser cristiana o católica  para tener claro que   estoy en una  Colombia tan hipócrita como injusta.]

Si en este momento dado las circunstancias  me pidieran una sola palabra para definir la sociedad Colombiana   mi palabra seria: REPULSIVA.

**Mar Candela – Ideóloga Feminismo Artesanal**
