Title: Defendamos la Paz se prepara para convocar a nuevas acciones por la vida
Date: 2019-07-29 18:25
Author: CtgAdm
Category: DDHH, Nacional
Tags: Defendamos la Paz, protección de líderes sociales
Slug: defendamos-la-paz-se-prepara-para-convocar-a-nuevas-acciones-por-la-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Defendamos-la-Paz-26-J.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Después de escucharse el grito del pasado 26 de julio que congregó a más de 100 ciudades y municipios en Colombia y más de 60 ciudades del exterior alrededor de la defensa de líderes y lideresas, la Plataforma Defendamos la Paz ya trabaja en más propuestas que trasciendan a la movilización, entre ellas, una de sus piedras angulares: la Oficina de la Alta Comisionada de Naciones Unidas en Colombia, que viene haciendo veeduría desde hace 20 años y que hoy ve comprometida su presencia en el país por cuenta del Gobierno.

### "Existe una frustración que es absolutamente justificada hacia el Gobierno" 

Para Laura Gil, politóloga e integrante de Defendamos la Paz, el Gobierno debió ser más precavido en su expresión de apoyo a la marcha del pasado 26 de julio, pues es evidente que **"existe una responsabilidad política del presidente"**,  sin embargo indica, que en la consecución de la sociedad colombiana, "no se puede empezar llamándole asesino al otro" , esto con referencia a la forma en la que el mandatario fue expulsado de la movilización en Cartagena.

Gil señala que la situación de amenazas se agrava por la falta de políticas adecuadas que contribuyan al desmonte de la guerra en todas sus expresiones, incluyendo el uso del discurso, que muchas veces puede ser la excusa para empoderar a sectores que no han sentido las consecuencias de sus acciones criminales.

El primer paso que debe dar el presidente Duque es el de "desautorizar a quienes dentro del Gobierno empoderan a los violentos a través de la palabra", como ha sido el caso del ministro de Defensa, Guillermo Botero o Luis Guillermo Echeverri, exgerente de la campaña presidencial de Iván Duque, quienes se han expresado de forma peyorativa hacia la problemática de asesinato de líderes sociales.

### Si el Gobierno responde es porque el mensaje está llegando 

Pese a ello, que el presidente haya asistido a la marcha, es para la plataforma, un síntoma de que se puede comprometer al Gobierno a que haga mucho más, **"nadie tiene la receta,  es un fenómeno que arrancó en el Gobierno anterior",** uno que demostró tener la voluntad política para abordar el problema, pero que tampoco logró detener un fenómeno que ha ido en aumento.

Ante esta percepción de apoyo de la ciudadanía, desde Defendamos la Paz, ya se discuten propuestas para darle continuidad a la marcha y a la protección de líderes sociales, las que tendrían que involucrar directamente la creación de medidas de seguridad con las comunidades, tal como debió hacer el Plan de Acción Oportuna (PAO) del Gobierno.  [(Lea también: Comunidades expresan respaldo a propuestas de Defendamos La Paz) ](https://archivo.contagioradio.com/comunidades-expresan-respaldo-a-propuestas-de-defendamos-la-paz/)

### Defendamos la Paz cree en el respaldo internacional 

Otro tema crucial, en la agenda de Defendamos la Paz es la permanencia de la  Oficina de la Alta Comisionada de Naciones Unidas para los Derechos en Colombia (OACNUDCH), organismo al que según se conoció, el Gobierno habría pedido reducir  de manera sustancial el contenido de su mandato en el país

Por tal motivo, la organización se dirigió a la Alta Comisionada, Michelle Bachelet, para que mantenga por varios años el mandato original de asistencia técnica y observación de la situación de derechos humanos que han venido cumpliendo en el país desde 1996 y al que se le agregó una función de verificación con la firma del Acuerdo de Paz.

> Exitosa reunión de delegación sobre derechos humanos y la paz en Colombia con la Alta Comisionada para los Derechos Humanos, Michelle Bachelet [@AlvaroLeyva](https://twitter.com/AlvaroLeyva?ref_src=twsrc%5Etfw) [pic.twitter.com/L0ulyStuU1](https://t.co/L0ulyStuU1)
>
> — Mariela (@MarielaInt) [July 29, 2019](https://twitter.com/MarielaInt/status/1155768044712529920?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Gil destaca que la labor que ha realizado la ONU durante 20 años en el país ha sido crucial por lo que es necesaria su continuación para que se implemente el Acuerdo de la  Habana en su totalidad, y aunque manifiestan que Bachelet se mostró respectiva frente a la carta, aún se desconoce cuál será el resultado del diálogo entre el Gobierno  la OACNUDH y su futuro en Colombia

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_39185059" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_39185059_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
