Title: Cada tercer día es agredido un líder político
Date: 2019-02-01 17:14
Author: AdminContagio
Category: DDHH, Líderes sociales, Política
Tags: asesinato de, Misión de Observación Electoral, Plan de Acción Oportuno
Slug: tercer-dia-agredido-opositor-del-gobierno-actual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/crimenes-de-lideres-sociales-por-el-estado-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [1 Feb 2019] 

Desde el inicio de la temporada electoral el 27 de octubre de 2018 hasta el 31 de enero de 2019, la Misión de Observación Electoral (MOE) reportó **30 casos de violencia en contra de precandidatos, servidores públicos elegidos popularmente y funcionarios de los organismos de control**, [según su último informe](https://moe.org.co/30-agresiones-contra-lideres-politicos-y-servidores-publicos-desde-el-inicio-del-proceso-electoral-moe/).

Alejandra Barrios, directora nacional de la Misión, expresó su preocupación por la falta de "**un plan especifico y dirigido para poder generar mecanismos de protección, con participación de los partidos políticos," para prevenir el aumento de este tipo de agresiones.** Cabe resaltar que el actual Plan de Acción Oportuno solo cobija líderes sociales, periodistas y defensores de derechos humanos.

Estas agresiones, que ocurren en un promedio de uno cada tercer día, llamaron la atención del país esta semana cuando dos precandidatos a las alcaldías regionales, Jorge Erney Castrillón Gutiérrez y Silvio Montaño Arango, fueron asesinados. Estos homicidios se suman a las amenazas que recibieron el 30 de enero cinco dirigentes de los sectores políticos de centro-izquierda en Bogotá. (Le puede interesar: "[Gobierno guarda silencio ante amenazas de Águilas Negras a dirigentes de izquierda](https://archivo.contagioradio.com/gobierno-guarda-silencio-ante-amenazas-aguilas-negras-dirigentes-izquierda/)")

### **Los líderes de oposición bajo ataque** 

Según el informe, una de las características que comparten la mayoría de los dirigentes amenazados o asesinados es su oposición al actual gobierno del Presidente Iván Duque. De acuerdo al MOE, este tipo de violencia tiene como propósito la "intimidación política contra líderes nacionales y locales que pertenecen a partidos y organizaciones que se han declarado abiertamente en oposición."

Para Barrios, estas agresiones por parte de grupos al margen de la ley se dan por dos razones: para excluir el contrario político de puestos de poder y para eliminar las amenazas a las economías ilegales. A lo segundo, la analista sostuvo que grupos criminales recurren a la violencia con la intención de "sacar del juego político la posibilidad de representación a líderes sociales, políticos o comunales que hacen defensa de temas como el medio ambiente, tierras o víctimas."

### **Las recomendaciones de la  MOE** 

Frente estos hechos, la MOE le instó al Gobierno Nacional a convocar la Comisión Nacional de Garantías de Seguridad, con la participación de las colectividades políticas, donde la MOE estaría presente para dar a conocer los actuales riegos para aspirantes políticos y una serie de recomendaciones de seguridad.

![Captura de pantalla (8)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-81-800x365.png){.alignnone .size-medium .wp-image-60995 width="800" height="365"}

De manera puntual, Barrios afirmó que estas medidas de protección deben incluir un enfoque regional dado que los comicios, programadas para el 27 de octubre, se realizarán en los territorios. El proceso electoral en las regiones ya suelen ser violentas para los candidatos y dado las cifras actuales de violencia, podría terminar "con unos datos muy graves de violencia política y de manera particular, violencia letal."

Por tal razón, Barrios manifestó la necesidad de crear comisiones paralelas en las regiones que permite trabajar las particularidades de las poblaciones y los territorios. Según el informe, las cinco regiones más afectadas por hechos de violencia política son: Cauca y Bogotá, con 6 hechos cada uno; Cesar, con tres hechos; y Valle del Cauca y Putumayo, con dos cada uno.

<iframe id="audio_32123660" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://www.ivoox.com/player_ej_32123660_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
