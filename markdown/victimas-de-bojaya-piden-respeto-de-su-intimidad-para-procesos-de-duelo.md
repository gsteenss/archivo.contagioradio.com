Title: Víctimas de  Bojayá piden respeto al duelo
Date: 2017-05-22 12:52
Category: DDHH, Nacional
Tags: Bojaya, Derechos de las víctimas, masacre
Slug: victimas-de-bojaya-piden-respeto-de-su-intimidad-para-procesos-de-duelo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/DSC_5063-e1495475519413.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Plural ] 

###### [22 may 2017] 

A partir de la polémica que se generó por un supuesto "veto" de las víctimas de la Masacre de Bojayá a las exhumaciones que realiza la fiscalía, se ha generado una discusión en que **la comunidad pidió respeto y creatividad a los periodistas para que con su labor no se revictimice** y se abran las puertas a una nueva interpretación de un proceso de re descubrimiento de la verdad y de reconciliación.

Las víctimas de Bojayá, respondieron así a la polémica generada por la periodista Patricia Nieto. **Le han pedido al país que se respete su derecho a realizar el duelo en la intimidad** mientras que avanza el segundo proceso de exhumación de cuerpos tras la masacre de Bojayá en 2002. Lea el artículo de Verdad Abierta acá: ["El Silencio de Bojayá"](http://www.verdadabierta.com/victimas-seccion/organizaciones/6634-el-silencio-de-bojaya)

Según el artículo de Nieto, las víctimas de Bojayá estarían impidiendo los procesos para cubrir y presentar información sobre procesos de confirmación de identidades y exhumación de cadáveres. Sin embargo, las víctimas de la masacre le han pedido a la sociedad colombiana que se respete **el derecho al duelo que por naturaleza tienen.**

La falta de comprensión de los procesos de duelo de las comunidades, ha hecho que **los medios de comunicación hagan cubrimientos sin control, especialmente, en la forma de tratar a las víctimas como fuentes.** Le puede interesar: ["Los saldos pendientes con las víctimas de la masacre de Bojayá"](https://archivo.contagioradio.com/los-saldos-pendientes-con-las-victimas-de-bojaya/)

Leider Palacios, portavoz del Comité de Defensa de los Derechos de las Víctimas de Bojayá y víctima directa de la masacre, afirma que **“hemos sentido que, con la discusión de los periodistas, se nos quiere tirar otra pipeta más para aumentar nuestro sufrimiento”.** Ellos y ellas han pedido que los medios de comunicación y la población en general, comprendan que el derecho a la intimidad hace parte de la Constitución.

En repetidas ocasiones han manifestado que su intención no es censurar a la prensa, sino que por el contrario, quieren que los **periodistas realicen un debate serio** sobre el papel de los medios en los procesos donde las víctimas buscan esclarecer y recomponer los procesos históricos de los cuales fueron víctimas.

El Comité de Defensa de los Derechos de las Víctimas de Bojayá, hizo público, el 11 de mayo de 2017, un protocolo para el manejo de las comunicaciones donde **destacan el rol de los medios de comunicación en el marco del proceso de paz en Bojayá.**

Sin embargo, fueron muy enfáticos en solicitarle a los medios de comunicación y periodistas de “abstenerse de filmar, tomar fotografías, grabar, escribir o realizar entrevistas individuales a las familias, o a cualquier persona vinculada con el proceso de la exhumación, entrega de los cuerpos, y ceremonias relacionadas con la masacre del 2 de mayo de 2002, desde el 4 de mayo de 2017 hasta el fin de las exhumaciones, **esto por respeto a nuestra dignidad, creencias y cultura”.** Le puede interesar: ["Bojayá viejo, el nacimiento de lo nuevo"](https://archivo.contagioradio.com/bojaya-viejo-el-nacimiento-de-lo-nuevo/)

En rueda de prensa el día 21 de mayo las víctimas y el comité de Derechos Humanos de la ONU, le informaron al país de los avances en las exhumaciones que iniciaron, el 2 de mayo en Bojayá. Además, Palacios aclaró que “de este proceso se espera que se logre identificar e individualizar más de 80 restos”. A parte de los 55 cuerpos que fueron exhumados y trasladados desde Bellavista hasta Medellín, la Fiscalía General de la Nación, ha identificado 32 fosas más que esperan sean exhumadas. Leider Palacio en nombre de las víctimas espera que, de la mano del Instituto Colombiano de Medicina Legal, **“se puedan hacer los análisis pertinentes para que todo el país conozca la verdad y el número exacto de muertes que dejó la masacre en Bojaya´”.**

<iframe id="audio_18830191" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18830191_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

 
