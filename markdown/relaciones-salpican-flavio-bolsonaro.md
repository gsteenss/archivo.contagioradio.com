Title: Hijo de tigre... las cuestionadas acciones de Flávio Bolsonaro
Date: 2019-01-23 13:57
Author: AdminContagio
Category: El mundo, Otra Mirada
Tags: bolsonaro, Brasil, corrupción
Slug: relaciones-salpican-flavio-bolsonaro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/27nov2018-flavio-bolsonaro-e-jair-bolsonaro-em-brasilia-1544700567036_v2_1920x1260-e1548266286168.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Adriano Machado 

###### 23 Ene 2019 

A las investigaciones adelantadas por el Ministerio Público Público y el Consejo de Control de Actividades Financieras (COAF) de Brasil, por **depósitos sospechosos encontrados en las cuentas de Flávio Bolsonaro**, congresista electo e hijo del Presidente del país, se suman los **cuestionamientos por contratar a la madre y esposa del principal sospechoso por el asesinato de la concejala y activista Marielle Franco** en marzo de 2018.

### **El caso Queiroz** 

Mientras realizaban seguimiento a una operación de compra de diputados, lavado de dinero, organización criminal en la Asamblea Legislativa de Río, y a las anomalías bancarias de más de veinte diputados y sus equipos, **los investigadores del COAF encontraron los sospechosos movimientos realizados en las cuentas de Fabricio Queiroz**, asesor de Flavio Bolsonaro y luego del mismo parlamentario.

Serían **48 depósitos por valor de 2.000 reales** (96.000 en total) realizados entre junio y julio de 2017 **en un lapso de 5 días y con pocos minutos de diferencia entre cada uno**, en un cajero de la Asamblea de Río de Janeiro donde Bolsonaro se desempeñaba como diputado estatal. Operaciones que ha justificado aludiendo a que corresponden a la compra y venta de un apartamento de forma legal.

Además de los depósitos fraccionados, el informe del C**OAF dió cuenta de un depósito por 1,16 millones de reales**, mismos que el exdiputado asegura hacen parte del mismo negocio por el partamento agregando que justificara con evidencias antes los entes competentes.

El caso Queiroz, que salió a la luz en diciembre del año anterior por el periódico Estadao, **registra un movimiento de 1,2 millones de reales** (300 mil dólares) en las cuentas del asesor, **una cantidad incompatible con el monto de sus ingresos**. Dentro de los registros se encuentra además **un depósito de 24 mil reales a la cuenta de Michelle Bolsonaro**, esposa del actual presidente, en **10 depósitos de 4 mil cada uno**, mismos que el mandatario ha justificado como pago de una vieja deuda.

Estos no serían los únicos movimientos, ya que han trascendido **informaciones que revelan operaciones bancarias hasta de 7 millones de reales en 3 años** (unos 1,8 millones de dólares), que, de comprobarse, agudizará los cuestionamientos frente a la familia de Jair Bolsonaro **quien en campaña utilizó como bandera la lucha contra la corrupción**.

### **Frente al asesinato de Marielle Franco** 

Hasta noviembre del año anterior, **Flávio Bosonaro tuvo contratadas a la madre y esposa del capitán de policía Adriano Magalhaes de la Nóbrega**, **principal sospechoso por el asesinato de Marielle Franco**, concejala y activista por los derechos humanos, ocurrida en marzo de 2018. (Le puede interesar: [Brasil exige respuestas ante asesinato de la activista Marielle Franco](https://archivo.contagioradio.com/brasil_asesinato_marielle_franco/))

Según información publicada por O Globo, **la madre del uniformado Raimunda Veras Magalhaes y su esposa Danielle Mendonça da Costa da Nóbrega, ocuparon cargos en el gabinete de Flavio Bolsonaro** con un salario de 6.490 reales. Ambas dejaron el cargo voluntariamente el 13 de noviembre de 2018 según el Diario Oficial del Estado citado por el medio brasileño.

**Magalhaes**, que hoy se encuentra prófugo de la justicia, **tiene un pedido de detención por parte de la Fiscalía brasileña que lo considera como el hombre fuerte de la oficina del crimen**, organización sospechosa de cometer el crimen contra Franco, uniformado que estaría implicado en varios homicidios desde hace más de una década.

Este martes, **fueron detenidos un comandante y un teniente de la Policía Militarizada por su implicación en el crimen de la activista**. Según medios locales, Ronal Paulo Alves Pereira, u**no uniformados, recibió un homenaje en 2004 por parte de Flávio Bolsonaro en su función como diputado estatal**, a lo que ha respondido que en cientos de ocasiones a realizado estos reconocimientos, añadiendo sin embargo que “quienes cometen errores deben ser responsables por sus actos”.

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
