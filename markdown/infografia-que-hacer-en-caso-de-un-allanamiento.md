Title: Infografía: ¿Qué hacer en caso de un allanamiento?
Date: 2019-11-20 00:10
Author: CtgAdm
Category: infografia, Paro Nacional
Tags: allanamiento, Paro Nacional
Slug: infografia-que-hacer-en-caso-de-un-allanamiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/QUE-HACER-EN-CASO-DE-UN-ALLANAMIENTO.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/QUÉ-HACER-EN-CASO-DE-UN-ALLANAMIENTO.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/QUÉ-HACER-EN-CASO-DE-UN-ALLANAMIENTO.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/QUÉ-HACER-EN-CASO-DE-UN-ALLANAMIENTO-1.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

A la hora de ser víctima de un allanamiento tenga en cuenta los siguientes puntos:

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/QUE-HACER-EN-CASO-DE-UN-ALLANAMIENTO-410x1024.png){.aligncenter .size-large .wp-image-76802 width="410" height="1024"}
