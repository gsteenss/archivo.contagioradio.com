Title: Alto Grado en Sonidos Urbanos
Date: 2016-08-17 13:20
Category: Sonidos Urbanos
Tags: Cultura, Música
Slug: alto-grado-en-sonidos-urbanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Altogrado1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Alto Grado 

El sonido animal de la banda bogotana 'Alto Grado' estuvo acompañándonos en Sonidos Urbanos.

**Martinika**, voz líder del grupo y  **Jonathan**, guitarrista, nos hablaron del recorrido musical de la banda que con sonidos del reggae, ska y rock ha recorrido escenarios nacionales e internacionales entre los que se cuentan Rock al Parque, Tortazo Reggae y el  House of Blues Sun Set of Hollywood entre otros.

En exclusiva Alto grado presenta las canciones de su nuevo **álbum titulado Boomfyah**, un disco grabado en los estudios de Audiovision y cargado con gran potencia musical, con temas en inglés y con el acompañamientos de algunos artistas de la escena reggae como es el caso de Javier, vocalista de la banda Alerta Kamarada y David Kawooq, uno de los fundadores de Doctor Krapula. Un disco con toda la energía y buena música de la escena alternativa e independiente de la capital.

Invitados a bailar y saltar con el sonido animal e historias de Alto Grado en este nuevo Sonidos Urbanos.

<iframe id="audio_12576115" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12576115_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
