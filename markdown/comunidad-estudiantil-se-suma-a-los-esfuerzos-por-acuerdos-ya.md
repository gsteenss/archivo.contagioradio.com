Title: Comunidad estudiantil se suma a los esfuerzos por Acuerdos Ya
Date: 2016-10-10 14:15
Category: Nacional, Paz
Tags: acuerdos de paz, Encuentro Nacional Universitario, Movimiento estudiantil Colombia
Slug: comunidad-estudiantil-se-suma-a-los-esfuerzos-por-acuerdos-ya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz64.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ContagioRadio] 

###### [10 Oct de 2016] 

El próximo sábado 15 de Octubre, se llevará a cabo en la Universidad Nacional sede Bogotá el Encuentro Nacional de Universidades e Instituciones de Educación Superior, con el objetivo de **[sumar propuestas y agendas dentro del compromiso por la construcción de Paz](https://archivo.contagioradio.com/paz-a-la-calle-se-sigue-movilizando-en-toda-colombia/)** y la exigencia de la implementación de los acuerdos pactados en La Habana.

Esta iniciativa tiene como objetivo la concienciación de la ciudadanía y del movimiento estudiantil, para **dar solución al conflicto armado y posibilitar otros cambios estructurales que demandan sectores sociales** de mujeres, víctimas, poblaciones rurales, mujeres y sectores LGBTI.

María Alejandra Rojas, una de las organizadoras, extiende la invitación a estudiantes, docentes, trabajadores, grupos de investigación y semilleros de todo el país, “para recuperar la iniciativa del movimiento estudiantil coordinando y uniendo iniciativas que potencien acciones colectivas a futuro”. **El encuentro será desde las 8:00am en la Universidad Nacional** y hasta el momento hay confirmadas 30 universidades y 200 personas.

La metodología del evento se dará en tres momentos, se inicia con un panel e intercambio de experiencias desde diferentes estamentos, rectores, profesores y estudiantes, quienes **hablarán sobre su papel y el de la universidad dentro de la construcción de paz** y [los compromisos que se deben asumir en adelante](https://archivo.contagioradio.com/cese-bilateral-plazo-para-que-no-retorne-la-guerra/).

En la tarde se instalarán dos mesas, una trabajará alrededor del tema de movilización social, para plantear alternativas y nuevas iniciativas y, la segunda mesa trabajará sobre la proyección política, “a que le apuntamos como movimiento universitario y cuál es el papel de la educación en la construcción de paz”. El cierre será a partir de las conclusiones de las dos mesas y un acto cultural a cargo de los **artistas que se encuentran en asamblea permanente por la paz**, señala Rojas.

Para mayor información, se puede consultar el [Evento a través de redes sociales como Facebook](https://www.facebook.com/events/1185025454889467/) y diligenciar el formulario de inscripción dispuesto en esta plataforma.

<iframe id="audio_13259695" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13259695_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
