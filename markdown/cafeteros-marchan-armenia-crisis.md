Title: Miércoles 24 de abril: cafeteros marchan en Armenia por crisis del sector
Date: 2019-04-09 23:02
Author: CtgAdm
Category: Economía, Movilización
Tags: Agro, Café, Paro Nacional, Precio
Slug: cafeteros-marchan-armenia-crisis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Protesta-campesinos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RCN Radio] 

Desde hace varios meses los caficultores vienen advertido un descenso en el precio de la carga de Café, esta situación ha alcanzado su punto más delicado recientemente, con un precio por carga (125 kilos) que oscila entre 650 y 670 mil pesos dependiendo de la calidad. Ante esta situación, los campesinos que se dedican a la siembra del grano están en alerta por las pérdidas que se acercan a los 100 mil pesos por carga, puesto que la producción de la misma llega a costar hasta 75 mil pesos.

Como lo explica el integrante del Comité Ejecutivo de Dignidad Agropecuaria y Cafetera, Alonso Osorio, la situación de las más de 500 mil familias que se dedican al cultivo del Café se ha ido complejizando porque antes de 1989 el precio de la carga estaba definido por la ley de oferta y demanda; después de ese año, con el ingreso del libre comerció esta situación cambió, y ahora los inventarios son manejados por los países consumidores de café.

A esta situación se suma la acción de fondos de inversión que tienen sus intereses en el mercado, y el control de los oligopolios, porque solo 6 multinacionales manejan el 70% del café a nivel global, controlando los inventarios y especulando con el precio.

### **La asistencia gubernamental es poca y no se hace mucho por salir de la crisis** 

Osorio indicó que no existe una política pública que ayude a palear el problema sobre los fondos de inversión y el control que tienen sobre el mercado; tampoco se ha avanzado en apoyos económicos para que los caficultores reduzcan sus costos de producción y transporte mediante subsidios en fertilizantes o la construcción de nuevas vías de transporte, peor que eso, el integrante de Dignidad Cafetera recordó que recientemente el Gobierno modificó el acceso a la salud de los cultivadores, al sacar del SISBEN a familias que tienen 20 mil plantas de café, lo que no es una producción demasiado grande.

La propuesta de la Federación Cafetera para aumentar los precios del grano es salir de la bolsa y no cotizar allí, pero los cafeteros temen que esa propuesta no venga acompañada por los principales productores de grano de alta calidad (Brasil, Vietnam, Indonesia, Honduras, Etiopía e India), cuyo efecto negativo sería que Colombia terminara sin mercado internacional para su producción. Por el contrario, si los 7 países aceptaran la propuesta, el precio podría volver a ser controlado por los productores, o al menos por la cantidad de grano disponible en el mercado.

Los integrantes de Dignidad Agropecuaria y Cafetera participarán el próximo 24 de abril en Armenia, Quindío, de una gran movilización con la que esperan llamar la atención sobre los problemas a los que se enfrenta el gremio; adicionalmente, los cafeteros están evaluando su participación en el paro nacional del 25 de abril. (Le puede interesar:["25 de abril paro nacional por la paz y contra el Plan Nacional de Desarrollo"](https://archivo.contagioradio.com/25-abril-paro-nacional-paz-pnd/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
