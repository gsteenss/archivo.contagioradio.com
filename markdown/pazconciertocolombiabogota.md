Title: Alabaos, cantos ancestrales, jazz y salsa por la paz
Date: 2016-11-29 12:54
Category: eventos
Tags: colombia, Concierto, paz, Plaza de Bolívar
Slug: pazconciertocolombiabogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/WhatsApp-Image-2016-11-29-at-7.15.47-AM-e1480441118184.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Acciones por el Acuerdo 

##### 29 Nov 2016 

La unión sinérgica entre manifestaciones artisticas y representaciones culturales tradicionales se encontrarán una vez mas en la Plaza de Bolívar para abogar por la construcción de paz en Colombia y la defensa unida de un pais para todos y todas, en el '**Concierto por el Acuerdo, una afirmación por la vida**'.

El encuentro, organizado por el colectivo **Acciones por el Acuerdo**, con el apoyo de la Universidad Nacional, el Centro de Memoria histórica e IDARTES, busca **reunir a través de la música, el llamado común a proteger la dignidad de las víctimas y líderes asesinados durante el proceso de paz**, la protección de los reclamantes de tierras y todas aquellas personas que han padecido la guerra en carne propia.

El acto combinará las intervenciones tradicionales como la de **Reinaldo Giagrekudo**, mayor tradicional Huitoto de la Chorrera, Amazonas, quien realizará un llamado a ritual con manguarés tocados por indígenas huitotos al atardecer; las más de **80 cantaoras de Bojayá** con sus alabaos, la **Red de Mujeres del Pacífico Sur** y de otros grupos desplazados radicados en Bogotá y la participación del músico de jazz **Antonio Arnedo**, **Hombre de Barro** y la energía de la salsa con la **Orquesta La 33**.

Desde las 5 p.m. de este martes en la Plaza de Bolívar, **se proyectarán además los rostros de los desaparecidos y asesinados** a quienes se les busca hacer un homenaje, insitiendo en que **la paz no da espera porque cada día cuenta y cada acción suma**, en esta oportunidad utilizando la música, en un pais de sonidos tan diversos como Colombia, como medio para hablar de manera colectiva de paz y reconciliación. Le puede interesar: [Los aportes de la cultura a la construcción de paz](https://archivo.contagioradio.com/los-aportes-de-la-cultura-a-la-construccion-de-paz/).

### Plaza de Bolívar, escenario de paz. 

Desde que se conocieron los resultados del plebiscito por la paz del 2 de octubre, la Plaza de Bolívar ha servido como epicentro para que pintores, músicos, dramaturgos, cineastas y fotográfos entre muchos otros, realicen diferentes manifestaciones como instalaciones, performances y pinturas desde las cuales se pueda invitar a continuar encaminando todos los esfuerzos hacia la paz.
