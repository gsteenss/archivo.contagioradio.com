Title: UBPD pide protección para sitio de inhumación en El Copey (Cesar)
Date: 2020-07-31 20:10
Author: AdminContagio
Category: Nacional
Tags: conflicto armado en Colombia, Desaparición forzada, Ejecuciones Extrajudiciales, El Copey (Cesar), Unidad de Busqueda
Slug: ubpd-pide-proteccion-para-sitio-de-inhumacion-en-el-copey-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Sitio-de-inhumación-en-El-Copey-Cesar.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Entrevista-Sebastián-Bojacá-CCJ-.mp3" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: UBPD

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La Unidad de Búsqueda de Personas dadas  por Desaparecidas -[UBPD](https://www.ubpdbusquedadesaparecidos.co/)- declaró el sitio de inhumación de El Copey (Cesar) como «*[lugar de interés para la búsqueda de personas desaparecidas](https://www.ubpdbusquedadesaparecidos.co/actualidad/ubpd-declara-sitio-de-inhumacion-de-el-copey-cesar-como-lugar-de-interes-para-la-busqueda-de-personas-desaparecidas/)*».** Esto a raíz de la denuncia ciudadana dada a conocer el pasado miércoles en la que se alertaba sobre el entierro de cadáveres de personas fallecidas por Covid-19 en dicho terreno. (Lea también: [Manejo erróneo de cementerios por Covid-19 pondría en riesgo memoria histórica del conflicto](https://archivo.contagioradio.com/manejo-erroneo-de-cementerios-por-covid-19-pondria-en-riesgo-memoria-historica-del-conflicto/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/maryluzherran/status/1288483400874176513","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/maryluzherran/status/1288483400874176513

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según la denuncia, **la Alcaldía de El Copey (Cesar); ordenó excavar diez fosas para enterrar los cuerpos de las personas fallecidas por Covid, en un terreno en el que presuntamente reposan los  restos de al menos 100 personas víctimas de desaparición forzada y ejecuciones extrajudiciales** hace más de 13 años.  Así lo denunció también la Comisión Colombiana de Juristas -[CCJ](https://www.coljuristas.org/)-, que acompaña a las familias de posibles víctimas enterradas en este lugar.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/UBPDcolombia/status/1289209438037880832","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/UBPDcolombia/status/1289209438037880832

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Sebastián Bojacá, abogado delegado por la CCJ en el caso, señaló que el terreno referido se ha denominado como un «*cementerio alterno*» por parte de la administración municipal, **pese a que —como se vio en el vídeo de la denuncia— no cuenta con ninguna especie de cerramiento, infraestructura o custodia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, que entre las al menos 100 personas que se encuentran allí inhumadas, se hallan los cuerpos de **Óscar Alexander Morales Tejada, Octavio Bilbao Becerra y Germán Leal Pérez presuntas víctimas de ejecuciones extrajudiciales** del **Batallón de Artillería La Popa No. 2 del Ejército Nacional** en 2008.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado denunció que en medio de las investigaciones para esclarecer los hechos, tanto las víctimas como las comisiones y organizaciones que las apoyan en el proceso, han recibido intimidaciones por parte de miembros de dicho Batallón, lo cual, calificó como un hecho «*revictimizante*».

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Excavaciones en El Copey (Cesar) ponen en riesgo los cuerpos de personas desaparecidas

<!-- /wp:heading -->

<!-- wp:paragraph -->

La orden del Alcalde de  El Copey (Cesar) se emitió pese a que la Unidad de Búsqueda -UBPD- dio a conocer el pasado mes de mayo un comunicado con [siete recomendaciones](https://www.ubpdbusquedadesaparecidos.co/actualidad/siete-recomendaciones-de-la-ubpd-para-preservar-cuerpos-en-cementerios-ante-emergencia-por-covid-19/)  para preservar cuerpos en cementerios y morgues ante emergencia por la pandemia; entre las que, justamente, **se previene a las autoridades locales para destinar espacios suficientes para sepultar los cuerpos de las personas fallecidas por Covid-19 y evitar su inhumación en fosas ocupadas por cadáveres no identificados.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sebastián Bojacá, insiste en que es necesario que a la declaratoria que realizó la UBPD, **se sume el decreto de medidas cautelares por parte de la Fiscalía y la JEP**, lo cual permita la protección del terreno y los cuerpos que allí reposan, teniendo en cuenta además, que **al sitio concurren miembros del Batallón La Popa, unidad que está directamente implicada en los hechos de presunta ejecución extrajudicial.** (Lea también:[Militares intervinieron cementerio de Dabeiba hace dos años](https://archivo.contagioradio.com/militares-intervinieron-cementerio-de-dabeiba-hace-dos-anos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, recalcó, que **la Comisión Colombiana de Juristas ha instaurado tres solicitudes formales  ante la Fiscalía y la JEP**, la más reciente de ellas, elevada el pasado 30 de julio a la luz de la denuncia ciudadana realizada y la «*gravedad y la urgencia de la situación*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según cifras del Ministerio del Interior **al menos 26.395 cuerpos no identificados están en 426 cementerios del país.** (Le puede interesar: [En cementerio de Tumaco podrían haber más de 150 personas sin identificar](https://archivo.contagioradio.com/en-cementerio-de-tumaco-podrian-haber-mas-de-150-personas-sin-identificar/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:audio {"id":87701} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Entrevista-Sebastián-Bojacá-CCJ-.mp3">
</audio>
  

<figcaption>
Escuche la entrevista completa con Sebastián Bojacá, Abogado de la Comisión Colombiana de Juristas

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
