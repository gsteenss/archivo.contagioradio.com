Title: ESMAD arremete contra indígenas del Pueblo Arhuaco en Valledupar
Date: 2017-11-16 12:34
Category: DDHH, Nacional
Tags: agresión a indígenas, arhuacos, derechos de los indígenas, ESMAD, indígenas Arhucacos
Slug: esmad-pueblo-arhuaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/arhuacos-valledupar-e1510853114944.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Confederación Indígena Tayrona] 

###### [16 Nov 2017] 

Los indígenas Arhuacos, que se encuentran reunidos en Valledupar, Cesar, denunciaron que **han sido agredidos por parte del ESMAD**. Los indígenas llegaron a las instalaciones de la Gobernación para protestar por la falta de voluntad política que ha tenido el Gobierno Nacional para atender sus solicitudes en el marco de la Minga por la Vida.

De acuerdo con la Comisión de Derechos Humanos de los Pueblos Indígenas, hacia las 7:30 de la mañana del día de hoy, “el ESMAD, con efectivos armados con trufly, **arremete en contra del pueblo Arhuaco** en ejercicio de su legítimo derecho a la protesta social en el Departamento del Cesar”.

Afirmaron que los efectivos del ESMAD utilizaron gases lacrimógenos y **“agredieron de manera directa a cerca de 400 personas”** entre las que se encontraban niños, adolescentes, mujeres y adultos mayores. Los indígenas indicaron que, por fortuna, no se encuentran personas heridas de gravedad. (Le puede interesar: ["Arhuacos exigen que la Sierra Nevada sea zona libre de minería"](https://archivo.contagioradio.com/arhuacos-exigen-que-la-sierra-nevada-sea-zona-libre-de-mineria/))

### **Gobierno Nacional no cumplió la cita con los Arhuacos** 

La decisión del pueblo indígena, de hacer presencia en la Gobernación del Cesar, responde a la falta de garantías que les ha brindado el Gobierno Nacional, **quien se había comprometido a instalar una mesa de negociación de alto nivel**, donde estuvieran presentes los Ministros de Interior, Salud, Educación, Agricultura y demás autoridades,  para atender las demandas de los Arhuacos.

Hermes Torres, secretario general de la Confederación Indígena Tayrona, indicó que la reunión con el Gobierno Nacional del día de ayer tuvo que ser suspendida porque **no se encontraban allí los delegados idóneos** y que habían solicitado los indígenas para que atendieran sus preocupaciones. Dijo que “fue necesario suspender la reunión en vista de la poca eficacia que tuvo el diálogo”. (Le puede interesar: ["Para el 2040 la Sierra dejará de ser Nevada"](https://archivo.contagioradio.com/nevados-de-la-sierra-nevada-de-santa-marta-desapareceria-en-el-2040/))

Torres enfatizó en que el pueblo Arhuaco **ha tenido una tradición de solución pacífica de los problemas** por lo que las vías de hecho y las movilizaciones no son acciones que ellos realicen. Sin embargo y teniendo en cuenta que sus demandas y preocupaciones no han sido atendidas, “nos vimos en la obligación de movilizarnos hasta la Gobernación”.

### **Arhuacos solicitan solución a sus problemas estructurales** 

Los indígenas han solicitado en varias ocasiones que se **respeten sus derechos sobre el territorio sagrado de la Sierra Nevada** que está siendo amenazada por las acciones de minería. Ante esto, en el marco de la Minga Indígena decidieron realizar una movilización pacífica “por la identidad cultural y el territorio ancestral”.

Torres manifestó que **ha habido una vulneración sistemática de los derechos** de los pueblos indígenas arhuacos quienes no tienen garantías para desarrollar sus tradiciones culturales en un territorio ancestral que les es propio. Por esto, han decidido utilizar la movilización como una forma de visibilizar los asuntos que quieren resolver con el Gobierno Nacional. (Le puede interesar: ["Más de 150 arhuacos en hacinamiento tras desalojo en Pueblo Bello, Cesar"](https://archivo.contagioradio.com/mas-de-150-arhuacos-en-hacinamiento-tras-desalojo-en-pueblo-bello/))

Finalmente, los indígenas **rechazaron las agresiones por parte del ESMAD** “quien no tiene en cuenta la presencia de grupos poblacionales tan sensibles” como los son los Arhuacos. Por esto, le hicieron un llamado a las autoridades del departamento del Cesar y de Valledupar para “propiciar diálogos fructíferos tendientes a garantizar los derechos del pueblo Arhuaco”.

Además, solicitaron la presencia de la Defensoría del Pueblo, de la Procuraduría General de la Nación y de las Naciones Unidas para verificar las actuaciones de la Fuerza Pública en la protesta.

<iframe id="audio_22116590" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22116590_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
