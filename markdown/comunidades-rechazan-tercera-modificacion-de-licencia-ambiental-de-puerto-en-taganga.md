Title: "¡No queremos muelle!": el grito de los habitantes de Taganga
Date: 2019-04-04 10:55
Author: CtgAdm
Category: Ambiente, Comunidad, DDHH
Tags: Taganga, Terlica, Terminal de graneles liquidos de daabon
Slug: comunidades-rechazan-tercera-modificacion-de-licencia-ambiental-de-puerto-en-taganga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-53-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Plena Prensa] 

###### [2 Abr 2019] 

[Habitantes de Taganga, en Magdalena, denuncian que la empresa **Terminal de Graneles Líquidos de Daabon (Terlica)** adelanta **la tercera modificación de la licencia ambiental otorgada para la construcción de un puerto en la Bahía** a pesar de que las comunidades se han manifestado en contra del proyecto por las afectaciones sociales, económicas y ambientales que ocasionaría. ]

El pasado 31 de marzo, Terlica convocó a las comunidades del corregimiento a una socialización del Estudio de Impacto Ambiental que se presenta en el marco de esta última modificación de la licencia ambiental. Este permiso fue otorgado en 2007 para la construcción de un atracadero que ahora buscan expandir en **un puerto multipróposito de 5.000 hectáreas para el cargue de graneles líquidos e hidrocarburos**. (Le puede interesar: "[Habitantes de Taganga se oponen al proyecto Sociedad Portuaria Las Américas](https://archivo.contagioradio.com/habitantes_taganga_santa_marta_se_oponen_a_proyecto_portuario/)")

En 2008 la ruptura de una válvula en las instalaciones de Terlica en Taganga ocasionó un derrame de aceite de palma crudo a la Bahía de Playa Grande, afectando a miles de corales y otros organismos marinos, según **Claudia Vinueza, miembro fundador de la Corporación Ambientalista Taganga Azul**. A pesar de este antecedente y que la empresa aún no ha terminado con las ordenes del Ministerio de Ambiente de compensar los daños, las autoridades ambientales han aprobado dos modificaciones a la licencia ambiental.

Por su lado, las comunidades temen que la emergencia ambiental que provocó Terlica en el 2008 se pueda repetir, perjudicando **al ambiente y la supervivencia de los 6.000 habitantes del corregimiento quienes dependen de la pesca y el turismo**. Además, Vinueza resalta que un accidente en el muelle podría generar afectaciones más graves dado que se pretende transportar aceite de palma, químicos derivados del petroleo e hidrocarburos por este puerto. Por lo tanto, las comunidades se han manifestado en contra del proyecto a través de movilizaciones sociales y procesos judiciales.

El pasado 1 de abril, las comunidades rechazaron la socialización que Terlica había convocado para ese día porque **se había anunciado**[** con solo dos días de anticipación** y se convocó en **Pescaíto, un barrio de Santa Marta que queda por fuera del territorio**. "Esto implicaba que la gente necesitaba de desplazarse para asistir a la socialización. Nos preguntamos, porque no pueden hacer estas socializaciones dándole la cara al pueblo entero?", manifestó la ambientalista.]

Terlica canceló la socialización tras las denuncias de las comunidades. Otra socialización que la empresa convocó para el día de ayer en el barrio San Jorge, en Santa Marta, también fue suspendida. Frente estos hechos, las comunidades piden que el proceso para la modificación para el permiso ambiental sea rechazado y finalmente anulada por la Autoridad Nacional de Licencias Ambientales. Además, solicitan la nulidad del contrato que le otorgó la Agencia Nacional de Infraestructura.

<iframe id="audio_34097176" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34097176_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
