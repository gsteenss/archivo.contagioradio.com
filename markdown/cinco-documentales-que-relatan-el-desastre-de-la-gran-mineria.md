Title: Cinco documentales que relatan el desastre de la gran minería
Date: 2019-04-22 15:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Día mundial contra gran minería en el mundo
Slug: cinco-documentales-que-relatan-el-desastre-de-la-gran-mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/cerroverde-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  Foto: celendinilibre 

En el día mundial contra la megaminería, que ha convocado por siete años a comunidades y organizaciones defensoras del territorio, resaltamos cinco documentales producidos en America latina que visibilizan  el alto impacto de la extracción minera en territorios biodiversos, convirtiéndose en una amenaza directa al ecosistema, los recursos naturales y la vida humana.

**["Por todo el oro de Colombia" ]**

En francés,"Pour tout l'or de Colombie", es un documental de Romeo Langloisen acerca de la minería de oro en Antioquia, Colombia,  realizado para la cadena francesa de televisión Canal + en el 2012. Esta producción revela la situación de la minería a gran escala impartida por la multinacional Gran Colombia Gold, la minería informal realizada por pequeños mineros, y la minería ilegal de la que se financian grupos armados como los paramilitares.

\[embed\]https://www.youtube.com/watch?v=tx2k6nOCYZg\[/embed\]

<div>

**["Lo que la tierra no perdona"]**

Documental que deja en evidencia los perjuicios de la actividad minera en Colombia, centrados en cuatro puntos de la geografía nacional: Guajira, Vichada, el altiplano cundiboyacense y Santander. El documental es producido por profesores y estudiantes de la Escuela de Cine y TV de la U.N.

\[embed\]https://www.youtube.com/watch?v=PBkTKCM-TQk\[/embed\]

**["Sipakapa no se vende"]**

"Sipakapa qal k´o pirk´ey xik", en lengua maya, revela la explotación de una mina de oro a cielo abierto en Guatemala a manos de la empresa transnacional canadiense-estadounidense Glamis Gold (hoy Gold Corp.)  El Pueblo Maya de Sipakapa rechaza la explotación minera en su territorio y defiende su autonomía frente al avance de los grandes proyectos neoliberales.  Año 2005.

\[embed\]https://www.youtube.com/watch?v=qfHDgWnBC5s\[/embed\]

**["Asecho a la ilusión"]**

Cuenta en paralelo la historia de la minería en la Argentina y la historia de la familia Flores Salas,  quien fue injustamente desalojada de su hogar a partir de la llegada del mayor emprendimiento minero del país: Minera Alumbrera.

Contiene la voz de las comunidades cercanas a la mina La Alumbrera; de especialistas en el tema, y la reflexión del realizador, además de documentos fílmicos y testimonios nunca antes vistos que nos dan la oportunidad de escuchar y analizar las razones históricas del imaginario colectivo acerca de los beneficios de la minería, su realidad actual y las consecuencias que genera.

\[embed\]https://www.youtube.com/watch?v=M3pjPBsDDPY\[/embed\]

**["Etnocidio en el Putumayo Colombiano"]**

Dirigido por Pau Soler , cuenta cómo la minería está afectando la zona del Putumayo, un área en el que hay 40 concesiones mineras, muchas en una fase iniciatica de exploración y algunas en una etapa inicial de explotación en el contexto de lo que el presidente de Colombia, Juan Manuel Santos, ha llamado la “locomotora minero-energética”, y que es uno de los planes emblemáticos de su gobierno y que está generando gravísimas consecuencias medioambientales y sociales. Si se ejecutan estas 40 concesiones mineras, peligra la supervivencia de los pueblos indígenas Cofanes, Kamsás, Ingas y Nasas.

\[embed\]https://www.youtube.com/watch?v=q3fr6gFnN\_I\[/embed\]

</div>
