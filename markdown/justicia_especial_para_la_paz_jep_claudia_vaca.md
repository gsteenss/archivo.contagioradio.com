Title: "Queremos conocer la altura moral de los aspirantes a la JEP": Claudia Vaca
Date: 2017-08-17 18:33
Category: Entrevistas, Paz
Tags: JEP, justicia especial para la paz
Slug: justicia_especial_para_la_paz_jep_claudia_vaca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/6b_vaca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Heraldo 

###### [17 Ago 2017 ] 

Claudia Vaca no es abogada y precisamente por eso fue seleccionada como la única mujer que integra el Comité  de escogencia de los funcionarios del Sistema de Verdad, Justicia y Reparación. Fue elegida por los rectores de las universidades públicas del país como una voz externa y diferente al Derecho, de manera que el sistema judicial sea sensible ante otras visiones. Vaca es científica de la química farmacéutica, además dirige un centro de pensamiento sobre acceso y uso a medicamentos. Con ella hemos resuelto algunos de los numerosos cuestionamientos alrededor de la selección de los magistrados para la JEP.

**¿Cómo va la tarea de escogencia entre las más de 2135 hojas de vida que han llegado para conformar el JEP?**

Estamos satisfechos por el interés demostrado, hay perfiles muy altos y de diferente tipo. vamos a tener un trabajo valioso y será interesante porque el punto de partida es muy alto. En estos días estamos dedicados terminar de armar la estrategia de recolección de información de la ciudadanía sobre estos perfiles. Un insumo importante porque vamos a conocer la altura moral y el reconocimiento social de esos candidatos. El siguiente mes se tendrá ese volumen de información y se analizarán los antecedentes y la producción académica de todos los aspirantes.

**Justamente pareciera que esta elección se estuviera volviendo una campaña política, ¿Cómo ven ustedes esa situación?**

Es cierto que los aspirantes están muy comprometidos con lograr alcanzar estos cargos y consideran importante que quienes los conocen envíen comentarios en gran cantidad, asumiendo que por el volumen tomaremos algún tipo de decisión, pero la verdad es que el volumen no es lo determinante, es la calidad del contenido de la información que nos llegue. En ese sentido nos interesa mucho conocer, con datos objetivos y verificables, si han habido antecedentes frente a estas personas sobre sus posturas en términos de manejo de los asuntos de género, violencias, posturas discriminatorias o no, es decir que tengan que ver más con el comportamiento humano de los aspirantes o la altura moral. De manera que tengamos elementos adicionales para hacer un juicio de cada aspirante.

**¿Cómo llegaría la información a la ciudadanía de los perfiles preseleccionados por ustedes?**

El comité ha establecido un espacio web para atender las expectaciones ciudadanas. Estamos pidiéndole de manera especial a la ciudadanía que envíe sus aportes de manera respetuosa. Pueden hacerlo personas individuales o colectivos u organizaciones que firmen sus comentarios, es decir que no se admiten comentarios anónimos. También se pide que se entienda que no se trata de una votación. No se trata de volumen sino de calidad de información acerca de lo que conoce la gente sobre cada uno de los aspirantes. La idea es que sea un valor agregado para que se haga un juicio completo de la integridad y la idoneidad de cada aspirantes.

**Se ha encontrado que muchos de los que se postulan para la fiscalía de la JEP son exfiscales, así sucede con los magistrados para las diversas salas. Es decir que podría existir un riesgo de que la misma fiscalía que ya ha habido sea la misma que quede para la JEP y así para las otras salas…**

La verdad es que para nosotros no existe una inhabilidad específica o un criterio de descarte de las personas que hayan ocupado dichos cargos. Puede ser incluso, que la formación y la experiencia concreta en el manejo de casos sea mucho más interesante porque enriquece la visión pragmática que se va a necesitar para hacer eficiente esta nueva JEP. Lo que si es importante es no repetir en términos de deficiencias y capacidades de resolución de casos o desconocimiento de la sensibilidad de las víctimas. Conocer cuál ha sido la decisión y la disposición para hacer eficientes las sentencias, los juzgamientos y las gestión de los casos, nos permitiría conocer su compromiso y dedicación a estos asuntos, de tal manera que se hayan resuelto casos en favor de las víctimas. De lograr documentar eso, será un elemento muy valioso. Es decir que el criterio no será descalificarlos por su trayecto, sino por la calidad de ese trayecto y la calidad y compromiso con la que asumieron los casos con los que trabajaron.

**¿Les preocupa los casos de personas aspirantes que hayan estado inmersas en escándalos? Por ejemplo, como sucede con el magistrado Francisco Ricaurte que está envuelto en el escándalo de corrupción de la Corte Suprema de Justicia**

Por supuesto que preocupa porque nos interesa que las personas que hayan sido seleccionadas no vayan a estar en cuestionamientos por posturas discriminatorias o escándalos de corrupción. Así que estaremos atentos al desarrollo de las discusiones. Lo que es importante recordar es que cualquier preocupación o denuncia sobre casos particulares debe enviarse por escrito a la página web para que pueda ser incorporada dentro de los soportes de la evaluación que vamos a hacer de los perfiles.

**Para los magistrados son 51 puestos, ¿Cómo se filtrará la información para que de las 2135 solo se escoja esas 51 personas?**

Nosotros estamos haciendo un análisis de fondo de cada una de las categorías de hoja de vida de estos consulados y tenemos unos indicadores internos que nos van a permitir valorar de una manera objetiva. Y la otra información la vamos a considerar como un elemento valioso que nos permita hacer listas más sencillas y cortas, especialmente la que vamos a hacer para entrevistas. Lo que se hará es una combinación entre la valoración objetiva de las hojas de vida, y las observaciones de la ciudadanía.

**¿Cuántas personas se entrevistarán finalmente?**

No se ha decidido un número, pero tendríamos que tener una cantidad en cada uno de los cargos que nos permita hacer una selección. Podría ser entre 2 y 3 entrevistados.

**En el congreso avanza con lentitud en la discusión sobre una Ley estatutaria de la JEP, ¿no debería avanzarse al mismo paso en ambas cuestiones?**

El comité ha sido muy sensible a los problemas de cronograma en estos temas, pero no es competencia nuestra porque nuestro mandato es muy concreto, y es alrededor de la selección de los magistrados, pero claramente el esfuerzo y el compromiso que hemos hecho de hacer un cronograma riguroso también implica que las demás partes que tienen que ver con el mismo asunto lo hagan de manera que rápidamente entre a funcionar esta Justicia Especial para la Paz.

<iframe id="audio_20404723" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20404723_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
