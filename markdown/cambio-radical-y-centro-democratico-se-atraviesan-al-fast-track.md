Title: Cambio Radical y Centro Democrático se atraviesan al Fast Track
Date: 2017-09-05 15:13
Category: Entrevistas, Política
Tags: Fast Track, Reforma Electoral
Slug: cambio-radical-y-centro-democratico-se-atraviesan-al-fast-track
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/pleno-del-congreso-e1495751460417.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [05 Sept 2017]

La reforma política y electoral irá a plenaria de la Caámara luego de que, según el representante por el Polo Democrático Alirio Uribe, existieran **“actos deliberados” por parte del Centro Democrático y Cambió Radical**, para dilatar y frenar el avance de este proyecto en la Comisión Primera de la Cámara de Representantes.

De acuerdo con Alirio Uribe, el primer campanazo de alerta es que “al paso que van está realmente en riesgo que se implementen todas las normas de Fast Track” porque son normas que requieren la aprobación de mayorías y procedimientos especiales que están “siendo obstruidas” por Cambio Rádical y el Centro Democrático, que **podrán hacer lo mismo en otros escenarios de debates y plenarias como el senado, a través de proposiciones de archivo que frenan los avances**.

### **Operación Tortuga** 

De acuerdo con el representante, las proposiciones de archivo fueron el mecanismo que se usó para hacer mucho más lento el trámite en la comisión primera **“argumentaron muchas cosas, por ejemplo, que la reforma política no tenía nada que ver con el acuerdo”** y las realizaban artículo por artículo, provocando que en muchos debates solo se pudiese exponer un artículo. (Le puede interesar: ["Reforma política suprime la Comisión de Acusaciones y crea el Tribunal de Aforados"](https://archivo.contagioradio.com/reforma-politica-46174/))

En total fueron **180 proposiciones para hacer modificaciones hechas por todos los partidos** y 9 debates los realizados por la Comisión Primera, de igual forma, Uribe manifestó que por este mismo escenario deberán pasar los proyectos de ley restante y considera que será “muy difícil” que los mismos alcancen a entrar en el Fast Track que termina en noviembre.

De la reforma política se eliminaron 9 artículos de los 21 que tenía el proyecto de ley, y que para Uribe fue necesario descartar para poder finalizar los debates “las eliminaciones se debieron al empantanamiento de los debates en la comisión primera” y agregó que de no hacerlo el debate se hubiese podido alargar hasta octubre. (Le puede interesar: ["Circunscripciones de paz se quedarían fuera de Fast Track por falta de voluntad politica"](https://archivo.contagioradio.com/circunscripciones-de-paz-se-quedarian-por-fuera-de-fast-track-si-no-hay-voluntda-politica/))

### **Los temores de Cambio Radical y el Centro Democrático** 

Según Alirio Uribe, los motivos para intentar frenar el avance de los proyectos legislativos en el Congreso, se infundan en las investigaciones que podrían destaparse y las consecuencias de los mismas “recordemos que el Uribismo parece una banda, todos los que estuvieron en el gobierno, ex ministros, **ex directores de departamentos administrativos, etc, procesados, condenados** y el propio hermano del presidente preso y en juicio por crímenes de lesa humanidad”.

Referente a Cambio Radical, el representante señaló que tiene **más de 19 congresistas condenados por parapolítica** “se nota de manera deliberada el miedo que hay a ciertos cambios y se van a expresar en la reforma política y en la JEP” afirmó Alirio Uribe. (Le puede interesar: "

### **El papel de la veeduría ciudadana** 

Frente a este panorama, el representante hizo un llamado a la sociedad para que respalde los acuerdos de paz y se continúe con las veedurías sociales a los debates “es importante rodear al Congreso, no hay suficiente voluntad política de implementar todos los compromisos de los acuerdos”. (Le puede interesar:["Reconciliación será el centro de la semana por la paz"](https://archivo.contagioradio.com/semana-por-la-paz/))

<iframe id="audio_20712464" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20712464_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
