Title: 90% de crímenes contra periodistas permanecen en la impunidad
Date: 2016-11-02 12:34
Category: DDHH, Nacional
Tags: colombia, Crimenes contra periodistas, FLIP Federación para la libertad de prensa Colombia, Libertad de Prensa
Slug: impunidad-crimenes-contra-periodistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/prensa1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Oro Negro Diario 

##### 2 Nov 2016 

En 2013 la Asamblea General de las Naciones Unidas aprobó declarar el 2 de noviembre como el **Día internacional para poner fin a la impunidad de los crímenes contra periodistas,** resolución que insta a los estados miembros a tomar medidas necesarias para prevenir las agresiones y asegurar que los responsables sean llevados ante la justicia garantizando así el derecho a las víctimas a la reparación.

### Periodistas en el mundo 

Según el 'Instituto de Prensa y Sociedad Internacional' de la Organización de las Naciones Unidas para la Educación, la Ciencia y la Cultura (UNESCO), **700 periodistas han sido asesinados a nivel mundial en lo corrido de 2016**, de los cuales sólo el **10% de los casos han recibido condena**. Esto, sin tener en cuenta las constantes violaciones sufridas en ataques no mortales como tortura, acoso,desaparición forzada, o detenciones arbitrarias. Le puede interesar: [En 2015 fueron asesinados 39 periodístas según la CIDH](https://archivo.contagioradio.com/en-2015-fueron-asesinados-39-periodistas-segun-la-cidh/).

Aproximadamente **94% de todos los periodistas asesinados fueron locales y sólo el 6% eran corresponsales extranjeros**, mientras que menos del **6% de los 680 casos de asesinatos a periodistas durante el periodo 2006 – 2014 han sido resueltos**. Según el informe emitido por Reporteros sin Fronteras en abril de 2016, de un total de 180 países, aquellos que gozan de una buena situación para el ejercicio periodístico son: Noruega, Suecia, Finlandia; Canadá, Australia y España son algunos de los países cuya situación se define más bien buena; mientras que **México, Irak, India y Colombia entran en el listado de los países donde la situación es difícil para los periodistas**.

### Situación en Colombia 

De acuerdo al segundo informe anual 'Paz en los Titulares, Miedo en la Redacción', emitido por la Fundación Libertad de Prensa (FLIP), el diagnóstico de la situación de los periodistas en Colombia para 2015 no fue nada bueno; con **147 ataques y un saldo de 232 víctimas**, convirtiéndose en uno de los años más violentos para el ejercicio periodístico desde que inició la presente década, **con** **un incremento en el número de violaciones del 39% respecto al 2014**, además de una grave panorama en materia de impunidad del 95%.

De los **152 casos de periodistas asesinados desde 1977 hasta el 2015, sólo en 4 de ellos se ha condenado al actor intelectual**. En 2015 se sumaron 74 casos prescritos, en los que **la justicia dejó vencer los procesos sin lograr sancionar a los responsables** de los crímenes, tanto actores intelectuales como materiales. Le puede interesar: [Fuerza pública es la mayor agresora de periodístas en Colombia](https://archivo.contagioradio.com/fuerza-publica-continua-siendo-la-mayor-agresora-de-periodistas-en-colombia/).

En lo corrido del 2016, en Colombia ya son **170 violaciones contra periodistas y 207 las víctimas**. Las que mayor número de incidencia tienen son la **amenazas con 67 casos; obstrucción al trabajo periodístico 34; agresiones 29; estigmatizaciones 12 y 11 atentados contra la infraestructura**. Los departamentos con mayor número de violaciones son; **Antioquia 14; la capital de Bogotá con 40 casos, Cauca 11; Casanare 10; Arauca 9 y Cesar con 7**. Le puede interesar: [Periodistas fueron agredidos 147 veces este año en Colombia](https://archivo.contagioradio.com/persisten-agresiones-e-impunidad-hacia-periodistas-en-colombia/).
