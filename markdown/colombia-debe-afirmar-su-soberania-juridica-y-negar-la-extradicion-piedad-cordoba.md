Title: "Colombia debe afirmar su soberanía jurídica y negar la extradición" Piedad Córdoba
Date: 2015-03-04 21:39
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Bernard Aronson, ELN, FARC, Horacio Serpa, piedad cordoba, Proceso de conversaciones de paz de la Habana, tratado de extradición Colombia-EEUU
Slug: colombia-debe-afirmar-su-soberania-juridica-y-negar-la-extradicion-piedad-cordoba
Status: published

###### Foto:Versión Final 

<iframe src="http://www.ivoox.com/player_ek_4160928_2_1.html?data=lZajkp6WfI6ZmKiak5mJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmpNTZ0dLGrcKfxcrPx5DFqsrmzsbfjdjZb9Tjw8rfw9OJh5SZoqnOjc%2FZtoa3lIqupsnNp8Kf2pDbx8zFtoyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Piedad Córdoba, Colombianos y Colombianas por la Paz] 

Para la defensora de Derechos Humanos y lideresa social, Piedad Córdoba, el debate sobre la extradición debe tener en cuenta, por una parte, que **Colombia no ha suscrito convenios de este tipo con los EEUU**, y por otra, que considerar extraditables a miembros de la guerrilla sería considerarlos narcotraficantes, negando su carácter político y subversivo.

En el marco del proceso de paz entre el gobierno y las guerrillas de las FARC y el ELN, se abre nuevamente el debate sobre el papel de la extradición de colombianos y colombianas hacia los EEUU.

Por una parte, mientras el presidente **Juan Manuel Santos** aseguró este martes 3 de marzo que el tema será motivo de discusión con los EEUU, para facilitar la firma de un acuerdo de paz que brinde confianza y garantías a la guerrilla; el asesor político **Shlomo Ben Amin** aseguró el lunes 2 de marzo que ya se están adelantando gestiones para la repatriación de **Simón Trinidad**.

El senador liberal, **Horacio Serpa**, se pronunció en el mismo sentido un su cuenta en twitter, declarando que "**Digan lo que digan, Simón Trinidad es un subversivo y no un narcotraficante. Ojalá vaya a La Habana**."

Para Piedad Córdoba, la presencia del delegado de los EEUU para la mesa de Diálogos de Paz de La Habana, **Bernard Aronson**, es importante en la medida en que puede hablarse el tema directamente con todas las partes implicadas.

Para Córdoba, no puede ser que los acuerdos que se firmen con la guerrilla no tengan credibilidad y confianza. **"Colombia debe afirmar su soberanía jurídica, y tiene el derecho y el deber de acabar con la confrontación armada y darle salida y viabilidad al proceso de paz".**

El gobierno no solo debería mostrar gestos de paz, sino tomar decisiones que permitan avanzar en el proceso, y en ese sentido, brindar garantías de reclusión digna y repatriación de Simón Trinidad y de Sonia, permitiría demostrar esa voluntad por parte del Estado. Colombia no solo puede esperar que la guerrilla haga gestos de paz, pues estos deberían venir de ambas partes. "El cese al fuego bilateral y la paz son Derecho y un deber ético impostergable, no solo porque este en la constitución", afirmó.

Para Córdoba, también **es "hora de que el gobierno le permita a los insurgentes de las FARC y el ELN hablarle directamente al país**, para explicar todo lo que se ha hecho, todos los esfuerzos y todo los obstáculos".

Un esfuerzo igualmente fundamental es la lucha contra el paramilitarismo. Para la defensora de Derechos Humanos, en Colombia no solo no se ha acabado con este flagelo, sino que el país se esta re-paramilitalizando, como respuesta de sectores terratenientes por defender sus beneficios, y por la incursión de sectores mineros y agro-industriales en diferentes regiones del país.
