Title: ¿Y qué dicen las comunidades rurales del glifosato?
Date: 2015-04-29 13:16
Author: CtgAdm
Category: Comunidad, Entrevistas
Tags: Alejandro Gaviria, Alejandro Ordoñez, Asociación Campesina del Norte de Antioquia, Conpaz, cultivos ilícitos, Glifosato, Juan Carlos Pinzón, ministerio de defensa, Ministerio de Salud, Naya, OMS, Organización Mundial de la Salud, Procuraduría General de la Nación, Putumayo
Slug: comunidades-tiene-propuestas-alternativas-al-glifosato-para-erradicar-cultivos-ilicitos
Status: published

##### Foto: [misionesonline.net]

<iframe src="http://www.ivoox.com/player_ek_4424173_2_1.html?data=lZmflpabd46ZmKiakp6Jd6KnmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTo0JDdy8rSt8LijNHO1ZDHs87pz87Rw8nJt4zYxtGYsMbdpYzn0Mffx5DJsIzbzc7T0djFuI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Celorio, CONPAZ ] 

<iframe src="http://www.ivoox.com/player_ek_4424169_2_1.html?data=lZmflpaafY6ZmKiak5eJd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTo0JDdy8rSt8LijNHO1ZDHs87pz87Rw8nJt4zYxtGYstrYuc7V2tSY1dTGtsafxtGYydHNqo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Sandra Lagos, Zonas de Reserva Campesina en Putumayo] 

Durante los últimos días se ha dado gran importancia a cada opinión que tienen las instituciones y actores gubernamentales sobre el glifosato, entre ellos, el Ministerio de Salud, de Defensa y la Procuraduría, hasta el senador Álvaro Uribe. Sin embargo, **se ha dejado de lado la voz de las comunidades,** que son las que experimentan los efectos de las fumigaciones con este herbicida.

**“Hace mucho tiempo estábamos esperando que dijeran que el glifosato afecta la salud”,** dice Sandra Lagos, de Zonas de Reserva Campesinas en el departamento de Putumayo, quien cuenta que la bebé de su hermana tiene problemas de salud, y lo único dicen los médicos es que la causa del estado de la niña se debe a la contaminación de químicos en el aire, “le pedíamos a los médicos que nos dieran algo por escrito, para sustentar el daño del glifosato, pero no lo hacen”, asegura Sandra.

Carlos Celorio, habitante del Naya, coincide con las afirmaciones de la vocera del Putumayo. Los pobladores a los que les cae gotas de glifosato, inmediatamente empiezan a **toser, sufren de enrojecimiento de los ojos y alergias en la piel, que generan piquiña en el cuerpo** que “a más de uno lo hace salir derecho a votarse al río”, dice Carlos.

Pero además de las afectaciones inmediatas a la salud, también las mujeres embarazas y los niños y niñas, sufren las consecuencias del herbicida a largo plazo, ya que suele causar **malformaciones en los bebes y hongos en la piel “que pican y duelen”,** cuenta la vocera del Putumayo, quien agrega que a pesar del aumento de las enfermedades, los médicos se abstienen de dar un diagnostico exacto del por qué de estas alteraciones en el organismo, pero **“dicen que hay contaminación en el aire y cuestiones toxicas que generan las enfermedades”.**

Según Víctor Tobón, de la Asociación Campesina del Norte de Antioquia, lo más “irónico’’ es que  “**la mata de coca es la que menos sufre con el glifosato,** en cambio, la yuca, el plátano, la caña y arroz son los cultivos más perjudicados’’, perdidas que significan un gran impacto económico y social para la población. En eso mismo coinciden las declaraciones de Carlos,  quien señala que el glifosato no solo está dirigido a “los pocos cultivos ilícitos que hay en el territorio”, sino que también **cae en los cultivos de pan coger,** como la papa china,  banano y el maíz, entre otros, ya que “cuando se fumiga comienzan a secarse los cultivos”, dice Celorio.

Sandra Lagos,  asegura que desde el Putumayo, las comunidades han trabajado en propuestas que permitan la erradicación total de la hoja de coca,  “sabemos que el cultivo de hoja de coca es un problema social, que el mismo gobierno ha permitido, porque no hay inversión social y no hay apoyo para el sector agropecuario, por lo que a algunas comunidades  no les queda otra alternativa (que cultivar coca) para sobrevivir”.  Aun así, **en Putumayo se han realizado cinco mil cartas donde se evidencia la voluntad de los pobladores  para acabar con los cultivo ilícitos,** “pero no hay voluntad del gobierno ni del ministro para que se acabe con ese problema de raíz”, indica Lagos.

La Organización Mundial de la Salud, OMS, reportó que el glifosato, es una sustancia toxica que se clasifica como una sustancia tipo 2A, es decir, que **puede causar cáncer a los  seres humanos, especialmente, patologías que tengan que ver con el sistema linfátic**o. En base a ese criterio el Ministro de Salud, Alejandro Gaviria, recomendó “suspender de manera inmediata el uso del glifosato en las operaciones de aspersión aérea para la erradicación de cultivos ilícitos”, sin embargo, el Ministro de Defensa, Juan Carlos Pinzón se mantiene firme en la decisión de continuar con las fumigaciones con glifosato.

En Colombia han sido fumigadas 1,5 millones de hectáreas de cultivos ilícitos desde el año 2000, lo que ha afectado la soberanía alimentaria de los campesinos, su salud y el ambiente, es por eso que Sandra, eleva una denuncia contra  el Procurador y el Ministro de Defensa, “me gustaría decirle al procurador al ministro que lentamente nos quieren acabar con las enfermedades, no les interesa la vida de ser humano, la permanencia de la gente en el territorio, **hacemos un llamado para que se acojan a las recomendaciones y propuestas de las comunidades para que se acabe radicalmente la hoja de coca”**.
