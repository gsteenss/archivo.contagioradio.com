Title: ¿Quién dijo miedo?
Date: 2017-05-10 11:58
Category: CENSAT, Opinion
Tags: consulta popular, Cumaral, hidrocarburos, Meta
Slug: quien-dijo-miedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Cumaral.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Espectador 

###### 10 May 2017 

#### **[Por: CENSAT Agua Viva /Amigos de la Tierra Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

En el nororiente de Villavicencio en la vía que conduce a Restrepo, en pleno piedemonte llanero se encuentra el municipio Cumaral – el nuevo escenario de consulta popular, programado para el 4 de junio. Esta vez, la consulta se realizara sobre la explotación de hidrocarburos.

![bloques petroleros](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/bloques-petroleros_piedemonte_2_2017.png){.wp-image-40297 .aligncenter width="450" height="299"}

Al norte, en lo alto de la cordillera está ubicado el Parque Natural Nacional Chingaza, proveyendo con bienes hídricos a la altillanura y los vastos ríos de la Orinoquía y Amazonía colombiana aportándole de esta manera agua y vida a toda una ecorregión del continente suramericano. No obstante de su importancia ecosocial, el municipio de Cumaral padece la tragedia de ser un territorio cubierto alrededor de un 90% por bloques petroleros (Condor, CP04, LLA 35, LLA 59 y LLA69 – ver mapa). Lo cual no puede ser considerado una excepción, sino más bien, la triste regla en un departamento donde cerca de un 80% del área cuenta con el mismo destino (ANH, 2017), aun sabiendo que la explotación petrolera agota, profundiza y contamina las aguas de manera irreparable.

Esta situación ha provocado fuertes resistencias en las comunidades locales y Cumaral es apenas un municipio más del piedemonte llanero, donde las comunidades se están organizando para evitar la entrada de empresas petroleras a sus territorios.

Es el caso de la vereda Pio XII, del Municipio Guamal, existe desde hace casi tres meses una asamblea permanente de sus habitantes en defensa de su territorio agropecuario y en contra de los planes de explotación del bloque CP09 por parte de Ecopetrol. Este mismo bloque petrolero amenaza además la sustentabilidad de los municipios de Guamal, San Carlos de Guaroa, Cubarral, San Martín, El Dorado y El Castillo y es por ello que sus habitantes no solo están pendientes de la consulta popular de Cumaral, apoyando cómo y dónde les es posible, sino que al mismo tiempo están en la construcción de consultas populares propias.

La consulta popular de Cumaral debe convocarnos nuevamente, como en Cajamarca, Cabrera, Piedras, Tauramena, para que quienes defendemos la vida y el agua, enfrentemos la dictadura minero-energética que se ha querido imponer en contra de la voluntad popular. Esta consulta es una nueva oportunidad para renovar la cultura política de este país, acostumbrados a ir a las urnas en medio de condiciones adversas, en los que los politiqueros dejan ver sus turbulentas formas corruptas.

Construir una consulta popular es mucho más que el derecho al voto: Es explorar nuevas pedagogías que construyen otras formas de hacer política en un país que tanto lo requiere; es acercarnos nuevamente a quienes nos rodean, es hacer ollas comunitarias, caminatas, tertulias; es conspirar, soñar, compartir, amar; es entender, apoyar y apostar; es sentir la solidaridad como ejercicio concreto y la construcción diaria de nuevas relaciones sociales y ambientales.

Es decir, cuando empezamos a tejer estas solidaridades, traspasando las fronteras municipales y departamentales, construyendo espacios concretos de lucha, alegría y resistencia como va a ser el 2 de junio durante el gran carnaval por la vida en Ibagué, Armenia, Cumaral, Florencia, Bogotá, Cabrera y Libano entre otros, la consulta popular debe entenderse como otro vehículo importante de movilización mas no como un fin en sí mismo como suelen equivocarse algunos y ante este escenario, la votación será apenas uno de los tantos frutos que cosecharemos y por ello reiteramos la pregunta: ¿Quién dijo miedo?
