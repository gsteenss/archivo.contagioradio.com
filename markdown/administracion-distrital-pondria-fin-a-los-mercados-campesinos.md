Title: Administración distrital pondría fin a los Mercados Campesinos
Date: 2016-03-31 16:02
Category: Movilización, Nacional
Tags: Enrique Peñalosa, mercados campesinos, secretaria de desarrollo bogotá
Slug: administracion-distrital-pondria-fin-a-los-mercados-campesinos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Mercados-Campesinos.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alcaldía Bogotá ] 

###### [31 Mar 2016 ]

Por lo menos 2000 pobladores de Tolima, Meta, Boyacá y Cundinamarca, que desde hace más de diez años realizan mercados campesinos en distintos parques de las localidades de Bogotá, exigen a la actual administración de la capital, y en particular a la 'Secretaria de Desarrollo Económico', que renueve los convenios que permiten su ejecución; teniendo en cuenta los **beneficios que han representado tanto para los productores como para los capitalinos**,  afirma Julián Corredor, integrante de 'Comité de Interlocución Campesino'.

De acuerdo con Corredor, estos mercados han beneficiado a los productores campesinos porque al no existir intermediarios, ellos apropian el 30% del valor de cada alimento vendido, mientras que **los capitalinos pueden comprar productos orgánicos, de calidad y a precios justos**. En suma, una práctica que además de estar amparada por el Acuerdo 455 de 2010 y el Decreto 315, abastece el 65% de la canasta de alimentación básica bogotana y permite fortalecer la política de seguridad alimentaria de la ciudad.

Según el actual Secretario de Desarrollo Económico, Freddy Castro, el retraso en la firma de los convenios se da en primer lugar, porque no se cuenta con los recursos necesarios para implementar los mercados en varios de los parques de la capital colombiana y en segundo instancia, **porque la actual administración decidió revisarlos ante posibles inconsistencias**, relacionadas con las organizaciones que los firman, los campesinos que se benefician y la calidad de los productos que son vendidos.

Argumentos que Corredor responde asegurando que los 2000 campesinos que conforman el programa, son **pequeños productores dueños o arrendatarios de modestos terrenos, que muchas veces deben poner recursos propios para traer sus productos a la ciudad**. Por otra parte, hay una capacitación para el manejo, presentación y producción de los alimentos, para que respondan a los diferentes estándares de calidad que se son exigidos, agrega.

Las organizaciones campesinas participantes de estos mercados están programando una concentración el próximo martes 5 de abril en las instalaciones de la Secretaria de Desarrollo Económico, para presionar la firma de los convenios o una respuesta concreta al respecto, pues de no continuarse con esta práctica **se vería afectada la política de soberanía alimentaria capitalina**, así como la supervivencia de las comunidades campesinas en condiciones dignas.

<iframe src="http://co.ivoox.com/es/player_ej_10999338_2_1.html?data=kpWmm56Xd5mhhpywj5aXaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncavpzc6SpZiJhZLijKjc1NfJqNDmhpewjc7SuMbb08bb1sqPqMafhpekpdTRrdWZpJiSo56PqMafqtPhx9fQs8TpxM6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 

 
