Title: Autoridades Tradicionales denuncian agresiones de la Policia en Putumayo
Date: 2020-09-07 18:36
Author: CtgAdm
Category: Actualidad, Comunidad
Slug: autoridades-tradicionales-denuncian-agresiones-de-la-policia-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/El-Mango-policia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Archivo Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La **Asociación de Autoridades Tradicionales Mesa Permanente de Trabajo por el Pueblo Cofán** y Cabildos Indígenas del municipio del Valle del Guamuez y San Miguel, ubicado en el departamento de [Putumayo](https://archivo.contagioradio.com/bombardeo-en-resguardo-alto-rio-bojaya/)**denunciaron agresiones por parte de la Policía Antinarcóticos encontra de integrantes de sus comunidades.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el comunicado, el jueves 3 de septiembre al Cabildo indígena ubicado en Villanueva, ingresó un helicóptero ocupado por agentes de la Policía Antinarcóticos quiénes, ***"de forma agresiva y vulgar, maltrataron a nuestros comuneros"*, quienes se desplazaron desde diferentes lugares en busca de respuestas ante esta intervención en su territorio.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las Autoridades Tradicionales denunciaron que, ***"sin medir palabra la policía utilizó gases lacrimógenos que fueron disparados algunos a quemarropa"*** contra la comunidad, sin importar la presencia de menores de edad, mujeres lactantes y funcionarios que hacían un recorrido por el territorio en el marco del acuerdo suscrito en defensa de la biodiversidad y las áreas protegidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, la agresiones de los agentes causaron graves lesiones en la integridad de varios indígenas. Producto de ello, tres personas fueron trasladadas al Hospital Sagrado Corazón de Jesús debido a la gravedad de las heridas. ([Policía Nacional estaría involucrada en masacre de los niños en Llano Verde, Cali](https://archivo.contagioradio.com/policia-nacional-estaria-involucrada-en-masacre-de-los-ninos-en-llano-verde-cali/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los afectados fueron identificados como, **Carlos Eduardo Chapal de 57 años y Gobernador del Cabildo, quién sufrió un trauma en el tórax**; también **Carlos Fernando Benavides**, de 37 años, tesorero del Cabildo, quien resultó herido en el muslo izquierdo y **Maximiliano Chachinoy, de 46 años, quien según el reporte médico** *"presenta una lesión en flanco izquierdo, con ulceración redondeada en anillo, de aproximadamente 4 cm de diámetro, y equimosis de aproximadamente 15 cm".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez, también **señalaron que la Mayora Rosa Nelli Chapal, fue atacada con varios disparos sin alcanzar a ser lesionada gracias a la protección de los árboles**. A este hecho de persecución se sumaron las amenazas que se registraron hacia los estudiantes que se encontraban en este territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, producto de la inhalación de los gases lacrimógenos un menor de aproximadamente 9 meses de edad presentó convulsiones, *"**abusaron de la fuerza, contaminaron el medio ambiente, intervinieron sitios sagrados y asustaron con su actuar a los estudiantes** que se encontraban visitando y realizando un reconocimiento de los sitios sagrados"* afirmaron las comunidades indígenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Destacando que el p**ueblo Cofán es una cultura 100% pacífica, y agredida y lesionada mientras se encontraba totalmente desarmada** y, *"a la merced de la brutalidad de los miembros de la Policía Antinarcóticos, estos efectivos no dieron oportunidad alguna de dialogar o explicar las razones de la invasión de nuestro territorio".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Se dirigieron con palabras soeces, denigrantes, discriminatorias, desafiantes despreciables hacia nuestros comuneros".*

<!-- /wp:quote -->

<!-- wp:paragraph -->

**Intervención que además desconoce que la comunidad está organizada y tienen una vocería a través de la Asociación de las Autoridades Tradicionales** de la Mesa Permanente de Trabajo, mecanismos que fueron saltados, agrediendo a varios de estos líderes y lideresas indígenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo, hicieron un llamado a la comunidad nacional e internacional ante estas agresiones y señalaron que este pueblo ha sido totalmente [olvidado por el Gobierno Naciona](https://www.justiciaypazcolombia.com/nuevo-asesinato-en-puerto-asis-por-la-estructura-armada-comandos-de-la-frontera/)l y las instituciones administrativas. **Por tanto una de las pocas alternativas que tienen para la supervivencia es la siembra de coca, que si bien no es justificación es una realidad que cubre la mitad de Colombia.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Es irónico que nosotros los indígenas tengamos que recordarle a la Fuerza Pública y al Estado colombiano que estamos en Estado Social de Derecho y no de hecho"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Finalmente convocando el apoyo y veeduría por parte de la comunidad internacional y los organismos de control del Estado, así como la Comisión Nacional de Derechos Humanos, destacaron que desde el 2004 la Corte Constitucional a través de la [sentencia T-025 de 2004,](https://www.corteconstitucional.gov.co/relatoria/2004/t-025-04.htm) **declaró que el pueblo indígena Cofán se encuentra en condición de exterminio, y por lo tanto es responsabilidad del Estado colombiano proteger respetar** y salvaguardar la vida de los integrantes de estas comunidades étnicas.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
