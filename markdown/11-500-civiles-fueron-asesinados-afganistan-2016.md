Title: 11.500 civiles fueron asesinados o heridos en Afganistán durante 2016
Date: 2017-02-06 15:30
Category: DDHH, El mundo
Tags: Afganistán, Civiles muertos, guerra, ONU
Slug: 11-500-civiles-fueron-asesinados-afganistan-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Afganistan.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Miled] 

###### [6 Feb. 2017] 

Para Afganistán el 2016 no fue un año esperanzador, **cerca de 11.500 civiles afganos perdieron la vida o estuvieron heridos,** lo más desalentador es que **más de 3.500 niños y niñas hacen parte de estas lamentables cifras,** dadas a conocer este lunes en un informe conjunto de la Oficina del Alto Comisionado de la ONU para los Derechos Humanos y la Misión de Asistencia de las Naciones Unidas en Afganistán (UNAMA).

**En relación con el año anterior hubo un aumento del 3% de víctimas civiles**, pero lo más grave y desalentador de este informe es que **son los niños y las niñas las más afectadas por el conflicto, cifra que aumentó en un 24% en comparación con el 2016.**

Los ataques aéreos que fueron llevados a cabo por fuerzas afganas e internacionales causaron **590 víctimas civiles (250 muertos y 340 heridos)** casi el doble que el registrado en 2015 y el más alto desde 2009. Le puede interesar:[ Personal de EEUU robó 52 millones de dólares en Irak y Afganistán](https://archivo.contagioradio.com/personal-de-eeuu-robo-52-millones-de-dolares-en-irak-y-afganistan/)

Para UNAMA “la violencia relacionada con el conflicto causó un gran daño en Afganistán en 2016” y aseguraron que **esta guerra ha destruido no solo vidas y comunidades en cada rincón de ese país, sino también los sueños de cientos de nuevas vidas** que comienzan a crecer en medio de la devastadora guerra.

"Los niños han sido asesinados, o han quedado incapacitados. Han tenido que ver pasar inadvertidamente ​​la muerte de sus amigos. **Algunos niños han perdido la vida mientras juegan con artefactos explosivos sin detonar que son negligentemente dejados atrás por las partes en el conflicto**” contó Zeid Ra'ad Al Hussein, El Alto Comisionado de las Naciones Unidas para los Derechos Humanos.

**Las mujeres también son víctimas de este conflicto sufriendo violaciones a sus derechos,** siendo brutalmente castigadas mientras esperan los llamados procesos de "justicia", dijo Zeid.

Han sido casi 40 años de conflicto armado que continua en evolución en Afganistán, por lo que la ONU manifestó que ya era hora de que las diversas partes en el conflicto cesaran la incansable comisión de crímenes de guerra y “pensaran en el daño que están haciendo a sus madres, padres, niños y futuras generaciones al continuar alimentando este conflicto sin fin y sin fin " dijo Zeid. Le puede interesar: [CIA y Fuerzas Armadas de Estados Unidos habrían cometido crímenes de guerra](https://archivo.contagioradio.com/cia-y-fuerzas-armadas-de-estados-unidos-habrian-cometido-crimenes-de-guerra/)

### **Desplazamiento forzado** 

Como mecanismo para salvaguardar la vida, cientos de familias optan por moverse de su lugar de habitación y otras deben hacerlo como consecuencia de los bombardeos que destruyen viviendas.

**Para el 2016 fueron 623.000 afganos los que se vieron obligados a abandonar sus casas**, es decir cerca de un 30% más que en 2015, según cifras de la Oficina para la Coordinación de Asuntos Humanitarios (OCHA). Le puede interesar: [Refugiados y esclavitudes](https://archivo.contagioradio.com/refugiados-y-esclavitudes/)

Por último, el informe hace un llamado urgente a las partes implicadas en este conflicto a que cesen las acciones militares y los ataques suicidas, dado que **las consecuencias de cada acto de violencia impactan de manera directa en cientos de familias y comunidades** enteras que quedan rotas, incapaces de sostenerse y en gran medida no logran obtener justicia o reparación de los hechos que son víctimas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio.

<div class="ssba ssba-wrap">

</div>
