Title: Paramilitares de las Autodefensas Gaitanistas arremeten contra la USO
Date: 2015-01-29 17:42
Author: CtgAdm
Category: DDHH, Otra Mirada
Tags: Paramilitarismo, SIndicatos, USO
Slug: paramilitares-de-las-autodefensas-gaitanistas-arremeten-contra-la-uso
Status: published

###### Foto: animalpolitico.com 

##### [Edwin Cardozo, USO]<iframe src="http://www.ivoox.com/player_ek_4010716_2_1.html?data=lZWekpyVeo6ZmKiakpuJd6Knl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRicXrytOYpcbWqNDu0IqfpZC0tsbnysnS0NnJb8XZjNHOjbq3k46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

La Unión Sindical Obrera de Colombia, USO, denuncia durante el mes de enero se vienen presentando hechos de **agresión en contra de sus dirigentes** en diferentes partes del país. Muchas de las amenazas provienen del grupo paramilitar denominado **Autodefensas Gaitanistas.** Una de las primeras víctimas fue Oscar García, quién el pasado 6 de enero, se movilizaba en un vehículo impactado en tres ocasiones con arma de fuego.

Para Edwin Castaño, presidente de la USO, esta ola de amenazas se debe a la decisión **férrea del sindicato por proteger los intereses del país** frente al propio gobierno nacional que pretende continuar privatizando y de las multinacionales que hacen presencia en el territorio nacional.

Castaño explica que la situación es preocupante puesto que las amenazas y los atentados son reiterativos en contra de los integrantes del Sindicato, y a pesar de ello, hasta el momento no hay resultados claros de la investigación de los organismos competentes y las medidas de protección cada vez son menos eficaces.

Según un comunicado de la USO “esta sucesión de hechos se presentan todos durante este mes de enero, **simultáneamente** en todo el país, en forma sistemática y guardando características similares lo cual hace notar un **plan preconcebido desde un centro único de coordinación”**

Otros de los dirigentes amenazados son David Mauricio Gómez, Luis Carlos López, Iván Guerrero, Carlos Emilio Rodríguez, Rafael Cabarcas, Ariel Corzo, Libardo Hernández, Jhon Rodríguez, Marco Antonio Montes, Iván Alberto Cáceres, Alexander Castro y Rodolfo Valentino Prada.
