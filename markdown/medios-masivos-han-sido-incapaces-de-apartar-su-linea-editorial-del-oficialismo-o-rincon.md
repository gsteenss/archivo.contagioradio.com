Title: Medios masivos "han sido incapaces de apartar su línea editorial del oficialismo" O Rincón
Date: 2020-09-17 11:48
Author: AdminContagio
Category: Actualidad
Tags: Estigmatización en los medios, Medios masivos, Movilización social, Protesta social
Slug: medios-masivos-han-sido-incapaces-de-apartar-su-linea-editorial-del-oficialismo-o-rincon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Estimatizacion-de-la-protesta-en-los-medios-masivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

A propósito del cubrimiento que han realizado algunos medios masivos de comunicación sobre las recientes protestas en contra de la brutalidad policial y los crímenes de Estado, desatadas entre otros hechos, por el asesinato de Javier Ordóñez a manos de la Policía Nacional, en los que se evidencian algunos sesgos y estigmatizaciones en contra de la movilización social y los manifestantes; [Contagio Radio](https://twitter.com/Contagioradio1) consultó a un experto para que expusiera su perspectiva frente al tema. (Lea también: [Antes del 9 de septiembre se presentaron por lo menos 1.708 denuncias de abuso policial](https://archivo.contagioradio.com/antes-del-9-de-septiembre-se-presentaron-por-lo-menos-1-708-denuncias-de-abuso-policial/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El profesor Omar Rincón, investigador y crítico de medios, señaló al respecto, que **las cadenas de comunicación masivas, actúan como altavoz de los discursos del Gobierno porque históricamente han sido incapaces de apartar su línea editorial del oficialismo.** Esto se debe, según Rincón, a un discurso institucional en el que se crea un enemigo común al que se le atribuyen todos los problemas en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Particularmente, frente al tema de la movilización, planteó que **existe una «*demonización de la protesta social*» a través de la reproducción de los señalamientos y estigmatizaciones que se originan desde el Gobierno.** Rincón, señaló que el Acuerdo de Paz había planteado un escenario en el que la protesta social había sido entendida como el ejercicio de la libertad de expresión; pero que en la actualidad se estaba volviendo a un discurso en el que se la relacionaba con grupos subversivos para quitarle legitimidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para el catedrático, se ha regresado a un paradigma discursivo en el que todo lo malo que sucede en el país es atribuible a la insurgencia o a un «*enemigo interno*». **Rincón, señaló que aplicar la lógica del Gobierno sería afirmar que había una organización subversiva esperando que la Policía asesinara a un ciudadano para sacar a la gente a las calles y en ese sentido, que “*los Policías que mataron a \[Javier\] Ordoñez son infiltrados del ELN*”.** (Le puede interesar: [ELN niega vinculación a manifestaciones y reitera voluntad de diálogo](https://archivo.contagioradio.com/eln-niega-vinculacion-a-manifestaciones-y-reitera-voluntad-de-dialogo/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> «Hemos vuelto  a un discurso antiguo. Anteriormente cada vez que pasaba algo era culpa de Pablo Escobar y de la guerrilla»
>
> <cite>Omar Rincón, investigador y crítico de medios</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por otro lado, el profesor Rincón, se refirió al expresidente Álvaro Uribe y afirmó que los medios tendrían que aplicar criterios periodísticos antes de replicar lo que este manifiesta, ya que, los medios solo se quedan en la declaración y no entran a verificar y a contrastar la veracidad de lo dicho. (Lea también: [‘Breaking News’ y el caso Uribe en medios tradicionales](https://archivo.contagioradio.com/breaking-news-y-el-caso-uribe-en-medios-tradicionales/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> «Uribe es el rey de Colombia porque tuitea y los medios inmediatamente reproducen lo que él dice»
>
> <cite>Omar Rincón, investigador y crítico de medios</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### El criterio de las audiencias frente al contenido  que reciben de los medios masivos

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para Rincón, lo primero que las audiencias deben tener claro a la hora de abordar la información transmitida por los medios masivos de comunicación, es que estos son actores políticos, y como tal, asumen una postura frente a cada tema coyuntural.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El experto destacó que existan las redes sociales, las cuales, logran construir una agenda informativa más allá de la que imponen los medios, pero previno sobre su adecuada utilización.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, sugirió que los usuarios deben apartarse de contenidos que no les generen confianza y aplicando una analogía con el contexto de la pandemia, señaló que el usuario debe aplicar «*una sana distancia*» cuando algo no le guste o le genere duda, «*ponerse la mascarilla y lavarse las manos*» para no llegar a reproducir este tipo de contenidos; porque cada vez que el usuario reenvía una información que le molesta, la está apoyando, teniendo en cuenta que lo que vale actualmente es cuántos *clicks* tiene una pieza comunicativa, sea cual sea.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> «No seamos tan bobos de promover esos contenidos, los contenidos que nos gustan, no los leemos, no los miramos, no los replicamos»
>
> <cite>Omar Rincón, investigador y crítico de medios</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->
