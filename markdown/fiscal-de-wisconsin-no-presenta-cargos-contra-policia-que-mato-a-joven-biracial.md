Title: Policía que mató a Tony Robinson queda libre de cargos por la Fiscalía
Date: 2015-05-13 16:52
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Ferguson protestas, Fiscal de Wisconsin no presentará cargos contra policía Kenny, Tony Robinson, Violencia policial en EEUU contra jóvenes desarmados afrodescendientes
Slug: fiscal-de-wisconsin-no-presenta-cargos-contra-policia-que-mato-a-joven-biracial
Status: published

###### Foto:Time.com 

El fiscal que lleva la investigación de la muerte del **joven biracial Tony Robinson de 19 años** quien murió a manos de la policía mientras estaba desarmado, ha declarado que no presentara cargos contra el policía que le disparó.

**Ismael Ozanne, fiscal de Wisconsin** "…Mi conclusión es que esta muerte trágica y lamentable fue el resultado del uso legítimo de la fuerza de la policía y que no deben de presentarse cargos contra el oficial Kenny en la muerte de Tony Robinson…".

Para el fiscal, el joven Robinson momentos antes de morir mantuvo una actitud peligrosa y agresiva que provocaba miedo para las personas y para los agentes de seguridad.

Tres llamadas al servicio de emergencias alertaron de la actitud del joven que estaba agrediendo a otros jóvenes y viandantes mientras corría sobre los coches e intentaba entrar en ellos.

El médico forense informó que en la autopsia al joven se le habían encontrado drogas en la sangre, tales como setas alucinógenas, marihuana y pastillas de Xanax, por lo que el joven habría actuado bajo la influencia de las sustancias tóxicas que había consumido.

Para la policía la actuación del agente Kenny fue la justa debida al peligro que reportaba el estado de alteración en el que estaba el joven.

Para la familia y organizaciones sociales de defensa de los derechos humanos de la comunidad negra como **la  “Coalición de Jóvenes, Talentosos y Negros”,** este es otro acto más de **racismo institucionalizado y de impunidad de la policía.**

Además las organizaciones desconfían del sistema judicial por ser racista como en el caso de Ferguson y acusan al fiscal de reproducir dichas dinámicas.

En Abril se iniciaron **protestas de carácter pacífico** que acabaron con la detención de 4 manifestantes por bloquear carreteras.

**Ozanne** se ha defendido de las críticas diciendo que él es uno de los primeros afrodescendientes que ocupa el cargo de fiscal en el distrito , además ha dicho que es importante la lucha no violenta contra las injusticias como dijo Martin Luther  King.

En el último año se ha recrudecido la violencia a manos de policías blancos contra jóvenes negros desarmados**,** algunos de los jóvenes asesinados  como **Michael Brown, en Ferguson**, Missouri; Eric Garner, en Nueva York; y Walter Scott, en Carolina del Sur.

Después del informe emitido por la justicia de EEUU que afirmaba que  la justicia en el estado de Ferguson sigue un **patrón racista**, no han parado de suceder asesinatos similares relacionados con la violencia racista institucional.
