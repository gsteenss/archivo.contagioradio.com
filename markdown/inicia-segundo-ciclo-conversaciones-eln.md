Title: Los retos del segundo ciclo de conversaciones del gobierno con ELN
Date: 2017-05-18 17:30
Category: Nacional, Paz
Tags: ELN, paz, Quito
Slug: inicia-segundo-ciclo-conversaciones-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/2017-01-18t232753z-995459138-rc12d1289d10-rtrmadp-3-colombia-eln-paz-e1495146480329.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Reuters] 

###### [18 May 2018] 

En medio de la zozobra por las recientes ordenes de la Corte Constitucional y los anuncios anuncios de las FARC, se abrió el segundo Ciclo de conversaciones entre el gobierno colombiano y la guerrilla del ELN, en Quito.

La mesa apoyada con el gobierno ecuatoriano. Pablo Beltrán, jefe de la delegación del ELN, expresó que aspiran a que la paz “ayude y traiga beneficios para todos los países vecinos incluido el Ecuador”. Por su parte, Juan Camilo Restrepo, por parte del gobierno, **manifestó sus deseos de que la mesa traiga frutos y acerque a las partes.**

En un comunicado del ELN agradeció al recién electo presidente Lenín Moreno por su esfuerzo y solidaridad y además saludó el apoyo de la comunidad internacional y los países garantes.

Asimismo, señalan que este segundo ciclo se inicia con la esperanza que inspira el  reciente encuentro entre el Comando Central – COCE, y el secretariado de las FARC, “Donde se concluyó  que el proceso de paz  de ambas organizaciones “mantiene objetivos comunes con  caminos diversos pero complementarios’’. Y agregan que **tras ese encuentro, uno de los objetivos es mantener el mecanismo permanente de coordinación y diálogo.**

Es así como se espera que la mesa logre acuerdos que  solucionen la situación humanitaria, de violencia en los territorios, y que generen garantías para la participación de todas las personas. En ese marco, anuncian que buscan acordar ** Audiencias Preparatorias donde se haría intercambio de ideas y experiencias  con diversos sectores de la sociedad.**

Finalmente el ELN indica que reafirma su “**compromiso y  voluntad por construir junto al pueblo una paz estable y duradera.** Colombianas y colombianos, trabajemos por una salida dialogada, por un proyecto de transformaciones necesarias y urgentes para el país”.

###### Reciba toda la información de Contagio Radio en [[su correo]
