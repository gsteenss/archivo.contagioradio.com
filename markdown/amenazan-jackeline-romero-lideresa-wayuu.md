Title: Amenazan a Jackeline Romero lideresa Wayúu
Date: 2016-12-15 17:23
Category: DDHH, Nacional
Tags: Amenazas, La Guajira, lideres sociales
Slug: amenazan-jackeline-romero-lideresa-wayuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Jacqueline-Romero-e1481840007816.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Identidad sin limites, blog 

###### [15 Dic 2016] 

La ola de acciones violentas contra líderes y lideresas sociales, llega ahora a La Guajira, donde Jackeline Romero, integrante de la organización Fuerza Mujeres Wayúu, es ahora víctima de una amenaza por parte de actores aún desconocidos.

“**No se meta en lo que no le incumbe evite problemas,** sus hijas están muy lindas y piense en ellas, gran malparida perjudicial evite problema porque  hasta su madre se la desaparezco para que siga de sapa…”, es el mensaje que llegó el celular de la lideresa indígena, el pasado 13 de diciembre a las 6:49 de la tarde.

Romero recibe la amenaza mientras participaba en un diálogo Multisectorial sobre  el derecho de la Consulta Previa en Cartagena, donde participó en un grupo de trabajo con organizaciones indígenas, entidades del estado y la cooperación internacional.

Así mismo, ha sido una de las personas que ha estado al frente de las denuncias por la falta de agua y alimentos para las comunidades Wayúu. Esta lideresa se ha centrado en las **denuncias por las licencias que ha dado el gobierno nacional a la empresa minera Carbones El Cerrejón,** que actualmente busca desviar una de las fuentes de agua más importantes para la población, el arroyo Bruno. [(Le puede interesar: “No solamente son niños, es todo el pueblo wayúu el que se está muriendo”).](https://archivo.contagioradio.com/no-solamente-son-ninos-es-todo-el-pueblo-wayuu-el-que-se-esta-muriendo/)

“Mientras los indígenas y demás comunidades siguen muriendo de sed a falta de agua, **se sigue otorgando licencias ambientales a empresas como El Cerrejón para que desvíe arroyos y ríos de los cuales se proveen las familias”,** señaló Jackeline Romero en la última entrevista que le hizo Contagio Radio.

Esta no es la primera vez que amenazan a la líder indígena y a su familia. Desde el año 2005 grupos paramilitares la vienen asediando. En el 2012  amenazaron directamente a Jazmín Romero Epiayu, hermana  de Jackeline y también lideresa de la comunidad Wayúu, posteriormente en el 2014 su hija menor, Génesis Gutiérrez, fue víctima de intimidaciones. Sin embargo, aunque los hechos han sido dados a conocer a las autoridades, **sigue sin conocerse los responsables de estas amenazas.  **

Frente a los nuevos hechos, ya se hicieron las denuncias pertinentes a la Fiscalía General de la Nación. Además la organización Fuerza de Mujeres Wayúu y El Resguardo Indígena Wayuu El Zahino del Municipio de Barrancas en el Sur de La Guajira hace un llamado a las autoridades nacionales y a la comunidad internacional para que **ponga especial atención a esta situación  y se adelanten las acciones pertinentes para proteger a la población Wayúu y a sus líderes.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
