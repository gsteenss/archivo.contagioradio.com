Title: Combates del ELN con paramilitares dejan 60 muertos según esa guerrilla
Date: 2017-05-08 12:30
Category: DDHH, Nacional
Tags: conflicto armado, ELN, paramilitares
Slug: combates-del-eln-con-paramilitares-dejan-60-muertos-segun-esa-guerrilla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Agencia-de-noticias-un.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia de Noticias UN] 

###### [08 May 2017] 

El Frente de Guerra Occidental de la guerrilla ELN afirmó que han producido más de 60 muertes en las filas de los paramilitares y denunciaron que **hay un respaldo abierto de las FFMM puesto que estarían trasladando a los heridos y ofreciendo dinero para reponer las bajas** en las filas de las estructuras armadas que refuerzan su presencia en el departamento y la región.

El comunicado conocido este 8 de Mayo, el Ejercito Liberación Nacional, expresó que ha sostenido enfrentamientos con paramilitares en el Bajo Atrato, exactamente en la parte alta del Río Truandó, a 50 kilómetros del casco urbano de Río Sucio, desde el mes de marzo, **dejando como resultado más de sesenta bajas en combate, desplazamientos de las comunidades afro e indígenas hacia Buenaventura** y el confinamiento de aproximadamente 3.000 mil personas en el Bajo San Juan Litoral.

El ELN indicó que el pasado 13 y 14 de abril bombardearon con artillería los campamentos paramilitares que se encontraban en el Caño de Truandó, **ocasionándoles 47 bajas “entre muertos, heridos y desaparecidos”** y señalaron que los sobrevivientes a este ataque se refugian en los barrios de Río Sucio.

De igual forma, denunciaron que para reponer las bajas de las estructuras paramilitares “**oficiales del ejército ofrecen hasta 10 millones de pesos a pobladores del norte del Chocó y a ex guerrilleros de las FARC-EP**”. Le puede interesar: ["El paramilitarismo sí existe y cometió más de 550 violaciones a los DDHH en el 2016"](https://archivo.contagioradio.com/paramilitarismo_si_existe_cinep/)

Desde hace 3 meses las comunidades del Bajo Atrato, han venido denunciado la presencia paramilitar y los enfrentamientos con la guerrilla del ELN que han provocado el desplazamiento de **más de 400 familias a Buenaventura y el confinamiento de otras 3.000 personas que no han podido abastecerse de alimentos** ni continuar con sus labores tradicionales por el temor a los combates. Le puede interesar: ["Mes y medio completan familias desplazadas del San Juan del Litoral"](https://archivo.contagioradio.com/un-mes-y-medio-completan-400-familias-desplazadas-del-san-juan-del-litoral/)

### **¿Qué esperar del encuentro ELN y FARC en Cuba?** 

Estos hechos de guerra ocurren en la víspera de que inicie el segundo ciclo de conversaciones entre el ELN y el equipo de diálogo del gobierno, bajo la dirección de Juan Camilo Restrepo, que busca avanzar en la construcción de una ruta **sobre los puntos de participación social y el desescalamiento del conflicto con acciones humanitarias**. Según el jefe del equipo negociador del gobierno el próximo ciclo iniciará el próximo 16 de Mayo en Quito.

De igual forma, delegaciones de las FARC-EP y del ELN, se encontrarán esta semana en Cuba, para, según el analista político e integrante de la Mesa Social para la Paz, Luis Eduardo Celis, “**compartir valoraciones de los que fue el proceso de paz de La Habana y de cómo avanza el de Quito**”, a su vez Celis señaló que este es el momento de plantearse una estrategia conjunta que le permita a los dos sectores continuar en las transformaciones políticas a las que aspiran.

<iframe id="audio_18561219" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18561219_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

*Comunicado de prensa del ELN*

La GUERRA en Colombia continua.  
Fecha: 2017 04 30  
Ejército de Liberación Nacional (ELN)  
47 BAJAS A PARAMILITARES Y MILITARES EN RÍO TRUANDÓ, CHOCÓ

En el municipio de Riosucio, norte del Departamento de Chocó continúa la expansión de los grupos paramilitares de extrema derecha, los que seguimos combatiendo; en igual forma como lo hemos hecho, en los últimos años, como en el 2015, cuando los confrontamos conjuntamente con unidades de las Farc, pese a que éstas se encontraban en los diálogos de La Habana.

En la parte alta del río Truandó, a 50 kilómetros del casco urbano de Riosucio y a 40 kilómetros de la frontera con Panamá, desde marzo pasado se han intensificado los combates entre unidades combinadas de ejército estatal y paramilitares, contra nuestra Compañía guerrillera Néstor Tulio Durán. Confrontación en la que hemos causado más de 60 bajas a estas bandas paramilitares.

Entre el jueves 13 de Abril a las 9 de la noche, y las 3 de la madrugada del 14 de abril, bombardeamos con artillería guerrillera los campamento de paramilitares del alto Truandó. 200 kilos de bombas de TNT, produjeron 47 bajas, entre muertos, heridos y desaparecidos. Los muertos y lisiados fueron evacuados por los militares hacia la zona urbana del municipio de Riosucio.

La contundencia del ataque obligó a los paramilitares supervivientes a abandonar el cañón de río Truandó y a refugiarse en los barrios de Riosucio, y en su reemplazo la Brigada del ejército gubernamental ha colocado patrullas de soldados profesionales.

Para reponer bajas, los oficiales del ejército estatal ofrecen hasta 10 millones de pesos a pobladores del norte del Chocó y a ex guerrilleros de las Farc, para que se enganchen en las diezmadas bandas paramilitares.

Una vez más queda a la vista pública, la asociación criminal entre militares y paramilitares, en un sólo plan dirigido contra las comunidades del Chocó. Debía darle vergüenza al gobierno de Santos, seguir sosteniendo que "los tales paramilitares no existen" o que "se acaban de enterar, que existen paramilitares de extrema derecha en Colombia".

¡Colombia para los trabajadores!  
¡Ni un paso atrás, liberación o muerte!

Dirección Frente de Guerra Occidental Omar Gómez

Montañas, ríos y costas del occidente colombiano  
Abril de 2017.

###### Reciba toda la información de Contagio Radio en [[su correo]
