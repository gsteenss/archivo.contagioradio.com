Title: “Los animalistas somos pacifistas”
Date: 2017-01-23 17:59
Category: Animales, Voces de la Tierra
Tags: Antitaurina, Movimiento animalista, Plataforma Alto, Toros
Slug: los-animalistas-somos-pacifistas-35020
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/protesta_regreso_toros-16-1200x800.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Juan Pablo Pino] 

###### [23 Ene. 2017] 

**Según cifras oficiales, más de 30.000 personas se dieron cita en los alrededores de la Plaza "La Santamaria" de Bogotá, para rechazar la temporada taurina que volvió a la capital luego de 5 años de ausencia** y de ires y venires con demandas, tutelas que por poco dan la victoria a los animalistas que recalcan que la llamada "Fiesta brava" es un acto de crueldad que no debería verse en un país que busca la construcción de la paz.

Sin embargo, luego de terminada la movilización pacífica de los defensores de la vida de los toros, hacia las 5:30 de la tarde, se presentaron algunos hechos  violentos y "aislados" que terminaron con las heridas a más de 11 personas y el anuncio de judicialización de dos de ellas. Para Natalia Parra, directora de la Plataforma ALTO, las reacciones a esos hechos dejan entre ver que la movilización fue infiltrada para desacreditarla.

Y es que los calificativos contra el movimiento animalista no se han hecho esperar, por lo que otras organizaciones también** expresaron su rechazo por dichos actos**, pero también aseguran que detrás de todo lo sucedido puede haber intereses que quieren hacer perder legitimidad a los actos de protesta. "era conveniente para los taurinos que los hechos sucedidos se dieran de la manera como acontecieron" afirma Parra.

“Somos conscientes que a quienes más le conviene que este tipo de manifestaciones pierdan legitimidad, que los animalistas seamos entendidos como violentos y por el contrario quienes se reúnen en las plazas a torturar animales sean entendidos como las víctimas del asunto es a los mismos taurinos” manifestó Natalia Parra. (Le puede interesar: [Crepes & Waffles, El Corral y Cine Colombia promocionan las corridas de toros](https://archivo.contagioradio.com/crepes-waffles-el-corral-y-cine-colombia-promocionan-las-corridas-de-toros/))

**Según Natalia, la movilización estuvo infiltrada por personas que no conocen y que fueron las que ocasionaron los choques entre las personas asistentes,** “animalista que se respete es un pacifista, por ende el hecho de que propendamos por la vida de los animales, quiere decir que propendemos también por la de los humanos (…) un animalista jamás va a herir a un humano, porque un humano es también un animal”.

Y dijo que “**no se puede asegurar que eran animalistas quienes ocasionaron los disturbios o más bien eran personas infiltradas, que querían quitarle legitimidad al ejercicio de manifestación legitima”.**

Ante las aseveraciones en las que se dijo que la movilización tuvo tintes políticos, Natalia dice “claro que llegaron políticos, pues era una movilización abierta a todos aquellos quienes han comenzado a preocuparse por los animales y en esa medida llegaron políticos de todas las orillas que han apoyado la causa animalista”.

De igual manera, **Catalina Parra vocera de la Coordinadora Antifascista de Bogotá añadió que las personas que están detenidas serán judicializadas por violencia contra servidor público** “lo que quiere la autoridad es crear miedo y terror para no volver a realizar este tipo de manifestaciones” y comentó que quienes están heridas fueron personas que estaban dentro de la marcha “en su mayoría son jóvenes, mayores de edad, con heridas en la cabeza, en el cuerpo y por lo que tuvieron que cogerles puntos” agregó.

De acuerdo con la versión de Catalina, hubo personas infiltrados en la movilización animalista que se enfrentaron a los taurinos, por lo que la fuerza pública intervino de manera desmedida contra las personas.

Concluye diciendo que es importante destacar que en la movilización estuvieron cerca de 5 mil personas presentes entre los que habían niños, niñas, jóvenes, mujeres "**las personas cantábamos, llevaron sus perros, sus hijos y si las personas hacen eso es porque sencillamente no quieren chocar contra ninguna otra persona”.**

Pese a los hechos acontecidos, los animalistas continuaran movilizándose y apoyando los proyectos de ley encaminados a proteger la vida de los animales. (Le puede interesar: [Desde el Congreso buscan alejar a los niños de la violencia de la tauromaquia](https://archivo.contagioradio.com/desde-el-congreso-buscan-alejar-a-los-ninos-de-la-violencia-de-la-tauromaquia/))

En la actualidad existen dos ponencias que serán debatidas este miércoles por la Corte Constitucional. La primera a nombre del magistrado Alejandro Linares quien manifiesta que las corridas deben mantenerse, y de ese modo espetar lo dicho en 2010 por la misma Corte.

La segunda, que es la que apoyan los antitaurinos es del Magistrado Alberto Rojas, en ella asegura que las corridas de toros deben ser desmontadas paulatinamente, dado que no representan a la mayoría de los ciudadanos.

Estas ponencias son parte de una demanda contra el maltrato de los animales, que pretenden penalizar "el rejoneo, coleo, las corridas de toros, novilladas, corralejas, entre otros actos" que causen sufrimiento a los animales.

Será entonces la Corte Constitucional la que desarrolle ese debate final, en el que revisarán la ley que contempla las penas contra el maltrato animal para definir si las corridas de toros en Colombia deben o no ser sancionadas como una forma de maltrato.

<iframe id="audio_16602023" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16602023_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_16602011" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16602011_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
