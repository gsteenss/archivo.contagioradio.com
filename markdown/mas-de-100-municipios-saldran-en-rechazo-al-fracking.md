Title: Más de 100 municipios saldrán en rechazo al fracking
Date: 2019-06-04 18:24
Author: CtgAdm
Category: Ambiente, Movilización
Tags: Alianza Colombia Libre del Fracking, Consejo de Estado, fracking
Slug: mas-de-100-municipios-saldran-en-rechazo-al-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/no-fracking.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comité Ambiental en Defensa de la Vida] 

**Ciudadanos de [alrededor de 100 municipios](https://docs.google.com/spreadsheets/d/1ahfyfMBoJ-6KaFE3Wkbwr7iEL1quDzD_L1F-gboLiHk/edit#gid=0) del país saldrán a las calles este viernes para manifestar su inconformidad con el avance de proyectos piloto de fracking** y la minería contaminante. Además, respaldará un proceso legal que reanudará el mismo día en el Consejo de Estado para anular dos normas que le dan vía al uso de esta técnica de extracción no convencional.

"Necesitamos hacernos sentir como ciudadanos de que no queremos que se generen estos procesos de experimentos en los territorios además de respaldar el Consejo de Estado con el fin de que siga en firme con contener la normatividad que permite la implementación del fracking en Colombia", afirmó Estefany Grajales, integrante de Alianza Colombia Libre del Fracking. (Le puede interesar: "[Expertos en fracking se quedan cortos con sus recomendaciones"](https://archivo.contagioradio.com/expertos-fracking-se-quedan-cortos-recomendaciones/))

A pesar de que el presidente de Ecopetrol Felipe Bayón expresó la semana pasada que la empresa estaría preparada para iniciar la fase de exploración de proyectos piloto que utilicen esta técnica en Magdalena Medio, grupos ambientales han reiterado que una moratoria interpuesta por el Alto Tribunal suspende cualquier actividad de fracking provisionalmente. (Le puede interesar:"'[En este momento en Colombia, no es legal hacer fracking](https://archivo.contagioradio.com/en-este-momento-en-colombia-no-es-legal-hacer-fracking/)'")

A esta marcha, se unirán las capitales como Cali, Ibagué, Bucaramanga, Barranquilla, Medellín, Leticia y Bogotá e incluso ciudades internacionales como París, Roma y Buenos Aires. Grajales sostuvo que el tema del fracking "nos compete a todos" porque la explotación de los territorios se realiza para el abastecimiento de energía en todo el país y en particular las gran ciudades. (Le puede interesar: "[Comunidades rechazan el avance de proyectos piloto de fracking](https://archivo.contagioradio.com/comunidades-rechazan-avance-proyectos-piloto-fracking/)")

<iframe id="audio_36727907" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36727907_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
