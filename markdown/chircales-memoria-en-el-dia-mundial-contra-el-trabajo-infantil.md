Title: "Chircales", memoria en el Día Mundial Contra el trabajo Infantil
Date: 2015-06-12 13:53
Author: AdminContagio
Category: 24 Cuadros, DDHH
Slug: chircales-memoria-en-el-dia-mundial-contra-el-trabajo-infantil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/57150_585_imagen__.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Fotograma de "Chircales" 

##### [12 jun 2015] 

Hace poco más de 40 años el mundo conoció a través de "**Chircales**", documental producido por los documentalistas **Marta Rodríguez y Jorge Silva,** la cruda realidad del trabajo infantil que por los años 60 y 70 realizaban niños del barrio Tunjuelito, ubicado al sur de la ciudad de Bogotá, en las rudimentarias fabricas de ladrillo y teja de barro.

Hoy, a propósito del **Día Mundial contra el trabajo infantil**,  más alla de ser un referente necesario dentro de la historia del cine "de lo real" en Colombia, el trabajo audiovisual de la pareja de documentalistas sirve para retomar, una vez más, la **reflexión sobre el panorama de explotación laboral infantil** que existe en el nuestro país y en el mundo.

Resultaría fácil categorizar el documental, producido entre 1966 y 1972, como otra de las historias utilizadas en el cine nacional para explotar la miseria, la imágen de un país donde prevalece la desigualdad y la lucha de clases, un rótulo que desconocería el trabajo de investigación, producción y la importancia que representa como registro de un momento social, económico y político para la história colombiana.

<iframe src="https://www.youtube.com/embed/YwDR-eU8XuM" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

##### ["Chircales"  Dir: Marta Rodríguez- Jorge Silva (1966-1972)] 

De acuerdo a los últimos datos, dilucidados por la Organización Internacional del Trabajo (OIT), en el 2012, **168 millones de niños trabajan en el mundo, de los cuales 120 millones son niños entre 5 y 14 años**. Según el estudio, en América Latina y el Caribe los niños que abandonan la escuela por ir a trabajar son el 8,8%, que equivale a 13 millones de niños, cifra que está muy por debajo de la situación dificultosa que en el África subsahariana se vive.

En Colombia, el DANE registró una tasa de trabajo infantil de 9,3 % en el trimestre octubre - diciembre de 2014, siendo el comercio, hoteles y restaurantes, la agricultura, ganadería, caza, silvicultura y pesca algunas de las actividades con mayor participación de menores entre los 5 y los 17 años.

**¡NO al trabajo infantil - SÍ a una educación de calidad!** Es el lema que en 2015 la Organización de las Naciones Unidas está promulgando para conmemorar el Día mundial contra el trabajo infantil alrededor del mundo.

 
