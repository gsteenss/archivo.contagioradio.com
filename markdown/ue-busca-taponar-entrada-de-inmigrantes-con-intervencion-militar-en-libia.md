Title: UE busca taponar entrada de inmigrantes con intervención militar en Libia
Date: 2015-05-12 17:00
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: UE busca apoyos en la ONU para intervención militar en Libia, UE lucha contra mafias de inmigrantes ilegales en Libia, UE quiere intervención militar contra mafias Libias, UE quiere intervención militar en Libia
Slug: ue-busca-taponar-entrada-de-inmigrantes-con-intervencion-militar-en-libia
Status: published

###### Foto:Elconfidencial.com 

La **UE quiere lanzar una operación militar** para frenar y detener a las **mafias** que están operando en las **costas de Libia** y así **frenar la emergencia humanitaria** de **inmigrantes** que intentan acceder al sur de Europa .

Para ello la UE **necesita el apoyo de la ONU**, de hecho la Alta Comisaria **Federica Mogherini** ha viajado a Nueva York para **exponer las razones que motivan la intervención** y poder sumar apoyos de toda la comunidad internacional.

Para los europeos es imprescindible el **apoyo de Rusos y Chinos** que actualmente **recelan de dicha operación**, además los Rusos no quieren que se pueda intervenir en el mar, como había propuesto la comisaria europea.

Mogherini  ha dicho ante el consejo de Seguridad de Naciones Unidas “Asumimos responsabilidades y trabajamos duro y rápido, pero no podemos hacerlo solos” y **“Contamos con su ayuda para salvar vidas y desmantelar las organizaciones criminales que explotan la desesperación de la gente”**, con el objetivo de poder convencer a las naciones reticentes.

La operación militar consistiría en la identificación de los mafiosos, de los barcos donde operan y de su inutilización, tampoco se descarta la opción coercitiva, en tal caso las fuerzas operarían en las costas Libias y en el mar mediterráneo.

Jens Stontelberg, secretario de la **OTAN** ha dicho que **“los esfuerzos europeos para dar una respuesta más amplia en Libia”**, pero la UE no está interesada en una intervención apoyada por la OTAN.

Libia hasta 2011 era el país de África con el PIB más alto y los índices de pobreza y hambre más bajos del continente, además era de los pocos países Africanos que garantizaba la seguridad social.

**En 2011**, en el contexto de la “Primavera Árabe”, cientos de ciudadanos protestaron contra el régimen Libio exigiendo más igualdad y la formación de un gobierno democrático. Fue entonces cuando **comenzó la guerra, entre facciones rebeldes armadas por Occidente y el régimen de Gadafi.**

Países como Francia, EEUU, Reino Unido apoyados por Naciones Unidas **intervinieron militarmente acabando con la aviación Libia** y apoyando a los grupos armados rebeldes.

En 2014 terminó la guerra con la muerte de Gadafi la caída del gobierno, entonces se formó el **CNT (Consejo Nacional Transitorio),** con la idea de formar un gobierno que hasta hoy en día a penas controla el país.

Actualmente **Libia está gobernada por mafias, grupos armados insurgentes, algunos de ellos de carácter islámico y la situación de ingobernabilidad es la orden del día**. En este contexto es donde se han **instalado las mafias** que traen a los inmigrantes ilegales desde el África subsahariana hasta el sur de Europa.

La UE quiere luchar contra las mafias pero para las **organizaciones de DDHH, dudan de que quiera resolver el problema de la inmigración** y opinan que detrás de esta operación existe la idea de **taponar totalmente la entrada de los inmigrantes a través del control militar marítimo.**

Mientras tanto la **emergencia humanitaria continúa** y las muertes se reproducen en el mar, en un contexto en el que las distintas organizaciones de defensa de los inmigrantes apuntan a que la gran mayoría de ellos son **potenciales refugiados políticos**, debido a las guerras de donde proceden.
