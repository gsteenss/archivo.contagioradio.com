Title: Sin luz ni internet no puede haber educación virtual de calidad
Date: 2020-06-10 17:05
Author: CtgAdm
Category: Actualidad, Otra Mirada, Programas
Slug: a-colombia-llego-la-educacion-virtual-mientras-el-20-de-los-estudiantes-no-tenia-ni-luz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Video-2020-06-05-at-10.24.13-AM.mp4" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde el inicio de la pandemia en Colombia son diferentes los sectores que han tenido que cambiar sus hábitos y formas de hacer. Uno de ellos es la educación, gremio que ha puesto en evidencia que el **país no esta preparado para la educación virtual.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desatando múltiples debates al rededor de como Colombia debe ejecutar los protocolos de bioseguridad para el retorno a las aulas en el segundo semestre del año **sin desconocer como han sido evidente la realidad de estudiantes y profesores en[zonas rurales](https://archivo.contagioradio.com/cuarentena-con-dignidad-movilizacion-social-en-arauca-en-medio-del-covid-19/) , condiciones precarias y contextos de violencia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Organizaciones como **Unicef y [Save the Children](https://www.savethechildren.es/actualidad/informe-covid-19-cerrar-la-brecha)han señalado que uno de los mayores riesgos de suspender la educación es que los niños no quieren volver**, incrementando así la desescolarización y la violencia intrafamiliar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A nivel internacional **186 países han tenido que cerrar sus colegios producto de la pandemia**, lo que significa que cerca de 1.800 millones de niños y niñas quedaron por fuera del sistema educativo tradicional y han migrado a la educación virtual.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la **Coalición Colombiana por el Derecho a la Educación, en el país se cerraron 53.202 colegios entre públicos y privados, lo que equivale a que 9.916.000 estudiantes** deberían estar siendo educados desde la virtualidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Contrario a esto y las campañas estatales para resaltar la evolución de las Tic´s en el país, **cerca del 62.6% de las sedes urbanas están sin internet y 20% de los estudiantes y profesores no tienen electricidad,** esto especialmente en zonas rurales, según lo indica la Coalición.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Cecilia Gómez vocera de Coalición, los resultados de la disminución en la calidad educativa en el país no serán motivo de alarma, *"el año 2018 se registró que 63% de los estudiantes o computador de grado 11 no tenían, eso quiere decir que **el Gobierno y las diferentes entidades eran conscientes de la disponibilidad tecnológica y aún así mandaron educación virtual**".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ello agregó, que las medidas adoptadas han dejaron en desventaja escolar a **cerca de 6 millones niños y niñas, al no poder recibir clases virtuales, *"la realidad es que en el país hay un atraso gigantesco en términos de avances de conectividad*** *especialmente en zonas rurales",* destacó Gómez.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Siendo eso poco antes las decisiones tomadas por el Gobierno para la educación, **la Directriz 05 de marzo del 2020 presenta el recorte al presupuesto para la educación de personas jóvenes y adultas que estudian en colegios nocturnos y de fines de semana**, *"muchos de estos son los papás y las mamás de los niños que se encuentran en condiciones precarias en el país".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Cómo retornar a las aulas luego de la pandemia?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hace alguno días el Ministerio de Educación profirió el Decreto 011 el cual indica la reanudación de clases presenciales desde el mes de agosto, para Gómez esta decisión es peligrosa ***"porque no existen las mínimas condiciones para que los estudiantes puedan ingresar empezando con la infraestructura de muchos[colegios](https://archivo.contagioradio.com/coronavirus-va-en-cohete-y-este-gobierno-va-en-mula/)".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ello la educadora indicó que *"el Gobierno tendría que pensar en hacer una gran inversión para estructurar elementos básicos como los baños de los colegios, y por lo que sabemos no pasará"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asímismo resaltó que desde la Coalición señalan que *"**volver a los colegios no es una alternativa** a no ser que quieran que el país entre a un nivel de contagio muy alto, siendo los colegios los principales focos del virus".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Así se vive en zonas rurales la "educación virtual"

<!-- /wp:heading -->

<!-- wp:video {"autoplay":false,"id":85266,"loop":false,"muted":false,"playsInline":false,"src":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Video-2020-06-05-at-10.24.13-AM.mp4"} -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Video-2020-06-05-at-10.24.13-AM.mp4">
</video>
  

<figcaption>
**Docente Jose Paya del resguardo Alpes Orientales Municipio de Puerto Caicedo**, cuentas las dificultades educativas en medio del COVID19, la falta de acceso a redes, así como las dificultades para hacer el seguimiento a estudiantes debido a las grandes distancias y condiciones existentes.

</figcaption>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

Múltiples vídeos de niños, niñas y docentes se hicieron virales haciendo evidentes las hazañas que tienen que realizar para poder realizar las clases. Hernando Muñoz rector de la Institución educativa Antonio Nariño del municipio La Tebaida, un lugar gravemente afectado desde 1999 por el terremoto que destruyó gran parte de Armenia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hizo referencia **la gran precariedad con la que trabajan muchos de sus colegas, que así como él junto a su equipo de profesores han tenido que buscar formas para brindar educación a sus estudiantes** en medio de reducidos presupuestos, escasos dispositivos de tecnológicos, contextos de violencia y grandes distancias entre sus alumnos

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esta escuela cerca del 90% de las tareas y la comunicación con sus estudiantes se logra por medio de WhatsApp o llamadas telefónicas,*[**"los estudiantes no tienen conectividad, ni mucho menos computadores, así que enviamos material a quienes logran una recarga o un celular prestado, a los otros les entregamos recursos en la puerta de la**]{}**institución".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar de los esfuerzos el rector destaca que *"hay estudiantes que están demasiado alejados y con ellos ha sido imposible la conexión"*, a eso se suma los riesgos económicos que obligan a que los niños trabajen y a las redes de microtráfico que giran en torno a la comunidad de La Tebaida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Riesgos que son mas latentes en territorios donde el microtrafico se transforma en redes gigantes de tráfico de drogas, presencia de grupos armados y desplazamiento de docentes a causa de las [amenazas](https://www.justiciaypazcolombia.com/se-teme-por-lideres-de-comunidad-indigena-jiw/) que ponen en riesgo sus vidas. (Le puede interesar: [Asesinan al rector Jairo de Jesús Jiménez, defensor de comunidades rurales de Abejorral](https://archivo.contagioradio.com/asesinan-al-profesor-jairo-de-jesus-jimenez-defensor-de-comunidades-rurales-de-abejorral/)).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último el rector señaló que *"el Ministerio de educación ha desconocido gran parte de las realidades de los procesos, denuncia y exigencias de los profesores en las zonas rurales, y **a avanzado en las medidas, ignorando el la realidad de las clases virtuales, lo poco aprendido, además de el temor y la resistencia de las comunidades al retorno de los estudiantes a las aulas**"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente la **Coalición Colombiana por el Derecho a la Educación** destacó que para antes de retornar en una foto total a las aulas, y teniendo el miedo general de la población, se deben optar por alternativas y estrategias de pedagógicas novedosas efectivas, que permitan llegar a las comunidades rurales y más alejadas, y que tener acceso a internet y un computar no se conviertan la única alternativa de educar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregaron que se debe provechar el acompañamiento de las familias *"nunca antes la familia había estado tan ligada a los procesos educativos de los hijos es un momento que se debe explotar para trabajar de la mano de ellos y ellas en procesos pedagógicos que nutran y enriquezcan los procesos pedagógicos de los niños y niñas, sin representar mayores cargar a los educadores"*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F287074339009986%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" style="border:none;overflow:hidden" scrolling="no" allowtransparency="true" allow="encrypted-media" allowfullscreen="true" width="734" height="413" frameborder="0"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
