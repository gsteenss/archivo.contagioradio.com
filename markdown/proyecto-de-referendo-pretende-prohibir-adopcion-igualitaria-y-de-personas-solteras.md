Title: Buscan prohíbir adopción a parejas homosexuales y personas solteras
Date: 2016-07-21 14:27
Category: LGBTI, Nacional
Tags: Adopción igualitaria
Slug: proyecto-de-referendo-pretende-prohibir-adopcion-igualitaria-y-de-personas-solteras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/gays.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: t13] 

###### [21 de Jul] 

La senadora Viviane Morales presentó un proyectó de Ley  de referendo con el que pretende que **se prohíba la adopción de niños por parte de parejas homosexuales o personas solteras,** hecho que es permitido desde el año 2015 cuando la Corte Constitucional dio vía libre a la adopción igualitaria.

De acuerdo con la senadora,[este proyecto se fundamenta en "la garantía que el Estado debe darle a los niños y niñas de restablecer su derecho a tener una mamá y un papá,](https://archivo.contagioradio.com/referendo-contra-adopcion-igualitaria-no-solo-perjudicaria-parejas-homosexuales/) el niño no perdió dos mamas o dos papas". De igual forma expone que en Colombia hay muchas parejas heterosexuales que quieren adoptar, pero que se quedan durante el proceso debido a los trámites burocráticos que hay para llevar a cabo para adoptar.

Con el referendo también se pretende que las **personas solteras no puedan adoptar debido** a que según Morales **el Estado debe brindar lo más optimo, por lo tanto, lo mejor es una pareja conformada por hombre y mujer**.

"Sabemos que hay suficientes familias heterosexuales buscando adoptar, el niño puede tener una familia, una como la que nunca tuvo, conformada por un hombre y una mujer, **el juego que históricamente y naturalmente le corresponde en una sociedad**" afirma la senadora.

Una de las voces que se declaró en firme oposición a este proyecto de Ley, es la representante por el Partido Verde, Angélica Lozano,  **quien aseguró que la propuesta es inviable y no tiene ningún fundamento legal ni constitucional**, por el contrario podría ser una estrategia muy rentable de campaña política que refuerza prejuicios y valores en contra de otros ciudadanos que hacen parte de un sector de la sociedad.

"Este no es un tema en contra de homosexuales, es un tema en contra de toda la ciudadanía, entonces con calma vamos a controvertir y [a defender la igualdad, la dignidad humana, a defender nuestras familias y a defender el derecho a postularse ante el Estado"](https://archivo.contagioradio.com/si-estan-en-contra-de-la-adopcion-igualitaria-revisen-su-etica-y-su-moral/).

<iframe src="http://co.ivoox.com/es/player_ej_12298361_2_1.html?data=kpefm52XepKhhpywj5adaZS1k5aah5yncZOhhpywj5WRaZi3jpWah5yncbfd187O0MqPkdDmwtHS1YqWh4zEwtfhy8nTb63dw8rfw9GRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe src="http://co.ivoox.com/es/player_ej_12298350_2_1.html?data=kpefm52XeZGhhpywj5aZaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncaLiyIqwlYqlfc3dxMaYrtTepc_jhpewjabQrcLi28aYuMrWqMahhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
