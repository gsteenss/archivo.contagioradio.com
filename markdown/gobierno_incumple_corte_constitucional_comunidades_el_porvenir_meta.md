Title: Gobierno incumple fallo de la Corte que devuelve tierras a comunidades de El Porvenir
Date: 2017-11-09 15:42
Category: DDHH, Nacional
Tags: agencia Nacional de Tierras, El Porvenir, Paramilitarismo, Puerto Gaitán
Slug: gobierno_incumple_corte_constitucional_comunidades_el_porvenir_meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/porvenir.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ivancepedacastro] 

###### [9 Nov 2017] 

Ha pasado un año de la sentencia de la Corte Constitucional con la que se le exigía a la **Agencia Nacional de Tierra restituir 27 mil hectáreas de terrenos baldíos, a 72 familias de la vereda El Porvenir, en Puerto Gaitán**, Meta. Sin embargo, dicha sentencia sigue sin ser cumplida y a la fecha, no hay una sola hectárea devuelta las víctimas, como lo denuncia la Corporación Claretiana Noman Pérez Bello.

"Hasta el momento lo que ha habido es una ineficiencia que no sabemos si es apropósito, por parte de las entidades del Estado y la Agencia Nacional de Tierra, para otorgar los derechos a la tierra a los campesinos del Porvenir. En cambio se sigue presentando invasión de las mismas tierras y ventas de ellas, pero además se sigue generando amenazas a los campesinos e indígenas", señala el acompañante de las víctimas e integrante de la Corporación, Jaime León.

### **Los incumplimientos del gobierno** 

Desde el inicio la restitución de derechos de los campesinos ha sido a todas luces demorada**. La sentencia SU426 emitida por la Corte** estaba lista desde agosto del 2016 pero solo hasta el 9 de noviembre del mismo año la Corte emitió la sentencia y ordenó que se le reconozca el derecho a la propiedad a las familias habitantes de El Porvenir. (Le puede interesar: [Corte Constitucional ordena adjudicar tierras a comunidades de El Por venir)](https://archivo.contagioradio.com/corte-constitucional-ordena-adjudicacion-tierras-comunidades-puerto-gaitan/)

De acuerdo con Jaime León, se trata de familias que han vivido allí más de 40 años, y por lo cual el gobierno debía crear una mesa técnica para identificar quienes son las víctimas y quienes tiene propiedad sobre la tierra. Es así como **el alto tribunal le da un año a las entidades estatales para restituir los derechos sobre la tierra y territorio** a las comunidades, pero en lo único que se ha avanzado es en llevar a cabo unos censos para identificar a las víctimas, pese a que las organizaciones de víctimas que acompañan a las familias han enviado derechos de petición para exigir el cumplimiento de la sentencia.

### **Continúan las amenazas** 

Pero además, en medio de la lentitud de las acciones por parte del Estado para cumplirle a las víctimas, se ha manifestado que **persiste el accionar de grupos paramilitares, militares y civiles** que han favorecido la ocupación ilegal por parte de presuntos herederos de la **familia Carranza.**

Además de que el derecho a la tierra sigue lejos de ser realidad, frente a las amenazas no hay ninguna investigación pese a que existen los elementos para indagar de dónde provienen. Para León, esto significa que continúa "reinando la impunidad y que los riesgos para la comunidad siguen siendo altos", ya que esas amenazas han llegado firmadas por grupos como AUC, integrantes de La Empresa y otras estructuras paramilitares.

Según el integrante de la Corporación indígenas pertenecientes a la comunidad Kubeo-Sikuani, en la inspección del porvenir, del municipio de Puerto Gaitán en Meta, estarían **en riesgo de ser masacrados y desplazados, por parte de una estructura armada que estaría operando en el territorio. (**[Le puede interesar: Paramilitares amenazan a familias de Puerto Gaitán)](https://archivo.contagioradio.com/paramilitares-amenazan-familias-porvenir-puerto-gaitan-meta/)

Ante dichas situaciones comunidades víctimas de la región de la Orinoquía, han decidido realizarán una gira la próxima semana en el país denunciando la situación en la que se encuentran. Con foros, y diferentes actividades, van a evidenciar el incumplimiento y despojo del que han sido víctimas.

###### Reciba toda la información de Contagio Radio en [[su correo]

###### 
