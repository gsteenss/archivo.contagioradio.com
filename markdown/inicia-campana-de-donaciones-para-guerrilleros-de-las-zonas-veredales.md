Title: Inicia campaña de donaciones para guerrilleros de las Zonas Veredales
Date: 2017-02-09 17:53
Category: DDHH, Nacional
Tags: ANZORC, FARC-EP, Implementación de los Acuerdos de paz, Zonas Veredales Transitorias de Normalización
Slug: inicia-campana-de-donaciones-para-guerrilleros-de-las-zonas-veredales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Captura-de-pantalla-2017-02-09-a-las-15.39.46.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ANZORC] 

###### [9 Feb 2017] 

80 mujeres en embarazo, 66 bebés que ya se encuentran en las Zonas Veredales y demás guerrilleros **“que no cuentan con atención médica, medicamentos, agua ni buena alimentación” **motivaron a la Asociación Nacional de Zonas de Reserva Campesina ANZORC a iniciar la campaña "Dignifica la Paz”, que tiene como objetivo exigir el cumplimiento de los Acuerdos por parte del Estado y llamar a la solidaridad de los colombianos y colombianas.

La invitación concreta es a **la recolección de alimentos no perecederos, implementos médicos, pañales, ropa y otros utensilios que serán enviados** a las Zonas Veredales con mayor estado de precariedad. "Mientras el Gobierno incumple ANZORC propone", es el lema de esta campaña que puso su mirada en la preocupante situación que viven actualmente. ([Le puede interesar: Continúan incumplimientos del gobierno en Zonas Veredales](https://archivo.contagioradio.com/continuan-incumplimientos-del-gobierno-en-zonas-veredales/)).

### ¿Por qué donar? 

Hasta la fecha, 6.300 guerrilleros ya están en las Zonas Veredales, **y en todos los puntos se han denunciado las condiciones indignas a las que han sido sometidos.** La Oficina del Alto Comisionado para la Paz, ha dicho que se intensificaran las visitas a las Zonas y “se mejorará la metodología” del Mecanismo de Monitoreo y Verificación.

La iniciativa no pretende restar responsabilidades al Gobierno, la intención es fortalecer la fase de implementación y agrupamiento de los guerrilleros **para evitar situaciones que pueda lamentar el pueblo colombiano a futuro, como la desmotivación de los integrantes** de las  FARC y facilitar el desarrollo de actividades pactadas de formación y salud.

Frente a las reiteradas denuncias hechas por organizaciones defensoras de derechos humanos, medios alternativos y las comandancias de las FARC, Carlos Córdoba, gerente de las Zonas Veredales Transitorias de Normalización, “excusa la incapacidad logística del Gobierno” con **inconvenientes por el terreno, problemas de contratación y demoras de los trámites en la capital.**

Muchas organizaciones veedoras y garantes de los acuerdos, manifiestan que a muchos sectores les inquieta que si el Estado colombiano es incapaz de cumplir una parte tan importante, necesaria y básica de los Acuerdos, **“entonces ¿qué pasará con otros puntos de la implementación de mayor complejidad?”.**

### ¿Cómo hacer sus donaciones? 

Las personas interesadas en aportar al mejoramiento de las condiciones de estadía en las ZVTN pueden hacer llegar sus donaciones al **Café de la Reserva ubicado en la Carrera 3 \#12-58, barrio La Candelaria, en Bogotá.**

### Los implementos que se requieren son: 

-   Pañales de tela o desechables
-   Crema antipañalitis
-   Tinas de baño para bebés
-   Algodón
-   Alcohol
-   Isodine espuma y loción
-   Ropa para bebés nueva
-   Ropa para adultos nueva
-   Baberos
-   Teteros
-   Chupones
-   Shampoo para niños y adultos
-   Toallas
-   Toallitas húmedas desechables para bebé
-   Cobijas delgadas y gruesas
-   Mosquiteros
-   Repelente para adultos y bebés
-   Loratadina
-   Acetaminofen para adultos y bebés
-   Ibuprofeno 800 mg y 400 mg
-   Naproxeno
-   Curas
-   Suero oral (30, 45 y 60 mEq)
-   Termómetros
-   Menticol
-   Cargadores de tela
-   Crema para pezones
-   Extractor de leche
-   Pañaleras
-   Leche en polvo
-   Gasas
-   Alimentos no perecederos: arveja, garbanzo, frijol, arroz, pastas y otros.
-   Botas de caucho

###### Reciba toda la información de Contagio Radio en [[su correo]
