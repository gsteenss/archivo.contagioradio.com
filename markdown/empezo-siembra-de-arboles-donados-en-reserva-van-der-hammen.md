Title: Empezó siembra de árboles donados en reserva Van der Hammen
Date: 2017-09-25 13:10
Category: Ambiente, Nacional
Tags: Bogotá, quebrada chorrillos, Reserva Thomas Van der Hammen, siembra de árboles
Slug: empezo-siembra-de-arboles-donados-en-reserva-van-der-hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/DKgTsdyWAAAMo71-e1506362996288.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ambiente y Sociedad ] 

###### [25 Sept 2017] 

Luego de que la ciudadanía en Bogotá, le regalara a la ciudad más de 3 mil árboles para reforestar la reserva Thomas Van der Hammen, **los bogotanos comenzaron las jornadas de siembra**. Ya fueron sembrados 300 árboles y se realizó el mantenimiento y cuidado de los que ya hacen parte de la reserva.

De acuerdo con Sabina Rodríguez, la jornada convocó a la ciudadanía para empezar la siembra de 300 especies donadas y **realizaron actividades de mantenimiento**, como el cercado de la zona para evitar que el ganado del sector se coma las plantas, y así continuar con la protección de las especies que ya hacen parte de la reserva.

Los árboles que fueron donados y que aún no se han sembrado, continúan guardados y protegidos  en Teusaquillo **mientras se realizan las especificaciones técnicas del territorio** con la Corporación Autónoma Regional de Cundinamarca-CAR. También hay algunos árboles que se encuentran guardados en diferentes viveros a espera de poder ser sembrados. (Le puede interesar: [Alcaldía de Bogotá no recibió 2339 árboles para reserva Van der Hammen"](https://archivo.contagioradio.com/alcaldia-de-bogota-no-recibio-donacion-de-arboles-para-reforestar-reserva-van-der-hammen/))

Rodríguez manifestó que, las organizaciones sociales que se encargan de realizar la siembra, **están a la espera de los resultados del informe técnico** sobre las zonas donde deben ser sembradas especies como el Cedro, el Mano de Oso o el Sauce Llorón.

### **Quebrada Chorrillos, junto a reserva Van der Hammen, está en peligro** 

Dentro de la zona rural de Suba, cercano a la reserva Thomas Van der Hammen se encuentra el humedal Chorrillos. Según la Fundación Humedales Bogotá, este ecosistema **aún no se ha declarado como área protegida** dentro del Plan de Ordenamiento Territorial-POT por lo que la CAR no tiene sustento jurídico para protegerlo.

La Fundación, en su portal web, indica que la zona del humedal ha sido **un terreno en donde hay intereses privados** para la construcción de casas, negocios y “demás equipamientos que permitan traer “desarrollo” de concreto a esta apetecida zona de Bogotá, sin importar poner en riesgo la sustentabilidad ambiental de toda la ciudad”. (Le puede interesar: ["Con siembra de árboles cuidadanos defienden la Reserva Van der Hammen"](https://archivo.contagioradio.com/con-siembra-de-arboles-ciudadanos-defienden-la-reserva-van-der-hammen/))

Ante esta problemática, Sabina Rodríguez afirmó que la preservación de este ecosistema debe hacerse en común con la conservación de la reserva Van der Hammen. Dijo que **están a la espera de articular trabajos con la población** de esta parte de la ciudad para realizar actividades que respondan a las necesidades de protección de los ecosistemas.

<iframe id="audio_21080935" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21080935_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-ensu-correoo-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-porcontagio-radio dir="ltr"}
