Title: Asesinan al líder sindical José Manrique en Bucaramanga
Date: 2019-02-26 14:48
Author: CtgAdm
Category: Comunidad, Entrevistas, Líderes sociales
Tags: Líderes sindicales asesinados
Slug: asesinan-al-lider-sindical-jose-manrique-en-bucaramanga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Líder.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sutimac 

###### 26 Feb 2019 

El Sindicato Unitario de Trabajadores de la Industria de Materias para la Construcción (SUTIMAC) denunció el asesinato del líder sindical** José Fernel Manrique Valencia** ,el pasado lunes 25 de febrero en la ciudad de Bucaramanga, cuando dos hombres que se desplazaban en moto dispararon en dos ocasiones contra el líder, quien murió frente a su vivienda en el barrio Café Madrid.

Según **Miguel Sierra, compañero del líder fallecido** José Manrique, de 34 años de edad, se desempeñaba  como operario de la multinacional empresa cementera Cemex Colombia en donde trabajaba desde hace diez años, además hacía parte de la **Junta Directiva Seccional.  **

Sierra, quien fue uno de los últimos en hablar con José, indicó que desde hace seis meses se encontraba incapacitado después de haber perdido una extremidad inferior en un accidente de tránsito, y que el líder pronto iba a regresar a sus labores como operario. ** [(Lea también Líder social Jorge Castrillón Gutiérrez es asesinado al sur de Córdoba)](https://archivo.contagioradio.com/lider-jorge-castrillon-gutierrez-es-asesinado-al-sur-de-cordoba/)**

Según Sierra,  aún se desconocen los motivos del asesinato de José, sin embargo la organización sindical **ha expresado su preocupación por la seguridad de otros dirigentes  amenazados de muerte, pues el Gobierno retiró la protección** que les fue asignada por la Unidad Nacional de Protección, alegando una falta de presupuesto. Desde la fundación de SUTIMAC en 1972, han sido asesinados más de 40 dirigentes de la organización sindical, crímenes de los que hasta entonces no se ha resuelto ninguno.

###### Reciba toda la información de Contagio Radio en [[su correo]
