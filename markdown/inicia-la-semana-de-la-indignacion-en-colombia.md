Title: Semana de la indignación en Colombia
Date: 2017-10-09 15:17
Category: Movilización, Nacional
Tags: 12 de Ocutubre, Día de la Dignidad
Slug: inicia-la-semana-de-la-indignacion-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Paro-nacional-bogota-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CableNoticias] 

Entre el 9 y el 13 de octubre se realizará la Semana de la indignación, con actividades que buscan movilizar a la ciudadanía en torno a diferentes causas sociales. Se espera que en ciudades como Medellín, Cali y Bogotá grandes movilizaciones se den el próximo **12 de octubre en rechazo al asesinato de líderes sociales, defensores de derechos humanos y la masacre de Tumaco. **

La semana inicia con el mitin en solidaridad con la Huelga de los pilotos de Avianca, que tendrá lugar el lunes desde las 4 de la tarde en el Aeropuerto El Dorado y demás aeropuertos del país. El mismo día se realizará un plantón en rechazo a la masacre de 6 campesinos en Tumaco- Nariño a partir de las 7 de la noche, en la carrera 7 con calle 26 como punto de encuentro.

En Cali, los ciudadanos se encontraran en la estación La Herminta, en calle 13 con carrera 4, a la**s 6:00pm para expresar su rechazó a la masacre de Tumaco**, al accionar violento por parte de la Fuerza Pública y en contra de la persecución a los líderes sociales y defensores de derechos humanos. (Le puede interesar:["En Colombia hay un déficit de 300 controladores aéreos"](https://archivo.contagioradio.com/en-colombia-hay-un-deficit-de-300-controladores-aereos/))

El martes 10 se realizará una rueda de prensa convocada por el Comando Nacional Unitario y la Coordinacion de Organizaciones Sociales para anunciar las actividades en el marco de la Semana de la Indignacion del 9 al 13 de octubre y la Movilizacion Nacional del 12 de octubre**, a las  8:00 a.m en la Sede de la CUT Naciona**l.

El 12 de octubre se tiene programada la Movilizacion Nacional del conjunto del Movimiento Sindical, Social, Popular y Sectores Politicos Alternativos, **a las 9:00 a.m en la carrera 30 con calle 26, y en todas las Ciudades Capitales del Pais. **(Le puede interesar: ["Por ataque del ESMAD muere comunicadora indígena en Kokonuko, Cauca"](https://archivo.contagioradio.com/por-ataque-del-esmad-muere-comunicadora-indigena-en-kokonuko-cauca/))

###### Reciba toda la información de Contagio Radio en [[su correo]
