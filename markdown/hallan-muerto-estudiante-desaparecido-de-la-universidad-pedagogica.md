Title: Hallan muerto estudiante desaparecido de la Universidad Pedagógica
Date: 2016-04-21 11:24
Category: DDHH, Nacional
Tags: soacha, universidad pedagogica
Slug: hallan-muerto-estudiante-desaparecido-de-la-universidad-pedagogica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/CggY2dTW8AAhHL6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [20 Abr 2016]

El joven Nelson Andrés Junca, graduado **el pasado 4 de abril de ciencias sociales de la Universidad Pedagógica Nacional, y desaparecido desde el 5 de abri**l, fue hallado muerto este miércoles.

Nelson había sido visto por última vez en el barrio Garcés Navas en la localidad de Engativá pero su cuerpo fue encontrado en zona rural de la localidad de Soacha.  Según la información que se tiene hasta el momento, **Junca habría sido asesinado 6 días después de su desaparición forzada.** Organizaciones sociales reclaman que se investigue y de con la verdad sobre la muerte de este joven.

La Asociación de Familiares de Desaparecidos, desde el espacio de la Comisión de Búsqueda, había solicitado la activación del Mecanismo de Búsqueda Urgente, pero según denuncian, "desafortunadamente y como siempre **la Fiscalía no respondió inmediatamente ** y mucho menos eficazmente y  por ello  hoy su familia llora por el asesinato de su ser querido".

El cuerpo fue encontrado en un estado avanzado de descomposición. ASFADES, también asegura que "la Fiscalía no hizo absolutamente nada, lo único que hizo feue investigar al desaparecido llevándose el computador de su casa", además, **"el Mecanismo de búsqueda se activo 1 día después y a él lo matan al sexto día y no hicieron nada".**

Cabe recordar que el pasado 6 de marzo,  el líder comunitario Klaus Zapata, miembro de la Juventud Comunista  y de la Red Juvenil de Soacha,  fue asesinado, víctima de un sujeto quien le propinó dos disparos una vez había finalizado un partido de fútbol en el barrio Ciudad Latina del municipio de Soacha.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
