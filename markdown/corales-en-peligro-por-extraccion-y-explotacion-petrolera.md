Title: Corales en peligro por extracción y explotación petrolera
Date: 2015-06-24 16:38
Category: Ambiente, Otra Mirada
Tags: colombia, corales, Cuenca Sinú Offshore, efectos de la explotación de petróleo, explotación de petróleo, extracción de petróleo, Grupo de Inmunología Evolutiva de la Universidad Nacional, Luis Fernando Cadavid, Universidad Nacional, vida marina
Slug: corales-en-peligro-por-extraccion-y-explotacion-petrolera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Los-ojos-del-subsuelo-buscan-bajo-las-profundidades-marinas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [[alef.mx] 

###### [24 de junio 2015]

La vida marina y específicamente los corales, también son víctimas de la extracción y exploración de petróleo, pero no solo por el derrame del crudo, sino también por la filtración de **lodos utilizados en las máquinas de perforación petrolera que pueden producir muerte celular de los corales.**

A esa conclusión llegó el **Grupo de Inmunología Evolutiva de la Universidad Nacional**, donde se estudió el impacto que tiene las sustancias o lodos aplicados a las brocas de las máquinas que perforan el suelo marino.

Luis Fernando Cadavid, profesor del Departamento de Biología y del Instituto de Genética de la Universidad Nacional, afirma que ese procedimiento trae como consecuencia la disminución de la tasa de crecimiento, aumento el consumo de oxígeno, y con esto, el **incremento en la mortalidad de la vida de los corales.**

Según el estudio, donde se realizó un experimento en una especie de coral expuesta al lodo de las brocas que perforan el suelo del mar, se comprobó el efecto que estas sustancias tienen en el equilibrio de la oxidorreducción en las células del coral, que trae como consecuencia "la inducción de muerte celular y el daño en el RNA y en las membranas celulares”, explica el docente de la Universidad Nacional. Esto demuestra que “**la exposición de corales al 100 % de concentración del lodo les causa la muerte**. En cuanto a la concentración de uno a uno, el experimento determinó una alteración funcional y la muerte después de tres días”, dice la Unal.

Cabe recordar que los **arrecifes coralinos, son ecosistemas marinos tropicales de alta biodiversidad,** resistentes a la erosión y tormentas, estabilizadores de las líneas de costa, especialmente en zonas tropicales bajas. Para que un arrecife se recupere luego de una exposición a sustancias como el petróleo se gastaría varios años y se genera un problema crónico en este tipo de ecosistemas.

En la actualidad, en Colombia se perfora, en fase de exploración, el pozo Kronos 1 A, ubicado en la Costa Atlántica, **Cuenca Sinú Offshore.**
