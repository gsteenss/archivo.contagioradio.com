Title: Foro Internacional busca la participación política de mujeres afro
Date: 2017-04-28 17:29
Category: Comunidad, Nacional
Tags: afro, foro internacional, participación política
Slug: foro-internacional-busca-la-participacion-politica-de-mujeres-afro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/no_mas_al_acismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo particular] 

###### [28 Abr. 2017] 

**Seis mujeres afro han ocupado lugares en** **curules dentro del Congreso de la República en toda la historia,** en vista de esta situación, el Foro Internacional “Participación Política de las Mujeres Afrocolombianas en la Construcción de Paz Territorial”, organizado por la Conferencia Nacional de Organizaciones Afrocolombianas (CNOA), se propone como escenario para la construccion de liderazgo de mujeres afrocolombianas en espacios deliberativos.

De acuerdo con la CNOA, “actualmente **ninguna mujer de esta población es gobernadora y solo hay una curul en las asambleas departamentales** ocupada por una mujer de esta población. En las alcaldías, su representatividad se limita a 11 de las 1.122 alcaldías, es decir, el 0.98%”.

Asimismo, Dora Vivanco, vocera del CNOA, señaló que “si bien las mujeres afrocolombianas son el 10,6% de las mujeres víctimas del conflicto armado, particularmente de desplazamiento forzado, **no se pueden reconocer sólo desde la condición de víctimas, sino también como sujetas políticas**”.

En este sentido, según la CNOA, con el Foro se busca **“visibilizar acciones y estrategias para promover la participación política de las mujeres afrodescendientes en América**, teniendo en cuenta, los aportes de esta población a la construcción de paz territorial”. Vivanco señala que a pesar de que el panorama es desalentador, este espacio es un momento de fortalecimiento y llamamiento a seguir planteando este tipo de escenarios.

Para ello, **se busca consolidar la integración de mujeres afro al interior de la participación electoral en los partidos políticos**, donde “no sólo se piensen en temas de financiación” sino también en un enfoque diferencial de género y étnico, para que esta comunidad siga participando de manera efectiva, con temas de incidencia que se necesitan visibilizar, afirma Vivanco. Le puede interesar: [Acuerdos no se pueden quedar entre Gobierno y FARC. Cumbre afro.](https://archivo.contagioradio.com/las-comunidades-afro-se-preparan-para-implementar-los-acuerdos/)

Referente a la construcción de paz de las mujeres afro, recalcan que **es fundamental reconocer los procesos de resistencia y de resiliencia que las mujeres afro han generado,** donde se propicie el goce efectivo de los derechos humanos, hablando tanto de participación política como de oportunidades laborales, formación con calidad y empoderamiento económico. Le puede interesar: [Las mujeres tejen paz desde su territorio.](https://archivo.contagioradio.com/las-mujeres-tejen-paz-desde-los-territorios/)

**El evento transcurre durante los días 27 y 28 de abril, en la Universidad del Rosario en Bogotá.** Cuenta con la participación de ponentes como Guadalupe Valdez, exdiputada de República Dominicana; Luz Marina Becerra de la Asociación de Afrodescendientes Desplazados – AFRODES y Margarette May, segunda vicepresidenta de la Comisión Interamericana de Derechos Humanos (CIDH).

<iframe id="audio_18402280" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18402280_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
