Title: Festival de poesía de Bogotá rinde homenaje a William Ospina
Date: 2017-05-04 13:32
Category: Cultura, Viaje Literario
Tags: Bogotá, Festival, poesia, William Ospina
Slug: festival-poesia-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Festival-de-poesia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 25 Festival de Poesía de Bogotá 

###### 04 Abr 2017 

Con una serie de eventos realizados como antesala y durante la FILBO 2017, inicia este 4 de mayo el Festival Internacional de Poesía de Bogotá que en su versión 25 rinde homenaje a la obra del escritor nacional William Ospina y tendrá a Noruega como país invitado de honor.

Durante la [programación](http://www.poesiabogota.org/?p=4952) del evento que tendrá lugar en diferentes espacios culturales, académicos y públicos de la ciudad, serán presentados dos libros de poemas inéditos del escritor homenajeado: "Más allá de la aurora y del Ganges" (2006) y "Sanzetti" (2017), mientra que por parte del país invitado de honor harán presencia los poetas Tale Naess y Liv Lundberg.

La presente edición busca resaltar la obra de los Maestros de la poesía iberoamericana contemporánea, entre los que se encuentran el español José Ramón Ripoll quien recientemente ganó el XXIX Premio Internacional de Poesía Fundación Loewe, el argentino Rodolfo Alonso, primer traductor de Fernando Pessoa y sus heteronónimos, el maestro Óscar Oliva de México y el venezolano Rafael Cadenas, uno de los grandes exponentes de la poesía modernista hispanoamericana.

![poesia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/25fip.jpg){.size-full .wp-image-40017 .aligncenter width="590" height="315"}

La instalación del Festival tendrá lugar en la Carpa VIP de Corferias a partir de las 4 de la tarde, con lectura de poesía por parte de Giovanni Quessep, Jotamario Arbeláez, Miguel Méndez Camacho y de William Ospina. Adicionalmente se presentarán una serie de poemas musicalizados de los invitados por parte de Leandro Sabogal (Dela Vitt).

 <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/316325922&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" width="100%" height="450" frameborder="no" scrolling="no"></iframe>
