Title: Reserva Thomas van der Hammen debe mantenerse sin un centimetro de cemento
Date: 2016-02-03 17:32
Category: Ambiente, Nacional
Tags: Enrique Peñalosa, Reserva forestal Tomas Van Der Hammen, Universidad Nacional
Slug: reserva-thomas-van-der-hammen-debe-estar-sin-cemento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Reserva-Tomas-Van-der-Hammen-e1457630338748.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cop 20 

###### [3 Feb 2016]

“Estamos empezando a ver escasez del recurso hídrico y mientras se resuelve el problema del uso del agua, es indispensable que la reserva Thomas Van der Hammen se mantenga viva, sin nada de cemento, **para que los humedales no se vean afectados**”, asegura el profesor Orlando Rangel, miembro del Instituto de Ciencias Naturales de la Universidad Nacional.

De acuerdo con la agencia de Noticias UN,  Unimedios, el profesor Rangel, asevera que “el agua que se conserva en esa zona alimenta al subsuelo y **nutre a los ríos y humedales cercanos como Torca, Guaymaral y la Conejera”.**

El profesor, que trabajó junto a Thomas Van Der Hammen, sostiene que el trabajo del científico se basó en estudiar la sabana de Bogotá, asegurando que **“El trabajo hace que la sabana de Bogotá sea el sitio con mejor conocimiento de la historia evolutiva biológica**… podríamos decir que es muy poco lo que no conocemos, sabemos sobre cantidad de agua, tipos de vegetación, casi todo”. Además, hay cualquier cantidad de tesis de doctorado e investigaciones desde hace años.

En 2011, la CAR se basa en los estudios de 1991 Van der Hammen para declarar las 1.395 hectáreas como reserva, destinándolas como punto de conectividad con los cerros orientales y el río Bogotá. **La zona de restauración, del 39,61 % de la reserva, fue destinada por la Corporación Autónoma Regional, CAR, para adelantar un restablecimiento ecológico** y así lograr la conexión con el río Bogotá, permitiendo el flujo del agua, ya que se estaría asegurando que los humedales permanezcan en buen estado, y además, permite controlar el clima para no agudizar el problema de las elevadas temperaturas que se están presentando.

Los terrenos también fueron declarados reserva para permitir que la comunidad académica adelante investigaciones científicas, de hecho **se tenía pensado realizar actividades de ecoturismo, lo que no sería recomendable,** según el profesor Orlando Rangel, es construir sobre la reserva, por lo que el científico prefiere aconsejarle al distrito que los proyectos de urbanización se desarrollen hacia el occidente de la ciudad. “Allí los suelos tienen baja oferta ambiental, si se adelantan construcciones hacia la zona occidental se estaría dando un buen uso al suelo sin afectar la riqueza ambiental que tiene Bogotá”.

Cabe recodar que el alcalde de Enrique Peñalosa, informó en los últimos días que pedirá a la CAR y al Ministerio de Ambiente, un ajuste en las normas de la reserva forestal para dar paso a sus proyectos de urbanización.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
