Title: Con militarización no se soluciona la crisis que afronta el Cauca
Date: 2018-07-04 16:24
Category: DDHH, Nacional
Tags: Bacrim, Cauca, Disidencias FARC, narcotrafico, paramilitares, Sustitución de cultivos de usos ilícito
Slug: el-cauca-no-necesita-militarizacion-necesita-planes-de-sustitucion-indepaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/cauca-militarizacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo/ Shimmy Berkley] 

###### [04 Jul 2018] 

**La masacre de las siete personas en Argelia, Cauca**, debe ser, de acuerdo con Camilo González Posso el director del Instituto de Estudios para el desarrollo para la Paz, un llamado de urgencia para el Estado, que sigue sin tener presencia en este departamento y que más allá de soluciones militares, **debe brindar posibilidades de desarrollo social, político y económico para los habitantes**.

### **El control de los grupos armados en el Cauca** 

Diferentes organizaciones defensoras de derechos humanos han denunciado el aumento de la presencia de estructuras armadas en el departamento producto de la lucha territorial por el control de las rutas del microtráfico y la siembre de cultivos de uso ilícito. Circunstancias que para González Posso, dan como resultado que **el Cauca sea el lugar con la mayor tasa de asesinatos a líderes sociales, indígeas y afros**. (Le puede interesar: ["Jamer Idrobo, líder social de Balboa, fue asesinado en el departamento del Cacua"](https://archivo.contagioradio.com/jamer-idrobo-lider-social-de-balboa-fue-asesinado-en-el-departamento-del-cauca/))

Sin embargo, pese a la gravedad de la situación, las comunidades de Argelia han tenido la voluntad de acogerse a los planes de sustitución de cultivos de uso ilícito, sin que haya mayor respuesta por parte de las instituciones estatales y que según González Posso evidencia que **“por un lado hay una lentitud de la presencia institucional y por el otro, una ofensiva de los carteles del narcotráfico que van detrás de los cultivos”**.

Además, aseguró que en estos territorios los campesinos han denunciado una connivencia de agentes del estado con negocios del narcotráfico que tienen que ser investigados por parte del Estado, "no se puede explicar que sigan saliendo 200 toneladas en el año, entre las zonas de Tumáco y municipios aledaños, y no pasa nada **¿En dónde esta la Fuerza Pública?, ¿Dónde esta la Armada?, ¿qué pasa con las acciones anticurrupción del país?"**.

### **La respuesta no es la militarización** 

Frente a esta situación, González manifestó que la respuesta que ha dado el Gobierno se ha constituido principalmente en aumentar el pie de Fuerza en la región, en lugar de programar un aceleramiento de los planes y pactos de sustitución o atender “las propuestas de una zona de **reserva campesina ambiental y un plan de desarrollo para el municipio de Argelia”** que permitirían a las comunidades tener garantías para hacer el tránsito entre los cultivos ilícitos a otro tipo de siembras.

“Los campesinos de Argelia que han firmado los planes de sustitución, son aliados de las políticas de reconversión económica y no quieren la presencia del narcotráfico ni de los paramilitares en sus territorios” afirmó González Posso. (Le puede interesar: ["Conmoción en Argelia, Cauca por asesinato de 7 personas"](https://archivo.contagioradio.com/masacre-en-argelia-cauca/))

Frente a la autoría de esta masacre el Ejército Nacional había manifestado que la guerrilla del ELN serían los responsables de este hecho, sin embargo, a través de un comunicado de prensa este grupo armado aseguró, que no son los causantes de los asesinatos. De igual forma, **otras versiones de habitantes del departamento han asegurado que detrás de esta tragedia se encontrarían bandas criminales, estructuras paramilitares o disidencias de las FARC**.

<iframe id="audio_26895676" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26895676_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
