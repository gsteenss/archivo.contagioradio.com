Title: Víctimas de Salvatore Mancuso piden su extradición a la Fiscalía
Date: 2020-08-13 12:21
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Extradición Mancuso, MOVICE, Paramilitarismo, Salvatore Mancuso, víctimas
Slug: victimas-de-salvatore-mancuso-piden-su-extradicion-a-la-fiscalia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Víctimas-piden-extradición-de-Salvatore-Mancuso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este jueves se llevó a cabo una rueda de prensa convocada por el Movimiento de Víctimas de Crímenes de Estado -[Movice](https://movimientodevictimas.org/)- donde **las víctimas del paramilitarismo solicitaron la extradición a Colombia de Salvatore Mancuso, excomandante de las Autodefensas Unidas de Colombia -AUC-.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Salvatore Mancuso fue condenado por más de 1.500 crímenes sobre los que no ha cumplido pena en Colombia, sin contar que se le atribuyen alrededor de 75.000 actos criminales asociados a su comandancia.** Las víctimas rechazaron «*la inacción del Estado colombiano que ha permitido un escenario de impunidad que se agravará con la posible deportación de Mancuso a Italia el próximo 26 de agosto*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Movice aseguró que **desde el 27 de marzo de este año, Mancuso quedó en libertad tras cumplir su condena por narcotráfico en los Estados Unidos y que desde entonces el Estado colombiano ha sido «*inoperante*» en solicitar la extradición para su regreso al país** para que cumpla las sentencias que tiene en firme por crímenes como masacres, desaparición forzada, homicidios, entre otros; pero sobre todas las cosas, para que con su testimonio aporte verdad para la reparación integral de las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Movice, la inacción del Estado colombiano se debería a que la Fiscalía —como ente investigador de la justicia ordinaria— habría renunciado a su competencia para solicitar la extradición, excusándose en que la Jurisdicción Especial para la Paz -JEP- está tramitando un pedido que hizo Mancuso para su sometimiento ante esa magistratura.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, **la JEP negó ese pedido de sometimiento en primera instancia, y adicionalmente su presidenta Patricia Linares remitió una carta al fiscal Francisco Barbosa, explicando que el proceso que adelanta Salvatore Mancuso ante esa jurisdicción no impide que las autoridades judiciales y gubernamentales del país tramiten su extradición para que regrese a Colombia.** (Lea también: [La JEP le cierra la puerta a Salvatore Mancuso](https://archivo.contagioradio.com/la-jep-le-cierra-la-puerta-a-salvatore-mancuso/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La verdad que podría aportar  Salvatore Mancuso para la reparación de las víctimas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este actuar omisivo por parte del Gobierno y la Fiscalía para solicitar la extradición de Mancuso, según el Movice, ratifica el temor que expresaban las  víctimas —en el marco de la implementación de la Ley de Justicia y Paz— de que **la extradición se utilizara como un instrumento para «*acallar la verdad y garantizar el silenciamiento*».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la rueda de prensa también se escucharon las voces de **Rosario Montoya Hoyos**, víctima de despojo y desplazamiento forzado por parte del paramilitarismo en Córdoba; **Rafael Francisco Oviedo Piñerez**, víctima de la Masacre del Chengue; **Rodolfo Sepulveda**, víctima de la  Masacre de Tibú, **Renata Cabrales Cueto**, víctima de un atentado terrorista perpetrado por los paramilitares en el que su hija falleció; y **Eduardo Carreño**, abogado del Colectivo de Abogados José Alvear Restrepo -CAJAR-; quienes también insistieron en que **Mancuso debe ser extraditado a Colombia para responder por sus crímenes y aportar verdad a los procesos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El exjefe paramilitar ha sido recurrente en manifestar su disposición de acogerse a la JEP** para aportar su verdad frente a los crímenes en los que participó y adicionalmente  frente a los presuntos nexos del expresidente Álvaro Uribe Vélez y las AUC, para lo cual ha dicho **«*Voy a decir la verdad convéngale a quien convenga y perjudique a quien perjudique*».** Sin embargo, su solicitud de sometimiento está siendo estudiada en segunda instancia, después de ser negada en primera por dicho tribunal. (Lea también: [Salvatore Mancuso reafirma su compromiso con las víctimas](https://archivo.contagioradio.com/salvatore-mancuso-reafirma-su-compromiso-con-las-victimas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a la decisión de primera instancia se han opuesto organizaciones de víctimas acreditadas ante la JEP afirmando que el rechazo de Mancuso debe ser revisado, puesto que, este desempeñó un papel fundamental como tercero civil en operaciones de las Fuerzas Militares y cualificó las operaciones de tipo paramilitar como parte de una estrategia de Estado para beneficiar a sectores políticos y empresariales.

<!-- /wp:paragraph -->

<!-- wp:html /-->

<!-- wp:block {"ref":78955} /-->
