Title: ELN anuncia cese de operaciones militares ofensivas desde el 9 hasta el 13 de Marzo
Date: 2018-02-26 12:42
Category: Nacional, Paz
Tags: Cese de operaciones militares ofensivas, ELN
Slug: eln-anuncia-cese-operaciones-militares-ofensivas-desde-9-13-marzo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/entrevista-eln.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: youtube] 

###### [26 Feb 2018] 

A través de un comunicado de este 25 de Enero el ELN informó que tomó una decisión de declarar un cese unilateral de operaciones militares ofensivas desde el 9 al 13 de Marzo, para facilitar el proceso electoral. Al mismo tiempo pidieron que se reinicie el ciclo de conversaciones de paz y que se avancen en nuevos acuerdos para generar alivios humanitarios. Por su parte el **presidente Santos afirmó que este es el tipo de medidas que se estaban esperando.**

En el comunicado el ELN también reafirman la importancia de seguir trabajando en avanzar en la agenda acordada en Caracas el 30 de Marzo de 2017 y **retomar el diálogo en torno a un "Gran Diálogo Nacional" que priorice la participación de sectores de la sociedad**. Además pidieron que la delegación de paz del gobierno se traslade a Quito para reiniciar las conversaciones.

Por su parte el presidente Juan Manuel Santos aseguró que este tipo de acciones eran las que se estaban esperando desde que se “rompió” el cese bilateral, “este comunicado y si lo respaldan con acciones, es el tipo de gestos que buscábamos para reanudar esos diálogos”. Se espera que en el transcurso del día la delegación de paz del gobierno viaje a Quito y se reanude la conversación. [Lea también: Gobierno y ELN deben acercar sus posiciones para avanzar en los diálogos](https://archivo.contagioradio.com/gobierno-y-eln-deben-acercar-sus-logicas-para-salvar-el-proceso-de-paz/)

Estas conversaciones se suspendieron una vez terminó el Cese Bilateral el 9 de Enero pasado por decisión unilateral del gobierno nacional. Desde ese momento organizaciones sociales, analistas y cientos de personas se han manifestado para que reinicien las conversaciones y se avance en la agenda de participación social. [Lea también: Proceso de paz con el ELN es un bien público.](https://archivo.contagioradio.com/proceso-de-paz-con-el-eln-es-un-bien-publico-y-debe-retomarse-movimientos-sociales/)

##### *"Un mensaje de respeto a quienes votan* 

##### *Ante las próximas elecciones del 11 de marzo -así no compartamos estos procesos viciados-; como una muestra de respeto a las colombianas y colombianos que van a depositar su voto, el Ejército de Liberación Nacional realizará un cese de operaciones militares ofensivas, entre el 9 y el 13 de marzo próximo.* 

##### *Valoramos y agradecemos los esfuerzos de facilitación en pos de la continuidad del proceso de paz, que realizan múltiples sectores en Colombia, en especial Monseñor Urbina Presidente de la Conferencia Episcopal de Colombia, el Senador Iván Cepeda y Álvaro Leyva, los Grupos sociales y Plataformas de paz, los Grupos de la comunidad internacional Garantes y Acompañantes de esta Mesa, y Jean Arnault el Jefe de la Misión de verificación de la ONU.* 

##### *Recordamos la importancia que tiene seguir avanzando en la Agenda pactada en Caracas el 30 de marzo de 2016, para llegar a acuerdos, pues las dos partes nos comprometimos a desarrollar la participación de la sociedad en la construcción de la paz, por medio de “un ejercicio dinámico y activo, incluyente y pluralista, que permita construir una visión común de paz, que propicie las transformaciones para la nación y las regiones”.* 

##### *En esa dirección el Quinto ciclo de conversaciones debe proyectar el Gran Diálogo Nacional, cuyas bases quedaron establecidas, desde finales de 2017, en las Audiencias de Tocancipá, donde participaron centenares de líderes de todos los sectores y regiones de Colombia.* 

##### *Así mismo, este Quinto ciclo debe evaluar el pasado cese, para pactar uno nuevo, donde se respete la bilateralidad en los mecanismos de verificación y control del cese; acuerdo que interprete a cabalidad a las dos partes. * 

##### *La Agenda debe continuarse desarrollando con rigurosidad y celeridad, respetando a la vez los instrumentos y protocolos pactados, para buscar un acuerdo que supere la confrontación armada y acordar transformaciones en búsqueda de una Colombia en paz y equidad.* 

##### *En consecuencia con lo anterior, proponemos al Presidente Santos fijar una fecha de inicio del Quinto ciclo de conversaciones y enviar su Delegación de Diálogo a Quito. En correspondencia, en esta fecha también se harían presentes todos nuestros delegados, en la capital de Ecuador.* 

##### *Comando Central* 

##### *Ejército de Liberación Nacional* 

##### *Montañas de Colombia* 

##### *Febrero 25 de 2018"* 

<iframe id="audio_24080862" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24080862_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
