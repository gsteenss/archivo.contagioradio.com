Title: Plena disposición de la JEP para que se esclarezcan investigaciones desde la Fiscalía
Date: 2018-09-07 16:15
Author: AdminContagio
Category: Paz, Política
Tags: FARC, Fiscalía, JEP, Nestor Humberto Martínez, Patricia Linares
Slug: plena-disposicion-de-la-jep-para-que-se-esclarezcan-investigaciones-desde-la-fiscalia
Status: published

###### [Foto:  JEP] 

###### [07 Sept 2018] 

Tras las declaraciones del fiscal general de la Nación, Néstor Humberto Martínez, referente a investigaciones en contra de funcionarios de la JEP, por supuestos encubrimientos a ex jefes de FARC para que salieran del país, **Patricia Linares, presidenta de esta instancia, aseguró que están en plena disposición para que se esclarezca este hecho.**

Asimismo, afirmó que la **Fiscalía dejó en claro que no se trata de ningún funcionario adscrito a la magistratura** y agregó que "la jurisdicción agradece la diligencia y el ejercicio preventivo" de esta autoridad, que redundan en el cabal cumplimiento de las funciones de la JEP. (Le puede interesar: ["Piden a la JEP proteger 16 lugares donde se hallarían personas desaparecidas"](https://archivo.contagioradio.com/piden-proteger-16-lugares-jep/))

![DmhKyI3V4AAmnbz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/DmhKyI3V4AAmnbz-565x800.jpg){.alignnone .size-medium .wp-image-56440 width="565" height="800"}

Néstor Humberto Martínez, afirmó que la Fiscalía "tiene evidencias de que algunos funcionarios estarían cocinando en este momento falsedades y fraudes procesales para que la JEP no actúe como debe actuar, cumpliendo con la Constitución y la ley". De igual forma, señaló que un Físcal especializado ya se encuentra llevando las correspondientes investigaciones, de las cuales se espera resultados prontamente.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
