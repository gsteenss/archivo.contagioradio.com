Title: FARC denuncia asesinato y abuso sexual de madre de un ex combatiente
Date: 2018-03-08 14:53
Category: DDHH, Nacional
Tags: Asesinatos, ex combatientes, FARC, Valle del Cauca
Slug: farc-denuncia-asesinato-de-madre-de-un-ex-combatiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/WhatsApp-Image-2018-01-09-at-15.40.18.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Entérate Cali] 

###### [08 Mar 2018] 

El partido político FARC denunció que el pasado 24 de febrero fue asesinada la señora Rosa Elena Bravo, **madre del excombatiente y prisionero político Jhony Bravo**. Los hechos ocurrieron en la vereda La Cabaña del municipio de Jamundí en el departamento del Valle del Cauca. Desde la firma de los Acuerdos de Paz, 14 familiares de ex combatientes han sido asesinados.

De acuerdo con la denuncia, integrantes de la Junta de Acción Comunal de ese municipio confirmaron que “cerca de las 4:00 pm, en el lugar de su residencia, **fue encontrado el cuerpo sin vida**” de la señora Rosa Bravo. Indicaron que según “la inspección realizada al cuerpo de la víctima hace presumir que se trata de una muerte violenta”.

Afirman además que hubo hechos “consistentes” de **acceso carnal violento** contra la madre del excombatiente y que “la información suministrada por  habitantes de la vereda, da cuenta que cerca al lugar se encontraba una patrulla del Ejército Nacional”. (Le puede interesar:["No cesan los asesinatos contra integrantes de la FARC"](https://archivo.contagioradio.com/asesinados-integrantes-de-las-farc-peque/))

### **FARC exige investigaciones pertinentes por parte del Gobierno Nacional** 

Ante esto, el partido político le exigió al Estado Colombiano y en especial a la Unidad Especial de Investigaciones de la Fiscalía General de la Nación que realice las investigaciones pertinentes. Recordaron que en esa región del Valle del Cauca, la Defensoría del Pueblo ha catalogado a **Jamundí como un municipio de alto riesgo**.

Le solicitaron al Gobierno Nacional que se “fortalezcan los esfuerzos tendientes a garantizar de manera inmediata **los mecanismos y medidas de protección**, especialmente para líderes, defensores y defensoras de Derechos Humanos, así como para los candidatos que han denunciado amenazas contra su vida e integridad física y los militantes de nuestro partido político”.

Hay que recordar que el partido político FARC ha venido alertando sobre la grave **situación de seguridad** que viven los ex combatientes como también sus familiares. Según sus cifras, han sido asesinados más de 49 integrantes de la comunidad “fariana”.

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
