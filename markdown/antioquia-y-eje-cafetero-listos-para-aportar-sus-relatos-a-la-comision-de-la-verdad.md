Title: Antioquia y Eje Cafetero, listos para trabajar junto a la Comisión de la Verdad
Date: 2019-03-26 17:34
Category: DDHH, Paz
Tags: apartado, Casas de la Verdad, comision de la verdad
Slug: antioquia-y-eje-cafetero-listos-para-aportar-sus-relatos-a-la-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/D2ReofUXgAATO12-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @ComisionVerdadC 

###### 26 Mar 2019

El pasado 23 de marzo, la Comisión de la Verdad abrió la primera Casa de la Verdad de la región de Antioquia y el Eje Cafetero, una región que será clave para el esclarecimiento de los hechos sucedidos en el marco del conflicto y que tendrá su sede en el municipio de Apartadó donde esperan conocer cerca de 2.000 testimonios.

**Max Yuri Gil macro coordinador de la Comisión de la Verdad** en la región de Antioquia, afirma que desde finales del 2018 identificaron la importancia de instaurar una sede física en Urabá, "todos sabemos que ha sido una región profundamente golpeada por el conflicto armado, prácticamente todos los actores de la violencia han pasado por el Urabá" indica el funcionario.

**Compromiso por parte de las víctimas y los empresarios**

En cuanto a la participación de los empresarios del Urabá y su participación en el conflicto,  Gil explica que desde la Comisión han entablado un relacionamiento y diálogos diferenciales con cada uno de los sectores económicos de la región, **reuniéndose con el grupo EPM, sectores empresariales de Puerto Antioquia y algunas comercializadoras de banano.**

"Hay algunos sectores que han manifestado su disposición a contar y es necesario profundizar en esta relación". En cuanto a los sectores que han demostrado desconfianza ante el mecanismo, el macro coordinador indica que desde la Comisión siempre han buscado tender puentes. [(Lea también: 10 pautas para presentar un caso ante la Comisión para el Esclarecimiento de la Verdad) ](https://archivo.contagioradio.com/10-pautas-para-presentar-un-caso-consolidado-ante-la-comision-de-la-verdad/)

Por otro lado. Maxi explica que las organizaciones sociales y de víctimas son unos aliados fundamentales con los que han establecido convenios de cooperación y a través de los cuales **esperan conocer un estimado de 2.000 casos en la región de Antioquia de los cuales 400 testimonios corresponderían al Urabá.**

Frente a la **reducción del 40% de presupuesto** destinado a la Comisión, el macro coordinador explica que a pesar de la difícil relación que existe con el Gobierno, la Comisión ha contado con el respaldo de agencias de cooperación y la comunidad internacional que promueven la financiación de esta iniciativa. [(Le puede interesar Comisión de la Verdad sigue firme en su respaldo a la JEP ) ](https://archivo.contagioradio.com/comision-de-la-verdad-deja-en-firme-su-respaldo-a-la-jep/)

<iframe id="audio_33753712" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33753712_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
