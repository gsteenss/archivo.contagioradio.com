Title: Policía gastaría más de 9.500 millones para comprar armas en plena pandemia
Date: 2020-05-06 20:14
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Abuso de fuerza ESMAD, Policía
Slug: policia-gastaria-mas-de-9-500-millones-para-comprar-armas-en-plena-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/27537460656_ed3d676360_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

Una licitación que vence el 29 de Mayo, para compra de armas para la Policía, por un valor de 9.515.844.030 millones de pesos es el nuevo escándalo por cuenta de gastos exorbitantes del Estado, en medio de un aislamiento obligatorio que tiene sin recursos y sin comida un altísimo porcentaje de la ciudadanía colombiana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En particular el gasto comprende la compra de **81.000 gases lacrimógenos y 13.000 balas para el ESMAD**, según denunció el Senador [Wilson Arias Castillo.](https://twitter.com/wilsonariasc)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La compra de la Policía, incluye **23.775 esferas marcadoras, 11.000 granadas de aturdimiento y 5.000 multi impacto, usadas por el Escuadrón Móvil Antidisturibios, ESMAD**. Lo que, para el senador, evidencia que la política de manejo de la movilización social se va a fortalecer, en lugar de fortalecer la política social que, en tiempos de pandemia, debería estar enfocada a atender las necesidades de salud y alimentación que han generado fuertes protestas en varias ciudades del país.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Compras y gastos en publicidad, camionetas y armas para el gobierno suman cerca de 21 mil millones de pesos

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A esta licitación de la policía se le suma la reciente compra de camionetas blindadas para el servicio de la presidencia por más de 9 mil millones y la contratación en publicidad para el presidente por 3.5 mil millones de pesos, pagados a una de las firmas que financió la campaña por el No en el plebiscito al acuerdo de paz con las FARC. [(Le puede interesar: Mejorar imagen de Duque con recursos para la paz: una muestra de insensbiilidad y desconexión hacia el país)](https://archivo.contagioradio.com/mejorar-imagen-de-duque-con-recursos-para-la-paz-una-muestra-de-insensibilidad-y-desconexion-hacia-el-pais/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Arias, durante una intervención en el congreso aseguró que **“Se espera que se pueda reorientar el gasto en época de emergencia y en función de paleativos de la pandemia en materia de salud, pero también de una intervención social. Pero pareciera ser que el Gobierno no está interesado en un verdadero plan social, sino en preparar una arremetida contra el pueblo una vez termine la pandemia**”

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Por qué produce tanta indignación el gasto elevado en la compra de armas de la Policía?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Recientemente el país se ha visto envuelto en una serie de protestas, muchas de ellas derivadas de la pandemia del COVID 19 y relacionadas con las malas definiciones en materia presupuestal por parte del gobierno de Iván Duque.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Una de las fuertes protestas se genera a partir del 22 de Marzo en las cárceles del país, y que ha dejado hasta el momento **un saldo de 23 personas muertas en la cárcel modelo de Bogotá en donde los internos exigían planes de choque para evitar que el COVID llegara al interior de los penales**, esta situación ha cobrado la vida de 3 internos en la cárcel de Villavicencio y el contagio de más de 600 de ellos, para poner solamente un ejemplo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1258109799851573249","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1258109799851573249

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Estas protestas que se extendieron por todo el país tienen como primera respuesta al ESMAD, que el 23 de Marzo, desde muy temprano estuvo atacando a familiares de internos de la modelo que llegaron a exigir información sobre sus familiares internos. [(Lea también: A pesar de exigencias de Movimiento carcelario muere primera persona por COVID 19)](https://archivo.contagioradio.com/a-pesar-de-exigencias-de-movimiento-carcelario-muere-primera-persona-por-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Pandemia, hambre y ESMAD, la combinación que reciben los más vulnerables

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Otra de las protestas tiene que ver con las condiciones precarias de alimentación que afrontan los habitantes de zonas periféricas de las grandes ciudades, como es el caso de la localidad de Ciudad Bolivar en Bogotá.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cuando sus habitantes han exigido una renta básica o ayudas en alimentación la presencia del Estado se hace sentir a través del mismo ESMAD que llega prontamente a desmontar la movilización ciudadana que se limita a cerrar algunas calles por la que el tránsito es casi ausente.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El campo no se queda atrás. Son varias las denuncias de comunidades que se ven enfrentadas al hambre a mediano y largo plazo por cuenta de las jornadas de erradicación forzada realizadas en principio por el ejército, pero que en muchas ocasiones se ve reforzado por la presencia, otra vez, del ESMAD.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Estas comunidades piden también una atención acorde al momento de crisis del país, pero la respuesta del Estado es el incumplimiento en los planes de sustitución acompañados fuerza pública para impedir protestas.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
