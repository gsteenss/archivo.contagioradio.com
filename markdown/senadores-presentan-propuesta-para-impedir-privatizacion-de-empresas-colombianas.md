Title: Senadores radican proyecto para impedir privatización de empresas colombianas
Date: 2015-11-12 16:14
Category: Nacional, Política
Tags: Alianza Verde, Consejo de Estado, hidrosogamoso, Jorge Robledo, Polo Democrático Alternativo, Venta de Isagen
Slug: senadores-presentan-propuesta-para-impedir-privatizacion-de-empresas-colombianas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Senado-colombiano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Lapatilla.com 

<iframe src="http://www.ivoox.com/player_ek_9368769_2_1.html?data=mpijmpyafY6ZmKiakpmJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8biwsnc1MrXb9HmxtjS0NnFsozk09Td18rXuMKf0cbfw5DNsdHZxc7fjdXWrdfV1c7nw8jNaaSnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jorge Robledo, senador Polo Democrático Alternativo] 

###### [12 Nov 2015]

**Cerca de 40 senadores radicaron un proyecto de Ley con el objetivo inicial de frenar la decisión del Consejo de Estado, con la que se dio vía libre a la venta de ISAGEN**. Se trata de una iniciativa que buscaría no sólo impedir la venta de esta empresa pública, sino la privatización de cualquier empresa del país.

“Es un proyecto de Ley que apunta a que en Colombia no se hagan privatizaciones sin que el Gobierno con estudios serios argumente la privatización y que está sea aprobada como una ley por el Congreso de la República de forma democrática”, explica el senador del Polo Democrático Alternativo, Jorge Robledo.

Para el congresista, vender ISAGEN **“es un pésimo negocio, la venden porque es un excelente empresa, lo mejor que tiene el Estado colombiano porque genera altas rentabilidades”,** y agrega que con la venta de esta empresa pública, el gobierno estaría obligando a los colombianos y colombianas a consumir energía costosa de acuerdo con las conveniencias de los privados.

El Gobierno prepara documento para subastar **ISAGEN, una empresa valorada en \$5,2 billones,** suma que se dirigiría a la construcción de las vías de cuarta generación, como lo ha anunciado el presidente Juan Manuel Santos.
