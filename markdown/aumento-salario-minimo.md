Title: Trabajadores buscan que aumento de salario mínimo sea de doble dígito
Date: 2018-12-11 18:25
Author: AdminContagio
Category: Economía, Nacional
Tags: CGT, CUT, salario minimo, Trabajadores
Slug: aumento-salario-minimo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/salario-minimo-dinero-grafica-e1544638667810-770x400-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Contamos] 

###### [11 Dic 2018] 

Inicia la puja por el salario mínimo, y mientras **trabajadores piden que la cifra sea de al menos doble dígito; la ministra de trabajo Alicia Arango, se ha limitado a señalar que debe superar la inflación, calculada en 3,27%** por el DANE. El aumento del salario mínimo es uno de los temas más importantes a final de año, pues de este pago dependen cerca de 10 millones de colombianos.

**Diógenes Orjuela, presidente de la Central Unitaria de Trabajadores (CUT)**, recordó que la agenda para debatir sobre el salario mínimo inició el 22 de noviembre, pero hoy será el día en que empresarios, Gobierno y trabajadores pondrán sobre la mesa su posición. En el caso de la Confederación General del Trabajo **(CGT), la cifra será del 10% de aumento, mientras que la CUT propondrá un 12%** de incremento sobre la base del salario existente.

Orjuela explicó que **el aumento propuesto tomó en cuenta 4 componentes**: La inflación; la brecha de inflación, que es la inflación que pagan los sectores de menores ingresos y suele ser mayor a la inflación general; la productividad laboral media, que es diferente a la productividad total de los factores; y la brecha por la productividad laboral. Adicionalmente, el Presidente de la CUT afirmó que tal aumento sería útil para reactivar la economía, otorgándole un mayor poder adquisitivo a los colombianos.

### **Calcular el salario mínimo según inflación y productividad es simplista** 

La ministra de trabajo, Alicia Arango, declaró que las cifras a tener en cuenta para evaluar el aumento del salario mínimo serían la productividad, calculada en 0,52%; la inflación del 3,27%; el crecimiento estimado de la economía en 2,7%; y la tasa de desempleo del 9,5%. No obstante, Orjuela criticó esta posición, y aseveró que **“la visión para calcular el salario mínimo únicamente basado en inflación y productividad es simplista”**.

A lo mencionado sobre la brecha que existe entre la inflación para sectores de altos y menores ingresos, el Representante de los trabajadores añadió que la regulación para determinar el salario mínimo es clara en que un factor a tomar en cuenta es la productividad laboral media y no a la de todos los factores, **de forma tal que esta cifra no sería de 0,52%, sino de 1,7% para 2018**.

Por ahora, como lo indicó Orjuela, el balón está en el campo del Gobierno, que tendrá que poner sus cifras, y basados en **la propuesta de aumentar en 4 puntos el salario por parte de los empresarios**, y la diferencia que haya entre las 3 partes en negociación, se sabrá la posibilidad de llegar a acuerdos. (Le puede interesar:["Salario M. no cuadra con aumento del costo de vida en Colombia"](https://archivo.contagioradio.com/salario_minimo_2018_colombia/))

### **Los números favorecen a los trabajadores** 

En el informe del Dane sobre empleo se muestra del total de ocupados (22,3 millones), 44% devengan hasta un salario mínimo, es decir cerca de 9,8 millones de personas.  A ello se suma que, según Orjuela, el 75% de las personas que devengan un salario mínimo son informales, y que el promedio de los salarios en Colombia es de 900 mil pesos; razones suficientes que avalan la necesidad de aumentar el mínimo de manera considerable. (Le puede interesar: ["Hueco fiscal se lo inventó el Ministro de Hacienda: Eduardo Sarmiento"](https://archivo.contagioradio.com/hueco-fiscal-hacienda/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
