Title: Gustavo García "Pantera'' en Sonidos Urbanos
Date: 2016-07-13 13:27
Category: Sonidos Urbanos
Tags: Fruko y sus tesos, Gustavo Garcia Pantera, Salsa colombiana
Slug: gustavo-garcia-pantera-en-sonidos-urbanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/pantera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Pantera Fiera Brass 

Cargado de la energía que caracteriza a la región caribe colombiana y a sus habitantes, e impregnado de la cultura bogotana y de una fuerza significativamente jovial, el reconocido músico **Gustavo García ''Pantera''** nos dio su confianza en Sonidos Urbanos para que, además de confirmar su amor por la salsa, nos permitiera conocer un poco más de su vida, su pasión por la fusión de los ritmos y su nueva apuesta musical: '[Pantera Fiera Brass](https://www.facebook.com/ElPanteraFieraBrass/?fref=ts)'.

''Pantera'' es, quizás, el seudónimo más acertado para este músico, compositor y arreglista, que con su potencia para imitar e interpretar el trombón se convirtió en uno de los pioneros de la salsa en el país. "*El trombón me evolucionó a mi, por eso en estos momentos creo que soy más trombonista que nunca*", a pesar de ser la guitarra el instrumento que en más oportunidades le ha acompañado en su vida musical.

Además de su paso por agrupaciones como **Fruko y sus tesos, The latin brothers **y la **[Banda de Los Felinos, ]{#sc-tooltip3 .sc-tooltip data-image="http://www.solarlatinclub.com/wp-content/uploads/2014/08/LaBandaDeLosFelinos-DirigeGustavoGarciaFolderLPFront.jpg" data-desc="La Banda De Los Felinos"}**[el compositor valduparense]{#sc-tooltip3 .sc-tooltip data-image="http://www.solarlatinclub.com/wp-content/uploads/2014/08/LaBandaDeLosFelinos-DirigeGustavoGarciaFolderLPFront.jpg" data-desc="La Banda De Los Felinos"} ha creado como solista ritmos que mezclan  el reggae, el jazz, el blues y la música electrónica, consolidando un nuevo género musical: "**ancestrónica**", una fusión de secuencias electrónicas con ritmos caribeños que sirve para "*quitar etiquetas*" a la música y construir melodías con sonidos plurales.

Pantera, quien a lo largo de su existencia ha expresado su ira e inconformidades a través de sus creaciones artísticas, considera que bailar es una forma para reconciliarnos, "c*omo caribeño mi música es para "fiestar", quiero incitar a la gente a bailar; a que los hombres y las mujeres bailen, a que los hombres con hombres bailen, porque estamos ahora en eso, aceptándonos a todos. Que la reconciliación sea bailando*".

Dale play a este programa y conoce el recorrido de la música y vida de Gustavo García, "Pantera".

<iframe src="http://co.ivoox.com/es/player_ej_12212108_2_1.html?data=kpefk5eVdJmhhpywj5WVaZS1lJ2SlaaWfY6ZmKialJKJe6ShkZKSmaiRdI6ZmKiassbSuMbmwpDS0JC3s8_dxdTgjbrWpsLi0Niah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
