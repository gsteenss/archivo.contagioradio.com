Title: Carlos Yunda y Weimar Galíndez, cultivadores de paz asesinados en Cauca
Date: 2019-07-10 12:57
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Carlos Yunda, Cauca, Escombatientes, Weimar Galíndez
Slug: carlos-yunda-weimar-galindez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Fotos-editadas1-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Este martes fueron asesinados **Luis Carlos Yunda y Weimar Galíndez,** integrantes de FARC que se acogieron al proceso de paz y llevaban adelante su proceso de reincorporación en Cauca. Por una parte, Yunda pertenecía a la cooperativa CEPRODED, trabajaba como cultivador de piña vivía en el Espacio Transitorio de Capacitación y Reincorporación (ETCR) de Monte Redondo en Miranda, por otra parte, Galíndez vivía cerca al municipio de Miranda junto a su familia.

**Luis Enríquez, integrante de la dirección del partido FARC** en Cauca señaló que según la información recibida desde la comunidad, Weimar se encontraba cerca a la vereda donde vivía departiendo con campesinos cuando hombres armados le propinaron tres disparos que acabaron con su vida. En el caso de Carlos Yunda, armados atentaron contra su integridad en la vereda El Jagual, en la que se encontraba junto a su hermano, quien se encuentra en delicado estado de salud.

> Aquí nos siguen matando, siguen condenando el sueño de la paz al fracaso. Carlos Yunda excombatiente en proceso de reincorporación acaba de ser asesinado en Corinto, Cauca, su hermano está herido. ¿Cuáles eran las medidas del presidente [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) para proteger nuestra vida?
>
> — Victoria Sandino (@SandinoVictoria) [9 de julio de 2019](https://twitter.com/SandinoVictoria/status/1148700770885427201?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Carlos Yunda y Weimar Galíndez, dos formas de llevar la reincorporación**

De acuerdo a Enríquez, Yunda era un líder muy querido en la zona entre los excombatientes y la misma población civil; "era conocido por llevar la vocería de las personas en la cooperativa". Por su parte, Weimar fue exprisionero político, no pertenecía a la cooperativa de El Tambo y llevaba su proceso de reincorporación de forma más individual y dedicado a su familia, **"producto del temor de la militancia nuestra en esa zona, porque han matado a varios miembros de la cooperativa que trabajan ahí".**

Tras los hechos, el Integrante del partido FARC en Cauca resaltó que la comunidad de la zona está muy dolida por ambos asesinatos porque recientemente, cerca al ETCR de Monte Redondo asesinaron al gringo, como era conocido[David.](https://archivo.contagioradio.com/anderson-perez-excombatiente-y-comunicador-fue-asesinado-en-cauca/) En ese sentido, puntualizó que "le han tocado las fibras más sensibles a la comunidad, porque estas personas que están persiguiendo son las más cercanas a la comunidad, las que han hecho de puente y que llevan las vocerías en su zona".

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fncprensa%2Fvideos%2F347756509255460%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **Se está viendo truncada la reincorporación**

Enríquez declaró que, en general, en Cauca los integrantes del partido FARC están constantemente sometidos a panfletos, llamadas amenazantes y seguimientos irregulares; "cuando se han identificado los que amenazan, lo hacen bajo el nombre de Águilas Negras", aclaró, y añadió que quienes han visto estas bandas operar, los han identificado como "grupos de corte paramilitar". (Le puede interesar: ["Con homicidio de Servio Cuasaluzán, son 134 excombatientes asesinados"](https://archivo.contagioradio.com/con-homicidio-de-servio-cuasaluzan-son-134-excombatientes-asesinados/))

El constante riesgo sobre la vida ha causado temor en los excombatientes , "porque **no hay garantía para desarrollar sus proyectos productivos, no puede ver su participación en política y están viendo truncada la reincorporación"**, aseguró Enriquez; razón por la que pidió a los entes investigadores adelantar los procesos necesarios para establecer la autoría material e intelectual de los asesinatos y amenazas. (Le puede interesar:["Aesinan a excombatiente de FARC, Jorge Enrique Sepúlveda en Córdoba"](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-farc-jorge-enrique-sepulveda-en-cordoba/))

Respecto a la protección física, el Integrante de FARC denunció que la Unidad Nacional de Protección (UNP) no está garantizando los desplazamientos para las personas que cuentan con esquemas de protección, y no entregan los viáticos a los escoltas para que puedan desarrollar su trabajo; razón por la que pidió que se mejore este proceso, y que se pongan en marcha las medidas de seguridad para solucionar este tipo de riesgos previstas en el Acuerdo de Paz, como impulsar las medidas de autoprotección comunitarias.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
