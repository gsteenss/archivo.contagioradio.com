Title: En la firma de la paz "lloramos de alegría"
Date: 2016-09-27 13:20
Category: Entrevistas, Paz
Tags: Cartagena, FARC, firma del acuerdo, Gobierno, paz, proceso de paz
Slug: en-la-firma-de-la-paz-lloramos-de-alegria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/luz-marina-cuchumbe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo - Justicia y Paz 

###### [27 Sep 2016] 

Luego de 52 años de guerra, el país celebró la firma final del acuerdo de paz entre el gobierno y la guerrilla de las FARC. Un logro que le pertenece también a las víctimas del conflicto armado colombiano que trabajaron en el desarrollo del proceso de paz, esperando llegar al tan anhelado 26 de septiembre.

**“Lloramos mucho de alegría, estoy muy contenta,** uno siente muchas cosas al lograr lo que las víctimas más añorábamos, lo que más pedía yo era que no se levantaran de la mesa”, dice Luz Marina Cuchumbe, una de las personas que hizo parte de la delegación de víctimas que fue a La Habana y madre de Hortensia Cuchumbé, quien hace 10 años fue víctima de ejecución extrajudicial por parte de efectivos militares del Batallón Piguanza en Inzá, Cauca.

Así también lo expresa Jany Silva, lideresa de la Zona de Reserva Campesina Perla Amazónica, lugar que durante años ha sido escenario de guerra. “**Para nosotros como campesinos es muy importante esta firma después de tanta lucha e insistencia,** el momento fue emotivo, esperamos que se pueda construir un país mejor”.

Pese al miedo que causó el sorpresivo sobre vuelo  de un avión Kafir durante el discurso de Timoleón en Cartagena, y llenó de pánico a los asistentes como a Luz Marina, ella prefirió concluir que ese sonido representa el fin de las balas.

Ambas aseguran que ahora el reto, es continuar con la pedagogía de los acuerdos, para la votación del 2 de octubre en el plebiscito, y el reto siguiente será la puesta en marcha de los acuerdos, donde las comunidades y organizaciones serán claves para que lo firmado en Cartagena sea una realidad en los territorios.

**“Hoy nos despertamos sintiendo mucha esperanza de lo que viene de aquí para adelante, nos alistamos para la campaña por el sí,** y para la pedagogía de los acuerdos”, dice Jany Silva. Por su parte, Luz Marina Cuchumbe indica que seguirá trabajando por las víctimas, por su región y por la paz, “Cuando asesinaron mi hija nunca pensé que iba a luchar por la paz de Colombia, **no quiero que más madres lloren y sufran todo lo que yo he vivido durante estos 10 años del asesinato de mi hija**”.

Aunque aceptan que no será fácil, resaltan los discursos de Juan Manuel Santos y Rodrigo Londoño durante la firma del acuerdo. Puntualmente Cuchumbe afirma que su esperanza en ese momento se representó en las palabras de Timoleón cuando se refirió al perdón a las víctimas, “fue lo más bonito que pudimos escuchar”, así mismo, reconoce lo importante que es para las comunidades que Santos haya dicho que el apoyo será ahora para el campo y para las víctimas.

“Voy a seguir trabajando por la paz de Colombia para que los hijos de mis hijos no pasen lo que yo tuve que pasar, **no esperemos que nos toque la guerra para decirle si a la paz”,** concluye Luz Marina, asegurando que hace un llamado a los colombianos y colombianas para que no se desaproveche la oportunidad en las votaciones del próximo 2 de octubre.

<iframe id="audio_13081471" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13081471_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
