Title: Los costos ambientales y sociales del fracking superan los beneficios económicos
Date: 2015-04-14 12:24
Author: CtgAdm
Category: Ambiente, Nacional
Tags: AIDA, Ambiente, Consejo Canadiense de las Academias de la Ciencia, Ecopetrol, fracking, hector herrera, juan carlos echeverry, Justicia Ambiental
Slug: los-costos-ambientales-y-sociales-del-fracking-superan-los-beneficios-economicos
Status: published

###### Foto: Argentina Sin Fracking 

<iframe src="http://www.ivoox.com/player_ek_4352096_2_1.html?data=lZiilJWdeo6ZmKiak5uJd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkNDnjMjc1dnTt4zVzsfWx9PYpc3Z1JDmjdjTp8rVzcrgjcnJsIza08bQzc7Sq4zn1tXS1MbSb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Hector Herrera, Asociación Interamericana para la Defensa del Ambiente] 

El pasado lunes 13 de abril el presidente de **Ecopetrol**, Juan Carlos Echeverry, aseguró que Colombia no se puede dar el lujo de no realizar explotaciones petroleras mediante fracking, ya que con esta producción se financia la salud y la educación en el país, y si se reduce "o nos tocará aceptar menos bienestar, o subir más los impuestos".

Este planteamiento no es nuevo. Por el contrario, se suma a las declaraciones del **Ministro de Minas y Energía**, Tomas Gómez, quien aseguró que gran parte del llamado "post-conflicto" se financiaría con el petróleo generado con este método de extracción. Es un discurso en que el gobierno asegura que es necesario sacrificar el medio ambiente para garantizar la financiación de otros derechos sociales: Aplicar la ley del *mal necesario*.

Para **Hector Herrera, asesor de la Asociación Interamericana para la Defensa del Ambiente** y coordinador de **Justicia Ambiental**, este discurso es un sofisma de distracción, pues la extracción no convencional de petróleo realizada mediante fracking genera daños ambientales que afectarán directamente y en el corto plazo a las comunidades que viven en el territorio circundante en que se realiza.

Por una parte, el fracking contamina las fuentes del subsuelo por la inyección de químicos, como ha demostrado en Canadá el **Consejo Canadiense de las Academias de la Ciencia**, y genera sequías como ha evidenciado en los Estados Unidos Anthony Ingraffea, profesor de la Universidad de Cornell. La liberación del metano genera cambios ambientales mucho más profundos que el dióxido de carbono.

En el corto y el mediano plazo, los problemas de salud que generará en la sociedad la utilización de este método, serán mucho más costosos de lo que el **sistema de salud** podrá costear, incluso con los recursos producidos por el mismo fracking, y que anunciara con satisfacción el presidente de Ecopetrol. **"¿Cuánto cuesta restablecer la salud publica? ¿Cuánto cuesta restablecer una fuente de agua?** Claramente los costos van a superar los beneficios", indica Héctor Herrera.

Además, los espacios poblacionales se verían afectados por la presencia de personas externas y maquinaria, que como han reflejado las intervenciones minero-energéticas como las hidroeléctricas en el **Quimbo, Ituango y Sogamoso**, se vincula a la presencia militar y paramilitar, tráfico de drogas, prostitución y **desplazamiento forzado**.

Las organizaciones sociales se preguntan por qué la industria y el gobierno colombiano busca implementar un sistema que, como el fracking, **ha sido prohibido en países como Francia, Bulgaria, Alemania y Estados Unidos**, y sancionadas moratorias en municipios de **EEUU, Canadá, España, Argentina, Suiza, Italia e Irlanda**, precisamente por los daños sociales y ambientales que genera.

Por su parte, el sindicato de trabajadores de Ecopetrol señaló que el presupuesto para el proceso de paz, y para la garantía de los Derechos Sociales y ambientales que la sustentan, se encuentra en la **tributación que Colombia debe exigir**, y que habría **dejado de recibir por exenciones a empresas multinacionales**, y no en el sacrificio de territorios y comunidades.
