Title: Estudiantes vuelven a las calles por el desmonte del ESMAD
Date: 2019-01-21 13:04
Author: AdminContagio
Category: Educación, Movilización
Tags: ESMAD, estudiantes, marcha, Movimiento social
Slug: desmonte-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/IMG_3000-e1547741787192-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [17 Ene 2019] 

Este jueves 24 de enero será la primera movilización estudiantil del año, luego de 3 meses de paro estudiantil, logrando masivas marchas cada semana. En esta ocasión, **los jóvenes volverán a las calles para exigir el desmonte del Escuadrón Móvil Anti Disturbios (ESMAD),** reclamando que la primera garantía para exigir el derecho a la educación es el respeto a la vida misma, y a la protesta. (Le puede interesar: ["Gobierno y estudiantes logran acuerdo en mesa de negociación"](https://archivo.contagioradio.com/estudiantes-gobierno-acuerdo/))

Luego de un final de año agitado socialmente y tras la firma de dos acuerdos sobre la educación referentes a la financiación de la misma; los jóvenes regresarán a las calles entendiendo que durante este 2019 se preparan movilizaciones de otros sectores sociales, y **el primer paso para garantizar efectivamente el derecho a la protesta es que se respete la vida de quienes exigen se manifiestan en las calles**. (Le puede interesar: ["Esteban Mosquera, víctima del Escuadrón en Cauca en el marco del paro estudiantil"](https://archivo.contagioradio.com/esteban-mosquera/))

Adicionalmente, la movilización se plantea la continuación de las dinámicas de exigencia que ha tenido el movimiento estudiantil, entendiendo que es necesario exigir el cumplimiento de los acuerdos pactados con el Gobierno y continuar negociando los demás puntos del pliego de peticiones; de igual forma, siguiendo la idea de que los estudiantes también están articulados con todo el movimiento social. (Le puede interesar: ["Tras 5 años del paro agrario la situación del campo no ha mejorado"](https://archivo.contagioradio.com/hay-una-crisis-acumulada-en-varios-sectores-agricolas-cesar-pachon/))

En las próximas fechas que se tienen previstas para el movimiento estudiantil están el **encuentro nacional de delegados de todas las Instituciones de Educación Superior (IES) el 25 y 26 de enero en la Universidad de Nariño**. Posteriormente, entre febrero y marzo se realizará el Encuentro Nacional de Estudiantes por la Educación Superior (ENEES) 3.0 para tomar decisiones sobre el referendo por la educación y la participación en un paro cívico nacional.

### **Estas son las rutas y horarios de la movilización:** 

La Universidad Nacional se encontrará a las 12 en la Plaza Che, saldrán  por la Calle 45 hasta la Carrera 24 hasta llegar al Concejo de Bogotá, de allí, partirán cerca de las 2 pm hasta el Parque de los Periodistas por la Calle 26.

La Universidad Pedagógica Nacional saldrá por la Calle 72 hasta el Concejo de Bogotá, y desde allí, seguirá con la Universidad Nacional hasta el Parque de los Periodistas.

Las universidades privadas se movilizarán junto a la Universidad Distrital desde la Carrera 7 con Calle 40 y posteriormente se desplazarán hasta encontrarse con las demás universidades.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
