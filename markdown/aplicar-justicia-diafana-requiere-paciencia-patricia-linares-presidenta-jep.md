Title: Aplicar justicia requiere paciencia: Patricia Linares, presidenta JEP
Date: 2020-04-20 13:57
Author: AdminContagio
Category: Actualidad, Paz
Tags: entrevista, JEP, Justicia restaurativa
Slug: aplicar-justicia-diafana-requiere-paciencia-patricia-linares-presidenta-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Patricia-Linares-presidenta-JEP.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @JEP\_Colombia {#foto-jep_colombia .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Patricia Linares, presidenta de la Jurisdicción Especial para la Paz (JEP) concedió una entrevista a Contagio Radio en la que habló sobre cómo la institución está enfrentando el Covid-19, los retos que se plantean y algunos de los debates trascendentes en medio de los que está la Jurisdicción: excarcelación de comparecientes, ingreso de paras y avance en los procesos que llevan.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Contagio Radio: Hay muchas expectativas sobre lo que será el trabajo de la JEP, pero también se sabe que hay muchos retos, ¿qué factores de riesgo enfrentan?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Patricia Linares: Por tratarse de un modelo inédito de Justicia, que se produce en un país cuyo gobierno en nobre del Estado lo impulsa para superar un conflicto armado interno, de unas dimensiones también inéditas en el mundo. Pero además, en medio de un conflicto que está vivo y respecto del cual hay unos debates políticos vivos muy intensos que llevaron al país a lo que los políticos - yo no lo soy- califican como una situación de polarización.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Situación que ha hecho que, por ejemplo, concluir la elaboración y expedición del marco normativo necesario supuso todo un debate político que implico para la Jurisdicción casi un año de retraso. En una norma que es esencial para cualquier juez en el mundo como es la Ley Estatutaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entonces, cuando a mí me preguntan que si me molestan o los asumo como agresiones esos debates, yo siempre digo que los debates que se dan en el ámbito democrático siempre serán buenos, y siempre produciran buenos resultados cualquiera sea ese resultado. Pero indudablemente eso afectó en términos de tiempo la Jurisdicción como la afecta el hecho de que muchas veces se dificulta acceder a ciertos territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nosotros no somos una justicia que puede operar desde Bogotá, por eso, intentamos estar en los territorios. Pero en la estructura que nosotros tenemos actualmente, no tenemos posibilidad de tener sedes territoriales. Ser creativos es una alternativa y lo hemos sido: teníamos convenios de colaboración armónica con Nariño, encabezado en su momento por Camilo Romero; pero todo eso requiere tiempo y es tiempo que se traduce en que no vamos tan rápido como al gente quisiera.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero más de fondo es este tema: si queremos garantizar una operación que implique una aplicación de justicia diáfana, cumpliendo todos los principios democráticos que se exigen en un Estado de derecho, pues tenemos que tener paciencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También hay todo un tema de aprendizaje, porque este modelo de justicia además de inédito, es muy diferente al de la justicia ordinaria. Por lo tanto, los actores que se involucran a la hora de materializar este modelo muchas veces tienen -o tenemos- que romper paradigmas que vienen muy arraigados en nuestro imaginario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ello pasa por entender que aquí se funciona de manera diferente: mire como es de difícil entender que nosotros sí imponemos sanciones, y siempre imponemos sanciones en cualquier caso. Pero en el imaginario nuestro, incluidos abogados, propios comparecientes, víctimas, la sociedad... está muy arraigado que sanción es igual a privación de la libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el caso nuestro, no necesariamente la sanción va a implicar privación de la libertad, valga decir: reclusión en una cárcel. Pero habrá, en su lugar, un tipo de sanción.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### CR: Una propuesta clara de sanciones alternativas es la que comunidades rurales han hecho con la [Universidad de Paz](https://archivo.contagioradio.com/universidad-de-paz-una-iniciativa-de-educacion-desde-las-comunidades/), ¿ustedes están abiertos a ese tipo de sanciones?

<!-- /wp:heading -->

<!-- wp:paragraph -->

PL: La imposición de las sanciones propias le corresponde diseñarlas e imponerlas a la sección con reconocimiento de responsabildiad, estas son las que van a implicar restricción de derechos pero no necesariamente restricción de la libertad, y a las que tendrán derechos aquellos comparecientes que cumplan en su integralidad sus compromisos de aportes a la verdad, contribución a la reparación y a las garantías de no repetición.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esa sección viene trabajando hace más de un año en el diseño de ese tipo de sanciones, es una tarea difícil porque debe garantizar que las víctimas participen aportando al diseño de esas sanciones, no participando en la imposición de las mismas porque esa es una tarea exclusiva del juez.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es útil un ejemplo: imagínese que un grupo de comparecientes haya, con su acción de guerra, impactado una zona de manera tal que destruyeron hectáreas de bosques con vegetación ntiva, fuentes de agua, incluso espacios simbólicos para una determinada comunidad... una sanción propia puede ser que esas personas durante un tiempo, ocho años, se vayan a trabar en la recuperación de esos bosques.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero no puede ser cualquier tipo de recuperación, tiene que ser orientada por las víctimas originarias de esa región, que son las que saben qué es lo que había, qué es lo que necesitan y cómo lo quieren. Igualmente, debe participar el Estado y eso es muy importante: las autoridades territoriales y nacionales, porque yo puedo mandar a cien personas a cumplir la tarea pero si no hay orientación técnica, recursos tecnológicos, asesoría científica... eso no nos va a servir de nada.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### CR: Otra de las cosas que las personas están esperando serán las resoluciones que tome la JEP respecto al sometimiento de quienes se acogieron a la Ley de Justicia y Paz, ¿cómo va ese tema?

<!-- /wp:heading -->

<!-- wp:paragraph -->

PL: De conformidad con lo acordado, los paramilitares no son sujetos de la JEP. Ellos tienen su juez natural en la justicia ordinaria y concretamente en Justicia y Paz. El mismo acuerdo (de paz) y las normas que lo desarollan establecen que máximos responsables que se prediquen colaboradores, estamos hablando de financiadores, propiciadores, etc.; sí pueden eventualmente tener espacio en la JEP.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto para decirles que supone un estudio muy estricto de condiciones, de aportes verificables para poder llegar a tener la posibilidad de ser contemplados para su acceso y sometimiento a la Jurisdicción. Pero la premisa fundamental es: los paramiltiares no son sujetos de la Jurisdicción.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### CR: Muchas personas también se preguntan si la JEP avanzará en la excarcelación de integrantes de FARC que aún no han salido de los penales, ¿qué acciones están tomando para proteger la vida de estas personas?

<!-- /wp:heading -->

<!-- wp:paragraph -->

PL: Nosotros estamos tratando de agilizar al máximo cierto tipo de actuaciones que pueden contribuir a salvaguardar la integirdad de nuestros comparecientes, pero siempre supeditados al debido proceso y al cumplimiento estricto de la Constitución y la Ley.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La premisa fundamental para el manejo de estas situaciones para nosotros es la salvaguarda del derecho a la salud, la integridad y a la vida de todos. Seguimos de manera estricta las directrices que imparten las autoridades competentes, y creemos que la mejor constribución es seguir cumpliendo con nuestra tarea y acelerar, en la medida de lo posible, aquellas actuaciones que puedan contribuir a ese propósito.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La entrevista completa con Patricia Linares

<!-- /wp:heading -->

<!-- wp:paragraph -->

[La entrevista completa en la que Patricia Linares se refiere con más profundidad a los tiempos que están empleando para tomar decisiones, los retos que enfrentaron con la dificultad para aprobar la Ley Estatutaria y las dificultades que han encontrado para que ciertas instituciones les brinden información la puede encontrar en este [enlace](https://www.facebook.com/113348540811/videos/222729388963459).]

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
