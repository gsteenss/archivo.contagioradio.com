Title: FARC comenzó a construir sus alojamientos pese a incumplimientos del Gobierno
Date: 2017-02-06 16:15
Category: Nacional, Paz
Tags: FARC, paz, Zonas Veredales
Slug: farc-comenzo-a-construir-a-pesar-de-incumplimientos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/zona-veredal-farc-e1484067202705.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [6 Feb. 2017] 

Instalaciones que no están adecuadas o que aún no existen, varios bebés recién nacidos sin condiciones dignas para vivir, mujeres gestantes sin acceso a brigadas de salud, falta de saneamiento básico, ausencia de agua potable y un listado que pareciera interminable, hace parte de las denuncias que siguen reportando desde los diversos frentes de las FARC, como parte del **incumplimiento del Gobierno Nacional a lo pactado en el Acuerdo de Paz y las Zonas Veredales Transitorias.**

En la actualidad, **los guerrilleros que se encuentran en las Zonas han comenzado a construir** sus lugares de habitación, en algunos casos con materiales que ellos logran conseguir y a trabajar para adecuar de manera precaria lugares para ducharse y para cocinar. Le puede interesar: [Continúan incumplimientos del Gobierno en Zonas Veredales](https://archivo.contagioradio.com/continuan-incumplimientos-del-gobierno-en-zonas-veredales/)

A través de sus redes sociales, el máximo jefe de esa guerrilla, Rodrigo Londoño ha manifestado todos los retrasos que han existido en la adecuación de las Zonas Veredales Transitorias. En un tweet aseguró **“En \#ZonaVeredal La Carmelita, Putumayo se ha construido solo una casa de 4 habitaciones, en el lugar se encuentran 400 guerrilleros”.**

> En [\#ZonaVeredal](https://twitter.com/hashtag/ZonaVeredal?src=hash) La Carmelita, Putumayo se ha construido solo una casa de 4 habitaciones, en el lugar se encuentran 400 guerrilleros . [pic.twitter.com/SvPHA0wAdu](https://t.co/SvPHA0wAdu)
>
> — Rodrigo Londoño (@TimoFARC) [5 de febrero de 2017](https://twitter.com/TimoFARC/status/828263609415376900)

<p style="text-align: justify;">
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
De igual manera, a través del Twitter Nueva Colombia Noticias se ha informado de manera constante cada una de las situaciones que se presentan en las Zonas Veredales de todo el país. A través de dicha plataforma Ramiro Durán, del Estado Mayor del Bloque Sur de las FARC aseguró **“llevamos 5 días pidiendo una moto bomba para tener agua potable”.** Le puede interesar: [Bloque Sur denuncia incumplimientos en Zona Veredal de Puerto Asís](https://archivo.contagioradio.com/bloque-sur-denuncia-incumplimientos-en-zona-veredal-de-puerto-asis/)

</p>
> [\#NCAhora](https://twitter.com/hashtag/NCAhora?src=hash) | "Llevamos 5 días pidiendo una moto bomba para tener agua potable" Ramiro Durán del Estado Mayor del Bloque Sur de [@FARC\_EPueblo](https://twitter.com/FARC_EPueblo) [pic.twitter.com/hjAaypT1Ya](https://t.co/hjAaypT1Ya) — Nueva Col. Noticias (@NCprensa) [6 de febrero de 2017](https://twitter.com/NCprensa/status/828625689725300737)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
En cuanto a la situación que viven las madres gestantes, los niños y los bebés se ha reportado que **“Madres de @FARC\_EPueblo soportan rigores del clima viviendo a la intemperie por incumplimiento de Gobierno en \#ZonasVeredales**” y en otra comunicación aseveran “Madres d @FARC\_EPueblo improvisan instalaciones para atender sus bebés en \#ZonasVeredales debido al incumplimiento del Gobierno”.

> [\#NCNoticias](https://twitter.com/hashtag/NCNoticias?src=hash) | Madres de [@FARC\_EPueblo](https://twitter.com/FARC_EPueblo) soportan rigores del clima viviendo a la intemperie por incumplimiento de Gobierno en [\#ZonasVeredales](https://twitter.com/hashtag/ZonasVeredales?src=hash) [pic.twitter.com/9Su1hVnI2N](https://t.co/9Su1hVnI2N)
>
> — Nueva Col. Noticias (@NCprensa) [5 de febrero de 2017](https://twitter.com/NCprensa/status/828298618792710145)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Solo por nombrar algunas, por ejemplo **en la ZVTN Playa Rica, las condiciones no permitieron que las niñas y niños hijos de los guerrilleros se quedaran en la zona** y en la ZVTN Santa Lucia en Ituango, Antioquia, el área del predio es insuficiente, hay condiciones de hacinamiento y no hay levantamiento de construcción, ni materiales.

Pese a las fotografías y los reportes entregados por las FARC, el Gobierno ha recalcado que de las 20 Zonas Veredales y los 7 Puntos Transitorios de Normalización, son 8 las que están construidas en un 80%.  Le puede interesar: [Zonas Veredales bajo la sombra paramilitar](https://archivo.contagioradio.com/zonas-veredales-bajo-la-sombra-paramilitar/)

Ya varias organizaciones sociales que han acompañado y seguido el tránsito de los diversos frentes de las FARC hacia las Zonas Veredales, lanzaron **un S.O.S por las precarias condiciones en las Zonas que a la fecha, no cuentan con las condiciones mínimas** y necesarias para garantizar la permanencia de los miembros de las FARC en condiciones dignas.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
