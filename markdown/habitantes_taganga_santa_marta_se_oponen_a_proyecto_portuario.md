Title: Habitantes de Taganga se oponen al Proyecto Sociedad Portuaria Las Américas
Date: 2017-06-28 08:18
Category: Ambiente, Voces de la Tierra
Tags: aceite de palma, Santa Marta
Slug: habitantes_taganga_santa_marta_se_oponen_a_proyecto_portuario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Colombia_puerto_America_foto_protesta_tagangueros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [EJAtlas]

###### [28 Jun 2017] 

Sobre la Bahía de Taganga, en Santa Marta, se planea la construcción del primer puerto especializado de gráneles líquidos de la región. La empresa encargada de la realización de estas infraestructuras es la Sociedad Portuaria Las Américas, perteneciente al **Grupo Daabon, con el que se busca fortalecer la industria del aceite de palma** en el departamento del Magdalena, y que tiene preocupados a los habitantes de la zona quienes aseguran que el turismo y la pesca se verán afectados.

De acuerdo con la Agencia Nacional de Infraestructura, el proyecto busca “Ocupar y utilizar en forma temporal y exclusiva las playas, los terrenos de bajamar y zonas marinas accesorias a aquellas o estos, **para la construcción de un puerto de servicio privado,** para cargue de carbón, clinker, cemento, calizas y demás cargas relacionadas con el giro ordinario de los negocios de la empresa, y la operación del mismo”, y especifica que el terminal servirá para transportar gráneles líquidos tales como**, aceites vegetales, bases lubricantes, hidrocarburos y derivados del petróleo, biocombustible, químicos líquidos.**

### La historia 

El Mapa de Justicia Ambiental (Enviromental Justice Atlas, EJATLAS), indica que para el año 2007 la autoridad ambiental local de Santa Marta otorgó la licencia ambiental a la empresa TERLICA para la construcción y operación de un atracadero para insumos líquidos por 5 años.

Un año después, **esa empresa fue responsable del derrame de 89 toneladas de aceite vegetal.** “El daño fue catalogado por el Instituto de Investigaciones Marinas -INVEMAR- como grave, e implicó la muerte de colonias enteras de coral, cambios en la estructura y composición biológica de comunidades algales y afectaciones  sobre el ecosistema del litoral rocoso de la bahía de Taganga reflejado en la muerte de especies animales que habitan este ecosistema”, dice el Mapa. Por esos daños causados, la empresa fue sancionada, sin embargo, a la fecha, esta no ha cumplido con las sanciones impuestas.

Pese a ello, en el 2011 la autoridad ambiental local expidió una nueva resolución en la que cedía la licencia ambiental de TERLICA a la Sociedad Portuaria de las Américas, por un periodo de 20 años, para la construcción del puerto multipropósito. **Finalmente para el 2015, se suscribe un contrato de concesión portuaria entre la Agencia Nacional de Infraestructura y la empresa Sociedad portuaria las Américas**, para la realización del proyecto.

### Las denuncias 

Dicho escenario ha generado protestas sociales y la constitución de veedurías ciudadanas que han iniciado acciones en el ámbito de jurídico, mediático y social para exigir que no se construya el puerto. La anterior experiencia de la comunidad tras el derrame en 2008 no genera confianza en la comunidad.

**"Esta catástrofe ha tenido muchas consecuencias que algunos de nosotros podemos recordar, entre ellas, el mal olor que invadió la bahía por meses,** la muerte casi total de los corales y de algunos animales que viven adentro y de las algas debido a la acidificación del agua por culpa del vertimiento de este aceite (...) Hoy, luego de casi 10 años, el ecosistema no está totalmente regenerado", dice mediante una petición a través de Chege.org, Carlos Andrés Vanegas Silva, integrante de la comunidad.

Aunque la alcaldía y la Sociedad Portuaria aseguran que el proyecto significará progreso para la región, y además no generará impactos ambientales; las comunidades que viven en la bahía rechazan el proyecto alegando que este no ha sido debidamente socializado, y además, se muestran preocupados por las posibles **consecuencias que pueda tener este tipo de infraestructuras en una zona donde sus habitantes viven del turismo y la pesca.**

Carlos Andrés Vanegas asegura también que “el movimiento provocado por las naves entrando en la bahía tendría un claro efecto sobre la población de peces”. Asimismo recuerdan que “**la Sierra Nevada de Santa Marta es considerada como zona de reserva de la biosfera desde 1979. La bahía de Taganga** hace parte de esta zona, sus arrecifes están entre los más lindos del planeta y es un lugar considerado por los indígenas de la sierra nevada, como un lugar sagrado y por lo tanto que hace parte de nuestro patrimonio histórico y presente”.

### Grupo Daabon 

Cabe recordar que le grupo Daabon es uno de los mayores **productores de aceite de palma en Colombia. Un grupo empresarial de la familia Dávila de Santa Marta que ha estado inmersa en el caso de despojo de tierras de la hacienda Las Pavas**. Son 123 familias las que se han enfrentado a las firmas Aportes San Isidro y C. I. Tequendama, que pertenece al Grupo Daabon. De acuerdo con una investigación del sociólogo y periodista Alfredo Molano, dos miembros de ese grupo están implicados en asuntos criminales. "Eduardo Dávila Armenta pagó cárcel por exportación de coca y ahora un fiscal antiterrorismo lo investiga por el presunto delito de concierto para delinquir agravado y nexos con paramilitares de Magdalena. José Domingo debe responder por paramilitarismo como protagonista principal del Pacto de Chivolo, firmado con el cabecilla paramilitar Jorge 40 que lo llevó a la gobernación del Magdalena".

El proyecto portuario en Taganga fue avalado por la Alcaldía, la Dimar, el Ministerio de Transporte, la DIAN y el Ministerio de Comercio exterior, entre otras entidades. Actualmente en la oficina de la Autoridad Nacional de Licencias Ambientales, ANLA, se tramita la licencia correspondiente.

<iframe id="audio_19522360" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19522360_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
