Title: Restringir el acceso a la U. Antioquía afecta los derechos de los Estudiantes
Date: 2017-06-09 16:06
Category: Educación, Nacional
Tags: Movimiento estudiantil, Universidad de Antioquia
Slug: restringir-el-acceso-a-la-u-antioquia-afecta-los-derechos-de-los-estudiantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/udea_02.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Informa] 

###### [09 Jun 2017] 

Estudiantes de la Universidad de Antioquia denunciaron que las medidas que han tomado las directivas de la institución para detener el microtráfico al interior del centro educativo, como pedir el carné y la cédula de los estudiantes, **solo estigmatizan al estudiantado y no acaban con la problemática de fondo de expendio y venta de drogas**.

Las medidas que tomaron las directivas fueron el cierre de la universidad la semana pasada durante tres días, **pedir a los estudiantes que además de presentar el carné al ingresar a la institución enseñen su cédula** y cerrar algunos puestos de ventas de los estudiantes. Le puede interesar: ["8 y 9 de Junio día del estudiante caído"](https://archivo.contagioradio.com/dia-del-estudiante-caido/)

Diego Matta, abogado egresado de la Universidad de Antioquia e integrante del movimiento Digamos, expresó que los estudiantes tienen miedo de que estas acciones lo que pretendan realmente sea hacer un seguimiento y **empadronamiento de los líderes del movimiento estudiantil, que podría terminar en falsos positivos judiciales**.

De igual forma señaló que cuando la Policía ingresó a la Universidad, “se estaba tratado de abrir diferentes lockers de los estudiantes, por fortuna hubo intervención de la Personería de Medellín para hacer la advertencia de la irregularidad e ilegalidad de la medida”. Sin embargo, esta medida, de acuerdo con Matta, **corroboró que no son los estudiantes los que venden las drogas al interior de la institución** porque solo se encontraron algunos gramos de marihuana que son dosis mínimas.

Respecto a los puestos de venta de alimentos que tienen los estudiantes, Matta señala que no se puede criminalizar la actividad puesto que no hay pruebas de que desde esos locales se expendan dorgas, y **por el contrario esta es la única forma de sustento de los universitarios para pagar su matrícula, alimentación y transportes**. Le puede interesar:["Gobierno no resuelve exigencias sobre déficit presupuestal de Educación: ADE"](https://archivo.contagioradio.com/gobierno-no-resuelve-exigencias-sobre-deficit-presupuestal-de-educacion-ade/)

Los estudiantes manifestaron que debería generarse una estrategia mucho más eficiente que parta desde políticas públicas anti drogas y que se desarrollen conforme a las necesidades de cada municipio y localidad en el país. “Aquí directamente se tiene que e**ntrar a intervenir en las estructuras financieras que alimentan el microtráfico**, centrar la atención en ellas y evitar que ingresen a la universidad” afirmó Matta.

<iframe id="audio_19180225" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19180225_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
