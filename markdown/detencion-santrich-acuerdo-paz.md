Title: La detención de Santrich y el Acuerdo de paz
Date: 2018-04-24 17:39
Category: Expreso Libertad
Tags: FARC, presos politicos, santrich
Slug: detencion-santrich-acuerdo-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Santrich.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: EFE 

###### [17 Abr 2018] 

[En esta emisión del Expreso Libertad, hablaremos con el director de la Corporación Jurídica Libertad Benitez y con el abogado de esta misma organización Manuel Garzón, quienes de cerca han seguido la detención de Jesús Santrich y han denunciado incongruencias no solo en la captura, sino en todo el proceso jurídico que hasta el momento se ha llevado a cabo.]

<iframe id="audio_25577822" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25577822_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
