Title: "En Colombia la lucha contra la droga, es contra los campesinos" Cesar Jeréz
Date: 2017-10-13 14:27
Category: Entrevistas, Movilización
Tags: COCCAM, Erradicación Forzada, Lucha contra la droga, Sustitució0n de cultivos, Tumaco
Slug: en-colombia-la-lucha-contra-la-droga-es-contra-los-campesinos-cesar-jerez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/coca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elespectador] 

###### [13 Oct 2017]

Luego de la masacre de Tumaco y las múltiples informaciones sobre las acciones de erradicación forzada de hoja de coca en varias regiones del territorio nacional, queda al descubierto que la política de lucha contra la droga no existe, sino lo que existe es **una lucha contra el campesino que deja intacta la cadena productiva** y no afecta el negocio de los carteles afirma Cesar Jeréz.

Por ello la Asociación de Cultivadores de Coca, Marihuana y Amapola están pensando en impulsar una movilización nacional que presione al gobierno nacional a cumplir con el acuerdo de sustitución de cultivos de uso ilícito presentes en el acuerdo de paz. [Lea también: Así fue la masacre de campesinos en Tumaco](https://archivo.contagioradio.com/9-personas-erradicacion-forzada-tumaco/)

Cesar Jeréz, vocero de la organización de y de la Asociación nacional de Zonas de Reserva Campesina, afirma que el Estado ha sostenido una política que criminaliza al campesino cultivador sin tener en cuenta que, muchas veces el **cultivo de la hoja de coca es la única opción en territorios en que la presencia del Estado es nula**. “Campesino siembra coca porque es la única alternativa y se les sigue persiguiendo”.

### **Las cifras concretas de la productividad de la Coca para un campesino** 

Mientras que a un campesino en cualquier región del país una hectárea de Coca le representa cerca de 4’00.000 al año y la compra se la hacen en la misma finca, mientras que una hectárea de cacao generaría solamente 3’200.000 al año vendiendo a buen precio y sin restar los costos del transporte que se incrementan en los lugares en que hay menos acceso a vías.

### **Los carteles de la droga están en territorios altamente militarizados** 

Otra de las situaciones que se evidencian en los territorios como Tumaco es que la militarización es altísima y por el contrario la presencia del Estado es nula. Esto quiere decir que las fuerzas militares no han actuado de manera eficiente contra los carteles de la droga y por el contrario lo que se evidencia es una alta criminalización de los campesinos que se ven asediados tanto por los carteles como por la Fuerza Pública que señala y criminaliza. [Lea también: Policía actúa de manera criminal en Tumaco](https://archivo.contagioradio.com/policia-sigue-actuando-de-manera-criminal-en-tumaco/)

Esta situación sumada a la doble moral en la que se habla del una lucha contra las drogas por un lado, pero que persigue a los campesinos, afecta mucho más a una población que en varias regiones ha manifestado su interés en hacer sustitución voluntaria, pero que se encuentra con contratos de erradicación y metas impuestas por gobiernos como el de Estados Unidos en los que el gobierno colombiano está comprometido a cumplir, afirma Jeréz.

### **La movilización de los cocaleros es la única salida** 

Con dichas metas establecidas, el gran aparato burocrático y la lentitud en la regulación normativa del acuerdo con las FARC en el congreso de Colombia, la única alternativa que tienen los campesinos cocaleros será la movilización en la que se le exija al gobierno nacional que cumpla con lo acordado y deje de criminalizar y perseguir a los pequeños cultivadores.

Además, Jeréz resalta que es necesario, en el plano electoral que los campesinos y otros sectores sociales se unan en acuerdos programáticos de cara a las elecciones del 2018, como única manera de salir de la crisis política actual y de reformular asuntos estructurales del modelo económico vigente. [Lea también: Plan de sustitución de cultivos debe ser integral o no durará](https://archivo.contagioradio.com/sustitucion-cultivos-ilicitos-cocam/)

Para Jeréz los acuerdos programáticos deben estar basados en la implementación del acuerdo con las FARC, el cumplimiento a las exigencias de la Cumbre Agraria y la implementación y los acuerdos con el ELN, de otra manera las precandidaturas actuales, de la izquierda, serán vencidas nuevamente en un panorama que no parece favorecer las grandes mayorías del país.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
