Title: Federación Nacional Campesina de Paraguay recuperará 300 mil hc de tierras
Date: 2015-03-27 20:36
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Campesinos de Paraguay ocuparán tierras, Campesinos sin tierra Paraguay, secretaria de la Federación nacional de campesinos de Paraguay, Sindicalistas convocan jornada de lucha nacional Paraguay, Teodolina Villalba
Slug: centrales-sindicales-y-campesinos-acuerdan-convocar-huelga-general-en-paraguay
Status: published

###### Foto:Youtube.com 

###### <iframe src="http://www.ivoox.com/player_ek_4274490_2_1.html?data=lZeklpmddI6ZmKiakp6Jd6KpmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdDqytLWx9PYs4zXwtLdx9jNstCfxtOYssbWpcjpwt6Y0cjZtMLmhqigh6aVb9Xdxtffw9iPqc%2Bf1dSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Teodolina Villalba, Secretaria de la Federación Nacional de Campesinos de Paraguay] 

Después de **13 mesas de negociación con el gobierno en un año**, todos los **sindicatos,** centrales obreras y organizaciones sociales de campesinos decidieron **retirarse de la mesa de negociaciones** con el gobierno e iniciar una movilización nacional, una **huelga general y una jornada de ocupación de tierras, ya que según los sindicatos el gobierno no se muestra proclive a negociar.**

El detonante final ha sido la **adjudicación a empresas trasnacionales de la construcción del aeropuerto internacional** y de obras viales de gran importancia para el país. Otro de los puntos del conflicto, es la posible aprobación de una **ley que dotaría de facilidades a los inversores extranjeros**.

Entre las reivindicaciones de los trabajadores está el **aumento salarial de un 25%**, debido a la subida de los precios en bienes de primera necesidad, y la **derogación de la ley de privatizaciones conocida como Ley de Alianza Público-Privada**.

Según **Teodolina Villalba**, **secretaria de la Federación Nacional de Campesinos de Paraguay**, sus principales reivindicaciones son la **defensa de la semilla nacional** frente a la extranjera, el programa de **producción agraria para pequeños productores y** **el reparto de las tierras**, con el fin de que todos los campesinos accedan a ella.

De acuerdo a Villalba, se han **recuperado 200.000 hectáreas** mediante los asentamientos de campesinos, la **ocupación de tierras** y el trabajo institucional realizado por las organizaciones sociales, pero aún son **necesarias 300.000** más para que se **cubran las necesidades de los campesinos**.

El gobierno no dispone de **ningún tipo de ayuda para los pequeños productores** y además les **exige cultivar soja** para vender a las grandes multinacionales.

Un ejemplo de la lucha de los campesinos es la **ocupación de tierras en 2004,** en dos departamentos del país, en la que durante 22 días se resistió, **liberando 8.000 hectáreas ocupadas por un latifundio.**

Teodolina Villalba, afirma que las organizaciones sociales campesinas, que cuentan con el apoyo de la ciudadanía, han **presentado varias propuestas al gobierno y nunca han recibido respuesta,** por ello se **han sumado a la movilización nacional** que  las centrales sindicales convocarán en los próximos días.
