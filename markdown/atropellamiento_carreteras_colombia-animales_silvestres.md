Title: Colombia carece de planes que garanticen la vida de la fauna cuando se desarrolla infraestructura: Plataforma ALTO
Date: 2018-04-29 00:50
Category: Animales, Nacional
Tags: Animales, Natalia Parra. 4G, Plataforma Alto
Slug: atropellamiento_carreteras_colombia-animales_silvestres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Portada-atropellamiento-fauna-e1524980949741.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Camilo Botero] 

###### [29 Abr 2018] 

Los animales continúan siendo víctimas de los proyectos de construcción de vías en Colombia. Múltiples han sido las denuncias en redes sociales donde se publican fotos de animales atropellados en diferentes vías del país, una situación que, de acuerdo con los defensores de los animales, se genera debido a que no existen planes ambientales que garanticen la vida de la fauna silvestre que se afectada por la fractura de los ecosistemas cuando se decide que allí debe pasar una carretera.

"A los ambientalistas y animalistas nos dicen que estamos en contra del desarrollo, pero no es exactamente eso, por supuesto que todas las vías son necesarias, pero la **planeación que se hace para la construcción es absolutamente desconocedora de las necesidades animales y ambientales",** señala la directora de la Plataforma ALTO, Natalia Parra.

### **Se necesitan planes de manejo ambiental ** 

De acuerdo con la líder animalista, en Colombia se carece de planes ambientales que realmente garanticen la vida de los animales. Cuando **de desarrollar infraestructura se habla, en el país más biodiverso del mundo por kilómetro cuadrado,** no se tienen en cuenta planes de manejo ambiental integrales en los que se pacte una estrategia de mitigación y traslado de especies animales y vegetales, además de los pasos y corredores para fauna silvestre.

"Cualquier política que se plantee en Colombia debería tener la condición de proteger la biodiversidad del país", dice la defensora de los animales. Sin embargo, la nación carece de ese tipo de planes en los que no solo se debería contemplar el paso y traslado de la fauna, sino donde también debe tenerse en cuenta la promoción de una conciencia vial y la aplicación de las multas actuales, en vías de proteger a vida de los animales de fauna silvestre, pero también urbana.

### **La fauna más afectada** 

Según explica Parra, aunque los atropellamientos de animales son frecuentes, con las vías nuevas, el número de animales víctimas de este tipo de situaciones se ha incrementado de "manera exagerada". Puntualmente en **Casanare, Meta, Arauca, Cauca, Caquetá y Putumayo, animales como zarigüeyas, chigüiro​s, el zorro perruno, osos hormigueros, serpientes, iguanas, tigrillos,** entre otras especies, son las principales víctimas.

El ganado también se ve afectado por esta problemática, así como los perros y gatos son abandonados en las carreteras, y por cuenta de esa primera forma de maltrato animal, muchas veces terminan atropellados en las vías.

Ante tal situación, las organizaciones animalistas en las regiones han procurado realizar algunas actividades y campañas de consientización vial sobre la vida de los animales, pero es de resaltar que entidades estatales como la **Agencia Nacional de Licencias Ambientales, el Ministerio de Ambiente y las Corporaciones Regionales** tiene toda la responsabilidad frente a ese tema.

"Las instituciones deben tener unos protocolos ante cualquier tipo de proyecto de infraestructura, y allí se debería exigir planes de manejo ambiental integrales, de lo contrario estas entidades ambientales no estarían cumpliendo con su papel", concluye la vocera de la Plataforma ALTO.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
