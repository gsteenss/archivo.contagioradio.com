Title: Boicot mundial a festival alemán financiado por Israel
Date: 2017-08-23 11:10
Category: Onda Palestina
Tags: Apartheid, BDS, Israel, Palestina
Slug: festival-israel-alemania
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Festival.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Boycott From Within @BFW\_IL 

###### 22 Ago 2017 

Una importante avalancha de apoyo al **boicot cultural a Israel** se ha precipitado esta semana luego de que se hiciera público el **financiamiento de la embajada Israelí en Alemania al Pop Kultur Festival**.

**Ocho de los invitados han anunciado su retiro del evento**: la banda de metal finlandesa Oranssi Pazuzu, la artista y académica Annie Goh,el grupo escoces de hip hop Young Fathers, el grupo de rap Sirio Mazzaj, el DJ egipcio Islam Chipsy, el intérprete sirio de música electrónica Hello Psychallepo, el grupo inglés de música experimental Iklan and Law Holt y la cantante Tunecina Emel Mathlouthi.

Este evento, según sus organizadores, es un “festival internacional que entre el **23 y el 25 de agosto** (…) le da la oportunidad a artistas de Berlin y del mundo entero para crear nuevo arte y mostrarlo en una premier mundial en el escenario… el festival permite una preparación intensa y conecta a socios locales e internacionales en el espíritu de la colaboración…. enfatizando en la internacionalidad y la diversidad sexual”.

Sin embargo, activistas que se autodenominan "**Berlin Contra el Lavado Rosa**", en referenia a la [utilización por Israel de la causa LGTB para tratar de ocultar sus crímenes](https://archivo.contagioradio.com/israel-palestina-campana-lgbti/), en un comunicado afirman que “**Cualquier cooperación que permita a un Estado mantener una ocupación ilegal** a costas de un supuesto progresismo (por ejemplo por medio de financiación cultural) **es indefendible**”.

Esta oleada de boicot se da respondiendo al llamado de la **Campaña Palestina por el Boicot Académico y Cultural a Israel**, que desde el 2004 ha dejado claro que todo evento o proyecto llevado a cabo bajo el patrocinio de, o en colaboración con, o con financiación de un organismo oficial israelí es merecedor de boicot. Este principio ha sido **respaldado por las más de 170 organizaciones de la sociedad civil palestina** que desde el 2005 impulsan el BDS.

En esta emisión de Onda Palestina, escuchamos algunas canciones de los artistas que hacen parte del boicot al Pop Kultur Festival, y como siempre nuestras secciones habituales en solidaridad con Palestina desde Colombia.

<iframe id="audio_20487253" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20487253_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
