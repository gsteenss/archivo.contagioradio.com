Title: Celebremos la radio: Mecanismo de resistencia en las comunidades
Date: 2020-02-13 17:48
Author: AdminContagio
Category: Comunidad, eventos
Tags: Día de la Radio, paz
Slug: en-el-dia-de-la-radio-unesco-destaca-la-importancia-del-pluralismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/radios_comunitarias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Archivo de Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde el 2011 se celebra el Día Mundial de la Radio, y la Organización de las Naciones Unidas para la Educación, la Ciencia y la Cultura (Unesco), presentó tres temas destacados sobre este día y la responsabilidad que deben tener quienes intervienen en esta.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Pluralismo en la radio

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Unesco el pluralismo en la radio consiste en el trabajado equitativo entre medios públicos, privados y comunitarios, teniendo en cuenta eso Colombia cuenta con 1.596 emisoras transmiten su señal por medio del espectro radioeléctrico de las bandas FM y AM, de estas 1.243 corresponden a F.M y 353 son AM.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De estas 667 emisoras son comerciales, 626 comunitarias y 303 de interés público, frente a esto la Subdirección de Radiodifusión Sonora del Min TIC, señaló que el medio de comunicación con más difusión en el país en 2019 es la radio con 48,3 millones de colombianos, lo que significa que 99,1 % de los colombianos tienen acceso a la radio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo Mónica Valdes, vicepresidenta de la Asociación Mundial de Radios Comunitarias (Amarc), organización que desde hace 40 años trabaja por la defensa del derecho a la comunicación afirmó que *"la equidad de la radio radica inicialmente en la equidad del espectro electromagnético, donde las diferentes expresiones ciudadanas puedan producir y escuchar contenido de su interés"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que la responsabilidad de esto se divide en tres agentes, primero el Estado que debe asignar espacios equitativos e influyentes, luego la comunidad debe sintonizar y exigir contenido diferentes, y por último los comunicadores radiales deben no solo visibilizar una voz oficial, también un voz secundaria que refleje el sentir, pensar y actuar de las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Afirmó también, *"en tanto no se encuentre un equilibrio y una democratización de los medios va ser muy difícil que espacios como las emisoras comunitarias tengan mayores alcances y visibilización".* (Le puede interesar: <https://archivo.contagioradio.com/docentes-de-universidades-publicas-denuncian-montaje-judicial/>)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Representación

<!-- /wp:heading -->

<!-- wp:paragraph -->

Unesco destacó también la importancia de la **representación,** haciendo referencia a los integrantes de los equipos de trabajo en donde se pueda visibilizar la diversidad social. (Le puede interesar: <https://archivo.contagioradio.com/si-no-nos-matan-nos-judicializan-lideresa-de-arauca/)>

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ello Colombia tiene cerca de 40 emisoras indígenas, campesinas y afros con su propio dial, que muchas veces se tienen que ligar su parrilla de programación a intereses estatales o de aquellos que financien las estaciones de radio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto Valdes agregó que la representación en las cadenas radiales colombianas es muy limitada, teniendo en cuenta que la directores de los principales programas la tienen hombres, dejando en un espacio muy pequeño para mujeres, indígenas o integrantes de la comunidad LGBTI.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *" El espacio en AM o FM se liga a intereses algunos pocos por eso cada vez crecen más las alterativas virtuales, las cuales abren el conocimiento a otras realidades que no son visibilizadas en los medios tradicionales"*
>
> <cite> Mónica Valdes, Amarc </cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Diversidad de contenido editorial

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por último Unesco habló sobre la **diversidad en el contenido** editorial, en donde se refleje la variedad de gustos, intereses e interrogantes de la audiencia, así como las necesidades de las comunidades, en conformidad con sus contextos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ejemplo de ellos es T Radio emisora FM de interés público ubicada en Tausa, Cundinamarca, la cual abrió un espacio dedicado para los campesinos y campesinas habitantes de los páramos en el programa *Páramos con Sumercé*, un programa hechos por los mismos habitantes, donde hablan de empoderamiento, agricultura, derechos humanos y ambientales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto Valdes afirmó, *"la radio debe ser un canal que permita la diversidad de pensamientos, de expresión de las comunidades y de difusión de distintos conocimientos, donde no se necesite pagar para poder dialogar, y que tampoco se requiera de un funcionario público par poder saber que está pasando con en las comunidades".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Hacer radio para la paz

<!-- /wp:heading -->

<!-- wp:paragraph -->

Voceros de diferentes emisoras regionales en Colombia, ante este día afirmaron el papel de las emisoras:

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"la importancia de la radio desde un territorio de gente humilde y trabajadora es de vital importancia, ya que desde esta se logra informar y educar a muchas familias en el contexto de la paz"*.
>
> <cite> Jhoanna Delgado, Integrante de la emisora Ecos del Caguán. </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Mónica Valdes, concluyó diciendo que la radio es de fácil acceso, y ante el contexto actual se debe trabar de manera conjunta con el sistema de verdad, justicia y reparación, *"el Gobierno deben encontrar en la radio un espacio de difusión permanente, un canal para reiterar los mensajes de lo que se quiere construir sobre el proceso que estamos viviendo de paz"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asímismo agregó que, *"no siempre en una nota televisiva o escrita se puede visibilizar el día a día del sistema, entonces la radio juega un rol importante para crear un canal de acercamiento mas directo a todo lo que está pasando en este periodo de transición y lo que implica la paz completa"*.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
