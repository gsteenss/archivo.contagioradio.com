Title: Camilo Torres Restrepo, símbolo de unidad popular y amor eficaz
Date: 2016-02-15 15:10
Category: Otra Mirada
Tags: camilo torres, Carmen de Chucurí, Patio Celemento, San Vicente de Chucurí
Slug: camilo-torres-amor-eficaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/d511b164-bc2b-459e-954e-d3d6c6319e7d.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_10444925_2_1.html?data=kpWhlpmddpahhpywj5aYaZS1lpeah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Z0ZC4s9PmxtiYtMrXuNPZ0dSSlKiPt4a3lIquptLGs83jjMnSjdrSrcXVxZDd0dXZsMLmjN6Yw9KRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [15 Feb 2016] 

A las 6 de la mañana jóvenes, estudiantes, líderes sociales, intelectuales, políticos y defensores derechos humanos, iniciaron su recorrido hacia el municipio del Carmen de Chucurí, con el objetivo de llegar a la vereda  Patio Cemento, lugar en el que fue alcanzado por una bala Camilo Torres Restrepo, sacerdote revolucionario que habló sobre el amor eficaz, y que se unió a las filas del Ejército de Liberación Nacional como último recurso después de haber intentado cambiar el país atravesando todos los caminos, y dándose cuenta de que la dirigencia colombiana no estaba dispuesta a entregar el poder por la vía pacífica.

**"Para cambiar las estructuras en favor de las mayorías, creo que es un sacrificio que vale la pena, pero seguramente eso va a ser deformado, se le va a atribuir toda clase de intensiones mezquinas**... y yo tengo la conciencia de que sigo siendo sacerdote, lo que me hace fundamentalmente sacerdote, es hacer que mis hermanos se amen entre sí de una forma eficaz y yo creo que la eficacia del amor no se logra sino con la revolución", dijo Camilo, en un discurso días antes de hacer parte del ELN.

Camilo Torres sigue vivo en el movimiento social colombiano, pese a las múltiples diferencias  que en éste puedan existir. Y es por eso que al cumplirse 50 años de su muerte, aproximadamente 700 personas decidieron desplazarse hacia El Carmen de Chucurí, para homenajear a Camilo. Días atrás esta conmemoración ya era objeto de amenazas e intentos de saboteo, pues dirigentes políticos de la región y periodistas habrían promovido la estigmatización de esta peregrinación, asociándola directamente con una actividad organizada por la guerrilla del ELN.

“Sabemos de las dificultades que tenemos, las dos últimas semana el grado de señalamientos y estigmatización ha sido muy fuerte, creando en la zona preocupación y zozobra en los campesinos. **Nos hemos propuesto evitar una confrontación, esta celebración está también dentro de la estrategia de la paz en Colombia y la reconciliación”**, explicó Hernando Hernández, exdirigente de la Unión Sindical Obrera y uno de los organizadores del evento.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/20160214_092624.jpg){.aligncenter .size-full .wp-image-20375 width="3264" height="1836"}

Aunque trataba de una homilía en la que claveles blancos dibujaban símbolos de paz, el camino hacia El Carmen de Chucurí, tuvo varios obstáculos para los seguidores de Camilo Torres. Siete árboles fueron tumbados en la carretera para bloquear el paso de las camionetas y los buses que se dirigían a Patio cemento. Con tachuelas pincharon uno de los carros en los que se desplazaban algunos líderes sociales. Pero esto no fue impedimento para hacer un homenaje a la persona que dedicó su vida al amor por el prójimo.

**“Nosotros siempre fuimos claros en expresar que no íbamos a hacer un homenaje al cura guerrillero, por sus tres meses y algo que duró en la guerrilla. Nosotros vamos a reivindicar al sacerdote, al sociólogo, al profesor, al ser humano** que durante sus 11 años en el sacerdocio lo llevó a articularse con los sectores populares, con los campesinos y con los estudiantes, siempre fuimos claros en ese tema y así se expresó a los 400 campesinos en el consejo de seguridad”, dijo Hernández.

[![Amor eficaz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Imagen1.png){.aligncenter .size-full .wp-image-20345 width="669" height="500"}](https://archivo.contagioradio.com/denuncian-persecusion-y-estigmatizacion-a-peregrinacion-por-el-amor-eficaz/imagen1/)

Este tipo de imprevistos durante el camino, causaron la indignación de los integrantes de las organizaciones sociales que desde diferentes puntos del país, se desplazaron hasta el departamento de Santander, para resaltar el trabajo entregado de Camilo Torres. Muchos, incluso pobladores de la zona que hacían parte de la peregrinación, se preguntaban por qué tanta represión contra un acto religioso, y no, contra las compañías que ingresaron fácilmente a esos territorios a explotar carbón, generando un daño ambiental invaluable, **”siento rabia, cuando aquí se metió la minería no hicieron esto, aquí la minería ha arrastrado gran parte del Carmen y eso sino la atacaron, ahora nos vienen a atacar a nosotros que vinimos hacer una homilía al padre Camilo Torres”.**

![20160214\_104827](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/20160214_104827.jpg){.aligncenter .size-full .wp-image-20376 width="3264" height="1836"}

La peregrinación empezó de la voz del sacerdote y defensor de derechos humanos, el Padre Javier Giraldo, quien presidió la peregrinación junto a los congresistas Iván Cepeda, Ángela María Robledo y Alberto Castilla, y al paso también de Gustavo Pérez Ramírez, sociólogo de Lovaina y compañero de Camilo.

“Vamos a avanzar hasta donde nos permite la fuerza, desafortunadamente parece que no vamos a poder llegar hasta donde queríamos, impedidos por fuerza mayor, pues no es nuestra voluntad” expresó el Padre Javier Giraldo, para dar inicio a la peregrinación.

Para no correr riesgos y evidenciar que se trataba de un acto de paz, la peregrinación decidió no llegar hasta el lugar donde murió Camilo Torres y **pese a las altas temperatura, bajo el rayo del sol, en medio de una carretera destapada y frente a un escuadrón del ESMAD, se realizó la homilía para honrar el legado del sacerdote**, recordando cómo fue la muerte de Camilo.

![20160214\_111618](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/20160214_111618.jpg){.aligncenter .size-full .wp-image-20377 width="3264" height="1836"}

“Eran las 7:30 de la mañana del 15 de febrero de 1966, bajando del cerro de los andes, cerca del corregimiento del centenario, donde tuvo lugar ese enfrentamiento bélico, en el que participó la batería 120 del Batallón Bogotá adscrito a la 5 brigada del Ejército de Liberación Nacional, en ese escenario, terminó la vida del padre Camilo Torres… 12 días antes Camilo había cumplido 37 años de edad y 4 meses antes había tomado la decisión  de sumarse a la insurgencia armada… afirman que luego de la orden de retirada Camilo se acercó al lugar donde estaban los cuerpos de los militares caídos, un soldado relató que Camilo no huyó cuando lo hicieron sus compañeros combatientes, sino que con la mirada fija en el firmamento se **fue acercando lentamente al sitio donde estaban los muertos y heridos militares en una actitud que ciertamente para este soldado no era de combate y que él interpretó como un gesto riesgoso de querer administrar los últimos sacramentos a los caídos en ese momento, alcanzado por un disparo** que hizo uno de los militares escondidos entre el follaje”, relató el Padre Javier Giraldo, quien se encontraba junto al Padre Alberto Franco, celebrando el acto espiritual.

Con los mensajes de Camilo Torres a campesinos, sindicalistas, mujeres, estudiantes, y otros actores sociales, se rememoraron sus palabras de esperanza y sed de revolución, de transformación de un país que cada vez más refleja las palabras de Camilo frente a un Estado que no quiere entregar el poder, y donde los pobres son cada vez más pobres y los ricos más ricos. Con cantos y arengas se acompañó la celebración.

Tumultos de claveles blancos formaron la oración “Camilo vive”, mientras los sacerdotes y defensores de derechos humanos presidian la eucaristía.

**“A nombre de la memoria de Camilo Torres, gracias por su amor eficaz”,** dijo Carlos Medina, integrante del Centro de Pensamiento y Seguimiento a los Diálogo de Paz de la Universidad Nacional, quien despidió de los asistentes a esta jornada, agradeciéndoles ante todo, que se logró demostrar que se trataba de una actividad que contribuye a la paz, como a su vez contribuye a esta, no olvidar a aquellos que dieron su vida luchando por un país mejor para todas y todos, como lo hizo Camilo Torres Restrepo.

![d511b164-bc2b-459e-954e-d3d6c6319e7d](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/d511b164-bc2b-459e-954e-d3d6c6319e7d.jpg){.aligncenter .size-full .wp-image-20381 width="951" height="535"}
