Title: Nueva derrota en Jericó para AngloGold Ashanti
Date: 2019-06-05 16:50
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Anglogold Ashanti, Consejo de Estado, Jericó
Slug: nueva-derrota-en-jerico-para-anglogold-ashanti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/33081661_10155900278132772_7763543078492700672_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

En una derrota para la multinacional AngloGold Ashanti, **el Juzgado 15 administrativo de Medellín decidió negarle la solicitud que interpuso la compañía buscando suspender un acto administrativo y Acuerdo municipal**, por medio de los cuales se prohibió la minería de metales en Jericó, Antioquia.

Según el fallo, la empresa pidió que se protegiera "de manera temporal e inmediata el ejercicio de la actividad legítima de la minería desarrollada". Cabe recordar que **AngloGold Ashanti busca comenzar la construcción de una mina de cobre, conocida como La Quebradona**, en Jericó para el año 2020. (Le puede interesar: "[Anglo Gold Ashanti desconoce acuerdo municipal que prohíbe la minería](https://archivo.contagioradio.com/anglo-gold-ashanti-desconoce-acuerdo-municipal-que-prohibe-la-mineria/)")

Sin embargo, **un Acuerdo municipal que fue aprobado el año pasado y luego fue respaldado por un acto administrativo del pasado mes de enero, han logrado oponerse al desarrollo de este proyecto**. La multinacional argumentó en su demanda que estas decisiones municipales implican graves consecuencias económicas para la empresa y sus trabajadores.

Además, sostuvo que la administración y el concejo municipal asumió "competencias de las que carece para prohibir en su jurisdicción en forma absoluta las actividades mineras por ser de interés nacional, requiriendo de estudios técnicos y una adecuada participación y concertación con las instancias gubernamentales correspondientes".

Al respecto, **el juez reconoció los argumentos que dieron el municipio para prohibir la minería, entre ellos, los graves impactos ambientales y sociales que ocasionarían estas actividades.** Además, resaltó el juicio del Consejo de Estado, por medio del cual determinó la posibilidad constitucional que ostentan los Concejos Municipales para dictar las normas necesarias para el control, la preservación y defensa del patrimonio ecológico y cultural del municipio.

[Según José Fernando Jaramillo, coordinador de la Mesa Ambiental de Jericó, **el municipio sigue a la espera de que se pronuncie el Tribunal Administrativo de Antioquia sobre la validez del acuerdo mencionado.** Además, indicó que el Juzgado 33 administrativo de Medellín también se encuentra revisando un demanda del Ministerio de Minas y Energía sobre esta mismo tema.]

El coordinador aseguró que la decisión del juzgado 15 generó una precedente importante para Jericó y el resto del país porque **reconoce el artículo 313 de la Constitución, que le da la facultad a los municipios para proteger sus patrimonios ecológicos y culturales**.

<iframe id="audio_36743207" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36743207_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
