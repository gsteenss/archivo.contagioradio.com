Title: "Es indignante que el Gobierno no tome enserio el paro"
Date: 2016-08-19 13:25
Category: Movilización, Nacional
Tags: Abandono estatal Chocó, Paro en Chocó, Paro en Quibdó
Slug: es-indignante-que-el-gobierno-no-tome-enserio-el-paro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Paro-cívico-Chocó..jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

###### [19 Ago 2016 ] 

Organizaciones sociales, líderes y participantes del paro pacífico que ya completa tres días en Quibdó, Chocó denuncian que este jueves, miembros del ESMAD los atacaron, provocaron y atropellaron, usando desmedidamente la fuerza e incumpliendo con su deber constitucional. **En el hecho resultaron 3 personas heridas** que están fuera de peligro, según afirma el poblador Emilio Pertúz.

De acuerdo con Pertúz desde hacía más de dos meses, diversos sectores sociales venían concertando la movilización pacífica y habían acordado no abrir oficinas ni locales comerciales; sin embargo, algunos negocios decidieron abrir sus puertas y cuando varios de los líderes intentaban dialogar con los comerciantes, **el ESMAD llegó a la zona y los atropelló**.

Pertúz agrega que para la población es indignante que el Gobierno no haya tomado enserio el paro y haya enviado a las reuniones **funcionarios sin capacidad de decisión y sin propuestas consolidadas**, por lo que aseguran que [[el paro continuará](https://archivo.contagioradio.com/levantaremos-el-paro-cuando-hayan-compromisos-serios-pueblo-chocoano/)] porque pese a que han interpuesto demandas, tutelas y derechos de petición, e incluso entidades como la Defensoría han documentado la crisis humanitaria del departamento, los incumplimientos persisten.

La población exige adecuación de vías, porque las únicas dos que tiene el municipio están sin pavimentar; reforma estructural del sector de la salud y de servicios públicos, porque la **intervención del Gobierno nacional sólo ha servido para aumentar la deuda y liquidar uno de los hospitales**, y no para garantizar que se cuente con energía eléctrica permanente y un sistema de alcantarillado. Demandan también una política de producción para el departamento y fortalecimiento institucional.

<iframe src="http://co.ivoox.com/es/player_ej_12601029_2_1.html?data=kpejkpaUdpqhhpywj5eWaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncabhytHW0ZC0qdPohqigh6elvo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
