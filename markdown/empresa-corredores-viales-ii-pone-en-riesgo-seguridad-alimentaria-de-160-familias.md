Title: Empresa Corredores Viales II tiene en riesgo seguridad alimentaria de 160 familias
Date: 2015-05-27 13:21
Category: Ambiente, Comunidad, Nacional
Tags: Cauca, comunidades afro, Consejería de Derechos Humanos de la presidencia, contaminación de ríos, Corredores Viales II, Defensoría del Pueblo, Dirección de Consulta Previa, Galindez, Ministerio del Interior, Palo Verde, Pilón, Presidencia de la República
Slug: empresa-corredores-viales-ii-pone-en-riesgo-seguridad-alimentaria-de-160-familias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/unnamed-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Consejo Comunitario Corredor Panamericano el Pilón 

<iframe src="http://www.ivoox.com/player_ek_4558256_2_1.html?data=lZqimpeZeo6ZmKiak5uJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic7k08rgw5Cns9Pmxsnc1MrXb7fdwtHS1ZCtjYzk0NPSjcrSb9PdxtjU0ZDXqcjp087Rw8mPpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Eduardo González, Consejo Comunitario Corredor Panamericano el Pilón] 

###### [27 de May 2015]

La empresa **Corredores Viales II, ha generado desde hace 10 años una grave situación de contaminación** que ha traído como consecuencia impactos sociales, ambientales y económicos para las comunidades negras que habitan cerca el río San Jorge, en el departamento del Cauca.

Aproximadamente **160 familias del Pilón, Galindez y Palo Verde,** se estarían viendo afectadas por las actividades de extracción de material de río que viene desarrollando la empresa. Sin embargo, de acuerdo a Eduardo González, integrante del Consejo Comunitario Corredor Panamericano el Pilón, "con una extracción tan severa, se afecta a  la población de la parte alta del río", teniendo en cuenta que son muy pocos los meses del año que se detiene la extracción de material.

Según Eduardo González, la compañía llegó en el año 2005, con el objetivo de explotar el material de arrastre del río, **sin cumplir el proceso de consulta previa de las comunidades negras**, desconociendo que había un grupo de asociación de areneros que hacían uso de esa mina de río, por lo que se afecta aproximadamente a 30 familias que vivían de esa actividad.

Dos años después de haber empezado a explotar el río, **54 familias perdieron sus predios donde tenían cultivos de pancoger** que eran la base de su economía alimentaria. Gonzáles, resalta que a la fecha el daño no ha sido reparado, y, en cambio, esto generó **desempleo y  desplazamiento de varios pobladores.**

Por otro lado, la comunidad de Palo Verde, donde hay **40 familias que deben tomar el agua directamente del río** ya que no cuentan con un acueducto, se han visto afectadas porque las máquinas ingresan a las aguas, contaminando la fuente hídrica con los combustibles y grasas de las retroexcavadoras; es decir, que las personas están **consumiendo agua contaminada.**

Además, en febrero de este año, la empresa estuvo reparando el puente del río, y, al no tomarse las medidas necesarias, la pintura que se aplicó, cayó al afluente de forma directa, causando **graves consecuencias en la fauna acuática,** generando un daño ambiental y de salud para la población que consume los peces, como lo afirma Eduardo.

Incluso las comunidades denuncian que la empresa ha **desviado el río unos 30 metros** para continuar con sus actividades de extracción.

Desde noviembre del año 2014, se han enviado oficios al **Ministerio del Interior en cabeza de la Dirección de Consulta Previa**, también se ha llamado la atención de la Defensoría del Pueblo, la Alta Consejería para los Derechos humanos y hasta la **Presidencia de la República**, con el  objetivo de solicitar la consulta previa a la cual tienen derecho las comunidades.

Aunque se han realizado todos los trámites necesarios, según el vocero comunitario, **no se ha tenido una respuesta clara y contundente;** lo único que se dijo, fue que iban a enviar profesionales de la Dirección de Consulta Previa en marzo para evaluar el caso y verificar la existencia de comunidades afrocolombianas. Pese a eso, hasta el momento no se ha llevado a cabo esta visita.

Cabe resaltar que de acuerdo a González, las comunidades se encuentran a unos 200 o 300 metros del río, lo que se usó como excusa para decir que allí no se afectaría ninguna población, **negando la existencia de las comunidades negras,** quienes tienen en riesgo  su seguridad alimentaria.

[![unnamed (4)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/unnamed-4.jpg){.size-full .wp-image-9248 .aligncenter width="673" height="505"}](https://archivo.contagioradio.com/empresa-corredores-viales-ii-pone-en-riesgo-seguridad-alimentaria-de-160-familias/unnamed-4/) [![unnamed (3)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/unnamed-3.jpg){.size-full .wp-image-9249 .aligncenter width="673" height="505"}](https://archivo.contagioradio.com/empresa-corredores-viales-ii-pone-en-riesgo-seguridad-alimentaria-de-160-familias/unnamed-3-2/) [![unnamed (2)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/unnamed-2.jpg){.size-full .wp-image-9250 .aligncenter width="673" height="505"}](https://archivo.contagioradio.com/empresa-corredores-viales-ii-pone-en-riesgo-seguridad-alimentaria-de-160-familias/unnamed-2-2/) [![unnamed](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/unnamed.jpg){.size-full .wp-image-9252 .alignnone width="673" height="505"}](https://archivo.contagioradio.com/empresa-corredores-viales-ii-pone-en-riesgo-seguridad-alimentaria-de-160-familias/unnamed-5/)
