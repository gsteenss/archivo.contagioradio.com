Title: La democracia en las universidades debe ser una exigencia de todo el país
Date: 2015-03-25 21:47
Author: CtgAdm
Category: Educación, Movilización
Tags: educacion, mane, mario hernandez, Mesa Amplia Nacional Estudiantil, Universidad Nacional, Universidad pública
Slug: la-democracia-en-las-universidades-debe-ser-una-exigencia-de-todo-el-pa
Status: published

Escrito por: MESA AMPLIA NACIONAL ESTUDIANTIL

<div id="post-body-1980823187336296110">

El pasado 18 de marzo se llevó a cabo la consulta para la designación de Rector de la Universidad Nacional de Colombia. En ella el profesor Mario Hernández, abanderando un programa construido por estudiantes, trabajadores y profesores defendiendo el carácter nacional, público y estatal de la UN, ganó la consulta con una participación histórica.

Por la normatividad establecida, queda en manos del Consejo Superior, CSU, designar al rector para los próximos 3 años. Este órgano compuesto por tan solo nueve (9) miembros, controlado directa e indirectamente por el gobierno nacional, que tiene la potestad de definir las directivas y el rumbo de la Universidad sesionará este miércoles 25 de marzo. Mecanismo perverso con el cual se ha hecho caso omiso de la voz de la comunidad universitaria históricamente y le ha permitido al gobierno avanzar en la política de autofinanciación y disminución de la calidad académica en las universidades del país y en la Universidad Nacional de Colombia, en particular.

Por esta razón, invitamos a la sociedad colombiana a exigirle al Gobierno Nacional no sólo que respete la decisión de la comunidad universitaria de la UN, sino que se establezcan mecanismos reales de participación en los claustros educativos como mecanismo para fundar la democracia en nuestro país. Como quedó consignado en el borrador de ley que construyó el movimiento estudiantil en 2012, la Democracia y el Cogobierno son base de la autonomía y son entendidos como “la participación activa y decisoria de las comunidades educativas para definir el rumbo de las IES en todos sus aspectos y a través del ejercicio democrático al interior de las mismas en sus múltiples escenarios.”

Está en juego si se respeta la voluntad de la comunidad universitaria o se ratifica la política que ha venido primando al interior del Alma Máter y que tiene a la universidad de los colombianos atravesando la peor crisis de su historia. Es por ello que invitamos a todos los y las estudiantes a alzar las voces en alto para hacer de la Democracia una realidad de la vida universitaria.

Por un país con soberanía, democracia y paz.

</div>

<div id="post-body-1980823187336296110" class="post-body entry-content">

</div>
