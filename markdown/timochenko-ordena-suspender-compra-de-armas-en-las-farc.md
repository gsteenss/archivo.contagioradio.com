Title: Timochenko ordena suspender compra de armas y municiones
Date: 2015-11-10 14:41
Category: Nacional, Paz
Tags: acuerdo de paz, Cese al fuego, cese bilateral, Diálogos de La Habana, FARC-EP
Slug: timochenko-ordena-suspender-compra-de-armas-en-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/las-noticiascartagena.com_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:lasnoticiascartagena.com 

###### 10 nov 2015 

El jefe máximo de las FARC- EP Timoleon Jiménez, alias "Timochenko" indicó en su cuenta de Twitter que el 30 de septiembre **dio la orden al interior de las filas de las FARC de suspender la compra de armas y municiones** en las diferentes estructuras guerrilleras, esto como muestra de detener las hostilidades en medio de los diálogos.

[![trino timo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/trino-timo.png){.size-full .wp-image-16956 .aligncenter width="624" height="208"}](https://archivo.contagioradio.com/timochenko-ordena-suspender-compra-de-armas-en-las-farc/trino-timo/)

El trino publicado la mañana de este martes, se suma a la orden impartida el 1 de octubre de detener los entrenamientos militares a los guerrilleros y realizar formación política, decisión que demostraría la intención de las FARC de avanzar en el desescalamiento del conflicto.

El mensaje de Timoleón Jiménez apunta a **que el proceso se de en medio de una “tregua bilateral previa” como paso gradual al cese definitivo de fuego y hostilidades**, tras advertir que existen operativos militares que ponen en riesgo el cese unilateral y en consecuencia al proceso de paz.
