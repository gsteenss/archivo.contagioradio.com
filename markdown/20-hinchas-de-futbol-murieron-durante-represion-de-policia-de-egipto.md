Title: 20 hinchas de futbol murieron durante represión de policía de Egipto
Date: 2015-02-09 20:52
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: 30 hinchas muertos, Egipto, enfrentamientos hinchas, Zamalek
Slug: 20-hinchas-de-futbol-murieron-durante-represion-de-policia-de-egipto
Status: published

###### Foto:Reuters 

Al menos **20 personas fallecieron en Egipto ayer en enfrentamientos con la policía antidisturbios al intentar entrar en el estadio de fútbol** donde se iba a disputar el partido de fútbol entre los equipos Zamalek y Enppi. En los hechos también resultaron heridas otras 30 personas.

Los fuertes disturbios **recuerdan lo sucedido el 1 de febrero de 2012 cuando en una situación similar murieron 74 hinchas. **Desde que comenzó la **revuelta egipcia** y el derrocamiento de Hosni Mubarak **la policía ya ha asesinado en protestas a más de 3.000 personas.**

Lo indignante del hecho, además de los asesinatos y del record de masacres perpetradas por la policía anitdisturbios de Egipto, es que después de la represión y las muertes el partido jugó en el propio estadio.

Ante los hechos, el **primer ministro egipcio** ha decidido **sacar al país de la Premier League** hasta que se esclarezcan los hechos y se produzca una investigación judicial.

Por su parte el presidente del equipo **Zamalek acusó a los Hermanos musulmanes de estar detrás de los enfrentamientos** y además de ser una estrategia de carácter terrorista.
