Title: 1.641 días de diálogos para poner fin a 52 años de guerra en Colombia
Date: 2016-09-26 15:39
Category: Otra Mirada, Paz
Tags: FARC, Gobierno, paz, proceso de paz, víctimas
Slug: 1-641-dias-de-dialogos-para-poner-fin-a-52-anos-de-guerra-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Paz-e1472858979891.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### [26 Sep 2016] 

Tras 52 años de guerra en Colombia, hoy se firma el acuerdo final de paz con la guerrilla de las FARC. Y aunque muchos y muchas se indignan con las cifras monetarias que serán necesarias invertir para que los guerrilleros se reincorporen a la vida civil que junto a los costos del plebiscito, suman un total aproximado de \$460 mil millones de pesos, lo cierto es que las cifras de víctimas de la guerra junto a los recursos diarios para combatir a la guerrilla evidencian que es más conveniente invertir en la paz.

El costo total para financiar la guerra en Colombia, **durante los últimos 52 años, ha sido de aproximadamente 179.000 millones de dólares,** según la investigación realizada por el ex viceministro de minas, Diego Otero. Un monto que posiciona a la nación en la lista de los 10 países que más invierten en la guerra, y que implica que la cifra promedio de gasto al día en guerra entre 1964-2016 fue de \$22 mil millones de pesos.

Pero el número de víctimas es mucho más alarmante. **La guerra ha dejado en Colombia 267.162 muertos.** De acuerdo con el más reciente informe de la Unidad de víctimas, más de 90 mil personas fueron desaparecidas, hubo más de 21 mil secuestros, 130 mil personas víctimas de amenazas, casi 55.000 víctimas de algún tipo de acto terrorista, 7 mil niños y niñas reclutados forzadamente y más de 4 mil casos de violencia sexual.

Por su parte, de acuerdo con el informe ‘Basta ya’, del Centro Nacional de Memoria Histórica, a las FARC  se le atribuyen la mayoría de los 24.482 secuestros, 3.899 asesinatos selectivos y 343 masacres. Además desde 1988 al 2012, se registraron  10.189 víctimas de minas antipersona, de las cuales 2.119 resultaron muertos y 8.070 heridos.

Según el mismo informe, los paramilitares son los responsables de **1.166 masacres, la fuerza Pública de 158, y los grupos paramilitares en conjunto con miembros de la fuerza pública con responsables de 20. ** Lo que dejó al Carmen de Bolívar, Turbo, Apartadó, Remedios, Medellín, Buenaventura, Ciénaga, Barrancabermeja, Cúcuta, Tierra Alta, Valledupar, Agustín Codazzi y Tibú como los municipios más afectados por la guerra.

Fueron 1.641 días de diálogos entre el gobierno y la guerrilla de las FARC, para llegar a este 26 de septiembre, día en que este acuerdo final termina con el conflicto armado colombiano que ha dejado un total de **7.210.949 personas en condición de desplazamiento y un total de 8.349.484 de víctimas.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
