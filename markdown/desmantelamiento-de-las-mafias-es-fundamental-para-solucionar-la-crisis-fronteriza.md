Title: Desmantelar las mafias, fundamental para solucionar la crisis fronteriza
Date: 2015-09-22 18:03
Category: El mundo, Nacional
Tags: Derechos Humanos, Juan Manuel Santos, Paramilitarismo, Quito, Rafel Correa, Reunión Maduro y Santos, unasur, Venezuela
Slug: desmantelamiento-de-las-mafias-es-fundamental-para-solucionar-la-crisis-fronteriza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Ecuador-Maduro-SAntos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: globovision.com 

<iframe src="http://www.ivoox.com/player_ek_8552779_2_1.html?data=mZqilJybfY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRiMbnzsbb1srQpdOfzcbgjdLFqsrV1IqfpZDKuc%2FYwtLS0NnFsIzkwtfOjdjTsNbXytTbw9ePsMKfjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Cesar Torres del Río, analista internacional] 

###### [22 sep 2015]

La reunión que se realizó en Quito para normalizar la situación de frontera entre **Colombia y Venezuela** definió puntos concretos para trabajar entre los dos gobiernos y [normalizar la situación de frontera](https://archivo.contagioradio.com/hemos-demostrado-que-con-voluntad-politica-si-se-puede/), sin embargo, estos consensos podrían verse afectados **si no se solucionan problemas estructurales de fondo,** como el paramilitarismo, señala Cesar Torres.

Estos acuerdos en la visión del analista están trazados a mediano y largo plazo, pero podrían afectarse si no se resuelven los problemas de violencia tanto paramilitar, como de mafias en la frontera “*mientras sigan mafias existiendo, se demuestra la inexistencia parcial del Estado,  es por esto que hay que insistir en el no monopolio de las armas en la frontera*”, indicó Torres.

Frente a la **apertura de la frontera** el analista internacional dice que “*podría haber una apertura parcial en los meses que restan del año*”, ya que este **cierre afecta a ambas naciones**, aunque los colombianos se han visto más afectados.

**Por el cierre de la frontera y la deportación de colombianos han retornado 19.686 personas**, y ha generado pérdidas económicas estimadas en **400.000 dólares diarios para las empresas colombianas en su mayoría.**
