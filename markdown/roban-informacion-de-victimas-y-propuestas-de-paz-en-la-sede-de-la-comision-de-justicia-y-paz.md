Title: Roban información de víctimas en sede de la Comisión de Justicia y Paz
Date: 2016-06-13 13:16
Category: DDHH, Nacional
Tags: Conpaz, Conversacioines de paz en Colombia, Jurisdicción Especial de Paz, Justicia y Paz
Slug: roban-informacion-de-victimas-y-propuestas-de-paz-en-la-sede-de-la-comision-de-justicia-y-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/robo-informacion-justiciaypaz-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: comisión de justicia y paz] 

###### [13 Jun 2016] 

En la mañana de este lunes se confirmó el robo de información sobre víctimas, procesos de defensa de las fuentes hídricas y de propuestas de paz en la sede de la Comisión Intereclesial de justicia en Paz en la ciudad de Popayan. Esta organización que acompaña comunidades víctimas y quien trabaja temas ambientales y de derechos humanos. Según Danilo Rueda, integrante de la organización, **el robo se perpetró entre el sábado y el domingo.**

En el hurto la organización perdió dos computadores con información sensible en temas ambientales, uno de ellos investigaciones que desarrolla en  La Plata Huila, **bases de datos territoriales trabajada junto con las** [**comunidades frente al derecho al agua** y la afección de operaciones extractivas](https://archivo.contagioradio.com/?s=inza); igualmente información que han trabajado para presentar propuestas de paz en las actuales negociaciones de paz con las guerrillas colombianas.

Danilo Rueda, integrante de la [Comisión de justicia y Paz](https://archivo.contagioradio.com/?s=comision+de+justicia+y+paz), resalta que **no se puede hablar de un asalto normal, ya que artículos de mayor valor económico no fueron hurtados mientras que si se llevaron facturas y recibos de compras de la organización**, bases de datos de derechos humanos e información proyectada para la Comisión de Esclarecimiento de la verdad y para la Jurisdicción Especial para la Paz fue robada.

El personal que ejerce su trabajo desde esta sede trabajó durante la semana anterior con las comunidades en la [Cátedra Sujetos Territoriales en el río Naya](https://archivo.contagioradio.com/?s=catedra), actividad [pedagógica](https://archivo.contagioradio.com/en-el-naya-se-inaugura-la-primera-sede-de-la-universidad-de-la-paz/)desarrollada por la Comisión de Justicia y Paz, **además en el ingreso a la sede no se forzaron las puertas de la sede.**

<iframe src="http://co.ivoox.com/es/player_ej_11885903_2_1.html?data=kpalmpqddJShhpywj5WaaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5yncaXVz87Z0ZC2ucbYwpCaja_Zt9XdxM7Ojd6PlMLujKjcztTRpsrVjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
