Title: Más de 30.000 indígenas acuerdan reactivar la Minga de 2008
Date: 2015-09-18 12:22
Category: DDHH, Movilización, Nacional
Tags: ACIN, CRIC, Desprestigio mediático, Detención de Feliciano Valencia, falsos positivos, Justicia Especial Indígena, Justicia ordinaria en Colombia, La María, Piendamó
Slug: mas-de-30-000-indigenas-acuerdan-reactivar-la-minga-de-2008
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/ONIC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: cms.onic.org.co] 

<iframe src="http://www.ivoox.com/player_ek_8459218_2_1.html?data=mZmim5eVfI6ZmKiakp2Jd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkYa3lIquk9iPqMaflJWbkpWUb8rixYqwlYqliMjZz8bgjcbHucbmxcbbjdfJpcToytvO1JDQpYzBytPUw5DIcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Emilsen Paz, Consejera Mayor del CRIC] 

###### [18 Sept 2015 ] 

[Luego de haber sesionado, la **junta directiva extraordinaria de autoridades indígenas** y líderes de movimientos sociales participantes de la Minga de 2008, en asamblea permanente, **declaró que la captura de Feliciano Valencia es un secuestro por parte de la justicia ordinaria colombiana**.]

[Emilsen Paz, Consejera Mayor del CRIC, asegura que **se decidió en la asamblea la reactivación de la Minga Social Indígena y Popular iniciada en 2008**, proyectando un mínimo de 30.000 participantes, con el fin de **persistir en la reivindicación de sus derechos y exigencias para la consolidación de un Estado social de Derecho en Colombia**.]

[Esta junta también decidió la **continuación de la asamblea permanente de los pueblos indígenas caucanos**, tanto en La María Piendamo, como en las demás comunidades del Cauca, para exigir la libertad de Feliciano Valencia, quien para 2008 se desempeñaba como Consejero Mayor del CRIC.]

[La consejera afirma que **reactivar la minga implica declarar en estado de emergencia las garantías de los derechos de los pueblos indígenas en Colombia**, evidente en los falsos positivos, asesinatos selectivos y campañas de desprestigio mediático de las que han sido víctimas.  ]

[“**La detención de Feliciano es un desconocimiento de la jurisdicción especial indígena**”, agrega Emilsen, por lo que se iniciará el proceso de casación y se estima establecer un **escenario de interlocución con la CIDH para entablar una tutela de hecho**, apelando a la sentencia 291.]

[La asamblea permanente continuará, así como el proceso de liberación de la madre tierra, adelantado desde 1971, “pues hace parte de nuestros planes de vida”, indica Emilsen.]

[**También denunciaron que tras estas acciones adelantadas por el CRIC y la ACIN, el territorio de La María Piendamó, está militarizado desde la tarde de ayer**.]

[Vea también: ][[Sectores sociales e indígenas rechazan detención del líder indígena Feliciano Valencia](https://archivo.contagioradio.com/sectores-sociales-e-indigenas-rechazan-detencion-del-lider-indigena-feliciano-valencia/) y [Movimiento indígena caucano rechaza detención de Feliciano Valencia](https://archivo.contagioradio.com/movimiento-indigena-caucano-rechaza-detencion-de-feliciano-valencia/)]

[ ]
