Title: Eduardo Galeano deja abiertas las venas de América Latina
Date: 2015-04-13 17:07
Author: CtgAdm
Category: Entrevistas, Política
Tags: Eduardo Galeano, Las venas abiertas de América Latina, Tabaré Vásquez
Slug: con-muerte-de-eduardo-galeano-quedan-abiertas-las-venas-de-america-latina
Status: published

###### Foto: verdecoloresperanza.blogspot.com 

Eduardo Galeano (1940- 2015)

El escritor uruguayo falleció en Montevideo a los 74 años de edad.

*“Al fin y al cabo, somos lo que hacemos para cambiar lo que somos”*

Una vida dedicada a interpretar y retratar la realidad latinoamericana desde las letras, el periodismo, y la cultura política, culmina este lunes su etapa corpórea con el fallecimiento del escritor Uruguayo Eduardo Galeano, luego de una semana de haber sido ingresado en un centro hospitalario de Montevideo por el agravamiento de su estado de salud según informaron sus familiares a la agencia EFE.

Eduardo Germán María Hughes Galeano, empezó a mostrar su inclinación por los temas sociales a la edad de 14 años con la publicación de caricaturas políticas firmadas bajo el seudónimo de “Gius” en el semanario socialista  “El sol”, dando así sus primero pasos en el periodismo que continuarían a inicios de los años 60 con su designación como jefe  de redacción del semanario “Marcha” y luego como director del diario “Época”.

En 1971 publica “Las venas abiertas de América Latina”, un análisis de la explotación del continente sudamericano desde la Colonia el presente, censurado por las dictaduras militares de Uruguay, Argentina y Chile. Luego del  golpe de Estado del 27 de junio de 1973, Galeano fue encarcelado y desterrado de su país.  Se fue a vivir a Argentina, donde fundó el magacín cultural Crisis y en 1976 continuó su exilio en España luego de ser agregado a la lista de los condenados del escuadrón de la muerte de Videla.

Estando allí escribiría en 1984 su trilogía: “Memoria del fuego”, una obra de notable talante crítico que combina elementos de la poesía, la historia y el cuento donde se narra de manera cronológica acontecimientos culturales e históricos sobre la identidad latinoamericana con el propósito de enfrentarse a la "usurpación de la memoria" que él denuncia en la historia oficial.

En 1985, cuando Julio María Sanguinetti asumió la presidencia del país por medio de elecciones democráticas, retorna a Montevideo y funda el semanario “Brecha” junto con Mario Benedetti, Hugo Alfaro y otros periodistas y escritores, antiguos colaboradores del semanario "Marcha". Posteriormente fundó y dirigió su propia editorial “El Chanchito”, publicando a la vez una columna semanal en el diario mexicano La Jornada.

En el año 2004, Galeano apoyó la victoria de la alianza Frente Amplio y de Tabaré Vázquez. Un año después junto a intelectuales de izquierda como Tariq Ali y Adolfo Pérez Esquivel se unen al comité consultivo de la reciente cadena de televisión latinoamericana TeleSUR.

La prolífica obra de Eduardo Galeano, reconocida en dos ocasiones con el premio Casa de las Américas con su novela La canción de nosotros (1975) y con el testimonio Días y noches de amor y de guerra (1978), abarca los más diversos géneros narrativos y periodísticos, entre los que destacan “Los días siguientes” (1962), “China, crónica de un desafío” (1964), “Guatemala, país ocupado” (1967), “Nosotros decimos no” (1989), “El libro de los abrazos” (1989), “Las palabras andantes” (1993), “El fútbol a sol y sombra” (1995), “Patas arriba. La escuela del mundo al revés” (1999), “Bocas del tiempo” (2004) y “Espejos. Una historia casi universal” (2008) entre otros.

La noticia del fallecimiento del escritor, confirmada en un comunicado publicado en sitio web del Ministerio de Educación y Cultura de Uruguay,  se da a pocos días de la presentación en España de "Mujeres" de la editorial Siglo XXI, un libro-antología de 200 páginas con los mejores textos de Galeano sobre las mujeres con relatos de personajes como Juana de Arco, Rosa Luxemburgo, Rigoberta Menchú, Marilyn Monroe y Teresa de Ávila. El portal web “The huffingtonpost” en su versión en español, publicó algunos fragmentos que componen la publicación disponible desde el próximo jueves.

*JUANA*

#### *Como Teresa de Ávila, Juana Inés de la Cruz se hizo monja para evitar la jaula del matrimonio.* 

#### *Pero también en el convento su talento ofendía. ¿Tenía cerebro de hombre esta cabeza de mujer? ¿Por qué escribía con letra de hombre? ¿Para qué quería pensar, si guisaba tan bien? Y ella, burlona, respondía:* 

#### *—¿Qué podemos saber las mujeres, sino filosofías de cocina?* 

#### *Como Teresa, Juana escribía, aunque ya el sacerdote Gaspar de Astete había advertido que a la doncella cristiana no le es necesario saber escribir, y le puede ser dañoso.* 

#### *Como Teresa, Juana no sólo escribía, sino que, para más escándalo, escribía indudablemente bien.* 

#### *En siglos diferentes, y en diferentes orillas de la misma mar, Juana, la mexicana, y Teresa, la española, defendían por hablado y por escrito a la despreciada mitad del mundo.* 

#### *Como Teresa, Juana fue amenazada por la Inquisición. Y la Iglesia, su Iglesia, la persiguió, por cantar a lo humano tanto o más que a lo divino, y por obedecer poco y preguntar demasiado.* 

#### *Con sangre, y no con tinta, Juana firmó su arrepentimiento. Y juró por siempre silencio. Y muda murió.* 

#### *LOUISE* 

#### *—Quiero saber lo que saben –explicó ella.* 

#### *Sus compañeros de destierro le advirtieron que esos salvaje no sabían nada más que comer carne humana:* 

#### *—No saldrás viva.* 

#### *Pero Louise Michel aprendió la lengua de los nativos de Nueva Caledonia y se metió en la selva y salió viva.* 

#### *Ellos le contaron sus tristezas y le preguntaron por qué la habían mandado allí:* 

#### *—¿Mataste a tu marido?* 

#### *Y ella les contó todo lo de la Comuna:* 

#### *—Ah –le dijeron–. Eres una vencida. Como nosotros.* 

#### *CELEBRACIÓN DE LA AMISTAD* 

#### *Juan Gelman me contó que una señora se había batido a paraguazos, en una avenida de París, contra toda una brigada de obreros municipales. Los obreros estaban cazando palomas cuando ella emergió de un increíble Ford a bigotes, un coche de museo, de aquellos que arrancaban a manivela; y blandiendo su paraguas, se lanzó al ataque.* 

#### *A mandobles se abrió paso, y su paraguas justiciero rompió las redes donde las palomas habían sido atrapadas. Entonces, mientras las palomas huían en blanco alboroto, la señora la emprendió a paraguazos contra los obreros.* 

#### *Los obreros no atinaron más que a protegerse, como Pudieron, con los brazos, y balbuceaban protestas que ella no oía: más respeto, señora, haga el favor, estamos trabajando, son órdenes superiores, señora, por qué no le pega al alcalde, cálmese, señora, qué bicho la picó, se ha vuelto loca esta mujer...* 

#### *Cuando a la indignada señora se le cansó el brazo, y se apoyó en una pared para tomar aliento, los obreros exigieron una explicación.* 

#### *Después de un largo silencio, ella dijo:* 

#### *—Mi hijo murió.* 

#### *Los obreros dijeron que lo lamentaban mucho, pero que ellos no tenían la culpa. También dijeron que esa mañana había mucho que hacer, usted comprenda...* 

#### *—Mi hijo murió –repitió ella.* 

#### *Y los obreros: que sí, que sí, pero que ellos se estaban ganando el pan, que hay millones de palomas sueltas por todo París, que las jodidas palomas son la ruina de esta ciudad...* 

#### *—Cretinos –los fulminó la señora.* 

#### *Y lejos de los obreros, lejos de todo, dijo:* 

#### *—Mi hijo murió y se convirtió en paloma.* 

#### *Los obreros callaron y estuvieron un largo rato pensando. Y por fin, señalando a las palomas que andaban por los cielos y los tejados y las aceras, propusieron:* 

#### *—Señora: ¿por qué no se lleva a su hijo y nos deja trabajar en paz?* 

#### *Ella se enderezó el sombrero negro:* 

#### *—¡Ah, no! ¡Eso sí que no!* 

#### *Miró a través de los obreros, como si fueran de vidrio, y muy serenamente dijo:* 

#### *—Yo no sé cuál de las palomas es mi hijo. Y si supiera, tampoco me lo llevaría. Porque ¿qué derecho tengo yo a separarlo de sus amigos?* 

El último trabajo del escritor será publicado por la Editorial Siglo XXI de manera póstuma. Se trata de un texto inédito, sin título por el momento, que estaría listo para entrar en imprenta y sería lanzado en el mes de Mayo de manera simultánea en España, México y Argentina, tres años después de la publicación de “Los hijos de los días” su obra más reciente.
