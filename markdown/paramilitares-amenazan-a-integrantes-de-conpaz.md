Title: Paramilitares amenazan a integrantes de CONPAZ
Date: 2016-04-20 18:11
Category: DDHH, Nacional
Tags: 'Clan Úsuga', Conpaz, ELN, FARC, Los restrojos, Paramilitarismo
Slug: paramilitares-amenazan-a-integrantes-de-conpaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/conpaz_4_contagioradio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CONPAZ] 

###### [20 Abril 2016]

A través de una mensaje de texto y una llamada telefónica fueron amenazados 4 integrantes de la red de Comunidades Construyendo Paz en los Territorios, CONPAZ, que se ha identificado por a**portar en con [propuestas diversas](https://archivo.contagioradio.com/?s=conpaz) en la construcción de los acuerdos de paz de la Habana y recientemente con la guerrilla del ELN.**

En el mensaje de texto el grupo que se identifica como "Plan Usuga", amenazan a los líderes indígenas **Marcia Mejia y Miller Mejia, integrante y presidente de la Asociación de cabildos de Valle ACIVA**, respectivamente y también a **Reinaldo Ibito gobernador del resguardo “La Delfina”**, reconocido **sitio de concentración de las comunidades indígenas** para las asambleas permanentes y movilizaciones que se realizan en esa zona del país.

Por otro lado, en una llamada telefónica, un paramilitar que se identifica con el nombre de Octavio y mando del grupo paramilitar [“Los Rastrojos”](https://archivo.contagioradio.com/?s=rastrojos) en el departamento del Huila, dio detalles de los movimientos de Ricardo Barragan, **reconocido ambientalista del municipio de “La Plata” y de Inzá en Cauca**. Asegura “Octavio” que si el líder no entrega medicamentos por valor de 2’500.000 a esa estructura tendría que irse de la región con su familia.

Además, en el municipio de Inza en Cauca, pobladores han asegurado que hay una fuerte **[incursión paramilitar](https://archivo.contagioradio.com/?s=paramilitares) para reclutar jóvenes de la región**. Asegura la denuncia de la Comisión de Justicia y Paz, que la persona a cargo del reclutamiento se ha identificado como exmilitar e integrante del grupo paramilitar “Los Rastrojos”.

Hasta el momento se desconocen las acciones de las autoridades para identificar y capturar a las personas responsables tanto de las amenazas como del reclutamiento.

<iframe src="http://co.ivoox.com/es/player_ej_11255275_2_1.html?data=kpafl5qWe5ahhpywj5WbaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5ynca7dzdHS1JCxqcuZpJiSo6nFcYy3sLO9o7%2BRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
