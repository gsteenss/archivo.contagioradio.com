Title: “Álvaro Hernán Prada pasará a ser el juez de sus jueces”
Date: 2020-11-06 19:38
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Álvaro Hernán Prada, Álvaro Uribe, Caso Uribe, Comisión de Acusación
Slug: alvaro-hernan-prada-pasara-a-ser-el-juez-de-sus-jueces
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Alvaro-Uribe-y-Alvaro-Hernan-Prada.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este jueves la plenaria de la **Cámara de Representantes** aprobó por mayoría (**88 votos a favor y 26 en contra**), la designación del congresista **Álvaro Hernán Prada **del Centro Democrático; como integrante de la **Comisión de Acusación**, **pese a estar siendo investigado y procesado por la Corte Suprema de Justicia por manipulación de testigos en el caso del expresidente Álvaro Uribe Vélez.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El nombramiento generó indignación en varios sectores, teniendo en cuenta que **Álvaro Hernán Prada entra a integrar la Comisión** **encargada, entre otras, de investigar y** **tramitar denuncias penales y quejas disciplinarías** contra altos funcionarios del Estado como el Presidente, el Fiscal General y Magistrados de las Altas Cortes; es decir, el congresista entrará a investigar a los propios jueces de la Corte Suprema que actualmente lo investigan; en una doble e incompatible calidad de investigado e investigador.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AlirioUribeMuoz/status/1324670706542628864","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AlirioUribeMuoz/status/1324670706542628864

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Parlamentarios como [David Racero](https://twitter.com/DavidRacero) del Movimiento Colombia Humana se pronunciaron en contra de la designación señalando que “*se termina mandado un mal mensaje al país, a la gente de a pie que no logra entender cómo alguien que está siendo investigado por delitos muy graves, tanto para que Uribe haya tenido que renunciar, va a ocupar un lugar en la Comisión de Acusación*”.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DavidRacero/status/1324390093101191169","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DavidRacero/status/1324390093101191169

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En igual sentido, se pronunció en entrevista para Contagio Radio la Representante **[Ángela María Robledo](https://twitter.com/angelamrobledo)**, quien señaló que del Congreso se puede esperar cualquier cosa, pero que “*escuchar a Álvaro Hernán Prada, invocando la ley, el debido proceso, lo que produce es risa*” cuando es una persona que está siendo procesada por la Justicia en uno de los casos más sonados del país y que lo más delicado es que **Prada pasará a ser “*el juez de sus jueces*”.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/GustavoBolivar/status/1324471343547621376","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/GustavoBolivar/status/1324471343547621376

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Asimismo el congresista Mauricio Toro, cuestionó el hecho de que tuviera que ser Álvaro Prada el designado, de todos los congresistas que tiene el Partido Centro  Democrático, agregó que se tendría que declarar impedido en todos los procesos contra magistrados y concluyó que **la única intención con esa elección era “*presionar a la Corte Suprema de Justicia*”.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/MauroToroO/status/1324394404975677441","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/MauroToroO/status/1324394404975677441

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### La implicación de Álvaro Hernán Prada en el caso Uribe

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Congresista Álvaro Hernán Prada está siendo investigado en la Corte Suprema en el mismo proceso que se adelanta contra Álvaro Uribe Vélez, por estar relacionado con **una aparente intención de manipular el testimonio del exparamilitar Juan Guillermo Monsalve, quien ha incriminado a Uribe en la conformación del Bloque Metro de las Autodefensas en Antioquia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La participación de Prada en el caso, es investigada  por cuanto se presume que realizó supuestos ofrecimientos al testigo Monsalve, para que cambiara las declaraciones en las que mencionaba a Uribe, a través de un intermediario llamado **Carlos López, alias *Caliche*,** quien le pidió que hiciera un video retractándose de todo y que culpara falsamente a Cepeda de haberle ofrecido dádivas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Contra Prada obran en el expediente algunos contactos que estableció con alias *Caliche*, presuntamente coordinando los ofrecimientos y los términos en los que tendría que darse la retractación de Monsalve. (Lea también: [Iván Cepeda denuncia falta de garantías por parte de la Fiscalía en caso Uribe](https://archivo.contagioradio.com/ivan-cepeda-denuncia-falta-de-garantias-por-parte-de-la-fiscalia-en-caso-uribe/))

<!-- /wp:paragraph -->
