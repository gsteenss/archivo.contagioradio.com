Title: ¿En qué va la Ley de Amnistía a dos años de su implementación?
Date: 2019-04-25 10:54
Author: CtgAdm
Category: Expreso Libertad
Tags: Cárceles en Colombia, Ley de Amnistia, prisioneros farc
Slug: ey-amnistia-dos-anos-implementacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/los_presos_politicos_de_farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

Tras dos años de la implementación de la Ley de Amnistía en Colombia, organizaciones defensoras de derechos humanos han denunciado la fuerte situación que afrontan tanto las personas beneficiarias de este procesos como aquellos que continúan en las cárceles del país.

En este programa del Expreso Libertad, los abogados William Acosta de la Corporación Semilla y Memoria, y Uldarico Flórez, presidente de la Brigada Eduardo Umaña Mendoza, enfatizaron en que continúan las faltas de respuestas por parte de los Jueces de Ejecución de Penas y aseguraron que algunos persisten en mantener la persecución política en contra de quienes permanecen en las cárceles por pertenecer a FARC.

De otro lado, manifestaron que desde la firma del Acuerdo de Paz, más de 90 ex combatientes han sido asesinados, hecho que podría constituirse en un genocidio, lo que para ellos es producto de la falta de garantías jurídicas y de derechos para quienes se acogieron a los Acuerdos de La Habana.

<div class="entry-content">

<div class="has-content-area" data-url="https://archivo.contagioradio.com/falsos-positivos-judiciales-contra-lideresas-sociales/" data-title="Falsos positivos judiciales contra lideresas sociales">

<iframe id="audio_34948046" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34948046_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 

</div>

</div>

<footer class="entry-footer">
<div class="post__tags">

</div>

</footer>

