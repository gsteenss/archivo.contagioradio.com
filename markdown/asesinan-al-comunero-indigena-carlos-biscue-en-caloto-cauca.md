Title: Asesinan al comunero indígena Carlos Biscue en Caloto, Cauca
Date: 2019-06-23 18:03
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Caloto, Cauca, Consejo Regional Indígena del Cauca
Slug: asesinan-al-comunero-indigena-carlos-biscue-en-caloto-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Screen-Shot-2019-06-23-at-4.49.45-PM.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto:] 

Durante la madrugada de este domingo, el comunero indígena Carlos Biscue fue asesinado en el resguardo Nasa, López Adentro, en el municipio de Caloto, Cauca por hombres armados, quienes dispararon de manera indiscriminadamente a una fiesta de la vereda donde se encontraba. Según información del Consejo Regional Indígena del Cauca (CRIC), el hombre de 30 años vivía en la vereda El Credo del resguardo indígena Huellas en el mismo municipio.

Noticias en desarrollo...

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
