Title: Nuevo robo de información a plataforma de organizaciones de DDHH
Date: 2015-10-15 14:35
Category: DDHH, Nacional
Tags: abogada de la Corporación Jurídica Yira Castro, Alberto Yepes, Claudia Erazo, COEUROPA, Coordinación Colombia- Europa-Estados Unidos, Noticias de Derechos Humanos, Paramilitarismo, Robo de informacion
Slug: nuevo-robo-de-informacion-a-plataforma-de-organizaciones-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/portatil_robado_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [rodrigoavilatv]

<iframe src="http://www.ivoox.com/player_ek_9007169_2_1.html?data=mpWdmZaafY6ZmKiak5qJd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRktbZ19SY1NTGs4zYxpDW0MvTts7VxM6SpZiJhpTijMaY0tHFuMLa0Nfaw5DIqYzj08zO0M7epcTd0NOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Claudia Erazo, abogada de la Corporación Jurídica Yira Castro] 

###### [15 Oct 2015] 

El hecho ocurrió el pasado 14 de Octubre, cuando 2 sujetos ingresaron a la habitación de la defensora de Derechos Humanos Lourdes Castro, integrante de la Secretaría Técnica de la **Coordinación Colombia - Europa – Estados Unidos**. Los sujetos ingresaron a la vivienda, intimidaron y encerraron a la persona que se encontraba allí en ese momento y **hurtaron un computador portátil que contenía información de diversas denuncias de víctimas del país**.

Según **Claudia Erazo, abogada de la Corporación Jurídica Yira Castro,** una de las 200 organizaciones de la plataforma, en la vivienda había muchos objetos de valor que hubiesen sido hurtados si se tratara de un robo común y agrega que esta ya es la segunda vez que se presentan agresiones de este tipo contra COEUROPA puesto que en meses anteriores fue robado el computador de **Alberto Yepes**.

Un elemento preocupante es la alta vulnerabilidad con que los defensores y defensoras de DDHH de esta organización realizan su trabajo dado que se desempeñan en su labor sin ningún tipo de protección para ellos o sus familias. El otro asunto que señala Erazo es que las **autoridades abordan el robo de información como un delito común que casi siempre termina sin que se asignen investigadores al caso, lo cual abre la puerta de la impunidad.**

Tanto en el caso del robo del computador de Alberto Yepez como en esta ocasión, la información robada corresponde a sensibles denuncias de víctimas, en las que se documentan casos de violaciones a los Derechos Humanos que algunas veces comprometen a agentes activos de instituciones del Estado.
