Title: Por aumento de la violencia, Alemania podría cortar financiación a Colombia
Date: 2020-09-11 10:40
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Alemania, Masacres
Slug: por-aumento-de-la-violencia-alemania-podria-cortar-financiacion-a-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Temblores-ONG-.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/200911_Escalada-de-violemcia-en-Colombia_Declaración-de-prensa_Heike-Hänsel_ES_DE.pdf" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Movilización desde Munich, Alemanía*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una comunicación fechada este 11 de Septiembre, la **parlamentaria alemana [Heike Hänsel](https://www.heike-haensel.de/)**, miembro del Bundestag, **se refirió con preocupación a los asesinatos en Colombia registrados en medio de las movilizaciones, desde este 9 de Septiembre**; y aseguró que esta situación podría generar un corte en la financiación de ese país a Colombia a manera de sanción.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Linksfraktion/status/1304415919855276034","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Linksfraktion/status/1304415919855276034

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La parlamentaria hizo un recuento de las muertes de personas en proceso de reincorporación, líderes sociales y personas que han participado en las protestas de estos dos días y que son[responsabilidad de la Policía](https://archivo.contagioradio.com/asesinato-de-javier-ordonez-sera-investigado-por-fiscal-especializado-en-dd-hh/).

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"En 2020 fueron asesinados más de 205 activistas sociales y 43 miembros del partido FARC, 53 masacres tuvieron lugar. El gobierno de Iván Duque es responsable de esta violencia. El 10 de septiembre, al menos once personas murieron y 58 manifestantes resultaron heridas por el uso arbitrario de armas de fuego por parte de los agentes de Policía. Las protestas se desencadenaron por el asesinato del abogado Javier Ordóñez, de 43 años, el 9 de septiembre en Bogotá por la policía. **Cuatro años después de la firma del Acuerdo de Paz entre el Estado colombiano y las entonces FARC-EP, no hay indicios de más paz y seguridad"***
>
> <cite>Apartado del comunicado enviado por la **parlamentaria alemana Heike Hänsel**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Además aseguró que la comunidad internacional no puede seguir tolerando las muertes diarias que se presentan en el país y señaló que la responsabilidad directa es del Gobierno de Iván Duque. *"L****os asesinatos diarios de activistas en Colombia por parte de [fuerzas estatales](https://archivo.contagioradio.com/organizaciones-sociales-convocaron-a-un-pacto-por-la-vida-y-la-paz/)y  
paramilitares no deben seguir siendo tolerados por la comunidad internacional"*.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último aseguró que después de casi 4 años de la firma del Acuerdo de Paz, no hay evidencias de mayor tranquilidad y paz para Colombia por lo que **el Gobierno Alemán, debería considerar el corte de la financiación que entrega a Colombia.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"El Gobierno alemán, que ha cofinanciado el Proceso de Paz, está en la obligación de exigir al Gobierno colombiano garantizar la seguridad de los ex combatientes, de las FARC y los activistas de los movimientos sociales, y en caso de incumplimiento de cortar el financiamiento del Proceso de Paz".*
>
> <cite>Pronunciamiento **parlamentaria alemana Heike Hänsel**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Le puede interesar leer:*

<!-- /wp:paragraph -->

<!-- wp:file {"id":89677,"href":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/200911_Escalada-de-violemcia-en-Colombia_Declaración-de-prensa_Heike-Hänsel_ES_DE.pdf"} -->

<div class="wp-block-file">

[Escalada de violencia en Colombia: Declaración de Heike-Hänsel](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/200911_Escalada-de-violemcia-en-Colombia_Declaración-de-prensa_Heike-Hänsel_ES_DE.pdf)[Descarga](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/200911_Escalada-de-violemcia-en-Colombia_Declaración-de-prensa_Heike-Hänsel_ES_DE.pdf){.wp-block-file__button}

</div>

<!-- /wp:file -->

<!-- wp:block {"ref":78955} /-->
