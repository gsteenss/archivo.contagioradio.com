Title: Israel sigue negando la larga historia de los pueblos palestinos
Date: 2017-07-26 15:55
Category: Onda Palestina
Tags: Apartheid, BDS, Israel, Palestina
Slug: israel-palestina-dkeika
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/historia-palestina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Reuters 

###### 19 Jul 2017 

El pueblo de **Dkeika**, situado en las colinas del sur de Hebrón, se e**nfrenta a una amenaza de demolición**. El estado de Israel dice que fue construido ilegalmente y nunca fue un pueblo real, pero los residentes de Dkeika dicen que su pueblo se remonta a más de un siglo y se niegan a mudarse, diciendo que viven en su propia tierra.

“**¿Por qué me obligan a abandonar mi tierra?**”, dijo Odeh Najada, un residente que asistió a la sesión del Tribunal Supremo donde este caso está siendo juzgado. “**Esta es la tierra de mi padre y de mi abuelo**”.

Netta Amar-Shiff, quien representa a las personas residentes de Dkeika y hace parte de la ONG Rabinos por los Derechos Humanos, dijo a The Jerusalem Post: “**Deben tener derecho a construir sobre sus propias tierras históricas**. Tenemos fotografías aéreas, de 1945 a 2000 que indican la expansión gradual del territorio. Dkeika estaba ahí antes de 1967. **Los pueblos existentes antes de 1967 no pueden ser reubicados o destruidos.**”

La verdadera razón por la cual quieren demoler el pueblo beduino, Amar-Shiff alega, es “**la ubicación estratégica de Dkeika, que interrumpe la continuidad territorial israelí entre Hebron y el Negev.**” Ella sugirió que un asentamiento judío podría ser construido en el lugar de Dkeika.

En abril, el ministro de Vivienda, **Yoav Gallant pidió intensificar la construcción las colonias israelíes en las colinas del sur de Hebrón**. “La zona del Valle Arad y el sur de las colinas de Hebrón tiene que estar bajo el control completo del Estado de Israel para asegurar su soberanía. La clave para esto son los asentamientos”, dijo.

Amar-Shiff afirmó que **obligar a los residentes a abandonar el pueblo de Dkeika sería un “crimen de guerra dentro de cualquier interpretación legal.** El artículo 49 de la Cuarta Convención de Ginebra prohíbe el traslado forzoso de poblaciones protegidas de sus territorios”.

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
