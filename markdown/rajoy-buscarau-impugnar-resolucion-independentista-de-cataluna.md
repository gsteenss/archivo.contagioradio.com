Title: Rajoy buscará impugnar resolución independentista de Cataluña
Date: 2015-11-09 16:03
Category: El mundo, Otra Mirada
Tags: Indepedencia Cataluña, Mariano Rajoy
Slug: rajoy-buscarau-impugnar-resolucion-independentista-de-cataluna
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Cataluña.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [09, Nov 2015]

Tras la histórica votación del Parlamento regional de Cataluña por el "Si" con el que se aprobó la resolución independentista, el presidente del gobierno español Mariano Rajoy, anunció que presentará un recurso ante el Tribunal Constitucional para invalidar la decisión secesionista.

La aprobación alcanzada gracias a los 72 votos de Junts pel Sí y la CUP, podría quedar automáticamente suspendida si el Tribunal Constitucional acepta el recurso que Rajoy interpondrá el próximo miércoles con el que podría suspenderse de forma cautelar la resolución hasta que los magistrados emitan su voto final.

"El gobierno que presido no va a permitir que esto continúe", afirmó el mandatario en intervención televisiva y agregó "Estamos decididos a utilizar todos los medios que el Estado de Derecho ha puesto a disposición de la democracia para defender la propia democracia". Advertencias que traerían como consecuencia inhabilidad de cargos públicos hasta la la suspensión de la autonomía de la región.

Por su parte, los promotores del proceso por el cual se crearía el estado Catalán, aseguran que en la resolución reza que la decisión "no se supeditará a las decisiones del Estado español, en particular del Tribunal Constitucional", por lo cuál seguirán adelante con su aspiración independentista.

Rajoy se reunirá este martes con el líder de la oposición Pedro Sànchez, secretario general del PSOE, partidario de la acción del gobierno, quien asegura que "la mayoría de catalanes no quiere la independencia".
