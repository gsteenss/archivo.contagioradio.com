Title: Desalojan Plazarte a pesar de haber sido reconocidos como poseedores
Date: 2017-12-27 10:21
Category: DDHH, Nacional
Tags: Medellin, Plazarte
Slug: artistas-de-medellin-denunciaron-el-desalojo-violento-del-centro-cultural-plazarte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/habitat-latente.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Hábitad Latente] 

###### [27 Dic 2017] 

En horas de la mañana de este 27 de diciembre, se dio el desalojo violento del centro cultural Pazarte, un espacio histórico en Medellín, ubicado en el barrio Prado, que que era lugar de encuentro para artistas de todas las edades. Durante el desalojo, **los artistas denunciaron el uso excesivo de gases lacrimógenos y del abuso de autoridad por parte de la Fuerza Pública**.

La Corporación Centro Plazarte adquirió los derechos de propiedad sobre el inmueble, sin embargo, posteriormente las instalaciones fueron vendidas por particulares que ya no tenían los derechos, a la Fundación Obra de Jesús Pobre, q**ue ha reclamado el inmueble en procesos en donde Plazarte no ha estado demandado.**

Por este motivo, Plazarte ha estado exigiendo a la Justicia que les permita defenderse, debido a que ya son reconocidos como los poseedores del inmueble en un proceso de pertenencia, **sin embargo, durante todo el pleito no han sido llamados para atestiguar sobre el estado actual de la casa**.

### **Así fue el desalojo** 

Pese a que las personas se encontraban al interior del recinto, en calma y a la espera de que se lograra algún tipo de mediación que evitara el desalojo, videos grabados desde el interior de la casa muestran como las autoridades entran violentamente al lugar, **lanzan gases lacrimógenos y empujan a las personas que allí se encontraban**.

**En total fueron 50 artistas los desalojados y el operativo culminó con el cierre de un escenario que funcionó por más de 12 años realizando** talleres de pintura, dibujo, baile, música, teatro, entre otras actividades artísticas y que al mismo tiempo posibilito que artistas tuviesen una formación cultural, que de no estar Plazarte, hubiese sido muy difícil de adquirir debido a los altos costos que ello implica.

<iframe src="https://www.youtube.com/embed/5N7UaxjiS_0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
