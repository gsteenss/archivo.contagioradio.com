Title: Edwin de Jesús Carrascal, firmante de la paz asesinado en Sucre
Date: 2020-03-11 14:07
Author: CtgAdm
Category: Actualidad, DDHH
Slug: edwin-de-jesus-carrascal-firmante-de-la-paz-asesinado-en-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Asesinan-a-Edwin-de-Jesús-Carrascal-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @SandraFARC {#foto-sandrafarc .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El partido Fuerza Alternativa Revolucionaria del Común (FARC) denunció que el pasado martes 10 de marzo en horas de la noche fue asesinado Edwin de Jesús Carrascal Barrios, firmante del Acuerdo de Paz que vivía en la vereda San Antonio del municipio de Coloso, Sucre. Según las primeras informaciones, hombres armados llegaron hasta su lugar de domicilio, y allí atentaron contra su vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Integrantes de FARC manifestaron que aunque Jesús Carrascal fue trasladado a un centro asistencial, no fue posible salvar su vida. Entre tanto, se conoció que la Policía no logró dar con la captura de los agresores que se movilizaban en una motocicleta. (Le puede interesar: ["El miedo que sentíamos en la guerra, es el miedo que ahora sentimos al salir de casa: excombatientes"](https://archivo.contagioradio.com/el-miedo-que-sentiamos-en-la-guerra-es-el-miedo-que-ahora-sentimos-al-salir-de-casa-excombatientes/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Con Jesús Carrascal, **son más de 190 ex combatientes asesinados desde la firma del Acuerdo** de Paz

<!-- /wp:heading -->

<!-- wp:paragraph -->

El partido FARC exigió al Gobierno garantías para la vida, tomando en cuenta que recientemente fue asesinada Astrid Conde en Bogotá, que junto a Jesús Carrascal, sumarían 16 firmantes de la paz víctimas de la violencia en lo corrido de 2020. De igual forma, el partido pidió pronta investigación para aclarar su caso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Precisamente sobre este tema, organizaciones sociales han señalado que el Gobierno de Colombia tiene un problema pues en el caso de homicidios de defensores de derechos humanos, el índice de sentencia alcanza solo el 11%. (Le puede interesar: "[Al Gobierno no le gusta que se hable de impunidad: Plataformas de DD.HH."](https://archivo.contagioradio.com/al-gobierno-no-le-gusta-que-se-hable-de-impunidad-plataformas-de-dd-hh/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, el partido convocó a realizar un cacerolazo el próximo 12 de marzo en el Parque Santander a las 6 de la tarde, por la vida de los firmantes de la paz que han sido asesinados. (Le puede interesar: ["Asesinan a Daniel Jimenez, excombatiente de las FARC en Puerto Guzmán"](https://archivo.contagioradio.com/asesinan-a-daniel-jimenez-excombatiente-de-las-farc-en-puerto-guzman/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Carlozada_FARC/status/1237800117085839360","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Carlozada\_FARC/status/1237800117085839360

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

También lo invitamos a consultar: [Un cacerolazo para amplificar la voz de casi 200 excombatientes asesinados](https://archivo.contagioradio.com/cacerolazo-mplificar-voz-casi-200-excombatientes-asesinados/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
