Title: ANLA suspende actividades de Hidroituango por daños socio-ambientales
Date: 2016-02-04 16:54
Category: Ambiente, Entrevistas
Tags: ANLA, Hidroeléctricas, Hidroituango
Slug: anla-suspende-actividades-de-hidroituango-por-danos-socio-ambientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/hidroituango-e1454622622131.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Debate Hidroituango 

###### [4 Feb 2016] 

Tras denuncias realizadas por el Movimiento Ríos Vivos de Antioquia y otras organizaciones de la región, se impuso una medida preventiva de sanción al **proyecto Hidroituango** de Empresa Públicas de Medellín (EPM), por daños ambientales, la medida es dictada por  la Agencia Nacional de Licencias Ambientales,  ANLA.

La resolución, resalta que la **ANLA halló incumplimiento de las obligaciones establecidas en la licencia ambiental**. La decisión se toma tras una visita de las autoridades a distintos puntos de obra del proyecto, acompañados de las comunidades donde se evidenciaron las problemáticas ambientales y sociales que ha generado la construcción de la represa.

La ANLA encontró que EPM está realizado un **manejo inadecuado al material sobrante que resulta de las actividades de excavación,** produciendo graves consecuencias  en las playas y cauces del río Cauca y sus afluentes. Además se identificó la aparición de conos de material sobrante de excavación, que provienen de las actividades constructivas de la vía, que luego llegan hasta el cauce del río.

"Los volúmenes de material aportados desde la vía son abundantes y sepultan la vegetación baja, fracturan y arrastran especies arbóreas y finalmente se depositan en el cauce del río Cauca y es arrastrado por sus aguas”, dice la resolución.

[Resolución Hidroituango](https://es.scribd.com/doc/298021533/Resolucion-Hidroituango "View Resolución Hidroituango on Scribd")

<iframe id="doc_70707" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/298021533/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
