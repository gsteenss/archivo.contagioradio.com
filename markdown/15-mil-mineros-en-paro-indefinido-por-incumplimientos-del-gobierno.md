Title: 15 mil mineros informales en paro indefinido por incumplimientos del gobierno
Date: 2016-09-21 20:57
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Antioquia, Gobierno, Mineria, movilizació, paro
Slug: 15-mil-mineros-en-paro-indefinido-por-incumplimientos-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/minera-ilegal-e1474508875981.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: País Minero 

###### [21 Sep 2016]

Cerca de 15 mil mineros de los municipios de Segovia y Remedios en Antioquia, permanecen en paro indefinido desde esta semana en rechazo al **Decreto 1421 de 2016**, en el que el gobierno contempla la implementación de las medidas para la comercialización de los minerales en Colombia, lo que para los mineros ancestrales, significa beneficios para las multinacionales y deja sin sustento a más de **150 mil personas en Antioquia y a 700 mil en todo el país, que viven de la minería informal.**

Así lo denuncia Rubén Darío Gómez Cano, secretario general de la Confederación Nacional de Mineros de Colombia, Conalminercol, quien asegura que el gobierno volvió a incumplirles con el inicio de los procesos de formalización, y en cambio lo que se estaría buscando es “sacar del negocio a los pequeños y medianos mineros”.

Gómez, afirma que por medio de este decreto, **el gobierno “encontró el mecanismos para entregar a las multinacionales nuestros soberanía, autonomía, territorio y recursos naturales”**, y nombra casos como el de Cerromatoso, en Córdoba, o el de Carbones del Cerrejón, en La Guajira, donde existen los mayores índices de pobreza del país, pues allí no ha habido inversión social.

“Esa minería no ha traído ningún beneficio para los colombianos. De acuerdo con cifras de Planeación en las zonas donde hay grandes explotaciones mineras, las necesidades básicas insatisfechas están por encima del 80%, mientras en zonas donde se desarrolla minería ancestral están por debajo del 30%”, dice.

Y es que de acuerdo con un informe de La Contraloría, citado por el secretario general de Conalminercol, **por cada \$100 que paga en Colombia una multinacional, los colombianos deben devolverle \$200.** Además en comparación a lo que deben pagar el común de los ciudadanos por la gasolina, para las mineras extranjeras un galón de combustible no vale de más de \$3.500.

Por otra parte, mientras en regalías una **multinacional entrega el 0,4%, un minero tradicional debe dar entre el 4 y el 6%.** Así mismo, los mineros informales indican que no hay mayor generación de empleo con ese tipo de minería, pues apenas la tercera parte  de los trabajadores de estas multinacionales son obreros nacionales y el resto son del exterior.

Finalmente, Gómez señala que la minería ancestral que se desarrolla en departamentos como Antioquia, Chocó y Cauca, puede recuperar el ambiente de la mano de la tecnología, y el acompañamiento de las corporaciones ambientales regionales. Sin embargo, las medidas del gobierno, no solo promueven que la minería a gran escala continúe deteriorando a los ecosistemas, y empeore las condiciones de vida de las comunidades, sino que además con **el cierre de 120 minas y 150 entables  se deja a miles de personas sin empleo.**

###### <iframe id="audio_12985637" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12985637_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
