Title: ¿Existen garantías de seguridad para excombatientes que habitan los ETCR?
Date: 2019-10-28 17:16
Author: CtgAdm
Category: Nacional, Paz
Tags: Alexander Parra, asesinato de excombatientes, etcr, Mesetas, Meta
Slug: existen-garantias-de-seguridad-para-excombatientes-que-habitan-los-etcr
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/zona-veredal-mariana-paez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

El asesinato de Alexander Parra  al interior del espacio territorial de capacitación y reincorporación (ETCR) Mariana Páez en Mesetas, Meta el pasado 24 de octubre, acrecentó la incertidumbre en los excombatientes quienes no sienten que existan garantías de seguridad en sus hogares, pese a la presencia de Policía y Ejército a escasos metros del lugar de los hechos.

Tulio Murillo, consejero político departamental del ETCR  de Mesetas,  explicó que la seguridad desplegada en cada uno de los ETCR, ha permitido generar cierto grado de confianza en los excombatientes para trabajar en sus proyectos productivos, no obstante,  casos como el de Alexander Parra  ponen en alerta a la población.  [(Lea también: Alexander Parra, representante ambiental y excombatiente es asesinado en ETCR) ](https://archivo.contagioradio.com/alexander-parra-representante-ambiental-y-excombatiente-es-asesinado-en-etcr/)

"Es lo mismo que ocurre en Cauca, en Norte de Santander, en Nariño, las regiones más militarizadas del país es donde se desarrollan más masacres contra nosotros", afirmó el consejero **quien viene advirtiendo sobre la presencia de personal no uniformado portando armas cortas mientras se desplazan en camionetas y motos**, en determinadas partes de las carreteras aledañas a Mesetas, desde hace meses, incluso días después del asesinato de Alexander en zonas como El Diamante y La Uribe.

\[caption id="attachment\_75663" align="aligncenter" width="800"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/zona-veredal-mariana-paez.jpg){.size-full .wp-image-75663 width="800" height="530"} Foto: Contagio Radio\[/caption\]

### No es un problema de presencia militar en los ETCR

Murillo señaló que no se trata de un problema relacionado al número de efectivos que brinden seguridad a los ETCR,  sino de cumplir con lo pactado en La Habana y desmontar los grupos paramilitares que continúan en los territorios, **"tenemos militares, policías, pero todo sigue pasando en medio de la seguridad que nos debería garantizar el libre derecho para la reincorporación política, económica y social"**.

Y aunque la Fiscalía conformó la Unidad Especial de investigación (UEI) para desmantelar las organizaciones criminales responsables de homicidios contra integrantes de los movimientos sociales y políticos, Murillo señaló que aún no existen resultados, contrastando el asesinato de Alexander con el que sucedió el pasado 15 de noviembre de 2018 contra el también excombatiente **Sebastián Coy Rincón**, allí mismo en Mesetas, sin que hasta el momento  existen avances en la investigación.  [(Lea también: Sin vida fueron encontrados los cuerpos de dos integrantes de FARC)](https://archivo.contagioradio.com/sin-vida-integrantes-farc/) 

El consejero agregó que en la actualidad las comunidades se encuentran atemorizadas y que el asesinato de Alexander se dio en medio de una época electoral, con el fin de "crear terror y distanciar a los electores de nuestras listas, de nuestros candidatos. Hay mucha forma de callar la voluntad popular".

Aunque el tema ha sido expuesto en los consejos de seguridad que se celebran junto a la ONU, la Fuerza Pública y diversas organizaciones, Murillo afirmó que desde el Gobierno no se han tomado las medidas necesarias para repeler esta amenaza que circunda los espacios territoriales, **"no sé cuántos muertos más tendremos que poner. Pactamos un acuerdo de dejar las armas para el libre derecho de ejercer políticamente una reincorporación con justicia social y somos muchos los amenazados"**.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
