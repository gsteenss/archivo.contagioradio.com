Title: Con un homenaje al maestro Santiago García llega Festibienal 2019
Date: 2019-04-09 19:15
Author: CtgAdm
Category: Cultura, eventos
Tags: Festivales, Santiago García, teatro
Slug: homenaje-santiago-garcia-festibienal-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/santiago-garcia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Milton Díaz 

Si no piensa salir en semana santa de la ciudad, prepárese porque el teatro se toma a la capital con la **Festibienal 2019: Un carnaval de teatro en Bogotá.**

Este evento organizado por el Teatro Ditirambo bajo la dirección de Rodrigo Rodríguez, y con el apoyo de las salas de teatro de la ciudad, presentaran del **12 al 21 de abril más de 177 funciones, en 30 salas**, con la participación de aproximadamente **500 artistas,** con una variada programación que incluye teatro para niños, dramas, comedias, artes vivas, clown, teatro físico, monólogos,  música y danza.

En esta tercera versión del Festival, la inauguración será por cuenta del clásico teatral colombiano **“Ni Mierda pal perro”**, tragicomedia ganadora de 4 premios internacionales, que narra la historia de Gilma Tocarruncho, una mujer humilde que sufre los embates del cambio, al trasladarse del campo a la ciudad detrás de un sueño que nunca cumple. Han sido 13 años y más de 600 funciones de esta pieza, escrita por **Rodrigo Rodríguez** e interpretada magistralmente por **Margarita Rosa Gallardo**.

Dentro de la agenda de Festibienal, se realizarán eventos académicos y un emotivo homenaje a la vida y obra del maestro Santiago García, director y co-fundador del Teatro La Candelaria, que con sus obras ha recreado las problemáticas de las clases más desposeídas y episodios de la historia sociopolítica colombiana, como es la obra **“Guadalupe años sin cuenta”** que en cada función que se realice durante el festival se hará un recorrido por su historia.

Esta gran fiesta del teatro que tiene lugar cada dos años ha crecido a pasos agigantados y seguirá encantando al público capitalino en las salas de **Casa Tea, Teatro Vreve, Hilos Mágicos, Umbral Teatro, Tecal, La Mama, Teatro Tierra, Teatrova, La Factoría L Explose, Casa del Silencio, Teatro Taller de Colombia, Corporación Colombiana de Teatro, y el Centro García Márquez**, entre otros.

Si desea asistir a algunas de las obra del Festibienal 2019 puede ingresar al siguiente[link](https://www.scribd.com/document/405633233/PROGRAMACION-DEFINITIVA) y consultar programación, horarios, teatros en los que se presentaran las obras. El costo de la boletería, general es de \$30.000, para estudiantes y personas de la 3ra edad \$ 20.000.
