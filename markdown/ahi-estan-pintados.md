Title: Ahí están pintados
Date: 2015-04-13 14:08
Author: CtgAdm
Category: Camilo, Opinion
Slug: ahi-estan-pintados
Status: published

**Por[ [Camilo de las Casas ](https://archivo.contagioradio.com/camilo-de-las-casas/)]**

Y todo ocurrió el mismo nueve. Ya ni sienten vergüenza y los colombianos tranquilos, domesticados en la exclusión, en la sobrevivencia, otros en el consumo y unos pocos, los de siempre en la explotación política, económica, cultural.

Ese nueve de abril, día de la memoria, de las víctimas por obra y gracia de la ley 1448, del gobierno de Santos, la mayorías organizadas e independientes salimos a la calle por la paz, que en realidad no es la paz sino la pax del Mercado de Santos; lamentable o no, una posibilidad para avanzar en los cambios estructurales o para la relegitimación del establecimiento, todo depende de esa sociedad organizada y conciente de cambios para una democracia con justicia socio ambiental.

Ese nueve de abril, insisto, día de las víctimas no de la pax santo uribista, el congreso debe escuchar a las víctimas, según, la imperfectísima e imagológica ley de víctimas, y así fue, casi medio centenar de ellas pidieron ser atendidas, pero como ocurre con nuestra clase dirigente estas estuvieron ausentes.

Esta actitud no es extraña, refleja el país santanderista, el de la leyes sin fuerza en la verdad ni en el espíritu. Se trata de legisladores, la mayoría de ellos, parte de un mercadeo de los derechos de las mayorías, retóricos, falsos, hipócritas, deshonestos. Aprueban las leyes por conveniencia y el engaño.

Las víctimas han sido usadas es un hecho. Las usan la bancada de la unidad en torno a Santos y la disidencia uribista a su acomodo, las usaron para decir que en Colombia hay un conflicto, meses antes decían, que no había que existían eran terroristas. Así es. Las usan para decir que tienen voluntad de paz, pero no es tan real, las usan para maquillar la democracia de terror y decir que todo es mejor que antes. Los legisladores son devoradores de las palabras. Sin escrúpulo usan las palabras verdad, justicia y reparación, pero sin honor a esas propias palabras, desprencian a las víctimas, como lo hicieron el nueve de abril con su inasistencia,  pantallaron y vacacionaron con la marcha por la paz y al tiempo ni las escucharon.

Así son y seguirán siendo porque no hay una transformación política, son el reflejo de un negocio, de unos traficantes de las ideas, hoy la idea es la pax no la paz, por eso marchan en la mañana como parte de la video politica y no escuchan por la tarde, el país no les importa. Por eso el gran evento de la paz del nueve de abril, volvió a reflejar ese rostro indolente de la clase politica, y nos quedamos en realidad con un día de la "paz" donde la prostituida ley de víctimas no pudo ser cuestionada, pues el país mediático empresarial que marcho es el que apoya la paz sin cortapisa. Así es de hábil el establecimiento.

Lamentablemente seguirán siendo elegidos y reelegidos ellos o en cuerpo ajeno. La cultura poĺítica instalada es de consentimiento y anuencia a esas prácticas, y la llamada izquierda cuando no es esquemática es canibal, peleas fratricidas entre pocos, mientras el país nacional sigue el garete y en una acostumbrada vida de desesperanza sin desesperación.

Ojalá los movimientos sociales rompan ya, el sectarismo para dar paso a esa paz con justicia socio ambiental que no va a surgir con Santos, pero sí de una sociedad popular organizada, creativa construyendo unas propuestas de poder, donde el día de las víctimas fundamente el día y la paz.

**[[@[CamilodCasas]**
