Title: Con un bicitour harán pedagogía sobre la consulta anticorrupción
Date: 2018-08-10 15:56
Category: eventos
Tags: anticorrupción, Bogotá, Consulta anticorrupción
Slug: bicitour-consulta-anticorrupcion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/recorrido-en-bicicleta1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Bogotá Travel Tours 

###### 10 Ago 2018 

Como parte de las iniciativas frente a la corrupción en Colombia, y como apoyo a la consulta del próximo 26 de agosto, un grupo de organizaciones ciudadanas promueve la realización de un **CorrupTour en bicicleta, un ejercicio de recuperación de memoria que tendrá lugar el próximo domingo 12 de agosto.**

Transparencia por Colombia y Datasketch con el apoyo de Burócratas, \#Votobici, El Avispero y Las Misses, invitan a **recorrer durante dos horas algunos de los principales lugares asociados con hechos de corrupción en Bogotá**, en busca de construir colectivamente soluciones a este tipo de flagelo (Le puede interesar: [La propuesta del Centro democrático que va en contra de la Consulta anticorrupción](https://archivo.contagioradio.com/centro-democratico-contra-anticorrupcion/))

A través del humor y de datos de investigaciones oficiales, los ciudadanos po**drán conocer  hechos de corrupción que han marcado la historia de la ciudad**, como los casos de la liquidada Saludcoop y la extinta Cafesalud; la corrupción en las elecciones legislativas y el Cartel del Programa de Alimentación Escolar-PAE, recorriendo los inmuebles y lugares que han sido símbolo de ello.

Durante el evento, los participantes **harán pedagogía sobre la Consulta Anticorrupción**, la cual se votará el próximo 26 de agosto de 2018, como una oportunidad histórica en la lucha contra la corrupción. El punto de encuentro y partida del CorrupTour es la **rotonda del parque Nacional con carrera 5ta a partir de las 9:30 a.m**.

![Corruptour](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Invitacion-Corruptour-en-bici-732x800.jpg){.alignnone .size-medium .wp-image-55566 width="732" height="800"}
