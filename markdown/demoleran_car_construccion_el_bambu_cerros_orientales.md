Title: Demolerán la construcción "El Bambú" en los cerros orientales
Date: 2017-05-27 19:58
Category: Ambiente, Nacional
Tags: CAR, cerros orientales
Slug: demoleran_car_construccion_el_bambu_cerros_orientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/el-bambu_-cerros-orientales-e1495928629200.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CAR 

###### [May 27 de 2017]

**La Comercializadora Kaysser y la Constructora Imaco**, son algunos de los responsables del deterioro ambiental de los cerros orientales. Tras casi un año después de haber sido noticia por ocasionar el secamiento de una quebrada de esa zona, hasta esta semana la **Corporación Autónoma Regional de Cundinamarca** (CAR) confirmó  la **demolición de la lujosa construcción que acabó con 7 hectáreas de bosque: **el predio “El Bambú”.

Se trata de un predio ubicado en el sector Rosales. Una de las construcciones que han sido denunciadas por instancias ambientales y las organizaciones ambientalistas de la ciudad, más aún después de que el Tribunal Administrativo de Cundinamarca dejara en firme la decisión de recuperar los límites originales de la reserva de los Cerros Orientales, como también lo reiteró en una sentencia el Consejo de Estado en el año 2013. [(Le puede interesar: Continúan construcciones en los cerros orientales)](https://archivo.contagioradio.com/cerro-de-los-alpes-golpe-al-ecosistema-de-cerros-orientales/)

La sanción

Es hasta este año que la CAR le formula cargos a los propietarios de estas construcciones por “realizar actividades de ocupación de cauce de la Quebrada Rosales sin autorización, realizar actividades de construcción en zona de Rerversa Forestal Protectora Bosque Oriental de Bogotá, **realizar actividades de intervención de zonas de ronda de las áreas forestales protectoras** y realizar actividades de adecuación y nivelación en el predio ubicado en la Calle 76 no. 2-60 E (Lote 11 El Bambú-El Bagazal) de Bogotá”.

Según indicó Franco, la demolición se realizará a más tardar en noviembre “Si los infractores no ejecutan la demolición".  Sumado a eso, el propietario deberá pagar una multa de 470 millones de pesos, y deberá presentar un plan de recuperación ambiental. No obstante, cabe mencionar que el daño ecológico por parte de esa construcción implica por lo menos 10 años en recuperación.

### Daño Ambiental 

Organizaciones como Amigos de la Montaña, aseguran que estas construcciones impactan gravemente el ecosistema de los Cerros y desvían el tránsito de la Quebrada Rosales, **poniendo en peligro el habitad de animales como el pez Capitancito enano**, que se encuentra en peligro de extinción.

### Los procesos que faltan 

La demolición de la edificación "Bambú" se da tras el anuncio del director de la CAR, quien también señaló que en esa zona existen otros **6 casos más por resolver. Son construcciones hasta de estrato** 8, entre las que se encuentran El Arraván, El Bambú, El Pachue, El Tuno y El Monteodoro, que tiene un costo \$5.000 y \$30.000 millones.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
