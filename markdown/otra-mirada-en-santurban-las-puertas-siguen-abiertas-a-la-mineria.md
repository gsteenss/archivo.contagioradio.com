Title: Otra Mirada: En Santurban las puertas siguen abiertas a la minería
Date: 2020-10-07 21:45
Author: AdminContagio
Category: Nacional, Otra Mirada, Programas
Tags: ´Mineria, Comité para la Defensa del Agua y el Páramo de Santurbán, Minesa, Páramo de Santurbán
Slug: otra-mirada-en-santurban-las-puertas-siguen-abiertas-a-la-mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Santurban.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Comité Santurbán

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Autoridad Nacional de Licencias Ambientales (ANLA), dio a conocer que se archivaría el trámite administrativo que evaluaba la licencia ambiental para el proyecto Soto Norte en el páramo de Santurbán. Sin embargo, se debate si la decisión es motivo de aplaudir o si no corta de raíz el problema de la minería. (Le puede interesar: [«ANLA no tomó decisión de fondo sobre la licencia ambiental de Minesa»](https://archivo.contagioradio.com/decision-de-anla-es-un-logro-pero-no-toca-de-fondo-licencia-de-minesa/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el programa de Otra Mirada sobre la incansable lucha en defensa del agua y del páramo estuvieron Luis Álvaro Pardo, economista especialista en Derecho Minero- Energético, Diego Fernando Jaimes del Comité para la Defensa del Agua y el Páramo de Santurbán y Camilo Mayorga, abogado del Comité para la Defensa del Agua y el Páramo de Santurbán. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A raíz de la decisión de la ANLA, **los invitados comparten que « la decisión de archivar es una buena noticia pero no una muy buena noticia, archivar no es negar. La amenaza de megaminería en Santurbán seguirá latente hasta cuando la ANLA decida negar la licencia ambiental» .**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, explican cuáles son las acciones que se pueden llevar a cabo en términos jurídicos y señalan cuál es la diferencia entre lo que ha venido presentando la ANLA a lo desde las comunidades se ha venido presentando en cuanto a posibles riesgos. A la vez, sostienen que el proyecto no va a generar desarrollo y describen cuales son realmente las realidades que viven las comunidades que habitan en Santurbán.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, señalan que el problema de fondo no es solo Minesa, si no el cúmulo de solicitudes que rondan al páramo de Santurbán. (Le puede interesar: [Si hoy se realizara el plebiscito los resultados serían más favorables: comunidades](https://archivo.contagioradio.com/si-hoy-se-realizara-el-plebiscito-los-resultados-serian-mas-favorables-comunidades/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También, comentan cómo hacer que los temas ambientales sigan siendo propiedad en los debates de los gobiernos y cuál es el reto de las futuras generaciones para generar un cambio en contra de la minería que pretende afectar el ambiente y las comunidades. (Si desea escuchar el programa del 5 de octubre: [Otra Mirada: Plebiscito por la paz: Cuatro años después)](https://www.facebook.com/contagioradio/videos/662932944618865)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el análisis completo: [Haga click aquí](https://www.facebook.com/contagioradio/videos/749166145636383)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
