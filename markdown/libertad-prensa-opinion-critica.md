Title: Hay que formar opiniones públicas críticas para combatir el boom de noticias falsas
Date: 2017-05-03 16:55
Category: DDHH, Nacional
Tags: colombia, día internacional, Libertad de Prensa, Reporteros Sin Fronteras
Slug: libertad-prensa-opinion-critica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/libertad-de-prensa-e1493848219511.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cartooning for peace 

###### 03 May 2017 

Este 3 de mayo se conmemora por vigésima cuarta ocasión el **Día mundial de la libertad de prensa**, una fecha para celebrar, evaluar y reflexionar sobre el estado de los derechos fundamentales a la libertad de expresión, de información y de prensa, en tiempos en que las transformaciones políticas, tecnológicas y económicas están cambiando el panorama mediático.

Para la presente edición la UNESCO ha extendido una invitación a **mantener una posición crítica sobre la información que a diario se consume y a los comunicadores a fomentar el periodismo de investigación**, esto ante la creciente ola de las noticias falsas que circulan a través de canales como las redes sociales.

De acuerdo con Fabiola León Posada de la organización Reporteros sin fronteras, el balance en cuanto a la libertad de prensa en el mundo no es alentador, situación que ha permitido que en naciones como **Gambia y Colombia** se perciba un mejor panorama para el ejercicio del periodismo. En el caso colombiano, esa sensación obedece al ambiente generado a partir de la firma de paz entre el gobierno y la guerrilla de las FARC EP.

Sin embargo, la periodista afirma que la situación es compleja por temas como la concentración mediática, las amenazas y agresiones de las que siguen siendo blanco los comunicadores en Colombia. Problemáticas que no se solucionan con los acuerdos de paz teniendo en cuenta que históricamente "**las FARC no eran las principales victimarias sino el Estado y los grupos paramilitares**".

Asuntos como el tráfico y micro tráfico de estupefacientes y la corrupción, generan este tipo de alianzas con grupos políticos, quienes terminan siendo los enemigos de los periodistas y **se oponen a que se conozca la verdad**, igualmente en temas sensibles como la explotación minera calificándolo como "un tema complicado de manejar" por los intereses que le subyacen.

La periodista sugiere algunas recomendaciones que pueden realizarse para mejorar la situación de la libertad de prensa, unas de tipo estructural que deben provenir del gobierno como lo son **combatir el paramilitarismo**, asunto acordado en La Habana, con el objetivo de "**eliminar un enemigo más de la libertad de expresión**" y asumir un compromiso real contra la corrupción.

Otras de las recomendaciones tienen que ver con la **apertura de medios a un sistema donde se amplíen las voces** que van a intervenir en las radios comunitarias y promocionar otros sectores como la televisión local, los periódicos y medios on line que implicaría un mayor despliegue en la cobertura de la internet en un país históricamente radial. Le puede interesar: [Medios de comunicación deben reflejar la diversidad y realidad de las mujeres](https://archivo.contagioradio.com/medios-de-comunicacion-deben-ser-reflejo-de-la-diversidad-de-mujeres/).

En cuanto a la audiencia, la comunicadora y docente asegura que se debe realizar **un proceso de "alfabetización"**, tanto en temas de libertad de expresión como en la formación de una opinión pública más crítica que les permita afrontar temas como el boom de las noticias falsas, la llamada post verdad o post política, a la que se refiere la UNESCO cuando asegura que **son tiempos críticos para la el periodismo**.

En relación con el trabajo de la academia, León Posada asegura que las facultades "deben retomar el periodismo como una de las herramientas fundamentales de la comunicación y **deben hacer cátedras, pedagogías con el derecho de libertad de expresión**" considerando que no se está apropiando como un derecho fundamental.

Propone que desde esos espacios se puedan diseñar **otros medios y formas de contar lo que pasa en el país** "hay que plantear un periodismo mas incluyente y eso implica **un periodismo que trabaje en la diferencia por que este es un país de diferentes**" en el que se debe brindar la posibilidad de participación de todas y todos desde su propia postura.

<iframe id="audio_18486846" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18486846_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
