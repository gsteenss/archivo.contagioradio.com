Title: Piden a ONU frenar la “embestida” del Gobierno contra el Acuerdo de Paz
Date: 2020-11-26 14:48
Author: AdminContagio
Category: Actualidad, Nacional
Tags: acuerdo de paz, ONU, paz
Slug: piden-a-la-onu-parar-la-embestida-del-gobierno-y-su-partido-contra-el-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Acuerdo-de-Paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Tras 4 años de la firma del **Acuerdo de Paz** suscrito entre el Estado colombiano y el extinto grupo armado de las FARC-EP en el Teatro Colón, varios sectores de la sociedad, integrados en el movimiento [Defendamos la Paz](https://twitter.com/DefendamosPaz); **solicitaron al Consejo de Seguridad de la ONU y su Misión de Verificación, exigir al Gobierno Nacional, encabezado por Iván Duque, un mayor compromiso con la implementación y cesar su embestida contra la Jurisdicción Especial para la Paz -JEP-,** que junto con los demás organismos del Sistema Integral de Verdad, Justicia, Reparación y No Repetición -SIVJRNR-, es la “*columna vertebral*” del Acuerdo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A través de un [comunicado](https://drive.google.com/file/d/1Ehut15GzSeplPBwQTwCc_gxEXj0PPCcr/view) Defendamos la Paz, alertó al organismo internacional, **sobre los asesinatos y agresiones de los que son constantemente víctimas los excombatientes de FARC que le apostaron a la paz y de las “*embestidas*” del Gobierno y su Partido Político Centro Democrático en contra de los entes del SIVJRNR y particularmente de la JEP;** entre las cuales menciona las **objeciones** que en su momento planteara el Presidente Duque frente a la ley estatutaria de esa jurisdicción y los **anuncios de reforma y eliminación del Tribunal de Paz impulsados por Álvaro Uribe como jefe natural del Partido.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “*El Sistema Integral de Justicia, Verdad, Reparación y Garantías de No Repetición está bajo el ataque del Presidente Duque, de su Gobierno y del partido de Gobierno*”.
>
> <cite>Extracto del comunicado de Defendamos la Paz ante la ONU.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Pese a estos embates, el excomisionado de Paz, Sergio Jaramillo, líder del equipo negociador del Gobierno en la Habana, se mostró positivo y aunque mostró la misma postura frente a los embates que ha recibido el Acuerdo, expresó: **“*Yo soy optimista. Si sobrevivimos a las embestidas de estos años –las objeciones, los entrampamientos, etc.–, es porque el proceso de paz real en los territorios tiene mucha más fuerza que sus detractores en la capital*”.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A Jaramillo le preocupa en particular el rezago en la implementación de varios puntos del Acuerdo y el recrudecimiento de la violencia en los territorios, pues considera que es una seria amenaza a la paz. **“*La inseguridad rampante en las regiones es de lejos la mayor amenaza a la paz. Sin seguridad, la reincorporación se pone en riesgo, porque le toca mover los ETCR, como ocurrió en Ituango. Algunas personas pueden comenzar, ante el riesgo, a titubear y a preferir meterse bajo la sombrilla de alguna organización ilegal para protegerse de los asesinatos. Por ese roto se nos va la paz*”,** expresó el excomisionado.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La implementación del Acuerdo de Paz

<!-- /wp:heading -->

<!-- wp:paragraph -->

En términos generales se cuestiona la lentitud con la que avanza la implementación del Acuerdo en sus diferentes puntos, por ejemplo frente al punto 1, que es la **reforma rural integral** la organización Dejusticia realizó un balance en el que da a conocer tres alertas en los que tiene que ver con entrega de tierras a los campesinos, la poca transparencia en las cifras oficiales frente a esta entrega y la falta de implementación y articulación entre los planes nacionales sectoriales y los Planes de Desarrollo con Enfoque Territorial -PDET- que se proyectan a nivel regional. Lo que para la organización implica que **“*a 4 años de la firma del Acuerdo Final, la reforma rural integral no arranca*”.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Dejusticia/status/1331296527785058306","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Dejusticia/status/1331296527785058306

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Frente a esto, el excomisionado de Paz, Sergio Jaramillo, expresó que el Gobierno ha hecho pequeñas obras que están necesitando las comunidades, con dinero de regalías y cooperación, pero ese avance es insuficiente. “Los PDET fueron diseñados para acelerar y articular la llegada de los llamados planes nacionales, **16 planes que van desde infraestructura, pasando por educación y salud, hasta comercialización y formalización laboral, para sacar al campo de esa especie de apartheid social al que ha estado sometido por décadas. El habitante del campo no puede seguir siendo en Colombia un ciudadano de segunda”,** expresó Jaramillo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Jaramillo, también hay rezagos en los compromisos pactado en el Acuerdo frente a la disminución de la pobreza, lo cual es un elemento indispensable para acortar las brechas sociales y consolidar la paz; según el excomisionado de Paz, **se acordó reducir la pobreza en el 50 % y eliminar la pobreza extrema en 15 años; pero las cifras del DANE en 2019 arrojaron, que contrario a este objetivo, la pobreza monetaria subió en un 1 % y la extrema en un 3 % y eso antes del Covid. “*Es decir, vamos para atrás*”,** agregó. (Le puede interesar: [Si usted gana más de \$137.350 ya está por encima de la línea de pobreza: DANE](https://archivo.contagioradio.com/si-usted-gana-mas-de-137-350-ya-esta-por-encima-de-la-linea-de-pobreza-dane/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, **frente a la implementación, también preocupa la protección a los firmantes del Acuerdo,** que justamente este martes en el aniversario de la firma del Acuerdo denunciaban el asesinato de Paula Andrea Osorio, la 243° víctima desde que este se suscribiera hace 4 años.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto, aseguran algunos expertos por la ineficiente gestión del Gobierno para desarrollar los instrumentos que prevé el Acuerdo de Paz para desmontar los grupos armados y por la política de guerra con la que ha afrontado esta situación, así lo expresó Camilo González Posso, director de Indepaz. (Lea también: [“La política de guerra del Gobierno reactivó todas formas de violencia”](https://archivo.contagioradio.com/la-politica-de-guerra-del-gobierno-reactivo-todas-formas-de-violencia/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “La dinámica de negación de los Acuerdos de Paz y de una política distinta en el manejo de la seguridad, por parte del Gobierno, ha permitido la reactivación de múltiples violencias. Se abandonó una política de paz para volver a asumir una política de guerra y eso está reactivando motores de violencia en muchos frentes”
>
> <cite>Camilo González Posso</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Los caminos para la implementación

<!-- /wp:heading -->

<!-- wp:paragraph -->

Lourdes Castro, coordinadora del Programa Somos Defensores, señala que si hubiese una intención por parte del Gobierno en asumir un compromiso con la implementación los puntos que tendría que priorizar, por su especial estado de rezago, serían **la reforma rural integral, la política de sustitución voluntaria de cultivos de uso ilícito y el desarrollo de las garantías de seguridad del Sistema Integral,** no solo para los excombatientes, sino para los liderazgos sociales; particularmente frente a este tercer punto; expresó que hay que avanzar en la tarea que tenía la **Comisión Nacional de Garantías de Seguridad de diseñar la estrategia de desmonte de las organizaciones sucesoras del paramilitarismo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, el abogado defensor de Derechos Humanos Alirio Uribe, ha propuesto, desde el Consejo Nacional de Paz, los puntos en los que tendría que avanzar el Gobierno para una implementación integral y de “*buena fe*” del Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Ccajar/status/1321183210445918210","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Ccajar/status/1321183210445918210

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Los avances pese a las dificultades

<!-- /wp:heading -->

<!-- wp:paragraph -->

Diana Sánchez, directora de la Asociación Minga y vocera de la plataforma Coeuropa, quiso enfocarse en los avances y aspectos positivos que trajo consigo el Acuerdo de Paz; pese a no estar previstos formalmente en su texto, avances “*intangibles*” que tienen que ver con **el fortalecimiento de la movilización social; el enfoque de la opinión pública frente aspectos distintos al conflicto armado,** lo cual permitió evidenciar la corrupción que aqueja al país; **y desenmascaramiento de la “*clase política tradicional que ha estado detrás de la violencia y la criminalidad*”.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DianaDefensora/status/1331334572185948161","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DianaDefensora/status/1331334572185948161

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AlirioUribeMuoz/status/1331224047997378565","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AlirioUribeMuoz/status/1331224047997378565

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**También destaca el compromiso con la paz que aún tiene el Partido FARC como actor protagónico en la firma del Acuerdo,** pese al asesinato sistemático de cientos de excombatientes que le apostaron a la paz al deponer sus armas y buscar reincorporarse a la vida civil.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1331407984795201544","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1331407984795201544

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
