Title: Lo que no se debe olvidar de Jaime Garzón, 20 años después de su asesinato
Date: 2019-08-14 18:19
Author: CtgAdm
Category: Entrevistas, Memoria
Tags: conmemoración, Homenaje, Jaime Garzon, Magnicidio
Slug: jaime-garzon-20-anos-asesinato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Jaime-Garzón.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @utcentroknight  
] 

El pasado martes 13 de agosto se cumplieron 20 años del asesinato de **Jaime Garzón,** razón por la que se llevó a cabo una conmemoración en el teatro León de Greiff de la Universidad Nacional de Colombia, sede Bogotá. Durante la conmemoración, Marisol Garzón (hermana de Jaime), dijo que el ejercicio de memoria tendría que permanecer vigente para que sus enseñanzas no se pierdan, así como que no se olvide que el suyo fue un asesinato que aún no se ha esclarecido.

### **Que Jaime sea una excusa para volver a querer este país**

En entrevista con Contagio Radio, **Marisol Garzón** manifestó que si se quiere conocer el pensamiento de Jaime, se debe acudir a las conferencias, "fundamentalmente a esa que dió en la Universidad Autónoma de Occidente de Cali" que sintetiza en buena medida su pensamiento. Por ejemplo, su hermana resalta que siempre él apostó por el diálogo para tramitar las diferencias, y "si se pierde el diálogo, hay que volver a sentarse a hablar". (Le puede interesar:["Juicio en caso Jaime Garzón revela nueva actividad criminal de la Brigada 13"](https://archivo.contagioradio.com/juicio-en-caso-jaime-garzon-revela-nueva-actividad-criminal-de-la-brigada-13/))

Una de las frases en las que enfatizó Garzón en su vida, y su hermana en su homenaje, fue la traducción hecha por el pueblo Wayúu a su lengua del artículo 12 de la Constitución que reza: **" Nadie podrá pasar por encima del corazón de nadie, ni hacerle mal en su persona aunque piense y diga diferente"**; para Jaime (y Marisol),  "con eso que nos aprendamos, salvamos a este país". Ella sentenció que "Jaime se tiene que convertir en un pretexto para volver a querer este país, para comprometernos con este país".

<iframe id="audio_39960660" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://www.ivoox.com/player_ej_39960660_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **¿Qué no se debe olvidar del caso de Jaime Garzón?**

El abogado **Alirio Uribe Muñoz, integrante del Colectivo de Abogados José Alvear Restrepo y que ha llevado el caso de Garzón**, declaró que algo clave que se debe recordar es que su homicidio fue un crimen de Estado, "y eso no es una especulación". El abogado recordó que el asesinato de Garzón fue parte de una operación para acallar su voz, tejida por manos de "grupos de ultraderecha, en coordinación con grupos paramilitares, concretamente con Carlos Castaño". (Le puede interesar: ["Jaime G. conciencia crítica de una generación"](https://archivo.contagioradio.com/jaime-garzon-conciencia-critica/))

Uribe Muñoz recordó que por el hecho ya fue condenado Castaño, y el exdirector del DAS **José Miguel Narváez, "que era el enlace con la cúpula militar y las autodefensas, es decir, que había órdenes desde el generalata colombiano hacía este grupo paramilitar** para realizar operaciones conjuntas de exterminio y aniquilamiento". En ese sentido, el abogado recordó que falta todavía investigar a uniformados, "así como ese grupo de civiles que eran 12 personas que, al parecer, diseñaban las estrategias para asesinar personas".

De igual forma, el abogado defensor de derechos humanos afirmó que era necesario recordar a Garzón por sus actos humanitarios, por ejemplo, sus numerosas acciones frente a las guerrillas para ayudar en la liberación de secuestrados. Una acción que le valió el odio por parte de la Fuerza Pública y el Gobierno, en momentos en que en el país se había puesto en marcha la Ley 40, "que era una Ley antisecuestro, que sacaba los bienes del secuestrado del mercado para evitar que fueran comercializados, y penalizaba que alguien hiciera cualquier gestión para ayudar a liberar personas".

### **Paola Turbay, y sus declaraciones sobre Carlos Castaño**

Con ocasión de la conmemoración tras los 20 años del asesinato de Garzón, en twitter fue tendencia una entrevista de Paola Turbay en la que ella señalaba que Jaime tenía demasiada información y  mucho poder, es decir, lo que sabía podía afectar a muchas personas. Por ello, declaraba que a Jaime Garzón no lo había mandado asesinar Carlos Castaño. (Le puede interesar: ["Las investigaciones que siguen por el magnicidio de Jaime G."](https://archivo.contagioradio.com/las-investigaciones-magnicidio-jaime-garzon/))

Respecto a esta declaración, Uribe Muñoz aseguró que lo que Turbay dice puede ser real "si uno piensa que los paramilitares fueron un aparato criminal paralelo para librar de responsabilidad al Estado colombiano sobre los crímenes que ordenaba". Es decir, que **"si lo que ella estaba diciendo es que Castaño era un mandadero de la fuerza pública, de los empresarios, de los industriales, de los políticos, pues está diciendo toda la verdad"**. (Le puede interesar: ["30 años de prisión pagará José Miguel Narváez por homicidio de Jaime G."](https://archivo.contagioradio.com/condena-narvaez-homicidio-garzon/))

<iframe id="audio_39960634" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://www.ivoox.com/player_ej_39960634_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
