Title: ELN afirma que Duque está haciendo trizas el proceso de paz
Date: 2018-09-28 17:46
Author: AdminContagio
Category: Paz, Política
Tags: dialogos, ELN, Iván Duque, paz
Slug: eln-trizas-proceso-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/posesion-duque.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia Colombia] 

###### [28 Sep 2018] 

En un comunicado el **Ejército de Liberación Nacional (ELN)** explicó que la decisión del presidente Duque de retirar a Venezuela como garante del proceso de paz entre esa guerrilla y el Gobierno colombiano, las exigencias hechas para el cese de hostilidades unilateralmente y el desconocimiento de acuerdos logrados, **está en la práctica, haciendo trizas el acuerdo de paz.**  (Le puede interesar: ["Por ahora, Duque no quiere la negociación sino la rendición del ELN: Víctor de Currea")](https://archivo.contagioradio.com/duque-quiere-rendicion-eln-currea/)

Aunque, Duque a lo largo de su campaña y en su discurso de posesión señaló que no acabaría con el proceso, el hecho de retirar los negociadores recientemente, y el enfoque de Desarme, Desmovilización y Reinserción con el que ha intentado asumir la negociación con el ELN afectan los diálogos de paz. A eso se suma que **tras casi dos meses en el poder, aún no concluye su evaluación de la mesa, y hace pedidos que son difíciles de cumplir por la dinámica de la guerrilla.** (Le puede interesar: ["ONU saluda la liberación de personas retenidas por el ELN"](https://archivo.contagioradio.com/onu-saluda-liberacion-eln/))

Por esto, la delegación de diálogos por parte de la guerrilla hizo un llamado a la sociedad colombiana, la comunidad internacional y al Gobierno para que no se levante la mesa de conversaciones; e igualmente **pidió a Duque no caer en provocaciones internacionales que intentan "emprender una confrontación bélica, absurda y fratricida contra Venezuela"**. (Le puede interesar: ["La sociedad tiene que presionar para que sigan las negociaciones con el ELN"](https://archivo.contagioradio.com/sociedad-negociaciones-eln/))

[ELN pide a Duque no hacer trizas el proceso de paz](https://www.scribd.com/document/389698466/ELN-pide-a-Duque-no-hacer-trizas-el-proceso-de-paz#from_embed "View ELN pide a Duque no hacer trizas el proceso de paz on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_83922" class="scribd_iframe_embed" title="ELN pide a Duque no hacer trizas el proceso de paz" src="https://www.scribd.com/embeds/389698466/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Nt2bgxQ1S99IQhzK9Yfc&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
