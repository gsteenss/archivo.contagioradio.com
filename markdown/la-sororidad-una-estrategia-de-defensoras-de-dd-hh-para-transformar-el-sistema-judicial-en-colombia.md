Title: La sororidad, una estrategia de defensoras de DD.HH para transformar el sistema judicial en Colombia
Date: 2020-07-22 18:53
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: Defensoras de Derechos Humanos, Expreso Libertad, feminismo, Sistema Judicial, sororidad
Slug: la-sororidad-una-estrategia-de-defensoras-de-dd-hh-para-transformar-el-sistema-judicial-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/1575411516_872057_1575411920_noticia_normal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: El País {#foto-de-el-país .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una de las mayores denuncias en torno al sistema judicial y penitenciario en Colombia, es la ausencia de un enfoque de género que permita dejar atrás las violencias de género de las que son víctimas las mujeres, en todos los escenarios de estos sistemas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde la población reclusa, pasando por las mujeres visitantes en las cárceles, hasta las mujeres que ejercen la defensa de los derechos humanos han manifestado los abusos de los que son víctimas por ser mujeres.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa de**l Expreso Libertad,** las abogadas defensoras de derechos humanos: Gloria Silva, Luisa Niño y Lorena Medina, denuncian las situaciones de violencia de las que han sido victimas en un rama del poder conformada mayoritariamente por hombres y la apuesta por hacer de la sororidad su mejor herramienta para transformar estos escenarios.

<!-- /wp:paragraph -->

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D206565470745677&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<!-- wp:paragraph -->

[Vea más: Programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
