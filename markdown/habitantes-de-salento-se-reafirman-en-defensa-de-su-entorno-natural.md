Title: Habitantes de Salento se reafirman en defensa de su entorno natural
Date: 2019-07-19 21:16
Author: CtgAdm
Category: Ambiente, Movilización
Tags: Mineria, Salento
Slug: habitantes-de-salento-se-reafirman-en-defensa-de-su-entorno-natural
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/salento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

------------------------------------------------------------------------

 

###### [Foto: Juan Pablo Pino ] 

En defensa del territorio los salentinos se dirigieron a la Plaza de Bolívar de Bogotá, específicamente al consejo de Estado para demandar de  esta corporación que acoja una postura de protección del medio ambiente y de rechazo a la minería, modificando así la sentencia expedida por el Tribunal Administrativo del Quindío el cual declaró nulo el acuerdo municipal que prohibía las actividades extractivas.

La movilización de jóvenes de Salento, Quindío y  ambientalistas aliados,  hacía la Plaza de Bolívar, partió  de la Universidad Jorge Tadeo Lozano, donde se realizó la Cumbre Ambiental Nacional, con el fin de ser escuchados por el Consejo de Estado, para que se prohíban las actividades extractivitas en la región. (Le puede interesar ["Ciudadanos piden anulación de títulos mineros otorgados en Cajamarca"](https://archivo.contagioradio.com/ciudadanos-piden-anulacion-de-titulos-mineros-en-cajarmarca/))

María Eugenia Pinedo, ambientalista,  Integrante de la fundación Bahareque de Salento afirma que en caso que el Consejo de Estado no falle a favor de la vida y la naturaleza,  procederán a otras instancias como la Corte Constitucional. El objetivo es conservar la vocación del municipio la cual ha sido agrícola y, esencialmente  turística.

### Salento no está solo para defender la vida natural y la vocación agrícola y turística 

Según Pinedo, los Salentinos cuentan con el respaldo y acompañamiento de ambientalistas de todo el  país que han luchado por la defensa del ambiente y de la cultura, como los  casos de Cajamarca, Tolima, donde le dijeron NO a la minería a través de la consulta popular del 2017,  también de  la población Jericó donde se adelantan procesos para que el acuerdo municipal 010 sea aprobado, además de los defensores del Páramo de Santurbán, en Santander. (Le puede interesar ["Nueva derrota en Jericó para AngloGold Ashanti"](https://archivo.contagioradio.com/nueva-derrota-en-jerico-para-anglogold-ashanti/))

Por la  defensa de la comunidad y del territorio en Salento, los ambientalistas han tomado distintas acciones como el acuerdo regional para que se prohíba la minería  en la región,  como lo fue  la semana pasada la realización de una marcha carnaval, que contó con la asistencia de más de seis mil  Salentinos.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38691598" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38691598_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
