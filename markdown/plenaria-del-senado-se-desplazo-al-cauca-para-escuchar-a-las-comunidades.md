Title: Plenaria del Senado se desplazó al Cauca para escuchar a las comunidades
Date: 2019-11-20 10:56
Author: CtgAdm
Category: Comunidad, Política
Tags: Cámara de Representantes, Cauca, Senado, víctimas, violencia
Slug: plenaria-del-senado-se-desplazo-al-cauca-para-escuchar-a-las-comunidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/EJv9T54W4AAatBO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:]@SenadoGovCo 

Este 19 de octubre alrededor de 52 Congresistas, entre Senadores y Representantes a la Cámara, viajaron para sesionar en Santander de Quilichao, Cauca, debido a la grave crisis humanitaria que viven las comunidades indígenas en los últimos meses; la decisión fue apoyada por algunos funcionarios a pesar de que en el interior del Senado se decía que no habían garantías para el traslado. (Le puede interesar: [Comunidades del Cauca no van a dejar que el terror se apodere de sus territorios](https://archivo.contagioradio.com/comunidades-cauca-no-terror/))

### "sabemos que hubo un intento de saboteo  para que no se diera la audiencia en Cauca"

La iniciativa se dio a inicios de noviembre cuando el Senado aprobó la proposición de  la representante María José Pizarro, para viajar al Cauca en respuesta a la ausencia de garantías de seguridad y vulneración de derechos humanos  en esta región, desplegando desde ese mismo día la logística que permitiera el desplazamiento de los funcionarios, ministros y comisionados. (Le puede interesar:[Con Mestizo Órtiz ya son 36 los líderes sociales asesinados en Cauca](https://archivo.contagioradio.com/36-los-lideres-sociales-asesinados-en-cauca/))

> Hay un interés de parte de la ministra del interior [@NancyPatricia\_G](https://twitter.com/NancyPatricia_G?ref_src=twsrc%5Etfw) y el nuevo ministro de defensa [@CarlosHolmesTru](https://twitter.com/CarlosHolmesTru?ref_src=twsrc%5Etfw) en sabotear la presencia de [@SenadoGovCo](https://twitter.com/SenadoGovCo?ref_src=twsrc%5Etfw) y de la [@CamaraColombia](https://twitter.com/CamaraColombia?ref_src=twsrc%5Etfw) en el departamento del Cauca.[@PizarroMariaJo](https://twitter.com/PizarroMariaJo?ref_src=twsrc%5Etfw) [@PoloDemocratico](https://twitter.com/PoloDemocratico?ref_src=twsrc%5Etfw)
>
> — Alexander López Maya (@AlexLopezMaya) [November 18, 2019](https://twitter.com/AlexLopezMaya/status/1196577669195939840?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Según Pizarro dos días antes del viaje se empezaron a difundir rumores al interior de la Cámara y Senado que llenaron de miedo a algunos congresistas, reflejo de ello fue la asistencia que se vio en la plenaria, "nosotros sabemos que hubo un intento de saboteo por parte de la Ministra de Interior y el Ministro de Defensa para la realización de esta audiencia, y a pesar de que todo estaba organizado desde día atrás en Senado y Cámara, ellos se pronunciaron diciendo que no habían condiciones de seguridad para ir".

A la sesión decidieron asistir 30 senadores y 17 representantes, además del Alto Comisionado para La Paz Miguel Ceballos, delegados de la Defensoría del Cauca, el Consejero Presidencial Rafael Guarín, la Directora de Prosperidad Social Susana Correa, el alcalde de Santander de Quilichao, entre otros, "estuvimos quienes consideramos que si no existen garantías para la comunidad del Cauca tampoco deben existir para nosotros. Es decir si las comunidades son capaces de sobrevivir allá y hay una inversión en seguridad y defensa, lo más lógico es que podamos viajar todos y escuchar a las comunidades que son los que están en medio del fuego cruzado", añadió Pizarro.

 

> El [\#CongresoEnCauca](https://twitter.com/hashtag/CongresoEnCauca?src=hash&ref_src=twsrc%5Etfw) condecora a la Guardia Indígena por su labor en defensa de la vida, los derechos humanos y el territorio en el norte del Cauca. [@SenadoGovCo](https://twitter.com/SenadoGovCo?ref_src=twsrc%5Etfw)
>
> ¡Cxha, cxha! ¡Fuerza, fuerza! [pic.twitter.com/oZ4W2tnypj](https://t.co/oZ4W2tnypj)
>
> — Feliciano Valencia ? (@FelicianoValen) [November 19, 2019](https://twitter.com/FelicianoValen/status/1196830765046218752?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
La congresista agregó también que "las comunidades nos esperaban y lo sentimos mucho por los que se dejaron amedrentar y no viajaron, pudo ser un hito histórico, y más faltando pocos días para el paro, que el Congreso escuchara directamente a las comunidades, esto hubiera tal vez calmado los ánimos, haciendo un papel de mediación en este momento de crisis".

Algunos de las acciones tratadas  en esta audiencia fueron, inicialmente escuchar a la comunidad para concretar acciones, seguido por la intervención del Estado para presentar  proyectos sociales, y por último desarrollo de atención integral para las víctimas. (Le puede interesar: [Su compromiso con la educación hoy tiene bajo amenaza a profesores del Cauca](https://archivo.contagioradio.com/compromiso-con-la-educacion-hoy-tiene-bajo-amenaza-a-profesores-del-cauca/))

Así mismo en el encuentro se hizo un reconocimiento a la Guardia Indígena del Cauca, a quienes el [Congreso de la República entregó la condecoración en el grado]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}[Cruz]{.css-901oao .css-16my406 .r-1qd0xha .r-vw2c0b .r-ad9z0x .r-bcqeeo .r-qvutc0} [Comendador,]{.css-901oao .css-16my406 .r-1qd0xha .r-vw2c0b .r-ad9z0x .r-bcqeeo .r-qvutc0}[considerando que juegan  un papel  gran importancia para la protección y defensa de los  pueblos indígenas. (Le puede interesar: [ACIN denuncia aparición de nuevo panfleto que amenaza a la guardia indígena](https://archivo.contagioradio.com/acin-panfleto-guardia-indigena/))]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

 

 

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
