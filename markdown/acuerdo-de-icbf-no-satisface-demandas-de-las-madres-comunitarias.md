Title: Acuerdo con ICBF no satisface demandas de las madres comunitarias
Date: 2016-04-15 16:32
Category: Movilización, Nacional
Tags: ICBF, Madres Comunitarias, paro madres comunitarias
Slug: acuerdo-de-icbf-no-satisface-demandas-de-las-madres-comunitarias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Madres-Comunitarias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Villanueva 24 Horas ] 

###### [15 Abril 2016]

Tras once días de paro, **65 mil madres comunitarias** logran llegar a acuerdos poco satisfactorios en relación a las [[demandas que inicialmente le hicieron al ICBF](https://archivo.contagioradio.com/7-mil-madres-comunitarias-en-paro-indefinido/)] en seguridad social y pensional, contratación y nutrición de niños y niñas.

Pese a que las madres comunitarias exigían ser contratadas indefinidamente por el ICBF, se acordó que las Empresas Administradoras de Servicios las contrataran hasta el 31 de octubre de este año, y luego fijaran con ellas, **contratos entre el noviembre del 2016 y julio de 2018**.

En salud y pensiones se acordó la instalación de mesas técnicas para revisar uno a uno el pago de cotizaciones del Sistema General de Seguridad Social, correspondiente a enero de este año. Frente a las **condiciones especiales de salud de algunas de las madres que no pueden continuar en ejercicio**, delegados de la Procuraduría, la Defensoría, el Ministerio del Trabajo, el ICBF y Sintracihobi, estudiarán sus casos para solucionar su situación.

Se acordó además que la Comisión Accidental del Senado presente el 20 de julio de este año, un **proyecto de ley que garantice las pensiones de las madres comunitarias**, especialmente las de aquellas que han superado la edad de pensión y de quienes presentan problemas de salud que les impidan continuar en ejercicio.

En materia nutricional se acordó establecer mesas de trabajo integradas por funcionarios del ICBF, madres comunitarias y nutricionistas por cada centro zonal, para **convenir los menús de los niños y las niñas de acuerdo a las necesidades calóricas de cada región**, así como revisar la entrega de alimentos por parte de las Empresas  Administradoras de Servicios, para verificar que tanto la cantidad como calidad sean las adecuadas.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
