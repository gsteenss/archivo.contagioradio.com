Title: Policía y Ejército reprimen movilización en Antioquia
Date: 2016-05-30 13:22
Category: Paro Nacional
Tags: Antioquia, Paro Nacional
Slug: policia-y-ejercito-reprimen-movilizacion-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Ríos_Vivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: M. Rios Vivos 

###### [30 May 2016]

Desde tempranas horas de la mañana cerca de 100 personas integrantes del Movimiento Ríos Vivos de Antioquia salieron para encontrarse con otros manifestantes el municipio de Puerto Valdivia, sin embargo, la constante represión y retenes por parte de la Policía Nacional y el Ejército ha impedido que lleguen a su punto de encuentro, como lo denuncia Isabel Cristina Zuleta, vocera del Movimiento.

Zuleta señaló que, el domingo llegaron cerca de 400 integrantes del Congreso de los Pueblos, quienes desde el sitio conocido como Los Cristos, tuvieron que trasladarse hacia el  municipio de Tarazá por presiones de la fuerza pública. Además se indica que  se ha tenido que cambiar varias veces los puntos de concentración por esas presiones de los militares y los policías.

Las exigencias puntuales por las que marcha el movimiento Ríos Vivos de Antioquia, tienen que ver con su rechazo a los desalojos forzosos por el proyecto hidroeléctrico Hidroiutuango, también exigen que se cumpla con los lineamientos de la licencia ambiental que tiene EPM de manera se superen los vacíos de la licencia, y finalmente exigen que se cree una Comisión nacional de represas.

<iframe src="http://co.ivoox.com/es/player_ej_11714950_2_1.html?data=kpakk5mdeZGhhpywj5WYaZS1kpaah5yncZOhhpywj5WRaZi3jpWah5yncarnwsfSzpCntsrn1c7bw5C%2Buc3Z1caYj5C2aaSnhqax0diPmsrq0Niah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
