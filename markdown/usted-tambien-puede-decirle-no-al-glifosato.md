Title: Usted también puede decirle NO al glifosato
Date: 2015-05-10 11:53
Author: CtgAdm
Category: Entrevistas, Nacional
Tags: AIDA, Asociación Interamericana para la Defensa del Ambiente, Astrid Puentes, Consejo Nacional de Estupefacientes, Corte Constitucional, Defensoría del Pueblo, Fiscalía General de la Nación, Glifosato, Ministerio de Agricultura, Ministerio de Salud, Monsanto, OMS, Organización Mundial de la Salud
Slug: usted-tambien-puede-decirle-no-al-glifosato
Status: published

##### [Foto: www.senalradiocolombia.gov.co]

<iframe src="http://www.ivoox.com/player_ek_4473397_2_1.html?data=lZmklZide46ZmKiakpyJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmdToxsmY1sbRpsqZmKfS0JDUucbYxpDRx8jNts3ZjLO8jcbQb8jgysvc1cbYs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Astrid Puentes, codirectora de la Asociación Interamericana para la Defensa del Ambiente, AIDA.] 

**El próximo 14 de mayo, en el Consejo Nacional de Estupefacientes se tomará una decisión sobre la suspensión, o no de fumigaciones con glifosato c**omo estrategia para combatir los cultivos ilícitos.

El Ministerio de Salud, el Ministerio de Agricultura, la Defensoría del Pueblo, la Fiscalía, la Corte Constitucional, entre otros entes gubernamentales han recomendado dejar de usar el glifosato, debido a los daños que causa al ambiente, y específicamente a la salud, de acuerdo al reporte de la Organización Mundial de la Salud (OMS), donde se evidencia que este herbicida puede causar cáncer.

Este tipo de fumigaciones también **trae graves consecuencias al ambiente.** Astrid Puentes, codirectora de la Asociación Interamericana para la Defensa del Ambiente, AIDA, asegura que el glifosato no solo destruye las hojas de las plantas de coca, sino que además a**fecta los cultivos de pan coger, contamina el agua y el suelo, afectando poblaciones de peces y anfibios**,  teniendo en cuenta que Colombia es uno de los países con mayor diversidad de especies de peces en el mundo.

Así mismo, de acuerdo a los estudio de la AIDA, se ha demostrado que el glifosato no es la única estrategia para hacerle frente a los cultivos ilícitos, según Puentes, **desde el narcotráfico se han ideado varias formas para que esta sustancia no afecte los cultivos.** Entre ellas, se ha detectado que pronto se realiza la fumigación se están lavando las hoja para que no se muera la planta, también rocían los cultivos con otro tipo de químicos que hacen que el glifosato no se pegue a la hoja y no tenga efecto, además, están modificando genéticamente la semilla para que sea resistente al glifosato, de hecho **“Monsanto tiene semillas resistentes al glifosato”**, afirma la codirectora de la AIDA.

Es por eso, que la **AIDA y otras organizaciones de Estados Unidos y Colombia se unieron para desarrollar una campaña** invitando a las personas a que firmen a través del portal web, change.org y le digan al presidente Juan Manuel Santos y al Consejo Nacional de Estupefacientes que detenga el uso de glifosato, y se piensen otras alternativas para detener los cultivos ilícitos.

**Si usted desea hacer parte de esta campaña, puede ingresar [aquí.](https://www.change.org/p/suspendan-ya-fumigaciones-a%C3%A9reas-con-glifosato-y-otros-qu%C3%ADmicos-da%C3%B1inos-en-colombia-minjusticiaco-juanmansantos-nofumigaci%C3%B3n)**
