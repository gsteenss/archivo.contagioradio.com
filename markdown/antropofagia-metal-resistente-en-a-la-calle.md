Title: Antropofagia, Metal resistente en "A la Calle"
Date: 2015-08-18 12:30
Category: Sonidos Urbanos
Tags: Antropofagía metal antireligioso y resistencia, Pornomiseria
Slug: antropofagia-metal-resistente-en-a-la-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/pornomiseria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_6652970_2_1.html?data=l5uilJ6bdI6ZmKiakpaJd6KkkYqgo5qbcYarpJKfj4qbh46ljoqkpZKUcYarpJKu0NnWs9Hjx8bUy8aJd6Kfrsrhw9GPpc%2FoypLfx9HNq8rj1NSY25DWqdTd1NnS0MjNpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Antropofagia, metal resistente en "A la Calle"] 

###### [18, Ago, 2015] 

En "A la calle" nos acompaño la banda de metal bogotana "Antropofagía", Kamo, su volcalista, comparte con nosotros los orígenes e influencias de la agrupació[n, asi como sus proyecciones y metas en el corto y largo plazo. La banda esta conformada por Alexander Aldana (Guitarrista Lider), Juan Pablo Barriga (Guitarrista y coros), Cristian Beltran (Bajista), y Leonardo Forero (Baterista).]{#descripcion_audio}

[![Antropofagia A la calle](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/unnamed-1-e1439918783666.jpg){.aligncenter .size-full .wp-image-12305 width="588" height="365"}](https://archivo.contagioradio.com/antropofagia-metal-resistente-en-a-la-calle/unnamed-1-4/)

##### [Luis Miguel, Kamo (Antropofagia) y Angel en "A la Calle"] 

[Con inicio en 2013, "Antropofagia" busca con sus letras visibilizar las inequidades que predominan en el mundo, en particular en Colombia; a partir de las élites, los sistemas políticos y económicos desarrollados a lo largo de la historia, haciendo así un llamado a la memoria y a la lucha de los pueblos. La banda plantea una crítica fuerte a las religiones, considerando que han servido como entes de legitimación de la explotación y dominación de la clase alta sobre la clase baja.]{#descripcion_audio}

Facebook: facebook.com/Antropofagia

Playlist: [Reverbnation](https://www.reverbnation.com/antropofagia6?profile_view_source=header_icon_nav)
