Title: Asesinados dos integrantes del Sindicato de Trabajadores Agrícolas en el Meta
Date: 2017-03-06 16:44
Category: DDHH, Nacional
Tags: Asesinatos, campesinos, Mesetas, Meta, SINTRAGRIM
Slug: asesinados-dos-integrantes-del-sindicato-de-trabajadores-agricolas-en-el-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/asesinatos-lideres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pueblo] 

###### [06 Mar. 2017] 

Este domingo** fueron asesinados dos hermanos integrantes del Partido Comunista y del Sindicato de Trabajadores Agrícolas Independientes del Meta (SINTRAGRIM)**. José Antonio Anzola Tejedor y de su Hermana Luz Ángela Anzola Tejedor, eran reconocidos por su trabajo en la región y José Antonio era integrante de la organización Cumbre Agraria.

Los hechos ocurrieron en la Vereda el Gobernador, en el Municipio de Mesetas, departamento del Meta, con dos horas de diferencia. José fue asesinado hacia las 5 de la tarde y Luz hacia las 7 de la noche.

Según **David Martínez, representante legal de SINTRAGRIM** estos dos integrantes del sindicato **“eran unas personas honestas, trabajadoras, muy humildes.** **Las personas que llegaron a la casa de la señora Luz Ángela estaban encapuchados y armados**. Llegaron en la noche se identificaron como grupos ilegales. A Luz Ángela la acribillan delante de su hijo y de su familia”.

Martínez cuenta que estas dos personas no habían sido amenazadas “no podemos decir que habían amenazas, pero lo que si podemos decir es que en la zona hay presencia de grupos al margen de la ley, y ya lo pusimos en conocimiento de las autoridades en un consejo de seguridad”. Le puede interesar: [Asesinada lideresa de Asokinchas en Medellín](http://Asesinada%20lideresa%20de%20Asokinchas%20en%20Medellín)

En dicho consejo de seguridad, las instituciones locales y nacionales se comprometieron a brindarles seguridad, sin embargo Martínez cuenta que **a la fecha “no ha habido presencia en la región de Fuerza Pública.** Tampoco en el sitio de los hechos. Todos los compromisos quedaron en el tintero” agregó Martínez.

**SINTRAGRIM repudió este hecho e instó a las instituciones a realizar las respectivas investigaciones,** para dar con los responsables de este hecho que hoy enluta a esta organización y a la familia de José Antonio y Luz Ángela, quien se vio en la obligación de desplazarse de su vereda, por miedo a represalias. Le puede interesar: [117 líderes fueron asesinados durante 2016](https://archivo.contagioradio.com/117-lideres-fueron-asesinados-durante-2016/)

“Esperamos que la Fiscalía cumpla con su tarea de investigar quiénes fueron los que cometieron estos hechos. Nosotros ya haremos las respectivas denuncias, hablar por los campesinos que estamos en la región, pero la tarea es de las instituciones” dijo el representante legal de SINTRAGRIM.

Adicionalmente, **Martínez realizó un llamado a la Policía, institución que en un comunicado de prensa manifestó que José y Luz eran ladrones** “en el comunicado dicen que a ellos los mataron supuestamente por ir a robar, por lo que nosotros hemos dicho es que ellos eran campesinos y se tergiversa el mensaje con este tipo de comunicados. Policía debería esperar la investigación”. Le puede interesar: [120 defensores de DDHH asesinados en 14 meses en Colombia: Defensoría](https://archivo.contagioradio.com/120-defensores-asesinados-defensoria/)

Desde 2013 y en reiteradas ocasiones el Sindicato de Trabajadores Agrícolas Independientes del Meta (SINTRAGRIM), ha denunciado los hostigamientos de los cuales han sido víctimas por parte del Ejército Nacional adscritos a la Cuarta División en el Meta, en las que reiteradas veces se han apropiado de productos agrícolas de las comunidades y han realizado bombardeos indiscriminados sobre fincas campesinas, afectando así a sus pobladores. Le puede interesar:[ Asesinatos de líderes sociales son práctica sistemática: Somos Defensores](https://archivo.contagioradio.com/asesinatos-de-lideres-son-practica-sistematica-33507/)

<iframe id="audio_17383544" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17383544_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
