Title: La historia y la Comisión Histórica
Date: 2015-05-14 12:18
Author: CtgAdm
Category: Cesar, Opinion
Tags: César Torres Del Río, colombia, comision de la verdad, Comision historica, Juan Manuel Santos, memoria, Verdad historica
Slug: la-historia-y-la-comision-historica
Status: published

###### Bogotá, 9 de abril 1948.[   - ]

#### **[César Torres Del Río](https://archivo.contagioradio.com/cesar-torres-del-rio/) ** 

La historia está hoy en el centro del debate. Lo que más llama la atención es que ahora su voz se escucha en los estrados, en la academia, en los medios, en el Congreso, en sectores empresariales, en la calle y en La Habana. En otros términos, se desencajonó, se salió de madre, navegó allende[ ]{.Apple-converted-space}los mares y ahora está fondeada en el puerto de la Corte Penal Internacional, no para permanecer allí sino para demostrar que tiene fuerza suficiente para no dejarse atornillar con los incisos y parágrafos del derecho internacional.

Eso sí, la verdad histórica nunca se conocerá totalmente; se trata de una aspiración, de una idea-meta sin más. La verdad se puede construir, claro, pero siempre[ ]{.Apple-converted-space}será parcial y relativa. Por relativo se debe entender que hay variados enfoques, métodos, que conllevan a diversas interpretaciones; de allí que algún historiador dijera que “Los muertos de La Bastilla nunca serían enterrados”. Quienes aspiran a una verdad histórica, y además de consenso, inevitablemente nos conducen al mundo de la historia patria y de sus héroes, a un sellamiento de la historia.

La historia hace parte de las ciencias sociales y como tal se ubica en el terreno científico: no predice el futuro, proyecta tendencias únicamente. Pero tampoco se queda en el pasado; al contrario de lo que se piensa, la historia es el estudio del devenir. El presente es, pues, objeto de estudio y escenario de confrontación histórica. De allí la intensidad de la polémica actual.

El gobierno de Juan Manuel Santos creó una comisión para que estudiara históricamente el conflicto social armado en Colombia. El resultado se plasmó en 16 interpretaciones, 14 de los comisionados y 2 de los coordinadores; se ha visto en ello un obstáculo para avanzar en[ ]{.Apple-converted-space}la terminación del conflicto social armado. El analista León Valencia incluso ha afirmado en una reciente columna que la Comisión fracasó porque no precisó las[ ]{.Apple-converted-space}*responsabilidades* históricas, que era lo esencial; cuán equivocado está.

Como hemos dicho en diferentes espacios públicos, no puede haber historia de consensos, ni *una* historia; claro, de ésta última tenemos experiencias, por ejemplo la historia “oficial” de la revolución rusa impuesta mundialmente por el stalinismo durante más de 60 años, o la historia “patria” de Henao y Arrubla, sobre héroes y tumbas.[ ]{.Apple-converted-space}Nuestro pasado es lo más actual en términos del presente. Y tanto uno como otro han enfrentado a múltiples actores lo que nos lleva, de nuevo, a variadas interpretaciones, a víctimas de distintas características[ ]{.Apple-converted-space}- en tanto que sujetos políticos -, a memorias individuales y colectivas, a visiones del mundo (culturales, religiosas e ideológicas). De allí que la “panteonización”, la “patrimonialización” de la Memoria a la que asistimos hoy es arma de doble filo: por una parte, los espacios memorísticos son vida para las víctimas; por otra, las recluye y unifica. Al “panteonizar” la diversidad de las víctimas se oculta, se pierde; hay víctimas antifascistas y antisistema, de paramilitares y de guerrillas, del modelo económico y del mercado, de agentes estatales y de la delincuencia común, de las religiones y de sus fundamentalismos, del terrorismo y de la democracia …

*Una* historia, un consenso, mata la vida y sus posibilidades. *Una* historia ofrece *una* víctima, sin más. La historia no se encarga de juzgar, ni de establecer responsabilidades; ese asunto es de la justicia. Verdad histórica y verdad judicial se relacionan; pero no pueden volverse una. Las 16 interpretaciones históricas de la Comisión de expertos, tendrán que ser reinterpretadas hoy y mañana. Unas y otras le servirán a la justicia, a *las* víctimas, a *las* memorias. Una nueva escritura de la historia se abre paso.
