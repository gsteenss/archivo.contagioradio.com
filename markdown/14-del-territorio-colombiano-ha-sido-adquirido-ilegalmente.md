Title: 14% del territorio colombiano ha sido adquirido ilegalmente según AI
Date: 2015-11-05 12:33
Category: DDHH, Nacional
Tags: Amnistía Internacional, Desplazamiento forzado, empresas extractivas, Erika Guevara Rosas, Restitución de tierras
Slug: 14-del-territorio-colombiano-ha-sido-adquirido-ilegalmente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/audiencia-CIDH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto: elpueblo 

###### [4 nov 2015] 

El más reciente informe de Amnistía Internacional, titulado "Colombia: restituir la tierra, asegurar la paz: los derechos territoriales de las comunidades indígenas", concluye que el gobierno colombiano debe ante poner el derecho de las comunidades indígenas y afrodescendientes  sobre los intereses económicos de las multinacionales.

Según la ONIC  existen 102 pueblos indígenas en Colombia ,18 de ellos están en peligro de extinción y 1,4 millones indígenas, es decir, el 70% viven en zonas rurales y resguardos reconocidos oficialmente que incluyen más del 28% del país.

Erika Guevara Rosas, directora del Programa Regional para América de Amnistía menciona que **“cualquier acuerdo de paz carecerá de sentido a menos que los derechos de las comunidades indígenas y afrodescendientes a regresar a sus tierras y a decidir cómo se usan tengan prioridad sobre el deseo de las empresas de explotar esas tierras en su propio beneficio”.**

De acuerdo con el informe, 6 millones de personas han sido objeto de desplazamiento forzado de sus hogares como consecuencia del conflicto armado, y al menos 8 millones de hectáreas de tierras, aproximadamente el 14% del territorio del país han sido abandonadas o adquiridas ilegalmente.

Los problemas más comunes para acceder a la tierra, principalmente son las amenazas de muerte contra líderes comunitarios por parte de las bandas criminales paramilitares como Águilas Negras, por lo que miles de comunidades se ven obligadas a abandonar sus terrenos en repetidas ocasiones después de años de amenazas y homicidios de las fuerzas de seguridad, indica el informe.

**Otra causa que genera el desplazamiento es la  minería**, actualmente el gobierno Colombiano ha otorgado licencias a empresas mineras con el fin de explotar tierras y recursos naturales en los territorios de comunidades indígenas y afrodescendientes donde se licita sin en el consentimiento de los pobladores.

En 2012 el gobierno creó un programa de restituciones de tierras y reparación para algunas de las víctimas, el documento menciona que **“el número de personas que reclaman la restitución de sus tierras que han podido regresar o conseguir la propiedad legal de esas tierras ha sido relativamente escaso, y las autoridades no han garantizado un apoyo efectivo a quienes han podido regresar”**

Específicamente hacen referencia a la Ley 1753, aprobada por el Congreso en junio de 2015, con la que se podría facilitar las operaciones de las empresas en tierras adquiridas indebidamente o en tierras sobre las cuales se ha conseguido el control mediante abusos y violaciones de derechos humanos, incluso en territorios que son propiedad colectiva de comunidades indígenas y afrodescendientes.

“Al no apoyar de manera efectiva el retorno sostenible de estas comunidades, las autoridades colombianas sencillamente están condenando a miles de personas a la pobreza y a la persistencia de violaciones y abusos de derechos humanos”, agregó Erika Guevara Rosas.

Con el informe, se espera que se lleve a cabo **investigaciones efectivas sobre los abusos y violaciones de derechos humanos** cometidos por las fuerzas de seguridad, los paramilitares y los grupos guerrilleros que dieron lugar a la apropiación indebida de tierras mediante la violencia.

[COLOMBIA: RESTITUIR LA TIERRA, ASEGURAR LA PAZ: LOS DERECHOS TERRITORIALES DE LAS COMUNIDADES INDÍGENAS](https://www.scribd.com/doc/288636719/COLOMBIA-RESTITUIR-LA-TIERRA-ASEGURAR-LA-PAZ-LOS-DERECHOS-TERRITORIALES-DE-LAS-COMUNIDADES-INDIGENAS "View COLOMBIA: RESTITUIR LA TIERRA, ASEGURAR LA PAZ: LOS DERECHOS TERRITORIALES DE LAS COMUNIDADES INDÍGENAS on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_62358" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/288636719/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-zOUUtxaBxJmv4Q9h3QMm&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>
