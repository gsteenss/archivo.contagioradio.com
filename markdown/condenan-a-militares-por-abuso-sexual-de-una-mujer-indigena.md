Title: Condenan a militares por abuso sexual de una mujer indígena
Date: 2016-09-15 14:39
Category: Mujer, Nacional
Tags: Abuso sexual, Ejército Nacional, genero, indígenas, Violencia contra las mujeres, violencia sexual
Slug: condenan-a-militares-por-abuso-sexual-de-una-mujer-indigena
Status: published

###### Foto:  confidencialcolombia.com 

###### [15 Sep 2016]

Dos militares integrantes de la Tercera Brigada del Ejército Nacional **deberán pagar entre 13 a 27 años de prisión por el abuso sexual de una mujer indígena de 22 años**, ocurrida el 10 de junio de 2009, en el municipio de Jambaló Cauca.

Se trata de los militares **Héctor Lujan Sánchez y Carlos Alberto Pulgarín**, quienes fueron condenados el pasado 9 de septiembre de 2016, por el Juzgado Primero Penal del Circuito Especializado de Popayán  que los encontró como responsables a título de coautores del delito de Acceso Carnal Violento en Persona Protegida.

De acuerdo con la Corporación  Justicia y Dignidad, el hecho ocurrió cuando la mujer se dirigía a su casa, en zona rural del municipio de Jambaló, cuando miembros del Ejército uniformados y portando armas, la retuvieron y le decomisaron su cédula. Luego la **accedieron carnalmente de manera violenta, y para que no los denunciara el Sargento Torres le ofreció cien mil pesos.**

Pese a las amenazas y el chantaje, la mujer denunció y fue amenazada junto a su familia, por lo que terminaron víctimas de desplazamiento forzado. Con ello, la investigación penal empezó estuvo a cargo del fiscal seccional de Silvia Cauca, luego pasó a la **Fiscalía 38 Especializada de la Unidad Nacional de Derechos Humanos donde pasó por seis fiscales.** Uno de ellos decidió archivar la investigación en octubre de 2011, al dar total credibilidad a los agresores, concluyendo que la mujer tenía conducta licenciosa*,* y afirmó que la víctima padecía un trastorno psicológico, pese al dictamen del Centro de Atención a víctimas de violencia sexual que certificó los hallazgos de abuso sexual.

Al encontrarse **irregularidades en la actuación de esa unidad de la Fiscalía,** se logró que nuevamente se abriera el caso pero mantuvo 3 años de total inactividad. Luego el caso fue reasignado a la fiscal 145 Seccional de Bogotá Destacada en Género, quien adelantó las **pruebas necesarias para concluir con la responsabilidad penal de los militares en el crimen.**

Tras la falta de garantías para el Acceso a la Justicia de la víctima, la Comisión Interamericana de Derechos Humanos le **otorgó medidas tanto a la víctima como a su familia.**  Las irregularidades en el caso fueron tan evidentes que, de acuerdo con la Corporación Justicia y Dignidad, el Magistrado Víctor Manuel Chaparro de la Sala Penal del Tribunal Superior de Cali, dijo que “la Fiscalía había vulnerado el derecho de petición de la mujer indígena; el derecho fundamental de Acceso a la Administración de Justicia; también revictimizó y había prejuzgado, ya que no creyó en la victima y le dio total credibilidad a los militares”.

Cabe recordar que **la indígena y su madre fueron hostigadas y amenazadas constantemente  **no solo por parte de los agresores sino también por medio de llamadas de la policía judicial, “incluso a altas horas de la noche en las que se les amenazaba con cobrarles el costo de las audiencias si no asistían a ellas; Así mismo les manifestaban recurrentemente que no tenían abogados que las representaran y que les iban a designar un defensor público”, dice la Corporación.

Aunque fueron constantemente revictimizados, tras 7 años de lucha la joven indígena y su familia lograron que los agresores fueran condenados y **el próximo 19 de diciembre, se conocerá la sanción que deberán pagar.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
