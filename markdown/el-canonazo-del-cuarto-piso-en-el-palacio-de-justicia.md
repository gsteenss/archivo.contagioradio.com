Title: El cañonazo del cuarto piso en el Palacio de Justicia
Date: 2015-11-19 12:32
Category: Opinion, Ricardo
Tags: 6 de noviembre de 1985, bomba cuarto piso palacio de justicia, ejercito en el palacio de justicia, Palacio de Justicia
Slug: el-canonazo-del-cuarto-piso-en-el-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/bomba-palacio-de-justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: tomada de internet] 

#### **[[Ricardo Ferrer Espinosa](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/) - [@ferr\_es](https://twitter.com/ferr_es?lang=es)]** 

###### [19 de Nov 2015] 

[El 6 de noviembre de 1985, vimos otro desastre nacional, donde la razón humana, brilló por su ausencia \[1\].  Desde el principio, la toma del Palacio de Justicia fue un desastre militar para el M-19, y la contratoma, dirigida por las FF MM, derivó en combate sin cuartel, sin cautivos.]

[Entre miles de imágenes y explicaciones,]**siempre me pregunté los motivos que tuvo el Ejército de Colombia para realizar el disparo al cuarto piso del edificio**[, un hecho absurdo, transmitido por televisión para toda Colombia.]

[Hernando Tapias Rocha, el 6 de noviembre de 1985 era presidente de la Corte de Casación Civil. En un relato publicado por su nieta, describe lo que vivió desde adentro del palacio, cuando era rehén:  ]**“El método del ejército para cumplir con la orden del gobierno fue tan burda como escalofriante: varios tanques Cascabel apostados en la Plaza dispararon. A punta de cañonazos abrieron varios boquetes en las paredes y en la puerta principal del Palacio”** [\[2\].]

[Es necesario recordar que en noviembre de 1985 no existía la tecnología suficiente para identificar a distancia y en tiempo real lo que ocurría dentro del edificio.]**Era imposible saber quién estaba detrás de cada una de las paredes.** [En consecuencia, la decisión de dirigir un cañonazo al cuarto piso rompió el principio de diferenciación entre combatientes y personal civil. \[3\].  Estremece pensar que el rescate de rehenes no era la prioridad de los comandos militares.]

El M-19 y las FF MM, omitieron todos sus deberes con los cautivos. Todos escuchamos la petición angustiosa de cesar el fuego, hecha por los magistrados, en las emisoras radiales. Pero sordo, el Estado, nunca abrió  un canal o vocero oficial para negociar la salida de los rehenes. Los únicos argumentos en esas 28 horas fueron el plomo y el fuego.

La televisión transmitió en directo los disparos que hizo un tanque Cascabel, que atravesaron las paredes y dejaron un boquete visible en el cuarto piso. Una imagen más, de poca trascendencia entre tantas explosiones, humo, fuego y absurdo.

[Esa misma semana pude conversar con un amigo que recientemente había terminado su servicio militar y le pregunté por el daño que podía ocasionar el disparo del tanque Cascabel \[4\].]

[El ex soldado me explicó que la carga disparada era especial para penetrar el hormigón. Con paciencia, me dibujó el principio de cono acumulativo y me mostró que hay un punto donde se acumula toda la fuerza del explosivo. Luego de perforar el muro, con un hueco relativamente pequeño, como un soplete pero a alta velocidad, el explosivo entra con calor de cientos de grados, gran sonido y presión, suficientes para matar a todo el que esté cerca. Si es un recinto cerrado, me explicó el exmilitar, toda la presión y el calor de la explosión se concentran. La gente queda como gelatina. Nadie aguanta algo como eso.]

Entre miles de disparos, el cañonazo al cuarto piso es por si mismo, un crimen de guerra, injustificable en cualquier argumento militar.

La primera vez que fui a Bogotá, a un encuentro de periodismo comunitario, aproveché para visitar la plaza de Bolívar y observar el palacio. El hueco del cañonazo al cuarto piso estaba cubierto por una bandera de Colombia.  Alguien tenía mucha vergüenza y la encubría con el pendón tricolor.

[La frase final del magistrado Hernando Tapias Rocha, en la entrevista que hizo para su nieta, resume la legalidad colombiana:]**“Unos días más tarde demolieron el edificio, y con él todas las pruebas”.**

###### [\[1\]]*[El]* *[Espectador 16 Jun 2013 ]***[El reporte secreto del Palacio de justicia ](http://bit.ly/1NecwYd)** 

###### [\[2\] [Laura Gallo Tapias 06.11.2012 / Recuerdos de un sobreviviente del Palacio](http://bit.ly/1dpLzjP)- ] 

###### [\[3\]][[ Comité Internacional de la Cruz Roja. PRINCIPIO DE DISTINCION. ]](http://bit.ly/1PCnRBE) 

###### [\[4\] -[Tanques Cascabel 1985](http://bit.ly/1QPZnnG) - ] 

[-]
