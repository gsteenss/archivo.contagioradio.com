Title: Policia invade escuela del Movimiento sin tierra MST en Brasil
Date: 2016-11-04 13:07
Category: DDHH, El mundo
Tags: abuso autoridad, Brasil, MST, policia
Slug: policia-invade-escuela-mst
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/30140710324_47faa885fa_b.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:MST 

##### 4 Nov 2016 

Sin ordenes judiciales y de manera violenta uniformados de la Policía Civil de Paraná y la de Sao Paulo, **irrumpieron la mañana de este viernes en las instalaciones de la Escuela Nacional Florestan Fernandes**, perteneciente al Movimiento de los Trabajadores Rurales Sin Tierra MST, ubicada en Guararema.

Durante la acción policial, para la que **fueron movilizados 10 vehículos que trasladaban agentes sin identificación**, los uniformados dispararon al aire en la entrada de la institución donde se encontraban algunos civiles, reteniendo a dos militantes de la organización social que horas mas tarde fueron liberados.

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F10153888693850812%2F&amp;width=500&amp;show_text=true&amp;height=379&amp;appId" width="500" height="379" frameborder="0" scrolling="no"></iframe>

El operativo representa un “**abuso de autoridad, violencia innecesaria, ilegal**”, como lo asegura Giane Alves, abogada del MST, accionar que revierte mayor gravedad al comprobarse por parte de algunos de los militantes del movimiento, que **las balas utilizadas eran letales y no de goma**. Le puede interesar: [Policia Militar asesina a dos campesinos del Movimiento Sin Tierra](https://archivo.contagioradio.com/policia-militar-asesina-a-dos-campesinos-del-movimiento-sin-tierra/)

En entrevista concedida al medio independiente Brasil de Fato, la abogada aseguró que **los policías no informaron las razones que motivaron su accionar**, sospechando que "está relacionado con las operaciones que se están produciendo en Paraná, pero **no sabemos que están buscando**”.

Las declaraciones de Alves, hacen referencia a un **accionar sistemático** que tiene como antecedente los casos ocurridos en otros estados de Brasil como **Paraná y Mato Grosso** do sul, en los que la policia civil ejecuta ordenes de captura contra militantes del Movimiento Sin Tierra, con la repetitiva justificación de **relacionar a los activistas con organizaciones criminales**.

La intención policial de criminalizar la movilización social, ha sido rechazada por diferentes organizaciones de Derechos Humanos, la Corte Suprema de Justicia y por el mismo MST, que en su portal de internet repudio el hecho, **exigiendo al gobierno tomar las medidas necesarias para garantizar el respeto a los derechos de la ciudadanía**.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
