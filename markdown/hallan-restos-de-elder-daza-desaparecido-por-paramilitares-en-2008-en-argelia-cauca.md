Title: Hallan restos de Elder Daza, desaparecido por  paramilitares en 2008 en Argelia, Cauca
Date: 2020-03-17 08:42
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Argelia, Elder Daza
Slug: hallan-restos-de-elder-daza-desaparecido-por-paramilitares-en-2008-en-argelia-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Elder.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Elder Daza/ Cortesía

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los pasados 14 y 15 de marzo tuvo lugar la diligencia de exhumación en el Zarzal, Patía, que tenía como fin localizar los restos de cuatro campesinos desaparecidos el 2 de junio de 2008 por el grupo paramilitar Los Rastrojos. Aunque se esperaba hallar todos los restos, únicamente se logró encontrar el cuerpo de Elder Daza en un departamento donde según la Unidad de Víctimas el Cauca representa el 2.3% de casos de desaparición forzada en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La diligencia encabezada por la [Comisión de Justicia y Paz](https://twitter.com/Justiciaypazcol), hizo parte del proceso de búsqueda de **Over Herney, Jesús Oleiver, Elder Daza, Gerardo Hoyos, Armando Cerón y Henry Gaviria** quienes en la madrugada del 2 de junio de 2008 fueron sacados de sus casas, amarrados y llevados con rumbo desconocido por paramilitares. Todo ello en medio de un escenario de militarización y control policial que nunca previno los hechos ni el desplazamiento masivo de más de 300 personas en el corregimiento de San Juan de la Guadua en Argelia, Cauca.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la Comisión Interamericana de Derechos benefició con medidas cautelares, en julio de 2010, a 29 familias víctimas de desplazamiento forzado, asesinato y abuso sexual, una década después el proceso de investigación avanza con lentitud y solo hasta esta fecha y gracias al apoyo de la comunidad de Zarzal y la información aportada por uno de los paramilitares se pudo avanzar con la investigación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El abogado de la Comisión de Justicia y Paz, Diego Manzano, quien desde su quehacer jurídico logró establecer un puente con la Fiscalía, el INPEC, la brigada 29 y el comandante del batallón de selva No 4 para que acompañaran la misión, se refirió a los detalles de la diligencia:

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que se estableció que la Policía acompañaría la exhumación y garantizaría la seguridad durante el trayecto, esta protección únicamente fue brindada hasta cerca de las 2:00 pm, hora en la que decidió abandonar el lugar - una zona de alto riesgo - lo que dificultó la continuidad de la búsqueda y limitó el accionar al hallazgo de Elder Daza, ocasionando un desconcierto en los familiares de las víctimas que esperaba hallar a todos sus seres queridos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Como familiares ha sido muy duro, sentimos un sabor agridulce porque al menos se encontró a uno, pero también un sabor amargo porque aún quedan tres desaparecidos, nuestra intención es seguir buscándolos hasta encontrarlos"**, manifestó Luz Nelly Meneses, compañera de Elder Plaza quien ha acompañado este proceso a lo largo de casi 11 años.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "El hallazgo de Elder Daza para mí es un alivio, porque tengo un hijo y su anhelo ha sido el de encontrar a su papá, así sean sus restos" - Luz Nelly Meneses

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque han sido hallados los restos de Eder, las familias continúan en la búsqueda de sus seres queridos, aguardando a que se conozca la verdad, se dé un proceso de reparación que vincule a los responsables de los hechos ocurridos. [(Le puede interesar: Pueblo Bello: 30 años comprometidos con la búsqueda de sus seres queridos)](https://archivo.contagioradio.com/pueblo-bello-30-anos-comprometidos-con-la-busqueda-de-sus-seres-queridos/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
