Title: Abajo la paz como cliché, arriba la autoprotección social
Date: 2016-11-29 12:58
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Slug: abajo-la-paz-como-cliche
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/la-paz-como-cliche.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [**Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)**] 

###### 29  Nov 2016

[Desde la academia, es soportable para muchos seguir insistiendo en la paz así mueran líderes sociales en medio de la más penosa injustica; eso no es un desacierto, pues al final ¿qué sería de la academia si no fuese un maremágnum de diferentes posiciones con fundamento?  ]

[Parece soportable desde los escenarios urbanos, rechazar sólo por Facebook los asesinatos de gente inocente y luego seguir la vida como si nada ¿acaso eso no es lo que nos brinda esta democracia? ¿libertad de expresión dirían unos? ¿libertad de morir dirían otros? … pues bien, desde mi rol como ciudadano, como sociólogo, como docente, debido al peso de la consciencia, debido a la importancia de la ética, no puedo seguir impulsando la paz como un somero cliché, pues eso sería similar a estar riéndome a carcajadas y dándome abrazos alegres en medio de los funerales de los más de 60 líderes sociales que han sido asesinados en el 2016.]

[¿Cuándo emerge el cliché? Cuando se promueve la paz sin estar dispuesto a sacrificar una posición social, y con esto no me refiero a la gente que ha vivido el conflicto y pide la paz, pues creo yo que ellos ya perdieron muchas posiciones sociales por culpa de la guerra. Por eso antes de señalar a quién me refiero… al que le caiga el guante…]

[Si bien el conflicto colombiano tiene múltiples factores que lo explican, no cabe duda de que, desde un principio, el grave problema ha sido la vieja costumbre de]*[asesinar al adversario político]*[, eso hoy, no ha cambiado; por eso es un cliché usar el término “paz” para comprender una realidad que niega la paz. La verdad creo firmemente, en medio del renovado plan estratégico de asesinato de líderes sociales, que con acuerdo o sin él, no habrá paz en Colombia. No por ahora.]

[Por supuesto hay que celebrar que la violencia ya no será entre guerrilla y Estado, ¡¡eso es muy positivo, claro!! pero la violencia seguirá golpeando a los verdaderos políticos colombianos, esos que no cobran 28 millones mensuales y no tienen esquemas de seguridad como la familia de Ordóñez, es decir: los líderes sociales.]

[Es probable que este artículo ya comience a incomodar a muchos y muchas que hoy siguen en la onda lingüística y hasta panfletaria de ponerle a todo el epíteto “por la paz” o “de la paz” y que viven hablando sobre paz de espaldas a la historia, con discursos lindos, pero tan impotentes… en este marco, menciono lo anterior porque me sentiría inhumano si asegurara que el país respira un aire de esperanza, eso es una posición bastante cómoda a mi parecer. Para repartir esperanza hay que ser pragmáticos, pues si el pragmatismo no hace presencia, entonces la esperanza resultaría ser nada más que una quimera, y hoy, la condición mínima para que se dé el pragmatismo, es que éste sea desarrollado abandonando las cómodas posiciones sociales.]

[Necesitamos algo más, obviamente no una apología a la continuidad de la guerra de guerrillas tal como la ha vivido el país, pero de pronto sí, una apología a la idea de que las formas de violencia contra el movimiento social no se detendrán tan sólo impulsando un concepto coyunturalmente desfasado como la “paz”, sino que sería mucho mejor, configurar métodos de resistencia e]***impulsar la autoprotección  social para darle vida a la esperanza política.***

[Quiero ser enfático en que aquello de dar “vida a la esperanza política” no es ninguna clase de poema, me refiero a que es necesario que en vez de seguir hablando de paz por aquí y paz por allá, pensemos, cómo garantizar, cómo proteger, cómo ayudar, o cómo aconsejar estratégicamente a los líderes sociales para que no los maten… ¿acaso cuántos años de formación política y moral cree que tiene que pasar un líder social? más de 10, 20, 30 años… ¿para que de un balazo todo se arruine? No es justo.]

[Ya no son los años ochenta cuándo las sociedades aún estaban sentadas sobre fuertes bases teório-políticas que eran impulsadas por la guerra fría y una cultura política arraigada; hoy estamos en el siglo XXI, en la época que algunos llaman equivocadamente “posmodernidad”, en la época que otros llaman acertadamente “hipermodernidad”, en la época donde la gente cree que se puede comprehender de política con un meme, un trino o un posteo… en fin, si los matan a todos, no habrá sustento real para los cambios y todo se quedará en las estructuradísimas palabras de los intelectuales en las ciudades, que con lindos discursos probablemente accedan a triunfos personales y celestinos, pero que dejarán a la sociedad anclada en una actitud muy crítica eso sí, pero sin método… es decir, estéril, pues la esperanza sola, hoy la están matando a balazos.  ]

[La cosa es tan compleja, que si estuvimos atentos a los últimos acontecimientos, pudimos dar cuenta de cómo se firmó un nuevo acuerdo en medio de las más arduas contradicciones: con un vicepresidente que tuvo el descaro de hacer prevalecer su propia agenda antes que asistir a un evento de interés superior, con un ex presidente que habló de revocar el congreso, con un presidente que por querer quedar bien con todo el mundo se le va a salir de las manos la implementación, con una ultraderecha matando en los campos y ciudades, con una ultraderecha sedienta de elecciones y trinando estupideces para sumar adeptos, con líderes sociales asesinados, otros amenazados y por último, con una gran porción de gente que se aleja cada vez más de la comprensión social y política, y prefiere creer que no necesita conocer nada más que un par me memes y trinos para sentir que su opinión política es muy valiosa... Me da hasta tristeza escribirlo, y dicen que con tristeza las cosas no salen muy bien, no obstante, qué más hacer, ¡qué más hacer! ¿la verdad? argumentar dos cosas: 1) ¡abajo la paz como cliché! 2) ¡arriba la autoprotección  social!]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.] 
