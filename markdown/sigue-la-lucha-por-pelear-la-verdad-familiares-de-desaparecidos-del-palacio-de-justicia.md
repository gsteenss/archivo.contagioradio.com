Title: “Sigue la lucha por pelear la verdad” familiares de desaparecidos del Palacio de Justicia
Date: 2015-10-20 19:29
Category: DDHH, Nacional
Tags: Coronel Plazas Vega, Cristina Guarín, Familiares de desaparecidos del Palacio de Justicia, Juan Manuel Santos, Lucy Amaparo Oviedo, Luz Mary Portela, ministerio de defensa, René Guarín, restos de tres desaparecidos del Palacio de justicia, Retoma del Palacio de Justicia, Rosa Milenja Portela
Slug: sigue-la-lucha-por-pelear-la-verdad-familiares-de-desaparecidos-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Sin-título1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [20 Oct 2015] 

**“Se ha mezclado una dosis de alegría con una profunda dosis de tristeza e incertidumbre”,** expresa René Guarín, hermano de Cristina del Pilar Guarín, desaparecida durante la retoma del Palacio de Justicia, cuyos restos fueron hallados en el cementerio Jardines de Paz, donde también se encontró partes del cuerpo de Luz Mary Portela. Los restos de Lucy Amparo Oviedo fueron hallados en el cementerio del Sur.

**“El descanso es completo cuando se hace la entrega de un cuerpo con la verdad ‘verdadera’ no con la verdad que le conviene al Estado**” dice Rosa Milena Portela, hermana de Luz Mary, quien agrega que “genera más interrogantes ver que sus familiares salieron vivos y ahora se los entregan calcinados”.

**"Lucy fue desaparecida y no extraviada,** hoy nos hacen entrega de unas vertebras que pertenecen a ella,  es la única parte que hay, el resto está disperso, lo que es una evidencia más de que ella no fue una N.N y que su cuerpo en su momento no fue llevado a la fosa del cementerio del Sur, no sabemos cómo llegó allí, tenemos certeza que ella salió con vida por un video, además de **las llamadas que nos hizo ese día en las que nos decía que se encontraba en la Casa del Florero, y que le lleváramos ropa porque estaba sucia**", cuenta la familiar de Lucy Amparo Oviedo.

<iframe src="http://www.ivoox.com/player_ek_9102806_2_1.html?data=mpadlJ2Ueo6ZmKiakpaJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMLgwsffw9iPqsLhytHWw9fJt4zYxpDRx9jFtMLmxsjWxtTXb8XZzZC9w9HFp8rjjMnSja%2FZt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Familiares de las tres mujeres desaparecidas] 

Aunque aseguran que se trata de un avance en la búsqueda de la verdad frente a lo que vivieron sus familiares antes morir “o ser asesinados”, como dice Guarín, afirman que **“sigue la lucha por pelear la verdad**”, es por eso que solicitaron que de forma urgente se convoque a la Oficina de Derechos Humanos de las Naciones Unidas para la conformación de un equipo internacional que acompañe este proceso, en el que se investigará las condiciones de tiempo, modo y lugar en que ocurrió la muerte de las tres mujeres.

<iframe src="http://www.ivoox.com/player_ek_9102630_2_1.html?data=mpadlJuXdI6ZmKiakpaJd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRisLhytHWw9fJt4zYxpDRx9jFtMLmxsjWxtTXb8bsyszS0JDFp9Dh0caSpZiJhpLVzs7S0NnTb8ri1cqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Abogada Liliana Ávila, Comisión de Justicia y Paz] 

El hermano de Cristina Guarín, señala que es necesaria la veeduría internacional teniendo en cuenta que el Estado, “ha sido juez y parte” y añade, **“No es un misterio que el Estado permitió que la toma del Palacio de Justicia ocurriera; ejecutó la violenta retoma; capturó, torturó, asesinó y desapareció a personas; produjo uno de los incendios dentro del Palacio y dejó 20 años sin investigar los hechos,** además, sacó de la investigación al personal del DAS, del F2, personal de la Policía Nacional y el personal del Departamento de Inteligencia del Ejército”.

<iframe src="http://www.ivoox.com/player_ek_9102751_2_1.html?data=mpadlJyZdY6ZmKiakpeJd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmptjhw8nTb8bnjM%2Fix9%2BPvYzkwtfhx4qWdozawtLWzs7FtsbnjMnSjcnJt8LkwtfSxc7Is9Sfxcqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [René Guarín, familiar de Cristina Guarín ] 

Por su parte, desde el grupo de abogados de los familiares de los desaparecidos, se aseguró que **las labores de exhumación de los restos, han sido hostigadas por personal de inteligencia militar** que, de acuerdo a ellos, ha estado vigilando y  ha intentado ingresar al Instituto de Medicina Legal sin tener facultad para ello. De manera que se hizo el llamado al presidente Juan Manuel Santos, para que ordene al Ministerio de Defensa suspender estas acciones y además, desde el Ejército se diga la verdad sobre los hechos que rodearon la desaparición de personas durante la retoma del Palacio de Justicia.
