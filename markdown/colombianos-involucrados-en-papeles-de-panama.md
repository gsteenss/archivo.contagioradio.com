Title: ¿Quiénes son los colombianos involucrados en la lista de los Papeles de Panamá?
Date: 2016-04-06 18:17
Category: Economía, Nacional
Tags: colombia, lavado de activos, Papeles de Panama, Paraisos fiscales
Slug: colombianos-involucrados-en-papeles-de-panama
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Papeles-de-Panama-e1459970719604.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Español 

###### [6 Abr 2016]

Según las filtraciones de la lista de los [Papeles de Panamá hay 850 colombianos](https://archivo.contagioradio.com/por-lo-menos-us100-mil-millones-tendrian-colombianos-en-paraisos-fiscales/) que tendrían grandes sumas de dinero en los paraísos fiscales. Desde políticos hasta empresarios, estos son algunos de los nombres que han aparecido en la lista:

**Alfredo Ramos Maya:** Es senador del partido Centro Democrático desde el año 2014, hijo de Luis Alfredo Ramos ex gobernador de Antioquía, investigado por nexos con el paramilitarismo y por detrimento patrimonial por un valor de \$18.000 millones de pesos en contratos de infraestructura. Ramos Maya fue director de la empresa Tronex S.A. desde el año 2008 hasta el año 2103 cuando decidió iniciar su carrera política.

Alfredo Ramos acudió a la firma de abogados Mossack Fonseca para crear una sociedad denominada Kirtland Holding SA, conformada el 27 de septiembre de 2013 y expedida el 30 de agosto del 2013, dos días después del arresto de su padre. Ramos dice que esta sociedad serviría como un *holding* para realizar inversiones en diferentes starups en América Latina, sin embargo, Kirtland Holding SA nunca opero y no fue declarada ante las autoridades colombianas.

**Roberto Hinestrosa Rey**: Es Presidente del Concejo de Bogotá, concejal por el partido Cambio Radical, Decano y fundador de la Facultad de Gobierno y Relaciones Internacionales de la Universidad Externado de Colombia.

Hinestrosa aparece como parte de Crownpeak Enterprises INC, desde el 6 de enero de 2011, según el presidente del Consejo de Bogotá, esta asociación de carácter familiar, tendría la intención de generar exportaciones de carbón para plantas eléctricas en Panamá, pero no llegaría a funcionar debido a que no tuvo capital inicial, motivo por el cual la empresa no se registró ante las autoridades colombianas. El domicilio de la empresa es la misma dirección de la Universidad Externado de Colombia (calle 12 \#1-17 este).

**Carlos Gutiérrez Robayo**: Es un médico veterinario de la Universidad de la Salle, es fundador y presidente de CGR Biotecnología Reproductiva, una de las empresas más importantes del sector agropecuario, además posee empresas en otros sectores como el inmobiliario, la construcción, el petróleo, el carbón, las tierras y las líneas férreas. En el 2012 fue sancionado por Corpoguavio con \$24 millones de pesos debido al uso de recursos hídricos sin autorización. A su vez Carlos Gutiérrez hace parte de la firma constructora Praga Servicios Inmobiliarios S.A.  que el año pasado obtuvo una licitación para construir un conjunto residencial en previos del Humedal La Conejera, ubicado en la localidad de Suba.

Gutiérrez creo 15 compañías en paraísos fiscales bajo el título de la empresa Trenaco Colombia, una parte de ellas se abrieron el 30 de enero de 2012 con cuentas en Mossack Fonseca. En el año 2014, la firma de abogados decide cerrar todas las cuentas de Carlos Gutiérrez debido a las denuncias de los medios sobre la legalidad de sus negocios y los nexos que podría tener con los hermanos Nule.

**Leon Eisenband:** Es la cabeza del grupo FEDCO desde el año 1979, Eisenband ha estado involucrado en diferentes escándalos en Colombia, uno de ellos tiene que ver con la imputación de cargos por peculado debido a la compra irregular de un local en el centro comercial Villa Country en Barranquilla, a través de la Dirección Nacional de Estupefacientes.

Leon Eisenband manejo cuentas con Mossack Fonseca, sin embargo el nombre de la firma o compañía con la que manejo sus cuentas se desconoce.

**Alberto Carrasquilla:** Ex Ministro de Hacienda durante el periodo de presidente Álvaro Uribe Vélez durante los últimos 3 años de su mandato y el primer año de su segundo mandato, actualmente Decano de la Facultad de Economía de la Universidad de Los Andes. Carrasquilla ha estado involucrado en diferentes hechos importantes económicos del país como la privatización de Megabanco o la venta de Ecogas. Bajo la firma Consultoría Konfigura Capital, lideró un controversial proyecto de venta de *“bonos de Agua”* que finalizo con el endeudamiento de los municipios que los adquirieron a 19 años con altas tasas de ínteres.

Carrasquilla tenía el 33% de las acciones de la sociedad Navemby Invesment Group INC, constituida el 1 de noviembre de 2007, esta empresa se creó a través de la Consultora Konfigura Capital, y no posee capitales desde hace varios años. La función de la sociedad era servir como *holding* con capital americano, lo que significaría que la moneda de manejo sería el dólar, motivo por el cual, según Carrasquilla, Panamá era el lugar ideal para realizar la apertura de la empresa.

**Augusto Acosta Torres:** Es ex presidente de la Bolsa de Valores, ex Superintendente Financiero y en la actualidad se desempeña como consultor económico y financiero.

Acosta, durante el año 2009 intento adquirir la empresa ORNA EQUITIES, a través de Mossack Fonseca, hecho que no se pudo consolidar debido a que el, en ese entonces era una figura pública y esto iba en contra de las políticas internas de la firma panameña, sin embargo, el poder fue otorgado a la esposa de Acosta y finalmente en el año 2013 logró tramitar su poder para manejar la sociedad.

En declaraciones dadas a los medios Augusto Acosta índica que registro la compra debidamente en el Banco de la República y que es un capital que aún no termina de pagar.

**Andrés Florez Villegas:** Abogado de la Universidad Javeriana y Magíster en Derecho de la Universidad de Cornell (Estados Unidos). Fue director de Fogafin hasta mediados del 2007 y como abogado particular representó a fondos de pensiones e inversionistas institucionales afectados por el caso Interbolsa. Fue socio de Konfigura, la misma empresa del exministro de Hacienda Alberto Carrasquilla.

Andrés Florez también se encuentra vinculado con la creación de Navemby Invesment Group INC. En la actualidad no tiene ninguna participación en la asociación ni en la Consultora Konfigura Capital.

**Luis Alberto Ríos Velilla**: Dueño de las empresas de aseo Servigenerales, Aseo Capital y accionista en las generadoras de energía Enerpereira y Enertolima y prestador del servicio de aseo en Panamá, Ríos Velilla posee uno de los emporios más grandes de aseo en Bogotá, durante la alcaldía de Gustavo Petro fue uno de los personajes que se opuso a la generación de un servicio público de recolección de las basuras. Durante el año 2010 Ríos tuvo que devolver \$781.739.399 millones a la Empresa de Energía de Pereira por un prestamos solicitado en el año 2006, que se celebró de forma inapropiada.

Luís Alberto Ríos tiene una sociedad desde el año 1998 denominada Canopy Consulting Limited. Esta empresa está dedicada a la oferta Bienes raíces en Miami.

**Hollman Carranza:** Hijo del fallecido esmeraldero Víctor Carranza, preso desde 1998 hasta el 2002 por sus nexos con el paramilitarismo, el despojo de tierras y secuestro. Hollman Carranza fue buscado por la firma Mossack Fonseca para ofrecerle sus servicios de incorporación y estructuración de sociedades.

**Miguel Silva Pinzón:** Abogado y filósofo de la Universidad de los Andes y especialista en comunicaciones estratégicas. Es la cabeza de la firma de comunicaciones FTI, fue editor político del periódico La Prensa de la familia Pastrana en 1998, editor político de El Tiempo y después asesor del presidente César Gaviria (1990-1994). En 1993 llegó a la Secretaría General de la Presidencia y después acompañó a Gaviria cuando este fue secretario general de la OEA. Tras su paso por el sector público, fundó la empresa de comunicaciones Gravitas, que en 2007 compró la multinacional FTI. Actualmente es muy cercano al presidente Santos y es consultado por el mismo pese a que no tiene un contrato oficial.

Silva posee la empresa Cezanne Propettiers S.A. que se creó a través de Mossock Fonseca, constitución se llevó a cabo el 12 de octubre de 2007. La asociación tenía como finalidad ser un *holding* de inversión extranjera.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
