Title: Asesinan a tres integrantes de guardias campesina e indígena durante fin de semana en Cauca
Date: 2020-02-17 09:55
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asesinato de líderes campesinos, Cauca
Slug: campesinos-indigenas-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Diseño-sin-título.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Cauca ACIN/ ANZORC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Organizaciones sociales denunciaron que el pasado 16 de febrero de 2020 cerca de las 8:30 de la noche se presentó un ataque en la vereda La Morena del municipio de Miranda, Cauca, en el que hombres armados asesinaron a los hermanos y defensores de derechos humanos **Albeiro y Luis Hugo Silva Mosquera.** En el ataque también resultó herido el comunero indígena **Daniel Remigio** que fue remitido a un centro hospitalario mientras en Buenos Aires, Cauca fue asesinado el indígena nasa Emilio Dauquí.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Versiones preliminares indican que los hermanos se disponían a salir de integración comunitaria cuando fueron abordados por un grupo de cerca de seis hombres armados quienes les dispararon sus armas de fuego en repetidas ocasiones hiriendo a Albeiro a Luis Hugo y al comunero indígena Daniel Remigio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El grupo de atacantes huyó a bordo de motocicletas mientras la guardia indígena era activada, pese a ellos, los armados se abrieron paso luego de intimidar a los guardias indígenas que se encontraban en uno de los puestos de control de la vereda.    
    
Albeiro y Luis Hugo Silva eran integrantes de la Guardia Campesina, la Junta de Acción Comunal de la Vereda La Morena, la Asociación Pro constitución Zona de Reserva Campesina del Municipio de Miranda Cauca ASPROZONAC, filial de la Federación Nacional Sindical Unitaria Agropecuaria FENSUAGRO – CUT, de la Asociación Nacional de Zonas de Reserva Campesina – ANZORC, del Proceso de Unidad Popular del Suroccidente Colombiano – PUPSOC y de la Coordinación Social y Política Marcha Patriótica Cauca.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Todo Cauca permanece bajo alerta

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El ataque ocurrido contra los tres hombres se da en medio de una lucha por el control territorial entre grupos como el EPL, disidencias de las FARC, las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y el más reciente paro armado anunciado por el ELN que causó tensión y zozobra en departamentos como el Cauca donde se presentaron otros hechos de tensión en municipios como Piamonte, Miranda, Santander de Quilichao y Caloto. [(Le puede interesar: Es declarada crisis humanitaria en tres regiones del país)](https://archivo.contagioradio.com/es-declarada-crisis-humanitaria-en-tres-regiones-del-pais/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que Fensuagro descartó que los homicidios sean consecuencia del paro armado, sin embargo han solicitado que las autoridades realicen las investigaciones que permitan establecer quiénes fueron los autores de este delito.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma durante la noche del pasado sábado, en Buenos Aires, Cauca, el indígena nasa **[Emilio Dauquí](https://twitter.com/FelicianoValen/status/1229221035045724160)** fue asesinado por impactos de arma de fuego pese al accionar de la guardia indígena que buscaron proteger a la víctima mientras sus agresores huyeron en motos, tomando camino entre Mondomo y La Agustina, dejando en evidencia el mismo modo que se viene operando para atacar a defensores de DD.HH. y que han denunciado organizaciones Somos Defensores. **Emilio Dauquí se convirtió en uno de los 13 comuneros asesinados desde inicios del 2020.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, en la vereda Bajo Congor en Piamonte, fueron incautadas 840 granadas cargadas con pentolita mientras en Santander de Quilichao la población del barrio el Porvenir permaneció en alerta luego de conocerse la instalación de un supuesto aparato explosivo que fue descartado por la Policía.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
