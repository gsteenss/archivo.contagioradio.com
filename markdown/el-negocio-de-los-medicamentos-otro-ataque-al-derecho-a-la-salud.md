Title: El negocio de los medicamentos, otro ataque al derecho a la salud
Date: 2016-02-25 16:18
Category: DDHH, Otra Mirada
Tags: Acceso a medicamentos, Comisión Colombiana de Juristas, crisis de la salud
Slug: el-negocio-de-los-medicamentos-otro-ataque-al-derecho-a-la-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/pastilla.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

<iframe src="http://co.ivoox.com/es/player_ek_10574986_2_1.html?data=kpWimZmdfJehhpywj5WbaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncabgjNPSydTHrdCfxcqYztTXb87Zxc7Qw9LJstXj1IqfpZDTuNPjjMbhw9bZqYzVzZDRx9fJp8njjJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Andrea Carolina Reyes, Misión Salud] 

###### [25 Feb 2016] 

Organizaciones de la Sociedad Civil y de la Iglesia Católica de Latinoamérica y el Caribe han solicitado a la Comisión Interamericana de Derechos Humanos una **audiencia temática regional para exponer el drama de la falta de acceso a medicamentos** a que viven las personas en esta parte del mundo y abrir el debate sobre las prácticas ilegales e ilegítimas que adelantan las corporaciones para **evitar que los medicamentos genéricos compitan en el mercado**.

De acuerdo con Andrea Carolina Reyes, subdirectora  de Misión salud Colombia, organización que de la mano de la Comisión Colombiana de Juristas (Colombia) y el CELAM (LAC) impulsa la iniciativa,  **la mayor parte de la población no relaciona su derecho de acceder a los medicamentos necesarios para curar o tratar una enfermedad con el de acceder a la salud**, aun cuando hace parte del mismo derecho “de forma inmanente y no tendrían por qué estar desconectados”.

Varias son las razones que la profesional aduce como causas de la situación que en la actualidad afecta a cerca de 2000 millones de personas en el mundo, entre las que se cuentan **los** **precios inaccesibles para acceder a los medicamentos, porque no existen para enfermedades que “no son mercado en la dinámica de mercado de la enfermedad”** o porque se impide desde los grandes laboratorios que los medicamentos genéricos lleguen a competir en el mercado.

**“Hay toda una campaña de descredito de los medicamentos genéricos donde se asegura que no son buenos, de mala calidad o que no funcionan**”, que de acuerdo con la funcionaria son mensajes que provienen de “medios de comunicación, en la droguería y hasta el médico mismo muchas veces dice eso”, indicando cuál es el medicamento que debería utilizar y que marca es más conveniente.

El alto costo de los medicamentos de alta tecnología fijados por cada corporación, perjudica directamente a los sistemas de salud en los países, situación que llevó a la firma de una “**Política Farmacéutica Nacional** “que tiene diez años para implementarse (2012-2022) con la que se pretende **resolver las ineficiencias del sistema frente a los medicamentos**, beneficiando una reducción significativa en el gasto 2 o 3 veces del actual.

En el marco de la solicitud de esta audiencia, las organizaciones vinculadas convocan a una **Twiteraton este jueves a partir de las 6 de la tarde**, utilizando **\#AudienciaAccesoAMedicamentos**, con el objetivo de visibilizar la problemática a la sociedad colombiana e impulsar la solicitud de audiencia para el mes de abril con el comisionado encargado de los derechos económicos y sociales de la CIDH.
