Title: "Política de miedo" de paramilitarismo debe ser afrontada con movilización social
Date: 2017-07-18 13:02
Category: DDHH, Nacional
Tags: asesinato de líderes, lideres sociales, movilizaciones sociales, paramilitares, protección a líderes
Slug: paramilitarismo-es-una-politica-del-miedo-impuesto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/marcha-patriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [18 Jul 2017] 

Frente a la alarmante situación de amenazas y asesinatos que viven los líderes y las lideresas en el país, **las organizaciones sociales han manifestado que la sociedad en general debe exigirle al Estado que trabaje por la protección del derecho a la vida** para transformar el "discurso de terror y el miedo impuesto" principalmente por el paramilitarismo. En lo que va del 2017 van 52 líderes asesinados en el país por razón de su actividad de defensa de los derechos humanos y de los territorios.

Para Camilo González, director de la organización Indepaz, “con relación al año anterior, **las cifras de asesinatos a líderes han aumentado** y las amenazas que los y las amedrentan es una práctica sistemática de terror”. Adicionalmente, él manifestó que “no hay una respuesta contundente por parte del Estado para disminuir la violencia que ocurre en las zonas donde las FARC salieron a las Zonas Veredales”.

Ante la situación del Cauca, que según González es **"la más calamitosa" en lo que se refiere al ejercicio de los líderes sociales en el país**, “hay que ver las implicaciones de estos asesinatos en su conjunto porque hay disputas por la tierra, las comunidades están en alerta por los grupos armados ilegales, hay disputas por los procesos mineros y hay enfrentamientos con los grupos que buscan copar los territorios de cultivos de coca”. (Le puede interesar: ["En 2017 han sido asesinados 15 líderes sociales en el Cauca"](https://archivo.contagioradio.com/en-2017-han-sido-asesinados-15-lideres-sociales-en-el-cauca/))

González fue enfático en que “**hay regiones que están viviendo una recomposición de los grupos ilegales** como el Clan del Golfo en Tumaco y las Autodefensas Gaitanistas en el Chocó”. De igual forma, “en el Cauca y en el Catatumbo, hay nuevos actores peleándose por el ingreso y el control de las rutas de la coca”. A esto se suma el hecho de que el Estado no reconoce la existencia de grupos ilegales paramilitares que buscan acallar a los defensores y defensoras.

### **La protección de los líderes sociales debe estar acompañada estrategias sociales** 

Ante la preocupación por la inoperancia de las Fuerzas Armadas, para contrarrestar las acciones de los paramilitares en estos territorios, González manifestó que “**si solo hay presencia militar los problemas siguen vivos** porque las economías están en disputa”. Así mismo afirmó que “hay una cultura y un discurso que no se corresponde con la situación real donde la protesta social tiene respuestas improcedentes por las elites”. (Le puede interesar: ["Asesinado en Cauca, Héctor William Mina, líder e integrante de Marcha Patriótica"](https://archivo.contagioradio.com/asesinado-hector-william-mina-marcha-patriotica/))

González afirmó que no se puede olvidar que hay un fenómeno que es muy importante y es la respuesta de la sociedad ante estos hechos. **“Hay manifestaciones de las comunidades que generan un pacto social para proteger la vida de estas personas”**. Por ejemplo hizo alusión a la creación de las guardias campesinas y cimarronas como mecanismo que sirven para presionar y visibilizar la situación que se vive en las comunidades a las ves que le exigen al Estado mecanismos de respuesta eficaces para detener la violencia.

Finalmente, González manifestó que “**las dinámicas sociales y las movilizaciones sirven como mensaje positivo** para evidenciar que estamos en un momento de una transición con posibilidades que requieren vigilancia, control y garantías”. Él hizo énfasis en que “debe haber una acción transformadora por parte de la sociedad como respuesta y resistencia al terror”.

<iframe id="audio_19868467" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19868467_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
