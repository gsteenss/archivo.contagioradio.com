Title: En Colombia no hay garantías para la primera infancia
Date: 2017-06-02 19:05
Category: DDHH, Educación
Tags: Derechos de los niños, niños, Save The Children
Slug: en-colombia-no-hay-garantias-para-la-primera-infancia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/no-hay-garantías-para-la-primera-infancia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [02 Jun. 2017] 

**En Colombia cada día son asesinados 2 niños o niñas,** cifra que ubica al país en cuarto lugar la tasa de homicidios de menores en latinoamérica, después de Honduras, Venezuela, y El Salvador. Así lo asegura el informe “Infancias Robadas” realizado por la organización internacional Save The Children.

De acuerdo con la investigación, **entre 172 Naciones a nivel global Colombia se ubica en el lugar 118 de los países en donde menos respetan los derechos de la infancia.** Según la vocera de la organización Luz Alcira Granada, durante el 2016 se presentaron 659 homicidios contra niños y niñas, estadística con la que se pretende llamar la atención del Estado por su falta de voluntad de tomar medidas en el asunto.

“Solo el 4% de los casos están siendo atendidos mientras que los otros quedan en situación de espera o no se terminan de resolver” agregó Granada.

El informe registra que de los miles de **niños y niñas que mueren al año muchos hubieran podido evitarse por que se dieron por causas prevenibles.** Más de 16.000 mueren antes de cumplir los 5 años y muchos de estos niños viven ahora en la peor situación de pobreza siendo vulnerables a diferentes enfermedades. Le puede interesar: [Gobierno no responde por derecho a la salud de los niños y las niñas](https://archivo.contagioradio.com/36779/)

### **Niños y niñas afectadas en el conflicto armado** 

Según Save The Children, **Colombia se encuentra en el quinto puesto mundial donde los niños y niñas han muerto por el conflicto armado**, después de Siria, Sudán del Sur, Somalia y República Centroafricana.

Cifras oficiales del Registro Único de Víctimas da cuenta de 2.400.000 niños y niñas víctimas del conflicto, es decir cerca del 10% de la población infantil en Colombia. Le puede interesar: [Hasta los niños son víctimas del ESMAD en Buenaventura](https://archivo.contagioradio.com/se-recrudece-la-represion-a-paro-civico-de-buenaventura/)

### **El trabajo infantil sigue siendo preocupante** 

Durante el último trimestre de 2016 Colombia registró una Tasa de Trabajo Infantil de 869.000 niños y niñas y en el Trabajo en el Hogar dio cuenta de 527000 menores, dando como resultado 1.396.000 niños y niñas que sufren el flagelo del trabajo infantil.

Este informe, manifiesta que **las áreas de trabajo donde los niños y niñas se desempeñan son la agricultura, el comercio, la manufactura y el transporte.** Le puede interesar: [Al hundirse el referendo "discriminatorio" ganan los niños y las niñas](https://archivo.contagioradio.com/al-hundirse-el-referendo-discriminatorio-ganan-los-ninos-y-las-ninas/)

### **Los niños y niñas trabajan porque no pueden continuar estudiando** 

De acuerdo al Ministerio de Educación la tasa de deserción uno de cada cinco estudiantes de no sigue estudiando en la primaria, por lo que ese es uno de los aspectos en los que más hay que trabajar.

Garantizando de esta manera que los niños puedan acceder a la educación y que el porcentaje de deserción sea menor.

![Cifras](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Cifras.jpg){.alignnone .size-full .wp-image-41589 .aligncenter width="865" height="428"}

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
