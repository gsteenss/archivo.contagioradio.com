Title: AGC amenazan a comunidad en proceso de retorno al Bajo San Juan, Chocó
Date: 2017-12-01 16:19
Category: DDHH, Nacional
Tags: AGC, Bajo San Juan, buenaventura
Slug: agc-amenazan-a-comunidad-en-proceso-de-retorno-al-bajo-san-juan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/Autodefensas-gaitanistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de Justicia y Paz] 

###### [1 Dic 2017] 

Debido a la grave situación de seguridad en la que se encuentra la comunidad de Cabeceras del Chocó, opor lo cual deberán permanecer un mes más en medio de las difíciles condiciones en las que viven en el Coliseo de Buenaventura.

De acuerdo con Dagoberto Pretel, representante legal del consejo comunitario, aunque todo ya estaba programado, en una reunión con la secretaría, la Unidad de Víctimas y el alcalde del municipio, se acordó que **lo mejor era volver a su territorio en los primeros días del mes de enero**, para así tener mayores garantías de seguridad a la hora de declarar su caserío como Zona Humanitaria, "lugar exclusivo de población civil, donde se  respete la vida y se pueda construir paz con justicia social, ambiental y de género".

Esta situación se da en medio de una serie de amenazas y hostigamientos entre los que se encuentra una llamada el pasado 26 de noviembre mediante la cual alias “Marco”, quien se identificó como miembro de las **Autodefensas Gaitanistas de Colombia amenazó a la comunidad y al líder Dagoberto.** Precisamente hace un mes  miembros de esta misma estructura neoparamilitar al mando de alias “Costeño” asesinaron al joven de 24 años Jefferson Pretel Waitoto integrante de esta misma comunidad, al parecer por negarse a ser reclutado.

### **Grave situación humanitaria** 

No obstante, en medio de esa situación de inseguridad, Pretel asegura que **"la comunidad sigue en pie y el proceso de retorno va".** Su comunidad no puede seguir viviendo en medio de una difícil situación humanitaria, debido a que en el Coliseo del centro de Buenaventura que es donde permanecen actualmente, los niños, mayores y el general de la comunidad  están padeciendo problemas de salud, alimentación y  no cuentan con acceso a agua potable.

"Donde estamos es un sitio no apto para vivir con dignidad" expresa Dagoberto, quien señala que en ese coliseo y en esas condiciones **están viviendo 119 personas y un promedio de 40 niñas y niños de diferentes edades.**

<iframe id="audio_22404188" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22404188_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
