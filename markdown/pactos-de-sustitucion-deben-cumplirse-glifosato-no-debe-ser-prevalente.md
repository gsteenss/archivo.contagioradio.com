Title: Pactos de sustitución deben cumplirse, glifosato no debe ser prevalente
Date: 2019-07-23 10:37
Author: CtgAdm
Category: Ambiente, Entrevistas
Tags: Aspersión Aérea con Glifosato, Corte Constitucional
Slug: pactos-de-sustitucion-deben-cumplirse-glifosato-no-debe-ser-prevalente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Glifosato-Putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

La Corte Constitucional ratificó que para reanudar el programa de aspersión aérea con glifosato deben cumplirse seis **exigencias sanitarias ambientales, que** demuestren la ausencia de daño para la salud y el medio ambiente, sin mebargo también se realizaron precisiones sobre la sentencia emitida en 2017, abriendo la puerta a que regrese la fumigación con glifosato sobre los cultivos de uso ilícito.

[Luis Felipe Cruz, investigador de política de drogas de Dejusticia indicó que la Corte no flexibilizó los requisitos para el programa de aspersión aérea, pero si hizo claridades sobre el alcance del fallo explicando que la sentencia no] “equivale a demostrar, por una parte, que existe certeza absoluta sobre la ausencia de daño, pero tampoco equivale a que la ausencia de daño es absoluta o que la actividad no plantea ningún riesgo".

### Claridades de la Corte Constitucional sobre el uso de glifosato

Aunque no está demostrado totalmente que la aspersión aérea con glifosato produzca algún tipo de afectación,  tampoco significa que el hacer uso de este pesticida este libre de riesgos o peligros para la salud. Por tal motivo el Gobierno debe demostrar "la ausencia de daño, con evidencia razonable pero no absoluta, a través de un análisis objetivo e imparcial que se aplique al caso colombiano que permita tomar una decisión".

El abogado resaltó además que la Corte recordó al Gobierno que las fumigaciones áereas son herramientas de última instancia **"y no debe ser la opción prevalente ni preferente y primero deben demostrar que el plan de sustitución no funcionó".** Además se especificó que la decisión de reanudar la aspersión con glifosato corresponde al Consejo Nacional de Estupefacientes.[(Le puede interesar: El regreso del glifosato, una amenaza para campesinos en zonas cocaleras)](https://archivo.contagioradio.com/el-regreso-del-glifosato-una-amenaza-para-campesinos-en-zonas-cocaleras/)

### Pactos de sustitución deben cumplirse

El Gobierno, por medio de la ministra de Justicia, Margarita Cabello, indicó que el Gobierno llevará su petición de regresar a la aspersión aérea al Consejo Nacional de Estupefacientes, **entidad que tendrá la responsabilidad de sopesar y valorar toda la evidencia científica y técnica con respecto a las afectaciones a la salud que podría ocasionar el glifosato.**

Frente a esta decisión, Cruz reiteró que el Gobierno no tiene autorizadas las aspersiones aéreas con glifosato, y que en caso  de que se hagan "estarían viciadas por ilegalidad, pues no hay un marco jurídico que autorice ese tipo de operaciones", sin embargo resaltó que las comunidades deben estar alerta para recolectar la mayor cantidad de pruebas posibles como recolección de muestras de agua y tierra en caso de presentarse un operativo sobre su territorio.

Al respecto, **campesinos del Sur de Córdoba** han alertado sobre el posible regreso de la fumigación manual con glifosato por lo que han anunciado que tomarán medidas para defender la "sustitución  concertada, gradual, voluntaria y sobre todo pacífica", por lo que indican se ven forzados a reactivar los comités anti-erradicación en las veredas de esta región.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38803752" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38803752_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
