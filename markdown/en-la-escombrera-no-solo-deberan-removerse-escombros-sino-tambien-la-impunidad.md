Title: "En La Escombrera no sólo deberán removerse escombros, sino también la impunidad"
Date: 2015-07-29 12:46
Category: DDHH, Judicial
Tags: Adriana Arboleda, arenera, Corporación Jurídica Libertad, desaparecido, escombrera, interior, Juan Fernando Cristo, Medellin, ministro, operación orion
Slug: en-la-escombrera-no-solo-deberan-removerse-escombros-sino-tambien-la-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Escombrera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ADN 

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Adriana-Arboleda.mp3"\]\[/audio\]

##### [Adriana Arboleda, Corporación Jurídica Libertad] 

###### [29 jul 2015] 

El lunes 27 de julio se llevó a cabo la diligencia ordenada por la Fiscalía, para iniciar el proceso de búsqueda en el sector de la Arenera, de La Escombrera en Medellín, para dar con los restos de personas desaparecidas durante la realización de la Operación Orion en la Comuna 13, en el año 2002.

En la diligencia se llevó a cabo un acto por la memoria y la verdad con el lema "Escarbando la verdad, desenterrando la justicia", promovida por las organizaciones de mujeres y familiares de los desaparecidos.

<div>

A pesar de la satisfacción que representa para las víctimas que luego de 13 años en búsqueda de sus familiares, el Estado inicie un proceso de investigación, esta se ve empañada por el anuncio del Ministro del Interior, Juan Fernando Cristo, quien aseguró en la misma audiencia que sólo hay presupuesto para buscar en 1 de los 4 puntos considerados de La Escombrera.

Es por este motivo que las familias víctimas presentaron una propuesta, y la anexaron al plan de búsqueda presentado por la Fiscalía, en la que se considera la búsqueda de recursos para garantizar la investigación en un 2do punto de La Arenera, en un sector clausurado de la Escombrera, y uno más que continúa activo. La Fiscalía determinó que la búsqueda en el primer punto podría tardar 5 meses, tiempo en el cual las organizaciones de familiares esperan que se organice la intervención en los otros sitios. "El gobierno ha dicho que va a buscar recursos en la cooperación internacional para eso, pero depende mucho del trabajo que hagamos las organizaciones sociales para que el compromiso no se quede en las palabras de ayer", afirmó Adriana Arboleda, de la Corporación Jurídica Libertad.

Las familias esperan "no solo remover escombros, sino remover la impunidad"; permitir que se reconozca públicamente que la comuna 13 ha sido un laboratorio de guerra, represión y control social, donde se han cometido crímenes de lesa humanidad, y en la cual el paramilitarismo continúa ejerciendo control.

</div>
