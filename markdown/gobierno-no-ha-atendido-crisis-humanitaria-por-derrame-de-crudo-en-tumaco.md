Title: Gobierno no ha atendido crisis humanitaria por derrame de crudo en Tumaco
Date: 2015-06-25 16:06
Category: Ambiente, Nacional
Tags: Alto Mira y Frontera, ataque a oleoducto, derrame de petróleo, Desastre ambiental, Ecopetrol, Gobierno Nacional, Guerra en Colombia, nariño, Tumaco
Slug: gobierno-no-ha-atendido-crisis-humanitaria-por-derrame-de-crudo-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/tumaco_4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.pulzo.com]

<iframe src="http://www.ivoox.com/player_ek_4687867_2_1.html?data=lZulmZ2ae46ZmKiak5eJd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi9DWysrf0NSPstCfycaYw9nJssXdxdSYxdfNt8rnjM3iz8bSrdXV087OjdXTtozYxtffw9LJb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Sara Valencia, presidenta del Consejo Comunitario Alto Mira y Frontera, Tumaco] 

En el municipio de Tumaco, Nariño, se vive un desastre ambiental por cuenta del atentado perpetrado por la guerrilla contra un oleoducto de Ecopetrol. Pese a que algunos medios han informado que las autoridades y la empresa atienden la tragedia, lo cierto es que **“el plan de contingencia de Ecopetrol no ha servido y nadie ha mostrado una acción concreta de ayuda a la comunidad”** dice Sara Valencia, presidenta del Consejo Comunitario Alto Mira y Frontera, en Tumaco.

**“Estamos en incertidumbre y no se encuentra una solución puntual por parte del gobierno"** dice Valencia, quien resalta que aunque el petróleo ha contaminado las fuentes hídricas que proveen a la población, “**las fumigaciones indiscriminadas con glifosato también están dañado las aguas”.**

Aunque por diversos medios de comunicación masiva, se ha anunciado la llegada de carrotanques para abastecer de agua a la población, según la habitante de Alto Mira y Frontera, estos no han llegado y no se ha atendido la situación humanitaria que viven las comunidades, razón por la que **dicen sentir discriminados y desconocidos por parte de las autoridades.**

La mancha de crudo ha recorrido aproximadamente **15 kilómetros, y ha contaminado cerca de 5 fuentes de agua**. El petróleo ya llegó a la bocatoma que da paso al mar, lo que genera graves consecuencias a la flora y fauna de la zona, pues ya se ha registrado **muerte de peces y aves que habitan ese ecosistema.**

Los habitantes de Tumaco, dicen no entender con quién es la guerra, cuando los que sufren las consecuencias de esta son las comunidades, que se quedan sin agua y sin luz.
