Title: ¿La justicia restaurativa puede contrarrestar la violencia de género?
Date: 2019-07-16 13:39
Author: CtgAdm
Category: Entrevistas, Mujer
Tags: Justicia restaurativa, LIMPAL, víctimas, Violencia de género
Slug: justicia-restaurativa-puede-contrarrestar-violencia-genero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

###### [Foto:Proyecto 341] 

A diario en Colombia, cerca de 55 niñas y adolescentes son víctimas de abusos sexuales, una es asesinada cada tres días y de enero a mayo de este año, hubo 288 homicidios. En el 2018 de los abusos reportados, el 85% fue contra mujeres y el 41, 9% fueron niñas entre los 10 y 13 años, de las cuales 5713 quedaron en embarazo. Hechos que demuestran un incremento aterrador de la violencia de género en el país contra menores de edad  y mujeres en Colombia.

Este incremento en agresiones lo confirma Diana Salcedo, directora de la Liga Internacional de Mujeres por la Paz y la Libertad (LIMPAL), quien señala que en  regiones como Meta, Guaviare, Montes de María y Putumayo, la cultura  de violencia contra  las mujeres en vulnerabilidad y en condiciones de desigualdad, prevalecen debido a que continúan siendo sumamente fuertes las conductas máchistas de dominación y la imposición de roles sociales.

(Le puede interesar ["Dos mil casos de violencia de género en medio de la guerra fueron entregados a la JEP"](https://archivo.contagioradio.com/dos-mil-casos-de-violencia-de-genero-en-medio-de-la-guerra-fueron-entregados-a-las-jep/))

Por otro lado, el aumento de estas cifras ha revivido el debate sobre castigos a quienes cometan abusos a menores como cadena perpetua o pena de muerte. Aún así estas medidas son consideradas insuficientes, pues en otros países este tipo de penas no reducen las agresiones, ni proponen transformaciones reales a una sociedad que perpetúa conductas violentas de género; por ello se ha planteado la justicia restaurativa como una posible solución.

“Ni la cadena perpetua, ni la pena de muerte son soluciones constitucionales para resolver esta situación, pues este tipo de medidas no mitigan las cifras de violencia contra las niñas y las mujeres”, lo ideal son políticas de prevención  y una justicia que garantice la dignidad de las víctimas, pues según indica la Directora de LIMPAL  "las niñas siguen con su historia de vida y el Estado hace caso omiso".

### **Las agresiones parecen ser un tema prohibido y silenciado en Colombia ** 

Según la directora de LIMPAL, la violencia contra niños, niñas y adolescentes lamentablemente se ha normalizado, porque históricamente ha sucedido. Además estas conductas violentas, no se ven como un abuso si no como algo que ocurre en el círculo de la víctima, pues los padres y madres evitan hablar de temas sexuales, imposibilitando círculos de confianza seguros en donde los menores puedan encontrar refugió a la hora de ser víctimas de estos hechos.

### **¿Qué ruta tomar cuando un menor de edad es violentado?** 

Lo primordial, es creer en las niñas, niños y adolescentes, el acompañamiento debe ser inicialmente por los padres y madres.  Salcedo afirma que “en el país hay 27 duplas de género, pertenecientes a la Defensoría del Pueblo, y cada una está conformada por un abogado y un psicólogo, quienes se encargan de realizar acompañamiento a las familias y a las víctimas”.

También cuando se presentan este tipo de casos el  seguimiento  debe ser realizado por varios entes encargados, como Medicina Legal, donde se reportan los abusos y se realiza un estudio físico a la víctima, además del acompañamiento de la Procuraduría.

Así mismo es vital para las  víctimas que sus padres y madres reporten los casos, pues los niños, niñas y adolescentes no cuentan con la vocería para que ellos mismos realicen las denuncias correspondientes. (Le puede interesar ["Denuncian impunidad en caso de Feminicidio en Marinilla, Antioquia"](https://archivo.contagioradio.com/denuncian-impunidad-en-caso-de-feminicidio-en-marinilla-antioquia/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38462754" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38462754_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
