Title: El 80% de la economía en Colombia la manejan las transnacionales
Date: 2016-01-13 15:52
Category: Economía, Nacional
Tags: alvaro uribe velez, Centro Democrático, ISAGEN
Slug: el-80-de-la-economia-en-colombia-la-manejan-las-transnacionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/CYd2BkYVAAAzeXm.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: leosatira.blogspot.com 

 

###### [13 Enero 2016] 

[Con la [venta de ISAGEN](https://archivo.contagioradio.com/isagen-paso-a-bolsillo-de-la-canadiense-brookfield/) en 6,49 billones de pesos a la firma canadiense Brookfield, diversos sectores de la sociedad colombiana aseguran que era **“lo que le quedaba por raspar a la olla”,** como lo ratifica el analista económico, Libardo Sarmiento, quien asevera que "cerca del 80% de la economía en Colombia la manejan las transnacionales”.]

[**“Ya no quedaría más por privatizar y desnacionalizar”, **añade el analista, afirmando que todos los gobiernos desde Virgilio Barco hasta el actual, tienen responsabilidad frente a la privatización del patrimonio colombiano.]

[“Desde el gobierno de Barco, pasando por el de Gaviria, privatizaron todo, acabaron con el agro, el sector energético, el petróleo, entregaron el negocio de la salud y la educación al sector privado y los recursos públicos, ya lo de raspar la olla era la venta de ISAGEN”, asegura.]

[Además, precisa que pese al rechazo del Centro Democrático a la venta de la empresa generadora de energía eléctrica, “hablar de Uribe, es hablar de un caso patológico”, y asegura que "ese tipo de **reacciones del  Centro Democrático son acciones histéricas que solo buscan mantener su oposición obteniendo rendimientos políticos”.**]

[A su vez, ese rechazo de los uribistas frente a la venta de [ISAGEN](https://archivo.contagioradio.com/?s=ISAGEN), contrasta con las decisiones de Uribe Vélez cuando fue presidente de Colombia, y quien en el **año 2007 vendió el 20% de las acciones de ISAGEN, y en el 2010, Óscar Iván Zuluaga, quien era el Ministro de Hacienda dio inicio con el proceso de enajenación de la totalidad de las acciones de la empresa.**]

[Así mismo, el economista toma como ejemplo lo que ocurrió con las regalías petroleras, ya que cabe recodar que desde el gobierno de Andrés Pastrana y con las medidas legislativas tomadas por el ahora senador Uribe, **el país perdió el 60% de la participación en los negocios petroleros.** De tal manera que el expresidente “No tiene ninguna autoridad moral para oponerse a venta de ISAGEN”, señala el analista.]

[Y el panorama continúa nublándose con la llegada de **Enrique Peñalosa a la Alcaldía de Bogotá, quien ya ha planteado su intensión administrativa y política de privatizar la empresa de telefonía de la ciudad, la ETB, y el Acueducto de Bogotá.** Lo que, según Sarmiento, impide que los Estados vuelvan al control de sectores estratégicos para responder a las necesidades de los ciudadanos y a la sostenibilidad de la sociedad.]

 
