Title: "Contrastes" música de corazón en "A la Calle"
Date: 2015-06-11 09:40
Category: Sonidos Urbanos
Slug: contrastes-musica-de-corazon-en-a-la-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11174298_429005910601511_7777321221751201205_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_4601932_2_1.html?data=lZudk56Xdo6ZmKiakpaJd6KklIqgo5WbcYarpJKfj4qbh46kjoqkpZKUcYarpJKy0NnWqdfd1NnOjazWudHjjIqflKjTstXmwtjhx9iJdpOhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Contrastes, entrevista en Contagio Radio.] 

**"Contrastes"** es una agrupación nueva,  que busca experimentar musicalmente con la **fusión de ritmos y estilos de los integrantes de la banda** (ska, reggae, rock, balada pop entre otros). **"Contrastes" nace en el año 2014** por iniciativa de Manuel Quinceno y Carlos Huertas, algunos meses más tarde inician proyectos con la Casa Cultural de Arabia, donde se unen  Ana y Angélica como nuevas integrantes. La agrupación se mantiene en búsqueda constante de espacios para innovar y crear.

\[caption id="attachment\_9838" align="aligncenter" width="960"\][![11261644\_441979249304177\_8657621631946577340\_n (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11261644_441979249304177_8657621631946577340_n-1.jpg){.wp-image-9838 .size-full width="960" height="720"}](https://archivo.contagioradio.com/contrastes-musica-de-corazon-en-a-la-calle/11261644_441979249304177_8657621631946577340_n-1/) El grupo "Contrastes" en compañía de Angel, Leonardo y Mateo de "A la Calle"\[/caption\]
