Title: Revista Generación Paz: una mirada independiente sobre el proceso de paz
Date: 2016-02-09 11:55
Category: Otra Mirada
Tags: Angie Palacios, Generacion Paz, Revista Generacion Paz
Slug: revista-generacion-paz-una-mirada-independiente-sobre-el-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Afiche-LAnzamiento-Gp.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Generación Paz ] 

<iframe src="http://www.ivoox.com/player_ek_10372973_2_1.html?data=kpWgmZede5Shhpywj5WcaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncbPZ187g1saPi8bixtfOxc6Jh5SZo5jbjbXFvoanopDi0MaPscrmwsnOjc7SqMbkxtPRy8rSuMaf1NTP1MqRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Angie Palacios, Generación Paz] 

###### [9 Feb 2016 ]

Este martes en el Teatro Pablo Tobón de la ciudad de Medellín, se lanza al público la Revista Generación Paz, una **iniciativa de comunicación independiente liderada por un equipo de trabajo de más de veinte personas** entre periodistas y fotógrafos, para contar lo que está pasando en esta recta final de los Diálogos de Paz, a través de los testimonios de sus protagonistas: víctimas, representantes de gobierno, guerrilleros, campesinos y ciudadanos del común.

De acuerdo con Angie Palacios, coordinadora de contenidos de la Revista, 'Generación Paz' surge de la intención de poner en diálogo "**distintos puntos de vista, con una mirada independiente, para llevar a la gente del común lo que está sucediendo en La Habana**". Está revista digital se editará semanalmente y contará con eventuales ediciones impresas, en las se podrán encontrar reportajes, caricaturas e infografías sobre el Proceso de Paz, para que la sociedad civil conozca la diversidad de voces que se han tejido frente a este momento histórico de nuestro país, agrega.

Quienes hacen integran esta estrategía de comunicación son en su mayoría periodistas y fotográfos de Bogotá y Medellín, que extienden la invitación a todos los colombianos a conocer la Revista y ponerse en contacto a través de la página oficial [[generacionpaz.co](http://generacionpaz.co/)], del correo [<generacionpazcolombia@gmail.com> ]y de las redes sociales [[@rgeneracionpaz](https://twitter.com/RGeneracionPaz)]. Así como a seguir de cerca el lanzamiento oficial desde las 6:30 pm a través de [[morada.co](http://morada.co/). ]

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 

######  
