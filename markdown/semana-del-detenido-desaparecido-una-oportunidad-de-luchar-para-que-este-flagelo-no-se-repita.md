Title: Semana del Detenido Desaparecido, una oportunidad de luchar para que este flagelo no se repita
Date: 2020-05-25 19:19
Author: CtgAdm
Category: Actualidad, Nacional
Slug: semana-del-detenido-desaparecido-una-oportunidad-de-luchar-para-que-este-flagelo-no-se-repita
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Desaparecidos_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La semana del Detenido Desaparecido en este 2020 plantea unos retos muy importantes en torno a los derechos de las víctimas. Algunos de los **retos tienen que ver con el resguardo de los cuerpos en los cementerios; proteger los sitios en los que, según las víctimas hay [desaparecidos](https://archivo.contagioradio.com/victimas-del-palacio-de-justicia-temen-que-jep-sea-escenario-de-impunidad-para-arias-cabrales/)**; identificar a los cientos de personas enterradas como NN y que pueden corresponder a personas dadas por desaparecidas; y buscar verdad y justicia sobre estos crímenes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y es que **aunque la desaparición forzada de personas ha sido una escabrosa práctica en muchos países del mundo y las dictaduras militares han sido las más señaladas por cometer este tipo de delito**, el caso colombiano sorprende, pues a pesar de que sus recientes gobiernos han sido elegidos por el voto popular, la cifra de personas detenidas desaparecidas podría rondar los cien mil, muy por encima de registros oficiales de países como Chile y Argentina.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La protección de los cementerios, un reto vinculante para quienes los han administrado

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aunque en **Colombia** **la legislación tiene algunas claridades en la Resolución 1447 de 2009, del Ministerio de la Protección Social, que reglamenta la prestación de los servicios de cementerios**, inhumación, exhumación y cremación de cadáveres, son muy diversas las figuras ciudadanas u organizativas que deberían aplicarla.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Organizaciones como el **[Colectivo Orlando Fals Borda](https://archivo.contagioradio.com/manejo-erroneo-de-cementerios-por-covid-19-pondria-en-riesgo-memoria-historica-del-conflicto/) o Equitas han señalado la importancia de resguardar los cementerios para intentar que los cuerpos que podrían ser de desaparecidos sean identificados**. Un llamado al que la institucionalidad debería atender con prontitud pues el tiempo pasa y las medidas deben ser inmediatas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La identificación de los cuerpos otro reto para quienes trabajan contra la desaparición forzada

<!-- /wp:heading -->

<!-- wp:paragraph -->

Recientemente se conoció la decisión de la J**urisdicción Especial para la Paz en la que ordenó medidas cautelares sobre los restos de personas que se encontraron en la morgue de la Universidad de Antioquia**, pues sobre ellos se presume pueden corresponder a víctimas de desaparición forzada. Este es un ejemplo que podría representar la situación de varias morgues y cementerios y otros sitios con esa realidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La memoria es fundamental en la lucha contra la desaparición forzada

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una de las grandes apuestas de los familiares de las víctimas de desaparición forzada es mantener viva la memoria de quienes hoy no están. Por ello la Semana del Detenido Desaparecido se mantiene como un ritual para ello. Por eso compartimos con ustedes la programación de esta jornada en Colombia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Semana del Detenido: \#MemoriaEnCasa**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**26 de mayo:** Recolección de firmas para acción en change. LANZAMIENTO VIRTUAL Campaña Dónde Están por la ratificación plena del Comité de la ONU contra las Desapariciones Forzadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**27 de mayo:** Webinar sobre **¿Cuál es el contexto actual de la Desaparición Forzada y con qué instituciones cuentan las víctimas?** (5:30 p.m.) Diferencias, articulación y garantía de los Derechos de las Víctimas desde la UBPD y la Comisión de Búsqueda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**27 de mayo:** Ausencias: Desaparición forzada y covid-19. Un diálogo para la vida, la dgnidad y la Esperanza. Modera César Augusto Muñoz, ASFADDES. Hora: 10:30 a.m. Facebook: Comisión de la Verdad

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**28 de mayo:** LANZAMIENTO Galería nos siguen desapareciendo web (casos de 2016 hasta 2020) Movice CCEEU (2:00 p.m - 5:00 p.m) 10 am: presentación informe Sucre a la CEV sobre casos de desaparición forzada en el Dpto. [Movice](https://movimientodevictimas.org/semana-internacional-del-detenido-desaparecido-2020/) -CSPP

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**29 de mayo: ** **4:00 p.m.** Conversatorio y Documental “Removiendo Tierra”, Corporación Claretiana Norman Pérez Bello

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**7:00 - 9:00 p.m**. Acción conmemorativa desde el hogar de cada familia que participa en la Fundación Hasta Encontrarlos buscando a su ser querido. **Acto de memoria y homenaje.** Se estarán enviando fotos y vídeos de acciones de conmemoración de las familias en sus hogares, desde Bogotá, Cartagena, Cali, San José del Guaviare, Pereira, Buenaventura, y desde Canadá, Francia, Holanda, Costa Rica, Uruguay, México., Fundación Hasta Encontrarlos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**25 de mayo al 30 (toda la semana):** Galería de la memoria en casa Movice. **\#MemoriaEnCasa.** Serenata vía WhatsApp a familiares de personas desaparecidas, Corporación para el Desarrollo Regional (Valle del Cauca)

<!-- /wp:paragraph -->
