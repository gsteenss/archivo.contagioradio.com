Title: Citan a declarar al Gral. Luis Fernando Navarro por bombardeo a menores
Date: 2020-10-28 12:39
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Bombardeo a menores, Carlos Holmes Trujillo, Luis Fernando Navarro, Procuraduría General de la Nación
Slug: citan-a-declarar-al-gral-luis-fernando-navarro-por-bombardeo-a-menores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Comandante-Luis-Fernando-Navarro-y-Carlos-Holmes-Trujillo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Para el próximo viernes 30 de octubre quedó agendada la declaración que rendirá el general, Luis Fernando Navarro, comandante de las Fuerzas Militares, en el marco de la indagación preliminar que adelanta la Procuraduría General de la Nación por el bombardeo** que se efectuó el 29 de agosto del año 2019 en contra de un campamento ubicado en San Vicente del Caguán, Caqueta en el que se encontraba el disidente, Rogelio Bolívar Córdoba, más conocido por su alias de “Gildardo Cucho”; en el que murieron al menos 8 menores de edad luego de que el Ejército abriera fuego en la zona.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a este caso, una reciente investigación de [Dejusticia](https://www.dejusticia.org/); reveló que informes de inteligencia realizados 5 días antes del operativo, daban cuenta de la presencia de menores en el campamento, ya que, **las Fuerzas Militares conocían que el esquema de seguridad de alias Gildardo estaba integrado por niños que este había reclutado forzosamente.** (Lea también: [Presidente Duque y altos mandos militares sabían de los menores y autorizaron bombardeo](https://archivo.contagioradio.com/presidente-duque-y-altos-mandos-militares-sabian-de-los-menores-y-autorizaron-bombardeo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De esa información se extrajo que tanto **los altos mandos militares que ejecutaron el operativo, como el entonces ministro de Defensa, Guillermo Botero y el Presidente Iván Duque quienes lo ordenaron, eran plenamente conscientes de la presencia de los niños en el campamento y aun así autorizaron el bombardeo,** lo cual causó la muerte de esos ocho menores, que podrían llegar a ser incluso 10, según la información que ha suministrado el senador Roy Barreras quien asegura que dos niños más, quedaron desaparecidos y podrían haber fallecido en el ataque. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Justamente el senador Barreras, reveló el pasado jueves, en medio del debate de control político contra el Ministro de Defensa, Carlos Holmes Trujillo, que había podido documentar la muerte de 36 menores, en medio de operativos militares adelantados por las Fuerzas Oficiales del Estado; lo cual daría cuenta de una práctica recurrente al interior de esas instituciones. (Le puede interesar: [Hay políticas que propician la criminalidad en las Fuerzas Militares: CCEEU](https://archivo.contagioradio.com/hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Gral. L**uis Fernando Navarro confirmó presencia de menores en campamento de alias “Uriel”**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ayer lunes mientras el Gobierno Nacional reportaba la muerte del jefe político del Frente Occidental del Ejército de Liberación Nacional -ELN-, alias “Uriel”; **se alertó sobre la posible presencia de menores en el campamento donde fue abatido el alto mando del ELN; lo cual fue confirmado por el mismo comandante, Luis Fernando Navarro** —hoy citado a declarar por la Procuraduría— quien señaló que en las labores de inteligencia se pudo determinar que había dos menores de edad que no hacían parte del grupo armado en dicho campamento.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RoyBarreras/status/1320496942846271493","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RoyBarreras/status/1320496942846271493

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ArielAnaliza/status/1320497952889229317","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ArielAnaliza/status/1320497952889229317

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Pese a esa información las Fuerzas Militares desplegaron el ataque en el que fue abatido alias Uriel; y aunque el comandante Navarro, afirmó que una vez realizado el levantamiento y la inspección del área en la que fue abatido el miembro del ELN, no había indicio de que los niños hubieran resultado afectados en la operación militar; la misma se desplegó, aun ante la posibilidad de que los menores hubiesen podido estar en la zona pudiendo resultar heridos o muertos. (Lea también: [Con operativo contra alias “Uriel”, Gobierno cierra más la puerta a salida negociada con el ELN](https://archivo.contagioradio.com/con-operativo-contra-alias-uriel-gobierno-cierra-mas-la-puerta-a-salida-negociada-con-el-eln/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las declaraciones del Ministro Carlos Holmes Trujillo

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este lunes en medio de la rueda de prensa en la que el Ministro de Defensa dio parte del operativo y confirmó la muerte de alias “Uriel”, manifestó que el derecho internacional no prohíbe hacer operativos militares cuando hay presencia de menores de edad. “*Se pueden hacer operaciones militares cuando hay menores*”, expresó en su declaración Holmes Trujillo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta directriz fue rechazada por varios sectores y se interpretó que el Ministro daba luz verde a las Fuerzas Armadas para bombardear zonas donde se tenga sospecha de la presencia de algún objetivo militar; sin importar si hay niños, aunque estos no pertenezcan al grupo armado contra el que se arremete o en todo caso, que hagan parte del mismo por haber sido reclutados forzosamente.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JuanPoe/status/1320795063220424706","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JuanPoe/status/1320795063220424706

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
