Title: Cinco mujeres reciben el Premio de Derechos Humanos 2016
Date: 2017-01-31 12:25
Category: Mujer, Nacional
Tags: Defensoras de Derechos Humanos, mujeres, Premios
Slug: cinco-lideresas-galardonadas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/premio-derechos-humanos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [31 Ene. 2017] 

Por su labor y entrega en pro de la defensa de los Derechos Humanos, **cinco lideresas de Colombia fueron galardonadas con el *Premio de Derechos Humanos 2016*** en España, otorgado por la Asociación Pro Derechos de Andalucía (Apdha). Este reconocimiento también resalta el trabajo de ellas en la construcción de la paz en Colombia.

Jahel Quiroga, directora de Reiniciar, Patricia Ariza sobreviviente del genocidio contra la Unión Patriótica, la ex senadora Gloria Inés Ramírez, la dirigente de Marcha Patriótica, Piedad Córdoba y Luz Marina Bernal, una de las madres de Soacha fueron las lideresas galardonadas. Le puede interesar: [Condena de CIDH a Colombia es una sentencia con rostro de mujer](https://archivo.contagioradio.com/condena-con-rostro-de-mujer/)

Durante la ceremonia de entrega del premio, Apdha manifestó que conceder este premio además de reconocer la ardua labor de las lideresas también hace un llamado y crea una “**alerta frente a los asesinatos constantes de los defensores en Colombia, ya que el incremento de la violencia y los asesinatos de dirigentes** de movimientos sociales desde el plebiscito por la paz, parece imparable”.

El premio, que es entregado cada año para apoyar y sensibilizar sobre las afectaciones que sufren violaciones de los DDHH, también pretende destacar la labor de las y los líderes y defensores y defensoras en diversos lugares del mundo.

El acto fue presidido por José María González, Alcalde de Andalucía y estuvo acompañado además por diversos líderes políticos y sociedad civil interesada en este galardón. Le puede interesar: ["Estado colombiano permitió, propició y toleró genocidio de la UP": Reiniciar](https://archivo.contagioradio.com/estado-colombiano-permitio-propicio-y-tolero-genocidio-de-la-up/)

**¿Quiénes son las lideresas galardonadas?**

**Jahel Quiroga.** Ingeniera Industrial, abogada y **reconocida defensora de Derechos Humanos. En la actualidad es directora de la Corporación Reiniciar.** Fue dos veces concejal de Barrancabermeja y una de las creadoras del Comité Regional para la Defensa de los Derechos Humanos (Credhos) en ese mismo municipio. La lideresa ha denunciado ante la Comisión Interamericana de Derechos Humanos ser víctima de estigmatización, acoso, amenazas, creando a menudo investigaciones judiciales sin fundamento, y llevando a cabo actividades de inteligencia en contra de ella. A través del tiempo ha abanderado la lucha por la verdad, justicia, reparación garantías de no repetición y salvaguardar la memoria del genocidio de la Unión Patriótica.

**Patricia Ariza.** Historiadora de arte de la Universidad Nacional de Colombia, dramaturga y directora. Cofundadora de la Casa de la Cultura (1966)-actual Teatro La Candelaria-; además es la fundadora de la Corporación Colombiana de Teatro (1969), y participo en el Movimiento Cultural con los Sectores Marginados (1995), y dirige los grupos Rapsoda, Travesía, Flores de Otoño y Tramaluna Teatro. Además **es promotora cultural y social. Es sobreviviente del genocidio de la Unión Patriótica.** Patricia Ariza se ha destacado a lo largo de sus 50 años de vida teatral por **apoyar las diferentes luchas y reivindicaciones del Movimiento de Mujeres en el país.**

**Piedad Córdoba. Abogada** e integrante del Partido Liberal. Es líder del Movimiento Poder Ciudadano  Siglo XXI. Fue senadora de Colombia desde 1994 hasta el momento en que es destituida en 2010. Pese a las investigaciones abiertas en su contra y un largo proceso de estigmatización por las acusaciones hechas por el ahora ex procurador Alejandro Ordoñez, de ser colaboradora de las FARC, el Consejo de Estado en 2016 retiró las os inhabilidades por falta de pruebas en su contra, lo que le devolvió la posibilidad de ocupar cargos públicos y de elección popular. **Actualmente es líder y dirigente del Movimiento Social y Político Marcha Patriótica.** De manera recientemente anunció su candidatura presidencial para el 2018.

**Luz Marina Bernal. Reconocida defensora de derechos humanos. Es una de las madres de Soacha (Bogotá) en Colombia.** Luz Marina es víctima del Estado por el asesinato de uno de sus hijos, quien fue posteriormente presentado como una ejecución extrajudicial o “falso positivo”. El hijo de Luz Marina tenía 26 y una condición especial, pese a eso la Brigada Móvil \#15 en Ocaña, Norte de Santander, lo presentó como parte de las FARC. **Luz Marina fue nominada al Premio Nobel de Paz 2015.**

**Gloría Inés Ramírez.** Licenciada en física y matemáticas. Es política y docente. **Entre 2006 y 2014 fue senadora de Colombia.** Integrante del Partido Comunista Colombiano, de la Unión Patriótica, del Frente Social y Político y en la actualidad es parte del  Polo Democrático Alternativo. **Reconocida por su actividad sindical y por la defensa de los derechos de la mujer y la equidad de género.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
