Title: Hallan sin vida a Juliana León, hija de sindicalista en Envigado
Date: 2015-09-11 15:30
Category: Uncategorized
Tags: Antioquia, El Dorado, El Poblado, Enviado, Hildebrando León, Juliana León, Sindicalismo en Colombia, UNEB
Slug: hallan-sin-vida-a-juliana-leon-hija-de-sindicalista-en-enviado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Juliana-Uneb1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CUT 

Este viernes a medio día, **se halló el cuerpo sin vida de la joven universitaria de 18 años Juliana León,  hija de Hildebrando León,** secretario general de la Unión Nacional de Empleados Bancarios UNEB.

El cadáver de la joven fue hallado por las autoridades en un árbol, en el sector del Salado, Envigado, en el Sur del Valle de Aburrá.

Desde el pasado 6 de septiembre en el municipio de Envigado, la hija del sindicalista se encontraba  desaparecida. Juliana había salido de su casa en el barrio El Dorado, el pasado domingo sobre las 8 de la mañana hacia El Poblado, pero nunca llegó a su destino y desde entonces no se sabía nada sobre su paradero.

**“Lamentamos con profundo dolor informar que hoy 11 de septiembre hacia el mediodía se ha confirmado el trágico deceso de JULIANA LEÓN PULGARÍN** hija de nuestro compañero Hildebrando León Cortés”, dice el comunicado de la UNEB, frente al trágico hecho.
