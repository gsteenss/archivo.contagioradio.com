Title: Continúa la tensión en Venezuela en medio de las conversaciones con la oposición
Date: 2016-11-01 17:06
Category: El mundo, Nacional
Tags: Democracia en Venezuela, Diálogos en Venezuela, Oposición Venezuela
Slug: continua-la-tension-en-venezuela-en-medio-de-las-conversaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/MesadeDialogo-Venezuela-e1478037801721.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [1 Nov de 2016] 

El pasado lunes 31 de Octubre, se llevó a cabo la primera reunión oficial entre el Gobierno del presidente Nicolás Maduro y la oposición venezolana. El encuentro que se extendió hasta la madrugada del martes, tuvo como conclusión **la necesidad de generar conversaciones con la oposición, que disten de meras retaliaciones y se dio paso a la conformación de cuatro mesas de trabajo. **Le puede interesar: [4 mesas de trabajo acuerdan gobierno y oposición en Venezuela.](https://archivo.contagioradio.com/dialogo-gobierno-oposicion-venezuela/)

Hernán Vargas, ciudadano venezolano e integrante de la Articulación Continental de Movimientos ALBA, señaló que estas mesas deberán **“girar sobre el respeto a la soberanía, la reparación de las víctimas, la construcción del cronograma electoral y el análisis de la situación social y económica”. **

Las mesas se dispondrán de la siguiente forma:

**La primera mesa**: Paz, Respeto al Estado de Derecho y a la Soberanía Nacional, y estará bajo la coordinación del ex presidente español, José Luís Rodríguez Zapatero.

**La segunda mesa**: Verdad, Justicia, Derechos Humanos, Reparación de Víctimas y Reconciliación, con la coordinación de un delegado del Vaticano.

**La tercera mesa:** Económico-Social, que trabajará también el tema de abastecimiento, será coordinada por el ex presidente de República Dominicana,  Leonel Fernández.

**La cuarta mesa:** Generación de Confianza y Cronograma Electoral, estará a cargo del ex presidente de Panamá, Martín Torrijos.

Según Vargas, la metodología es que en cada mesa hayan delegados internacionales, ex presidentes, expertos en el tema de Derechos Humanos, un representante del chavismo y uno de la oposición. Añadió que para la oposición el tema central será el asunto electoral, dejando de lado las problemáticas sociales que atraviesa el país. **“A la derecha solo le interesa hacer un golpe de estado, quedarse con el poder, pero no resolver las problemáticas de fondo de Venezuela”** dijo Vargas.

Por otra parte, manifestó que “los presos políticos que reclama la oposición están presos por actos criminales”, otro de los retos en estas conversaciones que tendrán continuidad el próximo 7 de Noviembre. Por último, invitó a los países hermanos de Venezuela a estar atentos de esta situación para evitar, y de ser necesario denunciar posibles violaciones a los Derechos Humanos, **“sabemos que hay sectores que van a generar conflictos de calle y confrontaciones en medio de los diálogos”.**

<iframe id="audio_13572042" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13572042_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
