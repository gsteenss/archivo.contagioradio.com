Title: Admiten demanda contra nuevo Código de Policía
Date: 2016-09-15 17:48
Category: DDHH, Nacional
Tags: código de policía, Corte Constitucional, Fuerza Pública
Slug: admiten-demanda-contra-nuevo-codigo-de-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/23_08_2015corte_constitucional-e1473979535347.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colprensa 

###### [15 Sep 2016]

La demanda contra el nuevo Código de Policía, en la que se alega que se violan derechos fundamentales estipulados en la Constitución Política, como lo es el derecho a la protesta y el impacto Pacto Internacional de Derechos Civiles y Políticos**, fue admitida hoy por la Corte Constitucional para su estudio.**

Esta acción fue interpuesta el pasado 8 de septiembre por congresistas y  organizaciones sociales como la Coordinación Colombia-Europa-Estados Unidos, que señala que este nuevo Código **es inconstitucional** por la  forma como presentó, pues de acuerdo con ellos, debió ser una ley estatutaria  para  tener control previo de constitucionalidad y no se permitió una discusión del proyecto.

Además, aseguran que se le otorga [una serie de facultades extraordinarias a la fuerza pública](https://archivo.contagioradio.com/nuevo-codigo-de-policia-es-totalmente-arbitrario/), por ejemplo se permite interponer una serie de sanciones sin algún tipo de control y contiene medidas que atentan contra el derecho a la protesta social o que violentan la libertad de domicilio.

Los demandantes esperan que la acción sea atendida "en virtud de los tiempos que paz que se acercan", pues esta **se conviertiría en un código "para dictaduras,** y que [no sirve para el posconflicto ni la convivencia ciudadana](https://archivo.contagioradio.com/este-codigo-de-policia-es-para-la-dictadura-no-para-el-posconflicto-alirio-uribe/)", como lo expresó el representante Alirio Uribe.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
