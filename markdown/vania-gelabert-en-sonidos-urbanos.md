Title: Vania Gelabert en Sonidos Urbanos
Date: 2016-06-27 16:35
Category: Cultura, Sonidos Urbanos
Tags: Cultura, Música
Slug: vania-gelabert-en-sonidos-urbanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Vania-Gelabert-concierto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Vania Gelabert 

Música hecha desde el corazón es la que compone la cantautora y folclorista **Vania Gelabert**, quien a muy temprana edad inició su carrera en el coro del colegio y grabando su voz en casetes. Nacida en Rusia y enamorada del folclor colombiano, en especial de la percusión de la región Caribe, con sus canciones recorre el amor, los problemas sociales y ambientales.  Vania nos acompaña con sus historias, su guitarra y sus secretos en un nuevo capítulo de Sonidos Urbanos.

Dale play a este programa y conoce el recorrido de la música y vida de **Vania Gelabert**.

<iframe id="audio_12018662" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12018662_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
