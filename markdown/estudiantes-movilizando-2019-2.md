Title: ¿Por qué los estudiantes se seguirán movilizando en 2019?
Date: 2019-01-30 16:18
Author: AdminContagio
Category: DDHH, Educación
Tags: ESMAD, estudiantes, Generación E, UNEES
Slug: estudiantes-movilizando-2019-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/IMG_2740-e1548881035360-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Ene 2019] 

Integrantes de la **Unión Nacional de Estudiantes por la Educación Superior (UNEES),** aseguran que el acuerdo alcanzado con el Gobierno para garantizar los recursos que permitan el funcionamiento de las **Instituciones de Educación Superior (IES)** públicas durante los próximos 4 años solo es un paso, y hay otros muy importantes que deben darse para transformar el modelo de educación en el país.

**Valentina Ávila, una de las delegadas ante el Gobierno de la UNEES,** sostuvo que el segundo paso que debe dar el movimiento es trabajar por el desmonte del ESMAD, "porque toda la represión que vivimos el año pasado nos mostró que **la defensa por la vida tiene que ser una bandera inamovible del movimiento estudiantil**", donde la protesta sirve como vehículo para presionar la negociación con el Gobierno y el posterior acuerdo.

No obstante, la activista reconoció que frente al respeto a la vida en el marco de la protesta ya se está avanzando, pues el Movimiento espera tener un acercamiento con el Gobierno en el Comité Permanente para los Derechos Humanos creado a partir del Acuerdo, y que empezará a sesionar este jueves 31 de enero. (Le puede interesar: ["Con panfletos y carteles amenazan a jóvenes de la U.de Antioquia"](https://archivo.contagioradio.com/con-panfletos-y-volantes-amenazan-a-estudiantes-de-la-u-de-antioquia/))

Para Ávila, el siguiente paso en la mesa de negociación con el Gobierno debería ser la **revisión y ajuste de la normatividad vigente con la que se aprueba el alza de precios en las matrículas de las IES privadas,** pues hasta el momento, los costos de las matriculas se elevan año tras año sin control, permitiendo que algunas Instituciones se lucren con la educación.

Otro paso, y quizá uno de los más importantes, tendría que darse en torno a la **autonomía y democracia universitaria** "porque necesitamos participación de carácter decisoria, y no solo consultiva" en las Universidades; de esta forma, los estudiantes podrían tener un rol importante en las determinaciones de sus Instituciones. Este paso también les permitiría a los jóvenes empoderarse en su rol como veedores de los recursos que posee cada administración.

### **Generación E: Una de las grandes batallas del movimiento estudiantil**

La estudiante sostuvo que "Generación E" es uno de los puntos a apostar en el movimiento, "porque si bien se lograron recursos para el cierre financiero del cuatrienio", la lucha estudiantil busca que se reforme el sistema integral de educación, lo que significa replantear la política en educación superior que es Generación E, **que destina recursos públicos para financiar matrículas en instituciones privadas**.

Al tiempo que se proponen alternativas a este modelo, Ávila afirmó que se debe cambiar la relación que el ICETEX tiene con los estudiantes, pues hay suficientes evidencias de que los créditos entregados por la entidad generan afectaciones económicas a sus solicitantes y familias, dadas las altas tasas de interés que se deben pagar. (Le puede interesar: ["Gobierno y estudiantes logran acuerdo en mesa de negociación"](https://archivo.contagioradio.com/estudiantes-gobierno-acuerdo/))

En ese sentido, una de las definiciones que tomará la UNEES se producirá en próximos días respecto al **Referendo por la Educación, con el que se pretende reformar los artículos 67 y 69** de la Constitución, convirtiendo a la educación superior en un derecho fundamental, que debe ser de acceso universal y gratuito en las IES estatales.

### **Próximamente, un nuevo ENEES para discutir estos temas** 

La activista por la educación sostuvo que en este momento los diferentes procesos asamblearios de las 61 IES que integran la UNEES se están reuniendo para activar nuevamente el movimiento; y en general han optado por levantar el paro, pero exigiendo el cumplimiento del Acuerdo logrado con el Gobierno, así como luchando por los pliegos locales de peticiones como en el caos de la Universidad Distrital o la Universidad Pedagógica Nacional.

Estos pliegos, así como la hoja de ruta que seguirá el movimiento estudiantil se discutirá en un Encuentro Nacional de Estudiantes por la Educación Superior (ENEES) que estaría por desarrollarse en el mes de febrero. (Le puede interesar: ["Estudiantes vuelven a las calles por el desmonte del ESMAD"](https://archivo.contagioradio.com/desmonte-del-esmad/))

<iframe id="audio_32002688" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32002688_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
