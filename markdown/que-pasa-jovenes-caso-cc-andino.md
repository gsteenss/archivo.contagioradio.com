Title: Liberados y re capturados: ¿Qué pasa con los jóvenes implicados en el caso del CC Andino?
Date: 2018-08-27 13:40
Category: Judicial, Nacional
Tags: Andino, Fiscalía, montaje judicial, Nestor Humberto Martínez
Slug: que-pasa-jovenes-caso-cc-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/jovenes-detenidos-andino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [27 Ago 2018]

Tras la orden de libertad dictada el pasado viernes por el juzgado que lleva el caso de los **10 jóvenes implicados en el atentado del Centro Comercial Andino**, la Fiscalía inició un nuevo proceso en su contra, señalando su participación en actos delictivos ocurridos en Medellín. Contagio Radio conoció un audio en el que los jóvenes afirman que esto es un nuevo montaje judicial que se está preparando desde el ente acusador.

**"La Fiscalía nos monta un nuevo proceso judicial, por hechos que sucedieron en febrero, cuando nosotros estábamos pagando una medida privativa de la libertad": Cesar Barrera desde la Cárcel la Picota.**

<iframe src="https://co.ivoox.com/es/player_ek_28108083_2_1.html?data=k52ekp2UfJShhpywj5WYaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncaTZ1MbfjafFttPZ08aSlKiPrc7kzc7Qw8nTb9Hj05DSzpDHpdTjjKibpZOPhc_YytPcj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En ese sentido, Gloria Silva, abogada de los implicados, sostuvo que el proceso de re captura no se realizó de forma adecuada y añadió que los nuevos hechos por los que se les sindica deberían ser revisados, teniendo en cuenta que para la fecha en que tuvieron lugar, los jóvenes se encontraban recluidos en centro penitenciarios. (Le puede interesar: ["La versión de los jóvenes capturados por el atentado"](https://archivo.contagioradio.com/entrevista-a-boris-rojas-y-lizeth-rodriguez-capturados-por-atentado-a-centro-comercial-andino/))

La abogada indicó que aún** desconoce la acusación, así como el paradero de sus defendidos,** y agregó que en las próximas horas debería realizarse la audiencia de legalización de captura. Adicionalmente, denunció que no se cumplió con la orden de libertad impartida por el juzgado el día viernes, prueba de ello es que **3 de los jóvenes fueron liberados y posteriormente re capturados el domingo, mientras el resto no han recibido siquiera orden de salida.**

En un comunicado emitido por la defensa, denunciaron que el domingo 26 de agosto, cuando los familiares esperaban la salida de Iván Darío Ramírez, Cristian Sandoval y Juan Camilo Pulido de la cárcel la Modelo, llegaron uniformados del ESMAD y de la Policía, quienes re capturaron a los 3 jóvenes. La Bancada sostuvo también que hay una orden de captura por el delito de rebelión contra los demás implicados, avalada por el Juez 4 penal municipal con funciones de control de garantías de Medellín

### **La Fiscalía acude a la arbitrariedad, como en el caso del Andino** 

<iframe src="https://co.ivoox.com/es/player_ek_28108250_2_1.html?data=k52ekp2WeZGhhpywj5aXaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5yncajg0NfWw5C3rc3qwoqfpZDFptDbwsnOjc7RtM3dxMbR0diPp8Ln0JCwpZClssXdz9Sah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para Silva, esta maniobra judicial es una muestra más de la arbitrariedad a la que acude la Fiscalía en busca de que los jóvenes no recobren su libertad, quienes según sus familiares, ya habían cumplido 14 meses recluidos, tiempo máximo en que se puede aplicar la medida de detención. (Le puede interesar:["Cárcel está motivada por 'posturas ideológicas'"](https://archivo.contagioradio.com/carcel-para-implicados-en-caso-andino-esta-motivada-por-posturas-ideologicas/))

Ante el anuncio de Nestor Humberto Martinez, Fiscal General de la Nación, de cambiar al Fiscal encargado del caso, Silva aseguró que esto no debería representar una demora para el proceso, y sostuvo que se trataba de una “sanción evidente al fiscal que venía asumiendo el caso”. (Le puede interesar:["Estas es la carta de Violeta Arango, acusada por el atentado"](https://archivo.contagioradio.com/esta-es-la-carta-de-violeta-arango-acusada-de-explosion-en-el-cc-andino/))

La Abogada concluyó que, a pesar de la orden de libertad, el proceso continuaría de forma normal, y afirmó que **Alejandra Méndez, Natalia Trujillo, Lina Jiménez, Lizeth Rodríguez, Iván Darío Ramírez, Boris Rojas, César Barrera, Cristian Sandoval, Juan Camilo Pulido y Andrés Bohórquez**; son quienes principalmente buscan saber la verdad sobre el atentado del Andino que tuvo lugar en junio del 2017.

[Comunicado Bancada de la Defensa de implicados en el caso del Andino](https://www.scribd.com/document/387159242/Comunicado-Bancada-de-la-Defensa-de-implicados-en-el-caso-del-Andino#from_embed "View Comunicado Bancada de la Defensa de implicados en el caso del Andino on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_51133" class="scribd_iframe_embed" title="Comunicado Bancada de la Defensa de implicados en el caso del Andino" src="https://www.scribd.com/embeds/387159242/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-nnIbYbjqy5oJmoq7DJlt&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
 

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
