Title: Por estas razones el mundo debe proteger la vida del oso de anteojos
Date: 2016-02-23 18:15
Category: Animales, Nacional
Tags: animales en vías de extinción, oso de anteojos, Parques Naturales de Colombia
Slug: por-que-defender-el-oso-de-anteojos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Oso-de-anteojos-e1456269308627.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colombia wiki 

###### [23 Feb 2016] 

En el mundo existen ocho especies de osos y de ellas, sólo una habita en Sudamérica, y ese es el **oso de anteojos**, el carnívoro más grande de esta parte del continente. También conocido como oso frontino, oso andino, oso sudamericano, ucumari y jukumari, esta especie puede llegar a medir 1.8 metros y el peso de los machos adultos ronda los 140 kg. Lo que más lo caracteriza es la presencia de manchas blancas o amarillentas en torno a los ojos, que en ocasiones llegan a la zona de la garganta y pecho.

**Desde el año 2001 este animal pasó de ser especie vulnerable a especie altamente vulnerable a extinción** según la declaratoria de la Unión Internacional para la Conservación de la Naturaleza (UICN), organismo que evalúa anualmente la situación de las especies de flora y fauna en el mundo.Para el año 2014 su condición de alta vulnerabilidad fue ratificada por la Resolución 192 de 2014 del Ministerio de Ambiente y Desarrollo Sostenible.

El peligro de extinción del oso de anteojos está determinado por la destrucción o "fragmentación" de su hábitat. Esto significa que **sus lugares de residencia son invadidos por agricultores, extractores forestales, cazadores y otras formas de actividad humana como el desarrollo de infraestructura vial,** el uso indiscriminado de pesticidas, el crecimiento acelerado de la minería y la explotación petrolera.

Frente a esta problemática que no parece frenar ni disminuir y con motivo del Día Internacional para la Protección de los Osos que se celebra el 21 de febrero, Parques Naturales Nacionales de Colombia anunció una campaña para promover el cuidado del oso de anteojos en el país. Según declaraciones de su directora, Julia Miranda,“Esta es una excelente oportunidad para que aquellos ciudadanos que vivimos en las ciudades nos concienticemos de su valor e importancia (de los osos) y aquellos que viven cerca de su hábitat aprendan a convivir con ellos”.

Y es que precisamente la importancia del oso de anteojos no es muy conocida por la sociedad colombiana, siendo éste un reforestador natural que dispersa las semillas de las plantas cuyos frutos consume, logrando el mantenimiento y recuperación del bosque. Además, actúa como un polinizador, transportando polen de diversas plantas en su espeso pelaje y estimula el crecimiento del bosque a través de la ruptura de ramas, lo cual ayuda a la entrada de los rayos solares hacia los niveles inferiores, impartiendo la energía necesaria para que las plantas desarrollen y se estabilice la estructuración del bosque.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
