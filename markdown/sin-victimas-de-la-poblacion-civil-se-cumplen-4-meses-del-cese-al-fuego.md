Title: Sin víctimas de población civil se cumplen 4 meses de cese al fuego
Date: 2017-01-02 15:37
Category: Nacional, Paz
Tags: acuerdo cese al fuego farc y gobierno, Cese al fuego bilateral, FARC, Fuerza Pública, Gobierno, paz
Slug: sin-victimas-de-la-poblacion-civil-se-cumplen-4-meses-del-cese-al-fuego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Captura-de-pantalla-2015-07-13-a-las-12.37.13.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [02 Ene. 2017] 

**Luego de mas de 122 días de Cese al Fuego Bilateral** entre la guerrilla de las FARC-EP y el Gobierno Nacional, el Centro de Recursos para el análisis de Conflictos (CERAC) ha asegurado en su último informe que han registrado un **cumplimiento "casi total" de los compromisos y protocolos que definen el cese.**

En dicho informe el CERAC hace un recuento de lo que han sido estos 4 meses con el cese al fuego bilateral, en el cual aseguran que "pese a haberse registrado un combate entre la fuerza pública y las FARC, en el que se mostró la violación al protocolo y en el que fallecieron dos guerrilleros", **a grandes rasgos el cese ha significado que durante este tiempo en el país "no se se registren víctimas mortales o heridos de la sociedad civil ni de la fuerza pública". **

Sin embargo, cabe recordar que el Mecanismo Tripartito** aseguró en un informe que dado el incumplimiento por parte de los integrantes de las Farc al protocolo, la Fuerza Pública abrió fuego en contra de ellos, sin pretender mediar otro tipo de acción previamente, argumentando que estaban ejecutando una acción contra el ELN”.** Ampliar información aquí: [Comisión de verificación del cese al fuego investiga muerte de guerrilleros](https://archivo.contagioradio.com/comision-de-monitoreo-y-verificacion-investiga-muerte-de-guerrilleros-de-las-farc-ep/)

En términos generales, el CERAC muestra un avance positivo en lo que respecta a salvaguardar la vida de civiles, integrantes de las FARC -EP y de la Fuerza Pública, sin embargo **la organizacion ha realizado un llamado frente a la expulsión de 3 integrantes de la guerrilla dado que es posible que "estos desertores se vinculen con grupos de crimen organizado, para mantener el control sobre las rentas ilegales y las rutas de narcotráfico en la región".**

Así mismo, el CERAC hace referencia en este último informe, a las denuncias realizadas por el Gobernador de Antioquia, en las que según el mandatario "menores de edad estarían siendo “reclutados” para ser explotados sexualmente en las zonas de concentración de las FARC en el departamento".

En el comunicado, el CERAC manifiesta que si bien pudieron corroborrar que se **"encontró que un guerrillero salió del punto de preconcentración sin autorización de su superior y sostuvo una discusión con un joven de la región, no hubo infracciones a los protocolos"** como lo había denunciado el Gobernador de Antioquia, Luis Pérez.

Versión que tambien fue entregada por el informe del Mecanismo Tripartiro de Monitoreo y Verificación, que comprobó que en el Punto de Preagrupamiento Temporal de Yondó, en Antioquia, **no hubo ejercicios de reclutamiento de menores, o de prostitución a los cuales se había referido Luis Perez**, gobernador de Antioquia. Le puede interesar: [Mecanismo de Monitoreo y Verificación desmintió al gobernador de Antioquia](https://archivo.contagioradio.com/mecanismo-de-monitoreo-y-verificacion-desmintio-al-gobernador-de-antioquia/)

De igual modo, CERAC manifiesta que ha registrado 18 acciones violentas que a juicio de esta organización, deben ser verificadas dado que han ocurrido en zonas de presencia histórica de las FARC y "pudiesen ser atribuidas o eventualmente responsabilidad de ese grupo".

Por último, e**l CERAC asegura que no se registraron reportes de hostilidades de tipo criminal o violento atribuibles a las FARC-EP que afectaran a la población civil y pudiesen ser corroboradas,** así como que ninguna de las partes realizó acciones ofensivas violatorias que afectaran el funcionamiento del Mecanismo de Monitoreo y Verificación (MMV) o de los Puntos de Pre Agrupamiento Temporal (PPT). Le puede interesar: [Mecanismo tripartito entrega informe sobre cese al fuego](https://archivo.contagioradio.com/mecanismo-tripartito-entrega-informe-sobre-cese-al-fuego/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)
