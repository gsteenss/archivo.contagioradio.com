Title: Europarlamentarios preocupados por incumplimientos al Acuerdo de Paz
Date: 2018-10-25 12:32
Author: AdminContagio
Category: Nacional, Paz
Tags: ELN, JEP, Jesús Santrich, Parlamento Europeo, paz
Slug: europarlamentarios-acuerdo-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz-contagioradio-e1476125455134.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [19 Oct 2018] 

**Diputados del Parlamento Europeo** en conjunto con diferentes organizaciones sociales, **expusieron sus preocupaciones sobre la implementación del Acuerdo de paz en Colombia**, la política de derechos humanos del gobierno Duque y por las irregularidades en el proceso contra Jesús Santrich. En el comunicado impulsado por la parlamentaria alemana Heike Haensel, se expone ello como riesgos que enfrenta el acuerdo y que deben ser superados de manera pronta y eficaz.

El principal énfasis de la comunicación gira en torno a señaladas irregularidades en el proceso contra Jesús Santrich, y aseguran que pone en riesgo la implementación del acuerdo de paz, dado que expone una inseguridad jurídica sobre lo acordado en la Habana. En la comunicación aseguran que es preocupante que existan tantos indicios de un proceso irregular, entre otras cosas porque las pruebas no han sido entregadas a la defensa.

Por su parte **Javier Couso Permuy**, Eurodiputado del Grupo Confederal de la Izquierda Unitaria Europea, aseguró que luego de 3 meses del gobierno Duque en Colombia, quienes firman la carta están preocupados respecto al **"retroceso evidente" en lo que respecta a la implementación del Acuerdo de paz,** así como en la mesa de diálogos con el Ejercito de Liberación Nacional.

El parlamentario explicó que los incumplimientos por parte del Gobierno, sobre lo pactado en el Acuerdo de Paz, aleja la posibilidad de negociar una salida dialogada al conflicto con el ELN. Adicionalmente, el caso de Jesús Santrich afecta las garantías que tendrían exguerrilleros en el sistema de justicia transicional, en tanto el proceso "viola la Constitución". (Le puede interesar: ["Autoridades de Antioquia descalificaron llamado de atención de Europa sobre Hidroituango"](https://archivo.contagioradio.com/autoridades-de-antioquia-descalificaron-llamado-de-atencion-de-europa-sobre-hidroituango/))

Couso sostuvo que **una de las grandes preocupaciones de los diputados son los asesinatos de líderes sociales en el país,** situación que vienen  alertando en el Parlamento Europeo. Sin embargo, reconoció que este órgano continental tiene los ojos puestos en Venezuela, y recordó que durante esta legislatura se ha logrado un récord de resoluciones contra esta nación, desconociendo lo que ocurre en Colombia, o con la crisis migratoria que sufre Centroamérica.

No obstante, el parlamentario aseguró que seguirían intentado presentar una resolución para que **desde la Unión Europea**, en su condición de garante y financiador del proceso de paz, se **acompañe de mejor forma la implementación**, y concluyó, enviando un saludo a los líderes sindicales, indígenas y campesinos que "están sufriendo el azote del nuevo paramilitarismo" afirmando que en Europa, hay una izquierda que los recuerda y defiende.

<iframe id="audio_29636892" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29636892_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
