Title: Debate en El Ecléctico: “Nuevo Código de Policía”
Date: 2016-08-04 13:28
Category: El Eclectico
Tags: código de policía, colombia, policia
Slug: debate-en-el-eclectico-nuevo-codigo-de-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/código-de-policía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

En este debate sobre las posibles violaciones a las libertades civiles que se presentan en la actualización del Código de Policía nos acompañaron Alejandro Matta, abogado y miembro de la Unidad de Trabajo Legislativo del representante a la cámara Víctor Correa; Camilo Jiménez, economista y miembro de la Unidad de Trabajo Legislativo de Alfredo Rangel; Tania López, politóloga y estudiante de derecho, quien además fue directora de Juventudes con Marta Lucía Ramírez en las pasadas elecciones presidenciales; Carolina Laverde, miembro del colectivo Vamos por los Derechos del Polo Alternativo y asistente en la Unidad de Trabajo Legislativo de Alirio Uribe  y Julián Triana, estudiante de derecho y editor de El Ecléctico.

Se habló sobre la imposición de multas económicas como medida punitiva y no preventiva, lo cual significaría un retroceso en la política criminal del país. Además se planteó la posibilidad de que algunos miembros de la Policía usen el Código y sus multas para beneficio personal a causa de la corrupción institucional.

Algo que preocupa a los sectores más conservadores de la opinión pública es el cuidado de la moral y de las buenas costumbres, por lo cual defienden las actualizaciones del Código. Este punto fue planteado en el debate dejando como duda lo que significa la obscenidad, aclarando que el concepto está sujeto a prejuicios propios, por lo que no debería constituirse como un pilar para plantear normas ciudadanas.

Los invitamos a escuchar el debate y unirse a la discusión.

<iframe src="http://co.ivoox.com/es/player_ej_12441045_2_1.html?data=kpehlpaUeJahhpywj5WVaZS1kZqSlaaXdY6ZmKialJKJe6ShkZKSmaiRdI6ZmKiapsrGpdXZjMrbjarQb6bXzYqwlYqlfcToysjch5ilb4a5k4qlkoqdh6_pxtvcjaiJh5SZo5jRy8zTb8XZjLXczs7HaaSnhqaxw4qpdoaskZKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
