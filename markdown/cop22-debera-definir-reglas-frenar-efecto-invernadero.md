Title: COP22 deberá definir reglas para frenar el efecto invernadero
Date: 2016-11-04 13:59
Category: Ambiente, Otra Mirada
Tags: Acuerdos COP 21, COP 22, gases efecto invernadero
Slug: cop22-debera-definir-reglas-frenar-efecto-invernadero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/GEI-e1478282894893.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pochtimes] 

###### [4 Nov de 2016] 

En 2015, durante la COP21 realizada en Paris, 195 países ratificaron su compromiso por la reducción de gases de efecto invernadero para evitar un aumento de 2ºC en la temperatura global. Lo acordado, **entra en vigor este viernes 4 de Noviembre, justo 3 días entes del inicio de la COP22 en Marruecos,** que busca establecer la metodología de implementación lo pactado en París.

A pesar que muchos medios han resaltado los beneficios de la cumbre de 2015, varias organizaciones ambientales como Amigos de la Tierra Internacional, afirman que hay muchas cosas que quedaron fuera de los acuerdos, y “las comunidades afectadas son las más vulnerables, merecen algo mejor que este acuerdo insuficiente; **son las que sufren los peores impactos porque los políticos no adoptan medidas lo suficientemente drásticas”.**

Por su parte, James Hansen científico estadounidense, manifestó que los acuerdos son **“un cúmulo de palabras y de promesas, sin acciones concretas”.** La organización Ecologistas en Acción de España, denunció que el pacto ignora “las luchas ciudadanas que ya están haciendo frente al cambio climático” y que “se perdió la oportunidad de reforzar un cambio de modelo, que **mantenga bajo tierra el 80% de los recursos fósiles, frene la industria extractivista y se ajuste a los límites planetarios”.**

Por otra parte, lo que pretende la cumbre en Marruecos, es la definición de las reglas de transparencia entre Estados, la búsqueda de ayuda financiera a los países en desarrollo, ayuda técnica para las **políticas de desarrollo sobre energías renovables, iniciativas de transporte y vivienda menos devoradores de energía y nuevas prácticas agrícolas.**

Una situación que preocupa, además de la vacuidad de los acuerdos COP es que los acuerdos ni siquiera han sido ratificados por todos, por ejemplo, en América Latina, Argentina, Brasil, México, Perú, Costa Rica, Bolivia, Honduras y Uruguay, ya ratificaron los acuerdos, pero en Europa, **Rusia, Japón, Italia, Polonia y Bélgica no lo han hecho. **Le puede interesar: [COP21: más emisiones, menos compromisos.](https://archivo.contagioradio.com/cop-21-mas-emisiones-menos-compromisos/)

La colombiana Paula Caballero, portavoz de la organización World Resources Institute, manifestó en un medio internacional, que “la apertura al diálogo con los países que no han ratificado no debe extenderse indefinidamente sino que **ha de fijarse un plazo hasta 2018, para que entren a formar parte del mismo o se queden fuera”.**

Además, los países asistentes a la COP22 deberán acordar una hoja de ruta para conseguir **100.000 millones de dólares anuales de financiación climática, para las nuevas iniciativas a implementar por los países en desarrollo.** Esto, a raíz de una propuesta presentada para debate por parte de Australia y Reino Unido.

La COP22, también tiene que perfilar el mecanismo para un informe de pérdidas y daños causados por el cambio climático a nivel mundial, y **fortalecer el diálogo con la sociedad civil, acuerdos logrados en la COP20 de Lima. **Le puede interesar: [¿Qué tiene que ver el cambio climático y con el Posconflicto?](https://archivo.contagioradio.com/como-se-articula-el-cambio-climatico-y-el-posconflicto/)

\[caption id="attachment\_31814" align="alignnone" width="800"\]![infografía COP21](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/cop21.jpg){.size-full .wp-image-31814 width="800" height="951"} NTX News\[/caption\]

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
