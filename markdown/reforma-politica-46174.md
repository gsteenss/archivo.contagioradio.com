Title: Reforma política suprime la Comisión de Acusaciones y crea tribunal de aforados
Date: 2017-09-04 17:03
Category: Nacional, Política
Tags: alirio uribe, Reforma Electoral
Slug: reforma-politica-46174
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/debate-congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elespectador] 

###### [04 Sept 2017] 

Concluye la aprobación de la reforma política y de acuerdo con el representante a la cámara por el Polo Democrático Alirio Uribe, es importante que haya una reforma de fondo para transformar las formas de hacer política del país. Estos serían los puntos más importantes de lo que quedó de la reforma electoral.

### **Coaliciones de pequeños partidos y financiación** 

En primera medida se encuentran las coaliciones de partidos pequeños que continúan igual, de acuerdo con Uribe, "por ahora, en los temas que nos interesan a las minorías, las coaliciones siguen como están en la Constitución, **para los partidos que tengan un umbral sumados todos inferiores a 15%**. Para efecto de las listas, queda claro que las listas cerradas y bloqueadas entraran a partir del año 2022, hay un parágrafo transitorio que dice que en el año 2018 y en el año 2019 las listas pueden ser preferentes".

En este punto Uribe manifestó que se logro que se establecieran reglas de juego mucho más democráticas para las campañas, y así posibilitar que los candidatos a diferentes instituciones **tengan una competencia en una situación de mucha mayor igualdad, fortaleciendo a los pequeños partido**s.

### **Tribunal Nacional de aforados** 

Además, **se aprobó el tribunal Nacional de Aforados,** como un nuevo mecanismo mucho más expedito y democrático, que implica que se suprime la Comisión de Acusaciones, una instancia que de acuerdo con diferentes políticos como Alirio Uribe, **no cumplía con su máxima función de investigar y sancionar a quienes hacen parte de las ramas del poder**.

"Se registraron más de **800 denuncias contra magistrados de las Cortes en la Comisión** de Acusación, y si uno mira la historia de esta comisión, después de Rojas Pinilla, solo está el caso de Samper y más recientemente el único caso donde se hizo una acusación fue a Pretelt, es decir el índice de resultados sobre más de 4.000 denuncias es ninguno, eso refleja la impunidad" afirmó Uribe.

Sin embargo, Uribe manifestó que hay un riesgo y es que la Corte Suprema señale que el Tribunal de Aforados es ilegal, debido a que no hace parte de ninguno de los puntos de los Acuerdos de Paz. (Le puede interesar: ["Cambio Radical y Centro Democrático se atraviesan al Fasst Track"](https://archivo.contagioradio.com/cambio-radical-y-centro-democratico-se-atraviesan-al-fast-track/))

### **Se acaba el Consejo Nacional Electoral ** 

El Consejo Nacional Electoral fue eliminado, conforme a la propuesta hecha por la Misión de Observación Electoral, para dar paso al Consejo Electoral Colombiano, con una nueva forma de elección de los magistrados y con nuevas funciones, para Alirio Uribe, estos cambios permiten despolitizar al Consejo Electoral:

"Lo que teníamos era un Consejo Nacional Electoral elegido por los partidos, que vigilaba a los partidos, las funciones de esta institución deben garantizar la transparencia, la moralidad, **entre otras cosas y esta institución estaba era reflejando las mayorías parlamentarias**"

De esta manera concluye uno de los debates más importantes en la primera comisión del Congreso para dar paso a otros debates como la Jurisdicción Especial de Paz y las circunscripciones de paz. (Le puede interesar: ["Circunscripciones de paz se quedarían fuera del Fast Track si no hay voluntad política"](https://archivo.contagioradio.com/circunscripciones-de-paz-se-quedarian-por-fuera-de-fast-track-si-no-hay-voluntda-politica/))

Aunque se espera que la discusión continúe, son varios los sectores políticos y sociales que insisten en la insuficiencia de los avances que se logran hasta el momento y hacen un llamado a continuar trabajando en una reforma política de fondo que sea capaz de acabar con los vicios de la política en nuestro país.

Uno de los temas que se quedan son **el voto electrónico y la financiación oficial de las campañas**, puntos que para la Misión de Observación Electoral, serían claves para avanzar. [Lea también: Esto es lo que queda de la reforma electoral](https://archivo.contagioradio.com/reforma-electoral-esta-permeada-por-personalismos-de-cara-a-elecciones-de-2018/) según la MOE.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
