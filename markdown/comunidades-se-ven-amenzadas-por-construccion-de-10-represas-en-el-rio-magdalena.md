Title: Comunidades se ven amenzadas por construcción de 10 represas en el Río Magdalena
Date: 2015-02-24 23:14
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Huila, licencias ambientales, medio ambiente, Río Magdalena
Slug: comunidades-se-ven-amenzadas-por-construccion-de-10-represas-en-el-rio-magdalena
Status: published

##### Foto: [notiagen.wordpress.com]

<iframe src="http://www.ivoox.com/player_ek_4128649_2_1.html?data=lZafmpuYfY6ZmKiakpyJd6KlkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Di1Nnf18jHrYa3lIqvldOPqMaf08rd1MrXpdSfxtOYx9GPloa3lIquptSPkcLbxcbZx9PFb8LhxtPO3MbScYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [ Flora Valenzuela, Comité por la Defensa del Río Magdalena.] 

El primero de marzo se realizará el primer **foro formativo “Somos la fuerza del campo”,** con el objetivo de rescatar la esencia campesina y la relación con la tierra y el río Magdalena, debido a que las comunidades están siendo amenazadas por el plan maestro de aprovechamiento del Río Magdalena, que contempla la construcción de 10 represas en el departamento del Huila y en toda la cuenca del río.

Durante el foro se tratarán temas como el **modelo económico y sus implicaciones en los territorios,  la economía campesina para resistir, y las zonas agroalimentarias,** para discutir propuestas con el fin de que se no se realice el mega proyecto que traería enormes impactos ambientales, económicos y sociales, debido a que este tipo de construcciones "cambian los pisos térmicos", según afirmó Flora Valenzuela, quien hace parte del Comité por la Defensa del Río Magdalena.

Se espera, que se de ser una realidad este proyecto, **“se inundaría 14 kilómetros cuadrados**, lo que generaría que la actividad económica entre en crisis”, de acuerdo a las palabras de la defensora del Río Magadalena, además, “una población bastante alta quede desplazada por los megaproyectos que se están implementando a través del gobierno nacional” agregó Valenzuela.

Las empresas, **Emgesa e HidroChina** serían las compañías, que estarían detrás de la licitación para realizar la construcción.
