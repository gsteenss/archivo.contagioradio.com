Title: Cese bilateral será tema central en el sexto ciclo de conversaciones
Date: 2018-06-15 15:59
Category: Paz, Política
Tags: ELN, Gustavo Bell, Gustavo Petro, Iván Duque, Mesa de La Habana
Slug: cese-bilateral-sera-tema-central-en-el-sexto-ciclo-de-conversaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Eln-gobierno-cese.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Jun 2018] 

A través de un comunicado de prensa las delegaciones del ELN y el gobierno Nacional, expresaron que, aunque se ha estado trabajado sobre la evaluación del último cese bilateral pactado el año pasado para retomar esta acción, el debate sobre este punto se retomara en el sexto ciclo de conversaciones que iniciará el próximo 25 de junio en La Habana, Cuba.

Para el analista Víctor de Currea el anuncio sobre el cese bilateral es un mensaje para los candidatos a las elecciones 2018, en el sentido de que “se respeta el camino de la democracia” y una muestra de voluntad de parte tanto del ELN como del Gobierno por aumentar la confianza en la paz y el **desescalamiento del conflicto armado en la cotidianidad de las comunidades.**

De igual forma señaló que este cese es producto de los informes que ambas partes presentaron sobre los errores y aciertos del anterior cese que ya se había pactado el año pasado, en donde diferentes incidentes se presentaron. **De esta manera se reformularon varios puntos y se corrigieron los mecanismos que garantizaban al mismo**.

Sin embargo, luego de este anunció, De Cuerrea expresó que la mesa debe aprovechar este mes que queda entre la elección y posesión del próximo presidente para tratar de avanzar principalmente en la construcción del mecanismo de participación social y **en garantizar un cese al fuego bilateral, nacional mucho más fuerte y duradero, que sea verificable**.

### **Iván Duque y su oposición a la mesa de La Habana** 

El analista, señaló que si bien es cierto que la mesa de La Habana ha adquirido un nuevo ritmo desde que Gustavo Bell asumió su liderazgo, el futuro de este proceso depende de quién sea el próximo presidente e Iván Duque, una de las opciones, manifestó que, de ser electo, levantará la mesa si el ELN no acepta reunir a sus tropas y acabar con prácticas como la extorsión.

Pasos que para Víctor de Currea no se dan, debido a que hacen falta las garantías necesarias que lo permitan. Razón por la cual afirmó que lo que se vota este 17 de junio “es el futuro de la paz” y **“dos modelos diferentes de como continuar o no con las negociaciones”**. (Le puede interesar: ["Cese bilateral es la garantía para la Mesa de La Habana: Carlos Velnadia"](https://archivo.contagioradio.com/cese-bilateral-el-objetivo-de-esta-semana/))

Además, de Currea también manifestó que la captura de Jesús Santrich, el regreso de Iván Márquez al Caquetá, el asesinato a integrantes de FARC y las amenazas de Iván Duque a este proceso, no solo ponen en riesgo los Acuerdos de paz ya pactados, **sino que lograría finalmente hacer “trizas” los intentos de construcción de paz en el país**.

Del otro lado, señaló que Gustavo Petro ha demostrado la intención de continuar con este proceso de paz y que ha logrado realizar alianzas como la de Antanas Mockus y Navarro Golf que apoyarían el reto de llevar a buen puerto la mesa de La Habana.

![WhatsApp Image 2018-06-15 at 3.43.05 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/WhatsApp-Image-2018-06-15-at-3.43.05-PM-572x800.jpeg){.alignnone .size-medium .wp-image-53919 width="572" height="800"}

<iframe id="audio_26560771" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26560771_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
