Title: Boletín informativo Mayo 27
Date: 2015-05-27 18:59
Category: datos
Tags: Actualidad Contagio Radio, Alfredo Araujo Castro, asesinato de los sindicalistas Valmore Locarno Rodríguez y Víctor Hugo Orcasita Amaya, Boletín Informativo, contaminación, Corredor vial II, Ley 087, Noticias del día, penaliza el maltrato animal y se declara a los animales como seres sintientes
Slug: boletin-informativo-mayo-27
Status: published

[*Noticias del Día.*]

<iframe src="http://www.ivoox.com/player_ek_4559792_2_1.html?data=lZqim5yddo6ZmKiakpuJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfk5yah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-La empresa **Corredores Viales II**, ha generado desde hace 10 años, **una grave situación de contaminación** que ha traído como consecuencia **impactos sociales, ambientales y económicos** para las comunidades negras que habitan cerca el río San Jorge, en el departamento del Cauca. **Eduardo Gonzales**, quien hace parte del **Consejo Comunitario Corredor Panamericano "el Pilón"**, hace la denuncia.

-30 jóvenes que participaron en la **Escuela Itinerante de Derechos Humanos "Jóvenes construyendo un nuevo territorio"**, que se llevó a cabo en el municipio de **Suacha entre el 11 de abril y el 30 de mayo**, serán **certificados como promotores de Derechos Humanos del municipio**. Habla **Heiner Gaitán, de la Red Juvenil de Suacha.**

-Este martes, **se aprobó por unanimidad en plenaria de Cámara de Representantes** el proyecto de **Ley 087** del representante por el partido Liberal, **Juan Carlos Losada**, por medio del cual **se penaliza el maltrato animal y se declara a los animales como seres sintientes.**

-Después de 14 años, **el 26 de Mayo fue capturado Alfredo Araujo Castro**, **Gerente de relaciones públicas de la multinacional Drummond del Cesar,** por ser el posible **responsable intelectual del asesinato** de los sindicalistas **Valmore Locarno Rodríguez y Víctor Hugo Orcasita Amaya**. Según se conoce, ha sido capturado con fines de indagatoria. Habla **Dairo Mosquera, tesorero nacional de Sintramienergética**.

-**Habitantes de Suba sufren desalojos urbanos** en relación con la **acción de los Bancos** sobre los propietarios de algunas viviendas. **Antonio Torres, habitante de la Casa Cultural 18 de Diciembre**, espacio donde las organizaciones sociales y juveniles adelantan procesos de formación y acompañamiento a la comunidad, explica cómo se han realizado los términos de desalojo.
