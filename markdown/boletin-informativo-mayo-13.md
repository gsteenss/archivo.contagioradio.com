Title: Boletín informativo Mayo 13
Date: 2015-05-13 21:59
Author: CtgAdm
Category: datos
Tags: Actualidad informativa, Noticias del día en Contagio Radio
Slug: boletin-informativo-mayo-13
Status: published

[*Noticias del Día: *]

<iframe src="http://www.ivoox.com/player_ek_4491601_2_1.html?data=lZmmk5uUdY6ZmKiakpmJd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfkpiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-En la mañana de este miércoles, nuevamente **un caballo fue víctima de explotación animal y murió en las calles de Cartagena** en plena vía pública, al parecer era **usado por cocheros** y se desplomó cuando caminaba con dificultad.

-Jovenes en Colombia **rechazan el Plan Nacional de Desarrollo** que se aprobó para el 2014-2018, al considerar que **carece de una política integral para la juventud**. Habla **Heidy Sánchez**, de la **Juventud Comunista Colombiana**.

-Distintas **organizaciones similares al Estado Islámico** han reivindicado el atentado contra un autobús que transportaba personas de la minoría **Chií ismaelita en Pakistán**. Diez personas en moto atacaron el autobús ametrallándolo y dejando la cifra de **45 muertos y 30 heridos**, en lo que se entiende como parte de la **violencia sectaria en el país**.

-**Comunidades indígenas de Arauca**, denuncian que les han sido **despojadas alrededor de 14.000 hectáreas de tierras,** además no hay agua potable, acceso a educación, ni salud. Habla **Alvaro Hernández**, vocero de las **Organizaciones Cívicas y Populares de Arauca**.

-El **fiscal de Wisconsin** no presentará cargos contra el policía que asesinó a **joven afrodescendiente Tony Robinson**.De acuerdo con el funcionario *"Mi conclusión es que esta muerte trágica y lamentable fue el resultado del uso legítimo de la fuerza de la policía y que no deben de presentarse cargos contra el oficial Kenny en la muerte de Tony Robinson"*

-Tras confirmación de la reunión entre e**l comandante de las FARC alias "Timochenko"** y el **comandante del ELN Nicolas rodríguez Bautista alias “Gabino”** en la que se presiona sobre la **necesidad de abordar un proceso con el ELN**, **Luis Eduardo Celis,** analista político, señala que **el principal problema puede ser el enfoque**. Mientras que con las FARC se plantea así: Acuerdo, dejación de armas y por último la implementación de los acuerdos, con el ELN se serían acuerdo, la implementación y por último dejación de armas.

-**El Procurador Alejandro Ordoñez**, usó un **informe del Instituto Nacional de Salud de 1984**, como herramienta **para criticar la recomendación del Ministerio de Salud** sobre dejar de **usar el glifosato para las fumigaciones**, sin embargo Ordoñez no habría tenido en cuenta que el informe concluía que **los expertos en toxicología contratados por el INS recomendaron no usar el herbicida**, pues al no existir evidencia sobre sus efectos equivaldría a experimentar con humanos.
