Title: Bolivia lejos de elecciones libres por las múltiples amenazas del gobierno de facto
Date: 2020-10-17 22:08
Author: AdminContagio
Category: Actualidad, Movilización
Slug: bolivia-lejos-de-elecciones-libres-por-las-multiples-amenazas-del-gobierno-de-facto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/EJXp1DzWwAEJvqW.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

  
Este domingo 18 de octubre, se llevarán a cabo las elecciones presidenciales, vicepresidenciales, de senadores y diputados en Bolivia. Ello en medio de militarización, estigmatización y ausencia de garantías para la veeduría internacional del proceso.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Militarización y agresión a la veeduría internacional

<!-- /wp:heading -->

<!-- wp:paragraph -->

El gobierno ordenó este sábado militarizar las principales ciudades del país y con el ejército en las calles se teme por nuevas agresiones en contra de la población por parte del gobierno de Añez. Adicionalmente se teme que si el resultado no favorece a los intereses del gobierno de facto se puedan presentar una toma del poder por parte de los militares.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual manera, preocupa el nivel de abstencionismo que se pueda suscitar en estas elecciones, pues la población tiene miedo de las represalias por parte del gobierno al ejercer su derecho al voto. [Lea también: Persecución del gobierno de facto contra diputados](https://archivo.contagioradio.com/diputados-de-bolivia-alertan-sobre-persecucion-judicial-del-gobierno-de-facto-en-su-contra/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Militares custodiarán los votos de la ciudadanía de Bolivia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Y aumenta la tensión aún más al sospecharse un posible intento de golpe de Estado por parte de las fuerzas militares y el gobierno de facto, también se sospecha que pueda darse un gran fraude electoral, con el nuevo sistema de computo que se quiere incorporar, además de poner como encargados a los militares del traslado de ánforas y actas electorales.  
.  
Es importante mencionar que, a dos días de iniciar esta jornada, fue detenido el diputado argentino Federico Fagioli en el aeropuerto de El Alto en La Paz, Bolivia, donde viajó como parte de la delegación argentina que monitoreará las elecciones presidenciales del domingo en ese país. Si bien el diputado ya fue puesto en libertad. L[ea también: Bolivia un golpe de Estado justificado con elecciones](https://archivo.contagioradio.com/bolivia-un-golpe-de-estado-que-quieren-justificar-con-elecciones/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"El Senado boliviano invitó formalmente a un grupo de diputados argentinos a que sean veedores internacionales, acompañantes electorales. Entramos en Cochabamba sin ningún problema, migraciones internacionales de Bolivia nos dejó pasar y acá (en La Paz) no nos dejan pasar. Ellos iban a garantizar que estuviéramos todos los veedores para que hubiera elecciones limpias y eso no está pasando. Retuvieron al diputado Fagioli y a todo el resto"*, indicó Grosso desde el aeropuerto de El Alto, a través de su cuenta de Twitter.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La OEA no ha actuado para garantizar elecciones libres en Bolivia

<!-- /wp:heading -->

<!-- wp:paragraph -->

“Legisladores argentinos fueron maltratados al llegar a La Paz para cumplir con sus tareas de veedores de las elecciones del próximo domingo. Es directa responsabilidad del gobierno de facto de @JeanineAnez preservar la integridad de la delegación argentina”. Afirmó Alberto Fernández. [Lea también: Así se gestaron por lo menos 24 asesinatos en protestas contra el gobierno](https://archivo.contagioradio.com/durante-gobierno-de-facto-en-bolivia-policia-habria-asesinado-24-personas/) de facto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se espera que haya acompañamiento internacional para garantizar un proceso democrático transparente y así se lleve a cabo en óptimas condiciones evitando los fraudes. Pues los partidos políticos y las Fuerzas Militares deben respetar la voluntad popular. Una jornada pacífica, libre de violencia, represión y nuevas violaciones a los derechos humanos, civiles y constitucionales de la ciudadanía, es lo que espera la comunidad boliviana.

<!-- /wp:paragraph -->
