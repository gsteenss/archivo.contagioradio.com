Title: Peña Nieto no está a la altura del pueblo mexicano
Date: 2017-01-27 16:23
Category: DDHH, El mundo
Tags: Donald Trump, Estados Unidos, mexico, peña nieto
Slug: pena-nieto-no-esta-la-altura-del-pueblo-mexicano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Peña-Nieto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: VoxBox] 

###### [27 Ene. 2017] 

Más violaciones a los derechos humanos de los inmigrantes, aumento del tráfico de drogas, desmejora de las condiciones económicas y nacionalismo excesivo, son tan solo algunas de las **consecuencias que verán próximamente los mexicanos a raíz del nuevo gobierno de Donald Trump en Estados Unidos.**

Así lo aseguró Francisco Cerezo, líder del Comité Cerezo, quien indicó que lo que se va a hacer es completar el muro que ha sido construido a lo largo de las últimas presidencias norteamericanas, pero eso no resta “el **alto riesgo que correrán todos los inmigrantes al intentar atravesar el desierto u otras zonas.** La violación a derechos humanos y el sufrimiento de todos los migrantes va a ser más grande”. Le puede interesar: ["El muro no va a ser en la frontera sino en México entero"](https://archivo.contagioradio.com/muro-no-va-a-ser-en-la-frontera-34924/)

Pese a que con la construcción del muro se pretende evitar que las personas entren a Estados Unidos, Francisco Cerezo dice que no podrán hacer lo mismo con las mercancías que también circulan por allí, incluida la droga que “aunque sea ilegal, va a tener un despunte porque necesitan una ganancia para paliar una crisis”.

Para el defensor de derechos humanos, lo que va a suceder es que la droga va a funcionar como el alcohol en la época del prohibicionismo en Estados Unidos, fortaleciendo las mafias de donde obtendrán riqueza “y esa riqueza es de la explotación de mucha gente” y agregó “(…) **si Estados Unidos cierra su frontera a la droga, tendría una revolución de enfermos de adicción en ese país”.**

Ante la posibilidad de redefinir una política exterior de México hacia América Latina, como lo planteó el presidente Evo Morales a Enrique Peña Nieto, el Comité Cerezo señaló que “como sobrevivencia económica puede ser posible, pero eso no significa un viraje hacia la izquierda. (…) **En México se está pensando un proteccionismo nacional como lo está haciendo Estados Unidos, que es primero nosotros a costa de todos los demás”.**

### **¿Cómo seguirán la sociedad y las organizaciones en México?** 

Según Francisco Cerezo la posición de las organizaciones y del pueblo mexicano es presionar a su Estado para que cumpla su obligación y añade **“evidentemente por la política neoliberal que tenemos pues no creemos que Peña Nieto vaya a hacer mucho"**. Le puede interesar: [Gasolinazo una medida regresiva para México](https://archivo.contagioradio.com/gasolinazo-una-medida-regresiva-para-mexico/)

De darse alguna decisión por parte del Estado, Francisco advierte que serían determinaciones casi mágicas, “pero no tenemos otra opción. Seguiremos organizándonos hasta que el Estado cumpla con su obligación de garantizar los derechos humanos”.

Agrega que desde **el movimiento social y las organizaciones de derechos humanos** tendrán más trabajo por lo que **“van a presionar al Gobierno para que proteja a todos los migrantes** y mejore las condiciones económicas en México, de modo que se evite toda esa inmigración”

Y concluye **“la esperanza está, vamos a hacer todo lo posible por lograr esa unidad y sobrevivir** a una crisis económica y política y global, pero nada está escrito”. Le puede interesar: [Amnístia Internacional invita a ser "Vigilantes de Trump"](https://archivo.contagioradio.com/amnistia-internacional-trump/)

<iframe id="audio_16690983" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16690983_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
