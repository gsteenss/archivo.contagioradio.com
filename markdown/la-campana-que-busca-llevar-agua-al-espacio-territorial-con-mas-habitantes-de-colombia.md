Title: La campaña que busca llevar agua al Espacio Territorial con más habitantes de Colombia
Date: 2019-02-27 15:10
Category: Comunidad, Mar Candela
Tags: cesar, etcr, Iniciativa Productivas
Slug: la-campana-que-busca-llevar-agua-al-espacio-territorial-con-mas-habitantes-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/WhatsApp-Image-2019-02-27-at-2.43.19-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: comunicaciones ETCR tierra grata] 

###### [27 Feb 2019]

El Espacio Territorial de Capacitación y Reincorporación (ETCR) ubicado en Tierra Grata, Cesar, se ha convertido en uno de los pocos asentamientos de este tipo donde su población ha ido en aumento desde la firma del Acuerdo de Paz,  a raíz de este incremento la campaña **'Un Metro de Manguera por el agua'** solucionaría la dificultad que existe para abastecer a sus habitantes del preciado líquido y potenciar sus iniciativas agropecuarias.

**Francisco Ortega, excombatiente de las Farc** y actual habitante del espacio territorial, explica que para finales de 2016 durante la etapa de dejación de armas la población alcanzaba los 160 excombatientes, cifra que que en la actualidad se ha elevado a un total de 293 habitantes, además expresa que en el momento solamente reciben dos carro tanques diarios que únicamente sumann 24.000 litros de agua, hecho que sumado a las altas temperaturas de la región ha incrementado la demanda de agua en el espacio.

Por tal motivo  y con la firme intención de asentarse en el lugar, los habitantes han decidido lanzar esta campaña, con la cual se instalaría una manguera de 10 kilómetros que llegaría hasta la bocatoma del **Río Chira.** Aunque el  agua no sería potable esta tendría que ser procesada por una planta de tratamiento que, no solo beneficiaría a los habitantes de La Paz sino también a la comunidad aledaña de **El Mirador**, ubicada a tan solo dos kilómetros.

Frente al por qué este espacio territorial a diferencia de los otros 23 del país aumento su población, Francisco indica que en particular en el Caribe colombiano el conflicto decreció de forma relativa a comparación de otras regiones del país como en el Guaviare, el Chocó o el Cauca, razón por la que en la actualidad, **el ETCR  se ha convertido en un espacio de reencuentro al que también han llegado paulatinamente los familiares de los excombatientes.**

La comunidad espera que para abril puedan alcanzar la meta de 80 millones de pesos, precio que tendría la manguera, su transporte e instalación, suma a la que han aportado \$200.000 de su renta básica cada uno de los excombatientes.

<iframe id="audio_32899103" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32899103_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [ Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
