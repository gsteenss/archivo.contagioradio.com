Title: Las preguntas incómodas a los presidentes Santos y Obama sobre el Plan Colombia
Date: 2016-02-04 17:28
Category: DDHH, Nacional
Tags: Conversaciones de paz con las FARC, ELN, Juan Manuel Santos, Obama, plan Colombia
Slug: las-preguntas-incomodas-a-los-presidente-santos-y-obama-sobre-el-plan-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio 

###### [4 Feb 2016.]

Organizaciones sociales que han seguido de cerca el desarrollo del Plan Colombia, así como el Plan de Acción Laboral establecido tras la firma del TLC, y las persistentes denuncias por violaciones a los DDHH que vinculan a personas de las FFMM de Colombia y el dinero de la cooperación destinada por Estados Unidos a través de diversos mecanismos.

Las preguntas se realizan en medio de las críticas por la nutrida delegación colombiana que asiste a los actos de conmemoración del Plan Colombia. Una delegación en la que no se nota la diferencia entre empresarios y miembros del gobierno, lo que despierta fuertes suspicacias entre los testigos y las organizaciones de DDHH que exigen una evaluación seria de las posibilidades de un Plan Colombia II.

### **Sobre los “logros del Plan Colombia” y posible “Plan Colombia II”** 

Al presidente Santos: Los años del Plan Colombia fueron testigos de muchos excesos cometidos por las fuerzas de seguridad: las ejecuciones extrajudiciales, la colaboración con los grupos paramilitares, y el espionaje ilegal. ¿Cómo va a garantizar la no repetición de este comportamiento en el período posterior al conflicto?

Al presidente Obama: Ahora que el conflicto armado en Colombia parece estar terminando, ¿cómo va a ayudar al cambio en ese país para hacer frente a sus necesidades después de un conflicto? ¿Es probable que un aumento sobre los niveles actuales?

### **Sobre las llamadas Ejecuciones extrajudiciales** 

Se documentaron más de 4.300 ejecuciones extrajudiciales presuntamente cometidas por miembros de las fuerzas armadas durante la década de los 2000 –

Presidente Santos: Con el acuerdo de la justicia de transicional ¿qué va a pasar con las miles de investigaciones en curso por las ejecuciones extrajudiciales y otras violaciones de derechos humanos flagrantes cometidas por miembros de las fuerzas armadas? ¿soldados y ex guerrilleros que confiesen crímenes de guerra serán condenados a la "restricción de la libertad" con penas alternativas en lugar de la cárcel? ¿Qué tan austeras se espera que sean esas condiciones de reclusión?

### **Sobre las conversaciones de paz con el ELN** 

Si no se alcanza un acuerdo conjunto, las organizaciones que formulan las preguntas plantean un escenario difícil para la aplicación del acuerdo con las FARC en zonas donde opera el ELN, también existe la posibilidad de que integrantes de las FARC se sumen a esa guerrilla y que defensores de DDHH y campesinos que han respaldado o se ven afectados por las conversaciones con el ELN sean estigmatizados o asesinados.

Presidente Santos: ¿Su gobierno está haciendo todo lo posible para hacer avanzar las conversaciones con el ELN? Lo que queda por ser acordado antes de las conversaciones formales pueden completarse? ¿Cómo puede el gobierno EE.UU. ser útil en este proceso?

Presidente Obama: ¿Hay un papel para los Estados Unidos para alentar o facilitar las conversaciones con el ELN?

### **Sobre los grupos sucesores de los paramilitares** 

Un acuerdo de paz con las FARC y el ELN por desgracia no traerá la consumación de la violencia en Colombia; grupos sucesores de los paramilitares y grupos de narcotraficantes siguen presentes y activos en el país. Hay informes de actividad extensa de grupos sucesores de los paramilitares en regiones como el Bajo Atrato, Mapiripán y el Bajo Cauca. Estos grupos son los principales responsables de amenazar y atacar a los defensores de derechos humanos y líderes de la comunidad. También existe la preocupación de que tales grupos son propensos a atacar los desmovilizados de las FARC.

Presidente Santos: ¿Qué medidas está tomando su administración para luchar contra estos grupos, para investigar y procesar a los paramilitares y los agentes estatales que podrían estarlos apoyando?

### **Sobre los defensores de DDHH y las víctimas** 

Muchas de las víctimas que participaron en las delegaciones a las conversaciones de paz recibieron amenazas de muerte a su regreso a Colombia. Las amenazas y los ataques continúan también contra los activistas de derechos humanos, sindicalistas y líderes de la sociedad civil, incluidos los reclamantes de tierras, afrocolombianos y líderes comunitarios indígenas. La organización Frontline registraron 54 defensores asesinados en 2015. La Unidad de Protección Nacional ha estado invadida de escándalos de corrupción y críticas por insuficiencia e ineficiencia, y por falta de apoyo en la investigación y el enjuiciamiento de los responsables de las amenazas y ataques.

Presidente Santos: ¿Cómo planea Colombia para prevenir y proteger contra los ataques contra defensores de derechos humanos, y cómo esas medidas se adoptaron y ampliaron después de la firma del acuerdo?

### **Sobre las comunidades afrocolombianas e indígenas** 

Las comunidades afro-colombianas e indígenas sufren de manera desproporcionada de los abusos, la violencia y el desplazamiento. Bajo  leyes de propiedad colectiva de la tierra y otros derechos especiales, y las zonas que habitan probablemente serán la sede de grandes poblaciones de guerrilleros desmovilizados. Sin embargo, las autoridades territoriales y grupos de la sociedad civil organizada no han sido invitados a la mesa de negociaciones para discutir cómo se aplicarán los acuerdos en sus territorios.

Presidente Santos: ¿Cuál es su administración haciendo para asegurar que las preocupaciones particulares de las comunidades afrocolombianas e indígenas se abordarán en el acuerdo de paz y su aplicación?

### **Sobre derechos laborales** 

La defensa del Congreso EE.UU. a favor de los derechos laborales anteriores a la aprobación de los EE.UU.-Colombia FTA en octubre de 2011 dio lugar a importantes avances en los derechos laborales en Colombia. Estos incluyen una reducción de los asesinatos de sindicalistas, la creación de un Ministerio de Trabajo, aumento del número de inspectores de trabajo y más atención a la protección de los sindicalistas. Mientras que el plan de acción de Trabajo de Estados Unidos-Colombia aún no se ha aplicado plenamente - por ejemplo, la subcontratación que impide la organización del trabajo todavía existe y la impunidad en los asesinatos de sindicalistas sigue siendo muy alta - que sirve como un importante marco para abordar estas cuestiones y se destacan los sectores que requiere mayor atención.

Presidente Obama: ¿Qué medidas se están tomando para garantizar que el Plan de Acción Laboral EE.UU.-Colombia siga avanzando y sea incorporado en los esfuerzos posteriores a los conflictos?

Se espera que luego de la reunión de los presidentes se puedan tener respuestas concretas a esta preguntas que fueron elaboradas y entregadas con anterioridad a la cita.
