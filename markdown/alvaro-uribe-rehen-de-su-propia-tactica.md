Title: Álvaro Uribe rehén de su propia táctica
Date: 2016-03-17 15:23
Category: Camilo, Opinion
Tags: Alvaro Uribe, Santiago uribe, uribismo
Slug: alvaro-uribe-rehen-de-su-propia-tactica
Status: published

#### **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [@CamilodCasas](https://twitter.com/CamilodCasas)** 

###### Mar 17 de 2016 

Pasa por una mala hora, pero aún puede seguir pataleando. El ex presidente, hoy senador, Álvaro Uribe, no está derrotado.

Uribe no se rinde, pero por primera vez en su reinado de casi una década su fórmula de distracción y de encubrimiento apelando al espejismo de las "masas",  le está saliendo mal. El Uribe se ve aullando con sus escoltas y el séquito del Centro Democrático.

Su perorata contra el proceso de conversaciones entre el gobierno de Santos y las FARC EP, y el *Castro Chavismo*, no le ha servido como comodín para que las "masas" salgan a rugir por una pobre víctima, como se quiere hacer ver,  a su hermano Santiago Uribe.

Sus acusaciones contra los defensores de derechos humanos Javier Giraldo, Daniel Prado y la Comisión de Justicia y Paz, por pago de testigos y montajes, e incluso responsabilizando al primero de ser partícipe de un asesinato, no han logrado tapar la operación criminal por la que es sindicado su hermano, calificado en los medios empresariales como un reconocido ganadero. Las descalificaciones de los testigos, a uno de ellos por problemas psiquiátricos, a otro por falsedad,  no le han válido.

La decisión de la Fiscalía de detener a Santiago Uribe basado en una serie de por lo menos siete pruebas e inicialmente tres testigos, por ser parte activa y liderar el grupo paramilitar los "12 Apóstoles" y ordenar asesinatos, entre ellos del conductor de Yarumal de apellido Barrientos, dentro de una lógica de exterminio a los civiles concebidos como guerrilleros o apoyos de las guerrillas, así como, a los "desechables", no ha sido controvertida con pruebas, solo con áulicos en el noticiero del  canal RCN.

Ni Diego Murillo ni Salvatore Mancuso se han retractado de lo dicho en cárceles de los Estados Unidos sobre Santiago Uribe, sobre la familia Uribe y sus andanzas desde la década de los ochenta, pues del negocio del polvillo blanco que se inhala  saben mucho, estos afirman, y lo saben las agencias de ese país en un secreto a voces.

Tampoco el famoso Tuso Sierra, que bien conoció a Santiago Uribe y Mario Uribe, cuando este oficiaba como especie de contador de la Oficina de Envigado, y quien hoy  vive libre en los Estados Unidos, se ha echado atrás de lo dicho ante las autoridades de ese país, y  de lo que supo que ocurría en la fachada de la Finca La Carolina, entre 1990 y 1995, centro de entrenamiento paramilitar y muchas cosas más.

Aquí todos juegan con el derecho y la política. Uribe, apelando a la Comisión Interamericana de Derechos Humanos, de la cual cuestionó en sus dos gobiernos, mostrando ante ella, que la fundada acusación es persecución política. Pero qué no decir de  Santos, pidiendo la intervención de la Procuraduría General de la Nación y una Veeduría Internacional. Ambas expresiones de la más alta hipocresía, cuando ellos han usado el derecho y las instituciones de justicia y sus facultades para hacer y deshacer, incluso contra derecho. ¡Para eso es el poder!, dirán algunos.

Para ambos no hay un mínimo de asumir la verdad de cara a las víctimas, pues todos resultan en el fondo siendo responsables de la criminalidad de Estado. La trascendente decisión sobre la vinculación de Santiago Uribe a un proceso penal, luego de veinte años de impunidad, debió seguramente contar con el beneplácito de los Estados Unidos. Ellos deben estar cansados de la retahíla uribesca contra el proceso de La Habana, que afecta su intervención en Colombia. Su privación de la libertad es un mensaje para el uribismo rancio y puro, o asumen o asumen el proceso con la guerrilla de las FARC EP, o se atienen a las consecuencias. Ese es el mensaje político de Santos. En esa mamadez están los gringos después de haber hablado una y otra vez en Colombia con el senador Uribe y de que sus bocanadas de obsesiva estupidez la haya dicho en los Estados Unido con el respaldo de unos pocos senadores republicanos.

Es hipócrita decir que la apelación de la inmunidad con la que arrecia el Uribismo es solo de ellos; es la misma de la que han gozado todos los expresidentes y sus familias López, Turbay, Betancour, Gaviria, Samper, Pastrana, Uribe y Santos.

El espaldarazo del Procurador General de la Nación que aboga por el hermano de Álvaro Uribe pidiendo la libertad a través de una detención domiciliaria, es una expresión, más que legal, de presión al decaído Fiscal y al propio presidente Santos, que se suma a las peroratas de Uribe y el partido Centro Democrático.

Ese es el derecho como instrumento útil de los poderosos, de los intocables para presionar y doblegar a su voluntad política; lo que es obstáculo. Estoy casi seguro, de que si Uribe se doblega con el realismo que expresa, de que esas "masas" ya están dejando de ser borregos lógicos y no lo alaban, y se acerca más a las tesis del Santismo de la Pax Neoliberal, que siempre han compartido;  es decir, si se baja del militarismo sacro santo para combinarlo con otros métodos, entonces el proceso penal contra el uribismo tendrá otro curso, y no habrá condena, y si hubiera condena, saldrá libre en una apelación. Tal cuál como sucedió con Plazas Vega, el Teniente Coronel que defendió la democracia desapareciendo 11 personas en noviembre de 1985.

La justicia es un ejercicio político, pocas veces un ejercicio en derecho sobre todo cuando de las élites se trata. Las reacciones descocadas de Uribe con su partido de bolsillo son comodín retórico de presión hacia la inmunidad, pero para asegurarla, debe pactar,  ese es el juego político sobre la mesa.

La urgencia de meterle "masas" a la acusación contra su hermano es una reacción preventiva frente a las acusaciones que pesan sobre el mismo expresidente, sus hijos, su excandidato Óscar Iván Zuluaga, por sus actuaciones ilegales con su ex hacker Andrés Sepúlveda. Esto parte no solo de una empresa criminal, sino de una cultura criminal que lleva más de 25 años desde la época de Pablo Escobar.

Las operaciones criminales no empezaron en La Carolina ni terminaron allí. Han pasado por la gobernación de Antioquia a finales de los 90, con  su guardia pretoriana policial, uno de cuyos bastiones pasa inmune ante el mundo y otro que ya declaró en los Estados Unidos. O por el DAS, de un buen muchacho costeño, ya condenado, siendo presidente, y una buena muchacha como María del Pilar Hurtado, o Andrés Felipe Arias, Luis Carlos Restrepo y Luis Alfonso Hoyos, y su ex secretario privado y su exministro de Salud, Diego Palacios.

Esta vez, inicialmente, la táctica le ha fallado a Uribe. Para asegurarse debe salir un pañuelo blanco para pactar la impunidad con el Santismo, y ahí si juntos enfrentar la Jurisdicción Especial de Paz y el Sistema Integral de Verdad, de Justicia, de Reparación Integral y de Garantías de No Repetición, o tomar la decisión de seguir siendo el "señor de las sombras".

[Pues la otra realidad, cierta y de apuño, es que solo a las víctimas de crímenes de Estado les queda por este tiempo, los restos de la justicia ordinaria, lo que logren retomar de la ley 975, la JEP o la ruptura en la memoria y la verdad, a lo que temen victimarios, planificadores y beneficiarios. El derecho es político. El derecho es expresión también de relaciones de poder.]{lang="ES-TRAD"}
