Title: Lider de San Vicente del Caguán denunció amenazas y exigió la protección que nunca llegó
Date: 2018-12-20 16:18
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: asesinato de líderes sociales, San Vicente del Caguán
Slug: lider-de-san-vicente-del-caguan-denuncio-amenazas-y-exigio-la-proteccion-que-nunca-llego
Status: published

###### [Foto: @vanetra/ @javieroliver\_ct] 

###### 20 Dic 2018 

En zona rural del municipio de San Vicente del Caguán fueron encontrados sin vida, Viviana Muñoz Marín, psicóloga de la Agencia Colombiana para la Reincorporación y Normalización (ARN) en el Caquetá y el líder cívico, José Ignacio Gómez, un hecho que pone en evidencia el incremento de violencia en la región y la inseguridad que sienten sus habitantes.

Según Herson Lugo, defensor de derechos humanos y presidente de la **Corporación Caguán Vive**, Viviana Muñoz e Ignacio Gómez fueron  interceptados el lunes en horas de la tarde mientras regresaban de la zona rural del municipio y fueron encontrados sin vida el miércoles hacia la orilla de una carretera con impactos de armas de fuego.

**'Nacho' ya había recibido amenazas**

Desde el 2017 Ignacio Gómez, o ‘Nacho’ como era conocido en la región **había recibido amenazas contra su vida**, hecho que se dio a conocer a la **Unidad Nacional de Protección** la cual tras realizar un estudio, no consideró que existiera un potencial riesgo para su vida, esto a pesar de las advertencias que habían realizado organizaciones y defensores de derechos de la zona.

[Ignacio Gómez, además de ser comerciante era un líder activo del sector lácteo, acompañaba las movilizaciones y era vocero de la Mesa Departamental por la defensa de la Vida Láctea. Por su parte, el presidente de la Corporación Caguán Vive señaló que hasta donde se tiene información la psicóloga Viviana Muñoz quien trabajaba junto a la ARN como facilitadora en procesos de reincorporación con ex combatientes de las Farc en los municipios de Puerto Rico y San Vicente del Caguán no había recibido amenazas.  
]

### **Incrementa la inseguridad en San Vicente del Caguán  
**

Aunque hasta el momento se desconocen los autores de los asesinatos o de las amenazas previas contra ‘Nacho’, Lugo asegura que existe una fuerte presión sobre el municipio, “en la región se viene incrementando la tasa de homicidios día tras día, por semana hay una o dos personas asesinadas".

El abogado además expresó que San Vicente del Caguán es un punto estratégico del país  y que **la población en general no siente confianza ni seguridad hacia la fuerza pública** debido a los recientes hechos de violencia en el municipio.

Las autoridades han ofrecido una recompensa de 20 millones de pesos a quien pueda otorgar información que contribuya o permita esclarecer la investigación mientras un grupo especial de la Policía Judicial y la Fiscalía fue designado para avanzar con el esclarecimiento del doble homicidio.

<iframe id="audio_30935899" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30935899_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [[su correo]
