Title: “Un Asunto de tierras” documental colombiano se presenta en Francia
Date: 2015-03-24 20:54
Author: CtgAdm
Category: 24 Cuadros, eventos
Tags: Alter Cine de Chile, Cinéma du Reel, Ley de Victimas y Restitución de Tierras, Pathos Audiovisual de Colombia, Patricia Ayala Ruíz
Slug: un-asunto-de-tierras-documental-colombiano-se-presenta-en-francia
Status: published

*“Si tener la tierra es tenerlo todo, entonces perder la tierra es perderlo todo” Patricia Ayala Ruíz, directora de la cinta.*

La segunda producción de Patricia Ayala Ruíz se estrena durante el “Cinéma du Reel”, Festival de cine documental más importante de Francia y uno de los más importantes del mundo que se realiza del 19 al 29 de marzo.

La coproducción entre Pathos Audiovisual de Colombia y Alter Cine de Chile, centra su atención en el primer año de la llamada Ley de Victimas y Restitución de Tierras en Colombia a partir de una comunidad que aplica al proceso.

Luego del éxito alcanzado con “Don Ca”, primer trabajo dentro del género, la directora colombiana llamó la atención del comité de programación del Festival, en donde tendrá tres presentaciones inlcuyendo una en el Centro Pompidou de Paris, participando en la competencia oficial de la sección “Primeras películas” como única producción latinoamericana presente.

<iframe src="https://player.vimeo.com/video/50559891" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

 “Un asunto de tierras”, ganadora del Fondo para el Desarrollo Cinematográfico 2011 a producción de largometraje documental, y del Premio Ibermedia 2012 coproducción de largometraje, hace parte además de la Selección oficial Festival Internacional de Cine de de Barranquilla (FICBAQ 2015) que se realizará del 20 al 28 de marzo, participando dentro de la categoría medio ambiente y pueblos en lucha.
