Title: 10 poemas de Martí convertidos en canción
Date: 2019-01-28 10:20
Author: AdminContagio
Category: Cultura, Viaje Literario
Tags: 163 años Marti, José Marti, Poemas de Matí
Slug: 10-poemas-de-marti-convertidos-en-cancion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/jose_marti_by_gerardogomez-d37x08n-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [28 Ene 2016]

El 28 de enero de 1853 nació en la Habana José Martí; hoy 163 años después tenemos la fortuna de recordar parte de su trabajo literario, gracias a las versiones que de sus poemas han convertido en canción algunos de los más importantes compositores e intérpretes cubanos.

En Contagio Radio, rendimos homenaje a su obra con una selección de 10 temas musicales inspirados en 1o escritos de José Martí, a ritmo de son, trova y nueva canción latinoamericana.

<iframe src="https://www.youtube.com/embed/ebsiXlOCTxI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Mi verso es como un puñal- Pablo Milanés

<iframe src="https://www.youtube.com/embed/tAIJWxI16gQ" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Fragmento del 27 de noviembre- Sara González

<iframe src="https://www.youtube.com/embed/1onNDkz7oa8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Dolora Griega - Amaury Pérez

<iframe src="https://www.youtube.com/embed/craeb9A7MQ8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Guantanamera - Joseíto Fernández

<iframe src="https://www.youtube.com/embed/M4e12oh0Wx0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Abril - Miguel Porcel

<iframe src="https://www.youtube.com/embed/HC7VpQRkeI8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Ya me voy pa la guerrilla - Los Olimareños<iframe src="https://www.youtube.com/embed/hDotrwiItZs" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La vi ayer, la vi hoy - Amaury Perez

<iframe src="https://www.youtube.com/embed/G4QUFSIaVhk" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Banquete de tiranos - Pablo Milánes

<iframe src="https://www.youtube.com/embed/XAAP6bfNGK4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La niña de Guatemala - Oscar Chávez

<iframe src="https://www.youtube.com/embed/FN0MsseH590" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Mis versos - Sara González

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ] 
