Title: Tribunal de Boyacá ordena suspender explotación minera en Tasco
Date: 2016-08-10 15:08
Category: Ambiente, Nacional
Tags: explotación de hierro en tasco, Minas Paz del Río, minería en Tasco, tribunal de boyacá
Slug: tribunal-de-boyaca-ordena-suspender-explotacion-minera-en-tasco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Pisba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Excelsio] 

###### [10 Ago 2016 ] 

Tras una acción popular interpuesta en octubre de 2015, el Tribunal Administrativo de Boyacá ordena la **suspensión inmediata de las actividades que viene desarrollando la empresa Minas Paz del Rio S.A**, con el aval de la Agencia Nacional Minera y la Corporación Autónoma Regional de Boyacá, en el municipio de Tasco, por los graves daños que la explotación de hierro ha generado en las fuentes hídricas y en la zona de amortiguamiento del páramo de Pisba.

La acción fue presentada por las comunidades, la Personería Municipal y la 'Corporación Podion', quienes argumentaron que desde los años 90, la explotación de hierro en la mina El Banco ha provocado la **pérdida de por lo menos 20 fuentes hídricas, agrietamientos en los terrenos y destrucción de la fauna y flora**. Todo ello en el marco del título minero prorrogado en 2012 por la Agencia Nacional Minera y el Plan de Manejo Ambiental aprobado por Corpoboyacá en 2006.

Frente a la intención que Minas Paz del Río ha manifestado para reiniciar sus actividades de explotación y sacar 8 mil toneladas de hierro al mes, las **500 familias afectadas** lideran un [[plantón pacífico en la mina](https://archivo.contagioradio.com/continua-la-explotacion-minera-en-el-paramos-de-pisba-en-tasco-boyaca/)] para impedir su reactivación

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
