Title: Cerca de 20 millones de personas no tienen vivienda en Colombia
Date: 2015-09-17 13:45
Category: DDHH, Entrevistas, Política
Tags: Alberto Castilla, Congreso de la República, Desalojos, Necesidades básicas insatisfechas, Personas desplazadas, Polo Democrático Alternativo, Servicios públicos domiciliarios, Vivienda digna en Colombia, Vivienda propia
Slug: hogar-digno-hogar-audiencia-publica-para-exigir-vivienda-digna-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Vivienda-digna2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: bogotalab.com] 

<iframe src="http://www.ivoox.com/player_ek_8454451_2_1.html?data=mZmilpmZdY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRh8bmxMaYxsqPdpGfzs7ZztTSqdSfxcqY0srWt9DiwtiY0NSPuMrZz8rbjdvNusrZz8nOjcrSb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alberto Castilla, Polo Democrático] 

###### [17 Sept 2015] 

En Colombia existen **15 millones de colombianos sin vivienda propia, 5 millones que necesitan mejoras en infraestructuras y hay un promedio nacional de necesidades básicas insatisfechas del 27%, 70% en el sector rural**. Todo ello, según el senador Alberto Castilla, como producto de políticas que han privilegiado intereses económicos privados.

En el marco de esa crisis y para llamar la atención sobre esa situación, mañana desde las 8 am, se llevará a cabo **la Audiencia Pública Nacional Senatorial “Hábitat, vivienda digna y servicios públicos domiciliarios**”, en el auditorio Luis Guillermo Vélez del Congreso de la República, convocada por el congresista del Polo Democrático Alternativo, Alberto Castilla.

De acuerdo al Senador, esta crisis de vivienda y servicios públicos domiciliarios, **es agudizada por los continuos desalojos por parte de la fuerza pública** en distintas ciudades del país, relacionados  estrechamente con proyectos de inversión que buscan abrir camino al desarrollo económico.

Contra lo demandado por la ley, el Estado no brinda garantías para las familias que son desalojadas, quienes ante la falta de propuestas de reubicación construyen asentamientos en condiciones precarias.

Por otra parte, agrega Castilla, los programas de vivienda para personas desplazadas no garantizan condiciones de vida digna, pues **desconocen sus características sociológicas y les obligan a vivir en casas de 40 m2,** que impiden su efectivo desarrollo.

Dado este déficit nacional en el acceso a servicios públicos domiciliarios y las condiciones precarias de cerca de 3 millones de viviendas, **el Senador adelanta una iniciativa legislativa para establecer el acceso a hábitat, vivienda digna y servicios públicos domiciliarios**, como derechos fundamentales para la población colombiana.
