Title: Deudas con Banco Agrario tendrían al borde del suicidio a indígenas Kankuamos
Date: 2018-02-26 13:12
Category: DDHH, Nacional
Tags: Banco Agrario, cáncer, Indígenas Kankuamos, Sierra Nevada de Santa Marta
Slug: deudas-con-banco-agrario-tendrian-al-borde-del-suicidio-a-indigenas-kankuamos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/pinteres-e1520438971238.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pinterest] 

###### [26 Feb 2018] 

Indígenas Kankuamos de la Sierra Nevada de Santa Marta, en César denunciaron que **130 integrantes de esta comunidad podrían suicidarse**, debido a las altas deudas que tienen con el Banco Agrario y la Federación Nacional de Cafeteros, adquiridas a través de un préstamo con la finalidad de renovar la siembra de café, que se hecho a perdidas por el verano de 18 meses que tuvo que aguantar esa población.

De acuerdo con Lot Villamizar, integrante de esta comunidad, los indígenas **no tienen cómo responder económicamente a la deuda y al hablar con la entidad bancaria**, les han respondido que les embargarían los predios donde viven, sin embargo, estos lugares pertenecen al territorio del resguardo indígena que por ley no puede ser expropiado por entidades.

“El Banco Agrario ahora está haciendo un acto de sometimiento a nuestra comunidad, llaman en la mañana, en la tarde para cobrarles esta deuda y mucha gente no haya que hacer, llevándolos al punto del suicidio y presionándolos aun sabiendo el banco que no puede expropiarlos” afirmó Villamizar.

De igual forma, los indígenas denuncian que la Federación Nacional de Cafeteros tiene firmadas unas pólizas de seguro para los préstamos, sin embargo, **las empresas que deberían responder no han hecho efectivo el seguro.**

**Las mujeres y el cáncer**

Otra de las denuncias que ha hecho la comunidad **es el aumento de muerte en las mujeres a causa del cáncer de pulmón**, debido a que son ellas las encargadas de cocinar en estufas de leña, que serían las que están provocando esta enfermedad. (Le puede interesar: ["Dos integrantes de la ACIN han sido víctimas de atentados este mes"](https://archivo.contagioradio.com/2-integrantes-de-la-acin-han-sido-victimas-de-atentados-este-mes/))

Frente a esta situación, Villamizar manifestó que se hace más de 3 años han estado “implorándole” al gobierno nacional y al gobierno departamental que lleve el gas natural hasta la comunidad. Además, debido a la falta de brigadas de salud y conocimientos médicos, los indígenas **no han podido establecer cuántas mujeres actualmente padecen de esta enfermedad o han muerto por la misma**. Solo las mujeres que han logrado asistir al médico son las que logran ser identificadas con cáncer.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
