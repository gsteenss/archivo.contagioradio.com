Title: La ejemplar consulta popular de San Lorenzo
Date: 2018-11-26 18:09
Author: AdminContagio
Category: Ambiente, Nacional
Tags: consulta popular, nariño, San Lorenzo
Slug: consulta-popular-san-lorenzo
Status: published

###### [Foto: @CumbreUrbana] 

###### [26 Nov 2018] 

Con un **98% de los votos, el municipio de San Lorenzo, Nariño, dijo no a la megaminería** en su territorio. Mediante consulta popular, completamente auto financiada por la comunidad, participó del mecanismo el 53% del censo electoral, casi 800 personas más que en la primera vuelta presidencial y apenas 203 menos que en la segunda vuelta.

Según **Aura Domínguez, integrante de la registraduría popular que realizó la consulta**, el proceso inició en 2008, luego de que se realizarán concesiones en el Municipio tituladas para la Anglo Gold Ashanti y otras 4 empresas, que pretendían extraer oro, plata, coltán, esmeraldas y petróleo en algunas zonas. (Le puede interesar: ["Diciembre 7: Gran movilización nacional en favor de las consultas populares"](https://archivo.contagioradio.com/diciembre-movilizacion-consultas/))

Los habitantes de esta zona biodiversa, en su mayoría agricultores, decidieron iniciar un proceso para evitar que su tierra y su agua se vea afectada de alguna forma por la gran minería. Aunque la administración municipal y la Registraduría Nacional del Estado Civil se comprometieron con la Consulta, en 2017 el Ministerio de Hacienda afirmó que no había recursos para su realización.

A este obstáculo se sumó **la decisión de la Corte Constitucional que dejó sin piso jurídico a las consultas populares,** puesto que como lo señaló la habitante de San Lorenzo, “nosotros como comunidad somos parte del Estado, y el Estado es el que toma las decisiones sobre lo que ocurre en el suelo nacional”.(Le puede interesar[: "¿Qué pasará con las consultas populares?"](https://archivo.contagioradio.com/que-pasara-con-las-consultas-populares-en-colombia/))

### **Consulta de San Lorenzo fue una fiesta por la vida** 

Domínguez afirmó que en la Consulta participaron más personas que en la elección presidencial, y hubo una articulación importante de diferentes instituciones y movimientos, como la policía, profesores y habitantes en general del municipio; hecho que resultó en que la de San Lorenzo sea **la primera consulta autofinanciada del país**.

La activista explicó que los habitantes del Municipio hicieron todo lo necesario para la realización de la Consulta, “**fuimos Registraduría, imprimimos los tarjetones, las actas de escrutinio, los certificados electorales**, todo lo hicimos tomando por ejemplo lo que hace esta entidad”; también los profesores donaron su tiempo para ser jurados de votación. (Le puede interesar: ["Gobierno quiere hacerle mico a consultas populares con el presupuesto general de la nación"](https://archivo.contagioradio.com/gobierno-quiere-hacerle-mico-a-las-consultas-populares-con-el-presupuesto-general-de-la-nacion/))

La celebre frase de “la fiesta de la democracia” se vivió en San Lorenzo, pero como lo anunció Domínguez, la fiesta continuará porque **“queremos generar normatividad municipal, trabajaremos por que los niños tengan derecho a un ambiente sano, seguiremos cuidando nuestras aguas”**. (Le puede interesar: ["¿Por qué las consultas populares están en riesgo?"](https://archivo.contagioradio.com/consultas-populares-riesgo/))

<iframe id="audio_30336897" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30336897_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
