Title: Arranca Paro Cívico en San Martín Cesar
Date: 2016-10-25 14:51
Category: Ambiente, Movilización
Tags: Gran Minería en Colombia, No más Fracking, san martin
Slug: paro-civico-en-san-martin-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/723be196-5fe7-4d37-8471-da60f580d6ca-e1477419615574.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CORDATEC] 

###### [25 Oct de 2016] 

Desde las 9:oopm del 25 de Octubre y hasta el próximo 27 de octubre a las 12:00am, las comunidades que se oponen al fracking en San Martín Cesar, darán inicio al Paro Cívico municipal, que busca exigir al Gobierno Nacional respuestas frente a la **licencia ambiental otorgada a Conoco Phillips y el abuso de fuerza del ESMAD para facilitar la entrada de maquinaria** a la vereda Cuatro Bocas.

Además, el mismo día en la capital, habrá una jornada de plantón frente a las instalaciones del ministerio de Medio Ambiente a partir del medio día. Le puede interesar: [En medio de gases y lágrimas Conoco Phillips entra maquinaria a San Martín.](https://archivo.contagioradio.com/en-medio-de-gases-y-lagrimas-conocophillips-logra-entrar-maquinaria-a-san-martin/)

Carlos Andrés Santiago integrante de la Corporación Defensora del Agua, el Territorio y los Ecosistemas CORDATEC, señaló que dicha jornada tiene como objetivo reclamar al Ministro, “se tomen las medidas necesarias para evitar que los recursos y la naturaleza de nuestro municipio sea destruida, **que defiendan los recursos naturales, no las multinacionales”.**

**Paro Cívico, tras 7 meses de Resistencia**

Este defensor del territorio, manifestó que el hecho de que haya entrado la maquinaria “no es una derrota, nos alienta y seguimos organizándonos (…) **hemos conversado con varios sectores económicos del municipio que se unen al paro**, comerciantes, transportadores, campesinos y sindicalistas, para hacerle un llamado de atención al Gobierno”.

Por otra parte, Santiago resalta la importancia de los avances en los diálogos con las guerrillas, sin embargo, señala que las comunidades sienten desazón “al ver cómo se avanza en diálogos, muy necesarios con actores armados, pero qué pasa con **las comunidades que nos manifestamos pacíficamente en contra de la locomotora minero energética, nos responden con golpes e intimidaciones”**.

En días anteriores, cuatro personas de civil se hicieron pasar por integrantes de la comunidad en un plantón y “días después nos dimos cuenta que se trataba de **policías de inteligencia, armados, con cámaras fotográficas profesionales y que se transportaban en motos sin placa”**.

Dicho accionar, "se ha convertido en una forma efectiva para para **señalar y estigmatizar a quienes nos oponemos a los proyectos minero energéticos”** denunció el integrante de CORDATEC. Le puede interesar: [Militarizada vereda Cuatro Bocas en San Martín luego de entrada de multinacional.](https://archivo.contagioradio.com/militarizada-vereda-san-martin/)

El Paro Cívico de San Martín, es a término definido, sin embargo, Santiago resaltó que de no obtener respuesta **“nos iremos a paro cívico indefinido con opciones de movilización y de plantón sobre la ruta del sol”**, reveló que “a esta iniciativa se unirían delegaciones de otros municipios vecinos”.

“Esto no es más que la puerta de entrada para el fracking a Colombia, pero haremos uso de nuestro derecho constitucional a ejercer la protesta y a manifestarnos de forma pública y pacifica” concluye Santiago.

[**Estas son las exigencias del Paro Cívico**]

1.  El Gobierno Nacional debe declarar la prohibición del Fracking en San Martín, y en consecuencia se suspendan todas las actividades en el Pozo PicoPlata 1.
2.  Que el Gobierno de apertura a un debate democrático, basado en estudios científicos y participación ciudadana, para definir los conflictos, impactos y consecuencias que genera el Fracking en nuestro país.
3.  Los funcionarios públicos que no esten dispuestos a defender los intereses de nuestra comunidad, den u paso al costado.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
