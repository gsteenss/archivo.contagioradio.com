Title: El Festival de cine por los Derechos Humanos llega a Barranquilla
Date: 2016-09-20 13:06
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Documentales, Festival de Cine por los Derechos Humanos, Hija de la laguna
Slug: el-festival-de-cine-por-los-derechos-humanos-llega-a-barranquilla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/maxresdefault.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Hija de la Laguna 

###### 20 Sep 2016 

Como parte de su etapa de circulación, la **3ra edición del Festival Internacional de Cine por los Derechos Humanos de Bogotá**, llega a la ciudad de Barranquilla.

Desde hoy y hasta el próximo 23 de Septiembre, producciones como "**Hija de la laguna**", "**Película Verde**" y "**Detrás de la puerta**", que hicieron parte de la selección oficial del Festival, serán presentadas en espacios de la ciudad entre los que se encuentran la Universidad Libre Sedes Norte y Centro, Universidad del Norte, y la sede principal de la Alianza Francesa.[  
](https://archivo.contagioradio.com/el-festival-de-cine-por-los-derechos-humanos-llega-a-barranquilla/prgoram/)

Complementariamente a las proyecciones, el Festival realizará algunoss cine foros que contarán con la participación de **Diana Arias** y **Edwin Díaz**, directores del Festival, intercalados con conversatorios coordinados por **Sheila Burkhardt, Juan Mulford, Alvaro Serje y Jorge Mario Suárez.**

![prgoram](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/prgoram.png){.wp-image-29548 .aligncenter width="622" height="811"}
