Title: EPM y ESMAD realizan desalojo forzoso a víctimas de Hidroituango
Date: 2015-03-27 16:48
Author: CtgAdm
Category: DDHH, Nacional
Tags: Antioquia, DDHH, desalojos forzosos, Empresas Públicas de Medellín, EPM, Hidroelectrica, Hidroituango
Slug: nuevamente-epm-y-esmad-realizan-desalojo-forzoso-por-hidroituango
Status: published

##### Foto: Movimiento Ríos Vivos 

##### <iframe src="http://www.ivoox.com/player_ek_4276063_2_1.html?data=lZekmJWad46ZmKiakpWJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmdPbxtPhx5Cpl661pZDRx9jFsNDewpDOjcjTsdbiysnOxpDIqYzAwpCu1MrSqdPVjNXc1JCsrY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [**Audio en vivo de la situación**] 

<iframe src="http://www.ivoox.com/player_ek_4283587_2_1.html?data=lZellZqce46ZmKialJiJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibHBjN6Yp7ixhaWfxcrgw9HTrsLijMvc1N%2FFqMLhxtPhx5DFb8PV08re18rWs9Sf0dTfja3NqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### **[Entrevista con Isabel Zuleta y familias víctimas (4:50 p.m)]** 

La información consignada en este \#MinutoaMinuto corresponde a conversaciones vía redes sociales con las personas desalojadas, así mismo los audios y las fotografías.

**9:49 pm:** "**Se fueron de la zona**(las FFMM). Nos quedamos solos. Tememos que estén en la parte alta de la montaña y disparen y después digan que fue la guerrilla"

**9:30 pm:** Campesinos denuncian que **escucharon a los militares planificar la simulación de un ataque para disparar y culpar a la guerrilla** de posibles daños irreparables contra los civiles.

<iframe src="http://www.ivoox.com/player_ek_4276999_2_1.html?data=lZekmJ6dfY6ZmKiakpWJd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlM3Vz5DRx5DRrc3d1cbfx9iPtMLmwpDO1sbHpdOfwpDQy9vNsMbnjMnS1cbQs8vVxdTgjdXTto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Barequero denuncia plan de FFMM] 

[**8:35 pm:**] "Estamos en el puente pescadero rodeados, llegó la noche y no hay soluciones de parte de la institucionalidad"

"Todos juntos están presionando para que nos dispersemos, no nos dan transporte, nos dijeron que si no nos vamos donde ellos nos dicen no nos dan los mercados"

[**5: 10 p.m  "No tenemos para donde ir, este era el único lugar de trabajo y vivienda que teníamos" víctima de EPM**]

[**5:00 p.m Víctima de desalojo EPM "Me siento muy triste, no sé de donde voy a sacar mi sustento, no sé cómo voy a trabajar" [~~@~~**RiosVivosCol**](https://twitter.com/RiosVivosCol)

**4: 50 p.m  **~~\#~~**Urgente** "Está llegando la noche, y no sabemos qué hacer, estamos en una zona de conflicto" [~~@~~**RiosVivosCol**](https://twitter.com/RiosVivosCol){.twitter-atreply .pretty-link}

**4: 44 p.m**: ~~\#~~**Urgente** "Las mujeres insistíamos en sacar la comida que teníamos y los animales" [~~@~~**RiosVivosCol**](https://twitter.com/RiosVivosCol){.twitter-atreply .pretty-link} [~~@~~**ISAZULETA**](https://twitter.com/ISAZULETA){.twitter-atreply .pretty-link}

[**3:50 p.m**:]  81 personas son desalojadas por 200 hombres del ESMAD, caminan por el río mientras ESMAD destruye ranchos de los barequeros que viven allí

[![Hidroituango-contagio-radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/Hidroituango-contagio-radio-300x225.jpg){.aligncenter .size-medium .wp-image-6556 width="300" height="225"}](https://archivo.contagioradio.com/nuevamente-epm-y-esmad-realizan-desalojo-forzoso-por-hidroituango/hidroituango-contagio-radio/)

En la Playa La Arenera, al Norte de Antioquia, los habitantes de esa zona se despertaron rodeados de agentes del ESMAD, que buscan desalojar a los pobladores, **a quienes les dan dos horas para irse,** debido a la construcción de la hidroeléctrica Hidroituango, desarrollada por **EPM, Empresas Públicas de Medellín.**

[![tuit ontagio radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/tuit-ontagio-radio.png){.aligncenter .wp-image-6523 width="431" height="455"}](https://archivo.contagioradio.com/nuevamente-epm-y-esmad-realizan-desalojo-forzoso-por-hidroituango/tuit-ontagio-radio/)

De acuerdo a Juan Pablo Soler, integrante del Movimiento Ríos Vivos, desde el mes de noviembre EPM solicitó amparo policivo para llevar a cabo el desalojo de las familias barequeras. Cabe recordar que por este proyecto se ha desalojado aproximadamente **a 400 familias,** **hoy sólo quedan 81 personas**, entre las cuales hay adultos mayores, familias víctimas del conflicto armado, niños y niñas de todas las edades, y **entre ellos una menor discapacitada.**

“Nos hemos comunicado con Nacionales Unidas, como garantes de la situación, pero ellos nos han dicho que no fueron comunicados sobre el desalojo”, denuncia Soler, quien afirma que el Movimiento Ríos Vivos, ha exigido el cumplimiento de las directrices de  Naciones Unidas para el desarrollo de proyectos como Hidroituango, y por lo tanto **la ONU, debió haber sido notificada sobre este desalojo.**

De acuerdo a esas directrices, las comunidades desalojadas por este tipo de obras, deberán tener vivienda garantizada igual o mejor a la que tenían. Sin embargo, “**EPM ha dicho que esa directriz no aplica y la ha rechazado”,** asegura el integrante del movimiento.

La situación que más se denuncia en este momento, **es la revictimización de las personas**, quienes ya han sido víctimas del conflicto armado y ahora, lo son por la empresa y el Estado, ya que para Juan Pablo Soler, aunque el gobierno diga que no realiza desplazamientos, es claro que si lo hace, pues “hoy se ve una situación bien diferente”.

Varios movimientos sociales han realizado una misión de verificación en la región, y pudieron **constatar la caracterización de la población y las irregularidades que se han cometido en contra de los pobladores**, al no cumplir con las condiciones de las Naciones Unidas, por lo que se han cometido infracciones al DIH, al que se rige Colombia.

Finalmente, concluye Juan Pablo Soler citando al comisionado de la ONU, “ningún modelo de desarrollo justifica la violación de los derechos humanos”.

<iframe src="http://www.ivoox.com/player_ek_4274263_2_1.html?data=lZeklpead46ZmKiakp2Jd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRisLhytHWw9iPqMafzcaYo9fJssbmwpDS1dmJh5SZopbbjdjNqc%2FY0JDRx9jFsNDewsnO1ZDUs9Ofpriah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan Pablo Soler, Mov Ríos Vivos] 
