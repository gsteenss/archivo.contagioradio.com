Title: #PorComerAnimales una iniciativa que desmiente el origen del Covid-19
Date: 2020-04-16 14:20
Author: CtgAdm
Category: Actualidad, Ambiente
Slug: porcomeranimales-una-iniciativa-que-desmiente-el-origen-del-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/PorComerAnimale-scaled.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto:Archivo FFW*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego del inicio de la crisis por el Covid-19 un consenso amplio de científicos argumentaron que era de origen animal atribuido especialmente a los murciélagos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la fecha y casi tres meses después de su origen no es claro el lugar de dónde proviene, lo que se conoce es que empieza a ser una infección significativa en una ciudad de China, con la claridad de que éste no es el origen, sino el lugar de donde se manifiesta primero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo que sí es evidente para los expertos es que, al igual que otros virus que se convirtieron en epidemias, la principal razón de expansión y creación de los virus es la destrucción de los hábitats naturales de las especies así como el consumo masivo y desmedido de animales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto, la Fundación Franz Weber (FFW) presentó la iniciativa [\#PorComerAnimales](https://www.ffw.ch/es/proyectos/porcomeranimales/), un espacio que presenta investigaciones, infografías y datos, que permiten conocer el origen de las pandemias mas grandes a los largo de la historia y originadas con el consumo de animales se relaciona con un modelo alimentario global.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mateo Córdoba investigador de la FFW, señaló que de las principales lecciones que deja el coronavirus es la manera como nos estamos alimentando y como estamos concibiendo el modelo industrial de alimentos.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Las grandes industrias alimentarias no tienen ningún problema en arriesgar millones de vidas con tal de seguir sacando ganancias de la naturaleza, poniendo los virus en nuestro plato de comida"*
>
> <cite> Mateo Córdoba |Investigador de la FFW </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

En el estudio \#PorComerAnimales los expertos de la Fundación, afirman que al año **70 millones de animales mueren cada año** **producto la industria ganadera,** 270 millones de toneladas de carne provienen de [fauna silvestre](https://archivo.contagioradio.com/resolucion-aprueba-caza-de-tiburones/), y se han confirmado 9 enfermedades de origen cárnica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

" *El* *consumidor tiene la responsabilidad de entender de una vez por todas qué las últimas pandemias que hemos tienen que ver con la forma en cómo estamos consumiendo criando y comerciando animales"*, afirmó Córdoba.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó esta iniciativa tiene como objetivo prevenir las pandemias en lugar de contrarrestarlas, promoviendo el acceso a la información veraz de las consecuencias de una dieta basada solamente en proteínas, así como la promoción de consumo de vegetales y frutas locales evitando la explotación animal en granjas e industrias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esta campaña lo que nosotros estamos haciendo una investigación de 8 pandemias y epidemias que en particular han afectado al ser humano en las últimas décadas y no nos hemos dado cuenta que efectivamente el consumo de animales es una causa directa de la entrada de estos virus al cuerpo humano y a generar toda esta crisis epidemiológica económica y social que se cae bien cada pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente el investigador añadió, *"debemos entender que este respiro que hoy tanto se está compartiendo en redes tiene que ver fundamentalmente con que el ritmo de la vida humana tal como la conocemos no es compatible con la vida de la naturaleza"*, y agregó que es importante reconocer que una alternativa como la agricultura campesina es la respuesta al fin de la crisis .

<!-- /wp:paragraph -->
