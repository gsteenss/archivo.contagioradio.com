Title: Comunidades del suroeste antioqueño unidas en rechazo a exploración minera
Date: 2018-05-21 11:44
Category: Ambiente, Voces de la Tierra
Tags: Anglo Gold, Antioquia, Mineria
Slug: exploracion-minera-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/33081661_10155900278132772_7763543078492700672_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comunidades Jericó- Támesis 

###### 21 May 2018 

Este fin de semana, comunidades de Jericó, Támesis y Fredonia en el suroeste de Antioquia se reunieron para manifestar pacíficamente su rechazo a las labores de exploración que la Empresa Quebradona (Anglo Gold Ashanti) esta realizando de cara a solicitar la licencia ambiental de explotación minera, en territorios que históricamente han tenido vocación agrícola y turística.

Fernando Jaramillo, integrante de la Mesa Ambiental de Jericó, asegura que no han logrado que la empresa acepte una intermediación directa con las comunidades, mientras que los campesinos afectados manifiestan que no aceptan las concertaciones  a través del Ministerio de Minas y la Secretaria de Minas de Antioquia, por tratarse de una presencia ilegitima e inconsulta.

Frente a la acción de las entidades del Estado, Jaramillo asegura que en la Secretaria de minas, delegación para fiscalizar y otorgar títulos mineros "no hemos encontrado un interlocutor que obedezca a la voluntad de las gentes de Jericó" y con el Ministerio y la ANLA afirma que la situación es peor por el empeño del gobierno en imponer el modelo extractivista, en municipios que no tienen vocación minera con las consecuencias que esto conlleva en lo social y lo ambiental.

Las comunidades continuarán en movilización constante y estarán atentos a las acciones de la empresa para elevar las denuncias del caso. Adicionalmente seguirán desde lo jurídico reclamando la aprobación de los Acuerdos que prohíben la minería metálica promulgados por los Concejos de 12 municipios de la región, que han sido "sistemáticamente rechazados por el Tribunal Administrativo de Antioquia" ante lo cual han presentado acciones de tutela ante el Concejo de Estado que aun no han sido falladas.

De igual forma, las administraciones de Jericó y Támesis vienen avanzado en su intención de actualizar los Esquemas de Ordenamiento Territorial en los cuales se incorporaría el acuerdo autónomo o en su defecto su idea de prohibir este tipo de proyectos (Le puede interesar: H[abitantes denucian intimidaciones por parte de la fuerza pública en Jericó, Antioquia](https://archivo.contagioradio.com/habitantes-denuncian-intimidaciones-por-parte-de-fuerza-publica-en-jerico-antioquia/))

Por último, Jaramillo manifiesta su preocupación sobre el tema del agua, ya que los campesinos de Palo Cabildo pudieron comprobar que la empresa esta realizando labores de perforación en el lugar, sin contar con la concesión por parte de CorAntioquia. Además denuncian que Anglo Gold Ashanti compró agua en carrotanques de la empresa de servicios públicos de Tarso, en una decisión que aseguran tomó el gerente de la entidad, sin consultar a la junta directiva o al alcalde municipal.

<iframe id="audio_26093933" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26093933_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
