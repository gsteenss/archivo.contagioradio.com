Title: Asesinan al líder ambientalista Jaime Monge
Date: 2020-08-19 17:49
Author: AdminContagio
Category: Actualidad, Nacional
Tags: ambientalistas, Global Witness, Jaime Monge
Slug: asesinan-al-lider-ambientalista-jaime-monge
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Asesinan-al-ambientalista-Jaime-Monge.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En la noche de este martes **fue asesinado el líder ambientalista Jaime Monge de 62 años, en el corregimiento de Villacarmelo, zona rural de Cali.** (Lea también: [5 niños afro fueron masacrados en el barrio Llano Verde en Cali](https://archivo.contagioradio.com/5-ninos-afro-fueron-masacrados-en-el-barrio-llano-verde-en-cali/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Secretario de Seguridad de Cali, Carlos Rojas, el líder ambientalista fue asesinado con impactos de bala y uno de los responsables del hecho resultó herido y las «*autoridades están tratando de identificar si en alguno de los centros médicos de la ciudad se encuentra esta persona*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Jaime Monge, era reconocido por abanderar la defensa del parque los Farallones de Cali y por ser fundador de la Organización ASOCAMPESINA y de la Fundación Pachamama** dedicada a temas medioambientales.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DianaDefensora/status/1296072311301378054","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DianaDefensora/status/1296072311301378054

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DKAmbColombia/status/1296081181675585537","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DKAmbColombia/status/1296081181675585537

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En la misma zona de Villacarmelo fue asesinado por hombres armados el también líder ambientalista Jorge Enrique Oramas el pasado mes de mayo. **Oramas, al igual que Jaime Monge, se oponía a la minería en Los Farallones.** (Le puede interesar: [Freddy Angarita y Enrique Oramas, nuevos líderes que pierde Colombia por la violencia](https://archivo.contagioradio.com/freddy-angarita-y-enrique-oramas-nuevos-lideres-que-pierde-colombia-por-la-violencia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A estos hechos se suma la masacre de cinco menores en el barrio Llano Verde de la capital del Valle del Cauca, lo cual refleja la crítica situación de violencia que vive la ciudad de Cali. (Lea también: [**Policía Nacional estaría involucrada en masacre de los niños en Llano Verde, Cali**](https://archivo.contagioradio.com/policia-nacional-estaria-involucrada-en-masacre-de-los-ninos-en-llano-verde-cali/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Jaime Monge otra víctima de la violencia contra ambientalistas en Colombia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hace unos días la ONG Global Witness alertó sobre el alto riesgo que corren las personas defensoras del medio ambiente en Colombia. Según el informe «*[Defending Tomorrow](https://www.globalwitness.org/en/campaigns/environmental-activists/defending-tomorrow/)»* **publicado por esta organización, Colombia es el país con más asesinatos de ambientalistas en todo el mundo.** (Consulte los detalles del informe [aquí](https://archivo.contagioradio.com/colombia-es-el-pais-con-mas-asesinatos-de-ambientalistas-en-el-mundo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ben Leather, investigador de Global Witness, señaló que en Colombia existen diversos factores que potencian la ocurrencia de asesinatos y agresiones en contra de líderes ambientalistas. El experto aseguró que **la impunidad, la ausencia de una política estatal eficaz, los altos niveles de militarización y la falta de regulación a las empresas que explotan recursos naturales; son algunas de las causas que generan que Colombia sea el país más violento frente a estos liderazgos a nivel mundial.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Quienes asesinan a los defensores del medio ambiente saben que pueden hacerlo prácticamente sin consecuencias, esto es una especie de luz verde para quienes buscan silenciar a los activistas»**
>
> <cite>Ben Leather, investigador de Global Witness</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
