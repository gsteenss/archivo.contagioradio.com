Title: Semana decisiva para Dilma Rousseff
Date: 2016-08-02 16:41
Category: El mundo
Tags: Brasil, Dilma Rousseff, Golpe de estado Brasil, Impeachment
Slug: semana-decisiva-para-dilma-rousseff
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/dt.common.streams.StreamServer.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: La prensa HN 

##### [2 Agos 2016] 

Con el informe expuesto este martes ante la Camara Alta del Senado por parte de Antonio Anastasia, relator del [impeachment ](https://archivo.contagioradio.com/futuro-de-rousseff-se-definira-en-plenaria-del-senado/)contra **Dilma Rousseff,** se reanuda el proceso por el que la mandataria ha sido apartada de su cargo como presidenta de Brasil desde el pasado 12 de mayo.

En el texto de 441 páginas, Anastasia insta por la destitución de Rousseff asegurando que "existen plenas razones" para justificar la solicitud de continuar con el juicio político en esta fase, argumentando que los delitos fiscales que se le indilgan son "un atentado a la constitución" que deberían traerle como consecuencia legal la pérdida del mandato.

A pesar de las evidencias presentadas por la defensa en cabeza de José Eduardo Cardozo, que dejan sin piso al proceso legal, el relator argumenta que el descontrol fiscal provocado por las acciones de la presidenta "comprometen la sustentabilidad de las políticas públicas y servicios fundamentales" provocando un desequilibrio que afecta los indices de endeudamiento público, la inflación y el PIB.

Luego de la presentación del informe, los colegiados tendrán 24 horas para analizarlo y su contenido deberá ser debatido el día miércoles por los miembros de la Comisión Especial del Impeachment. El jueves 4 se someterá a votación y al día siguiente deberá hacerse pública la resolución en el plenario de la Cámara, misma que se pronunciará al respecto en sesión el día martes 9.

**Crece la movilización.**

A pesar que no ha existido un acuerdo común entre algunos frentes sociales, particularmente los trabajadores, la actual situación de desempleo, la inflación, el costo de vida y las decisiones del vicepresidente y mandatario temporal Michel Temmer, han comenzado a generar un clima de insatisfacción que puede motivar a más organizaciones a  salir a las calles por sus derechos.

Las actividades iniciarán con una vigilia interreligiosa en el centro de Rio de Janeiro, como forma de denunciar los planes del gobierno interino. El día 5 de agosto, tendrá lugar una gran caminata de lucha unitaria con partida en Copacabana, convocada por  los Frentes Brasil Popular, Pueblo Sin Miedo y de Izquierda, movilización que se realizará de forma paralela con la inauguración de los Juegos Olímpicos, mostrando al mundo el golpe de estado que se vive en el país.

Después de la semana de votación del 29 de agosto, se realizarán una serie de movilizaciones en la capital Brasilia, y campamentos en todo el país, manifestando el rechazo a los senadores como representantes ilegítimos del pueblo.

 
