Title: "Acuerdo de paz sienta las bases para realizar transformaciones profundas": Jairo Estrada
Date: 2017-11-24 18:00
Category: Entrevistas, Paz
Tags: acuerdo de paz, voces de paz
Slug: voces_depaz_acuerdo_jairo_estrada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/jairo-estrada-e1511562950155.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia UN] 

###### [24 Nov 2017] 

En medio de las situaciones difíciles y obstáculos que se están presentando en la implementación del acuerdo de paz, para Jairo Estrada, integrante de Voces de Paz, el país ha dado algunos pasos hacia la construcción de la paz. **"Estamos en un terreno fangoso, pero en medio de ese terreno en todo caso se está avanzado",** dice.

Su afirmación va dirigida a que se debe tener presente que se trata de un proceso que, de acuerdo con experiencias internacionales, puede trascender a un periodo de una década, teniendo en cuenta que existen fuerzas opositoras que tratan de impedir que se avance en la implementación.

### **La importancia del acuerdo** 

Para el integrante de Voces de Paz, el acuerdo de paz va más allá de la FARC. Se trata de un **acuerdo "para el conjunto de la sociedad, ya que tiene el propósito de sentar las bases mediante reformas elementales para avanzar en transformaciones más profundas".** De tal manera que ese documento firmado hace un año por el presidente Juan Manuel Santos y Rodrigo Londoño, tiene un potencial de transformación muy importante que debe ser empujado por el conjunto de la sociedad.

Asimismo, es un acuerdo que abre un camino al cambio cultural en materia de la contienda política, pero además, se da la posibilidad de identificar los problemas reales del país, que van más allá del conflicto armado. "Ahora se pone al desnudo que los problemas reales de esta sociedad no tenían que ver con la persistencia de la guerra, sino que son de origen sistémico y que deben ser abordados de otra manera", expresa Estrada.

### **Los retos de la movilización social** 

El desafío en este posacuerdo va también dirigido a la movilización social. Jairo Estrada, señala que ahora se debe unir esos esfuerzos y dinámicas importantes que se están gestando, ya que siguen **siendo dispersos, fragmentados y además insuficientes.**

Si bien, son numerosas las manifestaciones contra la crisis política actual, teniendo en cuenta problemáticas tales como la corrupción y el clientelismo, "es necesario avanzar en propósito de unidad para consolidar la perspectiva de una paz completa en el país. Pero hay un balance político y de poder todavía no expresado en movimiento a favor de superar la guerra y sobre todo las condiciones que históricamente han generado ese conflicto", manifiesta.

Finalmente, Estrada dice que el movimiento social no solo debe trabajar para disputar el poder en el Estado, sino también p**ara producir poder social de manera que se logren transformaciones sociales profundas. **

<iframe id="audio_22332145" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22332145_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
