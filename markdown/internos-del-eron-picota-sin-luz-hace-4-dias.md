Title: Internos del Eron Picota sin luz hace 4 días
Date: 2015-12-23 16:29
Category: DDHH, Nacional
Tags: crisis carcelaria, Eron Picota
Slug: internos-del-eron-picota-sin-luz-hace-4-dias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/crisis-carcelaria1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [ecos1360.com]

<iframe src="http://www.ivoox.com/player_ek_9828797_2_1.html?data=mp2fmpyde46ZmKiakpeJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjc%2Foxtfb0diPqMbgjKrf0dOPlMrX0NnOjdjNsozg1t%2BYysbHqYyojMmSpZiJhaXV1JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [23 Dic 2015]

Los internos del centro penitenciario del Eron Picota, en Bogotá ya cumplen 4 días sin luz, y denuncian que si protestan por la situación, **el INPEC los ha amenazado con no permitir la visitas de sus familiares y amigos.**

De acuerdo a la denuncia, **se trata de los patios 9,10,11,12,13,14,15 y 16** que se encuentran sin energía, situación que pone en riesgo la vida de todos los reclusos  incluyendo la del personal de guardia, “**La oscuridad es total… No se puede distinguir el interno que está justo al lado, hemos tenido que hacer fogatas, no podemos ver nada, ni leer”**, expresa uno de los voceros de los internos.

Este martes, hubo una reunión entre representantes de la USPEC, Contratistas de Energía, Directivas de la Picota, Interventoría, Defensoría del Pueblo e internos, para tratar la grave situación que se ha denunciado. Tras la reunión, las autoridades han comprometido **solucionar el problema a más tardar este 23 de Diciembre.**

Pese a que habían decidido iniciar una jornada de desobediencia civil y pacífica, los internos de los patios afectados han decidido abstenerse de iniciar la desobediencia pacífica esperando que las autoridades mencionadas cumplan con su compromiso.

Por el momento, los presos  hacen un llamado a los organismos de control, al gobierno y a las organizaciones defensoras de derechos humanos para que estén atentos a esta situación que evidencia nuevamente la crisis en la que se encuentran los centros penitenciarios del país.
