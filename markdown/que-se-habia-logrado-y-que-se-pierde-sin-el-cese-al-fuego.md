Title: ¿Qué se había logrado, y qué se pierde sin el Cese al Fuego?
Date: 2015-05-22 16:36
Category: Nacional, Paz
Tags: Bombardeo, cerac, Cese al fuego, David Florez, FARC, frente amplio, Gobierno, habana, paz, Santos
Slug: que-se-habia-logrado-y-que-se-pierde-sin-el-cese-al-fuego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/PAZ1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#####  

<iframe src="http://www.ivoox.com/player_ek_4535886_2_1.html?data=lZqgl52ceo6ZmKiakpyJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BflpDax9jJt4zYxpDQx9jJb8LgjMvix8zTb9TZjNfSxtrOqdPjz5DZw9iPp9Dix9fc0NnFp46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [David Correal, Centro de Recursos para el Análisis del Conflicto] 

#####  

#####  

<iframe src="http://www.ivoox.com/player_ek_4535912_2_1.html?data=lZqgl56Vdo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRaaSmhqezs9qJh5SZop6Y1cqPrMLWhqigh6aopYzg0Mzfw8nTaZO3jN6Y09qJh5SZop6Y1cqPtMrZ08nSjdjNsozZzZCwx9jJb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [David Flórez, Frente Amplio por la Paz] 

#####  

###### 22 may 2015 

Según el informe presentado el 20 de mayo por el -CERAC-, al cumplirse 5 meses del cese unilateral al fuego decretado por las FARC, se redujeron en un 85% las acciones ofensivas; las muertes de la fuerza pública cayeron en un 64%; y las muertes de la población civil relacionadas con el conflicto armado, un 73%.

A pesar de la evidente reducción de las confrontaciones, tanto el CERAC como las organizaciones sociales y de Derechos Humanos que realizaron acompañamiento al cese al fuego entre diciembre del 2014 y mayo del 2015, denunciaron que se registraron al rededor de 50 evento de confrontación como consecuencia de acciones ofensivas del Ejercito contra la guerrilla.

Según los 3 informes presentados por el Frente Amplio por la Paz en febrero, marzo y abril, los bombardeos contra campamentos de la guerrilla fueron constantes durante el cese al fuego, y las acciones en su contra dejaron un saldo de 35 guerrilleros muertos, incluidos líderes históricos de la FARC, como el comandante del Bloque 57, José David Suarez alias 'Becerro'. Es por estos motivos que desde distintas orillas académicas y sociales se afirma que es muy difícil sostener un cese al fuego unilateral cuando hay una ofensiva militar constante por parte del Ejercito colombiano.

Las organizaciones que acompañaron el proceso del cese al fuego coinciden en asegurar que si bien la violencia armada persistió en el periodo analizado, su reducción fue importante para las comunidades rurales en Colombia, así como para los sectores económicos empresariales. Según el CERAC, por ejemplo, Colombia no registraba niveles tan bajos de confrontación desde 1984. "En el año 2013 se presentaron 400 ataques contra infraestructura petrolera en Colombia, y en el 2015 -en el marco del cese al fuego- solo se han presentado 2", asegura David Flórez, del Frente Amplio por la Paz.

Es por este motivo que desde diferentes estamentos de la sociedad civil se enfatizaba en la necesidad de avanzar en el desescalamiento del conflicto, y la concreción de un cese al fuego bilateral. En el marco del inicio del proceso piloto de desminado, expertos también aseguraron que para acelerar la limpieza de los territorios de los materiales bélicos se requería un cese bilateral de hostilidades.

Organizaciones sociales y de DDHH auguran un nuevo escalamiento del conflicto, si como anunció el presidente Santos, "esta seguirá siendo la orden".

Para David Flórez, el gobierno no ha renunciado a la idea de debilitar en el campo militar a la insurgencia para tener resultados en la mesa de diálogos de paz, máxime en el marco de la discusión sobre 'justicia transicional'. "Es una lógica bastante perversa, que es la lógica que se ha aplicado en los últimos 50 años, y cuyo único resultado ha sido la prolongación de la guerra".

<div>

El próximo 4 de junio el Frente Amplio por la Paz presentará su 4to informe, con un capítulo especial sobre el movimiento indígena que ha sufrido la perdida de 17 comuneros a manos de actores armados, en lo que va corrido del último mes.

</div>
