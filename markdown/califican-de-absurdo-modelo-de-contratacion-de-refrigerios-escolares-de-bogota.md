Title: Califícan de absurdo modelo de contratación de refrigerios escolares de Bogotá
Date: 2017-04-19 16:46
Category: DDHH, Nacional
Tags: Refrigerios Escolares
Slug: califican-de-absurdo-modelo-de-contratacion-de-refrigerios-escolares-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/la-campiña.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Campiña] 

###### [19 Abr 2017] 

150.000 mil estudiantes de colegios públicos de la capital se quedaron sin refrigerio debido al cambio en el modelo de contratación, que no tendría las **suficientes empresas para cubrir la demanda en alimentación y que también estaría entregando refrigerios en malas condiciones a los niños y niñas**.

En el nuevo modelo de contratación divide en tres pasos el proceso de compra de insumos, empaque y distribución que ahora se hacen de manera independiente, es decir hay empresas que **solo se encargan de comprar los alimentos, otras de empacarlos y finalmente otras de distribuirlos**.

En el anterior modelo las empresas se presentaban a un concurso de licitación con un presupuesto que **reunía la compra de los insumos, empacar los alimentos y distribuirlos**, este sistema presentó problemas de transparencia, calidad de los alimentos y cobertura para los colegios. Le puede interesar: ["No solamente son niños, es toso el pueblo Wayúu el que se está muriendo"](https://archivo.contagioradio.com/no-solamente-son-ninos-es-todo-el-pueblo-wayuu-el-que-se-esta-muriendo/)

Las críticas que padres de familia y concejales del Distrito han hecho a este nuevo modelo de contratación, es que **no plantea soluciones claras sobre la vigilancia a las empresas contratadas y por el contrario**, terceriza aún más el proceso para hacer y llevar los refrigerios.

La Asociación Colombiana de Proveedores de Alimentos, ya había expresado su preocupación porque para ellos, las empresas licitantes solo contarían con la infraestructura necesaria para **cumplir el 40% de los 736.427 refrigerios que se deben entregar.**

Sumado a ellos, aún no hay oferentes para las licitaciones que tienen que ver con frutas y postres, dos elementos primordiales en el menú infantil y los padres han señalado que **tampoco saben a qué empresa responsabilizar por la pésima calidad de los alimentos, ya que ahora existirían 3 responsables en el proceso**.

Para María Antonieta Caro, integrante de la Asociación Distrital de Educadores, la preocupación más grande es que **“miles de niños que pertenecen a la jornada única, que tienen clases durante 8 y 9 horas**, se quedaron sin alimentos ni comidas calientes” dificultado el proceso de aprendizaje de los menores. Le puede interesar: ["132 indígenas están sin clase por incumplimientos de la Secretaría de Educación"](https://archivo.contagioradio.com/indigenas-de-putumayo-se-toman-secretaria-de-educacion-exigiendo-inicio-de-clases/)

### **Licitaciones de refrigerios salpicadas de corrupción** 

Desde el anuncio del cambio de modelo de licitación diferentes concejales han expresado su preocupación por las formas en la que se ha llevado este proceso y porque una de las empresas licitantes, **Multimodal Express S.A., se ha visto envuelta en diferentes escándalos de corrupción con refrigerios en otras partes del país**.

Uno de ellos se dio en Santa Ana (Magdalena), **en el año 2011, cuando se descubrieron varias toneladas de alimentos que debería distribuir esta compañía fueron descubiertos** enterrados en cercanías de un colegio. El gerente de la compañía Duque Martínez, explicó que los alimentos estaban vencidos y por este motivo se clasificaron como destrucción material. Le puede interesar: ["Créditos del ICETEX, deudas impagables para estudiantes del país"](https://archivo.contagioradio.com/creditos-del-icetex-deudas-impagables-para-jovenes-del-pais/)

Sin embargo, Duque también ha sido señalado de prestar un mal servicio de refrigerios en el Putumayo en donde figura como el representante legal de la Unión Temporal del Putumayo Somos Todos, operador del Programa de Alimentación Escolar de este departamento con un contrato por \$10 mil millones de pesos. **Los padres de familia y rectores de colegios han denunciado que las raciones de alimentos son insuficientes y que algunos alimentos han llegado en mal estado.**

Con estas problemáticas el nuevo modelo de contratación para los refrigerios de la Capital parece continuar sin garantizar la **alimentación para los más de 700 mil estudiantes y la corrupción en las licitaciones.**
