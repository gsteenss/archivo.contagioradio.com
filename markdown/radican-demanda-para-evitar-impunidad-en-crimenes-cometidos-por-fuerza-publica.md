Title: Radican demanda para evitar impunidad en crímenes cometidos por Fuerza Pública
Date: 2018-10-11 12:51
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: Fuerza Pública, JEP, ministerio de defensa
Slug: radican-demanda-para-evitar-impunidad-en-crimenes-cometidos-por-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/fuerzasmilitares543-e1524675574963.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [11 Oct 2018] 

[El Movimiento de Víctimas de Crímenes de Estado y colectivos de abogados defensores de DD.HH instauraron ante la Corte Constitucional una demanda  contra varios artículos de la Ley 1922 sobre las normas de procedimiento de la JEP debido a que se afectarían y vulnerarían los derechos de las víctimas de crímenes de Estado y profundizaría la impunidad frente a los delitos que cometieron la Fuerza Pública en el marco del conflicto. ]

La abogada Soraya Gutiérrez, presidenta del Colectivo de Abogados José Alvear Restrepo, señaló que la principal preocupación recae en **"que la norma establece un plazo de 18 meses para que se cree una sala especial para el juzgamiento de miembros de la Fuerza Pública"**.

Hecho que no solo atenta contra **lo pactado en los Acuerdos de Paz, sino que también es una afrenta hacia las víctimas,** "dado que los integrantes de la Fuerza Pública podrán suspender hasta dentro de 18 meses sus procesos". (Le puede interesar: ["Piden a la Jep proteger 16 lugares en donde podrían encontrarse personas desaparecidas"](https://archivo.contagioradio.com/piden-proteger-16-lugares-jep/))

### **La impunidad sobre los crímenes de la Fuerza Pública** 

Asimismo, la abogada manifestó que en la Ley 1922 **se permite la participación del Ministerio de Defensa en los procesos que se tramiten en la JEP**, situación que violaría el equilibrio en los casos, debido a que ya hay una representación en este Tribunal tanto de las víctimas como de los comparecientes, y sí se adiciona el Ministerio de Defensa, se vulnera el derecho a la igualdad y habría una doble defensa para agentes de la Fuerza pública.

Además, Gutiérrez afirmó que el Congreso también estaría intentado profundizar la impunidad al establecer una serie de disposiciones "en las que se pretende que, frente  crímenes cometidos por miembros de la Fuerza Pública, **no se puede presumir sistematicidad y no se puede presentar elementos de contexto que dieron lugar a las violaciones de derechos humanos**, lo que implicaría que en materia de investigación, se restrinja ostensiblemente la posibilidad de que las víctimas de las Fuerzas Militares, puedan acceder a la verdad, a la justicia y a la reparación".

"Para nadie es un  secreto que la mayoría de crímenes cometidos en el marco del conflicto armado y la violencia política involucra responsabilidades de miembros de las Fuerzas Militares, y que la mayoría de miembros que se encuentran postulados a la JEP, están vinculados por los graven crímenes de ejecuciones extrajudiciales" aseguró Gutiérrez.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

######  
