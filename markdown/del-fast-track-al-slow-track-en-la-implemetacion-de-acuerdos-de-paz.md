Title: Del Fast Track al Slow Track en la implemetación de Acuerdos de Paz
Date: 2017-02-14 16:15
Category: Entrevistas, Paz
Tags: implementación acuerdos de paz, Nestor Humberto Martínez
Slug: del-fast-track-al-slow-track-en-la-implemetacion-de-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/congreso_17.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [14 Feb 2017]

Los debates del Fast Track , que permiten la implementación de los Acuerdos de Paz, se han convertido, según el senador Iván Cepeda del Polo Democrático, en **“Slow Track”** **producto de la lentitud del paso de los proyectos por el Congreso y al intento de re negociación** del acuerdo que ha tenido Cambio Radical y el Fiscal Néstor Humberto Martínez.

El debate a la modificación de la Ley Quinta para que se aprobara  la participación de los voceros de Voces de Paz en el senado se aplazó por falta de quórum la semana pasada, hecho que **provocó el retraso de los debates sobre Justicia Especial para la Paz,** sin embargo, para el senador Cepeda la falta de asistencia no es el único acto que entorpece los debates.

“Hay unas series contradicciones al interior de la Unidad Nacional, de una manera bastante irresponsable las contradicciones se están llevando al plano de la paz, algunos **políticos prefieren privilegiar sus rencillas burocráticas a la aprobación de la paz**”. Sumado a esto, Cepeda señala la intervención permanente del Fiscal Néstor Humberto Martínez  en los diferentes debates, **“convirtiéndose en el nuevo jefe de bancada”. **Le puede interesar: ["Crece el riesgo para los Acuerdos de Paz"](https://archivo.contagioradio.com/crece-el-riesgo-para-los-acuerdos-de-paz/)

Para Jairo Estrada, vocero de Voces de Paz “el **Fiscal ha pretendido un proceso de renegociación de los acuerdos particularmente en lo que se refiere a al JEP**”, no obstante las propuestas del Fiscal se han ido desmontando una a una, debido a la falta de argumentos para sostenerlas. Le puede interesar: ["Modificaciones a Jurisdicción Especial abriría la puerta a Corte Penal Internacional"](https://archivo.contagioradio.com/modificaciones-a-jurisdiccion-especial-de-paz-abririan-la-puerta-a-corte-penal-internacional/)

Lo que sigue en el debate son los temas gruesos de la JEP, como los delitos continuados estipulados desde el 1 de diciembre de 2016, por ejemplo rebelión, que para el Fiscal deberían entrar a ser juzgados en la Justicia Ordinaria, mientras que Estrada afirma que deben continuar bajo la JEP, de lo contrario“**abrirán un boquete al acuerdo de paz que permitiría que de manera fácil, incluso con montajes judiciales, se procediera con imputación de cargo a integrantes de las FARC-EP**".

Tanto Cepeda como Estrada, señalaron que el paso siguiente es que sectores políticos como los que representan intente mantener la esencia de los Acuerdos de Paz, **haciendo énfasis en que el Congreso ya voto y refrendo el acuerdo**, por lo que suena exabrupto dentro de esa lógica que quienes lo votaron a favor ahora lo cuestionen.

<iframe id="audio_17016338" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17016338_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
