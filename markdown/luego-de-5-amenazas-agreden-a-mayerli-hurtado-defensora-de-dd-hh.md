Title: Luego de 5 amenazas agreden a Mayerli Hurtado, defensora de DD.HH
Date: 2017-08-09 13:30
Category: DDHH, Nacional
Tags: asesinato defensores de derechos humanos, Autodefensas Gaitanistas de Colombia
Slug: luego-de-5-amenazas-agreden-a-mayerli-hurtado-defensora-de-dd-hh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/victimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [09 Ago 2017] 

La defensora de derechos humanos y líder social, Mayerli Hurtado fue víctima de agresiones físicas e intimidación por parte de un hombre que le provoco varías heridas con un arma blanca, mientras le decía que “olía a muerto”. **Esta sería la quinta amenaza que recibe la defensora de derechos humanos**.

Minutos después de que el hombre la agredió, se retiró del lugar y abordó una moto. De acuerdo con Deivin Hurtado, integrante de la Red de Derechos Humanos Francisco Isaías Cifuentes, **la idea de que fuese un robo se descartó después de la amenaza que hizo el sujeto**. (Le puede interesar: ["Asesinan a Nidio Dávila, líder de Marcha Patriótica, en Nariño"](https://archivo.contagioradio.com/asesinan-a-nidio-davila-lider-de-marcha-patriotica-en-narino/))

En las denuncias que se habían hecho previamente, la Policía se había comprometido a dar una medida de protección primaria que consistía en dar rondas en las cercanías de los lugares que frecuentaba Mayerli, **sin embargo, Hurtado manifestó que desde que se puso en marcha la medida, la Policía la incumplió**.

“**Vemos que la institucionalidad se está quedando, en muchos casos por negligencia y pareciera que en otros casos**, con el interés propio de no proteger a los líderes” manifestó Hurtado y agregó que ya se están realizando los trámites judiciales para que se tomen medidas frente a la protección de Mayerli. (Le puede interesar: ["Amenazan de muerte integrantes de la Juventud Rebelde y Marcha Patriótica"](https://archivo.contagioradio.com/44543/))

### **El asesinato de Nidio Dávila y la falta de acciones por parte de la Fuerza Pública** 

El pasado 6 de agosto fue asesinado el defensor de derechos humanos Nidio Dávila en medio de la comunidad, para Hurtado acciones como est**a demuestran una “complicidad” entre las acciones de las autodenominadas Autodefensas Gaitanistas de Colombia** y las Fuerzas Militares.

A estos hechos se suman las denuncias que han realizado los campesinos en donde señalan que hay **“hombres que usan los vehículos de la Policía”** para realizar retenes y preguntar por los integrantes de la Asociación Campesina sin pertenecer a la Fuerza Pública. (Le puede interesar: ["En 2017 han sido asesinados 15 líderes sociales en el Cauca"](https://archivo.contagioradio.com/en-2017-han-sido-asesinados-15-lideres-sociales-en-el-cauca/))

Debido a esta situación integrantes de la comunidad han expresado que podrían desplazarse del territorio por miedo a continuar siendo víctimas de intimidaciones por parte de estas estructuras armadas**. De igual forma, hasta el momento habitantes han asegurado que son nulas las acciones por parte de las Fuerzas Militares en el territorio.**

<iframe id="audio_20255885" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20255885_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
