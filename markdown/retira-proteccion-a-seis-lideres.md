Title: UNP retira protección a seis líderes sociales amenazados en Bajo Atrato
Date: 2019-03-21 11:53
Category: DDHH, Nacional
Tags: Amenaza a líderes sociales, Bajo Atrato, Tierra y Vida - Chocó, Unidad Nacional de protección
Slug: retira-proteccion-a-seis-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/imágenlíderes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @coeuropa] 

###### [21 Mar 2019] 

Mientras las comunidades del Bajo Atrato chocoano llevan un mes denunciando incursiones de grupos paramilitares, la Unidad Nacional de Protección (UNP) retiró las esquemas de seguridad de al menos **seis líderes sociales de la región que se desempeñan como reclamantes de tierras**.

Según Yeison Farid Mosquera, director y representante legal de **Tierra y Vida - Chocó**, la Unidad retiró sus dos escoltas y un carro blindado sin previo aviso el pasado 11 de marzo. El líder quedó solo con un celular y un chaleco antibalas, medidas que teme no son suficientes para garantizar su seguridad. Además, denunció que fue notificado de las modificaciones por su escolta y no directamente por la UNP, organismo del que hasta la fecha no ha recibido algún tipo de respuesta.

"La UNP me desmonta la esquema **sin darme ninguna explicación**. Ahora que no tengo la esquema, los grupos armados me llaman cada rato diciéndome que me van a matar por sapo", denunció el director. Pese a que Mosquera tuvo que dejar su hogar, continúa realizando sus labores como reclamante de tierras y radicará una denuncia ante las autoridades competentes.

La semana pasada, la UNP también levantó los esquemas de seguridad de **otros cinco integrantes de Tierra y Vida - Chocó** y retiró uno de los dos escoltas y el carro blindado que se había entregado a **tres compañeros más**. Mosquera indica que estas medidas habían sido concedidas en 2018 por la UNP frente a las amenazas que sufren estos líderes por sus labores. El director aclara que la asociación visibiliza la labor de la **restitución de tierras**, ayuda a la Defensoria del Pueblo en **emitir alertas tempranas** y había denunciado los presuntos **nexos entre el Ejército y los paramilitares** en la región.

Esta situación es "catastrófica" para los líderes dado el aumento de la violencia y control paramilitar que se viene registrando en el último mes en el Bajo Atrato. En consecuencia de las medidas de la UNP, la organización Tierra y Vida se ha visto obligada a limitar las visitas a las comunidades que realizaba en la región. (Le puede interesar: "[Paramilitares están retomando el control del Bajo Atrato](https://archivo.contagioradio.com/paramilitares-estan-retomando-el-control-del-bajo-atrato/)")

Frente a estos hechos, Tierra y Vida solicita que las medidas de protección sean restituidas a los líderes. Además, piden que la Fiscalía emita un reporte sobre las investigaciones de los asesinatos de **más de 35 líderes sociales** en el Bajo Atrato. (Le puede interesar: "[Asesinado líder reclamante de tierras Hernán Bedoya en Chocó](https://archivo.contagioradio.com/asesinado-reclamante-de-tierras-hernan-bedoya/)")

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
