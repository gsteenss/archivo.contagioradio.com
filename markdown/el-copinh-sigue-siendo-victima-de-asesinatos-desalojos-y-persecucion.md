Title: El COPINH sigue siendo víctima de asesinatos, desalojos y persecución
Date: 2016-03-16 16:09
Category: DDHH, Entrevistas
Tags: Bajo Aguan, Berta Cáceres, Copinh, honduras, Proyecto hidroeléctrico Agua Zarca
Slug: el-copinh-sigue-siendo-victima-de-asesinatos-desalojos-y-persecucion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/copinh-3-honduras-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elmundo.cr] 

###### [16 Mar 2016]

En el marco del desalojo a 150 familias, habitantes de 2 fincas en Río Chiquito, del municipio de Río Lindo, fue **asesinado con varios impactos de arma de fuego el líder ambientalista e integrante del COPINH, Nelson García**, quien asistió solidariamente a las familias desalojadas por los efectivos de la Policía de ese país. Según información del COPINH el hecho se produjo hacia las 6 de la tarde frente a la casa de uno de sus familiares.

El asesinato se produce apenas 13 días después del [asesinato de Berta Cáceres](https://archivo.contagioradio.com/?s=berta+caceres), líder y fundadora del **Consejo Cívico de Organizaciones Populares e Indígenas de Honduras**. Nelson García era uno de los líderes más reconocidos de este movimiento en la región de Intibuca. Con Nelson la cifra de personas asesinadas desde 2015 asciende a 106 entre las que se encuentran integrantes de comunidades indígenas, periodistas y líderes políticos.

Frente al desalojo en el municipio de Río Lindo, el CONPINH informó que las **víctimas fueron 150 familias que habitaban dos predios desde hace más de dos años**, y que habían sido recuperados luego de varios desalojos por parte de las empresas que pretenden controlar los territorios, muchos de ellos pertenecientes a comunidades indígenas. **Durante el desalojo se destruyeron las casas y los cultivos de pan coger de las familias.**

A pesar de las medidas cautelares de la **Comisión Interamericana de DDHH** a favor de los integrantes del [COPINH](https://archivo.contagioradio.com/indigenas-hondurenos-defienden-el-rio-gualcarque-de-construccion-de-represa/) y las múltiples denuncias por amenazas y hostigamientos por parte de las empresas contra las personas de esa organización, la policía de Honduras afirmó, en un comunicado público, que el asesinato de Nelson era un “hecho aislado” que no tenía nada que ver con el desalojo que se realizó en horas de la tarde.

Los integrantes del COPINH en medio del dolor y la zozobra, siguen exigiendo justicia por los recientes asesinatos pero reafirman que seguirán firmes en la defensa de sus territorios. Exigencia que fue respaldada por el **Banco Holandés de Desarrollo que anunció hoy que congela sus inversiones en Honduras incluyendo las relacionadas con el proyecto Agua Zarca**
