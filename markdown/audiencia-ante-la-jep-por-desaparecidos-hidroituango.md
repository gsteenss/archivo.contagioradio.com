Title: Ante la JEP buscan reconocimiento de los desaparecidos en HidroItuango
Date: 2019-10-07 18:00
Author: CtgAdm
Category: Judicial, Paz
Tags: Asesinatos, Hidroituango, JEP, víctimas
Slug: audiencia-ante-la-jep-por-desaparecidos-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/infografìa-hidroituango3.pdf" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/infografia-hidroituango.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-08-at-8.56.25-AM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Archivo] 

Este  8 y 9 de octubre se realizará  en el Palacio de Justicia de  Medellín, la audiencia que investiga los desaparecidos y la exhumacion e inhumación  de cuerpos de las victimas en municipios como Ituango, Toledo, Sabanalarga, Peque, Valdivia y Briseño que se encuentran en el territorio donde ahora está Hidroituango. (Le puede interesar:[Antioquia, segundo departamento más peligroso para la defensa de DD.HH.](https://archivo.contagioradio.com/antioquia-segundo-departamento-mas-peligroso-para-la-defensa-de-dd-hh/))

En  esta  audiencia se generará un dialogo que nutra la información sobre la necesidad de  crear medidas cautelares en los lugares donde se sospecha que existen cuerpos de desaparecidas. Ante ello Isabel Cristina  Zuleta  ambientalista y asistente a esta audiencia afirmó,"es importante que se entienda  que esto es el reflejo de una lucha de años, por la verdad, la justicia y el duelo de las familias".

Al tribunal se presentarán un aproximado de 100 personas representantes de la región, Occidente, Norte y Bajo Cauca además de la víctimas de la  Mojana, todos afectados por el megaproyecto; "pretendemos y exigimos que se tomen medidas para de protección y la recuperación de esos cuerpos de manera efectiva, al igual que la identificación  y entrega digna a los familiares de los desaparecidos"

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-08-at-8.56.25-AM-1014x1024.jpeg){.aligncenter .wp-image-74732 width="412" height="416"}

###### [Infografía por : Movice] 

Zuleta resaltó que este tema de las víctimas lleva siendo tratado  desde el 2010 ante organismos de Derechos Humanos, donde por medio de cartas y pedidos, solicitan atención ante esta organización y denuncian la ineficiencia de organismos como la Fiscalía para para buscar a las personas y proteger los sitios donde pueden estar enterrados. (Le puede interesar:[“¿Por qué los buscamos? Porque los amamos”](https://archivo.contagioradio.com/por-que-los-buscamos-porque-los-amamos/))

> *"esperamos que la JEP resguarde estos lugares para encontrar a los desaparecidos, tenemos esperanza con que esta audiencia va a resignificar el sentido de la muerte en el Cañon del Río Cauca,al igual que  el respeto por la muerte, por eso el sentido ha sido de esta audiencia es que se  escuchen a los muertos, para que cuenten lo ocurrido con sus vidas a todo el  país"*

La petición de esta medida cautelar será sustentada  por el Movimiento de Victimas de Crímenes de Estado (Movice) y el Movimiento Ríos Vivos, quienes podrán acordar, junto las alcaldías de las poblaciones afectadas, la Fiscalía, EPM, la Gobernación de Antioquia, el Ministerio de Interior y representantes de HidroItuango, los mecanismos que se aplicarán para el desarrollo de la búsqueda y reconocimiento de los desaparecidos.

<iframe id="audio_42763213" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_42763213_4_1.html?c1=ff6600"></iframe>  
**Síguenos en Facebook:**<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
