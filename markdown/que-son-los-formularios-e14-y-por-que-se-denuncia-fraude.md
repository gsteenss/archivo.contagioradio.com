Title: ¿Qué son los formularios E 14 y por qué se denuncia fraude?
Date: 2018-05-30 12:28
Category: Nacional
Tags: elecciones 2018, Gustavo Petro, Iván Duque, Registraduría Nacional
Slug: que-son-los-formularios-e14-y-por-que-se-denuncia-fraude
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/WhatsApp-Image-2018-05-30-at-5.55.47-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Popayan] 

###### [30 May 2018] 

El registrador nacional Juan Carlos Galindo, afirmó que no se ha comprobado la existencia de fraude en los formularios E 14. Sin embargo, las personas siguen denunciando la alteración de los números que se registran en estos documentos. De acuerdo con Alejandra Barrios, directora de la Misión de Observación Electoral, **este hecho evidencia la urgencia por cambiar las formas de recolección de la información electoral para garantizar la transparencia de la misma**.

Barrios afirmó que la MOE tendrá acceso a esos formularios a partir de hoy y una vez la Registraduría realice la consecuente revisión, el Observatorio procederá a analizar la información. Las denuncias que han hecho las personas radican en que los fórmularios E14 fueron llenados con tachones en los números, **lo que podría significar una alteración en los datos reales.**

### **¿Qué son los E 14?** 

De acuerdo con Barrios, una vez se hace el conteo de votos, los jurados y quienes estuvieron en la mesa de votación, se reúnen y llenan el formulario E 14, **que está conformado por tres cuerpos que debe llenarse de manera idéntica**. Uno de los documentos es el de delegados, que es el que las personas pueden ver en la paginas web de la Registraduria.

Otro de los E14 es el de transmisión, diligenciado por funcionarios de la Registraduría y el tercer formulario, que es el único que tiene valor jurídico, es el que tienen las comisiones escrutadoras, **sobre esa copia es que se realiza el proceso de escrutinio y se suman los votos para llenar otros formularios.**

“Esa es la información que realmente debemos conocer, si esa información tiene modificaciones estamos frente a una alteración de resultados, si no la tienen modificaciones, no hay alteración de resultados” afirmó Barrios. (Le puede interesar: ["Votos de Fajardo y De la Calle el escenario de disputa para las elecciones 2018"](https://archivo.contagioradio.com/votos-de-fajardo-y-de-la-calle-el-escenario-de-disputa-para-elecciones-0218/))

### **¿Qué podría pasar en segunda vuelta?** 

Frente a cómo garantizar que este hecho no se repita en la segunda vuelta, Barrios señaló que habló con el Registrador y se planteó la posibilidad de colocar un instructivo adicional para los jurados de votación.

Sin embargo, la directora de la MOE expresó que mientras no se garantice un mecanismo que copie la misma información en los formatos E14 y deje de hacerse de forma manual, el riesgo existirá, **“vamos a tener que generar mayores mecanismos de información a los jurados de votación para que no ocurra modificación de resultados”** afirmó Barrios.

<iframe id="audio_26263082" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26263082_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
