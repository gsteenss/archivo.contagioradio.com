Title: Con Suiza como invitado de honor arranca Eurocine 2016
Date: 2016-04-05 16:37
Category: Cultura, eventos
Tags: Festival Eurocine, Programación Eurocine
Slug: con-suiza-como-invitado-de-honor-arranca-eurocine-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/eurocine-e1459892334807.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Eurocine 

##### [5 Abr 2016] 

Con una selección especial de 11 producciones entre largometrajes, cortometrajes y documentales provenientes de Suiza, país invitado de honor, inicia este miércoles 6 de abril la edición número 22 del Festival de cine Europeo en Colombia "Eurocine", un espacio que busca crear vínculos y establecer puentes culturales entre continentes.

Para la presente edición, Eurocine rendirá homenaje al trabajo del  joven director suizo Lionel Baier, quien con su ópera prima "Garçon Stupide" hizo parte de la sección "Tout les Cinemas du Monde" del Festival de cine de Cannes 2006, entre otros reconocimientos, con una retrospectiva que incluye cuatro de sus producciones cinematográficas entre las que destaca "La Vanité" protagonizada por la consagrada actriz Carmen Maura, invitada especial del evento.

La muestra oficial, compuesta por 48 producciones clásicas, contemporáneas y estrenos de 18 países de Europa, recorrerá salas de cine y espacios culturales de cinco de las ciudades principales del país, empezando en Bogotá hasta el 17 de abril, Cali del 29 de abril al 3 de mayo, Medellín del 21 al 28 de abril, Barranquilla y Pereira del 5 al 11 de mayo.

Adicionalmente, el Festival llegará a 11 comunidades, municipios, y poblaciones marginales con la propuesta de "Eurocine Comunitario", proponiendo espacios para la reflexión y transformación positiva del entorno, utilizando la exhibición cinematográfica como agente catalizador de conflictos.

<iframe src="https://www.youtube.com/embed/lcciowl-zbs" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Eurocine abre en 2016 un espacio  para la música, desde su slogan "Dale Play" e imagen oficial del evento y la relación que guarda con el cine, como "canal para transmitir sentimientos y emociones" en cada uno de los relatos audiovisuales, así como también apuesta por producciones que sobresalen por su gran riqueza estética y artística.

[Programación Eurocine 2016](https://es.scribd.com/doc/307094447/Programacion-Eurocine-2016 "View Programación Eurocine 2016 on Scribd")

<iframe id="doc_40754" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/307094447/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
