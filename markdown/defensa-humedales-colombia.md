Title: La lucha por defender de los humedales
Date: 2018-08-13 09:30
Category: Voces de la Tierra
Tags: Ambiente, humedales Bogotá
Slug: defensa-humedales-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/16665140_1273042959446466_2589086781664668358_o-e1534168925718.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Humedales Bogotá- Daniel Bernal 

###### 9 Ago 2018 

Los humedales constituyen un importante sistema de tierras húmedas en la cordillera de los Andes, y una importante reserva de fauna y flora par la ciudad de Bogotá, entre ellas **más de 70 especies de aves migratorias, gran variedad de especies endémicas y vegetales.** Lamentablemente el paso del tiempo y el crecimiento urbano, han reducido su extensión **pasando de 150.000** [**hectáreas**]** en 1940 a 1500 al día de hoy.**

En Voces de la Tierra nos preguntamos ¿Sabes cuántos humedales hay en las zonas rurales y urbanas de Colombia? ¿Por qué son importantes para el planeta? , para eso nos acompañaron **Juan Carlos Gutiérrez,** subdirector de la Fundación Alma; organización que trabaja por la protección  de los valores naturales; y **Diana Castro**, administradora ambiental y joven lideresa del humedal La Vaca.

<iframe id="audio_27776211" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27776211_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
