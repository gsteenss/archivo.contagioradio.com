Title: La violencia un factor que rompe las tradiciones alimenticias en la niñez indígena
Date: 2019-08-27 13:05
Author: CtgAdm
Category: Nacional
Tags: amazonas, Cauca, Chocó, dia del niño indigena, Niñez indígena
Slug: alimentacion-ninez-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/alimentacion-de-ninos-indigenas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Julian Gómez - JyP] 

A propósito del **Día Nacional de la Niñez Indígena, celebrado el pasado 26 de agosto**, integrantes de diferentes organizaciones indígenas realizaron un homenaje a la memoria de todos los niños que han sido víctimas del desplazamiento forzado, explotación minera y cambio climático, **factores de peso para la extinción de sus tradiciones, una de ellas sus hábitos alimenticios.**

Lejandrina Pastor, consejera de Mujer y Familia de la Organización Nacional Indígena de Colombia (ONIC), resaltó que **para defender la niñez indígena se deben pensar leyes según cada región donde habiten,** “no es lo mismo la situación que pasan los niños nativos en Amazonas, a lo que pasan los del Chocó o La Guajira, hay que tener en cuenta factores como los cultivos, el clima e incluso si están o no dentro de las comunidades activas”.

Según esto, regiones como Chocó, Cauca, Amazonas y Orinoquía, han perdido de manera preocupante sus raíces, por causas como el desplazamiento forzado afectando directamente la cultura alimenticia, **“uno es lo que come, somos una cultura de la cosecha y no de la guía nutritiva, y esto lo hemos intentado explicar al gobierno”.** (Le puede interesar: [La ONU advierte que la situación de los pueblos indígenas en el mundo es peor que hace 10 años](https://archivo.contagioradio.com/el-mundo-sigue-rezagado-con-respecto-a-los-derechos-de-los-indigenas-10-anos-despues-de-su-declaracion-historica-advierten-expertos-de-la-onu/))

### Incentivar hábitos en la niñez indígena 

Desde las organizaciones se intentan incluir en los hábitos de formación el cuidado diferencial cultural y espiritual, manteniendo los juegos indígenas, los alimentos y especialmente el lenguaje, **“la tierra sabe lo que el niño debe comer, se alimentan con lo que la tierra provea dependiendo de la cosecha, al crecer lejos de su zona esto se pierde y se tienen que modificar los costumbres. ”**, afirma Lejandrina Pastor.

La consejera destaca que **estas prácticas de alimentación deben ser respetadas por el Gobierno y así mismo, ser cobijadas por la ley**, para que las niñas y niños indígenas desplazados de sus territorios, no pierdan sus tradiciones  al ser remplazadas por dietas diseñadas. (Le puede interesar: [“Niños y niñas indígenas no mueren de hambre sino de abandono”](https://archivo.contagioradio.com/ninos-y-ninas-indigenas-no-mueren-de-hambre-sino-de-abandono/))

Frente a esto y en memoria de todos los niños  indígenas que han fallecido o han crecido lejos de sus raíces, por causa del deslazamiento forzado,  se radicó un proyecto de ley en el congreso, donde se solicita institucionalizar este día y así asegurar el crecimiento exitoso de todos los niños indígenas.

**Síguenos en Facebook:**  
<iframe id="audio_40607836" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40607836_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
