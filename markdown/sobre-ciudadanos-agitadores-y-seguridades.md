Title: Sobre ciudadanos, agitadores y seguridades
Date: 2016-02-17 12:22
Category: Cesar, Opinion
Tags: Bogotá, Peñalosa, protesta
Slug: sobre-ciudadanos-agitadores-y-seguridades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/esmad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Noticias Uno 

#### **[Cesar Torres del Río  ](https://archivo.contagioradio.com/cesar-torres-del-rio/) ** 

###### 17 Feb 2016 

[Karl Kraus, reconocido intelectual y periodista alemán en los años 30, advertía un hecho evidente en las confrontaciones sociales durante la república de Weimar y el período nazi: “]*[El escándalo comienza cuando llega la Policía, no al contrario]*[”.  Las protestas normales por aumento de salarios, mejora de las políticas de administración ciudadana, mayores libertades públicas y cumplimiento de derechos (a la huelga, por ejemplo) eran “analizadas”  y tratadas por las autoridades como asunto de conspiraciones contra el régimen dirigidas por agitadores profesionales, y por tanto tenían que ser reprimidas por la fuerza.]

[Esa situación es hoy una constante en Europa, África o América Latina.  Para las élites dirigentes es la seguridad del Estado la que está en juego en cada movilización por asuntos netamente ciudadanos, de convivencia, de movilidad o de transporte público. El argumento de siempre es que actores externos (“agitadores”) engañan a las comunidades y en sus nombres realizan actos contrarios a la estabilidad institucional y democrática, actos que perjudican, además, a los propios ciudadanos.  Otro actor “externo” es el refugiado, que por el simple hecho de serlo es estigmatizado y responsabilizado de alterar la normalidad.]

[En Colombia ser ciudadano no es fácil. Somos sospechosos]*[per se]*[. El Estado es el que nos otorga la condición de ciudadanos, siempre y cuando nos conduzcamos según los parámetros que se nos impone. Aquí no tuvimos “Contrato Social” sino  Leviatán. El Estado nos vigila, nos acosa, nos “chuza”, vigila nuestra condición sexual, se nos “cuela” en la cama, pretende controlar nuestras acciones con las cámaras de video en las esquinas de las calles, en las estaciones de Transmilenio o en los centros comerciales. “Son para su tranquilidad”, se nos dice, “para  prevenir el delito”, se agrega. Si bien las acciones que tienden a garantizar la seguridad de las personas deberían ser consideradas como positivas las biopolíticas gubernamentales se dirigen a anular al ciudadano como tal, a despojarlo de su identidad, a inducirlo en la creencia de que hay un “otro”, un “tercero” que lo afectará,  y que es la policía, el ejército, la institucionalidad  o la suprema cámara lo que lo alejará del peligro.]

[Este tercero es el “agitador”, figura inventada por el Leviatán. Los problemas sociales no los generan las comunidades; se deben a la intervención del Estado: neoliberalismo, corrupción (Reficar, Policía, ICBF y demás), clientelismo, administración pública, políticas internacional  y educativa, y así]*[ad infinitum]*[. Frente a agresión estatal, las comunidades resisten y se movilizan. Es lo que pasó recientemente, y seguirá pasando, con el transporte público Transmilenio. Éste ya copó su funcionabilidad; hay que mejorarlo con más rutas,]**con baños en las estaciones**[ y con más buses, por ejemplo. Frente a los fallos repetidos del sistema de transporte los ciudadanos reaccionaron, con los consabidos excesos de parte de algunos de ellos – como ocurre siempre. ¿Porqué judicializar al agredido?]

[Bueno, la respuesta tiene que ver, parciamente, con  la llamada seguridad del Estado.  El Leviatán colombiano continúa criminalizando la protesta civil, social y ciudadana. Es el]*[enemigo]*[ el que actúa. Como tal hay que tratarlo; esto implica una fuerte represión, policial en principio. Es decir, el Estado se]*[defiende]*[. Aquí surgen preguntas para el período de “posconflicto” que se avecina: sin “enemigo” (insurgencia  armada) ¿el ciudadano seguirá siendo culpable de antemano? ¿el ciudadano seguirá siendo materia de la seguridad “nacional”? ¿la seguridad ciudadana reemplazará la criminalización de la protesta?]

[Hasta ahora nada nos indica que el estatuto penal  sea ajustado  - minimizado, adaptado – a lo que hoy se consideran como políticas de seguridad ciudadana. Impera en las corrientes civiles y militares la seguridad del Estado. El ciudadano “no pasará”; el agitador profesional mantendrá su primacía.]

[ADENDA.]

[La biopolítica del Leviatán enfoca ahora - incluidos algunos medios de comunicación del Capital - sus cámaras de video hacia la condición sexual de las personas y sus intimidades de alcoba (o de medios de transporte …). El senador Roy Barreras lo ha expresado bien, aunque de modo equivocado: llegados a este punto regresaríamos a la Inquisición. El problema, y el asunto, es que]*[ la Inquisición está actuando]*[. La Procuraduría la encabeza.]
