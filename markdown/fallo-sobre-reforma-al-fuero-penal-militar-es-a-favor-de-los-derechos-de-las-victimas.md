Title: Fallo sobre reforma al Fuero Penal Militar es a favor de los derechos de las víctimas
Date: 2016-03-01 17:43
Category: Judicial, Nacional
Tags: Derecho Internacional Humanitario, Derechos Humanos, fuero penal militar
Slug: fallo-sobre-reforma-al-fuero-penal-militar-es-a-favor-de-los-derechos-de-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Uniderecho 

<iframe src="http://co.ivoox.com/es/player_ek_10642518_2_1.html?data=kpWjlpeZdZmhhpywj5WdaZS1kZ2ah5yncZOhhpywj5WRaZi3jpWah5yncafVzdHcjdjTptPZjNfSyNTWscKfwtGYqNrJttCfscrbw9GPkcrgytnO1JDJt4zVjMvO2NTWb8Whhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Mateo Gómez, Comisión Colombiana de Juristas] 

###### [1 Mar 2016] 

El pasado 24 de febrero, la Corte Constitucional emitió el fallo sobre la demanda interpuesta por diversas organizaciones sociales y de víctimas que buscaban que el Derecho Internacional de los Derechos Humanos fuera tenido en cuenta a la hora de juzgar a los militares que cometieran delitos en el marco del conflicto armado.

Mateo Gómez, abogado de la Comisión Colombiana de Juristas, una de las organizaciones demandantes, asegura que están satisfechos con el fallo de la Corte, pues “Reiteró que el Derecho Internacional de los Derechos Humanos es aplicable en la investigación y juzgamiento de los delitos cometidos por miembros de fuerza pública. **Una decisión a favor de los derechos de las víctimas y el Estado de derecho”**, según dice la Comisión mediante  un comunicado.

De acuerdo con Gómez, el DIH es un marco jurídico aplicable a todo caso, por lo tanto no se puede afirmar que esté en contravía de Constitución  Política, o contra de los derechos de las víctimas, “Hemos dicho que era necesario que se incluyera el Derechos Internacional de los Derechos Humanos y **la Corte nos ha dado la razón cuando la dicho que la aplicación del DIH  es complementaria con los DDHH”.**

Finalmente, desde la Comisión Colombiana de Juristas se resalta que la Corte Constitucional ha dicho que los crímenes de Lesa Humanidad, como las **desapariciones o las ejecuciones extrajudiciales, no pueden ser competencia de la justicia penal militar,** y dada su gravedad, deben juzgarse por la justicia ordinaria.
