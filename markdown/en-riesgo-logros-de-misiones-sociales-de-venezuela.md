Title: En riesgo logros de misiones sociales de Venezuela
Date: 2015-12-08 08:36
Category: Economía, El mundo
Tags: alcances gobierno bolivariano, Elecciones Venezuela, MUD, Nicolas Maduro
Slug: en-riesgo-logros-de-misiones-sociales-de-venezuela
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/flag-protest-crop.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### foto: Venezuela.ve 

##### [7 Dic 2015]

Luego de conocerse los resultados de los comicios parlamentarios del 6 de diciembre, donde la Mesa de Unidad Democrática (MUD) obtuvo la mayoría de curules en el parlamento venezolano, crece la incertidumbre sobre la continuidad de algunas de las conquistas que en materia social había alcanzado el gobierno bolivariano en el vecino país.

A pesar de la marcada campaña de desprestigio iniciada por varios medios de comunicación internacionales y de la oposición, con la que se cuestionaba la legitimidad del accionar gubernamental en el desarrollo de las elecciones, estas trascurrieron en calma y los resultados fueron reconocidos por el mismo presidente Nicolás Maduro.

La insistencia de algunos medios empresariales en subrayar y maximixar de manera reiterada las falencias del gobierno venezolano, ha promovido el desconocimiento de los logros históricos obtenidos en los últimos 12 años ante la opinión pública, incrementando el descontento en sectores de la población.

Algunos de esos datos, ignorados por los comunicadores opositores al chavismo son los siguientes:

-   A través de 30 misiones sociales Venezuela logró reducir el porcentaje de hogares pobres, de 54% en el primer semestre de 2003, a 27,5% en el primer semestre de 2007, en tanto que los hogares en pobreza extrema disminuyeron de 25,1% a 7,6% en el mismo período.
-   La tendencia del bajo precio del petróleo no ha significado una desaceleración de los programas sociales, que han impactado favorablemente a 20 millones de venezolanos, posibilitando el mejoramiento de la calidad de vida de sus habitantes más pobres.
-   Informes de los organismos multilaterales de la región, la República Bolivariana de Venezuela es uno de los países que más ha avanzado en la reducción de la pobreza en el mundo hasta el día de hoy.
-   Uno de los índices más relevantes a nivel internacional, el Índice de Desarrollo Humano (IDH), ha experimentado en el país un crecimiento sostenido, pasando de un IDH bajo (0,6917) en 1998, a un IDH alto (0,8836) en 2010.
-   En los últimos 12 años, el gobierno bolivariano de Venezuela ha puesto en el centro de sus políticas públicas y sus acciones de protección en derechos humanos a la mujer, como pocos países de las américas.
-   En el plan mujeres del barrio, las mujeres cuentan con apoyo económico, social y psicológico para su empoderamiento integral, las mujeres han sido sujetos especiales de atención y de derechos., en particular, las cabezas de familia y las empobrecidas.
-   Se reconoce las labores del hogar como un trabajo, y por eso, hay una serie de derechos y el  acceso priorizado a través de políticas públicas a las madres, y mujeres; existe un programa priorizado para las mujeres en pobreza extrema, se llama Barrio Adentro.
-   Las mujeres cabeza de familia y empobrecidas, las de 55 años en adelante y los adultos mayores son beneficiados con programas especiales de atención por parte del gobierno. Venezuela ha sido uno de los pocos países en el mundo que creó el Ministerio para la Mujeres y desde el 2006 cuenta con el Plan Mujeres del Barrio.
-   de 28.946.1012 de habitantes, 49,7% de sexo masculino y 50,3% correspondiente del sexo femenino; los niños, niñas y adolescentes representan el 32,8% de la población nacional, y son parte de las mayores iniciativas de protección de sus derechos.
-   Las políticas económicas y sociales han contribuido a la disminución de la pobreza, evidenciando un descenso desde el 2004 cuando se registró un 22,5% de personas en situación de pobreza extrema, que descendió a 7,1% para el segundo semestre del 2012 según la Encuesta de Hogares por Muestreo (EHM) y se ha disminuido significativamente la brecha de desigualdad existente en el país, para el año 1994 el Coeficiente de Gini se ubicaba en 0,4911; en el año 2011 este indicador se ubicó en 0,3900, esto se traduce en una mejor distribución del ingreso entre aquellos grupos sociales con mayores ingresos y los que menos poseen.
-   La Gran Misión Vivienda Venezuela, creada en el 2011 como un programa social que garantiza el disfrute de una vivienda digna, mediante la cual se han construido más de 400.000 unidades habitacionales en todo el país.
-   La universalización de la educación primaria se ha logrado antes del año 2015, según la tendencia mostrada por la tasa neta de escolaridad, el período 2011-2012 esta llego al 92,2%. Según cifras del Censo 2011, el 95,1% de la población mayor de 10 años es alfabeta.
-   El 94,7 % de la población venezolana consume entre tres o más comidas al día y la disponibilidad calórica se ha incrementado en los últimos 10 años en un 49,6% alcanzando, 3.182 kilocalorías/persona/día. Esta disponibilidad diaria es notablemente superior a las 2.200 Kcal/día promedio de América Latina, Asia y África.
-   La Misión Alimentación atiende a 17,5 millones de personas, con una cobertura que llega a 61% de la población venezolana, quienes gozan de un subsidio que llega a alcanzar más del 80% del total del precio de los alimentos.
-   4.015.15811 niños y niñas comen en las escuelas sin costo alguno gracias al Programa deAlimentación Escolar (PAE) y 900.000 mil beneficiarios y aproximadamente 185.000.
-   Venezuela ha realizado progresos significativos en la erradicación del hambre, cumpliendo anticipadamente con la meta del milenio referente al hambre (reducir a la mitad antes del 2015 la proporción de personas que sufren hambre) y superado el compromiso internacional asumido en la Cumbre Mundial sobre la Alimentación (CMA), celebrada en el año 1996, donde se estableció la meta de reducir a la mitad el número de personas desnutridas en cada país antes del año 2015.

