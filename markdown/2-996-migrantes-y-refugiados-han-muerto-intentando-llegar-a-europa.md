Title: 2.996 migrantes y refugiados han muerto intentando llegar a Europa
Date: 2016-07-26 11:26
Category: El mundo
Tags: migrantes y refugiados, muertes en el Mediterráneo, Organización Internacional para las Migraciones
Slug: 2-996-migrantes-y-refugiados-han-muerto-intentando-llegar-a-europa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/OIM-refugiados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Canal de Noticia ] 

###### [26 Julio 2016] 

De acuerdo con el más reciente informe de la Organización Internacional para las Migraciones, **en lo que va corrido de este año han muerto 2.996 migrantes y refugiados** en su intento por cruzar el Mediterráneo, un aumento significativo en comparación con las cifras reportadas durante los primeros seis meses de 2015 cuando se registraron 1.906 muertes.

Durante este primer semestre, **por lo menos 250 mil migrantes y refugiados han llegado a Europa por vía marítima**, principalmente a Italia y Grecia. La primera nación ha reportado 2.606 muertes y 84.052 arribos, mientras que a la segunda llegaron 160.000 migrantes y 383 fallecieron en el intento.

<div class="field field-name-body field-type-text-with-summary field-label-hidden">

<div class="field-items">

<div class="field-item even">

"Sin duda, este año la cifra de 3.000 muertes se alcanzará meses antes que en cada uno de los dos años anteriores (...) **un promedio de 20 muertes diarias**", asegura la OIM; en 2014 esta cifra se alcanzó en septiembre, y el año pasado, en octubre.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

</div>

</div>

</div>

######  
