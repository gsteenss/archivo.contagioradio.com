Title: 35 indígenas han sido asesinados durante Gobierno Duque: ONIC
Date: 2018-12-06 17:30
Author: AdminContagio
Category: Educación, Nacional
Tags: Comunero, Cridec, indígenas, ONIC
Slug: asesinados-gobierno-duque
Status: published

###### [Foto: La Patria] 

###### [6 Dic 2018] 

La Organización Nacional Indígena de Colombia (ONIC) denunció que con el asesinato del comunero Edison de Jesús Naranjo Navarro, se han perpetrado 35 homicidios en los 4 meses que lleva Iván Duque como presidente de la República. Por esta razón, solicitaron medidas para detener los atentados contra los pueblos indígenas que, en los primeros 4 días de diciembre, ya han dejado 3 personas asesinadas y 4 heridas.

En la denuncia de la ONIC, desde el 7 de agosto contabilizan 1 herido por mina antipersona, 20 atentados, 61 amenazas y 35 homicidios; el último de ellos fue el del comunero Edison de Jesús Naranjo Navarro, quien era esposo de una de las hijas de la Gobernadora del resguardo indígena de Cañamomo Lomaprieta, Arnobia Moreno Andica. Los hechos se presentaron el 4 de diciembre, en el resguardo de Nuestra Señora Candelaria de la Montaña, en el Municipio de Río Sucio, Caldas.

De acuerdo al comunicado del Consejo Regional Indígena de Caldas (CRIDEC), Naranjo tenía 41 años y ya había sido amenazado. Según Higinio Obispo, secretario general de la ONIC, “como comunero, Edison se dedicaba a los cultivos, también al apoyo del ejercicio del gobierno propio y la guardia indígena”. (Le puede interesar: ["Familia embera es asesinada en Río Sucio, Caldas"](https://archivo.contagioradio.com/familia-embera-es-asesinada-en-riosucio-caldas/))

Obispo afirmó que en la zona donde fue asesinado el comunero hay paramilitares y otros actores armados, pero resaltó que “siempre ha sido el paramilitarismo el que asesina a nuestros compañeros”; esta situación, sumada al reciente homicidio de tres integrantes de una familia en el Resguardo de San Lorenzo, ha propagado la sensación de que el “Gobierno no garantiza la vida como derecho fundamental para la pervivencia de los pueblos indígenas”.

El secretario de la ONIC sostuvo que no había claridades sobre los posibles móviles del asesinato de Naranjo, pero indicó que los indígenas hacen una férrea defensa por el territorio y los derechos, situación que incomoda a los diferentes actores armados, y que finalmente causan retaliaciones. Además, Obispo resaltó que mientras los actores armados están incursionando en el territorio y asesinando a los comuneros, la fuerza pública no está haciendo lo suficiente para controlar esta situación.

### **Comunidades indígenas se declaran en asamblea permanente** 

Obispo manifestó que en un mes que debería ser de alegría y felicidad, se ha convertido para los indígenas en un momento de tristeza, zozobra y desconfianza ante la ocurrencia de estas situaciones, en medio de un proceso de paz. Por esta razón, las comunidades optaron por declararse en asamblea permanente. (Le puede interesar:["Aún no acaba 2018 y ya son 34 indígenas Awá asesinados"](https://archivo.contagioradio.com/indigenas-awa-asesinados/))

Las 3 exigencias planteadas por la asamblea son, que se garantice efectivamente el derecho a la vida de los pueblos indígenas; que se investigue diligentemente los hechos ocurridos, “porque estamos ante un mar de impunidad”; y que la comunidad internacional se pronuncie sobre estos acontecimientos, “porque no puede ser que se haga como si nada estuviera pasando en Colombia”.

Como concluyó Obispo, “se trata de protestar para que un Estado en situación de crisis, tome cartas en el asunto de inmediato y pueda generar las condiciones pertinentes para salvaguardar la vida de las comunidades Embera Chamí en el departamento”. (Le puede interesar: ["Difamación contra defensores de DD.HH. no será tolerada: Michel Forst"](https://archivo.contagioradio.com/difamacion-contra-defensores-de-dd-hh-no-sera-tolerada-michel-forst/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
