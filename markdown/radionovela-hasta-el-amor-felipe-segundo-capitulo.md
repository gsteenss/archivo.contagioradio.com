Title: Radionovela "Hasta el amor Felipe", segundo capítulo
Date: 2019-12-23 17:37
Author: CtgAdm
Category: Expreso Libertad
Tags: Expreso Libertad, Falsos Positivos Judiciales, radionovela
Slug: radionovela-hasta-el-amor-felipe-segundo-capitulo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/carcel-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Tatiana es la hermana menor de Felipe, un joven muchacho víctima de un falso positivo judicial. Ella junto con su padre tendrán que enfrentar las distintas arremetidas en contra de su familia, tras la captura de su hermano.

En este segundo capítulo Tatiana y su padre deberán tomar una difícil situación tras evidenciar las persecuciones a su familia. Felipe continúan recluido en la cárcel, mientras que su proceso sigue enfrentando dilaciones.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F293997091517857%2F&amp;width=600&amp;show_text=false&amp;appId=1237871699583688&amp;height=338" width="600" height="338" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Ver mas: [Radionovela "Hasta el amor Felipe", primer capítulo](https://archivo.contagioradio.com/radionovela-hasta-el-amor-felipe-primer-capitulo/)

 
