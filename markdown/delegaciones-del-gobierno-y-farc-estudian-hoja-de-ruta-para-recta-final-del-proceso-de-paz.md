Title: Delegaciones del gobierno y FARC estudian hoja de ruta para recta final del proceso de paz
Date: 2016-03-23 12:10
Category: Nacional
Tags: FARC, Gobierno Nacional, proceso de paz
Slug: delegaciones-del-gobierno-y-farc-estudian-hoja-de-ruta-para-recta-final-del-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/amnistia-proceso-de-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Tiempo 

###### [23 Mar 2016]

La demora en la firma del acuerdo sobre víctimas y la fuerte discusión en torno a los acuerdos para concentración de los integrantes de las FARC en varias zonas del país, así como los desacuerdos por la verificación de la dejación de armas obligan a **la postergación de la firma de un cese bilateral de fuego** definitivo en la mesa de conversaciones de la Habana.

Sin embargo, en este 23 de marzo, **las delegaciones de paz del gobierno nacional y las FARC se reúnen con el objetivo de trazar una nueva hoja de ruta** que permita trabajar y desarrollar más rápidamente los puntos finales que quedan en la agenda de negociación para firma de la paz lo más pronto posible.

Pablo Catatumbo, integrante de la delegación de paz de las FARC confirmó que hoy finalmente no se firmará ninguno de los puntos que quedan pendientes, pero hizo un llamado a que no se le pida a la guerrilla que se aleje de la población civil: “Hoy no se hará un anuncio de cese bilateral, pero esperemos a que culmine la reunión que están desarrollando en el palco. **Siempre hemos estado cerca a la población civil, no es concebible que se le pida hoy a las Farc que esté en lugares inaccesibles a la población**, son 50 años de confrontación y siempre hemos estado en contacto con la población civil”.

Por el momento, el país se encuentra a la espera de la reunión que se desarrolla en La Habana con mediación del país garante Noruega.

En Desarrollo…
