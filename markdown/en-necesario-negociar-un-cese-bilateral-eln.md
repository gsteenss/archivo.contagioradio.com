Title: En necesario negociar un cese bilateral: ELN
Date: 2020-04-03 11:48
Author: CtgAdm
Category: Nacional
Slug: en-necesario-negociar-un-cese-bilateral-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/pablo-beltrán-ELN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ELN - Pablo Beltrán/ Cortesía

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde La Habana, Cuba, Pablo Beltrán, jefe de la Delegación de paz del ELN se refirió al cese unilateral activo decretado en el territorio nacional, a su vez el comandante **reveló información sobre los acercamientos que si existieron entre el equipo negociador de la guerrilla y el senador Álvaro Uribe.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Como respuesta a la petición de las [Naciones Unidas](https://twitter.com/ONU_es) de dar un alto a todos los conflictos globales y enfocarse en combatir al Covid-19, el ELN anunció el pasado 30 de marzo el cese unilateral activo durante un mes en todo el territorio nacional hasta el primero de mayo. Al respecto Beltrán señala que **su dirección acogió el llamado hecho por la ONU el papa Francisco, las comunidades y múltiples organizaciones en toda Colombia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pablo Beltrán afirma que la propuesta del ELN es que después del 1 de mayo, fecha en que concluye el cese unilateral activo, se extienda al Gobierno una propuesta para extender este alto a la violencia y convertirlo en un cese al fuego bilateral, algo de lo que también dependerá el avance del Covid-19 en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Esto quiere decir que en el ELN no somos sordos, escuchamos a la gente, sabemos de la profundidad de la crisis humanitaria y sanitaria y esto es un gesto humanitario", expresó el jefe negociador, "invitando al Gobierno y a las élites gobernantes que asuman sus responsabilidades". [(Lea también: Comunidades piden cese al fuego y atención del Gobierno a problemas en los territorios)](https://archivo.contagioradio.com/comunidades-piden-cese-al-fuego-y-atencion-del-gobierno-a-problemas-en-los-territorios/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Beltrán también se refirió a la designación de Carlos Velandia y Francisco Galán, como gestores de paz y a la captura del también exnegociador del ELN, Juan Carlos Cuellar, privado de la libertad desde finales del 2019. [(Lea también: Cese unilateral al fuego del ELN, una oportunidad para la paz)](https://archivo.contagioradio.com/cese-unilateral-al-fuego-del-eln-una-oportunidad-para-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Beltrán relata que el 1 de diciembre, Juan Carlos Cuellar fue mandado llamar por el expresidente Álvaro Uribe con el fin de hablar y encontrar una forma de retomar las negociaciones, sin embargo, al final de mes, después del último de cuatro reunones fue capturado en Cali.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Esa es la sordera y la falta de voluntad del Gobierno para una solución política"**, afirma el comandante, quien sugiere que no existen avances en una mesa de diálogos por decisión de Estados Unidos, que buscaría justificar la existencia de un aparato militar y paramilitar para presionar en Venezuela.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/868143383599289/?__xts__%5B0%5D=68.ARDJpTCswIzBG0oni-2xMxF4x7molS4iGmvcmmllx2k6l7AaZ3W8QbE3OxGEnRFDPboMv2APE_hXG-ssAAn-sP5twpj7TgM4R4HZYeROPSBu_qWcs_gIdiMR3jcXXm16NMiT8EiWDqiCBxHn-AaYldcbc_lYKmSumNvMqTna5JPbazIbw97hawONOGSabWtbnKVn-NlpAQ706vUG1pfXyQyoZZekoaoqQZ7yeMSY9VJw13An54r_495oYFTncatFthb5F0hPNH3pU-tiKZehNl4WuZt2f0rkz3zeq0stbNjJboXiR3Hsni-KRAY_GTCe6bRtmYz1vILa7BTULeGFysGzy_c\u0026__tn__=-R","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/868143383599289/?\_\_xts\_\_%5B0%5D=68.ARDJpTCswIzBG0oni-2xMxF4x7molS4iGmvcmmllx2k6l7AaZ3W8QbE3OxGEnRFDPboMv2APE\_hXG-ssAAn-sP5twpj7TgM4R4HZYeROPSBu\_qWcs\_gIdiMR3jcXXm16NMiT8EiWDqiCBxHn-AaYldcbc\_lYKmSumNvMqTna5JPbazIbw97hawONOGSabWtbnKVn-NlpAQ706vUG1pfXyQyoZZekoaoqQZ7yeMSY9VJw13An54r\_495oYFTncatFthb5F0hPNH3pU-tiKZehNl4WuZt2f0rkz3zeq0stbNjJboXiR3Hsni-KRAY\_GTCe6bRtmYz1vILa7BTULeGFysGzy\_c&\_\_tn\_\_=-R

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph {"align":"justify"} -->

Con respecto a Carlos Velandia y Francisco Galán, Beltrán resalta que la decisión del Gobierno de nombrarlos como gestores tendrá algún propósito que por ahora desconoce, "razones tendrán, tendrán planes con ellos, les darán cargos, no lo sé, hay que esperar qué van a hacer, es adelantarme a hacer un juicio". concluye expectante.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
