Title: Acuerdo de la COP21 es “inhumano, engañoso y esquizofrénico”
Date: 2015-12-14 17:35
Category: El mundo, Entrevistas
Tags: Acuerdos COP, Calentamiento global, cambio climatico, combustibles fósiles, COP 21, Ecologistas en Acción, fracking, París, Tom Kucharz
Slug: acuerdo-de-la-cop21-es-inhumano-enganoso-y-esquizofrenico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/cop21-protestas-20151201125245.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [impactoevangelistico.net]

<iframe src="http://www.ivoox.com/player_ek_9723593_2_1.html?data=mpyflZqdd46ZmKiak5mJd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhcTpxtfR0ZDIqYzgwpCwsbWWdYzZ1JCSp5eJfJGZmqjW0M3ZscLi0IqfpZDJssjVhqigh6eVs9TjjN6Yx9jVucru0JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Tom Kucharz, Ecologistas en acción] 

###### [14 Dic 2015]

**“Debemos estar muy alarmadas y alarmados por tanta autocomplacencia que se da con este acuerdo”,** señala Tom Kucharz, integrante de Ecologistas en Acción, refiriéndose al acuerdo alcanzado en el marco de la Conferencia de las Partes en París, COP 21.

Pese a que la mayoría de los medios de comunicación señalan con euforia el acuerdo de la COP21 como algo “histórico”, para Tom, no es más que un texto que en realidad es “inhumano, engañoso y esquizofrénico”, teniendo en cuenta que para él, los gobiernos “han cruzado todas las líneas rojas marcadas por la ciencia y la sociedad civil” porque el acuerdo **acepta implícitamente que la temperatura del mundo pueda aumentar en los próximos años entre 2,5 ºC y 3,7 ºC.**

Así mismo, asegura que con las medidas a tomar de manera voluntaria por los gobiernos, no se logrará contener el aumento de la temperatura por debajo de los 2 ºC, ya que los países no quisieron acordar obligaciones de reducción de emisiones más ambiciosas para limitar el calentamiento a 1,5 ºC, lo que a su vez **significa que  nuevamente se juega a favor de los intereses del modelo económico actual que implica “la muerte, el desplazamiento y el sufrimiento de millones de personas”,** dice el integrante de Ecologistas en Acción.

A su vez, el acuerdo es “inhumano”, debido a que “ festeja las limosnas que se dan a los países más pobres para financiar las medidas de adaptación al cambio climático con el Fondo Verde para el Clima”, en cambio, es injusto porque impide anclar el derecho a compensación por daños y pérdidas, pues Estados Unidos y la Unión Europea lograron incluir una demanda con la cual los países más afectados por el aumento de la temperatura deberán renunciar su derecho legal a demandar a otros países, como EE.UU, responsables de la contaminación en el mundo.

Aunque cientos de organizaciones enviaron una solicitud el pasado 11 de diciembre a la COP21, para que los gobiernos prohibieran la práctica del fracking, lo cierto, **es que en el acuerdo, ni siquiera aparece la palabra “combustibles fósiles**”, es decir, que se hace todo lo posible por proteger los intereses de las empresas que han ocasionado la aceleración del aumento de la temperatura.

Finalmente Kucharz asegura que con las movilizaciones sociales por parte de más de 10 mil personas han sido muy importantes ya que **“se ha demostrado que la sociedad civil no debe esperar nada de sus gobiernos”,** de manera que la  esperanza está en el empoderamiento de los cambios impulsados desde la ciudadanía frente al calentamiento global.
