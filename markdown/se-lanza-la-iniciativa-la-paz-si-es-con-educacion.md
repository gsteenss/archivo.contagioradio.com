Title: Se lanza la iniciativa 'La Paz Sí es con Educación'
Date: 2016-08-19 15:27
Category: Nacional, Paz
Tags: educacion, paz, proceso de paz
Slug: se-lanza-la-iniciativa-la-paz-si-es-con-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/universidades-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [19 Ago 2016] 

Este viernes estudiantes, profesores e investigadores lanzan la **iniciativa 'La Paz SÍ es con educación'**, en el marco del encuentro distrital por el derecho a la educación, convocado por el Frente Amplio por la Educación, los Derechos y la Paz, integrado por jóvenes de colegios y universidades, así como maestros, madres y padres de familia y lideres de procesos organizativos y educativos de Bogotá.

La iniciativa comprende ejercicios de pedagogía de paz en universidades y colegios, y la **construcción de un programa educativo de cara al postacuerdo**, con el fin de lograr espacios de participación en los escenarios de implementación que se habiliten tras los acuerdos de paz con las FARC-EP y el ELN.

En el encuentro en el que se lanzará esta iniciativa, se realizará un diagnóstico de los **impactos del plan de desarrollo Bogotá 2016-2020** en el sector educativo, para construir propuestas alternativas que propicien escenarios de discusión, debate y construcción colectiva en torno a la educación en la ciudad.

<iframe id="doc_8730" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/321979837/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-lGNjldgrqzI6XICZpN5N&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
