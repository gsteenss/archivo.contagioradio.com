Title: De no negociar en Valle del Cauca 50 mil indígenas llegarían a Bogotá
Date: 2016-06-03 14:10
Category: Paro Nacional
Tags: Cumbre Agraria, Minga Nacional, ministerio de defensa, Paro Nacional
Slug: la-comunidad-y-los-medios-regionales-saben-que-la-fuerza-publica-mato-a-los-indigenas-onic
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Minga-Nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cumbre Agraria 

###### [3 Jun 2016] 

Al quinto día de la Minga Nacional, ya se registran 3 indígenas muertos, alrededor de 150 manifestantes heridos y 31 agentes de la fuerza pública lesionados en el marco de la represión a las protestas. Aunque desde el Ministerio de Defensa se pone en duda la situación que generó la muerte de dos indígenas al norte del Cauca.

Juvenal Arrieta secretario general de la Organización Nacional Indígena de Colombia, asegura que la comunidad y los medios de la región saben que se trató de unos asesinatos ocasionados por el ESMAD “Acá la comunidad y los medios regionales saben que los indígenas fueron asesinados por la fuerza pública, el ministro no tiene como demostrarle al país que pudo haber sido un infiltrado, así mismo sucedió con el indígena muerto en la vía delfina vía Buenaventura”.

El hecho sucedió cuando la policía llegó a evacuar a los indígenas, en ese momento la guardia indígena se concentró sobre el paso vehicular y se empezó la enfrentación y al no desplazarse a los comuneros, la policía empezó a disparar asesinando a un indígena y dejando a cuatros más heridos, pero horas después muere uno de ellos, explica Arrieta.

“La represión por parte del Ministerio de Defensa es muy organizada”, dice el indígena, quien añade que la actuación conjunta del ESMAD y el Ejército, es una clara violación al Derecho Internacional Humanitario, pues cuando los militares son los que atienden la protesta, se le está dando un trato de guerra a la movilización social.

**“El gobierno sigue mintiendo”**

Juvenal Arrieta, asegura que lo único que buscan es que el gobierno sea claro frente a lo que se puede cumplir o no, y que se había pactado desde el 2013. Además resalta que el gobierno ha reducido todo a un solo punto  de los acuerdos que tienen que ver con recursos por 250 mil millones de pesos de parte del Ministerio de Agricultura, un punto del cual solo se ha cumplido un 40% mientras que el resto de los acuerdos no se ha cumplido prácticamente nada, “los 8 puntos de la Cumbre Agraria están congelados y de los 29 acuerdos de 2013 con la Minga Indígena solo se sacaron 3 y 26 están congelados”.

Así mismo, se asegura que la mayoría de los puntos exigidos no implican la solicitud de recursos económicos, en realidad, se trata de medidas administrativas e institucionales y políticas que tienen que ver con la voluntad del gobierno.

Las comunidades indígenas, que este viernes están de luto, han decidido que si el sábado no se establece una ruta de negociación en Cali o en las carreteras, 50 mil indígenas de todas las regiones del país se movilizarían hacia Bogotá desde el lunes, asegurando que a medida que pasa el tiempo, la Minga continuará creciendo.

<iframe src="http://co.ivoox.com/es/player_ej_11773606_2_1.html?data=kpakmZiadJehhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncavp18rbw9GPhdPmysrhw5CRb7DCqqiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
