Title: Ríos Vivos denuncia intimidaciones por parte de EPM y autoridades de Ituango
Date: 2018-09-10 16:54
Author: AdminContagio
Category: Ambiente, DDHH
Tags: EPM, Hidroituango, Ituango, Rios Vivos
Slug: rios-vivos-denuncia-ituango
Status: published

###### [Foto: @ISAZULETA] 

###### [10 Sept 2018] 

La organización ganadora del **Premio Nacional a la Defensa de los Derechos Humanos** en la categoría experiencia o proceso colectivo del año, **Movimiento Ríos Vivos,** denunció el empadronamiento de sus integrantes, la persecución y estigmatización por parte del Alcalde, el Secretario de Gobierno y el Sacerdote de Ituango.

**Isabel Cristina Zuleta,** vocera de Rios Vivos sostuvo que de las marchas realizadas el fin de semana salieron "más amenazados" de lo que estaban por parte del secretario de Gobierno, el Alcalde y el sacerdote de Ituango. Aunque la movilización se realizaba de forma pacífica en respaldo de los damnificados por el desastre de Hidroituango, quienes integraban la caminata sufrieron persecución por parte de las autoridades locales.

### **Perseguidos, empadronados, señalados y retenidos** 

Zuleta denunció que desde su llegada al Municipio, los **500** integrantes de Ríos Vivos fueron empadronados por parte de las autoridades del lugar, quienes además los requisaron. Posteriormente, se les impidió la llegada al coliseo en el que se encontraban las **27 familias damnificadas por Hidroituango,** que además "no han recibido atención por parte de la administración municipal".

Ante ese hecho, el Movimiento solicitó una reunión con **Hernán Álvarez Uribe, alcalde de Ituango,** para pedirle que explique por qué no ha brindado las ayudas necesarias a las 27 familias, a pesar de que la Gobernación de Antioquia ha afirmado que ya fueron entregados estos recursos. Sin embargo, fueron atacados por **Francisco Castro Gutiérrez**, secretario de Gobierno del Municipio, quien los culpo por la ocupación que hacen los damnificados del **Coliseo Jaidukamá,** y el reclutamiento de los niños que allí habitan, por cuenta del conflicto armado.

### **Sacerdote de Ituango usa el púlpito contra Ríos Vivos** 

Luego, pidieron al sacerdote del municipio refugio en la iglesia, porque la policía había acordonado el Coliseo, pero **el Párroco acusó a Ríos Vivos de tomarse el templo.** Zuleta denunció además que el cura sigue haciendo homilías hablando en contra del Movimiento y poniendo a la comunidad en su contra. (Le puede interesar: ["La Gobernación de Antioquia acepta que tragedia de Hidroituango fue un error de construcción"](https://archivo.contagioradio.com/tragedia-hidroituango-fue-error-de-construccion/))

La vocera de Ríos Vivos sostuvo que tras salir de Ituango rumbo a Medellín, por la única carretera que conecta ambas poblaciones y que pasa cerca a Hidroituango, fueron retenidos por funcionarios de **Empresas Públicas de Medellín (EPM)**, quienes impidieron su tránsito por la zona por un supuesto derrumbe, aunque otros vehículos sí podían pasar. En horas de la tarde, optaron por cruzar en ferri el **río Cauca** para llegar a la capital de Antioquia, pero EPM dio la orden a la embarcación de no dejarlos pasar.

Por estos hechos, Zuleta pidió que la iglesia se pronuncie a favor de la protección de los líderes y defensores de derechos humanos, **se haga seguimiento al proyecto Hidroituango por parte de la Fiscalía y la Procuraduría**; y anunció que buscarán "las condiciones para nuestra protesta", y para que se protejan los derechos de quienes están refugiados en el Coliseo Jaidukamá.

<iframe id="audio_28460231" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28460231_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]

<div class="osd-sms-wrapper">

</div>
