Title: 10 reformas que necesita el sistema de salud en Colombia
Date: 2018-08-31 19:55
Category: DDHH, Nacional
Tags: Derecho, Ley 100, Reformas, Salud
Slug: 10-reformas-sistema-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/sistema-salud-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [31 Ago 2018] 

En el marco de la Cátedra de Bioética que se adelanta en la universidad El Bosque de Bogotá, diferentes invitados señalaron que en Colombia no hay un goce efectivo del derecho a la salud y propusieron **10 reformas** con las que se podría remediar esta situación. (Le puede interesar: ["El derecho a la salud: Propuestas y significados"](https://www.youtube.com/watch?v=q_QucebnWmw))

En el evento participaron el Presidente del Tribunal de Ética de Cundinamarca, **Edgar Montoya**; el Presidente de la Junta Médica Nacional, **Roberto Baquero;** el Coordinador del grupo de Sexualidad, Derechos Sexuales y Reproductivos del Ministerio de Salud, **Ricardo Luque, **el Dir General de la Fundación Santafé, **Henry Gallardo y** Dir de Departamento de Bioética de El Bosque, **Jaime Escobar Triana.**

Para el Doctor Montoya, existen una serie de barreras de tipo administrativo para lograr acceder al derecho a la salud, desde el proceso de llegada a urgencias, pasando por el modelo de atención e incluso, en el traslado mediante el sistema de ambulancias. En ese sentido, pidió que se garantice que los hospitales de primer nivel tengan la capacidad para decidir los tratamientos de los pacientes, y de esa forma descongestionar la sala de urgencias y preservar la vida.

Por su parte, Roberto Baquero, sostuvo que aunque la **Ley 1751 de 2015, o Ley Estatutaria** de la salud fue una buena oportunidad para que sea efectivamente un derecho, lo cierto es que "no ha sido reglamentada". Además el gasto en salud es bajo respecto al promedio en Latinoamérica, porque representa el **6,81% del PIB** del país, mientras la cobertura  nacional "es de papel".

### **"El fallido modelo de la Ley 100 no es un sistema de salud sino un modelo de atención"** 

Frente a esta afirmación, Baquero propone un Decálogo para mejorar el sistema de salud en Colombia:

1.  Hacer cumplir la Ley Estatutaria de la salud.
2.  Devolverle la rectoría al Estado con la dirección y coordinación de la política en salud, pero con una alta participación de la sociedad civil.
3.  Garantizar la atención a la población generando regiones solidarias que organicen la prestación de los servicios en salud e intervención en los determinantes sociales de la salud.
4.  Impulsar el fortalecimiento de la red pública de servicios, con énfasis en los hospitales universitarios.
5.  Fortalecer el Fondo Único Financiero.
6.  Modificar las estructuras de las juntas directivas de los Hospitales públicos para que dejen de ser fortines políticos.
7.  Que haya una política pública para el recurso humano en salud que garantice la dignificación, estabilidad laboral, reconocimiento y remuneración para los profesionales de la salud.
8.  Independencia de la Super Intendencia de Salud, respecto del Ministerio de Salud.
9.  Control de precios e historial de medicamentos.
10. Implementar un sistema integral de información en salud que garantice la confidencialidad.

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
