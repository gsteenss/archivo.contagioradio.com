Title: Indígenas Awá denuncian presencia de grupos armados en sus territorios
Date: 2017-09-08 15:18
Category: DDHH, Nacional
Tags: Comunidad indígena Awá, Violencia armada
Slug: indigienas-awa-denuncian-presencia-de-grupos-armados-en-sus-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Nasa-e1502385039236.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

###### [08 Sept 2017] 

Indígenas de la comunidad Awá, en Nariño, denunciaron el surgimiento y presencia de nuevos grupos armados que están amedrantando a los habitantes, **con acciones como enfrentamiento entre estructuras ilegales, el reclutamiento a menores, la restricción a la movilidad y el intento por controlar la vía Pasto – Tumaco**, al igual que la zona fronteriza con Ecuador, sin que se tomen medias por parte de las autoridades.

De acuerdo con las denuncias de las comunidades indígenas, desde el pasado 15 de agosto en sus territorios llegó información en donde se les avisaba del surgimiento y presencia de nuevos grupos armados denominados **Ejército Revolucionario del Pueblo y Guerrillas Unidas del Pacífico**. (Le puede interesar: ["Sí AGC se acogen a la justicia Chocó, Antioquia y Urabá serían los más beneficiados"](https://archivo.contagioradio.com/si-agc-se-acogen-a-la-justicia-choco-antioquia-y-uraba-seria-los-mas-beneficiados/))

Posteriormente el 29 de agosto habitantes señalaron que se presentaron enfrentamientos en su territorio, entre estos grupos, en los resguardos que **se encuentran ubicados en las zonas fronterizas con Ecuador, perteneciente al municipio de Tumaco**.

De igual forma, los indígenas manifestaron que en los resguardos de Piedra Sellada Yarumal, Hoja la Turbia, El Gran Sábalo, Kejuambí Feliciana, Chinguirito Mira, entre otros, se estaría presentando restricción a la movilidad de los pobladores, amenazas de **reclutamiento infantil a los jóvenes de las comunidades** y de llenar las vías principales de minas antipersona, si no se acata lo que estos grupos le ordenen a los habitantes.

Además, los indígenas expresaron que en los resguardos ha existido presencia del Ejército de Liberación Nacional, **que aumenta los riesgos de confrontación con otros grupos y los temores de las comunidades. **(Le puede interesar: ["Paramilitares asesinan a dos integrantes de la comunidad indígena Wounnan"](https://archivo.contagioradio.com/paramilitares-asesinan-a-dos-intengrantes-de-la-comunidad-wounnan/))

Frente a esta situación, las comunidades indígenas Awá, le exigen al gobierno que tome medidas urgentes para salvaguardar la vida de los pobladores y que garantice la seguridad en los territorios, así como la integridad, vida y pervivencia de los pueblos Awá, en ese mismo sentido, le están pidiendo a la Defensoría del Pueblo que tenga una presencia inmediata en el territorio, mientras se toman medidas por parte del Estado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
