Title: Organizaciones repudian liberación de alias “jj” implicado en crímenes contra Jineth Bedoya
Date: 2015-06-03 17:18
Category: Mujer, Nacional
Tags: Alejandro Cárdenas, Fiscalía General de la Nación, FLIP, FLIP Federación para la libertad de prensa Colombia, Jineth Bedoya, Ley Rosa Elvira Cely, Paramilitarismo
Slug: organizaciones-repudian-liberacion-de-alias-jj-implicado-en-crimenes-contra-jineth-bedoya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/jineth_bedoya-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elpais 

###### [3 Jun 2015] 

Cerca de una veintena de organizaciones que hacen parte de la campaña “**saquen mi cuerpo de la guerra”,** repudiaron la decisión de las autoridades judiciales de otorgar la libertad de **Alejandro Cárdenas Orozco, alias “JJ”,** quien es uno de los implicados en los crímenes cometidos contra la periodista Jineth Bedoya Lima, ocurridos el 25 de Mayo del año 2000.

Según las organizaciones *“este hecho es una prueba fehaciente de la existencia de un ambiente de impunidad permanente en Colombia y la ausencia de una política de tolerancia cero en materia de violencia contra las mujeres por parte del Estado Colombiano*”. Agregan que durante estos 15 años han sido testigos de la **falta de voluntad de investigar y sancionar a los responsables de estos hechos**.

Por su parte, la **Fundación para la Libertad de Prensa –FLIP**, afirmó que este es un retroceso en la búsqueda de la justicia y “*un mensaje de permisividad y no castigo frente a las agresiones contra la prensa*”.

Jineth Bedoya fue secuestrada por paramilitares en la cárcel “La Modelo” de Bogotá el 25 de mayo del año 2000,  fue torturada física, psicológicamente y también abusada sexualmente por sus captores. Tras **15 años de ocurridos los hechos no hay condenas y el crimen sigue en la impunidad.**

Recientemente Bedoya fue parte de una de las **delegaciones de víctimas escuchadas por las FARC y el gobierno nacional** en una de las sesiones de visita de las víctimas a la mesa de conversaciones, sin embargo, a pesar de los anuncios de compromiso con ellos y ellas por parte del gobierno nacional, hoy no hay hechos concretos y reales de esos compromisos.

Bedoya también manifestó su rechazo a la decisión de preclusión de la investigación contra alias “JJ” y afirmó en su cuenta de Twitter que "*Mientras apoyaba Ley de Feminicidio, @FiscaliaCol ordenó libertad de uno de mis violadores. Tengo el corazón golpeado y la dignidad intacta*", en referencia a la recientemente aprobada **ley “Rosa Elvira Cely”.**
