Title: Así será el procedimiento de salida de menores de las FARC
Date: 2016-09-06 17:44
Category: Nacional, Paz
Tags: Conpaz, FARC, niños y niñas, ONU, Unicef
Slug: asi-sera-el-procedimiento-de-salida-de-menores-de-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/niños-farc-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: cosmocisionnoticias] 

###### [06 Sep 2016]

En un nuevo comunicado las FARC y el gobierno colombiano dieron a conocer el mecanismo de salida de los menores de edad que están haciendo parte de sus filas. Según el comunicado el CICR recibirá a los y las menores, realizará una primera valoración médica junto a dos organizaciones civiles, luego se dará el traslado hacia lugares definidos por la UNICEF y posteriormente las **organizaciones civiles, como CONPAZ,** darán algunas recomendaciones a defensores de familia que también acompañarán el proceso.

En el [comunicado conjunto 96](https://archivo.contagioradio.com/ninos-y-ninas-de-las-farc-comenzaran-su-proceso-de-salida-el-10-de-septiembre/), el **gobierno de Colombia y las FARC afirmaron que están listos los mecanismos para la salida de menores en las filas de esa guerrill**a y que dicho proceso comenzará el próximo 10 de Septiembre. Recientemente las FARC afirmaron en otro comunicado que esa salida no se había iniciado porque correspondía a un acuerdo de las dos partes, esto ante las críticas de que supuestamente esa guerrilla estaba impidiendo la salida de menores de sus filas.

A continuación el comunicado conjunto 97...

##### "COMUNICADO CONJUNTO \# 97

##### La Habana, 6 de septiembre de 2016

##### El Gobierno Nacional y las FARC-EP, de conformidad con el comunicado conjunto \# 96 del 2 de septiembre en el que acordamos que a partir del 10 de septiembre se iniciará el proceso de salida de los menores de edad de los campamentos de las FARC- EP, informamos que hemos acordado el protocolo de salida y plan transitorio de acogida de niños, niñas y adolescentes que salgan de los campamentos de las FARC-EP.

##### Para ello se conformará un equipo de traslado desde el punto de salida, compuesto por personal del Comité Internacional de la Cruz Roja - CICR- que coordinará las operaciones, y dos delegados de las organizaciones sociales que hacen parte de la Mesa Técnica creada por el Comunicado \# 70 del pasado 15 de mayo.

##### Se preverá la disponiblidad de dos defensores de familia asignados para estos lugares transitorios, para resolver cualquier imprevisto que requiera su intervención.

##### En el lugar de salida, el CICR valorará preliminarmente el estado de salud física y mental de los menores de edad; verificará sus identidades y su información personal básica de lo cual dejará constancia en el acta de salida.

##### Terminado este momento, el equipo de traslado hará entrega de los niños, niñas y adolescentes al equipo de recepción conformado por representantes de UNICEF que, con el apoyo de OIM, operarán los lugares transitorios de acogida previamente definidos por UNICEF.

##### Allí se hará una segunda valoración médica y psicológica de cada niño, niña o adolescente con el fin de identificar cualquier situación que amerite atención especializada. Enseguida comenzarán las actividades preparatorias para la fase de reincorporación, reparación integral e inclusión social.

##### Por otro lado, se conformará un comité técnico de apoyo que tendrá como objetivo la elaboración de observaciones y conceptos dirigidos a los defensores de familia encargados del restablecimiento de los derechos de estos niños, niñas y adolescentes.

##### La Comisión de Implementación, Seguimiento, Verificación del Acuerdo Final de Paz y de Resolución de Diferencias, acordada el pasado 24 de agosto, realizará el seguimiento a la ejecución de las acciones previstas en el protocolo y el plan transitorio de acogida.

##### Invitamos a la Oficina de la Representante Especial del Secretario General de las Naciones Unidas para la Cuestión de los Niños y los Conflictos Armados; Centro Carter; Llamamiento de Ginebra; Coalición contra la Vinculación de Niños, Niñas y Jóvenes al Conflicto Armado en Colombia -Coalico- ; Asociación Nacional de Zonas de Reserva Campesina -Anzorc- y Comunidades Construyendo Paz en los Territorios -Conpaz-, a apoyar acompañar y /o hacer veeduría a este protocolo y el plan transitorio de acogida.

##### El CICR será el único vocero encargado de informar públicamente sobre los avances en el cumplimiento de este protocolo, en esta primera fase de salida de menores de edad de los campamentos de las FARC- EP"
