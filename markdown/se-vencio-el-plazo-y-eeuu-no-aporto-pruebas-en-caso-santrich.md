Title: Se venció el plazo, EEUU no aportó pruebas en caso Santrich
Date: 2019-01-28 18:25
Author: AdminContagio
Category: Nacional
Tags: EEUU, extradicion, JEP, santrich
Slug: se-vencio-el-plazo-y-eeuu-no-aporto-pruebas-en-caso-santrich
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Jesus-Santrich-6-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 28 ene 2019 

A través de  un comunicado de prensa, la Jurisdicción Especial de Paz informó que tras no ser adjuntado el material probatorio por parte del Estados Unidos para definir la situación del integrante del partido FARC Seuxis Pausias Hernández alias Jesús Santrich, el tribunal procederá a continuar con el trámite de garantía de no extradición.

Hoy 28 de enero a las 5:30 de la tarde, término en que se cumplió el plazo para la presentación de las pruebas, la JEP anunció además que la sección de revisión procedera a correr traslado para alegatos en el término de 5 días hábiles.

En desarrollo....

![santrich](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/DyCLpcbWoAIAkot-565x800.jpg){.alignnone .size-medium .wp-image-60710 .aligncenter width="565" height="800"}
