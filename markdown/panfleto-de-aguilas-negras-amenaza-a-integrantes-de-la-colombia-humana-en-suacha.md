Title: Panfleto de Águilas Negras amenaza a integrantes de la Colombia Humana en Suacha
Date: 2020-09-08 14:59
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #AguilasNegras, #ColombiaHumana, #HeinerGaitán, #Paramilitares, #Suacha
Slug: panfleto-de-aguilas-negras-amenaza-a-integrantes-de-la-colombia-humana-en-suacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Soacha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado 7 de septiembre, un panfleto firmado por un grupo que se autodenomina como las Águilas Negras Bloque Capital, llegó al correo de 16 personas, entre quienes se encuentran líderes sociales, ediles y concejales de Suacha, todos integrantes de la Colombia Humana. En la misiva se declara **"objetivo militar" a estas personas y se anuncia una "limpieza" en el territorio.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las personas que recibieron ese panfleto son los líderes sociales Marco Aurelio Caicedo, Carmenza Vargas, Carlos Palacio, Ernesto Morales, Sandra Morales, Fabian Morales, Juan Pinto, Mauricio Reyes, Nicolas Ricardo, Samuel Roberto, Alcides Alba, Ricardo Ibáñez; los ediles Luis Agudelo, Jorge Rodrígues e Ignacio Roya y el concejal Heiner Gaitán.

<!-- /wp:paragraph -->

<!-- wp:heading -->

El Panfleto de las autodenominadas Águilas Negras
-------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El texto señala que su accionar hace parte de una "campaña nacional" que iniciaría en Suacha. Asimismo declaran objetivo militar a organizaciones de la comunidad LGTBI, [organizaciones de derechos humanos y organizaciones de vendedores ambulantes.](https://archivo.contagioradio.com/reclutamiento-forzado-pobreza-e-ineficacia-oficial-las-tres-amenazas-para-jovenes-de-soacha/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, en la amenaza de muerte, aseguran que **"si no los mata el coronavirus, los materemos nosotros"** y añaden que a partir de la fecha "ejecutaremos la orden a través de nuestros bloques".

<!-- /wp:paragraph -->

<!-- wp:heading -->

Colombia Humana rechaza las amenazas a sus integrantes en Suacha
----------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En un reciente comunicado de prensa, el partido político Colombia Huma-UP rechazó las amenazas y expresó que las 16 personas intimidadas, tienen liderazgos en Suacha. Todos ellos haciendo **"ferrea oposición al gobierno local en cabeza del alcalde Juan Carlos Saldarriaga,** denunciando abusos y arbitrariedades".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente señalaron que "es preocupante la estigmatización de la política de izquierda y de oposición en el municipio". Situación que se ha recrudecido desde las denuncias y debates promovidos por el concejal Heiner Gaitán.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, Gaitán exigió al alcalde Saldarriaga un pronunciamiento frente esta situación y el **reconocimiento del accionar de grupos armados "herederos, en muchas ocasiones, de las estructuras paramilitares**, que siguen hoy ejerciendo control territorial en Suacha, reclutando jóvenes y lavando dinero en la economía, tanto formal como ilegal".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Colombia Humana afirmó que realizaran una denuncia ante la Fiscalía por este hecho. Sin embargo resaltaron la urgencia porque la institucionalidad [actúe frente a estos grupos paramilitares](https://www.facebook.com/GaitanParraHeiner/videos/712683339589141)y ponga fin al lenguaje guerrerista y belicoso.

<!-- /wp:paragraph -->
