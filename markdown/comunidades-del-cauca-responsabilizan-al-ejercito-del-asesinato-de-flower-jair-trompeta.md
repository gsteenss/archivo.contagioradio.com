Title: Comunidades del Cauca responsabilizan al Ejército del asesinato de Flower Jair Trompeta
Date: 2019-10-29 08:07
Author: CtgAdm
Category: DDHH, Nacional
Tags: Agresiones fuerza pública, Cauca, Corinto, ejecuciones extrajudiciales en Colombia
Slug: comunidades-del-cauca-responsabilizan-al-ejercito-del-asesinato-de-flower-jair-trompeta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/ejercito.militares.militar.soldado.soldados.afp_0_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

Comunidades campesinas de Corinto, Cauca, denuncian que integrantes del Ejército Nacional habrían retenido, torturado y posteriormente asesinado al joven **Flower Jair Trompeta Pavi**, quien según relata su familia, fue defensor de derechos humanos e integrante de la Asociación de Trabajadores Pro-Constitución Zonas de Reserva Campesina de Caloto, Cauca.

Según sus familiares, el joven fue torturado al poner su mano en una máquina despulpadora para luego ser asesinado por miembros del Ejército, aseguran sus seres queridos que era un civil y "que no tenia nada que ver en el conflicto armado", información adicional indica que alrededor de las 10:30 de la mañana de este 28 de octubre en la vereda La Laguna, lugar donde sucedieron los hechos, se escucharon disparos de arma de fuego y el sobrevuelo de helicópteros.

Flower Jair era integrante de la Asociación de Trabajadores Pro-Constitución Zonas de Reserva Campesina de Caloto (ASTRAZONACAL), organización filial de la Federación Nacional Sindical Unitaria Agropecuaria FENSUAGRO – CUT, de la Asociación Nacional de Zonas de Reserva Campesina  (ANZORC), del Proceso de Unidad Popular del Suroccidente Colombiano  (PUPSOC ) y de la Coordinación Social y Política Marcha Patriótica Cauca. [(Lea también: Con pintura, Ejército pretende ocultar responsabilidad de altos mandos en 'Falsos Positivos')](https://archivo.contagioradio.com/con-pintura-ejercito-pretende-ocultar-la-verdad-de-los-falsos-positivos/)

Noticia en desarrollo....

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]

 
