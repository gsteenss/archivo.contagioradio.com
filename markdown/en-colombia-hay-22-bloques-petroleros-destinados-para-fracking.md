Title: En Colombia hay 22 bloques petroleros destinados para fracking
Date: 2016-05-06 15:53
Category: Ambiente, Nacional
Tags: Camilo Prieto, fracking, Ministerio de Ambiente, Ministerio de Minas
Slug: en-colombia-hay-22-bloques-petroleros-destinados-para-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/fracking-e1460586646301.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Brugos Digital] 

###### [6 May 2016] 

Hace algunos días el ministro de Minas y Energía Germán Arce, aseguró que en Colombia se puede hacer fracturamiento hidráulico o fracking de manera segura y responsable, pero lo cierto es que **en Colombia aún no existen los estudios óptimos para dar ese tipo de declaraciones que más bien son “irresponsables”,** como lo expresa Camilo Prieto, vocero del Movimiento Ambientalista Colombiano.

A pesar de que Agencia Nacional de Licencias Ambientales aseguró que no hay contratos para fracking en Colombia, si hay evidencia de contratos aprobados como en San Martín, César. Se conoce que actualmente, **la Agencia Nacional de Hidrocarburos cuenta con 22 bloques asignados para este tipo de áreas y seis contratos firmados** para iniciar en forma la exploración de estos yacimientos en el Valle del Magdalena Medio y el Catatumbo.

Aunque el ambientalista reconoce que la demanda interna de petróleo que tiene el país sobrepasa la producción petrolera actual, y apenas se cuenta con 7 años de reservas tanto de gas como para petróleo, se debe pensar en los costos ambientales que generaría el fracking en un país donde el  **75% de los recursos hídricos se encuentran en las fuentes subterráneas.**

Colombia estudia la manera de reducir las tasas de material radioactivo en el agua que sale residual del fracking, pero no se ha logrado en ninguna parte del planeta disminuir esos índices hasta obtener los niveles aceptados por los entes de salud pública del mundo, de tal manera que **“no se ha desarrollado ningún estudio que demuestre que se puede realizar esta técnica de manera responsable”**, sostiene Camilo Prieto.

El vocero del Movimiento Ambientalista explica que **en el país no existen estudios de geotectónica amplios, además los análisis de hidrogeología son completamente limitados,** lo que quiere decir, que no hay mapas geológicos certeros sobre la situación en la que se encuentra la zona donde se pensaría realizar el Fracking, y si llega a haber una fuente de agua subterránea donde se lleve a cabo esa actividad, sería muy posible que esta termine con **residuos tóxicos y sustancias radioactivas que pueden ocasionar cáncer, o el gas metano puede generar que el agua se vuelva inflamable.**

Para Prieto, los ministerios de minas y de ambiente están alineados a favor del fracturamiento hidráulico, por lo tanto, no hay instituciones ambientales sólidas que cumplan su papel. Así mismo, se debe tener en cuenta que **en Colombia si una empresa logra desarrollar explotación convencional, con esa licencia ambiental puede apalancar la explotación no convencional, es decir el fracking.**

Es por eso, que desde el Movimiento Ambientalista Colombiano se adelanta una iniciativa a través del portal **Chanage.org** basada en las advertencias de la propia Contraloría General de la Nación que recomendó a Colombia adherirse al principio de precaución partiendo de la base de que no existen estudios concluyentes.  El objetivo, es reunir un millón de firmas para llamar la atención del gobierno nacional para que declare la moratoria al fracking en el país.

Si estás interesado en firmar la petición para que no haya fracking en Colombia puedes hacerlo[aquí.](https://www.change.org/p/juanmansantos-pedimos-la-moratoria-al-fracking-en-colombia-preferimos-el-agua-preferimos-la-vida?recruiter=112718980&utm_source=share_petition&utm_medium=twitter&utm_campaign=share_twitter_responsive)

https://www.youtube.com/watch?time\_continue=40&v=uORqBxGfhBM  
<iframe src="http://co.ivoox.com/es/player_ej_11438329_2_1.html?data=kpahlZ2Xdpqhhpywj5aWaZS1lpeah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Z0ZC0tsrZ1dSYj5Cxs9fdzs7S0NnTb6Lhw87S0NnFsMrn1caYpdTQs87Wysbb0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
 
