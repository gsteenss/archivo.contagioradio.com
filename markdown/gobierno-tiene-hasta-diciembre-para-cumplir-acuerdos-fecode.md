Title: Gobierno tiene hasta diciembre para cumplir acuerdos: FECODE
Date: 2017-10-04 15:15
Category: Educación, Nacional
Tags: fecode, Min Educación
Slug: gobierno-tiene-hasta-diciembre-para-cumplir-acuerdos-fecode
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/84841b8f-f84e-4697-8226-6c3ad2604df0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FECODE] 

###### [04 Oct 2017] 

Luego de 4 meses de la firma de los acuerdos del paro de docentes en Colombia, Over Dorado, integrante de FECODE, manifestó que se están generando reuniones periódicas para cumplir lo pactado, sobre todo en los temas de financiación del sector educativo y del sistema general de participación**, sin embargo afirmó que si no hay avances concretos, hasta antes de diciembre, el panorama para la educación será "complejo**".

Según el vocero, paralelamente se está trabajando en la **Comisión Tripartita** que habla sobre el estatuto docente, en las deudas que tiene el gobierno con el magisterio y en los cambios necesarios, en términos de decretos, que afectan las licenciaturas. (Le puede interesar:["No hay condiciones para implementar jornada única: ADE"](https://archivo.contagioradio.com/no-hay-condiciones-para-implementar-jornada-unica-ade/))

### **Los acuerdos se blindan con voluntad política y decisiones de Estado** 

Frente al cumplimiento de los acuerdos, Dorado señaló que “no basta que el gobierno se reúna con FECODE” y que los acuerdos solo se podrán blindar si hay **voluntad política por parte del gobierno, voluntad jurídica y voluntad económica para soportar los reclamos** y exigencias que hacen los docentes del país.

“Nosotros ya hemos dicho que si no hay avances de aquí a diciembre con los 25 puntos que se pactaron, **el próximo año inicia con una situación laboral bastante conflictiva con el gobierno de Juan Manuel Santos**” afirmó Dorado.

Sobre la movilización estudiantil nacional que se llevó a cabo el 4 de octubre, Dorado señaló que se suman a las exigencias que realizan los estudiantes, sobre aumentar la financiación a la educación pública y expresó que  para **el 12 de octubre se realizará una movilización nacional** en donde se espera que los diferentes sectores del movimiento social salgan a las calles. (Le puede interesar:["Universidad Nacional necesita presupuesto para seguir siendo pública"](https://archivo.contagioradio.com/presupuesto-un-regalo-urgente-para-la-u-nacional-en-sus-150-anos/))

<iframe id="audio_21273116" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21273116_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
