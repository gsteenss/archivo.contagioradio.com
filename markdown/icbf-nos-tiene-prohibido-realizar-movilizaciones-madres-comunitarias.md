Title: "ICBF nos tiene prohibido realizar movilizaciones" madres comunitarias
Date: 2017-07-25 13:37
Category: DDHH, Nacional
Tags: DDHH, ICBF, Madres Comunitarias
Slug: icbf-nos-tiene-prohibido-realizar-movilizaciones-madres-comunitarias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/madres_comunitarias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero ] 

###### [25 Jul 2017] 

Las **madres comunitarias siguen denunciando arremetidas por parte del Instituto Colombiano de Bienestar Familiar ICBF**. Ellas aseguran que les tienen prohibido realizar movilizaciones y que ahora tienen más condiciones para poder trabajar e incluso deben realizar mantenimiento de la infraestructura de los hogares con su dinero.

La Corte Constitucional en el mes de abril de este año reconoció que el ICBF debía realizar **el pago a 106 de ellas que no alcanzaban a tener todas las condiciones para ser pensionadas**. En razón de esta decisión es que las madres comunitarias manifiestan que están siendo presionadas por la institución. (Le puede interesar: ["Madres comunitarias denuncian presiones por parte del ICBF"](https://archivo.contagioradio.com/en-2016-han-fallecido-50-madres-comunitarias-por-estres-y-falta-de-garantias-laborales/))

Para Olinda García, líder del sindicato de madres comunitarias –SINTRACIHOBI-, **“el ICBF ha tomado la decisión anti constitucional de que podamos realizar marchas** y nos ha dicho que si por alguna razón se cierra una unidad, no la vuelven a abrir”. Adicional a esto, ella manifestó que las han puesto a llenar trámites que antes no tenían y esto aumenta el trabajo que tienen que realizar.

Según García “nos piden que hagamos cambios en la cocina, en las paredes y los pisos de nuestras casas que prestamos para el servicio y debemos pagarlo nosotras”. Las madres comunitarias han manifestado en varias ocasiones que sus condiciones laborales no son buenas y que muchas de ellas **se han enfermado y han muerto sin haber recibido los beneficios de haber trabajado toda la vida en función de la niñez**. García ha denunciado en varias ocasiones que, en los que va del 2017, han muerto 50 madres comunitarias a causa del estés.

García afirmó que ellas se han tratado de reunir con el ICBF en varias ocasiones y **esta institución ha respondido negativamente a sus solicitudes**. “Por medio de cartas nos dicen que ellos no tienen ningún vínculo con nosotras y aún no han pagado las pensiones que nos deben”. Ante el incumplimiento de los pagos, manifestaron que van a esperar a que se cumplan los 3 meses de plazo a partir de la emisión del fallo, para tomar acciones que presionen el respeto de sus derechos. (Le puede interesar: ["80 mil madres comunitarias a la espera de que Corte Constitucional ratifique sus derechos"](https://archivo.contagioradio.com/38523/))

Finalmente, García fue enfática en manifestar que el ICBF ha modificado el manual operativo de las actividades de las madres comunitarias sin consultarles. Ellas han interpretado esto como una presión ante los **derechos laborales de más de 63.000 madres que hay en todo el país.**

<iframe id="audio_19992031" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19992031_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
