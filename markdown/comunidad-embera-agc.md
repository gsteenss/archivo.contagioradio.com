Title: Comunidad Embera expulsó a cinco paramilitares de las AGC de su territorio en Chocó
Date: 2019-03-24 17:45
Author: CtgAdm
Category: DDHH, Nacional
Tags: AGC, Chocó, Duque, Enfrentamientos, paramilitares
Slug: comunidad-embera-agc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/conflicto-1-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de Justicia y Paz] 

###### [24 Mar 2019] 

Este domingo, comunidad Embera de Cañaveral, Chocó, mantuvo en custodia a 5 paramilitares integrantes de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC), como forma para protegerse de la presencia de este grupo armado y sus operaciones en el territorio. Esta situación se produjo luego de un incremento de acciones armadas por parte de este grupo en territorios de Jiguamiandó y Curbaradó, de acuerdo a denuncias hechas por la Comisión de Justicia y Paz.

La Comisión ha advertido sobre el incremento de las acciones paramilitares en la zona desde febrero; pero entre las operaciones recientes se presentaron desde el viernes 22 de marzo, momento en que desde la Zona Humanitaria Nueva Esperanza, "un grupo de 50 hombres con armas largas y camuflados integrantes de las AGC" fueron vistos dirigiéndose hacia el Alto Guayabal. Este grupo se uniría a otro de 30 hombres que iba más adelante.

La Organización señala que los armados fueron vistos por organismos nacionales e internacionales defensores de derechos humanos; y añade que ese viernes, en horas de la tarde, un escolta de la Unidad Nacional de Protección (UNP) que está asignado a la Comisión, informó de un cuerpo encontrado en la entrada de Caucheras, se trataba de un hombre que tenía "las manos atadas a la espalda, boca abajo y con evidentes signos de tortura".

### **El sábado persistió la presencia paramilitar, y hubo enfrentamientos** 

De acuerdo a información de pobladores de la zona, cerca de las 7 pm, al interior de la comunidad de Puerto Lleras se presentó un enfrentamiento entre el Ejército e integrantes de las AGC por cerca de 15 minutos, "minutos después se escuchó sobrevolar un helicóptero por la zona". Tras el combate, las comunidades denunciaron las afectaciones a sus viviendas, así como expresaron su temor ante la violencia.

> [\#Jiguamiandó](https://twitter.com/hashtag/Jiguamiand%C3%B3?src=hash&ref_src=twsrc%5Etfw) Contacto armado entre FFMM y AGC de anoche en Pto Lleras "fue para aterrorizarnos, ellos están operando juntos", poblador. Denuncias de actuación conjunta quieren ocultarse. [pic.twitter.com/bFF577nWAJ](https://t.co/bFF577nWAJ)
>
> — Comisión Justicia y Paz (@Justiciaypazcol) [24 de marzo de 2019](https://twitter.com/Justiciaypazcol/status/1109846360646471680?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
Ese mismo día, los pobladores de la Zona Humanitaria Nueva Esperanza en Jiguamiandó fueron informados de la presencia de un grupo indeterminado de hombres armados integrantes de las AGC en Las Palomas. Posteriormente, un grupo de aproximadamente 45 hombres fuertemente armados  de la misma estructura violenta ingresaron en la comunidad de Uradá. (Le puede interesar: ["Alerta por fuertes operaciones neoparamilitares en territorios de Curbaradó y Jiguamiandó"](https://archivo.contagioradio.com/operaciones-neoparamilitares-curvarado-jiguamiando/))

</p>
### **Comunidad retienen a armados y piden presencia integral del Estado** 

Ante la ineficacia del Estado colombiano para proteger la vida de sus ciudadanos, la comunidad indígena Embera de Cañaveral tomó a 5 integrantes de las AGC, despojandolos de sus armas largas, radios de comunicación y camuflado exigiendo el respeto a su vida. La comunidad protegió a los hombres y pidió explicación de sus ataques, posteriormente procedió a dejarlos en un lugar dónde pudiesen ser recibidos por funcionarios del Estado; no obstante, ninguno se hizo presente para recibir a los hombres.

> [\#Jiguamiandó](https://twitter.com/hashtag/Jiguamiand%C3%B3?src=hash&ref_src=twsrc%5Etfw) Comunidad Embera dejó en Búfalo a 4 paramilitares de las AGC, asegurando protección a sus vidas. Comunidades requieren del Estado transparencia y respeto a sus derechos con cero retalaciones por acciones de autoprotección [@PBIColombia](https://twitter.com/PBIColombia?ref_src=twsrc%5Etfw) [@gimena\_wola](https://twitter.com/gimena_wola?ref_src=twsrc%5Etfw) [@ABColombia1](https://twitter.com/ABColombia1?ref_src=twsrc%5Etfw) — Comisión Justicia y Paz (@Justiciaypazcol) [24 de marzo de 2019](https://twitter.com/Justiciaypazcol/status/1109949846109282304?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
