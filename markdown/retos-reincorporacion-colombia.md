Title: Los retos de la reincorporación en Colombia
Date: 2018-09-04 18:38
Author: AdminContagio
Category: Expreso Libertad
Tags: excombatientes, presos politicos, reincorporación
Slug: retos-reincorporacion-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/las-preocupaciones-de-las-farc-luego-de-la-entrega-de-armas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:archivo 

###### 4 Sep 2018 

Desde la **discriminación social, hasta la falta de cedulación por parte del Estado**, han sido algunos de los grandes obstáculos que han tenido que afrontar las personas que **se encuentran en el proceso de reincorporación del ahora partido FARC**.

**Guillermo Salazar,** ex prisionero político, relató para los micrófonos del Expreso Libertad **cómo ha sido este camino, la alegría de su retorno al hogar y las apuestas que ahora tiene en su intención de construir un país diferente**.

<iframe id="audio_30636573" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30636573_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
