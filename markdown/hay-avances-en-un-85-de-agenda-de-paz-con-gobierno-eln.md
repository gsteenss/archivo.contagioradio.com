Title: Hay avances en un 85% de agenda de paz con gobierno: ELN
Date: 2015-08-24 17:26
Category: Nacional, Paz
Tags: Conversaciones de paz gobierno - ELN, ELN, ICTJ, justicia transicional, justicia transicional en colombia, Luis Eduardo Celis, modelo de desarrollo, víctimas
Slug: hay-avances-en-un-85-de-agenda-de-paz-con-gobierno-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/gabino_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elpais 

<iframe src="http://www.ivoox.com/player_ek_7589776_2_1.html?data=mJqlm5ybeo6ZmKiakpuJd6Kkl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjMLtjMbjw9PHqdSfxtOY19OPfJaZk5qYxsqPpcjZz8nOjcnJb9HV25DQ0dOPq9DWysrf0NSJd6KfxtGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Eduardo Celis, investigador de la Corporación Nuevo Arco Iris] 

###### [24 Ago 2015]

Durante esta fase exploratoria Gobierno - ELN,  **seis han sido los puntos acordados, relacionados con la transformación social**, el tema de las víctimas y las garantías de participación política; sin embargo, hay un séptimo punto sobre el que no se ha llegado a consenso y tiene que ver con el tratamiento de las **personas privadas de la libertad en razón de su participación, presunta participación o montaje de participación en el conflicto armado, como integrantes del Ejército de Liberación Nacional.**

La mañana de este Lunes, a través de la cuenta en Twitter de Radio Nacional Patria Libre, el comandante del Ejército de Liberación Nacional, se pronunció oficialmente frente a los **avances de la fase exploratoria que el grupo guerrillero ha adelantado con el gobierno colombiano desde junio de 2014** y que podrían dar inicio a la mesa de conversaciones Gobierno – ELN.

En dicha entrevista, Bautista afirma que deben ser consecuentes con el acuerdo de confidencialidad a pesar de que el gobierno ha violado el acuerdo y reiteró el respeto a esa confidencialidad pero afirmó que es optimista y se calcula que **hay un avance del 85% de la agenda planteada con el gobierno “*al terminar eso que falta vendría la fase pública*”. **

Bautista agrega que todo el ELN está comprometido con la paz de Colombia y reafirmó que el reciente **5to congreso de esa organización ya tiene en marcha las decisiones que se tomaron y que además, en su mayoría, fueron tomadas por consenso.**

Luis Eduardo Celis, investigador de la Corporación Nuevo Arco Iris, afirma que la posible culminación de esta fase exploratoria y el anuncio de instalación de la mesa de conversaciones, da cuenta del interés del Gobierno y el ELN por **construir un proceso de negociación público**, como contribución importante para la finalización del conflicto armado en Colombia.

Lo que implicaría pensar estrategias para que las dos mesas de negociación, **FARC y ELN, sean compatibles y que no haya la necesidad de un tratamiento diferenciado**, en aras de buscar la profundización y refrendación de los contenidos jurídicos que se propongan para la materialización de los acuerdos, en el marco constitucional existente o en uno nuevo.
