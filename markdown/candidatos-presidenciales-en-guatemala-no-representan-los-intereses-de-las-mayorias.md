Title: Candidatos presidenciales en Guatemala no representan los intereses de las mayorías
Date: 2015-09-07 13:55
Category: El mundo, Entrevistas, Movilización, Política
Tags: elección parlamento, Elecciones presidenciales, Guatemala, Movilización, Primera vuelta, Resultados, segunda vuelta
Slug: candidatos-presidenciales-en-guatemala-no-representan-los-intereses-de-las-mayorias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/elecciones-en-guatemala-1_995x560.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: univision.com 

<iframe src="http://www.ivoox.com/player_ek_8165382_2_1.html?data=mZajl5icdo6ZmKialJiJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRb8TVz8nWxsbYs9Sf0dfS1c7Iqc%2FXysbZx9iPqc%2BfyNrO1srRpc3VjNPcjdfJtNPZ1Mrb1sbSb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Andrea Ixtziu, Prensa Comunitaria] 

###### [7 sep 2015] 

Que los guatemaltecos se hayan acercado a las urnas no significa que los candidatos representen sus intereses. L**os distintos candidatos se ven inmiscuidos con casos de corrupción y nexos con  la cúpula militar que participó en crímenes de lesa humanidad durante la dictadura.**

Según los resultados entregados por el Tribunal Supremo Electoral, **la abstención en esta jornada llegó cerca del 41%**, lo que indicaría que los movimientos sociales que han participado en las jornadas de protesta que generaron la renuncia de Otto Pérez Molina, no encontraron un candidato que respondiera a sus intereses. Además quienes asisten a la cita en segunda vuelta tienen cosas pendientes con la justicia.

La periodista Andrea Ixtziu afirma que Jimmy Morales, candidato del partido Frente de Convergencia Nacional, con una votación del 24%, es decir 1'147.645 votos, es uno de los candidatos confirmados para la segunda vuelta **está rodeado por la cúpula militar la cual tuvo complicidad en crímenes de lesa humanidad en la dictadura de Ríos Montt**.

Por su parte Manuel Baldizón, del Partido Libertad Democrática Renovada, con una votación 19.41% con 912.143 votos, empresario multimillonario, q**uien mediante la compra de votos, logró que el partido líder se posicionara como el más importante.** Además que durante las movilizaciones que se han presentado, las ha condenado y desprestigiado fuertemente.

Sandra Torres, del partido Unidad Nacional Renovada con una votación del 19.58%, es decir, 915.705 votos tiene a las fiscalías adelantando procesos por escándalos de corrupción por **delitos de lavado de dinero y otros activos, delitos administrativos y violaciones a los derechos humanos.**

[![guatemala 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/guatemala-2.png){.aligncenter .wp-image-13483 width="563" height="191"}](https://archivo.contagioradio.com/candidatos-presidenciales-en-guatemala-no-representan-los-intereses-de-las-mayorias/guatemala-2-3/)

**Reacomodo en el Parlamento:**

En la elección del parlamento también realizada este domingo hubo un reacomodo por parte de las fuerzas electas, en donde con los procesos que tienen algunos diputados por corrupción han impedido que estén en las elecciones, además que hay otras que están en proceso.

Por otra parte otros partidos y sectores políticos entraron a ser participes de nuevas curules, aunque todavía priman las fuerzas de derecha y tradicionales.

Con la elección al congreso los guatemaltecos que desde el 25 de abril se han movilizado,  buscan cambios profundos y estructurales en el estado y esto solo se puede dar desde la constitución, ahora la tarea es pedir al nuevo parlamento “*movilizar una agenda colectiva de reformas*” según Andrea Ixchiu, miembro de la agencia prensa comunitaria de Guatemala.
