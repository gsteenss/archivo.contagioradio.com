Title: El 99.9% de los crímenes de Estado están en la impunidad
Date: 2016-03-07 13:49
Category: DDHH, Nacional
Tags: crímenes de estado, Día de la Dignidad de las Víctimas de Crímenes de Estado, proceso de paz
Slug: el-99-9-de-los-crimenes-de-estado-estan-en-la-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Sin-Olvido-Palacio-de-Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [7 Mar 2016] 

Con diferentes actividades culturales y movilizaciones en todo el país, se conmemoró ayer el Día de la Dignidad de las Víctimas de Crímenes de Estado con el fin de  reivindicar la memoria  de las personas y organizaciones sociales que han sido torturadas, desaparecidas, desplazadas y asesinadas por el Estado colombiano.

“Esta conmemoración nos recordó los exterminios que hemos vivido y volvimos a estar en la mira de las y los colombianos, ya que es necesario erradicar las prácticas de la eliminación del otro a partir de la persecución, casos como el de la UP y ALUCHAR, no pueden quedar en el olvido y la impunidad”, señala Soraya Gutiérrez integrante del comité de impulso del Movimiento de Víctimas de Crímenes de Estado, MOVICE y abogada del Colectivo de Abogados José Alvear Restrepo.

De acuerdo con Gutiérrez, en Colombia “el conflicto armado ha servido de excusa para esconder la práctica de políticas sistemáticas desde el Estado que ha tenido como propósito exterminar a movimientos sociales, políticas, sindicales y de oposición”, lo que ha dejado **cerca de 3.500 sindicalistas asesinados, más de 700 defensores de Derechos Humanos muertos en los últimos 15 años y miles de masacres.**

**El 99.9% de los crímenes de Estado continúan en la impunidad debido a que las políticas estatales siguen siendo ineficientes** para dar con la verdad sobre estos hechos y las investigaciones no avanzan significativamente para hallar con los responsables intelectuales.

Frente a la Comisión de la Verdad, las organizaciones de víctimas afirman tener muchas esperanzas, pero también sienten que existen algunos vacíos que impedirían que se logre justicia. “Existen muchas limitaciones en relación con **la responsabilidad de agentes del Estado, ya que lo que se recopile en la Comisión de la Verdad no  podrá ser presentado dentro de la Jurisdicción Especial para la Paz,** lo que a su vez implica limitaciones para el acceso a la justicia”, dice la abogada Gutiérrez.

**“Es necesario empezar a documentar los procesos colectivos de exterminio y persecución,** teniendo en cuenta que la verdad permite establecer las causas estructurales por las cuales se originaron los crímenes, para que se generen reformas que permitan garantizar la no repetición de estos hechos”, sostiene la integrante del MOVICE, quien resalta la responsabilidad que tienen las organizaciones de víctimas en la recopilación de expedientes y documentos que verifiquen los crímenes.

En el marco de esta conmemoración el próximo **15 de marzo en Bogotá se realizará una movilización** que tendrá  dos propósitos fundamentales: **presentar una propuesta a la mesa de negociación para la creación de una subcomisión de garantías de no repetición y replantear el papel de las Fuerzas militares para el post-acuerdo,** en términos de generar una depuración de los agentes del Estados que han cometido  violación de derechos humanos, y llevar a la palestra pública temas como  la reducción del as Fuerzas Militares y  la reforma a la doctrina militar.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
