Title: Zona de reserva campesina de la Perla Amazónica celebra la Semana de la Juventud
Date: 2019-10-11 10:33
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Amazonía, campesinado, jovenes, Perla Amazónica, ZRCPA
Slug: semana-juventud-vida-biodiversidad-amazonica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Festival-ZRCPA.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión Intereclesial de Justicia y Paz] 

Del 10 al 14 de octubre, en motivo del 19 aniversario de la creación de la Zona de reserva campesina de la Perla Amazónica en  Putumayo, se realiza la semana de la juventud “Celebrando la vida y la Biodiversidad amazónica”, un proyecto que pretende reivindicar el valor de las riquezas que ofrece la tierra y mostrar a los jóvenes campesinos la importancia de las tareas que éstos desarrollan.

Este encuentro tiene como objetivo agrupar a los jóvenes del lugar para que compartan, aprendan y se diviertan a través de actividades lúdicas, así como conscienciarlos acerca del papel que tienen ellos en la construcción y protección de la Amazonía.

La jornada contará con la participación de profesores, biólogos y expertos en aves que compartirán sus conocimientos acerca de la biodiversidad de la región. "Algunos de los profesionales que van a estar haciendo este acompañamiento fueron miembros fundadores del grupo juvenil de la zona. Es muy significativo que puedan estar compartiendo su propio proceso, que muestren que los jóvenes campesinos también tienen opciones" afirma **Yaneth Silva, integrante de la Asociación de Desarrollo Integral Sostenible Perla Amazónica** (ADISPA).

Asimismo, se realizarán encuentros culturales relacionados con la protección de la biodiversidad y el respeto hacia los derechos bajo el lema “ZRCPA es Amazonía”. También celebrarán “La fiesta de la memoria Amazónica”, con el fin de enaltecer sus vidas y las de aquellos mayores que dejaron un legado. Se calcula una asistencia de 100 a 120 personas. (Le puede interesar: "[Mujeres, niños y jóvenes construyen memoria en la Zona de Reserva Campesina de Putumayo](https://archivo.contagioradio.com/jovenes-ninos-mujeres-zona-reserva-campesina-memoria/)").

> Silva expone que “la idea es que los jóvenes reconozcan el valor que tienen las zonas de reserva y se comprometan con éstas, que vean que hay oportunidades para ellos y que la opción no tiene por qué ser tomar un camino de armas”.

Con estas actividades, las comunidades buscan "abrir nuevos horizontes, para que los jóvenes tengan opciones y aprendan a valorar lo que tienen".

### En contexto 

Las Zonas de Reserva Campesina (ZRC) fueron creadas en 1994 con la finalidad de beneficiar a las comunidades rurales, fomentar y estabilizar su economía y resolver los conflictos sociales que se habían desarrollado en el campo en la época y avanzar hacia la construcción de paz.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
