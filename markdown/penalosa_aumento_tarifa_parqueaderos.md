Title: Administración Peñalosa aumentaría $15 la tarifa de parqueaderos
Date: 2018-01-17 16:48
Category: Economía, Nacional
Tags: Bogotá, Enrique Peñalosa, tarifa parqueaderos
Slug: penalosa_aumento_tarifa_parqueaderos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/parqueaderos.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Civico] 

###### [17 Ene 2017] 

Los usuarios de vehículos particulares deberán alistar sus bolsillos nuevamente ya que la administración de Enrique Peñalosa ha propuesto un nuevo aumento en la tarifa de los parqueaderos en Bogotá. Así lo señaló Juan Pablo Bocarejo, secretario de Movilidad quien dijo que la idea es que la ciudad logre un recaudo de **\$300.000 millones cada año, si se logra que quienes usan carro paguen \$15 más de lo que se cobra actualmente. **

No ha pasado un año desde que el distrito decidió aumentar dicho cobro. En mayo de 2017, ese costo pasó de **\$95 a \$105 por minuto en promedio**; mientras que las motocicletas, el valor máximo pasó de \$67 a \$74 por minuto. Ahora, si el Concejo de Bogotá le da el visto bueno a esa propuesta, esa tarifa alcanzaría los \$120.

### **Más y más aumentos** 

Los primeros que han salido a protestar por la propuesta de la alcaldía son los conductores, pues además del aumento en los costos en la gasolina (\$139 pesos en diciembre), y el del Seguro SOAT (que oscila entre los \$163.050 para ciclomotores y \$1.275.900 para carros de servicio público intermunicipal de 10 o más pasajeros), los usuarios de vehículos deberán pagar un 14% más por parquear sus automóviles.

Por su parte, el secretario de Movilidad,  ha dicho que “la idea es generar una contribución adicional a quienes estacionan sus vehículos en parqueaderos públicos y que** la totalidad de este tributo se utilice para el financiamiento del transporte público”.**

Sumado a eso, desde la alcaldía indican que se trataría de una medida con la que se buscaría promover el uso del transporte público. Sin embargo, expertos en la materia señalan que si se trata de estimular el uso del bus o el Transmilenio, no se debería aumentar entre \$100 y \$200, como también lo quiere la administración de Enrique Peñalosa.  (Le puede interesar:[En 2018 un bogotano deberá pagar \$110.400 mensuales en Transmilenio)](https://archivo.contagioradio.com/en-2018-un-bogotano-pagara-en-promedio-110-400-al-mes-en-sitp/)

En medio del descontento por esas propuestas, la Alcaldía de Bogotá dice que con el monto recogido se fortalecería la sostenibilidad financiera del sistema y se buscaría **mejorar la calidad del transporte de la ciudad.** Sin embargo no precisó cómo esa tarifa redundará en beneficios para el transporte colectivo.

**Dicha definición del aumento de \$15 en la tarifa por minuto de parqueaderos**, se tomó con base a lo establecido en el numeral **2 del artículo 33 de la Ley 1753 de 2015.**

<iframe id="doc_41039" class="scribd_iframe_embed" title="Proy-664-17-Articulado-1" src="https://www.scribd.com/embeds/369388818/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-41QpgVfm5s4BYKQhinpV&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
