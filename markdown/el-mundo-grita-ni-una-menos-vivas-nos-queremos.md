Title: El mundo grita, ni una menos vivas nos queremos
Date: 2016-10-18 13:53
Category: Entrevistas, Mujer
Tags: Argentina, Derechos, feminicidios, feminicidios colombia, Feminicidios en México, genero, mujer, violencia, Violencia de género
Slug: el-mundo-grita-ni-una-menos-vivas-nos-queremos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Ni-una-menos-e1476815647809.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ni una menos 

###### 18 de Oct 2016 

A 400 kilómetros de la capital de Buenos Aires, en una pequeña ciudad de 600.000 habitantes está Mar del Plata, lugar que ocupó los principales titulares con una triste noticia. **Lucía, una pequeña de 16 años, fue drogada, brutalmente violada y empalada en esa ciudad costera**. Hoy, sus padres lloran su pérdida y aunque nadie les devolverá a su hija, hermana, amiga, vecina, piden que se haga justicia y que el Estado garantice que estos casos no sigan sucediendo.

**Lucia, representa solo uno de los casos que a diario ocurren en América Latina y en el mundo entero**. Como si la situación no fuera bastante grave ya y no bastara con lo que ha sucedido, los crímenes continúan. Luego de Lucia, fueron asesinadas 3 mujeres más en Argentina. Silvia Ruiz de 55 años, Marilyn Méndez de 28 años quien además se encontraba en estado de embarazo y Vanesa Moreno de 38 años, todas a manos de sus ex parejas.

**Las cifras son alarmantes**, pero  tristes y dan cuenta del largo trabajo que aún hay que hacer. **En Argentina cada 30 horas  una mujer es asesinada por su condición de mujer**. Para el 2015 se registraron 235 feminicidios, el 18% de las víctimas tenía menos de 20 años, mientras que el 43% estaba entre los 21 y 40 años, el 25% entre 41 y 60 y el 9%, más de 60.

**En México, 6488 mujeres fueron asesinadas entre 2013 y 2015,** reflejando un aumento del 46% con relación al período 2007-2009. Sin embargo las cifras muestran que son dos décadas viendo partir a cientos de mujeres por su condición.

**La situación no cambia en países como Colombia, Perú y Bolivia que también hacen parte de los 25 países con más feminicidios.** Así mismo, Europa  ha dado a conocer algunas cifras en las cuales se encuentran países como Francia que tuvo 122 casos en 2015, Italia 59 casos de enero hasta junio del 2016 y en España 77 según cifras manejadas por plataformas de organizaciones sociales. Le puede interesar: [En la impunidad feminicidios de colombianas en México](https://archivo.contagioradio.com/en-la-impunidad-feminicidios-de-colombianas-en-mexico/).

Por su parte, **la OMS – Organización Mundial de la Salud - ha calificado como una “pandemia” la violencia que sufren muchas mujeres alrededor del mundo.** Según un estudio de esta organización titulado “Estimaciones mundiales y regionales de la violencia contra la mujer” asegura que en **uno de cada dos asesinatos a mujeres, el autor es su compañero sentimental o alguna persona muy cercana a la víctima.** Puede leer: [Feminicidios estarían directamente relacionados con multinacionales](https://archivo.contagioradio.com/feminicidios-estarian-directamente-relacionados-con-multinacionales/)

**El mundo se moviliza**

**El caso de Lucia ha logrado que países** como México, Nicaragua, Brasil, Chile, Perú, Venezuela y Colombia **hayan repudiado este hecho y  apoyado el llamado a movilizarse para gritar no más feminicidios.**

Melina Sánchez de la Organización Ni una Menos, aseguró que **el trabajo de poner en evidencia la situación del sistema patriarcal que existe, se ha hecho a través de diversas actividades convocadas** “para mostrar que las mujeres tenemos una agenda política en materia de derechos, igualdad de oportunidades, de acceso a la vivienda, a la educación, al trabajo” aseguró. Lea también: [70 mil mujeres argentinas contra el patriarcado y el ajuste](https://archivo.contagioradio.com/eln-responde-carta-enviada-por-los-ambientalistas-colombianos-a-esta-guerrilla/).

Aunque la marcha convocada para el miércoles 19 de Octubre ha tenido una gran acogida por parte de las organizaciones y la sociedad civil, Melina asegura que **el trabajo de movilización seguirá llevándose a cabo** para lograr que “el machismo que esta enquistado en la sociedad pueda ser repensado no solo desde las mujeres sino también desde los hombres, desde las nuevas masculinidades”.

Mientras tanto, Lucia ha hecho que miles de mujeres y hombres de Mar del Plata, de toda Argentina y América Latina, se unan en solidaridad para seguir pintando carteles, sus cuerpos, sus rostros,  para **gritar ni una menos, vivas nos queremos** y exigir a los Estados la garantía del derecho a la vida así como el repudio y acciones efectivas que conduzcan a que no existan más feminicidios.

**El más reciente caso en Colombia**

El lunes 17 de Octubre, en la ciudad de Bucaramanga fue asesinada Sandra Marcela González de 26 años. Según informó su familia a manos de su ex pareja, con la que convivió 6 años pero del cual se había separado hace 4 meses.

El hombre la atacó 6 veces, ocasionándole 1 herida en el brazo y 5 en el corazón. Sandra no resistió debido a la gravedad de las heridas.

<iframe id="audio_13374057" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13374057_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
