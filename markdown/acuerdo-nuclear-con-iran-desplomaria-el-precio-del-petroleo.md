Title: Acuerdo nuclear con Iran desplomaría el precio del petróleo
Date: 2015-04-07 17:22
Author: CtgAdm
Category: Economía, Otra Mirada
Tags: Acuerdo nuclear Iran y G5+1, Brent, colombia, EEUU, Irak, Iran, Precio del Petróleo, Unión europea, Venezuela, WTI
Slug: acuerdo-nuclear-con-iran-desplomaria-el-precio-del-petroleo
Status: published

###### Foto:Imagui.com 

Durante la última semana los **precios del petróleo comenzaron a bajar** pero no fue hasta el anuncio del **acuerdo entre el G5+1 con Irán** sobre el programa nuclear iraní cuando el precio **bajó un 5,4% el Brent en Londres y un 4% el WTI en Nueva York**.

**Irán tiene 30 millones de barriles de crudo almacenados** debido al embargo de la UE como parte de las sanciones por el supuesto intento de crear una bomba atómica, con el acuerdo se espera que estas reservas entren a circulación de nuevo tanto en el mercado europeo como en Asia y América, inflando la oferta y saturando el mercado, lo que produce una rebaja de los precios.

**Irak** el segundo productor de petróleo de la OPEP, continúa incrementando sus **exportaciones desde el 15 al 20%.** **EEUU,** por su parte **acumula en total 417 millones de barriles en excedentes**, justo en la época en la que comienzan las labores de mantenimiento de sus refinerías, con lo que el excedente va a aumentar debido al tiempo en el que no se va a producir.

Con dicho exceso de producción es fácil pronosticar que los **precios del crudo no superarán los 40 dólares** en, al menos el tiempo en el que Irán vuelva a poder exportar su crudo acumulado, y el que de nuevo pueda producir.

Esto va a significar un **alivio para los países dependientes del petróleo** que no tienen crudo, pero va a suponer un **fiasco para las energías renovables**, hasta ahora **más caras que el petróleo**. Al bajar los pecios va a ser difícil que estas puedan competir con las energías fósiles.

Para el país persa, que es la cuarta reserva mundial de crudo, la exportación de su petróleo es vital, pero su mercado de importación no solo era UE, **sino también China, India, Japón y Corea del Sur**. Por tanto esto va a crear un nuevo escenario geopolítico en un futuro con nuevas alianzas económicas y políticas, dejando las puertas a **Irán a convertirse** en una gran **potencia para el Medio Oriente**.

De tal manera **es EEUU, quién también está interesado en que sus relaciones con Irán se normalicen**, para que en la región exista la estabilidad necesaria para sus propios negocios relacionados con las energías fósiles.

Los países que dependen de la exportación del petróleo, como Colombia, Venezuela y otros de Sur América serán los más afectados puesto que muchos de los recursos oficiales provienen de la venta de crudo.

Este escenario deja a los **ciudadanos**, quienes se ven afectados en el día a día por las subidas y bajadas del precio, **sin ningún tipo de voz sobre la gestión económica de las energías a las que están obligados a consumir**.
