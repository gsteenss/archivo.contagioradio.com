Title: No avanzan investigaciones sobre asesinatos de excombatientes
Date: 2019-09-11 08:59
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: asesinato de excombatientes, FARC, Pacto por la No violencia
Slug: no-avanzan-investigaciones-sobre-asesinatos-de-excombatientes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/asesinados_farc-e1522961371379.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Teleantioquia] 

Tras conocerse el asesinato de tres excombatientes el pasado 6 de septiembre en Cúcuta y en Chocó, la senadora del Partido Fuerza Alternativa Revolucionaria del Común (FARC), Sandra Ramírez [ se refirió a la situación que viven las personas en proceso de reincorporación y los nulos avances en las investigaciones o condenas de estos homicidios.][(Podría leer: Asesinan a excombatientes Miltón Urrutía y José Peña en Cúcuta)](https://archivo.contagioradio.com/asesinan-excombatientes-cucuta/)

[Sobre el esclarecimiento y las posibles causas de los asesinatos que hoy ascienden a más de 140 de excombatientes,  la senadora señaló que siempre existe una serie de patrones en los homicidios vinculados al trabajo que realizan  junto al **Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito (PNIS)** o su rol como líderes de sus comunidades, como el caso de Wilson Saavedra, quien estuvo a cargo del ETCR de Marquetalia, en Planadas, Tolima.][(Le puede interesar: Asesinan a Wilson Saavedra ex comandante de FARC que aportó a la paz)](https://archivo.contagioradio.com/asesinan-a-wilson-saavedra-ex-comandante-de-farc-que-aporto-a-la-paz/)

> ["Nosotros firmamos un acuerdo para que no nos maten, firmamos un acuerdo para construir junto con él y la sociedad una nueva nación y eso nos está costando la vida".]

### **Impunidad en los casos relacionados a excombatientes** 

Pese a que fue firmado un pacto por la no violencia en campaña electoral la senadora resalta que aún no se han visto resultados por parte del mismo Gobierno para incentivar este acuerdo que también contó con diferentes organismos de la sociedad civil, la iglesia y las diversas bancadas. [(Le puede interesar: La importancia de firmar un pacto por la no violencia en época electoral)](https://archivo.contagioradio.com/pacto-por-la-no-violencia-electoral/)

Se refirió a su vez a las investigaciones de la Fiscalía, señalando que no se han realizado investigaciones profundas de los verdaderos móviles y aunque según esta entidad ya han sido esclarecidos el 53% de los casos de líderes asesinados, para los integrantes de FARC hasta ahora no existe un solo caso juzgado por estos crímenes. [(Lea también: Coronel Jorge Armando Pérez será investigado por asesinato de excombatiente Dimar Torres)](https://archivo.contagioradio.com/coronel-jorge-armando-perez-sera-investigado-por-asesinato-de-excombatiente-dimar-torres/)

Frente al emblemático caso de Dimar Torres, excombatiente asesinado el pasado 22 de abril a manos de integrantes del Ejército, se esperaba que este 9 de septiembre tuviera lugar la audiencia de imputación de cargos contra el grupo de militares investigado, sin embargo a petición de la defensa de los acusados solicitó que el proceso sea adelantado por la Justicia Penal Militar, algo que tendrá que definir el Consejo Superior de la Judicatura.

[Agrega que e]l impacto de estos asesinatos en los Espacios Territoriales según FARC ha generado temor ,  algo que se incrementa con las amenazas que llegan de grupos como [Las Águilas Negras, de quienes se desconoce su identidad o estructura, "no contamos con seguridad. En todas las reuniones le exigimos al Gobierno, en comunicados públicos le exigimos al garantías para nuestra seguridad, para nuestra vida", asegura. ]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
