Title: Con orgullo, Violeta recibe los restos de su madre, militante del M-19 desaparecida del Palacio de Justicia
Date: 2017-11-11 10:00
Category: DDHH, Nacional
Tags: Desaparición forzada, Palacio de Justicia
Slug: violeta_desaparecida_c_19_palacio_justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/4ea599e8-8e7d-4167-8dc7-25a1fb959220.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 Nov 2017] 

A los seis años de edad, Violeta perdió a su madre, pero solo hasta los 20 tuvo la valentía de levantar la cabeza y empezar su búsqueda. Cristina Garzón Reyes, era una mujer líder, veterinaria, agrónoma e integrante del M-19, que participó en la toma del Palacio de Justicia en 1985. Son 32 años de cuestionamientos y miedos, de reconocer ante el mundo que era la hija de una militante, que un día decidió empuñar las armas, pero que ante todo era un ser humano.

"Mi madre fue una gran mujer, fue  amiga, compañera, mamá, hermana, tía. Siempre fue una líder. Militó en el M-19 pero también era una persona, eso es lo más importante para nosotros la idea es demostrarle al país que los integrantes del M-19 también eran seres humanos, son personas que merecen ser enterrados y reconocidos", dice Luisa Violeta Martínez,  hija de la víctima.

### **32 años entre el miedo y la incertidumbre** 

No fue fácil. En medio de la estigmatización que implica ser hija de una guerrillera, Violeta admite que por muchos años ella y su familia vivieron llenos de temores y mentiras. Tan pronto sucedieron los hechos del 6 y 7 de noviembre,  su padre no se atrevió a acercarse a preguntar qué había pasado con su esposa. Violeta le decía a sus amigos que su madre había muerto en un accidente, y otras veces incluso aseguraba que había perdido la vida en la avalancha de Armero, aprovechando la cercanía de las fechas.

No tiene claro en qué momento supo que su madre había sido militante de esa guerrilla, pero siempre supo el legado que le dejó, y por eso, 14 años después de su desaparición tomó la decisión de empezar su búsqueda. Tocó varias puertas, la Universidad Nacional, Medicina legal, pero nadie daba respuesta alguna del paradero de Carmen Cristina, según Violeta por dos razones principales: se encontraba sin ningún respaldo de abogados, y por otro lado, era la hija de una integrante del M-19.

Sin embargo, los primeros años de lucha le sirvieron para que en 2002 le entregaran los primeros restos encontrados, pero ella decidió que no recibirlos y exigió exámenes de ADN. Efectivamente no todos eran de su madre, también habían restos óseos de Lucy Amparo Oviedo, y con ello se dio un nuevo curso a la investigación, para poder hallar otros desaparecidos.

En 2008 cuando nace la Comisión de la Verdad se logra reconocer que los miembros del M-19 también eran víctimas, y allí organizaciones como la Comisión de Justicia y Paz empiezan a apoyarla en su proceso de búsqueda. En 2013 la Fiscalía corrobora que los demás restos encontrados años atrás, sí eran los de Cristina Reyes, y se lo informan al Colectivo de Abogados José Alvear Restrepo quien empieza a representar a Violeta, de manera que en 2014, ella y su familia deciden recibir aquellos restos.

### **El encuentro con los demás familiares de los desaparecidos** 

Después de que asumió su búsqueda con orgullo, poco a poco fue ganando espacios de manera oculta con los familiares de los demás desaparecidos. Al principio Violeta acompañaba los actos de conmemoración de cada año desde la distancia y en medio de la soledad.

Con la entrega de restos de Cristina del Pilar, René Guarín la presentó con los demás integrantes del grupo, "Me recibieron con amor y compasión. Fue un acto muy bonito, todos entendimos que estábamos en la misma situación y se crea un lazo de amistad y solidaridad", expresa Violeta, aunque asegura que para todos nos ha sido fácil.

Cuenta que sus amigos no entendían porque todavía lloraba, pero conocer a los demás familiares de las víctimas le significó la posibilidad de compartir esos sentires y vacíos alrededor de lo que es ser "uno de los huérfanos del Palacio de Justicia".

### **La entrega de restos** 

Aunque para Violeta el proceso de búsqueda de su madre ha sido atravesado por la soledad y la estigmatización, hoy celebra que tras 32 años del holocausto del Palacio de Justicia, en Ibagué, la ciudad natal de su madre, tiene la posibilidad de enterrar dignamente los restos de Carmen Cristina.

"Es un logro para mi familia tener de vuelta a mi madre. Después de 32 años poder enterrarla es muy satisfactorio", dice.

En un acto público con presencia de la Fiscalía  y Medicina Legal le fueron entregados los restos óseos de su mamá. Con música, teatro, pinturas y poesía le rindieron un homenaje a a 'Violeta'. Allí la acompañaron los familiares de los empleados de la cafetería y demás organizaciones de derechos humanos que han estado con ellos durante años. "Al final lo que quedamos son humanos devastados por la guerra, y compartimos el dolor y la vida, no hay otra manera que reconciliarse", expresa Violeta, quien agrega que tiene un hijo de 2 años y lo que espera es "que cuando esté grande tenga otro país. Lo que sucede en Ibagué es muestra de que puede haber reconciliación, puede haber perdón y unión".

<iframe id="audio_22059156" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22059156_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
