Title: "No nos dejen solos", la petición de líderes sociales del Chocó ante amenazas
Date: 2019-09-23 16:23
Author: CtgAdm
Category: DDHH, Nacional
Tags: AGC, amenazas contra líderes sociales, Chocó, Clan del Golfo
Slug: no-dejen-solos-lideres-sociales-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Yeison-Mosquera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de la Verdad] 

Al celular del coordinador de **Tierra y Vida**, Yeison Mosquera llegó un mensaje atribuido al Clan del Golfo, en el que se le menciona a él junto a los líderes sociales de la organización, Julio León, Jorge Martelo, Cecilia Montenegro, Jorge Solano, Libardo Palomino y Cecilia Arrieta, amenazándoles de muerte. Para ellos las intimidaciones son producto de las continuas denuncias que han hecho contra la vulneración de DDHH en Chocó y la incursión de grupos armados que victmizan a las comunidades.

Mosquera, se refirió a las más recientes amenazas en su contra, manifestando que tuvo que salir del territorio pues las personas detrás de los hostigamientos aseguran que ya lo tienen ubicado a él y a varios de sus compañeros. [(Le puede interesar: Amenazan de muerte a líder social y escoltas en Riosucio, Chocó)](https://archivo.contagioradio.com/amenazan-de-muerte-a-lider-social-y-escoltas-en-riosucio-choco/)

### "Los líderes somos personas de carne y hueso "

Aunque varias de las personas amenazadas poseen esquemas de protección, la mayoría dejó el territorio, debido a la ausencia de garantías, "mientras hay otros personajes que tienen diez camionetas, el Gobierno dice que no tiene plata para proteger a la comunidad y a los líderes, entonces lo que decimos es que la vida de nosotros vale menos que la vida de una persona".

"Siento mucho miedo que me puedan asesinar, si me muero vendrán muchos tras de mí que querrán reclamar y saber la verdad de lo que pasa en Colombia.", expresa Mosquera y afirma que son las comunidades las que siempre son las afectadas por el conflicto por lo que es necesario que el Gobierno se siente a hablar con los diversos actores armados del territorio nacional, **"queremos una salida negociada del conflicto,  si podemos seriamos mediadores en cualquier escenario"**, señaló Yeison.

<iframe id="audio_41959433" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_41959433_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
