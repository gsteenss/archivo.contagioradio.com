Title: Asi luce la 3ra edición del Festival Internacional de cine por los DDHH
Date: 2016-05-05 11:38
Author: AdminContagio
Category: 24 Cuadros
Tags: Diana Arias, Festival de cine por los DDHH, Fundación Konrad Adenauer
Slug: asi-luce-la-3ra-edicion-del-festival-internacional-de-cine-por-los-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Afiche-Festival_r.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Impulsos Films 

##### [05 May 2016] 

Fue presentada la imagen y clip promocional oficial de la 3ra edición del Festival Internacional de cine por los Derechos Humanos que tendrá lugar entre el 24 y el 28 de mayo, de manera simultánea en 41 escenarios de Bogotá, Medellín y Cartagena.

La propuesta visual para esta edición "invita a abrazar la esperanza, el respeto, la defensa y promoción de los derechos humanos en todas sus manifestaciones, y presenta su imagen oficial con la que celebra la vida con el símbolo del abrazo" como expresan sus organizadores.

[![Festival DDHH](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Afiche-Festival_r1-e1462466833683.png){.aligncenter .wp-image-23604 width="351" height="527"}](https://archivo.contagioradio.com/asi-luce-la-3ra-edicion-del-festival-internacional-de-cine-por-los-ddhh/afiche-festival_r-2/)  
El papel simbólico del abrazo "representa las historias que se tejen en la narración audiovisual de cada una de las obras participantes que realizan un ejercicio de memoria y reivindicación de las personas y comunidades que le apuestan a la legitimización de los derechos humanos".

"El abrazo encarna la lucha por el respeto y la garantía de los derechos humanos en un contexto de reconciliación, de “seguir adelante” aceptando las diferencias del otro para construir, en conjunto, nuevas realidades y espacios donde predomine la paz" .

<iframe src="https://www.youtube.com/embed/YImoN6vq5ec" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>​

**Sobre la 3ra edición **

Luego de un juicioso trabajo de curaduría, encargado ve visualizar y escoger entre las 1063 películas de 52 países que participaron en la convocatoria del Festival, se conformó la Selección oficial con 80 películas de 27 países, 35 de estas producciones tendrán su estreno en el evento.

Paralelamente a la muestra audiovisual, compuesta por largometrajes, documentales nacionales e internacionales, cortometrajes nacionales e internacionales y animaciones, se desarrollará la programación cultural, académica y de formación que incluye: encuentros con directores, paneles con invitados internacionales, exposiciones fotográficas y un laboratorio de desarrollo cinematográfico (ImpulsoLab), entre otras actividades.

La ceremonia de inauguración en Bogotá se realizará el 24 de mayo en el Teatro Nacional La Castellana, a partir de las 7:00 p.m., con la presentación de la película Siembra y un conversatorio con los directores Ángela Osorio y Santiago Lozano, el protagonista Diego Balanta y la productora Gerylee Polanco.

\*El Festival Internacional de Cine por los Derechos Humanos Bogotá es organizado por Impulsos Films en alianza con el Programa Estado de Derecho para Latinoamérica de la Fundación Konrad Adenauer y el programa de subvención Movies That Matter, Contagio radio es medio aliado de la presente edición.
