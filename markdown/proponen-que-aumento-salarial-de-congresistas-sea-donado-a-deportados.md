Title: Proponen que aumento salarial de congresistas sea donado a deportados
Date: 2015-09-07 17:19
Category: Economía, Nacional, Política
Tags: alirio uribe, Aumento salarial congresistas, colombia, Crisis en frontera con Venezuela, Iván Cepeda, Polo Democrático Alternativo, Venezuela
Slug: proponen-que-aumento-salarial-de-congresistas-sea-donado-a-deportados
Status: published

###### Foto: archivo 

<iframe src="http://www.ivoox.com/player_ek_8182885_2_1.html?data=mZallJ2ceY6ZmKiakpqJd6KnlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPj0dTbx9OPtdbZjMbiz8rSuNCf1MbZw9fNpc2fxcqYxdTSq9PZ1M7g1sbXb9TZwpDR0dPFqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ivan Cepeda,Polo Democrático] 

###### [07 Sept 2015] 

[El senador Iván Cepeda y el representante Alirio Uribe del Polo Democrático, **apelando a la coherencia política de los congresistas,** les propusieron que consignaran el retroactivo de su aumento salarial en la cuenta que se ha abierto para la atención de los colombianos que han sido deportados de Venezuela.]

[**"Es hora de mostrar coherencia entre el discurso y los actos"**, afirma Cepeda, **este aumento que supera los 1000 millones puede aportar significativamente** al tratamiento de la crisis.]

[Cepeda asegura que esta propuesta se enmarca en **la solicitud que ha hecho en varias ocasiones al Gobierno para que revise exhaustivamente el régimen salarial y prestacional de los congresistas, así como sus aumentos**. La normatividad vigente afirma que este tema es competencia única del Gobierno, quien ha desatendido estas peticiones.]
