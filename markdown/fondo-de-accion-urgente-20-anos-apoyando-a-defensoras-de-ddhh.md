Title: Fondo de Acción Urgente, apoyando a las defensoras de DDHH
Date: 2016-07-19 16:00
Category: Hablemos alguito
Tags: Berta Cáceres, Fondo de Acción Urgente, Mujeres Defensoras de los territorios
Slug: fondo-de-accion-urgente-20-anos-apoyando-a-defensoras-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/cidh_nota_alquimia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Fondo Alquimia 

##### [19 Jul 2016] 

El Fondo de Acción Urgente (FAU) nace en los Estados Unidos como una propuesta colaborativa de apoyo a defensoras de Derechos Humanos y sus organizaciones, movilizando recursos cuando estas atraviesan situaciones imprevistas que ponen en riesgo el desarrollo de su labor.

Concebida en 1995 durante la IV Conferencia Mundial de la ONU en Beijing sobre la Mujer y fundada oficialmente en 1997 , la organización ha logrado traspasar las fronteras llegando desde 2009 a naciones en África, América Latina y el Caribe hispano-hablante. El trabajo para nuestro continente, coordinado desde Bogotá desde hace 7 años viene respaldando a mujeres a través de estrategias como el "Programa de Apoyos de Respuesta Rápida" con recursos flexibles otorgados de una manera estratégica y oportuna.

Adicionalmente, FAU ha creado algunas iniciativas colaborativas como "Mujeres, Resistencias y Poderes en la Sombra" con la que busca visibilizar la manera en que el narcotráfico y los poderes en la sombra inciden en la vida de las mujeres de América Latina y del Caribe y la iniciativa "Mujeres, territorio y medio ambiente" que apoya a mujeres defensoras de la naturaleza y las comunidades que habitan en territorios blanco del interés extractivista.

En Hablemos alguito, conversamos con Cristina Papadopoulou encargada de la construcción de alianzas y fortalecimiento de capacidades y Laura Carvajal encargada de la iniciativa mujeres medio ambiente y territorio de FAU en América Latina, para conocer más sobre la labor de esta organización feminista que tiene su sede regional en la ciudad de Bogotá.

<iframe src="http://co.ivoox.com/es/player_ej_12263490_2_1.html?data=kpefmJiYfZGhhpywj5WVaZS1kZySlaaYd46ZmKialJKJe6ShkZKSmaiRdI6ZmKiaqNTSqNCfxcqYo8jHrYa3lIqvldOPmdPbxtPhx5CRb6nVw9HSz9TXb8LgyNrW1tSPcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
