Title: Payasos contra el apartheid
Date: 2017-10-04 17:54
Category: Onda Palestina
Tags: Apartheid, BDS, Israel, Palestina
Slug: payasos-contra-el-apartheid
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/festiclown.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa Festiclown 

###### 3 Oct 2017

Por cuarta vez ‘Payasos en rebeldía’ dan un tour por Cisjordania llevando un tinte de alegría a todos los que allí habitan. Este es un equipo voluntario español que concibe la puesta en escena como la única forma real de luchar contra las injusticias del mundo. Provenientes de la región de Galicia, desde allí ayudaron a organizar el Festiclown; este festival atrae a cirqueros de todo el mundo a Palestina, para expresar la alegría que se puede llegar a tener aunque se estén pasando por momento de crisis de todo tipo. Su mensaje es simple: !aún hay un aliento de esperanza!

El festival se realizó durante ocho días con actuaciones en campos de refugiados y escuelas, entre otros espacios, junto a la compañía madrileña Kambahiota, el Circo brasileño No Ato, la fundación Tcyminigagua, los chilenos Academia de Tontos y una delegación de jóvenes de Rivas Vaciamadrid, con talleres pedagógicos y lúdicos. El 23 de Septiembre hubo una gala en la Palestinian Circus School, en Birzeit (Ramala), donde el equipo del Festiclown se reencontró con un colaborador muy querido: Mohammed Abu Sakha. Este artista y profesor de circo palestino permaneció 20 meses -desde diciembre de 2015 a este agosto- preso en las cárceles israelíes bajo la inhumana figura de detención administrativa, por la que una persona puede pasar un tiempo indeterminado en prisión sin pruebas ni juicio.

El domingo, día 24, el Festiclown continuó en Ramala, visitando una escuela para niños y niñas con necesidades especiales, para después salir hacia la villa de Asira, cerca de Nablus, donde se organizaron actividades, juegos y espectáculos para niños y niñas y se pintó un mural que reclama una Palestina “libre y feliz”. Entre el lunes 25 y el jueves 28, las compañías participantes visitaron el hospital y varias escuelas. Además, organizaron talleres de pintura o acrobacias para los chicos y chicas de la ciudad, y conocieron más de cerca Nablus.

En esta ocasión en Onda Palestina hablaremos del retorno oficial de la Autoridad Nacional Palestina dentro de territorio gazatí, retomamos el tema del castigo colectivo ilegal, algunos cierres que ejecutará el gobierno israelí en el marco de las celebraciones judías que se llevarán a cabo la siguiente semana, una entrevista con los íntimos detalles de los sucesos en Catalunya este fin de semana por parte de una colega del BDS, la suspensión de un ciclo cinematográfico que se llevaba a cabo en Cádiz- España y finalmente hablaremos sobre el Festiclown, una alternativa de jóvenes voluntarios unidos con el objetivo de sacarle sonrisas a todo un pueblo.

<iframe id="audio_21274888" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21274888_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
