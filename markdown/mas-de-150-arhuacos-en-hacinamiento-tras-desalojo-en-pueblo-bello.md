Title: Más de 150 arhuacos en hacinamiento tras desalojo en Pueblo Bello, Cesar
Date: 2017-07-21 13:10
Category: DDHH, Nacional
Tags: arhuacos, cesar, conidiciones de hacinamiento, Desalojo, Pueblo Bello
Slug: mas-de-150-arhuacos-en-hacinamiento-tras-desalojo-en-pueblo-bello
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/desalojo-pueblo-bello-cesar-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pueblo Arhuaco] 

###### [21 Jul 2017] 

La situación de 182 familias del pueblo Arhuaco, que habían sido desalojadas de sus territorios ancestrales en Pueblo Bello Cesar, se agrava con el pasar de los días. Los integrantes de la comunidad indígena se encuentran en medio de una **emergencia humanitaria debido al hacinamiento en el que se encuentran**.

Tras haber permanecido una semana en el parque central del municipio de Pueblo Bello, las familias se trasladaron a la casa indígena, que no cuenta con las condiciones estructurales para albergar a las 182 familias. Allí se encuentran, **en condiciones de hacinamiento, alrededor de 150 personas** incluidos niños, mujeres y adultos mayores. (Le puede interesar: ["Comunidad Arhuaca continúa sin garantía en Pueblo Bello, Cesar"](https://archivo.contagioradio.com/comunidad-arhuaca-continua-sin-garantias-en-pueblo-bello-cesar/))

Juan Vallejo, vocero de la comunidad Arhuaca en Pueblo Bello, aseveró “debido a las fiestas patronales que hay en el municipio, nos hemos llevado a las familias a la casa indígena para evitar enfrentamientos que puedan ocurrir con los habitantes de acá”. Igualmente, manifestó que “**no todas las personas desalojadas están en el lugar que establecimos como transitorio**, algunos han pedido alojamiento en viviendas de familias del municipio”.

Si bien los miembros del pueblo Arhuaco han mantenido una comunicación constante con las autoridades locales, Vallejo manifestó que **no han recibido ningún tipo de atención que los ayude a superar el hacinamiento** y la emergencia humanitaria en la que se encuentran. Los indígenas, de acuerdo con Vallejo, solicitarán a la administración local de Pueblo Bello que “se cumplan los protocolos de alojamiento digno”. (Le puede interesar: ["180 familias del pueblo Arhuaco serían desalojadas en Pueblo Bello, Cesar"](https://archivo.contagioradio.com/pueblo-arhuaco-desalojado-pueblo-bello/))

Luego de que puedan solucionar este problema, pasarán a discutir lo relacionado con el plan de desarrollo municipal. Vallejo fue enfático en manifestar que “la movilización y las exigencias las estamos haciendo porque **el pueblo Arhuaco no ha recibido los beneficios de plan de desarrollo**”.

Para Vallejo, **“la comunidad está cansada del desconocimiento que hay del pueblo Arhuaco en el municipio de Pueblo Bello”**.  La comunidad Arhuaca ha manifestado en repetidas ocasiones que su prioridad es recuperar los espacios sagrados que hacen parte de territorio ancestral indígena y que actualmente están en manos de un tercero tras la adjudicación que hizo el INCODER.

<iframe id="audio_19931106" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19931106_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
