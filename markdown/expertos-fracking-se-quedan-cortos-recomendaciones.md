Title: Expertos en fracking se quedan cortos con sus recomendaciones
Date: 2019-02-15 16:24
Author: AdminContagio
Category: Ambiente, Entrevistas
Tags: Alianza Colombia Libre del Fracking, Ambiente, fracking
Slug: expertos-fracking-se-quedan-cortos-recomendaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Fracking-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [15 Feb 2019] 

Un comité de expertos, conformado por el Ministerio de Ambiente y de Minas y Energía, presentó este jueves una serie de recomendaciones que el Gobierno debería considerar si decide implementar ésta técnica de explotación de hidrocarburos en el país, los cuales **defensores del ambiente calificaron como insuficientes para garantizar la protección del agua y las comunidades**.

Los autores del informe preliminar plantearon varias asuntos que deberían ser resueltos antes de iniciar una fase de exploración,  que incluye la desconfianza de las comunidades, la falta de información por el Gobierno de los riesgos asociados con el fracking, los desafíos para acceder a información pública, el incumplimiento de la normatividad ambiental y la debilidad de las instituciones de gestión de los territorios sostenibles.

Al respeto, el comité recomendó unos pilotos proyectos científicos en tres municipios del Magdalena Medio para recoger datos de línea base y analizar los riesgos que produzca este forma de explotación no tradicional, generar mecanismos de participación comunitaria en caso de definir fracking comercial y fortalecer las instituciones de seguimiento y control.

Tatiana Roa, vocera de la Alianza Colombia Libre del Fracking, sostuvo que si bien el informe afirma algunas de las preocupaciones que han manifestado organizaciones ambientales y las comunidades, también minimiza la magnitud de las problemáticas. Ahí, Roa resaltó los ataques sistemáticos a los mecanismos de participación ciudadana y las declaraciones del director de la Agencia Nacional de Licencias Ambientales quien afirmó que en Colombia no existe instituciones que implementen controles frente a los megaproyectos.

[“Por un lado, se dice que no hay las condiciones en el país para hacer el fracking pero también se abre todo un camino para que el fracking se haga," dijo la ambientalista. Además, resaltó que el estudio se enfocó exclusivamente en los posibles efectos ambientales y económicos que podrían presentarse en una fase de exploración y que un estudio que incluyera los de una fase de explotación podría demorar años.]

### **Las voces de las comunidades** 

Las comunidades de San Martín, Cesar; Puerto Wilches, Santander; y Barrancabermeja, Santander, los municipios donde se plantea realizar los proyectos pilotos, ya se han pronunciado en contra del uso de esta técnica de explotación en sus territorios. Roa sostuvo que este rechazo por parte de las comunidades se debe a las afectaciones ambientales que estas poblaciones ya han sufrido como resultado de proyectos petroleros.

Según la ambientalista, el desarrollo de estos proyectos en la región resultó en la contaminación de peces y acuíferos subterráneos, hechos que afecta la salud de las poblaciones aledañas y por los cuales, el sector petrolero no ha respondido. "Son temas que llevan que hoy la gente se pregunte: ¿si eso fue con la explotación convencional como va ser con el fracking?"

### **Las recomendaciones de los ambientalistas** 

Frente estos hechos, Roa resaltó que en primer lugar, las conclusiones del informe son cuestionables dado que la mayoría del comité tenían vínculos previos con la industria petrolera y el plazo del informe fue de solo tres meses cuando estudios al respecto de fracking en otros países como Estados Unidos han durado seis años. Y que en segundo lugar, el informe debió incluir una investigación profunda sobre las afectaciones que podrían darse en las aguas subterráneas, la biodiversidad y las dinámicas ecosistémicas en el Magdalena Medio.

\[aesop\_image img="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/WhatsApp-Image-2019-02-15-at-9.18.58-AM.jpeg" panorama="off" align="center" lightbox="on" captionposition="left" revealfx="off" overlay\_revealfx="off"\]

<div class="mceItem aesop-component-short aesop-component" data-mce-placeholder="1" data-aesop-sc="%5Baesop_image%20img%3D%22http%3A%2F%2Fwww.contagioradio.com%2Fwp-content%2Fuploads%2F2019%2F02%2FWhatsApp-Image-2019-02-15-at-9.18.58-AM.jpeg%22%20panorama%3D%22off%22%20align%3D%22center%22%20lightbox%3D%22on%22%20captionposition%3D%22left%22%20revealfx%3D%22off%22%20overlay_revealfx%3D%22off%22%5D">

</div>

Finalmente, asguró que los proyecto pilotos científicos no tienen la capacidad de determinar las afectaciones que se pueden dar a largo plazo como las filtraciones de líquidos contaminados y gases en las aguas[ subterráneas. Mientras tanto, la Alianza Colombia Libre del Fracking solicitó información del informe final, que aún no se ha divulgado al público, para conocer los detalles de esta investigación.]WcMgcq

<div class="mceItem aesop-component-short aesop-component" data-mce-placeholder="1" data-aesop-sc="%5Baesop_image%20img%3D%22http%3A%2F%2Fwww.contagioradio.com%2Fwp-content%2Fuploads%2F2019%2F02%2FWhatsApp-Image-2019-02-15-at-9.18.58-AM.jpeg%22%20panorama%3D%22off%22%20align%3D%22center%22%20lightbox%3D%22on%22%20captionposition%3D%22left%22%20revealfx%3D%22off%22%20overlay_revealfx%3D%22off%22%5D">

</div>

<iframe id="audio_32587484" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32587484_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
