Title: "Acuerdo sobre desminado es muy esperanzador" Pastor Alape, FARC-EP
Date: 2015-03-09 21:29
Author: CtgAdm
Category: Entrevistas, Paz
Tags: acuerdo, delegacion de paz, desminado, desminado en colombia, dialogos de paz, FARC, habana, justicia, minas, pastor alape, paz en colombia
Slug: acuerdo-sobre-desminado-es-muy-esperanzador-pastor-alape-farc-ep
Status: published

###### Foto: Internet 

<iframe src="http://www.ivoox.com/player_ek_4190268_2_1.html?data=lZamkpeafI6ZmKialZWJd6KplZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmq9rg1s7HrcKf1dfO0NjNp8rjz8bZjdnNqc%2FZjNbix5DYtsLi1M7hw9ePrMLXysaYzsaPrtbnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Pastor Alape, Estado Mayor de las FARC-EP] 

El pasado 7 de marzo, la Mesa de Diálogos de Paz entre la guerrilla de las FARC-EP y el gobierno de Juan Manuel Santos, anunció el acuerdo para iniciar el proceso de desminado en el territorio Colombiano. En **entrevista con Pastor Alape**, miembro del Secretariado del Estado Mayor de las FARC-EP y de la Delegación de Paz de esta insurgencia en La Habana, el comandante expuso su punto de vista frente a este y otros temas de debate público.

Según lo expuesto por Alape, "**Este es un acuerdo muy esperanzador**", que se da en el marco del desescalamiento del conflicto para avanzar en la construcción de confianzas, que además contribuyan a generar condiciones de seguridad para los colombianos y las colombianas que se encuentran en áreas operativas.

Cabe recordar que, si bien, la guerrilla de las FARC viene adelantando un cese unilateral al fuego desde el pasado 20 de diciembre del 2014, permitiendo que se reduzcan los niveles de confrontación, la ofensiva por parte del Ejercito no ha disminuido; El Ejercito realizó bombardeos contra campamentos guerrilleros en límites del Chocó y Risaralda en enero, acabando con la vida de dos guerrilleras, según señala Alape. Además se presentan operaciones ofensivas en el Sinú, en San Jorge y en el Cauca, por mencionar algunos. Para el comandante guerrillero, el cese al fuego unilateral se ha mantenido para insistirle al gobierno en que debe ser coherente y dar mensajes acordes a los avances que ha habido en la mesa de conversaciones.

Arturo Alape insiste en que "si hemos avanzado hasta donde estamos, hay puntos críticos del proceso que se podrán resolver". En relación con la Justicia Transicional, para la guerrilla es importante que se tenga en cuenta que el marco jurídico para la paz aprobado por el Congreso el 31 de julio del 2012, se adelantó de manera unilateral sin tener en cuenta a las partes en conflicto, y que por lo tanto, no puede ser la vara con la que se mida un proceso de paz en Colombia: en él, no se contempla el proceso de justicia para "presidentes que ordenaron planes de guerra, políticos que legislaron para la guerra, medios de comunicación que la han azuzado, latifundistas, generales, ministros de defensa, etc. La justicia no puede ser solo para los de ruana", afirmó.

Si se determina, como esperan las FARC, el derecho universal de los pueblos a la rebelión armada, y se debate la solución del conflicto en el marco del delito político, se puede alcanzar una solución de paz y reconciliación al largo conflicto que ha vivido el país. La propuesta de la insurgencia, indica el comandante, se basa en la idea de que "la justicia transicional tiene que transitar hacia la justicia social, no hacia la venganza, no hacia el castigo".

Entonces, el proceso de paz que transforme al país, buscaría dar un tratamiento diferente no sólo al tema de la justicia, sino también al de la lucha antidrogas, al principio de soberanía nacional, el latifundio y la extrangerización de la tierra, etc.

Finalmente, las FARC-EP consideran que "la presencia de Simón Trinidad en la mesa sería un mensaje rotundo de que el proceso no tiene reversa", ayudando fundamentalmente a la convicción de las bases guerrilleras de que el proceso va por buen camino. Los diálogos frente a este punto se están dando hace algunos meses, con la búsqueda de garantías humanitarias para Simón Trinidad, un hombre "sepultado en vida" en los EEUU. "Requerimos que el gobierno se mueva en esa dirección. Estamos esperando respuestas concretas", concluyó Alape.
