Title: Francia amanece bajo la sombra del pánico
Date: 2015-11-14 11:18
Category: El mundo
Tags: Estado Islámico, Terrorismo en Paris, Yo soy Paris
Slug: francia-amanece-bajo-la-sombra-del-panico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/248441-944-629.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Foto:  20minutos.es

Los últimos ataques terroristas en París de este viernes ya dejan 127 muertos y al menos 180 heridos, 80 de ellos de gravedad.

Tras la tragedia, el mandatario francés declaró tres días de duelo nacional. Holand aseguró que Francia está preparada para enfrentar este momento y  dijo que **los ataques habían sido planeados y organizados desde el exterior con ayuda de personas dentro del país.**

**A través de un comunicado el Estado Islámico se atribuye el ataque  asegurando que Francia sigue siendo un "objetivo prioritario"**.

Durante la mañana del  sábado Siria fue el escenario de un encuentro entre el gobierno francés y el presidente sirio Bashar al Assad quien señaló que "Francia conoció ayer lo que vivimos en Siria desde hace cinco años".

Cabe recordar que Francia participa desde comienzos de septiembre en la coalición internacional liderada por Estados Unidos que busca neutralizar las acciones del EI, sin embargo Al Assad ha manifestado la desaprobación de estas actuaciones.

Este sería el peor ataque que se registra en territorio europeo desde los atentados con bombas en Madrid en 2004.

**Este fin de semana 1.500 militares fueron movilizados y desplegados en las calles para apoyar a la policía.**
