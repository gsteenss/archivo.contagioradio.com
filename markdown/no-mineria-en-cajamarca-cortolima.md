Title: No a la minería en Cajamarca: CORTOLIMA
Date: 2018-07-16 17:11
Category: Ambiente, Nacional
Tags: Cajamarca, consultas populares, Corte Constitucional, CORTOLIMA
Slug: no-mineria-en-cajamarca-cortolima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RDSColombia] 

###### [16 Jul 2018] 

Por medio de la resolución 1963 del 2018, la Corporación Autónoma Regional del Tolima (CORTOLIMA), ratificó el cumplimiento de la Consulta Popular, que tuvo lugar el 26 de marzo del 2017, en la que los habitantes de Cajamarca dijeron no estar de acuerdo con que se ejecuten proyectos y actividades mineras en su territorio. (Le puede interesar: ["¡Sí se pudo!: Cajamarca le dijo no a la minería en su territorio"](https://archivo.contagioradio.com/si-se-pudo-pueblo-de-cajamarca/)).

En reconocimiento de dicho mecanismo de participación ciudadana, CORTOLIMA, no otorgará permisos para utilizar recursos naturales con la finalidad de explotación minera en la jurisdicción del Municipio. Adicionalmente, revisará los permisos existentes en la zona una vez se adopte el Esquema de Ordenamiento Territorial (EOT).

De esta forma, la autoridad ambiental del Departamento sienta un precedente sobre las acciones posteriores a la definición de una Consulta Popular que niega la extracción minero-energética en el territorio.(Le puede interesar: ["La discusión sobre las Consultas Populares en la Corte Constitucional"](https://archivo.contagioradio.com/consultas-popular-la-disputa-en-las-altas-cortes/))

Sin embargo, AngloGold Asahanti,  empresa que tenía intereses extractivos en Cajamarca, busca nuevos permisos de exploración, en razón del nuevo fallo que pueda emitir la Corte Constitucional sobre las Consultas Populares, relacionada con el caso Mansarovar Energy Colombia contra el Tribunal Administrativo del Meta.

### **AngloGold Ashanti Colombia prepara nuevas exploraciones en Cajamarca** 

La empresa minera AngloGold Ashanti, que hace un año reconoció por medio de un comunicado el resultado de la Consulta Popular y anunció su retiro de la zona, ahora insiste en un permiso de sustracción de la reserva forestal, para continuar con los estudios de exploración y preparar la fase de explotación minera del territorio.

De acuerdo con Renzo García, defensor de derechos humanos y ambientales de Tolima, este permiso que pretende reactivar [La Colosa](https://archivo.contagioradio.com/consulta-popular-en-cajamarca-pondria-fin-colosa/), hace parte de un plan nacional en el que están involucradas otras grandes empresas interesadas en la explotación de hidrocarburos en Colombia.

Según García, dicho plan se concentra en el fallo que emita la Corte Constitucional sobre el proceso de Masarovar, con lo cual se podrían acabar las Consultas Populares que refieran sobre el sector extractivo de la economía. Sin embargo, resaltó que AngloGold está actuando en contravención de la sentencia T-445 del 2016, y las leyes 1334 de 1994 y 1735 de 2015 que reconocen el mecanismo de la Consulta Popular, como contra la reciente resolución de CORPOTOLIMA.

[Por su parte, la empresa señaló en un comunicado que hay instituciones públicas "resolviendo el alcance e idoneidad de las decisiones tomadas por el mecanismo de la Consulta Popular",  y que mientras eso ocurre, deben seguir desarrollando las actividades económicas naturales de la empresa.]

 [Resolución 1963 de la Corporación Autónoma Regional del Tolima](https://www.scribd.com/document/383947417/Resolucion-1963-de-la-Corporacion-Autonoma-Regional-del-Tolima#from_embed "View Resolución 1963 de la Corporación Autónoma Regional del Tolima on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_45576" class="scribd_iframe_embed" title="Resolución 1963 de la Corporación Autónoma Regional del Tolima" src="https://www.scribd.com/embeds/383947417/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-w0EMmJnzynGnjn6p5PEP&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.8028545941123997"></iframe>  
 

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
