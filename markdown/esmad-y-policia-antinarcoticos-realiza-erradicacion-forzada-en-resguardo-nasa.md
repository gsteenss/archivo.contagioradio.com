Title: ESMAD y Policía antinarcóticos realiza erradicación forzada en Resguardo Nasa
Date: 2020-12-01 11:54
Author: AdminContagio
Category: Actualidad, DDHH
Tags: ESMAD, indigenas, Putumayo
Slug: esmad-y-policia-antinarcoticos-realiza-erradicacion-forzada-en-resguardo-nasa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-01-at-12.30.18-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Archivo Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Este lunes 30 de noviembre al resguardo Nasa El Descanso**, municipio de Puerto Guzmán, departamento de Putumayo, **ingresaron helicópteros de la Policía Antinarcóticos** acompañados de unidades del Escuadrón Móvil Antidisturbios (ESMAD), **con el fin de ejecutar acciones de erradicación forzada en este territorio indígena.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La [Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/con-esmad-policia-antinarcoticos-realiza-erradicacion-forzada-en-resguardo-nasa-en-puerto-guzman/), denunció que sobre las 9:00 am de este lunes, ingresaron 3 helicópteros con cerca de 50 Policías con bombas de espalda, acompañados por el ESMAD, ante esta intervención la [Guardia Indígena](https://archivo.contagioradio.com/matan-uno-nacen-mil-una-comunicadora-nasa-recuerda-a-sus-companeros-asesinados-yoreporto/)del resguardo NASA intentó dialogar con los uniformados, en respuesta y como lo señala la comisión ***"los uniformados atacaron con gases lacrimógenos del ESMAD"***, **en una acción donde resultaron heridos en sus manos y sus pies dos comuneros.**

<!-- /wp:paragraph -->

<!-- wp:image {"id":93508,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-01-at-12.30.18-PM-1024x768.jpeg){.wp-image-93508}  

<figcaption>
Foto: Cortesía Comisión de Justicia y paz

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

El objetivo según la Comisión, del escuadrón de Policía Antinarcóticos era realizar **acciones de fumigación con glifosato a una hectárea y media de los predios de dos familias de la comunidad NASA.** ([Comunidades siguen clamando por un acuerdo humanitario](https://archivo.contagioradio.com/comunidades-reiteran-necesidad-de-un-acuerdo-humanitario/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente sobre la una de la tarde los helicópteros regresaron al territorio y recogieron a los uniformados que habían ejecutado la erradicación forzada, acción que *"continua desconociendo la autonomía territorial de los pueblos indígenas Nasa, así como su **derecho fundamental a la consulta previa libre e informada**".* ([Operativo de erradicación deja a un menor herido en Zona de Reserva Campesina Perla Amazónica](https://archivo.contagioradio.com/operativo-de-erradicacion-deja-a-un-menor-herido-en-zona-de-reserva-campesina-perla-amazonica/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La ejecución de los diferentes operativos de erradicación forzada en los territorios indígenas y campesinos en departamentos como el Putumayo, Cauca, Nariño, entre otros; **agudiza aún más la crisis social y ambiental que enfrentan estos territorios, al tiempo que aumenta *"la desconfianza a las instituciones"* como consecuencia del incumplimiento a los diferentes pactos sociales** en donde no solamente se pone en riesgo el ecosistema sino que también se agrede la vida y la integridad de las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
