Title: INPEC atacó con gas lacrimógeno a recluso que pedía atención médica
Date: 2016-04-28 14:27
Category: DDHH, Nacional
Tags: Bucaramanga, Cárceles en Colombia, presos politicos
Slug: inpec-ataco-con-gas-lacrimogeno-a-recluso-que-pedia-atencion-medica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Recluso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vanguardia ] 

###### [28 Abril 2016 ]

De acuerdo con la más reciente denuncia del 'Equipo Jurídico Pueblos' durante este mes se reportaron **dos episodios de ataques violentos hacia la población reclusa de la ciudad de Bucaramanga**, uno de ellos contra una presa política y el otro contra un grupo de internos. En los hechos se vieron implicados funcionarios del INPEC que actuaron agresiva, ilegal y desproporcionadamente.

El primer hecho ocurrió el pasado 14 de abril cuando nueve internos se encontraban en el Palacio de Justicia esperando sus respectivas audiencias, y ante el llamado que hizo Jerry Rueda, uno de los reclusos para que un médico le atendiera una emergencia odontológica, miembros del INPEC respondieron **lanzando un gas lacrimógeno en un calabozo pequeño y con poca ventilación,** lo que distintas organizaciones han catalogado como un acto de tortura. ** **

El segundo caso se presentó el pasado 18 de abril en la Cárcel de Mujeres el Buen Pastor, en la que Fanny Castellanos, presa política, fue **atacada violentamente por otra de las reclusas con una cuchilla** infectada con sangre. Éste no es el único hecho que se ha presentado en el centro penitenciario, y pese a las denuncias presentadas, **la administración ha hecho oídos sordos, desconociendo sus deberes constitucionales** de proteger la vida e integridad de la población reclusa.

El Equipo asegura que "estas dos agresiones son únicamente los últimos episodios de una **ola de ataques criminales que viene sufriendo la población carcelaria de toda Colombia**", por lo que además de exigir a las autoridades tomar cartas en el asunto, solicita la solidaridad del pueblo colombiano, de las organizaciones populares y defensoras de los derechos humanos, para extender la denuncia por los abusos y las torturas que diariamente se presentan en las cárceles del país.

[![Reclusas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Reclusas.jpg){.aligncenter .size-full .wp-image-23299 width="888" height="500"}](https://archivo.contagioradio.com/inpec-ataco-con-gas-lacrimogeno-a-recluso-que-pedia-atencion-medica/reclusas/)

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

<div>

</div>

<div>

</div>
