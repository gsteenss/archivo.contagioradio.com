Title: De Roux pide a las organizaciones sociales acompañar a la Comisión de la Verdad
Date: 2018-07-06 17:16
Category: Nacional, Paz
Tags: comision de la verdad, Defensoría del Pueblo, Francisco de Roux, lideres sociales
Slug: hay-que-acompanar-la-comision-de-la-verdad-de-roux
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/22_06_francisco_de_roux_arquivo_pessoal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Instituto Humanitas Unisinos] 

###### [6 Jul 2018] 

En el marco del foro "Retos y propuestas frente a personas que defienden los derechos humanos y el territorio", el padre Francisco de Roux, director de la Comisión de Esclarecimiento de la Verdad, pidió que se acompañe a la Comisión que preside, pues **dicha institución tiene como función esclarecer los hechos que tuvieron lugar durante el conflicto armado colombiano, en compañía de las organizaciones defensoras de Derechos Humanos.**

Aunque de Roux aclaró que será imposible escuchar a todas las víctimas, se comprometió a investigar casos emblemáticos que ayuden a determinar patrones presentes “en cada una de las bestialidades que se han cometido en el país” durante la guerra, razón por la que se incluye la persecución a líderes y movimientos sociales.

Para Diana Sánchez, directora de Somos defensores, la importancia de la Comisión de la Verdad reside en que, **“si Colombia llega a saber gran parte de lo que ha ocurrido en el conflicto llegará al punto de no retorno” de la violencia,** razón por la cual, se suma al llamado de proteger, rodear y alimentar la Comisión de la verdad.

### **Líderes Sociales y la Comisión de la Verdad**

Jorge Enrique Calero, Vice-defensor del Pueblo, recordó en medio del foro que la Defensoría del Pueblo había presentado un informe en el que se establecía que, **entre el 1 de enero de 2016 y el 30 de junio del 2018 habían sido asesinados 311 defensores de Derechos Humanos,** siendo Cauca, Antioquia, Norte de Santander y Nariño los departamentos en los que se presentaba el mayor número de homicidios. (Le puede interesar: ["Estado debe reconocer sistematicidad en asesinato a líderes sociales: Somos Defensores"](https://archivo.contagioradio.com/estado-debe-reconocer-sistematicidad-en-asesinato-a-lideres-sociales-somos-defensores/))

De igual forma, la importancia de los líderes sociales para Sánchez, radica en que son fuente de la democracia, y es por esa razón  es que se los ataca violentamente; adicionalmente, de Roux aseguró que los líderes sociales eran personas que “defendieron de forma pacífica a los territorios y sus comunidades y es por esa razón que los están asesinando”, lamentando además que **“en otros países esas personas serían héroes, aquí en Colombia los matamos”.**

Por su parte, Soraya Gutiérrez, Directora del Colectivo de Abogados José Alvear Restrepo, agregó que para que sea posible la Defensa de los Derechos Humanos y el avance de la democracia se debe reconocer la existencia de estructuras paramilitares, así como eliminar prácticas de la fuerza pública y organismos de seguridad del Estado, que reconocen al liderazgo social como un enemigo, lo que justifica su eliminación. (Le puede interesar: ["Con militarización no se soluciona la crisis que afronta el Cauca"](https://archivo.contagioradio.com/el-cauca-no-necesita-militarizacion-necesita-planes-de-sustitucion-indepaz/))

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
