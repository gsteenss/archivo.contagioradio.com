Title: Arranca el Paro Internacional de Mujeres
Date: 2017-03-07 17:25
Category: El mundo, Mujer
Tags: Feminicidios en América LAtina, Paro Internacional de Mujeres, Women Strike
Slug: arranca-el-paro-internacional-de-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Paro-Internacional-de-Mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ElMundo.cr] 

###### [7 Mar 2017] 

Son 55 los países que se han unido a la iniciativa del Paro Internacional de Mujeres, organizaciones de mujeres y feministas han decidido llevar a las calles distintas problemáticas que afectan a mujeres, niñas y jovenes de todo el mundo, para exigir que no haya más impunidad frente a los **casos de violencias basadas en género y feminicidios, que hayan políticas públicas efectivas para proteger los derechos humanos** de las ciudadanas y demostrar que el trabajo de las mujeres es fundamental para el funcionamiento del mundo.

Buena parte de los países de América Latina han propuesto paro de actividades parcial, de 2 a 4 horas, incluso han hecho un fuerte llamado a las mujeres amas de casa para que también paren sus actividades y así mostrar que el trabajo del hogar, no remunerado y poco reconocido, es vital para el día a día. Mientras tanto, mujeres de países europeos, han anunciado que habrá paro de actividades total en varias de las ciudades principales del continente.

### Esta es la Agenda 

Con la consigna 'Si nuestras vidas no importan, que produzcan sin nosotras', países de América Latina realizarán las siguientes jornadas:

-   En Brasil, se movilizaran y harán un paro de actividades de 12:30pm a 1:30pm.
-   En Guatemala habrá un plantón en la Plaza de la Democracia a las 6:00pm.
-   En Panamá la cita es frente a la Procuraduría General de la Nacion a las 6:00pm.
-   En Paraguay, habrá una marcha festival desde la Plaza Uruguaya hasta la Plaza de la Democracia a las 6:00pm.
-   En Uruguay se hará un plantón en la la Plaza de la Independencia a las 6:00pm.
-   En Puerto Rico el plantón será a las 6:00pm en la Plaza de Armas de San Juan de Puerto Rico
-   En Chile el plantón será en la Plaza de Armas de Santiago de Chile a las desde las 5:00pm
-   En Perú, el habrá paro de actividades parciales y los puntos de encuentro serán en dos zonas de Lima,  en el Obelisco  y en la Plaza del Estudiante desde las 4:30pm, para marchar hacia la Plaza San Francisco y llegar a las 6:30pm.
-   En Argentina las ciudades de Bahía Blanca, La Plata, Lomas de Zamora y Buenos Aires, se movilizarán y harán tomas culturales en las plazoletas fundacionales, a partir de las 5:00pm y realizarán paro de actividades parciales de 12:00pm a 2:00pm.
-   En Colombia las ciudades de Manizales, Cali, Barranquilla, Pasto, Medellín, Bogotá, Bucaramanga y Cúcuta, harán movilizaciones por las principales vías, hasta las plazoletas centrales, exceptuando a Bogotá, las mujeres de la capital optaron por 'descentralizar' la protesta y la movilización se realizará en el sur de la ciudad. ([Le puede interesar: Así será el Paro Internacional de Mujeres en Colombia](https://archivo.contagioradio.com/asi-sera-el-paro-internacional-de-mujeres-en-colombia/))

Las organizaciones de los 55 países que participarán en el Paro Internacional de Mujeres, exigen que los gobiernos tomen medidas reales para resolver numerosos problemas relacionados con las violencias basadas en género, **la seguridad, el acceso gratuito a la salud, el establecimiento y aplicación de graves sanciones legales a criminales** en casos de violación, violencia en el hogar y demás tipos de violencias.

Las mujeres colombianas que hacen parte del ALBA, manifestaron que **"nosotras paramos para denunciar que las guerras nos toman como botín, incluso en medio de un proceso de paz, nos siguen asesinando"**, señalaron que el mayor retos es lograr la articulación de los grupos de mujeres y feministas en el país para exigir y velar por la implementación de los Acuerdos de La Habana y los diálogos con el ELN. ([Le puede interesar: Participación Política, el reto de organizaciones de Mujeres y Disidentes del Género](https://archivo.contagioradio.com/participacion-politica-reto-organizaciones-mujeres-disidentes-del-genero/)).

Argentina, Australia, Belgica, Bolivia, Brasil, Canada, Camboya, Chad, China, Chile, Colombia, Republica Dominicana, Korea del Sur, Costa Rica, Republica Checa, Ecuador, El Salvador, Fiji, Finlandia, Francia, Guatemala, Alemania, Haiti, Honduras, Hungaria, Irlanda del Norte, Republica de Irlanda, Israel, Italia, Malta, México, Montenegro, Myanmar, Nicaragua, Pakistán, Panamá, Paraguay, Perú, Polonia, Portugal, Puerto Rico, Rusia, Senegal, Escocia, España, Suiza, Tailandia, Turquia, Ucrania, Uruguay, Reino Unido, USA, Venezuela y Cuba, son los países que han confirmado su participación.

###### Reciba toda la información de Contagio Radio en [[su correo]
