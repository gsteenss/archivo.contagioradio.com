Title: En Potosí, Ciudad Bolívar denuncian posible reactivación ilegal de actividad minera
Date: 2017-02-26 19:47
Category: Ambiente, Nacional
Tags: ciudad bolivar, Mineria, Potosí
Slug: potosi_mineria_ciudad_bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/potosi_ciudad_bolivar-e1488156206782.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [Centro de Memoria, Paz y Reconciliación]

###### [25 Feb 2017] 

Aunque **la Mesa ambiental del Barrio Potosí de la localidad de Ciudad Bolívar le había ganado la batalla a la minería** que quería imponerse en la zona, desde el pasado martes la comunidad viene denunciando acciones con las que pretenden reactivar la actividad minera.

Según la denuncia, el martes 21 de febrero en horas de la mañana, la comunidad observó varios agentes de policía dotados de armas largas que acompañaban a un par de señores que dicen ser los propietarios de la montaña. "Estos sujetos manifiestan la intención de desalojar al vigilante que se encontraba en la casa ubicada cerca al palo del Ahorcado y a las familias que residen desde hace varios años cerca a la entrada del polígono minero. Finalmente estos **señores hacen posesión del territorio con su único sustento jurídico que es un documento radicado en la alcaldía**". Asimismo aseguran que el mismo día en la noche las mismas personas **incendiaron la casa del vigilante, de quien se desconoce el paradero. **

El día 24 de febrero la comunidad que los mismos sujetos cercaron el lugar incluyendo las viviendas de las familias que habitan cerca a la entrada del polígono, y quienes además estarían siendo víctimas de amenazas.

El sábado continuaron aumentó la tensión en la comunidad. **Al parecer se estaba utilizando dinamita con el fin de reactivar la explotación minera.** "Alrededor del barrio se escuchan las fuertes explosiones, esta situación se está presentando a pesar que dicho polígono se encuentra sellado por disposición de la medida cautelar proferida por el Tribunal Administrativo de Bogotá dentro de la acción popular en octubre del 2016", asegura La Mesa Ambiental.

Los pobladores señalan que esos hechos violan las medidas cautelares que **impiden la explotación en el polígono minero 15558 la Esmeralda.** Zona que se encuentra sellada temporalmente gracias a la movilización de la comunidad en contra de la actividad minera.

Frente a esta situación, los habitantes de Potosí exigen **la presencia de las autoridades distritales y nacionales** para garantizar los derechos colectivos e individuales de la comunidad. Además hacen "responsables a los entes distritales y nacionales como a los sujetos que hicieron posesión del territorio (que no se han identificado plenamente con la comunidad) de cualquier afectación a la integridad física y daños medio ambientales que se puedan producir".

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
