Title: "Nacionalización de Campo Rubiales es una felicidad a medias" USO
Date: 2015-03-17 23:37
Author: CtgAdm
Category: Economía, Nacional
Tags: campo, pacific, petroleo, rubiales, USO
Slug: nacionalizacion-de-campo-rubiales-es-una-felicidad-a-medias-uso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/yopal_petroleo1-e1498860896620.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: USO 

<iframe src="http://www.ivoox.com/player_ek_4229098_2_1.html?data=lZefm5WdfI6ZmKiak5mJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmr8bQy9TSpc3d28bQy4qnd4a2lNOYxsqPh8Lh0dSYtNrGrcLgxtiYx9iPuc%2FVjMvSzs7HrcXVxZDOjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Edwin Castaño, Presidente de la USO] 

El Estado colombiano, en cabeza de una de sus empresas más importantes, Ecopetrol, decidió terminar la concesión que había otorgado a Pacific sobre el campo Rubiales, que representa casi 300 millones en reservas entre el 2015 y el 2024.

Para Edwin Castaño, presidente de la Unión Sindical Obrera, el fin de la concesión es ambigua, y la felicidad es "a medias". Según expone el comunicado emitido por Ecopetrol sobre el tema, se abre una ventana para que Pacific continúe explotando el campo a través, por ejemplo, del crudo incremental.

La USO considera que Ecopetrol está preparada técnica, financiera y humanamente para el manejo de crudos pesados, y espera que el Estado garantice las condiciones medio ambientales en el territorio, y los derechos de las comunidades y los trabajadores, que fueron omitidas por Pacific.
