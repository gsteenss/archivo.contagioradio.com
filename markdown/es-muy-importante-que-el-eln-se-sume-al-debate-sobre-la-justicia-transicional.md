Title: “Es muy importante que el ELN se sume al debate sobre la justicia transicional"
Date: 2015-02-26 17:37
Author: CtgAdm
Category: Nacional, Paz
Tags: Conversaciones de paz en Colombia, ELN, justicia transicional, paz
Slug: es-muy-importante-que-el-eln-se-sume-al-debate-sobre-la-justicia-transicional
Status: published

###### foto: cablenoticias 

“Es muy importante que el ELN se sume al debate sobre la **justicia transicional** que se abre con la propuesta del ex presidente Gaviria” señala Luis Eduardo Celis de la Corporación Arco Iris, tras conocerse el comunicado de esa guerrilla publicado en su página de internet el pasado 23 de febrero.

A través de la editorial **“Perdón Eterno para el Terrorismo de Estado”** la guerrilla del **ELN** se pronunció ante la propuesta del expresidente Cesar Gaviria que señala algunos elementos de lo que puede convertirse en una propuesta de Justicia Transicional para Colombia.

Esta guerrilla resalta que por primera vez una persona ligada al “establecimiento” reconoce que la responsabilidad del conflicto recae sobre una parte importante de la sociedad civil, empresarios, políticos y hasta integrantes de la rama judicial que “de una u otra manera han sido también protagonistas de ese conflicto y que tienen muchas cuentas pendientes con la justicia colombiana” cita el comunicado.

Según el analista con esta toma de postura el ELN está ratificando su decisión de que la sociedad participe de manera amplia en las conversaciones de paz, reconoce la importancia de las víctimas y resalta el valor de la verdad histórica como base de construcción de una paz duradera. Todos estos elementos hacen parte de lo que se estaría conversando entre el gobierno nacional y esa guerrilla.
