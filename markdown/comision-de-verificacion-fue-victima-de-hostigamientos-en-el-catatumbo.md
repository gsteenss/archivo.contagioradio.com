Title: Comisión de verificación fue víctima de hostigamientos en el Catatumbo
Date: 2018-05-16 14:55
Category: DDHH, Paz
Tags: "No al Paso", Catatumbo, grupos armados
Slug: comision-de-verificacion-fue-victima-de-hostigamientos-en-el-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/15325287782_6b910b4473_b.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [16 May 2018] 

La misión de verificación de derechos humanos, que realizó la Comisión Por la Vida, la paz y la reconciliación, en la región del Catatumbo, denunció que fue víctima de hostigamientos y de haber quedado desprotegida con población civil, c**uando un grupo armado ilegal les disparó. Además, los campamentos humanitarios que ha recorrido se encuentran en graves condiciones**.

La misión se encontraba ayer visitando el corregimiento de Mesitas en el municipio de Acarí y de Villanueva, en el municipio de San Calixto, cuando se presentaron los hechos. De acuerdo con Ismael López, integrante de la misión de verificación, ser logró constatar **el estado de vulneración que afrontan las comunidades y las constantes violaciones a los derechos humanos**.

Razón por la cual exigen a los grupos armados que se encuentran en el territorio, que se pronuncien frente a los hechos y esclarezcan que fue lo que ocurrió. De igual forma, la comisión cuenta con la participación de un delegado del Ministerio del Interior, un delegado de la secretaría de víctimas del departamento y organizaciones defensoras de derechos humanos y hasta el momento **no se ha dado ningún pronunciamiento desde las autoridades o instituciones encargadas**. (Le puede interesar: ["Catatumbo necesita que se frenen las acciones violentas"](https://archivo.contagioradio.com/catatumbo-necesita-que-se-frenen-las-acciones-violentas-ascamcat/))

### **El avance de la comisión de verificación por la región del Catatumbo** 

Esta comisión se encuentra recorriendo las comunidades de la región del Catatumbo desde el pasado 11 de mayo y finalizará su recorrido el 19 del mismo mes. Hasta el momento las denuncias más graves que han podido verificar tienen que ver con el desplazamiento forzado de las personas hacia campamentos humanitarios y hostigamientos o amenazas contra la población.

De acuerdo con Ismael López, integrante de la comisión y vocero del Comité de Integración Social del Catatumbo, las personas que se encuentran en los campamentos tampoco cuentan con garantías en sus derechos **“las personas se encuentran en hacinamiento, sin alimento, en carpas, en el suelo, sin ninguna logística cuadrada para la dormida o para la habitabilidad**”.

Sumada a esta situación, en la vereda Villa Nueva hay una epidemia de diarrea en menores de edad y en mujeres embarazadas. Para López, las afectaciones que están viviendo las diferentes comunidades de la región del Catatumbo, son producto de la falta de políticas públicas del Estado y del abandono en el que se tiene al territorio.

<iframe id="audio_26015684" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26015684_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
