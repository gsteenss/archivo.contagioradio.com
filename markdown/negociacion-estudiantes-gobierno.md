Title: Así va la Mesa de Negociación entre estudiantes y Gobierno
Date: 2018-11-20 12:37
Author: AdminContagio
Category: Educación, Movilización
Tags: dialogos, Educación Superior, estudiantes, Mesa de negociación
Slug: negociacion-estudiantes-gobierno
Status: published

###### [Foto: Contagio Radio] 

###### [20 Nov 2018] 

Luego de la primera reunión entre estudiantes y Gobierno, tras el restablecimiento de la Mesa de Negociación, **el único acuerdo alcanzado es que no se harán declaraciones a medios en tanto no lleguen a conclusiones**; sin embargo, para los estudiantes, la presencia de la ministra María Victoria Angulo en el escenario de diálogo, es muestra de una voluntad política para tramitar las demandas del movimiento por la educación.

**Para Valentina Ávila, una de las delegadas de la Unión Nacional de Estudiantes por la Educación Superior (UNEES)**, tanto la presencia de la Ministra de Educación en la Mesa, como la de un delegado de la cartera de Hacienda, son muestras de una voluntad política para hacer adiciones presupuestales a la educación superior. (Le puede interesar: ["Con movilizaciones estudiantes presionarán al Gobierno para negociar"](https://archivo.contagioradio.com/movilizaciones-estudiantes/))

Aunque también se cumplieron las solicitudes de los estudiantes de tener presencia de delegados de la Procuraduría y la Defensoría del Pueblo como garantes nacionales, Ávila sostuvo que **la lógica de negociación por parte del Gobierno sigue siendo la misma empleada por el Viceministro de Educación**, razón por la que el único acuerdo logrado fue que no se darían declaraciones a la prensa hasta que se llegarán a conclusiones comunes a todos los actores en la Mesa.

### **Financiación, el primer tema que abordará la Mesa** 

Ávila sostuvo que la agenda de ayer estuvo marcada por el tema financiación, y **todos los sectores que participan en la Mesa presentaron sus propuestas para superar la crisis que atraviesa la educación superior**. Por lo tanto, este martes se ahondarán en estas discusiones, comenzando por el billón de pesos para la educación, que provendría de las regalías. (Le puede interesar: ["Las 3 condiciones de los estudiantes para levantar el paro nacional"](https://archivo.contagioradio.com/condiciones-levantar-paro-nacional/))

Aunque los estudiantes esperan llegar a Acuerdos y conclusiones más profundas, la integrante de la UNEES fue enfática en afirmar que el paro nacional sólo sería levantado en un Encuentro Nacional de Estudiantes por la Educación Superior, luego de llegar a Acuerdos y tiempos de cumplimiento. Entre tanto, el paro se mantiene, y **los escenarios de movilización continuarán el próximo 28 de noviembre, en todo el país**.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
