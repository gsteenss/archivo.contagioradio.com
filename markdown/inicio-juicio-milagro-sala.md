Title: Inició juicio en contra de Milagro Sala
Date: 2016-12-15 15:21
Category: El mundo, Otra Mirada
Tags: Argentina, Juicio, Milagro Sala, Tupac Amaru
Slug: inicio-juicio-milagro-sala
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/5852ca741d3eb_645x362.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Telam 

##### 15 Dic 2016 

En medio de la presión social, nacional e internacional por su libertad, **inició en el Tribunal Oral Federal (TOF) de Jujuy, el juicio oral y público en contra de la Diputada de Parlasur, líder barrial y popular Milagro Sala**, junto  otros referentes de la Red de Organizaciones sociales como son  Graciela López y Ramón Gustavo Salvatierra.

La dirigente de la organización Tupac Amaru, encarcelada desde inicios de enero de este año, aseguró en la primera jornada que "es mentira que organicé la protesta contra Gerardo Morales", ocurrida en 2009 y que es una de las causas con las que se argumentó su dentención, añadiendo que se habia enterado de la manifestación en mención a través de medios de comunicación.

Sala, afirmó en la primera de cuatro audiencias que "**la bronca de Morales es por que soy peronista y porque he decidido militar y comenzar a defender las banderas de la dignidad** y porque no quise ir con ellos cuando me invitaron a militar", rencor que asegura  en menos de seis meses se valió llenarse de tantas causas judiciales en su contra.

"Yo lo he perdonado" aseguró la dirigente social e indígena sobre Morales "**no le tengo rencor, no le tengo bronca, pertenezco a los pueblos originarios y nos enseñaron que a las personas que hacen daño las perdonamos internamente**". Sala ha sostenido que no tuvo nada que ver con el escrache ocurrido en Jujuy contra el actual Gobernador de Jujuy y Leandro Despouy ".

Sala, quien se encuentra recluída en el penal de mujeres del barrio Alto Comedero, manifestó que la acusación le ha causado "ocho años de persecución" y que apesar de perdonar a Morales por su acusación lo que le duele " es que mientan porque tengo familia".

De acuerdo con lo progamado en el Tribunal, l**as jornadas de audiencia continuarán los días 18, 20 y 21 de diciembre.** Le puede interesar: [Naciones Unidas pide libertad para Milagro Sala](https://archivo.contagioradio.com/naciones-unidas-pide-libertad-para-milagro-sala/)
