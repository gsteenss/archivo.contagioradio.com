Title: Asesinan a excombatiente  de Farc, Carlos Alberto Montaño en Cali
Date: 2019-07-26 15:54
Author: CtgAdm
Category: DDHH, Nacional
Tags: ex combatientes
Slug: asesinan-a-excombatiente-de-farc-carlos-alberto-montano-en-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Asesinado-integrantes-de-las-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

 

El 24 de julio fue asesinado Carlos Alberto Montaño Mosquera excombatiente del antiguo grupo guerrillero FARC- EP. El hecho ocurrió en el barrio El Rodeo de Cali (Valle del Cauca), hasta el momento se desconoce los autores del crimen. (Le puede interesar: [Asesinato de excombatientes, el gran riesgo para el Acuerdo: Defendamos la Paz)](https://archivo.contagioradio.com/gran-riesgo-acuerdo-paz/)

Carlos Alberto, era desmovilizado, y había estado preso por su actividad en el frente Manuel Cepeda Vargas de las extintas Farc-Ep. No obstante, en el marco de los Acuerdos de Paz había recobrado la libertad; en el momento del asesinato se encontraba trabajando en un taller de soldadura.

Carlos se suma a una cifra de 138 ex combatientes asesinados desde la firma del Acuerdo de paz, según IndePaz (Instituto de Estudio para el Desarrollo y la Paz). En meses pasos Carlos había logrado salir ileso de un atentado en el mismo sector y estaba recuperándose del mismo. (Le puede interesar: [Asesinan a excombatiente de FARC, Jorge Enrique Sepúlveda en Córdoba](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-farc-jorge-enrique-sepulveda-en-cordoba/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
