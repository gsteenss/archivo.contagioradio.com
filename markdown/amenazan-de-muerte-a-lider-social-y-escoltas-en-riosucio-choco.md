Title: Amenazan de muerte a líder social y escoltas en Riosucio, Chocó
Date: 2018-01-04 11:59
Category: DDHH, Nacional
Tags: Amenaza a líderes sociales, Chocó, defensores de derechos humanos, Riosucio
Slug: amenazan-de-muerte-a-lider-social-y-escoltas-en-riosucio-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/agc5.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunidad de paz de San José de Apartadó] 

###### [04 Ene 2018] 

De acuerdo con la información de la Comisión Intereclesial de Justicia y Paz, el 3 de enero de 2018, cuatro integrantes de las autodenominadas AGC, amenazaron de muerte al líder social **Yeison Mosquera**.

A las 4:00 pm en el casco urbano del municipio de Riosucio en el departamento de Chocó, los hombres de las **AGC abordaron el esquema de la Unidad Nacional de Protección** y a Yeison Mosquera en el barrio El Paraíso. Allí, “le expresaron que lo iban a matar por sus denuncias”. La organización manifestó que, “entre los autodenominados Autodefensas Gaitanistas de Colombia, AGC, se encontraba el llamado ‘Guajiro’, quien se encontraba en estado de embriaguez”.

### **Operaciones de las AGC se realizan a la vista de las autoridades civiles** 

Las diferentes organizaciones que acompañan a estas comunidades han manifestado que las operaciones paramilitares “se llevan a cabo se realizan a la vista de autoridades civiles, como de la **fuerza pública en Río Sucio**. Cuentan con dos oficinas de despacho en Las Casitas y Villa Rufina sin que ninguna autoridad en los últimos cinco años haya hecho algo”. (Le puede interesar: ["Aumentan las amenazas contra líderes sociales del Bajo Atrato y Urabá"](https://archivo.contagioradio.com/aumentan-las-amenazas-contra-lideres-sociales-del-bajo-atrato-y-uraba/))

Además, indican que las AGC se movilizan **vestidos de civil en todo el casco urbano** de Riosucio y “a pesar del reciente compromiso del gobierno de Colombia de asegurar la vida y libertad de los líderes sociales del bajo Atrato, las medidas son ineficaces”.

Cabe resaltar que más de **siete líderes se vieron obligados a salir** de sus territorios en los últimos días y “la ofensiva paraempresarial continúa desarrollándose sin que la estructura criminal sea desmontada en Curvaradó, Jiguamiandó, La Larga y Tumaradó y Pedeguita y Mancilla.” (Le puede interesar: ["Los empresarios son los que están detrás de los asesinatos": líderes del Bajo Atrato y Urabá"](https://archivo.contagioradio.com/hasta-cuando-y-cuantos-mas-continuan-la-violencia-contra-lideres-sociales-del-bajo-atrato-y-uraba/))

Yeison Mosquera es uno de los **25 líderes sociales amenazados en esa región** y quienes en repetidas ocasiones han hecho pública la situación que están viviendo los defensores de derechos humanos. Por esto, se han reunido con diferentes instituciones en busca de una solución que proteja la vida de estas personas.

###### Reciba toda la información de Contagio Radio en [[su correo]

 
