Title: ¡Estos son los finalistas del Premio Nacional a la Defensa de los Derechos Humanos!
Date: 2018-08-28 12:37
Category: DDHH, eventos
Tags: lideres sociales, Premio a la defensa de los derechos humanos
Slug: finalistas-premio-nacional-a-la-defensa-de-los-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/premio-nacional-derechos-humanos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Diakonia] 

###### [28 Ago 2018] 

El próximo **4 y 5 de Septiembre**, se celebrará la séptima edición de **Premio Nacional a la Defensa de los Derechos Humanos**. Un reconocimiento organizado por Diakonia Colombia, que busca visibilizar el aporte de los y las defensoras en la construcción de democracia y paz en el país.

En medio de la difícil situación de amenazas y muerte, por la que atraviesan los líderes sociales en la actualidad, el representante de Diakonia y cabeza del evento, **Cesar Grajales**, afirma que "el premio sigue siendo relevante" e insiste en que los defensores de la comunidad son un aporte a la sociedad, e importantes consolidadores de paz.

Además, Grajales asegura que **la comunidad internacional ha sido un "blindaje"** para los defensores, y resalta el apoyo del Alto Comisionado de Derechos Humanos de la ONU y la Unión Europea, reconociendo su fidelidad a los proyectos comunitarios desarrollados en Colombia.

La organización anunció este martes, los **finalistas en las 4 categorías** galardonadas y son:

**CATEGORÍA 1: "DEFENSOR O DEFENSORA DEL AÑO"**

**Edwin Mauricio Capaz**, coordinador de derechos humanos de la Asociación de Cabildos Indígenas del Norte del Cauca; **Luz Perly Córdoba**, madre soltera con ascendencia campesina e indígena, presidenta de la Asociación Campesina de Arauca, coordinadora Nacional de cultivadores de Coca, Amapola y Marihuana (CCOCAM); y **Germán Graciano Posso**, Perteneciente al consejo interno de la Comunidad de Paz de San José de Apartadó

**CATEGORÍA 2A: "EXPERIENCIA O PROCESO COLECTIVO DEL AÑO (PROCESO SOCIAL COMUNITARIO)"**

**Narrar para vivir**, una comunidad de 840 mujeres víctimas del conflicto armado en Montes de Maria que crean alternativas de sanación y resistencia a la violencia; **Junta de Gobierno de la Comunidad Negra/Afrodescendiente de Alto Mira y Frontera** (Nariño), quienes afirma y defienden los derechos y territorios de su comunidad en Tumaco; **Consejo Comunitario Alto Mira y Frontera**, organización defensora de los derechos étnicos mediante trabajo comunitario.

**CATEGORÍA 2B: "EXPERIENCIA O PROCESO COLECTIVO DEL AÑO (MODALIDAD DE ONG)"**

**Red de Mujeres del Magdalena Medio**, con mas de 114 grupos y organizaciones que luchan por la vida y dignidad de las mujeres de 26 municipios de Magdalena Medio; **Fundación Nydia Erika Bautista**, dedicados a la protección de los derechos de mujeres y familiares de víctimas de la desaparición forzada; **Movimiento Ríos Vivos Antioquia**, conformada por 15 organizaciones con enfoque Socio Ambiental, que procuran promover acciones no violentas para la resolución de conflictos y cuidado de los recursos naturales.

**CATEGORÍA 3: RECONOCIMIENTO A TODA UNA VIDA**

**Miguel Fernández**, docente y líder campesino y sindical, secretario de la Centra Unitaria de Trabajadores del Cauca y Coordinador del Comité de Integración del Macizo Colombiano; **Maria Ligia Chaverra**, lideresa pacifista de Curvaradó Chocó y represéntate de la Zona Humanitaria Las Camelias en el municipio de Carmen del Darien, Chocó; **Nevaldo Perea**, líder afrocolombiano, quien ha dedicado 35 años de su vida a procesos de reconocimiento de los derechos de las comunidades; líder del Consejo Comunitario Mayor de la Asociación Campesina Integran de Atrato.

El Programa Colombia de Diakonia, en conjunto con la Iglesia Sueca, ha otorgado este premio desde el año 2012 como un mecanismo de respaldo al trabajo que realizan los defensores de derechos humanos y del territorio en el país. Como en años pasados, el reconocimiento a los hombres, mujeres y organizaciones que contribuyen a la democracia y la paz se hará como parte de un mecanismo de protección ante la alarmante situación de riesgo en la que viven estas personas en Colombia.

El día 4 de Septiembre se llevarán a cabo dos conversatorios referentes al evento y el día 5 se llevará a cabo la premiación a partir de las 8:30 de la mañana.

<iframe id="audio_28142130" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28142130_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
