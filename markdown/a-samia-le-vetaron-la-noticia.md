Title: A Samia le vetaron la noticia
Date: 2015-03-02 16:00
Author: CtgAdm
Category: Eleuterio, Opinion
Tags: Afganistán, Pakistán, RAWA
Slug: a-samia-le-vetaron-la-noticia
Status: published

###### Foto: ww4report 

##### Por [[Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)] 

En 1998 comienza el proceso para extraditar a España al sanguinario dictador chileno Augusto Pinochet para juzgarlo por crímenes contra la humanidad; proceso que nunca se llevó a acabo. El mismo año el presidente estadounidense Bill Clinton admite haber mantenido relaciones sexuales con la famosa becaria Mónica Lewinsky y ordena bombardear Irak. El comandante Hugo Chávez recorre Venezuela en campaña electoral y Milosevic inicia una nueva guerra en los Balcanes al atacar Kosovo. En Francia se habla del fin del racismo porque la selección de fútbol gana el Mundial con un equipo repleto de jugadores de las excolonias y capitaneado por Zidane, de ascendencia argelina. Al portugués José Saramago, que se convertiría poco después en un referente del pensamiento antiglobalización, se le concede el premio nobel de literatura.

Lejos de todas estas noticias que uno encuentra cuando visita el anuario de aquel año, nos detenemos en el caso de un intento de noticia que nunca llegó a ser tal.

Es 1998, en Afganistán los talibanes han llegado al poder recientemente. Han sustituido a los muyahidines, grupos de fundamentalistas religiosos apoyados por occidente para causar problemas a la URSS, que tras la caída del muro de Berlín, accedieron finalmente al poder. No obstante, la CIA y sus aliados no están del todo contentos con los muyahidines y deciden dar una oportunidad a los grupos talibanes. Estos se hacen rápidamente con el control del país e instauran un régimen que [ejerce una terrible represión de carácter fundamentalista, sobre todo contra las mujeres. Entre otras cosas se les prohíbe la educación, no pueden trabajar fuera de casa y están obligadas a llevar el burka cuando salen a la calle. También proliferan las ejecuciones públicas.]{.s2}

Llegamos entonces a la historia de Samia. Esta joven afgana se preparó como cada día para salir a la calle enfundándose su burka. Tenía la intención de asistir a una de estas ejecuciones en una plaza de Kabul, iban a ajusticiar a una mujer. Bajo el vestido que la prohibía como mujer, Samia escondía una pequeña cámara de video, quería grabarlo todo. Cabe decir que durante los años de ocupación soviética, durante la época de los muyahidines y también bajo el gobierno talibán, existió siempre una resistencia civil que luchó en la clandestinidad. Entre sus actividades, la de mujeres que enseñaban a otras a leer y escribir en los sótanos de sus casas. La RAWA (Asociación Revolucionaria de Mujeres de Afganistán) era y es, parte de esa resistencia a la que nuestra compañera Samia pertenece.

Aquella mañana en la plaza pública, Samia grabó entre los agujeritos de su burka la ejecución de una compañera. Lo hizo rodeada de una multitud agresiva, con miedo a ser descubierta pero con el pulso firme. Días después emprendió un complicadísimo viaje para salir de su país hacia el vecino Pakistán. Se jugó la vida para hacer llegar su pequeña grabación a las grandes agencias de noticias internacionales. Cuando el video llegó a sus manos, la BBC y CNN rechazaron la grabación por considerar que resultaba demasiado dura y podía herir la sensibilidad de la opinión pública occidental. No interesaba, no era de buen gusto. Samia volvió a casa con la sensación de que los defensores del mundo libre le habían echado encima otro burka; a ojos del mundo las mujeres afganas no existían.

Durante 5 años los medios occidentales nunca hablaron de la realidad atroz del gobierno talibán en Afganistán. Eso sí, todos cambiaron radicalmente de opinión después del 11 de septiembre de 2001.
