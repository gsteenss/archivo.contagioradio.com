Title: Los desafíos de las defensoras ambientales en Colombia
Date: 2019-05-22 18:21
Author: CtgAdm
Category: Ambiente, Mujer
Slug: los-desafios-de-las-defensoras-ambientales-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-101.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONU Mujeres] 

Lideresas ambientales de diversas regiones del país y América Latina se reunieron ayer en Bogotá, en una conferencia convocada por ONU Mujeres, ONU Ambiente y la Fundación Natura, para compartir sus experiencias en la  defensa de los recursos naturales, así como discutir el rol de las mujeres en la mediación de conflictos socio-ambientales y el impulso del desarrollo sostenible.

Clemencia Carabalí, integrante de la Asociación de Mujeres Afrodescendientes del Norte del Cauca, manifestó que estos espacios de [intercambio "son muy valiosos, y que sin duda alguna, contribuyen a fortalecer nuestro trabajo y nuestros lazos de solidaridad"; y así como Carabalí compartió sus experiencias en el Norte del Cauca, capacitando a niñas y jóvenes Afrodescendientes en la defensa de la vida y el territorio, mujeres de Panamá y Ecuador también hablaron de sus realidades en los territorios.]

[En esta región del Cauca, lideresas Afrodescendientes han luchado para proteger el ambiente de la minería, los monocultivos y los cultivos de uso ilícito que se convierten en amenazas para el río, los cultivos de pan coger y la salud de las comunidades. En particular, Carabalí enfatizó que se han registrado casos de cáncer en mujeres jóvenes cuando antes no se conocía esta enfermedad en la región.]

### **Falta de garantías para defensoras ambientales**

Dentro el marco de este conversatorio, la líder resaltó la falta de garantías de seguridad que brinda el gobierno del Presidente Iván Duque a las defensoras del ambiente.  Según su testimonio, en la reunión que sostuvo con el primer mandatario, la lideresa lo cuestionó directamente por desaprovechar el momento histórico para consolidar la paz en los territorios y dar respuesta a otra problemáticas que afrontan las comunidades.

"Una de las cosas que nos manifestó en esa reunión era que estaba buscando la manera de garantizarnos un ambiente seguro", manifestó Carabalí, a lo que la defensora respondió que la **concesión de títulos mineros, desconocimiento de la consulta previa y la falta de voluntad política para implementar los Acuerdos de Paz bajo su Gobierno ponen en riesgo a las mujeres defensoras** del territorio. (Le puede interesar: "[Informe revela aumento en violencias contras las lideresas en Colombia](https://archivo.contagioradio.com/62024-2/)")

Cabe resaltar que Carabalí sufrió un atentado al lado de otros líderes y lideresas Afrodescendientes del Norte del Cauca, incluyendo la reconocida ambientalista Francia Márquez. Carabalí afirmó que grupos armados, interesados en explotar los recursos locales, agreden contra ambientalistas de la región por exigir sus derechos y proteger el terreno. (Le puede interesar: "[Atentan contra Francia Márquez y otros líderes de comunidades negras del Norte del Cauca](https://archivo.contagioradio.com/atentan-contra-lideresa-francia-marquez/)")

<iframe id="audio_36248103" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36248103_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
