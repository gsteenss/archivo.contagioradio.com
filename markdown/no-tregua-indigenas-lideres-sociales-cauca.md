Title: No hay tregua para indígenas y líderes sociales en Cauca
Date: 2018-12-10 12:26
Author: AdminContagio
Category: DDHH, Nacional
Tags: Asesinato de indígenas, lideres sociales
Slug: no-tregua-indigenas-lideres-sociales-cauca
Status: published

###### Foto: ONIC 

###### 10 Nov 2018 

Continúa el asesinato de líderes sociales e indígenas en el Cauca. El pasado 7 de diciembre, hombres armados acabaron con la vida de Edwin Dagua, gobernador del resguardo de Huellas, Caloto, a quien se suma el líder Gilberto Antonio Zuluaga, también asesinado el 9 de diciembre de 2018 en  el municipio de Corinto.

### **Gilberto Antonio Zuluaga Ramírez** 

Según el informe de la Red de Derechos Humanos Francisco Isaías Cifuentes, el 9 de diciembre, el líder social **Gilberto Antonio Zuluaga Ramírez** se encontraba cerca del mediodía sobre el paradero de vehículos que cubren la ruta interveredal al Corregimiento Los Andes, cuando un individuo lo abordó y le disparó con arma de fuego en la cabeza acabando con su vida en el lugar de los hechos.

Zuluaga, de 55 años de edad, hacia parte de la Asociación de Trabajadores Campesinos de la **Zona de Reservas Campesinas de Corinto (ASTRAZONAC)**, además, era integrante de la Guardia Campesina y de la Coordinación Social y Política Marcha Patriótica. A pesar de que no existían amenazas en su contra, para los pobladores es de conocimiento que en la zona hay presencia del EPL, el ELN y disidencias de las Farc.

**Cristóbal Guamanga**, integrante de FENSOAGRO y ANSORC, asegura que existe mucha preocupación en la comunidad pues aunque hay presencia del Ejército y la Policía, continúan los asesinatos, los desplazamientos y las amenazas en contra de las comunidades, no solo contra los líderes sino contra los habitantes que apoyan la sustitución de cultivos de uso ilícito.

“Las cosas están más complicadas de lo que creemos, antes podíamos ir a las veredas y a los municipios a hacer reuniones con los indígenas, pero ahora hay muchas amenazas” señala Guamanga quien también denunció la muerte de dos miembros de la comunidad de Caloto el mismo día que fue asesinado el líder Zuluaga. [(Le puede interesar En los primeros 9 meses de 2018 fueron asesinados 109 líderes sociales)](https://bit.ly/2Qn4Eel)

### **Edwin Dagua Ipia** 

En el mismo departamento, el pasado 7 de diciembre el gobernador indígena del resguardo Huellas Caloto, Edwin Dagua Ipia de 28 años fue asesinado por hombres armados que dispararon contra su humanidad mientras se encontraba en zona rural de la vereda La Buitrera, en la vía que conduce hacia El Palo.

Por medio de un comunicado del pasado mes de  julio, la ONIC solicitó protección al Estado para Dagua y otros líderes indígenas que habían recibido panfletos amenazantes de las **Águilas Negras.** Nelson Pacue, integrante de ACIN indicó que además de la recibida por parte de esa agrupación, Dagua había sido amedrentado por diversos grupos armados, en su mayoría por sus mandatos en la comunidad y el tema de control territorial.

Solo una semana atrás, Dagua había recibido un esquema de seguridad de la **Unidad Nacional de Protección**, sin embargo, al encontrarse al interior del resguardo el gobernador consideró que no habría peligro pues se respetaría el territorio indígena, motivo por el cual se encontraba desprovisto del esquema de protección al momento cuando fue atacado.

Ante la alarmante situación que se vive en los resguardos indígenas, la ONIC viene solicitando al Gobierno que brinde garantías de seguridad y celeridad a las investigaciones, mientras que a la comunidad internacional solicita se realice una misión de verificación de la situación humanitaria. **Con Dagua son ya 36 los indígenas asesinados durante el Gobierno de Iván Duque.**

<iframe id="audio_30703638" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30703638_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
