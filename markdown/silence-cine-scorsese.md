Title: Silence: mucho más que una película perfecta para semana santa
Date: 2017-04-12 17:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Scorsese, Silence
Slug: silence-cine-scorsese
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/unnamed-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Paramount Pictures 

##### Por: Jake Sebastián Estrada Charris / @24cuadrosradio 

¿Hasta qué punto es posible quebrar el espíritu y la voluntad humana? El legendario director Martin Scorsese responde a esta pregunta de una manera inesperada en ‘Silence’, su más reciente obra, proponiendo a través de metáforas, imágenes estéticamente hermosas, planteamientos propios en la filosofía oriental y occidental, que el espíritu puede trascender las imposiciones terrenales.

Hay quienes pensarán que los crímenes de la iglesia católica no se quedan atrás (La Santa Inquisición o Las Cruzadas por mencionar solo algunos de ellos), pero el discurso de la película basada en la aclamada novela del escritor Shūsaku Endō en donde se denuncia la persecución de los católicos en Japón que eran obligados a cometer apostasía (renunciar a su fe) y convertirse al budismo, no es absolutista; es decir, no obliga al espectador a odiar a una cultura determinada y eso la hace bella. Es disfrutable tanto para un ferviente feligrés, como para una persona indiferente hacia la religión.

![Silence 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Silence-2.jpg){.alignnone .size-full .wp-image-39109 width="1200" height="800"}

Silence es una película que duró más de 23 años en realizarse. Scorsese quería hacer la cinta, pero planeaba dar lo mejor de sí y esperó mientras dirigía producciones como El Lobo de Wall Street (2013) y Pandillas de Nueva York (2002). También mientras estuvo al frente de documentales musicales aclamados tales como No Direction Home: Bob Dylan (2005) o series de televisión de la talla de Boardwalk Empire (2010). De igual manera, esperó a entregar cortometrajes como The Key To Reserva (2007); actuó en algunas de sus películas (Hugo en 2011); en cintas que no eran de su autoría (El Espanta Tiburones en 2004) e incluso le dio tiempo hasta para ejercer de productor (Nyfes en 2004).

Al ver Silence, podemos darnos cuenta que la espera valió la pena y que es por sobre todas las cosas una cinta hecha con un equipo de trabajo envidiable: La fotografía de Rodrigo Prieto; acompañada de la música de Kim Allen Kluge y Kathryn Kluge; el toque personal de Jay Cocks y Thelma Schoonmaker; que están acompañados de las actuaciones maravillosas de Andrew Garfield, Adam Driver, Liam Neeson, Yôsuke Kubozuka e Issei Ogata. Todos estos nombres hacen que el sello de Martin Scorsese nos asegure que el cine y el arte necesitan más piezas de este estilo.

En el momento de escribir estas líneas es semana santa, una época en la que ver Silence cae como anillo al dedo; pero no solo es perfecta para estos tiempos, porque se encuentra muy lejos de estar casada con el credo; al contrario, lo hace con la espiritualidad y eso no deja de ser polémico. Algo similar ocurre con pasados trabajos del director de 74 años como La última Tentación de Cristo (1988) y Kundun (1997).

Si finalmente los conceptos de trascendencia y religiosidad no los convencen, podemos decir que más que un ejercicio de memoria, Silence recrea el dolor de la imposición de la religión, sea como se llame y en las presentaciones que se deseen durante momentos históricos diferentes (matanza de cristianos en el imperio romano, prohibición de religiones indígenas por parte de conquistadores europeos y guerras “santas” que lastimosamente aún tienen lugar en el mundo). Al fin y al cabo, todos tienen ese común denominador: la violencia de las prometidas religiones verdaderas hacia las que aparentemente son falsas. Le puede interesar: [La sociedad ´del Animal´](https://archivo.contagioradio.com/la-sociedad-del-animal/)

<iframe src="https://www.youtube.com/embed/z3N-X9GJ5wc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
