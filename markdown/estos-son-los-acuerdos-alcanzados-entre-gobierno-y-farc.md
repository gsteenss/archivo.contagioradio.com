Title: Estos son los acuerdos alcanzados entre Gobierno y FARC
Date: 2016-08-24 18:45
Category: Nacional, Paz
Tags: acuerdos de paz, conflicto armado, Gobierno, paz, proceso de paz
Slug: estos-son-los-acuerdos-alcanzados-entre-gobierno-y-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Caminata-por-la-paz-60.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo, Contagio Radio] 

###### [24 Ago 2016] 

Este miércoles  Colombia vive un día histórico. El presidente Juan Manuel Santos anunció la terminación de las conversaciones y el cierre de negociaciones entre el gobierno y la guerrilla de las FARC luego de 4 años de diálogos en La Habana Cuba. Ahora el reto que se avecina es lograr la votación del Plebiscito por la Paz, para lo que es esencial conocer los acuerdos, de manera que el voto sea una decisión informada.

Aquí le presentamos los acuerdos alcanzados.

### **Política de desarrollo agrario integral**

Este acuerdo se pactó el 26 de mayo de 2013. A través de él se busca sentar las bases para la transformación del campo de manera que exista condiciones dignas para la vida de los campesinos y campesinas, una de las causas estructurales por las cuales inició el conflicto armado en Colombia. Es por ello que con este punto, se promoverán garantías de acceso a la tierra, además se contemplan acciones de gran escala para proveer bienes y servicios públicos en materia de: Infraestructura y adecuación de tierras, Desarrollo social, y estímulos a la productividad. Así mismo, se habla de programas de desarrollo con enfoque territorial, para poner en marcha planes nacionales con mayor celeridad en las regiones más afectadas por el conflicto, pobreza, poca presencia de la institucionalidad y la presencia de economías ilegales.

<iframe id="doc_55291" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322098866/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

### 

### **Participación política**

Firmado el 6 de noviembre de 2013, este punto pretende fortalecer la participación de todas y todos los colombianos en la política, los asuntos públicos y la construcción de la paz, lo que significa que se dará la posibilidad de dar poder político a nuevas voces facilitando la creación de nuevos partidos políticos y fortaleciendo los mecanismos para promover la transparencia en los procesos electorales así como promover una mayor participación electoral. Además, para dar garantías a la oposición se busca romper con el vínculo entre política y armas.

<iframe id="doc_56762" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322099166/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

### 

### **Solución al problema de las drogas ilícitas**

Dado a conocer en La Habana, el 16 de mayo de 2014. Con este acuerdo se busca una solución definitiva al problema de las drogas ilícitas. En ese sentido, se promoverá la sustitución voluntaria de los cultivos de uso ilícito y la transformación de los territorios afectados, dando la prioridad que requiere el consumo bajo un enfoque de salud pública e intensificando la lucha contra el narcotráfico.

<iframe id="doc_81382" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322099343/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

   
**Víctimas del Conflicto armado**

A través de este acuerdo, firmado por las partes el 15 de diciembre de 2015. Se anunció la implementación de un Sistema Integral de Verdad, Justicia, Reparación y No Repetición, para lo cual se estableció la creación de la Comisión para el Esclarecimiento de la Verdad, la Convivencia y la No Repetición; una Unidad para la Búsqueda de Personas dadas por Desaparecidas; Medidas de reparación integral para la construcción de paz; y la Jurisdicción Especial para la Paz, que estará conformada por magistrados y magistradas elegidos por el papa Francisco, el secretario general de Naciones Unidas Ban Ki-moon, fueron invitados a nombrar delegados para la comisión que elegirá a esos jueces, la** **Corte Suprema de Justicia, el Centro Internacional de Justicia Transicional y la Comisión Permanente del Sistema Universitario del Estado.

<iframe id="doc_24965" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322099490/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

   
**Fin del Conflicto**

Este punto se acordó el pasado 23 de junio de 2016, donde se concretó el Cese al fuego y de hostilidades bilateral y Definitivo y Dejación de Armas, que tiene como objetivo la terminación definitiva de las acciones ofensivas entre la Fuerza Pública y las FARC evitando que los acuerdos se rompan, se afecte a la población civil, a la Fuerza Pública o a las FARC. En total son 23 protocolos y 2 anexos con procedimientos, fechas, criterios, medios, reglas y responsables de cómo se  llevarán a cabo los desplazamientos, ubicación de las estructuras de las FARC, el funcionamiento de las zonas veredales de transición, la dejación de las armas de la guerrilla, la seguridad, el monitoreo y la verificación, así como la logística.

<iframe id="doc_10076" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322099972/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>En este punto también se discutió el punto sobre las garantías de seguridad, por lo que el gobieerno deberá coordinará la revisión de la situación de las personas privadas, procesadas o condenadas por pertenecer o colaborar con las FARC. En forma paralela,  intensificará el combate para acabar la organizaciones criminales y sus redes de apoyo, esto incluye la lucha contra la corrupción y la impunidad, en particular contra cualquier organización responsable de homicidios y masacres o que atente contra defensores de derechos humanos, movimientos sociales o movimientos políticos. Así mismo, se revisará y hará las reformas y los ajustes institucionales necesarios para hacer frente a los retos de la construcción de la paz y en el marco de lo establecido en el punto 5 (Víctimas) se esclarecerá, entre otros, el fenómeno del paramilitiarismo.

<iframe id="doc_46695" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322100184/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

### **Implementación, verificación y refrendación**

La firma del Acuerdo Final da inicio a la implementación de todos los puntos acordados. En ese sentido, se acordó frente a la refrendación, el mecanimos que la Corte Constitucional apruebe, como lo fue el Plebiscito como una medida de consulta popular consagrada en la Constitución del 91 y, según el artículo 7 de la Ley 134 de 1994, es el pronunciamiento del pueblo convocado por el presidente de la República mediante el cual apoya o rechaza una determinada decisión del Ejecutivo.

Por otra parte, frente a la implementación y verificación de los acuerdos, la ONU creó una Misión luego de una petición unificada del Gobierno Nacional y las FARC. Esta estará compuesta por un grupo de 500 observadores** **militares desarmados que llegarán de varios países del mundo. El 85 % provienen de Latinoamérica y además se contará con la participación de 150 civiles, que ha iniciado su arribo al país para estar preparados una vez se firme el acuerdo final.

Los observadores tendrán presencia en las 23 zonas veredales y los ocho campamentos donde se agruparán los integrantes de la guerrilla como garantía de la paz y los acuerdos entre el Gobierno y las FARC.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
