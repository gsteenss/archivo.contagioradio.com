Title: “¡Las ideologías murieron!” … ¿me lo dices tú, desde tú ideología?
Date: 2016-02-09 15:02
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: ideologias, izquierda colombiana, medios de comunicación
Slug: las-ideologias-murieron
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/caricatura-ideologias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: neoliberate 

#### [**[Johan Mendoza Torres ](https://archivo.contagioradio.com/johan-mendoza-torres/)**] 

[Para nadie es un secreto que los debates contemporáneos sobre política, están marcados por una ausencia de profundidad tan mezquina, que uno no sabe si sentarse a reír o a llorar; escuchamos, vemos y leemos tantas cosas tan superfluas y cuanta pendejada se inventan en las famosas redes sociales, que a veces el dilema para saber qué es lo que falta se vuelve más complejo.]

[Las preguntas emergen para intentar saber si lo que falta es memoria histórica, clases de analítica, aprender a leer o aprender a escribir, no obstante, cada vez más, pareciera que aquellos que creyeron firmemente en el grito]*[“¡Las ideologías murieron!]*[”, un grito que lleva resonando casi un par de décadas, aparentemente tendrían razón.  ¿Será que en verdad las ideologías murieron? Cuando uno escucha un medio de información masivo, analiza el discurso de los gurús de la opinión pública, de grandes gerentes dando conferencias o de alcaldes que siguen anclados en la nefasta idea de que gobernar es igual a gerenciar, puede dar cuenta clara de que hubo “gato por liebre” con el temita de las ideologías y al parecer, actualmente, muchísimos incautos se convencen o los convencen de que hablar contra este penoso sistema político-económico que hay en Colombia es algo “poco objetivo” “un cliché” o “demagógico”.   ]

[Misteriosamente los colombianos nos acostumbramos a escuchar frases como: “es una posición demasiado ideologizada” “hoy se deben tomar medidas impopulares para gobernar” “eso es como populista” “ojalá cesara la horrible noche de esos gobiernos de izquierda” etc. Y lo que es algo más común, es continuar creyendo el cuento de que la gente es a-política. ¿ser a-político es una condición necesaria para crear perfectos consumistas que tan sólo se pregunten por la nueva versión de cuanta chuchería sale al mercado? O ¿ser a-político es la condición ideológica, unilateral e impuesta por un modelo, político, cultural y económico que triunfó por allá desde 1990?]

[Pues bien, hoy sí existe una ideología reinando y operando tranquilamente, es la que mantiene a muchos creyendo que en la vida sólo se vino a consumir o a endeudarse para consumir, o a consumir lo de otros para no endeudarse. Es esa ideología que llena las gargantas de gente que se dedica a criticar a la izquierda empleando frases acusatorias como “ese modelo es un fracaso” “no saben de gerencia”, pero que jamás voltean los ojos para preguntarse por qué el fracaso de la especulación neoliberal misteriosamente siempre queda cubierto con un manto lingüístico y de opinión que dice: “estalla otro escándalo”, pero jamás (que yo sepa) ninguno de esos a-políticos menciona: “el capitalismo es un fracaso”.  ]

[Hoy existe una ideología que convierte al banquero en un conferencista experto en responsabilidad social, hoy existe una ideología que propuso a los hermanos Nule como ejemplos de emprendimiento, hoy existe una ideología que convence a todos los asalariados de que existen clases medias, medias altas, medias bajas, bajas, pobres, muy pobres, pobres extremos, todo con el fin de que se evapore la condición general que determine el foco problemático de nuestra sociedad. Sí, hoy existe una ideología reinando, que baja la ira porque se pone el disfraz de democracia, que genera duda sobre cómo atacarla porque sólo habla de libertad, que convence hablando de neutralidad y por eso divide en “jijuemil” movimientos las luchas de los pueblos y lo peor de todo, es que ha fracasado más veces que el socialismo, sólo que ha recibido la cuota mediática de apoyo y por eso se mantiene. ¿Qué las ideologías murieron? Jamás.... eso lo dices tú, desde tú ideología.]

###### Feb 2016 
