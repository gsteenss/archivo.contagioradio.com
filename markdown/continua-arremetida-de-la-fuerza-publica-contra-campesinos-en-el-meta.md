Title: Continúa arremetida de la Fuerza Pública contra campesinos en el Meta
Date: 2017-07-22 15:21
Category: DDHH, Nacional
Tags: Erradicación Forzada, ESMAD, Meta
Slug: continua-arremetida-de-la-fuerza-publica-contra-campesinos-en-el-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/esmad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [22 Jul 2017] 

Campesinos de diferentes veredas de los municipios de Puerto Concordia y Puerto Rico, Meta, denunciaron una **fuerte arremetida por parte de la Fuerza Pública que pretendía hacer erradicación forzada dos personas desaparecidas y personas heridas**.

La arremetida se dio el pasado 20 de Julio, en la vereda La Cascada, en el municipio de Puerto Concordia, en donde, de acuerdo con las denuncias de los campesinos, resultaron **heridas un número aún de personas sin identificar por el accionar de un grupo del ESMAD** y la Policía que disparo indiscriminadamente contra la población cuando realizaban erradicación forzada de cultivos ilícitos.

Ese mismo día, sobre la cabecera del río Inírida, se presenta otra arremetida por parte de la Fuerza Pública que de según información de los campesinos **habría dejado a una persona muerta, varias heridas y otras desaparecidas**, sumado a ello periodistas de los medios REMA Y Exitosa Estéreo 107.8 FM expresaron que fueron víctimas por parte de integrantes del ESMAD.

Estos hechos se suman a los actos de violencia del pasado 15 de julio, en donde un campesino identificado como Arnulfo Sánchez resultó herido por disparos de fusil y actualmente se encuentra en un estado crítico de salud. (Le puede interesar:["Campesino herido en protesta contra erradicación forzada en el Meta"](https://archivo.contagioradio.com/protestas-erradicacion-forzada-puerto-rico-meta/))

La Fundación por la Defensa de los Derechos Humanos y del DIH del Oriente y Centro de Colombia, manifestó en un comunicado de prensa que la erradicación forzada que está llevando a cabo la Fuerza Pública en diferentes veredas del Meta es un incumplimiento a los acuerdos que **ya se habían pactado con las comunidades que se habían acogido a los planes de sustitución de cultivos de uso ilícito**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
