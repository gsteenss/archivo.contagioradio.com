Title: Comunidades rechazan el avance de proyectos piloto de fracking
Date: 2019-03-26 18:36
Category: Ambiente, Comunidad
Tags: Alianza Colombia Libre de Fracking, Ecopetrol, fracking, fracturación hidraúlica
Slug: comunidades-rechazan-avance-proyectos-piloto-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/No-al-Fracking1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gert Steenssens] 

###### [26 Mar 2019] 

Tras la decisión de la Autoridad Nacional de Licencias Ambientales (Anla) de archivar las solicitudes para realizar los proyectos piloto **[Piranga y Plata, ubicados en el sur del César, las comunidades de las zonas de influencia y organizaciones ambientales piden que **se respeta la moratoria interpuesta por el Consejo de Estado,** a través del cual se suspende provisionalmente el uso de la técnica de fracturación hidraúlica, conocida como fracking.]**

"Estamos mostrando las evidencias de que **no es legal que se sigan adelantando estos tipos de procedimientos ante la Anla** para la licencia ambiental. Pedimos que pongan atención al llamado que estamos haciendo desde diferentes sectores de que se tenga en cuenta la moratoria", dijo Estefany Grajales, integrante de la Alianza Colombia Libre del Fracking.

En febrero, una Comisión de Expertos dio la luz verde al uso del fracking en Colombia bajo ciertas condiciones. Mientras tanto, varias empresas, como Ecopetrol, ConocoPhillips, ExxonMobile, Parex, Canacol y Drummond, han manifestado su interés de proceder con proyectos de fracking en el país. Sin embargo, la solicitud de ConocoPhillips y Canacol fue archivada por la autoridad ambiental. (Le puede interesar: "[Expertos en fracking se quedan cortos con sus recomendaciones](https://archivo.contagioradio.com/expertos-fracking-se-quedan-cortos-recomendaciones/)")

Si bien la decisión de la Anla confirma algunas de las preocupaciones que han presentado las comunidades en relación a las afectaciones al agua que ocasionarían estos posibles proyectos, por otro lado, las comunidades insisten que esta solicitud debió ser rechazado por la autoridad ambiental puesto que al ser archivada, la solicitud puede reanudarse en el futuro.

La lideresa indica que para proceder con la solicitudes de licencias ambientales también deberían realizar una socialización con las comunidades para conocer la voluntad de las poblaciones que serían afectados, tal como se planteó en uno de las recomendaciones presentadas por la Comisión de los Expertos.

El presidente de Ecopetrol Felipe Bayón manifestó recientemente que la empresa minera ya ha entablado relaciones con los habitantes. Sin embargo, Grajales sostiene que estos acercamientos consisten de talleres que no tienen en cuenta las opiniones y preocupaciones de las comunidades, quienes ya se han posicionado en contra del avance de estos proyectos. En cambio, la lideresa indica que se debería realizar una consulta popular para que los municipios puedan decidir sobre el futuro del fracking en sus territorios.

Mientras tanto, las comunidades esperan que el proyecto de ley para prohibir el uso de esta técnica de extracción pueda proceder en el Congreso. Grajales indica que están a la espera de la entrega del informe de la Comisión de Expertos para que se pueda iniciar el debate en el Senado sobre la iniciativa.

<iframe id="audio_33775159" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33775159_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
