Title: Continúa movilización de habitantes por incumplimientos de Ecopetrol en Ayacucho, Cesar
Date: 2017-07-05 12:27
Category: DDHH, Nacional
Tags: Ayacucho, cesar, Ecopetrol
Slug: defensor-de-dd-hh-denuncia-torturas-del-esmad-durante-movilizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/WhatsApp-Image-2017-07-05-at-8.35.45-AM-e1501783033607.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Equipo Jurídico Pueblos ] 

###### [05 Jul 2017] 

Las comunidades campesinas de La Mata y Ayacucho, Cesar continúan movilizandose frente a los incumplimientos de Ecopetrol de **incluir laboralmente a los pobladores del municipio, la falta de responsabilidad social de la empresa** y los daños ambientales que estaría provocando.

Desd**e hace 6 años se venían adelantando reuniones entre Ecopetrol y la comunidad que lograron a establecer unos acuerdos en materia ambiental, social y laboral**, sin embargo, los campesinos han venido manifestando que se han incumplido y que por tal razón se movilizan.

Rommel Durán, abogado integrante del Equipo Jurídico Pueblos, aseguró que pese a que los campesinos han tenido la intensión de establecer una mesa de diálogos con un **Ecopetrol, este ha señalado que participará “cuando quiera” cerrando la puerta a las conversaciones.  **[(Le puede interesar: "Comunidades de 8 municipios del Meta rechaza proyecto petrolero de Ecopetrol")](https://archivo.contagioradio.com/comunidades-8-municipios-del-meta-rechazan-proyecto-petrolero-ecopetrol/)

### **Acciones arbitrarias del ESMAD en el marco de la movilización** 

Hasta el momento la movilización dejó como saldo la detención arbitraria y el sometimiento inhumano del **líder social Jorge Eliecer Alonzo, la periodista María Montiel y el abogado integrante del Equipo Jurídico Pueblos Rommel Durán.**

De acuerdo con Durán, durante la detención, agentes del ESMAD lo sometieron a tratos crueles, degradantes e inhumanos como golpes en los testículos y amenazas, que ya fueron expuestos y denunciados ante la Policía. ([Le puede interesar: "Abogado y periodista golpeados por agentes del ESMAD en protesta en Ayacucho, César"](https://archivo.contagioradio.com/esmad-habria-golpeado-y-detenido-una-periodista-un-abogado-y-un-campesino-en-cesar/))

“Se abalanzaron sobre mí 4 o 5 miembros del ESMAD y decían que me aplicaran llave para someterme, en eso me estaban doblando los dedos de la mano derecha, **casi me los fracturan, me dieron golpes en la espalda con puños, con los bolillos y las culatas** de las armas con las que disparan las balas de gomas, igualmente en un momento determinado cuando me esposan las manos, con las esposas las doblaban a un lado y lastimaban machismo”. Afirmó Durán

Los hechos transcurrieron durante la movilización del pasado 1 de julio, cuando los campesinos se encontraban marchando, Durán afirmó que “**cuando la gente estaba acompañando el ESMAD arremetió arbitrariamente hacia la comunidad**” y hacía el defensor de derechos humanos y la periodista.

En este momento el Equipo Jurídico Pueblos ya solicitó las medidas cautelares ante la Corte Interamericana de Derechos Humanos para los defensores y los integrantes de la comunidad de Ayacucho, de igual forma **los campesinos han denunciado que miembros del Batallón Energético Vial número 3 les han tomado fotografías** para luego realizar judicializaciones.

<iframe id="audio_19645228" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19645228_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
