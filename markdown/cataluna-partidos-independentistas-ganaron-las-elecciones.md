Title: Cataluña: partidos independentistas ganaron elecciones del parlamento regional
Date: 2015-09-28 16:30
Category: El mundo, Política
Tags: Anibal Garzón Baeza, Cataluña, CUP, elecciones Cataluña, Junts pel Si, Mariano Rajoy, partidos independentistas, Unión europea
Slug: cataluna-partidos-independentistas-ganaron-las-elecciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Cataluña.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: hispantv.com 

###### 28 sep 2015

En las elecciones de Cataluña durante el fin de semana, los partidos independentistas ganaron las elecciones. El partido **Junts pel Si** consiguió **62 curules** a solo seis de obtener mayoría absoluta en el parlamento y por otro lado, el **partido independentista, CUP, logró diez escaños**.

A las urnas este domingo se acercaron **5.5 millones de personas, con una participación del 77.5 %**. El presidente de Cataluña, Artur Mass, señaló que “*son un plebiscito, un referéndum*”, además son un sustituto del referéndum de autodeterminación que el gobierno español de Mariano Rajoy impidió celebrar en noviembre de 2014.

El gobierno ha considerado la independencia de Cataluña como algo ilegal y contrario a la Constitución, al igual que otros países miembros de la Unión Europea que consideran inviable esta propuesta.

De acuerdo al analista internacional Anibal Garzón Baeza, en entrevista para teleSUR, **"ha pesar del triunfo electoral de los partidos que apoyan la independencia catalana, el proceso de separación de España debe pasar por una reforma constitucional",** explicando que el Gobierno central español no cambia de postura en cuanto al derecho de autodeterminación del País Vasco, Galicia o Cataluña, donde actualmente se encuentra prohibido por las leyes españolas.
