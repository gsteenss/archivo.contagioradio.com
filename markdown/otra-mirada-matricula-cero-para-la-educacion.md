Title: Otra Mirada: Matrícula cero para la educación
Date: 2020-07-29 19:29
Author: AdminContagio
Category: Otra Mirada, Otra Mirada, Programas
Tags: educación, Matrícula cero
Slug: otra-mirada-matricula-cero-para-la-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/campamento-matrícula-cero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @ParvadaSinsonte

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El COVID-19 trajo consigo no solo una crisis sanitaria, sino también una crisis social y económica en que las victimas son trabajadores y estudiantes. Muchas personas quedaron desempleadas, pequeños y medianos negocios están luchando día a día por no quebrar y los estudiantes se vieron obligados a recibir clases virtualmente pero asumiendo los costos normales de los semestres.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Debido a esto, estudiantes de múltiples universidades están realizando campamentos y huelgas de hambre dentro de las instalaciones de las universidades para exigir “la matrícula cero” del segundo semestre del 2020 y el primero de 2021. (Le puede interesar: [Sin «matricula cero» estarían en riesgo un 40% de estudiantes de la Surcolombiana](https://archivo.contagioradio.com/estudiantes-de-la-universidad-surcolombiana-exigen-matricula-cero/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para compartir cómo se están articulando en universidades de todo el país, estuvieron Daniela Viuche Conde, estudiante de la Universidad Nacional, Pedro Hernández, profesor de la Universidad Nacional y presidente Nacional de la ASPU, Luis Humberto Perdomo, representante de los estudiantes ante el Consejo Superior de la Universidad Surcolombiana, Katherin Daniela Díaz, estudiante de la Universidad surcolombiana y Fabián Pineda, estudiante de la Universidad Pedagógica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los estudiantes explican cuáles son las exigencias que se están haciendo a cada una de las universidades y comentan si han logrado algún acercamiento o conversación con los entes pertinentes. Además, describen cómo ha sido el trato por parte de las instituciones para garantizar que su ejercicio de manifestación se lleve de forma segura. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, comparten cómo ha sido tomar clases en modalidad virtual y cuáles son las experiencias de muchos al enfrentarse a la crisis económica. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También explican que la voluntad política para favorecer a sectores ya favorecidos lo evidencio la pandemia y mantienen que si los gobiernos han decidido apoyar a las grandes empresas y a bancos, también es factible para el estado dirigir recursos para ayudar a las universidades y estudiantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez, el profesor de la universidad Nacional que acompañó este encuentro, compartió cuánta es la cantidad de estudiantes de universidades públicas y privadas que se podrían ver afectados si no se garantiza la matrícula cero y los procesos desde las organizaciones que se están realizando para apoyar a los estudiantes. (Si desea escuchar el programa del 28 de julio: [Otra Mirada: El Catatumbo entre fuego cruzado](https://www.facebook.com/contagioradio/videos/363792208114463))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Si desea escuchar el análisis completo

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://www.facebook.com/contagioradio/videos/210520340359898"} -->

<figure class="wp-block-embed">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/210520340359898

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
