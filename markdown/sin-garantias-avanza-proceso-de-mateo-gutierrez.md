Title: Sin garantías avanza proceso de Mateo Gutiérrez
Date: 2017-03-16 12:43
Category: DDHH, Judicial, Nacional
Tags: Falsos Positivos Judiciales, Mateo Gutierrez
Slug: sin-garantias-avanza-proceso-de-mateo-gutierrez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/mateo-gutierrez-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Libertad para Mateo] 

###### [16 Mar 2017] 

Luego de tres semanas de la captura de Mateo Gutiérrez, continúan las incongruencias en su proceso judicial. La audiencia de incautación del día de ayer no fue realizada debido a que el estudiante de Sociología **no fue trasladado por las autoridades competentes al lugar, impidiendo que esta se llevara a cabo**.

Congresistas del país a través de un comunicado de presa, exigieron que se den garantías para el proceso judicial de Mateo Gutiérrez y una veeduría por parte de la Procuraduría**,** en vista de los **diferentes inconvenientes y el poco avance que hay hasta el momento en el caso y la imputación de cargos,  ** entre los congresistas que firman esta petición se encuentran, Alirio Uribe, Víctor Correa, Ángela María Robledo, Claudia López, entre otros.

Aracely León, madre del estudiante, expresó que el pasado domingo pudo hablar con su hijo “**Mateo está fuerte, esperando a que se haga justicia manteniendo la calma en medio de esta situación tan difícil, y optimista**”. Le puede interesar: ["Se conforma red de solidaridad en defensa de Mateo Gutiérrez"](https://archivo.contagioradio.com/red-solidaridad-defensa-mateo-gutierrez/)

La audiencia que tendría que haberse dado el día de ayer, no ha sido re programada, lo que genera más trabas al proceso de Mateo. Sin embargo, Aracely indicó que la D**efensa, ya tiene todas las pruebas** referentes a en donde se encontraba el joven el día en que presuntamente lo señalan de haber puesto la bomba panfletaria.

Finalmente, frente a las diversas voces y actividades en solidaridad que se han hecho en defensa de Mateo, Aracely dejo que estas han servido para **“reconfortar a su hijo y darle alegría”**, tanto sus compañeros de colegio como de universidad han convocado diferentes escenarios para apoyar al estudiante. Le puede interesar: ["Exigen libertad de Mateo Gutiérrez a través de Change.org"](https://archivo.contagioradio.com/libertad-mateo-gutierrez-change-org/)

<iframe id="audio_17586906" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17586906_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
