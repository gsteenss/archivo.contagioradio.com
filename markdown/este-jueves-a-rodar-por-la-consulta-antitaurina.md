Title: Este jueves a rodar por la Consulta Antitaurina
Date: 2015-08-05 16:55
Category: eventos, Voces de la Tierra
Tags: 7 de agosto, Bogotá Sin Toreo, ciclovía, Concejo de Bogotá, Consulta Antitaurina, cuimple años de Bogotá
Slug: este-jueves-a-rodar-por-la-consulta-antitaurina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/11813310_1614351582179390_834861824562236477_n.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Este jueves desde las 6:00 de la tarde se realizará la tercera versión de la Ciclovía Nocturna en el marco del aniversario de fundación de Bogotá. Desde "Bogotá Sin Toreo", se convocan cuatro puntos de encuentro para que las personas que defienden la vida de los toros, demuestren su apoyo a la realización de la consulta popular para que la ciudadanía decida sobre el futuro de las corridas de toros en la ciudad.

Este año estarán a disposición 65.5 kilómetros de los 121 que se habilitan tradicionalmente en una jornada de Ciclovía dominical y festiva para integrar a los bogotanos, a través de la lúdica y el uso del tiempo libre, al cumpleaños de la Ciudad Capital.

"Animalistas estaremos presentes en este evento promocionando la iniciativa ciudadana de Consulta Antitaurina, por medio de la cual se busca demostrar que las corridas de toros ya no hacen parte del arraigo mayoritario de los bogotanos y por lo tanto no pueden ser permitidas en nuestra ciudad, según lo dictan sentencias de la Corte Constitucional", es la invitación del [evento.](https://www.facebook.com/events/1024848720893251/)

Los puntos de encuentra están ubicados en el Centro Comercial Gran estación; Eje Ambiental, en La Pola; -Carrera 15 Con 72-Parque del Virrey y la Av. 1º de mayo con Boyacá.

 
