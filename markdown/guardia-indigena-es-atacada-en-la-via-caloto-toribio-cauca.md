Title: Guardia Indígena es atacada en la vía Caloto-Toribio, Cauca
Date: 2019-07-26 15:20
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Cauca, CRIC
Slug: guardia-indigena-es-atacada-en-la-via-caloto-toribio-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/CRIC-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

El pasado 25 de julio, en el sector de La Chivera del corregimiento de El Palo en Caloto, Cauca sobre la vía que comunica con Toribío, mientras recuperaban un vehículo hurtado, la Guardia Indígena Kiwe Thegnas fue atacada en en el trayecto con artefactos explosivos y armas de fuego. Durante el suceso resultaron heridos el coordinador de la Guardia Indígena del Resguardo de las Delicias, Albeiro Camayo junto a tres guardias.

Joe Sauca, coordinador de Derechos Humanos del Consejo Regional Indigena del Cauca (CRIC) señala que durante los hechos, cuatro de los cinco guardias que se movilizaban presentaron heridas de distinta gravedad y fueron atendidos en el el hospital del municipio de Caloto, mientras dos personas recibieron la salida la noche del 25, los dos restantes continúan en estado estable al interior del centro médico. Los hechos ya son investigados por parte del CRIC.Los hechos son investigad

### La Guardia cree que también podría tratarse del Cartel de Sinaloa

Se tiene la hipótesis que los responsables pudieron ser disidencias de las FARC,  el EPL o  las  Aguilas Negras, sin embargo el CRIC también denuncia que podría tratarse de integrantes del Cartel de Sinaloa, grupo que recientemente emitió una amenaza contra la Guardia.

Mediante el panfleto dirigido a los municipios de **Toribio, Corinto, Caloto, Argenlia, Jambalo y Miranda** advierten a la Guardia Indígena que no interfieran con el tráfico de drogas que han llegado a ejercer en el territorio de lo contrario "van a ser masacrados".

"Los carteles están llegando con fuerza a los territorios, en el Norte del Cauca ya hay serias denuncias de gente foránea con acento diferente al de la región, eso se conecta con las amenazas que han salido estos días" explica Sauca quien advierte que el Gobierno no ha dado relevancia  a la llegada de estos grupos al país.

El integrante del CRIC agrega que han hecho seguimiento a este tema, pues no solo se trata del narcotráfico, desde la guardia han decomisado y destruido "armamento de largo alcance de tipo americano". [(Le puede interesar: 14 hechos de violencia contra los DD.HH. de la Minga en Cauca)](https://archivo.contagioradio.com/como-avanza-la-minga-en-la-defensa-de-dd-14%20hechos%20de%20violencia%20contra%20los%20DD.HH.%20de%20la%20Minga%20en%20Caucahh-de-los-pueblos-indigenas/)

Aunque ya se ha alertado de la presencia de este cartel en Tumaco, Nariño, **la Defensoría del Pueblo ha alertado sobre la presencia de mexicanos miembros del cartel de Sinaloa en Caquetá. **

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
