Title: Acuerdo con OTAN atentaría contra la paz continental
Date: 2016-12-27 16:59
Category: DDHH, Nacional
Tags: colombia, EEUU, intervención militar, OTAN
Slug: otan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/5ade1c3414d2a05b17adc8442407636c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  Foto: Escambray 

##### 27 Dic 2016 

El anuncio del presidente Juan Manuel Santos en el que daba cuenta de la aceptación de la Organización del Tratado del Atlántico Norte (OTAN) para iniciar conversaciones en miras de alcanzar un acuerdo de cooperación militar con Colombia, prende las alarmas sobre los alcances que esta intención podría traer para la estabilidad continental.

Las intenciones del gobierno Santos de acercarse a la alianza conformada desde 1949, vienen desde el año 2013 cuando el entonces ministro de defensa Juan Carlos Pinzón firmó en Bélgica un acuerdo de intercambio de información con la OTAN, acuerdo que en su momento fue rechazado por países vecinos como Venezuela, Nicaragua y Bolivia.

Aunque tal convenio fue suprimido en 2015 por la Corte Constitucional colombiana, bajo el argumento de que se presentaron vicios en su aprobación un año antes por parte del Congreso de la República, la posibilidad de establecer un nuevo acuerdo ha sido rechazada una vez más por diversos analistas e incluso por el gobierno de Venezuela, aludiendo que viola la declaratoria de Sur América como zona de paz.

A través de un comunicado de la cancillería, el gobierno de Nicolás Maduro expresó su profunda preocupación señalando que las conversaciones "violentan acuerdos" firmados por Colombia tanto en UNASUR como en la CELAC, y el tratado verbal convenido con Hugo Chavez en 2010 de "no concretar alianza militar con la Otán".

Varios analistas coinciden en que estos acercamientos no pueden desligarse de la posición estadounidense frente al proceso de paz en Colombia, y de su intención conjunta de controlar el espacio vital que permita la circulación de mercancías y de materiales extraídos de la naturaleza, particularmente de petróleo y gas en zonas de frontera.

Bajo esa misma hipótesis, la implementación del proceso de paz, permitiría que las intenciones norteamericanas encontraran vía libre al no tener la resistencia de las FARC  EP en varias de esas zonas en lo que han denominado como "asistencia técnica", lo que se traduce en mayor apoyo militar y estratégico en Colombia, como viene ocurriendo en otros países.

El temor de los analistas venezolanos, recae en la intencionalidad de que se realicen en frontera operaciones de inteligencia para desestabilizar el país vecino, generando fuertes tensiones militares por el control de los recursos energéticos y de paso terminen con la voluntad colectiva de paz en esta zona del continente, por lo que consideran debe llevarse el tema a instancias como la UNASUR.

De clarificarse un nuevo acuerdo, como lo planteó el presidente Santos para intercambio de información y lucha contra el crimen organizado, deberá ser presentado nuevamente a consideración del Congreso de la República.

Le puede interesar: [La terminal: Crónicas de una retención en Panamá](https://archivo.contagioradio.com/la-terminal-cronicas-de-una-retencion-en-panama/)
