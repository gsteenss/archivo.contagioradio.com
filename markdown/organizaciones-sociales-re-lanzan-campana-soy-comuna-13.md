Title: Organizaciones sociales re lanzan campaña Soy Comuna 13
Date: 2017-08-11 14:00
Category: DDHH, Nacional
Tags: crímenes de estado, escrombrera, Medellin, operaciones militares, orión
Slug: organizaciones-sociales-re-lanzan-campana-soy-comuna-13
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/soy-comuna-13.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Soy Comuna 13] 

###### [11 Ago 2017] 

Organizaciones sociales en Medellín, Antioquia, relanzan la campaña Soy Comuna 13 para conmemorar y construir procesos de memoria histórica en relación a los **15 años de las operaciones militares que se llevaron a cabo en la comuna 13 de Medellín en 2002**. La campaña además busca visibilizar la vulneración de los derechos humanos de las víctimas.

Natalia Muñoz, miembro de la Corporación Jurídica Libertad, manifestó que **“la campaña nació en 2012** cuando se estaban cumpliendo 10 de las operaciones militares como Antorcha, Mariscal y Orión”. Hoy, 5 años después quieren que la ciudadanía se vuelva a interesar por los hechos que ocurrieron en la comuna 13 donde a las víctimas aún no se les ha garantizado sus derechos y aún no se han encontrado a los desaparecidos.

En las operaciones militares, según las organizaciones, **hubo el pretexto de pacificar la comuna** y los paramilitares del Bloque Cacíque Nutibara actuaron de la mano de las Fuerzas Militares. Ellos utilizaron helicópteros, tanques y armas de largo alcance contra la población civil. En la Operación Mariscal, por ejemplo, murieron 9 civiles de los cuales 3 eran menores de edad. (Le puede interesar: ["¡Las víctimas de crímenes de Estado tenemos derecho a ser escuchadas!"](https://archivo.contagioradio.com/las-victimas-de-crimenes-de-estado-tenemos-derecho-a-ser-escuchadas/))

Por su parte, en la operación Orión, **murieron 16 personas y 200 más resultaron heridas.** Las personas que fueron desaparecidas y habrían sido sepultadas en "La Escombrera". Allí, se cree que hay más de 95 cuerpos enterrados y las autoridades no han reanudado la búsqueda en un lugar donde continuamente se vierten escombros y desechos. (Le puede interesar: ["Así debería ser la búsqueda de personas desaparecidas en la escombrera"](https://archivo.contagioradio.com/asi-deberia-ser-la-busqueda-de-personas-desaparecidas-en-la-escombrera/))

Según Natalia Muñoz, las víctimas de estas operaciones **“están organizadas exigiendo sus derechos”**. Ella dijo que la búsqueda de los desaparecidos “está parada” y el Estado aún no ha establecido qué acciones pondrá en marcha para seguir con la búsqueda de estas personas.

### **Soy comuna 13, una campaña para resguardar la memoria** 

Con el relanzamiento de la campaña, **las víctimas y la ciudadanía de Medellín van emprender un proceso de memoria colectiva** para que “se recuerde que fue lo que sucedió y que esto no vuelva a ocurrir”. Muñoz afirmó que “queremos contarle a las personas que no vivieron los hechos que en la comuna 13 pasaron cosas graves que deben ser reconocidas por el Estado”. (Le puede interesar: ["Víctimas de crímenes de Estado piden que se desclarifiquen archivos militares"](https://archivo.contagioradio.com/victimas-de-crimenes-de-estado-piden-que-se-desclasifiquen-archivos-militares/))

Finalmente, Muñoz indicó que **en lo que queda del año continuarán realizando actividades para involucrar a la ciudadanía en estos procesos de memoria.** “Vamos a realizar un film minuto para que las personas puedan contar como vivieron las operaciones militares y como fueron los procesos de resistencia”. Hoy el relanzamiento de la campaña se hará a las 4:00 pm en el Parque Biblioteca San Javier de Medellín.

<iframe id="audio_20289002" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20289002_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
