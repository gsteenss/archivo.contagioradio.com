Title: Mujeres listas y preparadas para trabajar por la paz
Date: 2016-11-25 12:58
Category: Mujer, Nacional
Tags: mujeres, participación política, paz
Slug: mujeres-preparadas-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Mujeres-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Semana] 

###### [25 Nov. 2016] 

Mujeres, jóvenes, adultas, mayores han trabajado por la erradicación de las violencias contra las mujeres históricamente a través de diversas acciones. Sin embargo, **las cifras de violencia contra la mujeres en todos los rincones del país sigue siendo parte de las estadísticas, que instituciones dan a conocer cada año.**

Por ello, Adriana Benjumea Directora de Humanas Colombia aseveró “las transformaciones tienen que ser ya (…) de todos los rincones nos vamos a movilizar, tenemos derechos a vivir una vida libre de violencias”.

En medio de la coyuntura actual, asegura Adriana, las mujeres han estado respaldando el proceso de paz **“es muy importante que se haya firmado este segundo acuerdo, estamos listas y preparadas para trabajar por la paz. Es un acuerdo que se puede mejorar como todos pero estamos muy** **contentas y es importante conmemorar este 25 después de la firma de la paz”.**

Otra de las luchas que desde siempre, y hoy más que nunca reivindican desde el movimiento social de mujeres, es que se cumplan las condenas en contra de los agresores, dado que el nivel de impunidad en casos de feminicidio es alto. Pero además, **con miras a la implementación de los acuerdos de paz las mujeres continúan haciendo la exigencia de garantizar una paridad política.**

“Hemos exigido desde los movimientos de mujeres que la institucionalidad que se cree en el proceso de la implementación de la paz debe garantizar paridad política. **Hoy el país tiene un gran déficit de mujeres en las instituciones y no se puede desconocer que existe una tolerancia institucional que soporta la violencia contra las mujeres. Es una tarea de toda la ciudadanía”** afirmó Adriana**.**

Hoy en el país las movilizaciones saldrán desde diversos puntos y pretenden hacer un llamado a la sociedad, para que en tiempos de paz se haga respetar a las mujeres “**tenemos movilizaciones y eventos públicos que a través del arte van a decir \#DéjameEnPaz**. Un slogan que fue construido por las jóvenes, que vincula el proceso de paz, porque no es posible una paz si seguimos tolerando la violencia contra las mujeres” contó Adriana.

Los colegios también tienen un papel importante en la construcción de un país en el que las violencias contra las mujeres no sean más parte de la vida cotidiana **“hay que construir masculinidades no violentas desde las escuelas. Que las mujeres puedan decidir sobre sus cuerpos”** dijo Adriana Benjumea.

Para la abogada y directora de Humanas Colombia, el país tiene que reflexionar sobre el papel de las mujeres en el actual sistema “**somos más del 52% de la población y si transitamos a la paz debemos reconocer el papel de las mujeres y no solo levantando muertos en las guerras, sino un lugar en el que las mujeres tenemos derechos a vivir una vida libre de violencia. En sociedades democráticas”. **Le puede interesar: [Mujeres sí a la paz, violencias ni una más](https://archivo.contagioradio.com/mujeres-si-a-la-paz-violencias-ni-una-mas/)

Y concluyó reflexionando que el país que se sueña, es uno donde “se reconozca a las mujeres víctimas de violencia sexual y del conflicto armado. Hoy las voces de estas mujeres no se escuchan pero ellas siguen fortalecidas y luchando”. Le puede interesar: [El trabajo de las mujeres en Colombia](https://archivo.contagioradio.com/trabajo-las-mujeres-colombia/)

<iframe id="audio_14023098" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14023098_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}

<div class="ssba ssba-wrap">

</div>
