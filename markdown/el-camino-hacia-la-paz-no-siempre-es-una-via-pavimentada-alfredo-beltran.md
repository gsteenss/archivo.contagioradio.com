Title: “El camino hacia la paz no siempre es una vía pavimentada” Alfredo Beltrán
Date: 2016-07-01 14:50
Category: Nacional, Paz
Tags: Conversaciones de paz de la habana, Corte Constitucional, FARC, plebiscito por la paz
Slug: el-camino-hacia-la-paz-no-siempre-es-una-via-pavimentada-alfredo-beltran
Status: published

###### [Foto: UN] 

###### [1 Jul 2016]

El ex presidente de la Corte Constitucional Alfredo Beltrán resalta que uno de los elementos más importantes del reciente acuerdo sobre cese bilateral, dejación de armas y refrendación, anunciado por la mesa de conversaciones de paz entre el gobierno y las FARC, es que se **acepta que hay una institucionalidad** a la cual le asignan una **responsabilidad muy amplia con el compromiso histórico de la construcción de la paz**.

Según el constitucionalista el acuerdo alcanzado es histórico y la[responsabilidad de las instituciones debe estar a la altura del reto](https://archivo.contagioradio.com/refrendacion-de-acuerdos-de-paz-queda-en-manos-de-la-corte-constitucional/). Por otro lado, frente a la decisión que tomará la Corte Constitucional, Beltrán señala que ya las posibilidades de esa decisión están marcadas por las tendencias en la **ponencia que se ha publicado**, es decir, los condicionamientos parecen estar por lo menos mencionados.

Uno de ellos tiene que ver con que el **plebiscito** será entendido como un [mecanismo de consulta](https://archivo.contagioradio.com/cuatro-millones-quinientos-mil-deberan-votar-si-en-plebiscito-para-la-paz/) a las personas en torno a los acuerdos de paz. Otro, que los partidos políticos no podrán realizar campaña por ninguna de las opciones y una tercera, que los [dineros públicos no podrán ser utilizados para promocionar ninguna de las opciones](https://archivo.contagioradio.com/?s=plebiscito), **incluso, no podrán ser utilizados para impulsar la participación.**

Frente a la posibilidad de que integrantes de las FARC sean acreedores de curules especiales en el congreso de la República, Beltrán señala que la constitución de 1991 contiene una reglamentación para ese tipo de asignaciones en contextos especiales, pero que su aplicación más que a una decisión legal o constitucional **depende de una voluntad política en la que tiene potestad el presidente de la república.**

<iframe src="http://co.ivoox.com/es/player_ej_12093944_2_1.html?data=kpedm5ideJWhhpywj5aYaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncaLgx9fSxtSPhsbg1deSpZiJhZLijJKYp92PscLbytjh1MbIs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
