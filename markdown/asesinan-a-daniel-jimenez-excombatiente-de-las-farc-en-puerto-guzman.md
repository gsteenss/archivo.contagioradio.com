Title: Asesinan a Daniel Jimenez, excombatiente de las FARC en Puerto Guzmán
Date: 2020-02-18 19:12
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato, Ex-Combatiente, Puerto Guzmán
Slug: asesinan-a-daniel-jimenez-excombatiente-de-las-farc-en-puerto-guzman
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Asesinan-excombatiente-en-Puerto-Guzmán.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Archivo Contagio Radio {#foto-archivo-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este martes se conoció el asesinato de Daniel Jiménez, firmante de la paz, en la vereda El Recreo, del municipio de Puerto Guzmán, Putumayo. El homicidio ocurrió el domingo 16 de febrero sobre las 9 de la noche, y su cuerpo fue trasladado al municipio de Curillo, en Caquetá. (Le puede interesar: ["Con Wilson Parra, son 169 excombatientes de FARC asesinados desde la firma del Acuerdo de Paz"](https://archivo.contagioradio.com/con-wilson-parra-son-169-excombatientes-de-farc-asesinados-desde-la-firma-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las primeras informaciones, hombres armados habrían atentado contra Jiménez, causándole la muerte tras disparar en repetidas ocasiones. Según datos ofrecidos por el partido FARC, desde la firma del Acuerdo de Paz han sido asesinados [más de 180 excombatientes](https://news.un.org/es/story/2019/12/1467341), ante lo que han pedido medidas de protección adecuadas para proteger la vida en la reincorporación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Puerto Guzmán, la violencia sigue azotando a sus habitantes en 2020**

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Contagio Radio](https://archivo.contagioradio.com/la-muerte-se-apodera-de-puerto-guzman-8-asesinatos-en-dos-semanas/) registró el asesinato de ocho personas (cinco dirigentes sociales y tres campesinos) en las primeras dos semanas de este año. Entre los hechos se cuenta un secuestro de dos líderes y su posterior asesinato en zona rural de Puerto Guzmán, al que se suma el homicidio del ex combatiente ocurrido durante el pasado fin de semana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por la cantidad de asesinatos, defensores de derechos humanos en Putumayo advirtieron que se estaban produciendo desplazamientos de entre 15 y 20 familias. Los desplazados estarían buscando salvaguardar sus vidas en Caquetá. (Le puede interesar: ["Asesinatos en Puerto Guzmán comienzan a generar desplazamientos forzados"](https://archivo.contagioradio.com/asesinatos-puerto-guzman-desplazamientos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según defensores de DD.HH., la situación de violencia se debe a la llegada de un nuevo actor armado al Municipio, que entró en disputa con las disidencias de las FARC. A ello se suma una fuerte presencia militar con hombres del Ejército y la Armada Nacional que sin embargo, no han evitado la continuidad en los asesinatos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar ["Hacer oposición en Putumayo nos cuesta la vida: Diputado UP"](https://archivo.contagioradio.com/oposicion-putumayo-cuesta-vida/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
