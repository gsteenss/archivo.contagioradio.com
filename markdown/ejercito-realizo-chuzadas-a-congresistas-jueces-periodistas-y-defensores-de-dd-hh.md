Title: Ejército realizó "chuzadas" a congresistas, jueces, periodistas y defensores de DD.HH.
Date: 2020-01-11 10:29
Author: CtgAdm
Category: DDHH, Nacional
Tags: chuzadas, ejercito, Iván Cepeda, Nicacion Martinez
Slug: ejercito-realizo-chuzadas-a-congresistas-jueces-periodistas-y-defensores-de-dd-hh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/ejercito-chuzadas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elpais.com] 

La revista SEMANA reveló una investigación en la que da cuenta de un sofisticado sistema de interceptaciones  utilizado por el Ejército a través de dos de las oficinas de inteligencia y contra inteligencia, durante el segundo semestre de 2019, que habría sido usado para espiar ilegalmente a magistrados de la Corte Suprema de Justicia (CSJ), congresistas, gobernadores, periodistas y defensores de DD.HH.

Esta nueva revelación destapa nuevamente la funcionalidad política de los servicios de inteligencia y además pone al descubierto las nuevas formas de operación de las FF.MM al servicio de intereses políticos del partido de gobierno, entre otras acciones. **Además asegura la investigación que las operaciones se realizaban desde guarniciones militares para evitar allanamientos sorpresivos y filtraciones a la prensa** como ocurrió con la conocida operación Andrómeda. (Lea también: [Salida del Gr. Martínez del Ejército, medida de evasión del Gobierno?)](https://archivo.contagioradio.com/salida-del-gr-martinez-del-ejercito-medida-de-evasion-del-gobierno/)

### Centro Democrático recibía del Ejército información de chuzadas a la Corte Suprema

Uno de los apartes de la información publicada señala que uno de los blancos de la operación de espionaje fue la Corte Suprema de Justicia y concretamente una de las magistradas de ese alto tribunal. De hecho en una reciente entrevista Gloria Ortíz, ex presidenta de la Corte Constitucional manifestó que tenían serios indicios de estar siendo “chuzados” en ese alto tribunal. (Lea también: [Nueva cúpula militar: ¿vuelve la seguridad democrática?](https://archivo.contagioradio.com/nueva-cupula-militar-vuelve-la-seguridad-democratica/))

Una de las afectadas sobre la que se conoce información concreta es la Magistrada Cristina Lombana quien fuera oficial del Ejército y en un tiempo tuvo a su cargo la investigación por falsos testigos en contra del senador Álvaro Uribe. Sobre esto, una de las fuentes periodísticas aseguró que **la información recopilada tenía como destinatario a un miembro del partido Centro Democrático.**

> Lo que es también escandaloso es que [@ivanduque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) y [@mindefensa](https://twitter.com/mindefensa?ref_src=twsrc%5Etfw) Holmes Trujillo hayan despedido a Nicacio Martinez como un héroe cuando ya sabían lo que hoy revela SEMANA. No hay duda el Centro Democrático no puede gobernar sino chuzando y espiando, y aun si todo va mal.
>
> — Ramiro Bejarano G (@RamiroBejaranoG) [January 11, 2020](https://twitter.com/RamiroBejaranoG/status/1215997315300974596?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### Tres gobernadores, tres congresistas y dos representantes son blanco de interceptaciones

Tanto Iván Cepeda, como Antonio Sanguino y Roy Barreras habían denunciado recientemente que estaban siendo víctimas de operaciones ilegales de interceptación. Uno de los ejemplos concretos fue el debate de control político por el bombardeo en que murieron 7 menores de edad. El senador Barreras aseguró que los altos mandos militares que se encontraban en el debate tuvieron información secreta acerca de las razones del debate y el hecho concreto de la denuncia que se realizaría. [Lea también: Interceptaciones contra congresistas deben ser investigadas](https://archivo.contagioradio.com/si-hubo-interceptaciones-ilegales-es-responsabilidad-de-nestor-humberto-martinez-ivan-cepeda/)

En este caso la investigación periodística confirmó que las oficinas de interceptación del ejército espiaron las conversaciones de congresistas y gobernadores entre los cuales se puede encontrar Camilo Romero, ex gobernador de Nariño quién ha demostrado fuerte oposición a algunas políticas del gobierno como las fumigaciones con glifosato y la militarización de los departamentos.

Adicionalmente las fuentes de la investigación revelaron que son varias organizaciones de DDHH, colectivos de abogados y otras las que están siendo blanco de las interceptaciones ilegales, ya que existen carpetas con información, fotos y videos de este tipo de blancos. (Le puede interesar: [Es necesaria una comisión independiente que diga qué pasa al interior del Ejército](https://archivo.contagioradio.com/es-necesaria-una-comision-independiente-que-diga-que-pasa-al-interior-del-ejercito/))

“Carpetas presentes en los discos duros y en las memorias USB que contienen presentaciones de informes, extractos de conversaciones y audios de entidades públicas, ONG y medios de comunicación. Incluso, hay soportes en videos y fotografías de seguimientos a periodistas de este medio de comunicación que adelantaban la investigación.”, reza el artículo periodístico.

Las reacciones a estas revelaciones no se han hecho esperar. Algunos líderes de opinión señalan que la despedida de Nicacio Martínez como comandante del ejército no debió ser la salida de un héroe, más aún cuando el propio gobierno conocía las actividades ilegales de esa entidad al mando de Martínez.
