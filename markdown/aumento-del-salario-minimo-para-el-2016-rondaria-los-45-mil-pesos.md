Title: Aumento del salario mínimo para el 2016 rondaría los 45 mil pesos
Date: 2015-12-18 15:16
Category: Economía, Nacional
Tags: central unitaria de trabajadores, Devaluación del peso, Salario Mínimo en Colombia
Slug: aumento-del-salario-minimo-para-el-2016-rondaria-los-45-mil-pesos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/salario-minimo-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eluniversal 

<iframe src="http://www.ivoox.com/player_ek_9772863_2_1.html?data=mpyklJ2ad46ZmKiak5eJd6KmmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdbhxtPh0ZDIqc2f1MbZw9fNs4zhhqigh52ossrh0JDdw9fFb8bgjJedk5uPttDixcbfh6iXaZm4wpDZ0diPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Alejandro Pedraza, CUT] 

###### [18 Dic 2015]

Según los integrantes de la Central Unitaria de Trabajadores, una de las centrales obreras en la negociación, los empresarios nunca han mostrado interés en un aumento real del salario. Desde antes de iniciar la conciliación los gremios han afirmado que el aumento debe ser igual a la inflación, es decir, el 6.8% según las predicciones, lo que equivaldría a **\$43815, es decir, el salario mínimo estaría por el orden de \$688115 (U\$200) para el 2016.**

Por otra parte se ha anunciado la posibilidad del **aumento de las tarifas del transporte masivo en todas las ciudades que rondaría los 200 pesos**, según lo afirmado por el actual gerente de Transmilenio, Sergio Paris; la misma situación se afrontaría en Cali y Medellín y Barranquilla. Además se estaría preparando el reajuste de las tarifas de servicios públicos y para el caso del mínimo vital  de agua se alistaría un desmonte durante los dos primeros años de gobierno de Peñalosa en la capital.

Para el dirigente sindical la tendencia de la economía hacia el sector extractivo, la devaluación del peso frente al dolar y los bajos precios del petróleo empujaran una crisis que el gobierno buscará resolver con una reforma tributaria en 2016, que aumentaría el IVA en 2 puntos y con consecuencias sensibles para la clase trabajadora por la pérdida del poder adquisitivo.

Luis Alejandro Pedraza, presidente de la CUT, afirma que las **centrales obreras llegan a la Comisión de Conciliación con las manos atadas pues las opciones son [aceptar la propuesta de los empresarios o que el gobierno defina por decreto](https://archivo.contagioradio.com/el-salario-en-colombia-seguira-siendo-minimo/)** los mismos condicionamientos de los gremios. Una situación similar se vivió en 2014 cuando el gobierno definió el aumento del salario mínimo en 4.6%. Por ello se hace necesaria una movilización de los trabajadores y trabajadoras para respaldar las exigencias y evitar la precarización del empleo, afirma el dirigente.
