Title: Quien pretenda negar el conflicto puede intentarlo, pero conocer la verdad es cuestión de tiempo: Sofía Macher
Date: 2020-03-06 16:51
Author: CtgAdm
Category: Actualidad, Paz
Tags: comision de la verdad, Negacionismo, Sofía Macher
Slug: quien-pretenda-negar-el-conflicto-puede-intentarlo-pero-conocer-la-verdad-es-cuestion-de-tiempo-sofia-macher
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Comisionada-Sofia-Macher-scaled.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Sofía-Macher1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Sofía Macher Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En el marco de su visita a Colombia, Sofía Macher, socióloga. e integrante de la Comisión de la Verdad de Perú creada en 2001 y que estudió la violencia cometida por grupos armados insurgentes y agentes estatales en el periodo 1980-2000 realizó un análisis sobre el trabajo que se viene realizando con la [Comisión de la Verdad en el país](https://twitter.com/ComisionVerdadC/status/1235682540813463552) basada en su experiencia como comisionada y defensora de los derechos de las mujeres.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Han transcurrido 15 años desde que Perú aportó a la verdad, ¿Qué escenarios nuevos enfrenta la Comisión de la Verdad en Colombia?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Sofía Macher:** Lo que pasa es que en Colombia están investigando crímenes del pasado y las metodologías son más o menos las mismas, vengo de Nicaragua donde hemos investigado la revuelta social del 2018 y es absolutamente diferente, nuestra investigación estuvo basada en 45 días de movilización y 10.000 videos en los que está documentado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Investigar los crímenes del pasado sigue teniendo la misma metodología, como la búsqueda de desaparecidos; por supuesto que las herramientas digitales para las bases de datos y el cruce de información son ventajas que te permiten llegar a datos que hacen más profunda y más compleja la investigación, son herramientas que se están usando en Colombia, es un paso adelante de lo que nosotros hicimos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Fundamentalmente nos servimos de la Comisión de la Verdad de Guatemala, que tenía ese componente indígena que tenemos en Perú, por ejemplo Chile y Argentina eran una cosa totalmente diferente, tomamos la experiencia de Suráfrica sobre audiencias públicas y creamos nuestra propia metodología, yo creo que Colombia es la síntesis de una experiencia larga de comisiones de la verdad en el mundo y es que ya no es solo una Comisión de la Verdad, han avanzado en todo el sistema de justicia, verdad, reparación y garantías de no repetición y lo están poniendo a prueba.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cuando leí el sistema del Acuerdo, decía, 'Se van a pisar los pies, va a ser un caos, la misma persona tiene que ir a cada uno de los mecanismos. ¿Por qué no hacen algo más en línea?' pero están manejando la información, está fluyendo, vamos a ver cómo termina, **pero me parece apropiado que la Comisión termine primero porque creo que es lo que da un marco general que con seguridad va a apoyar el trabajo de la JEP y de la Unidad de Búsqueda de Desaparecidos, porque te da un contexto de patrones, de la guerra y de la historia.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Cuáles son las nuevas narrativas que surgen con el fin del conflicto?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Sofía Macher:** Frente a las narrativas, en Perú es diferente de Colombia porque aquí hay mucha experiencia de reconstrucción histórica, como en el caso de los paramilitares, es decir no partes de cero. Creo que la complejidad de los actores es muy diferente que en Perú y sigue siendo un reto cuando aún hay violencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ahora creo que este experimento de tener tu justicia especial para esta guerra es también un reto del que estamos esperando sea una enseñanza y un aporte del proceso colombiano al mundo, así como han bebido de los procesos internacionales ahora van a poder a hacer un aporte. [(Lea también: Hay que enfocarse en las víctimas, ellas siguen aquí mientras los políticos vienen y van: David Crane)](https://archivo.contagioradio.com/hay-que-enfocarse-en-las-victimas-ellas-siguen-aqui-mientras-los-politicos-vienen-y-van-david-crane/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Es interesante cómo la narrativa del conflicto en general en las comisiones de la verdad en el mundo, se centra en las víctimas y ese es el enfoque y su tarea, no desarrolla con la misma amplitud las narrativas de los militares o de los subversivos que también tiene sus justificaciones y una ideología, pero no es el propósito de una comisión, y así tiene que ser, pero pasan los años y **una comisión de la verdad es solo un hito, un momento de un proceso para impulsar otros y en Perú estamos en un momento en que empezamos a abrir un campo que era impensable para el 2004.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Se trata de por ejemplo los hijos de quienes fueron integrantes de Sendero Luminoso en Perú que han tenido su propio calvario, de ser hijos de los terroristas, pero ellos no son terroristas ni escogieron serlo, además quieren a sus papás, esa es una nueva narrativa absolutamente nueva y todavía hay una resistencia en un sector de la sociedad para escucharla. [(Lea también: Vencer la apatía, el desafío más grande al buscar la verdad en Colombia: John Paul Lederach)](https://archivo.contagioradio.com/vencer-la-apatia-el-desafio-mas-grande-al-buscar-la-verdad-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Creo que es generacional, **tal vez los militares que estuvieron involucrados durante la lucha anti subversiva no lo van a aceptar y lo van a rechazar, pero hay toda una juventud que sí tiene una capacidad para ir incorporando narrativas que han sido hasta el momento censuradas,** es un proceso que recién se está viviendo como las narrativas nuevas de militares, pero no de los oficiales que se oponen a estos ejercicios de memoria sino soldados o subalternos que tuvieron que ejecutar órdenes y que recién están entrando en un proceso de contar, es algo que todavía no ha terminado de permear a otros militares que tal vez han sufrido lo mismo pero eso se da con el tiempo.

<!-- /wp:paragraph -->

<!-- wp:image {"id":81722,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![Sofía Macher](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Sofía-Macher1-1024x576.jpg){.wp-image-81722}  

<figcaption>
Foto: CEV Comisionadas Sofía Macher de Perú y Olandina Caeiro de Timor Oriental

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### ¿Qué hay de quienes se oponen a un Sistema Integral de justicia?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Sofía Macher:** Es un tremendo reto para la Comisión de la Verdad, que a pesar que tus hallazgos o las cosas que vas encontrando, incomoden y te van a odiar e insultar, hay que escribirlas, a nosotros nos odiaban, pero parte de las personas que más se oponían a la comisión en Perú, fueron los que más propaganda hicieron y han mantenido el informe hasta ahora; era tanto el odio que nadie ha olvidado el informe.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El trabajo de los comisionados es estresante, se trata de no equivocarte, de cruzar bien fuentes, de no poner en un libro algo que después no se puede modificar y que queda como una verdad oficial. **Es decir cosas que van a incomodar y que probablemente se puedan enfrentar a gente que está en el poder en el país**, esa es la tensión que uno vive como comisionado, pero al final tienes que hacerlo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En Colombia existe una percepción de que el Estado es negacionista ¿Cómo enfrentarlo y reafirmar que sí existen responsables del conflicto?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Sofía Macher:** Es una pugna fuerte, desde la Comisión estás defendiendo la narrativa de los débiles y creo que ese es el gran aporte de estos procesos, que por única vez, empoderas la versión e historia de quienes sufrieron, eso no se va a repetir y con seguridad va a confrontar a los poderosos que lo van a negar, pero creo que la experiencia en mi país y de lo que conozco de otros procesos dice que es cuestión de tiempo.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Creo que solo queda cerrar los ojos y seguir tercamente en tus convicciones y quien pretende negar que hubo guerra o desapariciones, puede intentarlo, pero creo que es cuestión de tiempo - Sofía Macher

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Estos informes y procesos que han involucrado a tanta gente, miles de personas que entregaron su testimonio y que saben que eso sucedió por que lo vivieron de cerca, eso no lo pueden borrar, y pueden pasar 10 años más y el informe queda y la contundencia y evidencia que quedan ahí, pero son procesos judiciales que confirman los patrones de violaciones a los DD.H.H. que fueron negados totalmente.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Creo que solo queda cerrar los ojos y seguir tercamente en tus convicciones y quien pretende negar que hubo guerra o desapariciones, puede intentarlo, pero creo que es cuestión de tiempo**, la experiencia a nivel mundial dice que la justicia llega por la contundencia de las evidencias de lo que hicieron, son gritos de ahogado, se demorará más o menos pero llegará.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Detrás de esos informes está un grupo de la sociedad que fue quien proporcionó esa información y la que vivió en carne propia los hechos, les va a llegar a los políticos, a las FARC, a los empresarios y a los políticos, creo que esa es la línea de los DD.HH, si no te separas de eso, si no entregas un informe político y no te separas de este juzgamiento ético, porque son matanzas y asesinatos premeditados, actos conscientes, personas que financiaron la violencia o que mataron o secuestraron directamente tienen que ser juzgados éticamente por sus comportamientos.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
