Title: Acuerdos no se pueden quedar entre gobierno y FARC: Cumbre Afro
Date: 2016-11-25 12:34
Category: Nacional, Paz
Tags: COMOSOC, Comunidades Afrodescendientes Colombia, Consejos Comunitarios Afro, Implementación de Acuerdos
Slug: las-comunidades-afro-se-preparan-para-implementar-los-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/1DSC_0849.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [25 Nov 2016] 

Desde el día 24 de Noviembre hasta el 25 comunidades afro de Cauca, Nariño y Valle del Cauca están reunidas en la Cumbre Afro de Paz en la ciudad de Cali, allí se han discutido temas relacionados con la **implementación de los acuerdos en sus territorios,** el papel de las **mujeres afro en la construcción de paz, alternativas económicas colectivas y los impactos de las actividades extractivas** en sus asentamientos.

Francia Márquez una de las integrantes de la Marcha por la Vida y el Cuidado de los Territorios Ancestrales, afirmo que las comunidades afro de todo el país tienen como reto conformar **agendas de construcción de paz desde las formas ancestrales propias** que incluyan de manera más amplia las demandas y propuestas de esta población y también “hacer que los acuerdos no se queden entre dos sectores sino que todas las comunidades los apropien e implementen”.

Por otra parte en uno de los boletines de prensa **respaldan el inicio de conversaciones con el ELN** y aseguran que los consejos comunitarios afro están dispuestos a **acompañar y ser veedores de dicho proceso.**

Jairo Porres líder de un consejo comunitario de Nariño manifestó que si bien el acuerdo tiene un punto que se refiere a las comunidades étnicas, “es importante que haya una mayor presencia en dichos acuerdos de las comunidades afro pues **en nuestros territorios es donde más violencia se ha sufrido”.**

Por otra parte afirmó que está pendiente una mayor discusión con el Gobierno sobre el tema de la **titulación de tierras para los consejos comunitarios y la forma de participación de estos en la verificación e implementación de los acuerdos.**

<iframe id="audio_14022171" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14022171_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}
