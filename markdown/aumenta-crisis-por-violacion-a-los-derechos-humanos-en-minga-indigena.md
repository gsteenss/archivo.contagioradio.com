Title: Aumenta crisis por violación a los derechos humanos en Minga Indígena
Date: 2017-11-05 10:19
Category: Movilización, Nacional
Tags: ESMAD, Fuerza Pública, indígenas, Minga Indígena, Movilización Indígena
Slug: aumenta-crisis-por-violacion-a-los-derechos-humanos-en-minga-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/DNqWXz0UEAEG086.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ACIVA] 

###### [03 Nov 2017] 

Las comunidades indígenas continúan en su quinto día de movilización nacional. En Caldas y el Valle del Cauca han realizado un balance de la situación de derechos humanos a raíz de los enfrentamientos que ha habido con la Fuerza Pública. En el sitio de concentración de la Delfina hay un manifestante herido en la frente y el Caldas **fue asesinada la comunera Elvia Azucena Vargas**.

Las autoridades indígenas indicaron que en el Cauca, Valle del Cauca y Caldas, “la fuerza pública ha contrarrestado con su accionar bélico contra los miles de mingueros que en unidad le apuestan al resarcimiento de los derechos de los pueblos y el cumplimiento de los acuerdos”.

Manifestaron que **16 miembros de la Fuerza Pública**, que estaban retenidos por parte de la Guardia Indígena, “ya fueron devueltos a los alcaldes de Pueblo Rico, Marsella, Carmen del Atrato y Quibdó (e), así como a la Defensoría del Pueblo de Chocó y Risaralda, Personería de Pueblo Rico, Procuraduría de Risaralda y la Iglesia Católica, con el acompañamiento de MAPP/OEA”.

### **En la Delfina denuncian que hay tres indígenas desaparecidos** 

<iframe src="https://co.ivoox.com/es/player_ek_21869517_2_1.html?data=k5almJ6ZdZihhpywj5WbaZS1k5uah5yncZOhhpywj5WRaZi3jpWah5yncajp1NnO2NSPlMLm08aY1dTGtsafzdSYw8jTstXZxM7R0ZDJsozgwpC6y9PLpYy9z8mSpZiJhaXbxtPOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con Gustavo Parra, miembro de la organización indígena ACIVA, por los ataques de la Fuerza Pública **resultó un indígena herido y hay tres desaparecidos**. Manifestó que aún no tienen claridad el tipo de arma que ha utilizado la Fuerza Pública, pero “uno de los impactos hirió a una persona en la cabeza por lo que hubo que atenderlo de urgencia”. (Le puede interesar: ["ESMAD está "atrincherado" en viviendas de Guascal en Caldas"](https://archivo.contagioradio.com/esmad-esta-atrincherado-en-viviendas-de-vereda-guascal-en-caldas/))

De igual forma, manifestó que los enfrentamientos **se han extendido por varias horas** y le han comunicado a la opinión pública la grave situación que están viviendo producto de disparos. Según Parra, “el coronel del ejército ha negado todos los hechos, pero tenemos fotos y muestras de los cartuchos de los fusiles de cuando nos han disparado”.

### **En Caldas fue asesinada una comunera** 

<iframe src="https://co.ivoox.com/es/player_ek_21870420_2_1.html?data=k5almZWYdpGhhpywj5WbaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5ynca7V09nOja3Jts-ZpJiSo5bSqMbuhpewjcjTstTZy8rfw5DIqc2fpNTb1crOs4zGxszW0dPFsIy9z8mSpZiJhaXbjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según Martha Hernández, consejera del Consejo Regional Indígena De Caldas, informó que el día de ayer fue asesinada Elvia Azucena Vargas en la comunidad Portachuelo del resguardo CañoMomo LomaPiedra, **a dos horas de la concentración de la Minga**. Indicó que fueron hechos confusos y que están siendo objeto de investigación.

Con respecto a la movilización, manifestó que **aún hay comunidades en donde hay presencia constante del ESMAD**. Esto lo han considerado como una intimidación “que va en contra de la ley porque no están acatando la norma que establece la consulta con las autoridades indígenas para ingresar a los territorios”. (Le puede interesar: ["Dos heridos graves en la Delfina por ataques del ESMAD"](https://archivo.contagioradio.com/dos-heridos-de-gravedad-en-la-delfina-por-ataques-del-esmad/))

Dijo además que los uniformados “están en algunos traipches y **casas que se encuentran solas**, el miércoles pasado lanzaron gases lacrimógenos desde allí y a las personas les tocó salir de sus casas”.

Frente al número de indígenas heridos, Hernández aseguró que tienen un registro de **4 personas lesionadas por enfrentamientos con el ESMAD**. Enfatizó que la fuerza pública está disparando directamente a la cara de las personas por lo que la situación les es preocupante. La misión médica, que hace presencia las 24 horas, ha atendido a las personas por lo que no ha sido necesario trasladar a las personas a centros médicos. (Le puede interesar: ["Indígenas denuncian agresiones con arma de fuego por parte del ESMAD"](https://archivo.contagioradio.com/indigenas-denuncian-agresiones-con-arma-de-fuego-por-parte-del-esmad/))

Finalmente, Hernández aclaró que **no se han quemado carros o motos en el marco de la protesta**. Indicó que la movilización indígena continuará a la espera de que las autoridades nacionales dispongan una mesa de negociación. Además, la ONIC indicó en Caldas continúa “la zozobra ante el pronunciamiento del Coronel de la Policía del departamento quien dijo que, si algún indígena sale a la carretera, lo priva de la libertad y lo ponía a disposición de la Fiscalía”.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
