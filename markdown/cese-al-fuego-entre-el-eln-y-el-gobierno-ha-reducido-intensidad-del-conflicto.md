Title: Listo mecanismo de monitoreo y verificación del cese bilateral entre ELN-Gobierno
Date: 2017-10-11 17:35
Category: Nacional, Paz
Tags: ELN, Proceso de paz Quito
Slug: cese-al-fuego-entre-el-eln-y-el-gobierno-ha-reducido-intensidad-del-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/proceso-de-paz-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Telegráfo] 

###### [11 Oct 2017] 

Hoy se presentó el mecanismo de monitoreo y verificación del cese bilateral entre el ELN y el Gobierno Nacional que cumple 11 días. En el mecanismo participaran por el Gobierno Nacional el General Alberto Rodríguez, Coronel Santiago Camelo y Alejandro Reyes Lozano, por el Ejército de Liberación Nacional estará Gustavo Martínez, Alejandro Montoya y Tomás García, por la Misión de Verificación de las Naciones Unidas en Colombia participará el General José Mauricio Villacorta y Jennifer Wright y por la Conferencia Episcopal de Colombia el Padre Darío Echeverry.

### **11 días del Cese Bilateral con un balance positivo** 

De acuerdo con el Centro de Recursos para el Análisis del Conflicto, desde el inicio de este **cese, la intensidad del conflicto armado ha disminuido significativamente, así como también se ha reducido la pérdida de vidas humanas.**

De igual forma la CERAC pudo establecer que los combates se han disminuido **en un 42%, en comparación con el mismo periodo durante el 2016**, para el Centro de Recursos, este hecho significa “una mayor cohesión del ELN en la mesa en Quito” y que las estructuras de esta misma guerrilla han acatado las decisiones del comando central en las regiones en las que hace presencia.

Por su parte Indepaz, a través de su más reciente informe de análisis al cese bilateral, señaló que se han presentado 33 enfrentamientos con la Fuerza Pública, **se registraron 59 ataques del ELN,  39 ataques a infraestructuras**, 13 homicidios en donde se pudo verificar la culpabilidad del ELN, 46 personas heridas que hacen parte de la población civil, 53 secuestros, 39 liberaciones, 3 secuestros fugados, 234 capturas a integrantes de esta guerrilla, 24 milicianos muertos en combate, 61 miembros de la Fuerza Pública heridos y 47 muertos en combate.

A su vez, el informe da cuenta de que los departamentos en donde más se ha intensificado el combate entre estas estructuras es Chocó, con 53 acciones y Norte de Santander con 43, regiones en donde tradicionalmente ha existido presencia de esta guerrilla. (Le puede interesar:["Misión de la ONU tendrá 33 equipos para verificar cese al fuego entre ELN y Gobierno"](https://archivo.contagioradio.com/mision-de-la-onu-tendra-33-equipos-para-verificar-cese-al-fuego-entre-gobierno-y-eln/))

###### Reciba toda la información de Contagio Radio en [[su correo]
