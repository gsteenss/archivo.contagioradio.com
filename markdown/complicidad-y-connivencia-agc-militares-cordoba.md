Title: Confirman complicidad y connivencia de AGC y militares en Córdoba
Date: 2017-04-23 19:51
Category: DDHH, Nacional
Tags: Paramilitarismo
Slug: complicidad-y-connivencia-agc-militares-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/paramilitares-cordoba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [23 Abr 2017]

Según denunció la comisión de Derechos Humanos de Marcha Patriótica en el departamento de Córdoba, y concretamente en la vereda Alto Cuartillo del Municipio de Saiza, se evidenció la connivencia de paramilitares de las llamadas Autodefensas Gaitanistas de Colombia, AGC, con las Fuerzas Militares, **concretamente del Batallón de Infantería No. 33 Junín de la Brigada XI del Ejército.**

Según la denuncia el pasado 22 de abril “Hombres de las AGC llegaron de civil y con un radio, a buscar al presidente de la JAC de Alto Cuartillo a la vereda del Llano, donde había presencia del Batallón de Infantería No. 33 Junín de la Brigada XI del Ejército. Citaron a las dos familias de A**lto Cuartillo en el segundo piso de una casa, en el primero estaba el Ejército, y en el segundo dos paramilitares armados con pistola**”.

Este hecho pone en evidencia que no solamente en el departamento del Chocó donde los paramilitares de las AGC afirmaron contar con el apoyo de las FFMM para el desarrollo de sus operaciones, sino que también esta situación se da en el departamento de Córdoba y hay otras denuncias de presencia de paramilitares en **regiones de control militar con en las veredas de San José de Apartadó y de la Comunidad de Paz.** ([Lea también: Asi es el control paramilitar en San José de Apartadó](https://archivo.contagioradio.com/paramilitares-controlan-veredas-de-san-jose-de-apartado/))

La comisión de DDHH de Marcha también denunció que varias familias han sufrido una fuerte persecución por parte de las AGC que los acusa, supuestamente de ser colaboradores de la guerrilla, por lo que 2 familias, de 4 familias, se vieron en la obligación de buscar resguardo en el PTN de Gallo en ese mismo departamento. ([Lea también: Paramilitarismo se afianza en Colombia](https://archivo.contagioradio.com/?s=paramilitares))

### **Desde Octubre de 2016 se ha incrementado la presencia de paramilitares** 

Además, denuncian las comunidades que la presencia de las AGC se ha venido incrementando desde Octubre de 2016, con cobros de vacunas, reclutamiento forzado de menores de edad y amenazas a los pobladores que aseguren que no trabajarán con ellos. Incluso habrían citado a los comités de cocaleros de la región para seguir afianzando el control que vienen realizando también de los cultivos de uso ilícito. ([Le puede interesar: Asesinado presidente de ASODECAS](https://archivo.contagioradio.com/asesinado-dirigente-de-asodecas-jose-yimer-cartagena/))

Esta situación que se presenta en el departamento de Córdoba, también se repite en el pacífico y los departamentos del Chocó, Nariño, Cauca, Antioquia entre otros. Sin embargo el gobierno central persiste en la afirmación de que no se trata de grupos paramilitares sino que se trata de Bandas criminales organizadas. Por su parte las propias comunidades siguen denunciando la complicidad de las FFMM y la persistencia del control paramilitar.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
