Title: La Rochela, 28 años sin respuesta del Estado colombiano
Date: 2017-01-18 13:35
Category: DDHH, Nacional, Sin Olvido
Tags: Corte Interamericana de Derechos Humanos, impunidad en Colombia, La Rochela, Magdalena Medio, Masacre de La Rochela, Paramilitarismo en el Magdalena Medio
Slug: la-rochela-28-anos-sin-respuesta-del-estado-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/LaRochela.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CNMH] 

###### [18 Ene 2017] 

El caso de la masacre ocurrida hace 28 años, el 18 de enero de 1989 en la vereda La Rochela, continúa en la impunidad. Hasta el momento la única condena por estos hechos es la que cumplió a mediados de 2015, el paramilitar Alonso de Jesús Baquero alias “el negro Vladimir”, quien también señaló a los militares Farouk Yanine Díaz, Juan Salcedo Lora, Carlos Julio Gil Colorado, Alfonso Vacca y al ex congresista Tiberio Villarreal Ramos, quienes aún no han sido formalmente acusados.

Jomary Ortegón abogada y presidenta de la Corporación Colectivo de Abogados José Alvear Restrepo –CCAJAR–, señaló que en su momento las autoridades judiciales ofrecieron la ley de justicia y paz como el medio para esclarecer los hechos, “pero ésta no ha servido para el esclarecimiento ni para garantizar justicia en el caso”. Le puede interesar: [Tras 27 años, masacre](https://archivo.contagioradio.com/masacre-la-rochela-continua-impunidad/)La Rochela[continúa en la impunidad.](https://archivo.contagioradio.com/masacre-la-rochela-continua-impunidad/)

### La situación de los responsables 

La abogada explicó que dos de los responsables intelectuales del crimen, Ernesto Báez y Ramón Isaza conocidos comandantes paramilitares, reconocieron su responsabilidad por línea de mando pero “no aportaron absolutamente nada en materia de verdad sobre la comisión de la masacre y hoy se encuentran en libertad no por haber aportado a la justicia, sino por el cumplimiento de los ocho años que es el máximo de sanción que establece la ley”.

Además, indicó que las únicas condenas son las de alias ‘Vladimir’ y dos integrantes de la fuerza pública, pero estos últimos “están prófugos hace mas de 20 años y no hay información de otros integrantes de las Fuerzas Militares que pueden estar implicados”.

Por otra parte, el militar Farouk Yanine Díaz falleció antes de que pudiera resolverse su situación jurídica y el congresista Tiberio Villareal, quien también fue llamado a ser investigado, tampoco ha respondido ante la justicia, “después de 28 años no existe para las víctimas verdad ni justicia frente a lo ocurrido sobre sus seres queridos ni sobre quién ordenó”, agregó Ortegón.

### La sentencia de la Corte Interamericana no ha sido cumplida 

En mayo de 2007 la Corte Interamericana de Derechos Humanos, condenó al Estado colombiano por la masacre de La Rochela, y en la sentencia dispuso una serie de medidas de reparación integral como becas estudiantiles, oportunidades laborales y atención en salud para las familias.

De acuerdo con la abogada, la mayoría de medidas relativas a la dignificación de la memoria de los funcionarios judiciales asesinados fueron cumplidas, se instalaron medidas recordatorias, hubo actos de reconocimiento de responsabilidad por parte del Estado, pero los demás compromisos adquiridos con las familias no se han cumplido. Le puede interesar: [CIDH reitera la grave situación de impunidad y violación de Derechos Humanos en Colombia.](https://archivo.contagioradio.com/cidh-reitera-la-grave-situacion-de-impunidad-y-violacion-de-derechos-humanos-en-colombia/)

Por último, Jomary Ortegón manifestó que desde la CCAJAR se seguirá trabajando en el esclarecimiento de lo sucedido con los integrantes de la comisión judicial que investigaba una serie de desapariciones, hostigamientos y homicidios cometidos por el paramilitarismo en los municipios de Simacota, Cimitarra y Puerto Parra, en el Magdalena Medio.

###### Reciba toda la información de Contagio Radio en [[su correo]
