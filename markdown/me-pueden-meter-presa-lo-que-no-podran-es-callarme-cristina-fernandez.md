Title: "Me pueden meter presa, lo que no podrán es callarme" Cristina Fernández
Date: 2016-04-13 16:05
Category: El mundo, Otra Mirada
Tags: Argentina, Cristina Fernández de Kirchner, La causa del dólar futuro
Slug: me-pueden-meter-presa-lo-que-no-podran-es-callarme-cristina-fernandez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/kirchner.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Telam 

###### 13 Abr 2016 

Luego de comparecer ante los tribunales de Comodoro Py por la presunta malversación de que se le acusa, la ex-presidenta argentina Cristina Fernández de Kirchner, se dirigió de manera sentida a los miles de ciudadanos que desde su retorno al país le acompañan, invitándolos a permanecer unidos ante las "graves calamidades" que vive la nación.

En su intervención Fernández de Kirchner, insistió que su llamado a comparecer ante la justicia hace parte de una "persecución política en su contra" con el objetivo según sus declaraciones de llevarla a prisión. “Me pueden citar 20 veces más, me pueden meter presa, pero lo que no podrán es callarme e impedirme estar con ustedes”, aseguró la ex-mandataria.

Ante la difícil situación que vive la Argentina en lo que Fernández denomina como "120 días de calamidades", la ex presidenta invitó al pueblo a conformar un 'Frente Patriótico' que integre sin distinción a todas y todos los ciudadanos, que exija al congreso, como responsable del poder legislativo, el cumplimiento de las conquistas sociales alcanzadas durante su gobierno.

La ex presidenta se refirió además al papel que han tenido los medios de comunicación a la hora de darle cobertura internacional de gran magnitud a su citación judicial y mientras que la posición de los medios locales en relación con las cuentas offshore que involucran a Macri y algunos de sus funcionarios, que han cubierto los titulares y portadas de la prensa mundial menos en la Argentina.

**Sobre el proceso.**

Ante el juez federal Claudio Bonadío, Cristina Fernández se presentó desde las 10 a.m. a comparecer por la la causa del dólar futuro, por el que acusa a la ex mandataria de dar la orden al Banco Central de vender dólares en el mercado de futuros a un precio muy bajo, violando la Carta Orgánica del banco y generando un "grave perjuicio" a las arcas de la nación.

Fernández de Kirchner, entregó al juez un escrito de 4 puntos, en los cuales da cuenta de la manera en que desde su salida ha iniciado un proceso de persecución y descalificación de su gestión y el desmonte de las conquistas y derechos adquiridos por la sociedad, citando ejemplos históricos de comportamientos similares. Como segundo punto la ex presidenta manifiesta que se trata de un "ejercicio abusivo de poder" con múltiples errores de forma y de fondo.

Al concluir la audiencia, la ex mandataria no respondió preguntas de la prensa, dirigiéndose directamente al escenario donde le esperaban algunos de sus seguidores quienes se encontraban desde horas de la noche acampando y otros que empezaron a llenar el lugar donde horas más adelante se presentarían algunos desmanes por la presencia policial.

[Escrito Cristina Fernandez de Kirchner](https://es.scribd.com/doc/308438900/Escrito-Cristina-Fernandez-de-Kirchner "View Escrito Cristina Fernandez de Kirchner on Scribd")

<iframe id="doc_75054" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/308438900/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
