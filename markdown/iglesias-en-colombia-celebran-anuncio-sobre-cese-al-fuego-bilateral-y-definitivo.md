Title: Iglesias en Colombia celebran anuncio sobre cese al fuego bilateral y definitivo
Date: 2016-06-23 11:19
Category: Nacional, Paz
Tags: Cese al fuego bilateral, conversaciones farc y gobierno Colombia, Diálogos de La Habana
Slug: iglesias-en-colombia-celebran-anuncio-sobre-cese-al-fuego-bilateral-y-definitivo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Dipaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: DiPaz ] 

###### [23 Junio] 

"Celebramos y afirmamos con esperanza el camino que continúa con este anuncio. Es un acto que tiene trascendencia para Colombia y el mundo. **Se trata de poner punto final a una confrontación armada entre el Estado Colombiano y la guerrilla de las FARC-EP por más de medio siglo**", aseguran los representantes de iglesias y organizaciones basadas en la fe, articuladas en DiPaz, luego de conocer que este jueves en La Habana se firmarán los acuerdos sobre el punto fin del conflicto.

El anuncio para las iglesias representa "un signo de los tiempos y una Buena Nueva (...) un paso importante de acciones no violentas" que implican el silenciamiento de las armas, **la no afectación a la población civil, el respeto por la diferencia y el diálogo, como opción de acuerdo político**, así como el respeto por un ambiente sano y la necesidad de consolidar justicia ambiental en los territorios.

Yenny Neme, integrante de DiPaz, asevera que este anuncio se esperaba desde hace mucho tiempo pues constituye un gran desafío para la continuidad del proceso con el ELN y el desmonte de los **14 grupos paramilitares que persisten en 149 municipios de 22 departamentos  del país**. "Por tanto, hacemos un llamado al gobierno colombiano y al ELN a iniciar la fase de conversaciones y a las organizaciones y la sociedad en general a velar por el desmonte de las organizaciones criminales sucesoras del paramilitarismo".

Este jueves iglesias de distintas vertientes religiosas realizarán celebraciones litúrgicas en diferentes puntos del país en acción de gracias y "en pro de la verdad, la justicia, la paz y el camino hacia la reconciliación", e invitan a que continúen durante el fin de semana.

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
