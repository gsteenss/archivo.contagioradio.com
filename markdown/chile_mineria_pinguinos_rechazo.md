Title: Chile rechaza proyecto minero para proteger pingüinos en peligro de extinción
Date: 2017-08-22 20:18
Category: Voces de la Tierra
Tags: Chile, Mineria
Slug: chile_mineria_pinguinos_rechazo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Reserva-pinguino-de-humbolt-e1503451015228.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Chile Travel 

###### [ 22 Ago 2017] 

Proteger la vida de una especie de pingüinos en peligro de extinción, ha sido la principal causa por la cual, en Chile un **comité integrado por los ministros de Ambiente, Energía, Minería, Agricultura, Economía y Salud** de Chile ha decidido impedir el desarrollo del megaproyecto minero Dominga, de la empresa Andes Iron.

La decisión se ha tomado en medio de una votación dividida que finalmente acabó determinando que, por los efectos ambientales, y la ver que el proyecto no cuenta con suficientes medidas de mitigación, se pondría en riesgo **la Reserva Nacional Pingüino de Humboldt, donde habita el 80 % de la población mundial de pingüinos de Humboldt.**

“No se hace cargo de manera adecuada de los eventuales impactos significativos al medioambiente y la salud de las personas; en particular en temas como calidad de aire, contaminación lumínica, ruido, riesgo de accidentes y derrames en la operación portuaria”, señaló Marcelo Mena, Ministro de Ambiente de Chile e integrante del Comité de Ministros para la Sustentabilidad.

A lo anterior, se suma que el Comité se adhiere a lo que ya había determinado la Comisión de Evaluación Regional de Coquimbo, que ya había calificado de "desfavorable al proyecto”.

El objetivo del proyecto Dominga era desarrollar una mina de hierro y cobre, además de la construcción de un puerto en el municipio de La Higuera, lo que hubiese requerido **una inversión de 2.500 millones de dólares**.

Por su parte, según medios chilenos, Iván Garrido, presidente de la minera Andes Iron, calificó como una "arbitrariedad" el veredicto, añadiendo que fue una decisión que no es democrática. Asimismo lo criticó la Sociedad Nacional de Minería de Chile asegurando que se trató de una determinación **"política" y "sin justificación".**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
