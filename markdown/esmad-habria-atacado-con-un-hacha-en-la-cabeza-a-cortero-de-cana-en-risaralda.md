Title: Cortero de caña en cuidados intensivos tras agresión del ESMAD
Date: 2015-03-04 17:55
Author: CtgAdm
Category: Movilización, Nacional
Tags: corteros, Corteros de Caña en Colombia, CUT, luis eduardo garzon, sintrainagro
Slug: esmad-habria-atacado-con-un-hacha-en-la-cabeza-a-cortero-de-cana-en-risaralda
Status: published

###### Foto: CUT 

 <iframe src="http://www.ivoox.com/player_ek_4164990_2_1.html?data=lZajlp6ddI6ZmKiak5eJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibTBoqmYys7WrYa3lIqvlZDHs8%2Bf1tOYysbHrMKfxtOYzsaPp8LWxt%2FOjcaPuNPVw8bXw8nTtozX0Neah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Guillermo Rivera, Presidente de SINTRAINAGRO] 

<iframe src="http://www.ivoox.com/player_ek_4165020_2_1.html?data=lZajl5WWdI6ZmKiakpmJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjNDn0c7hw9GPcYy5tLKuppDMrdPdhqigh6eXb8Tjz5Di0JDMpcTcwpDS0JDQpYzXwsfS3MaPpYzo08aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#####  [Marta Ramirez, Hospital Universitario San Jorge de Pereira] 

La madrugada de este martes 3 de Marzo, efectivos del **ESMAD atacaron el sitio de concentración de los 500 corteros de caña del ingenio Risalralda que acampaban frente a una de las plantas procesadoras,** para pedir más trabajo que el que asignan diariamente a cada cortero. A pesar de no estar bloqueando ninguna entrada, ni taponando ninguna vía, fueron brutalmente reprimidos por el ESMAD que los sorprendió mientras dormían, sin mediar aviso de desalojo. El resultado fue de **10 corteros heridos, uno de ello Luis Carlos Ossa Trejos.**

Ossa Trejos, ingresó al Hospital Universitario San Jorge, de Pereira, el martes 3 de marzo en horas de la madrugada. El diagnóstico médico determinó (según Marta Ramirez, jefe de comunicaciones del Hospital) trauma cráneo-encefálico resultado de una herida generada por un arma corto punzante. Actualmente **el cortero está en la Unidad de Cuidados Intensivos de la institución con pronóstico reservado.**

Carlos Ossa, de 40 años de edad, trabajó  los últimos 11 como cortero de caña del Ingenio Risaralda. En los últimos meses, él y sus compañeros afiliados al **Sindicato Sintrainagro,** filial de la CUT, han visto como se violan sus derechos laborales: mientras a los trabajadores no sindicalizados se les asignan entre 60 y 80 metros para cortar caña, a los trabajadores afiliados en Sintrainagro les asignan 20 metros.

Trabajan menos de 3 horas diarias, y ganan 20 mil pesos. Por este motivo, los cerca de 500 corteros de caña del **Ingenio Risaralda** decidieron realizar un plantón frente a las instalaciones de la empresa cortera, para exigir el cumplimiento de sus derechos laborales. "Ellos están en una situación muy difícil. Lo que están pidiendo, es trabajo, los compañeros", afirma **Guillermo Rivera, presidente de Sintrainagro.**

Mientras las directivas de Sintrainagro y de la CUT se encontraban en Bogotá, buscando una reunión con el Ministro de Trabajo, Luis Eduardo Garzón, para buscar soluciones al problema laborales generados por el Ingenio, la empresa envió al ESMAD a desalojar el plantón de los trabajadores. A las 3 de la mañana, el Escuadrón Móvil atacó a los trabajadores, que dormían en los cambuches improvisados, con gases lacrimógenos, golpes y machetasos.

10 trabajadores resultaron heridos, entre ellos, **Carlos Ossa, quien recibió un golpe con un hacha en la cabeza, que según afirman sus compañeros**, hizo que perdiera uno de sus ojos. Fue trasladado a Pereira y operado el mismo martes 3 de marzo. El Hospital afirma que el paciente está estable, pero que no puede dar más información porque el personal médico debe evaluar la evolución post operatioria, las horas más complicadas.

Entre tanto, los corteros de caña esperan reunirse con el **Vice-Ministro de Trabajo** para solucionarlos los problemas laborales que presenta el ingenio. "Este no es un problema de orden público, es un problema laboral que lo tiene que resolver el Ministerio del Trabajo y las empresas que están violando los derechos de los trabajadores", declaró Rivera. No puede ser que "si la empresa no logra resolver los problemas con los trabajadores, entonces van a resolverlos con la policía en condiciones infrahumanas".

"Estamos solicitando a la empresa que de manera responsable responda por los derechos de los trabajadores, y a la fuerza publica que responda por los trabajadores que fueron agredidos", concluyó Rivera.
