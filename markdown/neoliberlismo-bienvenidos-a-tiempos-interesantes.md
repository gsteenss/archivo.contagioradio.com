Title: Bienvenidos a tiempos interesantes
Date: 2016-01-27 10:29
Category: Javier Ruiz, Opinion
Tags: ISAGEN, multinacionales, neoliberalismo
Slug: neoliberlismo-bienvenidos-a-tiempos-interesantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/neoliberaismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: LibreRed] 

#### **[Javier Ruíz](https://archivo.contagioradio.com/javier-ruiz/)- [@ContraGodarria ](http://contragodarria/)** 

###### [27 Ene 2016] 

Slavoj Zizek, filósofo esloveno, decía que en China si odias a alguien se le suele maldecir diciéndole “que vivas en tiempos interesantes”. A su vez, Zizek afirma que los “tiempos interesantes” son estos que estamos viviendo a nivel global como las guerras, el posible fortalecimiento de los radicalismos (como el islámico o el fascista y xenofóbico en los EE.UU. y en Europa) un resurgir de fuerzas de derecha y de extrema derecha en países europeos y América Latina no está siendo ajena a un posible resurgir del neoliberalismo que se suponía derrotado con los denominados gobiernos “posneoliberales” de algunos países sudamericanos como Venezuela, Ecuador, Argentina o Bolivia.

El 2016 empezó muy difícil tanto a nivel nacional como a nivel continental. Los triunfos de la oposición de extrema derecha en Venezuela y el triunfo de Macri en Argentina es una muestra de que la tendencia neoliberal en el manejo del Estado y de la gestión pública quiere volver a los años 90 donde imperaban las privatizaciones y la reducción de los derechos de los ciudadanos.

En el caso de Argentina Mauricio Macri empezó de la peor manera que, a punta de “decretazos” como un dictador, está destruyendo los avances que la Argentina había conseguido tras su crisis en el año 2001. Macri, cuyo tipo ideal es Menem, ha iniciado un proceso de “restauración neoliberal” que consiste en privatizar todo lo que pueda y crear un ambiente favorable al gran capital nacional y extranjero sin tener en cuenta las demandas ciudadanas y callar voces disidentes como el del periodista deportivo Víctor Hugo Morales o la líder política Milagro Sala que ha sido detenida volviéndose en una prisionera política. Vaya ironía si tenemos en cuenta que Macri pretende dividir el continente en defensa de la ideología neoliberal como lo hizo en Mercosur atacando a Venezuela por defender a Leopoldo López y en esa defensa salió mal librado haciendo un “papelón” de no olvidar.

En Venezuela la oposición de extrema derecha busca como sea derrocar a Maduro para imponer, obviamente no bajo lógicas democráticas, su proyecto neoliberal de privatizaciones. Esa oposición no busca gobernar en pro del pueblo sino en su sed de revancha por el poder perdido desde los triunfos de Chávez busca recuperar el poder para favorecer a grandes capitales y tener influencia sobre el petróleo que, a pesar de estar en caída los precios, es un recurso valioso. No buscan la libertad y la democracia supuestamente arrebatadas. Antes quieren como sea reducir los espacios democráticos para que el chavismo no participe y sea excluido y si es de ser necesario recurrirán a las “guarimbas” y a las acostumbradas acusaciones de “dictadura” o “tiranía”.

En Colombia el neoliberalismo ha estado vigente pero parece que en este año quiere ser más agresivo. La venta de Isagen y la posible venta de la ETB ponen de nuevo el debate sobre la conveniencia o no de las privatizaciones. Desde los años 90 el Estado ha privatizado sectores estratégicos con la excusa de desarrollar más el país pero han sido un rotundo fracaso y las empresas privatizadas son ineficientes e ineficaces. El neoliberalismo es un discurso fracasado y una tendencia que no ha servido a la democracia ni a la libertad por sus decisiones impopulares que afectan a la gran mayoría mientras unos pocos se benefician de ello.

Así que no me queda más que decirles que bienvenidos a tiempos interesantes.
