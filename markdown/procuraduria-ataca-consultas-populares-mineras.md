Title: Procuraduría ataca Consultas Populares Mineras
Date: 2016-11-02 16:16
Category: Ambiente, Nacional
Slug: procuraduria-ataca-consultas-populares-mineras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/consulta-ibague.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las2 Orillas] 

###### [2 de Nov 2016] 

**La Procuraduría General de la Nación expresó en un comunicado de prensa observaciones** contra las consultas populares mineras, entre ellas menciona la alta demanda de inversión de recursos públicos y el impacto en las finanzas para el país, que de acuerdo con el texto sería incluso peor que el de la Reforma Tributaria. Este comunicado, de acuerdo con especialistas en ambiente como el abogado Jorge Negrete, **atenta contra las decisiones autónomas de los entes territoriales y la participación ciudadana**.

“**La Procuraduría está del lado del más fuerte que incluyendo las empresas mineras** que vivieron de un  boom y finalmente eso nunca se retribuyo al país, ahorita que los precios del petróleo están bajos, entonces el sacrificio es de los Colombianos y de los derechos fundamentales” afirmó Negrete. Las consultas populares además, tienen fallos de las Altas Cortes, en este caso de la Corte Constitucional, que reconocen la existencia de mecanismos participativos para los municipios que regule el uso de los suelos. Le puede interesar: ["Paralizadas 516 áreas mineras por no consultar a las comunidades"](https://archivo.contagioradio.com/paralizadas-516-areas-mineras-por-no-consultar-a-las-comunidades/)

El documento señala que “De llevarse a cabo las consultas populares para prohibir las mencionadas actividades extractivas el impacto en las finanzas públicas sería aún mayor y la reforma tributaria estructural que el gobierno Nacional adelanta “pasaría a ser una simple reforma más para tapar el grave hueco fiscal”. **Afirmaciones pasarían por encima de la sentencia 445 y C-123 de la Corte Constitucional**

“La Procuraduría da unos argumentos que se escapan mucho de lo que debe ser su función como Ministerio Público que fundamentalmente **debe velar es por el cumplimiento de las garantías de los derechos fundamentales de los colombianos** y no estar pensando en temas económicos de las empresas” expreso el abogado Rodrigo Negrete.

Negrete también cuestiona por qué en vez de que la Procuraduría se inquiete por las consultas mineras, **no lo hace por la cantidad de títulos y licencias mineras otorgadas en el país sin consultar a las comunidades o en zonas que están protegidas como páramos**, situaciones que deberían ser su trabajo y preocupación.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
