Title: Iglesias enfilan baterías hacia el desarrollo sostenible
Date: 2018-11-15 17:42
Author: AdminContagio
Category: Nacional, Paz
Tags: Desarrollo Sostenible, Naciones Unidas, PNUD
Slug: iglesias-enfilan-baterias-hacia-el-desarrollo-sostenible
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-15-at-5.38.37-PM-770x400.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: World Vision 

###### 15 Nov 2018 

Con el fin de destacar el rol de la iglesia como un actor clave en la construcción de paz, este 15 de noviembre será el lanzamiento oficial de **"Despertando al Gigante"** una plataforma impulsada por **World Vision y el Concilio de las Asambleas de Dios** la cual fomentará estrategias intereclesiales para cumplir los Objetivos de Desarrollo Sostenible planteados para el 2030 por el Programa de las Naciones Unidas para el Desarrollo.

Según **Viviana Machuca, **directora de relaciones interinstitucionales de World Vision, son  diversos los organismos de cooperación internacional quienes han determinado que una forma de cumplir dichos objetivos es **lograr movilizar la política pública en el país, y alinear sectores religiosos alrededor de la construcción de paz, género y ambiente**, razón por la cual “Despertando al gigante” surge como una apuesta de cambio impulsada por diversos grupos religiosos.

El proyecto ha lanzado cuatro pilotos en cuatro países diferentes: **Liberia, Tanzania, Estados Unidos y Colombia,** este último en particular fue escogido de toda **Latinoamérica** por la coyuntura en términos de construcción de paz, por haber establecido alianzas en temas de cooperación internacional durante su proceso de paz y por su ubicación geográfica que vincularía la parte sur y centro del continente

El mayor de los desafíos que enfrentará la plataforma será el de **lograr unidad en medio de la pluralidad de credos del país**, es por eso que se ha pensado llegar a dos tipos de sectores religiosos: el primero, mucho más cercano al lenguaje de la cooperación internacional y que debe ser  asesorado, mientras el otro sector, mucho más amplio y diverso al que se deberá sensibilizar en lo referente a la consecución de dichos objetivos e integración.

“Despertando al gigante” cuenta con el apoyo de la **Iglesia Sueca, la Subdirección de Libertad Religiosa y de Conciencia de la Alcaldía Mayor de Bogotá y la asesoría de la Dirección de Asuntos Religiosos del Ministerio del Interior** que facilitó la interlocución y las relaciones estratégicas con organizaciones como Programa de las Naciones Unidas para el Desarrollo.

Mientras que en Tanzania dicha plataforma ha funcionado durante seis meses, en el resto de países se están iniciando actividades y realizando acuerdos de voluntad entre diversos sectores, se espera que en un año se pueda compartir la experiencia en cada país y hacer de Colombia una experiencia piloto para llevar la iniciativa al resto de países suramericanos.

###### Reciba toda la información de Contagio Radio en [[su correo]
