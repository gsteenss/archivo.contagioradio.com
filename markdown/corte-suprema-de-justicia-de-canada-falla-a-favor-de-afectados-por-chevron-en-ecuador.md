Title: Corte Suprema de Justicia de Canadá falla a favor de afectados por Chevron en Ecuador
Date: 2015-09-07 08:10
Category: Ambiente, El mundo, Entrevistas
Tags: Campesinos ecuatorianos, Chevron, Corte Suprema de Justicia de Canadá, Corte Suprema de Justicia de Nueva York, ecuador, Indígenas ecuatorianos
Slug: corte-suprema-de-justicia-de-canada-falla-a-favor-de-afectados-por-chevron-en-ecuador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Ecuador.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: archivo Unidad de los Pueblos y los Trabajadores ] 

<iframe src="http://www.ivoox.com/player_ek_7924444_2_1.html?data=mJ6flpmYeI6ZmKiak5aJd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dm1cqYtdrUtsbhwpDRx5CuudToysjWw5DIqYy3wtPOxoqnd4a1kpDTw9HQpYzVjMvO2NTWb8XZjMaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Humberto  Piaguaje, UDAPT] 

###### [6 Sept 2015] 

[La Corte Suprema de Justicia de Canadá ratificó el fallo emitido en segunda instancia por el Tribunal de Apelaciones de Ontario, reconociendo que los 30.000 indígenas y campesinos ecuatorianos demandantes de la Petrolera Chevron, pueden hacer efectiva la sentencia que exige su indemnización con USD 9.500 millones, al comprobar la **responsabilidad de la multinacional en los daños ambientales provocados en la Amazonia ecuatoriana**.  ]

[Humberto  Piaguaje, Coordinador de la Unión de Afectados/as por Texaco UDAPT, afirma que pese a la **campaña de desprestigio de la corte de Nueva York**, quien dictaminó que se trataba de una extorsión a la compañía, la ratificación del fallo es un gran avance. **Tras 22 años de litigio se podrá iniciar la reparación de los territorios en términos ambientales, sociales, culturales y de infraestructura**.  ]

[Piaguaje, asegura que **la indemnización es insuficiente para reparar los impactos ambientales y a la salud de muchos de los habitantes**, pues varios de ellos, murieron padeciendo cáncer y centenares de especies animales y vegetales fueron destruidas, tras 27 años de extracción petrolera.]

[Se espera que inicie el juicio de cobro en Canadá y que el dinero, tal como lo indica la sentencia, vaya a un fideicomiso integrado por Ministerios Nacionales y autoridades indígenas, para garantizar efectivo control en el manejo del recurso en la implementación de las acciones de reparación a los territorios impactados por la transnacional Chevron.]
