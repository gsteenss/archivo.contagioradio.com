Title: Corte Constitucional da la razón a defensores del páramo de Santurbán
Date: 2017-11-07 13:05
Category: Ambiente, Voces de la Tierra
Tags: Corte Constitucional, delimitación páramo de Santurbán, medio ambiente, Páramo de Santurbán, santurbán
Slug: corte-constitucional-da-la-razon-a-defensores-del-paramo-de-santurban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Paramo-de-Santurban-e1472756604235.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Macondo] 

###### [07 Nov 2017] 

Luego de que la Corte Constitucional derogara la delimitación del Páramo de Santurbán exigiendo que las comunidades **deben ser consultadas sobre los alcances de esta reserva natural**, el Comité por la Defensa del Páramo de Santurbán indicó que es una decisión satisfactoria en la medida que le da la razón a las comunidades. Mientras no haya delimitación, los proyectos mineros deben ser suspendidos.

De acuerdo con Dadán Amaya, vocero del Comité por la Defensa del Páramo de Santurbán, “cuando la delimitación se anunció por primera vez, debido a la forma en que se anunció, nos pronunciamos en contra dado que el **Ministerio de Ambiente dijo que no habría participación por parte de la comunidad**”. Por esto, han saludado la decisión de la Corte Constitucional que, valida las exigencias de las comunidades de poder decidir sobre sus territorios.

### **Con el fallo, la Corte reconoce que la comunidad debió haber participado en la delimitación** 

Amaya indicó que la primera delimitación del páramo en 2014 la hizo el Gobierno Nacional teniendo en cuenta cierto criterios “técnicos, sociales y económicos”. Sin embargo, para los defensores del ambiente **significó la apertura a los proyectos minero energéticos** que se han querido desarrollar en este páramo. Además, esta delimitación le impedía a los mineros artesanales y campesinos realizar sus labores de siembra. (Le puede interesar:["Masiva marcha en Santander exige que se proteja el páramo de Santurbán"](https://archivo.contagioradio.com/movilizacion_santurban_proyecto_minero/))

El fallo de la Corte Constitucional, que aún no ha sido notificado al Comité del páramo, indica, según Amaya, que “**las comunidades debieron haber sido consultadas en la decisión de la delimitación del páramo”**. Dijo que es una noticia positiva en la medida que la delimitación “es el modelo que el Gobierno decidió aplicar y que ha estado lleno de vicios y sesgos a favor de la minería”.

### **¿Falta de delimitación abre la puerta a los proyectos mineros?** 

El Comité manifestó que “más allá de la delimitación, todo proyecto minero que se haga en ecosistemas que son esenciales para la producción de agua, **son inviables y no se pueden desarrollar**”. Por esto, dijo que la minería en Santurbán y los proyectos que se realizan “están por encima de las bocatomas del acueducto por lo que no se pueden realizar”.

Afirmó que, tras la medida de derogar la delimitación, **sigue un proceso participativo** y “mientras tanto los proyectos que puedan generar afectaciones ambientales en el ecosistema de páramo tienen que ser suspendidos”. Con esto en mente, el comité ambiental le ha pedido al Gobierno que cumpla con el principio de precaución mientras se toma la decisión de delimitación. (Le puede interesar: ["Piden a la ANLA publicar estudio de impacto ambiental en páramo de Santurbán"](https://archivo.contagioradio.com/defensa-del-paramo-de-santurban/))

Amaya recordó que, así en este momento no haya una delimitación del páramo, “hay que insistir en que, **si el proyecto se encuentra en un ecosistema proveedor de agua** para cualquier población, simplemente no se puede desarrollar”. Es por esto, que los defensores del ambiente han manifestado en varias ocasiones que hay factores más allá de la delimitación que impiden el desarrollo de proyectos mineros.

### **Comunidades tienen propuesta para delimitación** 

El ambientalista indicó que, a lo largo de la discusión por la realización de los proyectos minero energéticos en este territorio, “las comunidades han perfilado las opciones de delimitación en razón que han exigido que se les permita hacer agricultura de subsistencia y minería artesanal”. Así, han estado de acuerdo en que los grandes proyectos mineros de empresas como Minesa y Eco Oro, **generan un gran impacto en el desarrollo de actividades** de la comunidad y el ambiente que los surte de agua.

Finalmente, Amaya manifestó que el único proyecto que está en etapa de estudio en la Agencia Nacional de Licencias Ambientales es el “Soto Norte” de Minesa. Este proyecto, que está a 2640 metros sobre el nivel del mar y al lado de lo que el Gobierno consideró como páramo, **“contempla 500 kilómetros de construcción de túneles** dentro de la montaña”. (Le puede interesar: ["Ambientalistas piden al Banco Mundial no financiar proyecto en Santurbán"](https://archivo.contagioradio.com/ambientalistas-exigen-al-banco-mundial-no-financiar-proyecto-santurban/))

Ante la delimitación, el ambientalista indicó que el Gobierno **debe comenzar un proceso de delimitación participativa** “que contemple la posición de la población del páramo de Santurbán y de la población del área metropolitana de Bucaramanga”. Además, “deberán excluir los procesos de minería a gran escala”.

<iframe id="audio_21935500" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21935500_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
