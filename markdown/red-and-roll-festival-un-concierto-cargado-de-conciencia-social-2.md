Title: Red and Roll Festival: un concierto cargado de conciencia social
Date: 2019-03-21 17:54
Category: eventos
Tags: Conciencia Social, Hardcore, Punk proletario, Red and Roll Festival
Slug: red-and-roll-festival-un-concierto-cargado-de-conciencia-social-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-20-at-9.50.03-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 19 Mar 2019 

El próximo 30 de marzo tendrá lugar la primera edición del **Red And Roll Festival**, una iniciativa que a través del punk, hardcore y el metal busca manifestarse en contra de la guerra, la inequidad y las injusticias sociales que históricamente se han vivido en Colombia.

Según **Javier Castro, uno de los promotores del evento**, el festival nace "como una necesidad de romper con el cerco impuesto por el emporio de medios de comunicación e industria del espectáculo"  y con la idea de promover el talento y difundir la música que con esfuerzo y de manera independiente se continúa produciendo en nuestro país.

![WhatsApp Image 2019-02-25 at 2.30.14 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-02-25-at-2.30.14-PM-572x800.jpeg){.alignnone .size-medium .wp-image-63473 .aligncenter width="572" height="800"}

El evento tendrá lugar en el Ace of Spades Club en la Avenida Boyacá \#51-25 y las entradas pueden ser adquiridas en **Revolution Store, Rolling Disc,  House of Wings, Café  León y SPR Shop,** por un costo de \$70.000 por boleta.

La banda que encabezara el Red And Roll Festival es la agrupación española **Nucleo Terco**, un quinteto de rock proletario nacido en Madrid y que desde 2002 se han centrado en temáticas como la realidad de lucha de clases y la conquista del poder político a través de la música con notables influencias del punk y el hardcore.

<iframe src="https://www.youtube.com/embed/25uJ1fKu9is" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Junto a la agrupación ibérica, que pisara por primera vez tierras suramericanas,estarán la banda de heavy metal **Kilcrops**, que con una trayectoria de más de 20 años ha planteado varios cuestionamientos políticos y religiosos a través de sus canciones, compartiendo tarima con **Chite**, banda de punk que al mejor estilo de Ramones, ha puesto a saltar a los amantes de la música con sus canciones cargadas humor y crítica social.

<iframe src="https://www.youtube.com/embed/c5wfuRgVdq0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Se unen a esta alineación la agrupación de rock proletario **Brigada Oi!**, el punk feminista y juvenil de **Valdavia,** la energía hardcore de la agrupación **Mal Gusto **acompañada de  mensajes con una reflexión respecto a la realidad nacional harán vibrar a los asistentes de la primera edición de este **Red and Roll Festival.**

<iframe src="https://www.youtube.com/embed/Ej1O4mqdPSw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

######  
