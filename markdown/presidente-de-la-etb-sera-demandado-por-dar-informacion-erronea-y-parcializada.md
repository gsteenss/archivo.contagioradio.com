Title: Presidente de la ETB será demandado por dar información "errónea y parcializada"
Date: 2016-05-25 20:25
Category: Entrevistas, Judicial
Tags: Concejo de Bogotá, Enrique Peñalosa, ETB, Jorge Castellanos, Miguel Uribe, Plan distrital de desarrollo
Slug: presidente-de-la-etb-sera-demandado-por-dar-informacion-erronea-y-parcializada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/ETB.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: You Tube 

###### [25 May 2016] 

La Red de Justicia Tributaria anunció que denunciará penalmente a **Jorge Castellanos, presidente de la ETB, debido a que a través de sus declaraciones se inició una campaña para desprestigiar** la empresa de telecomunicaciones que sería vendida al sector privado si se aprueba en plenaria del Concejo de Bogotá su venta.

Aunque no se ha logrado desvalorizar el precio de la acción, que al contrario, pasó de \$519 al 30 de diciembre, a un valor actual de \$608, Justicia Tributaria señala que **Jorge Castellanos ha dado a conocer información “errónea y parcializada”**, para consolidar la supuesta necesidad vender las acciones de la ETB.

A su vez denuncian que la información bursátil no se ha manejado responsablemente y en cambio se ha ocultado otro tipo de datos para continuar con las declaraciones negativas que hacen parecer que la mejor opción es vender la ETB, pese a que entre 2003 y 2016 la ciudad recibió \$2,8 billones, según reportes de la propia ETB.

También se advirtió que **podría existir una posible demanda en contra de Miguel Uribe Turbay, secretario de Gobierno**, luego de haber divulgado que las deudas de la ETB eran de 600 millones de dólares, aunque realmente ascienden a  130 millones de dólares, de acuerdo con los datos de la La Red de Justicia Tributaria.

Con la venta de la ETB saldrían afectados no sólo los trabajadores de la compañía sino gran parte de la población estudiantil de la ciudad, esto debido a que la empresa durante los dos últimos años, le ha representado a la Universidad Distrital Francisco José de Caldas ingresos de entre 7 mil y 10 mil millones de pesos, así como la posibilidad de contar con el servicio de internet gratuito en 34 puntos de la ciudad, en los que se encuentran  bibliotecas y colegios públicos, siendo la ETB la única empresa que oferta internet de 150 megas.

Cabe recordar que [la venta del 84% de las acciones que tiene en la empresa de telecomunicaciones](https://archivo.contagioradio.com/venta-de-etb-sera-demandada-por-sindicatos/) se definirá en debate en la plenaria del Concejo este fin de semana.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
