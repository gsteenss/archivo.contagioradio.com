Title: Países garantes fueron claves en la superación del impase del Conejo en la Guajira
Date: 2016-02-24 13:06
Category: Entrevistas, Paz
Tags: Conversaciones de paz de la habana, FARC, Juan Manuel Santos
Slug: paises-garantes-fueron-claves-en-la-superacion-del-impase-del-conejo-en-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/FARC-1Conejo_contagioradio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: prensa] 

<iframe src="http://co.ivoox.com/es/player_ek_10559722_2_1.html?data=kpWil56bdpOhhpywj5WcaZS1lJ6ah5yncZOhhpywj5WRaZi3jpWah5yncbHVhqigh6aot8bnjMzO1MbSuMbnjMvix9fTsozXzcbjx9iPqc%2BfzcaY1drUqdPVxM6SpZiJhpTijMnSzpDNsY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Iván Cepeda, Polo Democrático] 

###### [24 Feb 2016] 

**Aunque el impase fue generado, en gran parte, por los medios de comunicación, tanto los países garantes del proceso de paz** como el Comité Internacional de la Cruz Roja tuvieron que intervenir para que se fijaran de nuevo los protocolos que permitieran a los integrantes de la delegación de paz de las FARC volver a Cuba para continuar las conversaciones de paz.

El Senador Iván Cepeda señala que “[hemos asistido a una serie de hechos que muestran las dificultades que va a tener, a futuro, el desarrollo del proceso de paz](https://archivo.contagioradio.com/farc-califican-de-polemica-injustificada-reacciones-frente-a-su-presencia-en-el-conejo/)” porque hay una serie de sectores que van a impedir que las tareas propias se desarrollen. Por otro lado lo que es novedoso es que **“tenemos a comandantes de la guerrilla haciendo esfuerzos para explicar el contenido de los acuerdos”, pero se hicieron muchos esfuerzos por darle una connotación de “crisis”**.

La discusión queda en el alcance de la **[pedagogía para la paz](https://archivo.contagioradio.com/farc-gobierno-y-pedagogia-de-paz/)** y los procedimientos para realizarla, el asunto es ahora llevar a consensos los procedimientos, puesto que mientras el gobierno afirma que no estaba previsto un contacto con la sociedad civil, la guerrilla de las FARC afirma que si estaba previsto. Lo cierto es que hay dos maneras de ver la situación pero es en la mesa de conversaciones y no en los medios donde debe darse la discusión.

Frente al “ultimátum” que el presidente Santos dio en medio de la controversia y que tiene que ver con el tiempo límite del 23 de Marzo, el Senador señala que **independientemente de la vehemencia con que se hagan los anuncios, lo que se debe garantizar es que los acuerdos sean de calidad** y que faciliten que la sociedad pueda darle carne a los acuerdos una vez firmados.
