Title: Aparece el periodista y ambientalista Alexander Roa en Bogotá
Date: 2019-06-30 08:39
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: alexander Roa, Casanare, líder ambienal, Secuestro
Slug: encuentran-periodista-alexander-roa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Alexanderroa-e1561901246230.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Chivas del LLano 

Tras haber sido reportado como desaparecido el sábado, el **periodista y líder ambientalista del Casanare Alexander Roa Carrillo** fue abandonado al  norte de Bogotá, al parecer con múltiples golpes sobre su humanidad.

El comunicador había desaparecido en la localidad de Chapinero la noche del **viernes 28 de junio mientras observaba un partido de fútbol** con algunos amigos, cuando se levantó al parecer para ir al baño sus amigos no supieron más de él, preocupación que creció cuando verificaron que **no volvió al hotel donde se hospedaba**.

Fuentes allegadas a su familia aseguraron que Roa fue raptado en una **camioneta Tracker de color negro**, en la que fue **encapuchado con una bolsa, golpeado y amenazado.** En medio de los insultos, al periodista y activista le decían que "huele a formol", antes de ser abandonado en terrenos en inmediaciones a la autopista norte.

Actualmente **Alexander Roa dirige la emisora La Frecuencia de Tauramena y es el jefe de redacción del informativo nacional de Comunicación para la Paz, Compaz.** Las fuentes afirman que el día de hoy **interpondrá denuncia ante las autoridades**, reclamando protección y pronta claridad sobre los hechos.

###### Reciba toda la información de Contagio Radio en [[su correo]
