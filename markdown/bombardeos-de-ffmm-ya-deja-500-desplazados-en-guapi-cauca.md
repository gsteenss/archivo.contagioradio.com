Title: Bombardeos de FFMM ya deja 500 desplazados en Guapi, Cauca
Date: 2015-05-26 11:37
Category: DDHH, Nacional
Tags: Asociación de Consejos Comunitarios del Cauca, bombardeos contra las FARC, Cauca, COCO Cauca, consecuencias de la guerra en colombia, Desplazamiento forzado, FARC-EP, Guapi, Orlando Pantoja, proceso de paz
Slug: bombardeos-de-ffmm-ya-deja-500-desplazados-en-guapi-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/desplazamiento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [angiemaya95.blogspot.com]

##### <iframe src="http://www.ivoox.com/player_ek_4552012_2_1.html?data=lZqilJWVdo6ZmKiak5iJd6Kmk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDhw8bfxsrTt4zYxpCzqLKxb9rVjMnSzMaPeZGkjMnS1dXQpdvVxdTgjcrSb6jpwtXWh5enb6TVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Orlando Pantoja,  Asociación de Consejos Comunitarios del Cauca] 

###### [May 26 de 2015] 

La agudeza de los operativos militares en Guapi, Cauca, ha generado que alrededor de **500 personas, entre niños, niñas, ancianos y mujeres embarazadas** se vean obligados a desplazarse, luego de que se desatara nuevamente la guerra tras el operativo de la fuerza pública que acabó con la vida de 26 guerrilleros de las FARC-EP el pasado 22 de mayo.

Según informa Orlando Pantoja, integrante de Asociación de Consejos Comunitarios del Cauca, **después de los bombardeos se presentaron enfrentamientos armados entre las fuerzas militares y las FARC**, lo que obligó a las comunidades de San Agustin, San Vicente y Santa Clara a desplazarse, aunque hubo familias que no pudieron salir de sus territorios, “por la situación tan compleja”, dice Pantoja, quien añade que las condiciones de la cabecera municipal no son aptas para recibir a los 500 desplazados.

Desde COCO Cauca, se cree que si no se detienen los enfrentamientos armados en la zona de los colectivos étnicos, **los desplazamientos van a continuar**, ya que cada día se movilizan más personas con el objetivo de apartase de la zona de conflicto.

El desplazamiento y en general la guerra, han generado una grave afectación psicosocial en las familias de Guapi, además, aunque no hay datos exactos, se cree que **puede haber población civil afectada físicamente** por los enfrentamientos debido al fuerte operativo militar.

Por su parte, **la respuesta del gobierno ha sido nula;** únicamente se ha mantenido la acción estrictamente militar, pero la ayuda humanitaria, que es la más necesaria, no se ha dado. Así mismo, el integrante del COCO Cauca, asegura que la única respuesta ha sido por parte del gobierno local, pero no desde el propio gobierno nacional.

Pantoja señala que **durante el cese unilateral del grupo guerrillero se sintió cierta sensación de calma** en el territorio, por lo que en este momento la comunidad se encuentra preocupada tras la suspensión del cese al fuego; es por ello, que se ve necesario el avance inmediato hacia un cese bilateral.

Los pobladores de  Guapi se han comprometido a que su territorio sea una zona especial de paz, por lo que empezarán a movilizarse  buscando la paz con justicia social que necesitan para retornar a sus hogares.
