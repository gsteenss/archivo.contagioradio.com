Title: Duras críticas generó aprobación del Presupuesto de la Nación para 2021
Date: 2020-10-20 22:12
Author: AdminContagio
Category: Actualidad, Nacional
Tags: PGN, Presupuesto, Presupuesto General de la Nación
Slug: duras-criticas-genero-aprobacion-del-presupuesto-de-la-nacion-para-2021
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Congreso-aprobo-Presupuesto-General-de-la-Nacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**La noche de este lunes fue aprobado por las plenarias del Senado y la Cámara de Representantes el** **Presupuesto General de la Nación para el año 2021 en un monto de 313,9 billones de pesos.** Ambas cámaras sesionaron de manera paralela el proyecto, que se cerró con una votación de 66 congresistas a favor y 11 en contra en el Senado; y 134 parlamentarios que votaron de manera afirmativa en la Cámara de Representantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque el monto del presupuesto subió respecto a la cifra del año anterior, expertos señalaron que el incremento obedece a un aumento importante en los rubros destinados a pagar y **refinanciar deuda pública y no a un incremento en la inversión.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

«*Es una refinanciación de deuda, más que otra cosa. Lo que vemos es un presupuesto muy parecido al que teníamos para este año*», expresó el economista Luis Carlos Reyes, director del [Observatorio Fiscal](https://twitter.com/OFiscalpuj) de la Universidad Javeriana, centro de pensamiento que estudió el proyecto de presupuesto desde su radicación en el Congreso por parte del Gobierno, quien cuestionó la cuantiosa destinación para ese rubro.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/luiscrh/status/1318399409810526208","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/luiscrh/status/1318399409810526208

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Congresistas de las bancadas alternativas también **cuestionaron que no se estimaran mayores recursos para la inversión social y denunciaron la negativa de las mayorías para acceder a programas de protección social como la renta básica y los subsidios para los pequeños y microempresarios.** (Le puede interesar: [Sí hay recursos para Avianca pero no para sectores más vulnerables](https://archivo.contagioradio.com/si-hay-recursos-para-avianca-pero-no-para-sectores-mas-vulnerables/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1318549514601570304","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1318549514601570304

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Asimismo, aunque el **rubro de educación** logró una mayor asignación que la del año pasado, la representante Ángela María Robledo advirtió que **no se contempló la financiación del programa «matricula cero»** que pudiese conjurar los efectos negativos de la pandemia en la deserción académica de los estudiantes; y en cambio **el Gobierno y las mayorías parlamentarias decidieron aumentar el presupuesto para Policía y Defensa en más de 4 billones de pesos pasando de \$35 billones en 2020 a \$39,1 billones el próximo año.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/angelamrobledo/status/1318566235504857089","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/angelamrobledo/status/1318566235504857089

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### El sometimiento del Congreso al proyecto de Presupuesto del Gobierno

<!-- /wp:heading -->

<!-- wp:paragraph -->

Varios sectores señalaron que el proyecto de presupuesto remitido por el Ministerio de Hacienda en representación del Gobierno Nacional, no tuvo mayores modificaciones en el Congreso en los aspectos álgidos, razón por la cual se tomó como un sometimiento del Legislativo hacia el Ejecutivo. (Le puede interesar: [En dos años del Gobierno Duque se ha derrumbado institucionalidad](https://archivo.contagioradio.com/en-dos-anos-del-gobierno-duque-se-ha-derrumbado-institucionalidad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido se pronunció Juan Oviedo, economista e integrante del Observatorio Fiscal, quién cuestionó la ausencia de debate y el hecho que las pocas modificaciones que tuvo el proyecto inicial hayan sido aprobadas con el «*visto bueno*» del Ministerio de Hacienda. (Le pude interesar: [Hay que recuperar el congreso que es la representación del pueblo: Iván Marulanda](https://archivo.contagioradio.com/hay-que-recuperar-el-congreso-que-es-la-representacion-del-pueblo-ivan-marulanda/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JuanOv13do/status/1318374874562351104","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JuanOv13do/status/1318374874562351104

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En el mismo sentido se expresó el senador Gustavo Petro quien vehementemente criticó la decisión aprobada por las mayorías; al tiempo que puso en duda que el crecimiento económico esperado por el Gobierno —en el cual se sustentó la proyección— se pueda dar en el escenario de pospandemia, lo cual dejaría desfinanciado el Presupuesto Nacional y peligrando su ejecución.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/petrogustavo/status/1318522972076937216","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/petrogustavo/status/1318522972076937216

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Desfinanciamiento de la JEP

<!-- /wp:heading -->

<!-- wp:paragraph -->

Otra de las grandes críticas y preocupaciones que se dieron en torno al Presupuesto de la Nación, fue la negativa de las mayorías parlamentarias para acceder a una ampliación de los recursos con destino a la Jurisdicción Especial para la Paz -JEP- y en general para el Sistema Integral de Justicia Transicional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el representante a la Cámara, José Daniel López **con la negativa del Congreso de aprobar un monto de 30.000 millones de pesos, la JEP queda desfinanciada para su operación y funcionamiento el próximo año, lo cual calificó como «*un duro golpe al Acuerdo de Paz*»** **ya que la JEP es la «*columna vertebral*» del mismo.** (Le puede interesar: [El trabajo de la JEP para llegar a la verdad histórica en Colombia](https://archivo.contagioradio.com/el-trabajo-de-la-jep-para-llegar-a-la-verdad-historica-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RoyBarreras/status/1318388915850661893","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RoyBarreras/status/1318388915850661893

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La propia JEP, a través de un comunicado expresó su preocupación frente al desfinanciamiento del Tribunal de Paz y explicó los rubros a los cuales se esperaba destinar los recursos, los cuales incluían la protección de víctimas y testigos (19,6 mil millones); la representación y atención de las víctimas (4,1 mil millones); y la inversión en tecnología para digitalizar y agilizar la administración de Justicia (6,3 mil millones).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JEP_Colombia/status/1318619506818846720","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JEP\_Colombia/status/1318619506818846720

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
