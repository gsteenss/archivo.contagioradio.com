Title: El teatro del absurdo de Beckett se presenta en Bogotá
Date: 2017-04-12 12:55
Category: eventos
Tags: Bogotá, Samuel Beckett, teatro
Slug: teatro-beckett-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Godot3-e1492018885243.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Anhelo del Salmón 

##### 12 Abr 2017 

En el marco del **Primer Festival del Anhelo de Bogotá**, del 13 al 22 de abril se rendirá **homenaje a Samuel Beckett**, dramaturgo, escritor, Premio Nobel de Literatura y uno de los mayores exponentes del teatro del absurdo, con una visión panorámica por algunas de las obras con las que revolucionó el teatro y la narrativa del siglo veinte.

Títulos como "**Esperando a Godot**", en versión de la compañía local El Anhelo del Salmón; "**Acto sin palabras I**", de la compañía de Teatro La Máscara de Cali; "**Primer amor**", dirigida por Manolo Orjuela y "**La última cinta de Krapp**" bajo la dirección de Camilo Carvajal, ambas piezas protagonizadas por el escritor australiano de origen irlandés Joe Broderick, un especialista en Beckett, hacen parte de la programación.

![KRAPP 8](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/KRAPP-8-e1492019707194.jpg){.size-full .wp-image-39128 .aligncenter width="800" height="544"}

Otro de los montajes que hace parte de la agenda del Festival es "**Grises**", obra inspirada en Acto sin palabras II, a cargo de la compañía La Tribu Teatro con la dirección de Juan Carlos Agudelo. En el evento se presentará además una selección de las obras cortas como son **Vaivén** (Come and Go), **Fragmento de Teatro I**, y **Nana** (Rockabye) interpretadas en un espacio de lectura por las actrices Aida Morales y Vanessa Tamayo.

El Festival del Anhelo, es un evento cultural organizado por El Anhelo del Salmón, una compañía de teatro independiente creada en 2006. **Las funciones se presentarán en La Factoría L’explose**, escenario donde se desarrollarán adicionalmente algunos eventos académicos y de reflexión con la participación de figuras como **Joe Broderick, Everett Dixon y Juan Carlos Agudelo**. Le puede interesar: [El teatro se solidariza con Mocoa](https://archivo.contagioradio.com/teatro-por-mocoa/).

**Programación**

**“PRIMER AMOR”**  
Jueves 13 y viernes 14 de abril 8:00 pm.  
Director: Manolo Orjuela Elenco: Joe Broderick

**MUJERES LEYENDO A BECKETT**  
Vaivén (Come and Go), Fragmento de Teatro I, y Nana (Rockabye)  
Grupo: Somos Siete  
Sábado 15 de abril a las 6:00 p.m.

**ACTOS SIN PALABRAS I**  
Sábado 15 a las 8:00 p.m. y domingo 16 de Abril a las 6:00 p.m.  
Grupo: Sensibus -La Máscara. Cali.

**LA ÚLTIMA CINTA DE KRAPP**  
Lunes 17 y martes 18 de abril a las 8:00 p.m.  
Grupo: El Anhelo del Salmón.

**“GRISES”**  
Miércoles 19 y jueves 20 de abril a las 8:00 p.m.  
Grupo: La Tribu Teatro – Juan Carlos Agudelo

**Conversatorio Grises**  
Juan Carlos Agudelo, Julián Santamaría y Rodrigo Hernández  
Jueves 20 de abril de 2:00 p.m. a 4:00 p.m.

**ESPERANDO A GODOT**  
Viernes 21 y sábado 22 de abril a las 8:00 pm  
Grupo: El Anhelo del Salmon

**Funciones:** lunes a sábado: 8:00 p.m. y domingo 6:00 p.m.  
**Bono de apoyo:** \$30.000 (General) – \$25.000 (Estudiantes y tercera edad)  
**Lugar:** Factoría L’EXPLOSE Carrera 25 \# 50-34  
**Informes y reservas:** 2496492
