Title: Las mujeres hablan de su papel en el cine colombiano
Date: 2016-11-04 08:48
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, colombiano, mujeres, Simposio
Slug: mujeres-cine-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/14344164_1129224463834746_282046657066638583_n.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Simposio Las Mujeres en el cine colombiano 

##### 3 Nov 2016 

Este viernes 4 de Noviembre se llevará a cabo el primer '**Simposio sobre los lugares de las mujeres en el cine Colombiano**'; un espacio que busca propiciar un dialogo pendiente en el país, sobre el papel de las mujeres la producción audiovisual y particularmente en la cinematografía nacional.

El evento pretende **hacer un análisis de los roles, la trayectoria y las contribuciones que han hecho hasta el momento las mujeres en el cine colombiano**, además de sentar un precedente para que este tipo de iniciativas se realicen con periodicidad y se puedan sumar más personas interesadas en la temática a lo largo del país, como afirmó la **escritora, crítica y docente Andrea Echeverri** una de las organizadoras.

El simposio integrará a diversidad de mujeres que trabajan en diferentes espacios de la industria cinematográfica, como por ejemplo: **Cristina Gallego** y **Diana Bustamante**, ambas productoras; **Conchita Fernández**, sonidista; **Camila Lobo Guerrero** y **Libia Stella Gómez** directoras y guionistas; **Viviana Rojas** y **María Posada**, animadoras; entre otras. Asimismo se realizará un homenaje a la documentalista **Marta Rodríguez** y habrán tres mesas temáticas: Gestión, administración y cine, Tecnologías y oficios, Dirección y guión

El simposio tendrá lugar en el **Hemiciclo de la Universidad Jorge Tadeo Lozano**, e iniciará a las 8:00 AM e irá hasta las 6:30 PM, posteriormente desde las 7:00 PM la clausura se llevará a cabo en la Cinemateca Distrital, donde se proyectarán algunos cortos dirigidos por mujeres. La entrada es libre con previa [inscripción](https://goo.gl/forms/EfFStVC7LYcA3Zqh2).

Vale la pena mencionar que aún falta un gran porcentaje del total de recaudación esperado para el \#Crowdfunding, por medio del cual las organizadoras buscaron el presupuesto para el evento, de tal forma aquellas personas interesadas en colaborar con esta iniciativa y recibir muchas recompensas, pueden hacerlo [aqui](http://idea.me/lasmujeresenelcinecolombiano).

<iframe id="audio_13613536" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13613536_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
