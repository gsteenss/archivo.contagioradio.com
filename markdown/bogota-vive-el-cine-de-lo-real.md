Title: Bogotá vive el cine de lo real "MIDBO 2015"
Date: 2015-10-26 18:15
Author: AdminContagio
Category: 24 Cuadros
Tags: Alados Colombia, Centro ático Universidad Javeriana, Documentales en Colombia, MIDBO Muestra Internacional Documental, Seminario "Pensar en lo real"
Slug: bogota-vive-el-cine-de-lo-real
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/12068535_610881509051530_9060262006711906732_o-e1445901230472.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[<iframe src="http://www.ivoox.com/player_ek_9180036_2_1.html?data=mpalkpWXeo6ZmKiakpmJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDb0NmSpZiJhZKf187jx5DJsIzXytPSjcnJb83jjNfSw9GPaZOmrq6xpLSPdpGlloqflJKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>]

##### [Laura Coronado, Productora General] 

##### [26 Oct 2015]

Con el Seminario Internacional "Pensar en lo real" inicia este martes la edición 17 de MIDBO, Muestra Internacional Documental de Bogotá, una cita con el cine de lo real que tendrá lugar en diferentes espacios académicos y de exhibición audiovisual de la capital colombiana. ([Ver MIDBO 2015, 17 años de documental en Bogotá](https://archivo.contagioradio.com/midbo-2015-17-anos-de-documental-en-bogota/))

El evento académico, punto de partida de la muestra, busca provocar una reflexión sobre el documental en el mundo contemporáneo, dando relevancia en la presente edición a 15 ponencias nacionales que giran en torno a los ejes ejes temáticos: política y prácticas documentales, teorías y estética en la representación documental, etnografías documentales, y experiencias en comunidades vulnerables.

Durante 3 días, los asistentes al Centro Ático de la Pontificia Universidad Javeriana, pordrán escuchar y disentir con las intervenciones de  invitados nacionales e internacionales sobre la creación sonora en el documental, documental experimental, la etnografía multisensorial y las artes de multimediales.

Entre los conferencistas internacionales se encuentran **Andrew Irving** de la Universidad de Manchester, Inglaterra, **Anna Kipervaser**, de la Universidad de Duke, Ucrania-Estados Unidos, **Ben Rusell**, Artista itinerante y curador, Estados Unidos, **Johannes Sjoberg**, Universidad de Manchester, Suecia-Inglaterra, **Pedro Lasch**, Universidad de Duke, México-Estados Unidos y Xavier Andrade, FLACSO, Ecuador.

El evento que inicia a las 5:30 p.m. tendrá como complemento el lanzamiento del libro: "Fronteras expandidas: el documental en Latinoamérica", recopilación de los textos producto del seminario realizado el año inmediatamente anterior.

La publicación presenta un panorama analítico de la producción documental colombiana, ecuatoriana y española, así como algunos debates teóricos contemporáneos sobre el cine documental, que asiste hoy a una estimulante transformación de formas narrativas, dispositivos tecnológicos y paradigmas conceptuales.

https://www.youtube.com/watch?v=Rl\_5WplojB0

**Sobre la muestra**

Desde este 27 de octubre y hasta el 4 de noviembre el MIDBO, uno de los festivales con mayor trayectoria en Bogotá, hará presencia en Bogotá con muestras documentales de 24 países que incluyen más de 100 películas, 95 proyecciones, 40 horas de programación académica, 16 invitados internacionales y más de 20 encuentros con directores invitados.

El crítico y experto en cine documental Bill Nichols, dice que los temas en los documentales cambian constantemente. La selección de títulos de la MIDBO corrobora esta afirmación, puesto que las producciones abordan temas culturales y sociales diversos; que desembocan en un mar de relatos que necesitan verse y pensarse para otorgarles sentido.

El festival contará con una competencia nacional de 8 producciones; una muestra de documentales nacionales (Panorama Nacional); trabajos de performance, instalaciones, documentales interactivos (Documental expandido); una vasta muestra internacional; una retrospectiva a la trasgresora obra del documentalista israelí Avi Mograbi; Chile y Puerto Rico como países invitados; además de una sección de ‘Miradas emergentes’, desarrollada para documentales nacionales que llaman la atención por su amplitud temática.

También se realizarán un taller de documental experimental, un seminario internacional sobre el documental, una clase magistral con el director francés Jean Gabriel Périot, un foro con los países invitados y un conversatorio de documental interactivo.

Toda la programación y los eventos del MIDBO se pueden consultar [aquí](http://www.midbo.co/)
