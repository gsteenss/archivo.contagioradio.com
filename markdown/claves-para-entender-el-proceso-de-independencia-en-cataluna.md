Title: Claves para entender el proceso de independencia en Cataluña
Date: 2015-11-14 08:00
Category: El mundo
Tags: entender el proceso de independencia, independencia en cataluña, nacionalismo político catalán, País Valenciano, pueblo catalan
Slug: claves-para-entender-el-proceso-de-independencia-en-cataluna
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/1347392291804.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RTVE] 

##### **[José Angel Sánchez Rocamora]** 

Luego de ser portada de los principales medios de comunicación en el  mundo  hemos decidido indagar sobre las claves para entender el proceso de independencia de Cataluña desde una perspectiva histórica, social y política.

[Cataluña actualmente es una comunidad autónoma del Estado Español. Históricamente perteneció a la Corona de Aragón junto con el País Valenciano, Aragón, Islas Baleares y distintas colonias por el Mediterráneo. Fue en 1716 con el reinado de Felipe V (monarquía de los Borbones), cuando comienza la represión y ocupación por parte de tropas borbónicas contra su identidad, sus fueros y todas sus formas de gobierno y autonomía, incluyendo la prohibición del uso de su idioma, el catalán.]

[Continuando con el proceso represivo conocido como castellanización o españolización de la sociedad catalana, en el siglo XX se forma el nacionalismo político catalán iniciando una confrontación con el nacionalismo español que ha durado hasta hoy día. En las décadas anteriores a la Guerra Civil, se conforma una sociedad obrera donde el anarquismo toma fuerza a través del sindicato CNT, siendo la fuerza política con mayor apoyo social.]

[Es cuando su presidente (Companys) declara en 1934 el Estado catalán dentro de una república federal española, siendo como respuesta por el Estado Español la suspensión de su autonomía. Con el golpe de estado fascista encabezado por Franco y apoyado por el fascismo internacional, Cataluña, igual que las demás autonomías, queda sumida en la represión cultural social e identitaria, el genocidio y una dictadura que duraría desde 1939 hasta 1975. Siendo así totalmente supeditada a la gobernación española de carácter fascista.]

\[caption id="attachment\_17192" align="alignright" width="325"\][![independencia cataluña](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/75908-944-550.jpg){.wp-image-17192 width="325" height="189"}](https://archivo.contagioradio.com/?attachment_id=17192) Foto Martha Perez - EFE\[/caption\]

[**Con la llegada de la democracia Cataluña recupera su autonomía y su estatuto**, consolidando el movimiento nacionalista e independentista. Con el tiempo y los distintos gobiernos nacionalistas el sentimiento independentista crecerá debido a las propias reivindicaciones de autodeterminación y autogobierno y a la represión continua por parte del nacionalismo español.]

[La última crisis económica, enmarcada en las políticas neoliberales auspiciadas por la UE, ha situado a Cataluña en una de las regiones más afectadas del estado a nivel de pobreza extrema, desempleo, y pérdida de libertades civiles, todo esto sumado a otros factores ha propiciado que el sentimiento independentista haya crecido lo suficiente como para poder iniciar el proceso democrático hacia la soberanía. No hay que olvidar que es la tercera autonomía que más aporta a las arcas del estado y de las que menos recibe en compensación.]

[Pero para poder entender la situación política actual es necesario revisar quiénes son los actores principales y cuáles son sus reivindicaciones y modos de hacer política.]

[Comenzando por **Convergencia y Unión (CIU)**, es el partido por excelencia de la derecha nacionalista catalana de corte burguesa y oligarca, tradicionalmente ha sido un partido neoliberal, europeísta y nacionalista, solo en los últimos tiempos ha adquirido un carácter independentista. Es responsable de los recortes sociales y de la crisis económica actual, además junto con otros miembros, su histórico líder **Jordi Pujol** está actualmente involucrado en un escándalo de corrupción que podría afectar a muchos políticos tanto catalanes como españoles de la década de los 80-90. Su actual líder, Artur Mas,  es quien ha encabezado el proceso independentista. Distintas fuerzas de izquierda han criticado el giro hacia el independentismo como cortina de humo para tapar los recortes sociales y el desempleo consecuencia de sus políticas.]

[El segundo partido independentista es **ERC, Izquierda Republicana de Cataluña**, histórico partido de izquierdas republicano de corte socialdemócrata, que junto con CIU formaron la coalición soberanista de cara a las últimas elecciones conocida como ¨Junts pel Sí¨ (Juntos por el sí), obteniendo 62 escaños como la fuerza más votada. Este último partido también ha participado de los recortes sociales, pero su política siempre ha conservado un carácter izquierdista.]

[Respecto a los partidos de corte españolista, contrarios a cualquier independencia y a la convocatoria de un referéndum, están el **PSC (Partido Socialista Catalán**), neoliberal y socialdemócrata fue quien incluyó el término ¨nación¨ en el actual estatuto de autonomía, propugnó en las últimas elecciones el federalismo para Cataluña dentro de España. **El PP (Partido Popular Catalán**), neoliberal conservador y españolista, en las últimas elecciones ha tenido el peor resultado de su historia. Por último y con su mejor resultado electoral el partido español de origen catalán conocido como Ciudadanos, de corte neoliberal, con fuertes relaciones con la ultraderecha catalana y española, y que con un fuerte discurso populista ha desbancado al PP y, ha hecho frente al partido de izquierdas conocido como Podemos.]

[¨Catsiqueespot¨ (Cataluña sí que se puede), es la marca de Podemos en Cataluña, fruto de una coalición de partidos de izquierdas catalanes como ICV (Iniciativa por Cataluña los Verdes), sus últimos resultados electorales no han sido los esperados, siendo estos una perdida con respecto a los de las municipales. Cuenta con la gobernación de la alcaldía de Barcelona, a cargo de Ada Colau, líder de la plataforma antidesahucios, de gran reconocimiento por toda la izquierda estatal y la sociedad en general por su lucha dentro de los movimientos sociales. Son partidarios de un referéndum pero también de la pertenencia de Cataluña en el Estado Español.]

[Por último el **partido revelación** que ha pasado de 3 a 10 escaños, es **CUP (Candidatura de Unidad Popular**), con 30 años, la formación independentista (su proyecto es Países Catalanes incluyendo el País Valenciano y las Islas Baleares), socialista y rupturista, se declara anticapitalista y aboga en su campaña por la desobediencia, ¨Desobeïm, governem-nos¨ (desobedeciendo, gobernémonos). Proveniente de los movimientos sociales, de carácter libertario y socialista se configura como un partido municipalista catalán y se nutre de las nuevas corrientes libertarias como el]*[municipalismo libertario]*[o] [del movimiento autónomo catalán donde convergen movimientos como el de Okupaciones]*[.]*[Su funcionamiento es íntegramente asambleario y horizontal y pregona el antiestatismo y la ruptura directa con el Estado Español, la UE y el capitalismo. Sus representantes responden a vocerías, pero no a liderazgo, este es asumido por las bases.]

\[caption id="attachment\_17194" align="alignleft" width="300"\][![CATALUÑA](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/CATALUÑA-300x225.jpg){.wp-image-17194 .size-medium width="300" height="225"}](https://archivo.contagioradio.com/?attachment_id=17194) Foto: wikipedia\[/caption\]

[Actualmente es la clave para la formación del gobierno de coalición independentista entre ¨Junts Pel Sí¨y su formación. En la última semana ha bloqueado dos veces la investidura del líder del proceso soberanista, Artur Mas, acusándolo de ser quién ha encabezado los recortes sociales, la represión contra las protestas sociales y por su vinculación con banqueros y empresarios, esta situación ha provocado un estancamiento en la formación del propio gobierno catalán. El gobierno entrante se encuentra en sus manos y su propuesta es para Raúl Romeva perteneciente a ERC, con amplio reconocimiento en la UE.]

[Todo parece comenzar con las elecciones autonómicas de 2010  que dan una holgada victoria al nacionalismo catalán, este negocia un pacto fiscal con España pero no lo consigue, aumentando así el descontento de la sociedad y acrecentando el independentismo. El 11 de Septiembre de 2012 en el Día de Cataluña es convocada una gran manifestación soberanista que presiona al gobierno de Artur Mas a convocar elecciones. Estas vuelven a ser ganadas por el soberanismo. En 2014 se convoca una consulta sobre la posible independencia de Cataluña, siendo ésta prohibida por el Tribunal Constitucional de Cataluña. **El resultado fue un 80% a favor de la independencia**, pero dentro de una escasa participación debido principalmente a la ilegalidad de la consulta.]

[Volviendo a la situación actual, las últimas elecciones fueron presentadas por el soberanismo como plebiscitarias para iniciar el proceso de independencia o desconexión de España. Este ha obtenido la mayoría suficiente para gobernar, es decir de 135 escaños, tienen 72. En el caso de incluir a la coalición catalana de Podemos para un eventual referéndum sumarían 83 escaños.]

[En la última semana el soberanismo, aún sin formar gobierno, ha iniciado el proceso, con la advertencia del Tribunal Constitucional Español de suspender a 21 cargos de sus funciones del parlamento Catalán si deciden continuar.]

[![Independentisme2001](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Independentisme20011.jpg){.wp-image-17199 .alignright width="321" height="351"}](https://archivo.contagioradio.com/?attachment_id=17199)

[Con el actual panorama se abren distintas interpretaciones y mecanismos políticos para la consecución de un derecho reconocido por Naciones Unidas como es el de autodeterminación de los pueblos.]

[Por un lado si existe una mayoría de catalanes que quiere decidir democráticamente pero no una mayoría absoluta que quiere iniciar una desconexión del estado español, aún así la legalidad española no permite la convocatoria de referéndums, la constitución no lo avala y el Tribunal Constitucional Español, de carácter conservador ya ha suspendido la declaración soberanista y condenado a su anterior presidente por la consulta celebrada en 2014.]

[Sólo en el caso de un cambio en la constitución y de un eventual gobierno de izquierdas encabezado por Podemos en España podría darse esta transformación y la posibilidad de convocar un referéndum legal. En todo caso las estadísticas sitúan a la izquierda lejos de gobernar y más aún de tener la mayoría para que en un cambio constitucional se incluyese el término.]

[La UE en manos de la derecha ya se ha declarado contraria a cualquier tipo de consulta sin el aval del Estado Español y de un futuro escenario de independencia descartando así su interlocución en el proceso. En todo caso la UE ha negado consultas en distintas regiones y en su territorio existen disputas y territorio con reivindicaciones soberanistas como Crimea, Moldavia, Escocia, Kosovo o en el seno de la propia Bélgica. Siempre en el recuerdo la trágica experiencia de los Balcanes.]

[Debido al derecho al veto, Cataluña no podría entrar en la ONU, pero si colarse por la puerta de atrás como ha hecho Kosovo a través de la UNESCO, donde no existe este derecho.]

[El actual partido que gobierna en España, el Partido Popular, derecha neoliberal españolista, ha negado en todo momento la posibilidad de una consulta y de la independencia bloqueando cualquier acción catalana a través del Tribunal Constitucional. En todo caso de los 4 partidos grandes en intención de voto, Partido socialista, PP, Ciudadanos y Podemos solo este último es proclive a una consulta. Dicho panorama también impide cualquier negociación política.]

[En el escenario actual es evidente que para llevar a cabo la consulta y el proceso independentista es necesario romper con la legalidad española o moverse fuera de su accionar. El proceso mantiene una ¨hoja de ruta¨ respetando la propia intimidad de las acciones, por lo que seguro es previsible el siguiente paso sea una consulta ciudadana.]

[Para finalizar es importante remarcar que cualquier persona que hace 2 años se le preguntará si se imaginaría dicho escenario respondería tanto en Cataluña como en España que no lo imaginaría. Lo que sí es seguro es que el pueblo catalán está dispuesto a llevar cabo el proceso soberanista al menos hasta una consulta que lo pueda o no refrendar. Siendo innegable el respeto por uno de los derechos humanos fundamentales como es de la autodeterminación de los pueblos.]

##### **José Angel Sánchez Rocamora, comunicador social del colectivo Radio Malva del País Valenciano - [@fefudelmar](https://twitter.com/fefudelmar)** 
