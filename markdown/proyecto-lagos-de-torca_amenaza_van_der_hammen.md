Title: Ya está listo el primer proyecto de urbanización que amenaza a la Van der Hammen
Date: 2018-01-26 18:18
Category: Ambiente, Nacional
Tags: Enrique Peñalosa, Lagos de Torca, Reserva Thomas Van der Hammen
Slug: proyecto-lagos-de-torca_amenaza_van_der_hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Reserva-Tomas-Van-der-Hammen-e1457630338748.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: bogota.gov.co] 

###### [26 Ene 2018]

El riesgo para la Reserva Thomas Van der Hammen continúa. El proyecto de viviendas Lagos de Torca, nuevamente prende las alarmas de quienes defienden el que sería el segundo pulmón de la ciudad. Se trata de una urbanización que daría pie también a la construcción de **vías y otras infraestructuras que pondrían en riesgo la conectividad de la sabana de Bogotá.**

Se trata del primer proyecto que la administración de Enrique Peñalosa, logra poner en marcha tras dos años de mandato. En **Lagos de Torca, se ejecutarán \$4 billones en infraestructura de la cual únicamente se podrán encargar empresas privadas, tales como Amarilo de Roberto Moreno,** para llevar a cabo lo que para la alcaldía será una “revolución urbanística”.

### **El impacto en la reserva** 

Con bombos y platillos Juan Camilo González, gerente de Ciudad Norte, explica que, **"**Será una ciudad con ciclorrutas **en todos los perfiles viales, en la que obligamos a que, donde haya vivienda, haya vías de barrio. Obligamos a los 28 colegios de la zona a quedarse, para que los estudiantes ya no tengan que coger una hora de bus y ojalá se puedan ir en bicicleta o hagan recorridos más cortos”,** tal y como lo ha buscado hacer Enrique Peñalosa.

De acuerdo con Sabina Rodríguez, vocera del colectivo en defensa de la Reserva Thomas Van der Hammen, esa zona se vería directamente afectada ya que se obstaculizaría la conexión con el río Bogotá, que permite el flujo del agua, y por tanto se pone en riesgo la permanencia de los humedales del norte y occidente de la ciudad. Impidiendo, por ejemplo el paso de aves migratorias.

> 1\. Con la misma imagen publicada hoy por [@EnriquePenalosa](https://twitter.com/EnriquePenalosa?ref_src=twsrc%5Etfw) demuestro porqué Lagos de Torca es el proyecto que aunque "no toca" la Reserva Van der Hammen, destruye su propuesta de conectividad.<https://t.co/wSXkSTITx0> [pic.twitter.com/klR2T29mH9](https://t.co/klR2T29mH9)
>
> — Daniel Bernal (@danielbernalb) [25 de enero de 2018](https://twitter.com/danielbernalb/status/956671347761041409?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Según la ambientalista, la alcaldía estaría desconociendo el plan de manejo ambiental y las obligaciones que tiene el distrito con la reserva. Rodríguez, explica que si bien, existe una parte que puede ser urbanizada para expandir la ciudad, **"el distrito desconoce que el ministerio le ordenó mantener la conectividad y por tanto debe dejar las 1.395  como zonas verdes,** por tanto no puede dejar caer una gota de cemento en esa área.

Sin embargo, como bien lo ha dicho Juan Camilo González, el proyecto comprende una serie de vías y obras, que alertan sobre una posible afectación a la reserva. Además, se cree que este sería el primer proyecto cercano a esa zona que significaría el impulso para nuevas urbanizaciones en ese lugar.

Ante esta situación los defensores de la reserva continuarán en su movilización permanente, con acciones pacíficas como la siembra de árboles. Además, indica Sabina Rodríguez, deberán estar pendientes de lo que sea la siguiente acción de la alcaldía luego de que se conozca **la nueva resolución del Ministerio de Ambiente sobre la relinderación de reservas** "la alcaldía puede estar esperando para presentar su proyecto de expandir el norte de Bogotá".

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
