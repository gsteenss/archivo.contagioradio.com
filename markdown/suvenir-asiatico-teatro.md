Title: “Souvenir Asiático" un relato de migraciones en el Teatro
Date: 2018-07-27 18:13
Category: eventos
Tags: migraciones, teatro
Slug: suvenir-asiatico-teatro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/image004-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Teatro Petra 

###### 27 Jul 2018 

El Teatro Petra abre sus puertas a la obra “Souvenir Asiático” de la compañía teatral Los Animistas, una pieza teatral poética y multidisciplinar que cuenta varias historias de migrantes, todas acompañadas por títeres, videos y actores en escena.

Con la actuación de ocho personajes, el director Javier Gámez, la dramaturga Martha Márquez, la actuación de Gina Jaimes y Juan Manuel Barona junto con los titiriteros Víctor Pérez y Henry López muestran las historias de dos chinos que intentan cruzar la frontera entre México y Estados Unidos, dos africanos sometidos por soldados marroquíes, una joven norcoreana que trata de comunicarse con un policía de Buenaventura, dos niños sirios rumbo a Europa, una mujer que dialoga con su madre y dos colombianos que se embarcan en un viaje con ilusión de llegar a Estados Unidos, pero terminan en Alemania.

Esta pieza liminal, fronteriza y con cargas de tragedia cuenta con un efecto sonoro 4.1 que acompaña el viaje de tránsito, de seis situaciones fragmentadas de seres que se debaten al margen de un país fue ganadora en 2017 de la Beca de montaje de Dramaturgias Colombianas del Ministerio de Cultura de Colombia y que ahora llega estará en el Teatro Petra hasta el 11 de agosto.

Souvenir Asiático se presentara de miércoles a viernes con doble función a las 6:oop.m y 8:30p.m. El costo de la boletería general es de \$38.000, para estudiantes y personas de 3ra edad será \$30.500.

Si desea saber más información de la obra puede consultar la siguiente página www.teatropetra.com.
