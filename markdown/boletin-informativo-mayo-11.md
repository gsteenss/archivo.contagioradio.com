Title: Boletín informativo Mayo 11
Date: 2015-05-11 17:25
Author: CtgAdm
Category: datos
Tags: acuerdo nuclear, amenazas contra los y las estudiantes universitarios, Arabia Saudí, Arbey Gañan, aves migratorias en peligro de extinción, Barack Obama, Comisión de Derechos Humanos, Consejo de Cooperación del Golfo, de la Universidad Nacional de Colombia, Departamento de Sociología, Edwin Bañol Álvarez, EEUU robó a Irak y Afganistán, El Carmelo, Francois Hollande, fumigadas con glifosato, Galilea, Gustavo Bañol Rodríguez, invasiones de EEUU, Irán y EEUU, isla de Cuba, La Ceiba, la esperanza, Ministerio de Ambiente y Desarrollo Sostenible, Ministro de Salud, municipio de Anserma, Paramilitarismo, Puerto Guzmán, Putumayo, Red de Organizaciones Sociales de Putumayo, reestablecer relaciones económicas, Resguardo de San Lorenzo Caldas, Rey Salman, simon ladino, Unión Internacional para la Conservación de la Naturaleza, Yuri Quintero
Slug: boletin-informativo-mayo-11
Status: published

[*Noticias del día:*]

<iframe src="http://www.ivoox.com/player_ek_4479761_2_1.html?data=lZmkm5yadY6ZmKiakpqJd6Kmk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfkpaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Personal de EEUU robó \$52 millones de dólares en Irak y Afganistán. Los fraudes se han cometido gracias al caos financiero y burocrático vivido durante las invasiones y el tiempo en que EEUU tuvo máxima presencia militar en esos países.

-La semana pasada las comunidades Galilea, La Esperanza y La Ceiba del municipio de Puerto Guzmán, Putumayo, fueron fumigadas con glifosato, pese a la recomendación del Ministro de Salud, es por eso que los pobladores proponen al gobierno nacional otras estrategias para combatir los cultivos ilícitos. Habla Yuri Quintero, responsable de la Comisión de Derechos Humanos de la Red de Organizaciones Sociales de Putumayo.

-Francois Hollande presidente de francia llego en la mañana de hoy a la isla de Cuba con una delegación de empresarios con el objetivo de romper el cerco y lograr reestablecer relaciones económicas con el país, convirtiendose en el primer mandatario europeo en romper el bloqueo político contra la isla.

-El paramilitarismo volvió a lanzar amenazas contra los y las estudiantes universitarios en en el pais, en esta oportunidad, contra alumnos del Departamento de Sociología, de la Universidad Nacional de Colombia. Los estudiantes aseguran que no caerán en la trampa de quienes amenazan a la universidad pública, y aquellos que buscan que dejen de movilizarse. Habla Simón Ladino, estudiante de Sociología.

-Arabia Saudí recela el acuerdo nuclear entre Irán y EEUU. En la Cumbre del miércoles del Consejo de Cooperación del Golfo no acudirá el Rey Salman de Arabia saudí, esta ausencia se interpreta como un desplante por parte de esa nacion a Barack Obama por el acuerdo nuclear firmado.

-Indígenas del Resguardo de San Lorenzo en Caldas denunciaron el asesinato del médico tradicional Gustavo Bañol Rodríguez y su hijo Edwin Bañol Álvarez luego de la realización de un ritual de sanación de la tierra en en la vereda El Carmelo, municipio de Anserma, Caldas. Arbey Gañan, ex gobernador del resguardo

-De acuerdo al Ministerio de Ambiente y Desarrollo Sostenible, en Colombia existen 275 especies de aves migratorias, de las cuales 10 están en peligro de extinción clasificadas por la Unión Internacional para la Conservación de la Naturaleza, UICN.
