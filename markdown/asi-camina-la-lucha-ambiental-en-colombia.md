Title: Así camina la lucha ambiental en Colombia
Date: 2017-06-09 10:50
Category: CENSAT, Opinion
Tags: Ambientalismo, consultas populares
Slug: asi-camina-la-lucha-ambiental-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Foto-editorial-Censat.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Censat 

###### 8 Jun 2017 

#### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

El pasado domingo 4 de junio, Cumaral votó masivamente en una consulta popular para decidir si se permitía o no, la actividad petrolera en su territorio. 97,04% de los votantes, es decir: 7,475 personas rechazaron “la exploración sísmica, la perforación exploratoria y la producción de hidrocarburos petrolera”, el resultado exacerbó los argumentos en contra del gobierno y los empresarios del sector, que se niegan a reconocer los impactos y conflictos que ha dejado la implementación de un modelo de desarrollo agresivo y antidemocrático. La tensión entre comunidades - gobierno - autoridades locales - empresarios expresa el choque de visiones distintas  respecto a los territorios, y la posición sorda del gobierno para debatir el modelo de desarrollo que se han impuesto violentamente.

[Las comunidades y organizaciones territoriales que llevan años oponiéndose al modelo extractivo impuesto, y en sus distintas formas de lucha han incorporado en sus estrategias de defensa territorial la utilización de mecanismos de participación, la movilización social, la comunicación popular, los procesos pedagógicos, los debates locales, regionales y nacionales y las acciones jurídicas para garantizar la permanencia digna en los territorios, consiguiendo importantes victorias como haber ganado con amplio margen las cinco consultas populares de Piedras, Tauramena, Cabrera, Cajamarca y Cumaral, además de haber avanzado también como el reconocimiento de la iniciativa popular normativa (Cerrito) y varios acuerdos municipales (en Huila y Antioquia), además de la suspensión de proyectos como el pozo Lorito 1, la exploración petrolera en La Macarena o San Andrés y Providencia, entre otros.]

[Esta intensa acción política ambiental ha potenciado la participación ciudadana y un importante debate político, logrando colocar en la agenda nacional y el escenario público, la cuestión ambiental en el centro de las preocupaciones actuales de las colombianas y los colombianos. Tanto así que en el marco de la sentencia T445 de 2016 la Corte Constitucional ordenó la creación de una Mesa de Trabajo interinstitucional para que expertos, entidades públicas y privadas, y grupos de interés investiguen desde las ciencias exactas y humanas los impactos de la minería en los ecosistemas del país.]

[Otro ejemplo es la sentencia T-622 de 2016 de la Corte Constitucional, anunciada el pasado 2 de mayo, la cual reconoce al río Atrato como sujeto de derechos y obliga al Estado colombiano a dar una respuesta idónea para la protección, conservación, mantenimiento y restauración del río de la mano con las comunidades étnicas. La sentencia producto del triunfo de la acción de tutela interpuesta por las comunidades de los Consejos Comunitarios de la cuenca del Atrato, representadas legalmente por las compañeras del Centro de Estudios para la Justicia Social “Tierra Digna” marca un hito no sólo en el escenario colombiano, sino incluso en el ámbito internacional, porque alimenta el debate para el reconocimiento de la naturaleza como sujeto de derechos, que solo la Constitución Política de Ecuador y la Ley de la Madre Tierra de Bolivia han incorporado.]

[Aunque el camino es “largo y culebrero” y “el papel aguanta todo”, la realidad muchas veces dista de lo establecido en la norma, depende de los pueblos movilizar sus estrategias colectivas para hacer posible que otros sectores entendamos la vida como un todo. Es indispensable replantear la relación del ser humano con la naturaleza, reconocer que somos naturaleza y nos hacemos con ella, que las afectaciones a la naturaleza son daños a la vida, el territorio y el ciclo socio-ambiental. El caso de la sentencia sobre el Río Atrato, plantea que no veamos el río de forma fragmentada, pero también que empecemos a escuchar otras voces y nos dispongamos a entender y construir el mundo desde otra perspectiva.]

[La lucha política ambiental ha logrado movilizar localmente a cientos de comunidades que hoy se expresan para frenar proyectos mineros, de hidrocarburos e hidroeléctricos pero también está dando pasos importantes de una articulación real que se materializa en los territorios, con la solidaridad y en la movilización. Es el caso de la reciente movilización del pasado 2 de junio, la Gran Marcha Carnaval en defensa del Agua, la Vida y el Territorio, que en su novena versión celebró el triunfo de la consulta popular de Cajamarca y, tuvo como epicentro a la ciudad de Ibagué, con expresiones en otros 25 municipios de Tolima, Caquetá, Quindío, Meta, Santander, y otras ciudades capitales: Armenia, Manizales, Bucaramanga y Bogotá; además de expresiones internacionales en Italia y Gran Bretaña.]

[Pese a estos esfuerzos, el movimiento ambiental tiene un reto mayor, y es pensar en cómo fortalecer la articulación, cómo pensar y construir-proponer otra forma de vivir, que sea una alternativa real a la forma en que el capitalismo ha ordenado, homogeneizado y deteriorado el planeta. Si seguimos organizándonos, el ambientalismo continuará siendo una fuerza social de cambio para la construcción de paz, que proponga nuevos referentes simbólicos, económicos, tecnológicos y políticos que ayuden a transformar la sociedad colombiana.]

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
