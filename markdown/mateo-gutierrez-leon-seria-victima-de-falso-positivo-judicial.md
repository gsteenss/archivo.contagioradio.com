Title: Mateo Gutierrez León sería víctima de otro falso positivo judicial
Date: 2017-02-25 17:26
Category: Judicial, Nacional
Tags: Falsos Positivos Judiciales, Mateo Gutierrez
Slug: mateo-gutierrez-leon-seria-victima-de-falso-positivo-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/mateo-gutierrez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: archivo familiar] 

###### [26 Feb 2017]

Según lo denunció la familia, el caso de Mateo Gutiérrez sería otro de los casos conocidos como “falso positivo judicial” en el que la fiscalía intenta mostrar resultados frente a los hechos de violencia que han sacudido la ciudad en las últimas semanas. Ademá**s lo que se afirma en algunos medios de información es falso y Mateo no tiene nada que ver con las explosiones en la capital**, asegura Omar Gutiérrez, padre del joven.

De acuerdo con Omar, aunque en los medios de información como RCN se intenta relacionar la captura de Mateo con las recientes explosiones en Bogotá, que dejaron un policía muerto y otros más de 15 heridos, **la verdad es que la fiscalía solamente imputó a Mateo una serie de delitos relacionados con la explosión de una “bomba panfletaria” el 18 de Septiembre de 2015**, fecha para la que Mateo estaba ingresando a la universidad. ([Lea también Capturas a lideres de movimientos sociales serían falsos positivos judiciales](https://archivo.contagioradio.com/capturas-a-lideres-del-movimiento-social-serian-falsos-positivos-judiciales/))

Además, para el día de la explosión en “La Macarena” Mateo estaba en un recorrido de vacaciones con su mamá. Así las cosas, según Omar Gutiérrez es perfectamente probable que **la fiscalía esté buscando un “chivo expiatorio” ante su incapacidad de dar con los responsables** de la ola de inseguridad en Bogotá.

“Mateo está triste porque quiere continuar en la universidad y nosotros estamos preocupados por la actitud de la fiscalía de querer buscar un chivo expiatorio” explica Omar, porque imputar a un joven como Mateo los delitos de terrorismo, hurto calificado y agravado, fabricación, tráfico o porte de armas, y concierto para delinquir puede arruinarle la vida. [(Le puede interesar Denuncian dos nuevos falsos positivos judiciales en Córdoba](https://archivo.contagioradio.com/denuncian-2-nuevos-falsos-positivos-judiciales-en-cordoba/))

### **Quién es realmente Mateo Gutiérrez**

Mateo, es un estudiante entregado a sus estudios, un joven de 20 años que acostumbra viajar mucho y que casi siempre lo hace en compañía de sus familiares. Su padre lo describe como un amante de la música, deportista consagrado y con una aguda sensibilidad social, que lo llevó a estudiar sociología en la Universidad Nacional y a comprometerse con el estudio.

Se espera que en los próximos días la defensa de Mateo logre demostrar su inocencia y que así como se han acelerado en afirmarlo como responsable de la serie de situaciones que se han presentado en Bogotá, también sea rápida la posibilidad de que se recupere el buen nombre del joven y de su familia.

El siguiente es el comunicado público de los padres de Mateo:

COMUNICADO A LA OPINIÓN PÚBLICA

*Como padres de Mateo Gutiérrez León deseamos manifestar lo siguiente respecto a su captura, el pasado jueves 23 de febrero de 2017.*

*Los medios de comunicación televisivos, excepto CityTV, han presentado una falsa semblanza de la imputación de cargos, de la personalidad y trayectoria de nuestro hijo, refriéndose a él como “alias” Mateo, siendo éste su nombre de pila. También mostraron fotos suyas con diferente apariencia como si se tratara de un camuflaje, cuando en realidad corresponden a diferentes momentos de su vida; y lo que es más grave aún, le indilgaron el estar involucrado en una serie de hechos nefastos a los cuales, en ningún momento la Fiscalía hizo alusión en la audiencia de imputación de cargos. Lo que nos lleva a pensar que este falso positivo judicial, lo piensan extender a otros casos para hacer de Mateo un chivo expiatorio.*

*Frente a tanto calificativo absurdo e injuriante, exigimos el derecho de réplica y rectificación de la imagen y dignidad de nuestro hijo. En este sentido, primero que todo debemos decir que Mateo es un estudiante consagrado, de quinto semestre de sociología de la Universidad Nacional. Su ocupación primordial es el pensamiento y la palabra, haciendo de él un ser libre. Amante de los animales, el rock, el punk, el cine, los buenos libros y el deporte.*

*Desde muy pequeño, nutrió su pensamiento de la literatura clásica: recordamos que a sus ocho años, su libro preferido era la versión bilingüe de la Odisea. En su adolescencia su pasión fue la lectura, la natación y el tiro con arco.*

*Mateo es un joven de carácter calmado y cálido, amado por sus amigos. Que en los últimos cinco años ha tenido la oportunidad, en cuatro oportunidades, de ir de vacaciones fuera del país; acompañado siempre de su mamá, han visitado: Ecuador, en 2012; Cuba en 2013; Perú y Bolivia en 2015 y nuevamente Cuba en 2017. Es un joven de ideas de avanzada, buen conocedor de teorías sociales y de la historia de América Latina y de Europa, en especial de España.*

*Mateo se conmueve con la situación del país y de los más humildes. Recordamos que a los cinco años de edad lo vimos llorar después de visitar el museo indígena en Santa Marta, al preguntarle por qué, nos respondió que le dolía lo que los españoles habían hecho a los Tayronas.*

*Ese es nuestro hijo. De ser sus padres, nos sentimos profundamente orgullosos. Sabemos que es inocente y víctima de un falso positivo judicial.*

*Agradecemos sus manifestaciones de solidaridad, cariño y apoyo.*  
*Unidos demostraremos su inocencia.*  
*Aracely León Medina - Omar Gutiérrez Lemus*  
*Bogotá, D.C. 26 de febrero de 2017*

###### Reciba toda la información de Contagio Radio en [[su correo]
