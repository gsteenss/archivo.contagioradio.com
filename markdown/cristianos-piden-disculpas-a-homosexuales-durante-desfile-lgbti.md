Title: Cristianos piden disculpas a homosexuales durante desfile LGBTI
Date: 2015-04-03 10:56
Author: CtgAdm
Category: Escritora, Opinion
Tags: Cristianos, Escritora encubierta, LGBTI, Semana Santa
Slug: cristianos-piden-disculpas-a-homosexuales-durante-desfile-lgbti
Status: published

#### Por: [[Escritora Encubierta](http://https://archivo.contagioradio.com/escritora-encubierta/) - [@[OneMoreHippie] 

Hace poco, me encontraba navegando en Internet cuando me topé con una noticia que, aunque sea un poco vieja, pudo devolverme la fe en la humanidad. Aprovechando las festividades actuales, desempolvaré y sacaré a relucir dicho suceso.

Alrededor del 2012, un grupo de personas cristianas en Chicago se presentó en un desfile gay. No para fomentar el odio como estamos acostumbrados a ver, sino para pedir disculpas a los homosexuales por los actos homofóbicos provenientes de la Iglesia.

El grupo decidió usar camisetas negras con mensajes de ''lo siento'' y llevar carteles con frases conmovedoras como ''lamento que los cristianos te juzguen'', ''lo siento por cómo la Iglesia te ha tratado'', ''lamento que los cristianos te hayan rechazo'' y ''solía ser homofóbico, lo siento''. Al parecer nunca es demasiado tarde para cambiar de mentalidad.

Entre el público hubo diversas reacciones; en su mayoría de shock y sorpresa. Incluso, uno de los jóvenes que desfilaba se detuvo con lágrimas en sus ojos para abrazar eufóricamente a uno de los cristianos.

A pesar de que este acto no se internacionalizó como debería, en Estados Unidos la noticia causó furor en su momento y pasó a ser documentada como una de las historias más insólitas de todos los tiempos.

Sin darle mucho rodeo, esta es una gran prueba de que no necesariamente debe haber un muro divisorio entre la religión y la homosexualidad. Los religiosos y los miembros de la comunidad LGBTI sí podemos vivir en paz y armonía, siempre y cuando aprendamos a respetarnos los unos a los otros y tolerar nuestras diferencias. Al fin y al cabo, todos somos humanos, todos tenemos los mismos derechos.

¡Feliz semana santa!
