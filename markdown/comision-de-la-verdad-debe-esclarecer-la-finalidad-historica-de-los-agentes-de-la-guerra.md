Title: Comisión de la Verdad debe esclarecer la finalidad histórica de los agentes de la guerra
Date: 2015-06-07 21:29
Category: Nacional, Paz
Tags: Alfredo Molano, Cese Bilateral de Fuego, cese unilateral de las FARC, comision de la verdad, Conversaciones de paz de la habana, Frente Amplio por la PAz, Padre Javier Giraldo
Slug: comision-de-la-verdad-debe-esclarecer-la-finalidad-historica-de-los-agentes-de-la-guerra
Status: published

###### Foto: contagioradio.com 

###### [07 Jun 2015] 

Este jueves se anunció desde la Habana que se acordó la creación de una Comisión de la Verdad cuando se hayan firmado los acuerdos de paz entre el gobierno colombiano y la guerrilla de las FARC. Para analizar este acuerdo al que se llega en la Habana, consultamos al periodista, escritor, y sociólogo Alfredo Molano.

Contagio Radio ¿Usted cómo ve esta propuesta de la Comisión de la Verdad?

Alfredo Molano.M: Es una salida a lo difícil que estaba la negociación en la habana. Se habría resuelto el problema de la tierra, de la participación política, en parte de la coca, en parte de las minas, se estaba, como dijo uno de los representantes de las FARC, la cuestión de la justicia como una mula atravesada en el camino. Yo creo que esta Comisión, que esa iniciativa, desempantana esa situación. Me parece trascendental. Me parece uno de los puntos más avanzado al que ha llegado la mesa porque, finalmente, resuelve, en principio, el problema de las víctimas y de la finalidad histórica que tienen los agentes de la guerra en el conflicto.

**["Yo creo que esta Comisión, que esa iniciativa, desempantana esa situación. Me parece trascendental".]** 
------------------------------------------------------------------------------------------------------------------------------------

C:¿Cómo ve el tema de la justicia en esta Comisión de la Verdad?

A.M: La realidad es que esa inconformidad me parece que ha sido una inconformidad destacada.  En primer lugar, por el uribismo, pero, de alguna manera, también tolerada por el gobierno, para mejorar los términos de negociación con la guerrilla; pero yo creo que el país se está dando cuenta del costo de la guerra, de la situación en la que está comprometido el país con el conflicto armado, y, por lo tanto, la opinión pública, aunque es muy manejada por los boletines de guerra de los batallones, y por la discusión que de ellos hacen los medios, de todas maneras se está dando cuenta de la necesidad de un cese de hostilidades bilateral y de la posibilidad de que el país en nombre de las víctimas no profundice la guerra.

**["el país se está dando cuenta del costo de la guerra"]** 
------------------------------------------------------------------------------------

C: ¿cuáles cree que son las claves para que esta Comisión de la Verdad realmente funcione?

Las claves, la primer clave, naturalmente, es la composición de la Comisión. Tienen que ser personajes, hombres y mujeres, de una gran calidad moral, como se dice, fuera de toda sospecha. Gente que haya entendido el problema de la guerra históricamente, políticamente, éticamente. Entonces, ese es el primer requisito. El segundo, debe tener una independencia completa del gobierno, del Estado, y de los mecanismos judiciales; no comprometer en sus conclusiones esteral judiciales de hecho, y superaría el problema de cárcel o no cárcel, que han estado  empeñando los representantes de ambas partes en la habana. Creo que eso de las posiciones más positivas de esa Comisión.

C: ¿Qué hacer con esta Comisión de la Verdad si la Comisión de Memoria Histórica se quedó como en un limbo?

A.M: Es que la Comisión de Memoria Histórica elaboró una serie de argumentos;  es un insumo para la Comisión de la Verdad. Es decir, la Comisión de la Verdad tiene que tener en cuenta estos trabajos que se hicieron, y yo me imagino que tendrán que ampliarlos, profundizarlos, detallarlos, y complementarlos, si se quiere. Pero son insumos básicos, como dice el mismo acuerdo, de la Comisión de la Verdad. Es el destino fundamental de esa Comisión de la Verdad Histórica.

**["la Comisión de la Verdad tiene que tener en cuenta estos trabajos que se hicieron"]** 
------------------------------------------------------------------------------------------------------------------

C:¿cómo se puede tener una clave para que el Estado también se responsabilice y haga parte realmente de esta Comisión de la Verdad?

A.M: El mandato de la Comisión de la Verdad es ese. Es decir, determinar responsabilidades históricas y políticas de los implicados en el conflicto armado; del lado de los paramilitares, de lado de la guerrilla, del lado del Estado, de todos; empresarios, políticos, militares, de los máximos responsables, naturalmente. Usted se imaginará que no puede ser el resto ni de las tropas, ni de los colaboradores, ni de los máximos jerarcas de las dos grandes partes; del Estado, y los paramilitares finalmente hicieron parte del Estado. Ahora bien, lo que sucede es que lo estudios de la Comisión de Verdad histórica, la mayoría de esos trabajos sí señalan al Estado como responsable, pero hay otros que no. Hay dos o tres, o cuatro, quizás, que son ambiguos, y dos que son tajantes en responsabilizar a la guerrilla como la causa de las víctimas.

C: En este momento únicamente falta un debate para que sea aprobado este refuerzo en el Fuero Penal Militar y, por otra parte estamos hablando de una Comisión de la Verdad, ¿usted cómo ve ese panorama?

A.M: Es que la Justicia Penal Militar es una instancia judicial de todas maneras, y la Comisión de la Verdad no; van cada una por su lado, digámoslo así; no se implican. La Comisión de la Verdad no puede hacer señalamientos que tengan carácter judicial, no tiene esos desarrollos. Se trata de establecer responsabilidades colectivas e individuales sobre agentes comprometidos en el conflicto, pero no tiene desarrollos judiciales, porque o si no, pues sería pues, imposible la verdad. Pero la verdad, de alguna manera, si se establece el sentido histórico y político, pues permitiría el descontrol para un recrudecimiento de la violencia o una perpetuación de ella.
