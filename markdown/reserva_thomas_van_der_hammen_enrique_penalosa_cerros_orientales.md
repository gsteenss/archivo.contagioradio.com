Title: "Peñalosa continúa firme en sus proyectos depredadores": María Mercedes Maldonado
Date: 2018-01-29 21:48
Category: Ambiente, Nacional
Tags: María Mercedes Maldonado, Ministerio de Ambiente, Reserva Thomas Van der Hammen
Slug: reserva_thomas_van_der_hammen_enrique_penalosa_cerros_orientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Reserva-Thomas-Van-der-Hammen-e1455219288852.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ambiente Bogotá] 

###### [29 Ene 2018] 

[En medio de la amenaza que representa el proyecto Lagos de Torca, para la permanencia de la Reserva Thomas Van der Hammen, el Ministerio de Ambiente le ha dado un respiro al ecosistema, ya que ante el temor sobre la resolución frente a los límites de las reservas forestales, **ha decidido abrir un espacio en el que los defensores de la naturaleza puedan aportar y participar en la creación de esa resolución.**]

Así lo ha decidido la cartera ambiental, teniendo en cuenta la carta que habían enviado una serie de científicos, y luego de llevar a cabo una reunión con veedores de la Reserva. Sin embargo, continúa en pie la idea de expedir una resolución sobre ese tema.

Se trataba de un proyecto que las organizaciones y las veedurías veían como "muy problemático", especialmente porque** se admitía reducir las áreas de las reservas cuando estaban degradadas,** lo que permitía que las CAR no cumplieran con su tarea de restauración, sino que por el contrario se dejara acabar con el ecosistema para luego dar vía libre a la implementación de, por ejemplo, proyectos urbanísticos, como podría suceder en la Van der Hammen.

### **¿Qué significa la decisión del Minambiente?** 

Por ahora, con la decisión de la cartera ambiental, se abre un espacio de mesas de trabajo con las organizaciones ambientales para revisar artículo por artículo el proyecto de resolución, de tal forma que sean escuchadas las observaciones de los ambientalistas, y de lograr una concertación, según explica María Mercedes Maldonado, integrante de Veeduría Cerros.

Sin embargo eso no significa que no se vaya a hacer algún tipo de modificación sobre los límites de las reservas forestales del país, ya que se trata de un proyecto que obligatoriamente deberá salir, aunque los ambientalistas aplauden la respuesta del Ministerio de Ambiente.

### ** ¿Que sigue en materia de protección de la Van der Hammen?** 

Teniendo en cuenta que la alcaldía de Enrique Peñalosa ya tiene en marcha su primer proyecto urbanístico, Lagos de Torca, con el cual se pone en riesgo la función de conectividad de la Reserva, los ambientalistas han interpuesto una acción de cumplimiento exigiendo a la alcaldía que cumpla la resolución de la cartera ambiental donde se dice que la franja que se piensa construir, hace parte de la Van der Hammen.

Eso además significaría que a **esa franja de la Reserva se le asignaría el uso de área verde,** y se tendría en cuenta la importancia de esa zona para la restauración del ecosistema. Según Maldonado, aunque dicha acción se interpuso en agosto del año pasado, aún no existe respuesta por parte de la juez que admitió la tutela, pues estas solicitudes usualmente deben decidirse en 20 días y tiene un procedimiento preferente.

"Es muy grave que la juez no haya pactado los términos constitucionales, y en cambio ya hay recursos de particulares para el proyecto de Lagos de Torca. Es decir que **hay una incertidumbre jurídica tanto para los ambientalistas como para los privados"**, dice la experta. (Le puede interesar: [Ya está listo el primer proyecto que pone en riesgo la Van der Hammen)](https://archivo.contagioradio.com/proyecto-lagos-de-torca_amenaza_van_der_hammen/)

### **Peñalosa sigue pensado en urbanizar los Cerros Orientales** 

Además de Lagos de Torca, los Cerros Ambientales de Bogotá siguen en riesgo. De acuerdo con Maldonado, por la personería de Bogotá, se enteraron que a finales de noviembre de 2017, Andrés Ortíz, el **secretario de Planeación, radicó la propuesta de volver urbana la zona de los cerros orientales**. Un total de 530 hectáreas, serían construidas de aprobarse dicha petición.

"Peñalosa insiste en estar de espaldas a la participación ciudadana. Le mintieron a la ciudadanía diciendo que se trataría de un sendero, cuando realmente más de 530 hectáereas serían urbanizadas", señala.

<iframe id="audio_23428833" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23428833_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
