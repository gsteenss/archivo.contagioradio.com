Title: 30 días para modificar pregunta de Consulta popular minera en Ibagué
Date: 2017-02-16 13:05
Category: Ambiente, Voces de la Tierra
Tags: Consejo de Estado, Consulta popular minera Ibagué
Slug: 30-dias-para-modificar-pregunta-de-consulta-popular-minera-en-ibague
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/consulta-ibague.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las2Orillas] 

#### [16 Feb 2017] 

La Consejo de Estado señaló, a través de un fallo, que la **pregunta que se formuló para llevar a cabo la consulta popular  minera en Ibagué debe replantearse**, motivo por el cual ordenó al Tribunal Administrativo de Ibagué junto a la alcaldía, realizar los ajustes en un plazo de 30 días para continuar con el proceso de la consulta, desmintiendo que se haya frenado.

La pregunta era **¿Está usted de acuerdo SÍ o NO que en el Municipio de Ibagué se ejecuten proyectos y actividades mineras que impliquen contaminación del suelo, pérdida o contaminación de las aguas o afectación de la vocación agropecuaria y turística del municipio?**, que para la Corte de Estado debe ser ajustada.

Sin embargo, pese a que esta decisión del Consejo significa una demora más en los trámites, para el Comité Ambiental en Defensa de la Vida, este fallo “**ratifica nuevamente que los municipios tienen competencia para adelantar consultas populares** sobre el desarrollo de proyectos y actividades mineras en su territorio, toda vez que tal competencia hace parte de la autonomía que el artículo 1º de la Carta Política les reconoció a las autoridades territoriales”. Le puede interesar: ["Consulta popular minera por fin tiene vía libre"](https://archivo.contagioradio.com/consulta-popular-minera-ibague-fin-via-libre/)

La Corte Constitucional **dejó en firme el fallo que permite a los alcaldes del país vetar y oponerse a la realización de proyectos mineros en sus municipios** si consideran que pueden afectar el ambiente y desarrollo de las comunidades.

Es por ello que el Comité Ambiental en Defensa de la Vida expresó que continuará apoyando la consulta minera en Ibagué hasta que sea una realidad: “como tolimenses nos corresponde seguir construyendo caminos para la defensa de nuestros municipios. **Los fallos que actualmente siguen su curso en torno a la consulta popular, muestran que estamos fortalecidos** para seguir mostrando que como ciudadanos conscientes estamos en la capacidad de defender nuestro territorio”. Le puede interesar: ["Consulta popular minera busca frenar 35 títulos mineros en Ibagué"](https://archivo.contagioradio.com/consulta-popular-en-ibague-busca-frenar-35-titulos-mineros/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
