Title: Un futuro incierto para el 'Plan Paz Colombia'
Date: 2016-03-11 12:08
Category: Entrevistas, Paz
Tags: plan Colombia, plan paz colombia
Slug: los-verdaderos-alcances-del-plan-paz-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Plan-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Desparchantes Argentinos ] 

<iframe src="http://co.ivoox.com/es/player_ek_10765600_2_1.html?data=kpWkmJqadJGhhpywj5adaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5ynca3j1JDjx9fIpcXZ09TgjcbQp8LixMrgjcnJsIyZk5y9zsbSb7HV25Cw0dHTscPdwoqfmZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Adam Isacson, WOLA] 

###### [11 Mar 2016] 

El Congreso de Estados Unidos discute la aprobación de 450 millones de dólares para la transición hacia la paz en el marco del llamado 'Plan Paz Colombia', proyecto que no está siendo consultado, del que aún no se saben mayores detalles y que comparte los mismos marcos con el 'Plan Colombia', pese a que destina menos recursos, de acuerdo con Adam Isacson, integrante de la Oficina en Washington para América Latina WOLA.

Isacson afirma que el 'Plan Paz Colombia' "decepciona", si se tiene en cuenta que durante 2008 y 2010, "cuando estaba en auge la guerra", la cifra destinada por el Gobierno Bush a través del 'Plan Colombia' fue de 750 millones de dólares; sin embargo, destaca que "por primera vez no se trata de ayuda militar ni policiva" y que serán cerca de 187 millones de dólares los que se asignaran para proyectos productivos en los territorios, pese a que siga contemplándose la erradicación manual de cultivos de coca.

Frente al debate en el Congreso, Isacson asegura que los republicanos temen que con la firma de los acuerdos de paz venga una explosión de los cultivos de coca y que la cantidad de cocaína que sale de Colombia con destino a otros países aumente, por lo que buscaran la extradición de líderes de las FARC-EP; sin embargo, es un sector minoritario el que ve negativo el proceso de paz, frente a otro pequeño grupo que considera que el acuerdo de justicia transicional no es lo suficientemente duro.

La implementación del 'Plan Paz Colombia' depende en gran medida de quien ocupe la presidencia en los Estados Unidos y según refiere Isacson la contienda se dará entre Hilary Clinton y Donald Trump, candidaturas que pueden ser leídas desde dos perspectivas. La victoria de Trump seria ganancia para el sector republicano compuesto por la minoría rica estadounidense que se vio obligada a buscar alianzas “sucias” con otros sectores para ganar mayoría, en suma un “partido dividido y en descomposición”, como afirma el integrante de Wola.

Desde otra óptica se considera que hay toda una ola populista en Estados Unidos, que se está revelando contra el Estado sea de izquierda o de derecha, hecho que Trump ha sabido aprovechar para impulsar su campaña, sin embargo se estima que sólo logre los votos de los blancos, varones y cristianos frente a las mujeres, latinos y afros que apoyarían la candidatura de Clinton, cuya presidencia significaría “continuismo”, sumado a una política un poco más militarista que la del mandatario actual, según afirma Isacson.

De acuerdo con el integrante de WOLA, el que Trump gane la presidencia significaría la implementación de un "isolacionismo" a través del que Estados Unidos tratara de revocar tratados de libre comercio, continuará con el intervencionismo y destinara menos recursos para ayuda internacional, teniendo en cuenta que se trata de "un empresario multimillonario que odia a los migrantes y quiere una postura guerrerista y pro tortura contra los musulmanes", aunque proponga apoyos en salud y haya manifestado su oposición a la guerra de Vietnam. "Es un personaje impredecible por lo que no sería una inclinación a la derecha", agrega Isacson.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
