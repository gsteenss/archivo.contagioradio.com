Title: Minería inconsulta afecta a comunidades y al río Apartadocito en Chocó
Date: 2016-12-23 11:01
Category: Ambiente, Nacional
Tags: Consejos Comunitarios, Consultas Previas, Curvarado, Jiguamiandó, Minería en Chocó
Slug: mineria-inconsulta-afecta-comunidades-al-rio-apartadocito-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/minería.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ytimg] 

###### [23 Dic 2016] 

Desde el mes de Noviembre, comunidades de los territorios colectivos de Jiguamiandó y Curvaradó en Chocó, han denunciado actividades de minería inconsulta en la parte alta del río Apartadocito. Las familias afectadas **han exigido que se retire la maquinaria, pero los operarios dicen que se amparan en un permiso emitido por el Consejo Comunitario** presidido por Ever Alberto Rentería.

Miembros de las comunidades interpelaron al presidente del Consejo Ever Rentería, sobre la explotación consentida por el Consejo Comunitario a lo que éste les respondió: **“ustedes no son nadie en el territorio para prohibir que la gente trabaje”**. Frente a ello, el Consejo Comunitario Menor de Apartadocito interpuso denuncia ante el personero de Carmen del Darién, el pasado 9 de Diciembre.

Por otra parte, a pesar de las acciones adelantadas, **hasta la fecha las maquinarias siguen operando y afectando los ríos Apartadocito, Caño Claro y Curvaradó.** En diálogos de la Comunidad de Apartadocito con el comandante de la policía de Pavarandó, éste confirmó que efectivamente ingresaron las retroexcavadoras exhibiendo un permiso que no se correspondía con la explotación minera, por lo que la policía se comprometió a emprender labores para el retiro de la maquinaria.

La junta del Consejo Comunitario del Curvaradó que autorizó el ingreso de la maquinaria, sin consultar con los habitantes del territorio afectado por la explotación minera, **fue elegida a instancias del Ministerio del Interior que legitimó la exclusión de afro mestizos, como los de la comunidad de Apartadocito,** de su derecho a elegir y ser elegidos, en contra vía de lo ordenado por la Corte Constitucional. Aún se desconoce cuales han sido los intereses tras la aprobación de esta actividad de minería, por parte de Ever Renteria.

###### Reciba toda la información de Contagio Radio en [[su correo]
