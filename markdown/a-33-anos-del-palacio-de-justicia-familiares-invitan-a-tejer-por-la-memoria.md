Title: Tejiendo, familiares conmemoran 33 años del Palacio de Justicia
Date: 2018-11-02 13:20
Author: AdminContagio
Category: Sin Olvido
Tags: Desaparecidos del Palacio de Justicia, Palacio de Justicia, Tejido de Memoria
Slug: a-33-anos-del-palacio-de-justicia-familiares-invitan-a-tejer-por-la-memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Palacio-de-Justicia-e1459457565788.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: RCN Radio 

###### 2 Nov 2018 

Durante este fin de semana festivo se conmemorará el **aniversario 33 de la toma y retoma del Palacio de Justicia**,son más de tres décadas en que los familiares de los desaparecidos siguen aguardando por los avances que puedan aportar las instituciones del Estado sobre lo sucedido el **6 y 7 de noviembre de 1985.**

Respecto a los adelantos en la investigación del caso, **Pilar Navarrete, esposa del desaparecido Héctor Jaime Beltrán**, asegura que los familiares son conscientes de que la resistencia y la lucha por la verdad son lo único que puede hacer frente al paso del tiempo, variable que dificulta la claridad en u**n proceso caracterizado por la lentitud de la justicia  y la ausencia de interés por parte de los Gobiernos en las últimas tres décadas, lo que ha derivado en impunidad frente a los casos.**

Particularmente frente al caso de su esposo, Pilar recuerda que los avances presentados por el Instituto Nacional de Medicina Legal han generado más zozobra en las familias, tras encontrar los restos de Héctor Jaime en la tumba del Magistrado auxiliar **Julio César Andrade,** que ha pretendido presentarse como argumento para asegurar que nunca hubo desparecidos sino que se trató de errores técnicos en el levantamiento de los cuerpos.

"El director de Medicina Legal, el doctor Valdés asume la responsabilidad de lo sucedido, eso es mentira eso no fue un error, fue a propósito; el sol no se puede tapar con un dedo, la verdad salió a flote y salió" y agrega que ante las evidencias que van surgiendo lo que queda claro es que"desaparecidos hay, los videos están, se comprobó, se condenó".

Frente a la solicitud presentada por el **Gral (r) Iván Ramírez Quintero de acogerse a la JEP**, Pilar Navarrete asegura que "vamos a ver si hay  algo que nos sirva o no" en relación con la verdad que pudiese aportar el uniformado. Sin embargo, señala que este sometimiento no es garantía, recordando lo ocurrido con **Alfonso Plazas Vega, condenado y luego absuelto** **por el caso**, gracias  a que "ha hecho un uso de los medios impresionante y de su popularidad pero eso no tapó todo lo que se ha descubierto, hay impunidad y no se puede negar".

**Un tejido por la memoria**

Navarrete explicó que la conmemoración tendrá lugar **el lunes 5 de noviembre desde las 10:00 am con una eucaristía para familiares y allegados y después de la 12:00 pm realizó una invitación para todos los que quieran asistir a la Plaza de Bolívar** y contribuir en la elaboración de un tejido de la memoria, una forma simbólica de no olvido para los más de 66.600 desaparecidos en el país.

Adicionalmente, dentro del marco de la conmemoración **se realizará una obra de teatro que será exhibida los días 8, 9 de noviembre en el Centro de Memoria a las 6:30 con entrada libre hasta llenar aforo y el día 10 una versión paga**, dicha obra fue realizada por algunos de los familiares de los desaparecidos y narra algunos de los hechos vividos aquel día con el fin de construir memoria y exigir verdad.

<iframe id="audio_29800012" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29800012_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
