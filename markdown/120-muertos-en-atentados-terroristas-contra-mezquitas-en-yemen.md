Title: 120 muertos en atentados terroristas contra mezquitas en Yemen
Date: 2015-03-20 20:24
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: 120 muertos en atentados contra mezquitas en Yemen, violencia contra mezquitas chiíes, Yemen atacadas mezquitas, Yemen atentados terroristas
Slug: 120-muertos-en-atentados-terroristas-contra-mezquitas-en-yemen
Status: published

##### Foto:Serie49.do 

En un **doble atentado contra las dos principales mezquitas chiíes** de la capital de Yemen, han muerto **120 personas** y hay más de 300 heridos.

Las mezquitas atacadas son la **Badr y  Al Hashahush, situadas en barrios densamente poblados y céntricos**, controladas por el grupo chií rebelde los **Hutíes**, éstas han sido atacadas con hombres **suicidas cargados de explosivos**, uno dentro y otro en la salida de las mezquita.

En el ataque ha muerto el Iman de la mezquita de **Badr**, líder religioso chií Mortada al Muhaduari y han sido  gravemente heridos otros líderes espirituales con amplio reconocimiento en el país.

EI **(Estado Islámico), ha reivindicado los atentados**, pero aún no ha habido confirmación por fuentes oficiales de su autoría.

Yemen atraviesa una de sus peores **crisis políticas** en medio de un conflicto armado que parece no acabar. Dividido en un norte controlado por los hutíes, el sur donde actualmente se encuentra recluido el presidente, y el centro controlado por fuerzas de **Al-Qaeda.**

**EEUU** ha participado activamente en la guerra del gobierno contra Al-Qaeda interfiriendo en asuntos políticos y **armando militarmente al ejército**. Para EEUU Yemen es un país clave en la lucha contra Al-Qaeda.

**Arabia Saudí** ha sostenido la tesis de que los **hutíes eran armados por Iran**, con el objetivo de poder controlar la península arábiga.

Actualmente el presidente se encuentra recluido tras un ataque al palacio presidencial por parte de los hutíes en el sur del país. **Los diálogos de paz han sido bloqueados y existen más de 3 frentes de guerra sin apagar.**
