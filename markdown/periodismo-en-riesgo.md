Title: Periodismo en riesgo
Date: 2019-06-09 08:40
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Censura, Libertad de expresión, medios, Periodismo en riesgo
Slug: periodismo-en-riesgo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/periodismo-en-riesgo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Me será imposible en esta entrada escribir nombres propios, puesto que lo que narraré brevemente, no se condensa en responsabilidades únicas. Esa mala costumbre de creer que los problemas sociales se pueden individualizar es tan solo una costumbre neoliberal.

Si bien muchos de los que se podrían denominar gurús del periodismo son aquellos/as en quienes evidentemente pienso y los veo más como “jueces” que como periodistas, siento también, que paulatinamente la libertad de expresión padece una metamorfosis y decididamente se convierte en una libertad tiránica; esta libertad está abriendo desvíos epistemológicos que transforman el ejercicio de la profesión periodística, donde en ocasiones se ha abandonado el estudio de la comunicología, y se ha impulsado la mordacidad como mecanismo mercadotécnico propio de una dinámica capitalista abiertamente lícita, pero en ocasiones mal sana para el difícil momento que vive hoy Colombia, un país en cuyas venas aún habitan esos odios sin resolver.

**¿Quedan periodistas en Colombia?** claro que sí, hay periodistas que tienen amistades en el poder, que tiene amistades en las bases sociales, que no tiene amistades y sin embargo ejercen la posibilidad de un acercamiento a los hechos que dejan abiertas las puertas para el juicio por parte de quienes los analizan de manera politizada o sociológica.

Que quede claro que politizar la realidad no es un crimen, es un ethos de la vida social, de hecho, politizar es creer en el lenguaje, es darle un sentido comunitario a la manera como nos afectan los hechos del país o del mundo; en este sentido, es también una polarización positiva porque tiene una médula solidaria que vincula al otro y no solo es afirmativa-agresiva como lo puede ser la opinión individual en las redes sociales.

Eso de “no polarices” es un cliché emotivo que huye de la confrontación porque cree que debatir es decir lo que “yo pienso” y confunde actitud-positiva (no discutir) con asertividad-dialógica (capacidad para discutir sin violentarnos). Por eso llamar a “no polarizar” solo para “no discutir” carece de todo sentido político, ya que finalmente consciente o inconscientemente estamos parados sobre una posición desde la que observamos la realidad y sobre la que emitimos opiniones, interpretaciones y posturas, sean escuetas o en formato APA, mediante ese dulce y picante mecanismo que nos hace seres políticos por naturaleza: el lenguaje.

> ...escuchamos o leemos las “sentencias” de los gurús del periodismo, quienes no se remiten a dar su opinión en las columnas de opinión, sino que exportan su posición política a esos programas de la mañana

Pero todo lo anterior, parece una arena muy fina que se escapa entre las manos cuando escuchamos o leemos las “sentencias” de los gurús del periodismo, quienes no se remiten a dar su opinión en las columnas de opinión, sino que exportan su posición política a esos programas de la mañana que escuchan millones de personas y que son presentados como escenarios supuestamente libres de toda parcialidad.

**¿Es un crimen tener una posición editorial?** claro que no. El problema es cuando un medio masivo, se presenta ante millones de personas como un agente neutral y resulta que no lo es. Esto produce tergiversación de la realidad, historias a medias, emergencia de matrices mediáticas emotivas que arrojan a las personas a vociferar violencias intempestivamente (como si las palabras no fueran el germen de las barbaries), y genera el fortalecimiento de las “fake news”, puesto que estas emergen desde el reflejo en el espejo que alienta esas ganas necias de querer narrar la realidad “a mí manera”. ¿Grandes cadenas mediáticas preocupadas por las “fake news”?

¿Pero acaso quién puso el ejemplo? ¡no de mentir! sino de ajustar la realidad “a su manera” … ¿quiénes?... Muchos gurús del periodismo están convencidos de que actúan neutralmente, pero de lo que no se dan cuenta es que tienen bastante poder como para crear un mundo más amplio de contradicciones que conduce su actividad profesional hacia las ramas de árboles en las que cantan (trinan) miles de aves que no se escuchan entre sí.

Últimos casos (muy renombrados) de censura, ocultamiento de información, juicios a diestra y siniestra, apoyo abierto a políticos, carga poderosa de adjetivos o emisión de moralejas amiguistas con tintes de falsa sabiduría luego de narrar un hecho; falta de respeto con los muertos, con los vivos y todo esto mantenido con la potencia que entregan al ego esos miles de seguidores en las redes, convence a los gurús de que son neutrales cuando no lo son. Eso es lo que los convierte en los peces más grandes del estanque sin duda alguna.

¿Cómo pescar esa clase de peces que se pintan subjetivamente con rayitas endebles de neutralidad?

1\) Los adjetivos. Vamos a los adjetivos que usan para referirse a los hechos, a las personas; en los adjetivos se halla la alegoría de toda su equivocación.

2\) La duración de las llamadas a “un amigo”. Cuánto duran hablando con unos, y cuánto duran hablando con otros. Cuánto: si, tiempo al aire, haga cuentas. Si el tiempo no es amigo ni enemigo, los gurús del periodismo saben manejarlo como aliado.

3\) La función apelativa o conativa que cargan sus palabras mientras narran los hechos; esta clave es difícil de percibir, consiste en que cuando los gurús hablan emiten un juicio que espera la aprobación del receptor. “Toda Colombia rechaza esto o aquello” “esto o aquello es una vergüenza” … esperan que los receptores afirmemos lo que dicen.

4\) Emotividad en las palabras. Oír a algunos hablar del exterminio de personas en Colombia como todos unos periodistas y luego escucharlos hablar de Venezuela como todos unos “Mikes Pompeos” es decepcionante. Incluso algunos gurús ya retirados, pedían una  
invasión militar ¡acto más degradante para la profesión!

5\) El juicio, el temible juicio al que se acostumbraron a realizar contra unos, pero no contra otros. En esto literalmente o nos creen tontos o son tontos.

El crimen no se halla en opinar o en tener una posición editorial, el crimen emerge cuando afirman que son la neutralidad y con dicha investidura imponen una única lectura de los hechos. El crimen reside en las palabras que fermentan el inicio de la barbarie, que justifican la guerra o que piden violencia vestidas de blanco castellano.

¿Acaso creen señores gurús del periodismo que sus palabras son lluvia que cae sobre la totalidad de las superficies? pueden ponerse muy calientes como cuando no ganan sus amigos o pueden ser fríos como cuando hablan de muertes que no les interesan… pero de seguro no están ni tibios.

------------------------------------------------------------------------

<div class="entry-content">

<div class="has-content-area" title="Las licencias obligatorias, una herramienta de política pública que salva vidas" data-url="https://archivo.contagioradio.com/las-licencias-obligatorias-una-herramienta-de-politica-publica-que-salva-vidas/" data-title="Las licencias obligatorias, una herramienta de política pública que salva vidas">

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

</div>

</div>
