Title: En diciembre se celebrará la vida de los habitantes de calle
Date: 2018-12-04 13:31
Author: AdminContagio
Category: DDHH, Nacional
Tags: Fiesta, Fray Gabriel, Fray Ñero, Habitantes de calle
Slug: habitantes-de-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Habitantes-de-calle..jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/habitantes-de-calle-5-e1471886708387.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/habitantes-de-calle-e1516372912796.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Fraygabrielofm] 

###### [4 Dic 2018] 

Este **29 de diciembre se desarrollará la tercera versión de la fiesta de navidad para habitantes de calle en Bogotá**, la celebración se llevará a cabo en el Parque Tercer Milenio y espera contar con más de 1.000 asistentes. Los organizadores de este evento esperan brindar un feliz día en diciembre a quienes están en condición de habitabilidad de calle, al tiempo que sensibilizan a la ciudadanía sobre esta difícil condición.

Según **Fray Gabriel Gutiérrez**, sacerdote franciscano y miembro de la fundación Callejeros por la Memoria, que organiza el evento, en el país hay más de 80 mil ciudadanos en condición de calle, y su situación es cada día más difícil al tiempo que son relegados en el olvido; razón por lo que se hace necesario llamar atención sobre este fenómeno. (Le puede interesar: ["Fin del Bronx no acaba con la violencia contra habitantes en condición de calle"](https://archivo.contagioradio.com/fin-del-bronx-no-acaba-con-la-violencia-contra-habitantes-de-calle/))

La situación que viven los habitantes de calle es similar en todo el país, esa fue la percepción del religioso el pasado 28 de agosto, cuando la Alcaldía de Medellín en conjunto con la Policía decidieron intervenir el llamado “Bronx” de esa ciudad. Al igual que pasó en Bogotá, lo que ocurrió después de esta operación se puede describir como la dispersión de los habitantes de la zona, y “la recuperación de predios, pero no de personas”.

Por estas razones, el Fray ha impulsado eventos que busquen hacer conciencia sobre las situaciones que tienen que sufrir quienes habitan en la calle**; en Medellín, la fiesta será el 22 de diciembre en la zona de Niquitao**, mientras en **Cali, el 25 de diciembre se adelantará el Banquete de Solidaridad en la Plaza San Francisco**, frente a la Gobernación del Valle.

### **“En las calles no hay fiestas, hay estigmatización y golpes”** 

El Fray sostuvo que “en las calles no hay fiestas, hay estigmatización y golpes, por eso, **la idea es acercar a la ciudadanía, los empresarios y las iglesias” a quienes, por muchas razones, no tienen un hogar**. Celebrando la vida en las calles es el nombre de la fiesta, que será ambientada por el grupo Son Callejero, y se espera la asistencia demás de 1.000 personas entre habitantes de calle, trabajadores sexuales y cachivacheros, como se conoce a quienes venden cosas de segunda.

Quienes deseen aportar con recursos económicos, o siendo voluntarios del evento, puede hacerlo poniéndose en contacto con la **Fundación Callejeros de la Misericordia** en internet, o al número: 313 613 4468. De igual forma, el banco Caja Social dispuso la cuenta 240 875 85100 para realizar donaciones, en dinero, a favor de esta causa. (Le puede interesar:["Aumentan asesinatos y desapariciones de habitantes de calle en Bogotá"](https://archivo.contagioradio.com/aumentan-asesinatos-y-desapariciones-de-habitantes-de-calle-en-bogota/))

<iframe id="audio_30547567" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30547567_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
