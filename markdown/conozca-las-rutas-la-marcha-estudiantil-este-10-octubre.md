Title: Conozca las rutas de la marcha estudiantil para este 10 de octubre
Date: 2018-10-09 17:26
Author: AdminContagio
Category: DDHH, Movilización
Tags: 10 de Octubre, marcha estudiantil, UNEES
Slug: conozca-las-rutas-la-marcha-estudiantil-este-10-octubre
Status: published

###### [Foto: UNEES ] 

###### [09 Octb 2018]

Los estudiantes de diferentes Institutos Tecnológicos, Universidades Públicas, docentes y algunas Universidades privadas de todo el país se movilizarán este miércoles 10 de octubre por la financiación de la educación pública. Aquí le mostramos las rutas que tomará la marcha estudiantil en Cúcuta, Barranquilla, Neiva, Medellín, Bogotá, Pamplona y Villavicencio.

[Barranquilla: Iniciará desde la Sede Norte (Carrera 51B) Bajará por la carrera 51B hasta llegar a la calle 90 en la cual se unirá la Universidad Autónoma del Caribe, posterior a este la marcha bajará por la calle 75 hasta llegar a la carrera 54, donde se encontrará con los y las estudiantes de la sede de Bellas Artes (calle 68), cruzando por esta hasta llegar a la carrera 47 donde bajará a la calle 64, para finalmente cruzar en la carrera 46 hacia la Plaza de la Paz. (Le puede interesar: ["Estamos ](https://archivo.contagioradio.com/gran-paro-estudiantil/)][‘ad portas’ de un gran paro estudiantil en Colombia"](https://archivo.contagioradio.com/gran-paro-estudiantil/))

[Medellín: Iniciará en el parque de Los Deseos, a la 1:00 p.m. La marcha partirá a las 2:00 p.m. Tomará la Avenida Ferrocarril hasta la Avenida Colombia, luego subirá hasta la Avenida Oriental, seguirá por la calle San Juan, después buscará la Avenida Las Vegas y culminará en el Politécnico Jaime Isaza Cadavid. ]Y en Medellín hay dos marchas más: La primera se reunirá a la 1:30 p.m. en el Rinconcito Ecuatoriano, sector San Germán, llegará hasta la Universidad Nacional y conectará el recorrido en la Minorista. Por su parte, los profesores que también saldrán, partirán de la sede de Adida (Calle 57 con carrera 42, Centro) y se sumarán en la Avenida Oriental.

[Pamplona: saldrá tres de las Sedes de la Universidad de Pamplona:  La Casona, Virgen del rosario y la Sede Principal. Luego se desplazará a la vía nacional frente al SENA, hasta llegar a la plazuela por la parte superior del mercado y después pasar por la Calle Real hasta llegar al parque. Se pretende terminar esta actividad con una asamblea general.]

\

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]{.s1}
