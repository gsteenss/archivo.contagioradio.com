Title: "La mejor herramienta para votar sí al plebiscito es la alegría" Angela María Robledo
Date: 2016-07-14 13:55
Category: Nacional, Paz
Tags: La paz si es contigo, plebiscito por la paz
Slug: la-mejor-herramienta-para-votar-si-al-plebiscito-es-la-alegria-angela-maria-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/utl.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ContagioRadio] 

###### [14 de Jul] 

Entre bombas, sonrisas y abrazos se lanzó el día de hoy la iniciativa "La Paz sí es contigo", que pretende, desde diversos mecanismos pedagógicos y culturales **abrir espacios para dar a conocer los acuerdos de paz y apostarle a que la votación del plebiscito sea un sí rotundo que propicie espacios de participación ciudadana**.

En el lanzamiento hicieron presencia personas que han vivido el conflicto armado desde diferentes orillas, es el caso de Iván Cepeda, hijo del dirigente político de la Unión Patriótica Manuel Cepeda, asesinado el 9 de agosto de 1994. Para el actual senador de la República lo más importante de el plebiscito por la paz es explicar los acuerdos, y exponer como son una herramienta para la lucha social y popular "**debemos explicarlos de forma sencilla y simple para la gente. La paz va a tener un impacto sobre la vida de cada uno de nosotros, va a significar un cambio favorable y positivo**".

Otra de las personas que estuvo presente y que apoya la iniciativa de "La paz sí es contigo" es la representante a la Cámara Angela María Robledo que dijo que "la mejor herramienta para el plebiscito por la paz es la alegría, salir a votar con alegría por el sí a la paz en Colombia, porque **la alegría, el entusiasmo y la esperanza, son la forma de poder neutralizar tanto odio**, tanto resentimiento e incluso el mismo miedo".

El consejal Hollman Morris también estuvo en el lugar y le aposto a que Bogotá se convierta en la capital de la paz: " **Bogotá debe ser motor de los afectos, motor de la reconciliación**. La historia nos enseña que lo que sucede en la capital se replica rápidamente en otros lugares, lamento que la alcaldía de Pañalosa valla en contra de lo que significa una paz con justicia social".

La cultura también hizo presencia con el director de cine Lisandro Duque que afirmo  estar seguro de "asistir a un momento que es una especie de epifanía, **el país esta concurriendo a unos tiempos inéditos que se reflejarán en la producción artística**. Se nos viene encima la tranquilidad, actos de solidaridad, memorias, reconstrucción de historias sin el miedo".

El[viernes 15 de julio se llevará a cabo la movilización](https://archivo.contagioradio.com/colombia-se-moviliza-por-el-si-a-la-paz/?preview=true&preview_id=26429&preview_nonce=9a2c5fa5dd&post_format=standard)nacional de "La paz sí es contigo" que tendrá diferentes puntos de concentración en el país y que contará con múltiples eventos culturales.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en [[Otra Mirada](http://bit.ly/1ICYhVU)] por Contagio Radio 

 
