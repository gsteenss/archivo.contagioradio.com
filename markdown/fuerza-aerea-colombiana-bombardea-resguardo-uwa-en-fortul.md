Title: Fuerza Aérea Colombiana bombardea Resguardo U'wa en Fortul
Date: 2016-09-07 13:40
Category: DDHH, Nacional
Tags: Agresiones fuerza pública, autoridades indígenas, bombardeos ejército, Uwa
Slug: fuerza-aerea-colombiana-bombardea-resguardo-uwa-en-fortul
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Fortul-Resguardo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: U'wa] 

###### [7 Sept 2016 ] 

Según denuncian los pobladores del Resguardo Indígena U'wa Cibarca, del municipio de Fortul en Arauca, el pasado domingo a las 2:15 de la madrugada fueron víctimas de un **bombardeo por parte de la Fuerza Aérea Colombiana, que destruyó los vidrios de la escuela** y generó la caída de una de las paredes del puesto de salud de la comunidad que resultó gravemente afectada por el hecho.

Cuatro familias que estaban en el lugar en un retiro espiritual, **a 700 metros de distancia de dónde impacto el bombardeo**, fueron afectadas por la onda explosiva que les provocó dolor de oído y de cabeza, vómito y malestar general. Sonia López, integrante de la Fundación Joel Sierra, asegura que la denuncia ya fue interpuesta ante las autoridades.

Para las comunidades que este tipo de hechos ocurran en el marco de los diálogos de paz, es lamentable, porque sus derechos continúan siendo vulnerados, en este sentido rechazan que la Fuerza Pública se haga presente en sus territorios, **atente contra su soberanía y ponga en peligro la supervivencia** de por lo menos 500 indígenas, que se resguardaban en el lugar.

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
