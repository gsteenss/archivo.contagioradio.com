Title: 65 familias desplazadas por enfrentamientos entre guerrilla y paramilitares en Chocó
Date: 2016-04-22 13:18
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas, enfrentamientos paramilitares guerrilla, FARC
Slug: 65-familias-desplazadas-por-enfrentamientos-entre-guerrilla-y-paramilitares-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Bajo-Atrato.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: IPS Noticias ] 

###### [22 Abril 2016]

⁠⁠⁠De acuerdo con la Comisión Intereclesial de Justicia y Paz y comunidades de la cuenca del Salaquí, Chocó, este jueves integrantes de las Autodefensas Gaitanistas de Colombia y de las FARC-EP se enfrentaron en inmediaciones del resguardo indígena de Isleta y de la comunidad afrodescendiente Villa Nueva Tamboral, en la que **65 familias se vieron obligadas a desplazarse al municipio de Riosucio**.

Tras los enfrentamientos que iniciaron a las cinco de la mañana, **los paramilitares han impedido la libre movilidad** de las comunidades de Playa Aguirre, Isleta, Salaquisito y Tamboral. "A quienes estamos en la zona no nos están dejando salir a realizar nuestras actividades agropecuarias", asegura un poblador.

La Cruz Roja Internacional y la Defensoría del Pueblo, han brindado acompañamiento a las comunidades mientras que **ninguna autoridad municipal o departamental se ha pronunciado**.

Con éste, son tres los episodios de enfrentamientos en el territorio colectivo de Salaqui que se han presentado en abril, aún cuando para llegar a él los **pobladores deben pasar por estrictos controles del Batallón Fluvial**, cuyos miembros los han acusado de ser colaboradores de la guerrilla.

Las comunidades denuncian que otros miembros de las [[Autodefensas Gaitanistas](https://archivo.contagioradio.com/?s=autodefensas+gaitanistas)] hace presencia en la cuenca del Cacarica, sitiando a las comunidades aledañas a Tamboral, en el Salaquí; mientras que **otros operan de civil en el casco urbano de Riosucio**.

<iframe src="http://co.ivoox.com/es/player_ej_11269744_2_1.html?data=kpafmJ6beJWhhpywj5WYaZS1kZeah5yncZOhhpywj5WRaZi3jpWah5yncanVw87hw9PYqYzHwtHO09rNcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
