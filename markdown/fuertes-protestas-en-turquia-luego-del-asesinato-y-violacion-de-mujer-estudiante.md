Title: Fuertes protestas en Turquía luego del asesinato y violación de mujer estudiante
Date: 2015-02-16 20:30
Author: CtgAdm
Category: DDHH, El mundo
Tags: #Özgecan, Özgecan Aslan, protestas Turquía, Turquía, Violencia de género
Slug: fuertes-protestas-en-turquia-luego-del-asesinato-y-violacion-de-mujer-estudiante
Status: published

###### Foto:Latimes.com 

Fuertes disturbios se han presentado en las principales capitales del país después de que el viernes se conociera la noticia de la violación y el posterior asesinato de una estudiante por parte de un conductor de autobús.

**Özgecan Aslan estudiante de 20 años llevaba desaparecida desde el 11 de febrero, hasta que el viernes fue encontrado su cadáver con signos violación y tortura**. Al parecer, para borrar las huellas dactilares, el cuerpo tenía las manos mutiladas y estaba calcinado.

Después de la investigación policial **se procedió a la detención del conductor del minibus que la transportaba de la universidad a su casa y de dos cómplices más**.

Rápidamente la noticia se extendió provocando la indignación de la población y en concreto de los estudiantes que marcharon durante el sábado y hoy lunes en grandes marchas fuertemente reprimidas por la policía.

El principal partido de la oposición, organizaciones sociales de derechos humanos, estudiantes, laicos y organizaciones feministas han realizado declaraciones **culpabilizando al actual gobierno de carácter islamista** de ser el responsable de la violencia contra las mujeres debido a su moral y a su **escaso trabajo para defender los derechos de las propias mujeres.**

En los **últimos diez años las muertes con un móvil de violencia de genero han alcanzado la cifra de 300 mujeres asesinadas en 2014**.

Son muchas las declaraciones del presidente Erdogan de carácter machista. También el portavoz del gobierno hace poco dijo que la igualdad de género era "antinatural". Según los **observatorios de la violencia de género de Turquía la situación de la mujer es grave debido a la propia moral y las costumbres basadas en el machismo** y una sociedad totalmente patriarcal que obliga a la mujer a ser víctima de agresiones de todo tipo en total impunidad.

<div style="text-align: justify;">

</div>
