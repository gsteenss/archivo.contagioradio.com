Title: Prisioneros de guerra de las FARC-EP se encuentran en huelga de hambre
Date: 2015-11-10 17:48
Category: DDHH, Nacional
Tags: crisis carcelaria, Derecho a la salud, FARC-EP, Huelga nacional carcelaria, INPEC, prisioneros de guerra, proceso de paz, represión en las cárceles de Colombia
Slug: prisioneros-de-guerra-de-las-farc-ep-se-encuentran-en-huelga-de-hambre-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/20140129_105323.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto:movimientocarcelesaldesnudo 

<iframe src="http://www.ivoox.com/player_ek_9344033_2_1.html?data=mpihlpWXd46ZmKiakp2Jd6KompKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPd1M7c0MrWs9SfxcqYydrJttPVjMnSjdHFt4y6orewj6q0b9TZjMrbxdrJstXmwtOYx9OPrI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Miguel Angel Beltrán] 

###### [10 nov 2015]

Desde este lunes se  inició una jornada nacional de huelga de hambre impulsada por los prisioneros de guerra de las FARC-EP con el objetivo exigir la liberación de los internos que se encuentran en grave estado de salud.

El profesor Miguel Ángel Bertrán prisionero en la cárcel ERON - Picota, afirma que esta huelga surgió debido que los prisioneros de guerra se encuentran en situaciones inhumanas de salud **“muchos prisioneros de guerra están lisiados y tienen enfermedades terminales que no son atendidas".**

Son 13 establecimientos penitenciarios los que iniciaron esta jornada con la que se pretende que todas las cárceles colombianas se unan a esta huelga de hambre de manera indefinida, **“hemos elaborado un listado de personas que se encuentran en situaciones delicadas de salud, consideramos debe ser un gesto político del gobierno de acuerdo a los avances en las conversaciones de paz con las FARC”** afirma el profesor, quien explica que la jornada está acompañada de la realización de diferentes actividades culturales con los prisioneros con la finalidad de “ilustrar la situación que se está viviendo”.

De acuerdo con el profesor, los prisioneros de guerra viven con un sometimiento complejo debido que estos llegan con heridas en diferentes partes del cuerpo por los combates, **“el INPEC no atiende estas emergencias por esta razón muchos prisioneros pierden sus extremidades, algunos quedan ciegos  entonces es una venganza que toma el INPEC”, **que** **“se hacen de los oídos sordos y responde con represión", asegura Beltrán.

Cabe recordar que los prisioneros han ganado varias tutelas para amparar el derecho a la salud, pero el INPEC continúa vulnerando los derechos de los internos con la excusa de que no hay recursos para atender las enfermedades de los reclusos.
