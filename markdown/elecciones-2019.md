Title: Elecciones 2019
Date: 2019-10-27 11:17
Author: CtgAdm
Slug: elecciones-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Captura-de-pantalla-2019-10-27-a-las-11.09.38.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Captura-de-Pantalla-2019-10-27-a-las-5.58.13-p.-m..png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

elecciones 2019
---------------

Seguimiento a la jornada electoral del 27 de octubre

[  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/logo-contagio-radio-web-1.png){width="201" height="76"}](https://archivo.contagioradio.com/)

Resultados electorales - Alcaldía Bogotá
----------------------------------------

<p>
<canvas role="img" aria-label="Resultados electorales - Alcaldía Bogotá ">
</canvas>
</p>
ELECCIONES
----------

</i>","nextArrow":"","autoplay":true,"autoplaySpeed":5000}' dir="ltr" data-typing="1"&gt;  
![Tres elementos claves para garantizar unas elecciones transparentes](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Elecciones-MOE-150x150.jpg "Tres elementos claves para garantizar unas elecciones transparentes"){width="50" height="50" sizes="(max-width: 50px) 100vw, 50px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Elecciones-MOE-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Elecciones-MOE-512x512.jpg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Elecciones-MOE-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Elecciones-MOE-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Elecciones-MOE-360x360.jpg 360w"} 17:33  
[Tres elementos claves para garantizar unas elecciones transparentes](https://archivo.contagioradio.com/tres-elementos-claves-para-garantizar-unas-elecciones-transparentes/)  
![Atentan contra sedes del Partido Comunista y Partido FARC a pocas semanas de elecciones](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Partido-Comunista-150x150.jpg "Atentan contra sedes del Partido Comunista y Partido FARC a pocas semanas de elecciones"){width="50" height="50" sizes="(max-width: 50px) 100vw, 50px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Partido-Comunista-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Partido-Comunista-512x512.jpg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Partido-Comunista-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Partido-Comunista-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Partido-Comunista-360x360.jpg 360w"} 10:07  
[Atentan contra sedes del Partido Comunista y Partido FARC a pocas semanas de elecciones](https://archivo.contagioradio.com/atentan-contra-sedes-del-partido-comunista-y-partido-farc-a-pocas-semanas-de-elecciones/)  
![Estado debe acelerar acciones para garantizar tranquilidad en elecciones](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/votaciones-mesa-voto-150x150.jpg "Estado debe acelerar acciones para garantizar tranquilidad en elecciones"){width="50" height="50" sizes="(max-width: 50px) 100vw, 50px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/votaciones-mesa-voto-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/votaciones-mesa-voto-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/votaciones-mesa-voto-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/votaciones-mesa-voto-360x360.jpg 360w"} 12:15  
[Estado debe acelerar acciones para garantizar tranquilidad en elecciones](https://archivo.contagioradio.com/estado-debe-acelerar-acciones-para-garantizar/)  
DENUNCIAS

Aida Avella denunció a través de redes sociales: «No mas abusos Sr . Registrador. Le puede decir a los ciudadanos porque le quitaron 900 testigos a la Colomba Humana UP en Bogotá? Sus software privados a quién le trabajan?.

La Secretaría de Gobierno de Antioquia confirmó que en puestos de votación de Medellín y Turbo **le entregaron a los ciudadanos tarjetones con el voto marcado.**

Candidato al concejo de El Agrado (Huila) Álvaro Chavarro, fue atacado la noche anterior a las elecciones. Aunque no se conoce la causa, la MOE recuerda la importancia de mantener las mayores medidas de seguridad sobre los candidatos 

Autoridades reportan publicidad ilegal en regiones como Los municipios desde los que más se reporta este fenómeno son: Bogotá, Anorí (Antioquia), Medellín (Antioquia), Cali (Valle del Cauca), Guasca (Cundinamarca) y Armero Guayabal (Tolima).

27 de octubre de 2019  
Fundación Social “CORDOBERXIA”  
Nit. 900.836.826-1

URGENTE

En las afueras de la Institución Educativa Germán Gómez de Puerto Libertador está un conglomerado que amenaza con realizar una asonada e incinerar la alcaldía municipal.

Advertimos a la Policía Nacional en cabeza del coronel Jairo Baquero y a las Unidades Militares adscritas a la FTN Aquiles en cabeza del General Alberto Rodríguez.

La MOE manifiesta su preocupación por la situación del departamento de Magdalena, donde la pugnacidad entre las campañas electorales y el estado de tensión registrado en días anteriores ha derivado en brotes de violencia que han afectado el normal desarrollo de la jornada en Pijiño del Carmen y de manera particular en el municipio de San Zenón

Observadores reportan casos de puestos de votación donde la Policía ha impedido el ingreso de los votantes, lo que ha causado molestia y manifestaciones en Cartagena (puesto de Playas de Acapulco), y en horas de la mañana, en El Cerrito (Valle del Cauca)

Fue capturado un candidato al concejo del Bagre (Antioquia), del Partido Liberal, por compra de votos a las afueras de un puesto de votación del municipio, al igual que un candidato al concejo en Nechí (Antioquia).

En Cartagena (Bolívar), en Rivera (Huila), San Zenón, Pijiño del Carmen y Pedraza (Magdalena) y Tunja (Boyacá) se reportaron capturas de ciudadanos por delitos electorales.

Un jurado fue capturado en Neiva (Huila) por constreñimiento al sufragante.

MINUTO A MINUTO

La Misión de Observación Electoral (MOE) hace entrega pública del primer informe de irregularidades electorales, recogiendo lo ocurrido desde las 7:00 am hasta las 10:30 am.

### Orden público

**La Macarena- Meta:** Traslado de puesto de votación hacia la cabecera del municipio en La Catalina, esto consecuencia de la explosión de un artefacto cerca al punto de votación.

**San Pablo – Bolivar: **Se registra en horas de la noche enfrentamientos entre ELN y ejercito, en este corregimiento se tenia prevista la votación e 208 personas.

**Pailitas, Pelaya y Curumaní – Cesar:** En la mañana de hoy circuló en redes sociales un panfleto   firmado presuntamente por el  ELN amenazando a la población firmando que tienen sus propios candidatos y amenazando los contrarios, sin dar nombres en particular.

**San Felipe de Montelíbano – Córdoba: **Se registran amenazas del Clan del Golfo para impedir que la población se desplace a las urnas.

**San Zenón – Magdalena : **Por ataque a la Registraduría por parte de ciudadanos a los que les fue anulada la inscripción de su cédula, se detienen elecciones en este municipio.

**Pijiño del Carmen – Magdalena:** Por las mismas razones de suspención de cédulas , ciudadanos destruyen  material electoral en puestos, incluyendo 14 urnas con votos que ya habían sido efectuados.

Por enfrentamiento entre partidos políticos Dibulla y Hatonuevo (La Guajira), Aratoca (Santander), Villagarzón (Putumayo) la MOE  hace un llamado a los partidos políticos y las campañas electorales para mantener una relación respetuosa y pacífica.

**Logística electoral **

**Barrancabermeja:**  A los puestos de votación en Nueva Generación con 744 inscritos y El Llanito con 2.381 potenciales votantes no llegaron tarjetones de Junta Administrativa Local. (JAL)

**Barranquilla:** En el Colegio Colón con 15.459 potenciales votantes se registran protesta por parte de los votantes luego de que la policía les informara que no iban a llegar los cartones para votar por JAL.

**Antioquia:** Secretaria del gobierno de este departamento confirmó que en Medellín y Turbo  se entregaron cartones marcados; ciudadanos reportaron 10 mesas de la Institución Educativa Inem José Félix de Restrepo en la capital antioqueña y 6 meseas en Turbo, razón que deja inhabilitados los puesto de votación hasta que se haga verificación de los votos.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Captura-de-Pantalla-2019-10-27-a-las-1.43.06-p.-m..png){width="475" height="590"}

**4:00 pm** : Se cierran las mesas de votación todo el país.

**4:20 pm**  **Bogotá** : Primer boletín en Bogotá con 0,09 mesas escrutadas

Claudia López  33,19 %

Carlos Galán   27,12%

Hollman Morris 18,62% 

Miguel Uribe 12,55%

**4:30pm  Bogotá: **0,81% de mesas escrutadas.

Claudia López  38,47 %

Carlos Galán   24,89%

Hollman Morris 19,24% 

Miguel Uribe 11,03%

**4:33pm  Medellín :** 0,25% de mesas escrutadas.

Daniel Quintero   49,18 %

Alfredo Ramos    20,68%

Santiago Gómez  9,93% 

Juan David  Valderrama  2,28%

**4:56 Armenia:**

Boletin \# 4 – 744 mesas informadas, en esta ciudad lidera el voto en blanco con 39% en comparación con el candidato que lidera con un 25% de votos.

**4:56 Boletín 7 en la Alcaldía de Cali:**  
Jorge Iván Ospina Gómez 36%  
Álvaro Eder Garcés 21%  
Roberto Ortíz Ureña 19%  
Danis Antonio Rentería 6%

**4:57pm  Bogotá: 56**,97% de mesas escrutadas.

Claudia López  35,44% (604.587)

Carlos Galán   32,14% (548.294)

Hollman Morris 14,36%  (244.990)

Miguel Uribe 13,29% (226.761)

En Blanco 4,75% (81.090)

**5:00pm  Bogotá:** 77,16% de mesas escrutadas, boletín 13

Claudia López  35,37% (845.090)

Carlos Galán  32,43% (774.752)

Hollman Morris 13,98%  (334.041)

Miguel Uribe 13,50% (322.565)

En Blanco 4,70% (112.513)

**5:28pm  Bogotá:** 83,56% de mesas escrutadas, informe \#14

Claudia López  35,31% (914.420)

Carlos Galán   32,49% (846.188)

Hollman Morris 13,92%  (362667)

Miguel Uribe 13,54% (352683)

En Blanco 4,71% (122.743)

**5:35 pm  Bogotá:** 91,75% de mesas escrutadas, informe \#16

**Claudia López  35,56% (1.015.629)**

Carlos Galán   32,54% (937.321)

Hollman Morris 13,88%  (399.944)

Miguel Uribe 13,58% (391.262)

En Blanco 4,71% (135.687)

**Medellín**

Daniel Quintero: 2654 votos Alfredo Ramos: 1143 votos Santiago Gómez: 668 votos Juan David Valderrama : 193 votos Beatriz Rave: 101 votos Víctor Javier Correa: 64 votos Gemma Mejía Ramírez: 40 votos

**5:40 pm** Bogotá tiene su primera alcaldesa Claudia López

**5:44 pm: Balance de elecciones **

Potencial de votantes 36.541.160

votantes 17.174.079

% votantes 46.94%

mesas instaladas 107.733

mesas informadas 84.679 (78,60%)

**5:53 pm** La Misión de Observación Electoral, registra con satisfacción que no se han presentado hasta el momento hechos de orden público relacionados con grupos armados ilegales que perturben de manera determinante las elecciones en algunos municipios del país.

La Misión de Observación Electoral, *ha **recibido entre las 12:01 am y las 3:00 pm un total de 898 reportes sobre posibles irregularidades y delitos electorales de 30 departamentos y 238 municipios ***

**5:58 pm  Moe reporta **

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Captura-de-Pantalla-2019-10-27-a-las-5.58.13-p.-m..png){width="773" height="289"}

**6:12 Así se posiciona cada partido en las diferentes regiones:**

##### AMAZONAS : 

##### **Votos validos 25.369 **

Lidera el Partido Liberal Colombiano 

##### ANTIOQUIA : 

##### **Votos validos 428.598**

Lidera el Partido Centro Democrático

##### ARAUCA  : 

##### **Votos validos 16.878  **

Lidera el Partido Liberal Colombiano

##### ATLANTICO  : 

##### **Votos validos 383.596**

Lidera el Partido Cambio Radical

##### BOGOTÁ D.C  : 

##### **Votos validos 1.097.728**

Lidera la Coalición Claudia Alcaldesa

##### BOLIVAR  : 

##### **Votos validos 115.894**

Lidera el Partido Liberal Colombiano

##### BOYACÁ : 

##### **Votos validos 65.486**

Lidera el Partido Alianza Verde 

##### CALDAS : 

##### **Votos validos 80.732**

Lidera el Partido Alianza Verde 

##### CAQUETÁ: 

##### **Votos validos 28.002**

Lidera el Partido Centro Democrático

##### CASANARE: 

##### **Votos validos 33.358**

Lidera el Partido Centro Democrático

##### CAUCA: 

##### **Votos validos 83.777**

Lidera el Partido Liberal Colombiano

##### CESAR : 

##### **Votos validos 102.567**

Lidera el Partido de la U.

##### CHOCÓ: 

##### **Votos validos 30.553**

Lidera el Partido Liberal Colombiano

##### CORDOBA: 

##### **Votos validos 104.831**

Lidera el Partido de la U

##### CUNDINAMARCA: 

##### **Votos validos 88.741**

Lidera el Partido Centro Democrático

##### GUAINIA: 

##### **Votos validos 4.312**

Lidera la Coalición Inirida de Todos

##### GUAVIARE: 

##### **Votos validos 8.610**

Lidera el Partido Cambio Radical

##### HUILA: 

##### **Votos validos 56.506**

Lidera la Coalición por Neiva 

##### LA GUAJIRA: 

##### **Votos validos 37.435**

Lidera el Partido Convervador Colombiano 

##### MAGDALENA: 

##### **Votos validos 110.582**

LideraG.S.C Fuerza Ciudadana  

**ALCALDES ELECTOR EN TODA COLOMBIA**

<div class="section" data-id="3d6dbaaa" data-element_type="section">

 

</div>

<div class="section" data-id="98b4483" data-element_type="section">

 
-

</div>

REDES SOCIALES

> ESTO SON los primeros resultados de nuestra labor de observación ? Estuvimos presentes en 2.498 mesas de votación de 31 departamentos.  
> La MOE deja constancia de su labor e invita a la ciudadanía a seguir reportando irregularidades en <https://t.co/90mDmhhDVM> [pic.twitter.com/2nXLxwwB5l](https://t.co/2nXLxwwB5l)
>
> — MOE (@moecolombia) [October 27, 2019](https://twitter.com/moecolombia/status/1188516981936480262?ref_src=twsrc%5Etfw)

> Vea, una mujer en Turbo denunció que los jurados de la [@Registraduria](https://twitter.com/Registraduria?ref_src=twsrc%5Etfw) le entregaron un tarjetón marcado por el [@CeDemocratico](https://twitter.com/CeDemocratico?ref_src=twsrc%5Etfw). No hay capturas.[@piedadcordoba](https://twitter.com/piedadcordoba?ref_src=twsrc%5Etfw) [@martinhroman](https://twitter.com/martinhroman?ref_src=twsrc%5Etfw) [@LuzMBernalParra](https://twitter.com/LuzMBernalParra?ref_src=twsrc%5Etfw) [@luzmamunera](https://twitter.com/luzmamunera?ref_src=twsrc%5Etfw)  
> [@moecolombia](https://twitter.com/moecolombia?ref_src=twsrc%5Etfw) [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) [@NachoGomex](https://twitter.com/NachoGomex?ref_src=twsrc%5Etfw) [@MabelLaraNews](https://twitter.com/MabelLaraNews?ref_src=twsrc%5Etfw). [\#EleccionesColombia](https://twitter.com/hashtag/EleccionesColombia?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/jbNfc7jLNK](https://t.co/jbNfc7jLNK)
>
> — Kanabico Objetor ? (@elANTIMILI) [October 27, 2019](https://twitter.com/elANTIMILI/status/1188511416736714754?ref_src=twsrc%5Etfw)

> [\#YoVotoLimpio](https://twitter.com/hashtag/YoVotoLimpio?src=hash&ref_src=twsrc%5Etfw) | Hasta las 12:00 m., la [@PGN\_COL](https://twitter.com/PGN_COL?ref_src=twsrc%5Etfw) ha recibido 93 quejas y denuncias del proceso electoral. Las regiones con mayor número de solicitudes son: Antioquia, Bogotá, Santander y Bolívar: [@ViceprocuCortes](https://twitter.com/ViceprocuCortes?ref_src=twsrc%5Etfw) [pic.twitter.com/wyzXLlXi4A](https://t.co/wyzXLlXi4A)
>
> — Procuraduría Colombia (@PGN\_COL) [October 27, 2019](https://twitter.com/PGN_COL/status/1188519291504746496?ref_src=twsrc%5Etfw)

> ASÍ PODRÁN IDENTIFICAR a nuestros OBSERVADORES MOE ? ⭐  
> Estaremos en 566 municipios defendiendo la democracia en estas [\#EleccionesColombia](https://twitter.com/hashtag/EleccionesColombia?src=hash&ref_src=twsrc%5Etfw)  
> COMPARTIR POR FAVOR [pic.twitter.com/QzKxunn0s7](https://t.co/QzKxunn0s7)
>
> — MOE (@moecolombia) [October 26, 2019](https://twitter.com/moecolombia/status/1188221529039036416?ref_src=twsrc%5Etfw)

> Nuestro voto por hombres y mujeres del común combatientes de la paz y la vida??️ Nuestro voto por el sueño de un país en paz y reconciliado. [@PartidoFARC](https://twitter.com/PartidoFARC?ref_src=twsrc%5Etfw) [\#EleccionesColombia](https://twitter.com/hashtag/EleccionesColombia?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/fdDLEGWfKK](https://t.co/fdDLEGWfKK)
>
> — Rodrigo Londoño (@TimoFARC) [October 27, 2019](https://twitter.com/TimoFARC/status/1188450151326781446?ref_src=twsrc%5Etfw)

> Vea, una mujer en Turbo denunció que los jurados de la [@Registraduria](https://twitter.com/Registraduria?ref_src=twsrc%5Etfw) le entregaron un tarjetón marcado por el [@CeDemocratico](https://twitter.com/CeDemocratico?ref_src=twsrc%5Etfw). No hay capturas.[@piedadcordoba](https://twitter.com/piedadcordoba?ref_src=twsrc%5Etfw) [@martinhroman](https://twitter.com/martinhroman?ref_src=twsrc%5Etfw) [@LuzMBernalParra](https://twitter.com/LuzMBernalParra?ref_src=twsrc%5Etfw) [@luzmamunera](https://twitter.com/luzmamunera?ref_src=twsrc%5Etfw)  
> [@moecolombia](https://twitter.com/moecolombia?ref_src=twsrc%5Etfw) [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) [@NachoGomex](https://twitter.com/NachoGomex?ref_src=twsrc%5Etfw) [@MabelLaraNews](https://twitter.com/MabelLaraNews?ref_src=twsrc%5Etfw). [\#EleccionesColombia](https://twitter.com/hashtag/EleccionesColombia?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/jbNfc7jLNK](https://t.co/jbNfc7jLNK)
>
> — Kanabico Objetor ? (@elANTIMILI) [October 27, 2019](https://twitter.com/elANTIMILI/status/1188511416736714754?ref_src=twsrc%5Etfw)

> Con su voto en la mesa número 1 de la Plaza de Bolívar en Bogotá, el Presidente [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) hizo la apertura de la jornada electoral de este domingo. [\#EleccionesColombia](https://twitter.com/hashtag/EleccionesColombia?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/ERGWO40Ef7](https://t.co/ERGWO40Ef7)
>
> — Presidencia Colombia (@infopresidencia) [October 27, 2019](https://twitter.com/infopresidencia/status/1188449134145146889?ref_src=twsrc%5Etfw)

> Recomendaciones de la [@moecolombia](https://twitter.com/moecolombia?ref_src=twsrc%5Etfw) para salir votar este 27 de octubre de 2019 en Colombia <https://t.co/gQZI47NUHj>
>
> — Revelados (@ReveladosCol) [October 26, 2019](https://twitter.com/ReveladosCol/status/1187884120359292929?ref_src=twsrc%5Etfw)

> ? Listo el voto. Lo que fue fue. || Voto verde en los tres tarjetones ? [\#EleccionesColombia](https://twitter.com/hashtag/EleccionesColombia?src=hash&ref_src=twsrc%5Etfw) ?? [pic.twitter.com/f0uv7KwgTY](https://t.co/f0uv7KwgTY)
>
> — Vladdo (@VLADDO) [October 27, 2019](https://twitter.com/VLADDO/status/1188463678225571841?ref_src=twsrc%5Etfw)

> ?[\#Encuesta](https://twitter.com/hashtag/Encuesta?src=hash&ref_src=twsrc%5Etfw) por quien Votará a la Alcaldía de [\#Bogota](https://twitter.com/hashtag/Bogota?src=hash&ref_src=twsrc%5Etfw)?[@MiguelUribeT](https://twitter.com/MiguelUribeT?ref_src=twsrc%5Etfw) [@PartidoLiberal](https://twitter.com/PartidoLiberal?ref_src=twsrc%5Etfw) [@ColJustaLibres](https://twitter.com/ColJustaLibres?ref_src=twsrc%5Etfw) [@CeDemocratico](https://twitter.com/CeDemocratico?ref_src=twsrc%5Etfw) [@PartidoMIRA](https://twitter.com/PartidoMIRA?ref_src=twsrc%5Etfw) [@ClaudiaLopez](https://twitter.com/ClaudiaLopez?ref_src=twsrc%5Etfw) [@PartidoVerdeCoL](https://twitter.com/PartidoVerdeCoL?ref_src=twsrc%5Etfw) [@PoloDemocratico](https://twitter.com/PoloDemocratico?ref_src=twsrc%5Etfw)[@CarlosFGalan](https://twitter.com/CarlosFGalan?ref_src=twsrc%5Etfw) [\#BogotaParaLaGente](https://twitter.com/hashtag/BogotaParaLaGente?src=hash&ref_src=twsrc%5Etfw)[@HOLLMANMORRIS](https://twitter.com/HOLLMANMORRIS?ref_src=twsrc%5Etfw) [@ColombiaHumana\_](https://twitter.com/ColombiaHumana_?ref_src=twsrc%5Etfw) [@UP\_Colombia](https://twitter.com/UP_Colombia?ref_src=twsrc%5Etfw) [@MovimientoMAIS](https://twitter.com/MovimientoMAIS?ref_src=twsrc%5Etfw)
>
> — Elecciones Colombia 2019 regionales (@EncuestasColom8) [October 26, 2019](https://twitter.com/EncuestasColom8/status/1188153448266637314?ref_src=twsrc%5Etfw)

> ?[\#Encuesta](https://twitter.com/hashtag/Encuesta?src=hash&ref_src=twsrc%5Etfw) |Si las elecciones a la Alcaldía de [\#Medellin](https://twitter.com/hashtag/Medellin?src=hash&ref_src=twsrc%5Etfw) fueran hoy, por cuál de los candidatos votaria usted ?[@AlfredoRamosM](https://twitter.com/AlfredoRamosM?ref_src=twsrc%5Etfw) [@CeDemocratico](https://twitter.com/CeDemocratico?ref_src=twsrc%5Etfw)[@QuinteroCalle](https://twitter.com/QuinteroCalle?ref_src=twsrc%5Etfw) [@independienteco](https://twitter.com/independienteco?ref_src=twsrc%5Etfw)
>
> [@BeatrizRave](https://twitter.com/BeatrizRave?ref_src=twsrc%5Etfw) [@PartidoVerdeCoL](https://twitter.com/PartidoVerdeCoL?ref_src=twsrc%5Etfw)[\#SantiagoGomez](https://twitter.com/hashtag/SantiagoGomez?src=hash&ref_src=twsrc%5Etfw) [\#SeguimosAvanzando](https://twitter.com/hashtag/SeguimosAvanzando?src=hash&ref_src=twsrc%5Etfw)
>
> Vota Y COMPARTE.
>
> — Elecciones Colombia 2019 regionales (@EncuestasColom8) [October 26, 2019](https://twitter.com/EncuestasColom8/status/1188173237114605570?ref_src=twsrc%5Etfw)

> ?[\#Encuesta](https://twitter.com/hashtag/Encuesta?src=hash&ref_src=twsrc%5Etfw) |Si las elecciones a la Alcaldía de [\#Popayan](https://twitter.com/hashtag/Popayan?src=hash&ref_src=twsrc%5Etfw) fueran hoy, por cuál de los candidatos votaria usted ? [@AlegriaAlcalde](https://twitter.com/AlegriaAlcalde?ref_src=twsrc%5Etfw) [@soyconservador](https://twitter.com/soyconservador?ref_src=twsrc%5Etfw) [@JoaquiRosalba](https://twitter.com/JoaquiRosalba?ref_src=twsrc%5Etfw) [@partidodelaucol](https://twitter.com/partidodelaucol?ref_src=twsrc%5Etfw)  
> [@NINOASAMBLEA](https://twitter.com/NINOASAMBLEA?ref_src=twsrc%5Etfw) [@MovimientoAda](https://twitter.com/MovimientoAda?ref_src=twsrc%5Etfw) [@mariojbustamant](https://twitter.com/mariojbustamant?ref_src=twsrc%5Etfw) [@CeDemocratico](https://twitter.com/CeDemocratico?ref_src=twsrc%5Etfw)
>
> Vota Y COMPARTE.
>
> — Elecciones Colombia 2019 regionales (@EncuestasColom8) [October 24, 2019](https://twitter.com/EncuestasColom8/status/1187218081065639937?ref_src=twsrc%5Etfw)

> ?[\#Encuesta](https://twitter.com/hashtag/Encuesta?src=hash&ref_src=twsrc%5Etfw) |Si las elecciones al Concejo de [\#Bogota](https://twitter.com/hashtag/Bogota?src=hash&ref_src=twsrc%5Etfw) fueran hoy, por cuál de los candidatos votaria usted ?[@marxiabogota](https://twitter.com/marxiabogota?ref_src=twsrc%5Etfw) [@CeDemocratico](https://twitter.com/CeDemocratico?ref_src=twsrc%5Etfw) [@JulioCPulidoP](https://twitter.com/JulioCPulidoP?ref_src=twsrc%5Etfw) [@PartidoVerdeCoL](https://twitter.com/PartidoVerdeCoL?ref_src=twsrc%5Etfw) [@Luz0568](https://twitter.com/Luz0568?ref_src=twsrc%5Etfw) [@partidodelaucol](https://twitter.com/partidodelaucol?ref_src=twsrc%5Etfw)
>
> [@Daniel\_CadenaC](https://twitter.com/Daniel_CadenaC?ref_src=twsrc%5Etfw) [@PartidoMIRA](https://twitter.com/PartidoMIRA?ref_src=twsrc%5Etfw)
>
> Vota Y COMPARTE.
>
> — Elecciones Colombia 2019 regionales (@EncuestasColom8) [October 23, 2019](https://twitter.com/EncuestasColom8/status/1187098027707768833?ref_src=twsrc%5Etfw)

> ?[\#Encuesta](https://twitter.com/hashtag/Encuesta?src=hash&ref_src=twsrc%5Etfw) |Si las elecciones al Concejo de [\#Cali](https://twitter.com/hashtag/Cali?src=hash&ref_src=twsrc%5Etfw) fueran hoy, por cuál de los candidatos votaria usted ?[@RobertoAndrsVi1](https://twitter.com/RobertoAndrsVi1?ref_src=twsrc%5Etfw) [@ColombiaHumana\_](https://twitter.com/ColombiaHumana_?ref_src=twsrc%5Etfw) [@PedroPabloAri12](https://twitter.com/PedroPabloAri12?ref_src=twsrc%5Etfw) [@SoyRenaciente](https://twitter.com/SoyRenaciente?ref_src=twsrc%5Etfw) [@helmermina](https://twitter.com/helmermina?ref_src=twsrc%5Etfw) [@PartidoVerdeCoL](https://twitter.com/PartidoVerdeCoL?ref_src=twsrc%5Etfw)
>
> [@CardonaMagdieli](https://twitter.com/CardonaMagdieli?ref_src=twsrc%5Etfw)  
> [@PartidoMIRA](https://twitter.com/PartidoMIRA?ref_src=twsrc%5Etfw)
>
> Vota Y COMPARTE.
>
> — Elecciones Colombia 2019 regionales (@EncuestasColom8) [October 23, 2019](https://twitter.com/EncuestasColom8/status/1187097843208773632?ref_src=twsrc%5Etfw)

> ?[\#Encuesta](https://twitter.com/hashtag/Encuesta?src=hash&ref_src=twsrc%5Etfw) |Si las elecciones a la Alcaldía de [\#Soledad](https://twitter.com/hashtag/Soledad?src=hash&ref_src=twsrc%5Etfw) fueran hoy, por cuál de los candidatos votaria usted? [@rodolfo\_ucros](https://twitter.com/rodolfo_ucros?ref_src=twsrc%5Etfw) [@PartidoLiberal](https://twitter.com/PartidoLiberal?ref_src=twsrc%5Etfw) [@CeDemocratico](https://twitter.com/CeDemocratico?ref_src=twsrc%5Etfw) [@PartidoMIRA](https://twitter.com/PartidoMIRA?ref_src=twsrc%5Etfw)[@BeatrizBP2109](https://twitter.com/BeatrizBP2109?ref_src=twsrc%5Etfw) [@MovimientoMAIS](https://twitter.com/MovimientoMAIS?ref_src=twsrc%5Etfw) [@Williamtorresa](https://twitter.com/Williamtorresa?ref_src=twsrc%5Etfw) [@PCambioRadical](https://twitter.com/PCambioRadical?ref_src=twsrc%5Etfw) [@RafaelPalmera6](https://twitter.com/RafaelPalmera6?ref_src=twsrc%5Etfw) [@ColombiaHumana\_](https://twitter.com/ColombiaHumana_?ref_src=twsrc%5Etfw)
>
> — Elecciones Colombia 2019 regionales (@EncuestasColom8) [October 23, 2019](https://twitter.com/EncuestasColom8/status/1186975293459881984?ref_src=twsrc%5Etfw)

> ?[\#Encuesta](https://twitter.com/hashtag/Encuesta?src=hash&ref_src=twsrc%5Etfw) |Si las elecciones a la Alcaldía de [\#Tunja](https://twitter.com/hashtag/Tunja?src=hash&ref_src=twsrc%5Etfw) fueran hoy,por cuál de los candidatos votaría usted? [@FunemeAlejandro](https://twitter.com/FunemeAlejandro?ref_src=twsrc%5Etfw) [@soyconservador](https://twitter.com/soyconservador?ref_src=twsrc%5Etfw) [@PartidoLiberal](https://twitter.com/PartidoLiberal?ref_src=twsrc%5Etfw) [@John\_Carrero](https://twitter.com/John_Carrero?ref_src=twsrc%5Etfw) [@PartidoVerdeCoL](https://twitter.com/PartidoVerdeCoL?ref_src=twsrc%5Etfw) [@andersonrolamm](https://twitter.com/andersonrolamm?ref_src=twsrc%5Etfw) [@partidodelaucol](https://twitter.com/partidodelaucol?ref_src=twsrc%5Etfw)
>
> [\#JorgeMoreno](https://twitter.com/hashtag/JorgeMoreno?src=hash&ref_src=twsrc%5Etfw) [@MovimientoAda](https://twitter.com/MovimientoAda?ref_src=twsrc%5Etfw)
>
> Vota
>
> — Elecciones Colombia 2019 regionales (@EncuestasColom8) [October 23, 2019](https://twitter.com/EncuestasColom8/status/1186831257788923905?ref_src=twsrc%5Etfw)

> ?[\#Encuesta](https://twitter.com/hashtag/Encuesta?src=hash&ref_src=twsrc%5Etfw) |Si las elecciones a la Alcaldía de [\#Yopal](https://twitter.com/hashtag/Yopal?src=hash&ref_src=twsrc%5Etfw) fueran hoy,por cuál de los candidatos votaría usted? [@FerYopal](https://twitter.com/FerYopal?ref_src=twsrc%5Etfw) [@PartidoLiberal](https://twitter.com/PartidoLiberal?ref_src=twsrc%5Etfw) [@acuatroojos](https://twitter.com/acuatroojos?ref_src=twsrc%5Etfw) [@Partido\_PRE](https://twitter.com/Partido_PRE?ref_src=twsrc%5Etfw) [@LecYopal](https://twitter.com/LecYopal?ref_src=twsrc%5Etfw) [@PartidoASI\_](https://twitter.com/PartidoASI_?ref_src=twsrc%5Etfw)
>
> [\#JuanJoseSarmiento](https://twitter.com/hashtag/JuanJoseSarmiento?src=hash&ref_src=twsrc%5Etfw) [@Unafuerzaparae1](https://twitter.com/Unafuerzaparae1?ref_src=twsrc%5Etfw)
>
> Vota
>
> — Elecciones Colombia 2019 regionales (@EncuestasColom8) [October 22, 2019](https://twitter.com/EncuestasColom8/status/1186788777701847040?ref_src=twsrc%5Etfw)

> ?[\#Encuesta](https://twitter.com/hashtag/Encuesta?src=hash&ref_src=twsrc%5Etfw) |Si las elecciones a la Alcaldía de [\#Riohacha](https://twitter.com/hashtag/Riohacha?src=hash&ref_src=twsrc%5Etfw) fueran hoy, por cuál de los candidatos votaria usted ? [@blasquinterom](https://twitter.com/blasquinterom?ref_src=twsrc%5Etfw) [@soyconservador](https://twitter.com/soyconservador?ref_src=twsrc%5Etfw) [@joseramirobc](https://twitter.com/joseramirobc?ref_src=twsrc%5Etfw) [@PartidoAICO](https://twitter.com/PartidoAICO?ref_src=twsrc%5Etfw) [@PartidoLiberal](https://twitter.com/PartidoLiberal?ref_src=twsrc%5Etfw)  
> [@WilderNavarroQ](https://twitter.com/WilderNavarroQ?ref_src=twsrc%5Etfw) [@SoyRenaciente](https://twitter.com/SoyRenaciente?ref_src=twsrc%5Etfw) [@PartidoVerdeCoL](https://twitter.com/PartidoVerdeCoL?ref_src=twsrc%5Etfw) [@gcastillodaza](https://twitter.com/gcastillodaza?ref_src=twsrc%5Etfw) [\#RiohachaDecide](https://twitter.com/hashtag/RiohachaDecide?src=hash&ref_src=twsrc%5Etfw)
>
> — Elecciones Colombia 2019 regionales (@EncuestasColom8) [October 22, 2019](https://twitter.com/EncuestasColom8/status/1186454598258778115?ref_src=twsrc%5Etfw)

> ?[\#Encuesta](https://twitter.com/hashtag/Encuesta?src=hash&ref_src=twsrc%5Etfw) |Si las elecciones a la Alcaldía de [\#Chia](https://twitter.com/hashtag/Chia?src=hash&ref_src=twsrc%5Etfw) fueran hoy, por cuál de los candidatos votaria usted ?[@Alcalde\_Segura](https://twitter.com/Alcalde_Segura?ref_src=twsrc%5Etfw) [@soyconservador](https://twitter.com/soyconservador?ref_src=twsrc%5Etfw) [@FlordeMariaCely](https://twitter.com/FlordeMariaCely?ref_src=twsrc%5Etfw) [@MovimientoAda](https://twitter.com/MovimientoAda?ref_src=twsrc%5Etfw)
>
> [@Ignaciocorreal2](https://twitter.com/Ignaciocorreal2?ref_src=twsrc%5Etfw) [@ColJustaLibres](https://twitter.com/ColJustaLibres?ref_src=twsrc%5Etfw) [@up\_chia](https://twitter.com/up_chia?ref_src=twsrc%5Etfw) [@ColombiaHumana\_](https://twitter.com/ColombiaHumana_?ref_src=twsrc%5Etfw) [@UP\_Colombia](https://twitter.com/UP_Colombia?ref_src=twsrc%5Etfw)
>
> Vota Y COMPARTE.
>
> — Elecciones Colombia 2019 regionales (@EncuestasColom8) [October 21, 2019](https://twitter.com/EncuestasColom8/status/1186282164356296707?ref_src=twsrc%5Etfw)

> ?[\#Encuesta](https://twitter.com/hashtag/Encuesta?src=hash&ref_src=twsrc%5Etfw) |Si las elecciones a la Alcaldía de [\#Valledupar](https://twitter.com/hashtag/Valledupar?src=hash&ref_src=twsrc%5Etfw) fueran hoy, por cuál de los candidatos votaria usted ? Grupo 1[@EVELIODAZA](https://twitter.com/EVELIODAZA?ref_src=twsrc%5Etfw) [@CeDemocratico](https://twitter.com/CeDemocratico?ref_src=twsrc%5Etfw) [@AlainJimenezFad](https://twitter.com/AlainJimenezFad?ref_src=twsrc%5Etfw) [@ColombiaHumana\_](https://twitter.com/ColombiaHumana_?ref_src=twsrc%5Etfw) [@PoloDemocratico](https://twitter.com/PoloDemocratico?ref_src=twsrc%5Etfw) [@compromisociu](https://twitter.com/compromisociu?ref_src=twsrc%5Etfw) [@MiguelM2019](https://twitter.com/MiguelM2019?ref_src=twsrc%5Etfw) [@PartidoAICO](https://twitter.com/PartidoAICO?ref_src=twsrc%5Etfw)
>
> Vota Y COMPARTE.
>
> — Elecciones Colombia 2019 regionales (@EncuestasColom8) [October 20, 2019](https://twitter.com/EncuestasColom8/status/1185951482421694468?ref_src=twsrc%5Etfw)

> Felicitaciones a los bogotanos!!!  
> Ganó Claudia López.  
> Ganó el mejor programa y la mejor candidata.  
> El fin de la pesadilla peñalosista.  
> Vencidos los mismos con las mismas.
>
> — Jorge Robledo (@JERobledo) [October 27, 2019](https://twitter.com/JERobledo/status/1188583071362224128?ref_src=twsrc%5Etfw)

> En Bogotá gana Claudia Lopez. No nos representa. El movimiento Colombia Humana será independiente a su gobierno.
>
> Felicito a Morris por mantener vivas las banderas de la Bogotá Humana, pero ya Bogotá decidió destruir el proyecto del metro subterráneo para la ciudad.
>
> — Gustavo Petro (@petrogustavo) [October 27, 2019](https://twitter.com/petrogustavo/status/1188585008027578370?ref_src=twsrc%5Etfw)

> Derrota de Miguel Uribe: Apaleados los partidos tradicionales en Bogotá <https://t.co/0vi8c52iBC>
>
> — Las2orillas (@Las2Orillas) [October 27, 2019](https://twitter.com/Las2Orillas/status/1188577406182985729?ref_src=twsrc%5Etfw)

### A LA HORA DE VOTAR TENGA EN CUENTA

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/1470399662_Marketing.png){width="128" height="128"}

</figure>
### Propuestas

Antes de ir a las urnas, conozca las propuestas de todos los candidatos, no se deje influenciar por lo que dicen las encuestas.

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/1470399671_SEO.png){width="128" height="128"}

</figure>
### No venda el voto

Piense que su voto tiene un sentido más grande, que el costo de un tamal, un plato de lechona o un mercado.

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/1470399715_E-Commerce.png){width="128" height="128"}

</figure>
### Pilas a la financiación de las campañas

Bien dicen que el candidato que paga para llegar, llega para pagar a sus acreedores. Un candidato con grandes deudas en su campaña no es garantía de transparencia en la gestión pública.

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/1470399674_App_Development.png){width="128" height="128"}

</figure>
### No todo son redes sociales

En las redes sociales usted puede encontrar verdades y mentiras, puede encontrar pauta y campañas de desprestigio. Que su voto no se rija por esto.

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/1470399667_Newsletter.png){width="128" height="128"}

</figure>
### No sea un idiota útil

Millones de mensajes se envían al día por WhatsApp, no reproduzca información que no esté confirmada.

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/1470399656_Branding.png){width="128" height="128"}

</figure>
### En el puesto de votación

Para votar solo necesitará su cédula de ciudadanía, debe registrarse, marcar una sola casilla (a menos que quiera que su voto sea nulo) y depositarlo en el lugar indicado. Su voto es secreto, y nadie debe tomar fotos del mismo, ni presionarlo para ejercer este derecho.

NOTICIAS RELACIONADAS
---------------------

[![Tres elementos claves para garantizar unas elecciones transparentes](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Elecciones-MOE-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/tres-elementos-claves-para-garantizar-unas-elecciones-transparentes/)  

#### [Tres elementos claves para garantizar unas elecciones transparentes](https://archivo.contagioradio.com/tres-elementos-claves-para-garantizar-unas-elecciones-transparentes/)

Posted by [Contagioradio](https://archivo.contagioradio.com/author/ctgadm/)[<time datetime="2019-10-21T17:36:33-05:00" title="2019-10-21T17:36:33-05:00">octubre 21, 2019</time>](https://archivo.contagioradio.com/2019/10/21/)Para estas elecciones es necesario prestar una especial atención a regiones como el norte de Antioquia, Chocó, la Costa Atlántica…[Leer más](https://archivo.contagioradio.com/tres-elementos-claves-para-garantizar-unas-elecciones-transparentes/)  
[![Atentan contra sedes del Partido Comunista y Partido FARC a pocas semanas de elecciones](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Partido-Comunista-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/atentan-contra-sedes-del-partido-comunista-y-partido-farc-a-pocas-semanas-de-elecciones/)  

#### [Atentan contra sedes del Partido Comunista y Partido FARC a pocas semanas de elecciones](https://archivo.contagioradio.com/atentan-contra-sedes-del-partido-comunista-y-partido-farc-a-pocas-semanas-de-elecciones/)

Posted by [Contagioradio](https://archivo.contagioradio.com/author/ctgadm/)[<time datetime="2019-10-11T10:03:07-05:00" title="2019-10-11T10:03:07-05:00">octubre 11, 2019</time>](https://archivo.contagioradio.com/2019/10/11/)Mediante disparos y bomba incendiaria, las sedes del Partido Comunista, Unión Patriótica y FARC fueron atacadas durante la madrugada.[Leer más](https://archivo.contagioradio.com/atentan-contra-sedes-del-partido-comunista-y-partido-farc-a-pocas-semanas-de-elecciones/)  
[![Violencia política contra Juntas de Acción Comunal aumentaría al acercarse elecciones regionales](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Juntas-de-Acción-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/violencia-politica-contra-juntas-de-accion-comunal-aumentaria-al-acercarse-elecciones-regionales/)  

#### [Violencia política contra Juntas de Acción Comunal aumentaría al acercarse elecciones regionales](https://archivo.contagioradio.com/violencia-politica-contra-juntas-de-accion-comunal-aumentaria-al-acercarse-elecciones-regionales/)

Posted by [Contagioradio](https://archivo.contagioradio.com/author/ctgadm/)[<time datetime="2019-06-14T18:26:41-05:00" title="2019-06-14T18:26:41-05:00">junio 14, 2019</time>](https://archivo.contagioradio.com/2019/06/14/)La violencia política está centrada en «personas que ejercen actividades de representación y construcción de poder al interior de las…[Leer más](https://archivo.contagioradio.com/violencia-politica-contra-juntas-de-accion-comunal-aumentaria-al-acercarse-elecciones-regionales/)
