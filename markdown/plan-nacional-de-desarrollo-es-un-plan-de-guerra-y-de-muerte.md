Title: "Plan Nacional de Desarrollo es un plan de guerra y de muerte"
Date: 2015-02-23 17:50
Author: CtgAdm
Category: Economía, Otra Mirada
Tags: alirio uribe, Angela Maria Robledo, Derecho a la eduación, Derecho a la salud, paz, Plan Nacional de Desarrollo, PND, Presupuesto para defensa en Colombia
Slug: plan-nacional-de-desarrollo-es-un-plan-de-guerra-y-de-muerte
Status: published

###### Foto: Contagio Radio 

Distintos analistas políticos y líderes sociales reunidos en la Audiencia Pública "Plan Nacional de Desarrollo 2014-2018, Derechos sociales y postconflicto", coincidieron en afirmar que **lejos está este, de ser un plan para la construcción de la "Paz, la equidad, y la educación"**, y que si bien, la retórica expuesta en la introducción tiene un carácter progresista, en la lectura del articulado puede notarse que **"es más de lo mismo"**: reformas re-encauchadas que el gobierno de Juan Manuel Santos no logró incorporar durante su primer mandato.

El **Plan Nacional de Desarrollo** pretende determinar la inversión y gasto presupuestal del Estado para 4 años de gobierno. Para el segundo mandato del presidente Santos, el PND 2014-2018 se ha denominado "Todos por un nuevo país", y plantea como objetivo "construir una Colombia en paz, equitativa y educada".

El articulado es, para el profesor Cesar Giraldo, una colcha de retazos de **leyes que intentan incorporar reformas profundas**, dando facultades reglamentarias al Gobierno Nacional, violando el control de instancias constitucionales. El ultimo articulo propuesto en el PND, por ejemplo, elimina 85 normas existentes. **"El documento de 700 páginas no tiene nada que ver con el borrador presentado en noviembre"**, y en ese sentido, no hubo un proceso de construcción democrática, sino "un ejercicio de mímica para que discutiéramos sobre algo que no es", señaló Giraldo.

La columna vertebral del **PND presentado por el gobierno no es la "paz, equidad y educación", sino "infraestructura, minero-energéticos y agro-industria"** , y sus principios financieros se sustentan en una concepción en que "el Estado pone el presupuesto en cadenas de valor privado, consciente de que no recibirá nada a cambio, casi a manera de subsidios", señaló por su parte el director de Indepaz, Camilo Gonzalez.

El PND no se prepara para la construcción de la paz en Colombia. Si bien, el gobierno asegura que la terminación del conflicto permitirá un crecimiento del PIB de cerca de 4 puntos, esto no se ve reflejado en la inversión social, aseguró Jennifer Mojíca, de la Comisión Colombiana de Juristas. Tampoco tiene en cuenta la inversión que requieren los puntos de acuerdo a los que se está llegando en La Habana; muy por el contrario, trata temas de debate en la Mesa de Diálogos, como la Tierra, Cultivos y Víctimas, pero para profundizar políticas que generan malestar al interior de la sociedad: **la entrega de valdíos a empresarios, fumigación de cultivos, y mantenimiento de la Ley de Víctimas**.

Tampoco presenta una buena situación para sectores como "educación" y "salud", ya que en los artículos en que se mencionan **reviven proyectos derrotados por la movilización social, como la financiación a la educación mediante el crédito.** El único rubro de inversión que aumenta en un **54% es el de Seguridad y Defensa, siendo 3 veces la inversión en educación, 6.5 veces la inversión en salud, y 100 veces la inversión en política pública ambiental.**

Así entonces, para los y las diferentes analistas, "el PND es un plan de guerra y de muerte dirigido a opacar la inversión social en los sectores sociales de la población", y no un plan para la paz.

Los representantes a la Cámara convocantes a la audiencia pública, **Alirio Uribe y Ángela María Robledo**, así como los y las líderes sociales e investigadores participantes, realizaron un llamado al conjunto de la sociedad colombiana para movilizarse y exigir al gobierno un real proceso de participación en la construcción y definición de lo que debería ser un Plan Nacional de Desarrollo para la paz.

\[embed\]https://www.youtube.com/watch?v=atSXdxxMf2c&feature=youtu.be \[/embed\]  
\[embed\]https://www.youtube.com/watch?v=-q0fB5\_v4P8&feature=youtu.be\[/embed\]
