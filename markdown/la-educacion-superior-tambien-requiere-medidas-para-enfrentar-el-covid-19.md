Title: La educación superior también requiere medidas para enfrentar el COVID 19
Date: 2020-05-07 11:10
Author: AdminContagio
Category: Actualidad, Educación
Tags: Matricula Cero, Universidades
Slug: la-educacion-superior-tambien-requiere-medidas-para-enfrentar-el-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Estudiantes-piden-suspender-semestres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Agencia de Noticias UN {#foto-agencia-de-noticias-un .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Con la decisión tomada por el presidente Duque de extender hasta la última semana de mayo el aislamiento obligatorio, algunas Instituciones de Educación Superior (IES) empezaron a plantearse la idea culminar este semestre de manera virtual, y continuar con esta modalidad hasta el fin del año. Pero la decisión no sería la única difícil que tendrían que tomar actores que están en el sector de la educación superior.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### \#MatriculaCero para educación superior estatal

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cristian Reyes, vocero nacional de la Unión Nacional de Estudiantes de Educación Superior (UNEES), recuerda que debido al paro cívico y estudiantil del año pasado y la modificación de los calendarios académicos, algunas instituciones apenas estaban reanudando semestres cuando se decretó el aislamiento obligatorio, como ocurrió en la Universidad Nacional. Otras, como la Universidad Distrital estaban terminando.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esa coyuntura, todas las IES se vieron en la obligación de suspender semestres o continuarlos de manera virtual, pero luego de casi dos meses, se han evidenciado los problemas en términos de acceso para los y las estudiantes: en primer lugar, el acceso a recursos como internet, dispositivos con conexión para navegar y acceso a energía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y en segundo lugar, la garantía al derecho mismo a la educación, pues temen que la situación económica del país obligue a los hogares a elegir entre pagar la matrícula del siguiente semestre o suplir sus necesidades básicas. (Le puede interesar: ["Eduación virtual por Covid-19, una decisión para la que no estamos preparados"](https://archivo.contagioradio.com/eduacion-virtual-por-covid-19-una-decision-para-la-que-no-estamos-preparados/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, en redes sociales se hizo viral \#MatriculaCero; una protesta de estudiantes de IES estatales para que la matrícula del siguiente semestre no tenga costo. Por otra parte, Reyes señala que también se debería pedir a las IES privadas que brinden soluciones como ya lo han hecho algunas instituciones mediante descuentos o distintos apoyos al estudiante, en lugar de dejar en poder del sistema financiero la posibilidad de estudiar.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las Universidades alertan por deserción de más del 50%

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las universidades no son ajenas al problema, algunas estatales han pedido adiciones presupuestales para cubrir los gastos extra que significará hacer el tránsito a la virtualidad y apoyar a los estudiantes para evitar la deserción y otras privadas, agrupadas en la Asociación Colombiana de Universidades (ASCUN) han pedido apoyos financieros para cubrir sus gastos de operación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las Universidades han señalado que la deserción estudiantil se elevaría más allá del 50%, y dado que la mayoría de sus ingresos provienen de las matrículas, piden apoyo al Gobierno para salvar el sector. (Le puede interesar: ["Al Ministerio de Educación se le salieron de las manos las universidades privadas"](https://archivo.contagioradio.com/ministerio-educacion-universidad-privada/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Oportunidades en medio de la crisis?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Otro tema relevante tiene que ver con los nuevos ingresos a las IES que se darán el próximo semestre y el primero de 2021, pues los jóvenes tendrán que presentar la prueba Saber, que dadas las actuales condiciones aún no se tiene claridad sobre cuándo y cómo se realizará. Tomando en cuenta que los resultados de la prueba son bien una barrera o una puerta de ingreso a la educación superior, Reyes reclama que se podría pensar en modificar el acceso a universidades estatales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ejemplo, en el caso de la Universidad Distrital, se podría pensar que la Alcaldía priorice el ingreso de personas con bajos recursos para garantizar que, por esta vez, aquellos que tienen vulnerabilidad económica puedan ingresar a la educación superior. Sin embargo, afirma que este tipo de protestas esperan que se puedan presentar en una mesa de urgencia que se establezca con el Gobierno para encontrar una salida a la crisis.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
