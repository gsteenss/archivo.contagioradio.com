Title: El Teatro Libre celebra 45 años con obras para toda la familia
Date: 2018-04-09 12:52
Category: Cultura
Tags: Bogotá, teatro, teatro libre
Slug: teatro-libre-anivesario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Canterville-Foto-@Sr.Mao-11_preview.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @Sr.Mao 

###### 06 Abr 2018 

Con 3 estrenos y un remontaje, el grupo del Teatro Libre conmemora 45 años de existencia enfocándose particularmente en el público infantil y familiar. Una temporada que incluye además algunas de las obras que hacen parte del repertorio de la agrupación fundada en 1973 sobre la base del grupo teatral de la Universidad de los Andes.

Iniciando el año, la compañía presentó su montaje de la obra “El fantasma de Canterville”, una adaptación libre del cuento de Óscar Wilde, bajo la dirección de  Julie Cuéllar y Miguel Diago y adaptada por Julie Cuéllar y Patricia Jaramillo. Temporada que terminó el pasado primero de abril en la sede del centro.

El segundo estreno es "La evitable ascensión de Arturo Ui", adaptación del director Diego Barragán que se encuentra en temporada desde el 5 e irá hasta el 21 de abril  y la obra del colombiano Juan Diego Arias "Historia de Navidad" que llegará al teatro del 27 de septiembre al 3 de noviembre, dirigida por Ricardo Camacho.

En cuanto al remontaje, el grupo presentará "El idiota" de Feodor Dostoievski en versión de Ricardo Camacho, con lo que la agrupación continua sobre su filosofía de montar obras del repertorio universal, de los principales autores de la literatura occidental y de jóvenes dramaturgos colombianos.

A las temporadas de estas obras se suman algunas del repertorio de la agrupación "Crimen y castigo" (del 5 al 21 de abril), "Los hermanos Karamazov" (del 7 de junio al 14 de julio) ambas versiones de las novelas de Fedor Dostoievski dirigidas por Ricardo Camacho y "Romeo y Julieta" (del 8 de noviembre al 15 de diciembre) de William Shakespeare en la versión y dirección de Diego Barragán.

Las funciones tendrán lugra en la sala del Teatro Libre Sede Centro, ubicada en la calle 12B \# 2-44. El costo de la boleta es de \$30.000 y los días viernes con promoción de 2x1. Aplican descuentos del 30% estudiantes, tercera edad y personas en situación de discapacidad. Para mayor información consulte la [página web](http://teatrolibre.com) del Teatro
