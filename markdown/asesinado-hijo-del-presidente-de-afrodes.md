Title: Asesinado hijo del presidente de AFRODES
Date: 2016-10-25 11:58
Category: DDHH, Nacional
Tags: AFRODESC, afrodescendientes, asesinato, Marino Córdoba, paz, Riosucio
Slug: asesinado-hijo-del-presidente-de-afrodes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Marino-e1477412901599.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Afro Bogotano 

###### 24 de Oct. 2016 

**Luego de varias amenazas e intimidaciones hechas a la organización AFRODES** – Asociación Nacional de Afrocolombianos Desplazados- y a su presidente el señor Marino Córdoba, **el pasado 24 de octubre se conoció del asesinato del hijo de este líder,** el joven Wilmar Córdoba.

**Wilmar, tenía 21 años** **y** al igual que su padre **hacia parte del proceso de AFRODES.** Desde allí se construyen propuestas diferenciadas para atender el impacto que ha dejado el desplazamiento y el conflicto armado en las Comunidades Afrocolombianas víctimas.

**Marino Córdoba junto con su familia, fueron desplazados el 20 de diciembre de 1996 por los paramilitares que se tomaron Riosucio (Chocó), en el marco de la “Operación Génesis”.** Para la época, Wilmar tenía tan solo un año y sumado a las inclemencias del conflicto armado, tuvo que vivir también el exilio en Estados Unidos, donde debieron pasar varios años.

**AFRODES**, que reúne  108 organizaciones de base conformadas por afrocolombianos, **repudió el asesinato de Willmar.** Aseguraron además, a través de un comunicado que “este hecho materializa las amenazas que han venido sucediendo en contra de la organización. Acabando con el sueño de nuestra juventud, sembrando dolor en madres, padres, familias y comunidades en general”. Le puede interesar: [Comunidades negras se movilizan por reiterados incumplimientos del gobierno](https://archivo.contagioradio.com/comunidades-negras-se-movilizan-por-reiterados-incumplimientos-del-gobierno/)

Así mismo, **instaron a las autoridades competentes a entregar resultados concretos con base en la investigación que se abra y que se dé con la captura de los autores materiales e intelectuales de este lamentable hecho. **Le puede interesar: [Incremento y reorganización del paramilitarismo alerta a comunidades](https://archivo.contagioradio.com/incremento-y-reorganizacion-del-paramilitarismo-alerta-a-comunidades/)

**AFRODES en la CIDH**

**En el año 2013,** una delegación que fue encabezada por **Marino Córdoba Berrio, dio a conocer a la Comisión Interamericana de Derechos Humanos diversos casos de líderes que habían sido amenazados**, y que a la hasta esa fecha no contaban con medidas de protección por parte del gobierno Colombiano. Le puede interesar: [Comisión Étnica se ofrece como mediadora para reanudar negociación Gobierno-Eln](https://archivo.contagioradio.com/?s=comision+etnica)

[Asesinato Del Hijo de Marino Cordoba Berrio](https://www.scribd.com/document/328897457/Asesinato-Del-Hijo-de-Marino-Cordoba-Berrio#from_embed "View Asesinato Del Hijo de Marino Cordoba Berrio on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_89245" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/328897457/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-mSmumRXASBLnhe3JztLX&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="ssba ssba-wrap">

</div>
