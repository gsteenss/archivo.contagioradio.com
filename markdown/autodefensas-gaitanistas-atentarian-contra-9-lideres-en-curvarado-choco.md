Title: Autodefensas Gaitanistas atentarían contra 9 líderes en Curvaradó, Chocó
Date: 2015-10-13 13:06
Category: Nacional, Paz
Tags: Amenazas de muerte en Curvaradó, Atentados contra líderes en Curvaradó, Autodefensas Gaitanistas, Autodefensas Gaitanistas de Colombia, Banacol, Comisión Intereclesial de Justicia y Paz, Consejo Municipal Carmen del Darién, Del Monte, Hermanos Castaño, Luis Felipe Molano, Palma aceitera, Paramilitarismo, Raúl Palacio, Uniban
Slug: autodefensas-gaitanistas-atentarian-contra-9-lideres-en-curvarado-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Autodefensas-gaitanistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: www.puzlo.com] 

###### [13 Oct 2015] 

[De acuerdo con la Comisión Intereclesial de Justicia y Paz, **paramilitares de las Autodefensas Gaitanistas planean atentados** contra la vida de la lideresa Ledis Tuirán y los líderes Eladio Cordero, Guillermo Díaz, Francisco Pérez, Eliodoro Hernández, Andrés Lance, Alfonso Falla, Julio Gómez, Francisco Gómez y Adrián Pérez de los consejos comunitarios de Curvaradó, en el Bajo Atrato chocoano.]

[Según la organización de defensa de los DDHH, una fuente vinculada a este grupo paramilitar hizo llegar la información y asegura que los paramilitares con el alias de "Andrés" y el "Paisita" dieron la **orden de asesinar a los 9 reclamantes** "Cargárselos, a penas den papaya" (...) **"***Que no se dejen ver esos hijueputas, no saben lo que les va a pasar***"**.]

[Esta fuente agregó que **para la mañana del 25 de septiembre, 5 paramilitares tenían planeado atentar con granadas** contra Ledis Tuirán y Andrés Lance, quienes frustraron el atentado al cambiar sus planes de trabajo para este día por la presencia de entes gubernamentales.]

[Los atentados contra las vidas de estos pobladores de Zonas Humanitarias y de Biodiversidad, se registran tras la **verificación de los predios usurpados por paramilitares desde 1996**, que adelantaron estos líderes el pasado 5 de septiembre con el acompañamiento de organizaciones nacionales e internacionales.]

[La comisión de verificación concluyó que **estos territorios continúan siendo explotados por repobladores de la extinta Casa de los hermanos Castaño Gil y de Luis Felipe Molano**, militar retirado vinculado con ACORE y cuya producción extensiva de palma aceitera y plátano es entregada a las empresas privadas Banacol, Uniban y Del Monte.]

[Ante las amenazas y atentados contra sus vidas estos líderes se han visto obligados a permanecer en sus comunidades sin posibilidad de trasladarse, incluyendo a sus sitios de trabajo. **Algunos de ellos aspiran al Consejo Municipal de Carmen del Darién** y apoyan la **candidatura a la alcaldía de Raúl Palacio, líder amenazado de muerte**.]
