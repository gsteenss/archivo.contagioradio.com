Title: Luto en Valle del Cauca por asesinato de Florelia Canas y Héctor Montoya
Date: 2018-08-28 13:04
Category: DDHH, Nacional
Tags: Florelia Canas, Héctor Montoya, lideres sociales, Valle del Cauca
Slug: luto-asesinato-florelia-canas-hector-montoya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo contagio Radio] 

###### [28 Ago 2018] 

El país lamenta la pérdicda de dos líderes sociales, se trata de la indígena **Florelia Canas,** asesinada el pasado viernes 24 de agosto en el municipio de Dagua, Vallle del Cauca; y del líder **Héctor Montoya,** asesinado el lunes 27 de agosto en la vía que conduce de Florencia a Morelia en Caqueta, lugar al cual había tenido que huir tras las amenazas sufridas en su natal, Cartago, Valle del Cauca.

Según la Organización Regional Indígena de Valle del Cauca, Florelia Canas fue fundadora del **cabildo indígena Nasa "Nuevo Despertar"**, en el municipio de Dagua. De acuerdo con el reporte, la líder fue asesinada por hombres que le dispararon hasta causarle la muerte en el municipio de **El Tambo, Cauca.** (Le puede interesar: ["Bajo constante amenaza viven líderes sociales en Cauca"](https://archivo.contagioradio.com/bajo-amenaza-viven-lideres-en-cauca/))

Por su parte, Héctor Montoya fue el creador de la **Fundación "Nuevo Amanecer"**, en Cartago, pero por amenazas recibidas en su contra, tuvo que abandonar la ciudad y radicarse en una finca de su propiedad en Caquetá, donde se desempeñaba como coordinador de la organización en ese departamento.

A pesar de las constantes denuncias de amenazas en su contra, Montoya fue asesinado por sicarios que lo abordaron mientras él y su esposa se desplazaban en su vehículo por la vía Florencia-Morelia. Aunque ambos fueron trasladados al hospital María Inmaculada de la capital del Departamento, los médicos no pudieron salvar sus vidas. (Le puede interesar: ["En últimos meses 20 mujeres han sido asesinadas en Caquetá"](https://archivo.contagioradio.com/en-ultimos-meses-20-mujeres-han-sido-asesinadas-en-caqueta/))

**343 líderes han sido asesinados en Colombia entre 2016 y 2018**

Según cifras de la Defensoría del Pueblo, hasta hace menos de una semana, desde 2016 habían sido asesinados **343 denfensores y defensoras de derechos humanos en el país.** La cifra fue presentada por el titular de la entidad, **Carlos Negret** en el acto que culminó con la firma del **Pacto por la Vida,** en el que participaron el presidente **Iván Duque, el Procurador general y la Vicefiscal general de la nación. **(Le puede interesar: ["Pacto por la vida se firmó excluyendo a líderes que la tienen en riesgo"](https://archivo.contagioradio.com/pacto-vida-excluyendo-lideres/))

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
