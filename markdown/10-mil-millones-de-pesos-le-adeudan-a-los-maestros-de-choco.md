Title: 10 mil millones de pesos le adeudan a los maestros de Chocó
Date: 2018-01-16 14:35
Category: Educación, Nacional
Tags: maestros Chocó, Ministerio de Educación, Ministerio de hacienda, paro de maestros Chocó
Slug: 10-mil-millones-de-pesos-le-adeudan-a-los-maestros-de-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/maestros-chocó-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Murcy] 

###### [16 Ene 2018] 

Por 10 mil millones de pesos que le adeudan a los maestros en Chocó con relación a **cesantías, primas y pagos a terceros**, más de 4 mil maestros se encuentran en paro y cese de actividades. El jueves se reunirán con el Ministro de Hacienda para solicitarle un crédito por 10 mil millones que es el valor de la deuda.

De acuerdo con Dilon Martínez, integrante de la Unión de Maestros del Chocó, “la administración de educación del departamento venía estando intervenida por el Ministerio Educación desde el 2009 y el Ministerio de Hacienda ha dicho que las razones que dieron origen a esa intervención **ya desaparecieron**”. Esto teniendo en cuenta el déficit fiscal que tiene el Chocó para prestar los servicios de educación.

Martínez reiteró que aún **faltan 10 mil millones de pesos por ser cancelados** en deudas laborales. Por esto, han estado presionando a las instituciones encargadas a nivel nacional y departamental manifestando que se deben identificar las fuentes de des financiación de los mecanismos de pago para que se paguen los salarios que están retrasado. (Le puede interesar: ["Chocó continúa sin saber qué es la paz"](https://archivo.contagioradio.com/el-choco-continua-sin-saber-que-es-la-paz/))

### **Deudas al sector de la educación alcanzan los 10 mil millones de pesos** 

Los maestros han manifestado que las deudas iniciales desde 2004 alcanzaban los **122 mil millones de pesos**, sin embargo, tras un proceso de depuración quedaron en 10 mil millones. “El Ministerio de Hacienda considera que esos avances son suficientes para que el Departamento asuma el pago de ese dinero y la educación pueda fluir”, argumentó.

Sin embargo, reiteró que **hay una debilidad del sistema financiero** en el departamento por lo que hay una amenaza constante contra los recursos que se destinan a la educación. Reiteró que estas deudas están relacionadas con el pago de cesantías, pagos a terceros, primas navideñas y vacaciones de los docentes, entre otros conceptos.

### **No hay consenso en la forma de sanear el déficit fiscal de la educación en Chocó** 

A los incumplimientos en los pagos se suman las deudas que existen de **195 procesos judiciales** que están en los juzgados de demandas contra el sistema educativo. Martínez explica que algunas demandas se han hecho contra el fondo de prestaciones sociales y por moratorias con las cesantías. (Le puede interesar: ["Maestros y maestras ¿un paro de tradición?"](https://archivo.contagioradio.com/maestros-y-maestras-un-paro-de-tradicion/))

Con esto en mente, el jueves 18 de enero, **los maestros se reunirán en Bogotá** con personas del Ministerio de Hacienda, del Ministerio de Educación, Gobernadores y Comités para tratar de hallar una salida. Por parte de ellos, le propondrán al Ministro de Hacienda que se haga un crédito por 10 mil millones y así sanear la parte que le corresponde al sector de la educación.

<iframe id="audio_23183026" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23183026_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
