Title: 99 mil millones de pesos gastó Peñalosa en publicidad entre 2016 y 2017
Date: 2018-09-20 15:40
Author: AdminContagio
Category: Nacional, Política
Tags: Enrique Peñalosa, FLIP, Pauta
Slug: 99-mil-millones-penalosa-publicidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Enrique-Peñalosa-e1461273574548.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Lafarge Holcim Foundation] 

###### [20 Sep 2018] 

Según denuncia de la **Fundación para la Libertad de Prensa (FLIP)**, la alcaldía de **Bogotá gastó entre 2016 y 2017, \$98.684.549.134** en pauta publicitaria; para **Jonathan Bock,** coordinador del Centro de Estudios de la FLIP, este gasto acarrea dos problemas: La transparencia sobre los contratos de propaganda, y los efectos que dichos contratos tienen en las audiencias que consumen los medios involucrados. (Le puede interesar: ["El metro elevado del Alcalde es el mejor ejemplo de corrupción sistémica: Hollman Morris")](https://archivo.contagioradio.com/metro-elevado-corrupcion-sistemica/)

### **Hay mucha opacidad en los contratos de la Alcaldía** 

En su estudio Pauta Visible, la FLIP descubrió que el 85% de los contratos se adjudicaron de forma directa a **RCN televisión, El Tiempo y el Canal Capital;** mientras los 11 restantes fueron agencias de publicidad o centrales de medios. Según la denuncia, la Alcaldía convirtió la **Empresa de Telecomunicaciones de Bogotá (ETB)** en su central de medios predilecta.

Gracias a la investigación, se descubrió que la ETB ejecutó varios contratos que sumados superan los 46 mil millones de pesos. Pero dado el carácter mixto de la empresa (publico-privada), es difícil acceder a información detallada de los contratos mediante derechos de petición y otros recursos; sin embargo, la Fundación logró saber que Caracol Televisión, **El Tiempo, RCN Televisión, Caracol Radio y RCN Radio recibieron \$19.021.921.367 por estos contratos.**

Bock sostuvo que la modalidad con la que se está contratando la Pauta es el **'Content Marketing',** lo que significa que la Alcaldía acuerda con los medios que la publicidad que pagan aparezca como parte de los contenidos que producen, haciendo que los consumidores no puedan distinguir lo que es pauta de aquello que es información. (Le puede interesar: ["Alcalde no puede intervenir la Van der Hammen"](https://archivo.contagioradio.com/penalosa-intervencion-van-der-hammen/))

### **Peñalosa está usando la pauta como un mecanismo de censura indirecta** 

Si bien el investigador señaló que la publicidad oficial es importante para entidades públicas y para los medios, la forma en que se está manejando crea un ambiente ficticio de favorabilidad hacía el Alcalde y su gobierno; paradójicamente, Bock sostuvo que este modelo de pauta también afecta a los medios, porque si llegasen a publicar información contraria al Distrito, podrían perder los contratos que tienen con la Alcaldía, y por ende una fuente importante de dinero.

La FLIP además listo algunas de las obras que la Alcaldía podría haber hecho con los casi **100 mil millones de pesos gastados en publicidad**, entre ellas, "5 mega colegios como el de La Felicidad en Fontibón, o 30 jardines infantiles como el Travesuras de Colores en Rafael Uribe Uribe". Según la fundación, el Distrito gastó en promedio 135 millones de pesos diarios en pauta oficial, dinero que habría podido ser utilizado para la construcción de, al menos, **4 proyectos de Vivienda de Interés Prioritario iguales a la "Plaza de la Hoja", cuyo costó se calculó en 22 mil millones de pesos.**

### **Serán los ciudadanos los que decidirán el tipo de información que consumen  ** 

A pesar de que en Colombia hay un marco legal sobre la publicidad oficial, Bock aseguró que se trata de unos principios básicos que deben profundizarse, como se está haciendo en Uruguay; e invitó a visitar la [página](http://pautavisible.org/) del proyecto, en la que es posible encontrar más de 1.500 contratos celebrados por 50 entidades públicas del país. Bock también anunció que están preparando nuevas investigaciones sobre las Alcaldías de Barranquilla y Medellín, así como la Gobernación del Atlántico.

Las investigaciones rastrearán al top 3 de entidades públicas que más pautan: Alcaldía de Bogotá, **Alcaldía de Barranquilla (\$68.628.813.379) y la Alcaldía de Medellin (\$21.792.727.937).**  El analista concluyó que la investigación sería útil para hacer veeduría sobre los contratos y los gastos de los entes territoriales, pero **será la ciudadanía, mediante su consumo, la que decida sobre qué tipos de medios e información consumen.** (Le puede interesar: ["Cabildo indígena Muisca asegura que Van der Hammen es territorio sagrado"](https://archivo.contagioradio.com/van-der-hammen-terreno-indigena/))

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio](http://bit.ly/1ICYhVU)]
