Title: Se firma el acuerdo sobre cese al fuego bilateral
Date: 2016-06-22 09:48
Category: Nacional, Paz
Tags: FARC, Gobierno, La Habana, proceso de paz
Slug: manana-se-firmara-acuerdo-sobre-cese-al-fuego-bilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Delegaciones-paz_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Presidencia 

###### [22 Jun 2016]

[Desde la Habana se anunció hoy que el acuerdo sobre cese al fuego bilateral ya está listo. Lo que ha generado reacciones a través de redes sociales, a través de los cuales los colombianos y colombianas expresan su júbilo por esta noticia con el hashtag \#ElÚltimoDíadeLaGuerra.]

[Es por eso que el presidente Juan Manuel Santos viajará mañana a La Habana, Cuba para firmar el cese al fuego y de hostilidades bilateral. La firma de este importante anuncio que acabaría con 60 años de guerra con las FARC será sobre el medio día.]

[Para este momento histórico estarán presentes Jhon Kerry, secretario de Estado de EEUU, un delegado del Vaticano, Fidel Castro líder histórico de la revolución cubana, Ban Ki-Moonel Secretario General de las Naciones Unidas, el presidente de Venezuela Nicolás Maduro, y se espera la confirmación de la presidenta de Chile, Michelle Bachellet.]

### Comunicado Conjunto \#75

*La Habana, Cuba, 22 de junio de 2016*

*Las delegaciones del Gobierno Nacional y de las FARC-EP informamos a la opinión pública que hemos llegado con éxito al Acuerdo para el Cese al Fuego y de Hostilidades Bilateral y Definitivo; la Dejación de las armas; las garantías de seguridad y la lucha contra las organizaciones criminales responsables de homicidios y masacres o que atentan contra defensores de Derechos Humanos, movimientos sociales o movimientos políticos, incluyendo las organizaciones criminales que hayan sido denominadas como sucesoras del paramilitarismo y sus redes de apoyo, y la persecución de las conductas criminales que amenacen la implementación de los acuerdos y la construcción de la paz.*

*El evento estará encabezado por el Presidente de Colombia, Juan Manuel Santos, el comandante de las FARC-EP, Timoleón Jiménez, y por los países garantes. Por Cuba, el Presidente Raúl Castro, y por Noruega, el Canciller, Borge Brende. También estarán en representación de los países acompañantes, la Presidenta de Chile, Michelle Bachelet y de Venezuela, el Presidente Nicolás Maduro.*

*La ceremonia contará con la presencia, como invitado especial, del Secretario General de las Naciones Unidas, Ban Ki-Moon, quien estará acompañado por el Presidente del Consejo de Seguridad y el Presidente de la Asamblea General.*

*Igualmente asistirá el Presidente de República Dominicana, en calidad de Presidente de la CELAC; el presidente de El Salvador, y los enviados especiales para el proceso de paz de los Estados Unidos y de la Unión Europea.*

*Los acuerdos se darán a conocer mañana al mediodía en el salón de protocolo de El Laguito.*

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
