Title: Galería: Así se vivió la X Conferencia de las FARC
Date: 2016-09-23 16:01
Category: Galerias, Nacional
Slug: galeria-asi-se-vivio-la-x-conferencia-de-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/DSC_0579.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [23 Sep 2016]

Del 17 al 23 de septiembre se llevó a cabo la X Conferencia de las FARC en la vereda El Diamente, en San Vicente del Caguán, Caquetá. Contagio Radio estuvo en el lugar y comparte algunas de las imágenes de este evento histórico, en el que la guerrilla más grande se convierte en un movimiento político dejando de lado las armas.

\
