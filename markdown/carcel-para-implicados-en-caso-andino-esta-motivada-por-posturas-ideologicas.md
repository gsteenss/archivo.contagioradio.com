Title: Cárcel para implicados en caso Andino está motivada por "posturas ideológicas"
Date: 2017-06-30 17:41
Category: DDHH, Nacional
Tags: Bogotá, Falsos Positivos Judiciales
Slug: carcel-para-implicados-en-caso-andino-esta-motivada-por-posturas-ideologicas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/andinopaloquemao_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador ] 

###### [30 Jun 2017] 

La juez 47 de control de garantías dictaminó medida de aseguramiento a las 8 personas detenidas en el caso del Centro Comercial Andino, sin embargo, los abogados de los sindicados siguen manifestando que **las pruebas son muy débiles y que harían parte de lo que se ha denominado como Falsos positivos Judiciales**.

Gloria Silva, representante de uno de los capturados manifestó que la medida de prisión intramural es **“absolutamente arbitraria”** y está acompañada de especulaciones por parte de los jueces y de raciocinios que **“demuestran una postura ideológica de derecha”**. Le puede interesar:["Hay falta de garantías procesales para los detenidos del caso Andino"](https://archivo.contagioradio.com/hay-falta-de-garantias-procesales-para-los-detenidos-del-caso-andino-subcomision-onu/)

De igual forma, Silva expresó, que con su decisión, la Juez se está apartando de la obligación que la ley exige de realizar un análisis individual de cada uno de los imputados y de las evidencias atribuidas a cada persona. Sumado a ello la abogada señaló que **no hay ninguna prueba que indique que estas personas fueron las que activaron los explosivos** en el Centro Comercial Andino.

Además, la abogada aseguró que el manejo que le han dado los medios de información y el acceso que han tenido al caso, hacen parte de una estrategia del Estado para dar un mensaje de tranquilidad, seguridad y operatividad a la ciudadanía y desviar los móviles del atentado. Le puede interesar: ["Proceso contra detenidos en caso Andino se desmontará cuando baje la presión de los medios"](https://archivo.contagioradio.com/42883/)

Gloria Silva, quien pudo conversar con los sindicados, **afirmó que ellos se encuentran indignados con la decisión de la Juez, sin embargo, aseveró que se ha contado con un gran respaldo y solidaridad.** Los jóvenes capturados serán trasladados a la Cárcel Modelo, mientras que las mujeres serán recluidas en El Buen Pastor.

<iframe id="audio_19567254" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19567254_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
