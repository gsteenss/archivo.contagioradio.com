Title: Paramilitares salieron de El Bagre en Antioquia, pero las FFMM no los han combatido
Date: 2017-07-07 12:22
Category: DDHH, Nacional
Tags: Aheramigua, Antioquia, paramilitares
Slug: 43248-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [07 Jul. 2017]

Luego de 6 días de haber hecho pública la denuncia en la que cerca de 50 paramilitares se tomaron la cabecera municipal del corregimiento Puerto López en El Bagre, Antioquia, hoy ese lugar esta en una “tensa calma” pues, si bien la respuesta del Ejército y Policía fue rápida y los paramilitares salieron del lugar, **hoy no se sabe hacia dónde partieron y cuándo podrían regresar porque no se les ha combatido**. Le puede interesar: [Cerca de 50 paramilitares se toman la cabecera municipal en Bagre, Antioquia](https://archivo.contagioradio.com/cerca-de-50-paramilitares-se-toman-la-cabecera-municipal-en-bagre-antioquia/)

Según Jairo Rodríguez, integrante de la Asociación de Hermandades Agroecológicas y Mineras de Guamocó - AHERAMIGUA “a las 10 a.m. hicieron presencia los paramilitares hasta las 3 p.m. Luego llegó el Ejército y la Policía con sobrevuelos a la zona. Luego los paramilitares salieron del pueblo”. Le puede interesar: [Comunidades denuncian 17 asesinatos a manos de paramilitares en El Bagre, Antioquia](https://archivo.contagioradio.com/comunidades-denuncian-17-asesinatos-a-manos-de-paramilitares-en-el-bagre-antioquia/)

Dice Rodríguez que no hubo enfrentamientos, ni se intentó combatir estas estructuras de tipo paramilitar, sino **las acciones estuvieron encaminadas solamente a sacarlos de ese territorio,** “no hubo fuego cruzado, no ha habido combate, ni desestructuramiento de los paramilitares. Hay presencia, controles, requisas del Ejército y no más”.

Dada las reiteradas denuncias que ha realizado AHERAMIGUA de la existencia y el control paramilitar en esa zona de Antioquia, las comunidades se encuentran temerosas por su vida **“estamos en Antioquia, es grande y no sabemos es hacia que parte se trasladaron los paramilitares** (…) debe primar la seguridad en este corregimiento” concluyó Rodríguez.

### **Las denuncias por presencia paramilitar vienen desde años anteriores** 

Cabe recalcar que desde el 9 de enero del 2016 AHERAMIGUA viene denunciando hechos graves que acontecen en el municipio por cuenta de la presencia de paramilitares como desapariciones forzadas, amenazas vía mensaje de texto y asesinatos de líderes de la región. Le puede interesar: [Con cruces negras en las casas amenazan a líderes en Puerto López](https://archivo.contagioradio.com/pintan-cruces-en-las-casas-de-lideres/)

<iframe id="audio_19682916" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19682916_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
