Title: Militares asesinan a campesino de resguardo indígena en Inzá, Cauca
Date: 2015-06-23 12:03
Category: DDHH, Nacional
Tags: autoridades indígenas, Cauca, Consejo Regional Indígena del Cauca, CRIC, Enrique Bastidas, FARC, Fuerza de Tarea Apolo, guardia indígena, Indigenas Nasa, Inzá, justicia indígena
Slug: militares-asesinan-a-comunero-de-resguardo-indigena-en-inza-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Captura-de-pantalla-2015-06-23-a-las-17.07.06.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Waira Jaku 

<iframe src="http://www.ivoox.com/player_ek_4678028_2_1.html?data=lZukmpWWfI6ZmKiakpuJd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcrgytnO1MrXb8LnxtjW0MbSb8KfxNTa19PJttCfxcqY1MrXq9bV08ncjc7SqIa3lIqupszJssKfxtOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Emigdio Velasco, Consejo Regional Indígena del Cauca, CRIC] 

###### [23 de junio 2015]

El lunes en la madrugada en el resguardo de San Andrés, en el municipio de Inzá, Cauca, un campesino identificado como **Enrique Bastidas, fue asesinado a manos de militares,** que según la comunidad, mantienen intimidados a los pobladores, por lo que los indígenas decidieron retener a los militares acusados del asesinato.

De acuerdo a Emigdio Velasco, **coordinador del programa de derechos humanos del Consejo Regional Indígena del Cauca, CRIC,** Enrique Bastidas de 28 años de edad, se movilizaba con otra persona en una motocicleta y al no detenerse por la señal de pare que le abrían hecho los militares, estos mismos habrían decidido dispararle dos veces, lo que ocasionó la muerte del campesino. Así mismo, Velasco, comenta que la persona que acompañaba a Bastidas, asegura que no había tal señal de “pare” y por lo tanto, no se detuvieron.

Los militares que atacaron al campesino, hacen parte de la **Fuerza de Tarea Apolo del Batallón José Ilario López,** por lo que desde el programa de derechos humanos del CRIC, en la personería, ya se adelanta el proceso de necropsia y las autoridades indígenas adelantan la investigación del caso, en el que **el ejército ya reconoció haber cometido “un error militar”** como lo dice el integrante del CRIC.

La guardia indígena, espera que de acuerdo al resultado de la investigación, **se aplique la justicia indígena para quienes cometieron el hecho**, así como sucedió el año pasado cuando las autoridades del resguardo retuvieron a siete guerrilleros de las FARC que asesinaron a dos personas de la comunidad Nasa.

Por otra parte, en Santa Rosa, continúan retenidos los militares que habrían acabado con la vida del campesino. “El CTI y la Fiscalía están próximos a llegar, pero con la gente vamos a tener al Ejército hasta que llegue el personal jurídico” expresó Elías Yugue, gobernador indígena.
