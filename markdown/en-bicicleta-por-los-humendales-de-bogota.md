Title: En bicicleta por los humendales de Bogotá
Date: 2015-02-02 21:34
Author: CtgAdm
Category: eventos
Slug: en-bicicleta-por-los-humendales-de-bogota
Status: published

**El próximo sábado 7 de febrero, todas y todos están invitados a la tercera versión de la Bicicaravana,** donde los asistentes tendrán la oportunidad de **recorrer varios ecosistemas** de la ciudad,  para conocer sus riquezas y amenazas con el objetivo de que se apropien de ellos, apropósito del **2 de febrero, día en que se conmemora el Día Mundial de los Humedales.**

El evento lo realiza la Fundación Humedales de Bogotá. El tema para este año es **"Humedales para el futuro",** con el objetivo visibilizar  y llamar a los ciudadano a cuidar estos ecosistemas que conservan la riqueza de la flora y la fauna bogotana.

La fundación invita a todos los ciclistas a hacer parte de este evento donde también habrá varias actividades culturales.

Para mayor información:

[www.humedalesbogota.com/bicicaravana/](http://humedalesbogota.com/bicicaravana/)

311 500 91 92

<info@humedalesbogota.com>
