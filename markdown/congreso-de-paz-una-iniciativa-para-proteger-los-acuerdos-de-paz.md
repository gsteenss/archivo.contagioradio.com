Title: Congreso de Paz una iniciativa para proteger los acuerdos de paz
Date: 2017-03-03 16:06
Category: Nacional, Paz
Tags: Implementación de acuerdos de paz, procesos de paz
Slug: congreso-de-paz-una-iniciativa-para-proteger-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [03 Mar 2017] 

Ante la difícil situación que atraviesan los acuerdos de paz en su implementación, la falta de garantías para los líderes sociales y las condiciones poco dignas en las que se encuentran los guerrilleros de las FARC-EP, **diversas organizaciones, representantes políticos, académicos y estudiantes decidieron conformar el Congreso de Paz.**

Este escenario, que surgío tras el plebiscito, pretende ser un espacio de diálogo e interlocución entre las universidades, las organizaciones sociales y el movimiento social, que busque generar **estrategias de acompañamiento a la implementación de los acuerdos de paz de La Habana, como a la mesa de conversaciones que se desarrolla con el ELN**. Le puede interesar: ["Nueva ola de solidariadad en las Zonas Veredales Transitorias"](https://archivo.contagioradio.com/nueva-ola-de-solidaridad-con-las-zonas-veredales-transitorias/)

De acuerdo con Ángela María Robledo, representante por el Partido Verde, esta iniciativa pretende que las universidades abran más espacios en donde **tanto indígenas, como campesinos, mujeres e incluso la insurgencia puedan puedan responder conjuntamente a el cómo construir la paz. **Le  puede interesar: ["Armas de las FARC se van a convertir en homenaje y memoria: Carlos Lozada"](https://archivo.contagioradio.com/armas-de-las-farc-se-van-convirtiendo-en-homenaje-y-memoria-carlos-lozada/)

El pasado 2 de marzo, se conformaron **4 mesas de trabajo que dinamizaran esta iniciativa, con la finalidad de que en abril o mayo se pueda dar el Congreso de Paz**, con la participación de universidades públicas y privadas de diferentes regiones del país.

Además, Robledo agregó que "esta es una oportunidad civil para que tanto las organizaciones sociales como el movimiento popular **impidan que populismos y sectores de la ultraderecha acaben con lo que tanto le ha costado a Colombia como lo es la paz**"

<iframe id="audio_17352002" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17352002_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
