Title: Comunidades rurales rechazan creación de Sala Especial para Militares
Date: 2018-10-31 13:02
Author: AdminContagio
Category: Comunidad, DDHH
Tags: Civiles, Congreso, JEP
Slug: comunidades-rechazan-sala-especial-militares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Caminata-por-la-paz-60.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo - Contagio Radio 

###### 31 oct 2018 

En una **[carta firmada](https://www.justiciaypazcolombia.com/pronunciamiento-comunidades-sobre-la-propuesta-de-la-sala-especial-para-militares-en-la-jep/)** por más de 50 comunidades dirigida al **Congreso de la República**, en particular al **Centro Democrático** y sectores de los partidos **Liberal, Conservador y Cambio Radical,** hacen un llamado a legitimar los derechos de las víctimas y señalan los riesgos de crear una **Sala Especial para el juzgamiento de militares ante la Jurisdicción Especial para la Paz (JEP)** y la modificación de la misma.

“Les escribimos porque creemos que es el tiempo de empezar a construir un nuevo país, porque es necesario cerrar los odios y la venganza. Y en ese escenario es que planteamos que nuestro encuentro como hijos de la matria y **la patria se dará en escuchar las verdades de todos**", advierte el comunicado.

En la misiva, numerosas comunidades de **cabildos, resguardos y organizaciones del Chocó, Cauca, Antioquia, Meta y Putumayo** señalan que dicha Sala Especial representaría la ausencia de responsabilidad de aquellos que perpetuaron la violencia sistemática, crímenes de lesa humanidad y crímenes de guerras en sus territorios.

La comunicación **apela además a la igualdad de condiciones,** en un país que ha visto  nacer a combatientes policías, militares, paramilitares y guerrilleros, “Muchos militares en el campo de batalla sí vivieron y conocieron los lugares remotos donde vivimos, incluso, acusándonos de guerrilleros aunque no lo fuéramos, y muchos de ellos saben que este país requiere inclusión, muchos de ellos obedecieron a civiles y poderosos”.

Debatida en la **Comisión Primera**, la propuesta sustentada en brindar plenas garantías a las Fuerzas Militares y dar condiciones diferentes que las que recibirían los exguerrilleros de las Farc, desviaría el foco de las investigaciones en las que **podrían vincularse terceros civiles implicados en dichos crímenes.**

###### Reciba toda la información de Contagio Radio en [[su correo]
