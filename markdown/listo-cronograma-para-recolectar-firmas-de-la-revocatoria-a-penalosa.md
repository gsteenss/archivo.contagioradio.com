Title: Listo cronograma para recolectar firmas de la revocatoria a Peñalosa
Date: 2017-01-20 13:01
Category: Movilización, Nacional
Tags: Bogotá, Firmas, revocatoria Peñalosa
Slug: listo-cronograma-para-recolectar-firmas-de-la-revocatoria-a-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/cronograma-revocatoria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unidos Revocaremos a Peñalosa] 

###### [20 Ene 2017] 

Ya está listo el cronograma de lugares y fechas en donde se estarán recolectando las firmas, por parte de los comités, para la revocatoria del alcalde Enrique Peñalosa en Bogotá. En la lista hay diferentes puntos de encuentro en cada una de las localidades de la capital, y se tendrá un plazo hasta el próximo 12 de julio para hacer la recolección. Le puede interesar:["Listos formatos de recolección de firmas para revocatoria de Peñalosa"](https://archivo.contagioradio.com/listos-formatos-de-recoleccion-de-firmas-para-revocatoria-de-penalosa/)

El proceso de revocatoria busca visibilizar la problemática de fondo que afronta Bogotá y su modelo de ciudad, frente a este tema Pinto señala que si bien es cierto que la capital posee dificultades estructurales de más de 50 años, en los últimos gobierno **se habían alcanzado avances en materia de garantías de derechos humanos que en este periodo se han visto recortados.**

![revocatoria](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/revocatoria.jpeg){.alignnone .size-full .wp-image-34951 width="1211" height="578"}

###### Reciba toda la información de Contagio Radio en [[su correo]
