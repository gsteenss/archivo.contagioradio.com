Title: Aún existe una verdad oculta del conflicto que aguarda en el exilio
Date: 2020-11-14 12:06
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Comisión de la Verdad, conflicto armado, Exilio
Slug: aun-existe-una-verdad-oculta-del-conflicto-que-aguarda-en-el-exilio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Exilio.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Exilio1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto:

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según cifras del Alto Comisionado de las Naciones Unidas para los Refugiados (ACNUR), en el marco del conflicto armado en Colombia, más de 550.000 personas se vieron forzadas a salir del país para proteger su vida, sin embargo en el país y el resto del mundo, es poco lo que se conoce sobre las víctimas que viven en el exterior. Es por tal motivo que este 13 de noviembre, la [Comisión de la Verdad](https://comisiondelaverdad.co/)desarrolló en nuevo encuentro que reconoció la dignidad, impactos, contextos, persistencias de quienes abandonaron su país de forma forzosa por la guerra.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con el encuentro “El retorno de nuestras voces”, la Comisión de la Verdad, que surge como parte del Sistema Integral de Verdad, Justicia, Reparación y No Repetición continúa recolectando las piezas del informe final que permitirá aportar al esclarecimiento del conflicto en Colombia, sus responsables, causas y consecuencias en los que la población exiliada resulta esencial para dicha reconstrucción.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el comisionado Carlos Berinstain, quien ha acompañado este proceso, **a la fecha, son 1.200 testimonios en un total de 23 países los que ha recopilado la Comisión de la Verdad de la población exiliada,** estos representan más del 10% del total de relatos que tiene la institución a la fecha, lo que demuestra la importancia de quienes viven en el exterior en la construcción del relato de un país que busca superar un conflicto armado de más de 60 años.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de que los testimonios en el exilio hablan de más de 3.600 víctimas, otro de los datos que resaltó el comisionado es que han sido los países en la frontera como Ecuador, Venezuela, Perú y Brasil, los principales destinos de quienes han acudido al exilio además de Estados Unidos, Canadá, Chile y países de Europa como España, Suecia, Gran Bretaña y Bélgica.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El exilio es un problema invisible en Colombia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, el comisionado Beristain resaltó que el exilio permanece invisibilizado debido a que la multiplicidad de hogares que ha tenido que salir del país no ha tenido un reconocimiento ni en sus estadísticas, ni en sus políticas por lo que este encuentro reconocerá "la injusticia de la gente que tuvo que salir del país, la gente piensa que es una solución pero es una enorme ruptura, es empezar desde las cenizas". [(Lea también: Comunidades claman por la verdad y por acuerdos humanitarios que alivien el dolor de la guerra)](https://archivo.contagioradio.com/comunidades-claman-por-la-verdad-y-por-acuerdos-humanitarios-que-alivien-el-dolor-de-la-guerra/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Tuve que sobreponerme, aprender el idioma y me sentí discriminada en temas laborales en Canadá aunque me he sentido más discriminada en mi país de origen": expresó Elsi Ángulo, fiscal exiliada tumaqueña quien continúa trabajando con poblaciones afro y con víctimas del conflicto.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para la Comisión, esto es evidente en la debilidad de las fuentes o subregistro del número de víctimas exiliadas bajo distintos estatus de protección además del silencio o la poca verdad que existe sobre los hechos que precedieron a su exilio, y los impactos posteriores sobre su vida, familia y organizaciones, partidos políticos y comunidades a los que pertenecían.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“Yo viví el exilio y es muy duro, lo marca a uno para siempre. Uno nunca deja de ser exiliado, pero más allá de esa marca, uno tiene esa fuerza interior de seguir adelante. Trabajar y luchar ayuda a luchar contra todo”: expresó durante el encuentro el también **comisionado y médico, Saúl Franco.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"El exilio es una violación en si misma, reconocer esas formas de revictimización y el miedo ha sido un potente agente expulsor"**, señala Beristain, por lo que es precisamente una de las principales exigencias de la población exiliada que se reconozca la violencia sufrida y sus afectaciones y la forma en que han tenido que afrontar una nueva realidad en el extranjero.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“Si olvidamos que sucedió y sus causas, queda el ocultamiento o, lo que es peor, la memoria manipulada. Y ya sabemos que las distorsiones históricas desvían la responsabilidad de quienes quiebran los Derechos Humanos”, expresó durante el encuentro Tila Uribe, educadora popular y alfabetizadora de adultos campesinos, obreros y mujeres y exiliada en Francia durante la década de los 70.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El comisionado Beristain resalta que una Comisión de la Verdad no solo " está orientada a crear un diálogo social con una fuerte centralidad en las víctimas, a su vez, es un proceso en un marco social de reconocimiento para una multiplicidad de experiencias individuales", por lo que este encuentro resulta esencial para [(Lea también: Tras la verdad, hay un sector que no quiere perder el poder del control ideológico)](https://archivo.contagioradio.com/sector-teme-verdad-no-quiere-perder-poder-control-ideologico/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"El “exilio es no lugar en la vida del país, es la mitad del camino hacia ninguna parte, están en un proceso de integración y hay muchos impactos de ese exilio que es importante visibilizar". [(Le puede interesar: Mancuso: "Colombia no conoce la verdad, porque no hubo ni existe interés político")](https://archivo.contagioradio.com/mancuso-colombia-no-conoce-la-verdad-porque-no-hubo-ni-existe-interes-politico/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque este fenómeno comenzó a vivirse desde la década de los setenta u y ochenta y tuve un auge que afectó a comunidades campesinas, afro e indígenas además de diferentes funcionarios de la rama judicial, el comisionado advierte que después de la firma del Acuerdo de Paz, en la actualidad el creciente asesinato de defensores de DD.HH. y líderes sociales ha dado paso a una nueva oleada de exilios.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
