Title: Las preocupaciones de los ambientalistas con la nueva alcaldía de Bogotá
Date: 2016-01-13 17:29
Category: Ambiente
Tags: ambiente Bogotá, Bogotá, Enrique Peñalosa
Slug: las-preocupaciones-de-los-ambientalistas-con-la-nueva-alcaldia-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/humedal-tibabuyes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [humedalesbogota.com]

<iframe src="http://www.ivoox.com/player_ek_10075030_2_1.html?data=kpWdmZqUd5Ghhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5ynca3V1JDd1MrTp9bkwsjW0dPJt4zYxpDZ0diPpc7Wysrb1sbQrdTowtiYxdTSb83VjNPix9vFb8Khhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Camilo Andrés Julio, Reacción ambiental] 

##### [13 Enero 2016]

Las organizaciones y colectivos ambientalistas de Bogotá, enviaron a inicios de año una carta dirigida al alcalde de la ciudad Enrique Peñalosa, en la que le piden que se de continuidad al enfoque ambiental con el que venía trabajando la anterior administración del exalcalde Gustavo Petro, es decir, de renaturalización ecológica.

Aquí les presentamos sus principales preocupaciones, teniendo en cuenta los proyectos anunciados por Peñalosa.

**Reserva Thomas Van Der Hammen:** En el discurso de posesión, Enrique Peñalosa habló sobre la necesidad de ampliar la ciudad hacia el norte, lo que podría incluir las casi 1500 hectáreas de esta reserva forestal que en este momento se encuentra en proceso de restauración ecológica.

La reserva es corredor estratégico para la reproducción de aves, mamíferos y reptiles que están en la zona, de urbanizarse, por dar un ejemplo, estarían en riesgo 2 especies de mariposas que solo se encuentran en esta reserva, y no las hay en ninguna otra parte del mundo, por ello, Camilo Andrés Vergara, integrante del colectivo ambientalista, Reacción ambiental indica que es necesario que haya una gran movilización para que se respeten las riquezas naturales y además invita a la ciudadanía a “hacer propuestas frente a la ciudad que queremos para nosotros y los otros seres vivos que la habitan”.

“Nos preocupa que todo el esfuerzo en restaurar la reserva se pierda, de ser así, se rompería  todo el cordón de biodiversidad de la zona, que fue creado para la reproducción de agua, ya que hay acuíferos” señala Vergara.

**Avenida Logitudinal ALO:** Pese a la propuesta del alcalde de construir esta avenida de forma elevada, de acuerdo con Vergara, sea cual sea la forma, la fauna y flora de los tres humedales por los que pasaría esta avenida se verían afectados. “Es una obra grandísima, de bastante tiempo, por el ruido, el polvo, las vigas que tienen que  poner para sostener la avenida longitudinal, algunas especies animales no volverán, muchas podrán morir durante la construcción”, dice el integrante de Reacción Ambiental.

Así mismo, con la construcción de la ALO, se taparán los espejos de agua y algunas especies vegetales del Humedal La Conejera, Capellanía y Tibabuyes podrían morir. Camilo Andrés, resalta específicamente el caso del humedal Capellanía que según él, desaparecería por completo. Así mismo, los otros 2 humedales “se partirían en dos”, poniendo en riesgo la gran diversidad de aves y reptiles y curíes que transitan en los humedales.

**Corredor ecológico en los Cerros orientales:** Aunque para algunas organizaciones ambientalistas de la ciudad, la propuesta puede ser “interesante”, preocupa que las obras que se realicen sean a base de concreto modificando las condiciones naturales del ecosistema como sucedió con el Humedal Tibabuyes en la anterior administración de Peñalosa. Los ambientalistas le han pedido al alcalde que se continúe con el enfoque de renaturalización ecológica con los corredores verdes, en los cerros y el río Bogotá.
