Title: Militares desorientaron a familiares de los desparecidos del Palacio de Justicia
Date: 2015-10-19 11:39
Category: DDHH, Nacional
Tags: Belisario Betancur, Cristina Guarín, Desaparición forzada Colombia, Familiares de desaparecidos del Palacio de Justicia, holocausto del Palacio de Justicia, Noemí Sanín, Palacio de Justicia, René Guarín, Retoma del Palacio de Justicia
Slug: militares-desorientaron-a-familiares-de-los-desparecidos-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Guarín.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas ] 

<iframe src="http://www.ivoox.com/player_ek_9075032_2_1.html?data=mpWkl5WXdo6ZmKiakp2Jd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkargytnO1MrXb8XZ1NTfy8rSuMLm0NOYw5DKpc7dzc7O1MrXb8XZjNHc1ZDIqdTkwtfSxc7Is46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [René Guarín, hermano de la víctima] 

###### [19 Oct 2015]

[Se conoció un documento inédito en el que el padre de **Cristina Guarín**, **desaparecida** tras la retoma del Palacio de Justicia, juramentaba el 18 de septiembre de 1986 ante la Notaria 29 de Bogotá, la **versión dada por oficiales** de la Armada y el Ejército Nacional de que su hija **junto con 4 personas más fueron conducidas a Praga**, **para ser deportadas y procesadas en Colombia como guerrilleros**.]

“Con el paso de los años la lectura que hacemos es que se trato de una **manipulación desde el Estado, desde los militares, del dolor de los familiares de los desaparecidos** del Palacio de Justicia, para desorientarnos, para desinformarnos y para ponernos a buscar en uno u otro lugar”, afirma Guarín. Esta versión es una “**Estrategia infame por parte del Estado colombiano** que jugó con el dolor de los familiares”, por lo que es desvirtuada completamente.

[De acuerdo, con René Guarín, hermano de la víctima, esta es una información que lleva guardada cerca de 30 años, producto del **contacto a su familia por parte de un abogado de apellido Arboleda**, enlace del Coronel Arévalo quien para ese momento era  Jefe de Prensa del Ministerio de Defensa.]

[Guarín asegura que el abogado los citó en una sede militar en la que el Coronel Arévalo y otro militar perteneciente a la Armada, dieron la versión que días más tarde su padre juramentó en una Notaria de la capital y les solicitaron que **“No revelara las fuentes y que no dijera nada hasta tanto el hecho fuera concretado”**.       ]

[Hacer público el documento es “Un **buen mensaje para que el Estado colombiano**, **los políticos**, Belisario Betauncur, los militares, el ministro Jaime Castro, la ministra Noemí Sanín, la Policía, el DAS, **le cuenten al país y al mundo que fue lo que hicieron con nuestros familiares desaparecidos**”.]

[“**El Estado colombiano debe romper ese pacto de silencio que ha rodeado la desaparición de las personas del Palacio de Justicia**… debe romper el miedo de decir la verdad de lo que fue lo que realmente hizo con nuestros familiares una vez salieron vivos del Palacio de Justicia, como es el caso de mi hermana Cristina Guarín”.]
