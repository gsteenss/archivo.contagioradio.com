Title: Soñar viviendo
Date: 2015-12-28 10:06
Category: Camilo, Opinion
Tags: Ambiente, COP 21, paz, proceso de paz
Slug: los-suenos-cop-21-y-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/sueoo-grafftti.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Acción Poética ] 

#### **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [~~@~~CamilodCasas](https://twitter.com/CamilodCasas)

###### 28 Dic 2015 

No se trata de ninguna retórica pragmática, es la expresión del rector de la escuela de Harry Potter, cuando el aprendiz se encuentra en las encrucijadas de su contingencia histórica. No se trata de abdicar, se trata de hacerse a los sueños, de vivirlos y no perderlos en disquisiciones o en ideas que pueden ser nobles, pero que no se hacen creíbles y que poco hacen realidad las transformaciones.

LA COP 21 de París y el acuerdo sobre Justicia  en las conversaciones del gobierno de Juan Manuel Santos con las FARC EP, expresan nuevamente la correlación de fuerzas. Poderes avasallantes y expresiones de cambios y transformaciones necesarias que se plantean desde otro lugar, sin que estas últimas logren definir una actitud distinta ante los asuntos de fondo.

Ese otro lugar, somos nosotros, los ciudadanos, que estamos del otro lado de una historia que queremos que sea distinta, un presente diferente de Vida. El peso de los retos, por decirlo de alguna manera, se encuentra del lado de los afectados, y somos solamente nosotros y no los mandatarios y los sectores de interés que expresan, los que podemos transformar o hacer realidad el derecho a la supervivencia del planeta y la satisfacción de los derechos de las víctimas. En ambos casos, desde el lado de lo transformante, tenemos el derecho de seguir cuestionando, pero urge pararse en la realidad y vivir.

La sociedad mundial y nacional se encuentra en un momento crucial frente a lo que proyecta como apuesta de país y de humanidad. Si bien existen los marcos normativos, las leyes, los acuerdos, las recomendaciones, las conclusiones de la COP y del Acuerdo de Justicia y son así, y es lo real, pero esto real sigue siendo reto transformante. O se ara en el mar o se contempla. Vivir es arar en el mar.

La normatividad es una formalidad en medio de las relaciones reales de poder. Esta formalidad hay que usarla u objetarla. Aun así, ¿la formalidad expresara lo que deseamos, esa es la realidad transformada y la transformación real?. Los sueños no son el papel pero hay que vivirlos

Era previsible que la COP 21 fuera nuevamente otra expresión maquillada de compromiso de los jefes de Estado frente a la obligatoriedad de reducir la emisión de gases de efecto invernadero para llegar a un máximo de calentamiento de 1.5 C. Suscribir algo distinto era enfrentar el modelo energético, el mundo de los agronegocios. Sus enunciaciones son promesas, sin compromisos reales, los verbos y las adjetivaciones de la declaración de lo más de 190 países es de intenciones, nada vinculantes. ¿Algunos que esperaban, que iban a cambiar? Pues, ¡no!, eso no era posible. ¿Esto significa abdicar de presionar? Tampoco. Parece olvidarse que justo los gobiernos, esos gobiernos son para administrar negocios, muchas veces de privados que les financian, o que les propician la gobernabilidad. Olvidamos que  en otros casos, redistribuyen las ganancias para hacer Estados más sociales, pero siempre derivando la acumulación y la distribución de la riqueza de las mismas fuentes de energía.

La misma actitud de queja se escucha en algunas organizaciones sobre el Acuerdo en La Habana y los derechos de las víctimas. ¿Por qué no se logró más frente a los expresidentes y presidentes?, ¿Por qué no se logran acuerdos de no repetición, qué significan transformar la antidemocracia colombiana? Olvidamos la correlación de fuerzas, los factores de poder real empujan a las conversaciones porque les es más costoso y dilatado seguir en la salida militar para sus intereses de control territorial en el modelo de mercado. Dejamos de lado que es un proceso que se desarrolla dentro de la aplicación del modelo económico neoliberal, sin que las víctimas del modelo ni la del conflicto armado, estén suficientemente organizadas para una transformación del orden de cosas a través de la desobediencia civil y la objeción resistente y propositiva. Olvidamos incluso, que bajo el maximalismo penal proyectado en una cárcel, se logra el silencio de ejecutores y de victimarios, que se llevan los nombres de los privados que les proyectaron en hacer la guerra. ¿Por qué quejarse?

Santos llevó a París su agenda de Colombia Magia Salvaje, su apuesta de reforestación y protección de la Amazonia, sus 2.4 millones de áreas protegidas, la delimitación de 36 páramos, sus ZIDRES como expresión coherente con las multinacionales de los agronegocios de alimentos y los agrocombustibles, la ampliación de las REED ,disfrazando todo de verde y de inclusión social, y cómo no es ningún tonto, por supuesto se negó a suscribir la relación de los asuntos ambientales con los derechos humanos, pues ahora hay “pax a la vista”. Y en La Habana, Santos logra excluirse a él y a los expresidentes de las eventuales demandas en la JEP, se salva a él mismo, como lo expresa, la misma lógica del mercado.

En ambos casos son innegables los avances de la sociedad, en la calle, en la presión, tímidas expresiones, pero importantes, nunca suficientes, pues estas deben estar articuladas a prácticas concretas de justicia ambiental, sumadas a propuestas al sector trabajador hoy vinculado con agronegocios o con agrocombustibles, y con el sistema energético, petrolero o de gas, y vinculadas en el caso de la justicia sancionatoria a dinámicas transversales. Es innegable que el Sistema Integral de Verdad, de Justicia, de Reparación y de no Repetición, es inédito, y que serán solamente las sociedades organizadas, sus víctimas, las que pueden lograr darle un contenido real, un alcance.

Al final, la justicia climática y la justicia restaurativa solo será real en la construcción de otro proyecto de poder, que se viva desde esas microsociedades hasta la revolución del ancien régimen y sus discursos de sometimiento a la mentira, que se muestran como verdad. El reto es vivir, soñando, pero viviendo, haciendo real la justicia.
