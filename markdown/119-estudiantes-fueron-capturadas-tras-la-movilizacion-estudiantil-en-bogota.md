Title: 119 estudiantes fueron capturados tras la movilización estudiantil en Bogotá
Date: 2018-11-16 13:25
Author: AdminContagio
Category: Educación, Nacional
Tags: estudiantes, Fuerza Públics, Paro Nacional, UNEES
Slug: 119-estudiantes-fueron-capturadas-tras-la-movilizacion-estudiantil-en-bogota
Status: published

###### [Foto: Contagio Radio] 

###### [16 Nov 2018] 

Tras las movilizaciones masivas de este jueves convocada por los estudiantes, la comisión de Derechos Humanos de la Unión Nacional de Estudiantes por la Educación Superior (UNEES) denunció que hubo uso excesivo de la fuerza por parte del ESMAD y la Policía, detenciones arbitrarias de más de **119 personas, tortura, traslados irregulares en vehículos sin placas y heridos en las marchas por acción de la Fuerza Pública.**

De acuerdo a la información recogida por la Comisión de Derechos Humanos de la UNEES, en Bogotá muchas de las capturas fueron irregulares, pues se realizaron por parte de integrantes de la Fuerza Pública vestidos de civil, y contra personas que incluso no hacían parte de la movilización. Adicionalmente, la Comisión denunció que en uno de los buses en los que trasladaban a los capturados**, la Policía apagó las luces, detuvo el vehículo y accionó un gas; constituyendose así, una tortura**.

Asimismo, denunciaron que, aún no se ha podido establecer el número total de personas heridas por el ESMAD, pero si de afectaciones graves como la de un estudiante de la Universidad Nacional, sede Palmira, **que fue agredido al parecer por una aturdidora en la frente. **(Le puede interesar: ["30 estudiantes resultaron heridos durante movilización en Bogotá"](https://archivo.contagioradio.com/al-menos-30-estudiantes-resultaron-heridos-durante-movilizacion-en-bogota/))

Según la representante a la Cámara **Maria José Pizarro**, en la marcha se hizo evidente que los desmanes los inició la Policía, que las marchas eran pacíficas, y que la ciudadanía apoyó masivamente a los estudiantes en sus justas demandas. Adicionalmente, recalcó que la protesta no debía empañarse por el acto vandálico de algunos pocos, que incluso no es posible establecer si eran estudiantes.

Ante esta situación, Pizarro sostuvo que toda la información recopilada de arbitrariedades y hechos irregulares perpetrados por parte de la Fuerza Pública fueron transmitidos a la Defensoría del Pueblo y la Procuraduría General de la Nación, de tal forma que sea posible investigar dichas situaciones, y llamar la atención de los agentes del Estado para que protejan los derechos constitucionales.

### **Ministra de educación pide a Estudiantes retomar los diálogos** 

La Representante a la Cámara recordó que María Victoria Angulo no asistió al debate de control político sobre el tema de educación superior que se realizó el martes, motivo que la hace dudar sobre las garantías que pueda tener el llamado a retomar el diálogo por parte de la Ministra. Sobre este encuentro la UNEES ha sido enfática en que es necesaria una **mesa de negociación y no de diálogos que permita tratar, entre otros, los temas de garantías para la movilización social.**

### **"Aquí lo primero es reafirmar que se debe desmontar el ESMAD"**

Pizarro afirmó que desde la ultima movilización se evidenció que había personas vestidas de civil al interior de la marcha que luego de ser retiradas de la marcha por parte de defensores de derechos humanos, comenzaron a caminar junto a agentes del ESMAD. Además, **señaló que hubo ataques a periodistas y medios de comunicación** que llevaban cámaras para evitar que estos videos salieran a la luz pública.

Para evitar que hechos como esos se repitan, la Representante sostuvo que es necesario "tener un ordenamiento jurídico claro que garantice el derecho constitucional a la libre protesta, y uno de los primeros pasos en ese sentido tendría que ser **el desmonte del ESMAD, porque es una fuerza que tiene una forma de operar contra los derechos humanos** y contra la protesta social. (Le puede interesar: ["ESMAD tiene intimidadas a comunidades afectadas por Hidroituango en Sabanalarga, Antioquia"](https://archivo.contagioradio.com/esmad-tiene-rodeadas-a-comunidades-afectadas-por-hidroituango-de-sabanalarga-antioquia/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
