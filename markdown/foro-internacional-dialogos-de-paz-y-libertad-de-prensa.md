Title: Foro Internacional: Diálogos de Paz y Libertad de Prensa
Date: 2015-05-06 18:11
Author: CtgAdm
Category: Movilización, Nacional
Tags: Diálogos de paz y libertad de prensa, FLIP, Foro internacional Diálogos de paz y libertad de prensa, Fundación para la Libertad de Prensa
Slug: foro-internacional-dialogos-de-paz-y-libertad-de-prensa
Status: published

######  

###### Foto:Contagioradio.com 

###### Entrevista con [**Marvin Galeas**], fundador de Radio Venceremos: 

<iframe src="http://www.ivoox.com/player_ek_4460297_2_1.html?data=lZmjkpede46ZmKiak5eJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYr8bWusrijKzOzsrFt4zYxtGYqLKykIzYxpCyzpC3pc3qwsnc1JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La **Fundación para la Libertad de Prensa** en Colombia con motivo de la celebración del día de la libertad de prensa 2015 ha realizado el Foro Internacional: Diálogos de paz y libertad de prensa.

Desde que se hizo público el **punto 2 de los Diálogos de la Habana, sobre participación política** en los que se acordó crear espacios públicos para radios comunitarias, ampliar el espectro electromagnético o la asignación de una pauta transparente, la Flip está organizando espacios de reflexión como este.

El **foro** se realizará los días **6, 7 y 8 de Mayo** y contará con la presencia de periodistas nacionales e internacionales expertos en libertad de prensa relacionada con conflictos armados y procesos de paz.

Hoy 6 de Mayo comenzaba el foro, destacando la intervención en la ponencia sobre **¨Libertad de prensa y Diálogos de la Habana¨** de la periodista víctima de secuestro y violación **Jineth Bedoya**, quién remarcó que los diálogos no pueden correr la misma suerte que la conocida desmovilización de los paramilitares donde no ha habido ni memoria ni verdad ni reparación ni perdón.

Además los ponentes advirtieron que estos **diálogos deben ser una oportunidad para la creación de más medios de comunicación alternativos** para que de esta forma puedan escucharse todas las voces de la sociedad colombiana.

En la ponencia Medios de comunicación en la reconciliación en el **caso de Sudáfrica, Willie Esterhuyse**, uno de los principales negociador en las conversaciones de paz sobre el apartheid, relató como los medios de comunicación habían sido actores dentro del conflicto armado y durante el **proceso de paz difundiendo la liberación de Mandela**, pero que a pesar de su papel no habían entendido la capacidad que tenían para apoyar los diálogos de paz.

**Marvin Galeas,** ex combatiente del Frente Farabundo Martí para la Liberación Nacional de El Salvador y fundador de la radio ¨Radio Venceremos¨, conversó sobre cómo una experiencia basada en la difusión de las ideas políticas de un grupo guerrillero, pasó después de la paz, a ser un mero medio de comunicación con amplio espectro publicitario.

Desde la experiencia chilena en la transición de la dictadura militar a una democracia parlamentaria, los medios de comunicación según **Laureano Checa**, director de la escuela de periodismo de la Universidad de Chile, incidieron públicamente en la convocatoria del plebiscito que terminó con la dictadura, para más tarde en la transición desaparecer de la esfera pública y privada.

Distintas **ponencias nacionales e internacionales** se escucharán esta semana en los distintos foros con el objetivo de reflexionar sobre un posible **escenario de paz y las consecuencias para la libertad de prensa**, así como de la necesidad de que los medios de comunicación incidan en el propio proceso de paz.

Entrevista con **Jineth Bedoya**, periodista del Tiempo,  víctima de secuestro y violación:

<iframe src="http://www.ivoox.com/player_ek_4460313_2_1.html?data=lZmjkpiVd46ZmKiakpyJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYrM7SqdXcjKfSxtTdpYampJDdx9fNs8Xd1NnOjcnJsIzIysra0tSPvYzqjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
Entrevista con **Laureano Checa,** director de la Escuela de periodismo de la Universidad de Chile:  
<iframe src="http://www.ivoox.com/player_ek_4460269_2_1.html?data=lZmjkpeafY6ZmKiak5WJd6KpmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYrsbZtsbVz9SYpc3Jp8Kfxc7fx8jYs9OfptjQ18rQpYzkxtfW0cnNt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
