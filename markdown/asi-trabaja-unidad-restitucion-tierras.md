Title: Lucha entre cifras y hechos, los resultados de la restitución de tierras
Date: 2018-09-10 17:37
Author: AdminContagio
Category: DDHH, Nacional
Tags: campesino, Comisión Colombiana de Juristas, Ley de Restitución de Tierras, Ley de Restitución de Tierras 1448
Slug: asi-trabaja-unidad-restitucion-tierras
Status: published

###### Foto: Contagio Radio 

###### 10 Sep 2018 

La **Comisión Colombiana de Juristas**, presentó recientemente un **informe llamado "Cumplir metas, negar derechos"** en donde revelan diferentes incosistencias en el proceso de restitución de tierras en el país, llevado a cabo por la Unidad de Restitución. Una de las cifras más alarmantes señala que de las  62.965 solicitudes hechas desde el 2015 hasta hoy, solo han sido aptas para inscripción administrativa 22.711.

De esos 62.965 procesos,  40.254 han sido negados, y de esas, solo en un 0.04% hubo un control de legalidad en donde se conocieron los motivos por los cuales no fue posible el reintegro de las tierras. De igual forma, **el 63% de las solicitudes no fueron aprobadas para registro**, por lo que no existió ningún tipo de respuesta hacia el campesino sobre por qué no fue posible la restitución, y tampoco existe registro de la utilidad de los terrenos tras la negación de devolución.

### **Las cifras son alarmantes** 

El periodo inicial de la restitución, que se dio entre 2010 al 2014, había un 78% de cumplimiento de la meta del procesamiento de solicitudes. **En el 2015, la meta cambió bruscamente de un 68% a un 179% en el proceso de solicitudes, sin que estas se vieran reflejadas en la realidad de las personas.**

En el informe, la Comisión explica que la estrategia para subir las cifras de cumplimiento consistió en cambiar los indicadores de metas, para beneficiar a la organización. Sin embargo, esta situación perjudica ampliamente a la población desplazada, puesto que las cifras se miden únicamente en resultados y no en hechos concretos de devolución de tierras.

Referente a por qué se niegan los registros la Unidad de Restitución de tierras afirma que existen 3 respuestas: la primera es la no inscripción de la solicitud, ya que es un requisito indispensable para el inicio de los trámites. No obstante, la no recepción de este proceso crece de forma anormal desde el 2015.

La segunda causa es el inicio no formal del estudio de solicitudes que ,Según la Comisión Colombiana de Juristas "un cuarto de la meta se basa en no estudiar los casos",  con lo que se descarta la posibilidad de la victima de ser participe de su derecho a la restitución; por último, el desistimiento, categorizado como inconstitucional, que se da cuando la URT no logra una comunicación con la víctima, y se desecha el caso.

### **¿Por qué no hay movilización de víctimas?** 

Alejandro Suarez, líder social, afirma que "la violencia en Colombia tiene como trasfondo la apropiación de la tierra, y las víctimas de esto son los campesinos" y alega la falta de información sistematizada. Además, cita la ley 1448, afirmando que "esta no fue creada para defender los derechos de los campesinos, si no para acatar a las exigencias de la élites políticas del país".

Asimismo, argumentó las causas de la falta de participación campesina "en un informe de Ricardo Sabogal para la Presidencia de la República, hablan del movimiento campesino como una insurgencia, nos ponen al mismo nivel del paramilitarismo, nos acusan de subversivos si queremos reclamar por nuestros derechos, por eso la víctima no se mueve,  por culpa del Pacto de Chicoral, los campesinos somos objetivo militar. Si no luchamos, es por que no podemos"

###  **¿Por que demanda la CCJ?** 

El pasado viernes 7 de Septiembre, la Comisión Colombiana de Juristas, interpuso una demanda contra el Decreto 1167, que cita " Las personas que pretendan ser incluidas en el Registro de Tierras Despojadas y Abandonadas Forzosamente contarán con tres (3) meses para presentar su solicitud, contados a partir de la vigencia de la presente modificación".

La demanda pretende alargar el plazo de cierre de la recepción de solicitudes de reintegración de parcelas que está programado para el 11 de octubre, argumentando las inconsistencias en los documentos que aún se encuentran en proceso de investigación. (Puede interesarle: [Octubre, fecha crucial para la restitución de tierras](https://archivo.contagioradio.com/octubre-limite-restitucion-de-tierras/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
