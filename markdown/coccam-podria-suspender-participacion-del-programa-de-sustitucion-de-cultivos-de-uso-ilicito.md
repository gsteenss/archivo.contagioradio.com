Title: COCCAM podría suspender participación del programa de sustitución de cultivos de uso ilícito
Date: 2018-03-26 09:33
Category: DDHH, Nacional
Slug: coccam-podria-suspender-participacion-del-programa-de-sustitucion-de-cultivos-de-uso-ilicito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/7d15abe6213b9e29ccb9bb6d04f6143e_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Archivo] 

###### [23 Mar 2018] 

[La Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana, informó que harán un ejercicio de socialización y consulta en las diferentes regiones para **definir si suspenden la participación** en el programa nacional de sustitución de cultivos de uso ilícito en el país. Esto teniendo de presente los reiterados incumplimientos del Gobierno Nacional y de la decisión del Ministerio de Justicia de presentar un proyecto de ley que pretende excluir del beneficio judicial a quienes ya se han acogido al programa de sustitución.]

[En un comunicado la organización reiteró que  el proyecto de trato diferencial penal para los pequeños cocaleros,  **pone en riesgo riesgo la implementación del punto cuatro**.  Afirmó que el proyecto "es una traición flagrante a los campesinos, afrodescendientes e indígenas que pusieron su confianza en el proceso de paz al inscribirse al programa". ]

### **Proyecto de ley perjudica a familias cocaleras** 

[En concreto, el proyecto de ley, **que tiene mensaje de urgencia,** reduce el área de las plantaciones de coca para ser tenidas en cuenta como pequeñas plantaciones. Esto quiere decir que antes, quién cultivara 3.8 hectáreas de siembra era considerado un pequeño cultivador y ahora se rán cultivos pequeños las áreas de plantaciones desde las  1.7 hectáreas. (Le puede interesar:["Cultivadores con más de 3.8 hectáreas no son narcotraficantes: campesinos"](https://archivo.contagioradio.com/cultivadores-de-coca-con-mas-de-3-8-hectareas-no-son-narcotraficantes-campesinos/))]

[Esto, como lo explica la COCCAM, perjudica a los campesinos que han manifestado **subsistir con plantaciones superiores a 1.7 hectáreas.** Además, el proyecto de ley, es considerado como “una traición al campesinado” teniendo en cuenta que el Gobierno Nacional había adquirido unos acuerdos en el Consejo Permanente de Dirección, donde se había establecido que “el área mínima de siembra de cultivos para que una familia pudiera subsistir es un máximo de 3.8 hectáreas”.]

### **Gobierno buscaría judicializar a pequeños cultivadores ** 

[De acuerdo con César Jerez, vocero de la Asociación Nacional de Reservas Campesinas ANZORC, el Gobierno Nacional se había comprometido “a sacar a los campesinos que son cultivadores de la cárcel **como antecedente del Acuerdo de la Habana**”. Sin embargo, “se estima que aún hay 3 mil campesinos en la cárcel por la ley 30 de tratamiento al tema del narcotráfico”.]

[Con este proyecto de ley, Jerez cree que Colombia está atendiendo a una **presión del Gobierno de Estados Unidos** para “encarcelar más campesinos y campesinas”. Dijo además que la reducción de las hectáreas complica la situación de las familias cultivadores pues “se incluye en el tratamiento penal a 300 mil familias que viven del cultivo de coca”. (Le puede interesar:["En el año se han asesinado a 17 líderes cocaleros en Colombia"](https://archivo.contagioradio.com/en-el-ano-han-asesinado-a-17-lideres-cocaleros-en-colombia/))]

[Le preocupa a las organizaciones que el proyecto de ley “fue concebido en un escritorio en Bogotá que “no resuelve el problema y por el contrario **criminaliza a los campesinos** cocaleros”. Además, manifestaron que los incumplimientos del Gobierno Nacional son reiterados y “ha implementado un plan de erradicación de forma paralela” que ha puesto en riesgo la forma de vida de los campesinos.]

<iframe id="audio_24808751" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24808751_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
