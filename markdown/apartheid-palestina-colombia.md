Title: Solidaridad con Palestina en la semana contra el Apharteid en Colombia
Date: 2017-03-27 13:13
Category: eventos
Tags: Apartheid Israel, colombia, Palestina, Semana contra el apartheid
Slug: apartheid-palestina-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/palestina-solidaridad-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Contagio Radio 

###### 27 Mar 2017 

**Del 28 al 31 de marzo se lleva acabo la Semana contra el Apartheid**, iniciativa que une los esfuerzos de los movimientos civiles, instituciones y universidades del mundo, para crear conciencia sobre las políticas, sistemas y proyectos colonialistas que los israelíes ejercen sobre el pueblo palestino desde hace más de 50 años.

En Colombia, la **Campaña de Boicot Desinversiones y Sanciones a Israel (BDS)**, ha preparado una serie de actividades académicas, culturales y políticas, con las que buscan unirse solidariamente a todos los movimientos a nivel internacional, entendiendo el Apartheid como como "**un sistema en el que a una población se le otorgan derechos diferenciados, y es lo que pasa con el pueblo palestino tanto los que viven en Israel como los que viven en territorio Palestino**", como asegura Isabel Riquel, vocera de la campaña.

Las actividades inician el **martes 28** en Bogotá a las 4 p.m., con un plantón contra los asentamientos israelíes en Palestina en la sede de la Embajada Israelí. El día **miércoles 29** se realizará una cátedra sobre la Historia Palestina en el Centro Cultural Gabriel Betancourt de la Universidad Pedagógica Nacional. **El jueves 30**, se realizará un concierto de entrada libre en Latino Power desde las 9 p.m., con la participación de las bandas African Soul, Sonido Sudaka y Suakata.

El cierre será el **viernes 31**, con un panel sobre la Militarización de la vida cotidiana en Palestina y Colombia, con las ponencias de Maren Mantovani, quien es Coordinadora de relaciones internacionales de StopTheWall.org, el profesor Victor De Currea-Lugo, la Colectiva La TULPA, la Misión Diplomática de Palestina - Colombia y BDS Colombia, en el auditorio CODEMA.

![apartheid](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/C7XcZywX0AIYndM.jpg){.alignnone .size-full .wp-image-38412 width="1024" height="728"}

BDS surge en 2005 a partir del llamado de **170 organizaciones de la sociedad civil Palestina**, en solidaridad para mejorar la situación que viven ante la pasividad de los Estados y frente a la defensa del derecho internacional y los derechos humanos de los palestinos. "Es algo que nos motiva mucho a nosotros  por se ellos mismos, palestinos y palestinas quienes nos piden que actuemos de esta forma. en la medida de lo posible acciones que les ayuden" asegura Riquel.

De manera pacífica, las acciones de BDS, **buscan que se reconozca también la responsabilidad de empresas e instituciones representantes de Israel en el mund**o, así como a empresas extranjeras que prestan servicios a Israel. Declaran ser un movimiento que no tiene ninguna afiliación política ni religiosa y por principio es no violento. Le puede interesar: [Las consecuencias ambientales de la ocupación israelí sobre palestina](https://archivo.contagioradio.com/las-consecuencias-ambientales-en-territorio-palestino-ocupado/).

<iframe id="audio_17789936" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17789936_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
