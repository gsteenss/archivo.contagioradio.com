Title: Huracán Irma continua su rumbo, siguen José y Katia
Date: 2017-09-07 14:01
Category: Ambiente, El mundo
Tags: cambio climatico, huracanes, irma, josé, katia, medio ambiente, república dominicana
Slug: huracan-irma-se-fortalece-siguen-jose-y-katia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/huracan-e1504810862575.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Univision Noticias] 

###### [07 Sept 2017] 

Por lo menos 20 mil personas de las provincias Samaná, Nagua y Punta Cana de República Dominicana, **han sido las mas afectadas por el paso del huracán Irma**.  Se prevee que también sean afectadas por paso del huracán José que se formó igualmente en las aguas del Atlántico.

La furia del huracán Irma, de categoría 5, y con vientos de 285 Km/h continúa expandiéndose por las naciones del Caribe mientras que Estados Unidos se prepara para su llegada. **Hasta el momento ha dejado un saldo de 8 muertos y más de un centenar de personas afectadas.**

Jorge Hernández, defensor de derechos humanos y habitante de República Dominicana indicó que las lluvias en ese país no se han detenido. **Dijo que el huracán afectó la zona noreste y sureste de la isla** pero que Haití se verá más afectado por los fuertes vientos.

Afirmó que en República Dominicana, **las personas más afectadas han sido los de bajos ingresos** cuyas casas se encuentran en malas condiciones. Ellos temen dejar sus hogares para albergarse en refugios y perderlo todo.

Según el PNUD, se estima que en la isla el 38.5% de las personas viven en condiciones de pobreza y el 7.9% en condiciones de pobreza extrema. Sin embargo, **“el gobierno lleva varios días tomando medidas a través de las diferentes instituciones** ubicando a las personas en refugios”.

**Detrás de Irma vienen Katia y José**

En el Caribe la preocupación se mantiene debido a que detrás de Irma vienen **los ya declarados huracanes de categoría uno José y Katia**. José tiene la misma trayectoria de Irma y llegaría primero a Puerto Rico que ya sufrió varias destrucciones. Por su parte, Katia se ha caracterizado por ser un huracán estacionario con tendencia a desaparecer.

De acuerdo con algunos medios de información, el Centro Nacional de Huracanes de Estados Unidos confirmó que el huracán Katia se formó en el Golfo de México mientras que José recorre la misma trayectoria de Irma y Harvey. **Los vientos de ambos huracanes superan los 120 kilómetros por hora.**

En el estado de Florida en los Estados Unidos se han preparado para recibir a Irma como **el huracán más fuerte después de Andrew en 1992.** Según los meteorólogos, el huracán estará en la Florida el sábado y el domingo.

### **Huracanes empeoran por el Cambio Climático ** 

Según el medio de comunicación peruano Gestión, los científicos han manifestado que, si bien los huracanes no son en sí producto de las afectaciones del medios ambiente, **esto sí los hace más fuerte** agudizando las destrucciones que dejan a su paso.

El medio, citando a un científico climático, asegura que **los huracanes consiguen su energía del calor del océano** y se potencializan con las altas temperaturas que tiene el agua del atlántico. Debido a la contaminación por la combustión del carbón, el gas y el petróleo, se calienta el planeta suministrando energía para la creación de tormentas que luego se convierten en huracanes.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
