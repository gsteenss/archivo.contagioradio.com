Title: EPM exigiría a damnificados que firmen documento que exonera de responsabilidades
Date: 2019-03-05 14:32
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Empresas Públicas de Medellín, Hidroituango
Slug: epm-exigiria-a-damnificados-documento-que-exonera-de-responsabilidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Hidroituango-e1486762164538.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Las 2 Orillas] 

###### [04 Mar 2019] 

Según una denuncia de Movimiento Ríos Vivos - Antioquia, Empresas Públicas de Medellín (EPM) estaría exigiendo a los afectadas de Hidroituango en Valdivia, Antioquia, la firma de un documento **que exonera a la empresa de sus responsabilidades como condición para recibir ayudas humanitarias,** necesarias para el sustento de estas familias.

Isabel Cristina Zuleta, integrante de la organización, afirma que esta presunta práctica de EPM se comenzó a detectar a principios de febrero, cuando las comunidades expresaron que los funcionarios de esta compañía los intervenían mientras esperaban en las filas dentro del Banco Agrario, para reclamar un pago de 1.400.00 pesos que es suministrado por la misma empresa. (Le puede interesar: "[Hidroituango, una tragedia anunciada por las comunidades](https://archivo.contagioradio.com/hidroituango-una-tragedia-anunciada/)")

La vocera sostiene que los funcionarios presionan a los damnificados a firmar un documento de dos páginas que exonera a la empresa de sus responsabilidades y declara que el firmante ha recibido el suministro de ayudas psicosociales y mercados, que según los habitantes es falso. Sin embargo, la organización social dice que **más de 140 familias han firmado el documento** desde febrero, por encontrarse en una situación crítica ecónomica, a raíz de las afectaciones de la represa.

"La gente se ve obligada a firmar este documento porque van dos meses sin recibir el pago, **no tienen comida, no tienen forma de pagar el arriendo, no tiene forma de regresar a sus viviendas** que están destruidas y tampoco tienen manera de seguir trabajando en lo que trabajan antes, en el barequeo y la pesca, porque todo eso quedó destruido," dijo Zuleta.

En respuesta, las comunidades interpusieron una queja a la Policía y lograron el retiro de estos funcionarios de las filas del Banco Agrario el mes pasado. No obstante, **los oficiales de EPM regresaron a principios de marzo** y nuevamente, el Movimiento ha acudido a la Policía para sacar a los funcionarios del edificio. (Le puede interesar: "[Desalojan familias afectadas por Hidroituango de albergues de EPM](https://archivo.contagioradio.com/desalojan-familias-afectadas-hidroituango-albergues-epm/)")

No obstante, las comunidades manifiestan que las medidas de la Policía son limitadas porque solo responden a quejas colectivos, no de individuos. "No debe ser así porque un solo llamado de un líder o una persona debe ser atentida," dijo Zuleta.

Por su parte, EPM no ha respondido a las denuncias de Movimiento Ríos Vivos - Antioquia. Al mismo tiempo, la Fiscalía, la Procuraduría y la Contraloría ha afirmado que estarán investigando estos hechos y estarán recibiendo testimonios de las personas afectadas.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
