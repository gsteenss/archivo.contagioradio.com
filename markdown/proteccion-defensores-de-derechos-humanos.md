Title: Protección a defensores de Derechos Humanos como garantía de Paz
Date: 2016-12-12 16:21
Category: DDHH, Nacional
Tags: Asesinatos de líderes Marcha Patriótica, asesinatos de líderes sociales, Asesinatos defensores de derechos humanos, Balance de Derechos Humanos, Oficina Internacional de Derechos Humanos Acción Colombia
Slug: proteccion-defensores-de-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Derechos-humanos-Mexico-e1472065664418.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Dic 2016] 

[La Oficina Internacional de Derechos Humanos Acción Colombia (OIDHACO) y otras 28 organizaciones afines, manifestaron a través de un comunicado su preocupación sobre la fragilidad de los acuerdos de paz debida a la **ola de asesinatos, ataques y amenazas contra defensores de derechos humanos**, líderes, lideresas comunitarias y miembros de movimientos políticos y la **negligencia del Estado colombiano para dar respuesta a las comunidades e iniciar las investigaciones pertinentes.**]

[En la misiva, aseguran que 31 de los más de 70 asesinatos a líderes **“han ocurrido desde la entrada en vigor del cese al fuego bilateral el 29 de agosto 2016”,** también resaltan que “desde su formación en el 2012, 128 miembros de Marcha Patriótica han sido asesinados debido a su afiliación política, y se teme que se **podría repetir una situación parecida al genocidio perpetrado en contra de los miembros de la Unión Patriótica”.**]** **

[El organismo internacional señala que los ataques “demuestran la ausencia de garantías de seguridad y de protección para que estas personas puedan llevar a cabo su legítima labor en pro de la paz, a través de movimientos rurales de base, asociaciones campesinas, proyectos de restitución de tierras y pedagogía para la paz".]

[Además, hacen un llamado al gobierno colombiano para que** "proteja a familias y comunidades que viven bajo una amenaza real y constante de ser objetivo de atentados, ataques y asesinatos”.**]

[Por lo anterior, la OIDHACO y sus miembros firmantes, instan al Estado colombiano para que se comprometa con algunos puntos que consideran fundamentales para lograr una paz estable y duradera:]

1.  [[Implementar de manera urgente el punto 3.4 del Acuerdo de Paz que contempla la creación de la Comisión Nacional de Garantías de Seguridad con el fin de desmontar las estructuras del paramilitarismo.]]
2.  [[Realizar investigaciones conclusivas para esclarecer la verdad sobre los hechos ocurridos en esta ola de violencia y juzgar y sancionar a los responsables, garantizando así, que no haya impunidad para violaciones perpetradas en contra de personas defensoras de la paz.]]
3.  [Garantizar la integridad física y psicológica de todas las personas defensoras de la paz, independientemente de sus alianzas políticas, permitiendo un proceso inclusivo y participativo en esta nueva etapa hacia la construcción de paz.]

### [Estas son las 28 organizaciones firmantes:] 

-   [Oficina Internacional de Derechos Humanos – Acción Colombia (OIDHACO)]
-   [ABColombia]
-   [Action des chrétiens pour l’abolition de la torture (ACAT)]
-   [Atelier]
-   [Broederlijk Delen]
-   [Brot für die Welt]
-   [Comité catholique contre la faim et pour le développement-Terre Solidaire  (CCFD-Terre Solidaire)]
-   [Coordinación Colombia-Europa-Estados Unidos (CCEEU)]
-   [Christian Aid]
-   [Cooperació]
-   [Diakonia]
-   [Fokus]
-   [Forum Syd]
-   [Fundación Sueca para los Derechos Humanos]
-   [Iglesia Sueca]
-   [International Caravana of Jurists - Colombian Caravana UK Lawyers Group]
-   [Justicia por Colombia]
-   [kolko]
-   [Misereor]
-   [Mundubat]
-   [Operation 1325]
-   [Organización Mundial Contra la Tortura (OMCT)]
-   [Plataforma Suiza por Colombia]
-   [Réseau France Colombie Solidarités]
-   [Rete italiana di solidarietà Colombia vive!]
-   [Soldepaz Pachakuti]
-   [Solidarité Socialiste]
-   [SweFOR]

###### [Reciba toda la información de Contagio Radio en][[su correo]](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por][[Contagio Radio.]](http://bit.ly/1ICYhVU) 
