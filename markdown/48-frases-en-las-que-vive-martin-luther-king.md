Title: 48 frases en las que vive Martin Luther King
Date: 2016-04-04 17:09
Category: Otra Mirada
Tags: Frases de Martin Luther King, Martin Luther King, Movimiento de Derechos Civiles
Slug: 48-frases-en-las-que-vive-martin-luther-king
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/luther-king.423432423d.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: John Littlewood 

##### [4 Abr 2016]

A las 6 de la tarde del 4 de abril de 1968, tras recibir varios impactos de bala, caía el cuerpo inerte de Martin Luther King, activista por los Derechos Civiles de los Afroestadounidenses, contra la guerra de Vietnam, la pobreza y la miseria. Un crimen de odio con el que se trato de acallar la voz de aquellos que pedían el fin a la segregación y la discriminación.

48 años después, los asesinos de King no lograron su objetivo y tanto sus palabras como ideales, permanecen en la mente y el corazón de todo quien se opone a la violencia, la indiferencia y la desigualdad de manera pacífica, entendiendo que desde el respeto por la diferencia es que se construye la democracia.

### Hoy recordamos 48 frases de [Martin Luther King](https://archivo.contagioradio.com/las-palabras-con-las-que-martin-luther-king-desafio-al-racismo/), una por cada año que ha pasado desde su partida. 

1-En el centro de la no violencia se encuentra el principio del amor.

2-Nada en todo el mundo es más peligroso que la ignorancia sincera y la conciencia estúpida.

3-He decidido seguir con el amor. El odio es una carga demasiado pesada de soportar.

4-Si ayudo a una sola persona a tener esperanza, no habré vivido en vano.

5-La injusticia en cualquier parte es una amenaza a la justicia en cualquier parte.

6-Si supiera que el mundo se acaba mañana, yo, hoy todavía, plantaría un árbol.

7-Debemos desarrollar y mantener la capacidad de perdonar. El que carece del poder de perdonar está desprovisto de la capacidad de amar. Hay algo bueno en lo peor de nosotros y algo malo en el mejor de nosotros. Cuando descubrimos esto, somos menos propensos a odiar a nuestros enemigos

8- El perdón no es un acto ocasional, es una actitud constante.

9-Debemos aceptar la decepción finita, pero nunca perder la esperanza infinita.

10-Nuestras vidas comienzan a terminar el día en que nos volvemos silenciosos sobre las cosas que importan

11-Debemos aprender a vivir juntos como hermanos o perecer juntos como necios.

12-Nadie sabe realmente porqué esta vivo hasta que saben porqué morirían.

13-La libertad nunca se da voluntariamente por el opresor; se debe demandar por el oprimido

14-Siempre es correcto el momento para hacer lo correcto.

15-Los que no buscan la felicidad tienen más probabilidades de encontrarlas, porque aquellos que la buscan se olvidan que la forma más segura de ser feliz es buscar la felicidad para otros.

16-Nada se olvida más despacio que una ofensa; y nada más rápido que un favor.

17-La oscuridad no puede expulsar a la oscuridad; sólo la luz puede hacerlo. El odio no puede expulsar al odio; sólo el amor puede hacer eso.

18- La pregunta de la vida más persistente y urgente es, ¿qué estas haciendo para otros?

19-La última medida de un hombre no es dónde se encuentra en momentos de comodidad y conveniencia, sino en donde se sitúa en tiempos de desafío y controversia.

20-No permitas que ningún ser humano te haga caer tan bajo como para odiarle.

21-La función de la educación es enseñar a pensar intensamente y pensar críticamente. Inteligencia más carácter – esa es la meta de la verdadera educación.

22-Un verdadero líder no es un buscador de consenso, sino un moldeador de consenso.

23-Tu verdad aumentará en la medida que sepas escuchar la verdad de los otros

24- La fe es dar el primer paso, incluso cuando no ves la escalera entera.

25-La paz no es solo una meta distante que buscamos, sino un medio por el que llegamos a esa meta.

26-No nos debemos concentrar solo en la expulsión negativa de la guerra, sino en la positiva afirmación de la paz.

27-La antigua ley del “ojo por ojo” deja a todos ciegos. Siempre es correcto el tiempo para hacer lo correcto.

28-No puede haber un gran decepción donde no hay un amor profundo.

29-La calidad, no la longevidad, de la vida de uno es lo que es importante.

30-La gente fracasa en llevarse bien porque se temen; se temen porque no se conocen; no se conocen porque no se han comunicado.

31-Hemos aprendido a volar como los pájaros, a nadar como los peces; pero no hemos aprendido el sencillo arte de vivir como hermanos.

32- El que acepta pasivamente la violencia esta tan implicado en el como el que ayuda a perpetuárlo. El que acepta el mal sin protestar esta cooperando con el.

33-Creo que la verdad desarmada y el amor incondicional tendrán la última palabra en la realidad.

34- Tengo un sueño que mis cuatro hijos vivirán un día en una nación donde no serán juzgados por el color de su piel, sino por el contenido de su carácter.

35-Quiero ser el hermano del hombre blanco, no su hermanastro.

36-Creo que la verdad desarmada y el amor incondicional tendrán la última palabra en la realidad..

39-Todo el mundo puede ser grande, porque todos pueden servir. No tienes que tener una carrera universitaria para servir…Solo tienes que tener un corazón lleno de gracia. Un alma generada por amor.

40-Nuestro poder científico ha superado a nuestro poder espiritual. Hemos guiado misiles y hombres equivocados.

41-Al final, no recordaremos las palabras de nuestros enemigos, sino el silencio de los amigos.

42-La ley y el orden existen por el propósito de establecer la justicia y cuando fracasan en este propósito se convierten en las presas que bloquean el flujo del progreso social.

43-El amor es la única fuerza capaz de transformar a un enemigo en amigo.

44-No hacemos la historia. Estamos hechos por la historia.

45-Si no puedes volar, corre; si no puedes correr, camina; si no puedes caminar, arrástrate, pero hagas lo que hagas, tienes que seguir hacia delante..

46-Cada hombre debe decidir si va a caminar en la luz del altruismo creativo o en la oscuridad del egoísmo destructivo.

47-La máxima tragedia no es la opresión y crueldad de las malas personas, sino el silencio de la buena gente.

48-El progreso humano no es ni automático ni inevitable … Cada paso hacia la meta de la justicia requiere sacrificio, sufrimiento y lucha.
