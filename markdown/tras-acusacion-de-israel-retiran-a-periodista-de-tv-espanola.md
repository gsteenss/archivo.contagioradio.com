Title: Tras acusación de Israel retiran a periodista de TV Española
Date: 2015-03-26 20:16
Author: CtgAdm
Category: El mundo, Otra Mirada, Política
Tags: Israel acusa a Yolanda Álvarez de colaborar con Hamas, TVE aparta de su cargo a periodista en Israel, Yolanda Álvarez periodista TVE
Slug: tras-acusacion-de-israel-retiran-a-periodista-de-tv-espanola
Status: published

###### Foto:Huffingpost.com 

La **TV pública española apartó de su cargo** a la periodista corresponsal en Israel y Palestina **Yolanda Álvarez,** tras **falsas acusaciones de Israel sobre colaboración con Hamas.**

La corresponsal de guerra cubrió la **invasión militar por parte de Israel en Gaza**, en Agosto de 2014, que **costó la vida a más de dos millares de Palestinos**. Fue entonces cuando la portavoz de la embajada de **Israel** en España Hamutal Rogel acusó a la periodista, de ser **"…correa de transmisión de los mensajes, cifras, imágenes y datos de Hamás…"** y de realizar "…crónicas dramatizadas…" sobre lo que estaba sucediendo.

Con estas palabras la embajadora calificó de inadmisible el cubrimiento de la periodista y de la TV española pública. El Consejo de Informativos de **RTVE** respondió en su momento que Israel intentaba **"amordazar y coaccionar a una periodista independiente, de un medio público español".**

La sección de **Reporteros sin Fronteras** también salió en defensa de la periodista calificando las acusaciones de Israel de ** "…perpetua actitud de intimidación…".**

Yolanda Álvarez ya es el **tercer periodista español que es apartado de su cargo en Israel**. En diciembre fueron trasladados los periodistas de **Efe** en Jerusalén, **Javier Martín**, y de **El País, Juan Gómez,** el primero apenas llevaba un año, y el segundo 6 meses, tiempo muy corto para la estancia de un periodista en una corresponsalía.
