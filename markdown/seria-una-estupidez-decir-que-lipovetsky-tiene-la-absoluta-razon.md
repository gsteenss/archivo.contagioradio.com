Title: Sería una estupidez decir que Lipovetsky tiene la absoluta razón
Date: 2016-02-22 12:20
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Gilles Lipovetsky, La estupidez: una reflexión urgente
Slug: seria-una-estupidez-decir-que-lipovetsky-tiene-la-absoluta-razon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/codigo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Telesur 

###### [22 Feb 2016]

#### **[Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)**

Asistí al foro llamado “La estupidez: una reflexión urgente” una iniciativa a mi juicio muy positiva que tuvo lugar en la Universidad Externado y se transmitió en vivo en la Universidad Nacional y en la Universidad Javeriana. Luego de escuchar la conferencia de Gilles Lipovetsky, puedo decir que es un autor de los que permite leer la estructura, de ese aparentemente “infalible”, pensamiento social contemporáneo, que como es sabido, tiene su meca en Europa occidental y en Norteamérica.

En esencia su conferencia sugirió el abordaje de siete ideas: 1) la posmodernidad, 2) las sociedades rebaño, 3) el consumismo como barbarie, 4) negar la existencia de mayor comunicación basada en tecnológica, 5) la muerte del arte, 6) equiparar el individualismo al egoísmo y 7) la información como conocimiento. Ideas que en definitiva para Lipovetsky: son una estupidez.

Bien, decir que tiene absoluta razón sería una estupidez. Perfecto, no se puede hablar de posmodernidad si todos los pilares modernos aún rigen geopolíticamente la vida económica y social de las naciones. También es verdad que a pesar de que el mundo parece tener una tendencia, para vestir, para comer, para “ser” etc. usted puede hacer de su vida una contraofensiva alternativa. Tampoco se puede negar que la tecnología (internet) democratizó funciones comunicativas como nunca antes habíamos tenido oportunidad.

Del mismo modo no se puede decir que el arte ha muerto, ya que por más de que se produzcan estereotipos en masa, existen pequeños grupos resistentes en cada país. Tampoco podemos negar que el internet informa, pero informar es una cosa y el conocimiento como función intelectual compleja es otra muy diferente… hasta allí, de acuerdo.

Pero (y aquí empieza el lío) Lipovetsky asegura que el consumismo es una respuesta a las aspiraciones de la humanidad, ¿será que por humanidad comprende esas sociedades que lo tienen todo a costa de otras? Esas aspiraciones, por ejemplo, se las da un Estado imperialista como Francia a sus ciudadanos cuando explota países en África, en Asia o bombardea oriente medio.

¿Cómo no equiparar consumismo con barbarie cuando para mantener estilos de vida con óptima libertad, como en Francia (como el de Lipovestsky), muchas personas en otros lugares tienen que morir bombardeados o perjudicados por decisiones geopolíticas? ¿cómo no equiparar consumismo con barbarie cuando aquí cualquiera pone el filtro de la bandera francesa en Facebook cuando explota una bomba en un café parisino, pero no pone el filtro de la bandera siria cuando explota una bomba en un centro comercial de Damasco? Y con esto no me refiero a una categorización de víctimas o negar la posibilidad de poner lo que se nos dé la gana en Facebook, sino a develar que el consumismo sí ha constituido barbaries por ejemplo desde el consumo mediático.

Casos como la omisión de reconocimientos mediáticos a víctimas de países en el olvido no significa que nadie las reconozca, pues afortunadamente hay medios alternativos, no obstante, “si nos ponemos la mano en el contexto”, aquí (Colombia) en nuestra sociedad el consumismo depende en gran medida de la influencia de grandes multinacionales presentes en los medios, en los bienes de consumo cotidiano, en el entretenimiento y lastimosamente también en la relación educación- mercado laboral.

¿Gozamos en Colombia de ese sincretismo típico del que nos hablan autores desde Europa, sobre la libertad y el consumismo como una aspiración humana? o más bien, “con la mano en el contexto” ¿aquí nos encontramos atravesando una carrera voraz de un “todos contra todos” en función de pocos, que hablan sobre un sueño de sociedad desarrollada, pero que estará pendiente hasta que tengamos plata y ejército para invadir así sea las fincas de nuestros terratenientes? Bueno, por ahora la única respuesta que se me ocurre es que es tan estúpido decir que todo consumismo es una barbarie, como decir que el consumismo no es barbarie.
