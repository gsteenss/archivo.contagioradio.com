Title: Familia Quilcué admite perdón de Min Defensa por asesinato de Edwin Legarda
Date: 2018-04-06 13:30
Category: DDHH, Nacional
Tags: Aida Quilcue, Ejército Nacional, ministerio de defensa
Slug: familia-quilcue-admite-perdon-de-ministeriod-e-defensa-por-asesinato-de-edwin-legarda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/anibal_fernandez_de_soto_e_hija_de_legarda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [06 Abr 2018] 

Tras 9 años y un desplante por parte del Ministerio de Defensa, el pedido de perdón por parte de esta institución, por fin llegó a Aida Quilcué, líder indígena, **luego de que su compañero Edwin Legarda fuese asesinado a manos del Ejercito el 16 de diciembre** de 2008, cuando se dirigía a recogerla. El acto se llevó a cabo en el Resguardo Indígena de Monterilla en Caldono, Cauca.

En principio este acto estaba planeado para realizarse el pasado 23 de febrero de este año, sin embargo, la delegación del Ministerio de Defensa que viajó no contó ni con el ministro ni con el viceministro, hecho que indignó a la comunidad indígena. (Le puede interesar: ["Ejército no llegó a acto de perdón por asesinato de comunero Edwin Legarda"](https://archivo.contagioradio.com/ejercito-reconocio-y-pidio-perdon-por-asesinato-del-comunero-indigena-eduin-legarda/))

Para Quilcué el pedido de perdón es un hecho agridulce, debido a que por un lado está la esperanza de haber encontrado justicia y por el otro, **el hecho de que se dió 3 años después de que un fallo del Tribunal contencioso Administrativo del Cauca**, responsabilizara a la Fuerza Pública por el asesinato de Legarda y le ordenará al Estado como medida de reparación, llevar a cabo el acto de perdón.

### **La petición de perdón de Ministerio de Defensa ** 

Ayer, al Resguardo indígena de Monterilla, llegó el vice ministro de Defensa Aníbal Fernández de Soto para pedir perdón por los actos cometidos el 16 de diciembre de 2008, cuando integrantes del Ejército Nacional de Colombia asesinaron a Edwin Legarda, compañero de la líder indígena Aida Quilcue. **En primeras versiones los abogados de los victimarios afirmaron que Quilcue había sido la responsable del homicidio**.

No obstante, la defensa de la líder, logró demostrar la participación de 5 soldados quienes fueron condenados a 40 años de presión y dos suboficiales que en segunda instancia quedaron absueltos. Todos fueron señalados de ser autores materiales de los hechos, sin embargo, no hay ninguna captura o investigación que señale a autores intelectuales.

###### Reciba toda la información de Contagio Radio en [[su correo]
