Title: Habrían hallado restos de 3 de los desaparecidos del Palacio de Justicia
Date: 2015-10-20 10:21
Category: Nacional
Tags: Palacio de Justicia
Slug: habrian-hallado-restos-de-3-de-los-desaparecidos-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/rueda-de-prensa-e1445361735668.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagioradio.com 

###### [20 Oct 2015]

Luego del anuncio presentado por parte del Instituto colombiano de Medicina legal, en el que corrobora la identificación de los restos mortales de Luz Mary Portela de Leon, Cristina del Pilar Guarín y Lucy Amparo Ovideo, desaparecidas durante la retoma del Palacio de Justicia, ocurrida en 1985; sus familiares expresaron el avance como "un paso importante".

El anuncio de la institución se da a poco tiempo de cumplirse un año de la sentencia de la Corte Interamericana de Derechos Humanos del 14 de Noviembre de 2014, en la que se declara responsable al estado colombiano por las violaciones a los DDHH cometidas durante el episodio, ordenandole investigar y esclarecer el paradero de los desaparecidos.

En rueda de prensa, los familiares instaron al gobierno a esclarecer las causas y responsables de sus muertes. Así mismo hicieron un llamado a continuar las investigaciones en el caso del magistrado Carlos Horacio Urán y el militante del M19 William Almonacid.

Los familiares exigieron adelantar investigaciones oportunas que permitan desvituar los falsos testimonios contrarios a las evidencias en video de la salida con vida de Cristina del Pilar Guarin del Palacio de Justicia, en que se han apoyado sus familiares desde su desaparición. Pesquisas que habian sido ordenadas por la Juez tercera especializada.

Por otra parte, en los casos de Luz Mary Portela León y Lucy Amparo Oviedo, los familiares de las víctimas insistieron en que las dos mujeres habrian salido con vida, apoyados en los testimonios de la periodista Julia Navarrete, el hijo de una de las víctimas, y algunas llamadas recibidas desde el interior de la casa del florero.

El anuncio de Medicina legal, representa un paso hacia la búsqueda de la verdad, en un proceso que ha tomado más de 30 años de lucha constante por encontrar justicia y reparación, donde el Estado y las Fuerzas Militares comprometidas en los hechos, no han sido efectivamnete sancionados.

Los familiares, solicitaron veeduría y acompañamiento por parte de organismos internacionales, ante lo que ellos consideran "injerencias indebidas de la inteligencia militar y profundas irregularidades" presentadas en cada uno de los casos.
