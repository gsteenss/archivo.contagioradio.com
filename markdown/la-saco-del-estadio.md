Title: La sacó del estadio
Date: 2016-05-17 13:14
Category: Camilo Alvarez, Opinion
Tags: Bogotá, Peñalosa, Rosa Elvira Cely
Slug: la-saco-del-estadio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/penalosa-la-saca-del-estadio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: hsb 

#### **[Camilo Álvarez Benítez](https://archivo.contagioradio.com/camilo-alvarez-benitez/)- [@camiloalvarezb](https://twitter.com/CamiloAlvarezB)** 

###### 17 May 2016 

[Los 4 meses pasados del gobierno de Peñalosa -muy pasados- permitió a las consultoras hacer un balance, dijeron que tiene problemas de imagen y que tal vez, imagen es lo único que le queda; aunque se le escurre entre los dedos como relleno fluido. Cada error se supera a sí mismo cada semana y sin mucho esfuerzo pierde credibilidad, su tabla de salvación: los resultados en concreto tampoco le suman puntos.]

[Él lo Intenta, quiere parecer un buen alcalde pero como las fotos de Consuelo Araujo con recicladores son más forzadas que entrar a transmilenio en hora pico. Con lo de este fin de semana, el alcalde la sacó del estadio, la sacó de la calle, la sacó de los parques, la sacó de los bares, un concepto jurídico avalado por su gobierno sacó a las mujeres de todas partes y las volvió culpables.]

De ahora en adelante las mujeres a sabiendas de lo insegura que es la ciudad, cundida de gente rara de comportamientos extraños, con bares abiertos y parques oscuros, recomienda este gobierno preferiblemente no salir para no exponerse y si lo hacen será bajo su total responsabilidad, si son agredidas puede que no lleguen ambulancias y  podrían hasta morir

[– Eso también pasa en otros lugares- dirá su secretario de salud.]

[Pero el burgues-maestre no paró ahí, de manera muy efectiva dijo que estaba indignado con la declaración; ¿cómo puede un alcalde indignarse con la postura del gobierno que dirige? A la vez pidió retractación y cacería de brujas. Literal como en la inquisición la responsabilidad no recayó sobre su segundo al mando-Hombre, sino sobre la contratista- mujer a la que le fue asignado el caso. Es cierto que tal concepto muestra la calidad ética “mejor para todos”, pero reafirma una idea del trato hacia las mujeres, en la mañana descarga la responsabilidad sobre la víctima en un crimen atroz, en la tarde vuelve victima laboral a la funcionaria que cumplió la función, porque ella “también se lo buscó” prueba fehaciente que Peñalosa terceriza hasta su responsabilidad y sus pantalones.]

[En ninguna de las dos situaciones hay responsabilidad política del mandatario; el ejercito de perfiles falsos en redes sociales intenta aun defender lo indefendible, su argumentos más fuertes: La culpa es de Petro, las normas no implican responsabilidad de todas las funciones; tecnicismos que lejos de mejorar la imagen dan cuenta real de cómo enfrentarán el desastre que empezamos a vivir. En cada error se señalara a otros, la seguridad se conseguirá limpiando grafitis y en los feminicidios las mujeres tienen la culpa; lindas ideas de quienes “recuperaron” el presupuesto de Bogotá mientras nos devuelven a las cavernas ya no solo con elefantes blancos, también con mamuts.]

[Hay que sumarle cada desacierto y cada retroceso durante un año para que corra el revocatorio, las movilizaciones ya empezaron y seguro irán creciendo, recogeremos las firmas, miraremos con lupa cada paso, no permitiremos la venta de lo poco que nos queda y seguro defenderemos lo que produce rentabilidad para los y las bogotanas. Agotaremos las instancias hasta el final y nos reiremos cuando en los tribunales paradójicamente una o más abogadas mujeres muy mujeres lo dirán:]

[“Peñalosa sale porque se lo buscó”]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
