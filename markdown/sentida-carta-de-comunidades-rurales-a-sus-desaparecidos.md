Title: Sentida carta de comunidades rurales a sus desaparecidos
Date: 2020-05-29 20:25
Author: CtgAdm
Category: Actualidad, Nacional
Slug: sentida-carta-de-comunidades-rurales-a-sus-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/familiares-desaparecidos-Es-1132x670.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En el cierre de la **[Semana Internacional del Detenido Desparecido](https://archivo.contagioradio.com/semana-del-detenido-desaparecido-una-oportunidad-de-luchar-para-que-este-flagelo-no-se-repita/)** **más de 110 comunidades ubicadas de varias regiones del país presentaron una sentida carta**, expresando que la ausencia sigue siendo motor para luchar contra este flagelo que, se considera, en Colombia alcanza la escanadalosa cifra de 100.000 personas[víctimas.](https://www.justiciaypazcolombia.com/colectivo-ram-voces-para-los-desaparecidos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A continuación compartimos el contenido completo de la carta:

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### CARTA ABIERTA 6

<!-- /wp:heading -->

<!-- wp:quote -->

> ¿Adónde van los desaparecidos?  
> Busca en el agua y en los matorrales  
> ¿Y por qué es que se desaparecen?  
> Porque no todos somos iguales  
> ¿Y cuándo vuelve el desaparecido?  
> Cada vez que los trae el pensamiento  
> ¿Cómo se le habla al desaparecido?  
> Con la emoción apretando por dentro  
> Rubén Blades. Desapariciones.

<!-- /wp:quote -->

<!-- wp:paragraph -->

Otra semana más en su presencia. Nunca en nuestro olvido. Nuestros desaparecidos se expresan, se dicen de muchas maneras, están en nuestros corazones. Nos hablan a veces en sueños. Se nos aparecen en la música. Su cuerpo físico se ha transformado en nosotros. En el sonido de un pájaro o nos arrullan en el sonido de un río.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Están en los recónditos lugares sagrados de nuestros territorios. Les buscamos y les hallamos de muchas maneras, la que sus verdugos menos se imaginan. Aparecen una y otra vez, más allá del silencio de algunos medios de información que nos bombardean con sabias mentiras para calmar la búsqueda. Sí, ellos viven. Están más acá y más allá de lo que los perpetradores de la desaparición quisieron.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Siguen vivos, sin desaparecer, están visibles entre las palmas que alguna vez empresarios criminales quisieron imponer. Están en fosas que la madre tierra conserva a pesar de aplanadoras de desarrollo extractivo o de agroindustrias pretenden negar. Están en los cauces de los ríos, en las hidroeléctricas. Están en cementerios clandestinos, en cementerios despojados con retroexcavadoras o en fosas comunes y algunos en cementerios públicos cercanos a nuestros territorios. Ellas y ellos están muy acá, en los espacios que sus victimarios nunca podrán tocar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nuestros desaparecidos y desaparecidas están acá. Son pasado, presente y horizonte. Les lloramos, pero también con ellos reímos, y construimos un nuevo vivir. Ellos son parte de nuestra memoria. Son nuestros hijos, o nuestros hermanos, nuestros tíos o nuestras padres y madres, son nuestras lideresas y líderes, los reclamantes de tierra que arrancaron con su cuerpo físico, pero nunca con sus sueños e ideas, que hoy recreamos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Son nuestros vecinos, son los tejedores, o sembradores, los motoristas, quienes están en la tienda o en las iglesias, comerciantes, locutores, agricultores, son las mujeres dadoras de vida. Sí son muchos oficios, profesiones, roles. Son y han sido nuestra historia. Nuestros detenidos, desaparecidos son parte de la lista de los más de 80.0000 que se cuentan, porque hay muchos sin contar, pero si en la memoria familias y en la memoria social que se hace más allá de ideas y de conceptos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Gracias a ellos y ellas descubrimos con un altísimo costo, el sentido de vivir asumiendo las perdidas intangibles. Aprendimos los dolores que deshacen en alma en miles de trizas. Gracias a ellos asumimos los miedos, rompimos cobardías para destapar el rostro de hierro, los testaferros, que hablan y dicen defender y amar la democracia, a la medida de unos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ustedes son parte de nuestra historia. Están acá marcando límites y posibilidades. Están mostrando los límites de la barbarie. Hoy les decimos que les honramos entre un aberrante Estado de Colombia que, deshecha la Vida por un amor ciego a la democracia excluyente, a la democracia de la desigualdad, a la democracia del más fuerte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, esos beneficiarios, planificadores y ejecutores olvidan, aunque se crean sobreseguros de haber hecho un criminal trabajo perfecto, que el amor es más fuerte, el sano amor emancipa a la vida de esa pretensión de desaparecer, y que, aunque se crea que el olvido fue la salida, de una generación a otra, esos responsables llevan en su historia la imposibilidad de un bello existir. Somos más que nuestros cuerpos. Somos más que sobreviviente de los desaparecidos, somos más que esa relación destructiva que crearon los responsables.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por eso hoy les decimos en la sabiduría ancestral nosotros nos hemos liberado del sufrimiento y en honor de ellos amamos la vida. En ustedes planificadores, ejecutores y beneficiarios queda todo. Ninguna riqueza amasada con nuestros desaparecidos, ninguna ventaja política ni militar es compensada con el alma de alguno de nuestros desaparecidos. Sus mentiras, sus silencios, sus negaciones están llevan a la destrucción de sus propias vidas y la de sus familias. Lo que se desconoce, vuelve a aparecer, una y otra vez.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Su democracia de progreso, su democracia de injusticias, su democracia de la seguridad se deshace en las almas de esos seres. La mayoría de ellas y ellos han sido torturados, sus cuerpos convertidos en blanco del exterminio y aleccionamiento colectivo. Han olvidado que el cuerpo se transforma, que los sueños se hacen de otras formas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin esperar lo que se entiende por justicia en las formalidades de las democracias. Estamos creando otras formas de justicia. Mientras las seguimos afirmando desde una perspectiva de las democracia profunda nos sumamos en estos día de honra de nuestros desaparecidos, exigiendo al Estado colombiano, en cabeza del presidente Duque: Ratificar plenamente la Convención Internacional contra las Desapariciones Forzadas y la entrada en vigencia de la Competencia del Comité contra la Desapariciones Forzadas de Naciones Unidas para contribuir en la búsqueda, localización y justicia que se encuentran en responsabilidad de la Fiscalía General de Colombia y la Unidad de Búsqueda de Personas dadas por Desaparecidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy rendimos homenaje y escribimos sus nombres un signo de su historia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy cada uno de ellos está en nuestro corazón.  
Hoy están en un marco en que sus letras se escriben.  
Hoy están acá en una ausencia presente.  
Hoy nos permiten vernos en nuestros propios rostros que son historia  
Hoy nos permiten ver atrás, ver el presente, y el horizonte.  
Hoy están acá, allá, su presencia está, su ausencia es presencia en nosotros.

<!-- /wp:paragraph -->
