Title: Censo agrario evidencia que aumenta  la concentración de la tierra en Colombia
Date: 2015-08-14 17:29
Category: DDHH, Entrevistas, Política
Tags: ANZORC, Censo Agrario, CNA, Coordinador Nacioinal Agrario, Dario Fajardo, Precios del Dolar, Unidad Agrícola Familiar
Slug: censo-agrario-evidencia-que-aumenta-la-concentracion-de-la-tierra-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/censo_agrario_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: youtube 

<iframe src="http://www.ivoox.com/player_ek_6748447_2_1.html?data=l5yhmpmYe46ZmKiakpuJd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8bi1NSYw8zWpdPd0JDS2M7Iqc%2FXysaY09rJb8Lpzsrb1saPaaSmhqadzsaPp9DixMrb1tfFp8qZpJiSpJjScYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Darío Fajardo, analista y experto en tierras] 

###### [14 Ago 2015]

Los primeros avances publicados del Censo Agrario demuestran que continúa la tendencia a la concentración de la propiedad territorial. Por ejemplo las **fincas de más de 500 hectáreas en Colombia, es decir el 41.1% de la tierra está en manos del 0.4%** de la población y el resto en manos del 58% de las personas. Según Darío Fajardo todo apunta a que esa situación se agudizaría.

Para el analista, el problema radica en que el modelo planteado en el Plan Nacional de Desarrollo es perverso, porque “*no solamente no están presentes elementos que pudieran incidir en reducir ese proceso de concentración de la propiedad sino que están agravando tendencias que ya se venían presentando previamente*”.

Lo que está haciendo el gobierno es desconocer la **Unidad Agrícola Familiar** y propiciar el acaparamiento de tierras por grandes empresas multinacionales. Además es preocupante la distancia que están tomando las políticas para las tierras de lo que se está acordando hasta el momento en el proceso de conversaciones de paz respecto a ese punto, agrega Fajardo.

Frente a los acuerdos en el proceso de paz, Fajardo afirma que estas cifras iniciales del Censo Agrario hacen evidente que el proceso de acumulación de la tierra, que ha participado de manera determinante en el inicio y la profundización de la guerra, y por otra parte, se evidencia que no hay disposición en lo concreto “***puede que hay muchos discursos pero no hay disposición en lo concreto para alterar esas tendencias***”

Así las cosas se demuestra que no es suficiente con que se pongan sobre la mesa los problemas de las tierras en Colombia, sino que es necesario que la iniciativa la tomen los sectores populares para cambiarle la ruta al país. **Lo que se está planteando es la ruta del fortalecimiento de la movilización social y su capacidad de presión** para producir cambios reales en la situación actual.

Además Fajardo plantea que también hay otro agravante es que hay sectores empresariales, que se están beneficiando del precio del dólar, puesto que la importación en Colombia ha crecido hasta el punto de que más del **50% de los productos agrícolas son importados**. Esto afecta y debe fomentar la movilización de sectores urbanos que son los que pagan los altos precios, producto de la devaluación del peso frente del dólar.
