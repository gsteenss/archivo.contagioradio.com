Title: "No hay ninguna Zona Veredal lista para recibir a las FARC"
Date: 2016-12-14 13:06
Category: Nacional, Paz
Tags: FARC, Gobierno, paz, Zonas Veredales
Slug: no-hay-zona-veredal-lista-parra-las-farc-33716
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/ZVT.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mesa de Conversaciones] 

###### [14 Dic. 2016] 

El pasado 13 de Diciembre la guerrilla de las FARC-EP **informó acerca del relevo de 5 mandos de los comandantes de frente y adicionalmente llamó a integrantes de esa guerrilla a que retornen a las filas y se unan al camino de la paz.**

Según informó dicha agrupación a través de un comunicado, esta decisión fue motivada por la conducta reciente de esos 5 mandes, que los ha llevado a entrar en contradicción con su línea político-militar.

**Para Carlos Medina, analista político esta decisión obedece al momento coyuntural por el que pasa no solo el país, sino también dicha guerrilla** “de estos procesos que se comienzan a manifestar, me parece que lo que es relevante es que las FARC han tomado una postura, buscando que se disciplinen los procesos locales y regionales”.

Según Medina, el propósito es que las estructuras puedan articularse en las dinámicas en las que se encuentran actualmente que es la marcha hacia las Zonas Veredales de Localización Transitoria.

Luego de las diversas dificultades que ha presentado el proceso de paz a partir del resultado del plebiscito del 2 de Octubre, del proceso de renegociación de los acuerdos, el “Fast Track” con el que se espera producir las seguridades jurídicas para los guerrilleros de las FARC-EP **para Medina “este tipo de decisiones eran de esperarse”**.

Sin asegurar con lo anterior, dice Medina, que exista un “resquebrajamiento” al interior de las FARC-EP **“yo creo que esto se va a superar, que el grueso de las FARC-EP va marchar hacia las Zonas Veredales Transitorias de Normalización y que lo mandatado por la décima conferencia se va a cumplir”.**

Sin embargo, luego de más de 14 días de haber declarado oficialmente el inicio del Día “D” **existen retrasos en la preparación de dichas zonas para el arribo de los y las guerrilleras, así como de la comunidad internacional que acompañará el proceso.**

**“La burocracia no ha permitido que la adecuación de estas Zonas Veredales se lleve a cabo de manera efectiva”** y agregó “cuando hablamos de las Zonas Veredales Transitorias no es solamente hablar de la construcción de las habitaciones que tienen que ser dignas, no pueden seguir en campamentos, sino también de espacios sociales, de restaurantes, de escuelas, de puestos de salud, de oficinas (…) **hablamos de un retorno para la construcción de futuro individual, colectivo, familiar de las FARC-EP”. **Le puede interesar: [Quedan definidas 20 Zonas Veredales y 7 Puntos Transitorios](https://archivo.contagioradio.com/quedan-definidas-20-zonas-veredales-y-7-puntos-transitorios/)

Ante la desinformación que se ha suscitado por el número de guerrilleros que podrían movilizarse hacia dichas Zonas Veredales, Medina manifestó que son cerca de 15 mil hombres y mujeres “**pero** **la verdad es que hasta que no se cuenten con las listas que las FARC debe desarrollar para que la Universidad Nacional haga el censo no se puede dar un dato certero”.**

Y concluye diciendo que hay que tener “paciencia, que el proceso se dé” aunque **preocupa que “no se encuentra ninguna Zona Veredal habilitada en las condiciones que se requieren para que reciban a los miembros de las FARC**, a los organismos internacionales que harán el seguimiento y la verificación. Le puede interesar: [Iglesias y organizaciones de fe de DiPaz acompañarán las Zonas Veredales Transitorias](https://archivo.contagioradio.com/iglesias-y-organizaciones-de-fe-de-dipaz-acompanaran-las-zonas-veredales-transitorias/)

<iframe id="audio_14984162" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14984162_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).

<div class="ssba ssba-wrap">

</div>
