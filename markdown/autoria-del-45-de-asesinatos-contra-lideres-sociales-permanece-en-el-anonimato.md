Title: El 84% del territorio colombiano registra violencia contra líderes sociales
Date: 2018-12-18 16:24
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: Amenaza a líderes sociales, lideres sociales
Slug: autoria-del-45-de-asesinatos-contra-lideres-sociales-permanece-en-el-anonimato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/imágenlíderes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 18 Dic 2018 

[***'¿Cuáles son los patrones? Asesinatos de Líderes Sociales en el Post Acuerdo**',* es el más reciente informe del Instituto de Estudios Políticos y Relaciones Internacionales de la Universidad Nacional **(IEPRI)**, que advierte una repetición no accidental, invariable y continua de la violencia cometida contra los líderes sociales y defensores de derechos humanos. una población civil cuyos asesinatos han ido en aumento desde la firma del acuerdo de paz.]

[El informe,](https://verdadabierta.com/wp-content/uploads/2018/12/cuales_son_los_patrones.pdf?fbclid=IwAR3SKKohM1RdcRyY6tObu-cGKg2QvAVYS5SOLkeCYRiS6X19MbIxS5_fIMQ) es la continuación de un trabajo realizado por [el **Centro de Investigación y Educación Popular (CINEP), el Instituto de Estudios para el Desarrollo y la Paz (INDEPAZ) y la Comisión Colombiana de Juristas (CCJ)** y ]reúne datos recopilados desde el día que se firmó el acuerdo de paz entre Gobierno y Farc,  24 de noviembre de 2016 hasta el 31 de julio de 2018 que  dan luz a la grave situación que viven los líderes sociales en Colombia; algunos de los datos más reveladores son:

-   Se registraron 257 violaciones al derecho a la vida,  de las víctimas, 215 pertenecían al género masculino, 38 al género femenino y 4 eran población LGBTI.
-   [[[[[[El  documento evidencia que las agresiones y asesinatos contra líderes sociales se presentaron en 27 de los 32 departamentos del país y en 142 municipios, lo que significa que el 84, 37% del territorio nacional ha sido afectado por este fenómeno, de estos 27 departamentos el más afectado fue el **Cauca** con 44 asesinatos, seguido de **Antioquia** con **40**  y **Norte de Santander** con 19.]]]]]] 
-   [[[Desde la firma del Acuerdo de Paz se han registrado **27 asesinatos de líderes sociales involucrados en la sustitución de cultivos de uso ilícito**. De los cuales uno ocurrió en el 2016, 12 en el 2017 y 14 entre enero y julio 31 de 2018, datos que revelan un aumento de violencia de derechos contra esta población en particular.]]] De los 257 líderes y defensores de derechos asesinados, al menos 27  (10,5%) habían recibido amenazas previas y de un total de 257 violaciones al derecho a la vida, al menos 106 (41.24%) fueron cometidas en los sitios de vivienda de las víctimas.

<!-- -->

-   [[[[[La información que existe sobre la presunta autoría de estos crímenes es limitada, en 118 casos (45,91%) no se han esclarecido quiénes fueron los responsables ni materiales ni intelectuales, mientras que en otros 35 casos (el 13,61%) se pudo determinar que se trató de un grupo armado.]]]]] 
-   Continuando con los responsables de dichos crímenes, en los 106 casos de los que se conoce quiénes fueron los autores, **44 atribuidos a grupo paramilitares**, [[seguidos de 19 casos atribuidos a las disidencias de las Farc, 14 casos atribuidos a la fuerza pública, 8 al ELN, otros 4 a grupos de seguridad privada y 2 al EPL.]]

<!-- -->

-   Desde la firma del Acuerdo de paz, la organización social que mayor número de ataques y asesinatos ha sufrido contra líderes y dirigentes ha sido **Marcha Patriótica**, quienes denuncian que desde su creación en 2012 hasta julio de 2018 han perdido a 169 de sus integrantes.

A través de dicho informe, el IEPRI sugiere detener el incremento de militarización en las regiones, pues en lugar de frenar la criminalidad la han incrementado, además de implementar políticas públicas que den garantías para el ejercicio de defensa de los DDHH y sobre todo reconocer a los defensores como actores importantes y valiosos para la construcción de paz. [(Le puede interesar En los primeros 9 meses de 2018 fuero asesinados 109 líderes sociales) ](https://archivo.contagioradio.com/en-los-primeros-9-meses-de-2018-fueron-asesinados-109-lideres-sociales/)

###### [Reciba toda la información de Contagio Radio en [[su correo] 
