Title: En las cárceles colombianas no existe un enfoque de género
Date: 2019-02-16 11:20
Author: AdminContagio
Category: Expreso Libertad
Tags: cárceles colombia, enfoque de género, prisioneras
Slug: carceles-colombianas-enfoque-genero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/mujeres-en-la-carcel-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CDN 

###### 12 Feb 2019 

El más reciente informe del Comité Internacional de la Cruz Roja (CICR), evidenció la **urgencia de que en Colombia se adopte el enfoque de género en el tratamiento penal a las mujeres**. De acuerdo al documento, en los últimos 3 años la población carcelaria de mujeres a aumentado en más de un 429% y las gran mayoría de ellas hacen parte de estratos 1 y 2, han sido víctimas de violencia intrafamiliar y tienen un bajo nivel de escolaridad.

En este programa del Expreso Libertad, hablamos con **Oscar Ayzanoa, asistente penal del CICR** quien se refirió a la urgencia de un tratamiento con perspectiva de género que comprenda los contextos de violencia y vulnerabilidad a la que están expuestas las mujeres.

<iframe id="audio_32514970" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32514970_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
