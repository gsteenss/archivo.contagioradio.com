Title: Comunidad de Mirandela debate entre preservar parque o permitir la construcción de un Colegio
Date: 2017-02-21 16:22
Category: Movilización
Tags: Bogotá, Mirandela
Slug: comunidad-de-mirandela-se-debate-entre-preservar-su-parque-o-permitir-la-construccion-de-un-colegio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/mirndela.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comité Mirandela Residencial ] 

###### [21 Feb 2017] 

Habitantes del barrio Mirandela, en la localidad de Suba, **denuncian la construcción de un colegio por parte de la Alcaldía de Bogotá en un predio que actualmente es usado como parque público**, sin que se haya hecho un proceso de socialización de la obra y afectado a aproximadamente 400 residentes que están alrededor del parque.

Desde el 20 de febrero personas de la comunidad han hecho presencia pacífica para impedir la  construcción del Colegio, que se viene adelantando desde febrero, sin embargo, también han **denunciado el accionar de fuerza desmedida por parte del ESMAD para retirarlos del lugar. **Le puede intersar: ["Enrique Peñalosa desconoce reservas en los Cerros Orientales"](https://archivo.contagioradio.com/enrique-penalosa-sigue-desconociendo-reservas-forestales-en-los-cerros-orientales/)

De acuerdo con Jaime Forero, este parque es el punto de encuentro de familias en Mirandela y una zona verde que amortigua la contaminación **“nosotros no peleamos porque se construya el colegio, peleamos porque se quiere acabar con un parque existiendo más predios en donde llevar a cabo el proyecto”.**

Además Forero agrega que la construcción de este colegio no subsana la crisis de 12 mil cupos estudiantiles que hay en el barrio porque solo generaría  50 cupos más. **De igual forma, indica que la estructura que se edificara puede ser una tercera sede del Colegio Nueva Zelandia, que presenta fallas estructurales.** Le puede intersar: ["Plan Zonal del Norte afectaría gravemente franjas de conectividad"](https://archivo.contagioradio.com/plan-zonal-del-norte-afectaria-gravemente-franjas-de-conectividad/)

Los habitantes ya han puesto varios derechos de petición, denegados todos, motivo por el cual seguirán movilizándose pacíficamente para evitar que se lleve a cabo la construcción de este Colegio en el parque de la comunidad.

<iframe id="audio_17148502" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17148502_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
