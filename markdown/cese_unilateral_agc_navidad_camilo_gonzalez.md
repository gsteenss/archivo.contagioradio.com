Title: "Declaratorias como las de las AGC deben tener una continuidad": Camilo González
Date: 2017-12-14 18:39
Category: Nacional, Paz
Tags: AGC, Autodefensas Gaitanistas de Colombia, cese unilateral
Slug: cese_unilateral_agc_navidad_camilo_gonzalez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/Grafitis-AGC-San-Jose-Apartado-Comunidad-Paz_19-04-16_Foto-Acasa-e1513291344474.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia de prensa IPC] 

###### [14 Dic 2017] 

Ante la declaratoria de las Autodefensas Gaitanistas de Colombia, (AGC), sobre su cese al fuego unilateral, Camilo González Posso, analista y director de Indepaz, ve con positivismo la noticia, expresando que "**todo lo que contribuya a la disminución de actos de violencia hay que valorarlo en el servicio de la vida".** A su vez afirma que** **estos anuncios deben tener una continuidad, ya que pueden desembocar en el desescalamiento de hechos violentos en el país.

De acuerdo con el comunicado de las AGC, "A partir de esta Navidad, en donde los corazones se llenan de esperanza por un país mejor, los colombianos merecemos la oportunidad por primera vez en la historia, a vivir en adelante sin la zozobra del conflicto armado, mientras éste se resuelve definitivamente. Que las familias se integren alrededor del regocijo de la natividad.  Por esta razón, declaramos un cese unilateral de acciones militares ofensivas en todo el territorio nacional **a partir del trece (13) de diciembre del presente año".**

[En el mismo texto, el grupo armado invita además a los grupos disidentes de las FARC y las BACRIM, LOS PELUSOS , LOS PUNTILLEROS, LAS OFICINAS, y en general a todos los actores armado  para parar la violencia y buscar mecanismos de salida para la paz. Sobre el ELN, **las AGC exhortan a esa guerrilla a se sumen a su llamado,** en medio de lo que son los enfrentamientos entre ambas partes por disputas territoriales.]

### **Un cese también con la población civil** 

Teniendo en cuenta las denuncias realizadas por 25 líderes sociales que llegaron a Bogotá, provenientes del Bajo Atrato y el Urabá Chocoano, el analista reconoce que no todas las estructuras están subordinadas al mando de alias 'Otoniel', y por tanto **existen "unos negocios de fondo" y por eso no van a cesar automáticamente las acciones violentas.**

En ese sentido, González dice que será trascendental que el Estado debe cumplir su obligación para brindar garantías y protección a las comunidades, acompañado de una estrategia particular y todos los mecanismo de contención necesarios para proteger a la población civil en los territorios.

### **¿Diálogo con las AGC?** 

[Para González Posso, este anuncio de las AGC, crea **"un ambiente propio para entablar diálogos alrededor del sometimiento a la justicia"**. El analista considera que dicha estructura podría estar buscando algún tipo de negociación con el gobierno colombiano para lograr tener un tratamiento similar al de las AUC, con beneficios especiales transicionales.]

<iframe id="audio_22674264" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22674264_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

<div class="osd-sms-title">

Compartir:

</div>

</div>

 
