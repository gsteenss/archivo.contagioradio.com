Title: Ordoñez, Uribe, Pastrana pasemos la página: Paz a la Calle
Date: 2016-11-18 18:11
Category: Nacional, Paz
Tags: Alejandro Ordoñez, Alvaro Uribe, Andrés Pastrana, Paz a la Calle
Slug: ordonez-uribe-pastrana-pasemos-la-pagina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/uribe-pastrana-ordonez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [18 Nov. 2016] 

Varias han sido las intervenciones en las que el ex procurador Alejandro Ordoñez, el senador Álvaro Uribe y el ex presidente Andrés Pastrana se han referido al tema de la paz y han sido promotores abiertos del NO en el plebiscito. Sus alocuciones no son bien recibidas por algunos sectores, mientras que otros han aprovechado las disidencias para construir.

Ese es el caso de **Paz a la Calle, que recientemente ha enviado una carta a estos tres personajes públicos instando a que ya es la hora de pasar la página** “Nos dirigimos a ustedes quienes han actuado como promotores del NO y que han tenido sus motivos para pedirles, como jóvenes, que trabajen en pro de un país diferente".

En la comunicación también aseguran que la idea es hace una **invitación a Uribe, Pastrana y Ordoñez a que** “**actúen con grandeza y generosidad, sobre todo pensando en el país que le dejan a esta nueva generación**. Mientras más voces haya, mejor”.

Para esta iniciativa ciudadana, la oportunidad que se presenta con el acuerdo definitivo dado a conocer por el Gobierno Nacional y la Farc el pasado 13 de Noviembre, permitirá poner fin prontamente al conflicto armado y así “evitar los riesgos de nuevos enfrentamientos y la muerte de más colombianos”. Le puede interesar: [Según comunidad, Ejército sí habría violado cese bilateral al fuego](https://archivo.contagioradio.com/segun-comunidad-ejercito-si-habria-violado-cese-bilateral-al-fuego/)

Así mismo, **han invitado a Ordoñez, Uribe y Pastrana a actuar “con grandeza y generosidad, sobre todo pensando en el país que le dejan a esta nueva generación. Mientras más voces haya, mejor”. **Le puede interesar: [Paz a la Calle se sigue movilizando en toda Colombia](https://archivo.contagioradio.com/paz-a-la-calle-se-sigue-movilizando-en-toda-colombia/)

[Carta a Alejandro Ordoñez, Álvaro Uribe y Andrés Pastrana del Campamento por la Paz](https://www.scribd.com/document/331576166/Carta-a-Alejandro-Ordonez-Alvaro-Uribe-y-Andres-Pastrana-del-Campamento-por-la-Paz#from_embed "View Carta a Alejandro Ordoñez, Álvaro Uribe y Andrés Pastrana del Campamento por la Paz on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_19369" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/331576166/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-ZmVqUEctFotQ1MSQtjzi&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
<iframe id="audio_13821074" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13821074_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
