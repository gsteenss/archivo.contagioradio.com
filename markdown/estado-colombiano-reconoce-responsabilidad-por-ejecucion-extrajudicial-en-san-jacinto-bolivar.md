Title: Estado colombiano reconoce responsabilidad por ejecución extrajudicial en San Jacinto, Bolívar
Date: 2016-06-10 14:04
Category: Nacional
Tags: CIDH, colectivo de Abogados José Alvear Restrepo, ejecuciones extrajudiciales en Colombia
Slug: estado-colombiano-reconoce-responsabilidad-por-ejecucion-extrajudicial-en-san-jacinto-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Reconocimiento-responsabilidad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CAJAR ] 

###### [10 Junio 2016] 

En el marco de un Acuerdo de Solución Amistosa firmado el pasado 6 de abril, entre el Estado colombiano y la familia del campesino Omar Zúñiga Vásquez, este sábado en la Plaza de la Memoria de la ciudad de Barranquilla, el Ministro de Justicia Jorge Eduardo Londoño, reconocerá públicamente la responsabilidad estatal en la ejecución extrajudicial del campesino y la tortura de su madre, Amira Vásquez de Zúñiga, cometidas en junio de 1992, **por integrantes de la I Brigada de Infantería de Marina en San Jacinto, Bolívar**.

Los hechos ocurrieron el lunes 1 de Junio del año 1992, cuando un treinta miembros de la Brigada Número 1 del Batallón de Fusileros de la Infantería de Marina, ingresaron violentamente a la casa de la señora Amira para obtener información sobre el paradero de la guerrilla, golpearon a Omar de 24 años, y al no tener respuesta, optaron por llevárselos.

Cuatro días después, los militares abandonaron a Amira en una carretera y le dijeron que su hijo Omar se había escapado y que no sabían de su paradero. "Nueve días después, cerca al corregimiento de El Paraíso, **el cuerpo de Omar fue encontrado con un impacto de arma de fuego en el cráneo y con la mandíbula fracturada**", según afirma el Colectivo de Abogados José Alvear Restrepo, representante legal de la familia.

En abril de 1999 el Tribunal Contencioso Administrativo de Bolívar declaró administrativa y patrimonialmente **responsable a la Brigada Número I de la Infantería de Marina** por los perjuicios causados con ocasión a la muerte de Omar Zúñiga Vásquez.

En 2011 se ordenaron las captura del Coronel Henry Rodríguez Botero, del dragoneante Pedro José Yepes, de los sargentos viceprimero Carlos Adolfo Bermúdez y Misael Villabona López, y de los infantes Oslavi Enrique de la Cruz, Guillermo Próspero Castillo y Alfonso Coronel Ortiz, **por su presunta responsabilidad en el delito de tortura y homicidio agravado**. Vale la pena destacar que durante estos procedimientos penales la familia Zúñiga Vásquez fue amenazada, hostigada e intimidada.

Tras 24 años de búsqueda de justicia y lucha contra la impunidad, se suscribió el Acuerdo ante la Comisión Interamericana de Derechos Humanos, que "contempla medidas como la entrega digna de los restos de la víctima, la **revisión del fallo que declaró la prescripción del caso contra los militares involucrados**, y la vinculación de la familia a programas de reparación integral" además de la realización del acto público de reconocimiento de responsabilidad.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ]
