Title: Las prisiones de Egipto sufren una crisis de salubridad.
Date: 2020-07-29 12:19
Author: AdminContagio
Category: DDHH, El mundo
Tags: Crisis Sanitaria, Human Rights Watch, Prisiones en Egipto
Slug: las-prisiones-de-egipto-sufren-una-crisis-de-salubridad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/37489267-f143-4c7a-b757-b00bb2114f04.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Gerson Galán Beras-Flickr

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según informe presentado por Human Rights Watch, la crisis humanitaria y de salud es cada vez más preocupante en las prisiones egipcias pues las autoridades no están tomando las medidas suficientes para contener e identificar los casos de coronavirus, además se encuentran superpobladas y los presos no tienen acceso a material protector ni a atención médica oportuna.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El reporte hecho por HRW está basado en cartas enviadas por dos prisiones del país y por las declaraciones de personas cercanas a los prisioneros. (Le puede interesar: [Muere prisionera por contagio de Covid 19 en Cárcel Buen Pastor](https://archivo.contagioradio.com/muere-prisionera-por-contagio-de-covid-19-en-carcel-buen-pastor/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Actualmente Egipto registra 92.482 personas con Covid-19 y 4652 muertes y según el Comité de Justicia (CFJ) en los centros de detención ya van 15 muertos y el número de casos confirmados entre detenidos, policías y trabajadores es de 312, de los cuales 212 son sospechosos y 100 confirmados en 48 centros de detención en 13 provincias.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De los 15 casos documentados, algunos murieron después de experimentar sospechas de contagio pero no se hicieron la prueba o recibieron un tratamiento médico adecuado. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, de las 14 muertes que se habían registrado en su momento, solo 9 prisioneros fueron transferidos a un hospital y en algunos casos, solo horas antes de sus muertes. “La enfermedad requiere cuidado y descanso y esto es imposible”, dijo alguien cercano a uno de los prisioneros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según entrevistados, los detenidos sólo cuentan con medicamentos y desinfectantes que les llevan sus familiares cuando pueden entregarlos y es que las visitas están suspendidas a pesar de que muchos de los sectores del país han reabierto. 

<!-- /wp:paragraph -->

<!-- wp:heading -->

La gestión por parte de los oficiales en las prisiones tampoco es favorable
---------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La situación tampoco mejora pues al menos en 3 prisiones, oficiales no permitieron  que los reclusos obtuvieran o usaran máscaras, dijeron testigos. Asimismo, las autoridades "han hecho poco para aislar" a los que han tenido síntomas y tomar medidas para proteger a las poblaciones de riesgo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“Autoridades egipcias deben tomar medidas inmediatamente para proveer a todos en las prisiones con cuidado médico adecuado y medidas para contener los aumentos de Covi-19”, dijo Joe Stork subdirector de Medio Oriente y África del Norte en Human Rights Watch.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También agrego, que "es esencial que Egipto aborde la propagación del virus acelerando las liberaciones de prisioneros" pero, a pesar de que las autoridades han liberado a unos 13,000 prisioneros desde finales de febrero, ese número es insuficiente para aliviar el hacinamiento en las cárceles y permitir que haya distanciamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, "en lugar de ofrecer asistencia médica adecuada y medidas sanitarias para prevenir la propagación de la COVID-19, el Gobierno egipcio intenta ocultar la grave situación en las prisiones", afirmó el vicedirector de HRW para Oriente Medio y el Norte de África.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez, se informó que la Agencia de Seguridad Nacional de Egipto convocó, interrogó y castigó a los reclusos cuando los medios de comunicación comenzaron a compartir sobre presuntos casos de virus en las cárceles. En un caso, los reclusos dijeron que fueron despojados, golpeados con palos y cables eléctricos por quejarse de sus condiciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, desde que se posesionó el presidente Abdel Fattah el-Sissi en el 2013, hay encarcelados cientos de trabajadores de la salud, periodistas y críticos, además de cerca de 4.000 personas por las protestas pacíficas que se presentaron en 2019.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Haga Clikc aquí [Si desea leer todo el informe](https://www.hrw.org/news/2020/07/20/egypt-apparent-covid-19-outbreaks-prisons)

<!-- /wp:paragraph -->
