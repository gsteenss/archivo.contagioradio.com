Title: Crece optimismo en torno a formalización de mesa de conversaciones de paz con el ELN
Date: 2015-09-10 14:33
Category: Movilización, Nacional, Paz
Tags: colombia, corporación nuevo Arcoíris, ELN, Gabino, Mesa de diálogo con el ELN, paz, proceso de paz
Slug: crece-optimismo-en-torno-a-formalizacion-de-mesa-de-conversaciones-de-paz-con-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/gabino-contagioradio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: canal capital, contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_8298252_2_1.html?data=mZemmpeZdo6ZmKiakp2Jd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9PZxMqY0dXYrc7d1NLcjcrSb9Xj09PcjcaPqtDmzsbZy9%2FFp8qZpJiSpJjSb8XZjNLS1caPqMafxNSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Luis Eduardo Celis, Nuevo Arcoiris] 

###### [10 sep 2015] 

Las declaraciones de “Gabino”, comandante del Ejercito de Liberación Nacional, ELN, quien afirmó que para formalizar los diálogos con el Gobierno Nacional falta un 3% **se reciben con optimismo para llegar a prontos acuerdos** que pongan fin al conflicto con esta otra guerrilla.

Luis Eduardo Celis, investigador de la Corporación Nuevo Arco Iris indicó que **“lo que hay que esperar es que el proceso se haga público y los dos aportes (ELN y Gobierno) presenten lo que se ha concertado”**, esto para formalizar y avanzar en estos acuerdos.

La propuesta del ELN para estos diálogos es **generar dos mesas de trabajo una en donde participen representantes del gobierno y el grupo guerrillero, y otra con la ciudadanía**, lo cual en palabras de Celis es un “reto” porque en estas mesas suelen presentarse incumplimientos “Si el gobierno asume reto de participación ciudadana es para cumplir”.

Pese a que el tiempo es un factor que prima en esta dinámica, los diálogos que se adelantan en la Habana pueden coincidir con los del ELN en **temas como la terminación del conflicto, integración a la democracia, participación y competición política en su pluralidad y en la refrendación de los acuerdos**.

Celis recalca lo importante de las declaraciones de “Gabino” frente a la contundencia y firmeza de sus declaraciones, además que resulta “un aporte a la información sobre este proceso” para la opinión pública y medios.

**Ver también:**

[¿Qué tan cerca esta el inicio oficial de los diálogos de paz con el ELN ](https://archivo.contagioradio.com/entrevista-que-tan-cerca-esta-el-inicio-oficial-de-los-dialogos-de-paz-con-el-eln/)  
[La paz sin el ELN sería una paz incompleta articulo](https://archivo.contagioradio.com/la-paz-sin-el-eln-seria-una-paz-incompleta/)
