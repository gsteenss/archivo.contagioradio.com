Title: Colegio AFLICOC conservando la cultura afro
Date: 2016-08-05 16:32
Category: Comunidad, Educación
Tags: Colegio Aflicoc, comunidades, Curbaradó, Educación Propia
Slug: colegio-aflicoc-conservando-la-cultura-afro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/IMG-20151207-WA0010.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [03 Ago 2016]

###### *Como parte del trabajo de formación de Contagio Radio con comunidades afros, indígenas, mestizas y campesinas en toda Colombia, un grupo de comunicadores realizó una serie de trabajos periodísticos recogiendo las apuestas de construcción de paz desde las comunidades y los territorios. Nos alegra compartirlas con nuestros y nuestras lectoras.* 

En el año 2013 se crea el colegio AFLICOC (Asociación de Familias de Consejos Comunitarios de la Cuenca del Río Curbaradó, Rio Jiguamiandó, Río Pedeguita y Mancilla, la Larga Tumaradó y Vigía de Curbaradó) en la zona humanitaria las camelias, en la cuenca del río curbarado en choco. Esta se crea para el arraigo a los territorios.

José Francisco Álvarez,  líder de la comunidad dice “el colegio fue construida desde de las comunidades, es construir futuro desde la resistencia”. Formar estudiantes que defiendan al proceso educativo, que defiendan sus derechos, que defiendan a la población y capacitar enfocado a los territorios

José francisco nos comenta “lo que enseña en el colegio no es más enseñar la identidad propia a su uso y costumbre. El colegio AFLICOC tiene su propio granja enfatizado en área agropecuario, para alimentar a los estudiantes es con el esfuerzo de la gente, no todo los días hacen la olla comunitaria, si no los viernes afínales de la semana hacemos la olla comunitaria para alimentar a los estudiantes.

El líder José francisco manifiesta que el “colegio es propio, no recibimos a todo mundo porque es de las comunidades  y para las comunidades, las comunidades AFLICOC tienen el propio PEC elaborado por los líderes, docentes, madres comunitarias, consejo comunitarios, de acuerdo con las mallas curriculares.

Algunos profesores son de las comunidades y otros vienen voluntariamente por medio de las comisiones y de las universidades. En el colegio AFLICOC se enseñan una semana al mes. El colegio AFLICOC tiene convenio con el ClareT que certifica a los estudiantes.

Los estudiantes presentan las prueba de ICFES, el colegio es grande y de madera, 133 estudiantes de diferentes comunidades, para venir al colegio vienen a pie, los que tiene posibilidad viene por motos.

El objetivo de las comunidades es formar líderes para que ellos sean los defensores de los territorios colectivos.

Erika Orejuela “unas de las estudiantes del colegio integral cabida quien se gradúa en el 2008, explica que sintió una gran alegría por haber ingresado al colegio los cuales los enseñaron como defender nuestros territorio colectivos.

El colegio integral cabida fue la base educativa del colegio Aflicoc por allí se inicia una experiencia de educación propia en el 2001.

Crea la universidad por la paz en jiguamiando municipio Carmen del Darién en la zona humanitaria nueva esperanza y así los estudiantes egresado del colegio puedan estudiar al estudio superior para fortalecer  la paz en los territorios.

[![Saul Chamarra 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Saul-Chamarra-2--200x150.jpg){.wp-image-27482 .size-post-thumbnail .alignleft width="200" height="150"}](https://archivo.contagioradio.com/colegio-aflicoc-conservando-la-cultura-afro/saul-chamarra-2/)

*Por **Saul Chamarra Quintero**, comunidad indígena Nonam, Comunicador de CONPAZ, Río San Juan, Valle del Cauca*.
