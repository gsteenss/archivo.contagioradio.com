Title: Otra Mirada: Riesgos de reclamar tierras en Colombia
Date: 2020-10-02 09:47
Author: PracticasCR
Category: Otra Mirada, Programas
Tags: Reclamante de tierras, Restitución de tierras, Unidad Nacional de protección
Slug: otra-mirada-riesgos-de-reclamar-tierras-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/11155140_967175476626653_9089835472884672392_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Forjando Futuros

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lejos de mesurarse y reducirse los riesgos que corren los hombres y las mujeres reclamantes de tierras, los peligrosos, las amenazas y secuestros siguen aumentado, sin haber garantías que les permitan volver y vivir en sus territorios con tranquilidad. [(Le puede interesar: 19.402 procesos de restitución de tierras siguen frenados en Antioquia](https://archivo.contagioradio.com/19-402-procesos-de-restitucion-de-tierras-siguen-frenados-en-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el programa de Otra Mirada, sobre la situación de los reclamantes de tierras, hicieron parte del panel Gerardo Vega, abogado, director de Forjando Futuros, Juan Carlos Posada, reclamante de tierras del Magdalena Medio y Blanca Irene López, directora de la Corporación Jurídica Yira Castro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los invitados explican cuál es la situación actual de los reclamantes de tierra ahora que la pandemia ha limitado en gran medida el avance de los procesos. Además, comentan cuáles son los riesgos que se han venido identificando tanto para reclamantes, como para líderes y quienes defienden los derechos de las víctimas. [(Le puede interesar: Tras cuatro años de la firma del Acuerdo de Paz, la deuda más grande es con el campo colombiano](https://archivo.contagioradio.com/tras-cuatro-anos-firma-del-acuerdo-de-paz-la-deuda-mas-grande-es-con-el-campo-colombiano/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, señalan que algunos de los actores responsables de muchos despojos son empresas ganaderas y bancos en departamentos como el Magdalena y agregan lo preocupante de la burocracia con la que se está manejando la Unidad Nacional de Protección -UNP -.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Pese a existir un marco normativo que reconoce los derechos de las víctimas para acceder a las tierras, lo que ellos están recibiendo es violencia y amenazas".
>
> <cite>Blanca Irene López</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por otro lado, señalan cuáles son los proyectos e iniciativas que se están planeando desde las organizaciones y las personas que esperan volver a su territorio algún día; finalmente, comparten qué los motiva a seguir cuando muchas veces parece más peligroso continuar. (Si desea escuchar el programa del 29 de septiembre: [Otra Mirada: El silencio de la guerra de cuarta generación](https://www.facebook.com/contagioradio/videos/794575171315072))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el análisis completo: [Haga click aquí](https://www.facebook.com/contagioradio/videos/3171911616368687)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
