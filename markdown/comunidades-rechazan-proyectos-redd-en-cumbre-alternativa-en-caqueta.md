Title: Comunidades rechazan proyectos REDD+ en cumbre alternativa en Caquetá
Date: 2019-05-06 08:37
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Amazonía, deforestación, REDD+
Slug: comunidades-rechazan-proyectos-redd-en-cumbre-alternativa-en-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-04-30-at-12.18.20-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FOSPA] 

Entre el 30 de abril y el 3 de mayo, las organizaciones integrantes del Foro Social Panamazónico (FOSPA) realizaron un encuentro en la Universidad de la Amazonía, en Florencia, Caquetá, donde dieron a conocer los impactos nocivos que generan algunas iniciativas internacionales para reducir las emisiones, como la denominada "Economía Verde" y el programa REDD+.

Esta reunión se estableció como un espacio alternativo a la **Cumbre Mundial sobre el Clima y los Bosques**,  realizada este año en Florencia, uno de los municipios más afectados por la deforestación en el país, donde se reunieron más de 40 gobernadores del mundo con la intensión de formular estrategias conjuntas para frenar el deterioro de bosques tropicales y la tasa acelerada del cambio climático.

Una de estas estrategias principales es REDD+, el programa bandera de las Naciones Unidas para reducir emisiones que ofrece incentivos económicos a países con menores ingresos que se comprometen con  la conservación de sus bosques, una medida indispensable para generar oxígeno y detener el cambio climático. (Le puede interesar: "[Deforestación sigue aumuentando en la Amazonía colombiana](https://archivo.contagioradio.com/deforestacion-siguen-aumentando-la-amazonia-colombiana/)")

Esta iniciativa internacional fue aplicado al caso de Colombia por el gobierno del expresidente Juan Manuel Santos al incluirlo en el Plan Nacional de Desarrollo. Ahora solo quedaría ejecutar los proyectos, afirmó Mercedes Mejía, profesora de la Universidad de la Amazonía, sin embargo las comunidades se han resistido a su implementación por la falta de una política o mecanismo claro.

Asímismo, comunidades indígenas que han participado en esta iniciativa señalan al Gobierno de no cumplir con la entrega del dinero acordado en los contratos y de ejercer control absoluto sobre la implementación de los proyectos, causando que estos pueblos originarios pierda tradiciones culturales y se debilite su tejido social.

Por otro lado, la profesora afirma que quienes benefician de estos contratos son empresas minero-energéticas porque reciban dinero por conservar bosques a pesar de ser, a la misma vez, uno de los principales causantes de emisiones invernaderos. Para Mejía, el mensaje detrás de estas "economías verdes" es que "el que peca y reza empata".

<iframe id="audio_35335088" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35335088_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
