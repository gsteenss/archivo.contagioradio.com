Title: Infografias
Date: 2019-06-11 11:45
Author: CtgAdm
Slug: infografias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/9-derechos-de-ppl_44767473435_o.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/10-crceles-con-mayor-sobrepoblacin-en-colombia_31268380387_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/20-aos-de-la-sentencia-t-153_43706849020_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/ciclo-de-escolarizacion-en-las-carceles_46092940531_o.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/comunidad-lgtbi-en-las-carceles_43706848310_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/cuanto-le-cuesta-un-preso-al-estado_44798714574_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/derechos-sexuales-y-reproductivos-en-las-crceles_46217749841_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/enfoque-diferencial-en-las-crceles_32147079428_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/entidades-encargadas-de-las-carceles_43706847360_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/establecimientos-carcelarios-y-recomendaciones-tcnicas-del-cicr_45141212634_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/hacinamiento-en-las-prisiones-colombianas_44798713874_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/indultos-y-amnistia_43706845620_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/la-educacin-en-las-crceles-de-colombia_32147082638_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/los-10-delitos-que-encabezan-la-lista-de-judializacin-en-colombia_44202393650_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/movimiento-nacional-carcelario_30926516657_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/mujeres-en-las-crceles_45526110681_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/nueva-cultura-penitenciaria_44798712904_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/primera-infancia-en-las-crceles_32147084138_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/qu-come-un-preso_-1_46194891011_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/que-es-el-delito-politico_44798712394_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/salud-en-las-carceles_31651636798_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/tramacua_el-guantanamo-de-colombia_44798711044_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/trastornos-mentales-en-las-crcerles_31153668907_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/violencia-contra-la-comunidad-lgbti-en-las-crceles_44798710224_o.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### Infografías Expreso Libertad

##### A través de infografías conoce las problemáticas de las cárceles en Colombia

<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/que-es-el-delito-politico_44798712394_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/que-es-el-delito-politico_44798712394_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/que-es-el-delito-politico_44798712394_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/que-es-el-delito-politico_44798712394_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/que-es-el-delito-politico_44798712394_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/que-es-el-delito-politico_44798712394_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/que-es-el-delito-politico_44798712394_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/que-es-el-delito-politico_44798712394_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/que-es-el-delito-politico_44798712394_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68740">
¿Qué es el delito político?  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/mujeres-en-las-crceles_45526110681_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/mujeres-en-las-crceles_45526110681_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/mujeres-en-las-crceles_45526110681_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/mujeres-en-las-crceles_45526110681_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/mujeres-en-las-crceles_45526110681_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/mujeres-en-las-crceles_45526110681_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/mujeres-en-las-crceles_45526110681_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/mujeres-en-las-crceles_45526110681_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/mujeres-en-las-crceles_45526110681_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68735">
Mujeres en las cárceles  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/la-educacin-en-las-crceles-de-colombia_32147082638_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/la-educacin-en-las-crceles-de-colombia_32147082638_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/la-educacin-en-las-crceles-de-colombia_32147082638_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/la-educacin-en-las-crceles-de-colombia_32147082638_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/la-educacin-en-las-crceles-de-colombia_32147082638_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/la-educacin-en-las-crceles-de-colombia_32147082638_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/la-educacin-en-las-crceles-de-colombia_32147082638_o.png?zoom=2&resize=310%2C310&ssl=1 620w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/la-educacin-en-las-crceles-de-colombia_32147082638_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68732">
Educación en las cárceles  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/20-aos-de-la-sentencia-t-153_43706849020_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/20-aos-de-la-sentencia-t-153_43706849020_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/20-aos-de-la-sentencia-t-153_43706849020_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/20-aos-de-la-sentencia-t-153_43706849020_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/20-aos-de-la-sentencia-t-153_43706849020_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/20-aos-de-la-sentencia-t-153_43706849020_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/20-aos-de-la-sentencia-t-153_43706849020_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/20-aos-de-la-sentencia-t-153_43706849020_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/20-aos-de-la-sentencia-t-153_43706849020_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68722">
20 Años sentencia T-153  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/cuanto-le-cuesta-un-preso-al-estado_44798714574_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/cuanto-le-cuesta-un-preso-al-estado_44798714574_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/cuanto-le-cuesta-un-preso-al-estado_44798714574_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/cuanto-le-cuesta-un-preso-al-estado_44798714574_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/cuanto-le-cuesta-un-preso-al-estado_44798714574_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/cuanto-le-cuesta-un-preso-al-estado_44798714574_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/cuanto-le-cuesta-un-preso-al-estado_44798714574_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/cuanto-le-cuesta-un-preso-al-estado_44798714574_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/cuanto-le-cuesta-un-preso-al-estado_44798714574_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68725">
¿Cuánto le cuesta un prisionero al Estado?  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/hacinamiento-en-las-prisiones-colombianas_44798713874_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/hacinamiento-en-las-prisiones-colombianas_44798713874_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/hacinamiento-en-las-prisiones-colombianas_44798713874_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/hacinamiento-en-las-prisiones-colombianas_44798713874_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/hacinamiento-en-las-prisiones-colombianas_44798713874_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/hacinamiento-en-las-prisiones-colombianas_44798713874_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/hacinamiento-en-las-prisiones-colombianas_44798713874_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/hacinamiento-en-las-prisiones-colombianas_44798713874_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/hacinamiento-en-las-prisiones-colombianas_44798713874_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68730">
Hacinameinto en las prisiones  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/indultos-y-amnistia_43706845620_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/indultos-y-amnistia_43706845620_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/indultos-y-amnistia_43706845620_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/indultos-y-amnistia_43706845620_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/indultos-y-amnistia_43706845620_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/indultos-y-amnistia_43706845620_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/indultos-y-amnistia_43706845620_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/indultos-y-amnistia_43706845620_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/indultos-y-amnistia_43706845620_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68731">
Indulto y Amnistia  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/trastornos-mentales-en-las-crcerles_31153668907_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/trastornos-mentales-en-las-crcerles_31153668907_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/trastornos-mentales-en-las-crcerles_31153668907_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/trastornos-mentales-en-las-crcerles_31153668907_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/trastornos-mentales-en-las-crcerles_31153668907_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/trastornos-mentales-en-las-crcerles_31153668907_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/trastornos-mentales-en-las-crcerles_31153668907_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/trastornos-mentales-en-las-crcerles_31153668907_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/trastornos-mentales-en-las-crcerles_31153668907_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68743">
Transtornos y enfermedades en las cárceles  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/ciclo-de-escolarizacion-en-las-carceles_46092940531_o-310x310.jpg){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/ciclo-de-escolarizacion-en-las-carceles_46092940531_o.jpg?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/ciclo-de-escolarizacion-en-las-carceles_46092940531_o.jpg?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/ciclo-de-escolarizacion-en-las-carceles_46092940531_o.jpg?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/ciclo-de-escolarizacion-en-las-carceles_46092940531_o.jpg?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/ciclo-de-escolarizacion-en-las-carceles_46092940531_o.jpg?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/ciclo-de-escolarizacion-en-las-carceles_46092940531_o.jpg?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/ciclo-de-escolarizacion-en-las-carceles_46092940531_o.jpg?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/ciclo-de-escolarizacion-en-las-carceles_46092940531_o.jpg)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68723">
Ciclo de escolarización  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/9-derechos-de-ppl_44767473435_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/9-derechos-de-ppl_44767473435_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/9-derechos-de-ppl_44767473435_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/9-derechos-de-ppl_44767473435_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/9-derechos-de-ppl_44767473435_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/9-derechos-de-ppl_44767473435_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/9-derechos-de-ppl_44767473435_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/9-derechos-de-ppl_44767473435_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/9-derechos-de-ppl_44767473435_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68720">
Derechos de las personas privadas de la libertad  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/10-crceles-con-mayor-sobrepoblacin-en-colombia_31268380387_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/10-crceles-con-mayor-sobrepoblacin-en-colombia_31268380387_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/10-crceles-con-mayor-sobrepoblacin-en-colombia_31268380387_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/10-crceles-con-mayor-sobrepoblacin-en-colombia_31268380387_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/10-crceles-con-mayor-sobrepoblacin-en-colombia_31268380387_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/10-crceles-con-mayor-sobrepoblacin-en-colombia_31268380387_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/10-crceles-con-mayor-sobrepoblacin-en-colombia_31268380387_o.png?zoom=2&resize=310%2C310&ssl=1 620w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/10-crceles-con-mayor-sobrepoblacin-en-colombia_31268380387_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68721">
Cárceles con mayor sobrepoblación  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/comunidad-lgtbi-en-las-carceles_43706848310_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/comunidad-lgtbi-en-las-carceles_43706848310_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/comunidad-lgtbi-en-las-carceles_43706848310_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/comunidad-lgtbi-en-las-carceles_43706848310_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/comunidad-lgtbi-en-las-carceles_43706848310_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/comunidad-lgtbi-en-las-carceles_43706848310_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/comunidad-lgtbi-en-las-carceles_43706848310_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/comunidad-lgtbi-en-las-carceles_43706848310_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/comunidad-lgtbi-en-las-carceles_43706848310_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68724">
Comunidad LGBTI en las cárceels  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/derechos-sexuales-y-reproductivos-en-las-crceles_46217749841_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/derechos-sexuales-y-reproductivos-en-las-crceles_46217749841_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/derechos-sexuales-y-reproductivos-en-las-crceles_46217749841_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/derechos-sexuales-y-reproductivos-en-las-crceles_46217749841_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/derechos-sexuales-y-reproductivos-en-las-crceles_46217749841_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/derechos-sexuales-y-reproductivos-en-las-crceles_46217749841_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/derechos-sexuales-y-reproductivos-en-las-crceles_46217749841_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/derechos-sexuales-y-reproductivos-en-las-crceles_46217749841_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/derechos-sexuales-y-reproductivos-en-las-crceles_46217749841_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68726">
Derechos sexuales y reproductivos  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/enfoque-diferencial-en-las-crceles_32147079428_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/enfoque-diferencial-en-las-crceles_32147079428_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/enfoque-diferencial-en-las-crceles_32147079428_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/enfoque-diferencial-en-las-crceles_32147079428_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/enfoque-diferencial-en-las-crceles_32147079428_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/enfoque-diferencial-en-las-crceles_32147079428_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/enfoque-diferencial-en-las-crceles_32147079428_o.png?zoom=2&resize=310%2C310&ssl=1 620w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/enfoque-diferencial-en-las-crceles_32147079428_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68727">
Enfoque diferencial en las cárceles  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/entidades-encargadas-de-las-carceles_43706847360_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/entidades-encargadas-de-las-carceles_43706847360_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/entidades-encargadas-de-las-carceles_43706847360_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/entidades-encargadas-de-las-carceles_43706847360_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/entidades-encargadas-de-las-carceles_43706847360_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/entidades-encargadas-de-las-carceles_43706847360_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/entidades-encargadas-de-las-carceles_43706847360_o.png?zoom=2&resize=310%2C310&ssl=1 620w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/entidades-encargadas-de-las-carceles_43706847360_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68728">
Entidades encargadas de las cárceles  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/establecimientos-carcelarios-y-recomendaciones-tcnicas-del-cicr_45141212634_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/establecimientos-carcelarios-y-recomendaciones-tcnicas-del-cicr_45141212634_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/establecimientos-carcelarios-y-recomendaciones-tcnicas-del-cicr_45141212634_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/establecimientos-carcelarios-y-recomendaciones-tcnicas-del-cicr_45141212634_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/establecimientos-carcelarios-y-recomendaciones-tcnicas-del-cicr_45141212634_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/establecimientos-carcelarios-y-recomendaciones-tcnicas-del-cicr_45141212634_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/establecimientos-carcelarios-y-recomendaciones-tcnicas-del-cicr_45141212634_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/establecimientos-carcelarios-y-recomendaciones-tcnicas-del-cicr_45141212634_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/establecimientos-carcelarios-y-recomendaciones-tcnicas-del-cicr_45141212634_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68729">
Recomendaciones técnicas del CICR a las cárceles  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/los-10-delitos-que-encabezan-la-lista-de-judializacin-en-colombia_44202393650_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/los-10-delitos-que-encabezan-la-lista-de-judializacin-en-colombia_44202393650_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/los-10-delitos-que-encabezan-la-lista-de-judializacin-en-colombia_44202393650_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/los-10-delitos-que-encabezan-la-lista-de-judializacin-en-colombia_44202393650_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/los-10-delitos-que-encabezan-la-lista-de-judializacin-en-colombia_44202393650_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/los-10-delitos-que-encabezan-la-lista-de-judializacin-en-colombia_44202393650_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/los-10-delitos-que-encabezan-la-lista-de-judializacin-en-colombia_44202393650_o.png?zoom=2&resize=310%2C310&ssl=1 620w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/los-10-delitos-que-encabezan-la-lista-de-judializacin-en-colombia_44202393650_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68733">
Los 10 Delitos que encabezan la judicialización  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/movimiento-nacional-carcelario_30926516657_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/movimiento-nacional-carcelario_30926516657_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/movimiento-nacional-carcelario_30926516657_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/movimiento-nacional-carcelario_30926516657_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/movimiento-nacional-carcelario_30926516657_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/movimiento-nacional-carcelario_30926516657_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/movimiento-nacional-carcelario_30926516657_o.png?zoom=2&resize=310%2C310&ssl=1 620w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/movimiento-nacional-carcelario_30926516657_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68734">
Moviemiento carcelario  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/nueva-cultura-penitenciaria_44798712904_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/nueva-cultura-penitenciaria_44798712904_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/nueva-cultura-penitenciaria_44798712904_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/nueva-cultura-penitenciaria_44798712904_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/nueva-cultura-penitenciaria_44798712904_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/nueva-cultura-penitenciaria_44798712904_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/nueva-cultura-penitenciaria_44798712904_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/nueva-cultura-penitenciaria_44798712904_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/nueva-cultura-penitenciaria_44798712904_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68737">
Nueva cultura penitenciaria  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/primera-infancia-en-las-crceles_32147084138_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/primera-infancia-en-las-crceles_32147084138_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/primera-infancia-en-las-crceles_32147084138_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/primera-infancia-en-las-crceles_32147084138_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/primera-infancia-en-las-crceles_32147084138_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/primera-infancia-en-las-crceles_32147084138_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/primera-infancia-en-las-crceles_32147084138_o.png?zoom=2&resize=310%2C310&ssl=1 620w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/primera-infancia-en-las-crceles_32147084138_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68738">
Primera infancia en las cárceles  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/qu-come-un-preso_-1_46194891011_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/qu-come-un-preso_-1_46194891011_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/qu-come-un-preso_-1_46194891011_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/qu-come-un-preso_-1_46194891011_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/qu-come-un-preso_-1_46194891011_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/qu-come-un-preso_-1_46194891011_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/qu-come-un-preso_-1_46194891011_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/qu-come-un-preso_-1_46194891011_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/qu-come-un-preso_-1_46194891011_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68739">
Alimentación en las cárceles  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/salud-en-las-carceles_31651636798_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/salud-en-las-carceles_31651636798_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/salud-en-las-carceles_31651636798_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/salud-en-las-carceles_31651636798_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/salud-en-las-carceles_31651636798_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/salud-en-las-carceles_31651636798_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/salud-en-las-carceles_31651636798_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/salud-en-las-carceles_31651636798_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/salud-en-las-carceles_31651636798_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68741">
Salud en las cárceles  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/tramacua_el-guantanamo-de-colombia_44798711044_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/tramacua_el-guantanamo-de-colombia_44798711044_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/tramacua_el-guantanamo-de-colombia_44798711044_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/tramacua_el-guantanamo-de-colombia_44798711044_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/tramacua_el-guantanamo-de-colombia_44798711044_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/tramacua_el-guantanamo-de-colombia_44798711044_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/tramacua_el-guantanamo-de-colombia_44798711044_o.png?zoom=2&resize=310%2C310&ssl=1 620w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/tramacua_el-guantanamo-de-colombia_44798711044_o.png?zoom=3&resize=310%2C310&ssl=1 930w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/tramacua_el-guantanamo-de-colombia_44798711044_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68742">
Tramacua  

</figcaption>
</figure>
<figure class="gallery-item">
[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/violencia-contra-la-comunidad-lgbti-en-las-crceles_44798710224_o-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/violencia-contra-la-comunidad-lgbti-en-las-crceles_44798710224_o.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/violencia-contra-la-comunidad-lgbti-en-las-crceles_44798710224_o.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/violencia-contra-la-comunidad-lgbti-en-las-crceles_44798710224_o.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/violencia-contra-la-comunidad-lgbti-en-las-crceles_44798710224_o.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/violencia-contra-la-comunidad-lgbti-en-las-crceles_44798710224_o.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/violencia-contra-la-comunidad-lgbti-en-las-crceles_44798710224_o.png?zoom=2&resize=310%2C310&ssl=1 620w"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/violencia-contra-la-comunidad-lgbti-en-las-crceles_44798710224_o.png)

</p>
<figcaption class="wp-caption-text gallery-caption" id="gallery-3-68744">
Violencia contra la comunidad LGBTI  

</figcaption>
</figure>
### Expreso Libertad

[  
](https://archivo.contagioradio.com/luche-por-la-libertad-de-mi-hijo-y-lo-logramos/)

##### «Luché por la libertad de mi hijo ¡y lo logramos!»

Contagioradio<time datetime="2019-06-05T10:21:38-05:00" title="2019-06-05T10:21:38-05:00">junio 5, 2019</time>0En Expreso libertad nos acompaño Aracely León madre de Mateo Gutiérrez, capturado el 27 de febrero de 2017 bajo un...</a>  
[![Ni las rejas han logrado frenar el empoderamiento de las mujeres](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/presos-politicos-640x410-150x150.jpg "Ni las rejas  han logrado frenar el empoderamiento de las mujeres"){width="150" height="150" sizes="(max-width: 150px) 100vw, 150px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/presos-politicos-640x410.jpg?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/presos-politicos-640x410.jpg?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/presos-politicos-640x410.jpg?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/presos-politicos-640x410.jpg?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/presos-politicos-640x410.jpg?zoom=3&resize=150%2C150&ssl=1 450w"}](https://archivo.contagioradio.com/empoderamiento-mujeres-carceles-colombia/)  

###### [Ni las rejas han logrado frenar el empoderamiento de las mujeres](https://archivo.contagioradio.com/empoderamiento-mujeres-carceles-colombia/)

[<time datetime="2019-05-28T16:55:53-05:00" title="2019-05-28T16:55:53-05:00">mayo 28, 2019</time>](https://archivo.contagioradio.com/2019/05/28/)Érika Aguirre víctima del falso...  
[![En Colombia se persigue al pensamiento crítico](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/pensamiento-critico-150x150.jpg "En Colombia se persigue al pensamiento crítico"){width="150" height="150" sizes="(max-width: 150px) 100vw, 150px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/pensamiento-critico.jpg?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/pensamiento-critico.jpg?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/pensamiento-critico.jpg?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/pensamiento-critico.jpg?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/pensamiento-critico.jpg?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/pensamiento-critico.jpg?zoom=3&resize=150%2C150&ssl=1 450w"}](https://archivo.contagioradio.com/colombia-persigue-pensamiento-critico/)  

###### [En Colombia se persigue al pensamiento crítico](https://archivo.contagioradio.com/colombia-persigue-pensamiento-critico/)

[<time datetime="2019-05-20T12:49:45-05:00" title="2019-05-20T12:49:45-05:00">mayo 20, 2019</time>](https://archivo.contagioradio.com/2019/05/20/)El uso de inteligencia e...  
[![Con huelga de hambre internos de Cárcel Palogordo reclaman su derecho a la salud](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Palogordo-150x150.png "Con huelga de hambre internos de Cárcel Palogordo reclaman su derecho a la salud"){width="150" height="150" sizes="(max-width: 150px) 100vw, 150px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Palogordo.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Palogordo.png?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Palogordo.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Palogordo.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Palogordo.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Palogordo.png?zoom=3&resize=150%2C150&ssl=1 450w"}](https://archivo.contagioradio.com/huelga-hambre-internos-carcel-palogordo-salud/)  

###### [Con huelga de hambre internos de Cárcel Palogordo reclaman su derecho a la salud](https://archivo.contagioradio.com/huelga-hambre-internos-carcel-palogordo-salud/)

[<time datetime="2019-05-08T12:29:49-05:00" title="2019-05-08T12:29:49-05:00">mayo 8, 2019</time>](https://archivo.contagioradio.com/2019/05/08/)Una semana completaron en huelga...  
[![¿En qué va la Ley de Amnistía a dos años de su implementación?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/los_presos_politicos_de_farc-150x150.jpg "¿En qué va la Ley de Amnistía a dos años de su implementación?"){width="150" height="150" sizes="(max-width: 150px) 100vw, 150px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/los_presos_politicos_de_farc.jpg?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/los_presos_politicos_de_farc.jpg?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/los_presos_politicos_de_farc.jpg?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/los_presos_politicos_de_farc.jpg?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/los_presos_politicos_de_farc.jpg?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/los_presos_politicos_de_farc.jpg?zoom=3&resize=150%2C150&ssl=1 450w"}](https://archivo.contagioradio.com/ey-amnistia-dos-anos-implementacion/)  

###### [¿En qué va la Ley de Amnistía a dos años de su implementación?](https://archivo.contagioradio.com/ey-amnistia-dos-anos-implementacion/)

[<time datetime="2019-04-25T10:54:58-05:00" title="2019-04-25T10:54:58-05:00">abril 25, 2019</time>](https://archivo.contagioradio.com/2019/04/25/)A dos años de la...  
[![Falsos positivos judiciales contra lideresas sociales](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/acsn1-150x150.png "Falsos positivos judiciales contra lideresas sociales"){width="150" height="150" sizes="(max-width: 150px) 100vw, 150px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/acsn1.png?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/acsn1.png?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/acsn1.png?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/acsn1.png?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/acsn1.png?zoom=3&resize=150%2C150&ssl=1 450w"}](https://archivo.contagioradio.com/falsos-positivos-judiciales-contra-lideresas-sociales/)  

###### [Falsos positivos judiciales contra lideresas sociales](https://archivo.contagioradio.com/falsos-positivos-judiciales-contra-lideresas-sociales/)

[<time datetime="2019-04-17T09:59:10-05:00" title="2019-04-17T09:59:10-05:00">abril 17, 2019</time>](https://archivo.contagioradio.com/2019/04/17/)Sara Quiñonez y Tulia Valencia,...  
[![La justicia restaurativa, una forma de tejer esperanza](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/1062621593-150x150.jpg "La justicia restaurativa, una forma de tejer esperanza"){width="150" height="150" sizes="(max-width: 150px) 100vw, 150px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/1062621593.jpg?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/1062621593.jpg?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/1062621593.jpg?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/1062621593.jpg?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/1062621593.jpg?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/1062621593.jpg?zoom=3&resize=150%2C150&ssl=1 450w"}](https://archivo.contagioradio.com/justicia-restaurativa-tejer-esperanza/)  

###### [La justicia restaurativa, una forma de tejer esperanza](https://archivo.contagioradio.com/justicia-restaurativa-tejer-esperanza/)

[<time datetime="2019-04-09T17:17:59-05:00" title="2019-04-09T17:17:59-05:00">abril 9, 2019</time>](https://archivo.contagioradio.com/2019/04/09/)Foto: AFP- Luis Robayo Con...  
[![¿Cómo se vive la sexualidad en las cárceles?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Amor-tras-las-rejas-reos-de-El-Pozo-se-casan-2-150x150.jpg "¿Cómo se vive la sexualidad en las cárceles?"){width="150" height="150" sizes="(max-width: 150px) 100vw, 150px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Amor-tras-las-rejas-reos-de-El-Pozo-se-casan-2.jpg?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Amor-tras-las-rejas-reos-de-El-Pozo-se-casan-2.jpg?resize=512%2C512&ssl=1 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Amor-tras-las-rejas-reos-de-El-Pozo-se-casan-2.jpg?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Amor-tras-las-rejas-reos-de-El-Pozo-se-casan-2.jpg?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Amor-tras-las-rejas-reos-de-El-Pozo-se-casan-2.jpg?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Amor-tras-las-rejas-reos-de-El-Pozo-se-casan-2.jpg?zoom=3&resize=150%2C150&ssl=1 450w"}](https://archivo.contagioradio.com/como-se-vive-la-sexualidad-en-las-carceles/)  

###### [¿Cómo se vive la sexualidad en las cárceles?](https://archivo.contagioradio.com/como-se-vive-la-sexualidad-en-las-carceles/)

[<time datetime="2019-04-02T20:30:00-05:00" title="2019-04-02T20:30:00-05:00">abril 2, 2019</time>](https://archivo.contagioradio.com/2019/04/02/)Foto: Abriendo brecha 25 Mar...  
[![Miguel Ángel Beltrán y su defensa del pensamiento crítico](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/miguel-angel-beltran--150x150.jpg "Miguel Ángel Beltrán y su defensa del pensamiento crítico"){width="150" height="150" sizes="(max-width: 150px) 100vw, 150px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/miguel-angel-beltran-.jpg?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/miguel-angel-beltran-.jpg?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/miguel-angel-beltran-.jpg?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/miguel-angel-beltran-.jpg?resize=360%2C360&ssl=1 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/miguel-angel-beltran-.jpg?zoom=3&resize=150%2C150&ssl=1 450w"}](https://archivo.contagioradio.com/miguel-angel-beltran-pensamiento-critico/)  

###### [Miguel Ángel Beltrán y su defensa del pensamiento crítico](https://archivo.contagioradio.com/miguel-angel-beltran-pensamiento-critico/)

[<time datetime="2019-03-26T11:59:16-05:00" title="2019-03-26T11:59:16-05:00">marzo 26, 2019</time>](https://archivo.contagioradio.com/2019/03/26/)El catedrático Miguel Ángel Beltrán,...  
[![La Roja, una cerveza artesanal fruto de la reincorporación](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/6POodqVh-150x150.jpg "La Roja, una cerveza artesanal fruto de la reincorporación"){width="150" height="150" sizes="(max-width: 150px) 100vw, 150px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/6POodqVh.jpg?resize=150%2C150&ssl=1 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/6POodqVh.jpg?resize=310%2C310&ssl=1 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/6POodqVh.jpg?resize=230%2C230&ssl=1 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/6POodqVh.jpg?zoom=3&resize=150%2C150&ssl=1 450w"}](https://archivo.contagioradio.com/roja-cerveza-artesanal-reincorporacion/)  

###### [La Roja, una cerveza artesanal fruto de la reincorporación](https://archivo.contagioradio.com/roja-cerveza-artesanal-reincorporacion/)

[<time datetime="2019-03-18T11:31:44-05:00" title="2019-03-18T11:31:44-05:00">marzo 18, 2019</time>](https://archivo.contagioradio.com/2019/03/18/)La Roja, una cerveza artesanal...
