Title: Una semana sin luz completa la comunidad de Sabana de Torres, en Santander
Date: 2017-08-24 17:02
Category: Ambiente, Nacional
Tags: Ambiente, hidrosogamoso
Slug: 50-familias-afectadas-por-proyecto-hidrosogamoso-en-sabana-de-torres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/file_image.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [24 Ago 2017] 

Las comunidades campesinas de la vereda San Luis de Río Sucio, en el municipio de Sabana de Torres, denuncian que la empresa ISAGEN – Brokfield, estaría ejerciendo diversas formas de presión para obligarnos a desalojar el puerto pesquero, fuente de trabajo de 50 familias que habitan el territorio, y continuar con el proyecto de Hidrosogamoso,  la última acción fue **el corte del suministro de energía que ya lleva 8 días**, impidiendo que las personas realicen muchas de sus actividades cotidianas.

Los habitantes señalaron que estos actos se vienen dando desde la puesta en marcha del proyecto eléctrico del Río Sogamoso (Hidrosogamoso) y que lo que busca la empresa es **provocar un desarraigo de los campesinos y de sus modos de subsistencia como lo son la pesca y la agricultura**. (Le puede interesar: ["73 familias afectadas por Hidrosogamoso esperan regresar a sus territorios tras acuerdo con ISAGEN"](https://archivo.contagioradio.com/73-familias-afectadas-por-hidrosogamoso-esperan-regresar-a-sus-territorios-tras-acuerdo-con-isagen/))

A esta situación se suman diferentes incumplimientos por parte de ISAGEN-Brokfield, hacia la comunidad, con la que se habían comprometido a iniciar proyectos productivos, la entrega de predios aptos para la producción agropecuaria y para el ejercicio de la actividad pesquera, sin embargo, los habitantes expresaron que por el contrario, **fueron reubicados en viviendas de interés social que no cuentan con servicios públicos como agua, luz y alcantarillado, además afirmaron que la pesca se ha mermado de forma alarmante. **

Frente a esta situación los campesinos manifestaron que continuaran en el puerto pesquero, debido a que es su única forma para subsistir y responsabilizan tanto a la Alcaldía de Sabana de Torres y a la gobernación del Santander, **de las afectaciones que puedan sufrir los habitantes, la falta de garantías en sus derechos y los daños ambientales que pueda provocar la multinacional**. (Le  puede interesar: ["Seis municipios afectados por la apertura de compuertas de Hidrosogamosa"](https://archivo.contagioradio.com/municipios_afectados_comuertas_hidrosogamoso/))

Además, le exigen a las autoridades administrativas que reconozcan la calidad de víctimas al ser familias que viven en la parte baja del Río Sogamoso, afectadas por la Hidroeléctrica operada por la empresa ISAGEN-BROOKFIELD, que se restablezca el servicio de la energía de forma urgente y que se garantice la vida digna de los campesinos y familias pesqueras que allí habitan.

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
