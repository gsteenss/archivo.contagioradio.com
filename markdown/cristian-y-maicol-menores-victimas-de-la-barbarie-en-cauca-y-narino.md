Title: Cristian y Maicol, menores víctimas de la barbarie en Cauca y Nariño
Date: 2020-08-10 20:37
Author: CtgAdm
Category: Actualidad, Nacional
Tags: Abandono Estatal, Asesinatos, Menores de edad, nariño, violencia
Slug: cristian-y-maicol-menores-victimas-de-la-barbarie-en-cauca-y-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/cauca-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 9 de agosto en horas de la noche organizaciones sociales y líderes comunales del departamento de Nariño, denunciaron el asesinato de dos menores de edad en medio de acciones ejecutadas por grupos armados en la zona.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho se presentó en la cabecera municipal, Leyva, donde los niños de 12 y 17 años, estudiantes del centro académico del corregimiento de Santa Lucía y perteneciente a la sección de bachillerato; **Cristian Felipe Caicedo y Maicol Ibarra oriundos de la vereda Mamaconde y Papayal Balboa en limite con el departamento de [Cauca](https://archivo.contagioradio.com/mientras-trabajaba-en-el-campo-fue-asesinado-el-lider-erminso-trochez-en-cauca/)**, fueron asesinado con arma de fuego.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La comunidad responsabiliza del hecho a los[grupos paramilitares](https://archivo.contagioradio.com/no-queremos-entrar-en-un-jubilo-ingenuo-ivan-cepeda/)que delinquen en esta zona, quienes en días anteriores han perpetrado 5 homicidios contra los pobladores, **asimismo a esta denuncia se suma la desaparición de dos jóvenes en el corregimiento de Puerto Nuevo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la defensora de derechos humanos y presidenta del Movimiento Alternativo Indígena y Social (MAIS), **los menores de 12 y 17 años fueron interceptados por hombres armados mientras se movilizaban al colegio a dejar una tarea,** *"condeno este hecho y el mundo entero debería rechazarlo Colombia está inmersa en la peor de las barbaries".*

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/marthaperaltae/status/1292866620986187778","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/marthaperaltae/status/1292866620986187778

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

A su vez el senador Roy Barreras repudio helecho señalando que ***"¿el asesinato de dos niños que iban a la escuela por paramilitares, no les generan "caravanas de solidaridad" a las élites uribistas?,** ¿les sigue pareciendo la "mayor injusticia de la historia de Colombia" el aislamiento en su hacienda del[senador Uribe](https://archivo.contagioradio.com/duque-y-su-partido-buscan-presionar-a-la-corte-suprema-en-caso-de-alvaro-uribe/)o del abogado [Cadena](https://archivo.contagioradio.com/no-queremos-entrar-en-un-jubilo-ingenuo-ivan-cepeda/)?".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las acciones violentas en el departamento de Nariño se han venido incrementando en el marco de la pandemia, ubicandolo según [Indepaz](http://www.indepaz.org.co/informe-especial-cauca-narino-crisis-de-seguridad-en-el-posacuerdo/) como el tercer departamento más violento, antecedido por Cauca y Antioquia. (Lea también:[971 defensores de DD.HH. y líderes han sido asesinados desde la firma del Acuerdo de paz](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y en donde la disputa territorial entre distintos frentes armados como el frente Oliver Sinisterra, Los Contador, las Autodefensa Unidas del Pacífico entre otros grupos armados que se suman a la presencia armada del Ejército, incrementan la conflictividad y los riesgos en la vida de las comunidades indígenas, campesinas y afrocolombianos que lo habitan.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"**Es una situación muy difícil que ha sido abandonada completamente por el Estado** y en donde hace presencia solamente con la que llegada de más y más Fuerza Pública, la cual no ha sido garantía de una seguridad para las comunidades"*, señaló Leonardo González, coordinador de proyectos de Indepaz .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Puede conocer un análisis de lo que ocurre en este departamento reviviendo, la emisión del 6 de agosto de **Otra Mirada: Nariño resistir ante el abandono estatal**)

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D899527523868967&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
