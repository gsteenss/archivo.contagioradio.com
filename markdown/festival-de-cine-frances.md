Title: Con documentales sobre Mayo del 68 inicia el 17 Festival de Cine Francés
Date: 2018-09-20 12:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Bogotá, Cine frances, Festival
Slug: festival-de-cine-frances
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/Frances-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa Festival 

###### 20 Sep 2018 

38 películas, entre las que se cuentan **29  largometrajes inéditos, 4 cortometrajes, cine de animación  y una experiencia de realidad virtual**, junto a componentes académicos y grandes invitados, hacen parte de la **edición 17 del Festival de Cine Francés** que se vive desde el 19 de septiembre al 17 de octubre en Colombia.

Serán **más de 10 ciudades de Colombia** a las que llegará el Festival, con una selección especial de **tres documentales para conmemorar los sucesos de Mayo del 68**; una retrospectiva del gran **Henri-Georges Clouzot** y una experiencia de realidad virtual  para acercar al público a nuevas tecnologías y nuevas experiencias de sentir, ver y escuchar el cine.

<iframe src="https://www.youtube.com/embed/vVkqQS4V498" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Como invitados del evento se encuentran el reconocido director de cine **Nicolas Boone**, quien presentará su cortometraje 'Las cruces', rodado en Bogotá y en el componente académico la participación de **Julián David Correa**, escritor, gestor cultural, crítico de cine y exdirector de la Cinemateca Distrital de Bogotá.

La muestra estará en la capital colombiana hasta el 3 de octubre, luego se trasladará del **4 al 17 a Medellín** y del **11 al 17  en Cali** y **Bucaramanga**, otras ciudades donde se presentará son Armenia, Cartagena, Cúcuta, Fusagasugá, Manizales, Neiva, Popayán, Pasto, San Gil, Santa Marta, Sincelejo y Tunja, del 5 al 22 de octubre.

Vea la programación completa haciendo click [acá](http://www.cinefrancesencolombia.com//programacion).

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
