Title: La Asociación Distrital de Educadores se toma la Secretaría de Educación
Date: 2017-08-15 15:47
Category: Educación
Tags: ade, Ministerio de Eduación
Slug: la-asociacion-distrital-de-educadores-se-toma-la-secretaria-de-educacion
Status: published

###### [Foto: Contagio Radio ] 

###### [15 Ago 2017] 

La Asociación Distrital de Educadores, a través de un comunicado de prensa, expresó que debido a los incumplimientos por parte del Ministerio de Educación ante los acuerdos pactados este año, **permanecerá en las instalaciones de la Secretaría de Educación, hasta que haya garantías para entablar un diálogo.**

De igual forma, en el comunicado manifiestan que han intentado reunirse con la secretaria de Educación María Victoria Angulo para exponerle **las represalias que hay en contra de los docentes provisionales y la falta de refrigerios** y almuerzos tanto para los estudiantes como para los docentes que hay actualmente en los colegios de la capital. (Le puede interesar:["Docentes podrían irse a paro si no se resuelve el Calendario para reponer clases"](https://archivo.contagioradio.com/docentes-estan-preocupados-por-el-calendario-de-recuperacion-de-clases/))

Otras de las demandas que hacen los maestros asociados a la ADE, son la falta de garantías para continuar con la implementación de la jornada única, que se brinden garantías para la evaluación del desempeño, **que se desarrolle la reposición de clases académicas en condiciones dignas para la comunidad educativa** y respeto por la democracia y autonomía escolar. (Le puede interesar: ["Hay propuestas pero el gobierno no tiene voluntad para negociar: FECODE"](https://archivo.contagioradio.com/maestros-tiene-propuestas-pero-el-gobierno-no-tiene-voluntad-para-negociar/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
