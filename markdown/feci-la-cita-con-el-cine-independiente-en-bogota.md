Title: FECI la cita con el Cine Independiente en Bogotá
Date: 2016-06-13 16:06
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine Tonalá, Cinemateca Distrital, FECI feria de cine independiente, Museo nacional Bogota
Slug: feci-la-cita-con-el-cine-independiente-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/13393932_2216028451871551_5865025033846498009_n-e1465824394433.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: FECI 

###### [13 Jun 2016] 

El cine y la producción audiovisual se encontrarán del 15 al 19 de junio en la segunda versión de la Feria Internacional de Cine Independiente de Bogotá, un evento pionero en el reconocimiento, visibilización y difusión del trabajo autogestionado de gestores y realizadores de Colombia y el mundo.

Henry Muñoz, director de la feria asegura que FECI "es un espacio para conocerse, interactuar y generar redes de apoyo entre los actores sector audiovisual independiente", propiciando un encuentro libre que facilite el diálogo y la apertura en el país a un mercado más democrático e incluyente.

Muestra de esa apuesta es la creación de la plataforma 'trueque sinergia', de la que se presentó un abrebocas en la primera edición y que busca generar una base de datos viva que alimente y retroalimente acciones audiovisuales a través de la aportación de ideas, de tiempo, de medios materiales o servicios.

La feria parte de la idea que en Colombia hay una industria que no se acomoda a las capacidades económicas del país, con un aparataje que requiere de altas inversiones en tiempo y dinero, para ser exhibidas por pocas semanas en salas con baja afluencia de público. Lo que se pretende mostrar, apoyado en experiencias internacionales, es que el cine puede hacerse con bajo presupuesto y ser de buena calidad.

La selección de producciones que harán parte de la [muestra oficial](http://fecibogota.com/recursos/archivos/programacion-fecibogota.pdf), está compuesta por más de 100 títulos, con 4 Festivales internacionales invitados, 71 participantes y 1 Festival nacional invitado, con funciones en Cine Tonalá, así como con la presentación de la plataforma Trueque sinergia en Impact Hub y en la Cinemateca Distrital de Bogotá.

Durante los 4 días del evento se realizará además un happening con la intervención de 5 directores invitados cuyo resultado se presentará al cierre del evento posterior al Bicinema, iniciativa que culminará en el Parkway, lugar de la proyección, la 1ra Muestra audiovisual de arte urbano, Batalla de Vjs, Mercado cinematográfico, un Taller de Cine recursivo , muestras y conversatorios (Ver la [programación completa](http://fecibogota.com/recursos/archivos/programacion-fecibogota.pdf)).

El lanzamiento de FECI tendrá lugar el miércoles 15 de junio desde las 6:15 de la tarde en el Museo Nacional, con proyecciones sobre la fachada y a las 7 p.m. al interior con una muestra en sala. Entrada libre.

<iframe src="http://co.ivoox.com/es/player_ej_11886029_2_1.html?data=kpalmpuUdpqhhpywj5aXaZS1kZmah5yncZOhhpywj5WRaZi3jpWah5yncanZz9fmjbLZaaSnhqee0d-PcYy6pqi2j4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
