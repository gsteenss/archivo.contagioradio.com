Title: "Despojar y desplazar: estrategia para el desarrollo de la orinoquía"
Date: 2018-02-15 14:25
Category: DDHH, Nacional
Tags: desplazamientos, despojos, empresas, lideres sociales, Organizaciones sociales, tierras
Slug: despojar-y-desplazar-estrategia-para-el-desarrollo-de-la-orinoquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Despojo-de-tierras-e1518786329996.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Protectionline] 

###### [15 Feb 2018] 

Diferentes organizaciones sociales que defienden los derechos humanos en el país, harán el lanzamiento del informe **“Despojar y Desplazar: Estrategia para el Desarrollo de la Orinoquía”**. En San José del Guaviare, el 16 de febrero, realizarán una socialización y un panel donde se abordará el tema de la tenencia de la tierra en los llanos orientales.

De acuerdo con Viviana Rodríguez, integrante de la Corporación Claretiana, el panel va a ser un espacio para discutir temas como **“¿en manos de quién está la tierra hoy y por qué?”** y a partir de ese análisis se abordarán casos específicos de empresas que han sido responsables de los desplazamientos de comunidades en lugares como el Meta, Vichada y Casanare.

### **Empresarios se han apropiado de las tierras de las comunidades** 

Entre algunos ejemplos de despojo cometidos por empresas, las organizaciones incluyeron en el informe los casos de los sectores de la agroindustria como el sector de la palma. Rodríguez indicó que se habla del caso de **Poligrow en Mapiripán**, el caso del grupo Corficolombia en Puerto López, Aceites Manuelita en Casanare y el grupo azucarero de los ingenios del Valle del Cauca que se trasladaron a los llanos.

Las organizaciones harán un llamado de atención sobre la situación que viven los **líderes sociales en el país** y que se han encargado de denunciar las acciones de estas empresas. Por esto, Rodríguez indicó que “se ha hecho un plan de incidencia en donde se manifiesta la necesidad de hablar sobre las responsabilidades de las empresas en medio del conflicto”. (Le puede interesar:["Los empresarios son los que están detrás de los asesinatos": Líderes del Bajo Atrato y Urabá"](https://archivo.contagioradio.com/hasta-cuando-y-cuantos-mas-continuan-la-violencia-contra-lideres-sociales-del-bajo-atrato-y-uraba/))

Además, en el informe “se trata el caso del Porvenir en Puerto Gaitán, que fue el primer intento de focalización de la de las Zonas de Interés de Desarrollo Rural Económico y Social” que han sido fuertemente cuestionadas. La ZIDRES, fueron denunciadas en el informe como otras leyes que buscan “acabar con la actividad agrícola familiar y **legalizar el acaparamiento de baldíos** por parte de personas que no son sujetos de reforma agraria”.

Finalmente, recalcó que el informe es un insumo para que en el debate se inste a las empresas a **reconocer su responsabilidad** y que “se profundice en las investigaciones”. El informe ya fue presentado ante la Defensoría del Pueblo y el Ministerio del Interior. El lanzamiento del informe se llevará a cabo el 16 de febrero en el auditorio UNAD de San José del Guaviare a las 2:00 pm.

<iframe id="audio_23809409" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23809409_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
