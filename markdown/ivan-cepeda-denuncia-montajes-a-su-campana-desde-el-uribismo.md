Title: Iván Cepeda denuncia montajes contra su campaña desde el uribismo
Date: 2018-02-06 13:20
Category: Otra Mirada, Política
Tags: Centro Democrático, Fernando Araujo, Iván Cepeda, Sergio Farjardo
Slug: ivan-cepeda-denuncia-montajes-a-su-campana-desde-el-uribismo
Status: published

###### [Foto: Archivo Particular] 

###### [06 Feb 2018] 

El senador Iván Cepeda, del partido Polo Democrático Alternativo, denunció que estaría siendo víctima de un montaje creado por uribistas con la finalidad de hacerle daño a su campaña electoral. Asimismo, aseveró que, detrás de estos hechos y las agresiones a otras campañas **hay intereses por parte de algunas corrientes políticas que buscan ''incitar al odio, la mentira y la difamación''.**

El montaje, de acuerdo con Cepeda, es una imagen en la que aparece al lado del candidato a la presidencia Sergio Fajardo, y en donde el número que se le asignó al actual senador, es erróneo, en vez del número 10, aparece un 2. **Este hecho para Cépeda encierra un conjunto de ''acciones mal intencionadas'' que lo único que buscarían es desinformar al electorado**.

“De manera premeditada se está confundiendo al elector con relación al número con el que me candidatizo al Senado”, afirmó Cépeda y aseguró que además le parece curioso que al lado de la imagen aparecieran comentarios provenientes de personas reconocidas como integrantes del uribismo y del Centro Democrático, cuestionando el hecho de que Fajardo tuviese dentro de sus aliados políticos a personas como el senador, a quien además han señalado de representar a la FARC.

Cepeda expresó que dentro de las personas que habrían compartido la imagen en sus cuentas de Twitter estarían **Fernando Araujo, senador por el Centro Democrático y hasta el presidente de este partido Álvaro Uribe Vélez**. (Le puede interesar: ["La respuesta de la FARC a las agresiones en Pereira](https://archivo.contagioradio.com/la-respuesta-de-la-farc-ante-las-agresiones-en-pereira/)")

![Montaje Campaña Cepeda](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/ivan.jpg){.alignnone .size-full .wp-image-51229 width="898" height="492"}

### **Las medidas de Cepeda para defender su campaña** 

El senador del Polo Democrático manifestó que su equipo de abogados está estudiando qué tipo de delito se comete cuando alguien usa elementos publicitarios para engañar al electorado. Además, la Ley 1864 del año 2017, protege los derechos del electorado, y de acuerdo con el congresista, **estas acciones pueden entenderse como un delito que puede ser castigado con prisión**.

Finalmente recordó que no es la primera vez que el Uribismo aplica esta estrategia y recordó lo sucedido con la campaña al plebiscito por la paz, en donde Juan Carlos Vélez, promotor de la campaña por el NO del Centro Democrático, afirmó que esta se basó en generar indignación y odio entre la ciudadanía y no en argumentos reales.

<iframe id="audio_23589252" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23589252_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
