Title: Los "Pájaros de Verano" aterrizan en las salas de cine
Date: 2018-08-01 15:29
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine Colombiano, Ciro Guerra
Slug: pajaros-de-verano-pelicula
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Portada.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pájaros de verano 

###### 01 Agos 2018 

Este 2 de agosto se estrena **Pájaros de Verano**, la más reciente película de Ciro Guerra (nominado al Oscar por El Abrazo de La Serpiente), que en esta ocasión, codirigió junto a Cristina Gallego; quien además aportó la idea original de la cinta.

Ambientada en la década de los 70 durante la época de la bonanza marimbera, cuenta la historia de **cómo el negocio de narcotráfico permeó las culturas indígenas Wayuú** y además, representó el enfrentamiento entre clanes por el poder y el dinero.

<iframe src="https://www.youtube.com/embed/YxiLJVwfZL0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Escrita por María Camila Arias y Jacques Toulemonde, Pájaros de Verano se aproxima a la historia de una familia de esa comunidad, que se divide entre la opulencia y sus propios valores ancestrales.

Un dato que vale la pena destacar es que **gran parte de la película es hablada en wayuunaiki**, la lengua amerindia de la península de La Guajira y además, se rodó en varios lugares de ese departamento y además, en la Sierra Nevada de Santa Marta.

Protagonizada por Natalia Reyes, Carmiña Martínez y José Acosta, cuenta además con laparticipación de un número importante de actores naturales de la región.

![pájaros de verano](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Imagen-2-600x400.jpg){.alignnone .size-medium .wp-image-55288 .aligncenter width="600" height="400"}

Medios especializados como The Hollywood Reporter y el diario Le Monde, la calificaron durante su proyección en la Quincena de realizadores de Cannes como una ficción antropomórfica que se emplea para contar un relato que a su vez es crudo y mordaz.

Pájaros de Verano también logró contar con distribución internacional en diversos países; entre ellos, Estados Unidos.

###### Encuentre más información sobre Cine en [24 Cuadros](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
