Title: Así es el decreto de emergencia cárcelaria en Colombia
Date: 2020-03-23 19:46
Author: CtgAdm
Category: Actualidad, Nacional
Tags: colombia
Slug: asi-es-el-decreto-de-emergencia-carcelaria-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Crisis-humanitaria-cárceles.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este lunes 23 de marzo el Instituto Nacional Penitenciario y Carcelario (INPEC) y el Ministerio de Justicia y luego de los hechos ocurridos en diferentes penitenciarias en todo el país, declaran estado de emergencia carcelaria. (Le puede interesar:<https://archivo.contagioradio.com/hay-negligencia-de-estado-contra-reclusos-que-protestaba-por-riesgo-de-covid/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los hecho en tema de salud y orden público que afectaron a los reclusos de las cárceles Buen Pastor, Picota, Modelo, Picaleña y Cómbita; así como a los de las cárceles de Palmira, Jamundí, y La Dorada en Caldas, fueron los detonantes para las medidas tomadas por estos organismos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Esto trae consigo la declaratoria de emergencia cárcelaria

<!-- /wp:heading -->

<!-- wp:paragraph -->

En primer lugar se determina que para descongestionar los centros de reclusión, se optará de manera transitoria por la medida de prisión domiciliaria a mayores de 60 años, enfermos, o personas con condenas menores a 5 años o por tengan delitos culposos; así como también darán permisos de de 72 horas a algunos reclusos. Acciones que por ahora tendrán un tiempo de tres meses prorrogables.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se restringirá la salida a personas que estén recluidas por por delitos de lesa humanidad, delitos contra niños, niñas y adolescentes, delitos contra la Administracion Pública, o que tengan vínculos directos o indirectos con el conflicto armado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez se dará facultades al INPEC, para realizar traslados presupuestales a prioridades actuales; así como el Ministerio de Defensa diseñará una estrategia de defensa juridica con garantías procesales en acompañamiento de la Defensoria del Pueblo, en los diferente casos denunciados este fin de semana, empezando por los hechos dados en la cárcel Modelo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Carta Abierta al Presidente Iván Duque

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por medio de un documento este lunes más de 200 organizaciones de Derechos Humanos, y cerca de 600 personas presentaron una carta al Presidente de Colombia solicitando que tome medidas preventivas y para afrontar el COVID19 en el sistema carcelario, penitenciario y centros transitorios de reclusión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dentro de las [peticiones](http://www.comitedesolidaridad.com/es/content/carta-abierta-al-presidente-iv%C3%A1n-duque) hay cinco claves para los firmantes, la primera se refiere al personal penitenciario y administrativo, el cual debe ser regulado, teniendo en cuenta que los funcionarios *"tienen contacto con el mundo exterior lo que lo convierte en un posible vehículo de contagio*".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Segundo se refieren al hacinamiento el cual oscila en un 55% en algunas cárceles, y otras supera un 260%, situación que *"imposible generar espacios de aislamiento social al interior de las cárceles y prisiones"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tercero señalan que muchas cárceles no tienen suministro de agua lo cual afecta las rutinas de limpieza corporal, espacios comunes y de celdas; conectado a esto está la solicitud de kits de aseo para las personas privadas de la libertad, los cuales hasta el momento deben ser entregados por el INPEC y han sido insuficientes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último señalan que el sistema de salud de las cárceles no alcanza a atender las necesidades actuales, esto debido a la carencia de personal médico, áreas de sanidad, insumos y medicamentos.

<!-- /wp:paragraph -->
