Title: Carlos Sánchez líder comunal es asesinado en Tibú, Norte de Santander
Date: 2020-05-05 11:02
Author: CtgAdm
Category: Actualidad, Nacional
Tags: asesinato, lideres sociales asesinados, Tibú
Slug: carlos-sanchez-lider-comunal-es-asesinado-en-tibu-norte-de-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Publicación-de-Facebook-Marrón-Collage-Liso-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 4 de mayo se conoció el asesinato del líder comunal **Carlos Andrés Sánchez Villa** en **zona rural de [Tibú](https://archivo.contagioradio.com/asesinan-a-efren-ospina-lider-de-sustitucion-de-cultivos-ilicitos-en-catatumbo/),** municipio del Catatumbo, al Norte de Santander.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según integrantes de la comunidad el hecho se presentó en horas de la noche cuando el líder se movilizaba en su camioneta, por una vía que conecta a Tibú con el corregimiento de La Gabarra; cuando **dos hombres armados en moto le dispararon en repetidas ocasiones hasta causar su muerte.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Carlos Sánchez tenía 37 años de edad y se desempeñaba como **vicepresidente de la Junta de Acción Comunal de la vereda La Raya**; ante su asesinato la comunidad manifiesta que a la fecha no había denunciado ningún tipo e amenaza en su contra, lo cual dificulta determinar los autores de este homicidio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado **Juan Carlos Quintero**, directivo de la Asociación Campesina del Catatumbo (**ASCAMCAT**), señala, en su cuenta de twitter, que es cuestionable que el asesinato de Carlos Sánchez **se presentara a pocos kilómetros de un puesto de control permanente del** [**Ejército Nacional**.](https://archivo.contagioradio.com/seguimientos-contra-periodistas-son-propios-de-regimenes-totalitarios-flip/) 

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/catatumbol281/status/1257474018636161024","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/catatumbol281/status/1257474018636161024

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Así mismo Gustavo Mestre, defensor de Derechos Humanos compartió una alarmante lista sobre los **21 lideres sociales y excombatientes asesinados solo en el mes de abril**, en los diferentes departamentos del país, en donde el mayor número se concentra la zona del Cauca.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/guenmecu/status/1257686283033477120","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/guenmecu/status/1257686283033477120

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Ante esto, y la cifra total de 84 liderazgos silenciados por la violencia desde enero hasta el 30 de abril del 2020, según el Instituto de Estudios para el Desarrollo y la Paz (Indepaz), voceros de Ascamcat advierten que este patrón de violencia no se detendrá, hasta que el Gobierno haga una acción contundente en camino a la paz definitiva.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(También le puede interesar leer: <https://www.justiciaypazcolombia.com/inteligencia-ilegal-contra-lideresas-y-defensores-de-jp/>)

<!-- /wp:paragraph -->
