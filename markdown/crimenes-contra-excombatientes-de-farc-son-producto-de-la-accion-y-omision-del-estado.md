Title: «Crímenes contra excombatientes de FARC son producto de la acción y omisión del Estado»
Date: 2020-07-15 22:51
Author: PracticasCR
Category: Actualidad, Nacional
Tags: etcr, Excombatientes de FARC, Exterminio sistemático, Partido FARC
Slug: crimenes-contra-excombatientes-de-farc-son-producto-de-la-accion-y-omision-del-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Excombatientes-de-FARC-asesinados.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/ETCR-Farc.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

218 excombatientes de FARC asesinados / Foto: Tiempo Real

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El **desplazamiento forzado de un grupo de excombatientes de FARC** desde el Espacio Territorial de Capacitación y Reincorporación –ETCR- «Santa Lucia» en Ituango, hacia la zona urabeña de Mutatá, es un hecho más, **que ha dejado en evidencia el peligro que corren las personas firmantes del Acuerdo de Paz.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El partido FARC ha denunciado un «exterminio sistemático» del que están siendo víctimas con **el asesinato de 218 excombatientes desde la firma del Acuerdo en 2016.** (Lea también: [Asesinan a José Antonio Rivera excombatiente de FARC](https://archivo.contagioradio.com/asesinan-a-jose-antonio-rivera-excombatiente-de-farc/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a este panorama, Camilo Fagua, abogado integrante de la Comisión de Seguimiento, Impulso y Verificación del Acuerdo de Paz, señaló que los firmantes del Acuerdo son sujetos de protección del Derecho Internacional Humanitario –DIH- aun cuando ya han depuesto sus armas y denunció la falta de presencia y soberanía en los territorios por parte del Estado Colombiano, lo que según él, ha llevado a la trágica cifra de asesinatos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En relación con el traslado forzoso de los excombatientes de FARC del ETCR de «Santa Lucia», el abogado aseguró que **no es el primer desplazamiento de personas en proceso de reincorporación, ya que, esto había sucedido previamente en los ETCR de Tierralta, Cordoba y Vidrí, Antioquia**; los cuales fueron suprimidos de manera unilateral por el Gobierno.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **En Ituango hoy tienen que trasladarse \[los excombatientes\] de ese territorio producto de la violencia y del continuo acecho de grupos paramilitares que están alrededor de ese ETCR \[…\] ante los ojos cómplices de la Fuerza Pública»**
>
> <cite>Camilo Fagua, abogado integrante de la Comisión de Seguimiento, Impulso y Verificación del Acuerdo de Paz</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Abandono estatal en los territorios y Espacios de Reincorporación de excombatientes de FARC

<!-- /wp:heading -->

<!-- wp:paragraph -->

Camilo Fagua aseguró también que **cerca del ETCR de «Santa Lucia» tienen influencia seis estructuras al margen de la ley y que en Ituango han sido asesinados 11 excombatientes** ante la inoperancia de la Fuerza Pública presente en dicha zona, lo cual, motivó el traslado forzado de las personas que adelantaban allí su proceso de reincorporación.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***Ituango es una población desprotegida y dejada a merced de la violencia por la acción y la omisión de parte del Estado Colombiano»***
>
> <cite>Camilo Fagua, abogado integrante de la Comisión de Seguimiento, Impulso y Verificación del Acuerdo de Paz</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por su parte, Oscar Yesid Zapata, integrante de la [Fundación Sumapaz](https://www.sumapaz.org/), señaló que el Estado Colombiano pese al conocimiento pleno que tiene de la situación en los territorios se niega a hacer algo al respecto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, aseguró que Mutatá también es un territorio completamente dominado por grupos paramilitares como las autodenominadas Autodefensas Gaitanistas de Colombia –AGC-, razón por la cual **el riesgo que corren los excombatientes trasladados sigue latente.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***Están trasladando un grave problema en Ituango a una región que igualmente padece una situación difícil en este momento como es la zona del Urabá antioqueño»***
>
> <cite>Óscar Yesid Zapata, defensor de DD.HH e integrante de la Fundación Sumapaz</cite>

<!-- /wp:quote -->

<!-- wp:image {"align":"center","id":86770,"sizeSlug":"large"} -->

<div class="wp-block-image">

<figure class="aligncenter size-large">
![Espacio Territorial de Capacitación y Reincorporación (ETCR)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/ETCR-Farc.jpg){.wp-image-86770}  
<figcaption>
Espacio Territorial de Capacitación y Reincorporación (ETCR) / Verdad Abierta
</figcaption>
</figure>

</div>

<!-- /wp:image -->

<!-- wp:paragraph -->

Según, Oscar Zapata, Urabá es una zona que conecta al departamento de Antioquia con la Costa Atlántica, lo que no es un hecho menor, si se tiene en cuenta que se convierte en una especie de enclave para el tráfico de sustancias de uso ilícito que circulan «*ante la permisividad de las autoridades*».

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***El Estado sabe por dónde está saliendo la cocaína y no hace nada»***
>
> <cite>Óscar Yesid Zapata, defensor de DD.HH e integrante de la Fundación Sumapaz</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### «**Reconfiguración paramilitar**»

<!-- /wp:heading -->

<!-- wp:paragraph -->

Oscar Zapata, advierte **la reconfiguración que están llevando a cabo los grupos paramilitares en el país**; ejemplo de ello es la «Operación Mil»  adelantada por las –AGC-, la cual, consiste en llevar mil hombres armados pertenecientes a sus filas a la zona de Urabá para ejercer el control territorial.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También alerta que dicha **reconfiguración del paramilitarismo se está dando con la connivencia por acción y omisión del Estado.** (Le puede interesar: [Urabá fue laboratorio de lo que pasó entre militares y paramilitares en el resto del país: Coronel (R) Velásquez](https://archivo.contagioradio.com/uraba-fue-laboratorio-de-lo-que-paso-entre-militares-y-paramilitares-para-el-pais-coronel-r-velasquez/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***Hemos evidenciado que en algunas partes \[de Antioquia\] estos grupos paramilitares y sus altos mandos son ayudados por miembros de la institucionalidad»***
>
> <cite>Óscar Yesid Zapata, defensor de DD.HH e integrante de la Fundación Sumapaz</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

En los estudios adelantados por la Fundación Sumapaz, señala Oscar Zapata, se ha confirmado que **los 125 municipios de Antioquia tienen presencia de al menos un grupo paramilitar en su territorio**, es decir, no hay un solo municipio de dicho departamento libre de la presencia de estos grupos armados.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Territorio en disputa**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Oscar Zapata, manifestó que el tema del **uso de la tierra en Antioquia, no solo atiende a las disputas territoriales de los grupos armados sino también a «*intereses de grandes poderes económicos y políticos*».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, que de la zona de Urabá históricamente han sido desplazados muchos campesinos por cuenta de la violencia y que sus predios fueron posteriormente apropiados y ocupados por grandes terratenientes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Oscar, señala que con la apertura de nuevos territorios, que antes estaban vedados para la explotación minera y extractiva de la industria, por el control ejercido por las FARC-EP, **podrían llegar a repetirse fenómenos de desplazamiento forzado de las comunidades y el despojo de sus tierras.** (Le puede interesar: [Denuncian que empresa bananera desarrolla campaña en contra de la Restitución de tierras](https://archivo.contagioradio.com/denuncian-que-empresa-bananera-desarrolla-campana-en-contra-de-la-restitucion-de-tierras/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***El dueño de la tierra es quien ejerce el uso de las armas»***
>
> <cite>Óscar Yesid Zapata, defensor de DD.HH e integrante de la Fundación Sumapaz</cite>

<!-- /wp:quote -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/606485226733305","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/606485226733305

</div>

<figcaption>
Otra Mirada: Desplazamiento en Ituango - Excombatientes buscan un futuro en paz

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
