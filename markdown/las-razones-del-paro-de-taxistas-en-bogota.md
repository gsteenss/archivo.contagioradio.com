Title: Las razones del paro de taxistas en Bogotá
Date: 2016-03-14 16:13
Category: Movilización, Nacional
Tags: Protestas contra Uber, Taxis Bogotá, Uber
Slug: las-razones-del-paro-de-taxistas-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Taxistas.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Periódico Excélsior ] 

<iframe src="http://co.ivoox.com/es/player_ek_10799427_2_1.html?data=kpWkm56Ydpihhpywj5eUaZS1kZiah5yncZOhhpywj5WRaZi3jpWah5ynca3V1JDfw9%2FTssbnjMnSzpDUpdPjjMnSjdnFvMrn1cbgjcrSb6PjyNThh6iXaaKljoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alonso Romero, ASOTAXIS] 

###### [14 Mar 2016 ]

Este lunes taxistas de Bogotá se manifiestan contra la implementación de la plataforma de transporte Uber, pues de acuerdo con Alonso Romero, presidenste de ASOTAXIS, **permitir a los carros particulares prestar servicios de transporte público sin regulación estatal, es ilegal** y agudiza las negativas condiciones laborales de un gremio que "está en quiebra", como él afirma.

Romero asevera que Uber al ser manejada por una multinacional, **beneficia al capital privado en detrimento de los derechos de los usuarios y de las condiciones laborales de los taxistas** que no cuentan con seguridad social y quienes se ven obligados a llegar sin dinero a sus casas, debido a las altas tarifas de las empresas en las que están inscritos y a los altos costos de los combustibles, "nos acabaron de arruinar, es el tope", agrega.

De acuerdo con el taxista, **resulta ilógico que el Gobierno apruebe la "competencia desleal" que representa Uber** que cobra tarifas elevadas y apropia el 20% de cada carrera, teniendo en cuenta las leyes establecidas para la regulación de las empresas de transporte, razón por la que exigen al Presidente y a los Ministros de Transporte y de las TIC impulsar soluciones que mejoren la prestación del servicio, **erradiquen la pirateria, cambien las tarifas de las empresas de taxis y posibiliten condiciones que garanticen estabilidad laboral**.

Los taxistas plantean propuestas para brindar un mejor servicio como la creación de una plataforma para que los usuarios y alguno de sus familiares sepan en que taxi van, con qué conductor y la ruta que va tomando, y esperan que esta movilización pacífica logre que el Gobierno impulse iniciativas para mejorar el servicio y sus condiciones laborales, así como derogar la aprobación de Uber, por lo **que continuarán en el denominado 'plan tortuga' hasta obtener una respuesta contundente**.

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
