Title: Juez podría dictar cárcel contra presidente Duque por desacato a tutela
Date: 2020-09-11 15:52
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Cese al fuego, Comunidades Indígenas en Colombia, comunidades negras, Iván Duque
Slug: juez-dictar-carcel-contra-presidente-duque-desacato-tutela
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Iván-Duque-ante-la-ONU.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Iván Duque / Presidencia de Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El Tribunal Administrativo de Cundinamarca, Sección Segunda, Subsección “F” tiene en sus manos un incidente de desacato a la acción de tutela presentada por un grupo de más de 100 comunidades negras, mestizas e indígenas en el país, **que exigen acuerdos humanitarios regionales y un cese al fuego para aliviar el conflicto en sus territorios.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La **tutela, que fue fallada a favor de las comunidades el pasado 20 de Agosto** ordena que se de respuesta a la solicitud de las comunidades en torno a la orden de un Cese al Fuego. [(Lea también: Tribunal ordena a Duque responder a comunidades que exigen cese al fuego en sus territorios)](https://archivo.contagioradio.com/tribunal-ordena-a-duque-responder-a-comunidades-que-exigen-cese-al-fuego-en-sus-territorios/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las comunidades habían expresado con anterioridad a través de [11 cartas,](https://www.justiciaypazcolombia.com/carta-abierta-9-nomassilencio/)se ampararan los derechos fundamentales a la paz, la vida, la salud y solicitando un Acuerdo Humanitario Global en los territorios ante las reiteradas vulneraciones que han sufrido históricamente. [(Conozca el contenido de las cartas firmadas por las comunidades)](https://www.justiciaypazcolombia.com/cartaabierta11-presidente-duque-pare-ya-la-matanza/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “SEGUNDO.- ORDÉNESE al PRESIDENTE DE LA REPÚBLICA IVÁN  
> DUQUE MÁRQUEZ, que en un término no mayor a cinco (5) días, contados a partir de la notificación de esta providencia, en su calidad constitucional de Comandante Supremo de las Fuerzas Armadas de la República responda de fondo las solicitudes radicadas el 27 de marzo y 9 de abril de 2020, por las comunidades étnicas accionantes con el respaldo de la [Comisión Intereclesial de Justicia y Paz](https://twitter.com/Justiciaypazcol), por medio de las cuales solicitaron el cese al fuego en las zonas étnicas que representan los accionantes”.
>
> <cite>Fallo de Tutela del 20 de Agosto</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Presidente Duque tiene 24 horas para responder al tribunal

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En el fallo del tribunal se otorgan 48 horas al presidente Iván Duque para que de respuesta a esta tutela, de lo contrario estaría avocado ese tribunal a ordenar un incidente de desacato que podría culminar en una medida de detención por incumplir el fallo del tribunal. [Organizaciones sociales convocaron a un Pacto por la Vida y la Paz](https://archivo.contagioradio.com/organizaciones-sociales-convocaron-a-un-pacto-por-la-vida-y-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El fallo indica en su parte resolutiva que el presidente responda, si se ha dado cumplimiento al fallo de tutela de 20 de agosto de 2020, en los términos indicados en la presente providencia. En caso que su respuesta sea afirmativa, debe adjuntar los soportes correspondientes, **en caso que no se haya cumplido el fallo, se informen las razones por las cuales no se ha dado cumplimiento.** [(Lea también:Comunidades le recuerdan al Gobierno que acudir a aspersión aérea sería ilegal)](https://archivo.contagioradio.com/comunidades-le-recuerdan-al-gobierno-que-acudir-a-aspersion-aerea-seria-ilegal/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la ausencia de respuestas por parte del presidente Duque, la Comisión Intereclesial de Justicia y Paz ha comenzado [una petición](https://www.change.org/p/presidente-tiene-48-horas-para-dar-cumplir-orden-de-juez-que-exige-respuesta-precisa-a-propuesta-de-cese-de-fuego?utm_content=cl_sharecopy_24646239_es-419%3A5&recruiter=29886948&utm_source=share_petition&utm_medium=copylink&utm_campaign=share_petition&utm_term=psf_combo_share_initial)a la que también se puede unir la ciudadanía, exigiendo respuestas en el marco de las garantías al derecho a la paz y la vida.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
