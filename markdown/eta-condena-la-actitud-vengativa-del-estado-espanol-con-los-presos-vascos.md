Title: ETA condena la actitud "vengativa" del estado español con los presos vascos
Date: 2015-01-29 19:46
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: españa, ETA, País Vasco
Slug: eta-condena-la-actitud-vengativa-del-estado-espanol-con-los-presos-vascos
Status: published

###### Foto:Vozpupuli.com 

Mediante un comunicado enviado a la dirección de la edición digital del diario GARA, el grupo armado **ETA**, ha recordado al preso etarra **Josu Uribetxebarria**, denunciando la situación carcelaria de todo el **colectivo de presos vascos**, como un acto de **venganza por parte del estado español.**

También ha puntualizado que el gobierno está buscando otras vías legales para mantener  a los presos que no han salido, en **prisión perpetua**, incluso devolver a la prisión a quienes ya pagaron una condena.

Ha criticado fuertemente al gobierno vasco por la actuación policial contra **16 abogados del colectivo de presos**, ya que en su opinión , el gobierno habla mucho de derechos humanos pero no impide las vulneraciones de este.

Es importante puntualizar que el preso político Josu Uribetxebarria, condenado a 32 años, y brutalmente torturado,  contrajo una enfermedad terminal en prisión, por la que debería de haber salido inmediatamente. Sólamente después de una larga lucha consiguió su libertad, pero aún libre y gravemente enfermo la justicia española intentó volver a encarcelarlo.
