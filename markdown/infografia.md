Title: Infografia
Date: 2015-02-27 14:41
Author: AdminContagio
Slug: infografia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Infografía-Somos-Defensores.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/INFOGRAFIA-MESP.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/infografia-salario.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/INFOGRAFIA-REFICAR-CONTAGIORADIO.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/INFOGRAFIA-REFICAR-CONTAGIO-RADIO.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/INFOGRAFIA-CORRECION.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/infografia_patrocinadores_corridas.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/INFOGRAFIA-CORRECION.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Infografia.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Infografia-Medios.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/INFOGRAFIA-PIEDRA-EN-EL-ZAPATO.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/infografia-lgbti-colombia.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/9-derechos-de-ppl_44767473435_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/10-crceles-con-mayor-sobrepoblacin-en-colombia_31268380387_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/20-aos-de-la-sentencia-t-153_43706849020_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/ciclo-de-escolarizacion-en-las-carceles_46092940531_o.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/comunidad-lgtbi-en-las-carceles_43706848310_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/cuanto-le-cuesta-un-preso-al-estado_44798714574_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/derechos-sexuales-y-reproductivos-en-las-crceles_46217749841_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/enfoque-diferencial-en-las-crceles_32147079428_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/entidades-encargadas-de-las-carceles_43706847360_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/establecimientos-carcelarios-y-recomendaciones-tcnicas-del-cicr_45141212634_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/hacinamiento-en-las-prisiones-colombianas_44798713874_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/indultos-y-amnistia_43706845620_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/la-educacin-en-las-crceles-de-colombia_32147082638_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/los-10-delitos-que-encabezan-la-lista-de-judializacin-en-colombia_44202393650_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/movimiento-nacional-carcelario_30926516657_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/mujeres-en-las-crceles_45526110681_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/nueva-cultura-penitenciaria_44798712904_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/primera-infancia-en-las-crceles_32147084138_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/qu-come-un-preso_-1_46194891011_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/que-es-el-delito-politico_44798712394_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/salud-en-las-carceles_31651636798_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/tramacua_el-guantanamo-de-colombia_44798711044_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/trastornos-mentales-en-las-crcerles_31153668907_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/violencia-contra-la-comunidad-lgbti-en-las-crceles_44798710224_o.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/fuerzas-armadas-de-policía-policía-judical-y-servicios-de-inteligencia-1-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/QUE-HACER-EN-CASO-DE-UN-ALLANAMIENTO.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/QUÉ-HACER-EN-CASO-DE-UN-ALLANAMIENTO.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/QUÉ-HACER-EN-CASO-DE-UN-ALLANAMIENTO.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/QUÉ-HACER-EN-CASO-DE-UN-ALLANAMIENTO-1.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

### INFOGRAFÍA

[![El Río Cauca en estado de emergencia tras operación de Hidroituango](https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-6.png "El Río Cauca en estado de emergencia tras operación de Hidroituango"){width="741" height="504" sizes="(max-width: 741px) 100vw, 741px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-6.png 741w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-6-300x204.png 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-6-370x252.png 370w"}](https://archivo.contagioradio.com/el-rio-cauca-en-emergencia-ambiental-por-sequias/)  

###### [El Río Cauca en estado de emergencia tras operación de Hidroituango](https://archivo.contagioradio.com/el-rio-cauca-en-emergencia-ambiental-por-sequias/)

[<time datetime="2019-01-30T16:06:56+00:00" title="2019-01-30T16:06:56+00:00">enero 30, 2019</time>](https://archivo.contagioradio.com/2019/01/30/)Habitantes de Caucasia, Antioquia expresaron alarma sobre los bajos niveles de agua en el Río Cauca.[LEER MÁS](https://archivo.contagioradio.com/el-rio-cauca-en-emergencia-ambiental-por-sequias/)  
[![Panfleto del Quintín Lame ¿una estrategia para justificar asesinatos de líderes?](https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1.jpg "Panfleto del Quintín Lame ¿una estrategia para justificar asesinatos de líderes?"){width="700" height="500" sizes="(max-width: 700px) 100vw, 700px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1.jpg 700w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1-300x214.jpg 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1-275x195.jpg 275w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1-370x264.jpg 370w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1-110x78.jpg 110w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1-240x170.jpg 240w"}](https://archivo.contagioradio.com/panfleto-del-quintin-lame-una-estrategia-para-justificar-asesinatos-de-lideres/)  

###### [Panfleto del Quintín Lame ¿una estrategia para justificar asesinatos de líderes?](https://archivo.contagioradio.com/panfleto-del-quintin-lame-una-estrategia-para-justificar-asesinatos-de-lideres/)

[<time datetime="2019-01-30T14:21:50+00:00" title="2019-01-30T14:21:50+00:00">enero 30, 2019</time>](https://archivo.contagioradio.com/2019/01/30/)Integrantes de la comunidad Nasa rechazaron los panfletos firmados por un grupo identificado como Quintin lame y abogaron por la paz[LEER MÁS](https://archivo.contagioradio.com/panfleto-del-quintin-lame-una-estrategia-para-justificar-asesinatos-de-lideres/)  
[![Sur de Bogotá: Condenado a recibir la basura de la ciudad por 37 años más](https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400.jpg "Sur de Bogotá: Condenado a recibir la basura de la ciudad por 37 años más"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400.jpg 770w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400-300x156.jpg 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400-768x399.jpg 768w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400-370x192.jpg 370w"}](https://archivo.contagioradio.com/sur-bogota-basura/)  

###### [Sur de Bogotá: Condenado a recibir la basura de la ciudad por 37 años más](https://archivo.contagioradio.com/sur-bogota-basura/)

[<time datetime="2019-01-30T13:18:23+00:00" title="2019-01-30T13:18:23+00:00">enero 30, 2019</time>](https://archivo.contagioradio.com/2019/01/30/)En caso de aprobarse la propuesta de la alcaldía Peñalosa, el sur de Bogotá tendría que seguir recibiendo la basura de la Ciudad por 37 años más[LEER MÁS](https://archivo.contagioradio.com/sur-bogota-basura/)  
[](https://archivo.contagioradio.com/espacio-humanitario-de-puente-nayero-denuncia-presencia-de-neoparamilitares/)  

###### [Espacio Humanitario Puente Nayero denuncia presencia de neoparamilitares](https://archivo.contagioradio.com/espacio-humanitario-de-puente-nayero-denuncia-presencia-de-neoparamilitares/)

[<time datetime="2019-01-30T12:27:21+00:00" title="2019-01-30T12:27:21+00:00">enero 30, 2019</time>](https://archivo.contagioradio.com/2019/01/30/)La Comisión de Justicia y Paz denunció la aparición de noparamilitares en el espacio humanitario Puente Nayero, en Buenaventura[LEER MÁS](https://archivo.contagioradio.com/espacio-humanitario-de-puente-nayero-denuncia-presencia-de-neoparamilitares/)
