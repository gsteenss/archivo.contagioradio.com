Title: Se esperan 4.5 millones de firmas que respalden el Referendo por el agro
Date: 2016-06-17 16:22
Category: DDHH, Nacional
Tags: Dignidad Agropecuaria, Producción agrícola en Colombia, Referendo por el agro
Slug: se-esperan-4-5-millones-de-firmas-que-respalden-el-referendo-por-el-agro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/campesinos-boyaca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [17 Junio 2016 ] 

La organización Dignidad Agropecuaria Colombiana lidera una iniciativa de Referendo por el agro que de acuerdo con el vocero Francisco Alvarado, busca **ampliar los artículos 65, 65, 66 y 100 de la Constitución Nacional** para que se amplíen las garantías para el sector del campesinado colombiano, como lo han logrado los pueblos afro e indígenas, con el fin de que  se respete producción nacional y se fortalezca la soberanía alimentaria.

Actualmente se importan cerca de 5 millones de toneladas de maíz, que antes se producían en los campos colombianos; sin embargo, las **familias campesinas se han visto obligadas a desplazarse a la ciudad al no contar con garantías** para la tenencia y producción de la tierra, frente a las ventajas con las que cuentan los extranjeros que llegan al país, para comprar terrenos y cultivar, según afirma el vocero.

Los campesinos también exigen que se re negocien los Tratados de Libre Comercio, pues las ganancias para los extranjeros son mayores que para los nacionales, pues **los productos están siendo vendidos por debajo de su costo real aunque el dólar ha subido**. "En otras naciones los gobiernos protegen las producciones , por eso estamos luchando, por toda la agricultura nacional y la soberanía alimentaria, previniendo a futuro los impactos de la crisis mundial alimentaria que se viene".

<iframe src="http://co.ivoox.com/es/player_ej_11941860_2_1.html?data=kpamlpacepGhhpywj5abaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5yncafmwtPQy9jHs4y1zdvO1MbIs4yhjKnWydPNqMLYjMbU1NTUqcTpwtfWw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
