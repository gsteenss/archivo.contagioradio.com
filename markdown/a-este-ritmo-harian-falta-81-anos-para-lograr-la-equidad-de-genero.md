Title: A este ritmo "harán falta 81 años más para lograr la equidad de género”
Date: 2015-09-15 10:58
Category: Entrevistas, eventos, Mujer
Tags: Adriana Benjumea, Corporación Humanas, Mujeres y participación política, Mujeres y Paz, Proceso de paz en Colombia, Sexo y poder", Universidad de los Andes
Slug: a-este-ritmo-harian-falta-81-anos-para-lograr-la-equidad-de-genero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Corporación-Humanas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corporación Humanas] 

<iframe src="http://www.ivoox.com/player_ek_8395221_2_1.html?data=mZiml5eWdY6ZmKiak5aJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhYzZ1NnSjdfNuM7jjIqflM3Ftoa3lIqupsbSb8fVzdnOjZ2Vb8KZpJiSpJbTt4zkwtfOjdHTq9PV05DZw5DJtdahhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Adriana Benjumea, Corporación Humanas] 

###### [15 Sept 2015] 

[El próximo 17 de Septiembre la Corporación Humanas realizará el Seminario Internacional “**Sexo y Poder: Hacia un posconflicto con apuestas feministas”** en la Universidad de los Andes, con el fin de conmemorar sus **10 años de activismo en torno a la participación de las mujeres en sociedades y sus propuestas en durante el conflicto y el posconflicto**.]

[Adriana Benjumea, directora de la Corporación, afirma que este seminario busca ser un espacio de reflexión sobre la participación política de las mujeres en un escenario de posconflicto **“sin la voz de las mujeres es imposible pensar en una paz duradera en Colombia”**.]

[**“Sí seguimos con este ritmo tan lento harán falta 81 años más para lograr la equidad de género”**, agrega Benjumea, quien añade que a partir de esa urgencia nacen espacios como el "Seminario Sexo y poder" donde se contará con la participación de mujeres de Centro América, América Latina, Estados Unidos y África, lideresas de **experiencias de resistencia y participación política comunitaria en territorios militarizados o en terminación del conflicto**.  ]

[En un escenario de posconflicto la participación de las mujeres tiene que ser central, señala Benjumea **“las mujeres tenemos que dejar de ser un comodín en la política, el cuidado debe ser asignado en partes iguales a hombres y mujeres”**.]

[Sí bien la participación de las mujeres en el actual proceso de negociaciones ha aumentado, es necesario **continuar el debate sobre las formas de violencia agenciadas en su contra en el marco del conflicto, así como de sus apuestas de paz y participación en los estamentos de la política formal,** concluye la directora de la Corporación Humanas.]

[Conozca la agenda:]

[![Agenda Sexo y Poder](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Agenda-Sexo-y-Poder.jpg){.aligncenter .size-full .wp-image-14051 width="951" height="2818"}](https://archivo.contagioradio.com/a-este-ritmo-harian-falta-81-anos-para-lograr-la-equidad-de-genero/agenda-sexo-y-poder/)

[ ]
