Title: Detienen 6 estudiantes de la Universidad Pedagógica durante protestas
Date: 2017-05-16 16:50
Category: Nacional
Tags: ESMAD, estudiantes, Universidad Pedagógica Nacional
Slug: detenidos-estudiantes-de-la-upn-por-agentes-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/C_-e7c1VoAAGH_Y.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [16 May 2017]

Desde tempranas horas, estudiantes de la Universidad Pedagógica Nacional salieron a manifestar su apoyo a las comunidades de Buenaventura que entraron en paro cívico y los docentes del país que cumplen 6 días en paro nacional. Mientras desarrollaban el plantón agentes del ESMAD los atacó generando un disturbio que provocó la captura de **6 estudiantes  y aún no se tendría información sobre su paradero.**

De acuerdo con la información recolectada los estudiantes que habrían sido detenidos por agentes del Escuadrón Móvil Anti Disturbio (ESMAD) serían: **Andrés Molina, Jhon Novoa y Luis Murillo y Edward Gallego, que no sería estudiante de la universidad.**

De igual forma los estudiantes expresaron que habrían 2 estudiantes más, menores de edad que estarían detenidos y 5 personas más heridas, dos de ellas serían de gravedad.

Noticia en desarrollo ...
