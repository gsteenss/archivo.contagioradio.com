Title: Citan a debate de control político a altos funcionarios por caso Santrich
Date: 2020-11-11 21:09
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Caso Santrich, Debate de Control Político, Fiscal Francisco Barbosa., Nestor Humberto Martínez, Wilson Ruíz
Slug: citan-a-debate-de-control-politico-a-altos-funcionarios-por-caso-santrich
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Debate-de-control-politico-caso-Santrich.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este martes, **un grupo de congresistas citaron a un debate de control político al Alto Comisionado para la Paz, Miguel Ceballos y al ministro de Justicia, Wilson Ruiz para que respondan ciertos cuestionamientos alrededor del caso Santrich.** (Lea también: [DEA y Fiscalía habrían consumado un concierto para delinquir y afectar el proceso de paz: E Santiago](https://archivo.contagioradio.com/entrampamiento-mas-que-una-persecucion-a-santrich-un-ataque-al-acuerdo-de-pz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tras la publicación de algunos de los audios que integran el expediente del caso Santrich, en los cuales, se reveló, entre otras cosas, **que la cocaína por la que se procesó al excomandante guerrillero, habría salido de la misma Fiscalía a través de una entrega controlada; y que los supuestos narcotraficantes mexicanos con los que se había establecido contacto, eran en realidad agentes de la DEA;** varios sectores cuestionaron el rol de la Fiscalía en el caso, llegando a señalar incluso que el procesamiento de Santrich, **fue un montaje para intentar torpedear el Acuerdo de Paz firmado entre el Estado y la extinta guerrilla de las FARC-EP.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto, varios congresistas, entre ellos, Iván Cepeda, Gustavo Petro, Roy Barreras y Antonio Sanguino; citaron a un debate de control político al que también fueron invitados **el Fiscal General, Francisco Barbosa; el Procurador General, Fernando Carrillo y el Presidente de la Jurisdicción Especial para la Paz -[JEP](https://www.jep.gov.co/Paginas/Inicio.aspx)-, Eduardo Cifuentes.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RoyBarreras/status/1326539911877468164","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RoyBarreras/status/1326539911877468164

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AndresCamiloHR/status/1326539459358150660","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AndresCamiloHR/status/1326539459358150660

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Justamente, el magistrado Cifuentes declaró el día de ayer que **la Fiscalía** **—en ese momento dirigida por Néstor Humberto Martínez—** **nunca entregó formalmente las pruebas solicitadas por la JEP** en relación con este proceso; y que falló en su deber de colaborar con el Tribunal de Paz, al que agregó, había afectado con la omisión de ese deber.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe recordar que para ese momento el país se dividió entre quienes pensaban que Santrich debía ser expulsado de la JEP y extraditado a Estados Unidos por haber incurrido presuntamente en actos delictivos luego de la firma del Acuerdo y entre quienes creían que se debía respetar su debido proceso y esperar que la Fiscalía aportara las pruebas en las que soportaba la incriminación contra el exintegrante del secretariado de las FARC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para ese entonces la JEP, quedó en medio de ese debate, pues era la autoridad que tenía que decidir si avalaba, o no, el pedido de extradición de Estados Unidos; ante la presión mediática que implicaba la divulgación de material del presunto contacto de Santrich con narcos mexicanos —que terminaron siendo agentes de la DEA a la luz de las revelaciones—; y las declaraciones del entonces Fiscal, incriminando al excomandante y presionando para que fuera expulsado de la JEP. Sin embargo, según lo expuesto por el magistrado Cifuentes, la Fiscalía de Néstor Humberto Martínez, nunca aportó formalmente las pruebas que justificaban la apertura del caso.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JEP_Colombia/status/1326502774784331777","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JEP\_Colombia/status/1326502774784331777

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JEP_Colombia/status/1326521738662981632","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JEP\_Colombia/status/1326521738662981632

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Es justamente sobre esas actuaciones y omisiones del exfiscal Martínez Neira, que se centrará el debate citado por los congresistas, a la espera de que se puedan esclarecer los hechos, que hoy tienen a varios sectores del país, señalando que todo se trató de un montaje para sabotear el Proceso de Paz. **“*¿La Fiscalía General de la Nación está realizando montajes para acabar con el proceso de paz?*”, es uno de los cuestionamientos que se encuentra en la petición de los solicitantes del debate, la cual, tendrá que ser resuelta por los citados.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De probarse una actuación en tal sentido, el exfiscal Néstor Humberto Martínez tendría que responder tanto penal como disciplinariamente. De hecho, frente a esta presunta responsabilidad, **el senador Iván Cepeda anunció ayer la ampliación de una denuncia en contra del exfuncionario, en la cual se le vincula con una “*presunta responsabilidad en delitos de prevaricato, fraude a resolución judicial*** ***y alteración de elemento material probatorio*”.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1326155488040538123","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1326155488040538123

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
