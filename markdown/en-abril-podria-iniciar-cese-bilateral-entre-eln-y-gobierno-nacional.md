Title: En abril podría iniciar cese bilateral entre ELN y Gobierno Nacional
Date: 2018-03-23 13:22
Category: Nacional, Paz
Tags: ELN, Gobierno, Mesa social Quito
Slug: en-abril-podria-iniciar-cese-bilateral-entre-eln-y-gobierno-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Eln-gobierno-cese.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Mar 2018] 

Gustavo Bell, jefe negociador del equipo de gobierno en el proceso de paz con el ELN, confirmó la posibilidad de que se instale un cese bilateral, luego de que se realice una evaluación del cese unilateral de esa guerrilla y de los mecanismos que deben mejorarse para hacerlo más duradero. **Este hecho podría ratificarse la primera semana de abril.**

Marylen Serna, vocera de Congreso de los Pueblos, señaló que la iniciativa contribuye a la confianza que debe construirse en la mesa de Quito y en la sociedad “**celebramos por las comunidades y territorios que están en una constante zozobra e incertidumbre**, que se evalúa el primer cese bilateral y cese unilateral del ELN”.

Sin embargo, considera que los obstáculos que ha afrontado este proceso deben ser revisados para mejorar los diferentes mecanismos, que deben garantizar que el diálogo continúe. Para ello señaló que es importante que esos mecanismos sean mucho más concretos y **precisos a la hora de realizar el seguimiento a las acciones que se realizan al cese bilateral.**

Así mismo afirmó que la presión y mediación que realizaron tanto los garantes internacionales como, las organizaciones sociales y el acompañamiento de la iglesia, jugaron un rol fundamental en el acercamiento a ambas partes, razón por la cual **“debe dársele mucha más fuerza al papel de la sociedad”.**

### **La mesa necesita compromisos** 

Otra de las problemáticas que de acuerdo con Serna, afecta el desarrollo de la mesa en Quito, tiene que ver con la falta de garantías para **los líderes sociales y defensores de derechos humanos que siguen siendo asesinados en Colombia**, razón por la que aseguró que debe ser un tema primordial en los puntos a dialogar.

Frente a las acciones humanitarias, afirma que diferentes comunidades en el país ya han hecho propuestas, el departamento del Chocó propuso el acuerdo humanitario, mientras que municipios del Cauca han propuesto desminados humanitarios. (Le puede interesar: ["Quinto ciclo de conversación entre ELN y Gobierno irá hasta el 18 de mayo"](https://archivo.contagioradio.com/eln_gobierno_conversaciones_quito/))

### **La declaración de guerra del EPL al ELN** 

Sobre la declaración de guerra que, según el ELN, les hizo la guerrilla del EPL, Marylen Serna manifestó que es preocupante la situación de las comunidades y las organizaciones que están presentes en esos territorios.

“Generalmente en esas confrontaciones es la población la que sale afectada” aseveró Marylen y agregó que por lo mismo debe encontrarse una solución dialogada y evitar que entre un tercer actor armado o pensar en militarizar las comunidades.

<iframe id="audio_24767475" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24767475_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
