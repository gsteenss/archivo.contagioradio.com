Title: La critica situación de los privados de la libertad
Date: 2015-11-22 14:39
Category: DDHH, Nacional
Tags: 30 guerrilleros idultados, Derechos Humanos, FARC, proceso de paz, situacion en las carceles de colombia
Slug: la-critica-situacion-de-los-privados-de-la-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Presos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto tomada de internet] 

###### [22 de nov 2015] 

A pesar de **indulto a 30 guerrilleros de las FARC EP**, la situación es crítica para los privados de libertad por su pertenencia a las guerrillas

La decisión del gobierno de conceder el indulto a 30 integrantes de la guerrilla de las FARC EP, por el delito de rebelión, es un gesto humanitario y de confianza en el desarrollo del proceso de conversaciones.

Así lo valoran organismos humanitarios de de derechos humanos, sin embargo, anotan que  la decisión "**no debe olvidar problemas de fondo, que padecen más de 120 mil detenidos en las cárceles colombianas,** en donde hay violaciones de derechos humanos, por las condiciones infrahumanas pero también por las torturas y la corrupción al interior del INPEC -entidad del ejecutivo responsable del sistema penitenciario-

Por su parte las FARC EP desde La Habana manifestó que en las 350 cárceles del país se encuentran 9500 prisioneros políticos que “el Gobierno persistentemente niega la existencia de prisioneros políticos en Colombia”.

Ricardo Téllez de la delegación de paz de las FARC EP, manifestó que lanzaron un SOS " porque hoy completan 13 días de una huelga de hambre que el Gobierno quiere silenciar".

Las protestas de los privados de la libertad han sido silenciadas y reprimidas por personal del INPEC, invisbilizando una situación en que se ha desconocido el derecho humanitario.

Para organismos de derechos humanos, urge definir la cantidad de detenidos por su pertenencia a las guerrillas de las FARC EP y el ELN. De acuerdo con cifras oficiales los privados en las cárceles colombianos por su participación como un integrante de las guerrillas no supera el número de 2500 personas, entre mujeres y hombres.

Agregan que la situación de las mujeres es delicada, a sus familias se les ha perseguido e incluso se les ha impedido en muchas el amamantar a sus hijos.

De acuerdo con un reporte de la agencia APA, Ricardo Téllez manifestó que pasaron  una lista de 80 presos "que están en situaciones, digamos, de urgencia en cuanto a la atención médica, y de esos hay 11 que están supremamente grave y que, en cualquier momento, cualquiera de ellos puede fallecer. Hay una gran cantidad de compañeros también con enfermedades terminales”.

En medio del estancamiento para cerrar el tema de justicia, el anuncio es celebrado como un signo importante para desentrabar aspectos humanitarios que desde hace más de 24 meses han sido planteado en la mesa de La Habana. (Ver [Persiste la tortura en establecimientos carcelarios de Colombia](http://bit.ly/1SdaANN))
