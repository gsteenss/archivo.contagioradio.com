Title: Persiste movilización contra la reforma laboral en Francia
Date: 2016-05-24 15:38
Category: El mundo, Movilización
Tags: protestas en Francia, reforma laboral Francia, represión en Francia
Slug: persiste-movilizacion-contra-la-reforma-laboral-en-francia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Francia-contra-reforma-laboral.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UMPeru] 

###### [24 Mayo 2016 ]

Desde el pasado 9 de marzo, por lo menos 20 **sindicatos y organizaciones sociales se movilizan contra la reforma laboral en Francia**, un país que ha sido referente para diversas naciones en lo que a la protección de los trabajadores se refiere, pero que de acuerdo con el analista político Manuel Hernández, estaría enfrentando un retroceso de más de 100 años, con la serie de medidas que el actual gobierno pretender implementar.

De acuerdo con el analista esta reforma** incrementa la jornada de trabajo, pasando de 35 a 60 horas semanales, no contempla el pago de horas extras,** plantea que los empresarios pueden despedir directamente a los trabajadores sin mediación sindical, reduce los permisos y tiempos de vacaciones, y desconoce las garantías conquistadas por los sindicatos; representa, en suma, un paquete de medidas de un gobierno de aparente izquierda pero con grandes contradicciones.

"Francia ha tenido que caer en las fauces de lo que se conoce como la troika europea, que está fundamentalmente conformada por la **Comisión Europea, el Banco Central Europeo y el Fondo Monetario Internacional**, que **son los que están poniendo en aprietos a todos estos países** y por supuesto desde ahí podemos conectarlo con todos los desfiles de migración, con todos los desfiles de desterrados por la guerra misma, que se encuentran con unos países que para nosotros son símbolos de libertad, pero que están sembrando sus fronteras con murallas de alambres de púas, de cercas eléctricas y de infamia", asegura el analista.

La historia de lucha de los sectores populares franceses es uno de los elementos más importantes para que **la exigencia de reivindicaciones persista, pese a la fuerte represión de la fuerza pública**. Esta movilización ha incluido el uso de herramientas como la Internet, la recolección de firmas y las marchas para que la ciudadanía en general comprenda que esta reforma es altamente lesiva con sus condiciones de vida.

<iframe src="http://co.ivoox.com/es/player_ej_11651447_2_1.html?data=kpajl5aYeJihhpywj5aZaZS1k5uah5yncZOhhpywj5WRaZi3jpWah5ynca7Vz9rSzpCsqdPihqigh6aVssXZ25CajabSpc2ZpJiSo6nXuMKf0dTZh6iXaaK41c7Q0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
