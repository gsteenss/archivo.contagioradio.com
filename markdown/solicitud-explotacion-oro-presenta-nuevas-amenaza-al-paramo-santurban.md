Title: Páramo de Santurbán nuevamente amenazado por solicitud de Minesa
Date: 2019-01-22 14:07
Author: AdminContagio
Category: Ambiente, Entrevistas
Tags: Autoridad Nacional de Lice, Comité San, Minesa, Páramo de Santurbán
Slug: solicitud-explotacion-oro-presenta-nuevas-amenaza-al-paramo-santurban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Paramo-de-Santurban-e1472756604235.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Radio Macondo 

###### 22 ene 2019 

A pesar de protestas de las comunidades locales, la empresa árabe Sociedad Minera de Santander (Minesa) presentó el 17 de enero una nueva solicitud de licenciamiento ante la Autoridad Nacional de Licencias Ambientales (Anla) para el proyecto de extracción de oro, Soto Norte, en las montañas adyacentes del Páramo de Santurbán, en Santander.

Según un comunicado de prensa, este nuevo proyecto incluye medidas para "mejorar la protección del territorio, su entorno y las comunidades" a través de una reducción de la huella física del proyecto a más de 14 hectáreas en California y Suratá, Santander. Además, plantea la construcción de un acueducto en California, un seguimiento a la calidad de agua y  la descontaminación de más de 360 toneladas de arena contaminadas de mercurio provenientes de minería informal de la zona.

Sin embargo, Mario Flores, integrante del Comité para la Defensa del Agua y el Páramo de Santurbán, afirma que las amenazas que este megaproyecto presenta siguen vigentes. Para Flores, las actividades de Soto Norte, que pretende ser la mina subterránea de oro más grande de Colombia, podrían contaminar las aguas que provienen de las fuentes hídricas del páramo. Este ecosistema es de gran importancia ecológica debido a que le surte agua a 2.5 millón de colombianos en 48 municipios, incluso Bucaramanga. [(Le puede interesar: "En Santander recalcaron que 'Santurbán no está en venta'")](https://archivo.contagioradio.com/en-santander-recalcaron-que-santurban-no-esta-en-venta/)

Para el Comité, un proyecto de esta escala no puede intervenir las áreas adyacentes al páramo sin afectarlo. Además, el Comité afirmó que Minesa pretende construir en zonas que quedan arriba de las bocatomas para el acueducto de Bucaramanga, lo cual es un riesgo para la salud pública y el medio ambiente. Estos hechos se suman a los planes de Minesa de reubicar familias que habitan 161 hectáreas en California y Suratá donde el proyecto plantea intervenir la superficie. Estos hechos serían un desplazamiento forzado, según lo calificó Flores.

Frente a estos, el líder ambientalista manifestó que el Comité está preparando acciones legales y organizativas, incluyendo una protesta en Bucaramanga que se estaría programando para los próximos días. [(Le puede interesar: "Corte Consitutcional da la razón a defensores del Páramo de Santurbán")](https://archivo.contagioradio.com/corte-constitucional-da-la-razon-a-defensores-del-paramo-de-santurban/)

<iframe id="audio_31688032" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31688032_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
