Title: Por amenazas e incumplimientos comunidad indígena nuevamente sale desplazada
Date: 2015-12-08 15:27
Category: DDHH, Nacional
Tags: buenaventura, comunidades indígenas, Desplazamiento forzado, Gobierno Nacional, Juan Manuel Santos, paramilitarismo en Colombia, Unidad de Víctimas, Unión Agua Clara
Slug: por-amenazas-de-paramilitares-comunidad-indigena-nuevamente-sale-desplazada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/IMG-20151207-WA0042.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comunicadores CONPAZ 

###### [8 Dic 2015] {#dic-2015 dir="ltr"}

El pasado 29 de noviembre 347 habitantes de la Comunidad Unión Agua Clara, río San Juan, retornaron a su lugar de origen bajo la figura de Territorio Humanitario y Biodiverso, pero 7 días después 24 personas **se vieron obligados nuevamente a desplazarse ante la presión  y amenazas de los paramilitares, sumado a los incumplimientos del Estado.**

De acuerdo a un informe de la Comisión de Justicia y paz, los paramilitares llegaron al territorio de la Comunidad Unión Agua Clara, a bordo de una embarcación (pisingo) de color azul y motor 15 cc y portando armas cortas en la cintura, como lo informa la organización que acompaña a los indígenas.

*Uno de los paramilitares descendió de la embarcación en la orilla del rio, preguntó a los pobladores de manera agresiva e insistente por el gobernador, al no obtener respuesta se dirigió hasta su vivienda a buscarle, en el lugar se encontraba su padre, Pilar Chamarra, a quien le insistió ver al gobernador.*

*El paramilitar expresó: "por qué se desplazaron indios brutos, ahora si voy a mandar a mi gente"; "me voy pero voy a mandar mi gente" . El armado agregó: "voy a matar a uno pa que no anden jodiendo con esta vaina, ya verán". Segundos después se acercó a la valla instalada por la comunidad en la cual aparece "Territorio Humanitario y Biodiverso Comunidad Unión Agua Clara, área exclusiva de población civil" e insistió "voy a mandar mi gente".*

[![IMG-20151208-WA0018](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/IMG-20151208-WA0018.jpg){.aligncenter .size-full .wp-image-18226 width="1600" height="1200"}](https://archivo.contagioradio.com/por-amenazas-de-paramilitares-comunidad-indigena-nuevamente-sale-desplazada/img-20151208-wa0018/)

Tras este hecho, en Asamblea General la comunidad determinó que desde este miércoles 8 de diciembre iniciaron  un nuevo desplazamiento forzado, **"No queremos salir desplazados del territorio, pero si no hay condiciones básicas de retorno, nos vemos en la necesidad de desplazarnos toda la comunidad"**, dice un comunicado emitido.

"Retornamos debido al cansancio de permanecer durante un año en el Coliseo deportivo, El cristal de Buenaventura  viviendo las penurias de la desatención por parte del gobierno distrital. Retornamos porque en Buenaventura los paramilitares amenazaron a nuestros líderes y creímos que el concepto de seguridad de la fuerza pública era real, es decir, que es eficaz el desmonte y combate a los paramilitares que nos desplazaron en el 2014", agrega el comunicado.

Sin embargo, **no hay tal desmonte del paramilitarismo, y además la Alcaldia de Buenaventura  y la Unidad de Victimas, no cumplió** con las condiciones básicas en agua potable, alimentación, canoas, mejoramiento de vivienda y entrega de semillas y herramientas, que debía haber garantizado a la comunidad para su retorno.

"Hoy luego de 7 días, carecemos de agua potable; no contamos con tanque para la recolección de agua lluvia, ni filtros para la misma; no se ha realizado la visita técnica del acueducto existente y su reparación (...) **Hoy más de 30  de nuestros niños y niñas menores de 7 años presentan fiebres, diarreas y vómito**; se están presentando infecciones en la piel (granos) en niños, niñas y adultos", describen.

Es por ello, que la comunidad exige una respuesta inmediata a su derecho de retorno en dignidad y protección, a los mínimos acordados el 25 de noviembre. **De no ser así y no recibir respuesta inmediata, en 48 horas, el resto de la comunidad, también se desplazará.**
