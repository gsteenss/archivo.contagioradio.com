Title: Amenazan a líder de pescadores que promueve defensa del ambiente en Sucre
Date: 2019-07-17 15:23
Author: CtgAdm
Category: Comunidad, Nacional
Tags: amenazas contra líderes sociales, Asociación Afrocolombiana de Pescadores, Sucre
Slug: amenazan-a-lider-de-pescadores-que-promueve-defensa-del-ambiente-en-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Pescadores-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Apescordel] 

El pasado 15 de junio, en el Nodo del Golfo de Morrosquillo, Sucre, la Asociación Afrocolombiana de Pescadores con Cordel (APESCORDEL), denunció amenazas contra el líder pesquero **Eduardo Estrada Muñoz** para que abandone el territorio, donde viene promoviendo iniciativas en contra de la tala y los derrames de hidrocarburos en los manglares.

Eduardo Estrada lidera actualmente la **Corporación De Pescadores Artesanales Del Golfo De Morrosquillo (Corpagolfo),** una organización con más de 20 años, que** defiende los derechos de 1.400 familias de pescadores de las zonas de Tolú, Coveñas y San Antero** y que además promueve campañas contra la tala indiscriminada en manglares de la región.

El presidente de APESCORDEL, Julian Medina Salgado afirma que en julio y agosto de 2014 se presentaron derrames de hidrocarburo en las aguas del Golfo, hecho que se repitió en octubre de 2017, situaciones que los han llevado a demandar a petroleras  como Ecopetrol o Anadarko.

Medina Salgado indica que en muchos de los trámites de denuncia, Eduardo como cabeza de la organización Corpagolfo es quien aparece como responsable de estas denuncias, razón por la que sería el foco de las amenazas. [(Lea también: Pescadores de Buenaventura que están en Paro Cívico son amenazados por paramilitares)](https://archivo.contagioradio.com/pescadores-de-buenaventura-son-amenazados-por-paramilitares/)

"Lo que podemos hacer es arroparlo, estamos en una lucha defendiendo este proceso, pero nosotros no tenemos autoridad para salvaguardar su vida, deben ser las autoridades" expresó Medina, agregando que  la comunidad de pescadores ya se reunió para demostrarle su apoyo al líder, quien ya se dirigió a la Defensoría del Pueblo para poner en conocimiento la situación.

### Trabajo ambiental de los pescadores en Sucre

Pese a que tanto las petroleras como los pescadores instalaron barreras que protegieran el manglar, el vertiminiento de crudo del 2017 alcanzó a contaminar las aguas de las poblaciones de San Onofre y Boquerón, hecho que sumada a la tala indiscriminada ha afectado el estado de los árboles que componen el manglar, una situación que ha impulsado a los pescadores a proteger y recuperar las cuencas y ciénagas.

 "Hemos puesto el grito en el cielo y estamos haciendo campañas para la recuperación de manglares, **no es fácil, hemos logrado sembrar cerca de 15.000 árboles pero los siguen talando, se está taponando y cedimentando las cienagas** y por ende la actividad pesquera está sufriendo las consecuencias de ello, afectando nuestra actividad y nuestro sustento y tenemos que defenderlo", afirma el líder pesquero.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38606714" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38606714_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
