Title: Gestión comunitaria del agua y extractivismo
Date: 2015-07-25 07:00
Category: CENSAT, Opinion
Tags: ACUALCOS, acueductos comunitarios, Agua, Castilla La Nueva, Censat agua Viva, Ecopetrol, Guamal, Tauramena
Slug: gestion-comunitaria-del-agua-y-extractivismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/foto2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semillas] 

#### **[[Censat Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva)** 

###### [25 Jul 2015] 

El riesgo de los territorios que sustentan el ciclo del agua.

En los cerros orientales de Bogotá, la Asociación de Servicios Públicos Comunitarios San Isidro I y II, San Luis y la Sureña (ACUALCOS) ha prestado el servicio de agua potable a 2.500 usuarios durante  30 años. Así como éste existen 12.000 acueductos comunitarios en el país que ante el sistemático olvido estatal, de forma auto-gestionada han construido los sistemas de abastecimiento de agua. En la actualidad esta gestión comunitaria se encuentra amenazada ante transformaciones territoriales, que agenciadas desde el gobierno y bajo la bandera de un modelo de desarrollo basado en el extractivismo, pone en riesgo los territorios que sustentan los principales ciclos del agua.

La amenaza histórica para los acueductos comunitarios han sido las políticas nacionales de agua, que desde los años noventa han privilegiado modelos de privatización de la gestión con la promoción de Asociaciones Público Privadas (APP). Siendo el sector privado, en su mayor parte de capital transnacional, quien recibe las mayores y mejores rentabilidades, pero que a su vez empeora las condiciones del servicio de agua a las comunidades. A pesar del gran fracaso que han representado las APP, la política de agua en el gobierno de Juan Manuel Santos mantiene una visión empresarial con el objetivo de vincular operadores especializados a la ruralidad. Los lineamientos de la política para el abastecimiento y saneamiento de agua en la ruralidad, el CONPES 3810, desconoce el sujeto campesino y los acueductos comunitarios al no mencionarlos ni tenerlos en cuenta en la planeación, y se fundamenta a partir de un enfoque de demanda en donde la decisión sobre los proyectos de inversión en los municipios es determinado por la capacidad y disposición de pago de las comunidades.

A todo esto se suma una nueva amenaza: el extractivismo, que suma un nuevo ingrediente a la creciente competencia por el agua. Esta se realiza en un escenario desigual, pues lamentablemente las políticas del actual gobierno, como se refleja en el Plan Nacional de Desarrollo, son construidas para el beneficio del poder y control corporativo.

Hay múltiples ejemplos que reflejan la disputa por el agua. En el 2013, las comunidades del Guamal – Meta denunciaron la construcción de la plataforma petrolera Lorito 1 por Ecopetrol y la empresa canadiense Talisman, que afectaría la zona de recarga hídrica y está aguas arriba de la bocatoma del acueducto comunitario de Humadea y a sólo 50 metros del acueducto que surte al municipio de Castilla La Nueva. En el mismo año, en Tauramena - Casanare se realizó una consulta popular en donde se rechazó el proyecto de exploración sísmica del bloque petrolero Odisea 3D, ya que ponía en riesgo zonas de recarga hídrica y una importante infraestructura construida para los acueductos comunitarios de la región.

En Tasco, Boyacá, el consejal Luis Carlos Ochoa, denunció que a raíz de la explotación minera en la región “*desaparecieron numerosas fuentes de agua, muchos terrenos se volvieron inestables, y nos desviaron dos quebradas …*”. Igual respuesta es expresada por los pobladores del mismo municipio al explicar por qué no quieren permitir la reapertura de la mina de hierro ‘El Banco’, por parte de la empresa Votorantim. Las comunidades de esta región fértil de papa, haba, hibias y rudas observan en la explotación minera la principal amenaza para las aguas abastecedoras de los acueductos y para sus actividades productivas.

La defensa del agua es vital para el futuro de Colombia y fundamental en la construcción de un país que anhela la paz. La gestión comunitaria debe ser revalorizada y defendida frente a los embates de la privatización y el despojo. Los acueductos comunitarios tienen el reto de propiciar con otros actores, la construcción de un gran movimiento social para la defensa de las aguas.
