Title: Plenaria de Cámara aprueba por unanimidad proyecto de protección animal
Date: 2015-05-27 08:39
Category: Animales, Nacional
Tags: Animalistas, defensa animal en Colombia, Juan Carlos Losada, Maltrato animal, Plenaria de Cámara de Representantes, protección animal, Proyecto de Ley 087
Slug: plenaria-de-camara-aprueba-proyecto-de-ley-de-proteccion-animal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/marcha_reforma_estatuto_proteccion_animales_baja.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Juan Carlos Losada 

<iframe src="http://www.ivoox.com/player_ek_4556710_2_1.html?data=lZqimJyVdI6ZmKiakpaJd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlM3Zz8bfy8aPqMafpIqwlYqldc7V08aYw9XWucbWwpDd1NTdqcTo0JDRx5CwqdqfxcqY0tfTuMbXxM6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan Carlos Losada, representante a la cámara] 

Este martes se aprobó **por unanimidad en plenaria de Cámara de Representantes el proyecto de Ley 087** del representante a la Cámara por el partido Liberal, Juan Carlos Losada, por medio del cual se penaliza el maltrato animal y se declara a los animales como seres sintientes.

Esta iniciativa  fortalecería y modificaría la Ley 84 de 1989, para que existan multas entre 5 y 50 salarios mínimos legales vigentes, para quienes maltraten a un animal.

De acuerdo al representante, el hecho de que se apruebe de manera unánime esta iniciativa, le da una fuerza muy importante al proyecto de Ley para el siguiente debate que se llevaría a cabo en el **segundo semestre del año 2015 en el senado de la República**, donde se espera que tenga la misma acogida por parte de los senadores.

**“Este proyecto de ley va a ser historia**, y se vuelve un hito en la lucha por los derechos de los animales en Colombia, no va a ser el último, pero sí es un paso fundamental hacia la verdadera protección para aquellos que no tienen voz”, expresó con euforia Losada, minutos después de que su propuesta fue aprobada.

Durante el debate se contó con la presencia de integrantes de organizaciones animalistas, quienes estuvieron al tanto de lo que sucedía, dando su voz de apoyo al representante, quien durante su intervención para la presentación del proyecto resaltó datos como que el **69% de los y las colombianas tienen animales de compañía** en sus casas, y que, además, Colombia tiene un compromiso muy grande con la defensa animal, teniendo en cuenta que es uno de los países **con mayor biodiversidad del planeta.**

Cabe recordar, que en el primer debate de la Comisión Primera de Cámara de Representantes, el proyecto de Ley también fue aprobado de forma unánime.
