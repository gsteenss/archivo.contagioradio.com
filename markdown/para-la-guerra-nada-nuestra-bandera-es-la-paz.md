Title: Para la guerra nada, nuestra bandera es la paz
Date: 2019-09-22 12:06
Author: JUSTAPAZ
Category: Opinion
Tags: bandera de la paz, colombia, OCHA, paz
Slug: para-la-guerra-nada-nuestra-bandera-es-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/paz-haremos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### *[Por: Mauricio Ramírez - JUSTAPAZ]* 

[Este año ha sido difícil para el trabajo en la construcción y defensa de la paz. Según el Monitor Humanitario de OCHA, en lo corrido del año 2019 se han presentado 467 casos de amenaza individual y/o colectiva, en la que se ha afectado a más de 1.200 individuos. Asimismo, los casos reportados de homicidio intencional en persona protegida (entre los que se suman algunos casos de líderes sociales) llegan a los 624 casos, en los que se suma un total de 705 personas afectadas.]

[Esta coyuntura, sumada a las voces que se suman a la guerra y los que pelean con dientes y uñas por mantener una cultura de la violencia, nos invita a realizar una gran reflexión; podemos entender que la paz, el perdón y el diálogo como principios para la transformación de conflictos son muy frágiles. Pensemos en estos principios como una lámina de vidrio templado: resisten vientos fuertes, nos permiten ver con claridad la realidad e incluso nos pueden cubrir del frio cruel de la indiferencia humana.]

[Sin embargo, incluso con estos beneficios los vidrios son frágiles y, sin una estructura fuerte que les soporte, se pueden romper y destruir fácilmente. Lo mismo pasa con las iniciativas de construcción de paz y transformación de conflictos; más allá de la belleza del perdón, del diálogo y todos los beneficios que traen para la sociedad, son frágiles, muy fáciles de fracturar.]

[Pero recordemos la estructura que soporta la lámina de vidrio, ya sea un marco para una ventana o un soporte para un florero o una estructura para un vitral, todos cuentan con una estructura firme. Acero rígido y resistente. Ahora pensemos en las iniciativas de paz, perdón, diálogo y conciliación en Colombia; este trabajo es una lucha constante, con acciones colectivas, en red y que se evidencian en pequeñas y grandes acciones. Igualmente, sin esta estructura están expuestas a miles de amenazas para los líderes y lideresas visibles.]

[Durante este año, y frente a estas situaciones violentas y los discursos de quienes apostaron nuevamente por la guerra, el sector de la sociedad civil ha venido alzando su voz para recordarle al país y a los grupos armados que seguimos firmes por la construcción de una sociedad en paz. Desde los colectivos que Justapaz acompaña en Nariño, Antioquia, Putumayo, Chocó, Tolima y otras regiones y desde el equipo nacional de nuestra organización, nos hemos unido a esta voz, recordándoles a los pobladores de zonas afectadas por el conflicto que la transformación de conflictos y la reconciliación se construyen desde actos pequeños, pero que toman tiempo en hacer incidencia. ]

[La paz es más amplia que las diversas violencias que se viven en nuestro país. Ante la voz de los violentos y el sonido de las armas, queremos decir que somos más los que día a día construimos paz y no vamos a dejar de hacerlo, porque la paz es un objetivo que dignifica la humanidad, que nos hermana y que construye un país en que cabemos todas y todos. ]

[Desde un acto simple como una sonrisa, desde una mirada amable o desde la movilización noviolenta, individual o masiva, en contra de las violencias de todo tipo, podemos sumarnos a esta estructura de acero firme que sostiene la paz. ]

[Cada ciudadano y ciudadana tiene el poder de posicionar los temas de interés nacional cuando se trabaja en red. Acá no hago referencia únicamente a la (in)movilización digital. La paz es un proceso social, que se logra mediante actos concretos de incidencia en la sociedad y que impactan positivamente en la realidad de las comunidades.]

[Desde nuestra cotidianidad, levantemos la voz, dignifiquemos la vida humana y tengamos como lema que “]**Para la guerra nada, nuestra bandera es siempre la paz”.**
