Title: Crisis fronteriza con Venezuela no puede debilitar el proceso de paz
Date: 2015-08-31 13:18
Category: Nacional, Política
Tags: colombia, crisis en la frontera, Fabrizio Hochschild, habana, ONU, piedad cordoba, Polo Democrático Alternativo, proceso de paz, unasur, Venezuela
Slug: crisis-fronteriza-con-venezuela-no-puede-debilitar-el-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Crisis-en-la-Frontera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.lapatilla.com]

###### [31 Ago 2015]

En el marco de la actual crisis de la frontera colombo-venezolana, **la ONU y el Polo Democrático Alternativo** se pronunciaron sobre las afectaciones que podría tener el Proceso de paz en la Habana de no continuar manejando la crisis fronteriza diplomáticamente.

"**Un problema con Venezuela que pudiera afectar el compromiso que siempre ha tenido con el proceso (de paz)** sería lamentable en un momento donde realmente parece que el proceso avanza", aseguró Fabrizio Hochschild, coordinador residente de la Organización de Naciones Unidas.

Hochschild, añadió que el alejamiento de Venezuela de los diálogos de paz, "sería muy grave” ya que Venezuela ha sido un actor muy importante para conseguir la paz en Colombia.

Por otro lado, Clara López Obregón, presidenta del Polo Democrático Alternativo, aseguró que la deportación indiscriminada ha vulnerado los derechos humanos de miles de colombianos y colombianas que vivían en Venezuela. Así mismo, añadió que la crisis fronteriza no se puede usar para debilitar los avances en el proceso de paz y la integración latinoameriacana de UNASUR, teniendo en cuenta que gracias a esta “**20 mil colombianos gozan de visa de trabajo y otros 20 mil de estudio en los países que hacen parte de esta alianza”, explicó López.**

Además la presidenta del Polo, invitó a las colectividades y sus líderes a desligar la situación en la frontera con la campaña electoral, teniendo en cuenta que durante la mañana del lunes **Clara López y la lider social, Piedad Córdoba denunciaron que sus cuentas de twitter fueron jackeadas,** pues se publicaron mensajes donde el Polo y Piedad decían estar de acuerdo con tratos inhumanos a los colombianos en la frontera.

Ante la denuncia, Piedad Córdoba aseguró que se trata de un montaje para incitar el odio, e indicó que **exigirá una investigación a fondo.**
