Title: Corte Constitucional se pondrá al ritmo del Fast Track
Date: 2017-01-23 22:05
Category: Nacional, Paz
Tags: acuerdo de paz, Corte Consitucional, Implementación
Slug: corte-constitucional-se-pondra-al-ritmo-del-fast-track
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/corte-e1495109388589.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fecospec] 

###### [23 Ene 2017] 

El Fast Track no solamente implica sesiones extraordinarias para el Congreso, la Corte Constitucional, de igual forma, tendrá que agilizar sus tiempos para cumplir con los debates y la aprobación de cada una de las leyes que permitirán la implementación del acuerdo del Teatro Colón. **Por este motivo los trámites que usualmente se realizaban en 120 días ahora se harán en 40 días.**

**Los primeros 20 días serán usados para repartir el proceso**, hacer su estudio y escuchar a las diferentes instituciones como la Procuraduría. Los siguientes **20 días serán de debate para tomar decisiones frente al proyecto de ley.**  Además el Fast Track también prevé que los controles que realice la Corte deberán ser automático, únicos y ser harán una vez las leyes sean aprobadas en el Congreso.

Sin embargo, no es necesario que tanto el Congreso como la Corte Constitucional trabajen en los mismos tiempos, esto debido a que si son leyes ordinarias, como la **Ley de Amnistía, que entró en vigencia inmediatamente se aprobó, no hay que esperar a que la Corte se pronuncie**, el control se realiza después. Por el contrario si es un acto legislativo si debe ser revisado por la Corte y se le suman los 40 días de discusión y aprobación,

Para la abogada Camila Moreno, del Centro Internacional para la Justicia Transicional, lo más importante del acto legislativo del Fast Track, “es la articulación que se da entre las diferentes instancias del Estado en disposición en la medida de la disposición del Estado para garantizar una plena implementación” sin que eso signifique la aprobación de un trámite sin que se den discusiones y agrego que lo que va a prevalecer es “e**l interés superior de la paz”**

Sobre la demanda que instauro el ex presidente de Colombia, Andrés Pastrana, la abogada Moreno señaló que lo que se debe recordar “**es que la Corte Constitucional ya hizo un estudio serio y afondo de la constitucional del acto legislativo** por el cual se aprueba el Fast Track”, razón por la cual considera que es difícil que la Corte vuelva a pronunciarse. Le puede interesar: ["El país está empezando a expresar su ansiedad por la paz: Imelda Daza"](https://archivo.contagioradio.com/pais-esta-empezado-expresar-ansiedad-la-paz-imelda-daza/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
