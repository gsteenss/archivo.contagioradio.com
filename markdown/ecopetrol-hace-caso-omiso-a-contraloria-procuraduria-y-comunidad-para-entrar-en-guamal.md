Title: Ecopetrol hace caso omiso a Procuraduría y comunidad en Guamal
Date: 2017-09-11 13:53
Category: DDHH, Nacional
Tags: Ecopetrol, ESMAD, Guamal, Meta, Trogón I
Slug: ecopetrol-hace-caso-omiso-a-contraloria-procuraduria-y-comunidad-para-entrar-en-guamal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/guamal-e1505155997434.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunidades vereda Pio XII] 

###### [11 Sept 2017] 

Las comunidades de Guamal en el Meta, **denunciaron atropellos por parte del ESMAD durante el fin de semana en la vereda Pio XII.** Según ellos, la maquinaria de la empresa Ecopetrol estaban ocupando la vía por donde transitan las personas para salir a realizar sus actividades diarias.

Según Sully Téllez, integrante de la comunidad, **las personas le estaban exigiendo al ESMAD vía libre para poder transitar por su territorio** a los que los uniformados “nos dijeron que nos retiráramos del lugar, se llevaron nuestros carros en grúas y agredieron a las mujeres”.

Adicional a esto, indicó que la maquinaria de la empresa, quien lleva a cabo el proyecto Trogón I en la vereda Pio XII, **“dañó árboles e incumplieron el decreto del alcalde del municipio** que establece ciertas horas para el ingreso de la maquinaria”. (Le puede interesar: ["Ecopetrol debería suspender proyecto Trogon I en Guamal, Meta"](https://archivo.contagioradio.com/46238/))

Cuando ellos y ellas exigieron el cumplimiento del decreto, “el mayor Castillo hizo caso omiso y siguió maltratando a la población que esperaba a algún funcionario de Ecopetrol, que nunca apareció”.

Téllez manifestó que **las alcantarillas de la vereda se han dañado producto del tránsito de la maquinaria pesada** de la empresa. Dijo que hoy se encuentran arreglando esas alcantarillas “cambiando tubos porque Ecopetrol solo echa arena para taparlas”.

### **Empresa ha pasado por encima de los pronunciamientos de la Procuraduría y la Contraloría** 

En días anteriores la **Procuraduría General de la Nación le había dado la razón a las comunidades** para suspender temporalmente el proyecto Trogón I aludiendo a los incumplimientos del manejo ambiental y la falta de socialización del mismo con las comunidades. (Le puede interesar:[" Cormacarena y Gobernacion del Meta señalan riesgos por actividades de Ecopetrol en Guamal"](https://archivo.contagioradio.com/cormacarena-gobernacion-del-meta-senalan-riesgos-actividades-ecopetrol-guamal/))

En este caso, Téllez manifestó que el jefe de las operaciones del proyecto de Ecopetrol argumentó que **“la Procuraduría no tiene voz ni voto”**. Esto lo hizo cuando las comunidades intentaron hacer alusión a la decisión de la institución cuando se presentaron los enfrentamientos.

<iframe id="audio_20808073" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20808073_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
