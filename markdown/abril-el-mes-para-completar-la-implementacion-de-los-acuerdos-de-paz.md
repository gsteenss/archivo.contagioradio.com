Title: Abril es el plazo para las tareas pendientes en implementacion del acuerdo de paz
Date: 2017-03-27 13:30
Category: Entrevistas, Paz
Tags: acuerdos de paz, FARC, Implementación, paz
Slug: abril-el-mes-para-completar-la-implementacion-de-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/implementacion-acuerdos-de-paz1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [27 Mar. 2017] 

Luego de dos días de intensas jornadas de trabajo entre delegados de las FARC y del Gobierno nacional que hacen parte de la Comisión de Seguimiento, Impulso y Verificación a la Implementación -CSIVI- se conocieron **las conclusiones y los compromisos a los que llegaron las partes para “poner el acelerador” a la implementación de los Acuerdos de Paz.**

A través de un comunicado que consta de 6 puntos, las partes aseguraron que luego de 100 días de la implementación **“se identificaron las dificultades y se tomaron decisiones”**. Le puede interesar: [Se creará Instancia con Pueblos Étnicos para implementación de Acuerdos de Paz](https://archivo.contagioradio.com/se-creara-instancia-con-pueblos-etnicos-para-implementacion-de-acuerdos-de-paz/)

Desarrollo legislativo, garantías de seguridad, tránsito a la legalidad, zonas veredales transitorias de normalización, reincorporación y el cronograma de la dejación de armas, fueron los 6 temas en los que lograron llegar a acuerdos.

En materia de **desarrollo legislativo las partes se comprometieron a priorizar el trámite de las normas necesarias para la implementación** del Acuerdo Final, dando como plazo el mes de abril para que el Gobierno nacional presente ante el Congreso el paquete de proyectos necesarios. Le puede interesar: [Se creará Grupo de Expertos en Tierras para implementación de Acuerdos de Paz](https://archivo.contagioradio.com/se-creara-grupo-de-expertos-en-tierras-para-implementacion-de-acuerdos-de-paz/)

Para **Francisco Toloza integrante del movimiento Voces de Paz**, esta reunión puede considerarse muy positiva por los avances que significó “en el mes de abril aspiramos dejar radicadas, sino aprobadas todas las iniciativas legislativas que se hablan en el punto 6.1.9 del Acuerdo final”.

En la actualidad ya hay radicadas varias iniciativas como el artículo 108 que permitirá que **los integrantes de las FARC puedan recuperar de manera progresiva sus derechos.** Le puede interesar: [Los retos de la implementación con enfoque de género](https://archivo.contagioradio.com/los-retos-d-ela-implementacion-con-enfoque-de-genero/)

En materia de tránsito a la legalidad, las FARC se comprometió **a entregar el listado de todas las personas que conforman esta guerrilla, de manera que el Gobierno deba dar todo el apoyo para llevar a cabo las amnistías**, muchas de ellas retrasadas hasta la fecha.

Las Zonas Veredales también fueron parte de esta reunión, tema en el cual se expusieron las fallas que ha habido hasta el momento, por lo cual el **Gobierno nacional se comprometió a “entregar en su totalidad la infraestructura de las zonas veredales,** incluida la instalación de las áreas destinadas para la recepción. De igual forma quedarán dotadas con ambulancias y personal médico” dice el comunicado.

“Cualquiera que pase por una Zona Veredal sabe que hay un incumplimiento por parte del Gobierno (…) no nos queda otra cosa que exigir que se cumpla lo acordado, mientras tanto la guerrillerada está poniendo toda su capacidad de trabajo para que todo esté listo prontamente” dijo Toloza. Le puede interesar: [Implementación de los Acuerdos requiere movilización social: Iván Cepeda](https://archivo.contagioradio.com/implementacion-de-los-acuerdos-requiere-movilizacion-social-ivan-cepeda/)

Los últimos dos puntos trabajados hacen referencia **a la reincorporación, para la que se definió la oferta institucional y proyectos productivos para los integrantes de las FARC,** así como el cronograma de la dejación de armas, punto en el que se manifestó que se avanzará en el cumplimiento de las fechas acordadas, pero para el que se pretende acelerar tanto la entrega de armas como la capacitación a las FARC.

Para Toloza, además de los compromisos a los que se llegaron y el Pacto Político en el que se trabajó “**la novedad del cónclave es el regreso a este equipo del General Óscar Naranjo, quien fue uno de los negociadores precisamente del punto de garantías en seguridad (…)** ahora el General Óscar Naranjo asume la coordinación de la implementación y participa en la implementación del punto 3.4” concluyó Toloza.

[VERSIÓN FINAL- Comunicado Conjunto No 16-1](https://www.scribd.com/document/343197574/VERSION-FINAL-Comunicado-Conjunto-No-16-1#from_embed "View VERSIÓN FINAL- Comunicado Conjunto No 16-1 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="audio_17790239" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17790239_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="doc_3152" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/343197574/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-1uc4tMLfwvDfdmnjDvDt&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
