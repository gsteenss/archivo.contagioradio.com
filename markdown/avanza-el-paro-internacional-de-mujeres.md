Title: Avanza el Paro Internacional de Mujeres
Date: 2017-02-27 14:57
Category: El mundo, Mujer, Nacional
Tags: 8 de marzo, Ni una menos, No más Feminicidios, Paro Internacional de Mujeres, vivas nos queremos
Slug: avanza-el-paro-internacional-de-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Paro_Mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Todosalta] 

###### [27 Feb 2017] 

Mujeres de **41 países se han unido al Paro Internacional de Mujeres que será el próximo 8 de Marzo, saldrán a las calles para exigir respuestas y protestar** por el aumento alarmante en las cifras de feminicidios, políticas públicas ineficaces para hacer frente a las violencias de género, retrocesos en proyectos de Ley que protegen los derechos sexuales y reproductivos, pocas oportunidades educativas y laborales, brechas salariales entre hombres y mujeres y la “derechización global”.

Hasta el momento, mujeres de Argentina, Australia, Bélgica, Bolivia, Brasil, Canadá, Chile, Colombia, Republica Dominicana, Corea del Sur, Costa Rica, República Checa, Ecuador, El Salvador, Finlandia, Francia, Guatemala, Alemania, Honduras, Irlanda del Norte, República de Irlanda, Israel, Italia, México, Nicaragua, Pakistán, Panamá, Paraguay, Perú, Polonia, Portugal, Puerto Rico, Rusia, Escocia, España, Suiza, Tailandia, Turquía, Uruguay, Reino Unido y USA, han confirmado su participación en la iniciativa global.

### Paro, tomas culturales y movilizaciones 

Si bien las denuncias, apuestas y objetivos son compartidos por las organizaciones de mujeres y feministas, los contextos de cada una de ellas en sus países es diferente, por tal motivo, algunas plataformas organizativas aseguraron que por motivos de seguridad, de aspectos económicos o culturales en sus países, **en lugar de un cese de actividades, realizarán tomas culturales, plantones frente a instituciones estatales y movilizaciones.** ([Le puede interesar: 92 Feminicidios en América Latina han sido registrados en menos de 2 meses](https://archivo.contagioradio.com/92-feminicidios-en-america-latina-han-sido-registrados-en-menos-de-2-meses/))

En el caso de Colombia, lideresas de la plataforma Déjame En Paz, quienes vienen organizando jornadas como el 25 de noviembre, Día de la No Violencia contra las Mujeres y el 8 de Marzo, aseguraron que debido a falta de garantías de seguridad, evidenciadas en los asesinatos contra líderes y lideresas en distintos territorios, se realizarán jornadas de movilización y plantones, **en ciudades como Cali, Manizales, Medellín, Bogotá, Pasto y otras que aún están por confirmar.**

Por otra parte organizaciones y centrales sindicales de mujeres en Uruguay, Chile, Argentina y Ecuador, han anunciado que debido a la situación diferencial de las mujeres asalariadas, no habrá un paro total de actividades, **proponen un paro parcial desde las 4:00pm hasta las 10:00pm del 8 de Marzo, para que distintas mujeres puedan adherirse** a las jornadas de movilización y protestas. ([Le puede interesar: Feminicidios estarían directamente relacionados con multinacionales](https://archivo.contagioradio.com/feminicidios-estarian-directamente-relacionados-con-multinacionales/))

###  

Con las premisas de que “**sin nosotras no hay producción, ni desarrollo, ni vida sostenible” y “la solidaridad es nuestra arma”, mujeres de todo el mundo invitan a dar continuidad a la iniciativa** de las mujeres textileras estadounidenses, que sentaron su voz de protesta un 8 de marzo de 1857 exigiendo mejores condiciones laborales y las mujeres islandesas, quienes en 1975 convocaron a un paro nacional en rechazo a un proyecto de ley que pretendía penalizar el aborto en ese país.

###### Reciba toda la información de Contagio Radio en [[su correo]
