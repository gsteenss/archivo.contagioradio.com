Title: "El fracking no es necesario, no le mientan al país"
Date: 2020-01-21 12:55
Author: CtgAdm
Category: Ambiente, Entrevistas
Slug: el-fracking-no-es-necesario-no-le-mientan-al-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EOmnKJiU0AEu5os.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @PizarroMariaJo

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Luego del cacerolazo que se desarrolló frente a la Universidad EAN el pasado 16 de enero, y las fallidas conversaciones que se han dado entre el sector ambiental y Gobierno entorno a los Proyectos Pilotos Integrales de Fracking (PPII), el Gobierno nuevamente mostró su apoyo a este proyecto afirmando, que necesitan "*el fracking para la autosuficiencia energética"* .

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante este pronunciamiento, Óscar Sampayo integrante de la Alianza Colombia Libre de Fracking afirmó, que *"el Gobierno ha salido con una nueva ofensiva, manifestando que se deben apoyar esos planes piloto, con un argumento muy flojo diciendo que se necesita para generar gas y llevar a la casa de los Colombianos"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Argumento que, según Sampayo no se soporta con la verdad, "teniendo en cuenta que la producción de gas va a generar energía a las termoeléctricas o a la actividad industrial, y no *trabajará en beneficio de las familias de clase media colombiana como lo afirma el Gobierno"*. (Le puede interesar: <https://archivo.contagioradio.com/plan-piloto-para-fracking/>)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un Gobierno de oídos sordos

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El 26 de diciembre del 2019, el presidente Iván Duque publicó su propuesta ante el Decreto de reglamentación de los pilotos del fracking y dio plazo a la ciudadanía para opinar y apelarlo hasta el 16 de enero del 2020, con la intención de emitirlo una semana después de analizar las propuestas recolectadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo, para Óscar Sampayo la acogida de estas acotaciones por parte del Gobierno fueron nulas, *"lo que vemos aún, es un diálogo de yo con yo donde el Gobierno tiene a sus funcionarios trabajando a favor de los pilotos, desconociendo a científicos, académicos en contraposición, y especialmente omitiendo a las personas en los territorios"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante esto Sampayo manifestó que como Alianza se abstendrán de participar en próximos diálogos y tomarán medias amparadas por la Constitución para reclamar la detención de esta practica en el país. (Le puede interesar: <https://archivo.contagioradio.com/iniciaria-fracking-2019/>)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AurelioSuarez/status/1218213045928939521","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AurelioSuarez/status/1218213045928939521

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:quote -->

> *"Gobierno muestra su falta de voluntad política de respetar los principios de participación"*
>
> <cite>Oscar Sampayo</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Sampayo agregó que Colombia debe cerrarle la puerta definitivamente al Fracking y enfocar sus esfuerzos políticos, tecnológicos, jurídicos y económicos en adelantar una transición energética justa y democrática, acorde a la magnitud de la emergencia climática que enfrentamos. (Le puede interesar: <https://archivo.contagioradio.com/celebracion-navidena-en-esta-realidad-del-pais/>)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué le espera a Colombia en 2020 con respecto al fracking?

<!-- /wp:heading -->

<!-- wp:image {"align":"center","id":79360,"width":512,"height":512,"sizeSlug":"large"} -->

<div class="wp-block-image">

<figure class="aligncenter size-large is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EOmnKJiU0AEu5os-1024x1024.jpg){.wp-image-79360 width="512" height="512"}  
<figcaption>
@ColombiaNoFrack  
  
</figcaption>
</figure>

</div>

<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Según la Alianza de Colombia Libre de Fracking, para el 2020 se tienen planificados pilotos en regiones como Barrancabermeja, Puerto Wilches, Cimitarra, San Martín, entre otros, acciones que *"generarían no solo un impacto ambiental, también social a causa del flujo de intereses en los territorios explotados".*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para Sampayo, un hecho que evidencia la violencia que se vive en Santander se dio el pasado 19 de enero en el **municipio de Cimitarra, cuando actores armados dispararon en contra de los habitantes de la comunidad: Saúl Mateus, Luisa Sánchez y causaron la muerte de Jimmy Quiroga**, en medio de un torneo de fútbol.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el vocero, este **hecho hace parte del contexto del territorio, que no es ajena a la activación de uno lo de los pilotos, perteneciente al bloque DMN9 de la empresa Parex**, *"rechazamos esta técnica y este tipo de planes pilotos de exploración, porque vulnera la integridad humana y genera la destrucción total de ecosistemas"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Lo que se espera con esto proyectos es una serie de violaciones a los Derechos Humanos" .*
>
> <cite>Oscar Sampayo</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Salimos también este \#21E

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El movimiento ambientalista se ha hecho visible en 2019 en las diferentes movilizaciones, y este 21 de enero no será la excepción; *"el movimiento ambiental se encuentra unido y fortalecido, dialogando y debatiendo diariamente sobre las problemáticas ambientales como el uso de glifosato, la explotación del páramo de Santurban y claro, el fracking"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Asimismo Sampayo señaló que desarrollarán iniciativas propias en el mes de marzo, con movilizaciones locales y regionales en Magdalena Medio, y afirmó el apoyo del Movimiento ante la anunciada movilización que harán desde el Páramo de Santurban el próximo 16 de marzo. (Le puede interesar: <https://www.justiciaypazcolombia.com/duque-perdio-consejo-de-estado-no-le-dio-via-libre-al-fracking/>)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:html -->  
<iframe id="audio_46801733" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46801733_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
