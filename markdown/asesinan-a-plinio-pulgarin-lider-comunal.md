Title: Asesinan a Plinio Pulgarin lider comunal de San Jose de Uré, Córdoba
Date: 2018-01-18 21:34
Category: DDHH, Nacional
Tags: Plinio Pulgarin, San Jose de Ure
Slug: asesinan-a-plinio-pulgarin-lider-comunal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/plinio-pulgarin-san-jose-de-ure.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: zoominformativo] 

###### [18 Ene 2017 ] 

La denuncia dada a conocer por la Asociación de Campesinos del Sur de Córdoba, ASCUSUR, asegura que un grupo armado desconocido asesinó en la madrugada de este 18 de enero a Plinio Pulgarín, presidente de la Junta de Acción Comunal de nla vereda San Pedrito de San José de Uré en el departamento de Córdoba, además ordenaron a la comunidad desplazarse inmediatamente de sus tierras.

Plion Pulgarín era integrante de Marcha Patriótica de la Asociación de Zonas de Reserva Campesina y de la Coordinadora Nacional de Cultivadores de Hoja de Coca, Marihuana y Ampaola COCCAM, por lo que también apoyaba el proceso de sustitución de cultivos de uso ilícito que se adelanta tras el acuerdo del gobierno con las FARC.

Aunque se desconocen los detalles del hecho, los armados habrían llegado en la madrugada a la comunidad para asesinar al líder comunal. Además emitieron la orden de desocupar el territorio por lo que las cerca de 50 familias resolvieron huir hacia la cabecera municipal de San José de Ure.

Adicionalmente la denuncia menciona que la campesina **Diana Dorado, quien también participa del proceso de sustitución de cultivos de uso ilícito fue amenazada por los armados**, quienes le dieron media hora para salir de su hogar, de lo contrario correría con la misma suerte de Plinio.

En días anteriores la comunidad había denunciado la presencia de grupos armados y se **había emitido una alerta temprana el pasado 14 de Enero en que la defensoría alertaba sobre la difícil situación de seguridad que afrontan las comunidades del sur de Córdoba** dada la presencia de paramilitares del Clan del Golfo y de disidencias de las FARC.

### **No paran los asesinatos de líderes sociales, comunales y de sustitución de cultivos** 

Ya las FARC habían denunciado el asesinato de 49 de sus integrantes y algunas cifras dan cuenta del asesainto de ceerca de 190 líderes sociales y comunales desde la firma del acuerdo de paz en Noviembre de 2016 en el teatro Colón de Bogotá. Sin embargo, a pesar de la contundencia de los hechos tanto el Ministerio de Defensa como la propia fiscalía siguen negando la sistematicidad de estos asesinatos.

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
