Title: Vigilia por la paz con justicia social en el Castillo, Meta
Date: 2016-10-25 13:25
Category: Movilización, Paz
Tags: El Castillo, FARC, Meta, paz, peregrinacion, vigila por la paz, Vigilia
Slug: vigilia-por-la-paz-en-el-castillo-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/vigilia-castillo-meta-15.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/CAstillo-Meta-e1477419386389.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Justicia y Paz] 

###### [25 de Oct 2016 ] 

**Diversas organizaciones sociales, comunidades bíblicas, entre otros, han decidido unirse para exigir la implementación de los acuerdos pactados en La Habana.** Por ello, se dan cita en el Meta, municipio de El Castillo, para encontrarse en un evento que han denominado **Peregrinación y Vigilia por la Paz.**

**El Castillo,** como muchos otros lugares de Colombia, ha sido **víctima de las implicaciones de la guerra por más de 5 décadas. Razón por la cual han decidido respaldar los acuerdos de paz.**

Henry Ramírez, misionero claretiano y vocero del evento, manifestó que **luego de tantos años de olvido estatal y violencia “no podemos dejar de reconocer el ambiente de calma que vivimos y que este es causa de los acuerdos de La Habana**”.

La peregrinación y vigilia por la paz, comenzará el 29 de Octubre, con la partida de varios buses que arribarán a El Castillo desde diversos lugares del país.

Dentro de la programación del evento, están una serie de **caminatas y actividades en las que reconocerán las afectaciones de la guerra a la vida y a sus territorios** “caminaremos por las trochas que los campesinos han tenido que andar durante estos 50 años y para entender los daños ocasionados por grupos paramilitares y de las Farc”.  
Le puede interesar: [Más de 500 organizaciones de víctimas respaldan jurisdicción especial para la paz](https://archivo.contagioradio.com/mas-de-500-organizaciones-de-victimas-respaldan-jurisdiccion-especial-para-la-paz/)

Adicionalmente, en la noche, **se realizará la vigilia en la cual se hará un acto de memoria a las víctimas,** reconocimiento del territorio y un compartir donde las comunidades y personas participantes hablarán de la paz “las alternativas que hemos venido construyendo en el territorio” puntualizó Henry.

**Millones de corazones unidos por la paz, así han denominado el acto de cierre de esta peregrinación y vigilia,** en la que muchas personas reunidas en El Castillo se unirán para manifestar la necesidad de implementar los acuerdos. Contenido similar: [Mesa Ecuménica lanza propuesta en respaldo a los acuerdos de paz](https://archivo.contagioradio.com/mesa-ecumenica-lanza-propuesta-en-respaldo-a-los-acuerdos-de-paz/)

**FARC abrirá campamentos para hacer vigilia por la paz**

**El Bloque Sur de las Farc dio a conocer  que el próximo 29 y 30 de Octubre realizarán una vigilia por la paz.** Aseguraron que quienes deseen acompañarles podrán  acudir a algunos campamentos que han decidido abrir a la población.

Lugares como Currillo (Caquetá), Victoria Baja (en inmediaciones a Florencia), Las Morras (San Vicente del Caguán), entre otros, son los sitios en los que se puede asistir a esta vigilia.

**Las personas que quieran acudir deben llevar camping, plato, utensilios, pocillo, y alimentos** para participar de la olla comunitaria que se realizará en los campamentos.

<iframe id="audio_13475215" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13475215_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
