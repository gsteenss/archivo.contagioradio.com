Title: "El sí es una posibilidad de encontrar nuestros desaparecidos": MOVICE
Date: 2016-08-30 16:57
Category: DDHH, Nacional
Tags: Desaparición forzada en Colombia, MOVICE, Palacio de Justicia
Slug: el-si-es-una-posibilidad-de-encontrar-nuestros-desaparecidos-movice
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Sin-Olvido.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [30 Ago 2016] 

Hace 31 años Pilar Navarrete no sabía que existía la desaparición forzada en Colombia, luego de que su esposo desapareció durante la retoma al Palacio de Justicia, la angustia de no cerrar el ciclo de búsqueda, de no tener un duelo, de siempre esperar y de no saber que decirle a sus cuatro hijas, la llevó a darse cuenta que **no se trataba sólo de su caso sino del de las más de 45 mil familias que aún esperan encontrar con vida a su familiar**.

Pilar junto a otros familiares y organizaciones de víctimas, se dieron cita este martes en el predio en el que será construido el Museo Nacional de la Memoria, para rendir homenaje a las víctimas de desaparición forzada en Colombia con un acto simbólico denominado 'Cuerpos Gramaticales' que [[ya se ha realizado en Medellín](https://archivo.contagioradio.com/mujeres-siembran-por-la-memoria-en-la-escombrera/)] y que busca que los familiares entierren sus cuerpos para **conmemorar a quienes han desaparecido como sí fuesen semillas de vida**.

En la plazoleta de la Alpujarra familiares y organizaciones también conmemoraron este día con un plantón y una galería de la memoria, con la que buscan llamar la atención de las autoridades locales y nacionales para que los **esfuerzos en la búsqueda de desaparecidos sean conjuntos e integrales** y no aislados, como ha venido sucediendo con las [[excavaciones en La Escombrera](https://archivo.contagioradio.com/a-13-anos-de-impunidad-florece-la-dignidad-entre-los-escombros-de-la-comuna-13/)] o con la intervención del cementerio Universal en el que se han sepultado gran cantidad de personas sin identificar o con identidades cambiadas, según afirma Marta Soto, Secretaria Técnica del MOVICE.

Para organizaciones como el MOVICE es urgente que se ponga en marcha un plan integral de búsqueda de personas desaparecidas, en el que puedan participar los familiares de las víctimas, pues Colombia es el país con [[mayor número de desapariciones](https://archivo.contagioradio.com/el-estado-colombiano-es-el-responsable-del-84-de-las-desapariciones-forzadas/)] en el continente, con [[una impunidad del 99%](https://archivo.contagioradio.com/99-de-los-casos-de-desaparicion-forzada-estan-en-la-impunidad/)]. Una grave situación frente a la que las víctimas esperan que se acojan las sentencias de la Corte Interamericana y que los colombianos voten [[si en el plebiscito](https://archivo.contagioradio.com/el-decreto-que-oficializa-el-plebiscito-para-la-paz/)] pues para ellas los acuerdos de paz son una **posibilidad para encontrar a sus familiares**.

<iframe src="http://co.ivoox.com/es/player_ej_12720164_2_1.html?data=kpeklJWVepWhhpywj5WaaZS1k5Wah5yncZOhhpywj5WRaZi3jpWah5yncbHdzcbfjbPFusLm08rhx4qWh4zawtLWzs7Ftoy4xtjO0sbWqcTdxdSYssbQpcTd0JDRx5CuudToypKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe src="http://co.ivoox.com/es/player_ej_12720183_2_1.html?data=kpeklJWVfJShhpywj5WcaZS1kZuah5yncZOhhpywj5WRaZi3jpWah5ynca7V09nOjbjTuNCZk6iYr7S6jaS5jLLSxsrQsIa3lIquptORaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
