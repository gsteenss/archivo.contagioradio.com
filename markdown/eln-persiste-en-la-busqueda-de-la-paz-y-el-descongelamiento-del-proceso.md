Title: ELN persiste en la busqueda de la Paz y el descongelamiento del proceso
Date: 2016-07-18 15:58
Category: Nacional, Paz
Tags: conversaciones de paz con el ELN, proceso de paz eln
Slug: eln-persiste-en-la-busqueda-de-la-paz-y-el-descongelamiento-del-proceso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/entrevista-eln.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ELNmedios] 

###### [18 de Jul] 

El Ejercito de Liberación Nacional, dio a conocer un vídeo en el que hablan sobre el **proceso de paz con el gobierno y el congelamiento por el que atraviesa**, además hacen referencia al anuncio del **cese bilateral entre la guerrilla del as FARC- EP** y el gobierno y a la importancia de la **participación ciudadana para que Colombia obtenga la paz**.

En el vídeo aparece el comando central del ELN conformada por Pablo Marín, Nicolas Rodríguez e Ismael Ramirez, quienes argumentaron que esa guerrilla "**desde que nació lucha por la paz** y en este gobierno del presidente de Juan Manuel Santos, [quién ha planteado la búsqueda de la paz somos nosotros](https://archivo.contagioradio.com/eln-explica-en-que-va-el-proceso-de-conversaciones-de-paz/). Ha sido el presidente quién ha suspendido los acuerdos que están establecidos dentro de la fase exploratoria y **nos ha condicionado con unas definiciones unilaterales que no están dentro de la agenda**" aseguro Nicolas Rodríguez.

Sobre el cese bilateral con la guerrilla del as FARC-EP dijeron que como ELN "**no pueden decir que con un proceso de paz, con una guerrilla ya se acabo la guerra**, ya se acabo la lucha, porque los movimientos sociales no van a permitir que se les impongan acuerdos".

A su vez, Edgar Mujica líder de la Unión Sindical Obrera y vocero de la plataforma social Congreso de los Pueblos señalo en este punto que es **es fundamental que se tenga la posibilidad para todas las insurgencias   del país de transitar por la vía de la solución política** y eso implica gestos de paz, que el gobierno cumpla lo pactado pero también que [escuche ese reclamo que viene haciendo la sociedad para que se avance en la fase pública](https://archivo.contagioradio.com/crece-respaldo-al-proceso-de-conversaciones-de-paz-con-farc-y-el-eln/).

Para finalizar, el ELN recalcó  la diferencia entre el proceso de paz con las FARC-EP y el proceso que ellos quieren continuar: la participación amplia e incluyente de la sociedad civil "Nosotros concebimos que un proceso de paz autentico en Colombia no puede ser ausente la sociedad colombiana, por el contrario **la sociedad sobretodo la excluida del poder, debe ser protagónica en un proceso de paz**, porque esa es la sociedad que ha padecido y que padece la guerra, pero además nosotros no tenemos el derecho de decidir por ella".

De acuerdo con Mujica [las organizaciones sociales y en general el movimiento social respalda que se descongelen los diálogos](https://archivo.contagioradio.com/51-congresistas-de-estados-unidos-respaldan-proceso-de-paz-con-el-eln/) entrte el ELN y el gobierno para que finalmente se genere una vía de discusión para la Paz.

<iframe src="http://co.ivoox.com/es/player_ej_12262783_2_1.html?data=kpefmJebfJShhpywj5aZaZS1lpuah5yncZOhhpywj5WRaZi3jpWah5yncabYyMbfjbLZrsrXwoqfpZC5l7Chhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

 
