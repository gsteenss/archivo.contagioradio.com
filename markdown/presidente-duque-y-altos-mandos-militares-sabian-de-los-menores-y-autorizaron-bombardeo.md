Title: Presidente Duque y altos mandos militares sabían de los menores y autorizaron bombardeo
Date: 2020-08-20 20:48
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Bombardeo a menores, Ejército Nacional, Operación Atai, San Vicente del Caguán
Slug: presidente-duque-y-altos-mandos-militares-sabian-de-los-menores-y-autorizaron-bombardeo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Bombardeo-a-menores.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Informe-de-inteligencia-No.-937.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Una investigación realizada por Cuestión Pública y [Dejusticia](https://www.dejusticia.org/), reveló que el bombardeo efectuado en San Vicente del Caguán, Caquetá, por el Ejército Nacional el pasado 29 de agosto, el cual acabó con la vida de al menos 8 menores de edad, **fue ejecutado, pese a que, los altos mandos militares, el entonces Ministro de Defensa, Guillermo Botero, y el presidente Iván Duque, tenían el pleno conocimiento de que en el lugar había presencia de menores.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe de inteligencia No. 937 del 24 de agosto de 2019, emitido cinco días antes de la denominada Operación Atai, reveló que **el Ejército tenía conocimiento de que el esquema de seguridad de alias “Gildardo Cucho”** —**objetivo principal del operativo**— **estaba conformado por menores de edad, los cuales habían sido reclutados de manera forzada.** (Le puede interesar: [En operación militar asesinan a Joel Villamizar líder indigena Uwa](https://archivo.contagioradio.com/en-medio-de-operacion-militar-fue-asesinado-el-indigena-joel-villamizar/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":88557,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Informe-de-inteligencia-No.-937.png){.wp-image-88557}  

<figcaption>
Extracto del informe de inteligencia No. 937 del 24 de agosto de 2019

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Pese a ello, **el presidente Iván Duque autorizó previamente el bombardeo y una vez efectuado, calificó el operativo como una «*labor estratégica, meticulosa, impecable*».**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanDuque/status/1167490954670092289","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanDuque/status/1167490954670092289

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Las consecuencias de dicho operativo son una responsabilidad que el entonces Ministro de Defensa, Guillermo Botero, también se negó a reconocer antes de dejar su cargo; señalando que su Ministerio no había recibido ningún requerimiento por parte de la Fiscalía General de la Nación frente al caso. (Lea también: [«Botero debe asumir responsabilidad política y penal por bombardeo a niños»](https://archivo.contagioradio.com/botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tampoco lo hicieron altos mandos como el comandante de las Fuerzas Militares, Luis Fernando Navarro y el comandante del Ejército Nacional, Nicacio Martínez; quienes aseguraron que **«*no se tenía conocimiento de la presencia de menores de edad que hicieran parte del esquema de seguridad de Gildardo Cucho*»** y que **«*de haberse sabido, la decisión hubiese sido otra*».**

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=UO4IMEnwTBc","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=UO4IMEnwTBc

</div>

<figcaption>
Rueda de Prensa Ministerio de Defensa y Comando de las FF.MM. (12:15 - 12:25)

</figcaption>
</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:heading {"level":3} -->

### El bombardeo a menores habría sido una operación precipitada

<!-- /wp:heading -->

<!-- wp:paragraph -->

Diana Salinas, investigadora de Cuestión Pública, señaló que por esos días, Iván Márquez había anunciado su regreso a las hostilidades dentro de las disidencias de FARC y el Gobierno, según Salinas, necesitaba «*un golpe de opinión muy fuerte*» para contrarrestar dicho  anuncio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, recordó que en ese momento también se habían hecho públicas las denuncias del New York Times, sobre una política dentro del Ejército colombiano similar a la implementada durante la época de los llamados falsos positivos. **Entre algunas de las directrices militares, se pedía doblar los resultados de ataques, capturas, rendiciones y muertes en combate; y disminuir los márgenes de certeza sobre el objetivo antes de ejecutar los operativos.** Lea también: [Hay políticas que propician la criminalidad en las Fuerzas Militares: CCEEU](https://archivo.contagioradio.com/hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La investigadora sugirió que podía existir una relación directa entre estos dos hechos y la decisión de desplegar la Operación Atai, que produjo la muerte de los menores en el bombardeo.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
