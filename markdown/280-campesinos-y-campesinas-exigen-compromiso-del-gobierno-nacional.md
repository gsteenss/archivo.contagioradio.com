Title: ASCAMCAT se toma pacíficamente Defensoría del Pueblo
Date: 2015-05-28 13:44
Category: Movilización, Nacional
Tags: ASCAMCAT, Asociación Campesina del Catatumbo, Catatumbo, Defensoría del Pueblo, detenciones arbitrarias, ESMAD, Fuerza Pública, Juan Manuel Santos, lucha contra la impunidad, Norte de Santander, Paro agrario 2013, zonas de reserva campesina
Slug: 280-campesinos-y-campesinas-exigen-compromiso-del-gobierno-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/11347706_10153221826655020_555755521_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### <iframe src="http://www.ivoox.com/player_ek_4564059_2_1.html?data=lZqjlpWZfY6ZmKiakp2Jd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhbS3orKwo7mPt8af1dTaw5DIqYza0Nfaw5DUpcSZpJiSo6nKrcTVjKnSyMrSt9Dmhqigh6aopYzYxtGYstrJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>[Olga Quintero, Asociación Campesina del Catatumbo] 

###### [28 de May 2015]

Desde la mañana de este jueves, **280 campesinos y campesinas provenientes del Catatumbo, Norte de Santander,** se tomaron de **forma pacífica** la entrada de la Defensoría del Pueblo en Bogotá, con el objetivo de pedir la presencia de Jorge Armando Otálora, defensor del pueblo, debido a los incumplimientos por parte del Gobierno Nacional.

Olga Quintero, vocera de la Asociación Campesina del Catatumbo, **ASCAMCAT,** informó que ella, al igual que los demás campesinos exigen al presidente Juan Manuel Santos, que **cumpla con los acuerdos pactados en la mesa de negociaciones** que abrían logrado suspender el pasado paro en esa región del país. Por otro lado, piden la reactivación de la Mesa de Interlocución y Acuerdo del Catatumbo el próximo 29 de mayo, y finalmente le reclaman al gobierno que haga cumplir la **Ley 160 y constituya la zona de reserva campesina** en área que comprende Hacarí – San Calixto – Teorama.

Para el campesinado, es necesaria la presencia del defensor del pueblo, ya que le piden que se comprometa en la **lucha contra la impunidad** de cuatro campesinos del Catatumbo, que fueron asesinados por el Ejército Nacional y la Policía en el paro del año 2013. Además, le solicitan a Otálora que busque garantías para la **no criminalización de la protesta social,** debido a que durante el paro anterior se detuvo arbitrariamente a **Elivaneth Uribe, Edgar León, y Ramón Elías Claro** quienes fueron condenados a 12 años de presión.

Quintero, afirma que el tiempo que permanezcan los y las campesinas frente a la defensoría dependerá de la respuesta de las autoridades. Así mismo, dijo que la **defensoría aseguró que se comprometía a que la fuerza pública no iba a reprimirlos.**

"Estamos en una acción pacífica pidiendo presencia del defensor del pueblo", señaló la vocera de ASCAMCAT, quien resaltó que pese a que se tiene la disposición de dialogar y buscar soluciones, si el gobierno mantiene su actitud de incumplimiento y de no garantías para la protesta social, **es posible que** **nuevamente haya paro campesino.**
