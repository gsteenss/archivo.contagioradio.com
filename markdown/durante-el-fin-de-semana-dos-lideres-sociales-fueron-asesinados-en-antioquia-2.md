Title: Durante el fin de semana dos líderes sociales fueron asesinados en Antioquia
Date: 2019-02-18 13:41
Author: AdminContagio
Category: Comunidad, DDHH, Líderes sociales
Tags: Antioquia, asesinato de líderes sociales, Caucasia
Slug: durante-el-fin-de-semana-dos-lideres-sociales-fueron-asesinados-en-antioquia-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Diseño-sin-título-6-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @Antioquiajoven 

###### 18 Feb 2019

El pasado 17 de febrero fue encontrado por las comunidades de la vereda La Ahuyama, Segovia el cuerpo sin vida de **Jaime Jiménez, líder minero del nordeste de Antioquia y habitante de la vererda Panamá 9**  quien según un comunicado emitido por las comunidades campesinas de la zona habría asesinado por el ELN.

La Corporación Acción Humanitaria por la Convivencia y la Paz del Nordeste Antioqueño (Cahucopana) dio a conocer la posición de las organizaciones sociales de la región quienes rechazaron la muerte de Jiménez y denunciaron el fortalecimiento del paramilitarismo en el lugar.

De igual forma, las comunidades de **La Cristalina, Cañaveral de Chicamoqué, Mulato, Tamar Alto, Panamá 9, Altos de Manila, Cañaveral, El Carmen, Lejanías, Ojos Claros y El Piñal** señalaron las constantes amenazas de las que son víctimas por parte de grupos armados tanto legales como ilegales e hicieron un llamado al Gobierno para reanudar la mesa de diálogos con el ELN y poder establecer un cese al fuego bilateral.

Frente al hecho, **Óscar Yesid Zapata, integrante de Coeuropa Antioquia **expresó que es necesario un desmantelamiento de las estructuras paramilitares y que el Plan de Acción Oportuna (PAO), medida planteada por el Gobierno para proteger a los líderes y defensores de derechos es "una oportunidad militarista para implementar una política de seguridad democrática que solo ha dejado muerte y desolación".

### **"Sentimos que las autoridades no están tomando las medidas efectivas" ** 

Zapata agregó que **Querubín Zapata, líder juvenil de 27 años** también fue asesinado el día sábado 16 en el barrio Las Brisas del municipio de **Caucasia**, el joven pertenecía a la plataforma municipal de juventud y era reconocido por su trabajo en la comunidad como activista y defensor de los grupos de diversidad de género.

El  integrante de Coeuropa Antioquía advierte que la Alcaldía y algunas autoridades del lugar tienen interés en desviar la investigación pues relacionan la venta y consumo de estupefacientes con la muerte del joven líder quien a menudo denunciaba los enfrentamientos entre grupos como los Caparrapos, las Autodefensas Gaitanistas de Colombia y el Clan del Golfo, por el control del territorio y de los cultivos de uso ilícito de la región.

<iframe id="audio_32651217" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32651217_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
