Title: Continúa proceso de liberación de la madre tierra en Aguas Tibias, Cauca
Date: 2017-04-11 14:45
Category: DDHH, Nacional
Tags: Cauca, liberación de la madre tierra
Slug: continua-proceso-de-liberacion-de-la-madre-tierra-en-aguas-tibias-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Congreso-CRIC-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

###### [11 Abr 2017] 

Indígenas del resguardo Kokonuko se encuentran en proceso de liberación de la madre tierra en la vereda Aguas Tibias, en el Cauca, debido al incumplimiento por parte del gobierno Nacional **que hace un año se comprometió a cederle predios que hacen parte de territorio ancestral, a esta comunidad y aún no hay ningún resultado**.

Esneido Avidama, gobernador del resguardo, señaló que **“la comunidad necesita que se haga un saneamiento del reguardo y que como se han llegado a unos acuerdos**”, el gobierno en concertación con el dueño del predio permita que ellos lo adquieran, y desmintió las versiones que indicaban que los indígenas se encontraban bloqueando la entrada al predio o la vía que comunica con el Huila.

De igual forma, Avidama señala que desde hace un año el gobierno había hecho estas promesas, sin embargo, **no ha cumplido los tiempos en los que debía realizar una oferta de compra, ni ha hecho un seguimiento por parte de las instituciones correspondientes** a los acuerdos. Le puede interesar:["Si el gobierno no nos atiende concertadamente , lo hará en las calles: Campesinos del Cauca"](https://archivo.contagioradio.com/si-el-gobierno-no-nos-atiende-concertadamente-nos-atendera-en-las-calles-campesinos-del-cauca/)

Los indígenas que perteneces al reguardo Kokonuko, expresaron que “**están colocando a disposición del propietario su voluntad de querer ofertar**” y señalan que no existe una intención de asentarse en estos territorios pese a que sean de su propiedad. Le puede interesar: ["Se instala Comisión Nacional de Garantías de Seguridad en el Cauca"](https://archivo.contagioradio.com/en-cauca-se-instalara-comision-nacional-de-garantias/)

Este miércoles se llevará a cabo **una asamblea en donde se espera que participen diferentes Instituciones del Estado y todos los garantes de este proceso** para darle un buen trámite a este proceso.

<iframe id="audio_18094994" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18094994_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
