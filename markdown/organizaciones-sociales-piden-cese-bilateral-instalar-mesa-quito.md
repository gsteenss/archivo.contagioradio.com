Title: Organizaciones Sociales piden Cese bilateral para instalar mesa en Quito
Date: 2016-10-28 15:43
Category: Nacional, Paz
Tags: ELN, Mesa de diálogos Quito, Organizaciones sociales
Slug: organizaciones-sociales-piden-cese-bilateral-instalar-mesa-quito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/mesa-en-quito.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Lanzas y Letras] 

###### [28 de Oct 2016] 

Pese a que no se dio el día de ayer la instalación de la mesa de conversaciones entre la guerrilla del ELN y el Gobierno de Colombia, 450 personas representante de la sociedad civil que participarían de este evento expresaron **la necesidad de que se aclaren los impases que se han presentado y se proceda a iniciar con los diálogos prontamente.**

De igual forma expidieron un comunicado de prensa en donde le piden tanto al Gobierno como al ELN que se dé un **cese bilateral al fuego que permita que se proceda con la mesa**, sin tener más impases. A su vez, evidenciaron la necesidad de **fortalecer los canales de comunicación entre las partes para que la participación y legitimidad de la mesa no se vean alteradas por malos manejos de la información.**

De acuerdo con el Representante del Polo Democrático, Alirio Uribe, que estuvo en el evento “el llamado es que haya un cese bilateral al fuego y de hostilidades, porque **dialogar en medio del conflicto armado genera mucho más ruido y torpedea la mesa de diálogos**”. Le puede interesar: ["Organizaciones sociales proponen diálogo nacional en mesa ELN-Gobierno"](https://archivo.contagioradio.com/organizaciones-sociales-proponen-dialogo-nacional-en-mesa-eln-gobierno/)

En el lugar también se encontraban impulsadores de la Mesa Social para la Paz. Estaban esperando la instalación de la mesa para presentarle a los equipos de conversaciones, esta **propuesta de participación para la sociedad civil y dar paso a la construcción del mismo. **Le puede interesar:["Ya están conformados equipos de ELN y Gobierno para diálogos en Quito"](https://archivo.contagioradio.com/conformados-equipos-de-eln-y-gobierno-para-dialogos-en-quito/)

Entre las organizaciones se encontraban voceros de la Coordinadora Nacional Agraria, Congreso de los Pueblos, diferentes académicos, representantes políticos como Alirio Uribe y Víctor Correa, organizaciones de víctimas, organizaciones ecuatorianas que están respaldando el proceso, entre otros.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
