Title: Indígenas Jiw del Meta completan 5 meses sin recibir atención humanitaria
Date: 2018-05-24 13:15
Category: DDHH, Movilización
Tags: indígenas jiw, mapiripan, Meta, pueblo jiw
Slug: indigenas-jiw-del-meta-completan-5-meses-sin-recibir-atencion-humanitaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/147.-Mapiripan-e1527185278487.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: PBI] 

###### [24 May 2018] 

Luego de que el Estado incumpliera con su obligación de proteger a las comunidades **indígenas Jiw que habitan en el Meta**, organizaciones defensoras de derechos humanos denunciaron que dichas comunidades se encuentran viviendo en condiciones de desnutrición y desprotección que ponen en riesgo la existencia de su cultura e identidad.

De acuerdo con la Comisión Intereclesial de Justicia y Paz, en 2012 “la Corte Constitucional **dispuso la adopción de medidas cautelares urgentes** para la protección de los derechos fundamentales de los pueblos indígenas Jiw”. A la fecha, estas disposiciones no han sido adoptadas por las diferentes autoridades como la Unidad de Víctimas, el Ministerio de Salud, el ICBF, gobernaciones y alcaldías entre otros.

### **Inidígenas Jiw enfrentan peligro de exterminio físico** 

De acuerdo con la organización, el pueblo indígena Jiw fue desplazado por la violencia hacia el Meta **desde sus territorios ancestrales en el departamento del Guaviare**. Actualmente, el 28.7% de la población Jiw habita en el Meta, específicamente en el municipio de Mapiripán donde desde el 2008,  “la gobernación de Meta dispuso provisionalmente, en arriendo,  una finca denominada la Zaragoza”.

Allí, se encuentran habitando entre 800 y 1200 personas que afrontan condiciones de vida que los esta llevando el **exterminio cultural y físico,** pues los Jiw se han caracterizado históricamente por ser nómadas, es decir, no son cultivadores. (Le puede interesar:["Niños y niñas indígenas no mueren de hambre sino de abandono"](https://archivo.contagioradio.com/ninos-y-ninas-indigenas-no-mueren-de-hambre-sino-de-abandono/))

Con esto en mente, lo Jiw están “a la espera de que el Estado **cumpla con su obligación de reubicación** y garantías de acceso dignas de conformidad a lo ordenado por los diferentes autos de la Corte Constitucional”. Esto teniendo en cuenta que las comunidades enfrentan “un grave peligro” debido al conflicto armado y “la omisión de las autoridades en brindarles una adecuada y oportuna atención”.

### **Corte Constitucional había dispuesto de un plan de reacción y contingencia** 

En 2012 y debido a la gravedad de la situación en la que se encontraban los pueblos indígenas en el Meta, la Corte Constitucional dispuso de un “Plan Provisional Urgente de Reacción y Contingencia” para **atender las necesidades de atención humanitaria** que aseguraran "la pervivencia de los pueblos indígenas desplazados Jiw”.

Para su cumplimiento, dio un plazo de **dos meses** donde las autoridades debían garantizar atención en salud, nutrición, seguridad alimentaria, refugio o alojamiento temporal. Sin embargo, “ninguna de las entidades accionadas ha dado respuesta de forma integral a la grave crisis humanitaria en la que se encuentra la población indígena Jiw en el departamento del Meta”.

Los indígenas “completan **5 meses sin recibir** alimentos por parte de la Unidad de Víctimas, como tampoco se ha efectuado la reubicación de estas comunidades en condiciones dignas”. De igual manera, “los Jiw no tienen ni los proyectos productivos ni la infraestructura necesaria para su sostenimiento nutricional”. (Le puede interesar:["En Mapiripán las mujeres indígenas juegan fútbol por su dignidad"](https://archivo.contagioradio.com/mujeresindigenasfutbol/))

### **Exigieron a las autoridades atender al pueblo Jiw** 

Ante estos hechos, la Comisión Intereclesial de Justicia y Paz exigió a las autoridades que se **desarrollen las medidas necesarias** para atender las necesidades humanitarias de los pueblos indígenas Jiw. De manera específica, le solicitaron a la Unidad de Víctimas “concretar un mecanismo idóneo y eficaz para la entrega efectiva de la ayuda humanitaria, alimentos y utensilios de aseo, teniendo en cuenta una dieta alimentaria especial para los niños, niñas y mujeres en embarazo”.

Además, solicitaron que se realicen **las investigaciones y sanciones** necesarias a las entidades que no cumplieron con lo exigido por la Corte Constitucional y han dejado a los pueblos indígenas desamparados. Esto, en teniendo en cuenta “el desconocimiento histórico de los autos de la honorable Corte Constitucional”.

###### Reciba toda la información de Contagio Radio en [[su correo]

 
