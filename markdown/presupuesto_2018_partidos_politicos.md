Title: Presupuesto 2018 es un "conejo gigante" a los sectores sociales: Libardo Sarmiento
Date: 2017-10-20 17:57
Category: Economía, Entrevistas
Tags: presupuesto 2018, ser pilo paga
Slug: presupuesto_2018_partidos_politicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/cms-image-000042976-e1508536840845.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Confidencial Colombia] 

###### [20 Oct 2017] 

"El presupuesto general de la nación refleja las fuerzas políticas en Colombia", es la conclusión del economista Libardo Sarmiento, respecto **la aprobación de \$235,6 billones de pesos aprobados por el Congreso de la República para el 2018.** Cuyos rubros más afectados son el sector agropecuario, las universidades públicas, el deporte, el ambiente, la ciencia y la tecnología, la cultura, entre otros.

De acuerdo con el analista, no se trata de un presupuesto que se piense en torno a la construcción y de la paz, ni en los sectores más desfavorecidos, por lo contrario, asegura que los terratenientes y el sector empresarial no pagan impuestos, ya que cada sector económico tiene congresistas que defienden sus intereses.

"**Es un concejo de raza gigante** el que le ha hecho el gobierno a las movilizaciones sociales y a los acuerdos de paz", en ese sentido, explica Sarmiento, pareciera que el país continuara en guerra, ya que el presupuesto en **defensa aumentó un 5,5% para un total de \$31,6 billones,** mientras que cayeron los recursos destinados al desarrollo del campo.

### **Para el posconflicto** 

De los 235 billones de pesos, el rubro para posconflicto es de apenas 2,8 millones de pesos, frente a esto se destaca que cálculos no oficiales señalan que **solo para la sustitución de cultivos de uso ilícito de 50.000 familias, se necesitarían 1,8 billones**. Adicional a eso, el sector de la agricultura tuvo una disminución de más del 20%, asimismo, el presupuesto para ambiente y desarrollo bajó un 8,8% y  el deporte un 6%, sin embargo se trata de sectores claves para construcción de paz.

### **Salud y educación** 

[Por otro lado, aunque se ha dicho el presupuesto para la educación aumentó un 5,8%, es de resaltar que mientras **se prioriza el presupuesto para el programa 'Ser pilo paga**',  que contará con 800 mil millones de pesos, en contraste con la asignación para las universidades públicas, solo hay  100 mil millones de pesos. La magnitud del problema es tal que solo la Universidad Nacional tiene un déficit superior a los 130 mil millones de pesos.]

[ De igual firma sucede con la salud, que aparece con un 6,2% de aumento, pero **en hospitales solo se invertirá \$500 mil millones,** y solo el Hospital Universitario del Valle tiene un déficit por más de 120 mil millones, cuando hay más de 300 hospitales públicos en el país.]

El economista señala que el gasto social que aparece en el presupuesto que implica 42 billones de pesos y en el que se contemplan las pensiones en Colombia de las **cuales solo gozan un 20% de lo los mayores de 65 años, de ellos el 66% de los 42 billones que se gasta el Estado en pensiones van para el 2% de los pensionados.** Eso se debe a que la mayor parte de esa suma se gasta en las grandes pensiones, de ex funcionarios públicos, tales como congresista, a quienes por 4 años en ese cargo, obtienen  pensiones por 24 millones de pesos.

En esa medida Libardo Sarmiento concluye, en la misma línea de la congresista Claudia López, que que los partidos políticos son carteles mafiosos que buscan es capturar rentas del Estado, y con ello manejar la bolsa de empleo que manejan los partidos, de allí todos **los problemas de corrupción anualmente hace que el país pierda \$50 billones anuales durante la ejecución de los gastos del Presupuesto General,** como lo afirma el contralor general, Edgardo Maya.

<iframe id="audio_21589989" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21589989_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
