Title: Sin estudios completos aprobarían cupo de endeudamiento para Transmilenio por la Séptima
Date: 2017-09-22 17:18
Category: DDHH, Nacional
Tags: Enrique Peñalosa, Transmilenio
Slug: sin-estudios-completos-aprobaria-cupo-de-endeudamiento-para-transmilenio-por-la-carrera-septima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/carreraseptima.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2Orillas] 

###### [22 Sept 2017] 

El concejal Manuel Sarmiento, del Polo Democrático, afirmó que dentro de 15 días ya se podría aprobar el cupo de endeudamiento para la construcción de Transmilenio por la Séptima, que le daría **2.4 billones de pesos al Alcalde Enrique Peñalosa, sin que hayan culminado los estudios para la realización de este proyecto**.

“Debería ser al revés primero que nos muestren el estudio definitivo y después sí que se apruebe el cupo de endeudamiento” afirmó el concejal y agregó que el afán se debe a los planes que se tendrían de construir Metro elevado por la Av. Caracas, y **para que no colapse la movilidad se necesitaría de forma urgente que transmilenio sistema** se instale por la séptima.

Esta Troncal mueve actualmente **50 mil pasajeros**, razón por la cual Sarmiento manifestó que de ser esta la intensión, a los costos del metro elevado, deberían añadírsele los costos de Transmilenio por la Av. Séptima, provocando un aumento en los costos del metro. (Le puede interesar:["Se aprueba cupo de endeudamiento para más Transmilenio"](https://archivo.contagioradio.com/se-aprueba-cupo-de-endeudamiento-para-mas-trasmilenio-en-bogota/))

Frente a lograr frenar el avance de este debate en el Consejo, Sarmiento afirmó que aún hay acciones que tomar, una de ellas debe ser que los ciudadanos asistan a las cesiones del Consejo y en una movilización se presiones a los **concejales peñalosistas a que no voten por Transmilenio en la Séptima.**

De otro lado los concejales que votaron a favor del cupo de endeudamiento fueron: Yefer Vega, Roberto Hinestrosa y Julio Acosta, de Cambio Radical; Jorge Torres y Hosman Martínez de la Alianza Verde; Ángela Garzón y Daniel Palacios del Centro Democrático; David Belles y Patricia Mosquera del Partido de la U; Germán García del Partido Liberal y Roger Carrillo del partido Conservador. (Le puede interesar: ["Revocatoria a Enrique Peñalosa podría realizarse en Noviembre"](https://archivo.contagioradio.com/revocatoria-a-enrique-penalosa-podria-realizarse-en-noviembre/))

<iframe id="audio_21041617" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21041617_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
