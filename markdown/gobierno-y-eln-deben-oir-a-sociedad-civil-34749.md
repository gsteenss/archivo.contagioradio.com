Title: Gobierno y ELN deben oír a la sociedad civil: De Currea
Date: 2017-01-16 10:37
Category: Paz
Tags: ecuador, ELN, Gobierno, Negociaciones, paz
Slug: gobierno-y-eln-deben-oir-a-sociedad-civil-34749
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo Contagio Radio 

###### 16 Ene 2017

En Ecuador, el Gobierno y la Guerrilla del Ejército de Liberación Nacional –ELN- se han dado cita hace varios días para poder avanzar hacia la instalación de la mesa de negociación, **aunque diversos analistas han asegurado que existe una voluntad política de las dos partes, sectores sociales han dicho estar poco expectantes, objetivamente, al respecto de ese proceso dado lo engorroso, pero que esperan se resuelva pronto.**

El analista político Víctor de Currea Lugo aseguró que dada la expectativa con la presentación de la agenda entre Gobierno y ELN, y de un comienzo pronto de los diálogos, estos no pueden convertirse en "**una ceremonia vacía, sino en acciones hacia “la construcción de ciudadanía”. **Le puede interesar:[ Gobierno y ELN deberían entender que “Ceder no es retroceder”: Carlos Medina](https://archivo.contagioradio.com/gobierno-y-eln-deberian-entender-que-ceder-no-es-retroceder/)

Por otro lado manifestó que “el reposo que tuvo la mesa en diciembre permitió al Gobierno y al ELN darse un aire y volver con una voluntad política de las dos partes que muestra se quiere seguir negociando. Eso no es cualquier cosa”. Le puede interesar: [ELN ratifica su compromiso con la Paz](https://archivo.contagioradio.com/eln-ratifica-su-compromiso-con-la-paz/)

Así mismo, dijo que **pronto podría haber una “salida humanitaria al tema de Odin Sánchez, porque se ha visto un deseo de iniciar cuanto antes las negociaciones para buscar la paz”.**

En días pasados diversas personas reconocidas en el país enviaron una carta al Gobierno Nacional y al ELN que se encuentran en la mesa en Ecuador, **carta que según dice De Currea “generó molestias”, pero que para el analista lo que buscan “es que haya una solución para que el proceso arranque**, lo que está de fondo es una discusión más amplia, más profunda. Hemos llamado a que se resuelvan esas condiciones pequeñas y estamos dispuestos a ayudar en todo lo necesario y empezar a dialogar”.

**“Es poco saludable salir a negociar en los micrófonos” agregó De Currea**, quien dijo que es posible que el desgaste de la sociedad puede deberse a la falta de oportunidad en la comunicación de los avances en el proceso **“si se van a comunicar con la sociedad sean transparentes y oportunos, no tenemos que enterarnos de todo y eso no es secretismo es precaución”.**

Y por ello sugirió a la mesa que se cree un mecanismo de comunicación con la sociedad colombiana **“no se necesita comunicar solo malas noticias y tampoco digo que solo buenas, sino lo que es necesario o importante comunicar”.**

### **Iniciativas de la sociedad civil importantes para la mesa** 

Para el analista político y docente, iniciativas como la Mesa Social para la Paz han permitido que diversidad de voces estén confluyendo, lo que hace que “se esté abierto a nuevos debates” y agregó **“es importante oír varios sectores que están interesados en hablar de paz como militares, empresarios, profesores. La participación de las regiones también es importante. La pluralidad de voces es primordial para los diálogos”.**

Y dijo “si el ELN y el Gobiernos nos invita a participar del proceso, pero si nos van a decir qué, cómo, cuándo y dónde decirlo pues mejor no nos inviten. Se requiere un cuidado cuando se invita a participar a la sociedad y **entender que hay unas luchas sociales que van más allá de los diálogos con el ELN y que también tienen sus agendas”. **Le puede interesar: [Propuestas de la sociedad civil son claves para destrabar mesa ELN-Gobierno](https://archivo.contagioradio.com/propuestas-de-la-sociedad-civil-son-claves-para-destrabar-mesa-eln-gobierno/)

### **Improvisaciones con FARC mandan un mensaje negativo** 

Ante las informaciones conocidas en cuanto a los bajos avances de las adecuaciones de las Zonas Veredales Transitorias a donde se movilizan los integrantes de las FARC-EP, Víctor De Currea afirmó estar muy preocupado por este tema y lo llamó como “improvisaciones en el proceso con las FARC que puede mandar un mensaje negativo al ELN. El Gobierno no ha podido resolver los temas y es una falta de respeto con las personas que le han apostado a la paz”.  Le puede interesar: ["No hay ninguna Zona Veredal lista para recibir a las FARC"](https://archivo.contagioradio.com/no-hay-zona-veredal-lista-parra-las-farc-33716/)

Añadió que **el tema del paramilitarismo es el “gran enemigo de la Paz” y que este no solamente se mantiene sino que se ha incrementado** “(…) tienen –el paramilitarismo- una gran fortaleza militar en los territorios. La extrema derecha hace eco de muchas de las consignas del paramilitarismo y eso crea un bloque que reproduce prácticas". Le puede interesar: [Tras firma del Acuerdo se han agudizado actos violentos contra líderes sociales](https://archivo.contagioradio.com/tras-firma-del-acuerdo-se-han-agudizado-actos-violentos-contra-lideres/)

E hizo un llamado ante los **asesinatos de líderes sociales diciendo que “aquí no se trata de casos aislados, estamos ante una “matazón” de líderes sociales que tiene que parar”. **Le puede interesar: [Estos son los líderes campesinos asesinados o perseguidos de Marcha Patriótica](https://archivo.contagioradio.com/estos-son-los-lideres-campesinos-asesinados-o-perseguidos-de-marcha-patriotica/)

<iframe id="audio_16372490" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16372490_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
