Title: La importancia de firmar un pacto por la no violencia en época electoral
Date: 2019-08-27 13:14
Author: CtgAdm
Category: Nacional, Política
Tags: Campaña electoral, Pactos, partidos politicos, violencia
Slug: pacto-por-la-no-violencia-electoral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Pacto-por-la-No-Violencia.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @infopresidencia  
] 

El lunes de esta semana, todos los partidos políticos se reunieron en la Casa de Nariño para firmar el **pacto por la cultura política y la no violencia para las próximas elecciones locales** que se desarrollarán en octubre de este año. El pacto nació como una idea del Consejo Nacional de Paz, Reconciliación y Convivencia, creado para establecer un acuerdo en la sociedad por la reconciliación, razón por la que este, es el segundo de una serie de pasos que se darán para cumplir con dicha misión.

### **El punto de partida: Más de 300 casos de agresiones a precandidatos y precandidatas** 

**Monseñor Héctor Fabio Henao, integrante del Consejo,** aseguró que el pacto inició manifestando la preocupación ante la situación de violencia que vive el país y la "forma polarizante de hacer política"; de ahí la importancia por incluir a todos los partidos políticos, que son los que dan los avales para presentarse a elecciones. Otra de las organizaciones que tiene parte en el Consejo y actuó como acompañante del pacto fue la **Misión de Observación Electoral (MOE),** que también otorgó insumos para dar razones a este pacto.

En ese sentido, la MOE entregó el informe sobre violencia política en el país que presentó en junio, en el que se recogían **228 ataques contra lideresas y líderes políticos entre octubre de 2018 y mayo de 2019**. En el 29% de estos casos, la agresión culminó en asesinatos; a ello se sumó la preocupación manifestada por la misión relacionada con la posibilidad de que se aumente la violencia conforme se acercan los comicios. (Le puede interesar:["La MOE ha registrado 228 hechos de violencia contra líderes sociales en últimos 8 meses"](https://archivo.contagioradio.com/moe-228-hechos-violencia-lideres/))

### **El pacto es el segundo, de varios pasos que se deben dar para alcanzar un pacto por la reconciliación** 

En aras de trabajar en el pacto por la reconciliación, el Consejo dió un primer paso el año pasado al promover un acuerdo de no violencia entre los candidatos presidenciales, el pacto de este año es el segundo paso, y luego de la participación por parte de todos los partidos políticos, monseñor deseó que ello se traduzca en el establecimiento de un clima político favorable a la paz. (Le puede interesar: ["Candidatos y partidos políticos deben pronunciarse contra violencia en campañas: MOE"](https://archivo.contagioradio.com/candidatos-y-partidos-politicos-deben-pronunciarse-contra-violencia-en-campanas-moe/))

El religioso también destacó las tres ideas centrales que contiene el pacto: reconocer que estamos en un país pluralista, y por lo tanto se requiere trabajar en capacidades para ejercer la política en términos de reconocimiento de las diferencias, el respeto y la capacidad de argumentar de manera racional; fomentar que la campaña no emplee formas de violencia verbales o físicas, y se centre en impulsar la convivencia ciudadana y la búsqueda del bien común; y que los partidos adquieran un compromiso para que no se atente contra la democracia, en términos de agresividad.

### **¿Cómo verificar lo pactado?**

Monseñor aseguró que para hacer seguimiento de lo pactado existen dos mecanismos, el primero, en manos de los organismos de la sociedad civil (como las iglesias) que tienen lugar en el Consejo Nacional de Paz, y que pueden hacer monitoreo de las campañas en las regiones. Asimismo, señaló que **la ciudadanía puede jugar un papel importante en este seguimiento.** (Le puede interesar: ["Violencia política contra Juntas de Acción Comunal aumentaría al acercarse elecciones regionales"](https://archivo.contagioradio.com/violencia-politica-contra-juntas-de-accion-comunal-aumentaria-al-acercarse-elecciones-regionales/))

El segundo mecanismo tiene que ver con algunas de las instituciones que también tiene asiento en el Consejo, por ejemplo, Procuraduría, Defensoría del Pueblo o Ministerio de Interior. Entes que tienen la capacidad, y el deber, de hacer control sobre algunos de los puntos de los que habla el pacto; de allí que mientras la sociedad civil hace la labor de seguimiento y monitoreo, estas instituciones puedan hacer un trabajo de verificación, y si es el caso, sanción de quienes incumplan el pacto.

### **Los siguientes pasos que se darán** 

El líder religioso añadió que aún no han pensado el siguiente paso que se dará en aras de trabajar en el pacto por la reconciliación, no obstante, adelantó que tras el acuerdo por la no violencia, buscarán establecer compromisos regionales similares en los departamentos donde se han presentado hechos de violencia como Antioquia, Córdoba o Arauca. Para ello, **Popayán será el epicentro del siguiente pacto, que será firmado el próximo 2 de septiembre.**

**Síguenos en Facebook:**  
<iframe id="audio_40607929" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_40607929_4_1.html?c1=ff6600"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
