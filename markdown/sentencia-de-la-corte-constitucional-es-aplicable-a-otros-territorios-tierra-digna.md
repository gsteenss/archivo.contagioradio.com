Title: "Sentencia de la Corte Constitucional sobre el Chocó es aplicable a otros territorios": Tierra Digna
Date: 2017-05-03 18:15
Category: Ambiente, Nacional
Tags: Chocó, Corte Consitucional
Slug: sentencia-de-la-corte-constitucional-es-aplicable-a-otros-territorios-tierra-digna
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/corte-e1495109388589.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colprensa 

###### [3 May 2017] 

El fallo de la Corte Constitucional sobre el Río Atrato, en Chocó, es revolucionario, no solo porque establece como sujeto de derechos a un afluente, sino porque además es una sentencia que no solo protege a los accionarios, sino a todas las comunidades del país que se encuentren en situaciones similares, **abriendo paso a que se posibiliten oportunidad para construir un marco de regulación de derechos bioculturales y de la naturaleza.**

Así lo explica la abogada de la ONG Tierra Digna, Xiména González, quien explica que la Corte incluyó en su respuesta a la demanda interpuesta por las comunidades, un efecto denominado ‘Inter comunis’, lo que da paso a que la sentencia pueda ser aplicable a otros territorios. “**Es un nuevo abordaje que permitirá impulsar reformas institucionales, lograr garantías más específicas** y reales frente a los derechos de las comunidades y del ambiente”, dice.

González, indica que la sentencia de la Corte Constitucional se da luego de todo un trabajo previo que venían realizando las comunidades de la región. “**Acudimos a la vía judicial luego de que las comunidades étnicas realizaron varias acciones** como movilizaciones, mesas de concertación con el gobierno, audiencias públicas y procesos formativos a nivel comunitario. La acción jurídica, solo es un complemento a esas acciones que hacen parte de un conjunto de estrategias que se habían pensado en el Chocó”. Una de ellas es una agenda regional de paz, que enfoca la solución del tema del fin del conflicto armado, desde un abordaje de la paz territorial y ambiental. [(Le puede interesar: Corte Constitucional declara al Río Atrato como sujeto de derechos)](https://archivo.contagioradio.com/corte_constitucional_rio_atrato_choco/)

### La situación del Chocó 

En el caso de esta región, desde Tierra Digna se señala que si bien, la minería es parte de la tradición ancestral de las poblaciones, esta actividad se ha trasformado debido a la intromisión de **actores externos de Brasil, Córdoba y Antioquia, asociados en la mayoría de los casos a grupos armados y economías ilegales,** que han logrado ocupar buena parte de las selvas de ese departamento.

Esa transformación en el proceso de la minería ha conllevado a que los niveles de destrucción de la selva chocoana sean excesivos. Según un reporte satelital de las Naciones Unidas, sobre el número de hectáreas degradadas por minería informal, **Colombia es el país con más hectáreas degradas por ese tipo de actividad, y además es el segundo país en el mundo con mayor cantidad de mercurio en sus ríos.**

De manera, que según Tierra Digna, los conflictos en esta región del país, y con ello la crisis ambiental y humanitaria, tienen que ver con la pérdida de autonomía y autodeterminación de las comunidades, impactos en salud, el derecho al agua, y por supuesto, la implementación de proyectos mineros informales, pero también a gran escala.

Frente a ese panorama las soluciones que se plantean gracias al mandato del alto tribunal al fin parecen abordar una estrategia estructural que realmente afronte las necesidades del departamento en material social y ambiental. “Lo que viene es mucho trabajo, no puede quedar en el papel la sentencia de la Corte, **debe haber un trabajo de movilización, acción comunitaria, organización, e incidencia política** para que se cumpla lo estipulado por la Corte”, asegura Xiména González.

<iframe id="audio_18498448" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18498448_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
