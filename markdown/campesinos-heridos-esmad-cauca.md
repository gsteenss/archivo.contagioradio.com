Title: Diez campesinos resultaron heridos en Cauca en medio de Paro Nacional
Date: 2017-10-31 14:43
Category: Movilización, Nacional
Tags: campesinos, campesinos de colombia, paro campesinos, paro nacional indefinido
Slug: campesinos-heridos-esmad-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/DNaw2j6WsAAEZFt-e1509479023483.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [31 Oct 2017] 

Campesinos de diferentes zonas del país, y que se encuentran realizando actividades de movilización en el marco del Paro Nacional Indefinido, han denunciado **fuertes arremetidas por parte de los agentes** del Escuadrón Móvil Antidisturbios de la Policía Nacional. Igualmente, manifestaron que el Gobierno Nacional incumplió la promesa de instalación de la Mesa de Interlocución.

De acuerdo con Jonathan Centeno, integrante de Marcha Patriótica en el Cauca, las comunidades **aún siguen dispuestas a continuar con la movilización** “insistiendo en el carácter pacífico”. Dijo que es desafortunada la decisión del ESMAD de arremeter contra la población “con bombas de aturdimiento y gases lacrimógenos”.

Afirmó que las asambleas **se han hecho de manera permanente** pues deben analizar la situación de las personas heridas y además necesitan coordinar las actividades que hacen parte de la movilización. Allí, han documentado 10 personas heridas, “tras tres horas de ataques del ESMAD”. (Le puede interesar:["Campesinos completan 8 días de paro nacional"](https://archivo.contagioradio.com/paro-nacional-ocho-dias/))

### **Campesinos insisten en la instalación de la mesa de interlocución** 

El departamento del Cauca **es el único que ha instalado de manera satisfactoria** la mesa regional de interlocución y allí están coordinando la defensa de los derechos de las diferentes comunidades. Además, han logrado mantener un diálogo permanente con las autoridades regionales.

### **La sustitución de cultivos es punto crítico en la discusión** 

El lider de la movilización aseguró que lo más difícil de negociar está relacionado con la política de erradicación forzada en la medida “el Gobierno Nacional ha incumplido los acuerdos de sustitución voluntaria”. (Le puede interesar:"[Campesinos alertan sobre inminente ataque del ESMAD en Catatumbo"](https://archivo.contagioradio.com/campesinos-catatumbo-esmad/))

Finalmente, la organización Marcha Patriótica, junto con ASCAMCAT y COCCAM indicaron que seguirán dispuestos al diálogo con los gobiernos departamentales por lo que es necesario que “haya **garantías de seguridad para que el campesinado** movilizado pueda ejercer su derecho a la protesta pacífica”. Además, responsabilizaron al Estado y a las Fuerzas Militares por las afectaciones y violaciones a los derechos humanos que se presentan en la movilización campesina”.

<iframe id="audio_21801139" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21801139_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
