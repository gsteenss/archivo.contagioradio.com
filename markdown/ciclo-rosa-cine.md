Title: Vuelve el Ciclo Rosa a la Cinemateca distrital
Date: 2017-09-15 15:30
Author: AdminContagio
Category: 24 Cuadros
Tags: Bogotá, Ciclo rosa, Cine, cinemateca destrital, LGBTI
Slug: ciclo-rosa-cine
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/ciclo_rosa_ii.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Cinemateca Distrital 

###### 15 Sep 2017 

Una de las muestras de cine LGBTI más antiguas del continente vuelve a la capital del país en su edición número 16. El ciclo rosa, se llevará de la Cinemateca Distrital, contará con la proyección de más de 25 títulos nacionales e internacionales, talleres, conversatorios y paneles, con el propósito promover obras audiovisuales sobre diversidad sexual y de género.

Fundado en 2001, el Ciclo Rosa tendrá este año la curaduría internacional a cargo del escritor, productor y director alemán Wieland Speck, quien ha trabajado para la sección Panorama del Festival Internacional de Cine de Berlín. Igualmente este año se realizará por primera vez la curaduría latinoamericana Queer I: viejos y nuevos clásicos, a cargo del cofundador del ciclo Julián David Correa.

El componente académico de esta edición cuenta con dos talleres de fanzine: Fanzinografía de una Bogotá diversa; dos paneles, uno acompañado por Colombia Diversa sobre la defensa de los derechos humanos en la “ideología de género” y el otro organizado por el grupo Fronteras de la Universidad Javeriana sobre cuerpo, sexualidad y actitudes disidentes en el cine; finalmente también contarán con la presencia de la Dj turco – alemana y activista queer Tülin Duman

Juliana Restrepo, directora del Instituto Distrital de las Artes – IDARTES señaló: “El Ciclo Rosa es una oportunidad para demostrar que en Colombia celebramos la diversidad, que podemos ser un país que respete las diferencias; las diferencias estéticas, políticas, religiosas y sexuales. En la medida en que asumamos está posibilidad, a través del arte, estamos construyendo mejores y más humanas realidades”.

La muestra inicia el próximo 15 de septiembre e irá hasta el 24 del mismo mes. Para más información sobre la programación consulte a continuación.

[Programación Ciclo Rosa](https://www.scribd.com/document/359015646/Programacion-Ciclo-Rosa#from_embed "View Programación Ciclo Rosa on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_99908" class="scribd_iframe_embed" title="Programación Ciclo Rosa" src="https://www.scribd.com/embeds/359015646/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-ms8ZjHRYnl7ETOJ6csCx&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.23106546854942234"></iframe>
