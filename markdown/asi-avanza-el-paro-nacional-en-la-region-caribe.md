Title: Así avanza el paro nacional en la Región Caribe
Date: 2016-05-30 00:06
Category: Paro Nacional
Tags: Paro Nacional
Slug: asi-avanza-el-paro-nacional-en-la-region-caribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/REGION-CARIBA.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

### Magdalena 

**5 Junio**

Pescadores del corregimiento de Palmira, en el municipio de Pueblo Viejo, realizan un plantón en apoyo a la Minga Nacional.

### Córdoba 

**2 Junio**

Indígenas Zenú se mantienen alerta ante la continúa presión de la fuerza pública, justo cuando se conmemoran 15 años de la desaparición del líder Kimy Pernía Domicó. La guardía indígena Embera del Alto Sinú, fue interceptada en horas de la noche cuando prestaba su servicio de vigilancia, por una persona que se transportaba en una motocicleta de alto cilindraje, recibieron intimidaciones y palabras amenazantes, mientras acampa en la Gobernación de Córdoba.

**1 Junio**

Miembros del ESMAD atacaron a los indígenas Zenú mientras se movilizaban en la troncal del Caribe, a la altura del municipio de Chinú. Una mujer embarazada resultó herida, un guardia permanece desaparecido y otro fue detenido. En la plaza principal de Sahagún, el magisterio y diversas organizaciones se manifestaron. Más de 300 indígenas Embera se manifiestan en Montería en la sede de la Gobernación, con rituales de armonización, danzas y música propia, a la iniciativa se suman estudiantes del SENA y de la Universidad de Córdoba.

**31 Mayo**

Sobre las 10 de la mañana, 2 mil indígenas Zenú fueron atacados por el ESMAD con gases lacrimógenos en inmediaciones de la finca 'Tierra Grata', en la troncal del Caribe entre Chinu y Sagún, pese a que se había acordado con las autoridades el taponamiento de la vía por una hora. Cinco personas resultaron heridas, tres permanecen hospitalizadas, las otras dos ya están fuera de peligro, entre ellas un menor de 7 años que recibió los impactos en su rostro; el capitán del Cabildo Chinú, Nell Durango, y el comunero del Cabildo Menor de Achiote de Sampues, José Nicolás Montes, a  quien le hirieron su mano derecha.

**30 Mayo**

Los trabajadores que conforman el sindicato de Cerro Matoso, mina explotadora de níquel en Córdoba, anunciaron que iniciarán la huelga el próximo 2 de junio, luego de no lograr acuerdos en la negociación de la nueva convención para los trabajadores que implicaba costos adicionales de 12 millones de dólares.

### Cesar 

**10 Junio**

Campesinos e indígenas de la Sierra Nevada se manifiestan en Valledupar, mientras que en los corregimientos de La Mata y Besotes, las comunidades bloquean la Ruta del sol, con la presencia del ESMAD.

**9 Junio**

Tras 48 horas de huelga de hambre y 3 días de campamento, miembros del ESMAD, la Policía y el Ejército llegan en camiones a las instalaciones de la Unidad de Víctimas en Valledupar, a desalojar violentamente a las 200 familias que decidieron acampar allí, para exigir la implementación de programas de vivienda digna y proyectos productivos, que vienen reclamando desde hace más de siete años.

[**7 Junio **]

Mas de 100 familias se toman desde la madrugada, la sede de la Unidad de Víctimas, para reclamar la garantía de los derechos que les han sido vulnerados por la entidad.

[![Unidad de víctimas Valledupar](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Unidad-de-víctimas-Valledupar.jpg){.aligncenter .size-full .wp-image-25098 width="1280" height="720"}](https://archivo.contagioradio.com/asi-avanza-el-paro-nacional-en-la-region-caribe/unidad-de-victimas-valledupar/)

**6 Junio**

En el corregimiento Besote del municipio de La Gloria, miembros del Batallón Ricaurte del Ejército Nacional continúan merodeando las zonas aledañas al punto de concentración y específicamente los lugares que las comunidades tienen destinados para el abastecimiento de agua y el aseo personal. La Policía también continúa rondando el punto y en horas de la tarde, realizaron sobrevuelos.

Autoridades de los cuatro pueblos indígenas que continúan concentrados pacíficamente en la Plaza Alfonso López, llevaron a cabo un ritual en el que construyeron una mandala de armonización.

[![Plaza Alfonso López](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Plaza-Alfonso-López.jpg){.aligncenter .size-full .wp-image-25037 width="1280" height="960"}](https://archivo.contagioradio.com/asi-avanza-el-paro-nacional-en-la-region-caribe/plaza-alfonso-lopez/)

**5 Junio **

[En el corregimient<wbr></wbr>o La Mata del municipio de La Gloria, el campesino al que le allanaron su negocio, fue abordado por el patrullero Oscar Martínez que le decomisó la motocicleta en la que se movilizaba, diciéndole en actitud retadora que si quería informara a Naciones Unidas y a la Defensoría del Pueblo. Entretanto miembros del ESMAD volvieron a situarse frente al punto de concentración. En horas de la tarde un helicóptero de la Policía sobrevolo la zona. ]{lang="ES-TRAD"}

**4 Junio**

[En el corregimiento La Mata horas de la noche, una moto sin placas de la Policía Nacional se desplazó con dos uniformados frente al punto de concentración, vía en la que también transitó una patrulla policial que se comprometió a indagar por la motocicleta sin placas.]{lang="ES-TRAD"}

[En otro punto del corregimiento, miembros de la Policía allanaron un negocio de uno de los integrantes de Fedeagromisbol, a quien le acusaron de ser "un sapo colaborador de la guerrilla".]{lang="ES-TRAD"}

**3 Junio **

El líder integrante del Congreso de los Pueblos, Guillermo Pérez Rangel, recibió vía correo electrónico una amenaza de muerte o exilio.

**2 Junio**

En el Corregimiento La Mata del municipio de La Gloria, sobre las 10 de la mañana un hombre desconocido vestido de civil, recorrió el sector , en actitud sospechosa, en una moto sin placas pero con inscripción del ejército nacional. Después a las 11 de la mañana, un supuesto vendedor ambulante aseguró ser conocido por las comunidades pero al ser interrogado se retiró inmediatamente en un carro que pasa a recogerlo. Sobre el medio día, un efectivo del ESMAD vestido de civil incursionó en el caserío, luego una camioneta con vidrios polarizados se estacionó al lado del campamento. A las 7 de la noche arribaron dos camiones con más de 30 miembros del ESMAD que se suman a los que ya estaban alojados en este lugar. En el punto permanece una tanqueta, cuatro camiones y una camioneta de Policía.

Luego de que se registraran sobrevuelos de la Policía Nacional en el punto conocido como Besotes, sobre las 4 de la tarde, un dragoneante y soldados del secretario de gobierno departamental, ingresan de manera arbitraria al campamento, los campesinos solicitan su retiro, pero los efectivos permanecieron en el lugar.

"Informamos que el ejército nacional ha obstaculizado la vía donde los campesinos vamos a recoger el agua para preparar los alimentos y bañarse, además han cortado la manguera que se acerca el agua al sitio de concentración... Han sido constantes los sabotajes que la fuerza pública está haciendo en contra de la movilización pacífica, hace un par de días se incautó marihuana que iba a ser distribuida en el ejército nacional, hay vídeo y audios que confirman este tipo de situación"

[![Sobrevuelos Besotes Cesa](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Sobrevuelos-Besotes-Cesa.jpg){.aligncenter .size-full .wp-image-24797 width="600" height="450"}](https://archivo.contagioradio.com/asi-avanza-el-paro-nacional-en-la-region-caribe/sobrevuelos-besotes-cesa/)

**1 Junio**

A los integrantes de la Federación Agrominera del Sur de Bolívar, la armada nacional les obstaculizó el cruce de un brazo del río Magdalena. Tuvieron que permanecer por un tiempo en la finca La Florida, en el municipio de La Gloria, Cesar; luego los registraron en vídeo. En la plaza Alfonso López, hombre armados intentaron llevarse al líder Guillermo Pérez. El punto de concentración del corregimiento El Boquerón, en la Jagua, tuvo que levantarse por presencia de armados y de miembros de la fuerza pública que amordazaron a dos personas y amenazaron al resto.

**31 Mayo**

Las guardias indígenas de los pueblos Kankuamo y Yukpa se concentran en la plaza Alfonso López de Valledupar, a la que se espera también lleguen los Wiwas y Koguis.

**30 Mayo**

Desde el pasado jueves, en medio de una fuerte militarización, por lo menos 20 mil pobladores del Magdalena Medio, el Sur de Bolívar y Norte de Santander, se concentran en el municipio de San Martín  para exigir al gobierno nacional soluciones concretas a las problemáticas mineroenergéticas de larga data en la región. ([[Haga clic para obtener más información](http://bit.ly/25wuOMV)])

Por otra parte, desde las 10 de la mañana de este lunes, trabajadores de la mina La Francia iniciaron la jornada de movilización con bloqueos en la vía entre la Loma y Calenturitas y la Jagua de Ibirico.

### Atlántico 

**10 Junio**

Mujeres del cabildo indígena Mokana de Barranquilla, marchan desde el Puente de la Cordialidad con Circunvalar hasta la plazoleta del barrio La Paz, en apoyo a la Minga Nacional. También se movilizan las integrantes de La Red de Mujeres del Caribe, la Red de Mujeres Afro y la Red de Jóvenes del Sur Occidente. En el municipio de Campo de la Cruz las mujeres se solidarizaron con el paro y salieron a las calles a marchar.

[![Barranquilla Minga](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Barranquilla-Minga.jpg){.aligncenter .size-full .wp-image-25284 width="1200" height="896"}](https://archivo.contagioradio.com/asi-avanza-el-paro-nacional-en-la-region-caribe/barranquilla-minga/)

**9 Junio**

Agentes de la Policía y miembros del ESMAD ingresan a la Universidad del Atlántico y disparan con armas de fuego contra los estudiantes. En el hecho resultan heridas dos personas.

**8 Junio**

Desde las 6 de la tarde, organizaciones femeninas participan de un cacerolazo en el parque Estercita Forero.

[![Cacerolazo Barranquilla](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Cacerolazo-Barranquilla.jpg){.aligncenter .size-full .wp-image-25169 width="1040" height="584"}](https://archivo.contagioradio.com/asi-avanza-el-paro-nacional-en-la-region-caribe/cacerolazo-barranquilla/)

**7 Junio**

Organizaciones preparan la Minga Carnaval que se llevará a cabo entre el 10 y 11 de junio en el Jardín Botánico de Barranquilla.

**5 Junio**

En apoyo a la Minga Nacional, en exigencia de contratación directa de mano de obra local y en rechazo a la contaminación de la Bahía, organizaciones y sindicatos convocan una marcha en la ciudad de Barranquilla que partirá desde las instalaciones del SENA a partir de las 5 de la mañana.

**2 Junio **

Desde las 5 de la tarde en la Plaza de la Paz, comunidades y organizaciones se movilizan para exigir cumplimientos al gobierno y respaldar el proceso de paz.

**1 Junio **

Sindicatos del magisterio se movilizaron por las calles de Barranquilla en apoyo a la Minga Nacional y para exigir reformas estructurales en el sistema de atención en salud.

**31 Mayo**

Campesinos e indígenas Macana se toman la vía de Juan de Acosta, al sur del Atlántico. Este miércoles, organizaciones de mujeres marcharán por la Minga y la Vida Digna desde la sede de la Universidad del Atlántico en Barranquilla.

### Bolívar 

**10 Junio**

Integrantes del Proceso de Comunidades Negras marcharon en Cartagena para mostrar su apoyo a las jornadas de paro nacional.

[![PCN Cartagena](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/PCN-Cartagena.jpe){.aligncenter .size-full .wp-image-25283 width="1280" height="720"}](https://archivo.contagioradio.com/asi-avanza-el-paro-nacional-en-la-region-caribe/pcn-cartagena/)

**8 Junio**

[Desde tempranas horas, sindicatos y organizaciones marchan en la ciudad de Cartagena en apoyo a la Minga Nacional. ]

[![Marcha Minga Cartagena](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Marcha-Minga-Cartagena.jpg){.aligncenter .size-full .wp-image-25099 width="1280" height="960"}](https://archivo.contagioradio.com/asi-avanza-el-paro-nacional-en-la-region-caribe/marcha-minga-cartagena/)

**31 Mayo**

Pueblo indígena Macana de Baranoa, Galapa, Usiacuri, se toma la troncal cordialidad en la vía que del Atlántico conduce a Cartagena.

### La Guajira 

**7 Junio**

Desde tempranas horas, transportadores de carga pesada bloquean las vías del sur del departamento, en apoyo a la Minga Nacional y para denunciar los incumplimientos que el gobierno nacional ha tenido con el sector.

**4 Junio**

El Pueblo Wayúu del Resguardo Mayabangloma, se moviliza en Fonseca con las consignas de defensa de la paz, la vida, la niñez indígena, el agua y los territorios ancestrales.

[![Minga Nacional Región Caribe Wayuu](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Minga-Nacional-Región-Caribe-Wayuu.jpe){.aligncenter .size-full .wp-image-25005 width="1137" height="640"}](https://archivo.contagioradio.com/asi-avanza-el-paro-nacional-en-la-region-caribe/minga-nacional-region-caribe-wayuu/)

**2 Junio**

En el Kilómetro 28 del municipio de Albania, por lo menos mil indígenas de los pueblos Wayúu Wuare Wuaren, Yanama y Aiwa de Maicao, Rioacha y Manaure, bloquearon la vía férrea del Cerrejón, bajo la consigna ¡Pueblos Indígenas en defensa de los derechos de la niñez indígena, el agua, la vida y la Madre Tierra!. Un integrante de la SIJIN se infiltró.

**1 Junio **

Indígenas Wayúu, asociados en la Organización de Waya Wayuu, se movilizaron frente a las instalaciones del ICBF. Integrantes del Resguardo Mayabangloma siguen en la finca la Rebeca y el Palomar, para recuperar parte de su territorio ancestral. Líderes Wayuu de Aciwasug continúan movilizándose en la vía que al Cesar con La Guajira, en Barrancas, con bloqueos intermitentes.

**31 Mayo**

Líderes indígenas Wayuu de Hatonuevo, Barrancas y Distracción, bloquean la carretera en el corregimiento Papayal, para sumarse a esta Minga Nacional y exigir la presencia de la actual gobernadora. Sin embargo, quienes llegan son el ESMAD y el Ejército.

[![Minga Nacional Región Caribe](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Minga-Nacional-Región-Caribe.jpe){.aligncenter .size-full .wp-image-25006 width="1109" height="960"}](https://archivo.contagioradio.com/asi-avanza-el-paro-nacional-en-la-region-caribe/minga-nacional-region-caribe/)

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]][ ] 
