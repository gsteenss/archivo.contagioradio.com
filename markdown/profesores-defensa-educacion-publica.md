Title: Profesores se suman a la defensa de la educación pública
Date: 2018-10-16 16:00
Author: AdminContagio
Category: Educación, Movilización
Tags: estudiantes, fecode, marcha, profesores
Slug: profesores-defensa-educacion-publica
Status: published

###### [Foto: FECODE] 

###### [16 Oct 2018] 

En el punto más álgido de los debates en el Congreso para definir el Presupuesto General de la Nación 2019, **los estudiantes nuevamente realizarán este miércoles una movilización a nivel nacional para exigir que se destine más dinero para la educació**n, iniciativa a la que se sumarán los profesores,  quienes este jueves definirán como entrarán a la jornada de paro anunciada por el gremio.

<iframe src="https://co.ivoox.com/es/player_ek_29361864_2_1.html?data=k56gmJacepWhhpywj5WYaZS1lZyah5yncZOhhpywj5WRaZi3jpWah5yncaLixdeSpZiJhZrnjLLczsbSs4ampJDS1dnZqMrVz9nSjcnJb83VjLq7jdjTptPZjNLc2M7QrdvVxM7c0MqRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según el **estudiante de ingeniería de la Universidad Nacional, Andrés Camilo Molano**, para la movilización del próximo **17 de octubre** esperan que llegue el doble de personas que asistieron a la del pasado miércoles, buscando presionar al Gobierno Nacional para que inyecte mayores recursos al sector educación, en una semana que será definitiva para ajustar el Presupuesto General de la Nación.

El estudiante sostuvo que las movilizaciones en esta ocasión llegarán a sitios que no suelen frecuentar, para que las personas vean que el movimiento estudiantil está en acción; razón por la cual ya hay diferentes Instituciones de Educación Superior (IES) que tienen programados plantones y recorridos que transitarán por sectores fronterizos de diferentes ciudades.

En la marcha **participarán diferentes actores organizativos como son la UNEES, ASPU, FENARES y ACRES**; los cuales se reunieron el pasado lunes, para exigir conjuntamente que el Gobierno Nacional instale una mesa de negociaciones que permita levantar el cese de actividades en el que se encuentran varias universidades. (Le puede interesar: ["Por qué están marchando los estudiantes?"](https://archivo.contagioradio.com/marchando-los-estudiantes/))

### **FECODE se suma a la batalla en defensa de la educación pública**

<iframe src="https://co.ivoox.com/es/player_ek_29367652_2_1.html?data=k56gmJyaeZOhhpywj5WYaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5ynca7dyNrSzpCJh5SZmZbbycrQb7HV08nch5enb9Tjw9fSjdHFb8nj08aYxcrWs4zYxtGY0sbWs4zYxpDaw8qRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La Federación Colombia de Trabajadores de la Educación **(FECODE) ya anunció que la hora cero para iniciar el paro de maestros será el próximo jueves**, e igualmente, informó que se sumarán a la movilización estudiantil del próximo miércoles 17 de octubre. (Le puede interesar: ["Desde la media noche de hoy inicia la hora cero del paro nacional indefinido"](https://archivo.contagioradio.com/desde-la-media-noche-de-hoy-inicia-la-hora-cero-del-paro-nacional-estudiantil-indefinido/))

Según el **secretario de asuntos pedagógicos y científicos de la organización, Miguel Angel Pardo**, la junta nacional de la Federación se reunirá el próximo jueves para decidir la modalidad del paro; hecho que tiene que ver con el respaldo a la movilización nacional de los estudiantes, y que tiene en común la misma exigencia: Que haya más recursos para la educación primaria y básica.

El Secretario sostuvo que el principal acuerdo al que se llegó el año pasado con el Gobierno fue realizar una reforma constitucional para asignar más recursos al sector educación, salud y agua potable; no obstante, actualmente cursa en el Congreso el **Proyecto de Ley 057 que dejaría sin presupuesto a los colegios en 1.100 municipios de la república.** (Le puede interesar: ["Profesores se suman a la gran movilización estudiantil del 10 de octubre"](https://archivo.contagioradio.com/profesores-movilizacion-estudiantil/))

Tras la jornada de movilización y las declaratorias del paro nacional por parte de estudiantes y maestros, **el próximo fin de semana se llevará a cabo en la Universidad Distrital** Francisco José de Caldas, el encuentro entre diferentes actores para **llegar a acuerdos sobre los puntos que deberían hacer parte de la mesa de negociación con el Gobierno**, de cara a mejorar la situación de la educación pública en el país.

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
