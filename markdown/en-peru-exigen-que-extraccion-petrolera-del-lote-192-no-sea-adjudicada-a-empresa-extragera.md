Title: En Perú exigen que extracción petrolera del "Lote 192" no sea adjudicada a empresa extrajera
Date: 2015-09-02 16:51
Category: Uncategorized
Slug: en-peru-exigen-que-extraccion-petrolera-del-lote-192-no-sea-adjudicada-a-empresa-extragera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/lote.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.andina.com.pe]

Durante la semana pasada pobladores de la ciudad de Iquitos Perú, realizaron una movilización bloqueando las principales calles de la ciudad, exigiendo que no se adjudique el **lote 192 a la multinacional canadiense Pacific Exploration & Production Corporation.**

Cientos de personas se reunieron en la Plaza 28 de Julio desde las 4:00 de la madrugada del pasado jueves, pidiendo que **el predio sea otorgado a la empresa estatal Petroperú** y no a capital extranjero como muestra de la soberanía de ese país.

Juan Castillo More dirigente de la Coalición Nacional de Sindicatos de Petroperú aseguró que la noticia generó desconcierto y que la entrega del lote 192 a una empresa extranjera “se debe a un direccionamiento político”.

Lotero es una zona rica en petróleo, sus habitantes denuncian que las actividades de explotación de hidrocarburos **han traído graves consecuencias para el ambiente y las comunidades ancestrales,** razón por la cual los pobladores exigen que Petroperú sea la empresa encargada de tratar ese lote.

Cabe recordar que el lote 1AB también conocido como el **lote 192, es operado por Pluspetrol desde 2001** y su contrato terminó en agosto de este año. El bombeo habitual de este lote es de entre **15.000 y 17.000 barriles por día, lo que implica la cuarta parte de lo que se extrae diariamente en todo Perú.**
