Title: "Cuenten con nosotros para la paz, nunca para la guerra": ONIC
Date: 2018-08-09 16:03
Category: DDHH, Paz
Tags: Informe, JEP, ONIC, ONU
Slug: cuenten-con-nosotros-para-la-paz-onic
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/JEP-INDIGENAS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [9 Ago 2018] 

La Organización Nacional Indígena de Colombia (ONIC) entregó este jueves el primer informe de "Afectaciones individuales y Colectivas que han sufrido los pueblos indígenas de Colombia en el marco del conflicto armado interno" a la Jurisdicción Especial para la Paz (JEP), en el que se denuncian los **231.144 hechos victimizantes sufridos entre 1926 y 2017.**

La entrega del informe se produce en el Día Internacional de los Pueblos Indígenas,  fecha escogida a propósito por la ONIC para dar cuenta, en palabras de su Consejero Mayor Luis Fernando Arias, de la tragedia que **"han tenido que afrontar durante siglos y sobre todo en las últimas décadas"**.

### **219.501 desplazamientos han sufrido los indígenas desde 1926 hasta 2017** 

En el informe, se da cuenta de los hechos victimizantes de los que han sido víctimas los indígenas desde 1926 hasta 2017. Entre las denuncias están el asesinato de **2.954 líderes indígenas, 123 masacres y 1.884 acciones bélicas en sus territorios**. Además, según el Registro Único de Víctimas, en los 91 años en que se enmarca el informe, 219.501 indígenas han sufrido el desplazamiento.

Por su parte la Presidenta de la JEP, Patricia Linares, señaló que "estos informes dan cuenta de los 34 pueblos indígenas en peligro de extinción física y cultural", cifra aportada por la Corte Constitucional, por lo tanto, declaró que el documento no es sólo un reto, sino también un compromiso para la aplicación de una justicia restaurativa, seria y rigurosa.

Para Linares, el informe evidencia que los pueblos indígenas "han sufrido la guerra de manera directa, cruenta y evidente"; por esa razón merecen recibir justicia y acceso al sistema de Verdad, Justicia, Reparación y Garantías de No Repetición. Por su parte, la oficina del alto comisionado para los derechos humanos de la ONU en Colombia, destacó la entrega del escrito, y lo consideró "un paso fundamental para visibilizar, reconocer y reparar los impactos desproporcionados que han sufrido los pueblos indígenas durante el conflicto".

### **"Aquí no quisimos traer nuestros muertos, sino nuestros sueños"**

Linares afirmó que se debe avanzar en términos de verdad, pero no con el sentimiento de venganza, sino la intención de saber qué fue lo que pasó y así evitar que se repita. De igual forma, **el Consejero Mayor de la ONIC, Luis Fernando Arias,** afirmó que con el informe "no quisimos traer a nuestros muertos, sino nuestros sueños, que se abra la época de la vida y no de la muerte".

En medio de la entrega oficial del documento que hizo la Consejera para los Derechos Humanos de la **ONIC Aida Quilcué**, a la Sala de Reconocimiento de Verdad y Responsabilidad, aseguró que en el informe van contenidos los sueños y esperanzas de los indígenas en el marco de la justicia, la verdad y la garantía de no repetición.

Por su parte, Arias aseguró que la JEP es un ejemplo de la institucionalidad que evidencia a Colombia como un Estado pluriétnico y multicultural, por lo tanto espera que los resultados de este informe, y otros más que la ONIC presentará, sirvan para que la paz llegue a los territorios y goce del enfoque de género que fue incluido en los acuerdos de La Habana.

### **"A muchos territorios nuestros la paz no llegó"**

Aunque el Consejero Mayor sostuvo que valió la pena que se incluyera un enfoque étnico en el acuerdo de paz, a los territorios de los pueblos indígenas la paz no llegó porque después de la firma del Acuerdo, **“han sido asesinados  cerca de 70 dirigentes indígenas y se han cometido más de 10 mil violaciones a los derechos humanos"** en materia de confinamiento, desplazamiento, tortura y reclutamiento.

Con este informe, las comunidades quieren demostrar que en Colombia existe la “sistematicidad y continuidad del exterminio hacia los Pueblos Indígenas por parte de todos los actores del conflicto armado interno”, y que sigue ocurriendo este genocidio. A pesar de lo anterior, insistieron en mantener su consigna “Cuenten con nosotros para la paz, nunca para la guerra”. (Le puede interesar:["66% de los pueblos indígenas está a punto de desaparecer: ONIC"](https://archivo.contagioradio.com/pueblos-indigenas-a-punto-de-desaparecer-onic/))

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio](http://bit.ly/1ICYhVU)]
