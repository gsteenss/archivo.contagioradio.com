Title: Se mantienen las fuertes protestas en Perú contra proyecto minero "Tía María"
Date: 2015-05-15 17:48
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Cocachacra, Protestas contra proyecto minero Tía María en Perú, Puerto mollendo, Rocío Silva Coordinadora de DDHH nacional Perú, Tía María proyecto minero Perú
Slug: se-mantienen-las-fuertes-protestas-en-peru-contra-proyecto-minero-tia-maria
Status: published

###### Foto:Cronicaviva.com.pe 

###### **Entrevista con [Rocío Silva], representante Coordinadora nacional de DDHH Perú:** 

<iframe src="http://www.ivoox.com/player_ek_4501080_2_1.html?data=lZqdk5WcdI6ZmKiak5mJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkNbXycaYxdTSuNPVjMrZjdXWs9rZxNncjdLNssbm0JDBh6iXaaK4wpC6w9eJh5SZoqnOjcrSb7HZ04qwlYqmhY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**53 días de protestas** contra el proyecto **minero "Tía María"** en la región de Arequipa en **Perú dejan 5 muertos y más de 200 heridos**, mientras el gobierno continúa sin pronunciarse sobre la concesión minera que rechaza el segundo departamento más grande del país y todos los sectores sociales de las poblaciones afectadas.

El proyecto minero "Tía María" fue presentado en **2011,** por la **multinacional de capital Mexicano Southern Perú Copper Corporation.** Es en este momento cuando ya hubo un primer conato de protestas que dejó **3 manifestantes muertos**. Entonces las organizaciones sociales de DDHH y afectados de la región demandaron a Naciones Unidas realizar un informe sobre las afectaciones en un futuro en el caso de que se aplicara.

Fue entonces cuando **UNOC emitió el informe en el que se advertía de 137 observaciones en referencia daños ambientales y de carácter social y económico para la región.**

El proyecto se desarrollaría en una zona árida, por un lado, y en otra zona altamente fructífera conocida como el **Valle del Tambo, donde más del 80% de la economía depende de la agricultura.** En total 13.000 hectáreas quedarían afectadas por el proyecto y en **grave riesgo de secarse el río Tambo.**

La afectación en el valle dejaría **sin agua para poder cultivar**, dejando toda la región sin la zona más prospera donde los agricultores y campesinos dependen de las exportaciones de fruta y verdura.

La concesión minera también **afectaría al Puerto de Mollendo, donde se instalaría una desalinizadora** con el objetivo de extraer más agua, los residuos vertidos por la planta y la propia infraestructura afectarían ambientalmente al puerto más importante del país que conecta el atlántico con el Pacífico.

En **2014** la multinacional encarga un estudio en el que refleja que las 137 observaciones del informe de UNOC ya se han solventado, se presenta al gobierno, y éste sin verificarlo **adjudica la concesión minera para la región de Arequipa.**

El anuncio de la concesión dio paso a las protestas que tienen paralizada la región con 53 días de paro local y **72 horas de paro total en Cocachacra,** a estas se han unido los estudiantes y las principales centrales sindicales del país.

Según **Rocío Silva, representante de la Coordinadora Nacional para los DDHH en Perú**, el gobierno está actuando inadecuadamente y en las **últimas protestas** ya han **muerto 5 personas**, dos por ataques al corazón, dos por proyectiles de la policía y un policía por una piedra.

Para Silva la **violaciones de DDHH están a la orden del día**, en especial en el municipio de **Cocachacra,** donde la policía ha sido grabada sembrando armas a campesinos.

La representante de DDHH también afirma que el **ejército esta patrullando** en la región y que el gobierno permite que la **policía sea subcontratada en secreto por las multinacionales para ejercer su defensa. **

Además hoy ha sido **detenido el conocido líder antiminería Pepe Julio Gutierrez**, conocido como el **"Lentejas"**, en otro acto más de represión del gobierno que esta actuando como abanderado de la multinacional.
