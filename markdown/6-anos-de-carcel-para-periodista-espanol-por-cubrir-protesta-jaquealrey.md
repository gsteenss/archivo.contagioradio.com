Title: 6 años de cárcel para periodista Español por cubrir protesta #JaqueAlRey
Date: 2015-05-14 17:08
Author: CtgAdm
Category: DDHH, El mundo
Tags: #JaqueAlRey, 7 periodistas españoles agredidos durante protestas contra el rey, españa, Lahaine.org, Marchas de la dignidad, Piden 6 años de cárcel para periodista español por la protesta #JaqueAlRey
Slug: 6-anos-de-carcel-para-periodista-espanol-por-cubrir-protesta-jaquealrey
Status: published

###### Foto:CuartoPoder.es 

###### [14 May 2015]

El 29 de Marzo de 2014, el periodista de la editorial de contrainformación **Lahaine.org**, conocido como Boro se desplazó hasta Madrid para cubrir la protesta \#JaqueAlRey, cuando fue agredido junto con otros 6 periodistas por agentes de la policía nacional.

A más de un año del suceso la fiscalía pide 6 años y una multa de 6.200 euros para el periodista por delitos contra la autoridad y lesiones a dos agentes de policía.

En el marco de las protestas de las **Marchas de la dignidad** y ante la abdicación del Rey y la coronación de su hijo sin ningún tipo de participación ciudadana, a pesar de que las encuestas eran totalmente desfavorables, organizaciones sociales convocaron una protesta que fue prohibida y que la policía reprimió.

La protesta conocida como **\#JaqueAlRey** fue disuelta y la policía cargo contra los manifestantes, poco después de finalizada la marcha, Boro se dirigía con su compañera a coger el coche cuando vio como dos furgonetas de la policía nacional paraban. De las furgonetas bajaron policías y comenzaron a agredir a otros periodistas de medios como Eldiario.es, Periodismo humano, Periodismo Digno y otros periodistas freelance.

En el momento de la detención del periodista se podían escuchar los gritos !Soy periodista de Lahaine.org, esta detención es ilegal!.

Los agentes de la policía lo denunciaron alegando que les había agredido y se tomaron en total 30 días de baja, ahora la fiscalía quiere inculpar al periodista solo con los testimonios de la policía en contra de los videos y las fotos como prueba de lo sucedido.

Video de toda la secuencia, agresión policial, golpiza a periodistas y detención del periodista de Lahaine:

\[embed\]https://www.youtube.com/watch?v=yNP-74rCIzc\[/embed\]  
Galería de fotos de la secuencia de la agresión y de la detención:  
\
